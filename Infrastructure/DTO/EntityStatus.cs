﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.DTO
{
    public static class EntityStatus
    {
        public  static string ACTIVE = "ACTIVE";
        public static string DELETED = "DELETED";
        public static string INACTIVE = "INACTIVE";
        public static string UPDATED = "UPDATED";


        public static string PENDING = "PENDING";
        public static string COMPLETED = "COMPLETED";
    }

    public static class Admission_Status
    {
      
        public static string REGISTERED = "REGISTERED";
        public static string PAID = "PAID";
        
    }
    public static class APP_TYPE
    {

        public static string ANDROID = "ANDROID";
        public static string IOS = "IOS";
        public static string WEB = "WEB";

    }
    public static class LOGIN_TYPE
    {

        public static string ADMISSION_LOGIN = "ADMISSION_LOGIN";
        public static string TRACQ_LOGIN = "TRACQ_LOGIN";

    }
    public static class RELATED_TYPE
    {

        public static string ADMISSION = "ADMISSION";
        public static string EMPLOYEE = "TRACQ";

    }
}
