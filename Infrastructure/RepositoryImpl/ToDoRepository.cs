﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.TodoAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
  public class TodoRepository: IToDoRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public TodoRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateTodo(Todo newToDo)
        {
            //try
            //{
            newToDo.InsertedDate = DateTime.Now;
            newToDo.ModifiedDate = DateTime.Now;
            _DbContext.Set<Todo>().Add(newToDo);
            _DbContext.SaveChanges();
            return newToDo.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }


        public bool DeleteTodo(long id)
        {
            try
            {
                Todo ob = _DbContext.Set<Todo>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Todos.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllTodo()
        {
            try
            {
                List<Todo> res = _DbContext.Set<Todo>().AsNoTracking().ToListAsync().Result;
                foreach (Todo ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Todos.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Todo> GetAllTodo()
        {
            try
            {
                IEnumerable<Todo> res = _DbContext.Set<Todo>().AsNoTracking().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception e1)
            {
                return null;
            }
        }

        public Todo GetTodoById(long id)
        {
            try
            {
                Todo ob = _DbContext.Set<Todo>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateTodo(Todo changedTodo)
        {
            try
            {
                changedTodo.ModifiedDate = DateTime.Now;
                _DbContext.Todos.Attach(changedTodo);
                var entry = _DbContext.Entry(changedTodo);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public long CreateTodoAttachment(TodoAttachment newTodoAttachment)
        {
            //try
            //{
            newTodoAttachment.InsertedDate = DateTime.Now;
            newTodoAttachment.ModifiedDate = DateTime.Now;
            _DbContext.Set<TodoAttachment>().Add(newTodoAttachment);
            _DbContext.SaveChanges();
            return newTodoAttachment.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }


        public bool DeleteTodoAttachment(long id)
        {
            try
            {
                TodoAttachment ob = _DbContext.Set<TodoAttachment>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.TodoAttachments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllTodoAttachment()
        {
            try
            {
                List<TodoAttachment> res = _DbContext.Set<TodoAttachment>().AsNoTracking().ToListAsync().Result;
                foreach (TodoAttachment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.TodoAttachments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<TodoAttachment> GetAllTodoAttachment()
        {
            try
            {
                IEnumerable<TodoAttachment> res = _DbContext.Set<TodoAttachment>().AsNoTracking().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public TodoAttachment GetTodoAttachmentById(long id)
        {
            try
            {
                TodoAttachment ob = _DbContext.Set<TodoAttachment>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateTodoAttachment(TodoAttachment changedTodoAttachment)
        {
            try
            {
                changedTodoAttachment.ModifiedDate = DateTime.Now;
                changedTodoAttachment.Status = EntityStatus.ACTIVE;
                _DbContext.TodoAttachments.Attach(changedTodoAttachment);
                var entry = _DbContext.Entry(changedTodoAttachment);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public long CreateTodoComment(TodoComment newTodoComment)
        {
            //try
            //{
            newTodoComment.InsertedDate = DateTime.Now;
            newTodoComment.ModifiedDate = DateTime.Now;
            _DbContext.Set<TodoComment>().Add(newTodoComment);
            _DbContext.SaveChanges();
            return newTodoComment.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }


        public bool DeleteTodoComment(long id)
        {
            try
            {
                TodoComment ob = _DbContext.Set<TodoComment>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.TodoComments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllTodoComment()
        {
            try
            {
                List<TodoComment> res = _DbContext.Set<TodoComment>().AsNoTracking().ToListAsync().Result;
                foreach (TodoComment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.TodoComments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<TodoComment> GetAllTodoComment()
        {
            try
            {
                IEnumerable<TodoComment> res = _DbContext.Set<TodoComment>().AsNoTracking().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public TodoComment GetTodoCommentById(long id)
        {
            try
            {
                TodoComment ob = _DbContext.Set<TodoComment>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateTodoComment(TodoComment changedTodoComment)
        {
            try
            {
                changedTodoComment.ModifiedDate = DateTime.Now;
                changedTodoComment.Status = EntityStatus.ACTIVE;
                _DbContext.TodoComments.Attach(changedTodoComment);
                var entry = _DbContext.Entry(changedTodoComment);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public long CreateTodoCommentAttachment(TodoCommentAttachment newTodoCommentAttachment)
        {
            //try
            //{
            newTodoCommentAttachment.InsertedDate = DateTime.Now;
            newTodoCommentAttachment.ModifiedDate = DateTime.Now;
            _DbContext.Set<TodoCommentAttachment>().Add(newTodoCommentAttachment);
            _DbContext.SaveChanges();
            return newTodoCommentAttachment.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }


        public bool DeleteTodoCommentAttachment(long id)
        {
            try
            {
                TodoCommentAttachment ob = _DbContext.Set<TodoCommentAttachment>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.TodoCommentAttachments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllTodoCommentAttachment()
        {
            try
            {
                List<TodoCommentAttachment> res = _DbContext.Set<TodoCommentAttachment>().AsNoTracking().ToListAsync().Result;
                foreach (TodoCommentAttachment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.TodoCommentAttachments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<TodoCommentAttachment> GetAllTodoCommentAttachment()
        {
            try
            {
                IEnumerable<TodoCommentAttachment> res = _DbContext.Set<TodoCommentAttachment>().AsNoTracking().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public TodoCommentAttachment GetTodoCommentAttachmentById(long id)
        {
            try
            {
                TodoCommentAttachment ob = _DbContext.Set<TodoCommentAttachment>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateTodoCommentAttachment(TodoCommentAttachment changedTodoCommentAttachment)
        {
            try
            {
                changedTodoCommentAttachment.ModifiedDate = DateTime.Now;
                changedTodoCommentAttachment.Status = EntityStatus.ACTIVE;
                _DbContext.TodoCommentAttachments.Attach(changedTodoCommentAttachment);
                var entry = _DbContext.Entry(changedTodoCommentAttachment);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public long CreateTodoPriority(TodoPriority newTodoPriority)
        {
            //try
            //{
            newTodoPriority.InsertedDate = DateTime.Now;
            newTodoPriority.ModifiedDate = DateTime.Now;
            _DbContext.Set<TodoPriority>().Add(newTodoPriority);
            _DbContext.SaveChanges();
            return newTodoPriority.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }


        public bool DeleteTodoPriority(long id)
        {
            try
            {
                TodoPriority ob = _DbContext.Set<TodoPriority>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.TodoPriorities.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllTodoPriority()
        {
            try
            {
                List<TodoPriority> res = _DbContext.Set<TodoPriority>().AsNoTracking().ToListAsync().Result;
                foreach (TodoPriority ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.TodoPriorities.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<TodoPriority> GetAllTodoPriority()
        {
            //try
           // {
                IEnumerable<TodoPriority> res = _DbContext.Set<TodoPriority>().AsNoTracking().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            //}
           // catch
           // {
               // return null;
           // }
        }

        public TodoPriority GetTodoPriorityById(long id)
        {
            try
            {
                TodoPriority ob = _DbContext.Set<TodoPriority>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateTodoPriority(TodoPriority changedTodoPriority)
        {
            try
            {
                changedTodoPriority.ModifiedDate = DateTime.Now;
                changedTodoPriority.Status = EntityStatus.ACTIVE;
                _DbContext.TodoPriorities.Attach(changedTodoPriority);
                var entry = _DbContext.Entry(changedTodoPriority);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public long CreateTodoStatus(TodoStatus newTodoStatus)
        {
            //try
            //{
            newTodoStatus.InsertedDate = DateTime.Now;
            newTodoStatus.ModifiedDate = DateTime.Now;
            _DbContext.Set<TodoStatus>().Add(newTodoStatus);
            _DbContext.SaveChanges();
            return newTodoStatus.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }


        public bool DeleteTodoStatus(long id)
        {
            try
            {
                TodoStatus ob = _DbContext.Set<TodoStatus>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.TodoStatus.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllTodoStatus()
        {
            try
            {
                List<TodoStatus> res = _DbContext.Set<TodoStatus>().AsNoTracking().ToListAsync().Result;
                foreach (TodoStatus ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.TodoStatus.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<TodoStatus> GetAllTodoStatus()
        {
            try
            {
                IEnumerable<TodoStatus> res = _DbContext.Set<TodoStatus>().AsNoTracking().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public TodoStatus GetTodoStatusById(long id)
        {
            try
            {
                TodoStatus ob = _DbContext.Set<TodoStatus>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateTodoStatus(TodoStatus changedTodoStatus)
        {
            try
            {
                changedTodoStatus.ModifiedDate = DateTime.Now;
                changedTodoStatus.Status = EntityStatus.ACTIVE;
                _DbContext.TodoStatus.Attach(changedTodoStatus);
                var entry = _DbContext.Entry(changedTodoStatus);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public long CreateTodoTimeline(TodoTimeline newTodoTimeline)
        {
            //try
            //{
            newTodoTimeline.InsertedDate = DateTime.Now;
            newTodoTimeline.ModifiedDate = DateTime.Now;
            _DbContext.Set<TodoTimeline>().Add(newTodoTimeline);
            _DbContext.SaveChanges();
            return newTodoTimeline.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }


        public bool DeleteTodoTimeline(long id)
        {
            try
            {
                TodoTimeline ob = _DbContext.Set<TodoTimeline>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.TodoTimelines.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllTodoTimeline()
        {
            try
            {
                List<TodoTimeline> res = _DbContext.Set<TodoTimeline>().AsNoTracking().ToListAsync().Result;
                foreach (TodoTimeline ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.TodoTimelines.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<TodoTimeline> GetAllTodoTimeline()
        {
            try
            {
                IEnumerable<TodoTimeline> res = _DbContext.Set<TodoTimeline>().AsNoTracking().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public TodoTimeline GetTodoTimelineById(long id)
        {
            try
            {
                TodoTimeline ob = _DbContext.Set<TodoTimeline>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateTodoTimeline(TodoTimeline changedTodoTimeline)
        {
            try
            {
                changedTodoTimeline.ModifiedDate = DateTime.Now;
                changedTodoTimeline.Status = EntityStatus.ACTIVE;
                _DbContext.TodoTimelines.Attach(changedTodoTimeline);
                var entry = _DbContext.Entry(changedTodoTimeline);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #region TodoStatusDetails
        public long CreateTodoStatusDetails(TodoStatusDetails newTodoStatusDetails)
        {
            //try
            //{
            newTodoStatusDetails.InsertedDate = DateTime.Now;
            newTodoStatusDetails.ModifiedDate = DateTime.Now;
            _DbContext.Set<TodoStatusDetails>().Add(newTodoStatusDetails);
            _DbContext.SaveChanges();
            return newTodoStatusDetails.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }


        public bool DeleteTodoStatusDetails(long id)
        {
            try
            {
                TodoStatusDetails ob = _DbContext.Set<TodoStatusDetails>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.TodoStatusDetails.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllTodoStatusDetails()
        {
            try
            {
                List<TodoStatusDetails> res = _DbContext.Set<TodoStatusDetails>().AsNoTracking().ToListAsync().Result;
                foreach (TodoStatusDetails ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.TodoStatusDetails.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<TodoStatusDetails> GetAllTodoStatusDetails()
        {
            try
            {
                IEnumerable<TodoStatusDetails> res = _DbContext.Set<TodoStatusDetails>().AsNoTracking().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public TodoStatusDetails GetTodoStatusDetailsById(long id)
        {
            try
            {
                TodoStatusDetails ob = _DbContext.Set<TodoStatusDetails>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateTodoStatusDetails(TodoStatusDetails changedTodoStatus)
        {
            try
            {
                changedTodoStatus.ModifiedDate = DateTime.Now;
                _DbContext.TodoStatusDetails.Attach(changedTodoStatus);
                var entry = _DbContext.Entry(changedTodoStatus);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

    }
}
