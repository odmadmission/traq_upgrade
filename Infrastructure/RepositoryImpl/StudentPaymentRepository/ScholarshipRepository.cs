﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using X.PagedList;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class ScholarshipRepository : IScholarshipRepository
    {
        private readonly ApplicationDbContext _DbContext;
        public ScholarshipRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateScholarship(Scholarship newScholarship)
        {
            try
            {
                newScholarship.InsertedDate = DateTime.Now;
                newScholarship.ModifiedDate = DateTime.Now;
                _DbContext.Set<Scholarship>().Add(newScholarship);
                _DbContext.SaveChanges();
                return newScholarship.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteAllScholarship()
        {
            try
            {
                List<Scholarship> res = _DbContext.Set<Scholarship>().ToListAsync().Result;
                foreach (Scholarship ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Scholarships.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteScholarship(long id)
        {
            try
            {
                Scholarship ob = _DbContext.Set<Scholarship>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Scholarships.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        //GetAllScholarshipData
        public IEnumerable<Scholarship> GetAllScholarshipData()
        {
            try
            {

                // return View(onePageOfEmps);
                IEnumerable<Scholarship> res = _DbContext.Set<Scholarship>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public PaginationScholarship GetPaginationScholarship(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue)
        {
            
                var v = (from a in _DbContext.Scholarships where a.Active==true && a.Name.Contains(searchValue) select a);
                //Sorting
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    if (sortColumnDir == "asc")
                    {
                        if (sortColumn == "Name")
                        {
                            v = v.OrderByDescending(a => a.Name);
                        }


                    }
                    //v = v.OrderBy(sortColumn + " " + sortColumnDir);
                }

                totalRecords = v.Count();
                var data = v.Skip(skip).Take(pageSize).ToList();
            //var fd = JsonSerializer.Serialize(data);
            PaginationScholarship str = new PaginationScholarship { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data };
            //var dfdf = JsonSerializer.Serialize(str);
            // return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });
            return str;
        }
        public IEnumerable<Scholarship> GetAllScholarship(int? page, int? noofData)
        {
            try
            {
                var pageNumber = page ?? 1;
                var PageSize = noofData ?? 10;
                var onePageOfEmps = _DbContext.Scholarships.ToPagedList(pageNumber, PageSize);
               // return View(onePageOfEmps);
                IEnumerable<Scholarship> res = _DbContext.Set<Scholarship>().Where(a => a.Active == true).ToListAsync().Result;
                return onePageOfEmps;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
       
       

        public Scholarship GetScholarshipById(long id)
        {
            try
            {
                Scholarship ob = _DbContext.Set<Scholarship>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public Scholarship GetScholarshipByName(string name)
        {
            try
            {
                Scholarship ob = _DbContext.Set<Scholarship>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateScholarship(Scholarship changedScholarship)
        {
            try
            {
                changedScholarship.ModifiedDate = DateTime.Now;
                changedScholarship.Status = changedScholarship.Status;
                _DbContext.Scholarships.Attach(changedScholarship);
                var entry = _DbContext.Entry(changedScholarship);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
