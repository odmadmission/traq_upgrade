﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.StudentPaymentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository
{
    public class PaymentTypeRepository:IPaymentType
    {
        private readonly ApplicationDbContext _DbContext;
        public PaymentTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreatePaymentType(PaymentType newPaymentType)
        {
            try
            {
                newPaymentType.InsertedDate = DateTime.Now;
                newPaymentType.ModifiedDate = DateTime.Now;
                _DbContext.Set<PaymentType>().Add(newPaymentType);
                _DbContext.SaveChanges();
                return newPaymentType.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public IEnumerable<PaymentType> GetAllPaymentType()
        {
            try
            {
                IEnumerable<PaymentType> res = _DbContext.Set<PaymentType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool UpdatePaymentType(PaymentType changePaymentType)
        {
            try
            {
                changePaymentType.ModifiedDate = DateTime.Now;
                changePaymentType.Status = "UPDATED";
                _DbContext.PaymentType.Attach(changePaymentType);
                var entry = _DbContext.Entry(changePaymentType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public PaymentType GetPaymentTypeById(long id)
        {
            try
            {
                PaymentType ob = _DbContext.Set<PaymentType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
     
        public PaymentType GetPaymentTypeByName(string name)
        {
            try
            {
                PaymentType ob = _DbContext.Set<PaymentType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public bool DeleteAllPaymentType()
        {
            try
            {
                List<PaymentType> res = _DbContext.Set<PaymentType>().ToListAsync().Result;
                foreach (PaymentType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.PaymentType.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeletePaymentType(long id)
        {
            try
            {
                PaymentType ob = _DbContext.Set<PaymentType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.PaymentType.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public PaymentType GetPaymentTypeByBillDeskCode(string code)
        {
            try
            {
                PaymentType ob = _DbContext.Set<PaymentType>().
                    FirstOrDefaultAsync(a => a.BillDeskTransactionCode
                    == code).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
    }
}
