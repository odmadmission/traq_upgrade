﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.SessionFeesType;
using OdmErp.ApplicationCore.Entities.Pagination;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository
{
  public  class SessionFessTypeRepository :ISessionFeesType
    {
        private readonly ApplicationDbContext _DbContext;
        public SessionFessTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateSessionFeesType(SessionFeesType newSessionFeesType)
        {
            try
            {
                newSessionFeesType.InsertedDate = DateTime.Now;
                newSessionFeesType.ModifiedDate = DateTime.Now;
                _DbContext.Set<SessionFeesType>().Add(newSessionFeesType);
                _DbContext.SaveChanges();
                return newSessionFeesType.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public IEnumerable<SessionFeesType> GetAllSessionFeesType()
        {
            try
            {
                IEnumerable<SessionFeesType> res = _DbContext.Set<SessionFeesType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool UpdateSessionFeesType(SessionFeesType changeSesseionFeesType)
        {
            try
            {
                changeSesseionFeesType.ModifiedDate = DateTime.Now;
                //changeSesseionFeesType.Status = "ACTIVE";
                _DbContext.SessionFeesType.Attach(changeSesseionFeesType);
                var entry = _DbContext.Entry(changeSesseionFeesType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public SessionFeesType GetSessionFeesTypeById(long id)
        {
            try
            {
                SessionFeesType ob = _DbContext.Set<SessionFeesType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        //public SesseionFeesType GetSessionFeesTypePriceByName(string name)
        //{
        //    try
        //    {
        //        SesseionFeesType ob = _DbContext.Set<SesseionFeesType>().FirstOrDefaultAsync(a => a. == name).Result;
        //        return ob;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}
        public bool DeleteAllSessionFeesType()
        {
            try
            {
                List<SessionFeesType> res = _DbContext.Set<SessionFeesType>().ToListAsync().Result;
                foreach (SessionFeesType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.SessionFeesType.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteSessionFeesType(long id)
        {
            try
            {
                SessionFeesType ob = _DbContext.Set<SessionFeesType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.SessionFeesType.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SessionFeesType> CheckSessionFeesType()
        {
            try
            {
                IEnumerable<SessionFeesType> res = _DbContext.Set<SessionFeesType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //public PaginationSessionFesstype GetPaginationAllSessionFeesType(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue)
        //{
        //   // var v = CommonSchoolModel.GetAllSessionFeesType().ToPagedList();
        //   var v = (from a in _DbContext.SessionFeesTypeView where a.Active == true && a.FeesTypeName.Contains(searchValue) select a);
        //    //Sorting
        //    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
        //    {
        //        if (sortColumnDir == "asc")
        //        {
        //            if (sortColumn == "Name")
        //            {
        //                v = v.OrderByDescending(a => a.FeesTypeName);
        //            }
        //        }
        //        //v = v.OrderBy(sortColumn + " " + sortColumnDir);
        //    }

        //    totalRecords = v.Count();
        //    var data = v.Skip(skip).Take(pageSize).ToList();
        //    //var fd = JsonSerializer.Serialize(data);
        //    PaginationSessionFesstype str = new PaginationSessionFesstype { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data };
        //    //var dfdf = JsonSerializer.Serialize(str);
        //    // return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });
        //    return str;
        //}
    }
}
