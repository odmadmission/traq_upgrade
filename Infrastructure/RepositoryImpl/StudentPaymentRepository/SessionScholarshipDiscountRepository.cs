﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class SessionScholarshipDiscountRepository : ISessionScholarshipDiscountRepository
    {
        private readonly ApplicationDbContext _DbContext;
        public SessionScholarshipDiscountRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateSessionScholarshipDiscount(SessionScholarshipDiscount newSessionScholarshipDiscount)
        {
            try
            {
                newSessionScholarshipDiscount.InsertedDate = DateTime.Now;
                newSessionScholarshipDiscount.ModifiedDate = DateTime.Now;
                _DbContext.Set<SessionScholarshipDiscount>().Add(newSessionScholarshipDiscount);
                _DbContext.SaveChanges();
                return newSessionScholarshipDiscount.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteAllSessionScholarshipDiscount()
        {
            try
            {
                List<SessionScholarshipDiscount> res = _DbContext.Set<SessionScholarshipDiscount>().ToListAsync().Result;
                foreach (SessionScholarshipDiscount ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.SessionScholarshipDiscounts.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteSessionScholarshipDiscount(long id)
        {
            try
            {
                SessionScholarshipDiscount ob = _DbContext.Set<SessionScholarshipDiscount>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.SessionScholarshipDiscounts.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SessionScholarshipDiscount> GetAllSessionScholarshipDiscount()
        {
            try
            {

                // return View(onePageOfEmps);
                IEnumerable<SessionScholarshipDiscount> res = _DbContext.Set<SessionScholarshipDiscount>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public PaginationSessionScholarshipDiscount GetPaginationSessionScholarshipDiscount(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue)
        {
            var v = (from a in _DbContext.SessionScholarshipDiscounts where a.Active == true && a.Name.Contains(searchValue) select a);
            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                if (sortColumnDir == "asc")
                {
                    if (sortColumn == "Name")
                    {
                        v = v.OrderByDescending(a => a.Name);
                    }


                }
                //v = v.OrderBy(sortColumn + " " + sortColumnDir);
            }

            totalRecords = v.Count();
            var data = v.Skip(skip).Take(pageSize).ToList();
            //var fd = JsonSerializer.Serialize(data);
            PaginationSessionScholarshipDiscount str = new PaginationSessionScholarshipDiscount { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data };
            //var dfdf = JsonSerializer.Serialize(str);
            // return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });
            return str;
        }

        public SessionScholarshipDiscount GetSessionScholarshipDiscountById(long id)
        {
            try
            {
                SessionScholarshipDiscount ob = _DbContext.Set<SessionScholarshipDiscount>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public SessionScholarshipDiscount GetSessionScholarshipDiscountByName(string name)
        {
            try
            {
                SessionScholarshipDiscount ob = _DbContext.Set<SessionScholarshipDiscount>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateSessionScholarshipDiscount(SessionScholarshipDiscount changedSessionScholarshipDiscount)
        {
            try
            {
                changedSessionScholarshipDiscount.ModifiedDate = DateTime.Now;
                changedSessionScholarshipDiscount.Status = changedSessionScholarshipDiscount.Status;
                _DbContext.SessionScholarshipDiscounts.Attach(changedSessionScholarshipDiscount);
                var entry = _DbContext.Entry(changedSessionScholarshipDiscount);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
