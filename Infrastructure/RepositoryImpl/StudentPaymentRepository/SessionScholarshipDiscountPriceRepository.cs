﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository
{
    public class SessionScholarshipDiscountPriceRepository : ISessionScholarshipDiscountPriceRepository
    {
        private readonly ApplicationDbContext _DbContext;
        public SessionScholarshipDiscountPriceRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateSessionScholarshipDiscountPrice(SessionScholarshipDiscountPrice newSessionScholarshipDiscountPrice)
        {
            try
            {
                newSessionScholarshipDiscountPrice.InsertedDate = DateTime.Now;
                newSessionScholarshipDiscountPrice.ModifiedDate = DateTime.Now;
                _DbContext.Set<SessionScholarshipDiscountPrice>().Add(newSessionScholarshipDiscountPrice);
                _DbContext.SaveChanges();
                return newSessionScholarshipDiscountPrice.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteAllSessionScholarshipDiscountPrice()
        {
            try
            {
                List<SessionScholarshipDiscountPrice> res = _DbContext.Set<SessionScholarshipDiscountPrice>().ToListAsync().Result;
                foreach (SessionScholarshipDiscountPrice ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.SessionScholarshipDiscountPrices.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteSessionScholarshipDiscountPrice(long id)
        {
            try
            {
                SessionScholarshipDiscountPrice ob = _DbContext.Set<SessionScholarshipDiscountPrice>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.SessionScholarshipDiscountPrices.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SessionScholarshipDiscountPrice> GetAllSessionScholarshipDiscountPrice()
        {
            try
            {

                // return View(onePageOfEmps);
                IEnumerable<SessionScholarshipDiscountPrice> res = _DbContext.Set<SessionScholarshipDiscountPrice>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public PaginationSessionScholarshipDiscountPrice GetPaginationSessionScholarshipDiscountPrice(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue)
        {
            throw new NotImplementedException();
        }

        public SessionScholarshipDiscountPrice GetSessionScholarshipDiscountPriceById(long id)
        {
            try
            {
                SessionScholarshipDiscountPrice ob = _DbContext.Set<SessionScholarshipDiscountPrice>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public SessionScholarshipDiscountPrice GetSessionScholarshipDiscountPriceByName(string name)
        {
            throw new NotImplementedException();
        }

        public bool UpdateSessionScholarshipDiscountPrice(SessionScholarshipDiscountPrice changedSessionScholarshipDiscountPrice)
        {
            try
            {
                changedSessionScholarshipDiscountPrice.ModifiedDate = DateTime.Now;
                changedSessionScholarshipDiscountPrice.Status = changedSessionScholarshipDiscountPrice.Status;
                _DbContext.SessionScholarshipDiscountPrices.Attach(changedSessionScholarshipDiscountPrice);
                var entry = _DbContext.Entry(changedSessionScholarshipDiscountPrice);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
