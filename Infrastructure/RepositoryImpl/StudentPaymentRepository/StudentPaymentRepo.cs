﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.StudentPaymentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using X.PagedList;

namespace OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository
{
    public class StudentPaymentRepo : IStudentPayment
    {
        private readonly ApplicationDbContext _DbContext;

        public StudentPaymentRepo(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateStudentPayment(StudentPayment newStudentPayment)
        {
            try
            {
                newStudentPayment.InsertedDate = DateTime.Now;
                newStudentPayment.ModifiedDate = DateTime.Now;
                _DbContext.Set<StudentPayment>().Add(newStudentPayment);
                _DbContext.SaveChanges();
                return newStudentPayment.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public IEnumerable<StudentPayment> GetAllStudentPayment()
        {
            try
            {                
                IEnumerable<StudentPayment> res = _DbContext.Set<StudentPayment>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
