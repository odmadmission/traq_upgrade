﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace OdmErp.Infrastructure.RepositoryImpl
{

    public class FeesTypePriceRepository : IFeesTypePrice
    {
        private readonly ApplicationDbContext _DbContext;
        public IFeesTypeApplicableRepository applicableTypeRepo;
        public IAcademicSessionRepository academicSessionRepository;
        public IFeesTypeRepository feestypeRepo;
        public IOrganizationRepository organisationRepo;
        private IStudentAggregateRepository studentAggregateRepository;
        private IStandardRepository standardRepository;
        private IBoardRepository boardRepository;
        public FeesTypePriceRepository(ApplicationDbContext dbContext, IFeesTypeApplicableRepository applicableTypeRepository, 
            IAcademicSessionRepository academicSessionRepository, IFeesTypeRepository feestypeRepo, IOrganizationRepository organisationRepo,
            IStudentAggregateRepository studentAggregateRepository, IStandardRepository standardRepository, IBoardRepository boardRepository


            )
        {
            _DbContext = dbContext;
            this.applicableTypeRepo = applicableTypeRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.feestypeRepo = feestypeRepo;
            this.organisationRepo = organisationRepo;
            this.studentAggregateRepository = studentAggregateRepository;
            this.standardRepository = standardRepository;
            this.boardRepository = boardRepository;
        }
        public long CreateFeesTypePrice(FeesTypePriceData newFeesTypePrice)
        {
            try
            {
                FeesTypePriceData newparent = new FeesTypePriceData();

                newparent.ID = newFeesTypePrice.ID;
                newparent.AcademicSessionId = newFeesTypePrice.AcademicSessionId;
                newparent.FeesTypePriceId = newFeesTypePrice.FeesTypePriceId;

                var dt = new DataTable();
                if (newFeesTypePrice.sessionFeesTypePriceApplicableData != null)
                {
                    dt.Columns.Add("Id", typeof(long));
                    dt.Columns.Add("ApplicableDataId", typeof(long));
                    dt.Columns.Add("Price", typeof(decimal));
                    dt.Columns.Add("Status", typeof(string));
                    dt.Columns.Add("ApplicableToId", typeof(string));
                    dt.Columns.Add("FeesTypePriceId", typeof(string));

                    foreach (var a in newFeesTypePrice.sessionFeesTypePriceApplicableData)
                    {

                        DataRow dr = dt.NewRow();
                        dr["Id"] = "0";
                        dr["ApplicableDataId"] = a.ApplicabledataId;
                        dr["Price"] = a.Price;
                        dr["Status"] = "Active";
                        dr["ApplicableToId"] = a.ApplicableToId;
                        dr["FeesTypePriceId"] = a.FeesTypePriceId;
                        dt.Rows.Add(dr);
                    }
                    var TypePriceData = new SqlParameter("@dt", SqlDbType.Structured);
                    TypePriceData.Value = dt;
                    TypePriceData.TypeName = "[dbo].[Type_PriceData]";
                    string Flag = "Insert";

                    //var Paramater2 = new SqlParameter("@Paramater2", "");
                    //Paramater2.Direction = ParameterDirection.Output;
                    //Paramater2.Size = 30;
                    //Paramater2.DbType = DbType.String;
                    var result=  _DbContext.Database.ExecuteSqlCommand($"SP_Save_FeesTypeData {newparent.ID},{newparent.AcademicSessionId}, {newparent.FeesTypePriceId },{TypePriceData},{Flag},{newparent.InsertedId}");

                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public IEnumerable<FeesTypePriceData> GetAllFeesTypePrice(int? page, int? noofData)
        {
            try
            {
                IEnumerable<FeesTypePriceData> res = _DbContext.Set<FeesTypePriceData>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IEnumerable<SessionFeesTypeApplicableData> GetAllFeesTypePriceApplicableData()
        {
            try
            {
                IEnumerable<SessionFeesTypeApplicableData> resappli = _DbContext.Set<SessionFeesTypeApplicableData>().Where(a => a.Active == true).ToListAsync().Result;
                return resappli;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public long UpdateFeesTypePrice(FeesTypePriceData changeFeesTypePrice)
        {
           
                
                try
                {
                    FeesTypePriceData newparent = new FeesTypePriceData();

                    newparent.ID = changeFeesTypePrice.ID;
                    newparent.AcademicSessionId = changeFeesTypePrice.AcademicSessionId;
                    newparent.FeesTypePriceId = changeFeesTypePrice.FeesTypePriceId;



                    var dt = new DataTable();
                if (changeFeesTypePrice.sessionFeesTypePriceApplicableData != null)
                {
                    dt.Columns.Add("Id", typeof(long));
                    dt.Columns.Add("ApplicableDataId", typeof(long));
                    dt.Columns.Add("Price", typeof(decimal));
                    dt.Columns.Add("Status", typeof(string));
                    dt.Columns.Add("ApplicableToId", typeof(string));
                    dt.Columns.Add("FeesTypePriceId", typeof(string));

                    foreach (var a in changeFeesTypePrice.sessionFeesTypePriceApplicableData)
                    {

                        DataRow dr = dt.NewRow();
                        dr["Id"] = a.ID;
                        dr["ApplicableDataId"] = a.ApplicabledataId;
                        dr["Price"] = a.Price;
                        dr["Status"] = "Active";
                        dr["ApplicableToId"] = a.ApplicableToId;
                        dr["FeesTypePriceId"] = a.FeesTypePriceId;
                        dt.Rows.Add(dr);
                    }
                    var TypePriceData = new SqlParameter("@dt", SqlDbType.Structured);
                        TypePriceData.Value = dt;
                        TypePriceData.TypeName = "[dbo].[Type_PriceData]";
                        string Flag = "Update";

                        _DbContext.Database.ExecuteSqlCommand($"SP_Save_FeesTypeData {newparent.ID},{newparent.AcademicSessionId}, {newparent.FeesTypePriceId },{TypePriceData},{Flag},{newparent.InsertedId}");

                    }
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
           
        }

        public long UpdateFeesTypePriceStatus(FeesTypePriceData changeFeesTypePrice)
        {
            try
            {
                FeesTypePriceData newparent = new FeesTypePriceData();

                newparent.ID = changeFeesTypePrice.ID;
                newparent.AcademicSessionId = changeFeesTypePrice.AcademicSessionId;
                newparent.FeesTypePriceId = changeFeesTypePrice.FeesTypePriceId;
                newparent.Status = changeFeesTypePrice.Status;


                var dt = new DataTable();
                if (changeFeesTypePrice.sessionFeesTypePriceApplicableData != null)
                {
                    dt.Columns.Add("Id", typeof(long));
                    dt.Columns.Add("ApplicableDataId", typeof(long));
                    dt.Columns.Add("Price", typeof(decimal));
                    dt.Columns.Add("Status", typeof(string));
                    dt.Columns.Add("ApplicableToId", typeof(string));
                    dt.Columns.Add("FeesTypePriceId", typeof(string));

                    foreach (var a in changeFeesTypePrice.sessionFeesTypePriceApplicableData)
                    {

                        DataRow dr = dt.NewRow();
                        dr["Id"] = a.ID;
                        dr["ApplicableDataId"] = a.ApplicabledataId;
                        dr["Price"] = a.Price;
                        dr["Status"] = changeFeesTypePrice.Status;
                        dr["ApplicableToId"] = a.ApplicableToId;
                        dr["FeesTypePriceId"] = a.FeesTypePriceId;
                        dt.Rows.Add(dr);
                    }
                    var TypePriceData = new SqlParameter("@dt", SqlDbType.Structured);
                    TypePriceData.Value = dt;
                    TypePriceData.TypeName = "[dbo].[Type_PriceData]";
                    string Flag = "UpdateStatus";

                    _DbContext.Database.ExecuteSqlCommand($"SP_Save_FeesTypeData {newparent.ID},{newparent.AcademicSessionId}, {newparent.FeesTypePriceId },{TypePriceData},{Flag},{newparent.InsertedId}");

                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }


        public FeesTypePriceData GetFeesTypePriceById(long id)
        {
            try
            {
                
                FeesTypePriceData DataObj = new FeesTypePriceData();
                FeesTypePriceData ob = _DbContext.Set<FeesTypePriceData>().FirstOrDefaultAsync(a => a.ID == id).Result;
                //SessionFeesTypeApplicableData obj = _DbContext.Set<SessionFeesTypeApplicableData>().FirstOrDefaultAsync(z => z.FeesTypePriceId==id).Result;
                IEnumerable<SessionFeesTypeApplicableData> resappli = _DbContext.Set<SessionFeesTypeApplicableData>().Where(a => a.Active == true && a.FeesTypePriceDataID==id).ToListAsync().Result;
                DataObj.AcademicSessionId = ob.AcademicSessionId;
                DataObj.FeesTypePriceId = ob.FeesTypePriceId;
                DataObj.ID = id;
                var sessionname = academicSessionRepository.GetAllAcademicSession().Where(x => x.ID == DataObj.AcademicSessionId).Select(x => x.DisplayName).FirstOrDefault();
                var FeesTypeName = feestypeRepo.GetAllFeesType().Where(x => x.ID == DataObj.FeesTypePriceId).Select(x => x.Name).FirstOrDefault();
                var parentfeeid = feestypeRepo.GetAllFeesType().Where(x => x.ID == DataObj.FeesTypePriceId).Select(x => x.ParentFeeTypeID).FirstOrDefault();
                var ParentFeesTypeName = feestypeRepo.GetAllFeesType().Where(x => x.ID == parentfeeid).Select(x => x.Name).FirstOrDefault()==null?"N A" : feestypeRepo.GetAllFeesType().Where(x => x.ID == parentfeeid).Select(x => x.Name).FirstOrDefault();
               

                foreach (var val in resappli)
                {
                    var ApplicableTo = applicableTypeRepo.GetFeesTypeApplicableNameById(val.ApplicableToId);
                    var ApplicableData = "";
                    if (ApplicableTo== "Organization" || ApplicableTo== "Organizations")
                    {
                        ApplicableData = organisationRepo.GetAllOrganization().Where(x => x.ID == val.ApplicabledataId && x.Active == true).Select(x => x.Name).FirstOrDefault();
                    }
                    else if (ApplicableTo == "Wing" || ApplicableTo == "Wings")
                    {
                        ApplicableData= studentAggregateRepository.GetAllWing().Where(x => x.ID == val.ApplicabledataId && x.Active == true).Select(x => x.Name).FirstOrDefault();
                    }
                    else if (ApplicableTo == "Board" || ApplicableTo == "Boards")
                    {
                        ApplicableData=boardRepository.GetAllBoard().Where(x => x.ID == val.ApplicabledataId && x.Active == true).Select(x => x.Name).FirstOrDefault();
                    }
                    else if (ApplicableTo == "Class" || ApplicableTo == "Classs")
                    {
                        ApplicableData = standardRepository.GetAllStandard().Where(x => x.ID == val.ApplicabledataId && x.Active == true).Select(x => x.Name).FirstOrDefault();
                    }
                    else if (ApplicableTo == "Student" || ApplicableTo == "Students" )
                    {
                        ApplicableData = studentAggregateRepository.GetAllStudent().Where(x => x.ID == val.ApplicabledataId && x.Active == true).Select(x => x.FirstName+" "+x.MiddleName+" "+x.LastName).FirstOrDefault();
                    }                    

                    DataObj.sessionFeesTypePriceApplicableData.Add(new SessionFeesTypeApplicableData() { ID=val.ID, SessionName= sessionname, FeesTypeName=FeesTypeName, Applicabledata= ApplicableData, ParentFeesType= ParentFeesTypeName==null?"N A": ParentFeesTypeName, ApplicabledataId = val.ApplicabledataId, FeesTypePriceId = val.FeesTypePriceId,Price = val.Price });                       
                }
                return DataObj;
            }
            catch
            {
                return null;
            }
        }
        public FeesTypePriceData GetFeesTypePriceByName(string name)
        {
            //    try
            //    {
            //        FeesTypePrice ob = _DbContext.Set<FeesTypePrice>().FirstOrDefaultAsync(a => a.Name == name).Result;
            //        return ob;
            //    }
            //    catch
            //    {
            return null;
            // }
        }
        public bool DeleteAllFeesTypePrice()
        {
            try
            {
                List<FeesTypePriceData> res = _DbContext.Set<FeesTypePriceData>().ToListAsync().Result;
                foreach (FeesTypePriceData ob in res)
                {
                    //ob.Active = false;
                    //ob.ModifiedDate = DateTime.Now;
                    //ob.Status = "DELETED";
                    //_DbContext.SessionFeesTypePrice.Attach(ob);
                    //var entry = _DbContext.Entry(ob);
                    //entry.State = EntityState.Modified;
                    //entry.Property(e => e.InsertedDate).IsModified = false;
                    //entry.Property(e => e.InsertedId).IsModified = false;
                    //_DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteFeesTypePrice(long id)
        {
            try
            {
                //SessionFeesTypePrice ob = _DbContext.Set<SessionFeesTypePrice>().FirstOrDefaultAsync(a => a.ID == id).Result;
                //ob.Active = false;
                //ob.ModifiedDate = DateTime.Now;
                //ob.Status = "DELETED";
                //_DbContext.SessionFeesTypePrice.Attach(ob);
                //var entry = _DbContext.Entry(ob);
                //entry.State = EntityState.Modified;
                //entry.Property(e => e.InsertedDate).IsModified = false;
                //entry.Property(e => e.InsertedId).IsModified = false;
                //_DbContext.SaveChanges();

                //List<SessionFeeTypeApplicableData> Items = _DbContext.SessionFeesTypeApplicableData.Where(q => q.ID == id).ToList();
                //SessionFeeTypeApplicableData obj_lst = new SessionFeeTypeApplicableData();
                //foreach (var d in Items)
                //{


                //    d.Active = false;
                //    d.ModifiedDate = DateTime.Now;
                //    d.Status = "DELETED";
                //    _DbContext.SessionFeesTypeApplicableData.Attach(d);
                //    var entry1 = _DbContext.Entry(d);
                //    entry1.State = EntityState.Modified;
                //    entry1.Property(e => e.InsertedDate).IsModified = false;
                //    entry1.Property(e => e.InsertedId).IsModified = false;
                //    _DbContext.SaveChanges();
                //}
                // userId = HttpContext.Session.GetInt32("userId").Value;
                FeesTypePriceData newparent = new FeesTypePriceData();
                newparent.ID = id;
                var TypePriceData = new SqlParameter("@dt", SqlDbType.Structured);
                TypePriceData.TypeName = "[dbo].[Type_PriceData]";
                string Flag = "Delete";
                _DbContext.Database.ExecuteSqlCommand($"SP_Save_FeesTypeData {newparent.ID},{0}, {0},{TypePriceData},{Flag},{0}");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteSub(long id)
        {
            try
            {
               
                SessionFeesTypeApplicableData newchild = new SessionFeesTypeApplicableData();
                newchild.ID = id;
                var TypePriceData = new SqlParameter("@dt", SqlDbType.Structured);
                TypePriceData.TypeName = "[dbo].[Type_PriceData]";
                string Flag = "DeleteSub";
                _DbContext.Database.ExecuteSqlCommand($"SP_Save_FeesTypeData {newchild.ID},{0},{0},{TypePriceData},{Flag},{0}");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

