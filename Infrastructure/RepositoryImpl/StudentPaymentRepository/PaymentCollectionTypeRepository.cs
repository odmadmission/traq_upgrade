﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.StudentPaymentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository
{
    public class PaymentCollectionTypeRepository : IPaymentCollectionType
    {
        private readonly ApplicationDbContext _DbContext;
        public IFeesTypeApplicableRepository applicableTypeRepo;
        public IAcademicSessionRepository academicSessionRepository;
        public IFeesTypeRepository feestypeRepo;
        public IOrganizationRepository organisationRepo;
        private IStudentAggregateRepository studentAggregateRepository;
        private IStandardRepository standardRepository;
        private IBoardRepository boardRepository;
        public PaymentCollectionTypeRepository(ApplicationDbContext dbContext, IFeesTypeApplicableRepository applicableTypeRepository,
            IAcademicSessionRepository academicSessionRepository, IFeesTypeRepository feestypeRepo, IOrganizationRepository organisationRepo,
            IStudentAggregateRepository studentAggregateRepository, IStandardRepository standardRepository, IBoardRepository boardRepository)
        {
            _DbContext = dbContext;
            this.applicableTypeRepo = applicableTypeRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.feestypeRepo = feestypeRepo;
            this.organisationRepo = organisationRepo;
            this.studentAggregateRepository = studentAggregateRepository;
            this.standardRepository = standardRepository;
            this.boardRepository = boardRepository;
        }
        public long CreatePaymentCollectionType(PaymentCollectionType newPaymentCollectionType)
        {
            try
            {
                PaymentCollectionType newparent = new PaymentCollectionType();

                newparent.ID = newPaymentCollectionType.ID;
                newparent.AcademicSessionId = newPaymentCollectionType.AcademicSessionId;
                newparent.OrganizationId = newPaymentCollectionType.OrganizationId;
                newparent.WingId = newPaymentCollectionType.WingId;
                newparent.Boardid = newPaymentCollectionType.Boardid;
                newparent.ClassId = newPaymentCollectionType.ClassId;
                newparent.SumAmount = newPaymentCollectionType.SumAmount;
                newparent.NoOfInstallment = newPaymentCollectionType.NoOfInstallment;
                newparent.Status = newPaymentCollectionType.Status;
                newparent.FeeTypeId = newPaymentCollectionType.FeeTypeId;

                var dt = new DataTable();
                if (newPaymentCollectionType.paymentCollectionTypeData != null)
                {
                    dt.Columns.Add("Id", typeof(long));
                    dt.Columns.Add("PaymentName", typeof(string));
                    dt.Columns.Add("Amount", typeof(decimal));
                    dt.Columns.Add("FromDate", typeof(DateTime));
                    dt.Columns.Add("ToDate", typeof(DateTime));
                    dt.Columns.Add("DeadLineDate", typeof(DateTime));
                    dt.Columns.Add("Status", typeof(string));
                    dt.Columns.Add("LatefeePercentage", typeof(string));

                    foreach (var a in newPaymentCollectionType.paymentCollectionTypeData)
                    {

                        DataRow dr = dt.NewRow();
                        dr["Id"] = "0";
                        dr["PaymentName"] = a.PaymentName;
                        dr["Amount"] = a.Amount;
                        dr["FromDate"] = a.FromDate;
                        dr["ToDate"] = a.ToDate;
                        dr["DeadLineDate"] = a.DeadLineDate;
                        dr["Status"] = a.Status;
                        dr["LatefeePercentage"] = a.LatefeePercentage;
                        dt.Rows.Add(dr);
                    }
                    var TypePriceData = new SqlParameter("@dt", SqlDbType.Structured);
                    TypePriceData.Value = dt;
                    TypePriceData.TypeName = "[dbo].[Type_PaymentCollectionData]";
                    string Flag = "Insert";

                    //var Paramater2 = new SqlParameter("@Paramater2", "");
                    //Paramater2.Direction = ParameterDirection.Output;
                    //Paramater2.Size = 30;
                    //Paramater2.DbType = DbType.String;
                    var result = _DbContext.Database.ExecuteSqlCommand($"SP_SaveUpdate_PaymentCollectionData {newparent.ID},{newparent.AcademicSessionId}, {newparent.OrganizationId },{newparent.WingId },{newparent.Boardid },{newparent.ClassId },{newparent.SumAmount},{newparent.NoOfInstallment},{TypePriceData},{Flag},{newparent.InsertedId},{newparent.Status},{newparent.FeeTypeId}");

                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public IEnumerable<PaymentCollectionType> GetAllPaymentCollectionType()
        {
            try
            {
                IEnumerable<PaymentCollectionType> res = _DbContext.Set<PaymentCollectionType>().Where(a => a.Active == true).ToList();
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IEnumerable<PaymentCollectionTypeData> GetAllPaymentCollectionTypeData()
        {
            try
            {
                IEnumerable<PaymentCollectionTypeData> resappli = _DbContext.Set<PaymentCollectionTypeData>().Where(a => a.Active == true).ToListAsync().Result;
                return resappli;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public long UpdatePaymentCollectionType(PaymentCollectionType changePaymentCollectionType)
        {

           
            try
            {
                PaymentCollectionType newparent = new PaymentCollectionType();

                newparent.ID = changePaymentCollectionType.ID;
                newparent.AcademicSessionId = changePaymentCollectionType.AcademicSessionId;
                newparent.OrganizationId = changePaymentCollectionType.OrganizationId;
                newparent.WingId = changePaymentCollectionType.WingId;
                newparent.Boardid = changePaymentCollectionType.Boardid;
                newparent.ClassId = changePaymentCollectionType.ClassId;
                newparent.SumAmount = changePaymentCollectionType.SumAmount;
                newparent.NoOfInstallment = changePaymentCollectionType.NoOfInstallment;
                newparent.Status = changePaymentCollectionType.Status;
                newparent.FeeTypeId = changePaymentCollectionType.FeeTypeId;


                var dt = new DataTable();
                if (changePaymentCollectionType.paymentCollectionTypeData != null)
                {
                    dt.Columns.Add("Id", typeof(long));
                    dt.Columns.Add("PaymentName", typeof(string));
                    dt.Columns.Add("Amount", typeof(decimal));
                    dt.Columns.Add("FromDate", typeof(DateTime));
                    dt.Columns.Add("ToDate", typeof(DateTime));
                    dt.Columns.Add("DeadLineDate", typeof(DateTime));
                    dt.Columns.Add("Status", typeof(string));
                    dt.Columns.Add("LatefeePercentage", typeof(decimal));

                    foreach (var a in changePaymentCollectionType.paymentCollectionTypeData)
                    {
                        DataRow dr = dt.NewRow();
                        dr["Id"] = a.ID;
                        dr["PaymentName"] = a.PaymentName;
                        dr["Amount"] = a.Amount;
                        dr["FromDate"] = a.FromDate;
                        dr["ToDate"] = a.ToDate;
                        dr["DeadLineDate"] = a.DeadLineDate;
                        dr["Status"] = a.Status;
                        dr["LatefeePercentage"] = a.LatefeePercentage;
                        dt.Rows.Add(dr);
                    }
                    var TypePriceData = new SqlParameter("@dt", SqlDbType.Structured);
                    TypePriceData.Value = dt;
                    TypePriceData.TypeName = "[dbo].[Type_PaymentCollectionData]";
                    string Flag = "Update";

                    var result = _DbContext.Database.ExecuteSqlCommand($"SP_SaveUpdate_PaymentCollectionData {newparent.ID},{newparent.AcademicSessionId}, {newparent.OrganizationId },{newparent.WingId },{newparent.Boardid },{newparent.ClassId },{newparent.SumAmount},{newparent.NoOfInstallment},{TypePriceData},{Flag},{newparent.InsertedId},{newparent.Status},{newparent.FeeTypeId}");

                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public long UpdatePaymentCollectionTypeStatus(PaymentCollectionType changePaymentCollectionType)
        {
            try
            {
                PaymentCollectionType newparent = new PaymentCollectionType();
                newparent.ID = changePaymentCollectionType.ID;
                newparent.AcademicSessionId = changePaymentCollectionType.AcademicSessionId;
                newparent.OrganizationId = changePaymentCollectionType.OrganizationId;
                newparent.WingId = changePaymentCollectionType.WingId;
                newparent.Boardid = changePaymentCollectionType.Boardid;
                newparent.ClassId = changePaymentCollectionType.ClassId;
                newparent.SumAmount = changePaymentCollectionType.SumAmount;
                newparent.NoOfInstallment = changePaymentCollectionType.NoOfInstallment;
                newparent.Status = changePaymentCollectionType.Status;
                newparent.FeeTypeId = changePaymentCollectionType.FeeTypeId;

                var dt = new DataTable();
                if (changePaymentCollectionType.paymentCollectionTypeData != null)
                {
                    dt.Columns.Add("Id", typeof(long));
                    dt.Columns.Add("PaymentName", typeof(string));
                    dt.Columns.Add("Amount", typeof(decimal));
                    dt.Columns.Add("FromDate", typeof(DateTime));
                    dt.Columns.Add("ToDate", typeof(DateTime));
                    dt.Columns.Add("DeadLineDate", typeof(DateTime));
                    dt.Columns.Add("Status", typeof(string));
                    dt.Columns.Add("LatefeePercentage", typeof(string));

                    foreach (var a in changePaymentCollectionType.paymentCollectionTypeData)
                    {
                        DataRow dr = dt.NewRow();
                        dr["Id"] = a.ID;
                        dr["PaymentName"] = a.PaymentName;
                        dr["Amount"] = a.Amount;
                        dr["FromDate"] = a.FromDate;
                        dr["ToDate"] = a.ToDate;
                        dr["DeadLineDate"] = a.DeadLineDate;
                        dr["Status"] = changePaymentCollectionType.Status;
                        dr["LatefeePercentage"] = a.LatefeePercentage;
                        dt.Rows.Add(dr);
                    }
                    var TypePriceData = new SqlParameter("@dt", SqlDbType.Structured);
                    TypePriceData.Value = dt;
                    TypePriceData.TypeName = "[dbo].[Type_PaymentCollectionData]";
                    string Flag = "UpdateStatus";

                    var result = _DbContext.Database.ExecuteSqlCommand($"SP_SaveUpdate_PaymentCollectionData {newparent.ID},{newparent.AcademicSessionId}, {newparent.OrganizationId },{newparent.WingId },{newparent.Boardid },{newparent.ClassId },{newparent.SumAmount},{newparent.NoOfInstallment},{TypePriceData},{Flag},{newparent.InsertedId},{newparent.Status},{newparent.FeeTypeId}");

                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }



        public PaymentCollectionType GetPaymentCollectionTypeById(long id)
        {
            try
            {

                PaymentCollectionType DataObj = new PaymentCollectionType();
                PaymentCollectionType ob = _DbContext.Set<PaymentCollectionType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                //SessionFeesTypeApplicableData obj = _DbContext.Set<SessionFeesTypeApplicableData>().FirstOrDefaultAsync(z => z.FeesTypePriceId==id).Result;
                IEnumerable<PaymentCollectionTypeData> rescollectiondata = _DbContext.Set<PaymentCollectionTypeData>().Where(a => a.Active == true && a.PaymentCollectionId == id).ToListAsync().Result;
                DataObj.AcademicSessionId = ob.AcademicSessionId;
                DataObj.OrganizationId = ob.OrganizationId;
                DataObj.WingId = ob.WingId;
                DataObj.Boardid = ob.Boardid;
                DataObj.ClassId = ob.ClassId;
                DataObj.SumAmount = ob.SumAmount;
                DataObj.NoOfInstallment = ob.NoOfInstallment;
                DataObj.ID = id;
                DataObj.FeeTypeId = ob.FeeTypeId;

                foreach (var val in rescollectiondata)
                {
                    DataObj.paymentCollectionTypeData.Add(new PaymentCollectionTypeData() { ID = val.ID, PaymentName = val.PaymentName, Amount = val.Amount, FromDate =val.FromDate, ToDate = val.ToDate, DeadLineDate = val.DeadLineDate,LatefeePercentage=val.LatefeePercentage});
                }
                return DataObj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public PaymentCollectionType GetPaymentCollectionTypeByName(string name)
        {
            return null;
        }
        public bool DeleteAllPaymentCollectionType()
        {
            try
            {
                List<PaymentCollectionType> res = _DbContext.Set<PaymentCollectionType>().ToListAsync().Result;
                foreach (PaymentCollectionType ob in res)
                {
                    //ob.Active = false;
                    //ob.ModifiedDate = DateTime.Now;
                    //ob.Status = "DELETED";
                    //_DbContext.SessionFeesTypePrice.Attach(ob);
                    //var entry = _DbContext.Entry(ob);
                    //entry.State = EntityState.Modified;
                    //entry.Property(e => e.InsertedDate).IsModified = false;
                    //entry.Property(e => e.InsertedId).IsModified = false;
                    //_DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeletePaymentCollectionType(long id)
        {
            try
            {
                PaymentCollectionType newparent = new PaymentCollectionType();
                newparent.ID = id;
                var TypePriceData = new SqlParameter("@dt", SqlDbType.Structured);
                TypePriceData.TypeName = "[dbo].[Type_PaymentCollectionData]";
                string Flag = "Delete";
                _DbContext.Database.ExecuteSqlCommand($"SP_SaveUpdate_PaymentCollectionData {newparent.ID},{0}, {0},{0},{0},{0},{0},{0},{TypePriceData},{Flag},{0}");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteSub(long id)
        {
            try
            {

                PaymentCollectionTypeData newchild = new PaymentCollectionTypeData();
                newchild.ID = id;
                var TypePriceData = new SqlParameter("@dt", SqlDbType.Structured);
                TypePriceData.TypeName = "[dbo].[Type_PaymentCollectionData]";
                string Flag = "DeleteSub";
                _DbContext.Database.ExecuteSqlCommand($"SP_SaveUpdate_PaymentCollectionData {newchild.ID},{0},{0},{TypePriceData},{Flag},{0}");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

