﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure
{
    public class ScholarshipApprovalRepoaitory : IScholarshipApprovalRepoaitory
    {
        private readonly ApplicationDbContext _DbContext;
        public ScholarshipApprovalRepoaitory(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateScholarshipApproval(ScholarshipApproval newScholarshipApproval)
        {
            try
            {
                newScholarshipApproval.InsertedDate = DateTime.Now;
                newScholarshipApproval.ModifiedDate = DateTime.Now;
                _DbContext.Set<ScholarshipApproval>().Add(newScholarshipApproval);
                _DbContext.SaveChanges();
                return newScholarshipApproval.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteAllScholarshipApproval()
        {
            try
            {
                List<ScholarshipApproval> res = _DbContext.Set<ScholarshipApproval>().ToListAsync().Result;
                foreach (ScholarshipApproval ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.ScholarshipApprovals.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteScholarshipApproval(long id)
        {
            try
            {
                ScholarshipApproval ob = _DbContext.Set<ScholarshipApproval>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.ScholarshipApprovals.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<ScholarshipApproval> GetAllScholarshipApproval()
        {
            try
            {

                // return View(onePageOfEmps);
                IEnumerable<ScholarshipApproval> res = _DbContext.Set<ScholarshipApproval>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ScholarshipApproval GetScholarshipApprovalById(long id)
        {
            try
            {
                ScholarshipApproval ob = _DbContext.Set<ScholarshipApproval>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public ScholarshipApproval GetScholarshipApprovalByName(string name)
        {
            throw new NotImplementedException();
        }

        public bool UpdateScholarshipApproval(ScholarshipApproval changedScholarshipApproval)
        {
            try
            {
                changedScholarshipApproval.ModifiedDate = DateTime.Now;
                changedScholarshipApproval.Status = changedScholarshipApproval.Status;
                _DbContext.ScholarshipApprovals.Attach(changedScholarshipApproval);
                var entry = _DbContext.Entry(changedScholarshipApproval);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
