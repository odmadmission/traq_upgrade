﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class RoleRepository : IRoleRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public RoleRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateRole(Role newRole)
        {
            try
            {
                newRole.InsertedDate = DateTime.Now;
                newRole.ModifiedDate = DateTime.Now;
                _DbContext.Set<Role>().Add(newRole);
                _DbContext.SaveChanges();
                return newRole.ID;
            }
            catch
            {
                return 0;
            }
        }

        public long CreateSuperAdminRole()
        {
            try
            {
                Role newRole = new Role();
                newRole.Status = EntityStatus.ACTIVE;
                newRole.Active = true;
                newRole.Name = "Super Admin";
                newRole.InsertedId = 0;
                newRole.ModifiedId = 0;
                newRole.InsertedDate = DateTime.Now;
                newRole.ModifiedDate = DateTime.Now;
                _DbContext.Set<Role>().Add(newRole);
                _DbContext.SaveChanges();
                return newRole.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllRole()
        {
            try
            {
                List<Role> res = _DbContext.Set<Role>().ToListAsync().Result;
                foreach (Role ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Roles.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteRole(long id)
        {
            try
            {
                Role ob = _DbContext.Set<Role>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Roles.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Role> GetAllRole()
        {
            try
            {
                IEnumerable<Role> res = _DbContext.Set<Role>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Role GetRoleById(long id)
        {
            try
            {
                Role ob = _DbContext.Set<Role>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Role GetRoleByName(string name)
        {
            try
            {
                Role ob = _DbContext.Set<Role>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateRole(Role changedRole)
        {
            try
            {
                changedRole.ModifiedDate = DateTime.Now;
                changedRole.Status = "UPDATED";
                _DbContext.Roles.Attach(changedRole);
                var entry = _DbContext.Entry(changedRole);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }



    }
}
