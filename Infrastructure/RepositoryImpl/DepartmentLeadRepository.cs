﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class DepartmentLeadRepository : IDepartmentLeadRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public DepartmentLeadRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

       
        public long CreateDepartmentLead(DepartmentLead obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<DepartmentLead>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteDepartmentLead(long id)
        {
            try
            {
                DepartmentLead ob = _DbContext.Set<DepartmentLead>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.DepartmentLeads.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllDepartmentLead()
        {
            try
            {
                List<DepartmentLead> res = _DbContext.Set<DepartmentLead>().ToListAsync().Result;
                foreach (DepartmentLead ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.DepartmentLeads.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<DepartmentLead> GetAllDepartmentLead()
        {
            try
            {
                IEnumerable<DepartmentLead> res = _DbContext.Set<DepartmentLead>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public DepartmentLead GetByDepartmentLeadId(long id)
        {
            try
            {
                DepartmentLead ob = _DbContext.Set<DepartmentLead>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateDepartmentLead(DepartmentLead changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.DepartmentLeads.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<DepartmentLead> GetAllByEmployeeId(long employeeId)
        {
            try
            {
                IEnumerable<DepartmentLead> res = _DbContext.Set<DepartmentLead>().Where(a => a.Active == true&&a.IsAvailable==true&&a.EmployeeID==employeeId).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }


        public IEnumerable<DepartmentLead> GetAllEmployeeIdByDepartment(long deptId)
        {
            try
            {
                IEnumerable<DepartmentLead> res = _DbContext.Set<DepartmentLead>().Where(a => a.Active == true && a.IsAvailable == true && a.DepartmentID == deptId).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

    }
}
