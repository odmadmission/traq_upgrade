﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolAggregateRepository
{
    public class AcademicHomeworkRepository : EfRepository<AcademicHomework>, IAcademicHomeworkRepository
    {
        public AcademicHomeworkRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<AcademicHomework> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicHomeworks.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicHomework>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicHomeworks.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicHomework>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicHomeworks.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
