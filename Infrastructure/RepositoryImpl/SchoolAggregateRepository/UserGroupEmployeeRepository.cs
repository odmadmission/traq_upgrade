﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolAggregateRepository
{
  public  class UserGroupEmployeeRepository : EfRepository<UserGroupEmployee>, IUserGroupEmployeeRepository
    {
        public UserGroupEmployeeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<UserGroupEmployee> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.UserGroupEmployees.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<UserGroupEmployee>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.UserGroupEmployees.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<UserGroupEmployee>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.UserGroupEmployees.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
