﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolAggregateRepository
{
  public  class UserGroupEmployeeTimeLineRepository :EfRepository<UserGroupEmployeeTimeLine>, IUserGroupEmployeeTimeLineRepository
    {
        public UserGroupEmployeeTimeLineRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<UserGroupEmployeeTimeLine> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.UserGroupEmployeeTimeLine.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<UserGroupEmployeeTimeLine>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.UserGroupEmployeeTimeLine.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<UserGroupEmployeeTimeLine>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.UserGroupEmployeeTimeLine.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
