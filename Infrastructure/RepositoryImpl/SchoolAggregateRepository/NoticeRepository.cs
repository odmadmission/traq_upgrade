﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolAggregateRepository
{
   public class NoticeRepository : EfRepository<Notice>, INoticeRepository
    {
        public NoticeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Notice> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.Notices.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<Notice>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.Notices.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }            
        }

        public async Task<IReadOnlyList<Notice>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.Notices.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
