﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolAggregateRepository
{
  public  class UserGroupTimeLineRepository : EfRepository<UserGroupTimeLine>, IUserGroupTimeLineRepository
    {
        public UserGroupTimeLineRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<UserGroupTimeLine> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.UserGroupTimeLines.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<UserGroupTimeLine>> ListAllAsyncIncludeAll()
        {

            try
            {
                var res = _dbContext.UserGroupTimeLines.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<UserGroupTimeLine>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.UserGroupTimeLines.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
