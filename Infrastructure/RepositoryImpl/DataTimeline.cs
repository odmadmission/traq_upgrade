﻿using OdmErp.ApplicationCore.Entities.GrievanceAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class DataTimelineRepository : IDataTimelineRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public DataTimelineRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateDataTimeline(DataTimeline timeline)
        {
            try
            {
                DataTimeline dataTimeline = new DataTimeline();
                dataTimeline.InsertedDate = DateTime.Now;
                dataTimeline.ModifiedDate = DateTime.Now;
                dataTimeline.Active = true;
                dataTimeline.Status = EntityStatus.ACTIVE;
                dataTimeline.IsAvailable = true;
                dataTimeline.EntityId = timeline.ID;
                dataTimeline.EntityName = timeline.EntityName;
                dataTimeline.Timeline = timeline.Timeline;

                _DbContext.DataTimelines.Add(timeline);

                return timeline.ID;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return 0;
            }
        }

        public bool DeleteDataTimeline(long id)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDataTimeline(ApplicationCore.Entities.GrievanceAggregate.DataTimeline timeline)
        {
            throw new NotImplementedException();
        }
    }
}
