﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using OdmErp.ApplicationCore.Entities.Pagination;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class FeesTypeRepository : IFeesTypeRepository
    {

        private readonly ApplicationDbContext _DbContext;

        public FeesTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
       

        public long CreateFeesType(FeesType newFeesType)
        {
            try
            {
                newFeesType.InsertedDate = DateTime.Now;
                newFeesType.ModifiedDate = DateTime.Now;
                _DbContext.Set<FeesType>().Add(newFeesType);
                _DbContext.SaveChanges();
                return newFeesType.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public IEnumerable<FeesType> GetAllFeesType()
        {
            try
            {
                IEnumerable<FeesType> res = _DbContext.Set<FeesType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool UpdateFeesType(FeesType changeFeesType)
        {
            try
            {
                changeFeesType.ModifiedDate = DateTime.Now;
                //changeFeesType.Status = "ACTIVE";
                _DbContext.FeesTypes.Attach(changeFeesType);
                var entry = _DbContext.Entry(changeFeesType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateFeesTypeParent(long id, bool HaveChild)
        {
            try
            {
                OdmErp.ApplicationCore.Entities.FeesType feesType1 = new FeesType();
                feesType1.ID = id;
                feesType1.HaveChild = HaveChild;
                _DbContext.FeesTypes.Attach(feesType1);
                var entry = _DbContext.Entry(feesType1);
                entry.State = EntityState.Modified;
                entry.Property(e => e.Name).IsModified = false;
                entry.Property(e => e.Description).IsModified = false;
                entry.Property(e => e.IsSubType).IsModified = false;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                entry.Property(e => e.ModifiedDate).IsModified = false;
                entry.Property(e => e.ModifiedId).IsModified = false;
                entry.Property(e => e.Status).IsModified = false;
                entry.Property(e => e.Active).IsModified = false;
                entry.Property(e => e.ParentFeeTypeID).IsModified = false;
                entry.Property(e => e.ParentFeeTypeName).IsModified = false;
                entry.Property(e => e.HaveInstallment).IsModified = false;

                _DbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public FeesType GetFeesTypeById(long id)
        {
            try
            {
                FeesType ob = _DbContext.Set<FeesType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public FeesType GetFeesTypeByName(string name)
        {
            try
            {
                FeesType ob = _DbContext.Set<FeesType>().FirstOrDefaultAsync(a => a.Name == name && a.Active==true).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public bool DeleteAllFeesType()
        {
            try
            {
                List<FeesType> res = _DbContext.Set<FeesType>().ToListAsync().Result;
                foreach (FeesType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.FeesTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteFeesType(long id)
        {
            try
            {
                FeesType ob = _DbContext.Set<FeesType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.FeesTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public PaginationFeesType GetPaginationAllFeesType(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue)
        {

            var v = (from a in _DbContext.FeesTypes where a.Active == true && a.Name.Contains(searchValue) select a);
            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                if (sortColumnDir == "asc")
                {
                    if (sortColumn == "Name")
                    {
                        v = v.OrderByDescending(a => a.Name);
                    }


                }
                //v = v.OrderBy(sortColumn + " " + sortColumnDir);
            }

            totalRecords = v.Count();
            var data = v.Skip(skip).Take(pageSize).ToList();
            //var fd = JsonSerializer.Serialize(data);
            PaginationFeesType str = new PaginationFeesType { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data };
            //var dfdf = JsonSerializer.Serialize(str);
            // return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });
            return str;
        }

        // public  List<FeesType> Get_FeesType2()
        //  {
        //CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                   // long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //if (commonMethods.checkaccessavailable("FeesType", accessId, "List", "StudentPayment", roleId) == false)
        //{
        //    return RedirectToAction("AuthenticationFailed", "Accounts");
        //}
        //List<FeesType> feesType = new List<FeesType>();
        //IEnumerable<FeesType> myGenericEnumerable = (IEnumerable<FeesType>)feesType;
        //FeesTypeRepository feesType1 = new FeesTypeRepository();
        //myGenericEnumerable = feesType1.GetAllFeesType();

        //var res = (from a in myGenericEnumerable.ToList()
        //           select new FeesType
        //           {
        //               ID = a.ID,
        //               Name = a.Name
        //           }).ToList();
        //feesType = res;
        //return feesType;
        //  }       
        public IEnumerable<FeesType> GetAllFeesTypes1()
        {
            try
            {
                IEnumerable<FeesType> res = _DbContext.Set<FeesType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
