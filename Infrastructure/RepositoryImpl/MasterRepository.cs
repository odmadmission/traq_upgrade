﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class FilePathUrlRepository : EfRepository<FilePathUrl>, IFilePathUrlRepository
    {
        public FilePathUrlRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public Task<FilePathUrl> GetByIdAsyncIncludeAll(long id)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<FilePathUrl>> ListAllAsyncIncludeAll()
        {
            throw new NotImplementedException();
        }
    }
}


