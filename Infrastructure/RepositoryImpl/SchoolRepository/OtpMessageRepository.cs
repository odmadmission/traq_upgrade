﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
    public class OtpMessageRepository : EfRepository<OtpMessage>, IOtpMessageRepository
    {

        public OtpMessageRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<OtpMessage> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.OtpMessages.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

       
        public async Task<IReadOnlyList<OtpMessage>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.OtpMessages.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }


    }
}
