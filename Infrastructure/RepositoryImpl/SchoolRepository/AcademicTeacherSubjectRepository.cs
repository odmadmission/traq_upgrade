﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
  public  class AcademicTeacherSubjectRepository : EfRepository<AcademicTeacherSubject>, IAcademicTeacherSubjectRepository
    {
        public AcademicTeacherSubjectRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

      

        public async Task<AcademicTeacherSubject> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicTeacherSubjects.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }
     

        public async Task<IReadOnlyList<AcademicTeacherSubject>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicTeacherSubjects.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicTeacherSubject>> ListAllAsyncIncludeAllNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicTeacherSubjects.AsNoTracking().ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
