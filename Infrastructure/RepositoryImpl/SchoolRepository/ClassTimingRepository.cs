﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class ClassTimingRepository : EfRepository<ClassTiming>, IClassTimingRepository
    {
        public ClassTimingRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }


        public async Task<ClassTiming> GetByIdAsyncIncludeAll(long id)
        {

            try
            {
                var res = _dbContext.ClassTimings.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<ClassTiming>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.ClassTimings.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<ClassTiming>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.ClassTimings.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<ClassTiming>> ListAllAsyncIncludeAllNoTrack()
        {
            try
            {
                var res = _dbContext.ClassTimings.AsNoTracking().ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
