﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Interfaces;
using System.Data.SqlClient;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ConceptRepository : EfRepository<Concept>, IConceptRepository
    {

        public ConceptRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Concept> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.Concepts.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

      

        public async Task<IReadOnlyList<Concept>> ListAllAsyncIncludeAll()
        {
            var res = _dbContext.Concepts.ToListAsync();
            return await res;


        }

        public long GetConceptCountBychapterid(long ChapterId)
        {
            try
            {
                long res = _dbContext.Concepts.ToListAsync().Result.Where(a => a.ChapterId == ChapterId).Count();

                return res;
            }
            catch
            {
                return 0;
            }
        }
    }
}
