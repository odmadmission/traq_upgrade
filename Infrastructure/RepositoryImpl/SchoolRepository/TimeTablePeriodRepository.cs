﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class TimeTablePeriodRepository : EfRepository<TimeTablePeriod>, ITimeTablePeriodRepository
    {
        public TimeTablePeriodRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<TimeTablePeriod> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.TimeTablePeriods.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }


        public async Task<IReadOnlyList<TimeTablePeriod>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.TimeTablePeriods.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<TimeTablePeriod>> ListAllAsyncIncludeAllNoTrack()
        {
            try
            {
                var res = _dbContext.TimeTablePeriods.AsNoTracking().ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<TimeTablePeriod>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.TimeTablePeriods.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

    }
}
