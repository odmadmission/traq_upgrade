﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
  public  class SyallbusRepository : EfRepository<Syllabus>, ISyallbusRepository
    {
        public SyallbusRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Syllabus> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.Syllabus.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<Syllabus>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.Syllabus.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
