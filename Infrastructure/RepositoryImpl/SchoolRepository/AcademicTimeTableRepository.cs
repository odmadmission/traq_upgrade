﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class AcademicTimeTableRepository : EfRepository<AcademicTimeTable>, IAcademicTimeTableRepository
    {
        public AcademicTimeTableRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AcademicTimeTable> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicTimeTables.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }


        public async Task<IReadOnlyList<AcademicTimeTable>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicTimeTables.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicTimeTable>> ListAllAsyncIncludeAllNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicTimeTables.AsNoTracking().ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<AcademicTimeTable>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicTimeTables.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

    }
}
