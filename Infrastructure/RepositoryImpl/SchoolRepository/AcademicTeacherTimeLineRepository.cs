﻿using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class AcademicTeacherTimeLineRepository : EfRepository<AcademicTeacherTimeLine>, IAcademicTeacherTimeLineRepository
    {
        public AcademicTeacherTimeLineRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AcademicTeacherTimeLine> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicTeacherTimeLines.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicTeacherTimeLine>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicTeacherTimeLines.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
