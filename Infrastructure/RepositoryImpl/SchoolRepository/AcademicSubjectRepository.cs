﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;



namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class AcademicSubjectRepository: EfRepository<AcademicSubject>, IAcademicSubjectRepository
    {
        public AcademicSubjectRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AcademicSubject> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicSubjects.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicSubject>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicSubjects.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
