﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
    public class AcademicTeacherRepository : EfRepository<AcademicTeacher>, IAcademicTeacherRepository
    {
        public AcademicTeacherRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public Task<AcademicTeacher> GetByIdAsyncIncludeAll(long id)
        {
            throw new NotImplementedException();
        }

        public async Task<IReadOnlyList<AcademicTeacher>> ListAllAsyncIncludeAll()
        {
            var res = await _dbContext.AcademicTeachers.ToListAsync();

            return res;
        }
        public async Task<IReadOnlyList<AcademicTeacher>> ListAllAsyncIncludeAllNoTrack()
        {
            var res = await _dbContext.AcademicTeachers.AsNoTracking().Where(a=>a.Active==true).ToListAsync();

            return res;
        }
    }
}
