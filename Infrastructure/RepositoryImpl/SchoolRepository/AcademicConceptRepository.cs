﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
  public  class AcademicConceptRepository : EfRepository<AcademicConcept>, IAcademicConceptRepository
    {
        public AcademicConceptRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AcademicConcept> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicConcepts.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicConcept>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicConcepts.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
