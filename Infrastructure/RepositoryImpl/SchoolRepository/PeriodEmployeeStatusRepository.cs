﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class PeriodEmployeeStatusRepository : EfRepository<PeriodEmployeeStatus>, IPeriodEmployeeStatusRepository
    {
        public PeriodEmployeeStatusRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<PeriodEmployeeStatus> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.PeriodEmployeeStatuses.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }


        public async Task<IReadOnlyList<PeriodEmployeeStatus>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.PeriodEmployeeStatuses.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<PeriodEmployeeStatus>> ListAllAsyncIncludeAllNoTrack()
        {
            try
            {
                var res = _dbContext.PeriodEmployeeStatuses.AsNoTracking().ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<PeriodEmployeeStatus>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.PeriodEmployeeStatuses.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

    }
}