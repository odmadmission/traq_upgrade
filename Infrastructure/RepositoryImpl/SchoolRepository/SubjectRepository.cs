﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Interfaces;
using System.Data.SqlClient;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class SubjectRepository : EfRepository<Subject>, ISubjectRepository
    {

        public SubjectRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Subject> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.Subjects.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<Subject>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.Subjects.ToListAsync();
                return  res;
            }
            catch
            {
                return null;
            }
        }

       


    }
}
