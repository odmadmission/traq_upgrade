﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class AcademicStandardRepository : EfRepository<AcademicStandard>, IAcademicStandardRepository
    {
        public AcademicStandardRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AcademicStandard> GetByIdAsyncIncludeAll(long id)
        {

            try
            {
                var res = _dbContext.AcademicStandards.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicStandard>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await  _dbContext.AcademicStandards.ToListAsync();
                return  res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<AcademicStandard>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicStandards.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
