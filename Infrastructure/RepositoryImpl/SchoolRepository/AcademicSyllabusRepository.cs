﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
  
    public class AcademicSyllabusRepository : EfRepository<AcademicSyllabus>, IAcademicSyllabusRepository
    {
        public AcademicSyllabusRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AcademicSyllabus> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicSyllabus.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicSyllabus>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicSyllabus.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
