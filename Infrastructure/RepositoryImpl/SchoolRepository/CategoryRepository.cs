﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
    public class CategoryRepository : EfRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
         

        public async Task<Category> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var category = _dbContext.Categories.FirstOrDefaultAsync(a => a.ID == id);
                if (category != null)
                {
                    return await category;
                }              
                return null;
            }
            catch
            {
                return null;
            }
        }

        public async Task<Category> GetCategoryNameById(long id)
        {
            try
            {
                var categoryName = _dbContext.Categories.FirstOrDefaultAsync(a => a.ID == id);
                return await categoryName;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<Category>> ListAllAsyncIncludeAll()
        {
            try
            {
                var category = _dbContext.Categories.Where(a => a.Active == true && a.IsAvailable == true).ToListAsync();
                if (category != null)
                {
                    return await category;
                }

                return null;
            }
            catch
            {
                return null;
            }



        }
    }
}
