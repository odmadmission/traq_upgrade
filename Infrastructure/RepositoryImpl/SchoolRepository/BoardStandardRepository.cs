﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Interfaces;
using System.Data.SqlClient;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class BoardStandardRepository : EfRepository<BoardStandard>, IBoardStandardRepository
    {

        public BoardStandardRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        private readonly ApplicationDbContext _DbContext;
        public async Task<BoardStandard> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.BoardStandards.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<BoardStandard>> ListAllAsyncIncludeAll()
        {

            var res = _dbContext.BoardStandards.ToListAsync();

            return await res;
        }
        public async Task<IReadOnlyList<BoardStandard>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.BoardStandards.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }


        public long GetStandardCountByBoardId (long BOARDId)
        {
            try
            {
                long res = _dbContext.BoardStandards.ToListAsync().Result.Where(a => a.BoardId == BOARDId).Count();

                return res;
            }
            catch
            {
                return 0;
            }
        }

        public long GetBoardIdByBoardStandardId(long BoardStandardId)
        {
            try
            {
                var res = _dbContext.BoardStandards.ToListAsync().Result.Where(a => a.ID == BoardStandardId).Select(a => a.BoardId);

                return res.FirstOrDefault();
            }
            catch
            {
                return 0;
            }

        }
        //public long GetStandardCountByBoardId(long BroupId)
        //{
        //    try
        //    {
        //        long ob = _DbContext.Set<BoardStandard>().Where(a => a.BoardID == BroupId).Count();
        //        return ob;
        //    }
        //    catch
        //    {
        //        return 0;
        //    }
        //}
        //public List<BoardStandard> GetBoards()
        //{ 
        //    var res = _db
        //}
    }
}
