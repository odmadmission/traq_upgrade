﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
  public  class AcademicTeacherSubjectsTimeLineRepository : EfRepository<AcademicTeacherSubjectsTimeLine>, IAcademicTeacherSubjectsTimeLineRepository
    {
        public AcademicTeacherSubjectsTimeLineRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AcademicTeacherSubjectsTimeLine> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicTeacherSubjectsTimeLines.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicTeacherSubjectsTimeLine>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicTeacherSubjectsTimeLines.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
