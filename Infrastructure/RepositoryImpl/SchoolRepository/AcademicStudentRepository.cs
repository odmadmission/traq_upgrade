﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class AcademicStudentRepository : EfRepository<AcademicStudent>, IAcademicStudentRepository
    {
        public AcademicStudentRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

       
        public async Task<AcademicStudent> GetByIdAsyncIncludeAll(long id)
        {

            try
            {
                var res = _dbContext.AcademicStudents.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicStudent>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicStudents.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<AcademicStudent>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicStudents.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
