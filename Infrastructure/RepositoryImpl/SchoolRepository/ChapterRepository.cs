﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Interfaces;
using System.Data.SqlClient;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ChapterRepository : EfRepository<Chapter>, IChapterRepository
    {

        public ChapterRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
            
        public async Task<Chapter> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.Chapters.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<Chapter>> ListAllAsyncIncludeAll()
        {
            var res = _dbContext.Chapters.ToListAsync();
            return await res;
        }

        public long GetChapterCountByBoardStandardId(long SubjectId)
        {
            try
            {
                long res = _dbContext.Chapters.ToListAsync().Result.Where(a => a.SubjectId == SubjectId).Count();

                return res;
            }
            catch
            {
                return 0;
            }
        }

       


    }
}
