﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
     public class TeacherRepository : EfRepository<Teacher>, ITeacherRepository 
     {
        public TeacherRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Teacher> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.Teachers.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }


        public async Task<IReadOnlyList<Teacher>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.Teachers.ToListAsync();
                return await res;
            }
            catch(Exception e)
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<Teacher>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.Teachers.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
