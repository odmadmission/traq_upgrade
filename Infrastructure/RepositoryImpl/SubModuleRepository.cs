﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class SubModuleRepository : ISubModuleRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public SubModuleRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateSubModule(SubModule newSubModule)
        {
            try
            {
                newSubModule.InsertedDate = DateTime.Now;
                newSubModule.ModifiedDate = DateTime.Now;
                _DbContext.Set<SubModule>().Add(newSubModule);
                _DbContext.SaveChanges();
                return newSubModule.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllSubModule()
        {
            try
            {
                List<SubModule> res = _DbContext.Set<SubModule>().ToListAsync().Result;
                foreach (SubModule ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.SubModules.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteSubModule(long id)
        {
            try
            {
                SubModule ob = _DbContext.Set<SubModule>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.SubModules.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SubModule> GetAllSubModule()
        {
            try
            {
                IEnumerable<SubModule> res = _DbContext.Set<SubModule>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public SubModule GetSubModuleById(long id)
        {
            try
            {
                SubModule ob = _DbContext.Set<SubModule>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public SubModule GetSubModuleByName(string name)
        {
            try
            {
                SubModule ob = _DbContext.Set<SubModule>().FirstOrDefaultAsync(a => a.Name == name && a.Active == true).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateSubModule(SubModule changedSubModule)
        {
            try
            {
                changedSubModule.ModifiedDate = DateTime.Now;
                changedSubModule.Status = "UPDATED";
                _DbContext.SubModules.Attach(changedSubModule);
                var entry = _DbContext.Entry(changedSubModule);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
