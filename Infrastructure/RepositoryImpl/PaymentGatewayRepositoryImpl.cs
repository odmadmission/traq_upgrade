﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class PaymentGatewayRepositoryImpl : IPaymentGatewayRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public PaymentGatewayRepositoryImpl(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreatePaymentGateway(PaymentGateway newAction)
        {
            try
            {
                newAction.InsertedDate = DateTime.Now;
                newAction.ModifiedDate = DateTime.Now;
                _DbContext.Set<PaymentGateway>().Add(newAction);
                _DbContext.SaveChanges();
                return newAction.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllPaymentGateway()
        {
            try
            {
                List<PaymentGateway> res =
                    _DbContext.Set<PaymentGateway>().ToList();
                foreach (PaymentGateway ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.paymentGateway.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeletePaymentGateway(long id)
        {
            try
            {
                PaymentGateway ob = _DbContext.Set<PaymentGateway>().
                    FirstOrDefault(a => a.ID == id);
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.paymentGateway.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<PaymentGateway> GetAllPaymentGateway()
        {
            try
            {
                IEnumerable<PaymentGateway> res = 
                    _DbContext.Set<PaymentGateway>().AsNoTracking().
                    Where(a => a.Active == true).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public PaymentGateway GetPaymentGatewayById(long id)
        {
            try
            {
                PaymentGateway ob = _DbContext.Set<PaymentGateway>().FirstOrDefault(a => a.ID == id);
                return ob;
            }
            catch
            {
                return null;
            }
        }
    
        public bool UpdatePaymentGateway(PaymentGateway changedAction)
        {
            try
            {
                changedAction.ModifiedDate = DateTime.Now;
                changedAction.Status = "ACTIVE";
                _DbContext.paymentGateway.Attach(changedAction);
                var entry = _DbContext.Entry(changedAction);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }


        public long CreatePaymentGatewayBTB(PaymentGatewayBTB newAction)
        {
            try
            {
                newAction.InsertedDate = DateTime.Now;
                newAction.ModifiedDate = DateTime.Now;
                _DbContext.Set<PaymentGatewayBTB>().Add(newAction);
                _DbContext.SaveChanges();
                return newAction.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllPaymentGatewayBTB()
        {
            try
            {
                List<PaymentGatewayBTB> res =
                    _DbContext.Set<PaymentGatewayBTB>().ToList();
                foreach (PaymentGatewayBTB ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.paymentGatewayBTB.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeletePaymentGatewayBTB(long id)
        {
            try
            {
                PaymentGatewayBTB ob = _DbContext.Set<PaymentGatewayBTB>().
                    FirstOrDefault(a => a.ID == id);
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.paymentGatewayBTB.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<PaymentGatewayBTB> GetAllPaymentGatewayBTB()
        {
            try
            {
                IEnumerable<PaymentGatewayBTB> res =
                    _DbContext.Set<PaymentGatewayBTB>().
                    Where(a => a.Active == true).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public PaymentGatewayBTB GetPaymentGatewayBTBById(long id)
        {
            try
            {
                PaymentGatewayBTB ob = _DbContext.Set<PaymentGatewayBTB>().FirstOrDefault(a => a.ID == id);
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdatePaymentGatewayBTB(PaymentGatewayBTB changedAction)
        {
            try
            {
                changedAction.ModifiedDate = DateTime.Now;
                changedAction.Status = "ACTIVE";
                _DbContext.paymentGatewayBTB.Attach(changedAction);
                var entry = _DbContext.Entry(changedAction);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public PaymentSnapshot GetPaymentSnapshotById(long id)
        {
            try
            {
                PaymentSnapshot ob = 
                    _DbContext.Set<PaymentSnapshot>().
                    FirstOrDefault(a => a.ID == id);
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<PaymentSnapshot> GetAllPaymentSnapshot()
        {
            try
            {
                IEnumerable<PaymentSnapshot> res =
                    _DbContext.Set<PaymentSnapshot>().
                    Where(a => a.Active == true).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreatePaymentSnapshot(PaymentSnapshot obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<PaymentSnapshot>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch(Exception e)
            {
                return 0;
            }
        }

        public bool UpdatePaymentSnapshot(PaymentSnapshot changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = "ACTIVE";
                _DbContext.PaymentSnapshots.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeletePaymentSnapshot(long id)
        {
            try
            {
                PaymentSnapshot ob = _DbContext.Set<PaymentSnapshot>().
                    FirstOrDefault(a => a.ID == id);
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.PaymentSnapshots.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllPaymentSnapshot()
        {
            try
            {
                List<PaymentSnapshot> res =
                    _DbContext.Set<PaymentSnapshot>().ToList();
                foreach (PaymentSnapshot ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.PaymentSnapshots.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public PaymentSnapshot GetPaymentSnapshotBySnapshotId(string id)
        {
            try
            {
                PaymentSnapshot ob =
                    _DbContext.Set<PaymentSnapshot>().
                    FirstOrDefault(a => a.PaymentSnapshotId == Guid.Parse(id));
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public PaymentSnapshot GetPaymentSnapshotByAdmissionId(long reltid)
        {
            try
            {
                PaymentSnapshot ob =
                    _DbContext.Set<PaymentSnapshot>().
                    FirstOrDefault(a => a.RelatedId == reltid);
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public PaymentSnapshot GetPaymentSnapshotByTransactionId(string transId)
        {
            try
            {
                PaymentSnapshot ob =
                    _DbContext.Set<PaymentSnapshot>().
                    FirstOrDefault(a => a.UniqueTxnId == transId);
                return ob;
            }
            catch
            {
                return null;
            }
        }
    }
}
