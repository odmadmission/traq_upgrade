﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class BoardRepository : IBoardRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public BoardRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateBoard(Board newBoard)
        {
            try
            {
                newBoard.InsertedDate = DateTime.Now;
                newBoard.ModifiedDate = DateTime.Now;
                _DbContext.Set<Board>().Add(newBoard);
                _DbContext.SaveChanges();
                return newBoard.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllBoard()
        {
            try
            {
                List<Board> res = _DbContext.Set<Board>().ToListAsync().Result;
                foreach (Board ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Boards.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteBoard(long id)
        {
            try
            {
                Board ob = _DbContext.Set<Board>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Boards.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Board> GetAllBoard()
        {
            try
            {
                IEnumerable<Board> res = _DbContext.Set<Board>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Board GetBoardById(long id)
        {
            try
            {
                Board ob = _DbContext.Set<Board>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Board GetBoardByName(string name)
        {
            try
            {
                Board ob = _DbContext.Set<Board>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateBoard(Board changedBoard)
        {
            try
            {
                changedBoard.ModifiedDate = DateTime.Now;
                //changedBoard.Status = "UPDATED";
                _DbContext.Boards.Attach(changedBoard);
                var entry = _DbContext.Entry(changedBoard);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

       


    }
}
