﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class EmployeeDocumentAttachmentRepository : IEmployeeDocumentAttachmentRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public EmployeeDocumentAttachmentRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateEmployeeDocumentAttachment(EmployeeDocumentAttachment newEmployeeDocumentAttachment)
        {
            try
            {
                newEmployeeDocumentAttachment.InsertedDate = DateTime.Now;
                newEmployeeDocumentAttachment.ModifiedDate = DateTime.Now;
                _DbContext.Set<EmployeeDocumentAttachment>().Add(newEmployeeDocumentAttachment);
                _DbContext.SaveChanges();
                return newEmployeeDocumentAttachment.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllEmployeeDocumentAttachments()
        {
            try
            {
                List<EmployeeDocumentAttachment> res = _DbContext.Set<EmployeeDocumentAttachment>().ToListAsync().Result;
                foreach (EmployeeDocumentAttachment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.employeeDocumentAttachments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployeeDocumentAttachment(long id)
        {
            try
            {
                EmployeeDocumentAttachment ob = _DbContext.Set<EmployeeDocumentAttachment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.employeeDocumentAttachments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<EmployeeDocumentAttachment> GetAllEmployeeDocumentAttachments()
        {
            try
            {
                IEnumerable<EmployeeDocumentAttachment> res = _DbContext.Set<EmployeeDocumentAttachment>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public EmployeeDocumentAttachment GetEmployeeDocumentAttachmentById(long id)
        {
            try
            {
                EmployeeDocumentAttachment ob = _DbContext.Set<EmployeeDocumentAttachment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEmployeeDocumentAttachment(EmployeeDocumentAttachment changedEmployeeDocumentAttachment)
        {
            try
            {
                changedEmployeeDocumentAttachment.ModifiedDate = DateTime.Now;
                changedEmployeeDocumentAttachment.Status = "UPDATED";
                _DbContext.employeeDocumentAttachments.Attach(changedEmployeeDocumentAttachment);
                var entry = _DbContext.Entry(changedEmployeeDocumentAttachment);
                entry.State = EntityState.Modified;
                if (changedEmployeeDocumentAttachment.DocumentPath == null)
                {
                    entry.Property(e => e.DocumentPath).IsModified = false;
                }
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

