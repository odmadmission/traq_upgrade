﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class CityRepository : ICityRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public CityRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateCity(City newCity)
        {
            try
            {
                newCity.InsertedDate = DateTime.Now;
                newCity.ModifiedDate = DateTime.Now;
                _DbContext.Set<City>().Add(newCity);
                _DbContext.SaveChanges();
                return newCity.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllCity()
        {
            try
            {
                List<City> res = _DbContext.Set<City>().ToListAsync().Result;
                foreach (City ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Cities.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteCity(long id)
        {
            try
            {
                City ob = _DbContext.Set<City>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Cities.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<City> GetAllCity()
        {
            try
            {
                IEnumerable<City> res = _DbContext.Set<City>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<City> GetAllCityByStateId(long stateId)
        {
            try
            {
                IEnumerable<City> res = _DbContext.Set<City>().Where(a => a.Active == true && a.StateID== stateId).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public City GetCityById(long id)
        {
            try
            {
                City ob = _DbContext.Set<City>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public City GetCityByName(string name)
        {
            try
            {
                City ob = _DbContext.Set<City>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateCity(City changedCity)
        {
            try
            {
                changedCity.ModifiedDate = DateTime.Now;
                changedCity.Status = "UPDATED";
                _DbContext.Cities.Attach(changedCity);
                var entry = _DbContext.Entry(changedCity);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
