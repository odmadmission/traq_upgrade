﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class StudentAggregateRepository : IStudentAggregateRepository
    {

        private readonly ApplicationDbContext _DbContext;

        public StudentAggregateRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateParent(Parent newParent)
        {
            try
            {
                newParent.InsertedDate = DateTime.Now;
                newParent.ModifiedDate = DateTime.Now;
                _DbContext.Set<Parent>().Add(newParent);
                _DbContext.SaveChanges();
                return newParent.ID;
            }
            catch(Exception e1)
            {
                return 0;
            }
        }

        public long CreateParentRelationshipType(ParentRelationshipType newParentRelationshipType)
        {
            try
            {
                newParentRelationshipType.InsertedDate = DateTime.Now;
                newParentRelationshipType.ModifiedDate = DateTime.Now;
                _DbContext.Set<ParentRelationshipType>().Add(newParentRelationshipType);
                _DbContext.SaveChanges();
                return newParentRelationshipType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public long CreateProfessionType(ProfessionType newProfessionType)
        {
            try
            {
                newProfessionType.InsertedDate = DateTime.Now;
                newProfessionType.ModifiedDate = DateTime.Now;
                _DbContext.Set<ProfessionType>().Add(newProfessionType);
                _DbContext.SaveChanges();
                return newProfessionType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public long CreateStudent(Student newStudent)
        {
            try
            {
                newStudent.InsertedDate = DateTime.Now;
                newStudent.ModifiedDate = DateTime.Now;
                _DbContext.Set<Student>().Add(newStudent);
                _DbContext.SaveChanges();
                return newStudent.ID;
            }
            catch
            {
                return 0;
            }
        }

        public long CreateStudentAddress(StudentAddress newStudentAddress)
        {
            try
            {
                newStudentAddress.InsertedDate = DateTime.Now;
                newStudentAddress.ModifiedDate = DateTime.Now;
                _DbContext.Set<StudentAddress>().Add(newStudentAddress);
                _DbContext.SaveChanges();
                return newStudentAddress.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllParent()
        {
            try
            {
                List<Parent> res = _DbContext.Set<Parent>().ToListAsync().Result;
                foreach (Parent ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Parents.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllParentRelationshipType()
        {
            try
            {
                List<ParentRelationshipType> res = _DbContext.Set<ParentRelationshipType>().ToListAsync().Result;
                foreach (ParentRelationshipType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.ParentRelationshipTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllProfessionType()
        {
            try
            {
                List<ProfessionType> res = _DbContext.Set<ProfessionType>().ToListAsync().Result;
                foreach (ProfessionType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.ProfessionTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllStudent()
        {
            try
            {
                List<Student> res = _DbContext.Set<Student>().ToListAsync().Result;
                foreach (Student ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Students.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllStudentAddress()
        {
            try
            {
                List<StudentAddress> res = _DbContext.Set<StudentAddress>().ToListAsync().Result;
                foreach (StudentAddress ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.StudentAddresses.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteParent(long id)
        {
            try
            {
                Parent ob = _DbContext.Set<Parent>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Parents.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteParentRelationshipType(long id)
        {
            try
            {
                ParentRelationshipType ob = _DbContext.Set<ParentRelationshipType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.ParentRelationshipTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteProfessionType(long id)
        {
            try
            {
                ProfessionType ob = _DbContext.Set<ProfessionType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.ProfessionTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteStudent(long id)
        {
            try
            {
                Student ob = _DbContext.Set<Student>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Students.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteStudentAddress(long id)
        {
            try
            {
                StudentAddress ob = _DbContext.Set<StudentAddress>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.StudentAddresses.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Parent> GetAllParent()
        {
            try
            {
                IEnumerable<Parent> res = _DbContext.Set<Parent>().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<ParentRelationshipType> GetAllParentRelationshipType()
        {
            try
            {
                IEnumerable<ParentRelationshipType> res = _DbContext.Set<ParentRelationshipType>().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<ProfessionType> GetAllProfessionType()
        {
            try
            {
                IEnumerable<ProfessionType> res = _DbContext.Set<ProfessionType>().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Student> GetAllStudent()
        {
            try
            {
                IEnumerable<Student> res = _DbContext.Set<Student>().AsNoTracking().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public IEnumerable<StudentAddress> GetAllStudentAddress()
        {
            try
            {
                IEnumerable<StudentAddress> res = _DbContext.Set<StudentAddress>().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Parent GetParentById(long id)
        {
            try
            {
                Parent ob = _DbContext.Set<Parent>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }

        }

        public string GetParentById()
        {
            throw new NotImplementedException();
        }

        public ParentRelationshipType GetParentRelationshipTypeById(long id)
        {
            try
            {
                ParentRelationshipType ob = _DbContext.Set<ParentRelationshipType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public ProfessionType GetProfessionTypeById(long id)
        {
            try
            {
                ProfessionType ob = _DbContext.Set<ProfessionType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public StudentAddress GetStudentAddressById(long id)
        {
            try
            {
                StudentAddress ob = _DbContext.Set<StudentAddress>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public Student GetStudentById(long id)
        {
            try
            {
                Student ob = _DbContext.Set<Student>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public string GetStudentById()
        {
            throw new NotImplementedException();
        }

        public Student GetStudentByStudentCode(string studentcode)
        {
            try
            {
                Student ob = _DbContext.Set<Student>().
                    FirstOrDefaultAsync(a => a.StudentCode == studentcode).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateParent(Parent changedParent)
        {
            try
            {
                changedParent.ModifiedDate = DateTime.Now;
                changedParent.Status = EntityStatus.ACTIVE;
                _DbContext.Parents.Attach(changedParent);
                var entry = _DbContext.Entry(changedParent);
                entry.State = EntityState.Modified;
                if (changedParent.Image == null)
                {
                    entry.Property(e => e.Image).IsModified = false;
                }
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateParentRelationshipType(ParentRelationshipType changedParentRelationshipType)
        {
            try
            {
                changedParentRelationshipType.ModifiedDate = DateTime.Now;
                changedParentRelationshipType.Status = EntityStatus.ACTIVE;
                _DbContext.ParentRelationshipTypes.Attach(changedParentRelationshipType);
                var entry = _DbContext.Entry(changedParentRelationshipType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateProfessionType(ProfessionType changedProfessionType)
        {
            try
            {
                changedProfessionType.ModifiedDate = DateTime.Now;
                changedProfessionType.Status = EntityStatus.ACTIVE;
                _DbContext.ProfessionTypes.Attach(changedProfessionType);
                var entry = _DbContext.Entry(changedProfessionType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateStudent(Student changedStudent)
        {
            try
            {
                changedStudent.ModifiedDate = DateTime.Now;
                changedStudent.Status = EntityStatus.ACTIVE;
                _DbContext.Students.Attach(changedStudent);
                var entry = _DbContext.Entry(changedStudent);
                entry.State = EntityState.Modified;
                if (changedStudent.Image == null)
                {
                    entry.Property(e => e.Image).IsModified = false;
                }
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public bool UpdateStudentAddress(StudentAddress changedStudentAddress)
        {
            try
            {
                changedStudentAddress.ModifiedDate = DateTime.Now;
                changedStudentAddress.Status = EntityStatus.ACTIVE;
                _DbContext.StudentAddresses.Attach(changedStudentAddress);
                var entry = _DbContext.Entry(changedStudentAddress);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


        //----------------------------------------------


        public Wing GetWingById(long id)
        {
            try
            {
                Wing ob = _DbContext.Set<Wing>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public string GetWingById()
        {
            throw new NotImplementedException();
        }

        //public SchoolWing GetSchoolWingById(long id)
        //{
        //    try
        //    {
        //        SchoolWing ob = _DbContext.Set<SchoolWing>().FirstOrDefaultAsync(a => a.ID == id).Result;
        //        return ob;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        public string GetSchoolWingById()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Wing> GetAllWing()
        {
            try
            {
                IEnumerable<Wing> res = _DbContext.Set<Wing>().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        //public IEnumerable<SchoolWing> GetAllSchoolWing()
        //{
        //    try
        //    {
        //        IEnumerable<SchoolWing> res = _DbContext.Set<SchoolWing>().
        //            Where(a => a.Active == true).ToListAsync().Result;
        //        return res;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        public long CreateWing(Wing newWing)
        {
            try
            {
                newWing.InsertedDate = DateTime.Now;
                newWing.ModifiedDate = DateTime.Now;
                _DbContext.Set<Wing>().Add(newWing);
                _DbContext.SaveChanges();
                return newWing.ID;
            }
            catch
            {
                return 0;
            }
        }

        //public long CreateSchoolWing(SchoolWing newSchoolWing)
        //{
        //    try
        //    {
        //        newSchoolWing.InsertedDate = DateTime.Now;
        //        newSchoolWing.ModifiedDate = DateTime.Now;
        //        _DbContext.Set<SchoolWing>().Add(newSchoolWing);
        //        _DbContext.SaveChanges();
        //        return newSchoolWing.ID;
        //    }
        //    catch
        //    {
        //        return 0;
        //    }
        //}

        public bool UpdateWing(Wing changedWing)
        {
            try
            {
                changedWing.ModifiedDate = DateTime.Now;
                //changedWing.Status = EntityStatus.ACTIVE;
                _DbContext.Wing.Attach(changedWing);
                var entry = _DbContext.Entry(changedWing);
                entry.State = EntityState.Modified;
                //if (changedWing.Image == null)
                //{
                //    entry.Property(e => e.Image).IsModified = false;
                //}
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //public bool UpdateSchoolWing(SchoolWing changedSchoolWing)
        //{
        //    try
        //    {
        //        changedSchoolWing.ModifiedDate = DateTime.Now;
        //        //changedSchoolWing.Status = EntityStatus.ACTIVE;
        //        _DbContext.SchoolWing.Attach(changedSchoolWing);
        //        var entry = _DbContext.Entry(changedSchoolWing);
        //        entry.State = EntityState.Modified;
        //        //if (changedSchoolWing.Image == null)
        //        //{
        //        //    entry.Property(e => e.Image).IsModified = false;
        //        //}
        //        entry.Property(e => e.InsertedDate).IsModified = false;
        //        entry.Property(e => e.InsertedId).IsModified = false;
        //        _DbContext.SaveChanges();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        public bool DeleteWing(long id)
        {
            try
            {
                Wing ob = _DbContext.Set<Wing>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Wing.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //public bool DeleteSchoolWing(long id)
        //{
        //    try
        //    {
        //        SchoolWing ob = _DbContext.Set<SchoolWing>().FirstOrDefaultAsync(a => a.ID == id).Result;
        //        ob.Active = false;
        //        ob.ModifiedDate = DateTime.Now;
        //        ob.Status = EntityStatus.DELETED;
        //        _DbContext.SchoolWing.Attach(ob);
        //        var entry = _DbContext.Entry(ob);
        //        entry.State = EntityState.Modified;
        //        entry.Property(e => e.InsertedDate).IsModified = false;
        //        entry.Property(e => e.InsertedId).IsModified = false;
        //        _DbContext.SaveChanges();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        public bool DeleteAllWing()
        {
            try
            {
                List<Wing> res = _DbContext.Set<Wing>().ToListAsync().Result;
                foreach (Wing ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Wing.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        //public bool DeleteAllSchoolWing()
        //{
        //    try
        //    {
        //        List<SchoolWing> res = _DbContext.Set<SchoolWing>().ToListAsync().Result;
        //        foreach (SchoolWing ob in res)
        //        {
        //            ob.Active = false;
        //            ob.ModifiedDate = DateTime.Now;
        //            ob.Status = EntityStatus.DELETED;
        //            _DbContext.SchoolWing.Attach(ob);
        //            var entry = _DbContext.Entry(ob);
        //            entry.State = EntityState.Modified;
        //            entry.Property(e => e.InsertedDate).IsModified = false;
        //            entry.Property(e => e.InsertedId).IsModified = false;
        //            _DbContext.SaveChanges();
        //        }
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        //------------------------------------------------

        public StudentClass GetStudentClassById(long id)
        {
            try
            {
                StudentClass ob = _DbContext.Set<StudentClass>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public string GetStudentClassById()
        {
            throw new NotImplementedException();
        }
        public IEnumerable<StudentClass> GetAllStudentClass()
        {
            try
            {
                IEnumerable<StudentClass> res = _DbContext.Set<StudentClass>().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateStudentClass(StudentClass newStudentClass)
        {
            try
            {
                newStudentClass.InsertedDate = DateTime.Now;
                newStudentClass.ModifiedDate = DateTime.Now;
                _DbContext.Set<StudentClass>().Add(newStudentClass);
                _DbContext.SaveChanges();
                return newStudentClass.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateStudentClass(StudentClass changedStudentClass)
        {
            try
            {
                changedStudentClass.ModifiedDate = DateTime.Now;
                changedStudentClass.Status = EntityStatus.ACTIVE;
                _DbContext.StudentClass.Attach(changedStudentClass);
                var entry = _DbContext.Entry(changedStudentClass);
                entry.State = EntityState.Modified;
                //if (changedSchoolWing.Image == null)
                //{
                //    entry.Property(e => e.Image).IsModified = false;
                //}
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteStudentClass(long id)
        {
            try
            {
                StudentClass ob = _DbContext.Set<StudentClass>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.StudentClass.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteAllStudentClass()
        {
            try
            {
                List<StudentClass> res = _DbContext.Set<StudentClass>().ToListAsync().Result;
                foreach (StudentClass ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.StudentClass.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region----------- student Document-----------------------

        public StudentAcademic GetStudentAcademicById(long id)
        {
            try
            {
                StudentAcademic ob = _DbContext.Set<StudentAcademic>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

       
        public IEnumerable<StudentAcademic> GetAllStudentAcademic()
        {
            try
            {
                IEnumerable<StudentAcademic> res = _DbContext.Set<StudentAcademic>().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateStudentAcademic(StudentAcademic newStudentClass)
        {
            try
            {
                newStudentClass.InsertedDate = DateTime.Now;
                newStudentClass.ModifiedDate = DateTime.Now;
                _DbContext.Set<StudentAcademic>().Add(newStudentClass);
                _DbContext.SaveChanges();
                return newStudentClass.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateStudentAcademic(StudentAcademic changedStudentClass)
        {
            try
            {
                changedStudentClass.ModifiedDate = DateTime.Now;
                changedStudentClass.Status = EntityStatus.ACTIVE;
                _DbContext.StudentAcademics.Attach(changedStudentClass);
                var entry = _DbContext.Entry(changedStudentClass);
                entry.State = EntityState.Modified;               
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteStudentAcademic(long id)
        {
            try
            {
                StudentAcademic ob = _DbContext.Set<StudentAcademic>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.StudentAcademics.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteAllStudentAcademic()
        {
            try
            {
                List<StudentAcademic> res = _DbContext.Set<StudentAcademic>().ToListAsync().Result;
                foreach (StudentAcademic ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.StudentAcademics.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }


        public IEnumerable<StudentAcademic> GetStudentAcademicByStudentId(long studentid)
        {
            try
            {
                IEnumerable<StudentAcademic> ob = _DbContext.Set<StudentAcademic>().Where(a => a.StudentID == studentid && a.Active == true).ToList();
                return ob;
            }
            catch
            {
                return null;
            }
        }



        #endregion

        #region----------- student Required Document-----------------------

        public StudentRequiredDocument GetStudentRequiredDocumentById(long id)
        {
            try
            {
                StudentRequiredDocument ob = _DbContext.Set<StudentRequiredDocument>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }


        public IEnumerable<StudentRequiredDocument> GetAllStudentRequiredDocument()
        {
            try
            {
                IEnumerable<StudentRequiredDocument> res = _DbContext.Set<StudentRequiredDocument>().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateStudentRequiredDocument(StudentRequiredDocument newStudentDoc)
        {
            try
            {
                newStudentDoc.InsertedDate = DateTime.Now;
                newStudentDoc.ModifiedDate = DateTime.Now;
                _DbContext.Set<StudentRequiredDocument>().Add(newStudentDoc);
                _DbContext.SaveChanges();
                return newStudentDoc.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateStudentRequiredDocument(StudentRequiredDocument changedStudentDoc)
        {
            try
            {
                changedStudentDoc.ModifiedDate = DateTime.Now;
                changedStudentDoc.Status = EntityStatus.ACTIVE;
                _DbContext.StudentRequiredDocuments.Attach(changedStudentDoc);
                var entry = _DbContext.Entry(changedStudentDoc);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteStudentRequiredDocument(long id)
        {
            try
            {
                StudentRequiredDocument ob = _DbContext.Set<StudentRequiredDocument>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.StudentRequiredDocuments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteAllStudentRequiredDocument()
        {
            try
            {
                List<StudentRequiredDocument> res = _DbContext.Set<StudentRequiredDocument>().ToListAsync().Result;
                foreach (StudentRequiredDocument ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.StudentRequiredDocuments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region----------- student Attachment Document-----------------------

        public StudentDocumentAttachment GetStudentDocumentAttachmentById(long id)
        {
            try
            {
                StudentDocumentAttachment ob = _DbContext.Set<StudentDocumentAttachment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }


        public IEnumerable<StudentDocumentAttachment> GetAllStudentDocumentAttachment()
        {
            try
            {
                IEnumerable<StudentDocumentAttachment> res = _DbContext.Set<StudentDocumentAttachment>().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateStudentDocumentAttachment(StudentDocumentAttachment newStudentAttachment)
        {
            try
            {
                newStudentAttachment.InsertedDate = DateTime.Now;
                newStudentAttachment.ModifiedDate = DateTime.Now;
                _DbContext.Set<StudentDocumentAttachment>().Add(newStudentAttachment);
                _DbContext.SaveChanges();
                return newStudentAttachment.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateStudentDocumentAttachment(StudentDocumentAttachment changedStudentAttachment)
        {
            try
            {
                changedStudentAttachment.ModifiedDate = DateTime.Now;
                changedStudentAttachment.Status = EntityStatus.ACTIVE;
                _DbContext.StudentDocumentAttachments.Attach(changedStudentAttachment);
                var entry = _DbContext.Entry(changedStudentAttachment);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteStudentDocumentAttachment(long id)
        {
            try
            {
                StudentDocumentAttachment ob = _DbContext.Set<StudentDocumentAttachment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.StudentDocumentAttachments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteAllStudentDocumentAttachment()
        {
            try
            {
                List<StudentDocumentAttachment> res = _DbContext.Set<StudentDocumentAttachment>().ToListAsync().Result;
                foreach (StudentDocumentAttachment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.StudentDocumentAttachments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

    

    }

    public class StudentLanguagesKnownRepository : EfRepository<StudentLanguagesKnown>, IStudentLanguagesKnownRepository
    {
        public StudentLanguagesKnownRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<StudentLanguagesKnown> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.studentLanguagesKnowns.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<StudentLanguagesKnown>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.studentLanguagesKnowns.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
}
