﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class EmployeeDesignationRepository : IEmployeeDesignationRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public EmployeeDesignationRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateEmployeeDesignation(EmployeeDesignation newEmployeeDesignation)
        {
            try
            {
                newEmployeeDesignation.InsertedDate = DateTime.Now;
                newEmployeeDesignation.ModifiedDate = DateTime.Now;
                _DbContext.Set<EmployeeDesignation>().Add(newEmployeeDesignation);
                _DbContext.SaveChanges();
                return newEmployeeDesignation.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllEmployeeDesignations()
        {
            try
            {
                List<EmployeeDesignation> res = _DbContext.Set<EmployeeDesignation>().ToListAsync().Result;
                foreach (EmployeeDesignation ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.EmployeeDesignations.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployeeDesignation(long id)
        {
            try
            {
                EmployeeDesignation ob = _DbContext.Set<EmployeeDesignation>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.EmployeeDesignations.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<EmployeeDesignation> GetAllEmployeeDesignations()
        {
            try
            {
                IEnumerable<EmployeeDesignation> res = _DbContext.Set<EmployeeDesignation>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<EmployeeDesignation> GetAllEmployeeDesignationsByDesignationId(List<long> desgIds)
        {
            try
            {
                IEnumerable<EmployeeDesignation> res = _DbContext.Set<EmployeeDesignation>().Where(a => a.Active == true&& desgIds.Contains(a.DesignationID)).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<EmployeeDesignation> GetAllEmployeeDesignationsByEmployeeId(long empid)
        {
            try
            {
                IEnumerable<EmployeeDesignation> res = _DbContext.Set<EmployeeDesignation>().Where(a => a.Active == true && a.EmployeeID==empid).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public EmployeeDesignation GetEmployeeDesignationById(long id)
        {
            try
            {
                EmployeeDesignation ob = _DbContext.Set<EmployeeDesignation>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
     
        public bool UpdateEmployeeDesignation(EmployeeDesignation changedEmployeeDesignation)
        {
            try
            {
                changedEmployeeDesignation.ModifiedDate = DateTime.Now;
                changedEmployeeDesignation.Status = "UPDATED";
                _DbContext.EmployeeDesignations.Attach(changedEmployeeDesignation);
                var entry = _DbContext.Entry(changedEmployeeDesignation);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}