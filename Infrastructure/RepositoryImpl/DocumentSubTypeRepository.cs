﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.DocumentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class DocumentSubTypeRepository : IDocumentSubTypeRepository
    {

        private readonly ApplicationDbContext _DbContext;

        public DocumentSubTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateDocumentSubType(DocumentSubType newDocumentSubType)
        {
            try
            {
                newDocumentSubType.InsertedDate = DateTime.Now;
                newDocumentSubType.ModifiedDate = DateTime.Now;
                _DbContext.Set<DocumentSubType>().Add(newDocumentSubType);
                _DbContext.SaveChanges();
                return newDocumentSubType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllDocumentTypes()
        {
            try
            {
                List<DocumentSubType> res = _DbContext.Set<DocumentSubType>().ToListAsync().Result;
                foreach (DocumentSubType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.DocumentSubTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDocumentSubType(long id)
        {
            try
            {
                DocumentSubType ob = _DbContext.Set<DocumentSubType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.DocumentSubTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<DocumentSubType> GetAllDocumentSubTypes()
        {
            try
            {
                IEnumerable<DocumentSubType> res = _DbContext.Set<DocumentSubType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public DocumentSubType GetDocumentSubTypeById(long id)
        {
            try
            {
                DocumentSubType ob = _DbContext.Set<DocumentSubType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public DocumentSubType GetDocumentSubTypeByName(string name)
        {
            try
            {
                DocumentSubType ob = _DbContext.Set<DocumentSubType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<DocumentSubType> GetAllDocumentSubTypeByDocumentTypeId(long doctypeId)
        {
            try
            {
                IEnumerable<DocumentSubType> ob = _DbContext.Set<DocumentSubType>().Where(a => a.DocumentTypeID == doctypeId).ToList();
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public long GetAllDocumentSubTypeCountByDocumentTypeId(long doctypeId)
        {
            try
            {
                long ob = _DbContext.Set<DocumentSubType>().Where(a => a.DocumentTypeID == doctypeId).Count();
                return ob;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateDocumentSubType(DocumentSubType changedDocumentSubType)
        {
            try
            {
                changedDocumentSubType.ModifiedDate = DateTime.Now;
                changedDocumentSubType.Status = "UPDATED";
                _DbContext.DocumentSubTypes.Attach(changedDocumentSubType);
                var entry = _DbContext.Entry(changedDocumentSubType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
