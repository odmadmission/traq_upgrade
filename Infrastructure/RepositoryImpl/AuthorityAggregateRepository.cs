﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AuthorityAggregateRepository : IAuthorityAggregateRepository
    {

        private readonly ApplicationDbContext _DbContext;

        public AuthorityAggregateRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateAuthority(Authority newAuthority)
        {

            //try
            //{
            newAuthority.InsertedDate = DateTime.Now;
            newAuthority.ModifiedDate = DateTime.Now;
                _DbContext.Set<Authority>().Add(newAuthority);
                _DbContext.SaveChanges();
                return newAuthority.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        
        }

        public long CreateAuthorityType(AuthorityType newAuthorityType)
        {
            newAuthorityType.InsertedDate = DateTime.Now;
            newAuthorityType.ModifiedDate = DateTime.Now;
            _DbContext.Set<AuthorityType>().Add(newAuthorityType);
            _DbContext.SaveChanges();
            return newAuthorityType.ID;
        }

        public long CreateDepartmentAuthority(DepartmentAuthority newDepartmentAuthority)
        {
            newDepartmentAuthority.InsertedDate = DateTime.Now;
            newDepartmentAuthority.ModifiedDate = DateTime.Now;
            _DbContext.Set<DepartmentAuthority>().Add(newDepartmentAuthority);
            _DbContext.SaveChanges();
            return newDepartmentAuthority.ID;
        }

        public bool DeleteAllAuthority()
        {
            try
            {
                List<Authority> res = _DbContext.Set<Authority>().ToListAsync().Result;
                foreach (Authority ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Authorities.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAuthorityType()
        {
            try
            {
                List<AuthorityType> res = _DbContext.Set<AuthorityType>().ToListAsync().Result;
                foreach (AuthorityType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.AuthorityTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllDepartmentAuthority()
        {
            try
            {
                List<DepartmentAuthority> res = _DbContext.Set<DepartmentAuthority>().ToListAsync().Result;
                foreach (DepartmentAuthority ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.DepartmentAuthorities.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAuthority(long id)
        {
            try
            {
                Authority ob = _DbContext.Set<Authority>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Authorities.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAuthorityType(long id)
        {
            try
            {
                AuthorityType ob = _DbContext.Set<AuthorityType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.AuthorityTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDepartmentAuthority(long id)
        {
            try
            {
                DepartmentAuthority ob = _DbContext.Set<DepartmentAuthority>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.DepartmentAuthorities.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Authority> GetAllAuthority()
        {
            try
            {
                IEnumerable<Authority> res = _DbContext.Set<Authority>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AuthorityType> GetAllAuthorityType()
        {
           try
            {
                IEnumerable<AuthorityType> res = _DbContext.Set<AuthorityType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<DepartmentAuthority> GetAllDepartmentAuthority()
        {
            try
            {
                IEnumerable<DepartmentAuthority> res = _DbContext.
                    Set<DepartmentAuthority>().Where(a => a.Active == true).
                    ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Authority GetAuthorityById(long id)
        {
            try
            {
                Authority ob = _DbContext.Set<Authority>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public AuthorityType GetAuthorityTypeById(long id)
        {
            try
            {
                AuthorityType ob = _DbContext.Set<AuthorityType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public DepartmentAuthority GetDepartmentAuthorityById(long id)
        {
            try
            {
                DepartmentAuthority ob = _DbContext.Set<DepartmentAuthority>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateAuthority(Authority changedAuthority)
        {
            try
            {
                changedAuthority.ModifiedDate = DateTime.Now;
                changedAuthority.Status = EntityStatus.ACTIVE;
                _DbContext.Authorities.Attach(changedAuthority);
                var entry = _DbContext.Entry(changedAuthority);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateAuthorityType(AuthorityType changedAuthorityType)
        {
            try
            {
                changedAuthorityType.ModifiedDate = DateTime.Now;
                changedAuthorityType.Status = EntityStatus.ACTIVE;
                _DbContext.AuthorityTypes.Attach(changedAuthorityType);
                var entry = _DbContext.Entry(changedAuthorityType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateDepartmentAuthority(DepartmentAuthority changedDepartmentAuthority)
        {
            try
            {
                changedDepartmentAuthority.ModifiedDate = DateTime.Now;
                changedDepartmentAuthority.Status = EntityStatus.ACTIVE;
                _DbContext.DepartmentAuthorities.Attach(changedDepartmentAuthority);
                var entry = _DbContext.Entry(changedDepartmentAuthority);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
