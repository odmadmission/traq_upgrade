﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AdmissionRepository : IAdmissionRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public AdmissionRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        #region--------------------------Admission------------------
        public long CreateAdmission(Admission newAdmission)
        {
            try
            {
                newAdmission.InsertedDate = DateTime.Now;
                newAdmission.ModifiedDate = DateTime.Now;
                _DbContext.Set<Admission>().Add(newAdmission);
                _DbContext.SaveChanges();
                return newAdmission.ID;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteAllAdmission()
        {
            try
            {
                List<Admission> res = _DbContext.Set<Admission>().ToListAsync().Result;
                foreach (Admission ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Admissions.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAdmission(long id)
        {
            try
            {
                Admission ob = _DbContext.Set<Admission>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Admissions.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Admission> GetAllAdmission()
        {
            try
            {
                IEnumerable<Admission> res = _DbContext.Set<Admission>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public Admission GetAdmissionById(long id)
        {
            try
            {
                Admission ob = _DbContext.Set<Admission>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Admission GetAdmissionByLeadReferenceId(string id)
        {
            try
            {
                Admission ob = _DbContext.Set<Admission>().
                    FirstOrDefaultAsync(a => a.LeadReferenceId == Guid.Parse(id)).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }


        public bool UpdateAdmission(Admission changedAdmission)
        {
            try
            {
                changedAdmission.ModifiedDate = DateTime.Now;              
                _DbContext.Admissions.Attach(changedAdmission);
                var entry = _DbContext.Entry(changedAdmission);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        #endregion------------------------------------------
        #region-----------------Addmission slot-------------------
        public AdmissionSlot GetAdmissionSlotById(long id)
        {
            try
            {
                AdmissionSlot ob = _DbContext.Set<AdmissionSlot>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AdmissionSlot> GetAllAdmissionSlot()
        {
            try
            {
                IEnumerable<AdmissionSlot> res = _DbContext.Set<AdmissionSlot>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
      
        public long CreateAdmissionSlot(AdmissionSlot newAdmission)
        {
            try
            {
                newAdmission.InsertedDate = DateTime.Now;
                newAdmission.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionSlot>().Add(newAdmission);
                _DbContext.SaveChanges();
                return newAdmission.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateAdmissionSlot(AdmissionSlot changedAdmission)
        {

            try
            {
                changedAdmission.ModifiedDate = DateTime.Now;               
                _DbContext.AdmissionSlots.Attach(changedAdmission);
                var entry = _DbContext.Entry(changedAdmission);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAdmissionSlot(long id)
        {
            try
            {
                AdmissionSlot ob = _DbContext.Set<AdmissionSlot>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmissionSlots.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAdmissionSlot()
        {
            try
            {
                List<AdmissionSlot> res = _DbContext.Set<AdmissionSlot>().ToListAsync().Result;
                foreach (AdmissionSlot ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AdmissionSlots.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion-------------------------------------------------------

        #region-----------------Admission Declaration------------------
        public AdmissionStudentDeclaration GetAdmissionStudentDeclarationById(long id)
        {
            try
            {
                AdmissionStudentDeclaration ob = _DbContext.Set<AdmissionStudentDeclaration>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AdmissionStudentDeclaration> GetAllAdmissionStudentDeclaration()
        {
            try
            {
                IEnumerable<AdmissionStudentDeclaration> res = _DbContext.Set<AdmissionStudentDeclaration>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateAdmissionStudentDeclaration(AdmissionStudentDeclaration newAdmission)
        {
            try
            {
                newAdmission.InsertedDate = DateTime.Now;
                newAdmission.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionStudentDeclaration>().Add(newAdmission);
                _DbContext.SaveChanges();
                return newAdmission.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateAdmissionStudentDeclaration(AdmissionStudentDeclaration changedAdmission)
        {

            try
            {
                changedAdmission.ModifiedDate = DateTime.Now;              
                _DbContext.AdmissionStudentDeclarations.Attach(changedAdmission);
                var entry = _DbContext.Entry(changedAdmission);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAdmissionStudentDeclaration(long id)
        {
            try
            {
                AdmissionStudentDeclaration ob = _DbContext.Set<AdmissionStudentDeclaration>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmissionStudentDeclarations.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAdmissionStudentDeclaration()
        {
            try
            {
                List<AdmissionStudentDeclaration> res = _DbContext.Set<AdmissionStudentDeclaration>().ToListAsync().Result;
                foreach (AdmissionStudentDeclaration ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AdmissionStudentDeclarations.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion-------------------------------------------------------

        #region-----------------Admission Source------------------
        public AdmissionSource GetAdmissionSourceById(long id)
        {
            try
            {
                AdmissionSource ob = _DbContext.Set<AdmissionSource>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AdmissionSource> GetAllAdmissionSource()
        {
            try
            {
                IEnumerable<AdmissionSource> res = _DbContext.Set<AdmissionSource>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public long CreateAdmissionSource(AdmissionSource newAdmission)
        {
            try
            {
                newAdmission.InsertedDate = DateTime.Now;
                newAdmission.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionSource>().Add(newAdmission);
                _DbContext.SaveChanges();
                return newAdmission.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateAdmissionSource(AdmissionSource changedAdmission)
        {

            try
            {
                changedAdmission.ModifiedDate = DateTime.Now;               
                _DbContext.AdmissionSources.Attach(changedAdmission);
                var entry = _DbContext.Entry(changedAdmission);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAdmissionSource(long id)
        {
            try
            {
                AdmissionSource ob = _DbContext.Set<AdmissionSource>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmissionSources.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAdmissionSource()
        {
            try
            {
                List<AdmissionSource> res = _DbContext.Set<AdmissionSource>().ToListAsync().Result;
                foreach (AdmissionSource ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AdmissionSources.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion-------------------------------------------------------

        #region-----------------Admission Standard Setting------------------
        public AdmissionStandardSetting GetAdmissionStandardSettingById(long id)
        {
            try
            {
                AdmissionStandardSetting ob = _DbContext.Set<AdmissionStandardSetting>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AdmissionStandardSetting> GetAllAdmissionStandardSetting()
        {
            try
            {
                IEnumerable<AdmissionStandardSetting> res = _DbContext.Set<AdmissionStandardSetting>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public long CreateAdmissionStandardSetting(AdmissionStandardSetting newAdmission)
        {
            try
            {
                newAdmission.InsertedDate = DateTime.Now;
                newAdmission.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionStandardSetting>().Add(newAdmission);
                _DbContext.SaveChanges();
                return newAdmission.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateAdmissionStandardSetting(AdmissionStandardSetting changedAdmission)
        {

            try
            {
                changedAdmission.ModifiedDate = DateTime.Now;               
                _DbContext.AdmissionStandardSetting.Attach(changedAdmission);
                var entry = _DbContext.Entry(changedAdmission);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAdmissionStandardSetting(long id)
        {
            try
            {
                AdmissionStandardSetting ob = _DbContext.Set<AdmissionStandardSetting>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmissionStandardSetting.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAdmissionStandardSetting()
        {
            try
            {
                List<AdmissionStandardSetting> res = _DbContext.Set<AdmissionStandardSetting>().ToListAsync().Result;
                foreach (AdmissionStandardSetting ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AdmissionStandardSetting.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion-------------------------------------------------------
        #region---------------------------AdmissionTimeline------------------
        public AdmissionTimeline GetAdmissionTimelineById(long id)
        {
            try
            {
                AdmissionTimeline ob = _DbContext.Set<AdmissionTimeline>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AdmissionTimeline> GetAllAdmissionTimeline()
        {
            try
            {
                IEnumerable<AdmissionTimeline> res = _DbContext.Set<AdmissionTimeline>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateAdmissionTimeline(AdmissionTimeline newAdmission)
        {
            try
            {
                newAdmission.InsertedDate = DateTime.Now;
                newAdmission.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionTimeline>().Add(newAdmission);
                _DbContext.SaveChanges();
                return newAdmission.ID;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        public bool UpdateAdmissionTimeline(AdmissionTimeline changedAdmission)
        {
            try
            {
                changedAdmission.ModifiedDate = DateTime.Now;              
                _DbContext.AdmissionTimelines.Attach(changedAdmission);
                var entry = _DbContext.Entry(changedAdmission);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAdmissionTimeline(long id)
        {
            try
            {
                AdmissionTimeline ob = _DbContext.Set<AdmissionTimeline>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmissionTimelines.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAdmissionTimeline()
        {
            try
            {
                List<AdmissionTimeline> res = _DbContext.Set<AdmissionTimeline>().
                    ToListAsync().Result;
                foreach (AdmissionTimeline ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AdmissionTimelines.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public AdmissionSlot GetAdmissionSlotByName(string name)
        {
            try
            {
                AdmissionSlot ob = _DbContext.Set<AdmissionSlot>().
                    FirstOrDefaultAsync(a => a.SlotName == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        #endregion-----------------------------------------------------------

        #region ------admissionStandardExamQuestion--------
        public AdmissionStandardExamQuestion GetAdmissionStandardExamQuestionById(long id)
        {
            try
            {
                AdmissionStandardExamQuestion ob = _DbContext.Set<AdmissionStandardExamQuestion>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AdmissionStandardExamQuestion> GetAllAdmissionStandardExamQuestion()
        {
            try
            {
                IEnumerable<AdmissionStandardExamQuestion> res = _DbContext.Set<AdmissionStandardExamQuestion>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public long CreateAdmissionStandardExamQuestion(AdmissionStandardExamQuestion newAdmissionStandardExamQuestion)
        {
            try
            {
                newAdmissionStandardExamQuestion.InsertedDate = DateTime.Now;
                newAdmissionStandardExamQuestion.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionStandardExamQuestion>().Add(newAdmissionStandardExamQuestion);
                _DbContext.SaveChanges();
                return newAdmissionStandardExamQuestion.ID;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        public bool UpdateAdmissionStandardExamQuestion(AdmissionStandardExamQuestion changedAdmissionStandardExamQuestion)
        {

            try
            {
                changedAdmissionStandardExamQuestion.ModifiedDate = DateTime.Now;
                _DbContext.admissionStandardExamQuestions.Attach(changedAdmissionStandardExamQuestion);
                var entry = _DbContext.Entry(changedAdmissionStandardExamQuestion);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public AdmissionFormPayment GetAdmissionFormPaymentById(long id)
        {
            try
            {
                AdmissionFormPayment res =
                    _DbContext.Set<AdmissionFormPayment>().
                    Where(a => a.Active == true&&a.ID==id).FirstOrDefault();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public AdmissionFormPayment GetAdmissionFormPaymentByAdmissionId(long id)
        {
            try
            {
                AdmissionFormPayment res =
                    _DbContext.Set<AdmissionFormPayment>().
                    Where(a => a.Active == true&&a.AdmissionId==id).FirstOrDefault();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AdmissionFormPayment> GetAllAdmissionFormPayment()
        {
            try
            {
                IEnumerable<AdmissionFormPayment> res =
                    _DbContext.Set<AdmissionFormPayment>().
                    Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateAdmissionFormPayment(AdmissionFormPayment newAdmission)
        {
            try
            {
                newAdmission.InsertedDate = DateTime.Now;
                newAdmission.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionFormPayment>().Add(newAdmission);
                _DbContext.SaveChanges();
                return newAdmission.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateAdmissionFormPayment(AdmissionFormPayment changedAdmission)
        {
            try
            {
                changedAdmission.ModifiedDate = DateTime.Now;
                _DbContext.AdmissionFormPayments.Attach(changedAdmission);
                var entry = _DbContext.Entry(changedAdmission);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAdmissionFormPayment(long id)
        {
            try
            {
                AdmissionFormPayment ob = _DbContext.Set<AdmissionFormPayment>()
                    .FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmissionFormPayments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAdmissionFormPayment()
        {
            throw new NotImplementedException();
        }

        public AdmissionFee GetAdmissionFeeById(string name, long id)
        {
            try
            {
                AdmissionFee res =
                    _DbContext.Set<AdmissionFee>().
                    Where(a => a.Active == true && a.AcademicSessionId == id
                    && a.Name == name).FirstOrDefault();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public AdmissionStudent GetAdmissionStudentById(long id)
        {
            try
            {
                AdmissionStudent res =
                    _DbContext.Set<AdmissionStudent>().
                    Where(a => a.Active == true && a.AdmissionId == id
                    ).FirstOrDefault();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public AcademicSession GetCurrentAdmissionSession()
        {
            try
            {
                AcademicSession res =
                    _DbContext.Set<AcademicSession>().
                    Where(a => a.Active == true && a.IsAdmission == true).FirstOrDefault();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Admission GetAdmissionByMobileAndSessionId(string mobile, long sessionId)
        {
            try
            {
                Admission res =
                    _DbContext.Set<Admission>().
                    Where(a => a.MobileNumber==mobile&&a.AcademicSessionId== sessionId).FirstOrDefault();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public OtpMessage SaveOtpMessage(OtpMessage otpMessage)
        {
            try
            {
                otpMessage.InsertedDate = DateTime.Now;
                otpMessage.ModifiedDate = DateTime.Now;
                _DbContext.Set<OtpMessage>().Add(otpMessage);
                _DbContext.SaveChanges();
                return otpMessage;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public long UpdateOtpMessage(OtpMessage otpMessage)
        {
            try
            {
                otpMessage.ModifiedDate = DateTime.Now;
                _DbContext.OtpMessages.Attach(otpMessage);
                var entry = _DbContext.Entry(otpMessage);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public OtpMessage GetOtpMessageById(long id)
        {


            try
            {
                OtpMessage res =
                    _DbContext.Set<OtpMessage>().
                    Where(a => a.ID==id).FirstOrDefault();
                return res;
            }
            catch
            {
                return null;
            }

        }

        public Login SaveLogin(Login login)
        {
            try
            {
                login.InsertedDate = DateTime.Now;
                login.ModifiedDate = DateTime.Now;
                _DbContext.Set<Login>().Add(login);
                _DbContext.SaveChanges();
                return login;
            }
            catch
            {
                return null;
            }
        }

        public AdmissionStudentStandard GetAdmissionStudentStandardByAdmissionStudentId(long id)
        {
            try
            {
                AdmissionStudentStandard res =
                    _DbContext.Set<AdmissionStudentStandard>().
                    Where(a => a.AdmissionStudentId == id).FirstOrDefault();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public AdmissionStudentContact GetAdmissionStudentContactByAdmissionStudentId(long id)
        {
            try{
                AdmissionStudentContact res =
                    _DbContext.Set<AdmissionStudentContact>().
                    Where(a => a.AdmissionStudentId == id&&a.ConatctType=="official").FirstOrDefault();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long UpdateAdmissionStudent(AdmissionStudent admStudent)
        {
            try
            {
                admStudent.ModifiedDate = DateTime.Now;
                _DbContext.AdmissionStudents.Attach(admStudent);
                var entry = _DbContext.Entry(admStudent);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public long UpdateAdmissionStudentStandard(AdmissionStudentStandard obj)
        {
            try
            {
                obj.ModifiedDate = DateTime.Now;
                _DbContext.AdmissionStudentStandards.Attach(obj);
                var entry = _DbContext.Entry(obj);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public long UpdateAdmissionStudentContact(AdmissionStudentContact obj)
        {
            try
            {
                obj.ModifiedDate = DateTime.Now;
                _DbContext.AdmissionStudentContacts.Attach(obj);
                var entry = _DbContext.Entry(obj);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public AdmissionStudent CreateAdmissionStudent(AdmissionStudent admStudent)
        {
            try
            {
                admStudent.InsertedDate = DateTime.Now;
                admStudent.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionStudent>().Add(admStudent);
                _DbContext.SaveChanges();
                return admStudent;
            }
            catch
            {
                return null;
            }
        }

        public AdmissionStudentStandard CreateAdmissionStudentStandard(AdmissionStudentStandard admStudent)
        {
            try
            {
                admStudent.InsertedDate = DateTime.Now;
                admStudent.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionStudentStandard>().Add(admStudent);
                _DbContext.SaveChanges();
                return admStudent;
            }
            catch
            {
                return null;
            }
        }

        public AdmissionStudentContact CreateAdmissionStudentContact(AdmissionStudentContact admStudent)
        {
            try
            {
                admStudent.InsertedDate = DateTime.Now;
                admStudent.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionStudentContact>().Add(admStudent);
                _DbContext.SaveChanges();
                return admStudent;
            }
            catch
            {
                return null;
            }
        }

        //public AdmissionFormPayment GetAdmissionFormPaymentByAdmissionId(string id)
        //{
        //    throw new NotImplementedException();
        //}



        //AdmissionStandardExamQuestion GetAdmissionStandardExamQuestionById(long id);
        ////AdmissionSlot GetAdmissionStandardExamQuestionByName(string name);
        //IEnumerable<AdmissionStandardExamQuestion> GetAllAdmissionStandardExamQuestion();
        //long CreateAdmissionStandardExamQuestion(AdmissionStandardExamQuestion newAdmissionStandardExamQuestion);
        //bool UpdateAdmissionStandardExamQuestion(AdmissionStandardExamQuestion changedAdmissionStandardExamQuestion);
        #endregion

    }
}
