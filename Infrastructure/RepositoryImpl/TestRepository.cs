﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class TestRepository : ITestRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public TestRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
       
        public IEnumerable<string> GetAllTablesName()
        {
            //var tablesName = _DbContext.Query<string>().AsNoTracking().FromSql("EXEC GetAllTablesNameFromDatabase").ToList();
            // var tablesName= _DbContext.Query<GetAllTablesNameFromDatabase>().AsNoTracking().FromSql("EXEC GetAllTablesNameFromDatabase").ToList();

            //List<string> results = _DbContext.("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG = 'ODMERPDB'").ToList();


            //var result = _DbContext.Database
            // .(new RawSqlString("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG = 'ODMERPDB'"));


            //return null ;


            return DbContextExtensions.GetTableNames(_DbContext);
        }

            
    }
}
