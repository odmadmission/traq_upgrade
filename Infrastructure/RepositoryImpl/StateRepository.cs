﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class StateRepository : IStateRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public StateRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateAllState(List<State> newState)
        {
          
            try
            {
                List<State> saveList = new List<State>();

                  foreach(State state in newState)
                {
                    state.InsertedDate = DateTime.Now;
                    state.ModifiedDate = DateTime.Now;
                    saveList.Add(state);
                }

                _DbContext.Set<State>().AddRange(saveList);
                return saveList.Count();

            }
            catch
            {
                return 0;
            }
        }

        public long CreateState(State newState)
        {
            try
            {
                newState.InsertedDate = DateTime.Now;
                newState.ModifiedDate = DateTime.Now;
                _DbContext.Set<State>().Add(newState);
                _DbContext.SaveChanges();
                return newState.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllState()
        {
            try
            {
                List<State> res = _DbContext.Set<State>().ToListAsync().Result;
                foreach (State ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.States.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteState(long id)
        {
            try
            {
                State ob = _DbContext.Set<State>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.States.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<State> GetAllState()
        {
            try
            {
                IEnumerable<State> res = _DbContext.Set<State>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<State> GetAllStateByCountryId(long countryId)
        {
            try
            {
                IEnumerable<State> res = _DbContext.Set<State>().Where(a => a.Active == true && a.CountryID== countryId).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public State GetStateById(long id)
        {
            try
            {
                State ob = _DbContext.Set<State>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public State GetStateByName(string name)
        {
            try
            {
                State ob = _DbContext.Set<State>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateState(State changedState)
        {
            try
            {
                changedState.ModifiedDate = DateTime.Now;
                changedState.Status = "UPDATED";
                _DbContext.States.Attach(changedState);
                var entry = _DbContext.Entry(changedState);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
