﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Interfaces;
using System.Data.SqlClient;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ErpBugRepository : EfRepository<ErpBug>, IErpBugRepository
    {

        public ErpBugRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<ErpBug> GetByIdAsyncIncludeAll(long id)
        {
            var res = _dbContext.ErpBugs.FirstOrDefaultAsync(a => a.ID == id);
            return await res;
        }

        public async Task<IReadOnlyList<ErpBug>> ListAllAsyncIncludeAll()
        {
            var res = _dbContext.ErpBugs.ToListAsync();
            return await res;
        }



    }
}
