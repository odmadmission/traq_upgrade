﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class DesignationLevelRepository : IDesignationLevelRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public DesignationLevelRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateDesignationLevel(DesignationLevel newDesignationLevel)
        {
            try
            {
                newDesignationLevel.InsertedDate = DateTime.Now;
                newDesignationLevel.ModifiedDate = DateTime.Now;
                _DbContext.Set<DesignationLevel>().Add(newDesignationLevel);
                _DbContext.SaveChanges();
                return newDesignationLevel.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllDesignationLevel()
        {
            try
            {
                List<DesignationLevel> res = _DbContext.Set<DesignationLevel>().ToListAsync().Result;
                foreach (DesignationLevel ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.DesignationLevels.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDesignationLevel(long id)
        {
            try
            {
                DesignationLevel ob = _DbContext.Set<DesignationLevel>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.DesignationLevels.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<DesignationLevel> GetAllDesignationLevel()
        {
            try
            {
                IEnumerable<DesignationLevel> res = _DbContext.Set<DesignationLevel>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public DesignationLevel GetDesignationLevelById(long id)
        {
            try
            {
                DesignationLevel ob = _DbContext.Set<DesignationLevel>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public DesignationLevel GetDesignationLevelByName(string name)
        {
            try
            {
                DesignationLevel ob = _DbContext.Set<DesignationLevel>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateDesignationLevel(DesignationLevel changedDesignationLevel)
        {
            try
            {
                changedDesignationLevel.ModifiedDate = DateTime.Now;
                changedDesignationLevel.Status = "UPDATED";
                _DbContext.DesignationLevels.Attach(changedDesignationLevel);
                var entry = _DbContext.Entry(changedDesignationLevel);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
