﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AdmissionLoginRepository : IAdmissionLoginRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public AdmissionLoginRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateAdmissionLogin(AdmissionLogin newAdmissionLogin)
        {
            try
            {
                newAdmissionLogin.InsertedDate = DateTime.Now;
                newAdmissionLogin.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionLogin>().Add(newAdmissionLogin);
                _DbContext.SaveChanges();
                return newAdmissionLogin.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAdmissionLogin(long id)
        {
            throw new NotImplementedException();
        }

        public bool DeleteAllAdmissionLogin()
        {
            throw new NotImplementedException();
        }

        public AdmissionLogin GetAdmissionLoginById(long id)
        {
            try
            {
                AdmissionLogin ob = _DbContext.Set<AdmissionLogin>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AdmissionLogin> GetAllAdmissionLogin()
        {
            try
            {
                IEnumerable<AdmissionLogin> res = _DbContext.Set<AdmissionLogin>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateAdmissionLogin(AdmissionLogin changedAdmissionLogin)
        {
            try
            {
                changedAdmissionLogin.ModifiedDate = DateTime.Now;
                //changedBoard.Status = "UPDATED";
                _DbContext.AdmissionLogins.Attach(changedAdmissionLogin);
                var entry = _DbContext.Entry(changedAdmissionLogin);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
