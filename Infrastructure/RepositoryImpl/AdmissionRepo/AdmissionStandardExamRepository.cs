﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionStandardExamRepository : EfRepository<AdmissionStandardExam>, IAdmissionStandardExamRepository
    {
        public AdmissionStandardExamRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionStandardExam> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionStandardExams.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStandardExam>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionStandardExams.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStandardExam>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionStandardExams.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
