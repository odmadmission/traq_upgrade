﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
  public  class AdmissionEcounsellingRepository : EfRepository<AdmissionEcounselling>, IAdmissionEcounsellingRepository

    {
        public AdmissionEcounsellingRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionEcounselling> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionEcounsellings.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionEcounselling>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionEcounsellings.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
