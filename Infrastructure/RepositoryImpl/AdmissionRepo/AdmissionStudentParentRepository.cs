﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionStudentParentRepository : EfRepository<AdmissionStudentParent>, IAdmissionStudentParentRepository
    {
        public AdmissionStudentParentRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionStudentParent> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionStudentParents.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStudentParent>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionStudentParents.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStudentParent>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionStudentParents.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
