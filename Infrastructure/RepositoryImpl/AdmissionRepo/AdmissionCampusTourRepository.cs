﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionCampusTourRepository : EfRepository<AdmissionCampusTour>, IAdmissionCampusTourRepository

    {
        public AdmissionCampusTourRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionCampusTour> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionCampusTours.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionCampusTour>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionCampusTours.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
