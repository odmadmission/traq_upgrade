﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
   public class AdmissionEcounsellingDateRepository : EfRepository<AdmissionEcounsellingDate>, IAdmissionEcounsellingDateRepository

    {
        public AdmissionEcounsellingDateRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionEcounsellingDate> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionEcounsellingDates.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionEcounsellingDate>> ListAllAsyncIncludeAll()
        {

            try
            {
                var res = _dbContext.AdmissionEcounsellingDates.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
