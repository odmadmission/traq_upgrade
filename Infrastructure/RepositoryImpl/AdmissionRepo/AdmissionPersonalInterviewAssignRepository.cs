﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionPersonalInterviewAssignRepository : EfRepository<AdmissionPersonalInterviewAssign>, IAdmissionPersonalInterviewAssignRepository
    {
        public AdmissionPersonalInterviewAssignRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionPersonalInterviewAssign> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionPersonalInterviewAssigns.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionPersonalInterviewAssign>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionPersonalInterviewAssigns.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionPersonalInterviewAssign>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionPersonalInterviewAssigns.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
