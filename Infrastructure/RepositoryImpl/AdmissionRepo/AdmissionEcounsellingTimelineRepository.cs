﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionEcounsellingTimelineRepository : EfRepository<AdmissionEcounsellingTimeline>, IAdmissionEcounsellingTimelineRepository

    {
        public AdmissionEcounsellingTimelineRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionEcounsellingTimeline> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionEcounsellingTimelines.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionEcounsellingTimeline>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionEcounsellingTimelines.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
