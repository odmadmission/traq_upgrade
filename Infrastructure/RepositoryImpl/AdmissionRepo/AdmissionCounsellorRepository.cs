﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AdmissionCounsellorRepository : EfRepository<AdmissionCounsellor>,
        IAdmissionCounsellorRepository
    {
        public AdmissionCounsellorRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionCounsellor> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionCounsellor.Where(a => a.ID == id && a.Active == true).FirstOrDefaultAsync();
                return await res;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public async Task<IReadOnlyList<AdmissionCounsellor>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionCounsellor.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public async Task<IReadOnlyList<AdmissionCounsellor>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionCounsellor.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
