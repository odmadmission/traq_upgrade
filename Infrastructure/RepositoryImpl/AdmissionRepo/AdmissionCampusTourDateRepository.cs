﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionCampusTourDateRepository : EfRepository<AdmissionCampusTourDate>, IAdmissionCampusTourDateRepository

    {
        public AdmissionCampusTourDateRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionCampusTourDate> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionCampusTourDates.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionCampusTourDate>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionCampusTourDates.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
