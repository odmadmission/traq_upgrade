﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
   public class AdmissionStudentKitRepository : EfRepository<AdmissionStudentKit>,
        IAdmissionStudentKitRepository
    {
        public AdmissionStudentKitRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionStudentKit> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionStudentKits.Where(a => a.ID == id && a.Active == true).FirstOrDefaultAsync();
                return await res;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public async Task<IReadOnlyList<AdmissionStudentKit>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionStudentKits.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public async Task<IReadOnlyList<AdmissionStudentKit>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionStudentKits.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
