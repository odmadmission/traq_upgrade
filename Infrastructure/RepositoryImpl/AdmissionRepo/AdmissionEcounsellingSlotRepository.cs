﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionEcounsellingSlotRepository : EfRepository<AdmissionEcounsellingSlot>, IAdmissionEcounsellingSlotRepository

    {
        public AdmissionEcounsellingSlotRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionEcounsellingSlot> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionEcounsellingSlots.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionEcounsellingSlot>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionEcounsellingSlots.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
