﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionCampusTourSlotRepository : EfRepository<AdmissionCampusTourSlot>, IAdmissionCampusTourSlotRepository

    {
        public AdmissionCampusTourSlotRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionCampusTourSlot> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionCampusTourSlots.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionCampusTourSlot>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionCampusTourSlots.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
