﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmitcardDetailsRepository : IAdmitcardDetailsRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public AdmitcardDetailsRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateAdmitcardDetails(AdmitcardDetails newAdmitcardDetails)
        {
            try
            {
                newAdmitcardDetails.InsertedDate = DateTime.Now;
                newAdmitcardDetails.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmitcardDetails>().Add(newAdmitcardDetails);
                _DbContext.SaveChanges();
                return newAdmitcardDetails.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAdmitcardDetails(long id)
        {
            try
            {
                AdmitcardDetails ob = _DbContext.Set<AdmitcardDetails>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmitcardDetails.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAdmitcardDetails()
        {
            try
            {
                List<AdmitcardDetails> res = _DbContext.Set<AdmitcardDetails>().ToListAsync().Result;
                foreach (AdmitcardDetails ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AdmitcardDetails.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public AdmitcardDetails GetAdmitcardDetailsById(long id)
        {
            try
            {
                AdmitcardDetails ob = _DbContext.Set<AdmitcardDetails>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public AdmitcardDetails GetAdmitcardDetailsByName(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AdmitcardDetails> GetAllAdmitcardDetails()
        {
            try
            {
                IEnumerable<AdmitcardDetails> res = _DbContext.Set<AdmitcardDetails>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public bool UpdateAdmitcardDetails(AdmitcardDetails changedAdmitcardDetails)
        {
            try
            {
                changedAdmitcardDetails.ModifiedDate = DateTime.Now;
                changedAdmitcardDetails.Status = "UPDATED";
                _DbContext.AdmitcardDetails.Attach(changedAdmitcardDetails);
                var entry = _DbContext.Entry(changedAdmitcardDetails);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
