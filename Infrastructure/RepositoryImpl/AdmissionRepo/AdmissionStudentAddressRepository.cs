﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
   public class AdmissionStudentAddressRepository : EfRepository<AdmissionStudentAddress>, IAdmissionStudentAddressRepository
    {
        public AdmissionStudentAddressRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionStudentAddress> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionStudentAddresss.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStudentAddress>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionStudentAddresss.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStudentAddress>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionStudentAddresss.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
