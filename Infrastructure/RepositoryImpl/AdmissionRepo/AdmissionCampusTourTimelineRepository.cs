﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionCampusTourTimelineRepository : EfRepository<AdmissionCampusTourTimeline>, IAdmissionCampusTourTimelineRepository

    {
        public AdmissionCampusTourTimelineRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionCampusTourTimeline> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionCampusTourTimelines.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionCampusTourTimeline>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionCampusTourTimelines.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
