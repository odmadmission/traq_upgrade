﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
   public class CounsellorStatusRepository : EfRepository<CounsellorStatus>, ICounsellorStatusRepository
    {
        public CounsellorStatusRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<CounsellorStatus> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.CounsellorStatuss.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<CounsellorStatus>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.CounsellorStatuss.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<CounsellorStatus>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.CounsellorStatuss.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
