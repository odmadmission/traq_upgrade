﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionDocumentVerificationAssignRepository : EfRepository<AdmissionDocumentVerificationAssign>, IAdmissionDocumentVerificationAssignRepository
    {
        public AdmissionDocumentVerificationAssignRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionDocumentVerificationAssign> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionDocumentVerificationAssigns.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionDocumentVerificationAssign>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionDocumentVerificationAssigns.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionDocumentVerificationAssign>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionDocumentVerificationAssigns.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
