﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class StudentRepository : IStudentRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public StudentRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateStudent(Student newStudent)
        {
            try
            {
                newStudent.InsertedDate = DateTime.Now;
                newStudent.ModifiedDate = DateTime.Now;
                _DbContext.Set<Student>().Add(newStudent);
                _DbContext.SaveChanges();
                return newStudent.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllStudent()
        {
            try
            {
                List<Student> res = _DbContext.Set<Student>().ToListAsync().Result;
                foreach (Student ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Students.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteStudent(long id)
        {
            try
            {
                Student ob = _DbContext.Set<Student>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Students.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Student> GetAllStudent()
        {
            try
            {
                IEnumerable<Student> res = _DbContext.Set<Student>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Student GetStudentById(long id)
        {
            try
            {
                Student ob = _DbContext.Set<Student>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Student GetStudentByStudentCode(string studentcode)
        {
            try
            {
                Student ob = _DbContext.Set<Student>().FirstOrDefaultAsync(a => a.StudentCode == studentcode).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateStudent(Student changedStudent)
        {
            try
            {
                changedStudent.ModifiedDate = DateTime.Now;
                changedStudent.Status = "UPDATED";
                _DbContext.Students.Attach(changedStudent);
                var entry = _DbContext.Entry(changedStudent);
                entry.State = EntityState.Modified;
                if (changedStudent.Image == null)
                {
                    entry.Property(e => e.Image).IsModified = false;
                }
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
