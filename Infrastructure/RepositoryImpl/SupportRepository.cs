﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.SupportAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class SupportRepository : ISupportRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public SupportRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        #region Building
        public long CreateBuilding(Building obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<Building>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteBuilding(long id)
        {
            try
            {
                Building ob = _DbContext.Set<Building>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Buildings.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllBuilding()
        {
            try
            {
                List<Building> res = _DbContext.Set<Building>().ToListAsync().Result;
                foreach (Building ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Buildings.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Building> GetAllBuilding()
        {
            try
            {
                IEnumerable<Building> res = _DbContext.Set<Building>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Building GetByBuildingId(long id)
        {
            try
            {
                Building ob = _DbContext.Set<Building>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateBuilding(Building changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.Buildings.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region SupportAttachment
        public long CreateAttachment(SupportAttachment obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<SupportAttachment>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteAttachment(long id)
        {
            try
            {
                SupportAttachment ob = _DbContext.Set<SupportAttachment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.SupportAttachments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAttachment()
        {
            try
            {
                List<SupportAttachment> res = _DbContext.Set<SupportAttachment>().ToListAsync().Result;
                foreach (SupportAttachment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.SupportAttachments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SupportAttachment> GetAllAttachment()
        {
            try
            {
                IEnumerable<SupportAttachment> res = _DbContext.Set<SupportAttachment>().Where(a => a.Active == true ).ToListAsync().Result;           




                return res;
            }
            catch
            {
                return null;
            }
        }

        public SupportAttachment GetByAttachmentId(long id)
        {
            try
            {
                SupportAttachment ob = _DbContext.Set<SupportAttachment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateAttachment(SupportAttachment changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.SupportAttachments.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion


        #region SupportCategory
        public long CreateCategory(SupportCategory obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<SupportCategory>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteCategory(long id)
        {
            try
            {
                SupportCategory ob = _DbContext.Set<SupportCategory>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.SupportCategories.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllCategory()
        {
            try
            {
                List<SupportCategory> res = _DbContext.Set<SupportCategory>().ToListAsync().Result;
                foreach (SupportCategory ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.SupportCategories.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SupportCategory> GetAllCategory()
        {
            try
            {
                IEnumerable<SupportCategory> res = _DbContext.Set<SupportCategory>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public SupportCategory GetByCategoryId(long id)
        {
            try
            {
                SupportCategory ob = _DbContext.Set<SupportCategory>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateCategory(SupportCategory changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.SupportCategories.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region SupportPriority
        public long CreatePriority(SupportPriority obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<SupportPriority>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeletePriority(long id)
        {
            try
            {
                SupportPriority ob = _DbContext.Set<SupportPriority>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.SupportPriorities.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllPriority()
        {
            try
            {
                List<SupportPriority> res = _DbContext.Set<SupportPriority>().ToListAsync().Result;
                foreach (SupportPriority ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.SupportPriorities.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SupportPriority> GetAllPriority()
        {
            try
            {
                IEnumerable<SupportPriority> res = _DbContext.Set<SupportPriority>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public SupportPriority GetByPriorityId(long id)
        {
            try
            {
                SupportPriority ob = _DbContext.Set<SupportPriority>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdatePriority(SupportPriority changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.SupportPriorities.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Room
        public long CreateRoom(Room obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<Room>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteRoom(long id)
        {
            try
            {
                Room ob = _DbContext.Set<Room>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Rooms.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllRoom()
        {
            try
            {
                List<Room> res = _DbContext.Set<Room>().ToListAsync().Result;
                foreach (Room ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Rooms.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Room> GetAllRoom()
        {
            try
            {
                IEnumerable<Room> res = _DbContext.Set<Room>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Room GetByRoomId(long id)
        {
            try
            {
                Room ob = _DbContext.Set<Room>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateRoom(Room changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.Rooms.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region SupportStatus
        public long CreateStatus(SupportStatus obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<SupportStatus>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteStatus(long id)
        {
            try
            {
                SupportStatus ob = _DbContext.Set<SupportStatus>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.SupportStatuses.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllStatus()
        {
            try
            {
                List<SupportStatus> res = _DbContext.Set<SupportStatus>().ToListAsync().Result;
                foreach (SupportStatus ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.SupportStatuses.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SupportStatus> GetAllStatus()
        {
            try
            {
                IEnumerable<SupportStatus> res = _DbContext.Set<SupportStatus>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public SupportStatus GetByStatusId(long id)
        {
            try
            {
                SupportStatus ob = _DbContext.Set<SupportStatus>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateStatus(SupportStatus changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.SupportStatuses.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region SupportSubCategory
        public long CreateSubCategory(SupportSubCategory obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<SupportSubCategory>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteSubCategory(long id)
        {
            try
            {
                SupportSubCategory ob = _DbContext.Set<SupportSubCategory>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.SupportSubCategories.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllSubCategory()
        {
            try
            {
                List<SupportSubCategory> res = _DbContext.Set<SupportSubCategory>().ToListAsync().Result;
                foreach (SupportSubCategory ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.SupportSubCategories.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SupportSubCategory> GetAllSubCategory()
        {
            try
            {
                IEnumerable<SupportSubCategory> res = _DbContext.Set<SupportSubCategory>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public SupportSubCategory GetBySubCategoryId(long id)
        {
            try
            {
                SupportSubCategory ob = _DbContext.Set<SupportSubCategory>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateSubCategory(SupportSubCategory changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.SupportSubCategories.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion


        #region SupportRequest
        public long CreateSupportRequest(SupportRequest obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<SupportRequest>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }


        public bool DeleteSupportRequest(long id)
        {
            try
            {
                SupportRequest ob = _DbContext.Set<SupportRequest>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.SupportRequests.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllSupportRequest()
        {
            try
            {
                List<SupportRequest> res = _DbContext.Set<SupportRequest>().ToListAsync().Result;
                foreach (SupportRequest ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.SupportRequests.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SupportRequest> GetAllSupportRequest()
        {
            try
            {
                IEnumerable<SupportRequest> res = _DbContext.Set<SupportRequest>().Where(a => a.Active == true).AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<SupportRequest> GetAllSupportRequestWithinactive()
        {
            try
            {
                IEnumerable<SupportRequest> res = _DbContext.Set<SupportRequest>().AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public SupportRequest GetBySupportRequestId(long id)
        {
            try
            {
                SupportRequest ob = _DbContext.Set<SupportRequest>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        //public SupportRequest GetBySupportRequestDepartmentId(List<long> id)
        //{
        //    try
        //    {
        //        SupportRequest ob = _DbContext.Set<SupportRequest>().FirstOrDefaultAsync(a => a.ID == id).Result;
        //        return ob;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        public bool UpdateSupportRequest(SupportRequest changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.SupportRequests.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion


        #region RoomCategory
        public long CreateRoomCategory(RoomCategory obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<RoomCategory>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteRoomCategory(long id)
        {
            try
            {
                RoomCategory ob = _DbContext.Set<RoomCategory>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.RoomCategories.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllRoomCategory()
        {
            try
            {
                List<RoomCategory> res = _DbContext.Set<RoomCategory>().ToListAsync().Result;
                foreach (RoomCategory ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.RoomCategories.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<RoomCategory> GetAllRoomCategory()
        {
            try
            {
                IEnumerable<RoomCategory> res = _DbContext.Set<RoomCategory>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public RoomCategory GetByRoomCategoryId(long id)
        {
            try
            {
                RoomCategory ob = _DbContext.Set<RoomCategory>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateRoomCategory(RoomCategory changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.RoomCategories.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion


        #region Location
        public long CreateLocation(Location obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<Location>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteLocation(long id)
        {
            try
            {
                Location ob = _DbContext.Set<Location>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Locations.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllLocation()
        {
            try
            {
                List<Location> res = _DbContext.Set<Location>().ToListAsync().Result;
                foreach (Location ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Locations.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Location> GetAllLocation()
        {
            try
            {
                IEnumerable<Location> res = _DbContext.Set<Location>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Location GetByLocationId(long id)
        {
            try
            {
                Location ob = _DbContext.Set<Location>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateLocation(Location changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.Locations.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Floor
        public long CreateFloor(Floor obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<Floor>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteFloor(long id)
        {
            try
            {
                Floor ob = _DbContext.Set<Floor>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Floors.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllFloor()
        {
            try
            {
                List<Floor> res = _DbContext.Set<Floor>().ToListAsync().Result;
                foreach (Floor ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Floors.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Floor> GetAllFloor()
        {
            try
            {
                IEnumerable<Floor> res = _DbContext.Set<Floor>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Floor GetByFloorId(long id)
        {
            try
            {
                Floor ob = _DbContext.Set<Floor>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateFloor(Floor changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.Floors.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region SupportType
        public long CreateSupportType(SupportType obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<SupportType>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteSupportType(long id)
        {
            try
            {
                SupportType ob = _DbContext.Set<SupportType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.SupportTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllSupportType()
        {
            try
            {
                List<SupportType> res = _DbContext.Set<SupportType>().ToListAsync().Result;
                foreach (SupportType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.SupportTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SupportType> GetAllSupportType()
        {
            try
            {
                IEnumerable<SupportType> res = _DbContext.Set<SupportType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public SupportType GetBySupportTypeId(long id)
        {
            try
            {
                SupportType ob = _DbContext.Set<SupportType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
      


        public bool UpdateSupportType(SupportType changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.SupportTypes.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region DepartmentSupport
        public long CreateDepartmentSupport(DepartmentSupport obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<DepartmentSupport>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteDepartmentSupport(long id)
        {
            try
            {
                DepartmentSupport ob = _DbContext.Set<DepartmentSupport>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.DepartmentSupports.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllDepartmentSupport()
        {
            try
            {
                List<DepartmentSupport> res = _DbContext.Set<DepartmentSupport>().ToListAsync().Result;
                foreach (DepartmentSupport ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.DepartmentSupports.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<DepartmentSupport> GetAllDepartmentSupport()
        {
            try
            {
                IEnumerable<DepartmentSupport> res = _DbContext.Set<DepartmentSupport>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public DepartmentSupport GetByDepartmentSupportId(long id)
        {
            try
            {
                DepartmentSupport ob = _DbContext.Set<DepartmentSupport>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateDepartmentSupport(DepartmentSupport changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.DepartmentSupports.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<SupportRequest> GetBySupportRequestSupportTypeId(List<Int64> id)
        {
            try
            {
                IEnumerable<SupportRequest> ob = _DbContext.
                    Set<SupportRequest>().Where(a =>id.Contains(a.SupportTypeID)&& a.Active == true).ToListAsync().Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<DepartmentSupport> GetSupportByDepartmentId(long id)
        {
            try
            {
                IEnumerable<DepartmentSupport> ob = _DbContext.
                    Set<DepartmentSupport>().Where(a => a.DepartmentID == id&&a.Active==true).ToListAsync().Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region SupportTimeLine
        public long CreateSupportTimeLine(SupportTimeLine obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<SupportTimeLine>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteSupportTimeLine(long id)
        {
            try
            {
                SupportTimeLine ob = _DbContext.Set<SupportTimeLine>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.SupportTimeLines.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteAllSupportTimeLine()
        {
            try
            {
                List<SupportTimeLine> res = _DbContext.Set<SupportTimeLine>().ToListAsync().Result;
                foreach (SupportTimeLine ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.SupportTimeLines.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SupportTimeLine> GetAllSupportTimeLine()
        {
            try
            {
                IEnumerable<SupportTimeLine> res = _DbContext.Set<SupportTimeLine>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public SupportTimeLine GetBySupportTimeLineId(long id)
        {
            try
            {
                SupportTimeLine ob = _DbContext.Set<SupportTimeLine>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateSupportTimeLine(SupportTimeLine changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.SupportTimeLines.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<SupportRequest> GetBySupportRequestByNotSupportTypeId(List<long> id)
        {
            try
            {
                IEnumerable<SupportRequest> ob = _DbContext.
                    Set<SupportRequest>().Where(a => !id.Contains(a.SupportTypeID) && a.Active == true).ToListAsync().Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }


        #endregion


        #region SupportComment
        public long CreateSupportComment(SupportComment obj)
        {
            try
            {
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                _DbContext.Set<SupportComment>().Add(obj);
                _DbContext.SaveChanges();
                return obj.ID;
            }
            catch
            {
                return 0;
            }
        }


        public bool DeleteSupportComment(long id)
        {
            try
            {
                SupportComment ob = _DbContext.Set<SupportComment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.SupportComments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteAllSupportComment()
        {
            try
            {
                List<SupportComment> res = _DbContext.Set<SupportComment>().ToListAsync().Result;
                foreach (SupportComment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.SupportComments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SupportComment> GetAllSupportComment()
        {
            try
            {
                IEnumerable<SupportComment> res = _DbContext.Set<SupportComment>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public SupportComment GetBySupportCommentId(long id)
        {
            try
            {
                SupportComment ob = _DbContext.Set<SupportComment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateSupportComment(SupportComment changed)
        {
            try
            {
                changed.ModifiedDate = DateTime.Now;
                changed.Status = EntityStatus.ACTIVE;
                _DbContext.SupportComments.Attach(changed);
                var entry = _DbContext.Entry(changed);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int GetUnreadSupportCommentCountBySupportID(long supportRequestId)
        {
            try
            {
                int count = _DbContext.Set<SupportComment>().Where(a => a.Active == true
                &&a.SupportRequestID== supportRequestId&&a.IsRead==false).CountAsync().Result;
                return count;
            }
            catch
            {
                return 0;
            }
        }

        public IEnumerable<SupportAttachment> GetAttachmentsBySupportIdAndType(long supportId, string type)
        {
            try
            {
                IEnumerable<SupportAttachment> res = _DbContext.Set<SupportAttachment>().Where(a => a.Active == true
                && a.SupportRequestID == supportId).AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<SupportTimeLine> GetTimelineBySupportId(long supportId)
        {
            try
            {
                IEnumerable<SupportTimeLine> res = _DbContext.Set<SupportTimeLine>().Where(a => a.Active == true
                &&a.SupportRequestID== supportId).AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public SupportComment GetLastCommentBySupportId(long supportId)
        {
            try
            {
               SupportComment res = _DbContext.Set<SupportComment>().Where(a => a.Active == true &&
                a.SupportRequestID == supportId).AsNoTracking().LastOrDefaultAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public int GetUnreadSupportCommentCountBySupportIdAndInsertedId(long id, long empId)
        {
            try
            {
                int res = _DbContext.Set<SupportComment>().Where(a => a.Active == true
                && a.SupportRequestID == id&&a.InsertedId!= empId&&a.IsRead==false).Count();
                return res;
            }
            catch
            {
                return 0;
            }
        }

        public SupportRequest GetBySupportRequestIdNoTracking(long id)
        {
            try
            {
                SupportRequest ob = _DbContext.Set<SupportRequest>().AsNoTracking().FirstOrDefault(a => a.ID == id);
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public SupportComment GetByLastTimelineStatus(long supportId, string status)
        {

            try
            {
                SupportComment ob = _DbContext.Set<SupportComment>().AsNoTracking(). 
                    LastOrDefaultAsync(a => a.ID == supportId&&a.Status==status).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<SupportRequest> GetAllBySupportTypeId(List<Int64> supportTypeId)
        {
            try
            {

                if(supportTypeId!=null&& supportTypeId.Count()>0)
                {

                    IEnumerable<SupportRequest> res = _DbContext.Set<SupportRequest>().Where(a => a.Active == true
                    && supportTypeId.Contains(a.SupportTypeID)).AsNoTracking().ToListAsync().Result;
                    return res;
                }
                else
                {

                    IEnumerable<SupportRequest> res = _DbContext.Set<SupportRequest>().Where(a => a.Active == true
                   ).AsNoTracking().ToListAsync().Result;
                    return res;
                }

            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<long> GetSupportTypeIdsByDepartmentIdIn(List<long> id)
        {
            try
            {
                IEnumerable<long> res = _DbContext.Set<DepartmentSupport>().Where(a => a.Active == true
                && id.Contains(a.DepartmentID)).AsNoTracking().Select(a=>a.SupportTypeID).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<SupportType> GetAllSupportTypeById(List<long> ids)
        {
            try
            {
                IEnumerable<SupportType> res = _DbContext.Set<SupportType>().Where(a => a.Active == true
                && ids.Contains(a.ID)).AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<SupportRequest> GetAllSupportRequestByUserId(long userId)
        {
            try
            {
                IEnumerable<SupportRequest> res = _DbContext.Set<SupportRequest>().Where(a => a.Active == true
                && a.InsertedId== userId).AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<SupportRequest> GetApprovalSupportRequestBySupportTypeId(List<long> id)
        {
            try
            {
                if(id!=null&&id.Count()>0)
                 {
                    IEnumerable<SupportRequest> res = _DbContext.Set<SupportRequest>().Where(a => a.Active == true && a.SendApproval == true
               && id.Contains(a.SupportTypeID)).AsNoTracking().ToListAsync().Result;
                    return res;
                }
                else
                {
                    IEnumerable<SupportRequest> res = _DbContext.Set<SupportRequest>().Where(a => a.Active == true && a.SendApproval == true
       ).AsNoTracking().ToListAsync().Result;
                    return res;
                }
               
                
            }
            catch
            {
                return null;
            }
        }

        //public IEnumerable<SupportRequest> GetBySupportRequestByNotSupportTypeId(List<long> id)
        //{
        //    try
        //    {
        //        IEnumerable<SupportRequest> ob = _DbContext.
        //            Set<SupportRequest>().Where(a => !id.Contains(a.SupportTypeID) && a.Active == true).ToListAsync().Result;
        //        return ob;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}


        #endregion

    }
}
