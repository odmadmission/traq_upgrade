﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.DocumentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class DocumentTypeRepository : IDocumentTypeRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public DocumentTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateDocumentType(DocumentType newDocumentType)
        {
            try
            {
                newDocumentType.InsertedDate = DateTime.Now;
                newDocumentType.ModifiedDate = DateTime.Now;
                _DbContext.Set<DocumentType>().Add(newDocumentType);
                _DbContext.SaveChanges();
                return newDocumentType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllDocumentTypes()
        {
            try
            {
                List<DocumentType> res = _DbContext.Set<DocumentType>().ToListAsync().Result;
                foreach (DocumentType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.DocumentTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDocumentType(long id)
        {
            try
            {
                DocumentType ob = _DbContext.Set<DocumentType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.DocumentTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<DocumentType> GetAllDocumentTypes()
        {
            try
            {
                IEnumerable<DocumentType> res = _DbContext.Set<DocumentType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public DocumentType GetDocumentTypeById(long id)
        {
            try
            {
                DocumentType ob = _DbContext.Set<DocumentType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public DocumentType GetDocumentTypeByName(string name)
        {
            try
            {
                DocumentType ob = _DbContext.Set<DocumentType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateDocumentType(DocumentType changedDocumentType)
        {
            try
            {
                changedDocumentType.ModifiedDate = DateTime.Now;
                changedDocumentType.Status = "UPDATED";
                _DbContext.DocumentTypes.Attach(changedDocumentType);
                var entry = _DbContext.Entry(changedDocumentType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
