﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ModuleRepository : IModuleRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public ModuleRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateModule(Module newModule)
        {
            try
            {
                newModule.InsertedDate = DateTime.Now;
                newModule.ModifiedDate = DateTime.Now;
                _DbContext.Set<Module>().Add(newModule);
                _DbContext.SaveChanges();
                return newModule.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllModule()
        {
            try
            {
                List<Module> res = _DbContext.Set<Module>().ToListAsync().Result;
                foreach (Module ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Modules.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteModule(long id)
        {
            try
            {
                Module ob = _DbContext.Set<Module>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Modules.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Module> GetAllModule()
        {
            try
            {
                IEnumerable<Module> res = _DbContext.Set<Module>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Module GetModuleById(long id)
        {
            try
            {
                Module ob = _DbContext.Set<Module>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Module GetModuleByName(string name)
        {
            try
            {
                Module ob = _DbContext.Set<Module>().FirstOrDefaultAsync(a => a.Name == name && a.Active == true).Result;
                return ob;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public bool UpdateModule(Module changedModule)
        {
            try
            {
                changedModule.ModifiedDate = DateTime.Now;
                changedModule.Status = "UPDATED";
                _DbContext.Modules.Attach(changedModule);
                var entry = _DbContext.Entry(changedModule);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
