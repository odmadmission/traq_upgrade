﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class NationalityTypeRepository :INationalityTypeRepository
    {

        private readonly ApplicationDbContext _DbContext;

        public NationalityTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateNationality(NationalityType newNationality)
        {
            try
            {
                newNationality.InsertedDate = DateTime.Now;
                newNationality.ModifiedDate = DateTime.Now;
                _DbContext.Set<NationalityType>().Add(newNationality);
                _DbContext.SaveChanges();
                return newNationality.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllNationalities()
        {
            try
            {
                List<NationalityType> res = _DbContext.Set<NationalityType>().ToListAsync().Result;
                foreach (NationalityType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.NationalityTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteNationality(long id)
        {
            try
            {
                NationalityType ob = _DbContext.Set<NationalityType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.NationalityTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<NationalityType> GetAllNationalities()
        {
            try
            {
                IEnumerable<NationalityType> res = _DbContext.Set<NationalityType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public NationalityType GetNationalityById(long id)
        {
            try
            {
                NationalityType ob = _DbContext.Set<NationalityType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public NationalityType GetNationalityByName(string name)
        {
            try
            {
                NationalityType ob = _DbContext.Set<NationalityType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateNationality(NationalityType changedCountry)
        {
            try
            {
                changedCountry.ModifiedDate = DateTime.Now;
                changedCountry.Status = "UPDATED";
                _DbContext.NationalityTypes.Attach(changedCountry);
                var entry = _DbContext.Entry(changedCountry);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

