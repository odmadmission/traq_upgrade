﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;

using OdmErp.ApplicationCore.Entities.DocumentAggregate;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Entities.ModuleAggregate;
using OdmErp.ApplicationCore.Entities.GrievanceAggregate;
using OdmErp.ApplicationCore.Entities.TodoAggregate;
using OdmErp.ApplicationCore.Entities.SoftwareAggregate;
using OdmErp.ApplicationCore.Entities.SupportAggregate;

using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Entities.SessionFeesType;
using OdmErp.ApplicationCore.Query;
using OdmErp.ApplicationCore.Entities.StudentPaymentAggregate;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;

namespace OdmErp.Infrastructure.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options) { }
 

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            // ...
           // optionsBuilder.Entity<Admission>().Property(x => x.ID).ValueGeneratedOnAdd();

        }

        public DbSet<FeesType> FeesTypes { get; set; }
        public DbSet<FeesTypeApplicable> FeesTypeApplicables { get; set; }
        public DbSet<ScholarshipApplicable> ScholarshipApplicables { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Access> Accesses { get; set; }
        public DbSet<ApplicationCore.Entities.Action> Actions { get; set; }
        public DbSet<ActionAccess> ActionAccesses { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<AddressType> AddressTypes { get; set; }
        public DbSet<BankType> BankTypes { get; set; }
        public DbSet<Admission> Admissions { get; set; }
        public DbSet<AdmissionType> AdmissionTypes { get; set; }
        public DbSet<BloodGroup> BloodGroups { get; set; }
        public DbSet<Board> Boards { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Designation> Designations { get; set; }
        public DbSet<DesignationLevel> DesignationLevels { get; set; }
        public DbSet<EducationQualification> EducationQualifications { get; set; }
        public DbSet<EducationQualificationType> EducationQualificationTypes { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeTimeline> EmployeeTimelines { get; set; }
        public DbSet<EmployeeExperience> EmployeeExperiences { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<OrganizationType> OrganizationTypes { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Standard> Standards { get; set; }
        public DbSet<State> States { get; set; }
        //public DbSet<Stream> Streams { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<SubModule> SubModules { get; set; }

        public DbSet<GrievanceForward> GrievanceForwards { get; set; }
        public DbSet<DocumentSubType> DocumentSubTypes { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }

        public DbSet<NationalityType> NationalityTypes { get; set; }

        public DbSet<ReligionType> ReligionTypes { get; set; }
        public DbSet<GrievanceInhouseComment> GrievanceInhouseComment { get; set; }
        public DbSet<EmployeeAddress> EmployeeAddresses { get; set; }

        public DbSet<EmployeeEducation> EmployeeEducations { get; set; }

        public DbSet<EmployeeDesignation> EmployeeDesignations { get; set; }
        public DbSet<AdmissionStandardExamDate> AdmissionStandardExamDate { get; set; }
        public DbSet<EmployeeRequiredDocument> EmployeeRequiredDocuments { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<BankBranch> BankBranches { get; set; }
        public DbSet<EmployeeBankAccount> EmployeeBankAccounts { get; set; }
        public DbSet<GrievanceExtendedTimeline> GrievanceExtendedTimelines { get; set; }
        public DbSet<GrievanceExtendedTimelineAttachment> GrievanceExtendedTimelineAttachments { get; set; }

        public DbSet<AccessType> AccessTypes { get; set; }

        public DbSet<GrievanceAccess> GrievanceAccess { get; set; }
        public DbSet<GrievanceAccessTimeline> GrievanceAccessTimelines { get; set; }
        public DbSet<GrievanceInhouseAttachment> GrievanceInhouseAttachment { get; set; }
        
        public DbSet<Authority> Authorities { get; set; }

        public DbSet<AuthorityType> AuthorityTypes { get; set; }


        public DbSet<DepartmentAuthority> DepartmentAuthorities { get; set; }

        public DbSet<DepartmentType> DepartmentTypes { get; set; }

        public DbSet<SupportRequestAssign> SupportRequestAssign { get; set; }

        public DbSet<FilePathUrl> FilePathUrls { get; set; }


        public DbSet<ParentRelationshipType> ParentRelationshipTypes { get; set; }
        public DbSet<ProfessionType> ProfessionTypes { get; set; }
        public DbSet<StudentAddress> StudentAddresses { get; set; }

        public DbSet<ActionDepartment> ActionDepartments { get; set; }

    
        public DbSet<EmployeeActionDepartment> EmployeeActionDepartments { get; set; }


        public DbSet<Grievance> Grievances { get; set; }
        public DbSet<GrievanceType> GrievanceTypes { get; set; }

        public DbSet<GrievanceStatus> GrievanceStatus { get; set; }
        public DbSet<EmployeeGrievance> EmployeeGrievances { get; set; }

        public DbSet<GrievanceAttachment> GrievanceAttachment { get; set; }
        public DbSet<GrievanceCategory> GrievanceCategory { get; set; }

        public DbSet<GrievanceComment> GrievanceComments { get; set; }
        public DbSet<GrievanceCommentAttachment> GrievanceCommentAttachments { get; set; }

        public DbSet<GrievancePriority> GrievancePriorities { get; set; }
        public DbSet<GrievanceTimeline> GrievanceTimelines { get; set; }


        #region Todo
        public DbSet<Todo> Todos { get; set; }
        public DbSet<TodoAttachment> TodoAttachments { get; set; }

        public DbSet<TodoComment> TodoComments { get; set; }
        public DbSet<TodoCommentAttachment> TodoCommentAttachments{ get; set; }


        public DbSet<TodoPriority> TodoPriorities { get; set; }
        public DbSet<TodoStatus> TodoStatus{ get; set; }
        public DbSet<TodoStatusDetails> TodoStatusDetails { get; set; }
        public DbSet<TodoTimeline> TodoTimelines { get; set; }
        #endregion

        public DbSet<Wing> Wing { get; set; }
        //public DbSet<SchoolWing> SchoolWing { get; set; }
        public DbSet<StudentClass> StudentClass { get; set; }
        public DbSet<ActionRole> ActionRoles { get; set; }

        public DbSet<SupportRequest> SupportRequests { get; set; }
        public DbSet<SupportAttachment> SupportAttachments { get; set; }
        public DbSet<SupportStatus> SupportStatuses { get; set; }
        public DbSet<SupportPriority> SupportPriorities { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<SupportCategory> SupportCategories { get; set; }
        public DbSet<SupportSubCategory> SupportSubCategories { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomCategory> RoomCategories { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Floor> Floors { get; set; } 
        public DbSet<DepartmentLead> DepartmentLeads { get; set; }
        public DbSet<SupportType> SupportTypes { get; set; }
        public DbSet<DepartmentSupport> DepartmentSupports { get; set; }
        public DbSet<SupportTimeLine> SupportTimeLines { get; set; }
        public DbSet<SupportComment> SupportComments { get; set; }
        public DbSet<ActionStudent> actionStudents { get; set; }
        public DbSet<ErpBug> ErpBugs { get; set; }
        public DbSet<ErpBugAttachment> ErpBugsAttachments { get; set; }
        public DbSet<OdmErpModel> OdmErpModels { get; set; }        
        public DbSet<EmployeeDocumentAttachment> employeeDocumentAttachments { get; set; }

        public DbSet<PaymentGateway> paymentGateway { get; set; }
        public DbSet<PaymentGatewayBTB> paymentGatewayBTB { get; set; }
        public DbSet<Login> Logins { get; set; }

        // School Belq

        public DbSet<AcademicChapter> AcademicChapters { get; set; }
        public DbSet<AcademicConcept> AcademicConcepts { get; set; }
        public DbSet<AcademicHomework> AcademicHomeworks { get; set; }
        public DbSet<AcademicHomeworkAttachment> AcademicHomeworkAttachments { get; set; }
        public DbSet<AcademicSection> AcademicSections { get; set; }
        public DbSet<AcademicSectionStudent> AcademicSectionStudents { get; set; }
        public DbSet<AcademicSession> AcademicSessions { get; set; }
        public DbSet<AcademicStandard> AcademicStandards { get; set; }
        public DbSet<AcademicStudent> AcademicStudents { get; set; }
        public DbSet<AcademicSubject> AcademicSubjects { get; set; }
        public DbSet<AcademicSyllabus> AcademicSyllabus { get; set; }
        public DbSet<AcademicTeacher> AcademicTeachers { get; set; }
        public DbSet<AcademicTimeTable> AcademicTimeTables { get; set; }
        public DbSet<BoardStandard> BoardStandards { get; set; }
        public DbSet<Chapter> Chapters { get; set; }
        public DbSet<Concept> Concepts { get; set; }
        public DbSet<OrganizationAcademic> OrganizationAcademics { get; set; }
        public DbSet<PeriodEmployee> PeriodEmployees { get; set; }
        public DbSet<PeriodEmployeeStatus> PeriodEmployeeStatuses { get; set; }
        public DbSet<PeriodSyllabus> PeriodSyllabus { get; set; }
        public DbSet<PeriodSyllabusFeedback> PeriodSyllabusFeedbacks { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Syllabus> Syllabus { get; set; }
        public DbSet<TimeTablePeriod> TimeTablePeriods { get; set; }

        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<OtpMessage> OtpMessages { get; set; }
        //public DbSet<BoardStandardStream> BoardStandardStreams { get; set; }
        //public DbSet<AcademicStandardStream> AcademicStandardStreams { get; set; }


        public DbSet<AcademicCalender> AcademicCalenders { get; set; }
        public DbSet<CalenderEngagement> CalenderEngagements { get; set; }
        public DbSet<CalenderEngagementType> CalenderEngagementTypes { get; set; }
        public DbSet<AcademicTeacherSubject> AcademicTeacherSubjects { get; set; }
        public DbSet<AcademicTeacherSubjectsTimeLine> AcademicTeacherSubjectsTimeLines { get; set; }
        public DbSet<ClassTiming> ClassTimings { get; set; }
        public DbSet<ActionAcademic> actionAcademics { get; set; }

        public DbQuery<View_All_Academic_Student> view_All_Academic_Students { get; set; }
        public DbQuery<View_All_TimeTable_For_Calender> view_All_TimeTable_For_Calenders { get; set; }
        public DbQuery<View_All_Academic_Standard> view_All_Academic_Standards { get; set; }
        public DbQuery<View_All_Academic_Student_Section> view_All_Academic_Student_Sections { get; set; }
        public DbQuery<View_All_TimeTable_Subjects> view_All_TimeTable_Subjects { get; set; }
        public DbQuery<View_All_Academic_Section> view_All_Academic_Sections { get; set; }
        public DbQuery<View_All_Academic_Subject> view_All_Academic_Subjects { get; set; }
        public DbQuery<View_All_Teachers> view_All_Teachers { get; set; }
        public DbQuery<View_Timetable> view_Timetables { get; set; }
        public DbQuery<View_StudentPaymentData> View_StudentPaymentData { get; set; }
        public DbQuery<getMandatoryPrice> getMandatoryPrice { get; set; }
        public DbQuery<getfeePrice> getfeePrice { get; set; }
        public DbQuery<getStudentPaymentData> getStudentPaymentData { get; set; }
        public DbSet<StudentAcademic> StudentAcademics { get; set; }
        public DbSet<StudentRequiredDocument> StudentRequiredDocuments { get; set; }

        public DbSet<StudentDocumentAttachment> StudentDocumentAttachments { get; set; }
        public DbSet<TeacherTimeLine> TeacherTimeLines { get; set; }

        public DbSet<AcademicTeacherTimeLine> AcademicTeacherTimeLines { get; set; }
  
 

        public DbSet<Scholarship> Scholarships { get; set; }
        public DbSet<SessionScholarship> SessionScholarships { get; set; }
        public DbSet<SessionScholarshipDiscount> SessionScholarshipDiscounts { get; set; }
        public DbSet<SessionScholarshipDiscountPrice> SessionScholarshipDiscountPrices { get; set; }
        public DbSet<SessionScholarshipDiscountApplicable> SessionScholarshipDiscountApplicables { get; set; }
        public DbSet<ScholarshipApproval> ScholarshipApprovals { get; set; }


        public DbSet<PaymentSnapshot> PaymentSnapshots { get; set; }
        public DbSet<DataTimeline> DataTimelines { get; set; }



        public DbSet<AdmissionLogin> AdmissionLogins { get; set; }

        public DbSet<MessagesContent> MessagesContents { get; set; }
        public DbSet<NoticeData> NoticeData { get; set; }
        public DbSet<Notice> Notices { get; set; }
        public DbSet<UserGroupEmployee> UserGroupEmployees { get; set; }
        public DbSet<UserGroupEmployeeTimeLine> UserGroupEmployeeTimeLine { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<UserGroupTimeLine> UserGroupTimeLines { get; set; }
        public DbSet<PaymentType> PaymentType { get; set; }

        public DbSet<AdmissionDocumentAssign> AdmissionDocumentAssign { get; set; }

        public DbSet<AdmissionCounsellor> AdmissionCounsellor { get; set; }

        #region Student
        public DbSet<Language> languages { get; set; }
        public DbSet<AdmissionStudentKit> AdmissionStudentKits { get; set; }
        public DbQuery<View_Admission_Progresssheet> progresssheets { get; set; }
        public DbSet<AcademicStandardSettings> AcademicStandardSettings { get; set; }
        public DbSet<AcademicStandardWing> academicStandardWings { get; set; }
        public DbSet<StudentLanguagesKnown> studentLanguagesKnowns { get; set; }
        public DbSet<SubjectGroup> SubjectGroups { get; set; }
        public DbSet<SubjectGroupDetails> SubjectGroupDetailss { get; set; }
        public DbSet<SubjectOptionalType> SubjectOptionalTypes { get; set; }
        public DbSet<SubjectOptionalCategory> SubjectOptionalCategories { get; set; }
        public DbSet<SubjectOptional> SubjectOptionals { get; set; }
        public DbSet<VehicleType> VehicleTypes { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<StudentEmergencyContact> StudentEmergencyContacts { get; set; }
        public DbSet<DepartureTime> DepartureTimes { get; set; }
        public DbSet<VehicleLocation> VehicleLocations { get; set; }
        public DbSet<StudentTransportation> StudentTransportations { get; set; }
        public DbSet<BoardStandardWing> BoardStandardWings { get; set; }
        public DbSet<AcademicSubjectGroup> AcademicSubjectGroups { get; set; }
        public DbSet<AcademicSubjectOptionalDetails> AcademicSubjectOptionalDetailss { get; set; }
        public DbSet<AcademicSubjectOptional> AcademicSubjectOptionals { get; set; }
        public DbSet<AcademicStudentSubjectOptional> academicStudentSubjectOptionals { get; set; }
        public DbQuery<View_All_Subject_Group> view_All_Subject_Groups { get; set; }
        public DbQuery<View_Common_List> view_Common_Lists { get; set; }
        public DbQuery<View_All_Subject_Optional_Category> view_All_Subject_Optional_Categories { get; set; }
        public DbQuery<View_Academic_Student> View_Academic_Students { get; set; }
        public DbQuery<View_All_Teachers_Subjects> view_All_Teachers_Subjects { get; set; }
        public DbQuery<View_Student_Admission_Counseller> view_Student_Admission_Counsellers { get; set; }
        public DbQuery<View_Previous_Academic> view_Previous_Academics { get; set; }
        #endregion
        #region----------------------Addmission-------------------------------
       
        public DbSet<ContactType> ContactTypes { get; set; }
        public DbQuery<View_All_Admission> View_All_Admissions { get; set; }
        public DbSet<AdmissionSlot> AdmissionSlots { get; set; }        
        public DbSet<AdmissionSource> AdmissionSources { get; set; }
        public DbSet<AdmissionStandardSeatQuota> AdmissionStandardSeatQuotas { get; set; }
        public DbSet<AdmissionStandardSetting> AdmissionStandardSetting { get; set; }
        public DbSet<AdmissionStudent> AdmissionStudents { get; set; }
        public DbSet<AdmissionStudentAcademic> AdmissionStudentAcademics { get; set; }
        public DbSet<AdmissionStudentAddress> AdmissionStudentAddresss { get; set; }
        public DbSet<AdmissionStudentContact> AdmissionStudentContacts { get; set; }
        public DbSet<AdmissionStudentCounselor> AdmissionStudentCounselors { get; set; }
        public DbSet<AdmissionStudentDeclaration> AdmissionStudentDeclarations { get; set; }
        public DbSet<AdmissionStudentDocument> AdmissionStudentDocuments { get; set; }
        public DbSet<AdmissionStudentExam> AdmissionStudentExams { get; set; }
        public DbSet<AdmissionStudentInterview> AdmissionStudentInterviews { get; set; }
        public DbSet<AdmissionStudentLanguage> AdmissionStudentLanguages { get; set; }
        public DbSet<AdmissionStudentParent> AdmissionStudentParents { get; set; }
        public DbSet<AdmissionStudentProficiency> AdmissionStudentProficiencys { get; set; }
        public DbSet<AdmissionStudentReference> AdmissionStudentReferences { get; set; }
        public DbSet<AdmissionStudentStandard> AdmissionStudentStandards { get; set; }
        public DbSet<AdmissionStudentTransport> AdmissionStudentTransports { get; set; }
        public DbSet<AdmissionTimeline> AdmissionTimelines { get; set; }
        public DbSet<AdmissionFormPayment> AdmissionFormPayments { get; set; }
        public DbSet<AdmissionStandardExam> AdmissionStandardExams { get; set; }
      
        public DbSet<AdmissionDocument> AdmissionDocuments { get; set; }
        public DbSet<AdmissionDocumentVerificationAssign> AdmissionDocumentVerificationAssigns { get; set; }
        public DbSet<AdmissionExamDateAssign> AdmissionExamDateAssigns { get; set; }
        public DbSet<AdmissionPersonalInterviewAssign> AdmissionPersonalInterviewAssigns { get; set; }
        public DbSet<MapClassToAdmission> MapClassToAdmissions { get; set; }
        public DbSet<AdmissionStandardExamResultDate> AdmissionStandardExamResultDates { get; set; }
        public DbSet<AdmissionStandardExamQuestion> admissionStandardExamQuestions { get; set; }
        public DbSet<CounsellorStatus> CounsellorStatuss { get; set; }
        public DbSet<AdmissionVerifier> AdmissionVerifier { get; set; }
        public DbSet<AdmissionInterViewer> AdmissionInterViewer { get; set; }
        public DbSet<AdmissionFee> AdmissionFees { get; set; }
        public DbSet<AdmitcardVenue> AdmitcardVenues { get; set; }
        public DbSet<AdmitcardVenueAddress> AdmitcardVenueAddress { get; set; }
        public DbSet<AdmitcardInstructions> AdmicardInstructions { get; set; }
        public DbSet<AdmitcardDetails> AdmitcardDetails { get; set; }
        public DbSet<MasterEmail> MasterEmail { get; set; }
        public DbSet<MasterSMS> MasterSMS { get; set; }
        #endregion

        #region--------------Admission Campus Tour And Ecounselling-------------------
        public DbSet<AdmissionEcounselling> AdmissionEcounsellings { get; set; }
        public DbSet<AdmissionEcounsellingDate> AdmissionEcounsellingDates { get; set; }
        public DbSet<AdmissionEcounsellingSlot> AdmissionEcounsellingSlots { get; set; }
        public DbSet<AdmissionEcounsellingTimeline> AdmissionEcounsellingTimelines { get; set; }
        public DbSet<AdmissionCampusTour> AdmissionCampusTours { get; set; }
        public DbSet<AdmissionCampusTourDate> AdmissionCampusTourDates { get; set; }
        public DbSet<AdmissionCampusTourSlot> AdmissionCampusTourSlots { get; set; }
        public DbSet<AdmissionCampusTourTimeline> AdmissionCampusTourTimelines { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder builder)
        {
            
            builder.Entity<Board>()
                .HasIndex(u => u.Name)
                .IsUnique();


        }

        ///Student Payment Module//
        ///
        /// Created by Niranjan
        ///

     //  public DbSet<SessionFeesTypeView> SessionFeesTypeView { get; set; }
        public DbSet<SessionFeesType> SessionFeesType { get; set; }

        public DbSet<FeesTypePriceData> FeesTypePriceData { get; set; }
        public DbSet<SessionFeesTypeApplicableData> SessionFeesTypeApplicableData { get; set; }

        public DbSet<PaymentCollectionType> PaymentCollectionType { get; set; }
        public DbSet<PaymentCollectionTypeData> PaymentCollectionTypeData { get; set; }

        public DbSet<StudentPayment> StudentPayment { get; set; }


       public DbSet<ScholarshipApply> ScholarshipApplys { get; set; }

        //protected override void OnModelCreating(ModelBuilder builder)
        //{
        //    builder.Entity<Group>()
        //        .HasIndex(u => u.Name)
        //        .IsUnique();

        //    builder.Entity<Role>()
        //     .HasIndex(u => u.Name)
        //     .IsUnique();

        //    builder.Entity<OrganizationType>()
        //   .HasIndex(u => u.Name)
        //   .IsUnique();

        //    builder.Entity<Organization>()
        //        .HasIndex(p => new { p.Name, p.GroupID })
        //        .IsUnique();


        //    builder.Entity<BloodGroup>()
        //      .HasIndex(u => u.Name)
        //      .IsUnique();

        //    builder.Entity<Country>()
        //     .HasIndex(u => u.Name)
        //     .IsUnique();

        //    builder.Entity<NationalityType>()
        //   .HasIndex(u => u.Name)
        //   .IsUnique();

        //    builder.Entity<ReligionType>()
        //   .HasIndex(u => u.Name)
        //   .IsUnique();

        //    builder.Entity<RoomCategory>()
        //   .HasIndex(u => u.Name)
        //   .IsUnique();

        //    builder.Entity<Floor>()
        //    .HasIndex(u => u.Name)
        //    .IsUnique();


        //    builder.Entity<State>()
        //        .HasIndex(p => new { p.Name, p.CountryID })
        //        .IsUnique();


        //    builder.Entity<City>()
        //        .HasIndex(p => new { p.Name, p.StateID })
        //        .IsUnique();

        //    builder.Entity<Room>()
        //        .HasIndex(p => new { p.Name, p.RoomCategoryID })
        //        .IsUnique();

        //    builder.Entity<Building>()
        //        .HasIndex(p => new { p.Name, p.LocationID })
        //        .IsUnique();

        //    builder.Entity<Location>()
        //        .HasIndex(p => new { p.Name, p.CityID, p.OrganizationID, p.Address })
        //        .IsUnique();

        //    builder.Entity<Room>()
        //    .HasIndex(p => new { p.Name, p.BuildingID, p.RoomCategoryID, p.FloorID })
        //    .IsUnique();
        //}
    }



}
