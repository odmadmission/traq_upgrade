﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OdmErp.Infrastructure.Migrations
{
    public partial class _15dec : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "admission");





            migrationBuilder.CreateTable(
              name: "AdmissionCampusTour",
              schema: "admission",
              columns: table => new
              {
                  ID = table.Column<long>(nullable: false)
                      .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                  InsertedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                  ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                  InsertedId = table.Column<long>(type: "bigint", nullable: false),
                  ModifiedId = table.Column<long>(type: "bigint", nullable: false),
                  Active = table.Column<bool>(type: "bit", nullable: false),
                  Status = table.Column<string>(type: "varchar(50)", nullable: true),
                  IsAvailable = table.Column<bool>(type: "bit", nullable: false),
                  AdmissionCampusTourDateId = table.Column<long>(type: "bigint", nullable: false),
                  AdmissionCampusTourSlotId = table.Column<long>(type: "bigint", nullable: false),
                  AdmissionId = table.Column<long>(type: "bigint", nullable: false),
                  smscontent = table.Column<string>(type: "nvarchar(500)", nullable: true),
                  IssmsSend = table.Column<bool>(type: "bit", nullable: false),
                  Isvisited = table.Column<bool>(type: "bit", nullable: false),
                  VisitedDate = table.Column<DateTime>(type: "datetime", nullable: true)
              },
              constraints: table =>
              {
                  table.PrimaryKey("PK_AdmissionCampusTour", x => x.ID);
              });

            migrationBuilder.CreateTable(
                name: "AdmissionCampusTourDate",
                schema: "admission",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InsertedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    InsertedId = table.Column<long>(type: "bigint", nullable: false),
                    ModifiedId = table.Column<long>(type: "bigint", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<string>(type: "varchar(50)", nullable: true),
                    IsAvailable = table.Column<bool>(type: "bit", nullable: false),
                    AcademicSessionId = table.Column<long>(type: "bigint", nullable: false),
                    CampusTourDate = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionCampusTourDate", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionCampusTourSlot",
                schema: "admission",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InsertedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    InsertedId = table.Column<long>(type: "bigint", nullable: false),
                    ModifiedId = table.Column<long>(type: "bigint", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<string>(type: "varchar(50)", nullable: true),
                    IsAvailable = table.Column<bool>(type: "bit", nullable: false),
                    AdmissionCampusTourDateID = table.Column<long>(type: "bigint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    No_Of_Registration = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    StartTime = table.Column<TimeSpan>(type: "time(7)", nullable: false),
                    EndTime = table.Column<TimeSpan>(type: "time(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionCampusTourSlot", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionCampusTourTimeline",
                schema: "admission",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InsertedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    InsertedId = table.Column<long>(type: "bigint", nullable: false),
                    ModifiedId = table.Column<long>(type: "bigint", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<string>(type: "varchar(50)", nullable: true),
                    IsAvailable = table.Column<bool>(type: "bit", nullable: false),
                    AdmissionCampusTourId = table.Column<long>(type: "bigint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(500)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionCampusTourTimeline", x => x.ID);
                });



            migrationBuilder.CreateTable(
             name: "AdmissionEcounselling",
             schema: "admission",
             columns: table => new
             {
                 ID = table.Column<long>(nullable: false)
                     .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                 InsertedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                 ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                 InsertedId = table.Column<long>(type: "bigint", nullable: false),
                 ModifiedId = table.Column<long>(type: "bigint", nullable: false),
                 Active = table.Column<bool>(type: "bit", nullable: false),
                 Status = table.Column<string>(type: "varchar(50)", nullable: true),
                 IsAvailable = table.Column<bool>(type: "bit", nullable: false),
                 AdmissionEcounsellingDateId = table.Column<long>(type: "bigint", nullable: false),
                 AdmissionEcounsellingSlotId = table.Column<long>(type: "bigint", nullable: false),
                 AdmissionId = table.Column<long>(type: "bigint", nullable: false),
                 smscontent = table.Column<string>(type: "nvarchar(500)", nullable: true),
                 IssmsSend = table.Column<bool>(type: "bit", nullable: true),
                 Isvisited = table.Column<bool>(type: "bit", nullable: true),
                 VisitedDate = table.Column<DateTime>(type: "datetime", nullable: true)
             },
             constraints: table =>
             {
                 table.PrimaryKey("PK_AdmissionEcounselling", x => x.ID);
             });

            migrationBuilder.CreateTable(
                name: "AdmissionEcounsellingDate",
                schema: "admission",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InsertedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    InsertedId = table.Column<long>(type: "bigint", nullable: false),
                    ModifiedId = table.Column<long>(type: "bigint", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<string>(type: "varchar(50)", nullable: true),
                    IsAvailable = table.Column<bool>(type: "bit", nullable: false),
                    AcademicSessionId = table.Column<long>(type: "bigint", nullable: false),
                    EcounsellingDate = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionEcounsellingDate", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionEcounsellingSlot",
                schema: "admission",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InsertedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    InsertedId = table.Column<long>(type: "bigint", nullable: false),
                    ModifiedId = table.Column<long>(type: "bigint", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<string>(type: "varchar(50)", nullable: true),
                    IsAvailable = table.Column<bool>(type: "bit", nullable: false),
                    AdmissionEcounsellingDateID = table.Column<long>(type: "bigint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    No_Of_Registration = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    StartTime = table.Column<TimeSpan>(type: "time(7)", nullable: false),
                    EndTime = table.Column<TimeSpan>(type: "time(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionEcounsellingSlot", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "AdmissionEcounsellingTimeline",
                schema: "admission",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InsertedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    InsertedId = table.Column<long>(type: "bigint", nullable: false),
                    ModifiedId = table.Column<long>(type: "bigint", nullable: false),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<string>(type: "varchar(50)", nullable: true),
                    IsAvailable = table.Column<bool>(type: "bit", nullable: false),
                    AdmissionEcounsellingId = table.Column<long>(type: "bigint", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(500)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdmissionEcounsellingTimeline", x => x.ID);
                });
                      
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropTable(
             name: "AdmissionEcounselling",
             schema: "admission");

            migrationBuilder.DropTable(
                name: "AdmissionEcounsellingDate",
                schema: "admission");

            migrationBuilder.DropTable(
                name: "AdmissionEcounsellingSlot",
                schema: "admission");

            migrationBuilder.DropTable(
                name: "AdmissionEcounsellingTimeline",
                schema: "admission");

            migrationBuilder.DropTable(
               name: "AdmissionCampusTour",
               schema: "admission");

            migrationBuilder.DropTable(
                name: "AdmissionCampusTourDate",
                schema: "admission");

            migrationBuilder.DropTable(
                name: "AdmissionCampusTourSlot",
                schema: "admission");

            migrationBuilder.DropTable(
                name: "AdmissionCampusTourTimeline",
                schema: "admission");

            migrationBuilder.DropTable(
                name: "AdmissionCounsellor",
                schema: "admission");



           
        }
    }
}
