﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/sms")]
    [ApiController]
    public class SmsController : ControllerBase
    {

        protected SmsMethod smsMethod;

        public SmsController(SmsMethod smsMethod)
        {
            this.smsMethod = smsMethod;
        }

        [Consumes("application/json")]
        [Route("send")]
        [HttpPost]
        public ActionResult<SmsResponseModel> SendSms(SmsRequestModel request)
        {
            return smsMethod.SendSms(request);
        }

        [Consumes("application/json")]
        [Route("verification")]
        [HttpPost]
        public ActionResult<SmsResponseModel> VerifySms(SmsRequestModel request)
        {
            return  smsMethod.VerifySms(request);
        }

        [Consumes("application/json")]
        [Route("validate_otp")]
        [HttpPost]
        public ActionResult<SmsResponseModel> VerifyOtp(SmsRequestModel request)
        {
            return smsMethod.VerifySms(request);
        }


        [Consumes("application/json")]
        [Route("send_otp")]
        [HttpPost]
        public ActionResult<SmsResponseModel> SendOtp(SmsInputModel model)
        {
            return smsMethod.SendOtp(model);
        }
    }
}