﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
  
    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {

        protected EmployeeMethod employeeMethod;

        public AccountController(EmployeeMethod employeeMethod)
        {
            this.employeeMethod = employeeMethod;
        }
       
        [HttpPost]
        [Consumes("application/json")]
        [Route("validate")]
        public ActionResult<LoginResponseModel> CheckCredentials( LoginRequestModel request)
        {
            return employeeMethod.CheckAccess(request);
        }
        [HttpPost]
        [Consumes("application/json")]
        [Route("login")]
        public ActionResult<LoginResponseModel> Login(LoginRequestModel request)
        {
            return employeeMethod.Login(request);
        }

        [HttpPost]
        [Consumes("application/json")]
        [Route("logout")]
        public ActionResult<BaseModel> Logout(LogoutRequestModel request)
        {
            return employeeMethod.LogoutAccount(request);
        }

        [HttpPost]
        [Consumes("application/json")]
        [Route("changePassword")]
        public ActionResult<LoginResponseModel> ChangePassword(ForgotPasswordRequest forgotPasswordRequest)
        {
            return employeeMethod.UpdatePassword(forgotPasswordRequest);
           
        }
    }
}