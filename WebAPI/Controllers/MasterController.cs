﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Entities.ModuleAggregate;
using OdmErp.ApplicationCore.Interfaces;
using WebAPI.Data;
using WebAPI.Models;
using WebAPI.Constants;
using static WebAPI.Models.SupportModel;


namespace WebAPI.Controllers
{
    [Produces("application/json")]
   
    [Route("api/master")]
    [ApiController]
    public class MasterController : ControllerBase
    {
        protected readonly IBloodGroupRepository bloodGroupRepository;
        protected readonly IReligionTypeRepository religionTypeRepository;
        protected readonly INationalityTypeRepository nationalityTypeRepository;
        protected readonly IProfessionTypeRepository professionTypeRepository;
        protected readonly IParentRelationshipTypeRepository parentRelationshipTypeRepository;
        protected readonly IGroupRepository groupRepository;
        protected readonly IOrganizationRepository organizationRepository;
        protected readonly IDepartmentRepository departmentRepository;
        protected readonly IModuleRepository moduleRepository;
        protected readonly ISubModuleRepository subModuleRepository;
        protected readonly IActionRepository actionRepository;
        protected readonly SupportMethod supportMethod;



        public MasterController(IBloodGroupRepository bloodGroupRepository, IReligionTypeRepository religionTypeRepository,
            INationalityTypeRepository nationalityTypeRepository, IProfessionTypeRepository professionTypeRepository,
            IParentRelationshipTypeRepository parentRelationshipTypeRepository, IGroupRepository groupRepository,
            IOrganizationRepository organizationRepository, IDepartmentRepository departmentRepository, 
            IModuleRepository moduleRepository, ISubModuleRepository subModuleRepository, IActionRepository actionRepository, SupportMethod supportMethod)
        {
            this.bloodGroupRepository = bloodGroupRepository;
            this.religionTypeRepository = religionTypeRepository;
            this.nationalityTypeRepository = nationalityTypeRepository;
            this.professionTypeRepository = professionTypeRepository;
            this.parentRelationshipTypeRepository = parentRelationshipTypeRepository;
            this.groupRepository = groupRepository;
            this.organizationRepository = organizationRepository;
            this.departmentRepository = departmentRepository;
            this.moduleRepository = moduleRepository;
            this.subModuleRepository = subModuleRepository;
            this.actionRepository = actionRepository;
            this.supportMethod = supportMethod;
        }


        //Get All BloodGroups
        [Route("getBloodGroups")]
        [HttpGet]
        public ActionResult<IEnumerable<BloodGroup>> GetBloodGroups()
        {
           
            return bloodGroupRepository.GetAllBloodGroup().ToList();
        }
        
        //Get All Religions
        [Route("getReligions")]
        [HttpGet]
        public ActionResult<IEnumerable<ReligionType>> GetReligions()
        {
            
            return religionTypeRepository.GetAllReligionTypes().ToList();
        }
       
        //Get All Nationality
        [Route("getNationalities")]
        [HttpGet]
        public ActionResult<IEnumerable<NationalityType>> GetNationality()
        {
            
            return nationalityTypeRepository.GetAllNationalities().ToList();
        }

        //Get Profession Types
        [HttpGet]
        [Route("getProfessionTypes")]
        public ActionResult<IEnumerable<ProfessionType>> GetAllProfessions()
        {
            var res = professionTypeRepository.GetAllProfessionTypes();
            return Ok(res);
        }

        //Get Parent Relationship Types
        [HttpGet]
        [Route("getParentRelationshipTypes")]
        public ActionResult<IEnumerable<ParentRelationshipType>> GetAllParentRelationshipTypes()
        {
            var res = parentRelationshipTypeRepository.GetAllParentRelationshipType();
            return Ok(res);
        }

        //Get GroupList
        [HttpGet]
        [Route("getGroupList")]
        public ActionResult<SupportBaseResponse> GetGroupList()
        {
           
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllGroupList().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }



        #region Support Master

        //Get Organisation List By GroupId
        [HttpGet]
        [Route("getOrganisationListByGroupId")]
        public ActionResult<SupportMaserBaseResponse> GetOrganizationList(long groupid)
        {

            SupportMaserBaseResponse model = new SupportMaserBaseResponse();
            try
            {
                var res = supportMethod.GetAllOrganizationListByGroupId(groupid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.organizationResponseModel = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.organizationResponseModel = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }



        }


        //Get  Department List By GroupID 
        [HttpGet]
        [Route("getDepartmentListByGroupId")]
        public ActionResult<SupportMaserBaseResponse> GetDepartmentListByGroupID(long groupid)
        {

            SupportMaserBaseResponse model = new SupportMaserBaseResponse();
            try
            {
                var res = supportMethod.GetAllDepartmentListByGroupID(groupid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.departmentModel = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.departmentModel = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }

        //Get OrganisationList  By Organisation ID
        [HttpGet]
        [Route("getDepartmentListByOrganisationId")]
        public ActionResult<SupportMaserBaseResponse> GetDepartmentListByOrganisationID(long organisationid)
        {

            SupportMaserBaseResponse model = new SupportMaserBaseResponse();
            try
            {
                var res = supportMethod.GetAllDepartmentListByOrganizationId(organisationid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.departmentModel = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.departmentModel = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }



        //Get Module List
        [HttpGet]
        [Route("getModuleList")]
        public ActionResult<SupportBaseResponse> GetModuleList()
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllModuleList().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }
        //Get SubModule List
        [HttpGet]
        [Route("getSubModuleList")]
        public ActionResult<SupportBaseResponse> GetSubModuleList()
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllSubModuleList().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }
        //Get Action List
        [HttpGet]
        [Route("getActionList")]
        public ActionResult<SupportBaseResponse> GetActionList()
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllActionList().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }

    }
    #endregion
}