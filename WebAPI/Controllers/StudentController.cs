﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Interfaces;
using WebAPI.Data;
using WebAPI.Models; 
using WebAPI.Constants;
using static WebAPI.Models.StudentModel;
using OdmErp.ApplicationCore.Entities.StudentAggregate;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        protected readonly IBloodGroupRepository bloodGroupRepository;
        protected readonly IReligionTypeRepository religionTypeRepository;
        protected readonly INationalityTypeRepository nationalityTypeRepository;
        protected readonly IStudentRepository studentRepository;
        protected readonly StudentMethod studentMethod;
        protected readonly IProfessionTypeRepository professionTypeRepository;
        protected readonly IParentRelationshipTypeRepository parentRelationshipTypeRepository;

        public StudentController(IBloodGroupRepository bloodGroupRepository, IReligionTypeRepository religionTypeRepository,
            INationalityTypeRepository nationalityTypeRepository, IStudentRepository studentRepository, StudentMethod studentMethod,
            IParentRelationshipTypeRepository parentRelationshipTypeRepository, IProfessionTypeRepository professionTypeRepository)
        {
            this.bloodGroupRepository = bloodGroupRepository;
            this.religionTypeRepository = religionTypeRepository;
            this.nationalityTypeRepository = nationalityTypeRepository;
            this.studentRepository = studentRepository;
            this.studentMethod = studentMethod;
            this.professionTypeRepository = professionTypeRepository;
            this.parentRelationshipTypeRepository = parentRelationshipTypeRepository;
        }


        //Get Parent Profile
        [HttpPost]
        [Route("getParentProfile")]
        public ActionResult<StudentBaseResponse> GetParentProfile(EmployeeBaseRequestModel employeeBaseRequestModel)
        {
            StudentBaseResponse model = new StudentBaseResponse();
            try
            {
                var res = studentMethod.GetParentProfileDetails(employeeBaseRequestModel);

                model.Status = "SUCCESS";
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.parentProfileModels = res.ToList();
                return model;
            }
            catch
            {
                return Ok();
            }

        }

        //Get Student Profile
        [HttpPost]
        [Route("getStudentProfile")]
        public ActionResult<StudentBaseResponse> GetStudentProfile(long id)
        {
            StudentBaseResponse model = new StudentBaseResponse();
            try
            {
                var res = studentMethod.GetStudentProfileDetails(id);

                model.Status = "SUCCESS";
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.studentProfileModels = res.ToList();
                return model;
            }
            catch
            {
                return Ok();
            }

        }

      
    }
}