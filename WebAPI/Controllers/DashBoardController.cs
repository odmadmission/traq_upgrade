﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static WebAPI.Models.DashBoardModel;

namespace WebAPI.Controllers
{

    [Produces("application/json")]
    [Route("api/DashBoard")]
    [ApiController]
    public class DashBoardController : ControllerBase
    {
        [HttpGet]
        [Route("dashBoard")]
        public ActionResult<DashBoardResponse> DashBoards()
        {
            DashBoardResponse dashBoardResponse = new DashBoardResponse();

            StoriesResponse[] storiesResponses = {
                new StoriesResponse{ ID = 1, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Stories/S1.jpg",Title = "ODMians win gold, silver and bronze in Taekwondo",Description = "8 states , 75 Schools and 886 Students participated in the tournament, 10 students from ODM Public School participated in this tournament and achieved One Gold, Two Silver and Three Bronze Medals in the particular tournament."},
                new StoriesResponse{ ID = 2, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Stories/S2.jpg",Title = "INVESTITURE CEREMONY CONDUCTED FOR ODMIANS",Description = "An investiture ceremony to induct ODMians as Captains and Vice Captains for the school and houses of the school"},
            };
            List<StoriesResponse> sto = storiesResponses.OfType<StoriesResponse>().ToList();

            ResultResponse[] resultResponses = {
                      new ResultResponse{ ID = 1, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Result/R1.jpg"},
                      new ResultResponse{ ID = 2, Image =  @"https://tracq.odmps.org/ODMImages/DashBoard/Result/R2.jpg"},
                      new ResultResponse{ ID = 3, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Result/R3.jpg"},
                      new ResultResponse{ ID = 4, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Result/R4.jpg"},
                      new ResultResponse{ ID = 5, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Result/R5.jpg"},
                      new ResultResponse{ ID = 6, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Result/R6.jpg"},
                      new ResultResponse{ ID = 7, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Result/R7.jpg"}
            };
            List< ResultResponse > res = resultResponses.OfType<ResultResponse>().ToList();

            AboutUs[] aboutUs = {
                new AboutUs{ ID = 1, Title = "Who We Are?",Desctiption = "In the year 1989, 10 students entered into a small dingy room which was supposedly their school and this was the first step towards a big dream visualized by a visionary educationist Dr. Satyabrata Minaketan, the Founder-Chairman of ODM educational group. With numerous degrees into his pocket and myriad career options before him, this man chose an unsteady and thorny path of establishing a school which would be a school with a difference and serve the mankind with real knowledge.That small room with 10 students was the beginning of what you see today as ODM Public School."},
                new AboutUs{ ID = 2, Title = "Mission",Desctiption = "To provide a platform to every child to explore his/her potentialImparting modernised education with eastern valuesTo create global leaders in every domainTo be a pioneer in quality educationTo mould every child into a good human"},
                new AboutUs{ ID = 3, Title = "Vision",Desctiption = "To set unique standards of quality education and inculcate humanity, leadership and integrity."},
                new AboutUs{ ID = 4, Title = "Motto",Desctiption = "Be Good and Make Others Good "},
                new AboutUs{ ID = 5, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/AboutUs/A1.jpg",Title = "ODM Documentary",Desctiption = "Place where we not only teach to learn, read and write but also to love and respect each other. A place which not only provides learning ground but also a modern high ground to its students. ODM School is where your imagination becomes a reality. The core values of ODM school is to promote the Humanity, Integrity and Leadership quality. Be good and make others good.  "},

            };
            List<AboutUs> about = aboutUs.OfType<AboutUs>().ToList();

            Curriculum[] curricula = {
                new Curriculum{ ID = 1, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Curriculam/C1.jpg",Title = "KIDS WORLD",Desctiption = "Education at ODM PUBLIC SCHOOL is an integrated process with a flexible curriculum that takes into account the heterogeneity of the learner and learning styles. Emphasis is laid on acquiring linguistic and logical ability and nurturing multiple intelligences by adopting innovative methods to stimulate interest and attention among the students. This opens up pathways to a holistic and harmonious development of the young learners.1.The Pre-Primary wing of ODM PUBLIC SCHOOL has Nursery, Lower and Upper Kindergarten classes.2.We follow the schedule and programmes with a fair amount of flexibility. Chances are made from time to time as demanded by the environment and the emerging situations in the classroom.3.The students are graded regularly on the basis of participation and performance in class activities.4.Area of growth and development : Physical, Language, Personal, Emotional, Social, Intellectual/ Cognitive Aesthetic Development, etc."},
                new Curriculum{ ID = 2, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Curriculam/C2.jpg",Title = "PRIMARY",Desctiption = "Our primary curriculum stresses strongly on the conceptual learning of the subjects, as here the skills the students developed in the Early Year Programme comes into action.Each year, as they begin their journey to primary classes from std-I to V they are introduced to a new curriculum in order to provide them with advanced learning. This phase serves as an intermediate phase between the conceptual learning stages to the career building stage.Everything learned in the primary classes will build the initial ground for the students to work towards their dream. In this crucial phase, every aspect of learning i.e. academics, inculcating values, discipline etc. is closely taken care of.The development during the primary stage of education empowers the comprehensive abilities enhancing their vision for both personal and academic goals.Our curriculum is meticulously designed to give students a clear understanding of the topics that have a vital significance throughout their life. We accomplish this through our innovative teaching methods focusing on the application of concepts and creating hands on experience for students, using learning kits and practical methods."},
                new Curriculum{ ID = 3, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Curriculam/C3.jpg",Title = "SECONDARY",Desctiption = "At ODM, Secondary education is backed by scientific learning along with the modern-day teaching pedagogy. Our teachers not only belief in providing students basic book-level exposure, we try at making the children think, ask questions like Why and How, and discover the answers themselves. Our teaching pedagogy is all about uplifting practical learning, inquisitiveness and thorough conceptual understanding.We generally follow a CBSE curriculum for all the classes. The Subjects available for the children vary from Class to Class. Maths, Science, English and Social Studies are compulsory subjects from Class VI-X. Students from Class VI-VIII, have to choose between the four available languages, Odia, Hindi, Sanskrit and French. However, in Class IX-X, they have to choose 1 language out of the two languages they opted in Class VIII."},
                new Curriculum{ ID = 4, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Curriculam/C4.jpg",Title = "SENIOR SECONDARY",Desctiption = "At ODM, Senior Secondary education is based on CBSE Curriculum. The major objective is to make students fundamentally clear about the concepts so that it will be helpful for them either in the competitive or in the future streams of academics. The teachers in the senior secondary department are widely experienced in providing CBSE teaching. The minimum teaching experience of any of our teacher is 7 years. The average teaching experience of all our teachers in 12.5 years.The children can choose from a wide variety of subjects. In Science, English, Physics, Chemistry are the three compulsory subjects. Apart from that the children can choose any two subjects out of Mathematics, Biology, Computer Science and Physical Education as 4th and 5th Optional. Please note, a combination of Biology and Comp. Sc is not allowed because of certain constraints.In commerce, similarly, students have to study English, Economics, Business Studies and Accountancy as four main subjects. They can choose either Entrepreneurship or Physical Education, as their 5th Optional.Along with these, all the Science and Commerce students are provided with an option to choose a 6th optional as well. Odissi and Painting are two subjects that are provided to the children. These subjects are provided to children to ensure a higher aggregate for them in the CBSE Boards."},
                new Curriculum{ ID = 5, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Curriculam/C5.jpg",Title = "DAY BOARDING",Desctiption = "ODM is established and is modelled on CBSE Curriculum a glimpse of which is provided here.Enquiry and skill based curriculum.Catering to individual learning styles.Learner and Learning centric.Focus on experimental things.Focus on analysis and discovery approach.Languages English, Mother tongue/Hindi/any other foreign language. "},
            };
            List<Curriculum> cur = curricula.OfType<Curriculum>().ToList();

            Videos[] videos = {
                new Videos{ID = 1, Image = @"https://tracq.odmps.org/ODMImages/DashBoard/Videos/V1.jpg", Title = "Blessing ceremony students of class X , 2019-20",Desctiption = "ODM Wishes Good Luck to all the students appearing for Board Examination 2020.On 15th February 2020, the students of class X, showered with blessings and loads of good luck for the forthcoming Board Examination 2020 by Chairman ODM Educational Group, Dr. Satyabrata Minaketan, Director F&A Mrs. Indumati Ray, Academic Counselor Mr.  B.N Rout, Academic Director Mr.  A.T Mishra, and the senior teachers of the school.Chairman ODM Educational Group, Dr Satyabrata Minaketan shared some valuable tips with the students to surmount their fear and so that students fare well in the upcoming board examination 2020. Vice Principals Mr. Rajesh Padhy and Ms. Jhinuk DuttaRay updated the students on the latest CBSE information on the examination. Students carefully listened to the tips shared and together took an oath to give their best for the board exams.The program culminated with the distribution of best wishes cards and memento. They departed among wistful wishes and dreams for a bright future promising to keep the ODM flag flying high.  "},
            };
            List<Videos> vid = videos.OfType<Videos>().ToList();


            dashBoardResponse.Videos = vid;
            dashBoardResponse.curriculum = cur;
            dashBoardResponse.aboutUs = about;
            dashBoardResponse.resultResponses = res;
            dashBoardResponse.storiesResponses = sto;

            return dashBoardResponse; 
        }
    }
}