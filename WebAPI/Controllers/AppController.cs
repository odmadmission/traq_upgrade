﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace WebAPI.Controllers
{
   
    public class AppController : Controller
    {


        public class ViewConstants
        {
            public static string NOT_APPLICABLE = "NA";
            public static string ARROW = "->";


        }

        public string NullableDateTimeToStringDate(DateTime? time)
        {
            return time == null ? ViewConstants.NOT_APPLICABLE : time.Value.ToString("dd-MMM-yyyy");

        }

        public string DateTimeToStringDate(DateTime time)
        {
            return time == null ? ViewConstants.NOT_APPLICABLE : time.ToString("dd-MMM-yyyy");

        }

        public (string Date, string Time, string DateTime) NullableDateTimeToStringDateTime(DateTime? time)
        {
            return time == null ? (ViewConstants.NOT_APPLICABLE, ViewConstants.NOT_APPLICABLE, ViewConstants.NOT_APPLICABLE) : (time.Value.ToString("dd-MMM-yyyy"),
                time.Value.ToString("hh:mm tt"), time.Value.ToString("dd-MMM-yyyy hh:mm tt"));

        }

        public (string, string, string) DateTimeToStringDateTime(DateTime time)
        {
            return time == null ? (ViewConstants.NOT_APPLICABLE, ViewConstants.NOT_APPLICABLE, ViewConstants.NOT_APPLICABLE) : (time.ToString("dd-MMM-yyyy"),
                  time.ToString("hh:mm tt"), time.ToString("dd-MMM-yyyy hh:mm tt"));


        }

    }
}