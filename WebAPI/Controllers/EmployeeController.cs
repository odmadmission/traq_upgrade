﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using WebAPI.Constants;
using WebAPI.Data;
using WebAPI.Models;


namespace WebAPI.Controllers
{
    [Produces("application/json")]

    [Route("api/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {

        protected readonly IEmployeeRepository employeeRepository;
        protected readonly CommonMethods commonMethods;

        protected readonly IModuleRepository moduleRepository;
        protected readonly ISubModuleRepository subModuleRepository;
        protected readonly IActionRepository actionRepository;
        protected readonly IAccessRepository accessRepository;
        protected readonly IEmployeeDesignationRepository employeeDesignationRepository;
        protected readonly IDesignationRepository designationRepository;
        protected readonly IActionAccessRepository actionAccessRepository;
        protected readonly IBankTypeRepository bankTypeRepository;
        protected readonly IBankRepository bankRepository;
        protected readonly IEmployeeBankAccountRepository employeeBankAccountRepository;
        protected readonly EmployeeMethod employeeMethod;
        protected readonly IAddressRepository addressRepository;
        protected readonly IEmployeeExperienceRepository employeeExperienceRepository;
        protected readonly IEmployeeEducationRepository employeeEducationRepository;
        protected readonly SupportMethod supportMethod;
        protected readonly IReligionTypeRepository religionTypeRepository;
        protected readonly INationalityTypeRepository nationalityTypeRepository;
        protected readonly IBloodGroupRepository bloodGroupRepository;
        protected readonly IAddressTypeRepository addressTypeRepository;
        protected readonly IEducationQualificationTypeRepository educationQualificationTypeRepository;

        public EmployeeController(IEmployeeRepository employeeRepository,
            CommonMethods commonMethods, IModuleRepository moduleRepository, SupportMethod supportMethod, IEducationQualificationTypeRepository educationQualificationTypeRepository,
        ISubModuleRepository subModuleRepository, IActionRepository actionRepository, IAddressTypeRepository addressTypeRepository,
            IAccessRepository accessRepository, IEmployeeDesignationRepository employeeDesignationRepository, IDesignationRepository designationRepository, IActionAccessRepository actionAccessRepository, IBankTypeRepository bankTypeRepository, IBankRepository bankRepository, IEmployeeBankAccountRepository employeeBankAccountRepository, EmployeeMethod employeeMethod, IAddressRepository addressRepository, IEmployeeExperienceRepository employeeExperienceRepository, IEmployeeEducationRepository employeeEducationRepository, IReligionTypeRepository religionTypeRepository,
            INationalityTypeRepository nationalityTypeRepository, IBloodGroupRepository bloodGroupRepository)
        {
            this.employeeRepository = employeeRepository;
            this.commonMethods = commonMethods;
            this.moduleRepository = moduleRepository;
            this.subModuleRepository = subModuleRepository;
            this.actionRepository = actionRepository;
            this.accessRepository = accessRepository;
            this.employeeDesignationRepository = employeeDesignationRepository;
            this.designationRepository = designationRepository;
            this.actionAccessRepository = actionAccessRepository;
            this.bankTypeRepository = bankTypeRepository;
            this.bankRepository = bankRepository;
            this.employeeBankAccountRepository = employeeBankAccountRepository;
            this.employeeMethod = employeeMethod;
            this.addressRepository = addressRepository;
            this.employeeExperienceRepository = employeeExperienceRepository;
            this.employeeEducationRepository = employeeEducationRepository;
            this.religionTypeRepository = religionTypeRepository;
            this.nationalityTypeRepository = nationalityTypeRepository;
            this.bloodGroupRepository = bloodGroupRepository;
            this.supportMethod = supportMethod;
            this.educationQualificationTypeRepository = educationQualificationTypeRepository;
            this.addressTypeRepository = addressTypeRepository;
        }







        #region EmployeeBasic

        [Route("getAllEmployeeList")]
        [HttpGet]
        public ActionResult<EmployeeResponseModel> GetAllEmployeeList(long roleId, long accessId, string choosedepartmenttypes, long? organisation, long? departments, string types)
        {
            try
            {
                EmployeeResponseModel responseModel = new EmployeeResponseModel();
                try
                {

                    if (commonMethods.checkaccessavailable("Employee", accessId, "List", "HR", roleId) == false)
                    {
                        responseModel.Status = "MODULE_ACCESS_DENIED";
                        responseModel.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    }

                    responseModel.type = supportMethod.GetAllType().ToList();
                    responseModel.empstatus = employeeMethod.EmployeeStatus().ToList();
                    var deptswithemp = commonMethods.GetAllDeptwithEmployee();
                    var res = employeeRepository.GetAllEmployee().Select(a => new GetEmployeeModel
                    {
                        ID = a.ID,
                        EmpCode = a.EmpCode,
                        FullName = employeeRepository.GetEmployeeFullNameById(a.ID),
                        DOB = a.DateOfBirth,
                        //DOBString = a.DateOfBirth,
                        Gender = a.Gender,
                        BloodGroupId = a.BloodGroupID,
                        Mobile = a.PrimaryMobile,
                        AlternateMobile = a.AlternativeMobile,
                        LandLineNumber = a.LandlineNumber,
                        Email = a.EmailId,
                        ReligionId = a.ReligionID,
                        NationalityId = a.NatiobalityID,
                        ImgUrl = "/ODMImages/EmployeeProfile/" + a.Image,
                        FirstName = a.FirstName,
                        MiddleName = a.MiddleName,
                        LastName = a.LastName,
                        Isleft = a.Isleft.Value,
                        ModifiedOn = a.ModifiedDate,
                        ModifiedOnString = a.ModifiedDate.ToString("dd-MMM-yyyy"),
                        Religion = a.ReligionID != null && a.ReligionID.Value > 0 ?
                                    religionTypeRepository.GetReligionTypeById(a.ReligionID.Value).Name : null,
                        Nationality = a.NatiobalityID != null && a.NatiobalityID.Value > 0 ?
                                         nationalityTypeRepository.GetNationalityById(a.NatiobalityID.Value).Name : null,
                        BloodGroup = a.BloodGroupID != null && a.BloodGroupID.Value > 0 ?
                                        bloodGroupRepository.GetBloodGroupById(a.BloodGroupID.Value).Name : null
                    }).ToList();

                    if (choosedepartmenttypes == "GroupType")
                    {
                        res = (from a in res
                               join b in deptswithemp on a.ID equals b.EmployeeID
                               where b.GroupID != 0 && b.OrganizationID == 0
                               select a).Distinct().ToList();
                    }
                    else if (choosedepartmenttypes == "OrganizationType")
                    {
                        res = (from a in res
                               join b in deptswithemp on a.ID equals b.EmployeeID
                               where b.GroupID == 0 && b.OrganizationID != 0
                               select a).Distinct().ToList();
                    }
                    if (organisation > 0)
                    {
                        res = (from a in res
                               join b in deptswithemp on a.ID equals b.EmployeeID
                               where b.OrganizationID == organisation
                               select a).Distinct().ToList();
                    }
                    if (departments > 0)
                    {
                        res = (from a in res
                               join b in deptswithemp on a.ID equals b.EmployeeID
                               where b.DepartmentID == departments
                               select a).Distinct().ToList();
                    }
                    if (types == "Current")
                    {
                        res = (from a in res
                               join b in deptswithemp on a.ID equals b.EmployeeID
                               where a.Isleft == false
                               select a).Distinct().ToList();
                    }
                    else if (types == "Left")
                    {
                        res = (from a in res
                               join b in deptswithemp on a.ID equals b.EmployeeID
                               where a.Isleft == true
                               select a).Distinct().ToList();
                    }

                    responseModel.empList = res;
                    if (responseModel != null)
                    {
                        responseModel.StatusCode = HttpStatusCode.SUCCESS;
                        responseModel.Status = CommonText.SUCCESS;
                    }
                }
                catch
                {
                    responseModel.StatusCode = HttpStatusCode.EXCEPTION;
                    responseModel.Status = CommonText.EXCEPTION;
                }

                return responseModel;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [Route("getViewEmployeeDetails")]
        [HttpGet]
        public ActionResult<Employeecls> GetViewEmployeeDetails(long? id)
        {
            Employeecls employeecls = new Employeecls();
            try
            {
                employeecls.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
                employeecls.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
                employeecls.religionTypes = religionTypeRepository.GetAllReligionTypes();

                Access access = new Access();


                var employeedetails = employeeRepository.GetEmployeeById(id.Value);
                employeecls.AlternativeMobile = employeedetails.AlternativeMobile;
                employeecls.BloodGroupID = employeedetails.BloodGroupID;
                employeecls.BloodGroupName = employeedetails.BloodGroupID != null && employeedetails.BloodGroupID != 0 ? bloodGroupRepository.GetBloodGroupById(employeedetails.BloodGroupID.Value).Name : "";
                employeecls.DateOfBirth = employeedetails.DateOfBirth != null ? employeedetails.DateOfBirth : null;
                employeecls.EmailId = employeedetails.EmailId;
                employeecls.EmpCode = employeedetails.EmpCode;
                employeecls.FirstName = employeedetails.FirstName;
                employeecls.Gender = employeedetails.Gender;
                employeecls.ID = employeedetails.ID;
                employeecls.LandlineNumber = employeedetails.LandlineNumber;
                employeecls.LastName = employeedetails.LastName;
                employeecls.MiddleName = employeedetails.MiddleName;
                employeecls.NatiobalityID = employeedetails.NatiobalityID;
                employeecls.NatiobalityName = (employeedetails.NatiobalityID != null && employeedetails.NatiobalityID != 0) ? nationalityTypeRepository.GetNationalityById(employeedetails.NatiobalityID.Value).Name : "";
                employeecls.PrimaryMobile = employeedetails.PrimaryMobile;
                employeecls.ReligionID = employeedetails.ReligionID;
                employeecls.ReligionName = employeedetails.ReligionID != null && employeedetails.ReligionID != 0 ? religionTypeRepository.GetReligionTypeById(employeedetails.ReligionID.Value).Name : "";

                var addressTypes = addressTypeRepository.GetAllAddressType();
                var address = commonMethods.GetAllAddressByUsingEmployeeId(employeedetails.ID);
                var employeeaddress = (from a in addressTypes
                                       select new EmployeeTypewithAddresscls
                                       {
                                           typeId = a.ID,
                                           typeName = a.Name,
                                           employeeAdress = address.Where(m => m.AddressTypeID == a.ID).FirstOrDefault()
                                       }).ToList();
                employeecls.employeeadresslist = employeeaddress;
                employeecls.employeeDesignationlist = commonMethods.GetAllDesignationByUsingEmployeeId(employeedetails.ID);
                employeecls.employeeExperiencelist = employeeExperienceRepository.GetExperienceByEmployeeId(employeedetails.ID);

                var banktypes = bankTypeRepository.GetAllBankType();
                var employeebank = commonMethods.GetAllBankByUsingEmployeeId(employeedetails.ID);
                var banks = (from a in banktypes
                             select new EmployeeTypewithAddresscls
                             {
                                 typeId = a.ID,
                                 typeName = a.Name,
                                 bankCls = employeebank.Where(m => m.BankTypeID == a.ID).FirstOrDefault()
                             }).ToList();

                employeecls.employeeBanks = banks;
                var qualificationtypes = educationQualificationTypeRepository.GetAllEducationQualificationType();
                var employeeEducations = commonMethods.GetAllEducationByUsingEmployeeId(employeedetails.ID);
                var education = (from a in qualificationtypes
                                 select new EmployeeTypewithAddresscls
                                 {
                                     typeId = a.ID,
                                     typeName = a.Name,
                                     employeeEducation = employeeEducations.Where(m => m.EducationQualificationTypeID == a.ID).FirstOrDefault()
                                 }).ToList();
                employeecls.employeeEducationlist = education;
                employeecls.employeeTimelines = employeeRepository.GetAllEmployeeTimeline() == null ? null : employeeRepository.GetAllEmployeeTimeline().Where(a => a.EmployeeId == employeedetails.ID && a.IsPrimary == true).ToList();
                employeecls.requiredDocumentCls = commonMethods.GetAllDocumentByUsingEmployeeId(employeedetails.ID);
                employeecls.employeeAccesscls = commonMethods.GetAccessTypeData(employeedetails.ID);
                if (employeecls.employeeAccesscls.AccessID != 0)
                {
                    employeecls.employeeModuleSubModuleAccess = commonMethods.givenAccesscls(employeecls.employeeAccesscls.AccessID).ToList();
                }
                var total = 12 + (employeedetails.Image != null ? 4 : 0) + (employeecls.employeeadresslist.Count() > 0 ? 14 : 0) + (employeecls.employeeDesignationlist.Count() > 0 ? 14 : 0) + (employeecls.employeeExperiencelist.Count() > 0 ? 14 : 0) + (employeecls.employeeBanks.Count() > 0 ? 14 : 0) + (employeecls.employeeEducationlist.Count() > 0 ? 14 : 0) + (employeecls.requiredDocumentCls.Count() > 0 ? 14 : 0);

                if (employeecls != null)
                {
                    employeecls.StatusCode = HttpStatusCode.SUCCESS;
                    employeecls.Status = CommonText.SUCCESS;
                }
            }
            catch
            {
                employeecls.StatusCode = HttpStatusCode.EXCEPTION;
                employeecls.Status = CommonText.EXCEPTION;
            }

            return employeecls;
        }

        [Route("getEmployeeModuleAndSubmoduleAccess")]
        [HttpGet]
        public ActionResult<EmployeeModuleAccess> GetEmployeeModuleAndSubmoduleAccess(long id)
        {
            EmployeeModuleAccess moduleAccess = new EmployeeModuleAccess();
            try
            {
                var empmoduleaccess = commonMethods.GetAccessTypeData(id);
                if (empmoduleaccess.AccessID != 0)
                {
                    moduleAccess.employeeModuleSubModuleAccess = commonMethods.givenAccesscls(empmoduleaccess.AccessID).ToList();
                }

                if (moduleAccess != null)
                {
                    moduleAccess.StatusCode = HttpStatusCode.SUCCESS;
                    moduleAccess.Status = CommonText.SUCCESS;
                }
                else
                {
                    moduleAccess.StatusCode = HttpStatusCode.No_Record;
                    moduleAccess.Status = CommonText.No_Record;
                }
            }
            catch
            {
                moduleAccess.StatusCode = HttpStatusCode.EXCEPTION;
                moduleAccess.Status = CommonText.EXCEPTION;
            }
            return moduleAccess;

        }



        //Get Employee Details By Employee ID
        [Route("getEmployeeById")]
        [HttpGet]
        public ActionResult<GetEmployeeModel> GetEmployeeById(long id)
        {
            var res = employeeRepository.GetEmployeeById(id);


            GetEmployeeModel getEmployeeModel = new GetEmployeeModel()
            {
                ID = res.ID,
                EmpCode = res.EmpCode,
                FullName = employeeRepository.GetEmployeeFullNameById(res.ID),
                DOB = res.DateOfBirth,
                Gender = res.Gender,
                BloodGroupId = res.BloodGroupID,
                Mobile = res.PrimaryMobile,
                AlternateMobile = res.AlternativeMobile,
                LandLineNumber = res.LandlineNumber,
                Email = res.EmailId,
                ReligionId = res.ReligionID,
                NationalityId = res.NatiobalityID,
                ImgUrl = res.Image,
                FirstName = res.FirstName,
                MiddleName = res.MiddleName,
                LastName = res.LastName,
                Religion = res.ReligionID != null && res.ReligionID.Value > 0 ?
                religionTypeRepository.GetReligionTypeById(res.ReligionID.Value).Name : null,
                Nationality = res.NatiobalityID != null && res.NatiobalityID.Value > 0 ?
                nationalityTypeRepository.GetNationalityById(res.NatiobalityID.Value).Name : null,
                BloodGroup = res.BloodGroupID != null && res.BloodGroupID.Value > 0 ?
                bloodGroupRepository.GetBloodGroupById(res.BloodGroupID.Value).Name : null
            };

            return getEmployeeModel;
        }

        //Update Employee 
        [Route("update")]
        [HttpPost]
        public ActionResult<EmployeeUpdateCommonModel> UpdateEmployee(GetEmployeeModel getEmployeeModel)
        {
            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();
            try
            {
                Employee employee = new Employee()
                {
                    FirstName = getEmployeeModel.FirstName,
                    MiddleName = getEmployeeModel.MiddleName,
                    LastName = getEmployeeModel.LastName,
                    EmpCode = getEmployeeModel.EmpCode,
                    DateOfBirth = getEmployeeModel.DOB,
                    Gender = getEmployeeModel.Gender,
                    BloodGroupID = getEmployeeModel.BloodGroupId,
                    PrimaryMobile = getEmployeeModel.Mobile,
                    AlternativeMobile = getEmployeeModel.AlternateMobile,
                    LandlineNumber = getEmployeeModel.LandLineNumber,
                    EmailId = getEmployeeModel.Email,
                    ReligionID = getEmployeeModel.ReligionId,
                    NatiobalityID = getEmployeeModel.NationalityId,
                    //Image = getEmployeeModel.ImgUrl,

                    ID = getEmployeeModel.ID

                };

                bool response = employeeRepository.UpdateEmployee(employee);

                if (response == true)
                {
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch(Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;


        }

        #endregion

        #region EmployeeAddress

        //Get Employee Address By Access ID
        [Route("getEmployeeAddress")]
        [HttpPost]
        public ActionResult<EmployeeBaseRepsoneModel> GetEmployeeAddress(EmployeeBaseRequestModel employeeBaseRequestModel)
        {
            EmployeeBaseRepsoneModel model = new EmployeeBaseRepsoneModel();
            if (commonMethods.checkaccessavailable("Address Type", employeeBaseRequestModel.AccessId, "List", "Master", employeeBaseRequestModel.RoleId) == false)
            {
                model.Status = "MODULE_ACCESS_DENIED";
                model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;

                return model;
            }
            else
            {
                var res = employeeMethod.GetEmployeeAddress(employeeBaseRequestModel);

                model.Status = "SUCCESS";
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.employeeAddresses = res.ToList();
                return model;
            }
        }

        //Update Employee Address By Access ID
        [Route("updateEmployeeAddress")]
        [HttpPost]
        public ActionResult<EmployeeUpdateCommonModel> UpdateEmployeeAddress(EmployeeAddressModel employeeAddressModel)
        {
            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();
            try
            {
               

                    Address Address = new Address()
                    {
                        ID = employeeAddressModel.ID,
                        AddressLine1 = employeeAddressModel.AddressLine1,
                        AddressLine2 = employeeAddressModel.AddressLine2,
                        PostalCode = employeeAddressModel.PostalCode,
                        CityID = employeeAddressModel.CityId,
                        StateID = employeeAddressModel.StateId,
                        CountryID = employeeAddressModel.CountryId,
                        AddressTypeID = employeeAddressModel.AddressTypeId,
                        EmployeeID = employeeAddressModel.EmployeeId
                    };

                   bool changes= addressRepository.UpdateAddress(Address);   
                if(changes)
                {
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status =CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
                
            }
            catch(Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;
        }

        #endregion

        #region EmployeeBankAccount

        //Get Employee Bank Account Details
        [Route("getEmployeeBankAccounts")]
        [HttpPost]
        public ActionResult<EmployeeBaseRepsoneModel> GetEmployeeBankAccountDetails(EmployeeBaseRequestModel employeeBaseRequestModel)
        {
            EmployeeBaseRepsoneModel model = new EmployeeBaseRepsoneModel();
            try
            {
                //if (commonMethods.checkaccessavailable("EmployeeBankDetails", employeeBaseRequestModel.AccessId, "List", "Employee", employeeBaseRequestModel.RoleId) == false)
                //{
                //    model.Status = "MODULE_ACCESS_DENIED";
                //    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;

                //    return model;
                //}
                //else
                //{
                    var res = employeeMethod.GetEmployeeBankAccount(employeeBaseRequestModel); 

                    model.Status = "SUCCESS";
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.empBankAccounts = res.ToList();
                    return model;                    
                //}
            }
            catch
            {
                return Ok();
            }
        }

        #endregion

        #region EmployeeExperience

        //Get Employee Experiences
        [Route("getEmployeeExperiences")]
        [HttpPost]
        public ActionResult<EmployeeBaseRepsoneModel> GetEmployeeExperience(EmployeeBaseRequestModel employeeBaseRequestModel)
        {
            EmployeeBaseRepsoneModel model = new EmployeeBaseRepsoneModel();
            try
            {
                //if (commonMethods.checkaccessavailable("EmployeeExperienceDetails", employeeBaseRequestModel.AccessId, "List", "Employee", employeeBaseRequestModel.RoleId) == false)
                //{
                //    model.Status = "MODULE_ACCESS_DENIED";
                //    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;

                //    return model;
                //}
                //else
                //{
                    var res = employeeMethod.GetEmployeeExperience(employeeBaseRequestModel);
                    if (res != null)
                    {
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.Status = "SUCCESS";
                        model.employeeExperiences = res.ToList();

                    }
                //}

            }
            catch
            {
                return Ok();
            }

            return model;
        }

        //Update Employee Experiences
        [Route("updateEmployeeExperiences")]
        [HttpPost]
        public ActionResult<EmployeeUpdateCommonModel> UpdateEmployeeExperience(EmployeeExperienceModel employeeExperienceModel)
        {
            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();
            try
            {
                if (ModelState.IsValid)
                {
                    EmployeeExperience employeeExperience = new EmployeeExperience()
                    {
                        ID = employeeExperienceModel.ID,
                        OrganizationName = employeeExperienceModel.OrganizationName,
                        Designation = employeeExperienceModel.Desingnation,
                        SalaryPerAnum = employeeExperienceModel.SalaryPerAnum,
                        FromDate = employeeExperienceModel.FromDate,
                        ToDate = employeeExperienceModel.ToDate,
                        EmployeeID = employeeExperienceModel.EmployeeId
                    };

                    var response = employeeExperienceRepository.UpdateExperience(employeeExperience);
                    if (response)
                    {
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.Status = CommonText.SUCCESS;
                    }
                    else
                    {
                        model.StatusCode = HttpStatusCode.FAILURE;
                        model.Status = CommonText.FAILED;
                    }
                }
            }
            catch(Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;
        }

        #endregion

        #region EmployeeEducations
       
        /// <summary>
        /// Returns Educational List for given accessId.
        /// </summary>
       
        [Route("getEmployeeEducations")]
        [HttpPost]
        public ActionResult<EmployeeBaseRepsoneModel> GetEmployeeEducations(EmployeeBaseRequestModel employeeBaseRequestModel)
        {
            EmployeeBaseRepsoneModel model = new EmployeeBaseRepsoneModel();
            try
            {
                var res = employeeMethod.GetEmployeeEducation(employeeBaseRequestModel);
                if (res != null)
                {
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = "SUCCESS";
                    model.employeeEducationModels = res.ToList();
                }
            }
            catch
            {
                return Ok();
            }

            return model;
        }

        //Update Employee Educations
        [Route("updateEmployeeEducations")]
        [HttpPost]
        public ActionResult<EmployeeUpdateCommonModel> UpdateEmployeeEducation(EmployeeEducationModel employeeEducationModel)
        {

            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();
            try
            {
                if (ModelState.IsValid)
                {
                    EmployeeEducation employeeEducation = new EmployeeEducation()
                    {
                        ID = employeeEducationModel.ID,
                        EducationQualificationID = employeeEducationModel.EducationQualificationId,
                        EmployeeID = employeeEducationModel.EmployeeId,
                        College = employeeEducationModel.College,
                        University = employeeEducationModel.Univarsity,
                        FromYear = employeeEducationModel.FromYear,
                        ToYear = employeeEducationModel.ToYear
                    };

                    var response = employeeEducationRepository.UpdateEmployeeEducation(employeeEducation);
                    if (response)
                    {
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.Status = CommonText.SUCCESS;
                    }
                    else
                    {
                        model.StatusCode = HttpStatusCode.FAILURE;
                        model.Status = CommonText.FAILED;
                    }
                }
            }
            catch(Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;
        }
        #endregion



    }
}