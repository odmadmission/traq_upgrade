﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities.TodoAggregate;
using OdmErp.ApplicationCore.Interfaces;
using WebAPI.Constants;
using WebAPI.Data;
using WebAPI.Models;
using static WebAPI.Models.TodoModel;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        protected readonly IToDoRepository toDoRepository;
        protected readonly TodoMethod todoMethod;
        protected readonly CommonMethods commonMethods;

        public TodoController(IToDoRepository toDoRepository, TodoMethod todoMethod, CommonMethods commonMethods)
        {
            this.toDoRepository = toDoRepository;
            this.todoMethod = todoMethod;
            this.commonMethods = commonMethods;

        }
        //// Return task status list 
        //[Route("getTodoStatus")]
        //[HttpGet]
        //public ActionResult<TodoBaseResponse> GetTodoStatusList()
        //{
        //    TodoBaseResponse model = new TodoBaseResponse();
        //    try
        //    {
        //        var res = todoMethod.GetAllTodoStatus().ToList();
        //        if (res.Count == 0)
        //        {
        //            model.Status = CommonText.No_Record;
        //            model.StatusCode = HttpStatusCode.No_Record;
        //            model.todoCommonModels = res;
        //            return model;
        //        }

        //        model.Status = CommonText.SUCCESS;
        //        model.StatusCode = HttpStatusCode.SUCCESS;
        //        model.todoCommonModels = res;
        //        return model;
        //    }
        //    catch
        //    {
        //        model.Status = CommonText.FAILED;
        //        model.StatusCode = HttpStatusCode.FAILURE;
        //        return model;
        //    }

        //}




        #region    get All Status


        [Route("getAllStatus")]
        [HttpGet]
        public ActionResult<TodoStatusResposnse> getAllStatus()
        {
            TodoStatusResposnse model = new TodoStatusResposnse();
            try
            {
                var res = todoMethod.GetAllStattus().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.StatusListModel = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.StatusListModel = res;


                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }


        }

        #endregion

        #region    get All Priority


        [Route("getAllPriority")]
        [HttpGet]
        public ActionResult<TodoPriorityResposnse> getAllPriority()
        {
            TodoPriorityResposnse model = new TodoPriorityResposnse();
            try
            {
                var res = todoMethod.GetAllTodoPriorities().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.PriorityListModel = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.PriorityListModel = res;


                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }

        #endregion

        #region    Return task Status and priority list

        [Route("getAllTodoStatusAndPriority")]
        [HttpGet]
        public ActionResult<TodoModelResponse> GetAllTodoStatusAndPriority()
        {
            TodoModelResponse model = new TodoModelResponse();
            try
            {
                var priorty = todoMethod.GetAllTodoPriorities().ToList();
                var status = todoMethod.GetAllStattus().ToList();

                if (status.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.todoStatusModel = status;
                    model.todoPriorityModel = priorty;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.todoStatusModel = status;
                model.todoPriorityModel = priorty;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region    get All TodoStatus And Priority And Employee

        [Route("getAllTodoStatusAndPriorityAndEmployee")]
        [HttpGet]
        public ActionResult<TodoModelResponse> getTodoStatusAndPriority(long userid)
        {
            TodoModelResponse model = new TodoModelResponse();
            try
            {
                var priorty = todoMethod.GetAllTodoPriorities().ToList();
                var status = todoMethod.GetAllStattusForTask().ToList();
                var employee = todoMethod.GetEmployeesForTasks(userid).ToList();
                if (status.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.todoStatusModel = status;
                    model.todoPriorityModel = priorty;
                    model.employeeModel = employee;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.todoStatusModel = status;
                model.todoPriorityModel = priorty;
                model.employeeModel = employee;
                return model;
            }
            catch (Exception e)
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region    Return all comments for taskid


        [Route("getAllCommentsByTaskId")]
        [HttpGet]
        public ActionResult<TodoCommentResponse> getAllCommentsByTaskId(long taskid, long userid)
        {
            TodoCommentResponse model = new TodoCommentResponse();
            try
            {
                var todo = toDoRepository.GetTodoById(taskid);
                if (todo.EmployeeID == userid || todo.AssignedId == userid)
                {
                    var comments = toDoRepository.GetAllTodoComment().Where(a => a.TodoID == taskid && a.InsertedId != userid && a.IsRead == false).ToList();
                    foreach (var a in comments)
                    {
                        TodoComment comment = a;
                        comment.IsRead = true;
                        comment.ModifiedId = userid;
                        toDoRepository.UpdateTodoComment(comment);
                    }
                }

                var res = todoMethod.GetComments(taskid, userid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.todoCommentModel = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.todoCommentModel = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }
        #endregion

        #region   Return all subtasks for taskid


        [Route("getSubtasksByTaskId")]
        [HttpGet]
        public ActionResult<TodoTaskResponse> GetgetSubtasksByTaskIdList(long todoid)
        {
            TodoTaskResponse model = new TodoTaskResponse();
            try
            {
                var priorty = todoMethod.GetAllTodoPriorities().ToList();
                var status = todoMethod.GetAllStattus().ToList();

                var res = todoMethod.GetAllTodoSubtaskByTodoTask(todoid).ToList();

                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.subTaskModel = res;
                    model.todoStatusModel = status;
                    model.todoPriorityModel = priorty;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.subTaskModel = res;
                model.todoStatusModel = status;
                model.todoPriorityModel = priorty;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region  Return department list for employeeid


        [Route("getDepartmentsByEmployeeId")]
        [HttpGet]
        public ActionResult<TodoDepartmentResponse> getDepartmentsByEmployeeIdList(long employeeid)
        {
            TodoDepartmentResponse model = new TodoDepartmentResponse();
            try
            {
                var res = todoMethod.GetAllDepartmentListByEmployeeID(employeeid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.todoDepartmentModel = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.todoDepartmentModel = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }
        #endregion

        #region Return all tasks and subtasks assigned by others for employeeid ..assign to me
        [Route("getTaskAndSubTaskAssignedByOther")]
        [HttpGet]

        public ActionResult<TaskAssigneResponseModel> GetTaskAssignedByOther(long accessId, long roleId, long employeeid, long[] TodoPriority, string[] TodoStatusName, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            TaskAssigneResponseModel model = new TaskAssigneResponseModel();
            try
            {

                if (commonMethods.checkaccessavailable("Assigned By Others", accessId, "View", "Task", roleId) == false)
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }
                else
                {
                    var res = todoMethod.GetAllTaskAssignByOther(employeeid, TodoPriority, TodoStatusName,FromDate,ToDate).ToList();
                    var statuses = todoMethod.GetAllStattus().ToList();
                    var priorities = toDoRepository.GetAllTodoPriority().Select(a => new TodoCommonModel { ID = a.ID, Name = a.Name }).ToList();
                    if (res.Count == 0)
                    {
                        model.Status = CommonText.No_Record;
                        model.StatusCode = HttpStatusCode.No_Record;
                        model.todoStatus = statuses;
                        model.todoPriority = priorities;
                        model.taskAssignResponseModel = res;
                        return model;
                    }

                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.todoStatus = statuses;
                    model.todoPriority = priorities;
                    model.taskAssignResponseModel = res;
                    return model;
                }
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        [Route("getTaskAndSubTaskAssignedByOtherAndriod")]
        [HttpPost]

        public ActionResult<TaskAssigneResponseModel> GetTaskAndSubTaskAssignedByOtherAndriod([FromBody]assignToMeModel assignme)
        {
            TaskAssigneResponseModel model = new TaskAssigneResponseModel();
            try
            {

                if (commonMethods.checkaccessavailable("Assigned By Others", assignme.accessId, "View", "Task", assignme.roleId) == false)
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }
                else
                {
                    var res = todoMethod.GetAllTaskAssignByOther(assignme.employeeid, assignme.TodoPriority, assignme.TodoStatusName,assignme.FromDate,assignme.ToDate).ToList();
                    var statuses = todoMethod.GetAllStattus().ToList();
                    var priorities = toDoRepository.GetAllTodoPriority().Select(a => new TodoCommonModel { ID = a.ID, Name = a.Name }).ToList();
                    if (res.Count == 0)
                    {
                        model.Status = CommonText.No_Record;
                        model.StatusCode = HttpStatusCode.No_Record;
                        model.todoStatus = statuses;
                        model.todoPriority = priorities;
                        model.taskAssignResponseModel = res;
                        return model;
                    }

                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.todoStatus = statuses;
                    model.todoPriority = priorities;
                    model.taskAssignResponseModel = res;
                    return model;
                }
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }
        #endregion

        #region get Task And SubTask Assigned ToOther

        //Return all tasks and subtasks assigned to others for emplopyeeid
        [Route("getTaskAndSubTaskAssignedToOther")]
        [HttpGet]

        public ActionResult<TaskAssigneResponseModel> GetTaskAssignedToOther(long accessId, long roleId, long employeeid, long[] employe, long[] TodoPriority, string[] TodoStatusName, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            TaskAssigneResponseModel model = new TaskAssigneResponseModel();
            try
            {


                if (commonMethods.checkaccessavailable("Assigned To Others", accessId, "View", "Task", roleId) == false)
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }
                else
                {
                    var res = todoMethod.GetAllTaskAssignToOther(employeeid, employe, TodoPriority, TodoStatusName,FromDate,ToDate);
                    var statuses = todoMethod.GetAllStattus().ToList();
                    var empList = todoMethod.GetEmployeeforAssignToTask(employeeid).ToList();
                    var priorities = toDoRepository.GetAllTodoPriority().Select(a => new TodoCommonModel { ID = a.ID, Name = a.Name }).ToList();

                    if (res != null)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.todoStatus = statuses;
                        model.todoPriority = priorities;
                        model.employeeList = empList;
                        model.taskAssignResponseModel = res;
                        return model;
                    }
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.todoStatus = statuses;
                    model.todoPriority = priorities;
                    model.employeeList = empList;
                    model.taskAssignResponseModel = res;
                    return model;

                }
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        //Return all tasks and subtasks assigned to others for emplopyeeid
        [Route("getTaskAndSubTaskAssignedToOtherAndriod")]
        [HttpPost]

        public ActionResult<TaskAssigneResponseModel> GetTaskAndSubTaskAssignedToOtherAndriod([FromBody] assignToOtherModel assignother)
        {
            TaskAssigneResponseModel model = new TaskAssigneResponseModel();
            try
            {
                if (commonMethods.checkaccessavailable("Assigned To Others", assignother.accessId, "View", "Task", assignother.roleId) == false)
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }
                else
                {
                    var res = todoMethod.GetAllTaskAssignToOther(assignother.employeeid, assignother.employe, assignother.TodoPriority, assignother.TodoStatusName,assignother.FromDate,assignother.ToDate);
                    var statuses = todoMethod.GetAllStattus().ToList();
                    var empList = todoMethod.GetEmployeeforAssignToTask(assignother.employeeid).ToList();
                    var priorities = toDoRepository.GetAllTodoPriority().Select(a => new TodoCommonModel { ID = a.ID, Name = a.Name }).ToList();

                    if (res != null)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.todoStatus = statuses;
                        model.todoPriority = priorities;
                        model.employeeList = empList;
                        model.taskAssignResponseModel = res;
                        return model;
                    }
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.todoStatus = statuses;
                    model.todoPriority = priorities;
                    model.employeeList = empList;
                    model.taskAssignResponseModel = res;
                    return model;

                }
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }
        #endregion

        #region get All Task Details ByTaskId   // get All SubTask Details ByTaskId

        [Route("getAllDetailsOfTaskByTaskId")]
        [HttpGet]
        public ActionResult<TaskDetailsResponseModel> GetTaskDetailsByTaskId(long taskid, long userid)
        {
            TaskDetailsResponseModel model = new TaskDetailsResponseModel();
            try
            {
                var res = todoMethod.GetSingleTasks(taskid);
                var Subtask = todoMethod.GetSingleTodoSubtaskByTodoTask(taskid);
                var TimeLine = todoMethod.GetSingleTaskTimelines(taskid);
                var Comment = todoMethod.GetSingleComments(taskid, userid);
                var AttachMent = todoMethod.GetSingleAttachments(taskid, userid);
                if (res == null)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.taskDetailsModel = res;
                    model.SubtaskDetailsModel = Subtask;
                    model.taskCommentsModel = Comment;
                    model.taskTimelinesModel = TimeLine;
                    model.taskAttachmentsModel = AttachMent;
                    return model;
                }
                else
                {
                    if (res.parentID == 0)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.taskDetailsModel = res;
                        model.SubtaskDetailsModel = Subtask;
                        model.taskCommentsModel = Comment;
                        model.taskTimelinesModel = TimeLine;
                        model.taskAttachmentsModel = AttachMent;
                        return model;
                    }
                    else
                    {
                        model.Status = CommonText.No_Record;
                        model.StatusCode = HttpStatusCode.No_Record;
                        return model;
                    }
                }
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }


        [Route("getTaskDetailsByTaskId")]
        [HttpGet]
        public ActionResult<TaskResponseModel> GetTaskDetailsByTaskId(long taskid)
        {
            TaskResponseModel model = new TaskResponseModel();
            try
            {
                var res = todoMethod.GetSingleTasks(taskid);
                //var Subtask = todoMethod.GetAllTodoSubtaskByTodoTask(taskid).ToList();
                //var TimeLine = todoMethod.GetTaskTimelines(taskid).ToList();
                //var Comment = todoMethod.GetComments(taskid, userid).ToList();
                //var AttachMent = todoMethod.GetAttachments(taskid, userid).ToList();
                if (res == null)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.taskDetailsModel = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.taskDetailsModel = res;

                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        [Route("getAllDetailsOfSubTaskBySubTaskId")]
        [HttpGet]
        public ActionResult<SubTaskResponseModel> GetSubTaskDetailsByTaskId(long Subtaskid, long userid)
        {
            SubTaskResponseModel model = new SubTaskResponseModel();
            try
            {
                //var res = todoMethod.GetSingleTasks(Subtaskid);
                var Subtask = todoMethod.GetAllSubtaskBySubTaskId(Subtaskid);
                var TimeLine = todoMethod.GetSingleTaskTimelines(Subtaskid);
                var Comment = todoMethod.GetSingleComments(Subtaskid, userid);
                var AttachMent = todoMethod.GetSingleAttachments(Subtaskid, userid);
                if (Subtask == null)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.SubtaskDetailsModel = Subtask;
                    model.taskCommentsModel = Comment;
                    model.taskTimelinesModel = TimeLine;
                    model.taskAttachmentsModel = AttachMent;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.SubtaskDetailsModel = Subtask;
                model.taskCommentsModel = Comment;
                model.taskTimelinesModel = TimeLine;
                model.taskAttachmentsModel = AttachMent;
                return model;


            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }


        #endregion

        #region Get All Employees
        [Route("getAllEmployees")]
        [HttpGet]
        public ActionResult<TaskAssigneResponseModel> GetAllEmployees()
        {
            TaskAssigneResponseModel model = new TaskAssigneResponseModel();
            try
            {
                var res = todoMethod.EmployeeList().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.employeeModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.employeeModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region Create Edit task and subtask 

        [Route("createTask")]
        [HttpPost]
        public ActionResult<CreateTaskResponseModel> CreateTask(TodoCreateModels todoModel)
        {
            CreateTaskResponseModel model = new CreateTaskResponseModel();
            try
            {
                var res = todoMethod.CreateNewTask(todoModel);
                if (res != 0)
                {
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Id = res;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }

        [Route("editTask")]
        [HttpPost]
        public ActionResult<EditTaskResponseModel> EditTask(TodoModels todoModel)
        {
            EditTaskResponseModel model = new EditTaskResponseModel();
            try
            {
                var res = todoMethod.SaveTask(todoModel);
                if (res != 0)
                {
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Id = res;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }

        [Route("createSubTask")]
        [HttpPost]
        public ActionResult<CreateTaskResponseModel> CreateSubTask(SubtaskModel subtaskModel)
        {
            CreateTaskResponseModel model = new CreateTaskResponseModel();
            try
            {
                var res = todoMethod.CreateSubTask(subtaskModel.userId, subtaskModel.taskId, subtaskModel.name, subtaskModel.priority,
                            subtaskModel.assigndeptid, subtaskModel.assignToId, subtaskModel.deptid, subtaskModel.duedate, subtaskModel.desc);
                if (res != 0)
                {
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Id = res;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }

        [Route("editSubTask")]
        [HttpPost]
        public ActionResult<EditTaskResponseModel> EditSubTask(SubtaskModel subtaskModel)
        {
            EditTaskResponseModel model = new EditTaskResponseModel();
            try
            {
                var res = todoMethod.SaveSubTask(subtaskModel.userId, subtaskModel.taskId, subtaskModel.subtaskid, subtaskModel.name, subtaskModel.priority,
                             subtaskModel.assigndeptid, subtaskModel.assignToId, subtaskModel.deptid, subtaskModel.duedate, subtaskModel.desc, subtaskModel.status);
                if (res != 0)
                {
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Id = res;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }


        #endregion

        #region Task Comment chat - Send message

        [Route("sentTaskCommentMessage")]
        [HttpPost]

        public ActionResult<BaseModel> sentTaskCommentMessage(CommentModel commentModel)
        {
            BaseModel model = new BaseModel();

            try
            {
                TodoComment todocomments = new TodoComment();
                todocomments.Active = true;
                todocomments.Description = commentModel.comments;
                todocomments.InsertedId = commentModel.userId;
                todocomments.IsAvailable = true;
                todocomments.IsRead = false;
                todocomments.ModifiedId = commentModel.userId;
                todocomments.Status = "Create";
                todocomments.TodoID = commentModel.taskid;
                long response = toDoRepository.CreateTodoComment(todocomments);

                if (response > 0)
                {

                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;
        }


        #endregion 

        #region Remove Comment

        [Route("RemoveComment")]
        [HttpPost]

        public ActionResult<BaseModel> RemoveComment(RemoveComment removeComment)
        {
            BaseModel model = new BaseModel();

            try
            {
                TodoComment todoComment = toDoRepository.GetTodoCommentById(removeComment.taskCommentId);
                if (todoComment.InsertedId == removeComment.userId)
                {
                    todoComment.IsAvailable = false;
                    todoComment.Active = false;
                    todoComment.ModifiedId = removeComment.userId;
                    todoComment.ModifiedDate = DateTime.Now;
                    var response = toDoRepository.UpdateTodoComment(todoComment);
                    if (response == true)
                    {
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.Status = CommonText.SUCCESS;
                    }
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;
        }


        #endregion

        #region Delete Task ById

        [Route("DeleteTaskById/{id:long}")]
        [HttpPost]

        public ActionResult<BaseModel> DeleteTaskById(long id)
        {
            BaseModel model = new BaseModel();

            try
            {
                var todo = toDoRepository.GetAllTodo();
                var gettodobyid = todo.Where(a => a.ID == id).FirstOrDefault();
                if (gettodobyid.ParentId == 0)
                {
                    var childs = todo.Where(a => a.ParentId == id).ToList();
                    if (childs != null)
                    {
                        foreach (var m in childs)
                        {
                            toDoRepository.DeleteTodo(m.ID);
                        }
                    }
                }
                var response = toDoRepository.DeleteTodo(gettodobyid.ID);

                if (response == true)
                {

                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;
        }

        [Route("deleteTaskByIdAndriod")]
        [HttpPost]

        public ActionResult<BaseModel> DeleteTaskByIdAndriod(long id)
        {
            BaseModel model = new BaseModel();

            try
            {
                var todo = toDoRepository.GetAllTodo();
                var gettodobyid = todo.Where(a => a.ID == id).FirstOrDefault();
                if (gettodobyid.ParentId == 0)
                {
                    var childs = todo.Where(a => a.ParentId == id).ToList();
                    if (childs != null)
                    {
                        foreach (var m in childs)
                        {
                            toDoRepository.DeleteTodo(m.ID);
                        }
                    }
                }
                var response = toDoRepository.DeleteTodo(gettodobyid.ID);

                if (response == true)
                {

                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;
        }

        #endregion


        #region My Task List

        //Return all tasks and subtasks assigned to others for emplopyeeid
        [Route("getMyTaskList")]
        [HttpGet]

        public ActionResult<TaskAssigneResponseModel> GetMyTaskList(long accessId, long roleId, long employeeid, long[] TodoPriority, string[] TodoStatusName, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            TaskAssigneResponseModel model = new TaskAssigneResponseModel();
            try
            {

                if (commonMethods.checkaccessavailable("My Tasks", accessId, "View", "Task", roleId) == false)
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }
                else
                {
                    var res = todoMethod.GetAllMyTask(employeeid, TodoPriority, TodoStatusName,FromDate,ToDate);
                    var todoPriorities = toDoRepository.GetAllTodoPriority().Select(a => new TodoCommonModel { ID = a.ID, Name = a.Name }).ToList();
                    var todoStatues = todoMethod.GetAllStattus().ToList();
                    model.todoPriority = todoPriorities;
                    model.todoStatus = todoStatues;
                    if (res != null)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.taskAssignResponseModel = res;
                        return model;
                    }
                    else
                    {
                        model.Status = CommonText.No_Record;
                        model.StatusCode = HttpStatusCode.No_Record;
                        model.taskAssignResponseModel = res;
                        return model;
                    }
                }
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        [Route("getMyTaskListAndriod")]
        [HttpPost]

        public ActionResult<TaskAssigneResponseModel> GetMyTaskListAndriod([FromBody] myTaskModel mytask)
        {
            TaskAssigneResponseModel model = new TaskAssigneResponseModel();
            try
            {

                if (commonMethods.checkaccessavailable("My Tasks", mytask.accessId, "View", "Task", mytask.roleId) == false)
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }
                else
                {
                    var res = todoMethod.GetAllMyTask(mytask.employeeid, mytask.TodoPriority, mytask.TodoStatusName,mytask.FromDate,mytask.ToDate);
                    var todoPriorities = toDoRepository.GetAllTodoPriority().Select(a => new TodoCommonModel { ID = a.ID, Name = a.Name }).ToList();
                    var todoStatues = todoMethod.GetAllStattus().ToList();
                    model.todoPriority = todoPriorities;
                    model.todoStatus = todoStatues;
                    if (res != null)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.taskAssignResponseModel = res;
                        return model;
                    }
                    else
                    {
                        model.Status = CommonText.No_Record;
                        model.StatusCode = HttpStatusCode.No_Record;
                        model.taskAssignResponseModel = res;
                        return model;
                    }
                }
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }




        #endregion

        #region Task Dashboard

        [Route("getTaskDashboard")]
        [HttpGet]

        public ActionResult<TaskDashboardclsResponseModel> getTaskDashboard(long userId, long accessId, long roleId, string fromdate, string todate)
        {
            TaskDashboardclsResponseModel model = new TaskDashboardclsResponseModel();
            try
            {


                if (commonMethods.checkaccessavailable("Dashboard", accessId, "View", "Task", roleId) == false)
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }
                else
                {
                    var res = todoMethod.GetAllTaskDashboard(userId, accessId, roleId, fromdate, todate);
                    if (res != null)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.TaskDashboardModel = res;
                        return model;
                    }
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.TaskDashboardModel = res;
                    return model;

                }
            }
            catch (Exception ex)
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region Assign Dead line

        [Route("AssignDeadLine")]
        [HttpGet]

        public ActionResult<BaseModel> AssignDeadLine(long userId, long taskid, DateTime DeadLineDate)
        {
            BaseModel model = new BaseModel();
            try
            {

                var todo = toDoRepository.GetTodoById(taskid);
                long res = 0;

                if (DeadLineDate != null)
                {
                    if (todo.DueDate != null)
                    {
                        if (Convert.ToDateTime(DeadLineDate).ToString("dd-MMM-yyyy") != todo.DueDate.Value.ToString("dd-MMM-yyyy"))
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = userId;
                            todoTimeline.RelatedDate = DeadLineDate;
                            //  todoTimeline.RelatedId = userId;
                            //   todoTimeline.Status = todoModel.TodoStatusName;
                            todoTimeline.TodoID = taskid;
                            todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                            res = toDoRepository.CreateTodoTimeline(todoTimeline);
                        }
                    }
                    else
                    {
                        TodoTimeline todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        todoTimeline.RelatedDate = DeadLineDate;
                        // todoTimeline.RelatedId = userId;
                        // todoTimeline.Status = todoModel.TodoStatusName;
                        todoTimeline.TodoID = taskid;
                        todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                        res = toDoRepository.CreateTodoTimeline(todoTimeline);
                    }
                }


                todo.DueDate = DeadLineDate;
                todo.ModifiedId = userId;
                toDoRepository.UpdateTodo(todo);

                if (res != 0)
                {
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;

                    return model;
                }

                model.Status = CommonText.No_Record;
                model.StatusCode = HttpStatusCode.No_Record;
                return model;

            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region View All Task

        [Route("GetViewAllTaskWithFilteration")]
        [HttpGet]

        public ActionResult<ViewAllTaskResponseModel> GetViewAllTask(long userId, long accessId, long roleId)
        {
            ViewAllTaskResponseModel model = new ViewAllTaskResponseModel();
            try
            {

                if ((commonMethods.checkaccessavailable("View Tasks", accessId, "View", "Task", roleId) == false))
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }

                else
                {
                    var todoPriorities = todoMethod.GetAllTodoPriorities().ToList();
                    var todoStatues = todoMethod.GetAllStattus().ToList();
                    var empList = todoMethod.Employee();
                    var res = todoMethod.GetAllViewAllTaskWithFilteration(userId, accessId, roleId);
                    if (res != null)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.todoPriority = todoPriorities;
                        model.todoStatus = todoStatues;
                        model.EmployeeList = empList;
                        model.Taskviewmodels = res;
                        return model;
                    }
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.todoPriority = todoPriorities;
                    model.todoStatus = todoStatues;
                    model.EmployeeList = empList;
                    model.Taskviewmodels = res;
                    return model;

                }
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }



        [Route("getViewAllTask")]
        [HttpGet]

        public ActionResult<ViewAllTaskFilterationResponseModel> GetViewAllTaskWithFilteration(long userId, long accessId, long roleId, string[] TodoStatusName, long[] TodoPriority, long[] employeeid,
            long[] departmentid, long? datetype, string FromDate, string ToDate, string Month)
        {
            ViewAllTaskFilterationResponseModel model = new ViewAllTaskFilterationResponseModel();
            try
            {

                if ((commonMethods.checkaccessavailable("View Tasks", accessId, "View", "Task", roleId) == false))
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }

                else
                {

                    var todoStatues = todoMethod.GetAllStattus().ToList();
                    var res = todoMethod.GetViewAllTask(userId, accessId, roleId, TodoStatusName, TodoPriority, employeeid, departmentid, datetype, FromDate, ToDate, Month);
                    if (res != null)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.todoStatus = todoStatues;
                        model.Taskviewmodels = res;
                        return model;
                    }
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.todoStatus = todoStatues;
                    model.Taskviewmodels = res;
                    return model;

                }
            }
            catch (Exception e)
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }


        [Route("getViewAllTaskAndriod")]
        [HttpPost]

        public ActionResult<ViewAllTaskFilterationResponseModel> GetViewAllTaskWithFilterationAndriod([FromBody] allTaskModel alltask)
        {
            ViewAllTaskFilterationResponseModel model = new ViewAllTaskFilterationResponseModel();
            try
            {

                if ((commonMethods.checkaccessavailable("View Tasks", alltask.accessId, "View", "Task", alltask.roleId) == false))
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }

                else
                {

                    var todoStatues = todoMethod.GetAllStattus().ToList();
                    var priority = todoMethod.GetAllTodoPriorities().ToList();
                    var res = todoMethod.GetViewAllTask(alltask.userId, alltask.accessId, alltask.roleId, alltask.TodoStatusName, alltask.TodoPriority, alltask.employeeid, alltask.departmentid, alltask.datetype, alltask.FromDate, alltask.ToDate, alltask.Month);
                    if (res != null)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.todoStatus = todoStatues;
                        model.todoPriprity = priority;
                        model.Taskviewmodels = res;
                        return model;
                    }
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.todoStatus = todoStatues;
                    model.Taskviewmodels = res;
                    return model;

                }
            }
            catch (Exception e)
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }


        #endregion

        #region New Task All List

        [Route("getAllTodoStatusAndPriorityAndEmployeeAndDepartment")]
        [HttpGet]
        public ActionResult<NewTask> GetAllTodoStatusAndPriorityAndEmployeeAndDepartment(long? employeeId)
        {
            NewTask taskModel = new NewTask();
            try
            {
                var todoPriorities = toDoRepository.GetAllTodoPriority().Select(a => new TodoCommonModel { ID = a.ID, Name = a.Name }).ToList();
                var todoStatues = todoMethod.GetAllStattus().ToList();
                List<TodoDepartmentModel> empDept = null;
                List<commonClsModel> employees = null;
                if (employeeId != null)
                {
                    empDept = todoMethod.GetAllDepartmentListByEmployeeID(employeeId.Value).ToList();
                    employees = todoMethod.GetEmployeesForTasks(employeeId.Value).ToList();
                }

                // var allemployees = todoMethod.EmployeeList().ToList();

                taskModel.todoPriority = todoPriorities;
                taskModel.todoStatus = todoStatues;
                taskModel.employeeModel = employees;
                taskModel.todoDepartmentModel = empDept;
                taskModel.Status = CommonText.SUCCESS;
                taskModel.StatusCode = HttpStatusCode.SUCCESS;
                return taskModel;

            }
            catch (Exception e)
            {
                taskModel.Status = CommonText.FAILED;
                taskModel.StatusCode = HttpStatusCode.FAILURE;

                return taskModel;
            }
        }

        #endregion

        #region Create Attachment in todo 

        [Route("createTaskAttachment")]
        [HttpPost]
        public ActionResult<CreateTaskResponseModel> createTaskAttachment(AttachmentModel attachment)
        {
            CreateTaskResponseModel model = new CreateTaskResponseModel();
            try
            {
                var res = todoMethod.CreateNewTaskAttachment(attachment.userId, attachment.file, attachment.attachmentname, attachment.taskid);
                if (res != 0)
                {
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Id = res;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;
        }

        #endregion     

        #region  Verify Task And Subtask     

         

        [Route("verifyTaskAndSubtask")]
        [HttpPost]
        public ActionResult<VeryfyTaskAndSubTask> VerifyTaskAndSubTask(VeryfyTaskAndSubtaskModel verifymodel)
        {
            VeryfyTaskAndSubTask model = new VeryfyTaskAndSubTask();

            try
            {
                var res = todoMethod.VerifyTaskAndSubTask(verifymodel.userId, verifymodel.accessId, verifymodel.roleId, verifymodel.id, verifymodel.score);

                model.result = res;
                model.Status = CommonText.UPDATED;
                model.StatusCode = HttpStatusCode.Updated;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
            }

            return model;
        }

        #endregion

        #region task And SubTask Status Change

        [Route("taskAndSubTaskStatusChange")]
        [HttpPost]
        public ActionResult<EditTaskResponseModel> taskAndSubTaskStatusChange(TaskAndSubtaskStatusChangeModel changeModel)
        {
            EditTaskResponseModel model = new EditTaskResponseModel();
            try
            {
                var res = false;
                var subtasks = toDoRepository.GetAllTodo().Where(m => m.ParentId == changeModel.id).ToList();
                if (subtasks.Count() > 0)
                {
                    foreach (var a in subtasks)
                    {

                        TodoTimeline todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = changeModel.userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = changeModel.userId;
                        //  todoTimeline.RelatedId = userId;
                        todoTimeline.Status = changeModel.status;
                        todoTimeline.TodoID = a.ID;
                        todoTimeline.TodoStatus = "Task " + changeModel.status;
                        toDoRepository.CreateTodoTimeline(todoTimeline);
                        Todo todo = a;
                        todo.Status = changeModel.status;
                        if (changeModel.status == "COMPLETED" || changeModel.status == "VERIFY")
                        {
                            todo.CompletionDate = DateTime.Now;
                        }
                        else
                        {
                            todo.CompletionDate = null;
                        }

                        res = toDoRepository.UpdateTodo(todo);
                    }
                }
                var ab = toDoRepository.GetTodoById(changeModel.id);
                if (ab != null)
                {
                    TodoTimeline todoTimeline1 = new TodoTimeline();
                    todoTimeline1.Active = true;
                    todoTimeline1.InsertedId = changeModel.userId;
                    todoTimeline1.IsAvailable = true;
                    todoTimeline1.ModifiedId = changeModel.userId;
                    //   todoTimeline1.RelatedId = userId;
                    todoTimeline1.Status = changeModel.status;
                    todoTimeline1.TodoID = ab.ID;
                    todoTimeline1.TodoStatus = "Task " + changeModel.status;
                    toDoRepository.CreateTodoTimeline(todoTimeline1);

                    ab.Status = changeModel.status;
                    if (changeModel.status == "COMPLETED" || changeModel.status == "VERIFY")
                    {
                        ab.CompletionDate = DateTime.Now;
                    }
                    else
                    {
                        ab.CompletionDate = null;
                    }
                    res = toDoRepository.UpdateTodo(ab);
                }


                if (res == true)
                {
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Id = changeModel.id;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }

        #endregion      

        #region Get TimeLine By todoid
        [Route("getTimeLineListByTaskId")]
        [HttpGet]
        public ActionResult<TaskTimeLineResponseModel> getTimeLineListByTaskId(long taskid)
        {
            TaskTimeLineResponseModel model = new TaskTimeLineResponseModel();
            try
            {
                var TimeLine = todoMethod.GetTaskTimelines(taskid).ToList();

                if (TimeLine == null)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.taskTimelinesModel = TimeLine;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.taskTimelinesModel = TimeLine;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }
        #endregion

        #region Get Attachment By todoid
        [Route("getAttachmentListByTaskId")]
        [HttpGet]
        public ActionResult<TaskAttachmentResponseModel> GetAttachmentListByTaskId(long taskid, long userid)
        {
            TaskAttachmentResponseModel model = new TaskAttachmentResponseModel();
            try
            {
                var AttachMent = todoMethod.GetAttachments(taskid, userid).ToList();

                if (AttachMent == null)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.taskAttachmentsModel = AttachMent;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.taskAttachmentsModel = AttachMent;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }
        #endregion

        [Route("taskResolveStatus")]
        [HttpGet]
        public ActionResult<StatusResponseModel> TaskResolveStatus()
        {
            StatusResponseModel model = new StatusResponseModel();

            try
            {
                var res = todoMethod.TodoStatusModel().ToList();

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.Statuses = res;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
            }

            return model;
        }

        [Route("getTaskCommentListCountWithUserVisibility")]
        [HttpGet]
        public ActionResult<BaseModel> GetCommentListCountWithUserVisibility(long empid, long todoid)
        {
            BaseModel model = new BaseModel();
            var taskComment = toDoRepository.GetAllTodoComment().Where(a => a.TodoID == todoid && a.Active == true).ToList();
            if (taskComment != null)
            {
                var empcomment = taskComment.Where(a => a.InsertedId != empid && a.Active == true).ToList();
                foreach (var c in empcomment)
                {
                    c.IsRead = true;
                    c.ModifiedDate = DateTime.Now;
                    toDoRepository.UpdateTodoComment(c);
                }
                model.Status = "SUCCESS";
                model.StatusCode = HttpStatusCode.SUCCESS;
            }
            else
            {
                model.Status = "Failed";
                model.StatusCode = HttpStatusCode.FAILURE;
            }
            return model;
        }
       
        #region Remove Attachment 

        [Route("RemoveAttachment")]
        [HttpPost]

        public ActionResult<BaseModel> RemoveAttachment(RemoveAttachment removeattachment)
        {
            BaseModel model = new BaseModel();

            try
            {
                TodoAttachment todoatt = toDoRepository.GetTodoAttachmentById(removeattachment.taskAttachmentId);
                if (todoatt.InsertedId == removeattachment.userId)
                {
                    todoatt.IsAvailable = false;
                    todoatt.Active = false;
                    todoatt.ModifiedId = removeattachment.userId;
                    todoatt.ModifiedDate = DateTime.Now;
                    var response = toDoRepository.UpdateTodoAttachment(todoatt);
                    if (response == true)
                    {
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.Status = CommonText.SUCCESS;
                    }
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;
        }


        #endregion
       

    }

}