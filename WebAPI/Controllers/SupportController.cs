﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities.SupportAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WebAPI.Constants;
using WebAPI.Data;
using WebAPI.Models;
using static WebAPI.Models.SupportModel;
using static WebAPI.Models.TodoModel;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/support")]
    [ApiController]
    public class SupportController : ControllerBase
    {
        protected readonly ISupportRepository supportRepository;
        protected readonly SupportMethod supportMethod;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ApplicationDbContext _dbContext;
        private readonly CommonMethods commonMethods;

        public SupportController(ISupportRepository supportRepository, SupportMethod supportMethod, IHostingEnvironment hostingEnv, CommonMethods commonMethods, ApplicationDbContext _dbcontext)
        {
            this.supportRepository = supportRepository;
            this.supportMethod = supportMethod;
            hostingEnvironment = hostingEnv;
            this.commonMethods = commonMethods;
            this._dbContext = _dbcontext;
        }
        #region  support commom Api

        #region get All In dependent List

        [Route("getAllList")]
        [HttpGet]
        public ActionResult<SupportAllResponse> GetAllList()
        {
            SupportAllResponse model = new SupportAllResponse();
            try
            {
                var type = supportMethod.GetAllSupportTypeList().ToList();
                var status = supportMethod.GetAllSupportStatus().ToList();
                var priority = supportMethod.GetAllSupportPriority().ToList();
                var alltype = supportMethod.GetAllType().ToList();
                var building = supportMethod.GetAllBuilding().ToList();
                if (type.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportTypeModel = type;
                    model.supportStatusModel = status;
                    model.supportPriorityModel = priority;
                    model.allDepartmentViewModel = alltype;
                    model.buildingModel = building;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportTypeModel = type;
                model.supportStatusModel = status;
                model.supportPriorityModel = priority;
                model.allDepartmentViewModel = alltype;
                model.buildingModel = building;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region  get SupportType And Priority And Building List

        [Route("getSupportTypeAndPriorityAndBuildingList")]
        [HttpGet]
        public ActionResult<SupportAllResponse> GetSupportTypeAndPriorityAndBuildingList()
        {
            SupportAllResponse model = new SupportAllResponse();
            try
            {
                var type = supportMethod.GetAllSupportTypeList().ToList();            
                var priority = supportMethod.GetAllSupportPriority().ToList();               
                var building = supportMethod.GetAllBuilding().ToList();
                var status = supportMethod.GetAllSupportStatus().ToList();
                if (type.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportTypeModel = type;                   
                    model.supportPriorityModel = priority;                  
                    model.buildingModel = building;
                    model.supportStatusModel = status;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportTypeModel = type;               
                model.supportPriorityModel = priority;              
                model.buildingModel = building;
                model.supportStatusModel = status;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region get All get Support status And Priority List
        [Route("getSupportstatusAndPriorityList")]
        [HttpGet]
        public ActionResult<SupportAllResponse> getSupportstatusAndPriorityList()
        {
            SupportAllResponse model = new SupportAllResponse();
            try
            {               
                var status = supportMethod.GetAllSupportStatus().ToList();
                var priority = supportMethod.GetAllSupportPriority().ToList();
               
                if (status.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;                 
                    model.supportStatusModel = status;
                    model.supportPriorityModel = priority;                  
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;             
                model.supportStatusModel = status;
                model.supportPriorityModel = priority;              
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }
        #endregion

        #region get SupportTypes List

        [Route("getSupportTypes")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetSupportTypeList()
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllSupportTypeList().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportTypeModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportTypeModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region Return support status list
        [Route("getSupportStatus")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetSupportStatusList()
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllSupportStatus().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region Return support priority list

        [Route("getSupportPriority")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetSupportPriorityList()
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllSupportPriority().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }

        #endregion

        #endregion

        #region  Get Support Categories List

        [Route("getSupportCategory")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetSupportCategoryList()
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllSupportCategoriesList().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region getSupportTypes by department list
        [Route("getSupportTypeListByDepartmentId")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetSupportTypeListByDepartmentID(long departmentid)
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetSupportTypeByDepartmentID(departmentid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region Return support category list for support type id
        [Route("getSupportCategoryListBySupportTypeId")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetSupportCategoryListBySupportTypeID(long supporttypeid)
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllSupportCategoryListByTypeId(supporttypeid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }

        #endregion

        #region Return Room list for Buildingid
        [Route("getRoomByBuildingId")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetRoomByBuildingID(long buildingid)
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetRoomByBuildingID(buildingid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.roomModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.roomModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }
        #endregion

        #region  getSupportSubCategoryByCategoryId
        [Route("getSupportSubCategoryByCategoryId")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetSupportSubCategoryByCategoryId(long categoryid)
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetSupportSubCategoryListByCategoryId(categoryid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region getBuilding ByOrganizationId

        [Route("getBuildingByOrganizationId")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetBuildingByOrganizationId(long organizationId)
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetBuildingsByOrganizationId(organizationId).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.buildingModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.buildingModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }
        #endregion

        #region  getSupportRequest By EmployeeId

        [Route("getSupportRequestByEmployeeId")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetSupportRequestByEmployeeId(long employeeId, long statusId, string mstatus, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                //if (commonMethods.checkaccessavailable("Raise", accessId, "List", "Support", roleId) == false)
                //{
                //    model.Status = "MODULE_ACCESS_DENIED";
                //    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                //    return model;
                //}
                //else
                //{
                    var res = supportMethod.GetSupportRequestByEmployeeId(employeeId, statusId,mstatus,FromDate,ToDate).ToList();
                    var supportStatusList = supportMethod.GetAllSupportStatus().ToList();
                    if (res.Count == 0)
                    {
                        model.Status = CommonText.No_Record;
                        model.StatusCode = HttpStatusCode.No_Record;
                        model.supportCommonModels = supportStatusList;
                        model.supportRequestResponseModels = res;
                        return model;
                    }

                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.supportCommonModels = supportStatusList;
                    model.supportRequestResponseModels = res;
               // }               
            }
            catch(Exception ex)
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
            }

            return model;

        }

        #endregion

        #region  getSupportRequest ById

        [Route("getSupportRequestById/{id:long}")]
        [HttpGet]
        public ActionResult<SupportResponse> GetSupportRequestById(long id)

        {
            SupportResponse model = new SupportResponse();
            try
            {
                var res = supportMethod.GetSupportRequestListById(id).FirstOrDefault();
                //var res1 = supportMethod.GetAllSupportCommentsList(id).ToList();
                if (res == null)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportRequestResponseModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportRequestResponseModels = res;
                return model;
            }
            catch (Exception e)
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion 

        #region Create and Edit and Delete support raised request
        [Route("createSupportRaiseRequests")]
        [HttpPost]
        public ActionResult<SupportRequestCommonModel> CreateSupportRaiseRequests(RaiseSupportModels rsm)
        {
            SupportRequestCommonModel model = new SupportRequestCommonModel();

            try
            {
                SupportRequest request = new SupportRequest();

                request.Description = rsm.description;
                request.CategoryID = rsm.categoryid;
                request.SubCategoryID = rsm.subcategoryid;
                request.LocationID = 0;
                request.Title = rsm.title;
                request.BuildingID = rsm.buildingid;
                request.RoomID = rsm.roomid;
                request.PriorityID = rsm.priorityid;
                request.ModifiedId = rsm.userId;
                request.InsertedId = rsm.userId;
                request.RequestedCompletionDate = rsm.CompletionDate;
                request.Active = true;
                request.StatusID = 1;
                request.SupportTypeID = rsm.supporttypeid;
                request.StatusName = "PENDING";
                request.Score = 0;
                request.ScoreGivenId = 0;
                request.Status = EntityStatus.ACTIVE;
                var type = supportRepository.GetBySupportTypeId(request.SupportTypeID);
                var allrequest = supportRepository.GetAllSupportRequestWithinactive() == null ? null : supportRepository.GetAllSupportRequestWithinactive().Where(a => a.SupportTypeID == request.SupportTypeID).ToList();
                if (allrequest == null)
                {
                    request.TicketCode = type.Name.Substring(0, 2) + "-1";
                }
                else
                {
                    request.TicketCode = type.Name.Substring(0, 2) + "-" + (allrequest.ToList().Count + 1);
                }
                long response = supportRepository.CreateSupportRequest(request);
                model.ID = request.ID;

                if (response > 0)
                {
                    SupportTimeLine supportTimeLine = new SupportTimeLine();
                    supportTimeLine.InsertedId = rsm.userId;
                    supportTimeLine.ModifiedId = rsm.userId;
                    supportTimeLine.Active = true;
                    supportTimeLine.Status = "PENDING";
                    supportTimeLine.SupportRequestID = model.ID;
                    supportTimeLine.SupportStatusID = 1;
                    supportRepository.CreateSupportTimeLine(supportTimeLine);

                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;

                    //if (rsm.files != null)
                    //{
                    //    SupportAttachment Attachment = new SupportAttachment();
                    //    Attachment.Active = true;
                    //    Attachment.Type = "RAISED";
                    //    if (rsm.files[0] != null)
                    //    {
                    //        // Create a File Info 
                    //        FileInfo fi = new FileInfo(rsm.files[0].FileName);
                    //        var newFilename = "File" + "_" + String.Format("{0:d}",
                    //                          (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    //        var webPath = hostingEnvironment.WebRootPath;
                    //        string path = Path.Combine("", webPath + @"wwwroot\ODMImages\Tempfile\" + newFilename);
                    //        var pathToSave = newFilename;
                    //        using (var stream = new FileStream(path, FileMode.Create))
                    //        {
                    //            rsm.files[0].CopyToAsync(stream);
                    //        }
                    //        Attachment.Path = pathToSave;
                    //        Attachment.Name = "File";
                    //    }

                    //    Attachment.InsertedId = rsm.userId;
                    //    Attachment.IsAvailable = true;
                    //    Attachment.ModifiedId = rsm.userId;
                    //    Attachment.Status = EntityStatus.ACTIVE;
                    //    Attachment.SupportRequestID = model.ID;
                    //    long response2 = supportRepository.CreateAttachment(Attachment);
                    //}


                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }


        [Route("editSupportRaiseRequests")]
        [HttpPost]
        public ActionResult<SupportRequestCommonModel> EditSupportRaiseRequests(SupportRaiseModels editraise)
        {
            SupportRequestCommonModel model = new SupportRequestCommonModel();

            try
            {
                SupportRequest request = supportRepository.GetBySupportRequestId(editraise.supportrequestid);

                request.Description = editraise.description;
                request.CategoryID = editraise.categoryid;
                request.SubCategoryID = editraise.subcategoryid;
                request.BuildingID = editraise.buildingid;
                request.RoomID = editraise.roomid;
                request.PriorityID = editraise.priorityid;
                request.ModifiedId = editraise.userId;
                request.RequestedCompletionDate = editraise.CompletionDate;
                request.SupportTypeID = editraise.supporttypeid;
                request.Title = editraise.titel;
                bool response = supportRepository.UpdateSupportRequest(request);
                if (response == true)
                {
                    model.StatusCode = HttpStatusCode.Updated;
                    model.Status = CommonText.UPDATED;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }

        [Route("supportRequestDelete")]
        [HttpPost]

        public ActionResult<SupportCommentModel> SupportRequestDelete(commonDelete delete)
        {
            SupportCommentModel model = new SupportCommentModel();

            try
            {
                bool response = supportRepository.DeleteSupportRequest(delete.id);
                if (response == true)
                {
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;

                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }

        #endregion

        #region Management approval status update 
        [Route("updateManagementApprovalStatus")]
        [HttpPost]
        public ActionResult<EmployeeUpdateCommonModel> UpdateManagementApprovalStatus(updateManagementApprovalStatus approvalStatus)
        {
            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();

            try
            {
                SupportRequest request = supportRepository.GetBySupportRequestId(approvalStatus.supportrequestid);

                request.ManagementApprovedDate = DateTime.Now;
                request.ManagementApprovalID = approvalStatus.userId;
                request.ManagementApproval = true;
                request.Comment = null;

                request.ManagementReject = false;
                request.ManagementRejectDate = null;
                request.ManagementRejectedID = 0;
                bool response = supportRepository.UpdateSupportRequest(request);
                if (response == true)
                {
                    SupportTimeLine supportTimeLine = new SupportTimeLine();
                    supportTimeLine.SupportRequestID = approvalStatus.supportrequestid;
                    supportTimeLine.InsertedId = approvalStatus.userId;
                    supportTimeLine.ModifiedId = approvalStatus.userId;
                    supportTimeLine.Active = true;
                    supportTimeLine.SupportStatusID = 0;
                    supportTimeLine.Status = "MANAGEMENT APPROVED";
                    supportRepository.CreateSupportTimeLine(supportTimeLine);

                    model.StatusCode = HttpStatusCode.Updated;
                    model.Status = CommonText.UPDATED;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }



        #endregion

        #region Support request status update
        [Route("updateSupportrequeststatus")]
        [HttpPost]
        public ActionResult<EmployeeUpdateCommonModel> UpdateSupportrequeststatus(updateSupportRequestStatus requestStatus)
        {

            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();
            var supportStatusList = supportRepository.GetByStatusId(requestStatus.statusid);

            string StatusName = supportRepository.GetByStatusId(requestStatus.statusid).Name;

            try
            {
                SupportRequest support = supportRepository.GetBySupportRequestId(requestStatus.supportrequestid);
                support.StatusID = requestStatus.statusid;
                support.StatusName = supportStatusList.Name;
                support.ModifiedId = requestStatus.userId;
                support.ModifiedDate = DateTime.Now;
                support.CompletionDate = DateTime.Now;
                support.Comment = requestStatus.reason;
                bool response = supportRepository.UpdateSupportRequest(support);
                if (response == true)
                {
                    SupportTimeLine supportTimeLine = new SupportTimeLine();
                    supportTimeLine.InsertedId = requestStatus.userId;
                    supportTimeLine.ModifiedId = requestStatus.userId;
                    supportTimeLine.Active = true;
                    supportTimeLine.Status = StatusName;
                    supportTimeLine.comment = null;
                    supportTimeLine.SupportRequestID = requestStatus.supportrequestid;
                    supportTimeLine.SupportStatusID = requestStatus.statusid;
                    supportRepository.CreateSupportTimeLine(supportTimeLine);

                    model.StatusCode = HttpStatusCode.Updated;
                    model.Status = CommonText.UPDATED;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }

        #endregion

        #region  getSupportRequestBy SupportCategoryId StatusId PriorityId
        [Route("getSupportRequestByEmployeeIdSupportCategoryIdStatusIdPriorityId")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetSupportRequestByEmployeeIdSupportCategoryIdStatusIdPriorityId([FromQuery]SupportRequestModel requestModel)
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetSupportRequestListBySupportCategoryIdStatusIdPriorityId(requestModel).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportRequestResponseModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportRequestResponseModels = res;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
            }

            return model;
        }

        #endregion

        #region  Support request send to management approval and support request verify


        [Route("supportRequestSendToManagementApproval")]
        [HttpPost]
        public ActionResult<EmployeeUpdateCommonModel> SupportRequestSendToManagementApproval(supportRequestSendToManagementApproval supportRequest)
        {
            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();

            try
            {
                SupportRequest support = supportRepository.GetBySupportRequestId(supportRequest.supportrequestid);
                support.SentApprovalID = supportRequest.userId;
                support.SendApproval = true;
                support.SentApprovalDate = DateTime.Now;
                bool response = supportRepository.UpdateSupportRequest(support);


                //support.ManagementApprovedDate = DateTime.Now;
                //support.ManagementApprovalID = supportRequest.userId; ;
                //support.ManagementApproval = true;
                //support.Comment = null;

                //support.ManagementReject = false;
                //support.ManagementRejectDate = null;
                //support.ManagementRejectedID = 0;


                //supportRepository.UpdateSupportRequest(support);

                //SupportTimeLine supportTimeLine = new SupportTimeLine();
                //supportTimeLine.SupportRequestID = support.ID;
                //supportTimeLine.InsertedId = supportRequest.userId;
                //supportTimeLine.ModifiedId = supportRequest.userId;
                //supportTimeLine.Active = true;
                //supportTimeLine.SupportStatusID = 0;
                //supportTimeLine.Status = "MANAGEMENT APPROVED";
                //supportRepository.CreateSupportTimeLine(supportTimeLine);
                //model.StatusCode = HttpStatusCode.Updated;
                //model.Status = CommonText.UPDATED;
                if (response == true)
                {
                    SupportTimeLine supportTimeLine = new SupportTimeLine();
                    supportTimeLine.SupportRequestID = supportRequest.supportrequestid;
                    supportTimeLine.InsertedId = supportRequest.userId;
                    supportTimeLine.ModifiedId = supportRequest.userId;
                    supportTimeLine.Active = true;
                    supportTimeLine.Status = "SENT FOR MANAGEMENT APPROVAL";
                    supportRepository.CreateSupportTimeLine(supportTimeLine);

                    model.StatusCode = HttpStatusCode.Updated;
                    model.Status = CommonText.UPDATED;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }


        [Route("supportRequestVerify")]
        [HttpPost]
        public ActionResult<EmployeeUpdateCommonModel> SupportRequestVerify(supportRequestVerify verify)
        {
            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();

            try
            {
                SupportRequest request = supportRepository.GetBySupportRequestId(verify.supportrequestid);
                request.ApprovedID = verify.userId;
                request.ApproveDate = DateTime.Now;
                request.ModifiedDate = DateTime.Now;
                request.IsApprove = true;
                request.StatusID = 6;
                request.StatusName = "VERIFIED";
                request.Score = verify.Score;
                request.ScoreGivenDate = DateTime.Now;
                request.ScoreGivenId = verify.userId;
                bool response = supportRepository.UpdateSupportRequest(request);

                if (response == true)
                {
                    SupportTimeLine supportTimeLine = new SupportTimeLine();
                    supportTimeLine.SupportRequestID = verify.supportrequestid;
                    supportTimeLine.Status = "VERIFIED";
                    supportTimeLine.SupportStatusID = 6;
                    supportTimeLine.ModifiedDate = DateTime.Now;
                    supportTimeLine.InsertedDate = DateTime.Now;
                    supportTimeLine.InsertedId = verify.userId;
                    supportTimeLine.ModifiedId = verify.userId;
                    supportTimeLine.Active = true;
                    supportRepository.CreateSupportTimeLine(supportTimeLine);

                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }

        [Route("supportRequestVerifyWithScoore")]
        [HttpPost]
        public ActionResult<EmployeeUpdateCommonModel> SupportRequestVerifyWithScoore(supportRequestVerify verify)
        {
            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();

            try
            {
                SupportRequest request = supportRepository.GetBySupportRequestId(verify.supportrequestid);
                if (request.StatusName == "VERIFIED" && request.StatusID == 6)
                {
                    request.Score = verify.Score;
                    request.ScoreGivenDate = DateTime.Now;
                    request.ScoreGivenId = verify.userId;
                    bool response = supportRepository.UpdateSupportRequest(request);
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                             

                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }


        #endregion

        #region Support request remove from management approval

        [Route("supportRequestRejectToManagementApproval")]
        [HttpPost]

        public ActionResult<EmployeeUpdateCommonModel> SupportRequestRejectToManagementApproval(supportRequestRejectToManagementApproval rejectApproval)
        {
            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();

            try
            {
                SupportRequest request = supportRepository.GetBySupportRequestId(rejectApproval.supportrequestid);
                request.ModifiedId = rejectApproval.userId;
                request.ManagementRejectedID = rejectApproval.userId;
                request.ManagementRejectDate = DateTime.Now;
                request.ManagementReject = true;
                request.ManagementApprovalID = 0;
                request.ManagementApprovedDate = null;
                request.ManagementApproval = false;
                request.Comment = rejectApproval.reason;
                bool response = supportRepository.UpdateSupportRequest(request);

                if (response == true)
                {
                    SupportTimeLine supportTimeLine = new SupportTimeLine();
                    supportTimeLine.InsertedId = rejectApproval.userId;
                    supportTimeLine.ModifiedId = rejectApproval.userId;
                    supportTimeLine.Active = true;
                    supportTimeLine.Status = "MANAGEMENT REJECTED";
                    supportTimeLine.comment = rejectApproval.reason;
                    supportTimeLine.SupportRequestID = rejectApproval.supportrequestid;
                    supportRepository.CreateSupportTimeLine(supportTimeLine);

                    model.StatusCode = HttpStatusCode.Updated;
                    model.Status = CommonText.UPDATED;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }
        #endregion

        #region No Need For Approval

        [Route("EditFromManagement")]
        [HttpPost]

        public ActionResult<EmployeeUpdateCommonModel> EditFromManagement(editFromManagement management)
        {
            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();

            try
            {
                SupportRequest request = supportRepository.GetBySupportRequestId(management.supportrequestid);
                request.SentApprovalID = 0;
                request.SentApprovalDate = null;

                request.SendApproval = false;

                bool response = supportRepository.UpdateSupportRequest(request);

                if (response == true)
                {
                    SupportTimeLine supportTimeLine = new SupportTimeLine();
                    supportTimeLine.InsertedId = management.userId;
                    supportTimeLine.ModifiedId = management.userId;
                    supportTimeLine.Active = true;
                    supportTimeLine.Status = "TOOK MANAGEMENT APPROVAL BACK";
                    supportTimeLine.SupportRequestID = management.supportrequestid;
                    supportRepository.CreateSupportTimeLine(supportTimeLine);

                    model.StatusCode = HttpStatusCode.Updated;
                    model.Status = CommonText.UPDATED;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }
        #endregion

        #region Assign deadline date to the support request

        [Route("assignDeadlineDateToTheSupportRequest")]
        [HttpPost]

        public ActionResult<EmployeeUpdateCommonModel> AssignDeadlineDateToTheSupportRequest(assignDeadlineToSupportRequest assignDeadline)
        {
            EmployeeUpdateCommonModel model = new EmployeeUpdateCommonModel();

            try
            {
                SupportRequest request = supportRepository.GetBySupportRequestId(assignDeadline.supportrequestid);

                request.DueDate = assignDeadline.requestedcompletiondate;
                bool response = supportRepository.UpdateSupportRequest(request);

                if (response == true)
                {
                    model.StatusCode = HttpStatusCode.Updated;
                    model.Status = CommonText.UPDATED;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }

        #endregion

        #region Support request chat - Send message

        [Route("supportRequestChatSendmessage")]
        [HttpPost]

        public ActionResult<SupportCommentModel> SupportRequestChatSendmessage(requestChatMessage chatMessage)
        {
            SupportCommentModel model = new SupportCommentModel();

            try
            {
                SupportComment comment = new SupportComment();
                comment.Active = true;
                comment.Name = chatMessage.comments;
                comment.IsRead = false;
                comment.InsertedId = chatMessage.userId;
                comment.IsAvailable = true;
                comment.ModifiedId = chatMessage.userId;
                comment.Status = "Create";
                comment.SupportRequestID = chatMessage.supportrequestid;
                long response = supportRepository.CreateSupportComment(comment);
                var SupportCommentid = comment.ID;

                if (response > 0)
                {
                    model.SupportCommentID = SupportCommentid;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }
        #endregion

        #region  Support request chat - Delete message
        [Route("supportRequestChatDeletemessage")]
        [HttpPost]

        public ActionResult<SupportCommentModel> SupportRequestChatDeletemessage(commonDelete delete)
        {
            SupportCommentModel model = new SupportCommentModel();

            try
            {
                bool response = supportRepository.DeleteSupportComment(delete.id);
                if (response == true)
                {
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;

                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }
        #endregion

        #region Support request attachment- add attachment
        [Route("supportRequestAddAttachment")]
        [HttpPost]

        public ActionResult<SupportAttachmentModel> SupportRequestAddAttachment(addAttachment attachment)
        {
            SupportAttachmentModel model = new SupportAttachmentModel();

            try
            {
                SupportAttachment Attachment = new SupportAttachment();
                Attachment.Active = true;
                Attachment.Type = "RAISED";
                if (attachment.file != null)
                {
                    // Create a File Info 
                    FileInfo fi = new FileInfo(attachment.file.FileName);
                    var newFilename = String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"E:/Website Data/tracq.odmps.org/wwwroot/ODMImages/Tempfile/" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        attachment.file.CopyTo(stream);
                    }
                    Attachment.Path = pathToSave;
                    Attachment.Name = newFilename;
                }

                Attachment.InsertedId = attachment.userId;
                Attachment.IsAvailable = true;
                Attachment.ModifiedId = attachment.userId;
                Attachment.Status = EntityStatus.ACTIVE;
                Attachment.SupportRequestID = attachment.supportrequestid;
                long response = supportRepository.CreateAttachment(Attachment);
                model.SupportAttachmentID = Attachment.ID;
                if (response > 0)
                {
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;

                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }
        #endregion

        #region Support request attachment-  delete attachment 

        [Route("supportRequestDeleteAttachment")]
        [HttpPost]

        public ActionResult<SupportAttachmentModel> SupportRequestDeleteAttachment(commonDelete delete)
        {

            List<SupportAttachmentModel> attachments = new List<SupportAttachmentModel>();
            SupportAttachmentModel model = new SupportAttachmentModel();

            try
            {

                //SupportAttachmentModel re = attachments.Where(a => a.SupportAttachmentID == supportattachmentid).FirstOrDefault();
                //re.isremove = true;
                bool response = supportRepository.DeleteAttachment(delete.id);


                if (response == true)
                {
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;

                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.EXCEPTION;
                model.Status = CommonText.EXCEPTION;
                model.Message = e.Message;
            }

            return model;

        }
        #endregion

        #region  Get Support TimeLines By Support Request Id

        [Route("getSupportTimeLineBySupportRequestId")]
        [HttpGet]
        public ActionResult<SupportAllBaseResponse> GetSupportTimeLineBySupportRequestId(long supportrequestid)
        {
            SupportAllBaseResponse model = new SupportAllBaseResponse();
            try
            {
                var res1 = supportMethod.GetSupportRequestListById(supportrequestid).ToList();
                var res = supportMethod.GetAllSupportTimeLineList(supportrequestid).ToList();
                if (res.Count == 0 && res1.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestDetailsModels = res;
                }
                else if (res.Count != 0 && res1.Count == 0)
                {
                    model.Status = CommonText.NotFound;
                    model.StatusCode = HttpStatusCode.NotFound;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestDetailsModels = res;
                }
                else if (res.Count == 0 && res1.Count != 0)
                {
                    model.Status = CommonText.RecordNotFound;
                    model.StatusCode = HttpStatusCode.RecordNotFound;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestDetailsModels = res;
                }
                else
                {
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestDetailsModels = res;
                }

                return model;

            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region  Get Support Comments By Support Request Id

        [Route("geSupportCommentsBySupportRequestId")]
        [HttpGet]
        public ActionResult<SupportTimeLineBaseResponse> GetSupportCommentsBySupportRequestId(long supportrequestid, long userid)
        {
            SupportTimeLineBaseResponse model = new SupportTimeLineBaseResponse();
            try
            {
                var res = supportMethod.GetAllSupportCommentsList(supportrequestid, userid).ToList();
                var res1 = supportMethod.GetSupportRequestListById(supportrequestid).FirstOrDefault();
                if (res.Count == 0 && res1 == null)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestDetailsModels = res;
                }
                else if (res.Count != 0 && res1 == null)
                {
                    model.Status = CommonText.NotFound;
                    model.StatusCode = HttpStatusCode.NotFound;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestDetailsModels = res;
                }
                else if (res.Count == 0 && res1 != null)
                {
                    model.Status = CommonText.RecordNotFound;
                    model.StatusCode = HttpStatusCode.RecordNotFound;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestDetailsModels = res;
                }
                else
                {
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestDetailsModels = res;
                }
                return model;

            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region  Get Support Attachments By Support Request Id
        [Route("getSupportAttachmentsBySupportRequestId")]
        [HttpGet]
        public ActionResult<SupportTimeLineBaseResponse> GetSupportAttachmentsBySupportRequestId(long supportrequestid, long userid)
        {
            SupportTimeLineBaseResponse model = new SupportTimeLineBaseResponse();
            try
            {
                var res1 = supportMethod.GetSupportRequestListById(supportrequestid).FirstOrDefault();
                var res = supportMethod.GetAllSupportAttachmentsList(supportrequestid, userid).ToList();
                if (res.Count == 0 && res1 == null)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestAttachmentModels = res;
                }
                else if (res.Count != 0 && res1 == null)
                {
                    model.Status = CommonText.NotFound;
                    model.StatusCode = HttpStatusCode.NotFound;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestAttachmentModels = res;
                }
                else if (res.Count == 0 && res1 != null)
                {
                    model.Status = CommonText.RecordNotFound;
                    model.StatusCode = HttpStatusCode.RecordNotFound;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestAttachmentModels = res;
                }
                else
                {
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.supportRequestResponseModels = res1;
                    model.supportRequestAttachmentModels = res;
                }

                return model;

            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion

        #region Return Buildings list

        [Route("getBuildings")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> GetgetBuildingList()
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllBuilding().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }

        #endregion

        #region  getSupportRequest ById

        [Route("getAllSupportRequestDetailsBySupportRequestById")]
        [HttpGet]
        public ActionResult<SupportAllSingleResponse> GetAllSupportRequestDetailsBySupportRequestById(long supportrequestid, long userid)
        {
            SupportAllSingleResponse model = new SupportAllSingleResponse();
            try
            {
                var res = supportMethod.GetSupportRequestListByIds(supportrequestid,userid).FirstOrDefault();
                var comment = supportMethod.GetSingleSupportComments(supportrequestid, userid);
                var attachment = supportMethod.GetAllSupportAttachmentsList(supportrequestid, userid).ToList();
                var timeline = supportMethod.GetSingleSupportTimeLine(supportrequestid, userid);
                if (res == null && attachment.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.SupportRequestDetailsModel = res;
                    model.supportRequestCommentModels = comment;
                    model.supportRequestAttachmentModels = attachment;
                    model.supportRequestTimeLineModels = timeline;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.SupportRequestDetailsModel = res;
                model.supportRequestCommentModels = comment;
                model.supportRequestAttachmentModels = attachment;
                model.supportRequestTimeLineModels = timeline;
                return model;
            }
            catch (Exception e)
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        #endregion 

        #region Support Resolve

        [Route("getSupportResolveList")]
        [HttpGet]
        public ActionResult<SupportBaseResolve> GetSupportResolveList(long roleId, long userId, long accessId, string Type, long[] OrgId,
            long[] DeptId, long[] SupportTypeId, long[] StatusId, long[] PriorityId, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate, string mstatus, long deptemp)
        {
            SupportBaseResolve model = new SupportBaseResolve();
            try
            {
                var supportAssign = _dbContext.SupportRequestAssign.Where(a => a.EmployeeId == userId && a.Active == true).FirstOrDefault();
                if (supportAssign == null)
                {
                    if (commonMethods.checkaccessavailable("Resolve", accessId, "Status Update", "Support", roleId) == false)
                    {
                        model.Status = "MODULE_ACCESS_DENIED";
                        model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    }
                    else
                    {
                        var res = supportMethod.SupportResolve(roleId, userId, accessId, Type, OrgId, DeptId, SupportTypeId, StatusId, PriorityId, FromDate, ToDate, mstatus, deptemp, supportAssign);
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.supportResponseModels = res;
                        model.IsEmployee = false;
                    }
                }
                else
                {
                    var res = supportMethod.SupportResolve(roleId, userId, accessId, Type, OrgId, DeptId, SupportTypeId, StatusId, PriorityId, FromDate, ToDate, mstatus, deptemp, supportAssign);
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.supportResponseModels = res;
                    model.IsEmployee = true;
                }
                //if (res == 0)
                //{
                //    model.Status = CommonText.No_Record;
                //    model.StatusCode = HttpStatusCode.No_Record;
                //    model.supportCommonModels = res;
                //    return model;
                //}



                return model;
            }
            catch(Exception e1)
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }

        [Route("getSupportResolveListAndroid")]
        [HttpPost]
        public ActionResult<SupportBaseResolve> GetSupportResolveListAndroid([FromBody]SupportforAndroid supportforAndroid)
        {
            SupportBaseResolve model = new SupportBaseResolve();
            try
            {
                var supportAssign = _dbContext.SupportRequestAssign.Where(a => a.EmployeeId == supportforAndroid.userId && a.Active == true).FirstOrDefault();
                if (supportAssign == null)
                {
                    if (commonMethods.checkaccessavailable("Resolve", supportforAndroid.accessId, "Status Update", "Support", supportforAndroid.roleId) == false)
                    {
                        model.Status = "MODULE_ACCESS_DENIED";
                        model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    }
                    else
                    {
                        var res = supportMethod.SupportResolve(supportforAndroid.roleId, supportforAndroid.userId, supportforAndroid.accessId, supportforAndroid.Type, supportforAndroid.OrgId, supportforAndroid.DeptId, supportforAndroid.SupportTypeId, supportforAndroid.StatusId, supportforAndroid.PriorityId, supportforAndroid.FromDate, supportforAndroid.ToDate, supportforAndroid.mstatus,supportforAndroid.deptemp, supportAssign);
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.supportResponseModels = res;
                        model.IsEmployee = false;
                    }
                }
                else
                {
                    var res = supportMethod.SupportResolve(supportforAndroid.roleId, supportforAndroid.userId, supportforAndroid.accessId, supportforAndroid.Type, supportforAndroid.OrgId, supportforAndroid.DeptId, supportforAndroid.SupportTypeId, supportforAndroid.StatusId, supportforAndroid.PriorityId, supportforAndroid.FromDate, supportforAndroid.ToDate, supportforAndroid.mstatus,supportforAndroid.deptemp, supportAssign);
                    model.Status = CommonText.SUCCESS;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.supportResponseModels = res;
                    model.IsEmployee = true;
                }
                //if (res == 0)
                //{
                //    model.Status = CommonText.No_Record;
                //    model.StatusCode = HttpStatusCode.No_Record;
                //    model.supportCommonModels = res;
                //    return model;
                //}



                return model;
            }
            catch (Exception e1)
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }
        
        #endregion

        #region resolve all dropdown get SupportTypes List

        [Route("getAllTypeList")]
        [HttpGet]
        public ActionResult<SupportResolveResponse> getAllTypeList()
        {
            SupportResolveResponse model = new SupportResolveResponse();
            try
            {
                var res = supportMethod.GetAllType().ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.AllDepartmentViewModel = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.AllDepartmentViewModel = res;


                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }


        }



        [Route("getAllOrganizationListByType")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> getAllOrganizationListByType(string Type)
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllOrganisationByTYpe(Type).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;


                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }


        }



        [Route("getAllDepartmentsListByOrganisation")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> getAllDepartmentsListByOrganisation(string Type, long[] Organizationid)
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllDepartmentById(Type, Organizationid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }

        [Route("getAllSupportTypeByDepartmentId")]
        [HttpGet]
        public ActionResult<SupportBaseResponse> getSupportTypeByDepartmentId(long[] departmenttid)
        {
            SupportBaseResponse model = new SupportBaseResponse();
            try
            {
                var res = supportMethod.GetAllSupportTypeByDepartmentId(departmenttid).ToList();
                if (res.Count == 0)
                {
                    model.Status = CommonText.No_Record;
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.supportCommonModels = res;
                    return model;
                }

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportCommonModels = res;
                return model;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }

        }




        #endregion


        #region //Get Employee Address By Access ID 
        [Route("getSupportManagementApprovalList")]
        [HttpGet]
        public ActionResult<SupportBaseResolve> getSupportManagementApprovalList(long userId, long roleId,     
            long accessId, string Type, long[] OrgId, long[] DeptId, long[] SupportTypeId, long[] StatusId, long[] PriorityId,string ManagementStatus, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate,long? deptemp)
        {
            SupportBaseResolve model = new SupportBaseResolve();
            if (commonMethods.checkaccessavailable("Management Approval", accessId, "List", "Support", roleId) == false)
            {
                model.Status = "MODULE_ACCESS_DENIED";
                model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;

                return model;
            }
            else
            {
                var res = supportMethod.SupportManagementApproval(userId, roleId, accessId, Type, OrgId, DeptId, SupportTypeId, StatusId, PriorityId, ManagementStatus, FromDate, ToDate,deptemp);

                model.Status = "SUCCESS";
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.SupportStatus = supportMethod.GetAllSupportStatus().ToList();
                model.ManagementStatus = supportMethod.ManagementStatus().ToList();
                model.SupportPriority = supportMethod.GetAllSupportPriority().ToList();
                model.AllDepartmentViewModel = supportMethod.GetAllType().ToList();
                model.supportResponseModels = res;
                return model;
            }
        }


        [Route("getSupportManagementApprovalListAndroid")]

        [HttpPost]
        public ActionResult<SupportBaseResolve> getSupportManagementApprovalListAndroid([FromBody]SupportforAndroid supportforAndroid)
        {
            SupportBaseResolve model = new SupportBaseResolve();
            if (commonMethods.checkaccessavailable("Management Approval", supportforAndroid.accessId, "List", "Support", supportforAndroid.roleId) == false)
            {
                model.Status = "MODULE_ACCESS_DENIED";
                model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;

                return model;
            }
            else
            {
                var res = supportMethod.SupportManagementApproval(supportforAndroid.userId, supportforAndroid.roleId, supportforAndroid.accessId, supportforAndroid.Type, supportforAndroid.OrgId, supportforAndroid.DeptId, supportforAndroid.SupportTypeId, supportforAndroid.StatusId, supportforAndroid.PriorityId,supportforAndroid.ManagementStatus,supportforAndroid.FromDate,supportforAndroid.ToDate,supportforAndroid.deptemp);

                model.Status = "SUCCESS";
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.SupportStatus = supportMethod.GetAllSupportStatus().ToList();
                model.ManagementStatus= supportMethod.ManagementStatus().ToList();
                model.SupportPriority = supportMethod.GetAllSupportPriority().ToList();
                model.AllDepartmentViewModel = supportMethod.GetAllType().ToList();
                model.supportResponseModels = res;
                return model;
            }
        }

        #endregion

        #region support Chart
        [Route("getAllSupportChartReportList")]
        [HttpGet]
        public ActionResult<SupportChartOptionsResponse> GetAllSupportChartReportList([FromQuery]SupportReportChartViewModel model1)
        {
            SupportChartOptionsResponse model = new SupportChartOptionsResponse();
            try
            {

                if (commonMethods.checkaccessavailable("Report", model1.accessId, "List", "Support", model1.roleId) == false)
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }

                else
                {
                    var res = supportMethod.PrepareReportData(model1);
                    if (res != null)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.SupportChartReportDetailsModel = res;
                        return model;
                    }
                    else
                    {
                        model.Status = CommonText.No_Record;
                        model.StatusCode = HttpStatusCode.No_Record;
                        model.SupportChartReportDetailsModel = res;
                        return model;
                    }                   
                }
            }
            catch (Exception ex)
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }

        [Route("getAllSupportChartReportListAndroid")]
        [HttpPost]
        public ActionResult<SupportChartOptionsResponse> GetAllSupportChartReportListAndroid([FromBody]SupportReportChartViewModel model1)
        {
            SupportChartOptionsResponse model = new SupportChartOptionsResponse();
            try
            {

                if (commonMethods.checkaccessavailable("Report", model1.accessId, "List", "Support", model1.roleId) == false)
                {
                    model.Status = "MODULE_ACCESS_DENIED";
                    model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
                    return model;
                }

                else
                {
                    var res = supportMethod.PrepareReportData(model1);
                    if (res != null)
                    {
                        model.Status = CommonText.SUCCESS;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.SupportChartReportDetailsModel = res;
                        return model;
                    }
                    else
                    {
                        model.Status = CommonText.No_Record;
                        model.StatusCode = HttpStatusCode.No_Record;
                        model.SupportChartReportDetailsModel = res;
                        return model;
                    }
                }
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
                return model;
            }
        }




        #endregion

        [Route("getSupportTypeByEmployeeId")]
        [HttpGet]
        public ActionResult<SupportTypeResponse> GetSupportTypeByEmployeeId(long empid)
        {
            SupportTypeResponse model = new SupportTypeResponse();
            var res = supportMethod.SupportTypeByEmployeeId(empid);
            if (res != null)
            {
                model.Status = "SUCCESS";
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.supportType = res.ToList();
            }
            else
            {
                model.Status = "Failed";
                model.StatusCode = HttpStatusCode.FAILURE;
            }

            return model;
            
        }
        #region get Management Status
        [Route("getManagementStatus")]
        [HttpGet]
        public ActionResult<StatusResponseModel> GetManagementStatus()
        {
            StatusResponseModel model = new StatusResponseModel();

            try
            {
                var res = supportMethod.ManagementStatus().ToList();

                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
                model.Statuses = res;
            }
            catch
            {
                model.Status = CommonText.FAILED;
                model.StatusCode = HttpStatusCode.FAILURE;
            }

            return model;
        }

        #endregion

        [Route("getCommentListCountWithUserVisibility")]
        [HttpPost]
        public ActionResult<BaseModel> GetCommentListCountWithUserVisibility(long empid,long requestid)
        {
            BaseModel model = new BaseModel();
            var supportComment = supportRepository.GetAllSupportComment().Where(a => a.SupportRequestID == requestid && a.Active == true).ToList();
            if (supportComment != null)
            {
                var empcomment = supportComment.Where(a => a.InsertedId != empid && a.Active == true).ToList();
                foreach (var c in empcomment)
                {
                    c.IsRead = true;
                    c.ModifiedDate = DateTime.Now;
                    supportRepository.UpdateSupportComment(c);
                }
                model.Status = "SUCCESS";
                model.StatusCode = HttpStatusCode.SUCCESS;
            }
            else 
            {
                model.Status = "Failed";
                model.StatusCode = HttpStatusCode.FAILURE;
            }
            return model;
        }

        #region Support Assign
        [Route("getAllDepartmentEmployee")]
        [HttpGet]
        public ActionResult<DepartmentEmployeeResponse> GetAllDepartmentEmloyee(long empid)
        {
            DepartmentEmployeeResponse employeeResponse = new DepartmentEmployeeResponse();
            try
            {
                var emp = supportMethod.GetallEmployeeByDeptId(empid).ToList();
                if(emp != null)
                {
                    employeeResponse.DeptEmp = emp;
                    employeeResponse.Status = "Success";
                    employeeResponse.StatusCode = HttpStatusCode.SUCCESS;
                }
                else
                {
                    employeeResponse.Status = "No record found";
                    employeeResponse.StatusCode = HttpStatusCode.RecordNotFound;
                }
            }
            catch (Exception)
            {
                employeeResponse.Status = "Failed";
                employeeResponse.StatusCode = HttpStatusCode.FAILURE;
            }

            return employeeResponse;
        }

        [Route("assignRequestToEmployee")]
        [HttpPost]
        public ActionResult<BaseModel> AssignRequestToEmployee(AssignRequestToEmployee assignRequest)
        {
            BaseModel model = new BaseModel();
            try
            {
                var result = supportMethod.AssignRequestToEmployee(assignRequest);
                if(result == true)
                {
                    model.Status = "Success";
                    model.StatusCode = HttpStatusCode.SUCCESS;
                }
                else
                {
                    model.Status = "No record found";
                    model.StatusCode = HttpStatusCode.RecordNotFound;
                }
            }
            catch (Exception)
            {
                model.Status = "Failed";
                model.StatusCode = HttpStatusCode.FAILURE;
            }

            return model;
        }
        #endregion
        //[Route("createSupportRequest")]
        //[HttpGet]
        //public ActionResult<CreateSupportRequestModel> CreateSupportRequest(long? supporttypeid, long? categoryid, long? buildingid)
        //{
        //    CreateSupportRequestModel model = new CreateSupportRequestModel();

        //    try
        //    {
        //        var supportType = supportMethod.GetAllSupportTypeList().ToList(); 
        //        var supportProrityList = supportMethod.GetAllSupportPriority().ToList();
        //        var getBuildingList = supportMethod.GetAllBuilding().ToList(); 

        //        List<SupportCommonModel> category = null;
        //        List<SupportCommonModel> subcategory = null;
        //        List<RoomModel> room = null;

        //        if (supporttypeid != null)
        //        {
        //            category = supportMethod.GetAllSupportCategoryListByTypeId(supporttypeid.Value).ToList();
        //        }
        //        if (categoryid != null)
        //        {
        //            subcategory = supportMethod.GetSupportSubCategoryListByCategoryId(categoryid.Value).ToList();
        //        }
        //        if (buildingid != null)
        //        {
        //            room = supportMethod.GetRoomByBuildingID(buildingid.Value).ToList();
        //        }

        //        model.Status = CommonText.SUCCESS;
        //        model.StatusCode = HttpStatusCode.SUCCESS;
        //        model.supportType = supportType;
        //        model.categoryList = category;
        //        model.subCategoryList = subcategory;
        //        model.priorityList = supportProrityList;
        //        model.buildingList = getBuildingList;
        //        model.roomList = room;

        //    }
        //    catch
        //    {
        //        model.Status = CommonText.FAILED;
        //        model.StatusCode = HttpStatusCode.FAILURE;
        //    }

        //    return model;
        //}

        //[Route("getAllSupportResolveLists")]
        //[HttpGet]
        //public ActionResult<SupportResolveModel> GetAllSupportResolveLists(string type, long[] organizationid, long[] departmenttid,
        //    long roleId, long userId, long accessId, long[] SupportTypeId, long[] StatusId, long[] PriorityId,
        //    Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        //{
        //    SupportResolveModel model = new SupportResolveModel();

        //    try
        //    {
        //        var types = supportMethod.GetAllType().ToList(); 
        //        var priority = supportMethod.GetAllSupportPriority().ToList();
        //        var status = supportMethod.GetAllSupportStatus().ToList();

        //        List<SupportCommonModel> org = null;
        //        List<SupportCommonModel> dept = null;
        //        List<SupportCommonModel> sType = null;

        //        if (type != null)
        //        {
        //            org = supportMethod.GetAllOrganisationByTYpe(type).ToList();
        //        }
        //        if (organizationid != null && type != null)
        //        { 
        //            dept = supportMethod.GetAllDepartmentById(type, organizationid).ToList();
        //        }
        //        if (departmenttid != null)
        //        { 
        //           sType = supportMethod.GetAllSupportTypeByDepartmentId(departmenttid).ToList();
        //        }

        //        if (commonMethods.checkaccessavailable("Resolve", accessId, "Status Update", "Support", roleId) == false)
        //        {
        //            model.Status = "MODULE_ACCESS_DENIED";
        //            model.StatusCode = HttpStatusCode.MODULE_ACCESS_DENIED;
        //        }
        //        else
        //        {
        //            var res = supportMethod.SupportResolve(roleId, userId, accessId, type, organizationid, departmenttid, SupportTypeId, StatusId, PriorityId, FromDate, ToDate);
        //            model.Status = CommonText.SUCCESS;
        //            model.StatusCode = HttpStatusCode.SUCCESS;
        //            model.Status = CommonText.SUCCESS;
        //            model.StatusCode = HttpStatusCode.SUCCESS;
        //            model.Type = types;
        //            model.Organizations = org;
        //            model.Department = dept;
        //            model.SupportType = sType;
        //            model.Priority = priority;
        //            model.Statuses = status;
        //            model.supportResponseModels = res;
        //        } 
        //    }
        //    catch
        //    {
        //        model.Status = CommonText.FAILED;
        //        model.StatusCode = HttpStatusCode.FAILURE;
        //    }

        //    return model;
        //}
    }
}