﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Constants;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class SchoolController : ControllerBase
    {

        //Get Parent Profile
        [HttpPost]
        [Route("getCalender")]
        public ActionResult<CalenderResponseModel> GetAcademicCalender(CalenderRequestModel requestModel)
        {
            CalenderResponseModel model = new CalenderResponseModel();
            try
            {
                List<CalenderEngagementModel> data = new List<CalenderEngagementModel>();

                
                    CalenderEngagementModel item = new CalenderEngagementModel();
                    item.CalenderEngagementTypeName = "PTM";
                    item.CalenderDate =requestModel.CalenderDate;
                    item.CalenderEngagementId = 1;
                    item.CalenderEngagementTypeId = 1;

                    data.Add(item);
                


                model.CalenderEngagements = data;
                model.Status = CommonText.SUCCESS;
                model.StatusCode = HttpStatusCode.SUCCESS;
            }
            catch(Exception e)
            {
                model.Status = CommonText.EXCEPTION;
                model.DisplayMessage = e.Message;
                model.StatusCode = HttpStatusCode.EXCEPTION;
            }

            return model;

        }

    }
}