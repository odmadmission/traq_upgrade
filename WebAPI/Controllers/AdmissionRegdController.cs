﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/AdmissionRegd")]
    [ApiController]
    public class AdmissionRegdController : ControllerBase
    {
        private IAdmissionRepository admissionRepository;
        public IAdmissionStudentRepository admissionStudentRepository;
        public IAdmissionStudentContactRepository admissionStudentContactRepository;
        public IAdmissionStudentAddressRepository admissionStudentAddressRepository;
        public IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        public IPaymentType paymentType;
        public IProfessionTypeRepository professionTypeRepository;
        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IAdmissionStudentCounselorRepository admissionStudentCounselorRepository;
        public IAdmissionStudentAcademicRepository admissionStudentAcademicRepository;
        public IAdmissionStudentLanguageRepository admissionStudentLanguageRepository;
        public IAdmissionStudentParentRepository admissionStudentParentRepository;
        public IAdmissionStudentProficiencyRepository admissionStudentProficiencyRepository;
        public IAdmissionStudentReferenceRepository admissionStudentReferenceRepository;
        public IAdmissionStudentTransportRepository admissionStudentTransportRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IBoardStandardRepository boardStandardRepository;
        public IStandardRepository StandardRepository;
        public ILanguageRepository languageRepository;
        public ICategoryRepository categoryRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private ICityRepository cityRepository;
        private IStateRepository stateRepository;
        private ISupportRepository supportRepository;
        private ICountryRepository countryRepository;
        private IOrganizationRepository organizationRepository;
        private IReligionTypeRepository religionTypeRepository;
        private IAddressRepository addressRepository;
        private IOrganizationAcademicRepository organizationAcademicRepository;
        private IAcademicStandardRepository academicStandardRepository;
        private IAcademicSessionRepository academicSessionRepository;
        private IBoardRepository boardRepository;
        private IWingRepository wingRepository;
        public AdmissionRegdController(IWingRepository wingRepo, IBloodGroupRepository bloodGroupRepo, IBoardRepository boardRepo, IAcademicSessionRepository academicSessionRepo, IAcademicStandardRepository academicStandardRepo, IOrganizationAcademicRepository organizationAcademicRepo, IAddressRepository addressRepo, IReligionTypeRepository religionTypeRepo, IOrganizationRepository organizationRepo, ICountryRepository countryRepo, ISupportRepository supportRepo, IStateRepository stateRepo, IAddressTypeRepository addressTypeRepo, ICityRepository cityRepo, ICategoryRepository categoryRepo, INationalityTypeRepository nationalityTypeRepo, ILanguageRepository languageRepo, IStandardRepository StandardRepo, IBoardStandardRepository boardStandardRepo, IBoardStandardWingRepository boardStandardWingRepo, IAcademicStandardWingRepository academicStandardWingRepo, IProfessionTypeRepository professionTypeRepo, IAdmissionRepository admissionRepo, IAdmissionStudentRepository admissionStudentRepository, IAdmissionStudentContactRepository admissionStudentContactRepository,
        IAdmissionStudentAddressRepository admissionStudentAddressRepository, IAdmissionStudentStandardRepository admissionStudentStandardRepository,
        IPaymentType paymentType, IAdmissionFormPaymentRepository admissionFormPaymentRepository, IAdmissionStudentCounselorRepository admissionStudentCounselorRepository, IAdmissionStudentAcademicRepository admissionStudentAcademicRepo, IAdmissionStudentLanguageRepository admissionStudentLanguageRepo, IAdmissionStudentParentRepository admissionStudentParentRepo,
        IAdmissionStudentProficiencyRepository admissionStudentProficiencyRepo, IAdmissionStudentReferenceRepository admissionStudentReferenceRepo, IAdmissionStudentTransportRepository admissionStudentTransportRepo)
        {
            organizationRepository = organizationRepo;
            wingRepository = wingRepo;
            countryRepository = countryRepo;
            boardRepository = boardRepo;
            bloodGroupRepository = bloodGroupRepo;
            academicSessionRepository = academicSessionRepo;
            academicStandardRepository = academicStandardRepo;
            organizationAcademicRepository = organizationAcademicRepo;
            addressRepository = addressRepo;
            stateRepository = stateRepo;
            supportRepository = supportRepo;
            religionTypeRepository = religionTypeRepo;
            cityRepository = cityRepo;
            addressTypeRepository = addressTypeRepo;
            categoryRepository = categoryRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            languageRepository = languageRepo;
            StandardRepository = StandardRepo;
            boardStandardRepository = boardStandardRepo;
            boardStandardWingRepository = boardStandardWingRepo;
            academicStandardWingRepository = academicStandardWingRepo;
            admissionRepository = admissionRepo;
            this.admissionStudentRepository = admissionStudentRepository;
            this.admissionStudentContactRepository = admissionStudentContactRepository;
            this.admissionStudentAddressRepository = admissionStudentAddressRepository;
            this.admissionStudentStandardRepository = admissionStudentStandardRepository;
            this.paymentType = paymentType;
            this.admissionFormPaymentRepository = admissionFormPaymentRepository;
            this.admissionStudentCounselorRepository = admissionStudentCounselorRepository;
            admissionStudentAcademicRepository = admissionStudentAcademicRepo;
            admissionStudentLanguageRepository = admissionStudentLanguageRepo;
            admissionStudentParentRepository = admissionStudentParentRepo;
            admissionStudentProficiencyRepository = admissionStudentProficiencyRepo;
            admissionStudentReferenceRepository = admissionStudentReferenceRepo;
            admissionStudentTransportRepository = admissionStudentTransportRepo;
            professionTypeRepository = professionTypeRepo;
        }


        [Route("getStudentProfile")]
        [HttpGet]
        public async Task<ActionResult<AdmissionRegdModel>> GetAllPersonalData(long admissionid)
        {
            AdmissionRegdModel regdModel = new AdmissionRegdModel();

            regdModel.code = 200;

            if (admissionid == 0)
            {
                regdModel.status = "FAILED";
                regdModel.message = "AdmissionId cannot be null";
               
            }

            var aid = Convert.ToInt64(admissionid);

            regdModel.status = "SUCCESS";

            var admissionSingle = admissionRepository.GetAdmissionById(aid);


            if (admissionSingle == null)
            {
               
                regdModel.message = "Admission details not found for "+aid;
             
            }

            var admissionStudentSingle = admissionRepository.GetAdmissionStudentById(admissionSingle.ID);

            if (admissionStudentSingle == null)
            {
               
                regdModel.message = "Student details not found for " + aid;
               
            }

            admissionStudentSingle.Image = admissionStudentSingle.Image == null ? ""
                : "/ODMImages/Admission/" + admissionSingle.ID + "/" + admissionStudentSingle.Image;
            var studentId = admissionStudentSingle.ID;
            var admissionStudentContactList = (from a in admissionStudentContactRepository.ListAllAsyncIncludeActiveNoTrack().Result
                                               where a.AdmissionStudentId == studentId
                                               select a).ToList();
            var admissionstudentaddressList = (from b in admissionStudentAddressRepository.ListAllAsyncIncludeActiveNoTrack().Result
                                               where b.AdmissionStudentId == studentId
                                               select b).ToList();
            var admissionstudentStandardSingle = (from c in admissionStudentStandardRepository.ListAllAsyncIncludeAll().Result
                                                  where c.AdmissionStudentId == studentId
                                                  join d in academicStandardWingRepository.ListAllAsyncIncludeAll().Result on c.AcademicStandardWindId equals d.ID
                                                  join e in boardStandardWingRepository.ListAllAsyncIncludeAll().Result on d.BoardStandardWingID equals e.ID
                                                  join f in boardStandardRepository.ListAllAsyncIncludeAll().Result on e.BoardStandardID equals f.ID
                                                  join g in StandardRepository.GetAllStandard() on f.StandardId equals g.ID
                                                  select new admissionStudentStandardcls
                                                  {
                                                      ID = c.ID,
                                                      AcademicStandardWindId = c.AcademicStandardWindId,
                                                      AdmissionStudentId = c.AdmissionStudentId,
                                                      StandardId = c.StandardId,
                                                      Nucleus = c.Nucleus,
                                                      OrganizationId = c.OrganizationId,
                                                      WingId = c.WingId,
                                                      BoardStandardID = e.BoardStandardID
                                                  }).FirstOrDefault();
            var admissionFormPaymentSingle = (from d in admissionFormPaymentRepository.ListAllAsyncIncludeAll().Result
                                              where d.AdmissionId == admissionSingle.ID
                                              select d).FirstOrDefault();
            var admissionStudentdeclarationSingle = (from e in admissionRepository.GetAllAdmissionStudentDeclaration()
                                                     where e.AdmissionId == studentId
                                                     select e).FirstOrDefault();
            var admissionStudentAcademicSingle = (from a in admissionStudentAcademicRepository.AllAdmissionStudentAcademic()
                                                  where a.AdmissionStudentID == studentId
                                                  select a).FirstOrDefault();
            var admissionStudentLanguageList = (from a in admissionStudentLanguageRepository.ListAllAsyncIncludeAll().Result
                                                join b in languageRepository.ListAllAsyncIncludeAll().Result on a.LanguageId equals b.ID
                                                where a.AdmissionStudentId == studentId
                                                select new commoncls
                                                {
                                                    id = b.ID,
                                                    name = b.Name
                                                }).ToList();
            var admissionStudentParentList = (from a in admissionStudentParentRepository.ListAllAsyncIncludeAll().Result
                                              where a.AdmissionStudentId == studentId
                                              select a).ToList();
            var admissionStudentProficiencySingle = (from a in admissionStudentProficiencyRepository.ListAllAsyncIncludeAll().Result
                                                     where a.AdmissionStudentID == studentId
                                                     select a).FirstOrDefault();

            List<AdmissionStudentReference> admissionStudentReferenceList = new List<AdmissionStudentReference>();
            var jjj = admissionStudentReferenceRepository.ListAllAsyncIncludeActiveNoTrack();
            if (admissionStudentReferenceRepository.ListAllAsyncIncludeAll() != null)
            {
                admissionStudentReferenceList = (from a in admissionStudentReferenceRepository.ListAllAsyncIncludeAll().Result
                                                 where a.AdmissionStudentId == studentId
                                                 select a).ToList();
            }
            else
            {
                admissionStudentReferenceList = null;
            }
            //var admissionStudentReferenceList = (from a in admissionStudentReferenceRepository.ListAllAsyncIncludeAll().Result
            //                                     where a.AdmissionStudentId == admissionStudentSingle.ID
            //                                     select a).ToList();

            var admissionStudentTransportSingle = (from a in admissionStudentTransportRepository.ListAllAsyncIncludeAll().Result
                                                   where a.AdmissionStudentId == studentId
                                                   select a).FirstOrDefault();

            var nationality = (from a in nationalityTypeRepository.GetAllNationalities()
                               select new commoncls
                               {
                                   id = a.ID,
                                   name = a.Name
                               }).ToList();
            var categories = (from a in categoryRepository.ListAllAsyncIncludeAll().Result
                              select new commoncls
                              {
                                  id = a.ID,
                                  name = a.Name
                              }).ToList();
            var bloodGroups = (from a in bloodGroupRepository.GetAllBloodGroup()
                               select new commoncls
                               {
                                   id = a.ID,
                                   name = a.Name
                               }).ToList();
            var professiontypes = (from a in professionTypeRepository.GetAllProfessionTypes()
                                   select new commoncls
                                   {
                                       id = a.ID,
                                       name = a.Name
                                   }).ToList();
            var languages = (from a in languageRepository.ListAllAsyncIncludeAll().Result
                             select new commoncls
                             {
                                 id = a.ID,
                                 name = a.Name
                             }).ToList();
            var locations = (from a in supportRepository.GetAllLocation()
                             select new commoncls
                             {
                                 id = a.ID,
                                 name = a.Name
                             }).ToList();
            var countries = (from a in countryRepository.GetAllCountries()
                             select new commoncls
                             {
                                 id = a.ID,
                                 name = a.Name
                             }).ToList();
            var religions = (from a in religionTypeRepository.GetAllReligionTypes()
                             select new commoncls
                             {
                                 id = a.ID,
                                 name = a.Name
                             }).ToList();

            List<string> genders = new List<string>() { "MALE", "FEMALE", "OTHERS" };
         
            regdModel.admission = admissionSingle;
            regdModel.admissionStudent = admissionStudentSingle;
            regdModel.admissionStudentAcademic = admissionStudentAcademicSingle;
            regdModel.admissionStudentAddress = admissionstudentaddressList;
            regdModel.admissionStudentEmergency = admissionStudentContactList.Where(a => a.ConatctType == "emergency").FirstOrDefault();
            regdModel.admissionStudentOfficial = admissionStudentContactList.Where(a => a.ConatctType == "official").FirstOrDefault();
            regdModel.admissionStudentLanguage = admissionStudentLanguageList;
            regdModel.admissionStudentDeclaration = admissionStudentdeclarationSingle;
            regdModel.admissionStudentFather = admissionStudentParentList == null ? null : admissionStudentParentList.Where(a => a.ParentRelationship == "father").FirstOrDefault();
            regdModel.admissionStudentMother = admissionStudentParentList == null ? null : admissionStudentParentList.Where(a => a.ParentRelationship == "mother").FirstOrDefault();
            regdModel.admissionStudentProficiency = admissionStudentProficiencySingle;
            regdModel.admissionStudentReference = admissionStudentReferenceList;
            regdModel.admissionStudentStandard = admissionstudentStandardSingle;
            regdModel.admissionStudentTransport = admissionStudentTransportSingle;

            regdModel.isadmissionStudent = admissionStudentSingle == null ? false : true;
            regdModel.isadmissionStudentAcademic = admissionStudentAcademicSingle == null ? false : true;
            regdModel.isadmissionStudentAddress = admissionstudentaddressList.Count() == 0 ? false : true;
            regdModel.isadmissionStudentEmergency = admissionStudentContactList.Where(a => a.ConatctType == "emergency").FirstOrDefault() == null ? false : true;
            regdModel.isadmissionStudentOfficial = admissionStudentContactList.Where(a => a.ConatctType == "official").FirstOrDefault() == null ? false : true;
            regdModel.isadmissionStudentLanguage = admissionStudentLanguageList.Count() == 0 ? false : true;
            regdModel.isadmissionStudentDeclaration = admissionStudentdeclarationSingle == null ? false : true;
            regdModel.isadmissionStudentParent = admissionStudentParentList.Count() == 0 ? false : true;
            regdModel.isadmissionStudentProficiency = admissionStudentProficiencySingle == null ? false : true;
            regdModel.isadmissionStudentReference = admissionStudentReferenceList.Count() == 0 ? false : true;
            regdModel.isadmissionStudentStandard = admissionstudentStandardSingle == null ? false : true;
            regdModel.isadmissionStudentTransport = admissionStudentTransportSingle == null ? false : true;

            regdModel.nationality = nationality;
            regdModel.category = categories;
            regdModel.bloodGroups = bloodGroups;
            regdModel.professiontypes = professiontypes;
            regdModel.languages = languages;
            regdModel.locations = locations;
            regdModel.countries = countries;
            regdModel.genders = genders;
            regdModel.religions = religions;
            return regdModel;
        }

        [Route("getStates")]
        [HttpGet]
        public async Task<ActionResult<List<commoncls>>> GetStates(long countryid)
        {
            var res = (from a in stateRepository.GetAllStateByCountryId(countryid)
                       select new commoncls
                       {
                           id = a.ID,
                           name = a.Name
                       }).ToList();
            return res;
        }
        [Route("getCity")]
        [HttpGet]
        public async Task<ActionResult<List<commoncls>>> GetCity(long stateid)
        {
            var res = (from a in cityRepository.GetAllCityByStateId(stateid)
                       select new commoncls
                       {
                           id = a.ID,
                           name = a.Name
                       }).ToList();
            return res;
        }

        [Route("getStandard")]
        [HttpGet]
        public async Task<ActionResult<List<commoncls>>> GetStandard()
        {
            var session = academicSessionRepository.ListAllAsync().Result.Where(a => a.IsAdmission == true && a.IsAvailable == true).FirstOrDefault();
            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                 Result.Where(a => a.AcademicSessionId == session.ID && a.Active == true).ToList().Select(a => a.ID);

            var boardStandards = academicStandardRepository.ListAllAsync().Result.
                    Where(a => orgAcademics.Contains(a.OrganizationAcademicId) && a.Active == true).ToList().Select(a => a.BoardStandardId);

            var standardsBoards = boardStandardRepository.ListAllAsync().
                    Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
            var standards = (from a in standardsBoards
                             select new commoncls
                             {
                                 id = a.ID,
                                 name = boardRepository.GetBoardById(a.BoardId).Name + "--" + StandardRepository.GetStandardById(a.StandardId).Name
                             }).ToList();

            return standards;
        }
        [Route("getStandardWing")]
        [HttpGet]
        public async Task<ActionResult<List<commoncls>>> GetStandardWing(long boardstandardid)
        {
            var standardsBoards = boardStandardRepository.ListAllAsync().
                  Result.Where(a => a.ID == boardstandardid && a.Active == true).ToList();
            var standardsBoardswing = boardStandardWingRepository.ListAllAsync().
                 Result.Where(a => a.BoardStandardID == boardstandardid && a.Active == true).ToList();
            List<commoncls> list = new List<commoncls>();
            var academicstandard = academicStandardRepository.ListAllAsync().Result;
            var academicstandardwing = academicStandardWingRepository.ListAllAsync().Result;
            var organisation = organizationRepository.GetAllOrganization();
            var session = academicSessionRepository.ListAllAsync().Result.Where(a => a.IsAdmission == true && a.IsAvailable == true).FirstOrDefault();
            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                 Result.Where(a => a.AcademicSessionId == session.ID && a.Active == true).ToList();
            var wings = wingRepository.ListAllAsync().Result;
            foreach (var org in orgAcademics)
            {
                var boardStandards = academicstandard.
                    Where(a => a.OrganizationAcademicId == org.ID && a.Active == true && a.BoardStandardId == boardstandardid).FirstOrDefault();
                foreach (var st in standardsBoardswing)
                {
                    var academicwing = academicstandardwing.Where(a => a.AcademicStandardId == boardStandards.ID && a.Active == true && a.BoardStandardWingID == st.ID).FirstOrDefault();
                    var wingname = wings.Where(a => a.ID == st.WingID).FirstOrDefault();
                    var organisationname = organisation.Where(a => a.ID == org.OrganizationId).FirstOrDefault().Name;
                    commoncls od = new commoncls();
                    od.id = academicwing.ID;
                    od.name = organisationname + "-" + wingname;
                    list.Add(od);
                }
            }

            return list;
        }


        #region Create Or Edit Profile Details

        [Route("createOrEditStudentDetails")]
        [HttpPost]
        public async Task<ActionResult<AdmissionStudent>> UploadStudentDetails([FromForm] admissionStudentDetailscls StudentData)
        {
            AdmissionStudent studentobj = admissionStudentRepository.GetByIdAsync(StudentData.ID).Result;
            if (studentobj == null)
            {
                studentobj = new AdmissionStudent();
                studentobj.FirstName = StudentData.FirstName;
                studentobj.MiddleName = StudentData.MiddleName;
                studentobj.LastName = StudentData.LastName;
                studentobj.Gender = StudentData.Gender;
                studentobj.BloodGroupID = StudentData.BloodGroupID;
                studentobj.NatiobalityID = StudentData.NatiobalityID;
                studentobj.ReligionID = StudentData.ReligionID;
                studentobj.CategoryID = StudentData.CategoryID;
                studentobj.MotherTongueID = StudentData.MotherTongueID;
                studentobj.PassportNO = StudentData.PassportNO;
                studentobj.AadharNO = StudentData.AadharNO;
                studentobj.DateOfBirth = StudentData.DateOfBirth;
                studentobj.IsAvailable = true;
                studentobj.ModifiedDate = DateTime.Now;
                studentobj.InsertedDate = DateTime.Now;
                studentobj.InsertedId = StudentData.AdmissionId;
                studentobj.ModifiedId = StudentData.AdmissionId;
                var id = admissionStudentRepository.AddAsync(studentobj).Result.ID;
                if (StudentData.languages.Count() > 0)
                {
                    foreach (var lang in StudentData.languages)
                    {
                        AdmissionStudentLanguage admissionStudent = new AdmissionStudentLanguage();
                        admissionStudent.Active = true;
                        admissionStudent.AdmissionStudentId = id;
                        admissionStudent.InsertedDate = DateTime.Now;
                        admissionStudent.InsertedId = StudentData.AdmissionId;
                        admissionStudent.IsAvailable = true;
                        admissionStudent.LanguageId = lang;
                        admissionStudent.ModifiedDate = DateTime.Now;
                        admissionStudent.ModifiedId = StudentData.AdmissionId;
                        admissionStudent.Status = "ACTIVE";
                        admissionStudentLanguageRepository.AddAsync(admissionStudent);
                    }
                }
                return studentobj;
            }
            else
            {
                if (StudentData.Image != null)
                {
                    studentobj.Image = StudentData.Image;
                }
                studentobj.FirstName = StudentData.FirstName;
                studentobj.MiddleName = StudentData.MiddleName;
                studentobj.LastName = StudentData.LastName;
                studentobj.Gender = StudentData.Gender;
                studentobj.BloodGroupID = StudentData.BloodGroupID;
                studentobj.NatiobalityID = StudentData.NatiobalityID;
                studentobj.ReligionID = StudentData.ReligionID;
                studentobj.CategoryID = StudentData.CategoryID;
                studentobj.MotherTongueID = StudentData.MotherTongueID;
                studentobj.PassportNO = StudentData.PassportNO;
                studentobj.AadharNO = StudentData.AadharNO;
                studentobj.DateOfBirth = StudentData.DateOfBirth;
                studentobj.IsAvailable = true;
                studentobj.ModifiedDate = DateTime.Now;
                admissionStudentRepository.UpdateAsync(studentobj);
                if (StudentData.languages.Count() > 0)
                {
                    foreach (var lang in StudentData.languages)
                    {
                        AdmissionStudentLanguage admissionStudent = admissionStudentLanguageRepository.ListAllAsync().Result.Where(a => a.LanguageId == lang && a.AdmissionStudentId == studentobj.ID && a.Active == true).FirstOrDefault();
                        if (admissionStudent == null)
                        {
                            admissionStudent = new AdmissionStudentLanguage();
                            admissionStudent.Active = true;
                            admissionStudent.AdmissionStudentId = studentobj.ID;
                            admissionStudent.InsertedDate = DateTime.Now;
                            admissionStudent.InsertedId = StudentData.AdmissionId;
                            admissionStudent.IsAvailable = true;
                            admissionStudent.LanguageId = lang;
                            admissionStudent.ModifiedDate = DateTime.Now;
                            admissionStudent.ModifiedId = StudentData.AdmissionId;
                            admissionStudent.Status = "ACTIVE";
                            admissionStudentLanguageRepository.AddAsync(admissionStudent);
                        }

                    }
                    var admissionStudentlist = admissionStudentLanguageRepository.ListAllAsync().Result.Where(a => a.AdmissionStudentId == studentobj.ID && a.Active == true).ToList();

                    if (admissionStudentlist.Count > 0)
                    {
                        var ddp = admissionStudentlist.Where(a => !StudentData.languages.Contains(a.LanguageId)).ToList();

                        foreach (var ss in ddp)
                        {
                            AdmissionStudentLanguage admissionStudent = new AdmissionStudentLanguage();
                            admissionStudent = ss;
                            admissionStudent.Active = false;
                            admissionStudent.ModifiedDate = DateTime.Now;
                            admissionStudent.ModifiedId = StudentData.AdmissionId;
                            admissionStudent.Status = "IN-ACTIVE";
                            admissionStudentLanguageRepository.UpdateAsync(admissionStudent);
                        }
                    }
                }
                return studentobj;
            }
        }

        #endregion

        #region Declaration
        [Route("createOrEditStudentDeclaration")]
        [HttpPost]
        public async Task<ActionResult<AdmissionStudentDeclaration>> UpdateDeclaration(AdmissionStudentDeclaration objAdDeclaration)
        {
            var sid = objAdDeclaration.AdmissionId;
            AdmissionStudentDeclaration studentaddressobj = new AdmissionStudentDeclaration();
            if (objAdDeclaration.ID != 0)
            {
                AdmissionStudentDeclaration studentDeclarobj1 = admissionRepository.GetAdmissionStudentDeclarationById(objAdDeclaration.ID);
                studentDeclarobj1.IsSon = objAdDeclaration.IsSon;
                studentDeclarobj1.IsDuaghter = objAdDeclaration.IsDuaghter;
                studentDeclarobj1.IsFather = objAdDeclaration.IsFather;
                studentDeclarobj1.IsMother = objAdDeclaration.IsMother;
                studentDeclarobj1.IsGuardian = objAdDeclaration.IsGuardian;
                studentDeclarobj1.ClassId = objAdDeclaration.ClassId;
                studentDeclarobj1.SignatureFileName = objAdDeclaration.SignatureFileName;
                studentDeclarobj1.StudentName = objAdDeclaration.StudentName;
                studentDeclarobj1.ParentName = objAdDeclaration.ParentName;
                studentDeclarobj1.ModifiedDate = DateTime.Now;

                admissionRepository.UpdateAdmissionStudentDeclaration(studentDeclarobj1);
                return studentDeclarobj1;
            }
            else
            {
                //  AdmissionStudentAddress studentaddressobj = new AdmissionStudentAddress();
                studentaddressobj.IsSon = objAdDeclaration.IsSon;
                studentaddressobj.IsDuaghter = objAdDeclaration.IsDuaghter;
                studentaddressobj.IsFather = objAdDeclaration.IsFather;
                studentaddressobj.IsMother = objAdDeclaration.IsMother;
                studentaddressobj.IsGuardian = objAdDeclaration.IsGuardian;
                studentaddressobj.ClassId = objAdDeclaration.ClassId;
                studentaddressobj.SignatureFileName = objAdDeclaration.SignatureFileName;
                studentaddressobj.StudentName = objAdDeclaration.StudentName;
                studentaddressobj.ParentName = objAdDeclaration.ParentName;
                studentaddressobj.InsertedId = 1;
                studentaddressobj.ModifiedId = 1;
                studentaddressobj.IsAvailable = true;
                studentaddressobj.Status = "ACTIVE";
                studentaddressobj.Active = true;
                studentaddressobj.AdmissionId = sid;
                var changeid1 = admissionRepository.CreateAdmissionStudentDeclaration(studentaddressobj);
                return studentaddressobj;
            }

        }

        #endregion

        #region Address
        [Route("createOrEditStudentAddress")]
        [HttpPost]
        public async Task<ActionResult<List<AdmissionStudentAddress>>> UpdateStudentAddress([FromForm] List<AdmissionStudentAddress> StudentPreAddressData)
        {
            foreach (var ob in StudentPreAddressData)
            {
                AdmissionStudentAddress studentaddressobj = new AdmissionStudentAddress();
                //var sidval = "sid_" + studentid;
                if (ob.ID != 0)
                {
                    AdmissionStudentAddress studentaddressobj1 = admissionStudentAddressRepository.GetByIdAsync(ob.ID).Result;
                    studentaddressobj1.AddressLine1 = ob.AddressLine1;
                    studentaddressobj1.AddressLine2 = ob.AddressLine2;
                    studentaddressobj1.AddressType = ob.AddressType;
                    studentaddressobj1.CountryId = ob.CountryId;
                    studentaddressobj1.StateId = ob.StateId;
                    studentaddressobj1.CityId = ob.CityId;
                    studentaddressobj1.PostalCode = ob.PostalCode;
                    studentaddressobj1.ModifiedDate = DateTime.Now;

                    var res = admissionStudentAddressRepository.UpdateAsync(studentaddressobj);
                    // return studentaddressobj1;
                }
                else
                {
                    //  AdmissionStudentAddress studentaddressobj = new AdmissionStudentAddress();
                    studentaddressobj.AddressLine1 = ob.AddressLine1;
                    studentaddressobj.AddressLine2 = ob.AddressLine2;
                    studentaddressobj.AddressType = ob.AddressType;
                    studentaddressobj.CountryId = ob.CountryId;
                    studentaddressobj.StateId = ob.StateId;
                    studentaddressobj.CityId = ob.CityId;
                    studentaddressobj.PostalCode = ob.PostalCode;
                    studentaddressobj.InsertedDate = DateTime.Now;
                    studentaddressobj.ModifiedDate = DateTime.Now;
                    studentaddressobj.InsertedId = 1;
                    studentaddressobj.ModifiedId = 1;
                    studentaddressobj.IsAvailable = true;
                    studentaddressobj.Status = "ACTIVE";
                    studentaddressobj.Active = true;
                    studentaddressobj.AdmissionStudentId = ob.AdmissionStudentId;
                    var changeid1 = admissionStudentAddressRepository.AddAsync(studentaddressobj);
                    //return studentaddressobj;
                }

            }
            return StudentPreAddressData;
        }

        #endregion
        #region transport
        [Route("createOrEditStudentTransport")]
        [HttpPost]
        public async Task<ActionResult<AdmissionStudentTransport>> UpdateStudentTransport(AdmissionStudentTransport StudentTransportData)
        {

            var sid = StudentTransportData.AdmissionStudentId;
            AdmissionStudentTransport obj = new AdmissionStudentTransport();
            if (StudentTransportData.ID != 0)
            {
                var studentTransport = admissionStudentTransportRepository.GetByIdAsync(StudentTransportData.ID).Result;
                if (StudentTransportData.IsTransport == true)
                {
                    studentTransport.IsTransport = true;
                    studentTransport.LocationId = StudentTransportData.LocationId;
                    studentTransport.Distance = StudentTransportData.Distance;


                }
                else
                {
                    studentTransport.IsTransport = false;
                    studentTransport.LocationId = "";
                    studentTransport.Distance = 0;
                }
                studentTransport.AdmissionStudentId = sid;
                admissionStudentTransportRepository.UpdateAsync(studentTransport);
                return studentTransport;
            }
            else
            {

                if (StudentTransportData.IsTransport == true)
                {
                    obj.IsTransport = true;
                    obj.LocationId = StudentTransportData.LocationId;
                    obj.Distance = StudentTransportData.Distance;
                }
                else
                {
                    obj.IsTransport = false;
                    obj.LocationId = "";
                    obj.Distance = 0;
                }
                obj.AdmissionStudentId = sid;
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.InsertedId = 1;
                obj.ModifiedId = 1;
                obj.IsAvailable = true;
                obj.Status = "ACTIVE";
                obj.Active = true;
                admissionStudentTransportRepository.AddAsync(obj);
                return obj;
            }


        }

        #endregion

        #region proficiency
        [Route("createOrEditStudentProficiency")]
        [HttpPost]
        public ActionResult<AdmissionStudentProficiency> UpdateStudentProficiency(AdmissionStudentProficiency studentProficiencyData)
        {
            AdmissionStudentProficiency obj = new AdmissionStudentProficiency();
            if (studentProficiencyData.ID != 0)
            {
                var academicProficiency = admissionStudentProficiencyRepository.GetByIdAsync(studentProficiencyData.ID).Result;
                academicProficiency.PerformingArts = studentProficiencyData.PerformingArts;
                academicProficiency.Sports = studentProficiencyData.Sports;
                academicProficiency.PositionHeld = studentProficiencyData.PositionHeld;
                academicProficiency.ModifiedDate = DateTime.Now;
                academicProficiency.ModifiedId = studentProficiencyData.AdmissionStudentID;
                admissionStudentProficiencyRepository.UpdateAsync(academicProficiency);
                return academicProficiency;
            }
            else
            {

                obj.Sports = studentProficiencyData.Sports;
                obj.PerformingArts = studentProficiencyData.PerformingArts;
                obj.PositionHeld = studentProficiencyData.PositionHeld;
                obj.AdmissionStudentID = studentProficiencyData.AdmissionStudentID;
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.InsertedId = studentProficiencyData.AdmissionStudentID;
                obj.ModifiedId = studentProficiencyData.AdmissionStudentID;
                obj.IsAvailable = true;
                obj.Status = "ACTIVE";
                obj.Active = true;
                admissionStudentProficiencyRepository.AddAsync(obj);
                return obj;
            }

        }

        #endregion
        #region Academic
        [Route("createOrEditStudentAcademic")]
        [HttpPost]
        public ActionResult<AdmissionStudentAcademic> UpdateStudentAcademic(AdmissionStudentAcademic StudentAcademicData)
        {
            AdmissionStudentAcademic newAdmissionStudentAcademicObj = new AdmissionStudentAcademic();

            if (StudentAcademicData.ID != 0)
            {
                var academicSingle = admissionStudentAcademicRepository.GetByIdAsync(StudentAcademicData.ID).Result;
                //academicSingle.AdmissionStudentID = Convert.ToInt64(studentid);
                academicSingle.AggregatePercentageOrGrade = StudentAcademicData.AggregatePercentageOrGrade;
                academicSingle.CountryId = StudentAcademicData.CountryId;
                academicSingle.StateId = StudentAcademicData.StateId;
                academicSingle.CityId = StudentAcademicData.CityId;
                academicSingle.ReasonForChange = StudentAcademicData.ReasonForChange;
                academicSingle.OrganizationName = StudentAcademicData.OrganizationName;
                admissionStudentAcademicRepository.UpdateAsync(academicSingle);
                return academicSingle;
            }
            else
            {
                // AdmissionStudentAcademic newAdmissionStudentAcademicObj = new AdmissionStudentAcademic();
                newAdmissionStudentAcademicObj.AdmissionStudentID = StudentAcademicData.AdmissionStudentID;
                newAdmissionStudentAcademicObj.AggregatePercentageOrGrade = StudentAcademicData.AggregatePercentageOrGrade;
                newAdmissionStudentAcademicObj.CountryId = StudentAcademicData.CountryId;
                newAdmissionStudentAcademicObj.StateId = StudentAcademicData.StateId;
                newAdmissionStudentAcademicObj.CityId = StudentAcademicData.CityId;
                newAdmissionStudentAcademicObj.OrganizationName = StudentAcademicData.OrganizationName;
                newAdmissionStudentAcademicObj.ReasonForChange = StudentAcademicData.ReasonForChange;
                newAdmissionStudentAcademicObj.IsAvailable = true;
                newAdmissionStudentAcademicObj.Active = true;
                newAdmissionStudentAcademicObj.Status = "ACTIVE";
                newAdmissionStudentAcademicObj.InsertedDate = DateTime.Now;
                newAdmissionStudentAcademicObj.InsertedId = 1;
                newAdmissionStudentAcademicObj.ModifiedDate = DateTime.Now;
                newAdmissionStudentAcademicObj.ModifiedId = 1;
                admissionStudentAcademicRepository.AddAsync(newAdmissionStudentAcademicObj);
                return newAdmissionStudentAcademicObj;
            }
        }
        #endregion
        #region Contact
        [Route("createOrEditStudentContact")]
        [HttpPost]
        public ActionResult<ContactViewmodel> UpdateStudenContact(ContactViewmodel contactViewmodel)
        {
            if (contactViewmodel.emegencyId != 0)
            {
                var id = contactViewmodel.emegencyId;
                var contactdetails = admissionStudentContactRepository.GetByIdAsync(id).Result;
                //contactdetails.AdmissionStudentId = Convert.ToInt64(studentid);
                contactdetails.FullName = contactViewmodel.txtEmrName;
                contactdetails.Mobile = contactViewmodel.txtEmrPhNo;
                contactdetails.ModifiedDate = DateTime.Now;
                contactdetails.ModifiedId = contactViewmodel.admissionStudentID;
                admissionStudentContactRepository.UpdateAsync(contactdetails);
                
            }
            else
            {
                AdmissionStudentContact obj = new AdmissionStudentContact();
                obj.AdmissionStudentId = contactViewmodel.admissionStudentID;
                obj.FullName = contactViewmodel.txtEmrName;
                obj.Mobile = contactViewmodel.txtEmrPhNo;
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = "ACTIVE";
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = contactViewmodel.admissionStudentID;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = contactViewmodel.admissionStudentID;
                obj.ConatctType = "emergency";
                admissionStudentContactRepository.AddAsync(obj);
               
            }
            if (contactViewmodel.ofcContactId != 0)
            {
                var id = contactViewmodel.ofcContactId;
                var contactdetails = admissionStudentContactRepository.GetByIdAsync(id).Result;
                //contactdetails.AdmissionStudentId = Convert.ToInt64(studentid);
                contactdetails.FullName = contactViewmodel.txtEmrOfcName;
                contactdetails.Mobile = contactViewmodel.txtEmrOfcPhNo;
                contactdetails.EmailId = contactViewmodel.txtEmrOfcEmail;
                contactdetails.ModifiedDate = DateTime.Now;
                contactdetails.ModifiedId = contactViewmodel.admissionStudentID;
                admissionStudentContactRepository.UpdateAsync(contactdetails);
            }
            else
            {
                AdmissionStudentContact obj = new AdmissionStudentContact();
                obj.AdmissionStudentId = contactViewmodel.admissionStudentID;
                obj.FullName = contactViewmodel.txtEmrOfcName;
                obj.Mobile = contactViewmodel.txtEmrOfcPhNo;
                obj.EmailId = contactViewmodel.txtEmrOfcEmail;
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = "ACTIVE";
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = contactViewmodel.admissionStudentID;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = contactViewmodel.admissionStudentID;
                obj.ConatctType = "official";
                admissionStudentContactRepository.AddAsync(obj);
            }
            return contactViewmodel;
        }


        #endregion
        #region Reference
        [Route("createOrEditStudentReference")]
        [HttpPost]
        public ActionResult<AdmissionStudentReference> UpdateStudentReference(AdmissionStudentReference studentReference)
        {

            AdmissionStudentReference obj = new AdmissionStudentReference();
            if (studentReference.ID != 0)
            {
                AdmissionStudentReference studentStandardobj = admissionStudentReferenceRepository.GetByIdAsync(studentReference.ID).Result;
                studentStandardobj.FullName = studentReference.FullName;
                studentStandardobj.ProfessionTypeId = studentReference.ProfessionTypeId;
                studentStandardobj.Mobile = studentReference.Mobile;
                studentStandardobj.Address = studentReference.Address;
                studentStandardobj.Pincode = studentReference.Pincode;
                studentStandardobj.CountrytId = studentReference.CountrytId;
                studentStandardobj.ModifiedDate = DateTime.Now;

                admissionStudentReferenceRepository.UpdateAsync(studentStandardobj);
            }
            else
            {
                obj.FullName = studentReference.FullName;
                obj.ProfessionTypeId = studentReference.ProfessionTypeId;
                obj.Mobile = studentReference.Mobile;
                obj.Address = studentReference.Address;
                obj.Pincode = studentReference.Pincode;
                obj.CountrytId = studentReference.CountrytId;
                obj.ModifiedDate = DateTime.Now;
                obj.AdmissionStudentId = studentReference.AdmissionStudentId;
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = "ACTIVE";
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = studentReference.AdmissionStudentId;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = studentReference.AdmissionStudentId;
                admissionStudentReferenceRepository.AddAsync(obj);
            }
            return studentReference;
        }

        #endregion
        #region Standard
        [Route("createOrEditStudentStandard")]
        [HttpPost]
        public ActionResult<AdmissionStudentStandard> UpdateStudentStandard(AdmissionStudentStandard StudentStandardData)
        {

            //var sidval = "sid_" + studentid;
            if (StudentStandardData.ID != 0)
            {
                AdmissionStudentStandard studentStandardobj = admissionStudentStandardRepository.GetByIdAsync(StudentStandardData.ID).Result;
                studentStandardobj.WingId = StudentStandardData.WingId;
                studentStandardobj.OrganizationId = StudentStandardData.OrganizationId;
                studentStandardobj.Nucleus = StudentStandardData.Nucleus;
                studentStandardobj.ModifiedDate = DateTime.Now;

                admissionStudentStandardRepository.UpdateAsync(studentStandardobj);
            }
            else
            {
                //AdmissionStudentAddress studentaddressobj = new AdmissionStudentAddress();
                //studentaddressobj.AddressLine1 = StudentPreAddressData.AddressLine1;
                //studentaddressobj.AddressLine2 = StudentPreAddressData.AddressLine2;
                //studentaddressobj.AddressType = StudentPreAddressData.AddressType;
                //studentaddressobj.CountryId = StudentPreAddressData.CountryId;
                //studentaddressobj.StateId = StudentPreAddressData.StateId;
                //studentaddressobj.CityId = StudentPreAddressData.CityId;
                //studentaddressobj.PostalCode = StudentPreAddressData.PostalCode;
                //studentaddressobj.InsertedDate = DateTime.Now;
                //studentaddressobj.ModifiedDate = DateTime.Now;
                //studentaddressobj.InsertedId = 1;
                //studentaddressobj.ModifiedId = 1;
                //studentaddressobj.IsAvailable = true;
                //studentaddressobj.Status = "ACTIVE";
                //studentaddressobj.Active = true;
                //studentaddressobj.AdmissionStudentId = Convert.ToInt64(studentid);
                //await admissionStudentAddressRepository.AddAsync(studentaddressobj);
            }
            return StudentStandardData;
        }

        #endregion
        #region Mother and Father
        [Route("createOrEditStudentFather")]
        [HttpPost]
        public async Task<ActionResult<AdmissionStudentParent>> UploadSaveFather(AdmissionStudentParent admissionStudentParent)
        {
            AdmissionStudentParent obj = new AdmissionStudentParent();
            if (admissionStudentParent.ID != 0)
            {
                AdmissionStudentParent studentParentobj = admissionStudentParentRepository.GetByIdAsync(admissionStudentParent.ID).Result;
                studentParentobj.FullName = admissionStudentParent.FullName;
                studentParentobj.PrimaryMobile = admissionStudentParent.PrimaryMobile;
                studentParentobj.AlternativeMobile = admissionStudentParent.AlternativeMobile;
                studentParentobj.EmailId = admissionStudentParent.EmailId;
                studentParentobj.ProfessionTypeId = admissionStudentParent.ProfessionTypeId;
                studentParentobj.ParentRelationship = admissionStudentParent.ParentRelationship;
                studentParentobj.Company = admissionStudentParent.Company;
                studentParentobj.ModifiedDate = DateTime.Now;
                if (admissionStudentParent.Image != null)
                {
                    studentParentobj.Image = admissionStudentParent.Image;
                }


                admissionStudentParentRepository.UpdateAsync(studentParentobj);
            }
            else
            {

                obj.FullName = admissionStudentParent.FullName;
                obj.PrimaryMobile = admissionStudentParent.PrimaryMobile;
                obj.AlternativeMobile = admissionStudentParent.AlternativeMobile;
                obj.EmailId = admissionStudentParent.EmailId;
                obj.ProfessionTypeId = admissionStudentParent.ProfessionTypeId;
                obj.ParentRelationship = admissionStudentParent.ParentRelationship;
                obj.Company = admissionStudentParent.Company;
                obj.Image = admissionStudentParent.Image;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.AdmissionStudentId = admissionStudentParent.AdmissionStudentId;
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = "ACTIVE";
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = 1;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = 1;
                admissionStudentParentRepository.AddAsync(obj);
            }
            return admissionStudentParent;

        }
        [Route("createOrEditStudentMother")]
        [HttpPost]
        public async Task<ActionResult<AdmissionStudentParent>> UploadSaveMother(AdmissionStudentParent admissionStudentParent)
        {
            AdmissionStudentParent obj = new AdmissionStudentParent();
            if (admissionStudentParent.ID != 0)
            {
                AdmissionStudentParent studentParentobj = admissionStudentParentRepository.GetByIdAsync(admissionStudentParent.ID).Result;
                studentParentobj.FullName = admissionStudentParent.FullName;
                studentParentobj.PrimaryMobile = admissionStudentParent.PrimaryMobile;
                studentParentobj.AlternativeMobile = admissionStudentParent.AlternativeMobile;
                studentParentobj.EmailId = admissionStudentParent.EmailId;
                studentParentobj.ProfessionTypeId = admissionStudentParent.ProfessionTypeId;
                studentParentobj.ParentRelationship = admissionStudentParent.ParentRelationship;
                studentParentobj.Company = admissionStudentParent.Company;
                studentParentobj.ModifiedDate = DateTime.Now;
                if (admissionStudentParent.Image != null)
                {
                    studentParentobj.Image = admissionStudentParent.Image;
                }
                studentParentobj.ModifiedDate = DateTime.Now;
                studentParentobj.ModifiedId = admissionStudentParent.AdmissionStudentId;

                admissionStudentParentRepository.UpdateAsync(studentParentobj);
            }
            else
            {

                obj.FullName = admissionStudentParent.FullName;
                obj.PrimaryMobile = admissionStudentParent.PrimaryMobile;
                obj.AlternativeMobile = admissionStudentParent.AlternativeMobile;
                obj.EmailId = admissionStudentParent.EmailId;
                obj.ProfessionTypeId = admissionStudentParent.ProfessionTypeId;
                obj.ParentRelationship = admissionStudentParent.ParentRelationship;
                obj.Company = admissionStudentParent.Company;
                obj.Image = admissionStudentParent.Image;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.AdmissionStudentId = admissionStudentParent.AdmissionStudentId;
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = "ACTIVE";
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = admissionStudentParent.AdmissionStudentId;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = admissionStudentParent.AdmissionStudentId;
                admissionStudentParentRepository.AddAsync(obj);
            }
            return admissionStudentParent;
        }

        #endregion

    }
}
