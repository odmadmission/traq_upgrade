﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;

using WebAPI.Constants;
using WebAPI.Data;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/admission")]
    [ApiController]
    public class AdmissionController : ControllerBase
    {
        protected readonly IReligionTypeRepository religionTypeRepository;
        protected readonly INationalityTypeRepository nationalityTypeRepository;
        protected readonly ICategoryRepository categoryRepository;
        protected readonly ICountryRepository countryRepository;
        protected readonly ILanguageRepository languageRepository;
        protected readonly IProfessionTypeRepository professionTypeRepository;
        protected readonly IStateRepository stateRepository;
        protected readonly ICityRepository cityRepository;
        protected readonly IAcademicSessionRepository academicSessionRepository;
        protected readonly IAdmissionRepository admissionRepository;
        protected readonly AdmissionMethod admissionMethod;
        protected readonly AdmissionDataMethod method; 

        public AdmissionController(IReligionTypeRepository religionTypeRepository, INationalityTypeRepository nationalityTypeRepository,
            ICategoryRepository categoryRepository, ICountryRepository countryRepository, ILanguageRepository languageRepository,
            IProfessionTypeRepository professionTypeRepository, IStateRepository stateRepository, ICityRepository cityRepository,
            IAcademicSessionRepository academicSessionRepository, IAdmissionRepository admissionRepository,
         AdmissionDataMethod method,

        AdmissionMethod admissionMethod)
        {
            this.method = method;
            this.religionTypeRepository = religionTypeRepository;
            this.nationalityTypeRepository = nationalityTypeRepository;
            this.categoryRepository = categoryRepository;
            this.countryRepository = countryRepository;
            this.languageRepository = languageRepository;
            this.professionTypeRepository = professionTypeRepository;
            this.stateRepository = stateRepository;
            this.cityRepository = cityRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.admissionRepository = admissionRepository;
            this.admissionMethod = admissionMethod;
        }

        //[Route("getAllPersonalData")]
        //[HttpGet]
        //public ActionResult<PersonalDataResponseModal> GetAllPersonalData()
        //{
        //    PersonalDataResponseModal model = new PersonalDataResponseModal();
        //    try
        //    {
        //        var religion = religionTypeRepository.GetAllReligionTypes().Select(a => new NameIdModal { id = a.ID, name = a.Name }).ToList();
        //        var nationality = nationalityTypeRepository.GetAllNationalities().Select(a => new NameIdModal { id = a.ID, name = a.Name }).ToList();
        //        var category = categoryRepository.ListAllAsyncIncludeAll().Result.Select(a => new NameIdModal { id = a.ID, name = a.Name }).ToList();
        //        var language = languageRepository.ListAllAsyncIncludeAll().Result.Select(a => new NameIdModal { id = a.ID, name = a.Name }).ToList();
        //        var country = countryRepository.GetAllCountries().Select(a => new NameIdModal { id = a.ID, name = a.Name }).ToList();
        //        var profession = professionTypeRepository.GetAllProfessionTypes().Select(a => new NameIdModal { id = a.ID, name = a.Name }).ToList();

        //        model.religions = religion;
        //        model.nationalities = nationality;
        //        model.categories = category;
        //        model.Languages = language;
        //        model.countries = country;
        //        model.professions = profession;
        //        model.StatusCode = HttpStatusCode.SUCCESS;
        //        model.Status = CommonText.SUCCESS;
        //    }
        //    catch (Exception e)
        //    {
        //        model.StatusCode = HttpStatusCode.FAILURE;
        //        model.Status = CommonText.FAILED;
        //    }

        //    return model;
        //}

        //[Route("getStatesByCountryId")]
        //[HttpGet]
        //public ActionResult<GeneralDataResponseModal> GetStatesByCountryId(long countryid)
        //{
        //    GeneralDataResponseModal model = new GeneralDataResponseModal();
        //    try
        //    {
        //        var states = stateRepository.GetAllStateByCountryId(countryid).Select(a => new NameIdModal { id = a.ID, name = a.Name }).ToList();
        //        if (states.Count() > 0)
        //        {
        //            model.states = states;
        //            model.StatusCode = HttpStatusCode.SUCCESS;
        //            model.Status = CommonText.SUCCESS;
        //        }
        //        else
        //        {
        //            model.StatusCode = HttpStatusCode.No_Record;
        //            model.Status = CommonText.No_Record;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        model.StatusCode = HttpStatusCode.FAILURE;
        //        model.Status = CommonText.FAILED;
        //    }

        //    return model;
        //}

        //[Route("getCityByStateId")]
        //[HttpGet]
        //public ActionResult<GeneralDataResponseModal> GetCityByStateId(long stateid)
        //{
        //    GeneralDataResponseModal model = new GeneralDataResponseModal();
        //    try
        //    {
        //        var city = cityRepository.GetAllCityByStateId(stateid).Select(a => new NameIdModal { id = a.ID, name = a.Name }).ToList();
        //        if (city.Count() > 0)
        //        {
        //            model.city = city;
        //            model.StatusCode = HttpStatusCode.SUCCESS;
        //            model.Status = CommonText.SUCCESS;
        //        }
        //        else
        //        {
        //            model.StatusCode = HttpStatusCode.No_Record;
        //            model.Status = CommonText.No_Record;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        model.StatusCode = HttpStatusCode.FAILURE;
        //        model.Status = CommonText.FAILED;
        //    }

        //    return model;
        //}

        //[Route("getAcademicSession")]
        //[HttpGet]
        //public ActionResult<AcademicSessionResponse> GetAcademicSession()
        //{
        //    AcademicSessionResponse model = new AcademicSessionResponse();
        //    try
        //    {
        //        var academicSession = academicSessionRepository.GetAllAcademicSession().Where(a => a.IsAvailable == true).Select(a => new NameIdModal { id = a.ID, name = a.DisplayName }).FirstOrDefault();
        //        if (academicSession != null)
        //        {
        //            model.academicSession = academicSession;
        //            model.StatusCode = HttpStatusCode.SUCCESS;
        //            model.Status = CommonText.SUCCESS;
        //        }
        //        else
        //        {
        //            model.StatusCode = HttpStatusCode.No_Record;
        //            model.Status = CommonText.No_Record;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        model.StatusCode = HttpStatusCode.FAILURE;
        //        model.Status = CommonText.FAILED;
        //    }

        //    return model;
        //}

        //[Route("getAdmissionSlotByAcademicSessionId")]
        //[HttpGet]
        //public ActionResult<AdmissionSlotResponse> GetAdmissionSlotByAcademicSessionId(long academicsessionid)
        //{
        //    AdmissionSlotResponse model = new AdmissionSlotResponse();
        //    try
        //    {
        //        var admissionSlots = admissionRepository.GetAllAdmissionSlot().Where(a => a.AcademicSessionId == academicsessionid).
        //            Select(a => new AdmissionSlotModal { id = a.ID, slotName = a.SlotName, startDate = a.StartDate, endDate = a.EndDate }).ToList();

        //        if (admissionSlots.Count() > 0)
        //        {
        //            model.admissionSlot = admissionSlots;
        //            model.StatusCode = HttpStatusCode.SUCCESS;
        //            model.Status = CommonText.SUCCESS;
        //        }
        //        else
        //        {
        //            model.StatusCode = HttpStatusCode.No_Record;
        //            model.Status = CommonText.No_Record;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        model.StatusCode = HttpStatusCode.FAILURE;
        //        model.Status = CommonText.FAILED;
        //    }

        //    return model;
        //}

        //[Route("getAdmissionClassList")]
        //[HttpGet]
        //public ActionResult<AdmissionClassListResponse> GetAdmissionClassList(long academicSessionId)
        //{
        //    AdmissionClassListResponse model = new AdmissionClassListResponse();
        //    try
        //    {
        //        var className = admissionMethod.GetClassByAcademicSession(academicSessionId);
        //        if (className.Count() > 0)
        //        {
        //            model.admissionClass = className;
        //            model.StatusCode = HttpStatusCode.SUCCESS;
        //            model.Status = CommonText.SUCCESS;
        //        }
        //        else
        //        {
        //            model.StatusCode = HttpStatusCode.No_Record;
        //            model.Status = CommonText.No_Record;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        model.StatusCode = HttpStatusCode.FAILURE;
        //        model.Status = CommonText.FAILED;
        //    }

        //    return model;
        //}

        [Route("getSchoolWingList")]
        [HttpGet]
        public ActionResult<WingResponse> getWingByBoardStandardId(long boardStandardId)
        {
            WingResponse model = new WingResponse();
            try
            {
                var academicSession = admissionRepository.GetCurrentAdmissionSession();
                var wingList = admissionMethod.GetWingByBoardStandardId(boardStandardId, academicSession.ID);
                if (wingList.Count() > 0)
                {
                    model.wings = wingList;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.Status = CommonText.No_Record;
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.FAILURE;
                model.Status = CommonText.FAILED;
                model.Message = e.Message;
            }

            return model;
        }

        [Route("validateMobile")]
        [HttpPost]
        public ActionResult<MobileValidateResponse> ValidateMobileNumber(string mobileno,long sourceId)
        {
            MobileValidateResponse model = new MobileValidateResponse();
            try
            {
             
                Admission admission = new Admission();
                var academicSession = admissionRepository.GetCurrentAdmissionSession();
                if (academicSession != null)
                {
                    
                        admission = admissionRepository.GetAdmissionByMobileAndSessionId(mobileno, academicSession.ID);
                        
                    
                    if (admission != null)
                    {
                        model.admissionId = admission.ID;
                        model.StatusCode = HttpStatusCode.SUCCESS;
                        model.Status = CommonText.SUCCESS;
                    }
                    else
                    {
                        var adm = method.SaveAdmission(
                    mobileno,
                     sourceId);

                        model.admissionId = adm;
                        model.StatusCode = HttpStatusCode.NEW_USER;
                        model.Status = CommonText.NEW_USER;
                    }
                }
            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.FAILURE;
                model.Status = CommonText.FAILED;
                model.Message = e.Message;
            }

            return model;
        }

        [Route("getClassList")]
        [HttpGet]
        public ActionResult<RegistrationResponse> GetRegistrationData()
        {
            RegistrationResponse model = new RegistrationResponse();
            try
            {
                var academicSession = admissionRepository.
                    GetCurrentAdmissionSession();
                if (academicSession != null)
                {
                  
                    var className = admissionMethod.GetClassByAcademicSession(academicSession.ID);

                    //model.academicSessionId = academicSession.ID;
                    //model.slotId = admissionSlots;
                    //model.sourceId = source;
                    model.admissionClass = className;
                   // model.admissionAmount = 1000;

                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.No_Record;
                    model.Status = CommonText.No_Record;
                }

            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.FAILURE;
                model.Status = CommonText.FAILED;
                model.Message = e.Message;
            }

            return model;
        }

        [Route("register")]
        [HttpPost]
        public ActionResult<SaveRegistrationResponse> SaveRegistrationData(AdmissionBasicStudentModel stumodel)
        {
            SaveRegistrationResponse model = new SaveRegistrationResponse();
            try
            {
                //var res = new { ID = 0 };
                var res = method.UpdateAdmissionDataAMobile
                    (stumodel.firstName,
                    stumodel.lastName,
                    stumodel.fullName,
                    stumodel.mobile,
                    stumodel.emailId,
                    stumodel.standardId + "",
                    stumodel.academicStandardWindId + "",
                    stumodel.academicSourceId, stumodel.admissionId
                    );
                if (res!=null)
                {
                    model.admissionId = res.ID;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }

            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.FAILURE;
                model.Status = CommonText.FAILED;
            }

            return model;
        }

        [Route("savePaymentDetails")]
        [HttpPost]
        public ActionResult<SavePaymentResponse> SavePaymentDetails(PaymentModal payment)
        {
            SavePaymentResponse model = new SavePaymentResponse();
            try
            {
                long res = admissionMethod.SavePaymentDetails(payment);
                if (res > 0)
                {
                    model.paymentDetailsId = res;
                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }

            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.FAILURE;
                model.Status = CommonText.FAILED;
            }

            return model;
        }

        [Route("getRegistrationDetails")]
        [HttpGet]
        public ActionResult<StudentBasicRegistrationResponse> GetBasicDetailsOfStudent(long admissionid)
        {
            StudentBasicRegistrationResponse model = new StudentBasicRegistrationResponse();
            try
            {
                var res = admissionMethod.GetBesicRegistration(admissionid);
                if (res != null)
                {

                    var admissionFee = admissionRepository.GetAdmissionFeeById("online", res.sessionId);
                    model.besicDetails = res;
                    model.admissionAmount = admissionFee.Ammount;

                    model.StatusCode = HttpStatusCode.SUCCESS;
                    model.Status = CommonText.SUCCESS;
                }
                else
                {
                    model.StatusCode = HttpStatusCode.FAILURE;
                    model.Status = CommonText.FAILED;
                }

            }
            catch (Exception e)
            {
                model.StatusCode = HttpStatusCode.FAILURE;
                model.Status = CommonText.FAILED;
            }

            return model;
        }

    }
}
