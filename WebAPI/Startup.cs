﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Infrastructure.RepositoryImpl.SchoolRepository;

using Swashbuckle.AspNetCore.Swagger;
using WebAPI.Controllers;
using WebAPI.Data;
using WebAPI.Models;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo;
using OdmErp.Infrastructure.RepositoryImpl.AddmissionRepository;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository;

using OdmErp.Infrastructure;
using OdmErp.WebAPI.Models;
using OdmErp.WebAPI.SendInBlue;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {


            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            //services.AddTransient<CommonMethods>();
            //services.AddTransient<commonsqlquery, commonsqlquery>();

            services.AddTransient<IEmployeeDocumentAttachmentRepository, EmployeeDocumentAttachmentRepository>();
        
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();
           
            services.AddTransient<IEmployeeDesignationRepository, EmployeeDesignationRepository>();
           
            services.AddTransient<IEmployeeAddressRepository, EmployeeAddressRepository>();
            services.AddTransient<IEmployeeExperienceRepository, EmployeeExperienceRepository>();
           
            services.AddTransient<IEmployeeEducationRepository, EmployeeEducationRepository>();
            services.AddTransient<IEmployeeBankAccountRepository, EmployeeBankAccountRepository>();
            services.AddTransient<IEmployeeRequiredDocumentRepository, EmployeeRequiredDocumentRepository>();
         
            services.AddTransient<IAccessRepository, AccessRepository>();
            services.AddTransient<IActionAccessRepository, ActionAccessRepository>();
            services.AddTransient<IAuthorityAggregateRepository, AuthorityAggregateRepository>();
            services.AddTransient<commonsqlquery,commonsqlquery>();
            services.AddTransient<IStudentAggregateRepository, StudentAggregateRepository>();
            services.AddTransient<IBloodGroupRepository, BloodGroupRepository>();
            services.AddTransient<INationalityTypeRepository, NationalityTypeRepository>();
            services.AddTransient<IReligionTypeRepository, ReligionTypeRepository>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IParentRepository, ParentRepository>();
            services.AddTransient<IOtpMessageRepository, OtpMessageRepository>();
            services.AddTransient<ILogingRepository, LoginRepository>();
            services.AddTransient<ILanguageRepository, LanguageRepository>();
            services.AddTransient<EmployeeMethod, EmployeeMethod>();
            services.AddTransient<SmsMethod, SmsMethod>();
            services.AddTransient<WebAPI.Data.CommonMethods, WebAPI.Data.CommonMethods>();
            services.AddTransient<TodoMethod, TodoMethod>();
          
            services.AddTransient<IAddressRepository, AddressRepository>();
            services.AddTransient<IAddressTypeRepository, AddressTypeRepository>();
            services.AddTransient<ICountryRepository, CountryRepository>();
            services.AddTransient<ICityRepository, CityRepository>();
            services.AddTransient<IStateRepository, StateRepository>();
            services.AddTransient<IModuleRepository, ModuleRepository>();
            services.AddTransient<ISubModuleRepository, SubModuleRepository>();
            services.AddTransient<IActionRepository, ActionRepository>();
            services.AddTransient<IDesignationRepository, DesignationRepository>();
            services.AddTransient<IDepartmentRepository, DepartmentRepository>();
            services.AddTransient<IDesignationLevelRepository, DesignationLevelRepository>();
            services.AddTransient<IGroupRepository, GroupRepository>();
            services.AddTransient<IOrganizationRepository, OrganizationRepository>();
            services.AddTransient<IBankRepository, BankRepository>();
            services.AddTransient<IBankBranchRepository, BankBranchRepository>();
            services.AddTransient<IEducationQualificationRepository, EducationQualificationRepository>();
            services.AddTransient<IEducationQualificationTypeRepository, EducationQualificationTypeRepository>();
            services.AddTransient<IDocumentTypeRepository, DocumentTypeRepository>();
            services.AddTransient<IDocumentSubTypeRepository, DocumentSubTypeRepository>();
            services.AddTransient<IAccessTypeRepository, AccessTypeRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<IEducationQualificationTypeRepository, EducationQualificationTypeRepository>();
            services.AddTransient<IEducationQualificationTypeRepository, EducationQualificationTypeRepository>();
            services.AddTransient<IGrievanceRepository, GrievanceRepository>();
            services.AddTransient<IStandardRepository, StandardRepository>();
            services.AddTransient<ISectionRepository, SectionRepository>();
            services.AddTransient<IBoardRepository, BoardRepository>();
            services.AddTransient<IToDoRepository, TodoRepository>();
            services.AddTransient<ISupportRepository, SupportRepository>();
            services.AddTransient<IErpBugRepository, ErpBugRepository>();
            services.AddTransient<IBankTypeRepository, BankTypeRepository>();
            services.AddTransient<StudentMethod, StudentMethod>();
            services.AddTransient<IStudentRepository, StudentRepository>();
            services.AddTransient<IProfessionTypeRepository, ProfessionTypeRepository>();
            services.AddTransient<IParentRelationshipTypeRepository, ParentRelationshipTypeRepository>();
            services.AddTransient<IOrganizationAcademicRepository, OrganizationAcademicRepository>();
            services.AddTransient<ISupportRepository, SupportRepository>();
            services.AddTransient<SupportMethod, SupportMethod>();
            services.AddTransient<IOrganizationTypeRepository, OrganizationTypeRepository>();
            services.AddTransient<IAcademicSessionRepository, AcademicSessionRepository>();
            services.AddTransient<IBoardStandardWingRepository, BoardStandardWingRepository>();
            services.AddTransient<IAcademicStandardWingRepository, AcademicStandardWingRepository>();
            services.AddTransient<IAdmissionRepository, AdmissionRepository>();
            services.AddTransient<IOrganizationAcademicRepository, OrganizationAcademicRepository>();
            services.AddTransient<IAcademicStandardRepository, AcademicStandardRepository>();
            services.AddTransient<IBoardStandardRepository, BoardStandardRepository>();
            services.AddTransient<IStandardRepository, StandardRepository>();
            services.AddTransient<IWingRepository, WingRepository>();
            services.AddTransient<AdmissionMethod, AdmissionMethod>();
            services.AddTransient<IAcademicSessionRepository, AcademicSessionRepository>();
            services.AddTransient<IAdmissionStudentRepository, AdmissionStudentRepository>();
            services.AddTransient<IAdmissionStudentStandardRepository, AdmissionStudentStandardRepository>();
            services.AddTransient<IAdmissionStudentContactRepository, AdmissionStudentContactRepository>();
            services.AddTransient<IAdmissionFormPaymentRepository, AdmissionFormPaymentRepository>();
          

            services.AddTransient<smssend, smssend>();
            services.AddTransient<SendInBlueEmailManager, SendInBlueEmailManager>();

            services.AddTransient<AdmissionDataMethod, AdmissionDataMethod>();
            services.AddTransient<IAdmissionRepository, AdmissionRepository>();
            services.AddTransient<IOrganizationRepository, OrganizationRepository>();
            services.AddTransient<IWingRepository, WingRepository>();
            services.AddTransient<IWingRepository, WingRepository>();



            services.AddTransient<IMasterSMSRepository,MasterSMSRepository>();
            services.AddTransient<IMasterEmailRepository, MasterEmailRepository>();

            services.AddTransient<CommonMethods, CommonMethods>();
            services.AddTransient<IGrievanceForwardRepository, GrievanceForwardRepository>();
          
            services.AddTransient<ISubjectRepository, SubjectRepository>();
            services.AddTransient<IBoardStandardRepository, BoardStandardRepository>();
            services.AddTransient<IChapterRepository, ChapterRepository>();
            services.AddTransient<IConceptRepository, ConceptRepository>();
            services.AddTransient<IAcademicSessionRepository, AcademicSessionRepository>();
            services.AddTransient<IOrganizationAcademicRepository, OrganizationAcademicRepository>();
            services.AddTransient<IAcademicStandardRepository, AcademicStandardRepository>();
            services.AddTransient<IAcademicSubjectRepository, AcademicSubjectRepository>();
            services.AddTransient<ISyallbusRepository, SyallbusRepository>();
            services.AddTransient<IAcademicChapterRepository, AcademicChapterRepository>();
            services.AddTransient<IAcademicStudentRepository, AcademicStudentRepository>();
            services.AddTransient<IAcademicSectionStudentRepository, AcademicSectionStudentRepository>();
            //  services.AddTransient<IBoardStandardStreamRepository, BoardStandardStreamRepository>();
            services.AddTransient<IAcademicSectionRepository, AcademicSectionRepository>();
            // services.AddTransient<IAcademicStandardStreamRepository, AcademicStandardStreamRepository>();
            services.AddTransient<ITeacherRepository, TeacherRepository>();
            services.AddTransient<IAcademicTeacherRepository, AcademicTeacherRepository>();
            services.AddTransient<ICalenderEngagementRepository, CalenderEngagementRepository>();
            services.AddTransient<ICalenderEngagementTypeRepository, CalenderEngagementTypeRepository>();
            services.AddTransient<IAcademicConceptRepository, AcademicConceptRepository>();
            services.AddTransient<IAcademicTeacherSubjectRepository, AcademicTeacherSubjectRepository>();
            services.AddTransient<IAcademicTeacherSubjectsTimeLineRepository, AcademicTeacherSubjectsTimeLineRepository>();
            services.AddTransient<IClassTimingRepository, ClassTimingRepository>();

            services.AddTransient<IAcademicCalenderRepository, AcademicCalenderRepository>();
            services.AddTransient<IAcademicTimeTableRepository, AcademicTimeTableRepository>();
            services.AddTransient<IAcademicSyllabusRepository, AcademicSyllabusRepository>();
            services.AddTransient<ITimeTablePeriodRepository, TimeTablePeriodRepository>();
            services.AddTransient<IPeriodEmployeeRepository, PeriodEmployeeRepository>();
            services.AddTransient<IPeriodEmployeeStatusRepository, PeriodEmployeeStatusRepository>();
            services.AddTransient<IPeriodSyllabusRepository, PeriodSyllabusRepository>();

           

            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IFeesTypeRepository, FeesTypeRepository>();
            services.AddTransient<IScholarshipApplicableRepository, ScholarshipApplicableRepository>();

            services.AddTransient<IFeesTypeApplicableRepository, FeesTypeApplicableRepository>();
            services.AddTransient<IFeesTypePrice, FeesTypePriceRepository>();

            services.AddTransient<ISessionFeesType, OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository.SessionFessTypeRepository>();

            services.AddTransient<ITeacherTimeLineRepository, TeacherTimeLineRepository>();
            services.AddTransient<IAcademicTeacherTimeLineRepository, AcademicTeacherTimeLineRepository>();

            services.AddTransient<IAdmissionVerifierRepository, AdmissionVerifierRepository>();
            services.AddTransient<IAdmissionInterViewerRepository, AdmissionInterViewerRepository>();

            services.AddTransient<IMessagesContentRepository, MessagesContentRepository>();

        

            services.AddTransient<IAdmissionStudentCounselorRepository, AdmissionStudentCounselorRepository>();

            services.AddTransient<IGrievanceRepository, GrievanceRepository>();
            services.AddTransient<IToDoRepository, TodoRepository>();
            services.AddTransient<IAccessRepository, AccessRepository>();
            services.AddTransient<IActionAccessRepository, ActionAccessRepository>();
            services.AddTransient<IAuthorityAggregateRepository, AuthorityAggregateRepository>();
            services.AddTransient<IStudentAggregateRepository, StudentAggregateRepository>();

            services.AddTransient<IAdmissionCounsellorRepository, AdmissionCounsellorRepository>();

            services.AddTransient<IPaymentGatewayRepository, PaymentGatewayRepositoryImpl>();
            services.AddTransient<IParentRepository, ParentRepository>();
            services.AddTransient<IAdmissionDocumentAssignRepository, AdmissionDocumentAssignRepository>();
            services.AddTransient<IAdmissionStudentDocumentRepository,
                AdmissionStudentDocumentRepository>();

            services.AddTransient<IAdmissionDocumentRepository,
               AdmissionDocumentRepository>();

            services.AddTransient<IAdmissionExamDateAssignRepository,
               AdmissionExamDateAssignRepository>();

            services.AddTransient<IAdmissionDocumentVerificationAssignRepository,
               AdmissionDocumentVerificationAssignRepository>();

         


            services.AddTransient<IAdmissionStandardExamRepository,
             AdmissionStandardExamRepository>();

            services.AddTransient<IAdmissionPersonalInterviewAssignRepository,
             AdmissionPersonalInterviewAssignRepository>();


            services.AddTransient<IMessagesContentRepository, MessagesContentRepository>();

            //Scholarship

            services.AddTransient<IMessagesContentRepository, MessagesContentRepository>();
            //Scholarship

            services.AddTransient<IMessagesContentRepository, MessagesContentRepository>();
            services.AddTransient<IScholarshipRepository, ScholarshipRepository>();
            services.AddTransient<ISessionScholarship, SessionScholarshipRepository>();
            services.AddTransient<ISessionScholarshipDiscountRepository, SessionScholarshipDiscountRepository>();
            services.AddTransient<ISessionScholarshipDiscountPriceRepository, SessionScholarshipDiscountPriceRepository>();
            services.AddTransient<ISessionScholarshipDiscountApplicableRepository, SessionScholarshipDiscountApplicableRepository>();
            services.AddTransient<IScholarshipApprovalRepoaitory, ScholarshipApprovalRepoaitory>();
            services.AddTransient<IScholarshipApplyRepository, ScholarshipApplyRepository>();

            services.AddTransient<IPaymentCollectionType, PaymentCollectionTypeRepository>();
            services.AddTransient<IPaymentType, PaymentTypeRepository>();
            services.AddTransient<IStudentPayment, StudentPaymentRepo>();
          
           
            services.AddTransient<IGrievanceForwardRepository, GrievanceForwardRepository>();
            services.AddTransient<IGrievanceExtendedTimelineAttachmentRepository, GrievanceExtendedTimelineAttachmentRepository>();
            services.AddTransient<IGrievanceAccessTimelineRepository, GrievanceAccessTimelineRepository>();
            services.AddTransient<IGrievanceExtendedTimelinesRepository, GrievanceExtendedTimelineRepository>();
            services.AddTransient<IOtpMessageRepository, OtpMessageRepository>();
           
            services.AddTransient<SupportMethod, 
               SupportMethod>();
            services.AddTransient<IContactTypeRepository, ContactTypeRepository>();
           
            services.AddTransient<StudentModelClass, StudentModelClass>();
            services.AddTransient<StudentMethod,StudentMethod>();
       
      
            services.AddTransient<IStudentLanguagesKnownRepository, StudentLanguagesKnownRepository>();
            services.AddTransient<ILanguageRepository, LanguageRepository>();
            services.AddTransient<IBoardStandardWingRepository, BoardStandardWingRepository>();
            services.AddTransient<IWingRepository, WingRepository>();
            services.AddTransient<ISubjectGroupRepository, SubjectGroupRepository>();
            services.AddTransient<ISubjectGroupDetailsRepository, SubjectGroupDetailsRepository>();
            services.AddTransient<ISubjectOptionalTypeRepository, SubjectOptionalTypeRepository>();
            services.AddTransient<ISubjectOptionalCategoryRepository, SubjectOptionalCategoryRepository>();
            services.AddTransient<IAcademicSubjectOptionalRepository, AcademicSubjectOptionalRepository>();
            services.AddTransient<ISubjectOptionalRepository, SubjectOptionalRepository>();
            services.AddTransient<IStudentEmergencyContactRepository, StudentEmergencyContactRepository>();
            services.AddTransient<IVehicleTypeRepository, VehicleTypeRepository>();
            services.AddTransient<IVehicleRepository, VehicleRepository>();
            services.AddTransient<IVehicleLocationRepository, VehicleLocationRepository>();
            services.AddTransient<IDepartureTimeRepository, DepartureTimeRepository>();
            services.AddTransient<IAcademicSubjectGroupRepository, AcademicSubjectGroupRepository>();
            services.AddTransient<IAcademicStandardWingRepository, AcademicStandardWingRepository>();
            services.AddTransient<IAcademicSubjectOptionalDetailsRepository, AcademicSubjectOptionalDetailsRepository>();
            services.AddTransient<IAcademicStudentSubjectOptionalRepository, AcademicStudentSubjectOptionalRepository>();
            services.AddTransient<IStudentTransportationRepository, StudentTransportationRepository>();
            services.AddTransient<IAcademicStandardSettingsRepository, AcademicStandardSettingsRepository>();
           

            #region--------------------------Admission------------------------------
            services.AddTransient<IContactTypeRepository, ContactTypeRepository>();
            services.AddTransient<IAdmissionRepository, AdmissionRepository>();
            services.AddTransient<IAdmissionStandardSeatQuotaRepository, AdmissionStandardSeatQuotaRepository>();
            services.AddTransient<IAdmissionStudentRepository, AdmissionStudentRepository>();
            services.AddTransient<IAdmissionStudentAcademicRepository, AdmissionStudentAcademicRepository>();
            services.AddTransient<IAdmissionStudentAddressRepository, AdmissionStudentAddressRepository>();
            services.AddTransient<IAdmissionStudentContactRepository, AdmissionStudentContactRepository>();
            services.AddTransient<IAdmissionStudentCounselorRepository, AdmissionStudentCounselorRepository>();
            services.AddTransient<IAdmissionStudentDocumentRepository, AdmissionStudentDocumentRepository>();
            services.AddTransient<IAdmissionStudentExamRepository, AdmissionStudentExamRepository>();
            services.AddTransient<IAdmissionStudentInterviewRepository, AdmissionStudentInterviewRepository>();
            services.AddTransient<IAdmissionStudentLanguageRepository, AdmissionStudentLanguageRepository>();
            services.AddTransient<IAdmissionStudentParentRepository, AdmissionStudentParentRepository>();
            services.AddTransient<IAdmissionStudentProficiencyRepository, AdmissionStudentProficiencyRepository>();
            services.AddTransient<IAdmissionStudentReferenceRepository, AdmissionStudentReferenceRepository>();
            services.AddTransient<IAdmissionStudentStandardRepository, AdmissionStudentStandardRepository>();
            services.AddTransient<IAdmissionStudentTransportRepository, AdmissionStudentTransportRepository>();
            services.AddTransient<IAdmissionFormPaymentRepository, AdmissionFormPaymentRepository>();
            services.AddTransient<IAdmissionStandardExamRepository, AdmissionStandardExamRepository>();
            services.AddTransient<IAdmissionStandardExamDateRepository, AdmissionStandardExamDateRepository>();
            services.AddTransient<IMapClassToAdmissionRepository, MapClassToAdmissionRepository>();
            services.AddTransient<IAdmissionStandardExamResultDateRepository, AdmissionStandardExamResultDateRepository>();
            services.AddTransient<IAdmissionDocumentRepository, AdmissionDocumentRepository>();
            services.AddTransient<IAdmissionPersonalInterviewAssignRepository, AdmissionPersonalInterviewAssignRepository>();
            services.AddTransient<IAdmissionDocumentVerificationAssignRepository, AdmissionDocumentVerificationAssignRepository>();

            services.AddTransient<ICounsellorStatusRepository, CounsellorStatusRepository>();
            services.AddTransient<IAdmissionFeeRepository, AdmissionFeeRepository>();
            services.AddTransient<IPaymentType, PaymentTypeRepository>();

            services.AddTransient<IAdmitcardVenueRepository, AdmitcardVenueRepository>();
            services.AddTransient<IAdmitcardDetailsRepository, AdmitcardDetailsRepository>();
            services.AddTransient<IAdmitcardInstructionsRepository, AdmitcardInstructionsRepository>();
            services.AddTransient<IAdmitcardVenueAddressRepository, AdmitcardVenueAddressRepository>();
           

            #endregion-----------------------------------------------------------
            services.AddTransient<IContactTypeRepository, ContactTypeRepository>();
           
            services.AddTransient<IAdmissionRepository, AdmissionRepository>();
            services.AddTransient<IAdmissionStandardSeatQuotaRepository, AdmissionStandardSeatQuotaRepository>();
            services.AddTransient<IAdmissionStudentRepository, AdmissionStudentRepository>();
            services.AddTransient<IAdmissionStudentAcademicRepository, AdmissionStudentAcademicRepository>();
            services.AddTransient<IAdmissionStudentAddressRepository, AdmissionStudentAddressRepository>();
            services.AddTransient<IAdmissionStudentContactRepository, AdmissionStudentContactRepository>();
            services.AddTransient<IAdmissionStudentCounselorRepository, AdmissionStudentCounselorRepository>();
            services.AddTransient<IAdmissionStudentDocumentRepository, AdmissionStudentDocumentRepository>();
            services.AddTransient<IAdmissionStudentExamRepository, AdmissionStudentExamRepository>();
            services.AddTransient<IAdmissionStudentInterviewRepository, AdmissionStudentInterviewRepository>();
            services.AddTransient<IAdmissionStudentLanguageRepository, AdmissionStudentLanguageRepository>();
            services.AddTransient<IAdmissionStudentParentRepository, AdmissionStudentParentRepository>();
            services.AddTransient<IAdmissionStudentProficiencyRepository, AdmissionStudentProficiencyRepository>();
            services.AddTransient<IAdmissionStudentReferenceRepository, AdmissionStudentReferenceRepository>();
            services.AddTransient<IAdmissionStudentStandardRepository, AdmissionStudentStandardRepository>();
            services.AddTransient<IAdmissionStudentTransportRepository, AdmissionStudentTransportRepository>();
            services.AddTransient<IAdmissionFormPaymentRepository, AdmissionFormPaymentRepository>();
           
            services.AddTransient<IAdmissionStandardExamRepository, AdmissionStandardExamRepository>();
            services.AddTransient<IAdmissionStandardExamDateRepository, AdmissionStandardExamDateRepository>();
            services.AddTransient<IMapClassToAdmissionRepository, MapClassToAdmissionRepository>();
            services.AddTransient<IAdmissionStandardExamResultDateRepository, AdmissionStandardExamResultDateRepository>();
            services.AddTransient<IAdmissionDocumentRepository, AdmissionDocumentRepository>();
            services.AddTransient<IAdmissionPersonalInterviewAssignRepository, AdmissionPersonalInterviewAssignRepository>();
            services.AddTransient<IAdmissionDocumentVerificationAssignRepository, AdmissionDocumentVerificationAssignRepository>();

            services.AddTransient<ICounsellorStatusRepository, CounsellorStatusRepository>();
            services.AddTransient<IAdmissionFeeRepository, AdmissionFeeRepository>();

            services.AddTransient<IAdmitcardVenueRepository, AdmitcardVenueRepository>();
            services.AddTransient<IAdmitcardDetailsRepository, AdmitcardDetailsRepository>();
            services.AddTransient<IAdmitcardInstructionsRepository, AdmitcardInstructionsRepository>();
            services.AddTransient<IAdmitcardVenueAddressRepository, AdmitcardVenueAddressRepository>();

           
            services.AddTransient<IMasterEmailRepository, MasterEmailRepository>();
            services.AddTransient<IMasterSMSRepository, MasterSMSRepository>();

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DevConnection"),
                    assembly => assembly.MigrationsAssembly("Infrastructure"));
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddHttpClient();
            services.AddHttpClient("admissionapi", client =>
            {
                client.BaseAddress = new Uri("https://localhost:44315/api/");
                //client.BaseAddress = new Uri("http://localhost:28453/api/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //c.DefaultRequestHeaders.Add("Accept", "application/vnd.github.v3+json");
                //c.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Sample");
            });
            // services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                
               
            });


           // app.UseHttpsRedirection();
            app.UseMvc();
            //app.uSE
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers();
            //});
        }
    }
}
