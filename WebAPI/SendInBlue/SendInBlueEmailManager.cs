﻿using Microsoft.AspNetCore.Hosting;
using OdmErp.WebAPI.Data;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OdmErp.WebAPI.SendInBlue
{
    public class SendInBlueEmailManager
    {
        private static string SEND_IN_BLUE_API_KEY = "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";
        private static string FromName = "ODM EDUCATIONAL GROUP";
        private static string HOST = "https://api.sendinblue.com/v3/smtp/email";
        private static string FromEmail = "tracq@odmegroup.org";




        private IHostingEnvironment hostingEnv;
        public SendInBlueEmailManager(IHostingEnvironment hostingEnv)
        {
            this.hostingEnv = hostingEnv;

        }
        private string SendMail(List<SendInBlueReceiverSender> receivers, string html, string subject)
        {

            try
            {
                SendInBlueRequestBody body = new SendInBlueRequestBody(html, receivers);
                SendInBlueReceiverSender sender = new SendInBlueReceiverSender();
                sender.name = FromName;
                sender.email = FromEmail;

                SendInBlueReceiverSender replyTo = new SendInBlueReceiverSender();
                replyTo.name = FromName;
                replyTo.email = FromEmail;

                body.replyTo = replyTo;
                body.sender = sender;
                body.subject = subject;

                var client = new RestClient(HOST);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("api-key", SEND_IN_BLUE_API_KEY);
                request.AddJsonBody(body);
                IRestResponse response = client.Execute(request);

                return response.StatusCode.ToString();

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void SendRegistrationMail
            (
             string session, string school,
            string board, string classname, string wing,
             string sname,
            string name,
            string mobile, string email,
            string receiver
            
            )
        {
            SendInBlueReceiverSender oner = new SendInBlueReceiverSender();
            oner.name = name;
            oner.email = receiver;
            List<SendInBlueReceiverSender> r = new List<SendInBlueReceiverSender>();
            r.Add(oner);

            string body = string.Empty;
            string filepath = hostingEnv.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "BasicRegistration_NotPaid.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Session}}", session).Replace("{{name}}", sname).Replace("{{School}}", school).Replace("{{Board}}", board)
                    .Replace("{{Class}}", classname).Replace("{{Wing}}", wing)
                    .Replace("{{Wing}}", wing)
                    .Replace("{{CName}}", name).Replace("{{Mobile}}", mobile)
                    .Replace("{{Email}}", email).Replace("{{link}}", 
                    UtilityModel.GetTinyUrl("Admission/Student/Login"));

            }
            Thread t1 = new Thread(delegate ()
            {
                SendMail(r, body, "Admissions "+session+" | Registration Successfull");

            });
            t1.Start();
           
        }

        public void SendCommonMail
           (
            
           string name,string msg,
           string receiver

           )
        {
            SendInBlueReceiverSender oner = new SendInBlueReceiverSender();
            oner.name = name;
            oner.email = receiver;
            List<SendInBlueReceiverSender> r = new List<SendInBlueReceiverSender>();
            r.Add(oner);

            string body = string.Empty;
            string filepath = hostingEnv.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "commontemplate.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{msg}}", msg)
                    .Replace("{{name}}", name);

            }
            Thread t1 = new Thread(delegate ()
            {
                SendMail(r, body, "Admissions | Notification");

            });
            t1.Start();

        }
        public void SendFinalRegistrationMail
            (
             
            string name, 
            string receiver
            
            )
        {
            SendInBlueReceiverSender oner = new SendInBlueReceiverSender();
            oner.name = name;
            oner.email = receiver;
            List<SendInBlueReceiverSender> r = new List<SendInBlueReceiverSender>();
            r.Add(oner);

            string body = string.Empty;
            string filepath = hostingEnv.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "finalsubmissionform.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name);

            }
            Thread t1 = new Thread(delegate ()
            {
                SendMail(r, body, "Application Form Submitted Successfully");

            });
            t1.Start();
           
        }

        public void SendPaymentMail
           (
            string session, string school,
           string board, string classname, string wing,
            string sname,
          
           string mobile, 
             string transid, string mode,
               string date, string amount,
               string link,
           string receiver, string email, string cname

           )
        {
            SendInBlueReceiverSender oner = new SendInBlueReceiverSender();
           // oner.name = name;
            oner.email = receiver;
            List<SendInBlueReceiverSender> r = new List<SendInBlueReceiverSender>();
            r.Add(oner);

            string body = string.Empty;
            string filepath = hostingEnv.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "BasicRegistration_Paid.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Session}}", session).Replace("{{name}}", sname).Replace("{{School}}", school).Replace("{{Board}}", board)
                    .Replace("{{Class}}", classname).Replace("{{Wing}}", wing)
                    .Replace("{{Wing}}", wing)

                     .Replace("{{TransactionId}}", transid)
                      .Replace("{{Mode}}", mode)
                       .Replace("{{amount}}", amount)
                        .Replace("{{paidDate}}", date)

                    .Replace("{{CName}}", cname).Replace("{{Mobile}}", mobile)
                    .Replace("{{Email}}", email).Replace("{{link}}",
                    link);

            }
            Thread t1 = new Thread(delegate ()
            {
                SendMail(r, body, "Admissions " + session + " | Registration Successfull");

            });
            t1.Start();

        }



    }


}
