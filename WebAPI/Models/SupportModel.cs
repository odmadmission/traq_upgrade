﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class SupportModel
    {
        public class SupportforAndroid
        {
            public long roleId { get; set; }
            public long userId { get; set; }
            public long accessId { get; set; }
            public string Type { get; set; }
            public long[] OrgId { get; set; }
            public long[] DeptId { get; set; }
            public long[] SupportTypeId { get; set; }
            public long[] StatusId { get; set; }
            public string ManagementStatus { get; set; }
            public long[] PriorityId { get; set; }
            public Nullable<DateTime> FromDate { get; set; }
            public Nullable<DateTime> ToDate { get; set; }
            public string mstatus { get; set; }
            public long deptemp { get; set; }
        }
        public class EmpClassModal
        {
            public long ID { get; set; }
            public string EmpName { get; set; }
            public long DeptId { get; set; }
            public string DeptName { get; set; }
        }
        public class DepartmentEmployeeResponse : BaseModel
        {
            public List<EmpClassModal> DeptEmp { get; set; }
        }
        public class AssignRequestToEmployee
        {
            public long userid { get; set; }
            public long supportid { get; set; }
            public long assignemp { get; set; }
        } 

        public class RoomModel
        { 
            public long ID{ get; set; }
            public string Name { get; set; }
            public long BuildingId { get; set; }
            public string BuildingName { get; set; }
            public long RoomCategoryId { get; set; }
            public string RoomCategoryName { get; set; }
            public long FloorId { get; set; }
            public string FloorName { get; set; }

        }
        public class SupportCommonModel
        {
            public long ID { get; set; }
            public string Name { get; set; }
        }

        public class SupportTypeModel
        {
            public long ID { get; set; }
            public string Name { get; set; }
            public bool IsAvailable { get; set; }

        }

        public class SupportCommonModelForFloor
        {
            public long ID { get; set; }
            public long FloorId { get; set; }
            public string Name { get; set; }

        }
        public class SupportCategoryModel
        {
            public long ID { get; set; }
            public string Name { get; set; }

        }
        public class BuildingModel
        {
            public long ID { get; set; }
            public string Name { get; set; }
            public long LocationId { get; set; }
            public string LocationName { get; set; }
            public long BuildingTypeId { get; set; }
            public string BuildingTypeName { get; set; }
            public long OrganizationId { get; set; }
            public string OrganizationName { get; set; }
            public string Address { get; set; }
            public long CityId { get; set; }
            public string CityName { get; set; }
            public long StateId { get; set; }
            public string StateName { get; set; }
            public long CountryId { get; set; }
            public string CountryName { get; set; }
        }

        public class SupportRequestModel
        {
           public long employeeId { get; set; }
            public long SupportStatusId { get; set; }
            public long SupportPriorityId { get; set; }
            public long SupportCategoryId { get; set; }
           public Nullable<DateTime>  FromDate { get; set; }
           public Nullable<DateTime> ToDate { get; set; }
        }

        public class SupportRequestBaseModel
        { 
            
        }
        public class SupportRequestResponseModel
        {
            public long ID { get; set; }
            public string OrgGroupName { get; set; }           
            public long CategoryID { get; set; }
            public string CategoryName { get; set; }         
            public long SubCategoryID { get; set; }
            public string SubCategoryName { get; set; }
            public long PriorityID { get; set; }
            public string PriorityName { get; set; }
            public long StatusID { get; set; }
            public string Status { get; set; }
            public long BuildingID { get; set; }
            public string BuildingName { get; set; }
            public long RoomID { get; set; }
            public string RoomName { get; set; }
            public long FloorID { get; set; }
            public string FloorName { get; set; }         
            public long LocationID { get; set; }
            public string LocationName { get; set; }          
            //public string assigntime { get; set; }
            public Nullable<bool> IsManagementRead { get; set; }
            public Nullable<bool> IsRead { get; set; }
            public string RequestedCompletionDate { get; set; }
            public long SupportTypeID { get; set; }
            public string SupportTypeName { get; set; }
            public long EmpId { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpFName { get; set; }
            public string EmpMName { get; set; }
            public string EmpLName { get; set; }
            public string EmpFullName { get; set; }         
            public string Reason { get; set; }  
            public string Description { get; set; }
            public string Title { get; set; }
            public string TicketCode { get; set; }
            public DateTime InsertedDate { get; set; }
            public (string Date, string Time, string DateTime) InsertedDateTime { get; set; }
            // public string InsertedDate { get; set; }
            public DateTime ModifiedDate { get; set; }
            public (string Date, string Time, string DateTime) ModifiedDateTime { get; set; }
            public bool IsAvailable { get; set; }
            public Nullable<DateTime> CompletionDate { get; set; }
            public string CompletionDateString { get; set; }
            public string Comment { get; set; }
            public long InsertedID { get; set; }
            public long UserID { get; set; }
            public bool IsAvailableLocation { get; set; }
            public string SentApprovalDate { get; set; }
            public bool SentApproval { get; set; }
            public string DueDate { get; set; }
            public string ManagementApprovalDate { get; set; }
            //public string ManagementRejectDate { get; set; }
            //public (bool Sent, string SentDate, string SentName, bool Approved, string ApprovedDate, string ApprovedName, bool Rejected, string RejectedDate,
            //   string RejectedName) Management
            //{ get; set; }
            public string ManagementRejectedName { get; set; }
            public string ManagementRejectedDate { get; set; }
            public bool ManagementRejected { get; set; }
            public string ManagementApprovedName { get; set; }
            public string ManagementApprovedDate { get; set; }
            public bool ManagementApproved { get; set; }
            public bool SendManagementApproval { get; set; }
            public string SendManagementApprovalDate { get; set; }
            public string SentManagementName { get; set; }
            public long ApprovedID { get; set; }
            public long RejectedID { get; set; }
            public string ApproveDate { get; set; }
            public string RejectDate { get; set; }
            public bool IsApprove { get; set; }
            public bool IsReject { get; set; }
            //public string ApproveReject { get; set; }
            public decimal? Score { get; set; }
            public long? ScoreGivenId { get; set; }
            public DateTime? ScoreGivenDate { get; set; }
            public string ScoreGivenDateString { get; set; }
            public int commentcount { get; set; }
            public string AssignedToEmp { get; set; }
            public string AssignedDate { get; set; }
            public long AssignToId { get; set; }

        }
        public class OrganizationModel
        {
            public long ID { get; set; }
            public string Name { get; set; }
            public long GroupId { get; set; }
            public string GroupName { get; set; }
            public long OrganizationTypeId { get; set; }
            public string OrganizationTypeName { get; set; }
        }


        public class DepartmentModel
        {
            public long ID { get; set; }
            public string Name { get; set; }
            public long OrganizationID { get; set; }
            public string OrganizationName { get; set; }          
            public long GroupID { get; set; }
            public string GroupName { get; set; }
        }

        public class SupportCommentModel : BaseModel
        {
            public string UpdateType { get; set; }

            public long SupportCommentID { get; set; }
            public string Name { get; set; }

        }
        public class SupportAttachmentModel : BaseModel
        {
            public long SupportAttachmentID { get; set; }

            //public bool isremove { get; set; }
        }

        public class SupportRaiseModel
        {
            public long CategoryID { get; set; }

            public long SubCategoryID { get; set; }


            public long RoomID { get; set; }

            public long BuildingID { get; set; }

            public long DepartmentID { get; set; }

            public long PriorityID { get; set; }

            public string Description { get; set; }

            public long StatusID { get; set; }
            public long SupportTypeID { get; set; }
            public long ApprovedID { get; set; }

        }

       
        public class SupportRequestDetailsModels
        {
          
            public long ID { get; set; } 
            public long SupportRequestID { get; set; }
            public string Description { get; set; }
            public long SupportTypeID { get; set; }
            public string SupportTypeName { get; set; }
            public long EmpId { get; set; }  
            public string EmployeeName { get; set; }
            public string EmployeeImage { get; set; }
            public string SupportStatus { get; set; }           
            public string C_Name { get; set; }
            public string PostedDate { get; set; }
            public string PostedTime { get; set; }
            public string ModifiedDate { get; set; }
            public string ModifiedTime { get; set; }
            public (string Date, string Time, string DateTime) ModifiedDateTime { get; set; }
            public (string Date, string Time, string DateTime) InsertedDateTime { get; set; }
            public string Path { get; set; }
            public List<SupportRequestDetailsModels> CommentList { get; set; }
          
        }
        public class SupportRequestAttachmentModels
        {
            public long ID { get; set; }
            public long SupportRequestID { get; set; }          
            public string EmployeeName { get; set; }
            //public string EmployeeImage { get; set; }
            public string SupportStatus { get; set; }
            public string C_Name { get; set; }
            public string PostedDate { get; set; }
            public string PostedTime { get; set; }
            public string ModifiedDate { get; set; }
            public string ModifiedTime { get; set; }
            public (string Date, string Time, string DateTime) ModifiedDateTime { get; set; }
            public (string Date, string Time, string DateTime) InsertedDateTime { get; set; }
            public string Path { get; set; }
          

        }

        public class updateManagementApprovalStatus
        {
            public long supportrequestid { get; set; }
            public long userId { get; set; }
        }

        public class updateSupportRequestStatus
        {
            public long supportrequestid { get; set; }
            public long statusid { get; set; }
            public long userId { get; set; }
            public string reason { get; set; }
        }

        public class supportRequestSendToManagementApproval
        {
            public long supportrequestid { get; set; }
            public long userId { get; set; }
        }

        public class supportRequestVerify
        {
            public long supportrequestid { get; set; }
            public long userId { get; set; }
            public decimal Score { get; set; } 
        }

        public class supportRequestRejectToManagementApproval
        {
            public long supportrequestid { get; set; }
            public long userId { get; set; }
            public string reason { get; set; }
        }

        public class editFromManagement
        {
            public long supportrequestid { get; set; }
            public long userId { get; set; }
        }

        public class assignDeadlineToSupportRequest
        {
            public long supportrequestid { get; set; }
            public DateTime requestedcompletiondate { get; set; }
        }

        public class requestChatMessage
        {
            public long supportrequestid { get; set; }
            public long userId { get; set; }
            public string comments { get; set; }
        }

        public class commonDelete
        {
            public long id { get; set; }
            public long userId { get; set; }
        }

        public class addAttachment
        {
            public long supportrequestid { get; set; }
            public long userId { get; set; }
            public IFormFile file { get; set; }
        }

        public class SupportAllBaseResponse : BaseModel
        { 
            public List<SupportRequestResponseModel> supportRequestResponseModels { get; set; }
            public List<SupportRequestDetailsModels> supportRequestDetailsModels { get; set; }   
            public List<SupportRequestResponseModel> SupportRequestDetailsModel { get; set; }
            public SupportRequestDetailsModels supportRequestCommentModels { get; set; } 
            public List<SupportRequestDetailsModels> supportRequestAttachmentModels { get; set; }
            public List<SupportRequestDetailsModels> supportRequestTimeLineModels { get; set; }    
        }

        public class SupportAllSingleResponse : BaseModel
        {
            public SupportRequestResponseModel SupportRequestDetailsModel { get; set; }
            public SupportRequestDetailsModels supportRequestCommentModels { get; set; }
            public SupportRequestDetailsModels supportRequestTimeLineModels { get; set; }
            public List<SupportRequestAttachmentModels> supportRequestAttachmentModels { get; set; }
             
        }

        public class RaiseSupportModels
        {  
            public long userId { get; set; }
            public long supporttypeid { get; set; }
            public long categoryid { get; set; }
            public long subcategoryid { get; set; }
            public long priorityid { get; set; }
            public Nullable<DateTime> CompletionDate { get; set; }
            public long buildingid { get; set; }
            public long roomid { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            //public List<IFormFile> files { get; set; }

        }


        public class SupportRaiseModels
        {            
            public long supportrequestid { get; set; }

            public long userId { get; set; }
            public long supporttypeid { get; set; }
            public long categoryid { get; set; }
            public long subcategoryid { get; set; }
            public long priorityid { get; set; }
            public Nullable<DateTime> CompletionDate { get; set; }
            public long buildingid { get; set; }
            public long roomid { get; set; }
            public string titel { get; set; }
            public string description { get; set; }           
           
        }

        public class SupportComments : BaseModel
        {
            public string Name { get; set; }
            public DateTime InsertedDate { get; set; }

        }


        public class SupportModel_List
        {
            public Nullable<long> ID { get; set; }
            public Nullable<long> StatusID { get; set; }
            public Nullable<int> commentcount { get; set; }
            public string Status { get; set; }
            public string Actions { get; set; }

            //public List<SupportRequestCls> SupportRequestcls { get; set; }
            //public List<DateStringValue> all_date { get; set; }
            public string t_Date { get; set; }
            public IEnumerable<NameIdViewModel> statuses { get; set; }
            public long PriorityID { get; set; }
            public IEnumerable<NameIdViewModel> priorities { get; set; }
            public string PriorityName { get; set; }
            public long? SupportTypeID { get; set; }
            public List<NameIdViewModel> SupportTypes { get; set; }
            public string SupportTypeName { get; set; }

            public long OrganizationID { get; set; }
            public List<NameIdViewModel> organizations { get; set; }
            public string OrganizationName { get; set; }

            public List<NameIdViewModel> departments { get; set; }

            public List<NameIdViewModel> Types { get; set; }

            public string Type { get; set; }
            public long[] PriorityId { get; set; }
            public long[] DeptId { get; set; }
            public long[] StatusId { get; set; }
            public long[] OrgId { get; set; }
            public long[] SupportTypeId { get; set; }
        }
        public class FilterVisibility
        {

            // Below three Modules access to SupportType action in resolve submodule of support module
            public bool DepartmentType { get; set; }
            public bool OrganizationList { get; set; }
            public bool DepartmentList { get; set; }

            // status update action in resolve submodule of support module
            public bool SupportType { get; set; }
            public bool SupportPriority { get; set; }
            public bool SupportStatus { get; set; }
        }

        public class SupportModelLists
        {
            public FilterVisibility FilterVisibility { get; set; }
            //public Nullable<long> ID { get; set; }
            //public Nullable<long> StatusID { get; set; }
            //public Nullable<int> commentcount { get; set; }
            //public string Status { get; set; }
            //public string Actions { get; set; }

            public List<SupportCommonModel> SupportStatus { get; set; }
            public List<SupportCommonModel> SupportPriority { get; set; }
            public List<NameIdViewModel> AllDepartmentViewModel { get; set; }
            public string Type { get; set; }
            public long[] PriorityId { get; set; }
            public long[] DeptId { get; set; }
            public long[] StatusId { get; set; }
            public long[] OrgId { get; set; }
            public long[] SupportTypeId { get; set; }
            public List<NameIdViewModel> SupportTypes { get; set; }
            public List<SupportRequestResponseModel> SupportRequestcls { get; set; }
            public List<NameIdViewModel> Types { get; set; }
            //public List<DateStringValue> all_date { get; set; }
            //public string t_Date { get; set; }
            //public IEnumerable<NameIdViewModel> statuses { get; set; }
            //public long PriorityID { get; set; }
            //public IEnumerable<NameIdViewModel> priorities { get; set; }
            //public string PriorityName { get; set; }
            //public long? SupportTypeID { get; set; }
            //public List<NameIdViewModel> SupportTypes { get; set; }
            public string SupportTypeName { get; set; }
            public long OrganizationID { get; set; }
            //public List<NameIdViewModel> organizations { get; set; }
            //public string OrganizationName { get; set; }

            //public List<NameIdViewModel> departments { get; set; }

            //public List<NameIdViewModel> Types { get; set; }

          
        }



       

        public class NameIdViewModel
        {
            public long ID { get; set; }
            public string TypeId { get; set; }
            public string Name { get; set; }
            public string Status { get; set; }
        }
        
        public class SupportResolveResponse:BaseModel
        {
           public List<NameIdViewModel> AllDepartmentViewModel { get; set; }
        }
        #region support chart model 
        public class ReportFilterVisibility
        {
          
            public bool DepartmentType { get; set; }
            public bool OrganizationList { get; set; }
            public bool DepartmentList { get; set; }          
            public bool SupportType { get; set; }          
        }
        public class SupportRequestNew
        {           
            public long StatusID { get; set; } 
            public bool IsApprove { get; set; }
            public bool IsReject { get; set; }            
            public bool ManagementApproval { get; set; }
            public bool SendApproval { get; set; }
            public bool ManagementReject { get; set; }
            public long SupportRequestId { get; set; }
            public long EmployeeId { get; set; }
        }
        public class SupportReportChartViewModel
        {
            public long userId { get; set; }
            public long roleId { get; set; }
            public long accessId { get; set; }
            public long? DeptEmp { get; set; }
            public string Type { get; set; }
            //public long RequestID { get; set; }
            public List<Int64> SupportTypeIds { get; set; }

            public List<Int64> DeptIds { get; set; }

            public List<Int64> OrgIds { get; set; }

            public List<Int64> GrpIds { get; set; }
            public Nullable<DateTime> FromDate { get; set; }
            public  Nullable<DateTime> ToDate { get; set; }

        }
        public class ApexChartOptions
        {
            public ReportFilterVisibility reportFilterVisibility { get; set; }
            public long TotalReportCount { get; set; }
            public Int64[] series { get; set; }
            public string[] labels { get; set; }

          //  public ApexChartSeriesChart chart { get; set; }

           // public ApexChartLegend legend { get; set; }
           // public ApexChartResponsive responsive { get; set; } 
            public string[] MonthNamesAsCategories { get; set; }

           // public ApexChartMonthWiseSeries[] MonthSeries { get; set; }

           // public ApexChartTimeLine[] timeline { get; set; }

        }
        public class SupportReportCountViewModel
        {

            public long SupportTypeId { get; set; }
            public long TotalSupportCount { get; set; }

            public long PendingCount { get; set; }
            public long OnHoldCount { get; set; }
            public long ProgressCount { get; set; }
            public long CompletedCount { get; set; }
            public long VerifiedCount { get; set; }

            public long RejectedCount { get; set; }

            public long MangementApprovedCount { get; set; }
            public long TotalMangementCount { get; set; }
            public long MangementRejectedCount { get; set; }

            public long MangementPendingCount { get; set; }

        }

        public class SupportChartOptionsResponse : BaseModel
        {
            public ApexChartOptions SupportChartReportDetailsModel { get; set; }         
        }
        #endregion

    }
}
