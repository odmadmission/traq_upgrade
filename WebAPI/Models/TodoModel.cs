﻿using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.Entities.TodoAggregate;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class TodoModel
    {
        public class ViewAllTaskResponseModel: BaseModel
        {        
            public List<TodoCommonModel> todoPriority { get; set; }
            public List<TodoStatusModel> todoStatus { get; set; }   
            public List<empClass> EmployeeList { get; set; }
            public viewModel Taskviewmodels { get; set; }
        }
        public class empClass
        {
            public long EmpId { get; set; }
            public string EmpCode { get; set; }
            public string EName { get; set; }
        }
        public class ViewAllTaskFilterationResponseModel : BaseModel
        {
         
            public List<TodoStatusModel> todoStatus { get; set; }
            public List<TodoCommonModel> todoPriprity { get; set; }
            public taskviewmodel Taskviewmodels { get; set; }
        }


        public class StatusModel
        {
            public string ID { get; set; }
            public string Name { get; set; }
        }

        public class StatusResponseModel : BaseModel
        { 
            public List<StatusModel> Statuses { get; set; }
        }

        public class SubtaskModel
        {
            public long userId { get; set; }
            public long taskId { get; set; }
            public long subtaskid { get; set; }
            public string name { get; set; }
            public long priority { get; set; }
            public long? assigndeptid { get; set; }
            public long? assignToId { get; set; }
            public long? deptid { get; set; }
            public DateTime? duedate { get; set; }
            public string desc { get; set; }
            public string status { get; set; }
        }

        public class CommentModel
        {
            public long userId { get; set; }
            public long taskid { get; set; }
            public string comments { get; set; }
        }

        public class RemoveComment
        {
            public long userId { get; set; }
            public long taskCommentId { get; set; }
        }
        public class RemoveAttachment
        {
            public long userId { get; set; }
            public long taskAttachmentId { get; set; }
        }

        public class AttachmentModel
        {
            public long userId { get; set; }
            public IFormFile file { get; set; }
            public string attachmentname { get; set; }
            public long taskid { get; set; }
        }

        public class TaskAndSubtaskStatusChangeModel
        {
            public long id { get; set; }
            public string status { get; set; }
            public long userId { get; set; }
        }

        public class VeryfyTaskAndSubtaskModel
        {
            public long userId { get; set; }
            public long accessId { get; set; }
            public long roleId { get; set; }
            public long id { get; set; }
           // public string status { get; set; }
            public decimal score{ get; set; } 
            
        }

        public class taskviewmodel
        {
            public TaskFilterVisibility filterVisibility { get; set; }

            public string[] TodoStatusName { get; set; }
            public long[] TodoPriority { get; set; }
            public long[] employeeid { get; set; }
            public long[] departmentid { get; set; }
            public long? datetype { get; set; }
            public string f_date { get; set; }
            public string t_date { get; set; }
            public string m_date { get; set; }
            //public IEnumerable<TodoPriority> Priorities { get; set; }
            public IEnumerable<commonCls> employeecls { get; set; }
            public IEnumerable<CommonMasterModel> departments { get; set; }
            public viewalltaskclass alltask { get; set; }
            //  public IEnumerable<commonCls> assigntoemployeecls { get; set; }
            // public IEnumerable<commonCls> assignbydepartmentcls { get; set; }
            // public Mytaskscls tasks { get; set; }


        }
        public class TaskFilterVisibility
        {
            public bool employee { get; set; }
            public bool department { get; set; }
        }




        public class CreateTaskResponseModel : BaseModel
        {
            public long Id { get; set; }
        }
        public class EditTaskResponseModel : BaseModel
        {
            public long Id { get; set; }
        }
        //public class Todo
        //{
        //    public long ID { get; set; }
        //    public DateTime InsertedDate { get; set; }
        //    public DateTime ModifiedDate { get; set; }
        //    public long InsertedId { get; set; }
        //    public long ModifiedId { get; set; }
        //    public bool Active { get; set; }
        //    public string Status { get; set; }
        //    public bool IsAvailable { get; set; }

        //    public long EmployeeID { get; set; }
        //    public long AssignByDepartmentID { get; set; }
        //    public long DepartmentID { get; set; }
        //    public long TodoPriorityID { get; set; }
        //    public long TodoStatusID { get; set; }
        //    public string Name { get; set; }
        //    public string Description { get; set; }
        //    public DateTime? DueDate { get; set; }
        //    public DateTime? AssignedDate { get; set; }
        //    public DateTime? CompletionDate { get; set; }
        //    public DateTime? VerifiedDate { get; set; }
        //    public long AssignedId { get; set; }
        //    public long ParentId { get; set; }
        //    public bool IsRead { get; set; }
        //}
        public class TodoModels
        {
            public long userId { get; set; }
            public long ID { get; set; }
            public string Name { get; set; }
            public long TodoPriorityID { get; set; }
            public long? DepartmentID { get; set; }
            public long? AssignByDepartmentID { get; set; }
            public long? AssignedId { get; set; }        
            public DateTime? DeadLineDate { get; set; }
            public string Description { get; set; }

            //For edit Purpose
         
            public string TodoStatusName { get; set; }
          //  public long EmployeeID { get; set; }

            //public long AssignedEmployeeID { get; set; } 



        }
        public class TodoCreateModels
        {
            public long userId { get; set; }           
            public string taskName { get; set; }
            public long todoPriorityID { get; set; }
            public long? departmentID { get; set; }
            public long? assignedId { get; set; }
            public long? assignByDepartmentID { get; set; }
            public DateTime? deadLineDate { get; set; }
            public string description { get; set; }
        }

        public class TodoCommonModel
        {
            public long ID { get; set; }
            public string Name { get; set; }

        }

      

        public class TodoStatusModel
        {
            public string ID { get; set; }
            public string Name { get; set; }

        }
        public class TodoStatusResposnse : BaseModel
        {
           public List<TodoStatusModel> StatusListModel { get; set; }

         
        }
        public class TodoPriorityResposnse : BaseModel
        {
            public List<TodoCommonModel> PriorityListModel { get; set; }


        }


        public class TodoBaseResponse : BaseModel
        {
            public List<TodoCommonModel> todoCommonModels { get; set; }
          
        }

        public class TodoModelResponse : BaseModel
        {
            public List<TodoCommonModel> todoPriorityModel { get; set; }
            public List<TodoStatusModel> todoStatusModel { get; set; }
            public List<commonClsModel> employeeModel { get; set; }
            
        }


        public class TodoCommentResponse : BaseModel
        {          
            public List<todocommentsModel> todoCommentModel { get; set; }
        }


        public class TodoTaskModel
        {
            public long TodoID { get; set; }
            public long ParentID { get; set; }
            public long EmployeeID { get; set; }
            public string EmployeeName { get; set; }
            public long AssignByDepartmentID { get; set; }
            public string AssignByDepartmentName { get; set; }
            public long DepartmentID { get; set; }
            public string DepartmentName { get; set; }
            public long TodoPriortityID { get; set; }
            public string TodoPriortityName { get; set; }

            public long TodoStatusID { get; set; }
            public string TodoStatusName { get; set; }
            public string TaskName { get; set; }
            public string Description { get; set; }
            public string DueDate { get; set; }
            public string AssignDate { get; set; }
            public string AssignTime { get; set; }
            
        }
        public class TodoTaskResponse : BaseModel
        {
            public List<TodoStatusModel> todoStatusModel { get; set; }
            public List<TodoCommonModel> todoPriorityModel { get; set; }         
            public List<SubTaskList_clsModel> subTaskModel { get; set; }
        }

        public class TodoDepartmentModel
        {
            public long DepartmentID { get; set; }
            public long GroupID { get; set; }
            public string Name { get; set; }

        }
        public class TodoDepartmentResponse : BaseModel
        {
            public List<TodoDepartmentModel> todoDepartmentModel { get; set; }
        }



        public class TodoDetailsModel
        {

            public long TodoID { get; set; }
            
            public string Name { get; set; }
           
            public string Description { get; set; }
            public DateTime? AssignedOn { get; set; }
            public DateTime? DueDate { get; set; }
            public long TodoPriorityID { get; set; }
            public string PriorityName { get; set; }
            public long TodoStatusID { get; set; }
            public string TodoStatusName { get; set; }
          
         

            public long AssignedEmployeeID { get; set; }
            public string AssignedEmployeeName { get; set; }
            public long ParentID { get; set; }
          
            public long AssignByDepartmentID { get; set; }
            public string AssignedDepartmentName { get; set; }
             public long EmployeeID { get; set; }
            public string EmployeeName { get; set; }
            public long DepartmentID { get; set; }
            public string DepartmentName { get; set; }
         
       
            public DateTime? CompletionDate { get; set; }

         
         
        }
        public class AllTodoResponseModel : BaseModel
        {
           
            public List<TodoDetailsModel> todoResponseModel { get; set; }
        }

        public class viewModel
        {
            public int totaltask { get; set; }
            public int Pendingtask { get; set; }
            public int onholdtask { get; set; }
            public int inprogresstask { get; set; }
            public int Completedtask { get; set; }
            public int Verifiedtask { get; set; }
        }
        public class SubTaskList_clsModel
        {
            public long todoid { get; set; }
            public string todoname { get; set; }
            public string tododescription { get; set; }
            public string status { get; set; }
            public string AssignedDate { get; set; }
            public string DueDate { get; set; }
            public string assignedto { get; set; }
            public string assignedby { get; set; }
            public string profile { get; set; }
            public long TodoPriorityID { get; set; }

            public string priorityName { get; set; }
          
            public long DepartmentID { get; set; }
            public string department { get; set; }
           
            public long AssignByDepartmentID { get; set; }
            public string AssignByDepartmentName { get; set; }
            public long AssignToID { get; set; }
            public long AssignByID { get; set; }
            public DateTime? AssignDate { get; set; }
            public (string Date, string Time, string DateTime) AssignDateTime { get; set; }
            public string assigntime { get; set; }
           
            public long parentID { get; set; }           
          
          
            public string completionDt { get; set; }
            public string VerifyDt { get; set; }
            public (string Date, string Time, string DateTime) InsertedDateTime { get; set; }
            //public string tag { get; set; }
            public DateTime? ModifiedDate { get; set; }
            public (string Date, string Time, string DateTime) ModifiedDateTime { get; set; }
            public int commentcount { get; set; }
            public decimal? Score { get; set; }
            public long? ScoreGivenId { get; set; }
            public DateTime? ScoreGivenDate { get; set; }
            public (string Date, string Time, string DateTime) ScoreGivenDateTime { get; set; }
            public int subtaskCount { get; set; }
            public List<SubTaskList_clsModel> subtasks { get; set; } 

        }

      
        public class EmployeeModel
        {
            public long ID { get; set; }
            public string EmpCode { get; set; } 
            public string FirstName { get; set; } 
            public string MiddleName { get; set; } 
            public string LastName { get; set; }
            public string FullName { get; set; }
            public string Gender { get; set; } 
            public Nullable<DateTime> DateOfBirth { get; set; } 
            public Nullable<long> BloodGroupID { get; set; } 
            public string BloodGroupName { get; set; }
            public string PrimaryMobile { get; set; } 
            public string AlternativeMobile { get; set; } 
            public string LandlineNumber { get; set; } 
            public string EmailId { get; set; } 
            public string Image { get; set; } 
            public Nullable<long> NatiobalityID { get; set; }
            public string NationalityName { get; set; }
            public Nullable<long> ReligionID { get; set; }
            public string ReligionName { get; set; }
            public long EmployeeGroupID { get; set; } 
            public string MaritalStatus { get; set; } 
            public Nullable<bool> Isleft { get; set; } 
            public Nullable<DateTime> LeftDate { get; set; }
        }

        public class todocommentsModel
        {
            public long id { get; set; }
            public long TodoID { get; set; }
            public string Description { get; set; }
            public string EmployeeName { get; set; }
            public long EmployeeID { get; set; }
            public string postedON { get; set; }
            public string profile { get; set; }
            public DateTime insertedDate { get; set; }
            public (string Date, string Time, string DateTime) insertedDateTime { get; set; }
            public DateTime modifiedDate { get; set; }
            public (string Date, string Time, string DateTime) modifiedDateTime { get; set; }
        }

        public class todoTimelinesModel
        {
            public long id { get; set; }
            public long TodoID { get; set; }
            public DateTime InsertedDate { get; set; }
            public (string Date, string Time, string DateTime) insertedDateTime { get; set; }
            public (string Date, string Time, string DateTime) modifiedDateTime { get; set; }
            public DateTime? RelatedDate { get; set; }
            public long RelatedId { get; set; }
            public string Status { get; set; }
            public string TodoStatus { get; set; }
            public string RelatedName { get; set; }
            public string insertedBy { get; set; }
        }
        public class todoAttachmentModel
        {
            public long id { get; set; }
            public long TodoID { get; set; }
            public string AttachmentName { get; set; }
            public string Attachmenturl { get; set; }
            public long employeeid { get; set; }
            public string employeeName { get; set; }         
            public DateTime insertedDate { get; set; }
            public DateTime modifiedDate { get; set; }
            public (string Date, string Time, string DateTime) insertedDateTime { get; set; }
            public (string Date, string Time, string DateTime) modifiedDateTime { get; set; }
        }
        public class TaskAssigneToOtherResponseModel : BaseModel
        {

            public List<SubTaskList_clsModel> SubTaskAssignResponseModel { get; set; }
        }

        public class TaskAssigneResponseModel : BaseModel
        {
            public List<TodoStatusModel> todoStatus { get; set; }
            public List<TodoCommonModel> todoPriority { get; set; }
            public List<TodoCommonModel> employeeList { get; set; }
            public List<SubTaskList_clsModel> taskAssignResponseModel { get; set; }
            public SubTaskList_clsModel taskDetailsModel { get; set; }
            public List<SubTaskList_clsModel> SubtaskDetailsModel { get; set; }
            public List<todocommentsModel> taskCommentsModel { get; set; }
            public List<todoTimelinesModel> taskTimelinesModel { get; set; }
            public List<todoAttachmentModel> taskAttachmentsModel { get; set; }
            public IEnumerable<EmployeeModel> employeeModels { get; set; }
            // public IEnumerable<subtasklist_cls> taskList { get; set; }
        }

        public class TaskDetailsResponseModel : BaseModel
        {          
            public SubTaskList_clsModel taskDetailsModel { get; set; }
            public SubTaskList_clsModel SubtaskDetailsModel { get; set; }
            public todocommentsModel taskCommentsModel { get; set; }
            public todoTimelinesModel taskTimelinesModel { get; set; }
            public todoAttachmentModel taskAttachmentsModel { get; set; }          
           
        }

        public class TaskResponseModel : BaseModel
        {
            public SubTaskList_clsModel taskDetailsModel { get; set; }
        }
        public class SubTaskResponseModel : BaseModel
        {
            public SubTaskList_clsModel SubtaskDetailsModel { get; set; }
            public todocommentsModel taskCommentsModel { get; set; }
            public todoTimelinesModel taskTimelinesModel { get; set; }
            public todoAttachmentModel taskAttachmentsModel { get; set; }
        }

        public class TaskDashboardclsModel
        {
            public int TotalMyTasks { get; set; }
            public int PendingMyTasks { get; set; }
            public int PendingMyTasksPer { get; set; }
            public int onholdMyTasks { get; set; }
            public int onholdMyTasksPer { get; set; }
            public int inprogressMyTasks { get; set; }
            public int inprogressMyTasksPer { get; set; }
            public int CompletedMyTasks { get; set; }
            public int CompletedMyTasksPer { get; set; }
            public int VerifiedMyTasks { get; set; }
            public int VerifiedMyTasksPer { get; set; }
            //2nd phase
            public int TotalTaskother { get; set; }
            public int PendingTaskother { get; set; }
            public int PendingTaskotherPer { get; set; }
            public int onholdTaskother { get; set; }
            public int onholdTaskotherPer { get; set; }
            public int inprogressTaskother { get; set; }
            public int inprogressTaskotherPer { get; set; }
            public int CompletedTaskother { get; set; }
            public int CompletedTaskotherPer { get; set; }
            public int VerifiedTaskother { get; set; }
            public int VerifiedTaskotherPer { get; set; }

            //3rd phase
            public int TotalTasktome { get; set; }
            public int PendingTasktome { get; set; }
            public int PendingTasktomePer { get; set; }
            public int onholdTasktome { get; set; }
            public int onholdTasktomePer { get; set; }
            public int inprogressTasktome { get; set; }
            public int inprogressTasktomePer { get; set; }
            public int CompletedTasktome { get; set; }
            public int CompletedTasktomePer { get; set; }
            public int VerifiedTasktome { get; set; }
            public int VerifiedTasktomePer { get; set; }
        }
        public class TaskDashboardclsResponseModel : BaseModel
        {
            public TaskDashboardclsModel TaskDashboardModel { get; set; }
         
        }

        public class NewTask : BaseModel
        {
            public List<TodoCommonModel> todoPriority { get; set; }
            public List<TodoStatusModel> todoStatus { get; set; }
            public List<TodoDepartmentModel> todoDepartmentModel { get; set; }
            public List<commonClsModel> employeeModel { get; set; }
          //  public List<EmployeeModel> Allemployees { get; set; }
        
        } 

        public class VeryfyTaskAndSubTask : BaseModel
        {
            public bool result { get; set; } 
        }

       
        public class TaskTimeLineResponseModel : BaseModel
        {         
            public List<todoTimelinesModel> taskTimelinesModel { get; set; }           
        }
        public class TaskAttachmentResponseModel : BaseModel
        {          
            public List<todoAttachmentModel> taskAttachmentsModel { get; set; }
        }

        public class commonClsModel
        {
          
            public long id { get; set; }
            public string name { get; set; }
            public string profile { get; set; }
        }
        public class assignToMeModel
        {
            public long accessId { get; set; }
            public long roleId { get; set; }
            public long employeeid { get; set; }
            public long[] TodoPriority { get; set; }
            public string[] TodoStatusName { get; set; }
            public Nullable<DateTime> FromDate { get; set; }
            public Nullable<DateTime> ToDate { get; set; }

        }
        public class assignToOtherModel
        {
            public long accessId { get; set; }
            public long roleId { get; set; }
            public long employeeid { get; set; }
            public long[] employe { get; set; }
            public long[] TodoPriority { get; set; }
            public string[] TodoStatusName { get; set; }
            public Nullable<DateTime> FromDate { get; set; }
            public Nullable<DateTime> ToDate { get; set; }
        }
       
        public class myTaskModel
        {
            public long accessId { get; set; }
            public long roleId { get; set; }
            public long employeeid { get; set; }
            public long[]  TodoPriority { get; set; }
            public string[] TodoStatusName { get; set; }
            public Nullable<DateTime> FromDate { get; set; }
            public Nullable<DateTime> ToDate { get; set; }

        }
        public class allTaskModel
        {
            public long userId { get; set; }
            public long accessId { get; set; }
            public long roleId { get; set; }
            public string[] TodoStatusName { get; set; }
            public long[] TodoPriority { get; set; }
            public long[] employeeid { get; set; }

            public long[] departmentid { get; set; }
            public long? datetype { get; set; }
            public string FromDate { get; set; }
            public string ToDate { get; set; }
            public string Month { get; set; }

        }
    }

}
