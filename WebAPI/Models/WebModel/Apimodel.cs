﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Apimodel
    {
        private readonly IHostingEnvironment hostingEnvironment;
        public Apimodel(IHostingEnvironment hostingEnv)
        {
            hostingEnvironment = hostingEnv;
        }
        public string uploadsupportfile(IFormFile file)
        {
            try
            {
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "File" + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath+@"/wwwroot/ODMImages/Tempfile/" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                return pathToSave;
            }
            catch {
                return null;
            }
        }
    }
}
