﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class AllTaskModel
    {
        public long Id { get; set; }
        public TodoModel toDoModel { get; set; }

        public List<TodoModel> toDoCls { get; set; }
    }
}
