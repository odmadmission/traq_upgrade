﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
namespace WebAPI.Models
{
    public class CommonTaskModel
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime ModifiedDate { get; set; }

    }
    public class WorkViewModel
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Title")]
        public string Title { get; set; }
        public string Description { get; set; }

    }


    public class WorkSectionViewModel
    {
       
        public long WorkID { get; set; }
        [Required(ErrorMessage = "Please Select Work")]
        [Display(Name = "Work")]
        public string WorkTitle { get; set; }

        public long ID { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        public string Description { get; set; }


        public DateTime InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public bool Active { get; set; }
        public IEnumerable<WorkViewModel> works { get; set; }

    }

    public class TaskViewModel
    {

        public long WorkID { get; set; }
       
        [Display(Name = "Work")]
        public string WorkTitle { get; set; }


        public long WorkSectionID { get; set; }

        [Display(Name = "Work Section")]
        public string WorkSectionName { get; set; }

        public long ID { get; set; }

        [Required(ErrorMessage = "Please Enter Title")]
        public string Title { get; set; }
        public string Description { get; set; }


        public DateTime InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public bool Active { get; set; }
        

    }
}
