﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Entities.SupportAggregate;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class StudentModelClass
    {
        public long ID { get; set; }
        [Display(Name = "Student Code")]
        [Required(ErrorMessage = "Please Enter Student Code")]
        public string StudentCode { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please Enter Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Please Enter Gender")]
        public string Gender { get; set; }
        [Display(Name = "Date Of Birth")]
        [Required(ErrorMessage = "Please Enter Date Of Birth")]
        public DateTime DateOfBirth { get; set; }
        [Display(Name = "Blood Group")]
        [Required(ErrorMessage = "Please Enter Blood Group")]
        public long BloodGroupID { get; set; }
        public string BloodGroupName { get; set; }
        public IEnumerable<BloodGroup> bloodGroups { get; set; }
        [Display(Name = "Nationality")]
        [Required(ErrorMessage = "Please Enter Nationality")]
        public long NatiobalityID { get; set; }
        public string NatiobalityName { get; set; }
        public IEnumerable<NationalityType> nationalityTypes { get; set; }
        [Display(Name = "Religion")]
        [Required(ErrorMessage = "Please Enter Religion")]
        public long ReligionID { get; set; }
        public string ReligionName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public List<documentTypesCls> documentTypesCls { get; set; }
        public IEnumerable<ReligionType> religionTypes { get; set; }
        public IEnumerable<StudentAdressCls> studentAdresses { get; set; }
        public IEnumerable<StudentParentsCls> studentParentsCls { get; set; }
        public StudentClassDetails studentClassDetails { get; set; }
        public IEnumerable<EmployeeAccesscls> accesscls { get; set; }
        public List<GivenAccesscls> employeeModuleSubModuleAccess { get; set; }

    }
    public class StudentAdressCls
    {
        public long StudentAddressId { get; set; }
        public long StudentId { get; set; }
        public long ID { get; set; }
        [Display(Name = "Address Line1")]
        [Required(ErrorMessage = "Please Enter Address Line1")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line2")]
        [Required(ErrorMessage = "Please Enter Address Line2")]
        public string AddressLine2 { get; set; }
        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Please Enter Postal Code")]
        public string PostalCode { get; set; }
        [Display(Name = "City")]
        [Required(ErrorMessage = "Please Select City")]
        public long CityID { get; set; }
        [Display(Name = "State")]
        [Required(ErrorMessage = "Please Select State")]
        public long StateID { get; set; }
        [Display(Name = "Country")]
        [Required(ErrorMessage = "Please Select Country")]
        public long CountryID { get; set; }
        [Display(Name = "Address Type")]
        [Required(ErrorMessage = "Please Select Address Type")]
        public long AddressTypeID { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string AddressTypeName { get; set; }
        public IEnumerable<Country> countries { get; set; }
        public IEnumerable<State> states { get; set; }
        public IEnumerable<City> cities { get; set; }
        public IEnumerable<AddressType> addressTypes { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
    }
    public class StudentParentsCls
    {
        public long ID { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please Enter Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Primary Mobile")]
        public string PrimaryMobile { get; set; }
        [Display(Name = "Alternative Mobile")]
        public string AlternativeMobile { get; set; }
        [Display(Name = "Landline Number")]
        public string LandlineNumber { get; set; }
        [Display(Name = "Email Id")]
        public string EmailId { get; set; }
        [Display(Name = "Profession Type")]
        public long ProfessionTypeID { get; set; }
        public string ProfessionTypeName { get; set; }
        public List<CommonMasterModel> ProfessionTypes { get; set; }
        public long StudentID { get; set; }
        [Display(Name = "Parent Relationship")]
        public long ParentRelationshipID { get; set; }
        public string ParentRelationshipName { get; set; }
        public string Imagepath { get; set; }
        public List<CommonMasterModel> ParentRelationships { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
    public class StudentClassDetails
    {
        public long ID { get; set; }
        public long StudentID { get; set; }
        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Standard")]
        public long StandardID { get; set; }
        public string StandardName { get; set; }
        public List<Standard> standards { get; set; }
        [Display(Name = "Section")]
        [Required(ErrorMessage = "Please Select Section")]
        public long SectionID { get; set; }
        public string SectionName { get; set; }
        public List<Section> sections { get; set; }
        [Display(Name = "Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long WingID { get; set; }
        public string WingName { get; set; }
        public List<Schoolwingcls> wings { get; set; }
        [Display(Name = "Organisation")]
        [Required(ErrorMessage = "Please Select Organisation")]
        public long OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public List<Organization> organizations { get; set; }
        [Display(Name = "Current Standard")]
        public bool IsCurrent { get; set; }
        [Display(Name = "Board")]
        [Required(ErrorMessage = "Please Select Board")]
        public long BoardID { get; set; }
        public string BoardName { get; set; }
        public List<Board> boards { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
    public class Schoolwingcls : BaseModel
    {
        public long ID { get; set; }
        [Display(Name = "Organisation")]
        [Required(ErrorMessage = "Please Select Organisation")]
        public long OrganizationAcademicID { get; set; }
        [Display(Name = "Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long WingID { get; set; }
        public long[] MultiWingID { get; set; }
        public string OrganisationName { get; set; }
        public string wingName { get; set; }
        public List<Organization> organizations { get; set; }
        public List<Wing> wings { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long AcademicSessionId { get; set; }
        public long OrganisationID { get; set; }
        public long OrganisationIDByAcademic { get; set; }
        public string OrganisationNameByAcademic { get; set; }



    }
    public class supporttimelinecls
    {
        public string Description { get; set; }
        public string commentedBy { get; set; }
        public string commentedByName { get; set; }
        public string insertedOn { get; set; }
        public IEnumerable<SupportAttachment> ResolveAttachments { get; set; }
        public string StatusName { get; set; }
    }
    public class cls_supportrequest_details
    {
        public (bool Sent, string SentDate, string SentName, bool Approved, string ApprovedDate, string ApprovedName, bool Rejected, string RejectedDate,
          string RejectedName) Management
        { get; set; }
        public List<supporttimelinecls> supporttimelinecls { get; set; }
        public long ID { get; set; }
        public long BuildingID { get; set; }
        public string BuildingName { get; set; }
        public string Comment { get; set; }
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }

        public string ElapsedTime { get; set; }

        public string Description { get; set; }
        public long LocationID { get; set; }
        public string LocationName { get; set; }
        public long PriorityID { get; set; }
        public string PriorityName { get; set; }
        public string RequestedCompletionDate { get; set; }
        public long RoomID { get; set; }
        public string RoomName { get; set; }
        public string StatusName { get; set; }
        public long SubCategoryID { get; set; }
        public string SubCategoryName { get; set; }
        public long SupportTypeID { get; set; }
        public string SupportTypeName { get; set; }
        public long RequestEmployeeID { get; set; }
        public string RequestOn { get; set; }

        public string ModifiedDateTime { get; set; }

        public string DeadlineDate { get; set; }
        public string ReqCompletionDate { get; set; }

        public string CompletionOn { get; set; }
        public string RequestEmployeeName { get; set; }
        public string RequestEmployeeCode { get; set; }
        public IEnumerable<SupportAttachment> RequestAttachments { get; set; }
        public IEnumerable<SupportAttachment> ResolveAttachments { get; set; }
        public List<supporttimelinecls> supporttimelinecomments { get; set; }

        public string TimelineComment { get; set; }
        public string Title { get; set; }
        public string TicketCode { get; set; }
    }
    public class StudentTypewithAddresscls
    {
        public long typeId { get; set; }
        public string typeName { get; set; }
        public StudentAdressCls studentAdress { get; set; }

    }
    public class AcademicStudentcls
    {
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long AcademicStandardId { get; set; }
        public bool StandardStreamType { get; set; }
        public string StandardName { get; set; }

        [Display(Name = "Oraganisation Academic")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }


        public string OraganisationAcademicName { get; set; }

        public string OraganisationName { get; set; }

        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        [Display(Name = "School Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }
        public long CountAcademicSection { get; set; }
        public long CountAcademicStandardStream { get; set; }
        public IEnumerable<AcademicSession> academicSessions { get; set; }
        public IEnumerable<Board> boards { get; set; }
        public long AcademicStandardStreamId { get; set; }
        public long[] StudentId { get; set; }
        public long AcademicSectionId { get; set; }
        public string[] RollNo { get; set; }
        public long[] AcademicStudentId { get; set; }
    }
    public class AcademicStudentViewModel : BaseModel
    {
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        [Display(Name = "BoardStandardId")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long StandardId { get; set; }
        public bool StandardStreamType { get; set; }

        public string StandardName { get; set; }

        [Display(Name = "OraganisationAcademicId")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }


        public string OraganisationAcademicName { get; set; }

        public string OraganisationName { get; set; }

        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }

        [Display(Name = "MultiBoardStandardId")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long[] MultiBoardStandardId { get; set; }

        [Display(Name = "SchoolWingId")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }
        public long CountAcademicSection { get; set; }
        public long CountAcademicStandardStream { get; set; }
        public string studentname { get; set; }
        public string studentcode { get; set; }
        public long StudentId { get; set; }
        public string streamName { get; set; }
    }
}
