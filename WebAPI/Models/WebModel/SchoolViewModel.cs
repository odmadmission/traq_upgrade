﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace WebAPI.Models
{
    public class BoardStandardViewModel : BaseModel
    {
        public long ID { get; set; }

        [Display(Name = "Board")]
        public long BoardID { get; set; }
        public string BoardName { get; set; }

        [Display(Name = "Standard")]
        public long StandardID { get; set; }
        public string StndardName { get; set; }

        [Display(Name = "Stream")]
        public long StreamID { get; set; }
        public string StreamName { get; set; }
        public string StandardName { get; set; }

        public long[] StandardId { get; set; }

        public long StreamCount { get; set; }


        public string ModifiedText { get; set; }
    }

    public class SubjectViewModel : BaseModel
    {
        public long ID { get; set; }

        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Please Enter Subject Name")]
        public string Name { get; set; }

        [Display(Name = "SubjectCode")]
        [Required(ErrorMessage = "Please Enter Subject Code")]
        public string SubjectCode { get; set; }

        [Display(Name = "IsSubSubject")]
        public bool IsSubSubject { get; set; }

        [Display(Name = "ParentSubjectId")]
        public long ParentSubjectId { get; set; }

        [Display(Name = "BoardStandard")]
        public long BoardStandardId { get; set; }
        public long BoardId { get; set; }
        public string BoardStandardName { get; set; }

        public long BoardStandardStreamId { get; set; }

        public string Subject { get; set; }
        public string StandardName { get; set; }
        public string BoardName { get; set; }
        public long CountChapter { get; set; }

        public bool IsOptional { get; set; }
    }
    public class ChapterViewModel : BaseModel
    {
        public long ID { get; set; }

        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Subject")]
        public long SubjectId { get; set; }
        public string SubjectName { get; set; }

        public string SubjectCode { get; set; }


        [Display(Name = "IsSubChapter")]
        public bool IsSubChapter { get; set; }

        [Display(Name = "ParentChapter")]
        public long ParentChapterId { get; set; }
        public string ParentChapterName { get; set; }
        public string Chapter { get; set; }

        public long BoardId { get; set; }
        public long BoardStandardId { get; set; }
        public long BoardStandardStreamId { get; set; }

        public long CountConcept { get; set; }

    }
    public class AcademicSessionModel : BaseModel
    {
        public long ID { get; set; }
        [Display(Name = "SessionName")]
        [Required(ErrorMessage = "Please Fill The Academic Session Name")]
        public string SessionName { get; set; }
    }
    public class OrganizationAcademicViewModel : BaseModel
    {

        [Display(Name = "AcademicSessionID")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long AcademicSessionID { get; set; }

        [Display(Name = "OrganisationID")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OrganisationID { get; set; }

        [Display(Name = "OrganisationName")]
        public string OrganisationName { get; set; }

        [Display(Name = "AcademicSessionName")]
        public string AcademicSessionName { get; set; }

        public long[] OrganisationId { get; set; }

        public long CountSchoolWing { get; set; }



    }

    public class AcademicSubjectViewModel : BaseModel
    {


        [Display(Name = "AcademicSessionID")]
        [Required(ErrorMessage = "Please Select AcademicSession")]
        public long AcademicSessionID { get; set; }

        [Display(Name = "SubjectID")]
        [Required(ErrorMessage = "Please Select Subject")]
        public long SubjectID { get; set; }

        public long[] SubjectIDArray { get; set; }


        [Display(Name = "Board")]

        public long BoardID { get; set; }

        [Display(Name = "Class")]

        public long ClassID { get; set; }

        [Display(Name = "Stream")]

        public long? StreamID { get; set; }

        [Display(Name = "SubjectName")]
        public string SubjectName { get; set; }

        [Display(Name = "AcademicSessionName")]
        public string AcademicSessionName { get; set; }

        public bool MapAll { get; set; }

        public string SubjectCode { get; set; }



        public (string BoardName, string ClassName, string StreamName) BoardClassStream { get; set; }



    }
    public class SyallbusViewModel : BaseModel
    {

        [Display(Name = "Titel")]
        [Required(ErrorMessage = "Please fill Up Titel")]
        public string Titel { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please fill Up Description")]
        public string Description { get; set; }


    }

    public class CalenderViewModel : BaseModel
    {


        [Display(Name = "AcademicSession")]
        public long AcademicSessionID { get; set; }

        [Display(Name = "Organization")]

        public long OrganizationID { get; set; }

        [Display(Name = "Wing")]
        public long WingID { get; set; }



    }

    public class BoardStandardStreamViewModel : BaseModel
    {
        [Display(Name = "BoardStandardId")]
        [Required(ErrorMessage = "Please Select Board Class")]
        public long BoardStandardId { get; set; }

        [Display(Name = "StreamId")]
        [Required(ErrorMessage = "Please Select Stream")]
        public long StreamId { get; set; }
        public long BoardId { get; set; }
        public long StandardId { get; set; }

        public string StreamName { get; set; }
        public string BoardStandardName { get; set; }

        public long BoardStandarStreamSubCnt { get; set; }
    }

    public class CoceptViewModel : BaseModel
    {


        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }


        public long SubjectId { get; set; }
        public string SubjectName { get; set; }

        public string SubjectCode { get; set; }


        [Display(Name = "IsSubConcept")]
        public bool IsSubConcept { get; set; }

        [Display(Name = "ParentConceptId")]
        public long ParentConceptId { get; set; }
        public string ParentConceptName { get; set; }
        public string Chapter { get; set; }

        public long BoardId { get; set; }
        public long BoardStandardId { get; set; }
        public long BoardStandardStreamId { get; set; }
        public long ChapterId { get; set; }
        public long CountConcept { get; set; }




    }


    public class AcademicStandardViewModel : BaseModel
    {
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        [Display(Name = "BoardStandardId")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long StandardId { get; set; }
        public bool StandardStreamType { get; set; }

        public string StandardName { get; set; }

        [Display(Name = "OraganisationAcademicId")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }


        public string OraganisationAcademicName { get; set; }

        public string OraganisationName { get; set; }

        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }

        [Display(Name = "MultiBoardStandardId")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long[] MultiBoardStandardId { get; set; }

        [Display(Name = "SchoolWingId")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }
        public long CountAcademicSection { get; set; }
        public long CountAcademicStandardStream { get; set; }

    }

    public class AcademicSectionViewModel : BaseModel
    {

        public long AcademicStandardId { get; set; }
        [Display(Name = "SectionId")]
        [Required(ErrorMessage = "Please Select Section ")]
        public long SectionId { get; set; }

        [Display(Name = "MultiSectionId")]
        [Required(ErrorMessage = "Please Select Section ")]
        public long MultiSectionId { get; set; }

        public string OraganisationAcademicName { get; set; }
        public string OraganisationName { get; set; }
        public long StandardId { get; set; }
        public string StandardName { get; set; }
        public long BoardStandardId { get; set; }

        public long OraganisationAcademicId { get; set; }
        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }

        public long AcademicStandardStreamId { get; set; }
        public string StreamName { get; set; }




    }
    public class AcademicStandardStreamViewModel : BaseModel
    {

        public long AcademicStandardId { get; set; }
        [Display(Name = "BoardStandardStreamId")]
        [Required(ErrorMessage = "Please Select Stream ")]
        public long BoardStandardStreamId { get; set; }

        [Display(Name = "MultiBoardStandardStreamId")]
        [Required(ErrorMessage = "Please Select Stream ")]
        public long MultiBoardStandardStreamId { get; set; }

        public string OraganisationAcademicName { get; set; }
        public string OraganisationName { get; set; }
        public long StreamId { get; set; }
        public string StreamName { get; set; }
        public long BoardStandardId { get; set; }

        public long OraganisationAcademicId { get; set; }
        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }

        public long CountSection { get; set; }
    }




    public class TeacherViewModel : BaseModel
    {
        public long EmpId { get; set; }
        public long OrganizationId { get; set; }
        public string EmpName { get; set; }
        public string EmpCode { get; set; }
        public long GroupId { get; set; }
        public long EmpType { get; set; }


    }

    public class AcademicTeacherViewModel : BaseModel
    {
        public long EmpId { get; set; }
        public long OrganizationId { get; set; }
        public string EmpName { get; set; }
        public string EmpCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }


    public class AcademicChapterViewModel : BaseModel
    {


        [Display(Name = "ChapterId")]
        [Required(ErrorMessage = "Please Select Chapter")]

        public long ChapterId { get; set; }

        [Display(Name = "ChapterId")]
        [Required(ErrorMessage = "Please Select Chapter")]

        public long[] ChapterArrayId { get; set; }
        public long AcademicSubjectId { get; set; }
        public long CountAcademicConcept { get; set; }

        public string ChapterName { get; set; }
        public string SubjectName { get; set; }
    }
    public class AcademicConceptViewModel : BaseModel
    {


        [Display(Name = "ConceptId")]
        [Required(ErrorMessage = "Please Select Concept")]

        public long ConceptId { get; set; }
        public long ChapterId { get; set; }

        [Display(Name = "ConceptArrayId")]
        [Required(ErrorMessage = "Please Select Concept")]

        public long[] ConceptArrayId { get; set; }
        public long AcademicSubjectId { get; set; }
        public long AcademicChapterId { get; set; }
        public long AcademicConceptId { get; set; }

        public string ChapterName { get; set; }

        public string ConceptName { get; set; }
        public string SubjectName { get; set; }

    }

    public class AcademicTeacherSubjectViewModel : BaseModel
    {
        [Display(Name = "BoardId")]
        [Required(ErrorMessage = "Please Select Board")]
        public long BoardId { get; set; }



        public long AcademicSessionId { get; set; }
        public long OrganisationAcademicId { get; set; }
        public long SchoolWingId { get; set; }
        public long AcademicStandardId { get; set; }
        public long BoardStandardId { get; set; }
        public long AcademicStandarStreamdId { get; set; }

        public long AcademicSectiondId { get; set; }

        public long AcademicSubjectId { get; set; }
        public long[] ArrayAcademicSubjectId { get; set; }
        public long AcademicSectionId { get; set; }

        [Display(Name = "TeacherId")]
        [Required(ErrorMessage = "Please Select Teacher")]
        public long TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string ChapterName { get; set; }
        public string SubjectName { get; set; }
        public long SubjectId { get; set; }

        public string SectionName { get; set; }
    }

    public class SessionTeacherViewModel : BaseModel
    {
        public long TeacherId { get; set; }
        public long OrganizationId { get; set; }
        public long WingId { get; set; }
    }

    public class AcademicClassTeacherViewModel : BaseModel
    { 
        public long StandardId { get; set; }
        public string Name { get; set; }
        public long SectionId { get; set; }
        public long OrganizationId { get; set; }
        public long WingId { get; set; }
        public long BoardId { get; set; }
        public long TeacherId { get; set; }
        public long AcademicSessionId { get; set; }
        public long OrganizationAcademicId { get; set; }
        public long SchoolWingId { get; set; }
        public long AcademicSectionId { get; set; }
        public long StreamId { get; set; }
        public long EmpId { get; set; }
        public string EmpFullName { get; set; }
        public long SessionId { get; set; }
        public string OrgName { get; set; }
        public string WingName { get; set; }
        public string BoardName { get; set; }
        public string StandardName { get; set; }
        public string SectionName { get; set; }
    }


}
