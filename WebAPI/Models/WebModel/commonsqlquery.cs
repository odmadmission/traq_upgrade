﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Query;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class commonsqlquery
    {
        public ApplicationDbContext context;
        public commonsqlquery(ApplicationDbContext cont)
        {
            context = cont;
        }
        public List<View_All_Academic_Student> Get_view_All_Academic_Students()
        {
            var res = context.view_All_Academic_Students.FromSql(@"use ODMERPDB
                        select a.ID as ID,c.OrganizationAcademicId as OraganisationAcademicId,
                        c.BoardStandardId as BoardStandardId,
                        f.Name as OraganisationName,
                        d.AcademicSessionId as AcademicSessionId,
                        e.DisplayName as SessionName,
                        g.StandardId as StandardId,
                        h.Name as StandardName,
                        h.StreamType as StandardStreamType,
                        c.SchoolWingId as SchoolWingId,
                        a.ModifiedDate as ModifiedDate,
                        j.Name as WingName,
                        a.StudentId as StudentId,
                        (b.FirstName+' '+b.LastName) as studentname,
                        b.StudentCode as studentcode,
                        k.Name as BoardName,
                        (case when (a.AcademicStandardStreamId is null or a.AcademicStandardStreamId=0) then '' else 
                        (select top(1) Name from Streams 
                        where Active=1 and ID=(select top(1) StreamId from BoardStandardStreams 
                        where Active=1 and ID=(select top(1) BoardStandardStreamId from AcademicStandardStreams where Active=1 and ID=a.AcademicStandardStreamId))) end) as streamName,
                        e.IsAvailable as IsAvailable
                         from AcademicStudents as a
                        inner join Students as b on a.StudentId=b.ID
                        inner join AcademicStandards as c on a.AcademicStandardId=c.ID
                        inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                        inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                        inner join Organizations as f on d.OrganizationId=f.ID 
                        inner join BoardStandards as g on c.BoardStandardId=g.ID
                        inner join Standards as h on g.StandardId=h.ID
                        inner join SchoolWing as i on c.SchoolWingId=i.ID
                        inner join Wing as j on i.WingID=j.ID
                        inner join Boards as k on g.BoardId=k.ID
                        where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1
                            and j.Active=1 and k.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Academic_Standard> Get_view_All_Academic_Standards()
        {
            var res = context.view_All_Academic_Standards.FromSql(@"use ODMERPDB
                            select c.ID as ID,
                            c.OrganizationAcademicId as OraganisationAcademicId,
                            c.BoardStandardId as BoardStandardId,
                            f.Name as OraganisationName,
                            d.AcademicSessionId as AcademicSessionId,
                            e.DisplayName as SessionName,
                            g.StandardId as StandardId,
                            h.Name as StandardName,
                            h.StreamType as StandardStreamType,
                            c.SchoolWingId as SchoolWingId,
                            c.ModifiedDate as ModifiedDate,
                            j.Name as WingName,
                            k.Name as BoardName,
                            e.IsAvailable as IsAvailable,
                            c.Status as Status,
                            (select COUNT(*) from AcademicSections where AcademicStandardId=c.ID and Active=1) as CountAcademicSection,
                            (select COUNT(*) from AcademicStandardStreams where AcademicStandardId=c.ID and Active=1) as CountAcademicStandardStream
                             from AcademicStandards as c
                            inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                            inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                            inner join Organizations as f on d.OrganizationId=f.ID 
                            inner join BoardStandards as g on c.BoardStandardId=g.ID
                            inner join Standards as h on g.StandardId=h.ID
                            inner join SchoolWing as i on c.SchoolWingId=i.ID
                            inner join Wing as j on i.WingID=j.ID
                            inner join Boards as k on g.BoardId=k.ID
                            where c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1
                            and j.Active=1 and k.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Academic_Student_Section> Get_view_All_Academic_Student_Sections()
        {
            var res = context.view_All_Academic_Student_Sections.FromSql(@"use ODMERPDB
                        select l.ID as ID,c.OrganizationAcademicId as OraganisationAcademicId,
                        c.BoardStandardId as BoardStandardId,
                        f.Name as OraganisationName,
                        d.AcademicSessionId as AcademicSessionId,
                        e.DisplayName as SessionName,
                        g.StandardId as StandardId,
                        h.Name as StandardName,
                        h.StreamType as StandardStreamType,
                        c.SchoolWingId as SchoolWingId,
                        a.ModifiedDate as ModifiedDate,
                        j.Name as WingName,
                        a.StudentId as StudentId,
                        (b.FirstName+' '+b.LastName) as studentname,
                        b.StudentCode as studentcode,
                        k.Name as BoardName,
                        (case when (a.AcademicStandardStreamId is null or a.AcademicStandardStreamId=0) then '' else 
                        (select top(1) Name from Streams 
                        where Active=1 and ID=(select top(1) StreamId from BoardStandardStreams 
                        where Active=1 and ID=(select top(1) BoardStandardStreamId from AcademicStandardStreams where Active=1 and ID=a.AcademicStandardStreamId))) end) as streamName,
                        e.IsAvailable as IsAvailable,
						l.AcademicStudentId as AcademicStudentId,
						l.AcademicSectionId as AcademicSectionId,
						l.StudentCode as RollNo,
						n.Name as SectionName
                         from AcademicSectionStudents as l
						inner join AcademicStudents as a on l.AcademicStudentId=a.ID
						inner join AcademicSections as m on l.AcademicSectionId=m.ID
						inner join Sections as n on m.SectionId=n.ID
                        inner join Students as b on a.StudentId=b.ID
                        inner join AcademicStandards as c on a.AcademicStandardId=c.ID
                        inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                        inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                        inner join Organizations as f on d.OrganizationId=f.ID 
                        inner join BoardStandards as g on c.BoardStandardId=g.ID
                        inner join Standards as h on g.StandardId=h.ID
                        inner join SchoolWing as i on c.SchoolWingId=i.ID
                        inner join Wing as j on i.WingID=j.ID
                        inner join Boards as k on g.BoardId=k.ID
                        where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1
                            and j.Active=1 and k.Active=1 and l.Active=1 and m.Active=1 and n.Active=1").AsNoTracking().ToList();
            return res;
        }
    }
}
