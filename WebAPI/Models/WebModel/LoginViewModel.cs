﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class LoginViewModel
    {
        public long ID { get; set; }

        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Fill The Type")]
        public string Type { get; set; }        

        [Display(Name = "Access")]
        public long AccessID { get; set; }

        [Display(Name = "FCM")]
        public string FCMID { get; set; }

        [Display(Name = "Data")]
        [Required(ErrorMessage = "Please Fill The Data")]
        public string Data { get; set; }
        
    }
}
