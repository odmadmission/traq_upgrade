﻿using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.GrievanceAggregate;
using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class GrievanceModalClass
    {
        private IGrievanceRepository grievanceRepository;
        private IEmployeeRepository employeeRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IRoleRepository roleRepository;
        private IParentRepository parentRepository;

        public GrievanceModalClass(IGrievanceRepository grievanceReposito, IEmployeeRepository employeeRepo, IStudentAggregateRepository studentAggregateRepo, IRoleRepository roleRepo, IParentRepository parentReposito)
        {
            grievanceRepository = grievanceReposito;
            employeeRepository = employeeRepo;
            studentAggregateRepository = studentAggregateRepo;
            roleRepository = roleRepo;
            parentRepository = parentReposito;
        }

        public IEnumerable<GrievanceViewClass> GetAllGrievanceForView()
        {
            try
            {
                var grievance = grievanceRepository.GetAllGrievance();
                var grievancetype = grievanceRepository.GetAllGrievanceType();
                var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievancecategory = grievanceRepository.GetAllGrievanceCategory();
                var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                var grievanceemployee = grievanceRepository.GetAllEmployeeGrievance();
                var roles = roleRepository.GetAllRole();
                var grievanceattachment = grievanceRepository.GetAllGrievanceAttachment();
                var grievancetimelines = grievanceRepository.GetAllGrievanceTimeline().Where(a => a.GrievanceStatusID == 4).ToList();

                IEnumerable<GrievanceViewClass> res = (from a in grievance
                                                       join b in grievancetype on a.GrievanceTypeID equals b.ID
                                                       join c in grievancestatus on a.GrievanceStatusID equals c.ID
                                                       join f in roles on a.GrievanceCategoryID equals f.ID
                                                       select new GrievanceViewClass
                                                       {
                                                           ID = a.ID,
                                                           GrievanceCategoryName = f.Name,
                                                           GrievanceTypeName = b.Name,
                                                           GrievanceStatusName = c.Name,
                                                           Code = a.Code,
                                                           Description = a.Description,
                                                           CompletionId = a.CompletionId,
                                                           CompletionName = a.CompletionId == 0 ? "" : getEmployeeById(a.CompletionId),
                                                           CompletionDate = a.CompletionDate,
                                                           VerificationDate = a.VerificationDate,
                                                           VerifiedId = a.VerifiedId,
                                                           VerifiedName = a.VerifiedId == 0 ? "" : getEmployeeById(a.VerifiedId),
                                                           RequestType = a.RequestType,
                                                           GrievancePriorityName = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.GrievancePriorityID).FirstOrDefault().Name,
                                                           GrievanceRelatedId = a.GrievanceRelatedId,
                                                           GrievanceRelatedName = a.GrievanceRelatedId == 0 ? "" : getRoleById(a.GrievanceCategoryID, a.GrievanceRelatedId),
                                                           attachments = grievanceattachment.Where(u => u.GrievanceID == a.ID && u.InsertedId == a.InsertedId).ToList(),
                                                           employeeattachments = grievanceattachment.Where(t => t.GrievanceID == a.ID && t.InsertedId == a.CompletionId).ToList(),
                                                           GrievanceCategoryID = f.ID,
                                                           GrievancePriorityID = a.GrievancePriorityID,
                                                           GrievanceStatusID = c.ID,
                                                           GrievanceTypeID = b.ID,
                                                           RelatedCode = getCodeById(a.GrievanceCategoryID, a.GrievanceRelatedId),
                                                           CreatedDate = a.InsertedDate,
                                                           RelatedMobile = getMobileById(a.GrievanceCategoryID, a.GrievanceRelatedId),
                                                           grievanceempcls = (from m in grievanceemployee
                                                                              where m.GrievanceID == a.ID

                                                                              select new GrievanceEmployeeClass
                                                                              {
                                                                                  AssignedByID = m.InsertedId,
                                                                                  AssignedOn = m.InsertedDate,
                                                                                  AssignedByName = getEmployeeById(m.InsertedId),
                                                                                  AssignedToName = getEmployeeById(m.EmployeeID),
                                                                                  EmployeeID = m.EmployeeID,
                                                                                  GrievanceID = m.GrievanceID


                                                                              }).FirstOrDefault(),
                                                           grievancecommentscls = getgrievancecommentscls(a.ID)
                                                       }).ToList().OrderByDescending(a => a.ID);
                return res;
            }
            catch(Exception e1)
            {
                return null;
            }
        }
        
        public List<grievancecommentscls> getgrievancecommentscls(long id)
        {
            try
            {
                var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievancecomments = grievanceRepository.GetAllGrievanceComment();
                var grievancecommentattachment = grievanceRepository.GetAllGrievanceCommentAttachment();
                var res = (from a in grievancecomments
                           join b in grievancestatus on a.GrievanceStatusID equals b.ID
                           where a.GrievanceID == id
                           select new grievancecommentscls {
                               Description=a.Description,
                               commentedBy=a.InsertedId,
                               commentedByName= getEmployeeById(a.InsertedId),
                               insertedOn=a.InsertedDate,
                               grievanceCommentAttachments = grievancecommentattachment.Where(m=>m.GrievanceCommentID==a.ID).ToList(),
                               GrievanceStatusID=a.GrievanceStatusID,
                               GrievanceStatusName=b.Name
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public string getEmployeeById(long id)
        {
            var a = employeeRepository.GetAllEmployee().Where(m => m.ID == id).FirstOrDefault();
            string empname = a.FirstName + " " + (a.MiddleName == null ? (a.LastName) : (a.MiddleName + " " + a.LastName));
            return empname;
        }

        //public string getStudentById(long id)
        //{
        //    var a = studentAggregateRepository.GetAllStudent().Where(m => m.ID == id).FirstOrDefault();
        //    string studname = a.FirstName + " " + a.MiddleName == null ? (a.LastName) : (a.MiddleName + " " + a.LastName);
        //    return studname;
        //}
        public string getRoleById(long roleid, long id)
        {
            var a = roleRepository.GetAllRole().Where(m => m.ID == roleid).FirstOrDefault();
            if (roleid == 5 || roleid == 6)
            {
                var stud = studentAggregateRepository.GetAllStudent().Where(m => m.ID == id).FirstOrDefault();
                string studname = stud.FirstName + " " + (stud.MiddleName == null ? (stud.LastName) : (stud.MiddleName + " " + stud.LastName));
                return studname;

            }
            else
            {
                var emp = employeeRepository.GetAllEmployee().Where(m => m.ID == id).FirstOrDefault();
                string empname = emp.FirstName + " " + (emp.MiddleName == null ? (emp.LastName) : (emp.MiddleName + " " + emp.LastName));
                return empname;
            }
            return "";
        }

        public string getCodeById(long roleid, long id)
        {
            var a = roleRepository.GetAllRole().Where(m => m.ID == roleid).FirstOrDefault();

            if (roleid == 5 || roleid == 6)
            {
                var stud = studentAggregateRepository.GetAllStudent().Where(m => m.ID == id).FirstOrDefault();
                string studcode = stud.StudentCode;
                return studcode;

            }
            else
            {
                var emp = employeeRepository.GetAllEmployee().Where(m => m.ID == id).FirstOrDefault();
                string empcode = emp.EmpCode;
                return empcode;
            }
            return "";

        }
        public string getMobileById(long roleid, long id)
        {
            var a = roleRepository.GetAllRole().Where(m => m.ID == roleid).FirstOrDefault();

            if (roleid == 5 || roleid == 6)
            {
                var par = parentRepository.GetAllParent().Where(m => m.ID == id).FirstOrDefault();
                string parno = par.PrimaryMobile;
                return parno;

            }
            else
            {
                var emp = employeeRepository.GetAllEmployee().Where(m => m.ID == id).FirstOrDefault();
                string empno = emp.PrimaryMobile;
                return empno;
            }
            return "";

        }

    }
    public class grievancecommentscls
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public long GrievanceStatusID { get; set; }
        public string GrievanceStatusName { get; set; }
        public long commentedBy { get; set; }
        public string commentedByName { get; set; }
        public DateTime insertedOn { get; set; }
        public List<GrievanceCommentAttachment> grievanceCommentAttachments { get; set; }

    }
    public class FilterGrievanceClass
    {
        public List<MyGrievanceClass> myGrievanceClasses { get; set; }
        public long categoryId { get; set; }
        public long typeId { get; set; }
        public DateTime? F_Date { get; set; }
        public DateTime? L_Date { get; set; }
        public List<GrievanceType> grievanceTypes { get; set; }
        public List<grievancecategorycls> grievancecategories { get; set; }
    }
    public class grievancecategorycls
    {
        public long id { get; set; }
        public string name { get; set; }
    }
    public class MyGrievanceClass
    {
        public long ID { get; set; }
        [Display(Name = "Grievance ID")]
        public string Code { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please Enter Description")]
        public string Description { get; set; }
        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Select Type")]
        public long GrievanceTypeID { get; set; }
        public string GrievanceTypeName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public IEnumerable<GrievanceType> types { get; set; }
        public string status { get; set; }
        public long GrievanceCategoryID { get; set; }
        public string GrievanceCategory { get; set; }
        public long GrievancePriorityID { get; set; }
        public string Priority { get; set; }
        public long GrievanceRelatedId { get; set; }
        public long GrievanceStatusID { get; set; }
        public List<GrievanceAttachment> grievanceAttachments { get; set; }
    }
    public class grievanceattachments
    {
        public long id { get; set; }
        public string path { get; set; }
        public string Name { get; set; }
        public string file { get; set; }

    }


    public class GrievanceTypeClass
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Role")]
        [Required(ErrorMessage = "Please Select Role")]
        public long RoleID { get; set; }
        public string RoleName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public IEnumerable<Role> roles { get; set; }
    }

    public class GrievanceViewClass
    {
        public long ID { get; set; }
        public long GrievanceCategoryID { get; set; }

        public string GrievanceCategoryName { get; set; }
        public long GrievanceTypeID { get; set; }

        public string GrievanceTypeName { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
        public long GrievanceStatusID { get; set; }

        public string GrievanceStatusName { get; set; }
        public DateTime? CompletionDate { get; set; }
        public long CompletionId { get; set; }
        public string CompletionName { get; set; }

        public DateTime? VerificationDate { get; set; }

        public long VerifiedId { get; set; }

        public string VerifiedName { get; set; }
        public string RequestType { get; set; }

        public long GrievancePriorityID { get; set; }
        public string GrievancePriorityName { get; set; }

        public long GrievanceRelatedId { get; set; }
        public string GrievanceRelatedName { get; set; }
        public string RelatedCode { get; set; }
        public string RelatedMobile { get; set; }

        public DateTime? CreatedDate { get; set; }

        public GrievanceEmployeeClass grievanceempcls { get; set; }

        public List<GrievanceAttachment> attachments { get; set; }
        public List<GrievanceAttachment> employeeattachments { get; set; }
        public List<GrievanceTimeline> grievanceTimelines { get; set; }
        public List<grievancecommentscls> grievancecommentscls { get; set; }
    }

    public class GrievanceEmployeeClass
    {
        public long EmployeeID { get; set; }

        public long GrievanceID { get; set; }

        public long AssignedByID { get; set; }
        public string AssignedByName { get; set; }

        public string AssignedToName { get; set; }

        public DateTime? AssignedOn { get; set; }


    }


    public class GrievancePublicFormCls
    {
        public long ID { get; set; }

        public long EmployeeID { get; set; }

        public long GrievanceID { get; set; }

        public long StudentID { get; set; }

        public string EmpCode { get; set; }

        public string StudentCode { get; set; }
        public string EmployeeName { get; set; }

        public string StudentName { get; set; }

        public string ParentName { get; set; }
        public string Organisation { get; set; }

        public string Department { get; set; }
        public string Description { get; set; }

        public List<GrievanceAttachment> attachments { get; set; }
    }
    public class publicgrievancecls
    {
        [Display(Name ="Staff Name")]
        [Required(ErrorMessage ="Enter Staff Name")]
        public string staffname { get; set; }
        [Display(Name = "Staff Code")]
        [Required(ErrorMessage = "Enter Staff Code")]
        public string staffcode { get; set; }
        [Display(Name = "Student Name")]
        [Required(ErrorMessage = "Enter Student Name")]
        public string studentname { get; set; }
        [Display(Name = "Student Id")]
        [Required(ErrorMessage = "Enter Student Id")]
        public string studentcode { get; set; }
        [Display(Name = "Grievance ID")]
        public string Code { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please Enter Description")]
        public string Description { get; set; }
        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Select Type")]
        public long GrievanceTypeID { get; set; }
        public string GrievanceTypeName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public IEnumerable<GrievanceType> types { get; set; }
        public string status { get; set; }
        public long GrievanceCategoryID { get; set; }
        public string GrievanceCategory { get; set; }
        public long GrievancePriorityID { get; set; }
        public string Priority { get; set; }
        public long GrievanceRelatedId { get; set; }
        public long GrievanceStatusID { get; set; }
    }
    public class staffGrievanceDetails
    {
        public long EmployeeId { get; set; }
        public string Empcode { get; set; }
        public string EmployeeName { get; set; }
        public IEnumerable<EmployeeDesignationCls> employeeDesignations { get; set; }
        [Display(Name = "Grievance and Solution Required")]
        [Required(ErrorMessage = "Please Enter Grievance and Solution Required")]
        public string Description { get; set; }
        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Select Type")]
        public long GrievanceTypeID { get; set; }
        public string GrievanceTypeName { get; set; }
        public IEnumerable<GrievanceType> types { get; set; }
    }
    public class studentGrievanceDetails
    {
        public long StudentId { get; set; }
        public string Studentcode { get; set; }
        public string StudentName { get; set; }
       public string school { get; set; }
        public string wing { get; set; }
        public string standard { get; set; }
        public string section { get; set; }
        [Display(Name = "Grievance and Solution Required")]
        [Required(ErrorMessage = "Please Enter Grievance and Solution Required")]
        public string Description { get; set; }
        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Select Type")]
        public long GrievanceTypeID { get; set; }
        public string GrievanceTypeName { get; set; }
        public IEnumerable<GrievanceType> types { get; set; }
    }
}
