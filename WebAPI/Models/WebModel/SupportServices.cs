﻿using Microsoft.Extensions.Hosting;
//using OdmErp.Web.Models;
//using OdmErp.Web.Areas.Tasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Models;

namespace OdmErp.WebAPI.Models
{
   public class SupportServices
    {
        private CommonMethods commonMethods;
        private smssend smssend;
        public SupportServices(CommonMethods common, smssend sms)
        {
            commonMethods = common;
            smssend = sms;
        }
        
        public void Sendweeklymailsupportreport()
        {
            try
            {
                DateTime givenDate = DateTime.Today;
                DateTime startOfWeek = givenDate.AddDays(-(int)givenDate.DayOfWeek);
                DateTime endOfWeek = startOfWeek.AddDays(7);
                var tasks = commonMethods.GetallTasksFormail().Where(a=>a.AssignDate>=startOfWeek && a.AssignDate<=endOfWeek).ToList();
                var employees = commonMethods.GetAllEmployees();
                foreach (var m in employees)
                {
                    if (m.EmailId != null)
                    {
                        var userId = m.ID;
                        TaskDashboardcls taskDashboardcls = new TaskDashboardcls();
                        taskDashboardcls.TotalMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0)).ToList().Count();
                        taskDashboardcls.PendingMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "VERIFY").ToList().Count();


                        taskDashboardcls.TotalTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId).ToList().Count();
                        taskDashboardcls.PendingTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "VERIFY").ToList().Count();


                        taskDashboardcls.TotalTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId).ToList().Count();
                        taskDashboardcls.PendingTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "VERIFY").ToList().Count();
                       // smssend.weeklytaskmail("Weekly Task Report",m.EmailId, taskDashboardcls);
                    }
                }
            }
            catch
            {

            }
        }
        public void Sendmonthlymailtaskreport()
        {
            try
            {
                DateTime givenDate = DateTime.Today;               
                DateTime startOfWeek = new DateTime(givenDate.Year, givenDate.Month, 1);
                DateTime endOfWeek = startOfWeek.AddMonths(1).AddDays(-1);
                var tasks = commonMethods.GetallTasksFormail().Where(a => a.AssignDate >= startOfWeek && a.AssignDate <= endOfWeek).ToList();
                var employees = commonMethods.GetAllEmployees();
                foreach (var m in employees)
                {
                    if (m.EmailId != null)
                    {
                        var userId = m.ID;
                        TaskDashboardcls taskDashboardcls = new TaskDashboardcls();
                        taskDashboardcls.TotalMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0)).ToList().Count();
                        taskDashboardcls.PendingMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "VERIFY").ToList().Count();


                        taskDashboardcls.TotalTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId).ToList().Count();
                        taskDashboardcls.PendingTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "VERIFY").ToList().Count();


                        taskDashboardcls.TotalTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId).ToList().Count();
                        taskDashboardcls.PendingTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "VERIFY").ToList().Count();
                        //smssend.weeklytaskmail("Weekly Task Report", m.EmailId, taskDashboardcls);
                    }
                }
            }
            catch
            {

            }
        }
    }
}

