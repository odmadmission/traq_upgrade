﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.SupportAggregate;
using OdmErp.ApplicationCore.Interfaces;
using static WebAPI.Models.SupportModel;

namespace WebAPI.Models
{

    public class SupportMasterModel
    {
        public long ID { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        public bool IsAvailable { get; set; }
    }

    public class Supportallcommentscls
    {
        public List<supportcomments> comments { get; set; }
        public long SupportRequestID { get; set; }

        public string SupportRequestRaisedName{ get; set; }
        public string SupportRequestDescription { get; set; }

        public string SupportRequestDate { get; set; }

        // public string SupportTypeName { get; set; }
    }

    public class supportcomments
    {
        public long id { get; set; }
        public long SupportRequestID { get; set; }
        public string Name { get; set; }
        public string EmployeeName { get; set; }
        public long EmployeeID { get; set; }
        public string postedON { get; set; }
        public string profile { get; set; }
        public DateTime insertedOn { get; set; }
        public DateTime modifiedOn { get; set; }
    }

    public class SupportTypeCls
    {
        public long ID { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        public bool IsAvailable { get; set; }
        public string DeptName { get; set; }

        [Display(Name = "Department")]
        [Required(ErrorMessage = "Please Select Department")]
        public long DepartmentID { get; set; }
        public List<Departmentcls> departments { get; set; }
    }


    public class DepartmentSupportCls
    {
        public long ID { get; set; }
        [Display(Name = "Name")]
        // [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        public bool IsAvailable { get; set; }
        public string DeptName { get; set; }
        public long DepartmentID { get; set; }
        public List<Departmentcls> departments { get; set; }
        public string SupportTypeName { get; set; }
        public long SupportTypeID { get; set; }
        public List<SupportType> supportTypes { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
    }

    public class SupportCategoryCls
    {
        public long ID { get; set; }
        [Display(Name = "Support Type")]
        [Required(ErrorMessage = "Please Select Support Type")]
        public long SupportTypeID { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }

        public string SupportTypeName { get; set; }

        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public List<SupportType> supportTypes { get; set; }
        public bool IsAvailable { get; set; }
    }
    public class SupportSubCategoryCls
    {
        public long ID { get; set; }
        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please Select Category")]
        public long CategoryID { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }

        public string CategoryName { get; set; }
        [Display(Name = "Other Name")]
        [Required(ErrorMessage = "Please Enter Other(s) Name")]
        public string OthersName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public List<SupportMasterModel> category { get; set; }
        public bool IsAvailable { get; set; }
    }
    public class LocationCls
    {
        public long ID { get; set; }

        [Display(Name = "Organization Name")]
        [Required(ErrorMessage = "Please Select Organization")]
        public long OrganizationID { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Please Enter Address")]
        public string Address { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string OrganizationName { get; set; }
        public bool IsAvailable { get; set; }
        public List<Organisationcls> organisations { get; set; }

        [Display(Name = "City Name")]
        [Required(ErrorMessage = "Please Select City")]
        public long CityID { get; set; }
        public string CityName { get; set; }
        public List<Citycls> city { get; set; }
    }

    public class BuildingCls
    {
        public long ID { get; set; }

        public long BuildingTypeID { get; set; }

        [Display(Name = "Location Name")]
        [Required(ErrorMessage = "Please Select Location")]
        public long LocationID { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }

        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }

        public string LocationName { get; set; }
        public bool IsAvailable { get; set; }
        public List<LocationCls> locations { get; set; }
    }
    public class RoomCls
    {
        public long ID { get; set; }

        [Display(Name = "Building Name")]
        [Required(ErrorMessage = "Please Select Building")]
        public long BuildingID { get; set; }
        [Display(Name = "Room Category")]
        [Required(ErrorMessage = "Please Select Room Category")]
        public long RoomCategoryID { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string BuildingName { get; set; }
        public string RoomCategoryName { get; set; }
        public List<SupportMasterModel> roomcategory { get; set; }
        public List<BuildingCls> buildings { get; set; }
        public bool IsAvailable { get; set; }

        [Display(Name = "Floor")]
        [Required(ErrorMessage = "Please Select Floor")]
        public long FloorID { get; set; }
        public string FloorName { get; set; }
        public List<Floor> floors { get; set; }
    }

    public class DateStringValue
    {
        public string ActiveDateValue { get; set; }
        public DateTime ActiveDate { get; set; }
    }
    public class SupportRequestCls
    {
        public string profile { get; set; }

        public string OrgGroupName { get; set; }

        public long DepartmentID { get; set; }

        public string department { get; set; }
        public string assigntime { get; set; }
        public Nullable<bool> IsManagementRead { get; set; }
        public Nullable<bool> IsRead { get; set; }
        public string RequestedCompletionDate { get; set; }
        public long SupportTypeID { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public int commentcount { get; set; }
        public int AttachmentCount { get; set; }
        public string SupportTypeName { get; set; }
        public List<supportcomments> comments { get; set; }
        public List<allsupportattachmentcls> attachements { get; set; }
        public List<SupportType> supportTypes { get; set; }

        // public string Active { get; set; }
        public string emp_Name { get; set; }
        public string emp_Phone { get; set; }
        public string emp_Dept { get; set; }
        public string Reason { get; set; }
        public DateTime? t_Date { get; set; }
        public long ID { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please Select Category")]
        public long CategoryID { get; set; }

        [Display(Name = "Sub Category")]
        [Required(ErrorMessage = "Please Select Sub Category")]
        public long SubCategoryID { get; set; }

        [Display(Name = "Location")]
        [Required(ErrorMessage = "Please Select Location")]
        public long LocationID { get; set; }
        [Display(Name = "Building")]
        [Required(ErrorMessage = "Please Select Building")]
        public long BuildingID { get; set; }
        [Display(Name = "Room(Floor)")]
        [Required(ErrorMessage = "Please Select Room")]
        public long RoomID { get; set; }
        [Display(Name = "Priority")]
        [Required(ErrorMessage = "Please Select Priority")]
        public long PriorityID { get; set; }

        public string Description { get; set; }
        public DateTime InsertedDate { get; set; }

        public (string Date,string Time,string DateTime) InsertedDateTime { get; set; }

        // public string InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string BuildingName { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string RoomName { get; set; }
        public string PriorityName { get; set; }
        public string LocationName { get; set; }
        [Display(Name = "Title")]
        [Required(ErrorMessage = "Please Enter Title")]
        public string Title { get; set; }
        public string TicketCode { get; set; }

        public List<SupportCategoryCls> category { get; set; }
        public List<SupportMasterModel> priority { get; set; }
        public List<BuildingCls> buildings { get; set; }
        public List<SupportSubCategoryCls> subcategory { get; set; }
        public List<RoomCls> room { get; set; }
        public List<LocationCls> location { get; set; }
        public string Status { get; set; }
        public bool IsAvailable { get; set; }
        public List<SupportAttachment> Attachments { get; set; }
        public List<SupportAttachment> Attachments2 { get; set; }
        public string CompletionDate { get; set; }
        public string Comment { get; set; }
        public long StatusID { get; set; }
        public List<SupportStatus> statuses { get; set; }

        public (bool Sent, string SentDate, string SentName, bool Approved,string ApprovedDate, string ApprovedName, bool Rejected, string RejectedDate,
            string RejectedName) Management { get; set; }


        [Display(Name = "Floor")]
        [Required(ErrorMessage = "Please Select Floor")]
        public long FloorID { get; set; }
        public string FloorName { get; set; }
        public List<Floor> floors { get; set; }
        public List<DateStringValue> all_date { get; set; }
        public long InsertedID { get; set; }
        public long UserID { get; set; }
        public bool IsAvailableLocation { get; set; }

        public bool SentApproval { get; set; }

        public string DueDate { get; set; }
        public long ApprovedID { get; set; }
        public long RejectedID { get; set; }
        public string ApproveDate { get; set; }
        public string RejectDate { get; set; }
        public bool IsApprove { get; set; }
        public bool IsReject { get; set; }
        public string ApproveReject { get; set; }
        public decimal? score { get; set; }
    }

    public class SupportRequestattachments
    {
        public long id { get; set; }
        public string path { get; set; }
        public string Name { get; set; }
        public string file { get; set; }
        public bool isremove { get; set; }

    }

    public class allsupportattachmentcls
    {
        public List<supportattachmentclass> attachments { get; set; }
        public long SupportRequestID { get; set; }
        //public string todoName { get; set; }
    }

    public class supportattachmentclass
    {
        public long SupportRequestID { get; set; }
        public string AttachmentName { get; set; }
        public string Attachmenturl { get; set; }
        public long employeeid { get; set; }
        public string employeeName { get; set; }
        public long id { get; set; }
        public DateTime insertedOn { get; set; }
        public DateTime modifiedOn { get; set; }
    }

    public class SupportResolveattachments
    {
        public long id { get; set; }
        public string path { get; set; }
        public string Name { get; set; }
        public string file { get; set; }
        public bool isremove { get; set; }

    }
    public class SupportModelList
    {
        public Nullable<long> ID { get; set; }
        public Nullable<long> StatusID { get; set; }
        public Nullable<int> commentcount { get; set; }
        public string Status { get; set; }
        public string Actions { get; set; }

        public List<SupportRequestCls> SupportRequestcls { get; set; }
        public List<DateStringValue> all_date { get; set; }
        public string t_Date { get; set; }
        public IEnumerable<NameIdViewModel> statuses { get; set; }
        public long PriorityID { get; set; }
        public IEnumerable<NameIdViewModel> priorities { get; set; }
        public string PriorityName { get; set; }
        public long? SupportTypeID { get; set; }
        public List<NameIdViewModel> SupportTypes { get; set; }
        public string SupportTypeName { get; set; }

        public long OrganizationID { get; set; }
        public List<NameIdViewModel> organizations { get; set; }
        public string OrganizationName { get; set; }

        public List<NameIdViewModel> departments { get; set; }

        public List<NameIdViewModel> Types { get; set; }

        public string Type { get; set; }
        public long[] PriorityId { get; set; }
        public long[] DeptId { get; set; }
        public long[] StatusId { get; set; }
        public long[] OrgId { get; set; }
        public long[] SupportTypeId { get; set; }

       

    }

    public class SupportRequestDetails
    {
        public long ID { get; set; }
        public long CategoryID { get; set; }

        public string CategoryName { get; set; }
        public long SubCategoryID { get; set; }

        public string SubCategoryName { get; set; }
        public long RoomID { get; set; }

        public string RoomName { get; set; }
        public long BuildingID { get; set; }

        public string BuildingName { get; set; }
        public long PriorityID { get; set; }

        public string PriorityName { get; set; }
        public long StatusID { get; set; }

        public string StatusName { get; set; }
        public long SupportTypeID { get; set; }

        public string SupportTypeName { get; set; }

        public string Description { get; set; }
        public string Comments { get; set; }
        public List<SupportAttachment> attachments { get; set; }

        public List<SupportAttachment> attachments2 { get; set; }

    }

    public class EmployeeDepartmentDetails
    {
        public long ID { get; set; }
        public List<Departmentcls> departments { get; set; }
        public List<Designationcls> designations { get; set; }
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public long DesignationID { get; set; }
    }

    public class EmployeeDetails
    {
        public long ID { get; set; }
        public string EmployeeName { get; set; }
        public string Mobile { get; set; }
        public string ProfileImage { get; set; }
    }

    public class SupportDetailsViewCls
    {
        public long ID { get; set; }
        public SupportRequestDetails requests { get; set; }
        public IEnumerable<EmployeeDesignationCls> depart { get; set; }
        public EmployeeDetails employees { get; set; }
        public bool IsApprove { get; set; }
        public bool IsReject { get; set; }
    }

    public class SupportReports
    {
        public long Raised { get; set; }
        public long Resolved { get; set; }
        public long Pending { get; set; }
        public List<SupportCategory> categories { get; set; }
        public long CategoryID { get; set; }
        public string Category { get; set; }
        public List<SupportSubCategory> subcategories { get; set; }
        public long SubCategoryID { get; set; }
        public string SubCategory { get; set; }
        public List<SupportType> supportTypes { get; set; }
        public long SupportTypeID { get; set; }
        public string SupportType { get; set; }
        public string t_Date { get; set; }
        public string f_Date { get; set; }
        public DateTime InsertedDate { get; set; }
    }

    public class SupportTimeLineCls
    {
        public long SupportID { get; set; }
        public long ID { get; set; }
        public string EmployeeName { get; set; }
        public string SupportStatus { get; set; }
        public List<SupportRequest> supportRequests { get; set; }
        public DateTime InsertedDate { get; set; }
        // public List<MaintStatus> statuses { get; set; }
    }
    public class supportReportDashboard
    {
        public long ID { get; set; }
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public long StatusID { get; set; }
        public long SupportTypeID { get; set; }
        public string StatusName { get; set; }
        public bool SendApproval { get; set; }
    }
    public class SupportTabularReportCls
    {
        public long ID { get; set; }
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int Raise { get; set; }
        public int RaiseP { get; set; }
        public int Resolve { get; set; }
        public int ResolveP { get; set; }
        public int Pending { get; set; }
        public int PendingP { get; set; }
        public int Reject { get; set; }
        public int RejectP { get; set; }
        public int Verify { get; set; }
        public int VerifyP { get; set; }
        public int Inprogress { get; set; }
        public int InprogressP { get; set; }
        public int Onhold { get; set; }
        public int OnholdP { get; set; }
        public int Approve { get; set; }
        public int ApproveP { get; set; }
        public int ApprovalPending { get; set; }
        public int ApprovalPendingP { get; set; }
        public int MgtReject { get; set; }
        public int MgtRejectP { get; set; }

        public int SendApprovalP { get; set; }
        public int SendApproval { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<Department> departments { get; set; }
        public List<SupportTabularReportClsList> supportTabularReportClsLists { get; set; }
    }

    public class SupportTabularReportClsList
    {
        public long ID { get; set; }
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int Raise { get; set; }
        public int RaiseP { get; set; }
        public int Resolve { get; set; }
        public int ResolveP { get; set; }
        public int Pending { get; set; }
        public int PendingP { get; set; }
        public int Reject { get; set; }
        public int RejectP { get; set; }
        public int Verify { get; set; }
        public int VerifyP { get; set; }
        public int Inprogress { get; set; }
        public int InprogressP { get; set; }
        public int Onhold { get; set; }
        public int OnholdP { get; set; }
    }

    public class SupportResolveCls
    {
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string StatusName { get; set; }
        public long SupportTypeID { get; set; }

        public string SupportTypeName { get; set; }

        public List<SupportType> supportTypes { get; set; }

        // public string Active { get; set; }
        public string emp_Name { get; set; }
        public string emp_Phone { get; set; }
        public string emp_Dept { get; set; }
        public string t_Date { get; set; }
        public long ID { get; set; }

        public long CategoryID { get; set; }

        public long SubCategoryID { get; set; }

        public long LocationID { get; set; }

        public long BuildingID { get; set; }

        public long RoomID { get; set; }

        public long PriorityID { get; set; }

        public string Description { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string BuildingName { get; set; }
        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public string RoomName { get; set; }
        public string PriorityName { get; set; }
        public string LocationName { get; set; }

        public List<SupportCategoryCls> category { get; set; }
        public List<SupportMasterModel> priority { get; set; }
        public List<BuildingCls> buildings { get; set; }
        public List<SupportSubCategoryCls> subcategory { get; set; }
        public List<RoomCls> room { get; set; }
        public List<LocationCls> location { get; set; }
        public string Status { get; set; }
        public bool IsAvailable { get; set; }
        public List<SupportAttachment> Attachments { get; set; }
        public List<SupportAttachment> Attachments2 { get; set; }
        public string CompletionDate { get; set; }
        public string Comment { get; set; }
        public long StatusID { get; set; }
        public List<SupportStatus> statuses { get; set; }

        public long FloorID { get; set; }
        public string FloorName { get; set; }
        public List<Floor> floors { get; set; }
        public List<DateStringValue> all_date { get; set; }
        public long InsertedID { get; set; }
        public long UserID { get; set; }
        public bool IsAvailableLocation { get; set; }

        public string DueDate { get; set; }
        public long ApprovedID { get; set; }
        public long RejectedID { get; set; }
        public string ApproveDate { get; set; }
        public string RejectDate { get; set; }
        public bool IsApprove { get; set; }
        public bool IsReject { get; set; }
        public string ApproveReject { get; set; }
    }

}