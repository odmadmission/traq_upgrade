﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebAPI.Models
{
    public class CommonMasterModel
    {
        public long GroupID { get; set; }
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        public string Url { get; set; }
        public string Status { get; set; }
        public long organisationID { get; set; }
    }
    public class DocumentSubTypecls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Document Type")]
        [Required(ErrorMessage = "Please Select Document Type")]
        public long DocumentTypeID { get; set; }
        public string DocumentTypeName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> documentTypes { get; set; }

        public long DocumentTypeCount { get; set; }
        public bool Active { get; set; }
        public string ModifiedName { get; set; }
        public string Status { get; set; }

    }
    public class Bankbranchcls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Bank")]
        [Required(ErrorMessage = "Please Select Bank")]
        public long BankID { get; set; }
        public string BankName { get; set; }
        [Display(Name = "IFSC Code")]
        [Required(ErrorMessage = "Please Enter IFSC Code")]
        public string IfscCode { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> banks { get; set; }
    }
    public class Statecls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Country")]
        [Required(ErrorMessage = "Please Select Country")]
        public long CountryID { get; set; }
        public string CountryName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> countries { get; set; }
    }
    public class Authoritycls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Authority Type")]
        [Required(ErrorMessage = "Please Select Authority Type")]
        public long AuthorityTypeID { get; set; }
        public string AuthorityTypeName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> authorityTypes { get; set; }
    }
    public class Actioncls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Sub Module")]
        [Required(ErrorMessage = "Please Select Sub Module")]
        public long SubModuleID { get; set; }
        public string SubModuleName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<SubModule> submodules { get; set; }
        [Display(Name = "Module")]
        [Required(ErrorMessage = "Please Select Module")]
        public long ModuleID { get; set; }
        public string ModuleName { get; set; }
        public IEnumerable<CommonMasterModel> modules { get; set; }
    }
    public class Departmentcls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Organisation")]
        [Required(ErrorMessage = "Please Select Organisation")]
        public bool IsPrimary { get; set; }
        [Display(Name = "Parent Department")]
        public long? ParentID { get; set; }
        public string ParentName { get; set; }
        public long OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> organizations { get; set; }
        public IEnumerable<CommonMasterModel> departments { get; set; }
        [Display(Name = "DepartmentType")]
        [Required(ErrorMessage = "Please Select DepartmentType")]
        public string DepartmentType { get; set; }

        public IEnumerable<CommonMasterModel> groups { get; set; }
        public long GroupID { get; set; }
        public long DesignationCount { get; set; }
        public bool Active { get; set; }
        public string GroupName { get; set; }
    }
    public class Designationcls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Designation Level")]
        [Required(ErrorMessage = "Please Select Designation Level")]
        public long DesignationLevelID { get; set; }
        public string DesignationLevelName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> designationLevels { get; set; }
        [Display(Name = "Department")]
        [Required(ErrorMessage = "Please Select Department")]
        public long DepartmentID { get; set; }

        public long DesignationCount { get; set; }
        public bool Active { get; set; }

        public string DepartmentName { get; set; }
        public IEnumerable<CommonMasterModel> departments { get; set; }
    }
    public class EducationalQualificationcls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Educational Qualification Type")]
        [Required(ErrorMessage = "Please Select Educational Qualification Type")]
        public long EducationQualificationTypeID { get; set; }
        public string EducationalQualificationTypeName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> educationalQualificationType { get; set; }
    }

    public class EmpGrievancecls
    {
        public long ID { get; set; }

        [Display(Name = "Grievance Type")]
        [Required(ErrorMessage = "Please Select Grievance Type")]
        public long GrievanceTypeID { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }

        [Display(Name = "Employee")]
        [Required(ErrorMessage = "Please Select Employee")]
        public long EmployeeID { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string FirstName { get; set; }


        public IEnumerable<CommonMasterModel> grievanceType { get; set; }
        public IEnumerable<CommonMasterModel> employees { get; set; }
        public object GrievanceID { get; internal set; }
    }
    public class Standardcls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Board")]
        [Required(ErrorMessage = "Please Select Board")]
        public long BoardID { get; set; }
        public string BoardName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> boards { get; set; }
        public bool StreamType { get; set; }
        public string Status { get; set; }
    }
    public class Streamcls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Standard")]
        public long StandardID { get; set; }
        public string StandardName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> Standards { get; set; }
        public string status { get; set; }
    }
    public class SubModulecls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Module")]
        [Required(ErrorMessage = "Please Select Module")]
        public long ModuleID { get; set; }
        public string Url { get; set; }
        public string ModuleName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> Modules { get; set; }

    }
    public class Citycls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "State")]
        [Required(ErrorMessage = "Please Select State")]
        public long StateID { get; set; }
        public string StateName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> States { get; set; }
        [Display(Name = "Country")]
        [Required(ErrorMessage = "Please Select Country")]
        public long CountryID { get; set; }
        public string CountryName { get; set; }
        public IEnumerable<CommonMasterModel> Countries { get; set; }
    }
    public class CityViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
      
        
        public string State { get; set; }
       
     
     
        public string Country { get; set; }

        public DateTime Modified { get; set; }


    }
    public class DepartmentLeadcls
    {
        public long ID { get; set; }

        [Display(Name = "Department")]
        [Required(ErrorMessage = "Please Select Department")]
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<Department> departments { get; set; }
        [Display(Name = "Employee")]
        [Required(ErrorMessage = "Please Select Employee")]
        public long EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public IEnumerable<CommonMasterModel> employees { get; set; }
        public bool IsAvailable { get; set; }
    }
    public class Organisationcls
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Group")]
        [Required(ErrorMessage = "Please Select Group")]
        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> groups { get; set; }
        [Display(Name = "Organization Type")]
        [Required(ErrorMessage = "Please Select Organization Type")]
        public long OrganizationTypeID { get; set; }
        public string OrganizationTypeName { get; set; }
        public IEnumerable<CommonMasterModel> OrganizationTypes { get; set; }

        public long DepartmentCount { get; set; }
        public bool Active { get; set; }
        public string ModifiedName { get; set; }
        public string Status { get; set; }

        public bool ParentActive { get; set; }
    }
    public class DepartmentAuthoritycls
    {
        public long ID { get; set; }
        [Display(Name = "Authority Type")]
        [Required(ErrorMessage = "Please Select Authority Type")]
        public long AuthorityTypeID { get; set; }
        public string AuthorityTypeName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> authorityTypes { get; set; }
        [Display(Name = "Authority")]
        [Required(ErrorMessage = "Please Select Authority")]
        public long AuthorityID { get; set; }
        public string AuthorityName { get; set; }
        public IEnumerable<CommonMasterModel> Authoritys { get; set; }
        [Display(Name = "Department")]
        [Required(ErrorMessage = "Please Select Department")]
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public IEnumerable<CommonMasterModel> Departments { get; set; }
        [Display(Name = "Employee")]
        [Required(ErrorMessage = "Please Select Employee")]
        public long EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public IEnumerable<CommonMasterModel> Employees { get; set; }
        [Display(Name = "Organisation")]
        [Required(ErrorMessage = "Please Select Organisation")]
        public long OrganisationID { get; set; }
        public string OrganisationName { get; set; }
        public IEnumerable<CommonMasterModel> Organisations { get; set; }

    }

    public class StudentGrievancecls
    {
        public long ID { get; set; }

        // public bool IsPrimary { get; set; }
        // [Display(Name = "Parent")]
        public long ParentID { get; set; }
        public long GrievanceStatusID { get; set; }
        public string ParentName { get; set; }

        //[Display(Name = "StudentID")]
        [Required(ErrorMessage = "Please Select Student")]
        public long StudentID { get; set; }
        public long GrievanceTypeID { get; set; }
        public string GrievanceTypeName { get; set; }
        public string StudentName { get; set; }
        public string Description { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<CommonMasterModel> students { get; set; }
        public IEnumerable<CommonMasterModel> parents { get; set; }
        public string GrievanceStatusName { get; set; }
        public IEnumerable<CommonMasterModel> grievancetypes { get; set; }
        public IEnumerable<CommonMasterModel> grievancestatus { get; set; }

    }
    public class logincls
    {
        public bool isRemember { get; set; }
        public bool isExist { get; set; }
        public string userName { get; set; }
        public string Password { get; set; }
        public long userId { get; set; }
        public string otp { get; set; }
        public string newPassword { get; set; }
        public long accessid { get; set; }
        public string confirmpassword { get; set; }
        public string mobile { get; set; }
        public string[] mobilelist { get; set; }
    }
    public class EmployeeDocumentUploadCls
    {
        public IEnumerable<IFormFile> files { get; set; }
        public List<EmployeeDocumentAttachment> attachments { get; set; }
        public bool isverified { get; set; }
        public bool isapproved { get; set; }
        public bool verifiedaccess { get; set; }
        public bool approveaccess { get; set; }
        public long ID { get; set; }
        public long EmployeeID { get; set; }
    }
    public class ClassTimingcls
    {
        public long ID { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Start Time")]
        [Required(ErrorMessage = "Please Select Start Time")]
        public TimeSpan StartTime { get; set; }
        [Display(Name = "End Time")]
        [Required(ErrorMessage = "Please Select End Time")]
        public TimeSpan EndTime { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Display(Name = "Available")]
        [Required(ErrorMessage = "Please Select Available Or Not")]
        public bool IsAvailable { get; set; }
        public bool Active { get; set; }
    }
}
