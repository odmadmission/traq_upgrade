﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.TodoAggregate;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    //public class TodoModel
    //{

    //    public long ID { get; set; }
    //    [Display(Name = "Task Name")]
    //    [Required(ErrorMessage = "Enter Task Name")]
    //    public string Name { get; set; }
    //    [Display(Name = "Description")]
    //    //[Required(ErrorMessage = "Enter Description")]
    //    public string Description { get; set; }
    //    public DateTime? AssignedOn { get; set; }
    //    [Display(Name = "Priority")]
    //    [Required(ErrorMessage = "Enter Task Priority")]
    //    public long TodoPriorityID { get; set; }
    //    public string PriorityName { get; set; }
    //    public long TodoStatusID { get; set; }
    //    public string TodoStatusName { get; set; }
    //    [Display(Name = "Deadline Date")]
    //    public DateTime? DueDate { get; set; }

    //    public long AssignedEmployeeID { get; set; }
    //    public string AssignedEmployeeName { get; set; }
    //    public long ParentID { get; set; }
    //    [Display(Name = "Choose Your Department")]
    //    // [Required(ErrorMessage = "Select Department")]
    //    public long AssignByDepartmentID { get; set; }
    //    public long? AssignedId { get; set; }
    //    [Display(Name = "Department")]
    //    // [Required(ErrorMessage = "Select Department")]
    //    public long DepartmentID { get; set; }
    //    public string EmployeeName { get; set; }
    //    public string DepartmentName { get; set; }
    //    public DateTime? CompletionDate { get; set; }
    //    public DateTime? VerifiedDate { get; set; }
    //    public IEnumerable<TodoPriority> Priorities { get; set; }
    //    public List<commonCls> employeecls { get; set; }
    //    public List<commonCls> departmentcls { get; set; }
    //    public List<commonCls> assignbydepartmentcls { get; set; }
    //    public List<attachmentclass> todoattachments { get; set; }
    //    public List<todocomments> todocomments { get; set; }
    //    public List<subtasklist_cls> subtasklist_Cls { get; set; }
    //}
    public class viewtaskclass
    {
        public subtasklist_cls task { get; set; }
        public List<Cls_TodoTimelines> todoTimelines { get; set; }
        public List<attachmentclass> todoattachments { get; set; }
        public List<todocomments> todocomments { get; set; }
        public List<subtasklist_cls> subtasklist_Cls { get; set; }
    }
    public class DatewiseTasks
    {
        public string AssignDate { get; set; }
        public IEnumerable<subtasklist_cls> Pendinglist { get; set; }
        public IEnumerable<subtasklist_cls> onholdlist { get; set; }
        public IEnumerable<subtasklist_cls> Completedlist { get; set; }
        public IEnumerable<subtasklist_cls> inprogresslist { get; set; }
        public IEnumerable<subtasklist_cls> Verifiedlist { get; set; }
    }
    public class allattachmentcls
    {
        public List<attachmentclass> todo { get; set; }
        public long todoID { get; set; }
        public string todoName { get; set; }
    }
    public class allcommentscls
    {
        public List<todocomments> todo { get; set; }
        public long todoID { get; set; }
        public string todoName { get; set; }
    }
    public class attachmentclass
    {
        public long TodoID { get; set; }
        public string AttachmentName { get; set; }
        public string Attachmenturl { get; set; }
        public long employeeid { get; set; }
        public string employeeName { get; set; }
        public long id { get; set; }
        public DateTime insertedOn { get; set; }
        public DateTime modifiedOn { get; set; }
    }
    public class subtasklist_cls
    {
        public long todoid { get; set; }
        public string todoname { get; set; }
        public string tododescription { get; set; }
        public string status { get; set; }
        public string AssignedDate { get; set; }
        public string DueDate { get; set; }
        public string assignedto { get; set; }
        public string assignedby { get; set; }
        public string profile { get; set; }
        public string priority { get; set; }
        public string department { get; set; }
        public string assigntime { get; set; }
        public long AssignByDepartmentID { get; set; }
        public string AssignByDepartmentName { get; set; }
        public long AssignToID { get; set; }
        public long AssignByID { get; set; }
        public DateTime? AssignDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long parentID { get; set; }
        public DateTime? DueDt { get; set; }
        public long DepartmentID { get; set; }
        public long priorityID { get; set; }
        public string completionDt { get; set; }
        public string VerifyDt { get; set; }
        public string tag { get; set; }
        public int commentcount { get; set; }
        public int SubtaskCount { get; set; }
        public List<subtasklist_cls> subtasks { get; set; }
        public decimal? score { get; set; }       
    }
    public class Cls_TodoTimelines
    {
        public DateTime InsertedDate { get; set; }
        public DateTime? RelatedDate { get; set; }
        public long RelatedId { get; set; }
        public string Status { get; set; }
        public string TodoStatus { get; set; }
        public string RelatedName { get; set; }
        public string insertedBy { get; set; }
    }
    public class commonCls
    {
        public long groupid { get; set; }
        public long id { get; set; }
        public string name { get; set; }
        public string profile { get; set; }
    }
    public class todoattachments
    {
        public long id { get; set; }
        public long TodoID { get; set; }
        public string AttachmentName { get; set; }
        public string Attachmenturl { get; set; }
        public string EmployeeName { get; set; }
        public string postedON { get; set; }
    }
    public class todocomments
    {
        public long id { get; set; }
        public long TodoID { get; set; }
        public string Description { get; set; }
        public string EmployeeName { get; set; }
        public long EmployeeID { get; set; }
        public string postedON { get; set; }
        public string profile { get; set; }
        public DateTime insertedOn { get; set; }
        public DateTime modifiedOn { get; set; }
    }
    public class TaskDashboardcls
    {
        public int TotalMyTasks { get; set; }
        public int PendingMyTasks { get; set; }
        public int onholdMyTasks { get; set; }
        public int inprogressMyTasks { get; set; }
        public int CompletedMyTasks { get; set; }
        public int VerifiedMyTasks { get; set; }
        public int TotalTaskother { get; set; }
        public int PendingTaskother { get; set; }
        public int onholdTaskother { get; set; }
        public int inprogressTaskother { get; set; }
        public int CompletedTaskother { get; set; }
        public int VerifiedTaskother { get; set; }
        public int TotalTasktome { get; set; }
        public int PendingTasktome { get; set; }
        public int onholdTasktome { get; set; }
        public int inprogressTasktome { get; set; }
        public int CompletedTasktome { get; set; }
        public int VerifiedTasktome { get; set; }
    }
    public class viewalltaskclass
    {
        public Mytaskscls tasks { get; set; }
        public int totaltask { get; set; }
        public int Pendingtask { get; set; }
        public int onholdtask { get; set; }
        public int inprogresstask { get; set; }
        public int Completedtask { get; set; }
        public int Verifiedtask { get; set; }
    }
    public class taskviewmodelcls
    {
        public string[] TodoStatusName { get; set; }
        public long[] TodoPriority { get; set; }
        public long[] employeeid { get; set; }
        public long[] departmentid { get; set; }
        public long? datetype { get; set; }
        public string f_date { get; set; }
        public string t_date { get; set; }
        public string m_date { get; set; }
        public IEnumerable<TodoPriority> Priorities { get; set; }
        public IEnumerable<commonCls> employeecls { get; set; }
        public IEnumerable<commonCls> assigntoemployeecls { get; set; }
        public IEnumerable<commonCls> assignbydepartmentcls { get; set; }
        public Mytaskscls tasks { get; set; }
        public viewalltaskclass alltask { get; set; }
        public IEnumerable<CommonMasterModel> departments { get; set; }
    }
    public class Mytaskscls
    {
        public List<subtasklist_cls> TodoList { get; set; }
        public List<subtasklist_cls> PendingList { get; set; }
        public List<subtasklist_cls> OnholdList { get; set; }
        public List<subtasklist_cls> InprogressList { get; set; }
        public List<subtasklist_cls> CompletedList { get; set; }
        public List<subtasklist_cls> VerifiedList { get; set; }
    }
    public class taskViewModels
    {
        public List<subtasklist_cls> TodoList { get; set; }
    }


}
