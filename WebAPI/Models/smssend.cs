﻿using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo;
using OdmErp.WebAPI.Data;
using RestSharp;
using sib_api_v3_sdk.Api;
using sib_api_v3_sdk.Client;
using sib_api_v3_sdk.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace OdmErp.WebAPI.Models
{
    public class smssend
    {
        public static string From = "tracq@odmegroup.org";
        public static string UserName = "tracq@odmegroup.org";
        public static bool EnableSsl = true;
        public static string Host = "smtp.gmail.com";
        public static string Password = "odm@1234";
        public static int Port = 587;





        private readonly IHostingEnvironment hostingEnvironment;

        public IMasterSMSRepository smsrepo;
        // private MasterSMSRepository repo;

        public smssend(IHostingEnvironment hostingEnv, IMasterSMSRepository r)
        {
            hostingEnvironment = hostingEnv;
            smsrepo = r;
        }
        public bool Sendsmstoemployee(string mobile, string message)
        {
            var smsdata = smsrepo.ListAllAsyncIncludeAll().
                Result.Where(a => a.Active == true).FirstOrDefault();
            string authKey = smsdata.ApiKey;//"232520Aycx7OOpF5b790d71";
            //Multiple mobiles numbers separated by comma
            string mobileNumber = mobile;
            //Sender ID,While using route4 sender id should be 6 characters long.
            string senderId = smsdata.SenderId;  //ODMEGR
            //Your message to send, Add URL encoding here.
            string msg = System.Web.HttpUtility.UrlEncode(message);

            //Prepare you post parameters
            StringBuilder sbPostData = new StringBuilder();
            sbPostData.AppendFormat(smsdata.ApiKey);
            sbPostData.AppendFormat("&");
            sbPostData.AppendFormat(smsdata.Mobile, mobileNumber);
            sbPostData.AppendFormat("&");
            sbPostData.AppendFormat(smsdata.Message, msg);
            sbPostData.AppendFormat("&");
            sbPostData.AppendFormat(smsdata.SenderId);
            sbPostData.AppendFormat("&");
            sbPostData.AppendFormat(smsdata.Route);//4

            try
            {
                //Call Send SMS API
                string sendSMSUri = smsdata.Host;// "http://api.msg91.com/api/sendhttp.php";
                //Create HTTPWebrequest
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                //Prepare and Add URL Encoded data
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(sbPostData.ToString());
                //Specify post method
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                //Get the response
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string responseString = reader.ReadToEnd();

                //Close the response
                reader.Close();
                response.Close();
                return true;
            }
            catch (SystemException ex)
            {
                return false;
            }
            //try
            //{
            //    string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=ODMEGR&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
            //    + "&country=91&message=" + message;

            //    HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);

            //    HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
            //    System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            //    string responseString = respStreamReader.ReadToEnd();
            //    respStreamReader.Close();
            //    myResp.Close();
            //    return true;

            //}
            //catch (Exception e1)
            //{
            //    return false;
            //}
        }


        public void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            try
            {
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress(From);

                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.To.Add(new MailAddress(recepientEmail));
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = Host;
                    smtp.EnableSsl = EnableSsl;
                    System.Net.NetworkCredential NetworkCred =
                        new System.Net.NetworkCredential();
                    NetworkCred.UserName = UserName;
                    NetworkCred.Password = Password;
                    //smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = Port;

                    smtp.Send(mailMessage);
                }
            }
            catch (Exception e)

            {

            }
        }


        public string newgrievance(string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "grievancetemplate.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string sendadmitcardemail(string name,
            string formnumber, string phone, string url,
             string subject,
            string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "sendadmitcard.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{formnumber}}", formnumber).Replace("{{name}}", name).
                    Replace("{{phone}}", phone).Replace("{{downloadLink}}", url);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string assigngrievance(string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "assigngrievance.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string resolvegrievance(string code, string complain, string raisedby, string employeename, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "resolvegrievance.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{code}}", code).Replace("{{complain}}", complain).Replace("{{raisedby}}", raisedby).Replace("{{employeename}}", employeename);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string verifygrievance(string grievancedetails, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "verifiedgrievance.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{grievancedetails}}", grievancedetails);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string SendReminderToEmployee(string Description, string subject, string sender, string raisedname, string studentcode, string email, string mobile, string category, string type, string subcategory)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "sendReminderToEmployee.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Description}}", Description).Replace("{{subject}}", subject).Replace("{{name}}", raisedname).Replace("{{code}}", studentcode)
                    .Replace("{{email}}", email).Replace("{{phone}}", mobile).Replace("{{category}}", category).Replace("{{type}}", type).Replace("{{subtype}}", subcategory);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string GrievanceSubAssign(string Description, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "grievanceSubAssign.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Description}}", Description).Replace("{{subject}}", subject);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string SendReminder(string Description, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "Reminderpage.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Description}}", Description).Replace("{{subject}}", subject);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public string accountcreated(string name, string sender, string code)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "accountcreation.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{empcode}}", code);
                SendHtmlFormattedEmail(sender, "Account Created", body);
            }

            return body;
        }

        public string Passwordcreated(string name, string studentname, string sender, string code)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "PasswordCreation.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{studentName}}", studentname).Replace("{{empcode}}", code);
                SendHtmlFormattedEmail(sender, "Password Created", body);
            }

            return body;
        }

        public string Parentcreated(string name, string sender, string code)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "accountcreation.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{empcode}}", code);
                SendHtmlFormattedEmail(sender, "Parent Created", body);
            }

            return body;
        }

        public string passwordchange(string name, string sender, string code)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "passwordchange.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name);
                SendHtmlFormattedEmail(sender, "Password Change", body);
            }

            return body;
        }

        public string Studentpasswordchange(string name, string studentname, string sender, string code)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "studentPasswordChange.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{studentname}}", studentname);
                SendHtmlFormattedEmail(sender, "Password Change", body);
            }

            return body;
        }





        public void Authenticate()
        {
            string[] scopes = new string[] {
          CalendarService.Scope.Calendar //, // Manage your calendars
 	  //CalendarService.Scope.CalendarReadonly // View your Calendars
       };
            string cal_user = "parismita@thedigichamps.com"; //your CalendarID On which you want to put events
                                                             //you get your calender id "https://calendar.google.com/calendar"
                                                             //go to setting >>calenders tab >> select calendar >>Under calender Detailes at Calendar Address:
            var webPath = hostingEnvironment.WebRootPath;
            string filePath = Path.Combine("", webPath + @"\Key\key.json");
            var service = ServiceAccountExample.AuthenticateServiceAccount("google-calender-app@healthville.iam.gserviceaccount.com", filePath, scopes);
            //"xyz@projectName.iam.gserviceaccount.com" this is your service account email id replace with your service account emailID you got it .
            //when you create service account https://console.developers.google.com/projectselector/iam-admin/serviceaccounts
            Event myEvent = new Event
            {
                Summary = "Visa Counselling",
                Location = "Gurgaon sector 57",
                Start = new EventDateTime()
                {
                    DateTime = new DateTime(2019, 12, 21, 5, 0, 0),
                    TimeZone = "(GMT+05:30) India Standard Time"
                },
                End = new EventDateTime()
                {
                    DateTime = new DateTime(2019, 12, 21, 6, 0, 0),
                    TimeZone = "(GMT+05:30) India Standard Time"
                }
                //,
                // Recurrence = new String[] {
                //"RRULE:FREQ=WEEKLY;BYDAY=MO"
                //}
                //,
                // Attendees = new List<EventAttendee>()
                // {
                // new EventAttendee() { Email = "Srivastava998@gmail.com" }
                //}
            };
            insert(service, cal_user, myEvent);

        }



        public static Event insert(CalendarService service, string id, Event myEvent)
        {
            try
            {
                return service.Events.Insert(myEvent, id).Execute();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
        public static string random_password(int length)
        {
            try
            {
                const string valid = "1234567890";
                StringBuilder res = new StringBuilder();
                Random rnd = new Random();
                while (0 < length--)
                {
                    res.Append(valid[rnd.Next(valid.Length)]);
                }
                return res.ToString();
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public bool Sendotp(string otp, string mobile)
        {
            try
            {
                string msg = "Please use " + otp + " as OTP(One Time Password) for activating your account. OTP expires in 1 hour. ODM";
                Sendsmstoemployee(mobile, msg);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool SendRegistrationSms(string name, string mobile)
        {

            Thread t1 = new Thread(delegate ()
            {
                try
                {
                    string msg = "Hi " + name + ", Thank you for registering with ODM Public School," +
                        " Kindly Login to Pay Admission Form Fee for Admission Process and Your " +
                        "Registered Mobile Number is " + mobile + " for relogin.\n" +
                        UtilityModel.GetTinyUrl("Admission/Student/Login") + TEAM_EDU_GROUP;
                    Sendsmstoemployee(mobile, msg);

                }
                catch
                {

                }

            });
            t1.Start();

            return true;
        }
        public bool SendcOMMONSms(string name, string mobile, string msg)
        {
            Sendsmstoemployee(mobile, msg);
            return true;
        }
       
        public bool SendFinalSubmissionRegistrationSms(string name, string mobile)
        {

            Thread t1 = new Thread(delegate ()
            {
                try
                {
                    string msg = "Hi " + name + @", Your application has been successfully submitted.Based on vacancy, Admission Team will send you notification for further process.
For any changes in application form, please visit ODM campus!!!, " + TEAM_EDU_GROUP;
                    Sendsmstoemployee(mobile, msg);

                }
                catch
                {

                }

            });
            t1.Start();

            return true;
        }
        private string TEAM_EDU_GROUP = "\n\nRegards,\nTeam ODM";
        public bool SendPaymentSms(string name, string mobile, string amount, string date, string link)
        {
            try
            {
                string msg = "Hi " + name + ", We received an amount of Rs " + amount + " for admission form fee on " + date + ". Please download the payment receipt from  " + link + "" + TEAM_EDU_GROUP;
                Sendsmstoemployee(mobile, msg);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public bool SendotpForParent(string otp, string mobile)
        {
            try
            {
                string msg = "Please use " + otp + " as OTP(One Time Password) for " +
                    "login. OTP expires in 10 minutes. ODM EDUCATIONAL GROUP";
                Sendsmstoemployee(mobile, msg);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool SendAdmitCard(string otp, string mobile)
        {
            try
            {
                string msg = "Please download the admit card from " + otp + ".";
                Sendsmstoemployee(mobile, msg);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SendSms(string firstname, string mobile)
        {
            try
            {
                string msg = "Hello" + firstname + ", \nYour password has been created successfully, \nWith Reagards \nODM Public School";
                Sendsmstoemployee(mobile, msg);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool SendPasswordChangeSms(string firstname, string mobile)
        {
            try
            {
                string msg = "Hello" + firstname + ", \nYour password has been Changed successfully, \nWith Reagards \nODM Public School";
                Sendsmstoemployee(mobile, msg);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SendStudentPasswordCreateSms(string firstname, string studentname, string mobile)
        {
            try
            {
                string msg = "Hello" + firstname + ", \nYour Child " + studentname + " password has been created successfully, \nWith Reagards \nODM Public School";
                Sendsmstoemployee(mobile, msg);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool SendStudentPasswordChangeSms(string firstname, string studentname, string mobile)
        {
            try
            {
                string msg = "Hello" + firstname + ", \nYour Child " + studentname + " password has been Changed successfully, \nWith Reagards \nODM Public School";
                Sendsmstoemployee(mobile, msg);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool PaymentReminderSms(string name, string parentPhNo, string studentname, string parentName, DateTime FromDate, DateTime ToDate)
        {
            try
            {
                string msg = "Hello" + parentName + ", \nYour Child " + studentname + " have the school installment fee in between " + FromDate.ToString("dd/MM/yyyy") + " to " + ToDate.ToString("dd/MM/yyyy") + ". If already paid igone this. \nWith Reagards \nODM Public School";
                Sendsmstoemployee(parentPhNo, msg);
                return true;
            }
            catch
            {
                return false;
            }
        }


        #region-----------------Student PI and Document Message--------------------------
        public bool SendPISms(string mobile,string Message)
        {

            Thread t1 = new Thread(delegate ()
            {
                try
                {
                    string msg = Message;
                    Sendsmstoemployee(mobile, msg);

                }
                catch
                {

                }

            });
            t1.Start();

            return true;
        }
        public bool SendPIStatusSendSms(string mobile, string Message)
        {

            Thread t1 = new Thread(delegate ()
            {
                try
                {
                    string msg = Message;
                    Sendsmstoemployee(mobile, msg);

                }
                catch
                {

                }

            });
            t1.Start();

            return true;
        }
        public bool SendDocumentSendSms(string mobile, string Message)
        {

            Thread t1 = new Thread(delegate ()
            {
                try
                {
                    string msg = Message;
                    Sendsmstoemployee(mobile, msg);

                }
                catch
                {

                }

            });
            t1.Start();

            return true;
        }
        public bool SendDocumentStatusSendSms(string mobile, string Message)
        {

            Thread t1 = new Thread(delegate ()
            {
                try
                {
                    string msg = Message;
                    Sendsmstoemployee(mobile, msg);

                }
                catch
                {

                }

            });
            t1.Start();

            return true;
        }
        #endregion
    }
}
