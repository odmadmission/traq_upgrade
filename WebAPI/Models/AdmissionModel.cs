﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class NameIdModal
    {
        public long id { get; set; }
        public string name { get; set; }
    }
    public class AdmissionSlotModal
    {
        public long id { get; set; }
        public string slotName { get; set; }
        public Nullable<DateTime> startDate { get; set; }
        public Nullable<DateTime> endDate { get; set; }            
    }
    public class academicClassModal
    {
        public string boardClassName { get; set; }
        public long boardId { get; set; }
        public string boardName { get; set; }
        public long standardId { get; set; }
        public long boardStandardId { get; set; }
    }
    public class WingOrgModel
    {
        public long SchoolId { get; set; }
        public long WingId { get; set; }
        public string Name { get; set; }
        public long AcademicStandardWindId { get; set; }
    }
    public class AdmissionBasicStudentModel
    {       
        public long academicSessionId { get; set; }
        public long academicSourceId { get; set; }
        public long admissionSlotId { get; set; } 
        public string firstName { get; set; } 
        public string lastName { get; set; }
        public string fullName { get; set; }
        public string mobile { get; set; }
        public string emailId { get; set; }
        public long standardId { get; set; }
        public long academicStandardWindId { get; set; }
        public long admissionId { get; set; }
        //public long AdmissionStudentId { get; set; }

        public string formNumber { get; set; }

    }
    public class PaymentModal
    {
        public long admissionId { get; set; }
        public string transactionId { get; set; }
        public long paymentModalId { get; set; }
        public int paidAmount { get; set; }
        public long receivedId { get; set; }
    }
    public class BesicRegistrationModal
    {
        public long academicStandardWingId { get; set; }
        public long admissionId { get; set; }
        public long sessionId { get; set; }
        public string formNumber { get; set; }
        public string academicSessionName { get; set; }
        public string academicSourceName { get; set; }
        public string admissionSlotName { get; set; }
        public string mobile { get; set; }
        public string firstName { get; set; }
        //public string middleName { get; set; }
        public string lastName { get; set; }
        //public string gender { get; set; }
        //public string bloodGroup { get; set; }
        //public string nationality { get; set; }
        //public string religion { get; set; }
        //public string category { get; set; }
        //public string motherTongue { get; set; }
        //public string passportNo { get; set; }
        //public string aadharNo { get; set; }
        //public string image { get; set; }
        //public Nullable<DateTime> DOB { get; set; }
        public string fullName { get; set; }
        public string EmgencyMob { get; set; }
        public string email { get; set; }
        public string standard { get; set; }
        public string wing { get; set; }
        public string board { get; set; }
        
        public string school { get; set; }
        //public string organization { get; set; }
        //public string academicStandardwing { get; set; }
        //public string board { get; set; }

        //public string contactType { get; set; }

    }
}
