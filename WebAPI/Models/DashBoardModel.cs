﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class DashBoardModel
    {
        public class DashBoardResponse
        { 
            public List<StoriesResponse> storiesResponses { get; set; }
            public List<ResultResponse> resultResponses { get; set; }
            public List<AboutUs> aboutUs { get; set; }
            public List<Curriculum> curriculum { get; set; }
            public List<Videos> Videos { get; set; }
        }

        public class StoriesResponse
        { 
            public long ID { get; set; }
            public string Image { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
        }

        public class ResultResponse
        { 
            public long ID { get; set; }
            public string Image { get; set; }
        }

        public class AboutUs
        { 
            public long ID { get; set; }
            public string Image { get; set; }
            public string Title { get; set; }
            public string Desctiption { get; set;}
        }

        public class Curriculum
        { 
            public long ID { get; set; }
            public string Image { get; set; }
            public string Title { get; set; }
            public string Desctiption { get; set; }
        }

        public class Videos
        {
            public long ID { get; set; }
            public string Image { get; set; }
            public string Title { get; set; }
            public string Desctiption { get; set; }
        }
    }
}
