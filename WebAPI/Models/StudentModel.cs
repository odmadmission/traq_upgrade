﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class StudentModel
    {
        public class ParentProfileModel
        {
            public long ID { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public string PrimaryMobile { get; set; }
            public string AlternativeMobile { get; set; }
            public string LandLineNumber { get; set; }
            public string Email { get; set; }
            public long ProfessionTypeId { get; set; }
            public string Organization { get; set; }
            public long StudentId { get; set; }
            public long ParentRelationshipId { get; set; }
            public string FullName { get; set; }
            public string ProfessionName { get; set; }
            public string StudentName { get; set; }
            public string ParentRelationshipName { get; set; }
        }

        public class StudentProfileModel
        {
            public long ID { get; set; }
            public string StudentCode { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public string Gender { get; set; }
            public DateTime DOB { get; set; }
            public long BloodGroupId { get; set; }
            public long ReligionId { get; set; }
            public long NationalId { get; set; }
            public string FullName { get; set; }
            public string BloodGroupName { get; set; }
            public string NationalityName { get; set; }
            public string ReligionName { get; set; }
        }
    }
}
