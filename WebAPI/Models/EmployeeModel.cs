﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static WebAPI.Models.SupportModel;

namespace WebAPI.Models
{
    public class GetEmployeeModel
    {
        public long ID { get; set; }
        public string EmpCode { get; set; }
        public string FullName { get; set; }
        public string Mobile { get; set; }
        public string AlternateMobile { get; set; }
        public string Email { get; set; }
        public Nullable<DateTime> DOB { get; set; }
        public string DOBString { get; set; }
        public string Nationality { get; set; }
        public string Religion { get; set; }
        public string LandLineNumber { get; set; }
        public string ImgUrl { get; set; }
        public string Gender { get; set; }
        public string BloodGroup { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public long? BloodGroupId { get; set; }
        public long? ReligionId { get; set; }
        public long? NationalityId { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedOnString { get; set; }
        public bool Isleft { get; set; }

    }

    public class EmployeeModuleAccess : BaseModel
    {
        public List<GivenAccesscls> employeeModuleSubModuleAccess { get; set; }
    }
    public class EmployeeResponseModel : BaseModel
    {
        public List<NameIdViewModel> type { get; set; }
        public List<NameIdViewModel> empstatus { get; set; }
        public List<GetEmployeeModel> empList { get; set; }

    }
    public class EmployeeAddressModel  
    { 
        public long ID { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PostalCode { get; set; }
        public long CityId { get; set; }
        public long StateId { get; set; }
        public long CountryId { get; set; }
        public long AddressTypeId { get; set; }
        public long EmployeeId { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string AddressType { get; set; }
        public string EmpName { get; set; }
    }

    public class EmpBankAccounts
    { 
        public long ID { get; set; }
        public long BankTypeId { get; set; }
        public long BankNameId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountHolderName { get; set; }
        public long EmployeeId { get; set; }
        public bool IsPrimary { get; set; }
        public string BranchName { get; set; }
        public string IFSCcode { get; set; }
    }

    public class EmployeeExperienceModel
    {
        public long ID { get; set; }
        public string OrganizationName { get; set; }
        public string Desingnation { get; set; }
        public int SalaryPerAnum { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public long EmployeeId { get; set; }
    }

    public class EmployeeEducationModel
    { 
        public long ID { get; set; }
        public long EducationQualificationId { get; set; }
        public long EmployeeId { get; set; }
        public string College { get; set; }
        public string Univarsity { get; set; }
        public int? FromYear { get; set; }
        public int? ToYear { get; set; }
    }


    public class EmployeeUpdateCommonModel:BaseModel
    {
        public string UpdateType { get; set; }
       
    }

    public class SupportRequestCommonModel : BaseModel
    {
      
        public long ID { get; set; }

    }


   
}
