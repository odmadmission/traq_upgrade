﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
   

    public class SmsRequestModel
    {
        public string AppType { get; set; }
        public string OtpType { get; set; }
        public string Mobile { get; set; }
        public DateTime CurrentDate { get; set; }
        public long AccessId { get; set; }
        public long EmployeeId { get; set; }

        public long RelatedId { get; set; }
        public string RelatedName { get; set; }

        public string RecievedOtp { get; set; }

        public long OtpId { get; set; }

        public string DeviceData { get; set; }
        public string FcmId { get; set; }
    }


    public class SmsInputModel
    {
        public string AppType { get; set; }
        public string OtpType { get; set; }
        public string Mobile { get; set; }

        public string RecievedOtp { get; set; }

        public long OtpId { get; set; }

        public string FcmId { get; set; }
    }

    public class SmsResponseModel:BaseModel
    {
        public string OtpCode { get; set; }
        public long OtpId { get; set; }

        public long LoginId { get; set; }
        public long AdmissionId { get; set; }
        public bool IsBasicRegistrationDone { get; set; }
        public bool IsPaymentDone { get; set; }
    }
}
