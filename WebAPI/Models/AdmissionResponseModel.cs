﻿
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class PersonalDataResponseModal : BaseModel
    {
        public List<NameIdModal> religions { get; set; }
        public List<NameIdModal> nationalities { get; set; }
        public List<NameIdModal> categories { get; set; }
        public List<NameIdModal> Languages { get; set; }
        public List<NameIdModal> countries { get; set; }
        public List<NameIdModal> professions { get; set; }
    }

    public class GeneralDataResponseModal : BaseModel
    {
        public List<NameIdModal> states { get; set; }
        public List<NameIdModal> city { get; set; }
    }
    public class AcademicSessionResponse : BaseModel
    {
        public NameIdModal academicSession { get; set; }
    }
    public class AdmissionSlotResponse : BaseModel
    {
        public List<AdmissionSlotModal> admissionSlot { get; set; }
    }
    public class AdmissionClassListResponse : BaseModel
    {
        public List<academicClassModal> admissionClass { get; set; }
    }
    public class WingResponse : BaseModel
    {
        public List<WingOrgModel> wings { get; set; }
    }
    public class MobileValidateResponse : BaseModel
    {
        public long admissionId { get; set; }
    }
    public class RegistrationResponse : BaseModel
    {
        public NameIdModal academicSessionId { get; set; }
        public AdmissionSlotModal slotId { get; set; }
        public List<NameIdModal> sourceId { get; set; }
        public List<academicClassModal> admissionClass { get; set; }
        public int admissionAmount { get; set; }

    


    }
    public class SaveRegistrationResponse : BaseModel
    {
        public long admissionId { get; set; }
    }
    public class SavePaymentResponse : BaseModel
    {
        public long paymentDetailsId { get; set; }
    }
    public class StudentBasicRegistrationResponse : BaseModel
    {
        public BesicRegistrationModal besicDetails { get; set; }
        public long admissionAmount { get; set; }
        public string admissionLogo { get; set; }
        public string  admissionOrganizaionName { get; set; }
        public string admissionDescription { get; set; }

    }

  
}
