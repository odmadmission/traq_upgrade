﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class CalenderRequestModel
    {
        public long LoginId { get; set; }
        public long AccessId { get; set; }

        public long StudentId { get; set; }

        public DateTime CalenderDate { get; set; }


    }

    public class CalenderResponseModel:BaseModel
    {

        public List<CalenderEngagementModel> CalenderEngagements { get; set; }

    }

    public class CalenderEngagementModel 
    {
        public long CalenderEngagementId { get; set; }
        public long CalenderEngagementTypeId { get; set; }
        public DateTime CalenderDate { get; set; }
        public string EngagementName { get; set; }

        public string CalenderEngagementTypeName { get; set; }
    }
}
