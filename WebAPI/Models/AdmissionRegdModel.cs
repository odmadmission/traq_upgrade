﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class AdmissionRegdModel
    {
        public Admission admission { get; set; }
        public AdmissionStudent admissionStudent { get; set; }
        public AdmissionStudentContact admissionStudentOfficial { get; set; }
        public AdmissionStudentContact admissionStudentEmergency { get; set; }
        public AdmissionStudentAcademic admissionStudentAcademic { get; set; }
        public List<AdmissionStudentAddress> admissionStudentAddress { get; set; }
        public AdmissionStudentDeclaration admissionStudentDeclaration { get; set; }
        public List<commoncls> admissionStudentLanguage { get; set; }
        public AdmissionStudentParent admissionStudentFather { get; set; }
        public AdmissionStudentParent admissionStudentMother { get; set; }
        public AdmissionStudentProficiency admissionStudentProficiency { get; set; }
        public List<AdmissionStudentReference> admissionStudentReference { get; set; }
        public admissionStudentStandardcls admissionStudentStandard { get; set; }
        public AdmissionStudentTransport admissionStudentTransport { get; set; }

        public List<commoncls> nationality { get; set; }
        public List<commoncls> category { get; set; }
        public List<commoncls> bloodGroups { get; set; }
        public List<commoncls> professiontypes { get; set; }
        public List<commoncls> languages { get; set; }
        public List<commoncls> locations { get; set; }
        public List<commoncls> countries { get; set; }
        public List<commoncls> religions { get; set; }
        public List<string> genders { get; set; }

        public bool isadmissionStudent { get; set; }
        public bool isadmissionStudentOfficial { get; set; }
        public bool isadmissionStudentEmergency { get; set; }
        public bool isadmissionStudentAcademic { get; set; }
        public bool isadmissionStudentAddress { get; set; }
        public bool isadmissionStudentDeclaration { get; set; }
        public bool isadmissionStudentLanguage { get; set; }
        public bool isadmissionStudentParent { get; set; }
        public bool isadmissionStudentProficiency { get; set; }
        public bool isadmissionStudentReference { get; set; }
        public bool isadmissionStudentStandard { get; set; }
        public bool isadmissionStudentTransport { get; set; }
        public int code { get; set; }
        public string status { get; set; }
        public string message { get; set; }
    }
    public class commoncls
    {
        public long id { get; set; }
        public string name { get; set; }
    }
    public class admissionStudentStandardcls
    {
        public long ID { get; set; }
        public long AcademicStandardWindId { get; set; }
        public long AdmissionStudentId { get; set; }
        public long StandardId { get; set; }
        public bool Nucleus { get; set; }
        public long OrganizationId { get; set; }
        public long WingId { get; set; }
        public long BoardStandardID { get; set; }
    }
    public class admissionStudentDetailscls
    {
        public long ID { get; set; }
        public long AdmissionId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public Nullable<DateTime> DateOfBirth { get; set; }
        public long BloodGroupID { get; set; }
        public long NatiobalityID { get; set; }
        public long ReligionID { get; set; }
        public long CategoryID { get; set; }
        public long MotherTongueID { get; set; }
        public string PassportNO { get; set; }
        public string AadharNO { get; set; }
        public string Image { get; set; }
        public List<long> languages { get; set; }
    }
    public class ContactViewmodel
    {
        public string txtEmrName { get; set; }
        public string txtEmrPhNo { get; set; }
        public string txtEmrOfcName { get; set; }
        public string txtEmrOfcPhNo { get; set; }
        public string txtEmrOfcEmail { get; set; }
        public int emegencyId { get; set; }
        public long admissionStudentID { get; set; }
        public int ofcContactId { get; set; }
    }
}
