﻿using OdmErp.ApplicationCore.Entities.EmployeeAggregate;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static WebAPI.Models.StudentModel;
using static WebAPI.Models.SupportModel;
using static WebAPI.Models.TodoModel;

namespace WebAPI.Models
{
    
    public class BaseModel
    {
        public int StatusCode { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }

        public string DisplayMessage { get; set; }

    }
    public class LoginRequestModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }

        public DateTime CurrentDate { get; set; }
    }

    public class LogoutRequestModel
    {
        public long LoginId { get; set; }
        public long EmployeeId { get; set; }

        // public DateTime CurrentDate { get; set; }
    }

    public class LoginResponseModel :BaseModel
    {
        public CommonModel commonModel { get; set; }


    }

  
    public class CommonModel
    {
        public string Mobile { get; set; }
      
        public long AccessId { get; set; }
        public long LoginId { get; set; }

        public long EmployeeId { get; set; }
        public string UniqueCode { get; set; }

        public long RoleId { get; set; }      
    }

    public class EmployeeBaseRequestModel
    {
        public long AccessId { get; set; }
        public long RoleId { get; set; }
    }

    public class EmployeeBaseRepsoneModel:BaseModel
    {
        public List<EmployeeAddressModel> employeeAddresses { get; set; }
        public List<EmpBankAccounts> empBankAccounts { get; set; }
        public List<EmployeeExperienceModel> employeeExperiences { get; set; }
        public List<EmployeeEducationModel> employeeEducationModels { get; set; }

    }

    public class ForgotPasswordRequest
    {
        public string Type { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string UserName { get; set; }
    }

    public class StudentBaseResponse : BaseModel
    { 
        public List<ParentProfileModel> parentProfileModels { get; set; }
        public List<StudentProfileModel> studentProfileModels { get; set; }
    }
    public class SupportAllResponse : BaseModel
    { 
        public List<SupportTypeModel> supportTypeModel { get; set; }
        public List<SupportCommonModel> supportStatusModel { get; set; }
        public List<SupportCommonModel> supportPriorityModel { get; set; }

        public List<NameIdViewModel> allDepartmentViewModel { get; set; }

        public List<SupportCommonModel> buildingModel { get; set; }
        
    }
        public class SupportBaseResponse : BaseModel
    { 
        public List<RoomModel> roomModels { get; set; }
        public List<SupportCommonModel> supportCommonModels { get; set; }
        public List<SupportTypeModel> supportTypeModels { get; set; }        
        public List<BuildingModel> buildingModels { get; set; } 
        public List<SupportRequestResponseModel> supportRequestResponseModels { get; set; }
        public List<SupportRequestDetailsModels> supportRequestDetailsModels { get; set; }

    }

    public class SupportResponse : BaseModel
    {
        public SupportRequestResponseModel supportRequestResponseModels { get; set; }
    }
        public class SupportAttachmentResponse : BaseModel
    {
        public List<SupportRequestDetailsModels> supportRequestDetailsModels { get; set; }
        public List<SupportRequestResponseModel> supportRequestResponseModels { get; set; }

    }
    public class SupportTimeLineBaseResponse : BaseModel
    {
        public List<SupportRequestDetailsModels> supportRequestDetailsModels { get; set; }
        public SupportRequestResponseModel supportRequestResponseModels { get; set; }
        public List<SupportRequestAttachmentModels> supportRequestAttachmentModels { get; set; }        

    } 

    public class SupportMaserBaseResponse : BaseModel
    {
        public List<OrganizationModel> organizationResponseModel { get; set; }
        public List<DepartmentModel> departmentModel { get; set; }
        public List<SupportCommonModel> supportCommonModels { get; set; }
    }
    public class SupportTypeResponse : BaseModel
    { 
        public List<SupportCategoryModel> supportType { get; set; }
    }

        public class SupportBaseResolve : BaseModel
    {
        public List<NameIdViewModel> AllDepartmentViewModel { get; set; }
        public List<SupportCommonModel> SupportStatus { get; set; }
        public List<StatusModel> ManagementStatus { get; set; }
        public List<SupportCommonModel> SupportPriority { get; set; }
        public SupportModelLists supportResponseModels { get; set; }
        public bool IsEmployee { get; set; }

    }

    public class CreateSupportRequestModel : BaseModel
    {
        public List<SupportTypeModel> supportType { get; set; }
        public List<SupportCommonModel> categoryList { get; set; }
        public List<SupportCommonModel> subCategoryList { get; set; }
        public List<SupportCommonModel> priorityList { get; set; }
        public List<SupportCommonModel> buildingList { get; set; }
        public List<RoomModel> roomList { get; set; }
    }

    public class SupportResolveModel : BaseModel
    {
        public List<NameIdViewModel> Type { get; set; }
        public List<SupportCommonModel> Organizations { get; set; }
        public List<SupportCommonModel> Department { get; set; }
        public List<SupportCommonModel> SupportType { get; set; }
        public List<SupportCommonModel> Priority { get; set; }
        public List<SupportCommonModel> Statuses { get; set; }
        public List<SupportRequestResponseModel> supportResponseModels { get; set; }
        //public SupportModelLists supportResponseModels { get; set; }
    }



}
