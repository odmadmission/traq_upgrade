﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Constants
{
    public static class HttpStatusCode
    {
        public static int LOGIN_PASSWORD_AVAILABLE = 1013;
        public static int LOGIN_SUCCESS = 1000;
        public static int LOGIN_INVALID_CREDENTIALS = 1001;
        public static int LOGIN_NO_PASSWORD = 1002;
        public static int LOGIN_USERNAME_INVALID = 1003;
        public static int EMPLOYEE_NOT_FOUND = 1004;
        public static int EMPLOYEE_JOIN_DETAILS_NOT_FOUND = 1005;

        public static int SMS_SENT_SUCCESS = 1006;
        public static int SMS_SENT_FAILED = 1007;

        public static int SMS_VERIFICATION_OTP_NOT_FOUND = 1008;
        public static int SMS_VERIFICATION_OTP_INCORRECT = 1009;
        public static int SMS_VERIFICATION_SUCCESS = 1010;
        public static int SMS_VERIFICATION_INACTIVE = 1011;

        public static int SERVER_ERROR = 1012;




        public static int MODULE_ACCESS_DENIED = 1016;
        public static int SUCCESS = 1014;
        public static int FAILURE = 1015;
        public static int EXCEPTION = 1017;
        public static int No_Record = 1103;
        public static int Updated = 1104;
        public static int NotFound = 1105;
        public static int RecordNotFound = 1105;

        public static int PASSWORD_UPDATE = 1018;
        public static int PASSWORD_NOT_UPDATED = 1019;


        public static int PARENT_NOT_FOUND = 1101;


        public static int LOGIN_NOT_FOUND = 1102;
        public static int NEW_USER = 1107;
   

    }

    public static class OtpTypeText
    {

        public static string OTP_LOGIN_APP = "OTP_LOGIN_APP";
        public static string OTP_CREATE_PASSWORD = "OTP_CREATE_PASSWORD";
        public static string OTP_FORGOT_PASSWORD = "OTP_FORGOT_PASSWORD";
        public static string OTP_CHANGE_PASSWORD = "OTP_CHANGE_PASSWORD";
        public static string ADMISSION_REGISTRATION = "ADMISSION_REGISTRATION";
    }
    public static class PasswordType
    {

      
        public static string CREATE_PASSWORD = "CREATE_PASSWORD";
        public static string FORGOT_PASSWORD = "FORGOT_PASSWORD";
        public static string CHANGE_PASSWORD = "CHANGE_PASSWORD";

    }

    public static class EmployeeText
    {

        public static string EMPLOYEE_PROFILE = "EMPLOYEE_PROFILE";
        public static string EMPLOYEE_ADDRESS = "EMPLOYEE_ADDRESS";
      

    }

    public static class CommonText
    {

        public static string SUCCESS = "SUCCESS";
        public static string FAILED = "FAILED";
        public static string EXCEPTION = "EXCEPTION";
        public static string No_Record = "No Record Found";
        public static string UPDATED = "UPDATED";
        public static string NotFound = "Record is Missing";
        public static string RecordNotFound = "One Record Not Found";
        public static string NEW_USER = "NEW_USER";

    }

    public static class DisplayText
    {

        public static string PASSOWRD_UPDATE_SUCCESS_TEXT = "Password updated successfully";
        public static string PASSOWRD_UPDATE_FAILED_TEXT = "Password update failed";
        public static string PASSOWRD_UPDATE_OLD_WRONG_TEXT = "Current password is invalid";

    }
}
