﻿
using Microsoft.AspNetCore.Builder;
using OdmErp.ApplicationCore.Entities.SupportAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Controllers;
using static WebAPI.Models.SupportModel;
using static WebAPI.Models.TodoModel;

namespace WebAPI.Data
{
    public class SupportMethod : AppController
    {
        protected readonly ISupportRepository supportRepository;
        protected readonly ICityRepository cityRepository;
        protected readonly IStateRepository stateRepository;
        protected readonly ICountryRepository countryRepository;
        protected readonly IOrganizationRepository organizationRepository;
        protected readonly IEmployeeRepository employeeRepository;
        protected readonly IDepartmentRepository departmentRepository;
        protected readonly IGroupRepository groupRepository;
        protected readonly IOrganizationTypeRepository organizationTypeRepository;
        protected readonly IModuleRepository moduleRepository;
        protected readonly ISubModuleRepository subModuleRepository;
        protected readonly IActionRepository actionRepository;
        protected readonly CommonMethods commonMethods;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IDesignationRepository designationRepository;
        private readonly ApplicationDbContext _dbContext;

        public SupportMethod(ISupportRepository supportRepository, ICityRepository cityRepository, IStateRepository stateRepository,
           ICountryRepository countryRepository, IOrganizationRepository organizationRepository, IEmployeeRepository employeeRepository,
            IDepartmentRepository departmentRepository, IGroupRepository groupRepository, ApplicationDbContext _dbContext,
            IOrganizationTypeRepository organizationTypeRepository, IModuleRepository moduleRepository, ISubModuleRepository subModuleRepository,
            IActionRepository actionRepository, CommonMethods commonMethods, IEmployeeDesignationRepository employeeDesignationRepository, IDesignationRepository designationRepository)
        {
            this.supportRepository = supportRepository;
            this.cityRepository = cityRepository;
            this.stateRepository = stateRepository;
            this.countryRepository = countryRepository;
            this.organizationRepository = organizationRepository;
            this.employeeRepository = employeeRepository;
            this.departmentRepository = departmentRepository;
            this.groupRepository = groupRepository;
            this.organizationTypeRepository = organizationTypeRepository;
            this.moduleRepository = moduleRepository;
            this.subModuleRepository = subModuleRepository;
            this.actionRepository = actionRepository;
            this.commonMethods = commonMethods;
            this.employeeDesignationRepository = employeeDesignationRepository;
            this.designationRepository = designationRepository;
            this._dbContext = _dbContext;
        }

        public IEnumerable<RoomModel> GetRoomByBuildingID(long buildingid)
        {
            var roomCategoryList = supportRepository.GetAllRoomCategory();
            var floorList = supportRepository.GetAllFloor();
            var roomList = supportRepository.GetAllRoom();

            var res = (from a in roomList
                       join b in roomCategoryList on a.RoomCategoryID equals b.ID
                       join c in floorList on a.FloorID equals c.ID
                       where a.BuildingID == buildingid
                       select new RoomModel
                       {
                           ID = a.ID,
                           Name = a.Name,
                           BuildingId = a.BuildingID,
                           RoomCategoryId = a.RoomCategoryID,
                           RoomCategoryName = b.Name,
                           FloorId = a.FloorID,
                           FloorName = c.Name
                       }).ToList();

            return res;
        }

        public IEnumerable<SupportCommonModel> GetAllSupportStatus()
        {
            List<SupportCommonModel> supports = new List<SupportCommonModel>();
            supports.Add(new SupportCommonModel { ID = 1, Name = "PENDING" });
            supports.Add(new SupportCommonModel { ID = 2, Name = "ON-HOLD" });
            supports.Add(new SupportCommonModel { ID = 3, Name = "IN-PROGRESS" });
            supports.Add(new SupportCommonModel { ID = 4, Name = "MANAGEMENT APPROVED" });
            supports.Add(new SupportCommonModel { ID = 5, Name = "REJECTED" });
            supports.Add(new SupportCommonModel { ID = 6, Name = "COMPLETED" });
            supports.Add(new SupportCommonModel { ID = 7, Name = "VERIFIED" });


            return supports;
        }
        public IEnumerable<SupportCommonModel> GetAllSupportPriority()
        {
            var supportPriorityList = supportRepository.GetAllPriority().Select(a => new SupportCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportPriorityList;
        }
        public IEnumerable<SupportCommonModel> GetAllSupportCategoryListByTypeId(long supporttypeid)
        {
            var supportCategoryList = supportRepository.GetAllCategory().Where(a => a.SupportTypeID == supporttypeid).Select(a => new SupportCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportCategoryList;
        }
        public IEnumerable<SupportCommonModel> GetSupportTypeByDepartmentID(long departmentid)
        {
            var supportTypeList = supportRepository.GetAllSupportType().Where(a => a.Active == true && a.DepartmentID == departmentid).Select(a => new SupportCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportTypeList;
        }
        public IEnumerable<SupportTypeModel> GetAllSupportTypeList()
        {
            var supportTypeLists = supportRepository.GetAllSupportType().Where(a => a.Active == true).Select(a => new SupportTypeModel
            {
                ID = a.ID,
                Name = a.Name,
                IsAvailable = a.IsAvailable
            }).ToList();

            return supportTypeLists;
        }
        public IEnumerable<SupportCommonModel> GetSupportSubCategoryListByCategoryId(long categoryid)
        {
            IEnumerable<SupportCommonModel> result = supportRepository.GetAllSubCategory().Where(a => a.CategoryID == categoryid).Select(a => new SupportCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return result;
        }

        public IEnumerable<BuildingModel> GetBuildingsByOrganizationId(long organizationId)
        {
            var buildingList = supportRepository.GetAllBuilding();
            var locationList = supportRepository.GetAllLocation().Where(a => a.OrganizationID == organizationId);
            var organizationList = organizationRepository.GetAllOrganization();
            var citiesList = cityRepository.GetAllCity();
            var stateList = stateRepository.GetAllState();
            var countryList = countryRepository.GetAllCountries();

            IEnumerable<BuildingModel> result = (from a in buildingList
                                                 join b in locationList on a.LocationID equals b.ID
                                                 join c in organizationList on b.OrganizationID equals c.ID
                                                 join d in citiesList on b.CityID equals d.ID
                                                 join e in stateList on d.StateID equals e.ID
                                                 join f in countryList on e.CountryID equals f.ID
                                                 select new BuildingModel
                                                 {
                                                     ID = a.ID,
                                                     Name = a.Name,
                                                     BuildingTypeId = a.BuildingTypeID,
                                                     BuildingTypeName = a.BuildingTypeID == 1 ? "Campus" : "Hostel",
                                                     LocationId = a.LocationID,
                                                     LocationName = b.Name,
                                                     Address = b.Address,
                                                     OrganizationId = b.OrganizationID,
                                                     OrganizationName = c.Name,
                                                     CityId = b.CityID,
                                                     CityName = d.Name,
                                                     StateId = d.StateID,
                                                     StateName = e.Name,
                                                     CountryId = e.CountryID,
                                                     CountryName = f.Name

                                                 }).ToList();

            return result;

        }

        public IEnumerable<SupportRequestResponseModel> GetSupportRequestByEmployeeId(long employeeId, long statusId, string mstatus, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {

            var supportRequestList = supportRepository.GetAllSupportRequest().Where(x => x.InsertedId == employeeId).ToList();

            if (supportRequestList != null)
            {
                var employee = employeeRepository.GetAllEmployee();
                var cat = supportRepository.GetAllCategory().ToList();
                var subcat = supportRepository.GetAllSubCategory().ToList();
                var loc = supportRepository.GetAllLocation().ToList();
                var prior = supportRepository.GetAllPriority().ToList();
                var support = supportRepository.GetAllSupportType().ToList();
                var room = supportRepository.GetAllRoom();
                var assignedTo = _dbContext.SupportRequestAssign.ToList();
                var build = supportRepository.GetAllBuilding();
                var floorList = supportRepository.GetAllFloor();

                IEnumerable<SupportRequestResponseModel> result = (from a in supportRequestList
                                                                   join b in cat on a.CategoryID equals b.ID
                                                                   join c in subcat on a.SubCategoryID equals c.ID
                                                                   join g in prior on a.PriorityID equals g.ID
                                                                   join h in support on a.SupportTypeID equals h.ID


                                                                   select new SupportRequestResponseModel
                                                                   {
                                                                       ID = a.ID,
                                                                       CategoryID = a.CategoryID,
                                                                       CategoryName = b.Name,
                                                                       SubCategoryID = a.SubCategoryID,
                                                                       SubCategoryName = c.Name,
                                                                       BuildingID = a.BuildingID,
                                                                       PriorityID = a.PriorityID,
                                                                       PriorityName = g.Name,
                                                                       RoomID = a.RoomID,
                                                                       Title = a.Title,
                                                                       TicketCode = a.TicketCode != null ? a.TicketCode : "NA",
                                                                       BuildingName = a.BuildingID != 0 ? build != null ? build.Where(m => m.ID == a.BuildingID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE : ViewConstants.NOT_APPLICABLE,
                                                                       RoomName = a.RoomID != 0 ? room != null ?
                                                                       room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name /*+ floorList.Where(z => z.ID == room.FirstOrDefault().FloorID).Select(v => v.Name).FirstOrDefault()*/ :
                                                                       ViewConstants.NOT_APPLICABLE : ViewConstants.NOT_APPLICABLE,
                                                                       FloorID = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().FloorID : 0,
                                                                       FloorName = a.RoomID != 0 ? floorList.Where(x => x.ID == room.Where(z => z.ID == a.RoomID).FirstOrDefault().FloorID).Select(v => v.Name).FirstOrDefault() : ViewConstants.NOT_APPLICABLE,
                                                                       AssignedToEmp = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                                         employeeRepository.GetEmployeeFullNameById(assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId) : "NA" : "NA",
                                                                       AssignedDate = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                                          assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().InsertedDate.ToString() : "NA" : "NA",

                                                                       OrgGroupName = commonMethods.GetOrgGroupName(a.InsertedId),
                                                                       IsApprove = a.IsApprove,
                                                                       StatusID = GetSupportStatusId(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                       Status = GetSupportStatus(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                       SupportTypeID = a.SupportTypeID,
                                                                       ModifiedDate = a.ModifiedDate,
                                                                       SupportTypeName = h.Name,
                                                                       InsertedDate = a.InsertedDate,
                                                                       InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                                       Description = a.Description,
                                                                       CompletionDate = a.CompletionDate,
                                                                       CompletionDateString = NullableDateTimeToStringDate(a.CompletionDate),
                                                                       SentApproval = a.SendApproval,
                                                                       EmpId = a.InsertedId,
                                                                       EmpCode = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.EmpCode).FirstOrDefault() : "NA",
                                                                       EmpFName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault() : "NA",
                                                                       EmpMName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.MiddleName).FirstOrDefault() : "NA",
                                                                       EmpLName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.LastName).FirstOrDefault() : "NA",
                                                                       EmpFullName = employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault() + " " + employee.Where(z => z.ID == a.InsertedId).Select(z => z.LastName).FirstOrDefault(),

                                                                       EmpName = employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault(),

                                                                       RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                                       DueDate = NullableDateTimeToStringDate(a.DueDate),
                                                                       SentApprovalDate = NullableDateTimeToStringDate(a.SentApprovalDate),
                                                                       ApproveDate = NullableDateTimeToStringDate(a.ApproveDate),
                                                                       ManagementApprovalDate = NullableDateTimeToStringDate(a.ManagementApprovedDate),
                                                                       //ManagementRejectDate = NullableDateTimeToStringDate(a.ManagementRejectDate),
                                                                       commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, a.InsertedId),
                                                                       Comment = a.Comment,
                                                                       SendManagementApprovalDate = NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                                                       SentManagementName = a.SentApprovalID != null && a.SentApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       ManagementApproved = a.ManagementApproval,
                                                                       ManagementApprovedDate = NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                                                       ManagementApprovedName = a.ManagementApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       ManagementRejected = a.ManagementReject,
                                                                       ManagementRejectedDate = NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                                                       ManagementRejectedName = a.ManagementRejectedID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       SendManagementApproval = a.SendApproval,
                                                                       //Management = (
                                                                       //a.SendApproval,
                                                                       // NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                                                       //  a.SentApprovalID != null && a.SentApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       //a.ManagementApproval,
                                                                       //NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                                                       //a.ManagementApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       //a.ManagementReject,
                                                                       //NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                                                       //a.ManagementRejectedID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, a.InsertedId) : ViewConstants.NOT_APPLICABLE


                                                                       //),
                                                                       ApprovedID = a.ApprovedID,
                                                                       RejectedID = a.RejectedID,
                                                                       RejectDate = NullableDateTimeToStringDate(a.RejectDate),
                                                                       IsReject = a.IsReject,
                                                                       Score = a.Score,
                                                                       ScoreGivenId = a.ScoreGivenId,
                                                                       ScoreGivenDate = a.ScoreGivenDate


                                                                   }
                         ).OrderByDescending(a => a.ID).ToList();

                string finalTime = "23:59:59";
                Nullable<DateTime> tt = DateTime.Parse(finalTime);
                Nullable<DateTime> toDate = ToDate + tt.Value.TimeOfDay;


                if (FromDate != null || FromDate.ToString() != "")
                {
                    if (FromDate != null && ToDate != null)
                    {
                        result = result.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= toDate).ToList();
                    }
                    else
                    {
                        result = result.Where(a => a.InsertedDate >= FromDate).ToList();
                    }
                }
                if (statusId != 0)
                {
                    result = result.Where(a => a.StatusID == statusId).ToList();
                }
                if (mstatus != null)
                {
                    if (mstatus == "Pending")
                    {
                        result = result.Where(a => a.SendManagementApproval == true && a.ManagementApproved == false && a.ManagementRejected == false).ToList();
                    }
                    else if (mstatus == "Approved")
                    {
                        result = result.Where(a => a.SendManagementApproval == true && a.ManagementApproved == true && a.ManagementRejected == false).ToList();
                    }
                    else
                    {
                        result = result.Where(a => a.SendManagementApproval == true && a.ManagementRejected == true && a.ManagementApproved == false).ToList();
                    }
                }
                return result;

            }
            return null;
        }

        public IEnumerable<SupportRequestResponseModel> GetSupportRequestListById(long id)
        {

            var statusList = supportRepository.GetAllStatus();
            var requests = supportRepository.GetAllSupportRequest().Where(a => a.ID == id).ToList();

            if (requests != null)
            {
                var employee = employeeRepository.GetAllEmployee();
                var cat = supportRepository.GetAllCategory().ToList();
                var subcat = supportRepository.GetAllSubCategory().ToList();
                var loc = supportRepository.GetAllLocation().ToList();
                var prior = supportRepository.GetAllPriority().ToList();
                var support = supportRepository.GetAllSupportType().ToList();
                var room = supportRepository.GetAllRoom();
                var build = supportRepository.GetAllBuilding();
                var floorList = supportRepository.GetAllFloor();

                IEnumerable<SupportRequestResponseModel> result = (from a in requests
                                                                   join b in cat on a.CategoryID equals b.ID
                                                                   join c in subcat on a.SubCategoryID equals c.ID
                                                                   join g in prior on a.PriorityID equals g.ID
                                                                   join h in support on a.SupportTypeID equals h.ID


                                                                   select new SupportRequestResponseModel
                                                                   {
                                                                       ID = a.ID,

                                                                       CategoryID = a.CategoryID,
                                                                       CategoryName = b.Name,
                                                                       SubCategoryID = a.SubCategoryID,
                                                                       SubCategoryName = c.Name,
                                                                       BuildingID = a.BuildingID,
                                                                       PriorityID = a.PriorityID,
                                                                       PriorityName = g.Name,
                                                                       RoomID = a.RoomID,
                                                                       TicketCode = a.TicketCode,
                                                                       BuildingName = a.BuildingID != 0 ? build != null ? build.Where(m => m.ID == a.BuildingID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE : ViewConstants.NOT_APPLICABLE,
                                                                       RoomName = a.RoomID != 0 ? room != null ?
                                                                       room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name :
                                                                       ViewConstants.NOT_APPLICABLE : ViewConstants.NOT_APPLICABLE,
                                                                       //FloorID = room.FirstOrDefault().FloorID,
                                                                       // FloorName = floorList.Where(x=>x.ID == room.FirstOrDefault().FloorID).Select(v=>v.Name).FirstOrDefault(),

                                                                       FloorID = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().FloorID : 0,
                                                                       FloorName = a.RoomID != 0 ? floorList.Where(x => x.ID == room.Where(z => z.ID == a.RoomID).FirstOrDefault().FloorID).Select(v => v.Name).FirstOrDefault() : ViewConstants.NOT_APPLICABLE,

                                                                       OrgGroupName = commonMethods.GetOrgGroupName(a.InsertedId),
                                                                       IsApprove = a.IsApprove,
                                                                       StatusID = GetSupportStatusId(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                       Status = GetSupportStatus(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                       SupportTypeID = a.SupportTypeID,
                                                                       ModifiedDate = a.ModifiedDate,
                                                                       Title = a.Title != null ? a.Title : "NA",
                                                                       SupportTypeName = h.Name,
                                                                       InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                                       Description = a.Description != null ? a.Description : "NA",
                                                                       CompletionDate = a.CompletionDate,
                                                                       CompletionDateString = NullableDateTimeToStringDate(a.CompletionDate),
                                                                       SendManagementApproval = a.SendApproval,
                                                                       EmpId = a.InsertedId,
                                                                       EmpCode = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.EmpCode).FirstOrDefault() : "NA",
                                                                       EmpFName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault() : "NA",
                                                                       EmpMName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.MiddleName).FirstOrDefault() : "NA",
                                                                       EmpLName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.LastName).FirstOrDefault() : "NA",
                                                                       EmpFullName = employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault() + " " + employee.Where(z => z.ID == a.InsertedId).Select(z => z.LastName).FirstOrDefault(),

                                                                       EmpName = employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault(),
                                                                       SentApproval = a.SendApproval,
                                                                       RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                                       DueDate = NullableDateTimeToStringDate(a.DueDate),
                                                                       SentApprovalDate = NullableDateTimeToStringDate(a.SentApprovalDate),
                                                                       ApproveDate = NullableDateTimeToStringDate(a.ApproveDate),
                                                                       ManagementApprovalDate = NullableDateTimeToStringDate(a.ManagementApprovedDate),
                                                                       //ManagementRejectDate = NullableDateTimeToStringDate(a.ManagementRejectDate),
                                                                       commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, a.InsertedId),
                                                                       Comment = a.Comment,
                                                                       SendManagementApprovalDate = NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                                                       SentManagementName = a.SentApprovalID != null && a.SentApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       ManagementApproved = a.ManagementApproval,
                                                                       ManagementApprovedDate = NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                                                       ManagementApprovedName = a.ManagementApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       ManagementRejected = a.ManagementReject,
                                                                       ManagementRejectedDate = NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                                                       ManagementRejectedName = a.ManagementRejectedID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, a.InsertedId) : ViewConstants.NOT_APPLICABLE,

                                                                       //Management = (
                                                                       //a.SendApproval,
                                                                       // NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                                                       //  a.SentApprovalID != null && a.SentApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       //a.ManagementApproval,
                                                                       //NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                                                       //a.ManagementApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       //a.ManagementReject,
                                                                       //NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                                                       //a.ManagementRejectedID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, a.InsertedId) : ViewConstants.NOT_APPLICABLE

                                                                       //),
                                                                       ApprovedID = a.ApprovedID,
                                                                       RejectedID = a.RejectedID,
                                                                       RejectDate = NullableDateTimeToStringDate(a.RejectDate),
                                                                       IsReject = a.IsReject
                                                                   }
                         ).OrderByDescending(a => a.ID).ToList();
                return result;

            }


            return null;

        }

        public IEnumerable<SupportRequestResponseModel> GetSupportRequestListByIds(long id, long userid)
        {

            var statusList = supportRepository.GetAllStatus();
            var requests = supportRepository.GetAllSupportRequest().Where(a => a.ID == id).ToList();

            if (requests != null)
            {
                var employee = employeeRepository.GetAllEmployee();
                var cat = supportRepository.GetAllCategory().ToList();
                var subcat = supportRepository.GetAllSubCategory().ToList();
                var loc = supportRepository.GetAllLocation().ToList();
                var prior = supportRepository.GetAllPriority().ToList();
                var support = supportRepository.GetAllSupportType().ToList();
                var room = supportRepository.GetAllRoom();
                var build = supportRepository.GetAllBuilding();
                var floorList = supportRepository.GetAllFloor();
                var assignedTo = _dbContext.SupportRequestAssign.ToList();

                IEnumerable<SupportRequestResponseModel> result = (from a in requests
                                                                   join b in cat on a.CategoryID equals b.ID
                                                                   join c in subcat on a.SubCategoryID equals c.ID
                                                                   join g in prior on a.PriorityID equals g.ID
                                                                   join h in support on a.SupportTypeID equals h.ID


                                                                   select new SupportRequestResponseModel
                                                                   {
                                                                       ID = a.ID,

                                                                       CategoryID = a.CategoryID,
                                                                       CategoryName = b.Name,
                                                                       SubCategoryID = a.SubCategoryID,
                                                                       SubCategoryName = c.Name,
                                                                       BuildingID = a.BuildingID,
                                                                       PriorityID = a.PriorityID,
                                                                       PriorityName = g.Name,
                                                                       RoomID = a.RoomID,
                                                                       TicketCode = a.TicketCode,
                                                                       BuildingName = a.BuildingID != 0 ? build != null ? build.Where(m => m.ID == a.BuildingID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE : ViewConstants.NOT_APPLICABLE,
                                                                       RoomName = a.RoomID != 0 ? room != null ?
                                                                       room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name :
                                                                       ViewConstants.NOT_APPLICABLE : ViewConstants.NOT_APPLICABLE,
                                                                       //FloorID = room.FirstOrDefault().FloorID,
                                                                       // FloorName = floorList.Where(x=>x.ID == room.FirstOrDefault().FloorID).Select(v=>v.Name).FirstOrDefault(),

                                                                       FloorID = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().FloorID : 0,
                                                                       FloorName = a.RoomID != 0 ? floorList.Where(x => x.ID == room.Where(z => z.ID == a.RoomID).FirstOrDefault().FloorID).Select(v => v.Name).FirstOrDefault() : ViewConstants.NOT_APPLICABLE,

                                                                       OrgGroupName = commonMethods.GetOrgGroupName(a.InsertedId),
                                                                       IsApprove = a.IsApprove,
                                                                       StatusID = GetSupportStatusId(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                       Status = GetSupportStatus(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                       SupportTypeID = a.SupportTypeID,
                                                                       ModifiedDate = a.ModifiedDate,
                                                                       Title = a.Title,
                                                                       SupportTypeName = h.Name,
                                                                       InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                                       ModifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                                                                       Description = a.Description,
                                                                       InsertedDate = a.InsertedDate,
                                                                       CompletionDate = a.CompletionDate,
                                                                       CompletionDateString = NullableDateTimeToStringDate(a.CompletionDate),
                                                                       SendManagementApproval = a.SendApproval,
                                                                       EmpId = a.InsertedId,
                                                                       EmpCode = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.EmpCode).FirstOrDefault() : "NA",
                                                                       EmpFName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault() : "NA",
                                                                       EmpMName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.MiddleName).FirstOrDefault() : "NA",
                                                                       EmpLName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.LastName).FirstOrDefault() : "NA",
                                                                       EmpFullName = employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault() + " " + employee.Where(z => z.ID == a.InsertedId).Select(z => z.LastName).FirstOrDefault(),
                                                                       AssignedToEmp = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                                            employeeRepository.GetEmployeeFullNameById(assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId) : "NA" : "NA",
                                                                       AssignedDate = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                                            assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().InsertedDate.ToString() : "NA" : "NA",

                                                                       EmpName = employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault(),
                                                                       SentApproval = a.SendApproval,
                                                                       RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                                       DueDate = NullableDateTimeToStringDate(a.DueDate),
                                                                       SentApprovalDate = NullableDateTimeToStringDate(a.SentApprovalDate),
                                                                       ApproveDate = NullableDateTimeToStringDate(a.ApproveDate),
                                                                       ManagementApprovalDate = NullableDateTimeToStringDate(a.ManagementApprovedDate),
                                                                       //ManagementRejectDate = NullableDateTimeToStringDate(a.ManagementRejectDate),
                                                                       commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, a.InsertedId),
                                                                       Comment = a.Comment,
                                                                       SendManagementApprovalDate = NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                                                       SentManagementName = a.SentApprovalID != null && a.SentApprovalID > 0 ? a.SentApprovalID == userid ? "me" : commonMethods.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       ManagementApproved = a.ManagementApproval,
                                                                       ManagementApprovedDate = NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                                                       ManagementApprovedName = a.ManagementApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       ManagementRejected = a.ManagementReject,
                                                                       ManagementRejectedDate = NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                                                       ManagementRejectedName = a.ManagementRejectedID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, a.InsertedId) : ViewConstants.NOT_APPLICABLE,

                                                                       //Management = (
                                                                       //a.SendApproval,
                                                                       // NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                                                       //  a.SentApprovalID != null && a.SentApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       //a.ManagementApproval,
                                                                       //NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                                                       //a.ManagementApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                       //a.ManagementReject,
                                                                       //NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                                                       //a.ManagementRejectedID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, a.InsertedId) : ViewConstants.NOT_APPLICABLE

                                                                       //),
                                                                       ApprovedID = a.ApprovedID,
                                                                       RejectedID = a.RejectedID,
                                                                       RejectDate = NullableDateTimeToStringDate(a.RejectDate),
                                                                       IsReject = a.IsReject,
                                                                       Score = a.Score,
                                                                       ScoreGivenDate = a.ScoreGivenDate,
                                                                       ScoreGivenId = a.ScoreGivenId,
                                                                       ScoreGivenDateString = NullableDateTimeToStringDate(a.ScoreGivenDate),
                                                                   }
                         ).OrderByDescending(a => a.ID).ToList();
                return result;

            }


            return null;

        }

        public IEnumerable<SupportRequestResponseModel> GetSupportRequestListBySupportCategoryIdStatusIdPriorityId(SupportRequestModel requestModel)
        {
            try
            {

                IEnumerable<SupportRequest> supportRequestList = null;
                DateTime From_date = Convert.ToDateTime(requestModel.FromDate).Date;
                DateTime To_date = Convert.ToDateTime(requestModel.ToDate).Date;
                // Employe Id Filter
                if (requestModel.employeeId != 0)
                {
                    supportRequestList = supportRepository.GetAllSupportRequest().Where(a => a.InsertedId == requestModel.employeeId).ToList();
                }



                if (supportRequestList != null)
                {
                    var employee = employeeRepository.GetAllEmployee();
                    var cat = supportRepository.GetAllCategory().ToList();
                    var subcat = supportRepository.GetAllSubCategory().ToList();
                    var loc = supportRepository.GetAllLocation().ToList();
                    var prior = supportRepository.GetAllPriority().ToList();
                    var support = supportRepository.GetAllSupportType().ToList();
                    var room = supportRepository.GetAllRoom();

                    var build = supportRepository.GetAllBuilding();
                    var floorList = supportRepository.GetAllFloor();

                    IEnumerable<SupportRequestResponseModel> result = (from a in supportRequestList
                                                                           //join b in cat on a.CategoryID equals b.ID
                                                                           //join c in subcat on a.SubCategoryID equals c.ID
                                                                           //join g in prior on a.PriorityID equals g.ID
                                                                           //join h in support on a.SupportTypeID equals h.ID


                                                                       select new SupportRequestResponseModel
                                                                       {
                                                                           ID = a.ID,
                                                                           CategoryID = a.CategoryID,
                                                                           CategoryName = a.CategoryID != 0 ? cat.Where(z => z.ID == a.CategoryID).Select(z => z.Name).FirstOrDefault() : "NA",
                                                                           SubCategoryID = a.SubCategoryID,
                                                                           SubCategoryName = a.SubCategoryID != 0 ? subcat.Where(z => z.ID == a.SubCategoryID).Select(z => z.Name).FirstOrDefault() : "NA",
                                                                           BuildingID = a.BuildingID,
                                                                           PriorityID = a.PriorityID,
                                                                           PriorityName = a.PriorityID != 0 ? prior.Where(z => z.ID == a.PriorityID).Select(z => z.Name).FirstOrDefault() : "NA",
                                                                           RoomID = a.RoomID,
                                                                           Title = a.Title,
                                                                           TicketCode = a.TicketCode,
                                                                           BuildingName = a.BuildingID != 0 ? build != null ? build.Where(m => m.ID == a.BuildingID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE : ViewConstants.NOT_APPLICABLE,
                                                                           RoomName = a.RoomID != 0 ? room != null ?
                                                                           room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name :
                                                                           ViewConstants.NOT_APPLICABLE : ViewConstants.NOT_APPLICABLE,
                                                                           FloorID = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().FloorID : 0,
                                                                           FloorName = a.RoomID != 0 ? floorList.Where(x => x.ID == room.Where(z => z.ID == a.RoomID).FirstOrDefault().FloorID).Select(v => v.Name).FirstOrDefault() : ViewConstants.NOT_APPLICABLE,
                                                                           OrgGroupName = commonMethods.GetOrgGroupName(a.InsertedId),
                                                                           IsApprove = a.IsApprove,
                                                                           StatusID = GetSupportStatusId(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                           Status = GetSupportStatus(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                           SupportTypeID = a.SupportTypeID,
                                                                           ModifiedDate = a.ModifiedDate,
                                                                           SupportTypeName = a.SupportTypeID != 0 ? support.Where(z => z.ID == a.SupportTypeID).Select(z => z.Name).FirstOrDefault() : "NA",
                                                                           InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                                           ModifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                                                                           Description = a.Description,
                                                                           CompletionDate = a.CompletionDate,
                                                                           CompletionDateString = NullableDateTimeToStringDate(a.CompletionDate),
                                                                           SentApproval = a.SendApproval,
                                                                           InsertedDate = a.InsertedDate,
                                                                           EmpId = a.InsertedId,
                                                                           EmpCode = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.EmpCode).FirstOrDefault() : "NA",
                                                                           EmpFName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault() : "NA",
                                                                           EmpMName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.MiddleName).FirstOrDefault() : "NA",
                                                                           EmpLName = a.InsertedId != 0 ? employee.Where(z => z.ID == a.InsertedId).Select(z => z.LastName).FirstOrDefault() : "NA",
                                                                           EmpFullName = employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault() + " " + employee.Where(z => z.ID == a.InsertedId).Select(z => z.LastName).FirstOrDefault(),
                                                                           SendManagementApproval = a.SendApproval,
                                                                           EmpName = employee.Where(z => z.ID == a.InsertedId).Select(z => z.FirstName).FirstOrDefault(),

                                                                           RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                                           DueDate = NullableDateTimeToStringDate(a.DueDate),
                                                                           SentApprovalDate = NullableDateTimeToStringDate(a.SentApprovalDate),
                                                                           ApproveDate = NullableDateTimeToStringDate(a.ApproveDate),
                                                                           ManagementApprovalDate = NullableDateTimeToStringDate(a.ManagementApprovedDate),
                                                                           commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, a.InsertedId),
                                                                           Comment = a.Comment,
                                                                           SendManagementApprovalDate = NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                                                           SentManagementName = a.SentApprovalID != null && a.SentApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                           ManagementApproved = a.ManagementApproval,
                                                                           ManagementApprovedDate = NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                                                           ManagementApprovedName = a.ManagementApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                           ManagementRejected = a.ManagementReject,
                                                                           ManagementRejectedDate = NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                                                           ManagementRejectedName = a.ManagementRejectedID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, a.InsertedId) : ViewConstants.NOT_APPLICABLE,

                                                                           ApprovedID = a.ApprovedID,
                                                                           RejectedID = a.RejectedID,
                                                                           RejectDate = NullableDateTimeToStringDate(a.RejectDate),
                                                                           IsReject = a.IsReject,
                                                                           Score = a.Score,
                                                                           ScoreGivenId = a.ScoreGivenId,
                                                                           ScoreGivenDate = a.ScoreGivenDate,
                                                                           ScoreGivenDateString = NullableDateTimeToStringDate(a.ScoreGivenDate),



                                                                       }
                             ).OrderByDescending(a => a.ID).ToList();



                    // FromDate  Filter-0
                    if (requestModel.FromDate != null)
                    {
                        result = result.Where(a => a.InsertedDate.Date == From_date.Date).ToList();
                    }
                    // FromDate and ToDate Range  Filter-1
                    if (requestModel.FromDate != null && requestModel.ToDate != null)
                    {
                        result = result.Where(a => (a.InsertedDate.Date >= From_date.Date) && (a.InsertedDate.Date <= To_date.Date)).ToList();
                    }

                    // Support Category Id  Filter-2

                    if (requestModel.SupportCategoryId != 0)
                    {
                        result = result.Where(x => x.CategoryID == requestModel.SupportCategoryId);
                    }

                    // Support Status Id  Filter -3
                    if (requestModel.SupportStatusId != 0)
                    {
                        result = result.Where(a => a.StatusID == requestModel.SupportStatusId);
                    }

                    // Support Priority Id   Filter-4
                    if (requestModel.SupportPriorityId != 0)
                    {
                        result = result.Where(a => a.PriorityID == requestModel.SupportPriorityId);
                    }
                    return result;

                }
            }
            catch (Exception e1)
            {

                return null;
            }
            return null;
        }


        #region  Support Master



        public IEnumerable<SupportCommonModel> GetAllGroupList()
        {
            var supportTypeLists = groupRepository.GetAllGroup().Where(a => a.Active == true).Select(a => new SupportCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportTypeLists;
        }
        public IEnumerable<OrganizationModel> GetAllOrganizationListByGroupId(long groupid)
        {


            var organizationList = organizationRepository.GetAllOrganizationByGroupId(groupid).Where(a => a.Active == true).ToList();
            var GroupList = groupRepository.GetAllGroup();
            var OrganizationTypesList = organizationTypeRepository.GetAllOrganizationType();

            IEnumerable<OrganizationModel> result = (from a in organizationList
                                                     join c in GroupList on a.GroupID equals c.ID

                                                     select new OrganizationModel
                                                     {
                                                         ID = a.ID,
                                                         Name = a.Name,
                                                         GroupId = c.ID,
                                                         GroupName = c.Name,
                                                         OrganizationTypeId = a.OrganizationTypeID,
                                                         OrganizationTypeName = OrganizationTypesList.Where(e => e.ID == a.OrganizationTypeID).Select(e => e.Name).FirstOrDefault()
                                                     }).ToList();

            return result;
        }







        public IEnumerable<DepartmentModel> GetAllDepartmentListByGroupID(long groupid)
        {


            var departmenList = departmentRepository.GetDepartmentListByGroupId(groupid).Where(a => a.Active == true).ToList();
            var GroupList = groupRepository.GetAllGroup();
            var OrganizationList = organizationRepository.GetAllOrganization();

            IEnumerable<DepartmentModel> result = (from a in departmenList
                                                   join c in GroupList on a.GroupID equals c.ID

                                                   select new DepartmentModel
                                                   {
                                                       ID = a.ID,
                                                       Name = a.Name,
                                                       GroupID = a.GroupID,
                                                       GroupName = c.Name,
                                                       OrganizationID = a.OrganizationID,
                                                       OrganizationName = a.OrganizationID != 0 ? OrganizationList.Where(e => e.ID == a.OrganizationID).Select(e => e.Name).FirstOrDefault() : "NA"
                                                   }).ToList();

            return result;
        }







        public IEnumerable<DepartmentModel> GetAllDepartmentListByOrganizationId(long organisationid)
        {


            var departmenList = departmentRepository.GetDepartmentListByOrganizationId(organisationid).Where(a => a.Active == true).ToList();
            var GroupList = groupRepository.GetAllGroup();
            var OrganizationList = organizationRepository.GetAllOrganization();

            IEnumerable<DepartmentModel> result = (from a in departmenList
                                                   join c in OrganizationList on a.OrganizationID equals c.ID

                                                   select new DepartmentModel
                                                   {
                                                       ID = a.ID,
                                                       Name = a.Name,
                                                       GroupID = a.GroupID,
                                                       GroupName = a.GroupID != 0 ? GroupList.Where(e => e.ID == a.GroupID).Select(e => e.Name).FirstOrDefault() : "NA",
                                                       OrganizationID = a.OrganizationID,
                                                       OrganizationName = c.Name
                                                   }).ToList();

            return result;
        }
        //Module List
        public IEnumerable<SupportCommonModel> GetAllModuleList()
        {
            var supportTypeLists = moduleRepository.GetAllModule().Where(a => a.Active == true).Select(a => new SupportCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportTypeLists;
        }


        //Sub Module List
        public IEnumerable<SupportCommonModel> GetAllSubModuleList()
        {
            var supportTypeLists = subModuleRepository.GetAllSubModule().Select(a => new SupportCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportTypeLists;
        }

        //Action List
        public IEnumerable<SupportCommonModel> GetAllActionList()
        {
            var supportTypeLists = actionRepository.GetAllAction().Select(a => new SupportCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportTypeLists;
        }


        #endregion


        #region GetAllSupportCategoriesList

        public IEnumerable<SupportCommonModel> GetAllSupportCategoriesList()
        {
            var supportStatusList = supportRepository.GetAllCategory().Select(a => new SupportCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportStatusList;
        }
        #endregion

        #region






        public IEnumerable<SupportRequestDetailsModels> GetAllSupportTimeLineList(long supportrequestid)

        {
            var timeline = supportRepository.GetAllSupportTimeLine().Where(z => z.SupportRequestID == supportrequestid).ToList();
            var request = supportRepository.GetAllSupportRequest().ToList();
            var emp = employeeRepository.GetAllEmployee().ToList();
            var SupportType = supportRepository.GetAllSupportType().ToList();

            IEnumerable<SupportRequestDetailsModels> res = (from a in timeline
                                                            join b in request on a.SupportRequestID equals b.ID
                                                            join c in emp on a.InsertedId equals c.ID
                                                            select new SupportRequestDetailsModels
                                                            {
                                                                ID = a.ID,
                                                                SupportRequestID = a.SupportRequestID,
                                                                EmployeeName = c.FirstName + " " + c.LastName,
                                                                EmployeeImage = c.Image != null ? c.Image : "NA",
                                                                PostedDate = a.InsertedDate.ToString("dd-MMM-yyyy"),
                                                                PostedTime = a.InsertedDate.ToString("hh:mm tt"),
                                                                SupportStatus = a.Status,
                                                                C_Name = a.comment != null ? a.comment : ""
                                                            }
                     ).ToList();




            return res;


        }


        public IEnumerable<SupportRequestDetailsModels> GetAllSupportCommentsList(long supportrequestid, long userid)
        {

            var Comment = supportRepository.GetAllSupportComment().Where(z => z.SupportRequestID == supportrequestid).ToList();
            var request = supportRepository.GetAllSupportRequest();
            var emp = employeeRepository.GetAllEmployee();
            var SupportType = supportRepository.GetAllSupportType().ToList();

            IEnumerable<SupportRequestDetailsModels> res = (from a in Comment
                                                            join b in request on a.SupportRequestID equals b.ID
                                                            join c in emp on a.InsertedId equals c.ID
                                                            select new SupportRequestDetailsModels
                                                            {
                                                                ID = a.ID,
                                                                SupportRequestID = a.SupportRequestID,
                                                                Description = b.Description != null ? b.Description : "NA",
                                                                SupportTypeID = b.SupportTypeID,
                                                                SupportTypeName = b.SupportTypeID != 0 ? SupportType.Where(x => x.ID == b.SupportTypeID).Select(x => x.Name).FirstOrDefault() : "NA",
                                                                EmpId = a.InsertedId,
                                                                EmployeeName = userid != a.InsertedId ? c.FirstName + " " + c.LastName : "me",
                                                                EmployeeImage = c.Image != null ? c.Image : "NA",
                                                                PostedDate = a.InsertedDate.ToString("dd-MMM-yyyy"),
                                                                PostedTime = a.InsertedDate.ToString("hh:mm tt"),
                                                                SupportStatus = a.Status,
                                                                C_Name = a.Name != null ? a.Name : "",
                                                                ModifiedDate = a.ModifiedDate.ToString("dd-MMM-yyyy"),
                                                                ModifiedTime = a.ModifiedDate.ToString("hh:mm tt"),
                                                                ModifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                                                                InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                                //models.CommentList = AllComments(supportrequestid)

                                                            }
                     ).ToList();

            return res;
        }

        public SupportRequestDetailsModels GetSingleSupportComments(long supportrequestid, long userid)

        {
            List<SupportRequestDetailsModels> models = new List<SupportRequestDetailsModels>();

            var Comment = supportRepository.GetAllSupportComment().Where(z => z.SupportRequestID == supportrequestid).ToList();
            var request = supportRepository.GetAllSupportRequest();
            var emp = employeeRepository.GetAllEmployee();
            var SupportType = supportRepository.GetAllSupportType().ToList();


            var res = (from a in Comment
                       join b in request on a.SupportRequestID equals b.ID
                       join c in emp on a.InsertedId equals c.ID
                       select new SupportRequestDetailsModels
                       {
                           ID = a.ID,
                           SupportRequestID = a.SupportRequestID,
                           EmployeeName = userid != a.InsertedId ? c.FirstName + " " + c.LastName : "me",
                           EmployeeImage = c.Image != null ? c.Image : "NA",
                           PostedDate = a.InsertedDate.ToString("dd-MMM-yyyy"),
                           PostedTime = a.InsertedDate.ToString("hh:mm tt"),
                           SupportStatus = a.Status,
                           ModifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                           InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                           C_Name = a.Name != null ? a.Name : ""

                       }
                      ).ToList().LastOrDefault();

            return res;
        }

        public SupportRequestDetailsModels GetSingleSupportTimeLine(long supportrequestid, long userid)

        {
            var timeline = supportRepository.GetAllSupportTimeLine().Where(z => z.SupportRequestID == supportrequestid).ToList();
            var request = supportRepository.GetAllSupportRequest().ToList();
            var emp = employeeRepository.GetAllEmployee().ToList();
            var SupportType = supportRepository.GetAllSupportType().ToList();

            var res = (from a in timeline
                       join b in request on a.SupportRequestID equals b.ID
                       join c in emp on a.InsertedId equals c.ID
                       select new SupportRequestDetailsModels
                       {
                           ID = a.ID,
                           SupportRequestID = a.SupportRequestID,
                           EmployeeName = userid != a.InsertedId ? c.FirstName + " " + c.LastName : "me",
                           EmployeeImage = c.Image != null ? c.Image : "NA",
                           PostedDate = a.InsertedDate.ToString("dd-MMM-yyyy"),
                           PostedTime = a.InsertedDate.ToString("hh:mm tt"),
                           SupportStatus = a.Status,
                           ModifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                           InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                           C_Name = a.comment != null ? a.comment : ""
                       }
                     ).ToList().LastOrDefault();




            return res;


        }


        public IEnumerable<SupportRequestDetailsModels> AllComments(long requestId)
        {
            List<SupportRequestDetailsModels> comment = new List<SupportRequestDetailsModels>();
            var comments = supportRepository.GetAllSupportComment()
                    .Where(a => a.SupportRequestID == requestId).
                    ToList();
            var emp = employeeRepository.GetAllEmployee().ToList();

            var res = (from a in comments
                       join b in emp on a.InsertedId equals b.ID
                       select new SupportRequestDetailsModels
                       {
                           EmployeeName = b.FirstName + " " + b.LastName,
                           EmployeeImage = b.Image != null ? b.Image : "NA",
                           C_Name = a.Name != null ? a.Name : "",
                           PostedDate = a.InsertedDate.ToString("dd-MMM-yyyy"),
                           PostedTime = a.InsertedDate.ToString("hh:mm tt"),
                           ModifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                           InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                       }).ToList();
            //foreach (var a in comments)
            //{ 
            //  comment.FirstOrDefault().C_Name = a.Name != null ? a.Name : "";
            //  comment.FirstOrDefault().PostedDate = a.InsertedDate.ToString("dd-MMM-yyyy");
            //  comment.FirstOrDefault().PostedTime = a.InsertedDate.ToString("hh:mm"); 
            //}

            return res.ToList();
        }

        public IEnumerable<SupportRequestAttachmentModels> GetAllSupportAttachmentsList(long supportrequestid, long userid)
        {
            var Attachment = supportRepository.GetAllAttachment().Where(z => z.SupportRequestID == supportrequestid).ToList();
            var request = supportRepository.GetAllSupportRequest().ToList();
            var emp = employeeRepository.GetAllEmployee().ToList();
            var SupportType = supportRepository.GetAllSupportType().ToList();

            IEnumerable<SupportRequestAttachmentModels> res = (from a in Attachment
                                                            join b in request on a.SupportRequestID equals b.ID
                                                            //join c in emp on a.InsertedId equals c.ID
                                                            select new SupportRequestAttachmentModels
                                                            {
                                                                ID = a.ID,
                                                                SupportRequestID = a.SupportRequestID,
                                                                EmployeeName = userid != a.InsertedId ? employeeRepository.GetEmployeeFullNameById(a.InsertedId):"me",
                                                                //EmployeeImage = emp.Where(x => x.ID == a.InsertedId).Select(x=>x.Image).FirstOrDefault() != null ? emp.Where(x => x.ID == a.InsertedId).Select(x => x.Image).FirstOrDefault() : "NA",
                                                                PostedDate = a.InsertedDate.ToString("dd-MMM-yyyy"),
                                                                PostedTime = a.InsertedDate.ToString("hh:mm tt"),
                                                                ModifiedDate = a.ModifiedDate.ToString("dd-MMM-yyyy"),
                                                                ModifiedTime = a.ModifiedDate.ToString("hh:mm tt"),
                                                                ModifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                                                                InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                                SupportStatus = a.Status,
                                                                C_Name = a.Name != null ? a.Name : "",
                                                                Path = a.Path!=null? "http://odmpserp.thesuperguard.in/" + "ODMImages/Tempfile/" + a.Path:"NA"
                                                            }
                     ).OrderByDescending(a=>a.ID).ToList();



            return res;
        }

        #endregion
        public IEnumerable<SupportCommonModel> GetAllBuilding()
        {
            var supportStatusList = supportRepository.GetAllBuilding().Where(a => a.IsAvailable == true).Select(a => new SupportCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportStatusList;
        }

        #region Support Resolve Section


        public SupportModelLists SupportResolve(long roleId, long userId, long accessId, string Type,
          long[] OrgId,
           long[] DeptId, long[] SupportTypeId,
           long[] StatusId, long[] PriorityId, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate, string mstatus, long deptemp,SupportRequestAssign supportAssign)

        {

            SupportModelLists model = new SupportModelLists();
            model.FilterVisibility = supportResolveFilterVisibility(accessId, roleId);
            List<long> pIds = new List<long>();
            List<long> statusIds = new List<long>();
            List<long> sTypeIds = new List<long>();
            List<long> deptIds = new List<long>();
            List<long> orgIds = new List<long>();


            model.Type = Type;
            model.OrgId = OrgId;
            model.DeptId = DeptId;
            model.SupportTypeId = SupportTypeId;
            model.StatusId = StatusId;
            model.PriorityId = PriorityId;

            if (PriorityId != null && PriorityId.Count() > 0)
            {
                pIds.AddRange(PriorityId);
            }

            if (OrgId != null && OrgId.Count() > 0)
            {

                orgIds.AddRange(OrgId);
            }

            if (DeptId != null && DeptId.Count() > 0)
            {
                deptIds.AddRange(DeptId);
            }

            if (SupportTypeId != null && SupportTypeId.Count() > 0)
            {
                sTypeIds.AddRange(SupportTypeId);
            }

            if (StatusId != null && StatusId.Count() > 0)
            {
                statusIds.AddRange(StatusId);
            }

            bool deptlead = false;

            List<NameIdViewModel> list = new List<NameIdViewModel>();

            List<SupportRequest> request = new List<SupportRequest>();
            model.SupportStatus = GetAllSupportStatus().ToList();
            model.SupportPriority = GetAllSupportPriority().ToList();
            if (commonMethods.checkaccessavailable("Resolve", accessId, "Support Type", "Support", roleId) == true)
            {

                model.AllDepartmentViewModel = GetAllType().ToList();

                if (Type == null || Type == "All")
                {
                    request.AddRange(supportRepository.GetAllSupportRequest().ToList());
                }
                else
                if (Type == "GroupType")
                {
                    if (deptIds.Count() > 0)
                    {
                        if (sTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.GetAllBySupportTypeId(sTypeIds).ToList());
                        }
                        else
                        {
                            var supportTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds);

                            if (supportTypeIds != null && supportTypeIds.Count() > 0)
                            {
                                request.AddRange(supportRepository.
                                    GetAllBySupportTypeId(supportTypeIds.ToList()).ToList());
                            }

                        }
                    }
                    else
                        request.AddRange(supportRepository.GetAllBySupportTypeId(null).ToList());
                }
                else
                if (Type == "OrganizationType")
                {

                    if (orgIds.Count() > 0)
                    {
                        if (deptIds.Count() > 0)
                        {
                            if (sTypeIds.Count() > 0)
                            {
                                request.AddRange(supportRepository.
                                    GetAllBySupportTypeId(sTypeIds).ToList());
                            }
                            else
                            {
                                var supportTypeIds = supportRepository.
                                    GetSupportTypeIdsByDepartmentIdIn(deptIds);

                                if (supportTypeIds != null && supportTypeIds.Count() > 0)
                                {
                                    request.AddRange(supportRepository.
                                        GetAllBySupportTypeId(supportTypeIds.ToList())
                                        .ToList());
                                }

                            }
                        }
                        else
                        {

                            var allOrgDeptIds = departmentRepository.
                                GetDepartmentListByOrganizationIdIn(orgIds).Select(a => a.ID);
                            if (allOrgDeptIds != null && allOrgDeptIds.Count() > 0)
                            {
                                var supportTypeIds = supportRepository.
                                    GetSupportTypeIdsByDepartmentIdIn(allOrgDeptIds.Distinct().
                                    ToList());
                                if (supportTypeIds != null && supportTypeIds.Count() > 0)
                                {
                                    request.AddRange(supportRepository.
                                        GetAllBySupportTypeId(supportTypeIds.ToList())
                                        .ToList());

                                }

                            }


                        }

                    }
                    else
                    {

                        var allOrgDeptIds = departmentRepository.GetAllDepartment().Where(a => a.GroupID == null || a.GroupID == 0).Select(a => a.ID);
                        if (allOrgDeptIds != null && allOrgDeptIds.Count() > 0)
                        {
                            var supportTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(allOrgDeptIds.Distinct().ToList());
                            if (supportTypeIds != null && supportTypeIds.Count() > 0)
                            {
                                request.AddRange(supportRepository.GetAllBySupportTypeId(supportTypeIds.ToList()).ToList());

                            }

                        }
                    }

                }

            }
            else
            {
                model.AllDepartmentViewModel = GetAllTypeListAdmin().ToList();
                List<long> supportTypeIds = null;
               
                if (commonMethods.checkaccessavailable("Department Head", accessId, "AccessResolve", "Support", roleId) == true)
                {
                    deptlead = true;
                    var allDeptIds = commonMethods.GetDepartmentsByEmployeeID(userId).Select(a => a.id);
                    if (allDeptIds != null && allDeptIds.Count() > 0)
                    {
                        supportTypeIds = supportRepository.
                                GetSupportTypeIdsByDepartmentIdIn(allDeptIds.Distinct().
                                ToList()).ToList();
                        if (supportTypeIds != null && supportTypeIds.Count() > 0)
                        {


                            model.SupportTypes = (from a in
                                                      supportRepository.
                                                      GetAllSupportType().Where(a => supportTypeIds.Contains(a.ID)).ToList()
                                                  select new NameIdViewModel
                                                  {
                                                      ID = a.ID,
                                                      Name = a.Name

                                                  }

                                    ).ToList();

                        }

                    }

                    if (sTypeIds.Count() > 0)
                    {
                        request.AddRange(supportRepository.
                            GetAllBySupportTypeId(sTypeIds).ToList());
                    }
                    else
                    {

                        if (supportTypeIds != null && supportTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.
                                    GetAllBySupportTypeId(supportTypeIds)
                                    .ToList());
                        }


                    }
                }
                //    if (supportAssign == null)
                //{
                   
                //}
                else                                 //if request is assiged to another employee by dept head
                {
                    //bool isEmployee = true;
                    //request.AddRange(isEmployee);
                    request.AddRange(supportRepository.GetAllSupportRequest().ToList());
                }
            }




            if (request.Count() > 0)
            {

                List<SupportRequest> filterList = new List<SupportRequest>();

                if (statusIds.Count() > 0 && pIds.Count() > 0)
                {
                    filterList.AddRange(request.Where(a => statusIds.Contains(a.StatusID) &&
                    pIds.Contains(a.PriorityID)));
                }
                else
                if (pIds.Count() > 0)
                {
                    filterList.AddRange(request.Where(a =>
                   pIds.Contains(a.PriorityID)));
                }
                else
                if (statusIds.Count() > 0)
                {
                    filterList.AddRange(request.Where(a =>
                   statusIds.Contains(a.StatusID)));
                }
                else
                    filterList.AddRange(request);

                if (filterList.Count() > 0)
                {

                    var cat = supportRepository.GetAllCategory().ToList();
                    var subcat = supportRepository.GetAllSubCategory().ToList();
                    var loc = supportRepository.GetAllLocation().ToList();
                    var build = supportRepository.GetAllBuilding();
                    var room = supportRepository.GetAllRoom();
                    var priorities = supportRepository.GetAllPriority().ToList();
                    var assignedTo = _dbContext.SupportRequestAssign.ToList();

                    List<SupportRequestResponseModel> res = (from a in request
                                                             join b in cat on a.CategoryID equals b.ID
                                                             join c in subcat on a.SubCategoryID equals c.ID

                                                             join m in supportRepository.GetAllSupportType() on a.SupportTypeID equals m.ID

                                                             select new SupportRequestResponseModel
                                                             {
                                                                 ID = a.ID,
                                                                 Title = a.Title != null ? a.Title : "NA",
                                                                 CategoryID = a.CategoryID,
                                                                 CategoryName = b.Name,
                                                                 SubCategoryID = a.SubCategoryID,
                                                                 SubCategoryName = c.Name,
                                                                 BuildingID = a.BuildingID,
                                                                 RoomID = a.RoomID,
                                                                 EmpId = a.InsertedId,
                                                                 RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                                 BuildingName = a.BuildingID != 0 ? (build.Where(y => y.ID == a.BuildingID).FirstOrDefault() == null ? ViewConstants.NOT_APPLICABLE : build.Where(y => y.ID == a.BuildingID).FirstOrDefault().Name) :
                                                                                                      ViewConstants.NOT_APPLICABLE,
                                                                 RoomName = a.RoomID != 0 ? (room.Where(z => z.ID == a.RoomID).FirstOrDefault() == null ? ViewConstants.NOT_APPLICABLE : room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name) : ViewConstants.NOT_APPLICABLE,
                                                                 PriorityName = priorities != null ? priorities.Where(x => x.ID == a.PriorityID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE,
                                                                 StatusID = GetSupportStatusId(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                 Status = GetSupportStatus(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                 OrgGroupName = commonMethods.GetOrgGroupName(a.InsertedId),
                                                                 //t_Date = a.DueDate != null ? a.DueDate : (a.RequestedCompletionDate == null ? a.InsertedDate : a.RequestedCompletionDate),
                                                                 PriorityID = a.PriorityID,
                                                                 CompletionDate = a.CompletionDate,
                                                                 CompletionDateString = NullableDateTimeToStringDate(a.CompletionDate),
                                                                 EmpFullName = commonMethods.GetEmployeeNameByEmployeeId(a.InsertedId, userId),
                                                                 //emp_Phone = i.PrimaryMobile != null ? i.PrimaryMobile : "",
                                                                 Description = a.Description != null ? a.Description : "NA",
                                                                 SupportTypeID = a.SupportTypeID,
                                                                 SupportTypeName = m.Name,
                                                                 Comment = a.Comment,
                                                                 commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, userId),
                                                                 InsertedDate = a.InsertedDate,
                                                                 ModifiedDate = a.ModifiedDate,
                                                                 InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                                 ModifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                                                                 SentApproval = a.SendApproval,
                                                                 IsRead = a.IsRead == null ? false : a.IsRead,
                                                                 IsManagementRead = a.IsManagementRead == null ? false : a.IsManagementRead,
                                                                 DueDate = NullableDateTimeToStringDate(a.DueDate),
                                                                 ApprovedID = a.ApprovedID,
                                                                 RejectedID = a.RejectedID,
                                                                 ApproveDate = NullableDateTimeToStringDate(a.ApproveDate),
                                                                 RejectDate = NullableDateTimeToStringDate(a.RejectDate),
                                                                 IsApprove = a.IsApprove,
                                                                 IsReject = a.IsReject,
                                                                 AssignedToEmp = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                            employeeRepository.GetEmployeeFullNameById(assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId) : null : null,
                                                                 AssignedDate = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                          assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().InsertedDate.ToString() : "NA" : "NA",
                                                                 AssignToId = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                          assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId : 0 : 0,

                                                                 SendManagementApproval = a.SendApproval,
                                                                 SendManagementApprovalDate = NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                                                 SentManagementName = a.SentApprovalID != null && a.SentApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                 ManagementApproved = a.ManagementApproval,
                                                                 ManagementApprovedDate = NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                                                 ManagementApprovedName = a.ManagementApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                 ManagementRejected = a.ManagementReject,
                                                                 ManagementRejectedDate = NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                                                 ManagementRejectedName = a.ManagementRejectedID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, a.InsertedId) : ViewConstants.NOT_APPLICABLE,

                                                                 //commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, userId)


                                                             }
                             ).OrderByDescending(a => a.ID).ToList();

                    string finalTime = "23:59:59";
                    Nullable<DateTime> tt = DateTime.Parse(finalTime);
                    Nullable<DateTime> toDate = ToDate + tt.Value.TimeOfDay;

                    if (statusIds != null && statusIds.Count() > 0)
                    {
                        res = res.Where(a => statusIds.Contains(a.StatusID)).ToList();
                    }
                    if (sTypeIds != null && sTypeIds.Count() > 0)
                    {
                        res = res.Where(a => sTypeIds.Contains(a.SupportTypeID)).ToList();
                    }
                    if (pIds != null && pIds.Count() > 0)
                    {
                        res = res.Where(a => pIds.Contains(a.PriorityID)).ToList();
                    }
                    if (FromDate != null || FromDate.ToString() != "")
                    {
                        if (FromDate != null && ToDate != null)
                        {
                            res = res.Where(a => a.ModifiedDate >= FromDate && a.ModifiedDate <= toDate).ToList();
                        }
                        else
                        {
                            res = res.Where(a => a.InsertedDate >= FromDate).ToList();
                        }
                    }
                    if (mstatus != null)
                    {
                        if (mstatus == "Pending")
                        {
                            res = res.Where(a => a.SendManagementApproval == true && a.ManagementApproved == false && a.ManagementRejected == false).ToList();
                        }
                        else if (mstatus == "Approved")
                        {
                            res = res.Where(a => a.SendManagementApproval == true && a.ManagementApproved == true && a.ManagementRejected == false).ToList();
                        }
                        else
                        {
                            res = res.Where(a => a.SendManagementApproval == true && a.ManagementRejected == true && a.ManagementApproved == false).ToList();
                        }
                    }
                    if (deptemp > 0)
                    {
                        res = res.Where(a => a.AssignToId == deptemp).ToList();
                    }
                    if (deptlead == false)
                    {
                        if (supportAssign != null)
                        {
                            res = res.Where(a => a.AssignToId == userId).ToList();
                        }
                    }
                    model.SupportRequestcls = res;

                }


            }

            return model;



        }

        public string GetSupportStatus(string status, bool sent, bool reject, bool approve)
        {
            if (status == "PENDING" && (sent == false || reject == false))
            {
                return "PENDING";
            }
            if (status == "ON-HOLD" && (sent == false || reject == false))
            {
                return "ON-HOLD";
            }
            if (status == "IN-PROGRESS" && (sent == false || reject == false))
            {
                return "IN-PROGRESS";
            }
            if (sent == true && approve == true && reject == false)
            {
                return "MANAGEMENT APPROVED";
            }
            if ((sent == true && approve == false && reject == true) || status == "REJECTED")
            {
                return "REJECTED";
            }
            if (status == "COMPLETED")
            {
                return "COMPLETED";
            }
            if (status == "VERIFIED")
            {
                return "VERIFIED";
            }
            else
            {
                return "";
            }
        }
        public long GetSupportStatusId(string status, bool sent, bool reject, bool approve)
        {
            if (status == "PENDING" && (sent == false || reject == false))
            {
                return 1;
            }
            if (status == "ON-HOLD" && (sent == false || reject == false))
            {
                return 2;
            }
            if (status == "IN-PROGRESS" && (sent == false || reject == false))
            {
                return 3;
            }
            if (sent == true && approve == true && reject == false)
            {
                return 4;
            }
            if ((sent == true && approve == false && reject == true) || status == "REJECTED")
            {
                return 5;
            }
            if (status == "COMPLETED")
            {
                return 6;
            }
            if (status == "VERIFIED")
            {
                return 7;
            }
            else
            {
                return 0;
            }
        }
        public FilterVisibility supportResolveFilterVisibility(long accessId, long roleId)
        {
            FilterVisibility fv = new FilterVisibility();
            if (commonMethods.checkaccessavailable("Resolve", accessId, "Support Type", "Support", roleId) == true)
            {
                fv.DepartmentType = true;
                fv.OrganizationList = true;
                fv.DepartmentList = true;
                fv.SupportType = true;
                fv.SupportPriority = true;
                fv.SupportStatus = true;
            }
            else
            {
                fv.DepartmentType = false;
                fv.OrganizationList = false;
                fv.DepartmentList = false;
                fv.SupportType = true;
                fv.SupportPriority = true;
                fv.SupportStatus = true;
            }
            return fv;
        }

        #endregion


        #region  Support Relosve filteration
        public IEnumerable<NameIdViewModel> GetAllType()
        {
            List<NameIdViewModel> list = new List<NameIdViewModel>();
            NameIdViewModel o = new NameIdViewModel();
            o.TypeId = "GroupType";
            o.Name = "Group";

            NameIdViewModel tw = new NameIdViewModel();
            tw.TypeId = "OrganizationType";
            tw.Name = "Organization";

            NameIdViewModel th = new NameIdViewModel();
            th.TypeId = "All";
            th.Name = "All";


            list.Add(th);
            list.Add(o);
            list.Add(tw);
            IEnumerable<NameIdViewModel> res = list.ToList();

            return res;
        }
        public IEnumerable<NameIdViewModel> GetAllTypeListAdmin()
        {
            List<NameIdViewModel> list = new List<NameIdViewModel>();
            NameIdViewModel o = new NameIdViewModel();
            o.TypeId = "NoType";
            o.Name = "NoType";
            list.Add(o);
            IEnumerable<NameIdViewModel> res = list.ToList();
            return res;
        }


        public IEnumerable<SupportCommonModel> GetAlldepartmentlist(List<long> organizationid)
        {
            var allOrgDeptIds = departmentRepository.
                                 GetDepartmentListByOrganizationIdIn(organizationid).Select(a => new SupportCommonModel
                                 {
                                     ID = a.ID,
                                     Name = a.Name
                                 }).ToList();

            return allOrgDeptIds;
        }
        public IEnumerable<SupportCommonModel> GetAllOrganisationByTYpe(string Type)
        {

            if (Type == "OrganizationType")
            {

                var supportStatusList = organizationRepository.GetAllOrganization().Select(a => new SupportCommonModel
                {
                    ID = a.ID,
                    Name = a.Name
                }).ToList();
                return supportStatusList;

            }


            else
            {
                return null;
            }

        }


        public IEnumerable<SupportCommonModel> GetAllDepartmentById(string Type, long[] Organizationid)
        {
            if (Type == "GroupType")
            {
                IEnumerable<SupportCommonModel> depts = (from a in departmentRepository.GetDepartmentListByGroupId(1).ToList()
                                                         join b in supportRepository.GetAllDepartmentSupport()
                                                         on a.ID equals b.DepartmentID
                                                         select new SupportCommonModel
                                                         {
                                                             ID = a.ID,
                                                             Name = a.ParentID != null && a.ParentID > 0 ?
                                                           departmentRepository.GetDepartmentById(a.ParentID.Value).Name + ViewConstants.ARROW + a.Name : a.Name,
                                                         }).ToList();
                return depts;
            }
            else
            if (Type == "OrganizationType")
            {

                IEnumerable<SupportCommonModel> depts = (from a in departmentRepository.GetDepartmentListByOrganizationIdIn(Organizationid.ToList()).ToList()
                                                         join b in supportRepository.GetAllDepartmentSupport()
                                                         on a.ID equals b.DepartmentID
                                                         select new SupportCommonModel
                                                         {
                                                             ID = a.ID,
                                                             Name = a.ParentID != null && a.ParentID > 0 ?
                                                             departmentRepository.GetDepartmentById(a.ParentID.Value).Name + ViewConstants.ARROW : a.Name,
                                                         }).ToList();
                return depts;

            }
            else
                return null;


        }

        public IEnumerable<SupportCommonModel> GetAllSupportTypeByDepartmentId(long[] departmenttid)
        {
            var allOrgDeptIds = supportRepository.GetAllSupportTypeById(
                supportRepository.GetSupportTypeIdsByDepartmentIdIn(departmenttid.ToList()).ToList()).ToList().Select(a => new SupportCommonModel
                {
                    ID = a.ID,
                    Name = a.Name
                }).ToList();

            return allOrgDeptIds;
        }

        #endregion




        #region resolve Details and SupportManagementApproval

        public FilterVisibility SupportManagementApprovalVisibility(long accessId, long roleId)
        {
            FilterVisibility fv = new FilterVisibility();
            if (commonMethods.checkaccessavailable("Management Approval", accessId, "Support Type", "Support", roleId) == true)
            {
                fv.DepartmentType = true;
                fv.OrganizationList = true;
                fv.DepartmentList = true;
                fv.SupportType = true;
                fv.SupportPriority = true;
                fv.SupportStatus = true;
            }
            else
            {
                fv.DepartmentType = false;
                fv.OrganizationList = false;
                fv.DepartmentList = false;
                fv.SupportType = true;
                fv.SupportPriority = true;
                fv.SupportStatus = true;
            }
            return fv;
        }


        public SupportModelLists SupportManagementApproval(long userId, long roleId, long accessId, string Type, long[] OrgId, long[] DeptId, long[] SupportTypeId, long[] StatusId, long[] PriorityId, string ManagementStatus, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate,long? deptemp)
        {

            SupportModelLists model = new SupportModelLists();
            //if (commonMethods.checkaccessavailable("Management Approval", accessId, "List", "Support", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            model.FilterVisibility = SupportManagementApprovalVisibility(accessId, roleId);
            List<long> pIds = new List<long>();
            List<long> statusIds = new List<long>();
            List<long> sTypeIds = new List<long>();
            List<long> deptIds = new List<long>();
            List<long> orgIds = new List<long>();


            model.Type = Type;
            model.OrgId = OrgId;
            model.DeptId = DeptId;
            model.SupportTypeId = SupportTypeId;
            model.StatusId = StatusId;
            model.PriorityId = PriorityId;

            if (PriorityId != null && PriorityId.Count() > 0)
            {
                pIds.AddRange(PriorityId);
            }

            if (OrgId != null && OrgId.Count() > 0)
            {
                orgIds.AddRange(OrgId);
            }

            if (DeptId != null && DeptId.Count() > 0)
            {
                deptIds.AddRange(DeptId);
            }

            if (SupportTypeId != null && SupportTypeId.Count() > 0)
            {
                sTypeIds.AddRange(SupportTypeId);

            }

            if (StatusId != null && StatusId.Count() > 0)
            {
                statusIds.AddRange(StatusId);
            }

            List<NameIdViewModel> list = new List<NameIdViewModel>();

            List<SupportRequest> request = new List<SupportRequest>();

            //if (commonMethods.checkaccessavailable("Management Approval", accessId, "Support Type", "Support", roleId) == false)
            //{

            if (Type == null || Type == "All" || Type == "")
            {
                request.AddRange(supportRepository.GetApprovalSupportRequestBySupportTypeId(null).ToList());
            }
            else
            if (Type == "GroupType")
            {
                if (deptIds.Count() > 0)
                {
                    if (sTypeIds.Count() > 0)
                    {
                        request.AddRange(supportRepository.GetApprovalSupportRequestBySupportTypeId(sTypeIds).ToList());
                    }
                    else
                    {
                        var supportTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds);

                        if (supportTypeIds != null && supportTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.
                                GetApprovalSupportRequestBySupportTypeId(supportTypeIds.ToList()).ToList());
                        }

                    }
                }
                else
                    request.AddRange(supportRepository.GetApprovalSupportRequestBySupportTypeId(null).ToList());
            }
            else
            if (Type == "OrganizationType")
            {

                if (orgIds.Count() > 0)
                {
                    if (deptIds.Count() > 0)
                    {
                        if (sTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.
                                GetApprovalSupportRequestBySupportTypeId(sTypeIds).ToList());
                        }
                        else
                        {
                            var supportTypeIds = supportRepository.
                                GetSupportTypeIdsByDepartmentIdIn(deptIds);

                            if (supportTypeIds != null && supportTypeIds.Count() > 0)
                            {
                                request.AddRange(supportRepository.
                                    GetApprovalSupportRequestBySupportTypeId(supportTypeIds.ToList())
                                    .ToList());
                            }

                        }
                    }
                    else
                    {

                        var allOrgDeptIds = departmentRepository.
                            GetDepartmentListByOrganizationIdIn(orgIds).Select(a => a.ID);
                        if (allOrgDeptIds != null && allOrgDeptIds.Count() > 0)
                        {
                            var supportTypeIds = supportRepository.
                                GetSupportTypeIdsByDepartmentIdIn(allOrgDeptIds.Distinct().
                                ToList());
                            if (supportTypeIds != null && supportTypeIds.Count() > 0)
                            {
                                request.AddRange(supportRepository.
                                    GetApprovalSupportRequestBySupportTypeId(supportTypeIds.ToList())
                                    .ToList());

                            }

                        }



                    }

                }
                else
                {
                    request.AddRange(supportRepository.GetApprovalSupportRequestBySupportTypeId(null).ToList());

                    //var allOrgDeptIds = departmentRepository.GetAllDepartment().Where(a => a.GroupID == null || a.GroupID == 0).Select(a => a.ID);
                    //if (allOrgDeptIds != null && allOrgDeptIds.Count() > 0)
                    //{
                    //    var supportTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(allOrgDeptIds.Distinct().ToList());
                    //    if (supportTypeIds != null && supportTypeIds.Count() > 0)
                    //    {
                    //        request.AddRange(supportRepository.GetApprovalSupportRequestBySupportTypeId(supportTypeIds.ToList()).ToList());

                    //    }

                    //}
                }

            }

            //}
            //else
            //{

            //    NameIdViewModel th = new NameIdViewModel();
            //    th.Name = "NoType";
            //    th.Status = "NoType";
            //    list.Add(th);
            //    model.Types = list;
            //}


            if (request.Count() > 0)
            {
                List<SupportRequest> filterList = new List<SupportRequest>();

                if (statusIds.Count() > 0 && pIds.Count() > 0)
                {
                    filterList.AddRange(request.Where(a => statusIds.Contains(a.StatusID) &&
                    pIds.Contains(a.PriorityID)));
                }
                else
                if (pIds.Count() > 0)
                {
                    filterList.AddRange(request.Where(a =>
                   pIds.Contains(a.PriorityID)));
                }
                else
                if (statusIds.Count() > 0)
                {
                    filterList.AddRange(request.Where(a =>
                   statusIds.Contains(a.StatusID)));
                }
                else
                    filterList.AddRange(request);

                if (filterList.Count() > 0)
                {


                    var cat = supportRepository.GetAllCategory().ToList();
                    var subcat = supportRepository.GetAllSubCategory().ToList();
                    var loc = supportRepository.GetAllLocation().ToList();
                    var build = supportRepository.GetAllBuilding();
                    var room = supportRepository.GetAllRoom();
                    var prior = supportRepository.GetAllPriority().ToList();
                    var supptype = supportRepository.GetAllSupportType().ToList();
                    var assignTo = _dbContext.SupportRequestAssign.ToList();


                    List<SupportRequestResponseModel> res = (from a in filterList
                                                             join b in cat on a.CategoryID equals b.ID
                                                             join c in subcat on a.SubCategoryID equals c.ID
                                                             join g in prior on a.PriorityID equals g.ID
                                                             //join i in emp on a.InsertedId equals i.ID
                                                             join m in supptype on a.SupportTypeID equals m.ID
                                                             //join n in det on m.DepartmentID equals n.ID


                                                             select new SupportRequestResponseModel
                                                             {
                                                                 ID = a.ID,
                                                                 Title = a.Title != null ? a.Title : "NA",
                                                                 CategoryID = a.CategoryID,
                                                                 CategoryName = b.Name,
                                                                 SubCategoryID = a.SubCategoryID,
                                                                 SubCategoryName = c.Name,
                                                                 BuildingID = a.BuildingID,
                                                                 RoomID = a.RoomID,
                                                                 EmpId = a.InsertedId,
                                                                 TicketCode = a.TicketCode,
                                                                 ApproveDate = NullableDateTimeToStringDate(a.ApproveDate),
                                                                 RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                                 BuildingName = a.BuildingID != 0 ? (build.Where(y => y.ID == a.BuildingID).FirstOrDefault() == null ? ViewConstants.NOT_APPLICABLE : build.Where(y => y.ID == a.BuildingID).FirstOrDefault().Name) :
                                                                 ViewConstants.NOT_APPLICABLE,
                                                                 RoomName = a.RoomID != 0 ? (room.Where(z => z.ID == a.RoomID).FirstOrDefault() == null ? ViewConstants.NOT_APPLICABLE : room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name) : ViewConstants.NOT_APPLICABLE,
                                                                 PriorityName = prior != null ? prior.Where(x => x.ID == a.PriorityID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE,
                                                                 StatusID = GetSupportStatusId(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                 Status = GetSupportStatus(a.StatusName, a.SendApproval, a.ManagementReject, a.ManagementApproval),
                                                                 OrgGroupName = commonMethods.GetOrgGroupName(a.InsertedId),
                                                                 //t_Date = a.DueDate != null ? a.DueDate : (a.RequestedCompletionDate == null ? a.InsertedDate : a.RequestedCompletionDate),
                                                                 PriorityID = a.PriorityID,
                                                                 CompletionDate = a.CompletionDate,
                                                                 CompletionDateString = NullableDateTimeToStringDate(a.CompletionDate),
                                                                 EmpFullName = commonMethods.GetEmployeeNameByEmployeeId(a.InsertedId, userId),
                                                                 //emp_Phone = i.PrimaryMobile != null ? i.PrimaryMobile : "",
                                                                 Description = a.Description != null ? a.Description : "NA",
                                                                 SupportTypeID = a.SupportTypeID,
                                                                 SupportTypeName = m.Name,
                                                                 Comment = a.Comment,
                                                                 commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, userId),
                                                                 InsertedDate = a.InsertedDate,
                                                                 ModifiedDate = a.ModifiedDate,
                                                                 InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                                 ModifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                                                                 SentApproval = a.SendApproval,
                                                                 IsRead = a.IsRead == null ? false : a.IsRead,
                                                                 IsManagementRead = a.IsManagementRead == null ? false : a.IsManagementRead,
                                                                 DueDate = NullableDateTimeToStringDate(a.DueDate),
                                                                 AssignedToEmp = assignTo != null ? assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                        employeeRepository.GetEmployeeFullNameById(assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId) : null : null,
                                                                 AssignedDate = assignTo != null ? assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                      assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().InsertedDate.ToString() : "NA" : "NA",
                                                                 AssignToId = assignTo != null ? assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                          assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId : 0 : 0,

                                                                 SendManagementApprovalDate = NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                                                 SentManagementName = a.SentApprovalID != null && a.SentApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                 ManagementApproved = a.ManagementApproval,
                                                                 ManagementApprovedDate = NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                                                 ManagementApprovedName = a.ManagementApprovalID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, a.InsertedId) : ViewConstants.NOT_APPLICABLE,
                                                                 ManagementRejected = a.ManagementReject,
                                                                 ManagementRejectedDate = NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                                                 ManagementRejectedName = a.ManagementRejectedID > 0 ? commonMethods.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, a.InsertedId) : ViewConstants.NOT_APPLICABLE,

                                                             }

                             ).ToList();

                    string finalTime = "23:59:59";
                    Nullable<DateTime> tt = DateTime.Parse(finalTime);
                    Nullable<DateTime> toDate = ToDate + tt.Value.TimeOfDay;

                    if (FromDate != null && FromDate.ToString() != "")
                    {
                        if (FromDate != null && FromDate.ToString() != "" && ToDate != null && ToDate.ToString() != "")
                        {
                            res = res.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= toDate).ToList();
                        }
                        else
                        {
                            res = res.Where(a => a.InsertedDate >= FromDate).ToList();
                        }
                    }
                    if (ManagementStatus == "Pending")
                    {
                        res = res.Where(m => m.SentApproval == true && m.ManagementApproved == false && m.ManagementRejected == false).ToList();
                    }
                    if (ManagementStatus == "Approved")
                    {
                        res = res.Where(m => m.SentApproval == true && m.ManagementApproved == true && m.ManagementRejected == false).ToList();
                    }
                    if (ManagementStatus == "Rejected")
                    {
                        res = res.Where(m => m.SentApproval == true && m.ManagementApproved == false && m.ManagementRejected == true).ToList();
                    }
                    if (Type == "OrganizationType" && orgIds.Count() == 0)
                    {
                        res = res.Where(s => s.OrgGroupName != "ODM Educational Group").ToList();
                    }
                    if (Type == "GroupType" && deptIds.Count() == 0)
                    {
                        res = res.Where(s => s.OrgGroupName == "ODM Educational Group").ToList();
                    }
                    if (Type == "GroupType" && deptIds.Count() > 0)
                    {
                        res = res.Where(s => s.OrgGroupName == "ODM Educational Group").ToList();
                    }
                    if(deptemp != null)
                    {
                        res = res.Where(s => s.AssignToId == deptemp).ToList();
                    }
                    model.SupportRequestcls = res;


                }
            }

            return model;
        }
        #endregion

        #region support Report chart

        public ReportFilterVisibility PrepareReportDataVisibility(long accessId, long roleId)
        {
            ReportFilterVisibility fv = new ReportFilterVisibility();
            if (commonMethods.checkaccessavailable("Report", accessId, "Support Type", "Support", roleId) == true)
            {
                fv.DepartmentType = true;
                fv.OrganizationList = true;
                fv.DepartmentList = true;
                fv.SupportType = true;
            }
            else
            {
                fv.DepartmentType = false;
                fv.OrganizationList = false;
                fv.DepartmentList = false;
                fv.SupportType = false;

            }
            return fv;
        }

        public ApexChartOptions PrepareReportData(SupportReportChartViewModel model)
        {

            ApexChartOptions chartOptions = new ApexChartOptions();
            chartOptions.reportFilterVisibility = PrepareReportDataVisibility(model.accessId, model.roleId);
            List<SupportType> types = new List<SupportType>();
            List<SupportRequest> requests = new List<SupportRequest>();

            if (model.Type == null || model.Type == "NoType")
            {
                if (model.SupportTypeIds == null || model.SupportTypeIds.Count() == 0)
                {
                    var allempdept = commonMethods.GetDepartmentsByEmployeeID(model.userId);
                    var supportIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(allempdept.Distinct().Select(a => a.id).ToList());

                    types.AddRange(supportRepository.GetAllSupportType().Where(a => supportIds.Contains(a.ID)).ToList());
                    requests.AddRange(supportRepository.GetAllBySupportTypeId(types.Select(a => a.ID).ToList()).ToList());
                }
                else
                {
                    requests = supportRepository.
                    GetAllBySupportTypeId(model.SupportTypeIds).ToList();
                }

            }
            else
            if (model.Type == "All")
            {

                types.AddRange(supportRepository.GetAllSupportType().ToList());
                requests.AddRange(supportRepository.GetAllBySupportTypeId(types.Select(a => a.ID).ToList()).ToList());
            }
            else
                  if (model.Type == "GroupType")
            {
                if (model.DeptIds != null && model.DeptIds.Count() > 0)
                {
                    if (model.SupportTypeIds != null && model.SupportTypeIds.Count() > 0)
                    {
                        requests = supportRepository.
                        GetAllBySupportTypeId(model.SupportTypeIds).ToList();
                    }
                    else
                    {

                        var sTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(model.DeptIds).ToList();
                        if (sTypeIds != null && sTypeIds.Count() > 0)
                        {
                            requests = supportRepository.
                      GetAllBySupportTypeId(sTypeIds)
                      .ToList();
                        }



                    }

                }
                else
                {

                    var deptIds = departmentRepository.GetDepartmentListByGroupIdIn(model.GrpIds);


                    if (deptIds != null && deptIds.Count() > 0)
                    {
                        var sTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds.Select(a => a.ID).ToList()).ToList();

                        if (sTypeIds != null && sTypeIds.Count() > 0)
                        {
                            requests = supportRepository.
                  GetAllBySupportTypeId(sTypeIds)
                  .ToList();

                        }
                    }


                }
            }
            else
                  if (model.Type == "OrganizationType")
            {

                if (model.OrgIds != null && model.OrgIds.Count() > 0)
                {


                    if (model.DeptIds != null && model.DeptIds.Count() > 0)
                    {
                        if (model.SupportTypeIds != null && model.SupportTypeIds.Count() > 0)
                        {
                            requests = supportRepository.
                            GetAllBySupportTypeId(model.SupportTypeIds).ToList();
                        }
                        else
                        {
                            requests = supportRepository.
                            GetAllBySupportTypeId(supportRepository.GetAllSupportTypeById(
                             supportRepository.GetSupportTypeIdsByDepartmentIdIn(model.DeptIds).ToList()).Select(a => a.ID).ToList())
                            .ToList();


                        }

                    }
                    else
                    {

                        var deptIds = departmentRepository.GetDepartmentListByOrganizationIdIn(model.OrgIds);

                        requests = supportRepository.
                            GetAllBySupportTypeId(supportRepository.GetAllSupportTypeById(
                             supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds.Select(a => a.ID).Distinct().ToList())
                             .ToList()).Select(a => a.ID).ToList())
                            .ToList();

                    }

                }
                else
                {

                    var orgs = organizationRepository.GetAllOrganizationByGroupId(1);


                    if (orgs != null && orgs.Count() > 0)
                    {


                        var deptIds = departmentRepository.GetDepartmentListByOrganizationIdIn(orgs.Select(a => a.ID).ToList());


                        if (deptIds != null && deptIds.Count() > 0)
                        {
                            var sTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds.Select(a => a.ID).ToList()).ToList();

                            if (sTypeIds != null && sTypeIds.Count() > 0)
                            {
                                requests = supportRepository.
                      GetAllBySupportTypeId(sTypeIds)
                      .ToList();

                            }
                        }
                    }
                    //requests = supportRepository.
                    //    GetAllBySupportTypeId(supportRepository.GetAllSupportTypeById(
                    //     supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds.Select(a => a.ID).Distinct().ToList())
                    //     .ToList()).Select(a => a.ID).ToList())
                    //    .ToList();
                }

            }

            //if (model.SupportTypeIds == null)
            //{


            //    types.AddRange(supportRepository.GetAllSupportType().ToList());
            //    requests.AddRange(supportRepository.GetAllBySupportTypeId(types.Select(a => a.ID).ToList()).ToList());
            //}
            //else
            //{
            //    requests = supportRepository.
            //    GetAllBySupportTypeId(model.SupportTypeIds).ToList();
            //}






            if (requests != null)
            {
                SupportReportCountViewModel vm = new SupportReportCountViewModel();
                string finalTime = "23:59:59";
                Nullable<DateTime> tt = DateTime.Parse(finalTime);
                Nullable<DateTime> toDate = model.ToDate + tt.Value.TimeOfDay;

                if (model.FromDate != null && model.ToDate != null)
                {
                    requests = requests.Where(a => a.ModifiedDate >= model.FromDate && a.ModifiedDate <= toDate).ToList();
                }

                var requestAssign = _dbContext.SupportRequestAssign.Where(a => a.Active == true).ToList();
                var requestas = (from a in requests
                                 select new SupportRequestNew
                                 {
                                     StatusID = a.StatusID,
                                     ManagementReject = a.ManagementReject,
                                     SendApproval = a.SendApproval,
                                     SupportRequestId = a.ID,
                                     EmployeeId = requestAssign != null ? requestAssign.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ? requestAssign.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId : 0 : 0
                                 }).ToList();

                if (model.DeptEmp != null)
                {
                    requestas = requestas.Where(a => a.EmployeeId == model.DeptEmp).ToList();
                }


                vm.PendingCount = requestas.Where(a => a.StatusID == 1 && a.ManagementReject == false)
                    .ToList().
                    Count();

                int ppc = requestas.Where(a => a.StatusID == 1 && a.SendApproval == true &&
                  a.ManagementApproval == false && a.ManagementReject == false)
                    .ToList().
                    Count();

                vm.ProgressCount = requestas.Where(a => a.StatusID == 3 && a.ManagementReject == false
                ).ToList().Count();

                int pprc = requestas.Where(a => a.StatusID == 3 && a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false)
                .ToList().
                Count();

                vm.CompletedCount = requestas.Where(a => a.StatusID == 2 && a.ManagementReject == false
                ).ToList().Count();

                int cc = requestas.Where(a => a.StatusID == 2 && a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false)
               .ToList().
               Count();

                vm.OnHoldCount = requestas.Where(a => a.StatusID == 4 && a.ManagementReject == false
               ).ToList().Count();

                int ohc = requestas.Where(a => a.StatusID == 4 && a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false)
            .ToList().
            Count();

                vm.RejectedCount = requestas.Where(a => a.StatusID == 5 && a.ManagementReject == false
               ).ToList().Count();

                int rjc = requestas.Where(a => a.StatusID == 5 && a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false)
    .ToList().
    Count();

                vm.VerifiedCount = requestas.Where(a => a.StatusID == 6 && a.ManagementReject == false
               ).ToList().Count();

                int vrc = requestas.Where(a => a.StatusID == 6 && a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false)
.ToList().
Count();

                vm.VerifiedCount = vm.VerifiedCount > vrc ? vm.VerifiedCount - vrc : vrc - vm.VerifiedCount;
                vm.RejectedCount = vm.RejectedCount > rjc ? vm.RejectedCount - rjc : rjc - vm.RejectedCount;
                vm.OnHoldCount = vm.OnHoldCount > ohc ? vm.OnHoldCount - ohc : ohc - vm.OnHoldCount;
                vm.CompletedCount = vm.CompletedCount > cc ? vm.CompletedCount - cc : cc - vm.CompletedCount;
                vm.ProgressCount = vm.ProgressCount > pprc ? vm.ProgressCount - pprc : pprc - vm.ProgressCount;
                vm.PendingCount = vm.PendingCount > ppc ? vm.PendingCount - ppc : ppc - vm.PendingCount;


                vm.MangementApprovedCount = requestas.Where(a => a.ManagementApproval == true).ToList().Count();
                vm.MangementRejectedCount = requestas.Where(a => a.ManagementReject == true).ToList().Count();
                vm.MangementPendingCount = requestas.Where(a => a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false).ToList().Count();

                vm.TotalSupportCount = vm.PendingCount + vm.ProgressCount + vm.CompletedCount + vm.OnHoldCount +
                    vm.RejectedCount + vm.VerifiedCount + vm.MangementApprovedCount + vm.MangementRejectedCount
                    + vm.TotalMangementCount;

                chartOptions.series = new Int64[8];

                chartOptions.series[0] = vm.PendingCount;
                chartOptions.series[1] = vm.ProgressCount;
                chartOptions.series[2] = vm.CompletedCount;
                chartOptions.series[3] = vm.OnHoldCount;
                chartOptions.series[4] = vm.RejectedCount;
                chartOptions.series[5] = vm.VerifiedCount;
                chartOptions.series[6] = vm.MangementPendingCount;
                chartOptions.series[7] = vm.MangementRejectedCount;

                chartOptions.labels = new string[8];


                chartOptions.labels[0] = "PENDING";
                chartOptions.labels[1] = "PROGRESS";
                chartOptions.labels[2] = "COMPLETED";
                chartOptions.labels[3] = "ON-HOLD";
                chartOptions.labels[4] = "REJECTED";
                chartOptions.labels[5] = "VERIFIED";
                chartOptions.labels[6] = "APPROVAL PENDING";
                chartOptions.labels[7] = "MANAGEMENT REJECTED";
                chartOptions.TotalReportCount = requests.Count();


                return chartOptions;
            }
            else
            {
                return null;
            }
        }
        #endregion

        public IEnumerable<SupportCategoryModel> SupportTypeByEmployeeId(long empid)
        {
            try
            {
                var emp = employeeRepository.GetEmployeeById(empid);
                var empdes = employeeDesignationRepository.GetAllEmployeeDesignationsByEmployeeId(emp.ID);
                var designation = designationRepository.GetAllDesignation();
                var deptsupport = supportRepository.GetAllDepartmentSupport();
                var supporttype = supportRepository.GetAllSupportType();

                var result = (from a in empdes
                              join b in designation on a.DesignationID equals b.ID
                              join c in deptsupport on b.DepartmentID equals c.DepartmentID
                              join d in supporttype on c.SupportTypeID equals d.ID
                              select new SupportCategoryModel
                              {
                                  ID = d.ID,
                                  Name = d.Name
                              }).ToList();
                return result;
            }
            catch
            {
                return null;
            }
        }

        public List<StatusModel> ManagementStatus()
        {
            try
            {
                var status = new List<StatusModel> {
                                    new StatusModel { ID = "Pending", Name = "Pending" },
                                    new StatusModel { ID = "Approved", Name = "Approved" },
                                    new StatusModel { ID = "Rejected", Name = "Rejected" }
                                }.ToList();
                return status;
            }
            catch
            {
                return null;
            }
        }        
        public List<EmpClassModal> GetallEmployeeByDeptId(long empid)
        { 
            var deptId = commonMethods.GetDepartmentsByEmployeeID(empid).Select(a => a.id);
            var empList = commonMethods.GetAllDeptwithEmployee().Where(a => deptId.Contains(a.DepartmentID)).ToList();
            //var emp = employeeRepository.get.GetAllEmployee();
            var res = (from a in empList
                       select new EmpClassModal
                       {
                           ID = a.EmployeeID,
                           EmpName = employeeRepository.GetEmployeeFullNameById(a.EmployeeID),
                           DeptId = a.DepartmentID,
                           DeptName = a.DepartmentName
                       }).ToList();

            return res;
        }
        public bool AssignRequestToEmployee(AssignRequestToEmployee assignRequest)
        {
            var req = _dbContext.SupportRequestAssign.Where(a => a.SupportRequestId == assignRequest.supportid).FirstOrDefault(); 

            if (req == null)
            {

                SupportRequestAssign requestAssign = new SupportRequestAssign();
                requestAssign.InsertedDate = DateTime.Now;
                requestAssign.InsertedId = assignRequest.userid;
                requestAssign.ModifiedDate = DateTime.Now;
                requestAssign.ModifiedId = assignRequest.userid;
                requestAssign.Active = true;
                requestAssign.Status = "ACTIVE";
                requestAssign.IsAvailable = true;
                requestAssign.SupportRequestId = assignRequest.supportid;
                requestAssign.EmployeeId = assignRequest.assignemp;

                _dbContext.SupportRequestAssign.Add(requestAssign);
                _dbContext.SaveChanges();

                if (requestAssign.ID > 0)
                {
                    SupportTimeLine timeLine = new SupportTimeLine();
                    timeLine.InsertedDate = DateTime.Now;
                    timeLine.InsertedId = assignRequest.userid;
                    timeLine.ModifiedDate = DateTime.Now;
                    timeLine.ModifiedId = assignRequest.userid;
                    timeLine.Active = true;
                    timeLine.Status = "REQUEST IS ASSIGNED";
                    timeLine.SupportRequestID = assignRequest.supportid;
                    timeLine.ReassignedToId = assignRequest.assignemp;

                    _dbContext.SupportTimeLines.Add(timeLine);
                    _dbContext.SaveChanges();
                }
            }
            else
            {
                req.ModifiedDate = DateTime.Now;
                req.ModifiedId = assignRequest.userid;
                req.Active = true;
                req.Status = "UPDATED";
                req.IsAvailable = true;
                req.SupportRequestId = assignRequest.supportid;
                req.EmployeeId = assignRequest.assignemp;

                _dbContext.Update(req);
                _dbContext.SaveChanges();

                if (req.ID > 0)
                {
                    SupportTimeLine timeLine = new SupportTimeLine();
                    timeLine.InsertedDate = DateTime.Now;
                    timeLine.InsertedId = assignRequest.userid;
                    timeLine.ModifiedDate = DateTime.Now;
                    timeLine.ModifiedId = assignRequest.userid;
                    timeLine.Active = true;
                    timeLine.Status = "REQUEST IS REASSIGNED";
                    timeLine.SupportRequestID = assignRequest.supportid;
                    timeLine.ReassignedToId = assignRequest.assignemp;

                    _dbContext.SupportTimeLines.Add(timeLine);
                    _dbContext.SaveChanges();
                }
            }

            return true;
        }
    }

}
