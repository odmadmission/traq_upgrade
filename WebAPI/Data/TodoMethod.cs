﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.Entities.TodoAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.RepositoryImpl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Controllers;
using static WebAPI.Models.SupportModel; 

using static WebAPI.Models.TodoModel;


namespace WebAPI.Data
{
    public class TodoMethod : AppController
    {
        protected readonly IToDoRepository toDoRepository;
        protected readonly CommonMethods commonMethods;
        //protected readonly CommonTaskMethods commonTaskMethods;
        protected readonly IEmployeeRepository empRepository;
        protected readonly IDepartmentRepository departmentRepository;
        protected readonly IToDoRepository todoRepository;
        protected readonly IBloodGroupRepository bloodGroupRepository;
        protected readonly INationalityTypeRepository nationalityTypeRepository;
        protected readonly IReligionTypeRepository religionTypeRepository;
        protected readonly IEmployeeRepository employeeRepository;
        private readonly IHostingEnvironment hostingEnvironment;


        public TodoMethod(IToDoRepository toDoRepository, CommonMethods commonMethods, INationalityTypeRepository nationalityTypeRepository,
            /*CommonTaskMethods commonTaskMethods,*/ IEmployeeRepository empRepository, IBloodGroupRepository bloodGroupRepository,
            IDepartmentRepository departmentRepository, IToDoRepository todoRepository, IReligionTypeRepository religionTypeRepository,
            IEmployeeRepository employeeRepository, IHostingEnvironment env)
        {
            this.toDoRepository = toDoRepository;
            this.commonMethods = commonMethods;
            //this.commonTaskMethods = commonTaskMethods;
            this.empRepository = empRepository;
            this.departmentRepository = departmentRepository;
            this.todoRepository = todoRepository;
            this.bloodGroupRepository = bloodGroupRepository;
            this.nationalityTypeRepository = nationalityTypeRepository;
            this.religionTypeRepository = religionTypeRepository;
            this.employeeRepository = employeeRepository;
            hostingEnvironment = env;

        }
        public IEnumerable<TodoCommonModel> GetAllTodoStatus()
        {
            var supportStatusList = toDoRepository.GetAllTodoStatus().Select(a => new TodoCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportStatusList;
        }
        public IEnumerable<TodoCommonModel> GetAllTodoPriorities()
        {
            var supportStatusList = toDoRepository.GetAllTodoPriority().Select(a => new TodoCommonModel
            {
                ID = a.ID,
                Name = a.Name
            }).ToList();

            return supportStatusList;
        }



        public SubTaskList_clsModel GetAllSubtaskBySubTaskId(long subTaskId)
        {

            var employees = empRepository.GetAllEmployeewithleft();
            var todo = todoRepository.GetAllTodo().Where(a => a.ID == subTaskId).ToList();
            var todostatus = todoRepository.GetAllTodoStatus().ToList();
            var priority = todoRepository.GetAllTodoPriority();
            var departments = commonMethods.GetDepartments();
            var comments = todoRepository.GetAllTodoComment();

             SubTaskList_clsModel result = (from a in todo


                                                        select new SubTaskList_clsModel
                                                        {
                                                            todoid = a.ID,
                                                            todoname = a.Name,
                                                            status = a.Status,
                                                            AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                                                            tododescription = a.Description,
                                                            DueDate = a.DueDate != null ? a.DueDate.Value.ToString("dd-MMM-yyyy") : "",
                                                            AssignByDepartmentID = a.AssignByDepartmentID,

                                                            AssignByDepartmentName = (a.AssignByDepartmentID == 0) ? "NA" : departments.Where(m => m.ID == a.AssignByDepartmentID).FirstOrDefault().Name,

                                                            DepartmentID = a.DepartmentID,
                                                            assignedto = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).Select(p => p.FirstName + " " + p.LastName).FirstOrDefault() : "",
                                                            assignedby = a.EmployeeID == 0 ? "" : employees.Where(m => m.ID == a.EmployeeID).Select(p => p.FirstName + " " + p.LastName).FirstOrDefault(),
                                                            profile = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).FirstOrDefault().Image : "",
                                                            priorityName = a.TodoPriorityID == 0 ? "" : priority.Where(m => m.ID == a.TodoPriorityID).FirstOrDefault().Name,
                                                            department = (a.DepartmentID == 0) ? "" : departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name,
                                                            assigntime = a.AssignedDate != null ? a.AssignedDate.Value.ToString("hh:mm tt") : "NA",
                                                            AssignByID = a.EmployeeID,
                                                            AssignDate = a.AssignedDate,
                                                            AssignDateTime = NullableDateTimeToStringDateTime(a.AssignedDate),
                                                            ModifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                                                            InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                            AssignToID = a.AssignedId,
                                                            ModifiedDate = a.ModifiedDate,
                                                            parentID = a.ParentId,
                                                            TodoPriorityID = a.TodoPriorityID,
                                                            completionDt = a.CompletionDate != null ? a.CompletionDate.Value.ToString("dd-MMM-yyyy") : "",
                                                            VerifyDt = a.VerifiedDate != null ? a.VerifiedDate.Value.ToString("dd-MMM-yyyy") : "",
                                                            commentcount = comments != null ? comments.Where(m => m.TodoID == a.ID /*&& m.InsertedId != employeeid*/ && m.IsRead == false).ToList().Count() : 0

                                                        }).ToList().FirstOrDefault();

            return result;

        }



        public IEnumerable<SubTaskList_clsModel> GetAllTodoSubtaskByTodoTask(long todoid)
        {
          
            var employees = empRepository.GetAllEmployeewithleft();
            var todo = todoRepository.GetAllTodo().Where(a => a.ParentId == todoid).ToList();
            var todostatus = todoRepository.GetAllTodoStatus().ToList();
            var priority = todoRepository.GetAllTodoPriority();
            var departments = commonMethods.GetDepartments();
            var comments = todoRepository.GetAllTodoComment();

           
            IEnumerable<SubTaskList_clsModel> result = (from a in todo

                                                        select new SubTaskList_clsModel
                                                 {
                                                     todoid = a.ID,
                                                            
                                                     todoname = a.Name,                                                  
                                                     status = a.Status,
                                                     AssignedDate = NullableDateTimeToStringDate(a.AssignedDate),
                                                     tododescription = a.Description,
                                                     DueDate = a.DueDate != null ? NullableDateTimeToStringDate(a.DueDate) : "",
                                                     AssignByDepartmentID = a.AssignByDepartmentID,

                                                     AssignByDepartmentName = (a.AssignByDepartmentID == 0) ? "NA" : departments.Where(m => m.ID == a.AssignByDepartmentID).FirstOrDefault().Name,
                                                    
                                                     DepartmentID =a.DepartmentID,     
                                                     assignedto = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).Select(p => p.FirstName + " " + p.LastName).FirstOrDefault() : "",
                                                     assignedby = a.EmployeeID == 0 ? "" : employees.Where(m => m.ID == a.EmployeeID).Select(p => p.FirstName + " " + p.LastName).FirstOrDefault(),
                                                     profile = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).FirstOrDefault().Image : "",
                                                     priorityName = a.TodoPriorityID == 0 ? "" : priority.Where(m => m.ID == a.TodoPriorityID).FirstOrDefault().Name,
                                                     department = (a.DepartmentID == 0) ? "" : departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name,
                                                            assigntime = a.AssignedDate != null ? a.AssignedDate.Value.ToString("hh:mm tt") : "NA",
                                                            AssignByID = a.EmployeeID,
                                                     AssignDate = a.AssignedDate,
                                                     AssignDateTime=NullableDateTimeToStringDateTime(a.AssignedDate),
                                                     ModifiedDateTime= NullableDateTimeToStringDateTime(a.ModifiedDate),
                                                     InsertedDateTime= NullableDateTimeToStringDateTime(a.InsertedDate),
                                                     AssignToID = a.AssignedId,
                                                     ModifiedDate = a.ModifiedDate,
                                                     parentID = a.ParentId,                                                     
                                                     TodoPriorityID = a.TodoPriorityID,
                                                     completionDt = a.CompletionDate != null ? NullableDateTimeToStringDate(a.CompletionDate) : "",
                                                     VerifyDt = a.VerifiedDate != null ? NullableDateTimeToStringDate(a.VerifiedDate) : "",
                                                     commentcount = comments != null ? comments.Where(m => m.TodoID == a.ID /*&& m.InsertedId != employeeid*/ && m.IsRead == false).ToList().Count() : 0

                                                 }).ToList();

            return result;

        }

        public SubTaskList_clsModel GetSingleTodoSubtaskByTodoTask(long todoid)
        {
            try
            {
                var res = GetAllTodoSubtaskByTodoTask(todoid).ToList().LastOrDefault();
                return res;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

           
            public List<TodoDepartmentModel> GetAllDepartmentListByEmployeeID(long employeeid)
        {
            try
            {
                var departments = commonMethods.GetDepartments();
                var employeedepartments = (from a in commonMethods.GetAllDesignationByUsingEmployeeId(employeeid)
                                           select new { a.DepartmentID }).ToList().Distinct();
                var res = (from a in employeedepartments
                           join b in departments on a.DepartmentID equals b.ID
                           select new TodoDepartmentModel
                           {
                               DepartmentID = b.ID,
                               Name = b.Name,
                               GroupID = b.GroupID
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

       


        public List<SubTaskList_clsModel> GetAllTaskAssignByOther(long employeeid , long[] TodoPriority, string[] TodoStatusName, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            try
            {
                List<SubTaskList_clsModel> tasks = GetallTasks(employeeid).OrderByDescending(a => a.todoid).ToList();

              var res = (from a in tasks
                                             where a.AssignToID == employeeid && a.AssignToID != 0 && a.AssignByID != employeeid
                                             select a).ToList();

                string finalTime = "23:59:59";
                Nullable<DateTime> tt = DateTime.Parse(finalTime);
                Nullable<DateTime> toDate = ToDate + tt.Value.TimeOfDay;


                if (FromDate != null || FromDate.ToString() != "")
                {
                    if (FromDate != null && ToDate != null)
                    {
                        res = res.Where(a => Convert.ToDateTime(a.InsertedDateTime.Date) >= FromDate && Convert.ToDateTime(a.InsertedDateTime.Date) <= toDate).ToList();
                    }
                    else
                    {
                        res = res.Where(a => Convert.ToDateTime(a.InsertedDateTime.Date) >= FromDate).ToList();
                    }
                }

                if (TodoPriority != null && TodoPriority.ToList().Count > 0 && TodoPriority.ToList()[0]!=0)
                {
                    res = res.Where(a => TodoPriority.Contains(a.TodoPriorityID)).ToList();
                }
                if (TodoStatusName != null && TodoStatusName.ToList().Count > 0 && TodoStatusName.ToList()[0]!= "string")
                {
                    res = res.Where(a => TodoStatusName.Contains(a.status)).ToList();
                }



                List<SubTaskList_clsModel> parents = res.Where(a => a.parentID == 0).ToList();
                if (parents.Count() > 0)
                {
                    foreach (SubTaskList_clsModel pa in parents.ToList())
                    {
                        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();
                        var ch = res.Where(a => a.parentID == pa.todoid).ToList();
                        if (ch.Count > 0)
                        {
                            foreach (var t in ch)
                            {
                                res.Remove(t);
                            }
                        }
                    }

                   
                }

                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public IEnumerable<TodoCommonModel> GetEmployeeforAssignToTask(long employeeid)
        {
            try
            {

                var tasks = GetallTasks(employeeid);
                var employees = employeeRepository.GetAllEmployee();
                if (tasks != null)
                {
                    List<TodoCommonModel> res = (from a in tasks
                                                 where a.AssignToID != employeeid && a.AssignToID != 0 && a.AssignByID == employeeid
                                                 group a by a.AssignToID into g
                                                 select new TodoCommonModel
                                                 {
                                                     ID = g.Key,
                                                     Name = employees.Where(m => m.ID == g.Key).Select(b => (b.FirstName + " " + b.LastName)).FirstOrDefault()
                                                 }).Distinct().ToList();
                    return res;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public List<SubTaskList_clsModel> GetAllTaskAssignToOther(long employeeid , long[] employe, long[] TodoPriority , string[] TodoStatusName, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            try
            {
                List<SubTaskList_clsModel> tasks = GetallTasks(employeeid).OrderByDescending(a => a.todoid).ToList();
                var res = (from a in tasks
                        where a.AssignToID != employeeid && a.AssignToID != 0 && a.AssignByID == employeeid
                        select a).ToList();

                    if ( employe != null && employe.ToList().Count > 0 && employe.ToList()[0]!=0 )
                    {
                        res = res.Where(a => employe.Contains(a.AssignToID)).ToList();
                    }
                    if ( TodoPriority != null && TodoPriority.ToList().Count > 0 && TodoPriority.ToList()[0]!=0 )
                    {
                        res = res.Where(a => TodoPriority.Contains(a.TodoPriorityID)).ToList();
                    }
                    if ( TodoStatusName!= null && TodoStatusName.ToList().Count > 0 && TodoStatusName.ToList()[0]!= "string" )
                    {
                        res = res.Where(a => TodoStatusName.Contains(a.status)).ToList();
                    }

                string finalTime = "23:59:59";
                Nullable<DateTime> tt = DateTime.Parse(finalTime);
                Nullable<DateTime> toDate = ToDate + tt.Value.TimeOfDay;


                if (FromDate != null || FromDate.ToString() != "")
                {
                    if (FromDate != null && ToDate != null)
                    {
                        res = res.Where(a => Convert.ToDateTime(a.InsertedDateTime) >= FromDate && Convert.ToDateTime(a.InsertedDateTime) <= toDate).ToList();
                    }
                    else
                    {
                        res = res.Where(a => Convert.ToDateTime(a.InsertedDateTime) >= FromDate).ToList();
                    }
                }

                List<SubTaskList_clsModel> parents = res.Where(a => a.parentID == 0).ToList();
                if (parents.Count() > 0)
                {
                    foreach (SubTaskList_clsModel pa in parents.ToList())
                    {
                        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                        var ch = res.Where(a => a.parentID == pa.todoid).ToList();
                        if (ch.Count > 0)
                        {
                            foreach (var t in ch)
                            {
                                res.Remove(t);
                            }
                        }
                    }
                }

                return res;

               


            }
            catch (Exception e1)
            {
                return null;
            }
        }

    


        public SubTaskList_clsModel GetSingleTasks(long taskid)
        {
            try
            {
                var employees = empRepository.GetAllEmployeewithleft();
                var todo = todoRepository.GetAllTodo().Where(x=>x.ID == taskid);
                var todostatus = todoRepository.GetAllTodoStatus().ToList();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = commonMethods.GetDepartments();
                var comments = todoRepository.GetAllTodoComment();
                var res = (from a in todo
                               //join e in employees on a.EmployeeID equals e.id
                               //join c in priority on a.TodoPriorityID equals c.ID
                           
                           select new SubTaskList_clsModel
                           {
                               todoid = a.ID,
                               todoname = a.Name,
                               status = a.Status,                              
                               AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                               tododescription = a.Description,
                               DueDate = a.DueDate != null ? a.DueDate.Value.ToString("dd-MMM-yyyy") : "",
                               AssignByDepartmentID = a.AssignByDepartmentID,
                               AssignByDepartmentName = (a.AssignByDepartmentID == 0) ? "" : departments.Where(m => m.ID == a.AssignByDepartmentID).FirstOrDefault().Name,
                               assignedto = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).Select(p => p.FirstName + " " + p.LastName).FirstOrDefault() : "",
                               assignedby = a.EmployeeID == 0 ? "" : employees.Where(m => m.ID == a.EmployeeID).Select(p => p.FirstName + " " + p.LastName).FirstOrDefault(),
                               profile = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).FirstOrDefault().Image : "",
                               priorityName = a.TodoPriorityID == 0 ? "" : priority.Where(m => m.ID == a.TodoPriorityID).FirstOrDefault().Name,
                               department = (a.DepartmentID == 0) ? "" : departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name,
                               assigntime = a.AssignedDate != null ? a.AssignedDate.Value.ToString("hh:mm tt") : "NA",
                               AssignByID = a.EmployeeID,
                               AssignDate = a.AssignedDate,                             
                               AssignToID = a.AssignedId,                             
                               ModifiedDate = a.ModifiedDate,
                               AssignDateTime = NullableDateTimeToStringDateTime(a.AssignedDate),
                               ModifiedDateTime =NullableDateTimeToStringDateTime(a.ModifiedDate),
                               InsertedDateTime= NullableDateTimeToStringDateTime(a.InsertedDate),
                               parentID = a.ParentId,
                               DepartmentID = a.DepartmentID,
                               TodoPriorityID = a.TodoPriorityID,
                               ScoreGivenDateTime = NullableDateTimeToStringDateTime(a.ScoreGivenDate),
                               completionDt = a.CompletionDate != null ? a.CompletionDate.Value.ToString("dd-MMM-yyyy") : "",
                               VerifyDt = a.VerifiedDate != null ? a.VerifiedDate.Value.ToString("dd-MMM-yyyy") : "",
                               commentcount = comments != null ? comments.Where(m => m.TodoID == a.ID /*&& m.InsertedId != employeeid*/ && m.IsRead == false).ToList().Count() : 0
                               
                           }).ToList().FirstOrDefault();

                

                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }


        public List<SubTaskList_clsModel> GetallTasks(long employeeid)
        {
            try
            {
                var employees =commonMethods.GetEmployeesForTaskswithleft(employeeid);               
                var todo = todoRepository.GetAllTodo().ToList();
                var todostatus = todoRepository.GetAllTodoStatus();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = commonMethods.GetDepartments();
                var comments = todoRepository.GetAllTodoComment();
                var res = (from a in todo
                               //join e in employees on a.EmployeeID equals e.id
                               //join c in priority on a.TodoPriorityID equals c.ID

                           select new SubTaskList_clsModel
                           {
                               todoid = a.ID,
                               todoname = a.Name != null ? a.Name : "NA",
                               status = a.Status != null ? a.Status : "NA",
                               AssignedDate = a.AssignedDate != null ? NullableDateTimeToStringDate(a.AssignedDate) : string.Empty,
                               tododescription = a.Description != null ? a.Description : "NA",
                               DueDate = a.DueDate != null ? NullableDateTimeToStringDate(a.DueDate) : string.Empty,
                               AssignByDepartmentID = a.AssignByDepartmentID,
                               AssignByDepartmentName = (a.AssignByDepartmentID != 0) ? departments.Where(m => m.ID == a.AssignByDepartmentID).FirstOrDefault().Name : "",
                               assignedto = a.AssignedId != 0 ? employees.Where(m => m.id == a.AssignedId).FirstOrDefault().name != null ? employees.Where(m => m.id == a.AssignedId).FirstOrDefault().name : "NA" : "",
                               assignedby = a.EmployeeID != 0 ? employees.Where(m => m.id == a.EmployeeID).FirstOrDefault().name != null ? employees.Where(m => m.id == a.EmployeeID).FirstOrDefault().name : "NA" : "",
                               profile = a.AssignedId != 0 ? employees.Where(m => m.id == a.AssignedId).FirstOrDefault().profile != null ? employees.Where(m => m.id == a.AssignedId).FirstOrDefault().profile : "NA" : "",
                               priorityName = a.TodoPriorityID != 0 ? priority.Where(m => m.ID == a.TodoPriorityID).FirstOrDefault().Name != null ? priority.Where(m => m.ID == a.TodoPriorityID).FirstOrDefault().Name : "NA" : "",
                               department = (a.DepartmentID != 0) ? departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name != null ? departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name : "NA" : "",
                               assigntime = a.AssignedDate != null ? a.AssignedDate.Value.ToString("hh:mm tt") : "NA",
                               AssignByID = a.EmployeeID,
                               AssignDate = a.AssignedDate,
                               AssignDateTime=NullableDateTimeToStringDateTime(a.AssignedDate),
                               ModifiedDateTime=NullableDateTimeToStringDateTime(a.ModifiedDate),
                               InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                               AssignToID = a.AssignedId,
                               ModifiedDate = a.ModifiedDate,
                               parentID = a.ParentId,
                               DepartmentID = a.DepartmentID,
                               TodoPriorityID = a.TodoPriorityID,
                               completionDt = a.CompletionDate != null ? a.CompletionDate.Value.ToString("dd-MMM-yyyy") : "NA",
                               VerifyDt = a.VerifiedDate != null ? a.VerifiedDate.Value.ToString("dd-MMM-yyyy") : "NA",
                               Score = a.Score,
                               ScoreGivenId = a.ScoreGivenId,
                               ScoreGivenDate = a.ScoreGivenDate,
                               ScoreGivenDateTime=NullableDateTimeToStringDateTime(a.ScoreGivenDate),
                               subtaskCount = (from s in todo
                                               where s.ParentId == a.ID
                                               select a.ParentId).Count(),
                               commentcount = comments != null ? comments.Where(m => m.TodoID == a.ID && m.InsertedId != employeeid && m.IsRead == false).ToList().Count() : 0

                           }).ToList();



                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }


        public IEnumerable<EmployeeModel> EmployeeList()
        {
            try
            {
                var empList = empRepository.GetAllEmployee();
                var bloodGroupList = bloodGroupRepository.GetAllBloodGroup();
                var nationalityList = nationalityTypeRepository.GetAllNationalities();
                var religionList = religionTypeRepository.GetAllReligionTypes(); 
                
                var res = (from a in empList
                       select new EmployeeModel
                       {
                           ID = a.ID,
                           EmpCode = a.EmpCode,
                           FirstName = a.FirstName,
                           MiddleName = a.MiddleName,
                           LastName = a.LastName,
                           FullName = a.FirstName + " " + a.MiddleName + " " + a.LastName,
                           Gender = a.Gender,
                           DateOfBirth = a.DateOfBirth,
                           BloodGroupID = a.BloodGroupID,
                           BloodGroupName = (a.BloodGroupID != null && a.BloodGroupID!=0) ? bloodGroupList.FirstOrDefault(b => b.ID == a.BloodGroupID).Name!=null? bloodGroupList.FirstOrDefault(b => b.ID == a.BloodGroupID).Name:"NA" : "NA",
                           PrimaryMobile = a.PrimaryMobile,
                           AlternativeMobile = a.AlternativeMobile,
                           LandlineNumber = a.LandlineNumber,
                           EmailId = a.EmailId,
                           Image = a.Image,
                           NatiobalityID = a.NatiobalityID,
                           NationalityName = (a.NatiobalityID != null && a.NatiobalityID != 0) ? nationalityList.FirstOrDefault(b => b.ID == a.NatiobalityID).Name !=null ? nationalityList.FirstOrDefault(b => b.ID == a.NatiobalityID).Name :"NA": "NA",
                           ReligionID = a.ReligionID,
                           ReligionName = (a.ReligionID != null && a.ReligionID!=0) ? religionList.FirstOrDefault(b => b.ID == a.ReligionID).Name !=null? religionList.FirstOrDefault(b => b.ID == a.ReligionID).Name :"NA": "NA",
                           MaritalStatus = a.MaritalStatus,
                           Isleft = a.Isleft,
                           LeftDate = a.LeftDate
                       }).ToList(); 

                return res;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<commonClsModel> GetEmployeesForTasks(long id)
        {
            try
            {
                var employees = employeeRepository.GetAllEmployee();

                var self = (from a in employees
                            where a.ID == id
                            select new commonClsModel
                            {
                                id = a.ID,
                                name = "me",
                                profile = a.Image!=null?a.Image:"NA"
                            }).ToList();
                var exceptself = (from a in employees
                                  where a.ID != id
                                  select new commonClsModel
                                  {
                                      id = a.ID,
                                      name = a.FirstName + " " + a.LastName,
                                      profile = a.Image != null ? a.Image : "NA"
                                  }).ToList();

                var res = self.Union(exceptself).ToList();

                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public long CreateNewTask(TodoCreateModels todoModel)
        { 
                Todo todo = new Todo();
                long id = 0; 
            //var emp = employeeRepository.GetEmployeeById(userId);
            //var assignemp = employeeRepository.GetEmployeeById(todoModel.AssignedId.Value);
            //var priorities = todoRe 
                    todo.Active = true;
                    todo.AssignedDate = DateTime.Now;
                    todo.AssignedId = todoModel.assignedId == null ? todoModel.userId : todoModel.assignedId.Value;
                    todo.DueDate = todoModel.deadLineDate;
                    todo.AssignByDepartmentID = todoModel.assignByDepartmentID==null? 0: todoModel.assignByDepartmentID.Value;
                    todo.EmployeeID = todoModel.userId;
                    todo.InsertedId = todoModel.userId;
                    todo.ModifiedId = todoModel.userId;
                    todo.DepartmentID = todoModel.departmentID==null? 0: todoModel.departmentID.Value;
                    todo.Description = todoModel.description;
                    todo.Name = todoModel.taskName;
                    todo.ParentId = 0;
                    todo.Status = "PENDING";
                    todo.TodoPriorityID = todoModel.todoPriorityID;
                    todo.Score =0;
                    todo.ScoreGivenId = 0;
                    todoRepository.CreateTodo(todo);
                    id = todo.ID;
                    TodoTimeline todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = todoModel.userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = todoModel.userId;
                    // todoTimeline.RelatedDate = DateTime.Now;
                    // todoTimeline.RelatedId = userId;
                    //if (todoModel.TodoStatusName != null)
                    //{
                    //    todoTimeline.Status = todoModel.TodoStatusName;
                    //}
                    //else
                    //{
                    todoTimeline.Status = "PENDING";
                    //}
                    todoTimeline.TodoID = id;
                    todoTimeline.TodoStatus = "Task Created";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                    if (todoModel.assignedId != 0 && todoModel.assignedId != null)
                    {

                        todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = todoModel.userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = todoModel.userId;
                        //  todoTimeline.RelatedDate = DateTime.Now;
                        todoTimeline.RelatedId = todoModel.assignedId == null ? 0 : todoModel.assignedId.Value;
                        //  todoTimeline.Status = todoModel.TodoStatusName;
                        todoTimeline.TodoID = id;
                        todoTimeline.TodoStatus = "Task Assigned";
                        todoRepository.CreateTodoTimeline(todoTimeline);
                        //sms.taskmail("You have been assigned a new task by " + emp.FirstName + " " + emp.LastName + ".<br/>Task Name : " + todoModel.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + todoModel.Description + "<br/><br/>We are sure you will complete the given task before the deadline. In case of any issue with the task, contact the assignor.", "Task Assigned", assignemp.EmailId);
                    }
                    if (todoModel.deadLineDate != null)
                    {
                        todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = todoModel.userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = todoModel.userId;
                        todoTimeline.RelatedDate = todoModel.deadLineDate;
                        //  todoTimeline.RelatedId = userId;
                        // todoTimeline.Status = todoModel.TodoStatusName;
                        todoTimeline.TodoID = id;
                        todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                        todoRepository.CreateTodoTimeline(todoTimeline);
                    }
              
             

            if (id != 0)
                return id;
            else
                return 0;

        }

        public List<todoTimelinesModel> GetTaskTimelines(long taskid)
        {
            var timelines = todoRepository.GetAllTodoTimeline().Where(a => a.TodoID == taskid).ToList();
            var employees = employeeRepository.GetAllEmployee();
            var res = (from a in timelines
                       join b in employees on a.InsertedId equals b.ID
                       select new todoTimelinesModel
                       {
                           id=a.ID,
                           TodoID=a.TodoID,
                           InsertedDate = a.InsertedDate,
                           RelatedDate = a.RelatedDate,
                           RelatedId = a.RelatedId,
                           Status = a.Status,
                           TodoStatus = a.TodoStatus,
                           RelatedName = a.RelatedId != 0 ? employees.Where(c => c.ID == a.RelatedId).Select(c => new { Name = c.FirstName + " " + c.LastName }).FirstOrDefault().Name : "",
                           insertedBy = b.FirstName + " " + b.LastName,
                           insertedDateTime=NullableDateTimeToStringDateTime(a.InsertedDate),
                           modifiedDateTime= NullableDateTimeToStringDateTime(a.ModifiedDate),

                       }).ToList();
            return res;
        }

        public todoTimelinesModel GetSingleTaskTimelines(long taskid)
        {
            try
            {
             var res = GetTaskTimelines(taskid).ToList().LastOrDefault();          
            return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public List<todocommentsModel> GetComments(long taskid, long empid)
        {
            try
            {
                var comments = todoRepository.GetAllTodoComment();
                var employees = commonMethods.GetEmployeesForTaskswithleft(empid);
                var res = (from a in comments
                           join b in employees on a.InsertedId equals b.id
                           where a.TodoID == taskid
                           select new todocommentsModel
                           {
                               id = a.ID,
                               Description = a.Description,
                               EmployeeName = b.name,
                               EmployeeID = a.InsertedId,
                               TodoID = a.TodoID,
                               postedON = a.InsertedDate.ToString("dd-MMM-yyyy hh:mm tt"),
                               profile = b.profile,
                               insertedDate = a.InsertedDate,
                               modifiedDate = a.ModifiedDate,
                               insertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                               modifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),
                           }).Distinct().ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public todocommentsModel GetSingleComments(long taskid, long empid)
        {
            try
            {               
                var res = GetComments(taskid, empid).ToList().LastOrDefault();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<todoAttachmentModel> GetAttachments(long taskid, long empid)
        {
            try
            {
                var attachments = todoRepository.GetAllTodoAttachment();
                var employees = commonMethods.GetEmployeesForTasks(empid);
                var res = (from a in attachments
                           join b in employees on a.InsertedId equals b.id
                           where a.TodoID == taskid
                           select new todoAttachmentModel
                           {
                               AttachmentName = a.AttachmentName,
                               Attachmenturl = "http://odmpserp.thesuperguard.in/"+"ODMImages/ToDoAttachments/" + a.Attachmenturl,
                               employeeid = a.InsertedId,
                               employeeName = b.name,
                               TodoID = a.TodoID,
                               id = a.ID,
                               insertedDate = a.InsertedDate,
                               modifiedDate = a.ModifiedDate,
                               insertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                               modifiedDateTime = NullableDateTimeToStringDateTime(a.ModifiedDate),

                           }).OrderByDescending(a => a.id).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public todoAttachmentModel GetSingleAttachments(long taskid, long empid)
        {
            try
            {               
                var res = GetAttachments(taskid, empid).ToList().LastOrDefault();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public IEnumerable<TodoStatusModel> GetAllStattus()
        {
            List<TodoStatusModel> list = new List<TodoStatusModel>();
            TodoStatusModel s1 = new TodoStatusModel();
            s1.Name = "PENDING";
            s1.ID = "PENDING";

            TodoStatusModel s2 = new TodoStatusModel();
            s2.Name = "ON-HOLD";
            s2.ID = "ON-HOLD";

            TodoStatusModel s3 = new TodoStatusModel();
            s3.Name = "IN-PROGRESS";
            s3.ID = "IN-PROGRESS";

            TodoStatusModel s4 = new TodoStatusModel();
            s4.Name = "COMPLETED";
            s4.ID = "COMPLETED";
            TodoStatusModel s5 = new TodoStatusModel(); 
            s5.Name = "VERIFY";
            s5.ID = "VERIFY";
            


            list.Add(s1);
            list.Add(s2);
            list.Add(s3);
            list.Add(s4);
            list.Add(s5);  
            IEnumerable<TodoStatusModel> res = list.ToList(); 

            return res;
        }
        public IEnumerable<TodoStatusModel> GetAllStattusForTask()
        {
            List<TodoStatusModel> list = new List<TodoStatusModel>();
            TodoStatusModel s1 = new TodoStatusModel();
            s1.Name = "PENDING";
            s1.ID = "PENDING";

            TodoStatusModel s2 = new TodoStatusModel();
            s2.Name = "ON-HOLD";
            s2.ID = "ON-HOLD";

            TodoStatusModel s3 = new TodoStatusModel();
            s3.Name = "IN-PROGRESS";
            s3.ID = "IN-PROGRESS";

            TodoStatusModel s4 = new TodoStatusModel();
            s4.Name = "COMPLETED";
            s4.ID = "COMPLETED";
            
            list.Add(s1);
            list.Add(s2);
            list.Add(s3);
            list.Add(s4);
             
            IEnumerable<TodoStatusModel> res = list.ToList();

            return res;
        }

        public List<SubTaskList_clsModel> GetAllMyTask(long employeeid, long[] TodoPriority, string[] TodoStatusName, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            try
            {
                List<SubTaskList_clsModel> tasks = GetallTasks(employeeid);
                List<SubTaskList_clsModel> res = (from a in tasks
                           where a.AssignByID == employeeid && (a.AssignToID == employeeid || a.AssignToID == 0) && a.parentID == 0 && a.DepartmentID == 0
                           select a).ToList();

                if ( TodoPriority != null && TodoPriority.ToList().Count > 0 && TodoPriority.ToList()[0] != 0 )
                {
                 res = res.Where(a => TodoPriority.Contains(a.TodoPriorityID)).ToList();
                }
                if (  TodoStatusName != null &&  TodoStatusName.ToList().Count > 0 && TodoStatusName.ToList()[0] != "string" )
                {
                    res = res.Where(a => TodoStatusName.Contains(a.status)).ToList();
                }

                string finalTime = "23:59:59";
                Nullable<DateTime> tt = DateTime.Parse(finalTime);
                Nullable<DateTime> toDate = ToDate + tt.Value.TimeOfDay;


                if (FromDate != null || FromDate.ToString() != "")
                {
                    if (FromDate != null && ToDate != null)
                    {
                        res = res.Where(a => Convert.ToDateTime(a.InsertedDateTime.Date) >= FromDate && Convert.ToDateTime(a.InsertedDateTime.Date) <= toDate).ToList();
                    }
                    else
                    {
                        res = res.Where(a => Convert.ToDateTime(a.InsertedDateTime.Date) >= FromDate).ToList();
                    }
                }

                List<SubTaskList_clsModel> parents = res.Where(a => a.parentID == 0).ToList();
                if (parents.Count() > 0)
                {
                    foreach (SubTaskList_clsModel pa in parents.ToList())
                    {
                        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                        var ch = res.Where(a => a.parentID == pa.todoid).ToList();
                        if (ch.Count > 0)
                        {
                            foreach (var t in ch)
                            {
                                res.Remove(t);
                            }
                        }
                    }
                }

                return res;




            }
            catch (Exception e1)
            {
                return null;
            }
        }


        public long SaveTask(TodoModels todoModel)
        {
            
                Todo todo = new Todo();
                long id = 0;
               
                    if (todoModel.AssignedId != 0 && todoModel.AssignedId != null)
                    {
                        if (todoModel.AssignedId != todo.AssignedId)
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = todoModel.userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = todoModel.userId;
                            //  todoTimeline.RelatedDate = DateTime.Now;
                            todoTimeline.RelatedId = todoModel.AssignedId == null ? 0 : todoModel.AssignedId.Value;
                            //  todoTimeline.Status = todoModel.TodoStatusName;
                            todoTimeline.TodoID = todoModel.ID;
                            todoTimeline.TodoStatus = "Task Assigned";
                            todoRepository.CreateTodoTimeline(todoTimeline);
                            //sms.taskmail("You have been assigned a new task by " + emp.FirstName + " " + emp.LastName + ".<br/>Task Name : " + todoModel.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + todoModel.Description + "<br/><br/>We are sure you will complete the given task before the deadline. In case of any issue with the task, contact the assignor.", "Task Assigned", assignemp.EmailId);
                        }
                    }
                    if (todoModel.DeadLineDate != null)
                    {
                        if (todo.DueDate != null)
                        {
                            if (todoModel.DeadLineDate.Value.ToString("dd-MMM-yyyy") != todo.DueDate.Value.ToString("dd-MMM-yyyy"))
                            {
                                TodoTimeline todoTimeline = new TodoTimeline();
                                todoTimeline.Active = true;
                                todoTimeline.InsertedId = todoModel.userId;
                                todoTimeline.IsAvailable = true;
                                todoTimeline.ModifiedId = todoModel.userId;
                                todoTimeline.RelatedDate = todoModel.DeadLineDate;
                                //  todoTimeline.RelatedId = userId;
                                //   todoTimeline.Status = todoModel.TodoStatusName;
                                todoTimeline.TodoID = todoModel.ID;
                                todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                                todoRepository.CreateTodoTimeline(todoTimeline);
                            }
                        }
                        else
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = todoModel.userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = todoModel.userId;
                            todoTimeline.RelatedDate = todoModel.DeadLineDate;
                            // todoTimeline.RelatedId = userId;
                            // todoTimeline.Status = todoModel.TodoStatusName;
                            todoTimeline.TodoID = todoModel.ID;
                            todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                            todoRepository.CreateTodoTimeline(todoTimeline);
                        }
                    }
                    if (todoModel.TodoStatusName != todo.Status)
                    {
                        TodoTimeline todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = todoModel.userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = todoModel.userId;
                        //  todoTimeline.RelatedDate = todoModel.DueDate;
                        //  todoTimeline.RelatedId = userId;
                        todoTimeline.Status = todoModel.TodoStatusName;
                        todoTimeline.TodoID = todoModel.ID;
                        todoTimeline.TodoStatus = "Task " + todoModel.TodoStatusName.ToLower();
                        todoRepository.CreateTodoTimeline(todoTimeline);
                        if (todoModel.TodoStatusName == "COMPLETED")
                        {
                            var assignemp1 = employeeRepository.GetEmployeeById(todo.AssignedId);
                            //sms.taskmail("The task assigned by you to " + assignemp1.FirstName + " " + assignemp1.LastName + ", has been marked completed.<br/>Task Name : " + todoModel.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + todoModel.Description + "<br/><br/>Kindly verify the same and mark it complete from your end.", "Task Completed", emp.EmailId);
                        }
                    }
                    todo.ID = todoModel.ID;
                    todo.Active = true;
                    todo.AssignedDate = DateTime.Now;
                    todo.AssignedId = todoModel.AssignedId == null ? 0 : todoModel.AssignedId.Value;
                    todo.DueDate = todoModel.DeadLineDate;
                    todo.EmployeeID = todoModel.userId;
                    todo.ModifiedId = todoModel.userId;
                    todo.DepartmentID = todoModel.DepartmentID==null? 0: todoModel.DepartmentID.Value;
                    todo.Description = todoModel.Description;
                    todo.Name = todoModel.Name;
                    todo.AssignByDepartmentID = todoModel.AssignByDepartmentID==null?0: todoModel.AssignByDepartmentID.Value;
                    todo.TodoPriorityID = todoModel.TodoPriorityID;
                    if (todoModel.TodoStatusName == "COMPLETED")
                    {
                        todo.CompletionDate = DateTime.Now;
                    }
                    else
                    {
                        todo.CompletionDate = null;
                    }
                    todo.Status = todoModel.TodoStatusName;
                    todoRepository.UpdateTodo(todo);
                    var subtasks = todoRepository.GetAllTodo().Where(m => m.ParentId == todoModel.ID).ToList();
                    if (subtasks.Count() > 0)
                    {
                        foreach (var a in subtasks)
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = todoModel.userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = todoModel.userId;
                            //  todoTimeline.RelatedId = userId;
                            todoTimeline.Status = todoModel.TodoStatusName;
                            todoTimeline.TodoID = a.ID;
                            todoTimeline.TodoStatus = "Task " + todoModel.TodoStatusName;
                            todoRepository.CreateTodoTimeline(todoTimeline);
                            //if (todoModel.TodoStatusName == "COMPLETED")
                            //{
                            //    var empss = employeeRepository.GetEmployeeById(a.EmployeeID);
                            //    var prioritiess = todoRepository.GetTodoPriorityById(a.TodoPriorityID).Name;
                            //    var assignemp1 = employeeRepository.GetEmployeeById(a.AssignedId);
                            //    //sms.taskmail("The task assigned by you to " + assignemp.FirstName + " " + assignemp.LastName + ", has been marked completed.<br/>Task Name : " + a.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + a.Description + "<br/><br/>Kindly verify the same and mark it complete from your end.", "Task Completed", emp.EmailId);
                            //}
                            Todo todoS = a;
                            todoS.Status = todoModel.TodoStatusName;
                            todoS.ModifiedId = todoModel.userId;
                            if (todoModel.TodoStatusName == "COMPLETED")
                            {
                                todoS.CompletionDate = DateTime.Now;
                            }
                            todoRepository.UpdateTodo(todoS);
                        }
                    }
                    id = todoModel.ID;
               
               
            return id;
        }


        public  long CreateSubTask(long userId, long taskId, string name, long priority, long? assigndeptid, long? assignToId,
            long? deptid, DateTime? duedate, string desc)
        {            
            Todo todo = new Todo();
            long id = 0;
              
                todo.AssignByDepartmentID = assigndeptid==null? 0: assigndeptid.Value;
                todo.Active = true;
                todo.AssignedDate = DateTime.Now;
                todo.AssignedId = assignToId==null? userId : assignToId.Value;
                todo.DueDate = duedate;
                todo.EmployeeID = userId;
                todo.InsertedId = userId;
                todo.ModifiedId = userId;
                todo.DepartmentID = deptid==null? 0: deptid.Value;
                todo.Description = desc;
                todo.Name = name;
                todo.ParentId = taskId;
                todo.Status = "PENDING";
                todo.TodoPriorityID = priority;
                todo.Score = 0;
                todo.ScoreGivenId = 0;
            todoRepository.CreateTodo(todo);
                id = todo.ID;
                TodoTimeline todoTimeline = new TodoTimeline();
                todoTimeline.Active = true;
                todoTimeline.InsertedId = userId;
                todoTimeline.IsAvailable = true;
                todoTimeline.ModifiedId = userId;
                // todoTimeline.RelatedDate = DateTime.Now;
                // todoTimeline.RelatedId = userId;
                todoTimeline.Status = "PENDING";
                todoTimeline.TodoID = id;
                todoTimeline.TodoStatus = "Task Created";
                todoRepository.CreateTodoTimeline(todoTimeline);
                if (assignToId != 0 && assignToId != null)
                {
                    todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId;
                    //todoTimeline.RelatedDate = DateTime.Now;
                    todoTimeline.RelatedId = assignToId.Value;
                    todoTimeline.Status = "PENDING";
                    todoTimeline.TodoID = id;
                    todoTimeline.TodoStatus = "Task Assigned";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                    //sms.taskmail("You have been assigned a new task by " + emp.FirstName + " " + emp.LastName + ".<br/>Task Name : " + name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + desc + "<br/><br/>We are sure you will complete the given task before the deadline. In case of any issue with the task, contact the assignor.", "Task Assigned", emp.EmailId);
                }
                if (duedate != null)
                {
                    todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId;
                    todoTimeline.RelatedDate = duedate;
                    //  todoTimeline.RelatedId = userId;
                    //   todoTimeline.Status = "PENDING";
                    todoTimeline.TodoID = id;
                    todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                }

            if (id != 0)
                return id;
            else
                return 0;
            
        }

        public long SaveSubTask(long userId, long taskId, long subtaskid, string name, long priority, long? assigndeptid, long? assignToId,
            long? deptid, DateTime? duedate, string desc, string status)
        {            
            Todo todo = new Todo();
            long id = 0;
             
                if (assignToId != 0 && assignToId != null)
                {
                    if (assignToId != todo.AssignedId)
                    {
                        TodoTimeline todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        //  todoTimeline.RelatedDate = DateTime.Now;
                        todoTimeline.RelatedId = assignToId.Value;
                        // todoTimeline.Status = todo.Status;
                        todoTimeline.TodoID = subtaskid;
                        todoTimeline.TodoStatus = "Task Assigned";
                        todoRepository.CreateTodoTimeline(todoTimeline);
                        //sms.taskmail("You have been assigned a new task by " + emp.FirstName + " " + emp.LastName + ".<br/>Task Name : " + name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + desc + "<br/><br/>We are sure you will complete the given task before the deadline. In case of any issue with the task, contact the assignor.", "Task Assigned", emp.EmailId);
                    }
                }
                if (duedate != null)
                {
                    if (todo.DueDate != null)
                    {
                        if (duedate.Value.ToString("dd-MMM-yyyy") != todo.DueDate.Value.ToString("dd-MMM-yyyy"))
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = userId;
                            todoTimeline.RelatedDate = duedate;
                            // todoTimeline.RelatedId = userId;
                            // todoTimeline.Status = todo.Status;
                            todoTimeline.TodoID = subtaskid;
                            todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                            todoRepository.CreateTodoTimeline(todoTimeline);
                        }
                    }
                    else
                    {
                        TodoTimeline todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        todoTimeline.RelatedDate = duedate;
                        //  todoTimeline.RelatedId = userId;
                        //   todoTimeline.Status = todo.Status;
                        todoTimeline.TodoID = subtaskid;
                        todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                        todoRepository.CreateTodoTimeline(todoTimeline);
                    }
                }
                todo.ID = subtaskid;
                todo.AssignByDepartmentID = assigndeptid==null?0: assigndeptid.Value;
                todo.Active = true;
                todo.AssignedDate = DateTime.Now;
                todo.AssignedId = assignToId==null? 0: assignToId.Value;
                todo.DueDate = duedate;
                todo.EmployeeID = userId;
                todo.InsertedId = userId;
                todo.ModifiedId = userId;
                todo.DepartmentID = deptid==null? 0: deptid.Value;
                todo.Description = desc;
                todo.Status = status;
                todo.Name = name;
                todo.ParentId = taskId;
                todo.TodoPriorityID = priority;
                todoRepository.UpdateTodo(todo);
                id = subtaskid;           
            return id;
        }


        public TaskDashboardclsModel GetAllTaskDashboard(long userId, long accessId, long roleId, string fromdate, string todate)
        {
            var tasks = commonMethods.GetallTasks(userId);
            if (fromdate != null && todate != null)
            {
                DateTime F_dt = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                DateTime T_dt = Convert.ToDateTime(todate + " 23:59:59 PM");              
            }
            //else
            //{
            //    var month = DateTime.Now.Month;
            //    var year = DateTime.Now.Year;
            //    var totaldays = DateTime.DaysInMonth(year, month);
            //    DateTime F_dt = Convert.ToDateTime(year + "-" + month + "-01 00:00:00 AM");
            //    DateTime T_dt = Convert.ToDateTime(year + "-" + month + "-" + totaldays + " 23:59:59 PM");            
            //    tasks = tasks.Where(a => a.AssignDate.Value >= F_dt && a.AssignDate.Value <= T_dt).ToList();
            //}

            TaskDashboardclsModel taskDashboardcls = new TaskDashboardclsModel();
            taskDashboardcls.TotalMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0)).ToList().Count();
            taskDashboardcls.PendingMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "PENDING").ToList().Count();
            taskDashboardcls.onholdMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "ON-HOLD").ToList().Count();
            taskDashboardcls.inprogressMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "IN-PROGRESS").ToList().Count();
            taskDashboardcls.CompletedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "COMPLETED").ToList().Count();
            taskDashboardcls.VerifiedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "VERIFY").ToList().Count();


            taskDashboardcls.PendingMyTasksPer = taskDashboardcls.PendingMyTasks != 0 ? (taskDashboardcls.PendingMyTasks * 100) / taskDashboardcls.TotalMyTasks : 0;
            taskDashboardcls.onholdMyTasksPer = taskDashboardcls.onholdMyTasks != 0 ? (taskDashboardcls.onholdMyTasks * 100) / taskDashboardcls.TotalMyTasks : 0;
            taskDashboardcls.inprogressMyTasksPer = taskDashboardcls.inprogressMyTasks != 0 ? (taskDashboardcls.inprogressMyTasks * 100) / taskDashboardcls.TotalMyTasks : 0;
            taskDashboardcls.CompletedMyTasksPer = taskDashboardcls.CompletedMyTasks != 0 ? (taskDashboardcls.CompletedMyTasks * 100) / taskDashboardcls.TotalMyTasks : 0;
            taskDashboardcls.VerifiedMyTasksPer = taskDashboardcls.VerifiedMyTasks != 0 ? (taskDashboardcls.VerifiedMyTasks * 100) / taskDashboardcls.TotalMyTasks : 0;


            taskDashboardcls.TotalTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId).ToList().Count();
            taskDashboardcls.PendingTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "PENDING").ToList().Count();
            taskDashboardcls.onholdTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "ON-HOLD").ToList().Count();
            taskDashboardcls.inprogressTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "IN-PROGRESS").ToList().Count();
            taskDashboardcls.CompletedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "COMPLETED").ToList().Count();
            taskDashboardcls.VerifiedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "VERIFY").ToList().Count();

            taskDashboardcls.PendingTaskotherPer = taskDashboardcls.PendingTaskother != 0 ? (taskDashboardcls.PendingTaskother * 100) / taskDashboardcls.TotalTaskother:0;
            taskDashboardcls.onholdTaskotherPer = taskDashboardcls.onholdTaskother != 0?(taskDashboardcls.onholdTaskother * 100) / taskDashboardcls.TotalTaskother:0;
            taskDashboardcls.inprogressTaskotherPer = taskDashboardcls.inprogressTaskother != 0 ? (taskDashboardcls.inprogressTaskother * 100) / taskDashboardcls.TotalTaskother : 0;
            taskDashboardcls.CompletedTaskotherPer = taskDashboardcls.CompletedTaskother != 0 ? (taskDashboardcls.CompletedTaskother * 100) / taskDashboardcls.TotalTaskother : 0;
            taskDashboardcls.VerifiedTaskotherPer = taskDashboardcls.VerifiedTaskother != 0 ? (taskDashboardcls.VerifiedTaskother * 100) / taskDashboardcls.TotalTaskother : 0;
           



            taskDashboardcls.TotalTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId).ToList().Count();
            taskDashboardcls.PendingTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "PENDING").ToList().Count();
            taskDashboardcls.onholdTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "ON-HOLD").ToList().Count();
            taskDashboardcls.inprogressTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "IN-PROGRESS").ToList().Count();
            taskDashboardcls.CompletedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "COMPLETED").ToList().Count();
            taskDashboardcls.VerifiedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "VERIFY").ToList().Count();

            taskDashboardcls.PendingTasktomePer = taskDashboardcls.PendingTasktome != 0 ? (taskDashboardcls.PendingTasktome * 100) / taskDashboardcls.TotalTasktome : 0;
            taskDashboardcls.onholdTasktomePer = taskDashboardcls.onholdTasktome != 0 ? (taskDashboardcls.onholdTasktome * 100) / taskDashboardcls.TotalTasktome : 0;
            taskDashboardcls.inprogressTasktomePer = taskDashboardcls.inprogressTasktome != 0 ? (taskDashboardcls.inprogressTasktome * 100) / taskDashboardcls.TotalTasktome : 0;
            taskDashboardcls.CompletedTasktomePer = taskDashboardcls.CompletedTasktome != 0 ? (taskDashboardcls.CompletedTasktome * 100) / taskDashboardcls.TotalTasktome : 0;
            taskDashboardcls.VerifiedTasktomePer = taskDashboardcls.VerifiedTasktome != 0 ? (taskDashboardcls.VerifiedTasktome * 100) / taskDashboardcls.TotalTasktome : 0;


            return taskDashboardcls;
        }
        public TaskFilterVisibility AllTaskFilterVisibility(long accessId, long roleId)
        {
            TaskFilterVisibility fv = new TaskFilterVisibility();
            if (commonMethods.checkaccessavailable("View Tasks", accessId, "Management", "Task", roleId) == true)
            {
                fv.employee = true;
                fv.department = true;
               
            }
            else
            {
                fv.employee = false;
                fv.department = false;
               

            }
            return fv;
        }

        public taskviewmodel GetViewAllTask(long userId, long accessId, long roleId, string[] TodoStatusName, long[] TodoPriority, long[] employeeid,
           long[] departmentid, long? datetype, string FromDate, string ToDate, string Month)
        {

            //if (commonMethods.checkaccessavailable("View Tasks", accessId, "View", "Task", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            taskviewmodel taskview = new taskviewmodel();
            taskview.filterVisibility = AllTaskFilterVisibility(accessId, roleId);
            taskview.TodoStatusName = TodoStatusName;
            taskview.TodoPriority = TodoPriority;
            //taskview.Priorities = todoRepository.GetAllTodoPriority();

            taskview.datetype = datetype;
            if (Month != null)
            {
                taskview.m_date = Month;
            }
            else
            {
                taskview.m_date = null;
            }

            if (commonMethods.checkaccessavailable("View Tasks", accessId, "Management", "Task", roleId) == true)
            {
                taskview.departments = commonMethods.GetDepartments();
                taskview.departmentid = departmentid;
                taskview.employeecls = commonMethods.GetEmployeesForTasks(userId, departmentid);
                taskview.employeeid = employeeid;
            }
            else if (commonMethods.checkaccessavailable("View Tasks", accessId, "Departmentwise", "Task", roleId) == true)
            {
                var allempdept = commonMethods.GetDepartmentsByEmployeeID(userId);
                List<CommonMasterModel> mast = new List<CommonMasterModel>();
                foreach (var a in allempdept)
                {
                    var departments = commonMethods.GetallchilddepartmentbasedonParent(a.id);
                    mast.AddRange(departments);
                }
                taskview.departments = mast.Distinct();
                taskview.departmentid = departmentid;
                taskview.employeecls = commonMethods.GetEmployeesForTasks(userId, departmentid);
                taskview.employeeid = employeeid;
            }
           
            else
            {
                taskview.departments = null;
                taskview.departmentid = null;
                taskview.employeecls = null;
                taskview.employeeid = null;
            }
            if (FromDate != null && ToDate != null)
            {
                //DateTime f_dt = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);
                //DateTime t_dt = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);
                DateTime f_dt = Convert.ToDateTime(FromDate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                DateTime t_dt = Convert.ToDateTime(ToDate, System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                taskview.f_date = f_dt.ToString("yyyy-MM-dd");
                taskview.t_date = t_dt.ToString("yyyy-MM-dd");
                if (f_dt > t_dt)
                {

                    taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, null, null, null, null, null, null, null, null);
                    return taskview;
                }
                else
                {
                    taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, TodoStatusName, TodoPriority, employeeid, departmentid, datetype, f_dt, t_dt, Month);
                    return taskview;
                }
            }
            else if ((FromDate != null && ToDate == null) || (FromDate == null && ToDate != null))
            {
                taskview.f_date = null;
                taskview.t_date = null;
                //ViewBag.msg = "Both From date and To date Required";
                taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, null, null, null, null, null, null, null, null);
                return taskview;
            }
            else
            {
                taskview.f_date = null;
                taskview.t_date = null;
            }
            taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, TodoStatusName, TodoPriority, employeeid, departmentid, datetype, null, null, Month);
            return taskview;

        }


        public long CreateNewTaskAttachment(long userId,IFormFile file, string attachmentname, long todoid)
        {
            TodoAttachment todoAttachment = new TodoAttachment();
            long id = 0;
            todoAttachment.Active = true;
            todoAttachment.AttachmentName = attachmentname;
            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = attachmentname + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"..\Web\wwwroot\ODMImages\ToDoAttachments\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                todoAttachment.Attachmenturl = pathToSave;
            }
            todoAttachment.InsertedId = userId;
            todoAttachment.IsAvailable = true;
            todoAttachment.IsRead = false;
            todoAttachment.ModifiedId = userId;
            todoAttachment.Status = "Uploaded";
            todoAttachment.TodoID = todoid;
            todoRepository.CreateTodoAttachment(todoAttachment);
            id = todoAttachment.ID;
            if (id != 0)
                return id;
            else
                return 0;
        }

        public bool VerifyTaskAndSubTask(long userId, long accessId, long roleId, long id, decimal score)
        {

            long Id = id; 
            var subtasks = todoRepository.GetAllTodo().Where(m => m.ParentId == Id).ToList();
            if (subtasks.Count() > 0)
            {
                foreach (var a in subtasks)
                {
                    var emp = employeeRepository.GetEmployeeById(a.EmployeeID);
                    var priorities = todoRepository.GetTodoPriorityById(a.TodoPriorityID).Name;
                    TodoTimeline todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId; 
                    todoTimeline.Status = "VERIFY";
                    todoTimeline.TodoID = a.ID;
                    todoTimeline.TodoStatus = "Task " + "VERIFY";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                   
                    Todo todo = a;
                    todo.Status = "VERIFY";
                    todo.VerifiedDate = DateTime.Now;
                    todo.Score = score;
                    todo.ScoreGivenId = userId;
                    todo.ScoreGivenDate = DateTime.Now;
                    todoRepository.UpdateTodo(todo);
                }
            } 

            var ab = todoRepository.GetTodoById(Id);
            var emp1 = employeeRepository.GetEmployeeById(ab.EmployeeID);
            var priorities1 = todoRepository.GetTodoPriorityById(ab.TodoPriorityID).Name;
            TodoTimeline todoTimeline1 = new TodoTimeline();
            todoTimeline1.Active = true;
            todoTimeline1.InsertedId = userId;
            todoTimeline1.IsAvailable = true;
            todoTimeline1.ModifiedId = userId; 
            todoTimeline1.Status = "VERIFY";
            todoTimeline1.TodoID = ab.ID;
            todoTimeline1.TodoStatus = "Task " + "VERIFY";
            todoRepository.CreateTodoTimeline(todoTimeline1);
           
            ab.Status = "VERIFY";
            ab.VerifiedDate = DateTime.Now;
            ab.ModifiedDate = DateTime.Now;
            ab.ModifiedId = userId;
            ab.Score = score;
            ab.ScoreGivenId = userId;
            ab.ScoreGivenDate = DateTime.Now;
            todoRepository.UpdateTodo(ab); 

            return true;

        }

       
        public bool RemoveTaskAndSubtask(long id)
        {
            var task = toDoRepository.GetAllTodo().Where(a => a.ID == id && a.Active == true).FirstOrDefault(); ;

            Todo todo = new Todo();

            todo.ID = task.ID;
            todo.InsertedId = task.InsertedId;
            todo.ModifiedId = task.ModifiedId;
            todo.InsertedDate = task.InsertedDate;
            todo.ModifiedDate = DateTime.Now;
            todo.Active = false;
            todo.Status = "DELETED";
            todo.IsAvailable = false;
            todo.EmployeeID = task.EmployeeID;
            todo.AssignByDepartmentID = task.AssignByDepartmentID;
            todo.DepartmentID = task.DepartmentID;
            todo.TodoPriorityID = task.TodoPriorityID;
            todo.TodoStatusID = 0;
            todo.Name = task.Name;
            todo.Description = task.Description;
            todo.DueDate = task.DueDate;
            todo.AssignedDate = task.AssignedDate;
            todo.CompletionDate = task.CompletionDate;
            todo.VerifiedDate = DateTime.Now;
            todo.AssignedId = task.AssignedId;
            todo.ParentId = task.ParentId;
            todo.IsRead = false;

            toDoRepository.UpdateTodo(todo);

            return true;
        }

        public List<StatusModel> TodoStatusModel()
        {
            var result = new List<StatusModel> { 
                                new StatusModel { ID = "PENDING", Name="PENDING" },
                                new StatusModel { ID = "ON-HOLD", Name = "ON-HOLD" },
                                new StatusModel { ID = "IN-PROGRESS",Name = "IN-PROGRESS"},
                                new StatusModel { ID = "COMPLETED",Name = "COMPLETED"}
                                }.ToList();
            return result;
        }
     

        #region
        public viewModel GetAllViewAllTaskWithFilteration(long userId, long accessId, long roleId)
        {
            try
            {
                viewModel resultCount = new viewModel();
                IEnumerable<CommonMasterModel> det = new List<CommonMasterModel>();
                var alltasks = GetallTasks(userId).Where(a => a.DepartmentID != 0 && a.AssignByDepartmentID != 0).ToList();
                if (commonMethods.checkaccessavailable("View Tasks", accessId, "Management", "Task", roleId) == true)
                {
                    det = commonMethods.GetDepartments();
                }
                else if (commonMethods.checkaccessavailable("View Tasks", accessId, "Departmentwise", "Task", roleId) == true)
                {
                    var allempdept = commonMethods.GetDepartmentsByEmployeeID(userId);
                    List<CommonMasterModel> mast = new List<CommonMasterModel>();
                    foreach (var a in allempdept)
                    {
                        var departments = commonMethods.GetallchilddepartmentbasedonParent(a.id);
                        mast.AddRange(departments);
                    }
                    det = mast.Distinct();
                }
                else
                {
                    // emploid = employeeid;
                    det = commonMethods.GetDepartments();
                    alltasks = alltasks.Where(a => a.AssignByID == userId || a.AssignToID == userId).ToList();
                }
              
                var tasks1 = (from a in alltasks
                              join b in det on a.DepartmentID equals b.ID
                              select a).ToList();
                var tasks2 = (from a in alltasks
                              join b in det on a.AssignByDepartmentID equals b.ID
                              select a).ToList();
                var tasks = tasks1.Union(tasks2).Distinct();



                List<SubTaskList_clsModel> res = (from a in tasks
                                             select a).ToList();

                if (res != null)
                {
                    resultCount.totaltask = res.Count();
                    resultCount.Completedtask = res.Where(a => a.status == "COMPLETED").ToList().Count();
                    resultCount.inprogresstask = res.Where(a => a.status == "IN-PROGRESS").ToList().Count();
                    resultCount.onholdtask = res.Where(a => a.status == "ON-HOLD").ToList().Count();
                    resultCount.Pendingtask = res.Where(a => a.status == "PENDING").ToList().Count();
                    resultCount.Verifiedtask = res.Where(a => a.status == "VERIFIED").ToList().Count();
                }
                else
                {
                    resultCount.totaltask = 0;
                    resultCount.Completedtask = 0;
                    resultCount.inprogresstask = 0;
                    resultCount.onholdtask = 0;
                    resultCount.Pendingtask = 0;
                    resultCount.Verifiedtask = 0;
                }
                //List<SubTaskList_clsModel> parents = res.Where(a => a.parentID == 0).ToList();
                //if (parents.Count() > 0)
                //{
                //    foreach (SubTaskList_clsModel pa in parents.ToList())
                //    {
                //        pa.subtasks = res.Where(m => m.parentID == pa.todoid).ToList();

                //        var ch = res.Where(a => a.parentID == pa.todoid).ToList();
                //        if (ch.Count > 0)
                //        {
                //            foreach (var t in ch)
                //            {
                //                res.Remove(t);
                //            }
                //        }
                //    }
                //}

                return resultCount;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        #endregion

        public List<empClass> Employee()
        {
            var emp = empRepository.GetAllEmployee().Select(a => new empClass { 
                EmpId = a.ID,
                EmpCode = a.EmpCode,
                EName = a.FirstName + " " + a.LastName
            }).ToList();

            return emp;
        }

     
    }
}
