﻿using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Entities.TodoAggregate;
using System.IO;

namespace WebAPI.Data
{
    public class CommonMethods
    {
        private IEmployeeRepository employeeRepository;
        private IDepartmentRepository departmentRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private ICityRepository cityRepository;
        private IStateRepository stateRepository;
        private ICountryRepository countryRepository;
        private IDesignationLevelRepository designationLevelRepository;
        private IDesignationRepository designationRepository;
        private IGroupRepository groupRepository;
        private IOrganizationRepository organizationRepository;
        private IReligionTypeRepository religionTypeRepository;

        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IAddressRepository addressRepository;
        private IEmployeeAddressRepository employeeAddressRepository;
        private IEmployeeBankAccountRepository bankAccountRepository;
        private IBankRepository bankRepository;
        private IBankBranchRepository bankBranchRepository;
        private IEmployeeEducationRepository employeeEducationRepository;
        private IEducationQualificationRepository educationQualificationRepository;
        private IEducationQualificationTypeRepository educationQualificationTypeRepository;
        private IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IEmployeeExperienceRepository employeeExperienceRepository;
        private IAccessRepository accessRepository;
        private IAccessTypeRepository accessTypeRepository;
        private IRoleRepository roleRepository;
        private IModuleRepository moduleRepository;
        private ISubModuleRepository subModuleRepository;
        private IActionAccessRepository actionAccessRepository;
        private IActionRepository actionRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IGrievanceRepository grievanceRepository;
        private IStandardRepository standardRepository;
        private ISectionRepository sectionRepository;
        private IBoardRepository boardRepository;
        private IToDoRepository todoRepository;
        private IEmployeeDocumentAttachmentRepository employeeDocumentAttachmentRepository;
        private ISupportRepository supportRepository;
        private IErpBugRepository erpBugRepository;
        private ILogingRepository logingRepository;

        private IOrganizationAcademicRepository organizationAcademicRepository;
        //public commonsqlquery commonsql;
        /*  Session  */
      private readonly IHttpContextAccessor _httpContextAccessor;
        //private readonly ISession _session;
        /*  Session  */
        public CommonMethods(IEmployeeDocumentAttachmentRepository employeeDocumentAttachmentRepo, IEmployeeRepository employeeRepo, IDepartmentRepository departmentRepo, IBloodGroupRepository bloodGroupRepo,
            INationalityTypeRepository nationalityTypeRepo, IAddressTypeRepository addressTypeRepo, ICityRepository cityRepo,
            IStateRepository stateRepo, ICountryRepository countryRepo, IDesignationLevelRepository designationLevelRepo, IDesignationRepository designationRepo,
            IGroupRepository groupRepo, IOrganizationRepository organizationRepo, IReligionTypeRepository religionTypeRepo, IEmployeeDesignationRepository employeeDesignationRepo,
            IAddressRepository addressRepo, IEmployeeAddressRepository employeeAddressRepo, IEmployeeBankAccountRepository bankAccountRepo, IBankRepository bankRepo,
            IBankBranchRepository bankBranchRepo, IEmployeeEducationRepository employeeEducationRepo, IEducationQualificationRepository educationQualificationRepo,
            IEducationQualificationTypeRepository educationQualificationTypeRepo, IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepo,
            IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo, IEmployeeExperienceRepository employeeExperienceRepo,
            IAccessRepository accessRepo, IAccessTypeRepository accessTypeRepo, IRoleRepository roleRepo, IModuleRepository moduleRepo, ISubModuleRepository subModuleRepo,
            IActionAccessRepository actionAccessRepo, IActionRepository actionRepo, IStudentAggregateRepository studentAggregateRepo, IGrievanceRepository grievanceRepo,

            IStandardRepository standardRepo, ISectionRepository sectionRepo, IBoardRepository boardRepo, IToDoRepository todoRepo, ISupportRepository suppoRepository,
            IErpBugRepository erpBugRepo, ILogingRepository logingRepo, IOrganizationAcademicRepository organizationAcademicRepository,
            IHttpContextAccessor httpContextAccessor /*commonsqlquery commonsqlquery*/
           )

        {
            employeeDocumentAttachmentRepository = employeeDocumentAttachmentRepo;
            todoRepository = todoRepo;
            employeeRepository = employeeRepo;
            departmentRepository = departmentRepo;
            bloodGroupRepository = bloodGroupRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            addressTypeRepository = addressTypeRepo;
            cityRepository = cityRepo;
            stateRepository = stateRepo;
            countryRepository = countryRepo;
            designationLevelRepository = designationLevelRepo;
            designationRepository = designationRepo;
            groupRepository = groupRepo;
            organizationRepository = organizationRepo;
            religionTypeRepository = religionTypeRepo;
            employeeDesignationRepository = employeeDesignationRepo;
            addressRepository = addressRepo;
            employeeAddressRepository = employeeAddressRepo;
            bankAccountRepository = bankAccountRepo;
            bankRepository = bankRepo;
            bankBranchRepository = bankBranchRepo;
            employeeEducationRepository = employeeEducationRepo;
            educationQualificationRepository = educationQualificationRepo;
            educationQualificationTypeRepository = educationQualificationTypeRepo;
            employeeRequiredDocumentRepository = employeeRequiredDocumentRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            employeeExperienceRepository = employeeExperienceRepo;
            accessRepository = accessRepo;
            accessTypeRepository = accessTypeRepo;
            roleRepository = roleRepo;
            moduleRepository = moduleRepo;
            subModuleRepository = subModuleRepo;
            actionAccessRepository = actionAccessRepo;
            actionRepository = actionRepo;
            studentAggregateRepository = studentAggregateRepo;
            grievanceRepository = grievanceRepo;
            standardRepository = standardRepo;
            sectionRepository = sectionRepo;
            boardRepository = boardRepo;
            supportRepository = suppoRepository;
            erpBugRepository = erpBugRepo;
            logingRepository = logingRepo;
            this.organizationAcademicRepository = organizationAcademicRepository;
            _httpContextAccessor = httpContextAccessor;
            //_session = _httpContextAccessor.HttpContext.Session;
            //commonsql = commonsqlquery;
        }

        public List<SupportRequestCls> GetallSupportFormail()
        {
            try
            {
                var employees = employeeRepository.GetAllEmployeewithleft();
                var req = supportRepository.GetAllSupportRequest();
                var priority = supportRepository.GetAllPriority();
                var departments = GetDepartments();
                var res = (from a in req
                           join e in employees on a.InsertedId equals e.ID
                           join c in priority on a.PriorityID equals c.ID
                           select new SupportRequestCls
                           {
                               ID = a.ID,
                               Status = a.Status,
                               //InsertedDate = a.InsertedDate.Value.ToString("dd-MMM-yyyy"),
                               Description = a.Description,
                               DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                               //assignedto = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).Select(m => m.FirstName + " " + m.LastName).FirstOrDefault() : "",
                               //assignedby = e.FirstName + " " + e.LastName,
                               profile = a.InsertedId != 0 ? employees.Where(m => m.ID == a.InsertedId).FirstOrDefault().EmailId : "",
                               PriorityName = c.Name,
                               department = a.DepartmentID != 0 ? departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name : "",
                               assigntime = a.InsertedDate.ToString("hh:mm tt"),
                               InsertedID = a.InsertedId,
                               // AssignDate = a.AssignedDate,
                               //AssignToID = a.AssignedId,
                               // DueDt = a.DueDate,
                               ModifiedDate = a.ModifiedDate,
                               //  parentID = a.ParentId,
                               DepartmentID = a.DepartmentID,
                               PriorityID = a.PriorityID
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }



        public string GetOrgGroupName(long employeeId)
        {
            return (from a in employeeDesignationRepository.GetAllEmployeeDesignationsByEmployeeId(employeeId).ToList()
                    join b in designationRepository.GetAllDesignation().ToList()
                       on a.DesignationID equals b.ID
                    join c in departmentRepository.GetAllDepartment().ToList()
                   on b.DepartmentID equals c.ID
                    select new
                    {
                        Name = c.GroupID == 0 ? organizationRepository.GetOrganizationById(c.OrganizationID).Name :
                        groupRepository.GetGroupById(c.GroupID).Name
                    }
                   ).Select(x => x.Name).FirstOrDefault();

        }
        public List<supportattachmentclass> GetSupportAttachments(long id, long empid)
        {
            try
            {
                var attachments = supportRepository.GetAllAttachment();
                //var employees = GetEmployeesForTasks(empid);
                var employees = employeeRepository.GetAllEmployee().ToList();
                var res = (from a in attachments
                           join b in employees on a.InsertedId equals b.ID
                           where a.SupportRequestID == id
                           select new supportattachmentclass
                           {
                               AttachmentName = a.Name,
                               Attachmenturl = a.Path,
                               employeeid = a.InsertedId,
                               employeeName = empid == a.InsertedId ? "Me" : b.FirstName + " " + b.LastName,
                               SupportRequestID = a.SupportRequestID,
                               id = a.ID,
                               insertedOn = a.InsertedDate,
                               modifiedOn = a.ModifiedDate
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }


        public string GetEmployeeNameByEmployeeId(long employeeId, long loginEmployeeId)
        {

            return employeeId == loginEmployeeId ? "Me" : employeeRepository.GetEmployeeFullNameById(employeeId);

        }
        public List<supportcomments> GetSupportComments(long id, long empid)
        {
            try
            {
                var comments = supportRepository.GetAllSupportComment();
                //var employees = GetEmployeesForTasks(empid);
                var employees = employeeRepository.GetAllEmployee().ToList();
                var res = (from a in comments
                           join b in employees on a.InsertedId equals b.ID
                           where a.SupportRequestID == id
                           select new supportcomments
                           {
                               id = a.ID,
                               Name = a.Name,
                               EmployeeName = empid == a.InsertedId ? "Me" : b.FirstName + " " + b.LastName,
                               EmployeeID = a.InsertedId,
                               SupportRequestID = a.SupportRequestID,
                               postedON = a.InsertedDate.ToString("dd-MMM-yyyy hh:mm tt"),
                               profile = b.Image,
                               insertedOn = a.InsertedDate,
                               modifiedOn = a.ModifiedDate
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<SupportTabularReportCls> GetAllReportDetails(long? datetype, DateTime? f_date, DateTime? t_date, string m_date, long? supporttype_ddl, long accessId, long roleId, long userId)

        {
            try
            {
                var request = supportRepository.GetAllSupportRequest();

                var supporttype = supportRepository.GetAllSupportType();
                var allempdept = GetDepartmentsByEmployeeID(userId);
                List<CommonMasterModel> mast = new List<CommonMasterModel>();
                foreach (var a in allempdept)
                {
                    var departments = GetallchilddepartmentbasedonParent(a.id);
                    mast.AddRange(departments);
                }
                var dept = mast.Distinct();
                if (checkaccessavailable("Report", accessId, "Support Type", "Support", roleId) == true)
                {
                    var alldept = departmentRepository.GetAllDepartment();
                    dept = (from a in alldept
                            select new CommonMasterModel
                            {
                                ID = a.ID,
                                Name = a.Name
                            }).ToList();
                }

                if (supporttype_ddl != null && supporttype_ddl != 0)
                {
                    request = request.Where(a => a.SupportTypeID == supporttype_ddl.Value).ToList();
                }
                if (f_date != null && t_date != null)
                {
                    request = request.Where(a => a.InsertedDate.Date >= f_date.Value.Date && a.InsertedDate.Date <= t_date.Value.Date).ToList();
                }
                if (m_date != null && m_date != "")
                {
                    DateTime m_dt = Convert.ToDateTime(m_date);
                    request = request.Where(a => a.InsertedDate.Month == m_dt.Month && a.InsertedDate.Year == m_dt.Year).ToList();
                }
                var res = (from a in request
                           join b in supporttype on a.SupportTypeID equals b.ID
                           join c in dept on b.DepartmentID equals c.ID
                           select new supportReportDashboard
                           {
                               ID = a.ID,
                               DepartmentID = c.ID,
                               DepartmentName = c.Name,
                               StatusID = a.StatusID,
                               StatusName = a.StatusName,
                               SupportTypeID = a.SupportTypeID,
                               SendApproval = a.SendApproval
                           }).ToList();

                var result = (from a in res
                              group a by a.DepartmentID into g
                              select new SupportTabularReportCls
                              {
                                  DepartmentID = g.FirstOrDefault().DepartmentID,
                                  DepartmentName = g.FirstOrDefault().DepartmentName,
                                  Raise = g.Count(),
                                  Resolve = g.Where(x => x.StatusName == "COMPLETED").ToList().Count(),
                                  Pending = g.Where(x => x.StatusName == "PENDING").ToList().Count(),
                                  Reject = g.Where(x => x.StatusName == "REJECTED").ToList().Count(),
                                  Verify = g.Where(x => x.StatusName == "VERIFIED").ToList().Count(),
                                  Onhold = g.Where(x => x.StatusName == "ON-HOLD").ToList().Count(),
                                  Inprogress = g.Where(x => x.StatusName == "IN-PROGRESS").ToList().Count(),
                                  Approve = g.Where(x => x.StatusName == "APPROVED").ToList().Count(),
                                  SendApproval = g.Where(x => x.SendApproval == true).ToList().Count(),
                                  ApprovalPending = g.Where(x => x.StatusName == "APPROVAL PENDING").ToList().Count(),
                                  MgtReject = g.Where(x => x.StatusName == "MANAGEMENT REJECTED").ToList().Count(),
                                  RaiseP = (int)Math.Round((double)(100 * (g.Count())) / g.Count()),
                                  SendApprovalP = (int)Math.Round((double)(100 * (g.Where(x => x.SendApproval == true).ToList().Count())) / (g.Count())),
                                  ResolveP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "COMPLETED").ToList().Count())) / (g.Count())),
                                  RejectP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "REJECTED").ToList().Count())) / (g.Count())),
                                  VerifyP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "VERIFIED").ToList().Count())) / (g.Count())),
                                  OnholdP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "ON-HOLD").ToList().Count())) / (g.Count())),
                                  PendingP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "PENDING").ToList().Count())) / (g.Count())),
                                  InprogressP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "IN-PROGRESS").ToList().Count())) / (g.Count())),
                                  ApproveP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "APPROVED").ToList().Count())) / (g.Count())),
                                  ApprovalPendingP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "APPROVAL PENDING").ToList().Count())) / (g.Count())),
                                  MgtRejectP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "MANAGEMENT REJECTED").ToList().Count())) / (g.Count()))
                              }).ToList();



                return result;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public IEnumerable<EmployeeAdressCls> GetAllAddressByUsingEmployeeId(long id)
        {
            try
            {

                var employeeAddresses = employeeAddressRepository.GetEmployeeAddressByEmployeeID(id);
                var address = addressRepository.GetAllAddress();
                var addressType = addressTypeRepository.GetAllAddressType();
                var city = cityRepository.GetAllCity();
                var state = stateRepository.GetAllState();
                var country = countryRepository.GetAllCountries();

                IEnumerable<EmployeeAdressCls> res = (from a in employeeAddresses
                                                      join b in address on a.AddressID equals b.ID
                                                      join c in addressType on b.AddressTypeID equals c.ID
                                                      join d in city on b.CityID equals d.ID
                                                      join e in state on b.StateID equals e.ID
                                                      join f in country on b.CountryID equals f.ID
                                                      select new EmployeeAdressCls
                                                      {
                                                          AddressLine1 = b.AddressLine1,
                                                          AddressLine2 = b.AddressLine2,
                                                          AddressTypeID = b.AddressTypeID,
                                                          AddressTypeName = c.Name,
                                                          CityID = b.CityID,
                                                          CityName = d.Name,
                                                          CountryID = b.CountryID,
                                                          CountryName = f.Name,
                                                          ID = a.ID,
                                                          PostalCode = b.PostalCode,
                                                          StateID = b.StateID,
                                                          StateName = e.Name,
                                                          ModifiedDate = a.ModifiedDate,

                                                          InsertedDate = a.InsertedDate

                                                      }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeDesignationCls> GetAllDesignationByUsingEmployeeId(long id)
        {
            try
            {
                var employeeDesignation = employeeDesignationRepository.GetAllEmployeeDesignationsByEmployeeId(id);
                var designation = designationRepository.GetAllDesignation();
                // var designationLevel = designationLevelRepository.GetAllDesignationLevel();
                var department = departmentRepository.GetAllDepartment();
                var groups = groupRepository.GetAllGroup();
                var organization = organizationRepository.GetAllOrganization();

                IEnumerable<EmployeeDesignationCls> res = (from a in employeeDesignation
                                                           join b in designation on a.DesignationID equals b.ID
                                                           //join c in designationLevel on b.DesignationLevelID equals c.ID
                                                           join d in department on b.DepartmentID equals d.ID
                                                           //  join e in organization on d.OrganizationID equals e.ID
                                                           // join f in groups on e.GroupID equals f.ID
                                                           select new EmployeeDesignationCls
                                                           {
                                                               DepartmentID = b.DepartmentID,
                                                               DepartmentName = d.Name,
                                                               DesignationId = a.DesignationID,
                                                               //  DesignationLevelID = b.DesignationLevelID,
                                                               //  DesignationLevelName = c.Name,
                                                               DesignationName = b.Name,
                                                               EmployeeID = id,
                                                               GroupID = d.GroupID,
                                                               GroupName = d.GroupID > 0 ?
                                                               groups.Where(x => x.ID == d.GroupID).FirstOrDefault().Name : d.OrganizationID > 0 ?
                                                               groups.Where(x => x.ID == organization.Where(y => y.ID == d.OrganizationID).
                                                               FirstOrDefault().GroupID).FirstOrDefault().Name : "",
                                                               ID = a.ID,
                                                               ModifiedDate = a.ModifiedDate,
                                                               OrganizationID = d.OrganizationID,
                                                               OrganizationName = d.OrganizationID > 0 ?
                                                               organization.Where(x => x.ID == d.OrganizationID).FirstOrDefault().Name : ""
                                                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeDesignationCls> GetAllDeptwithEmployee()
        {
            try
            {
                var employeeDesignation = employeeDesignationRepository.GetAllEmployeeDesignations();
                var designation = designationRepository.GetAllDesignation();
                // var designationLevel = designationLevelRepository.GetAllDesignationLevel();
                var department = departmentRepository.GetAllDepartment();
                var groups = groupRepository.GetAllGroup();
                var organization = organizationRepository.GetAllOrganization();

                IEnumerable<EmployeeDesignationCls> res = (from a in employeeDesignation
                                                           join b in designation on a.DesignationID equals b.ID
                                                           //join c in designationLevel on b.DesignationLevelID equals c.ID
                                                           join d in department on b.DepartmentID equals d.ID
                                                           //  join e in organization on d.OrganizationID equals e.ID
                                                           // join f in groups on e.GroupID equals f.ID
                                                           select new EmployeeDesignationCls
                                                           {
                                                               DepartmentID = b.DepartmentID,
                                                               DepartmentName = d.Name,
                                                               DesignationId = a.DesignationID,
                                                               DesignationName = b.Name,
                                                               EmployeeID = a.EmployeeID,
                                                               GroupID = d.GroupID,
                                                               ID = a.ID,
                                                               ModifiedDate = a.ModifiedDate,
                                                               OrganizationID = d.OrganizationID
                                                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeBankCls> GetAllBankByUsingEmployeeId(long id)
        {
            try
            {
                var employeeBank = bankAccountRepository.GetEmployeeBankAccountByEmployeeID(id);
                var bank = bankRepository.GetAllBank();
                var bankbranch = bankBranchRepository.GetAllBankBranch();

                IEnumerable<EmployeeBankCls> res = (from a in employeeBank
                                                    join b in bank on a.BankNameID equals b.ID
                                                    select new EmployeeBankCls
                                                    {
                                                        AccountHolderName = a.AccountHolderName,
                                                        AccountNumber = a.AccountNumber,
                                                        BankNameID = a.BankNameID,
                                                        BankBranchName = a.BankBranchName,
                                                        IfscCode = a.IfscCode,
                                                        BankName = b.Name,
                                                        EmployeeID = a.EmployeeID,
                                                        BankTypeID = a.BankTypeID,
                                                        ID = a.ID,
                                                        ModifiedDate = a.ModifiedDate,
                                                        InsertedDate = a.InsertedDate
                                                    }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeEducationCls> GetAllEducationByUsingEmployeeId(long id)
        {
            try
            {
                var employeeEducation = employeeEducationRepository.GetAllEmployeeEducationsByEmployeeId(id);
                var qualifications = educationQualificationRepository.GetAllEducationQualification();
                var qualificationTypes = educationQualificationTypeRepository.GetAllEducationQualificationType();

                IEnumerable<EmployeeEducationCls> res = (from a in employeeEducation
                                                         join b in qualifications on a.EducationQualificationID equals b.ID
                                                         join c in qualificationTypes on b.EducationQualificationTypeID equals c.ID
                                                         select new EmployeeEducationCls
                                                         {
                                                             EducationQualificationID = a.EducationQualificationID,
                                                             EducationQualificationName = b.Name,
                                                             EducationQualificationTypeID = b.EducationQualificationTypeID,
                                                             EducationQualificationTypeName = c.Name,
                                                             EmployeeID = a.EmployeeID,
                                                             ID = a.ID,
                                                             ModifiedDate = a.ModifiedDate,
                                                             InsertedDate = a.InsertedDate,
                                                             College = a.College,
                                                             University = a.University,
                                                             FromYear = a.FromYear,
                                                             ToYear = a.ToYear
                                                         }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public bool employeeRequiredDocument(long empId, long typeId, long subtypeId)
        {
            EmployeeRequiredDocument employeeRequired = employeeRequiredDocumentRepository.GetAllEmployeeRequiredDocuments().Where(a => a.DocumentTypeID == typeId && a.DocumentSubTypeID == subtypeId && a.EmployeeID == empId).FirstOrDefault();
            if (employeeRequired == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public List<EmployeeDocumentDetailsCls> GetDocumentDetails(long id)
        {
            try
            {
                List<EmployeeDocumentDetailsCls> documentDetailsCls = (from a in employeeRequiredDocumentRepository.GetAllEmployeeRequiredDocuments().Where(a => a.EmployeeID == id).Select(a => a.DocumentTypeID).Distinct().ToList()
                                                                       select new EmployeeDocumentDetailsCls
                                                                       {
                                                                           ID = a,
                                                                           DocumentTypeName = documentTypeRepository.GetDocumentTypeById(a).Name,
                                                                           // requiredDocumentCls = GetAllDocumentByUsingEmployeeId(id, a)
                                                                       }).ToList();
                return documentDetailsCls;
            }
            catch
            {
                return null;
            }
        }
        public List<EmployeeRequiredDocumentCls> GetAllDocumentByUsingEmployeeId(long id)
        {
            try
            {
                var employeeEducation = employeeRequiredDocumentRepository.GetAllEmployeeRequiredDocuments().Where(a => a.EmployeeID == id).ToList();
                var documentType = documentTypeRepository.GetAllDocumentTypes();
                var documentSubType = documentSubTypeRepository.GetAllDocumentSubTypes();
                var documents = employeeDocumentAttachmentRepository.GetAllEmployeeDocumentAttachments();

                List<EmployeeRequiredDocumentCls> res = (from a in employeeEducation
                                                         join b in documentType on a.DocumentTypeID equals b.ID
                                                         join c in documentSubType on a.DocumentSubTypeID equals c.ID
                                                         select new EmployeeRequiredDocumentCls
                                                         {
                                                             ID = a.ID,
                                                             totaldocument = documents.Where(m => m.EmployeeRequiredDocumentID == a.ID).ToList().Count,

                                                             DocumentSubTypeID = a.DocumentSubTypeID,
                                                             DocumentSubTypeName = c.Name,
                                                             DocumentTypeID = a.DocumentTypeID,
                                                             DocumentTypeName = b.Name,
                                                             EmployeeID = a.EmployeeID,
                                                             IsApproved = a.IsApproved,
                                                             IsMandatory = a.IsMandatory,
                                                             IsReject = a.IsReject,
                                                             IsVerified = a.IsVerified,
                                                             RelatedID = a.RelatedID,
                                                             VerifiedAccessID = a.VerifiedAccessID,
                                                             VerifiedDate = a.VerifiedDate,
                                                             relatedDetails = GetAllRelatedData(a.EmployeeID, b.Name, a.RelatedID)
                                                         }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public string GetAllRelatedData(long eid, string typeName, long RelatedId)
        {

            if (typeName == "EXPERIENCE")
            {
                EmployeeExperience employeeExperience = employeeExperienceRepository.GetExperienceById(RelatedId);
                return employeeExperience.OrganizationName;
            }
            else if (typeName == "BANK")
            {
                EmployeeBankAccount employeeBankAccount = bankAccountRepository.GetEmployeeBankAccountById(RelatedId);
                return bankRepository.GetBankById(employeeBankAccount.BankNameID).Name;
            }
            else if (typeName == "EDUCATIONAL")
            {
                EmployeeEducation employeeEducation = employeeEducationRepository.GetEmployeeEducationById(RelatedId);
                return educationQualificationRepository.GetEducationQualificationById(employeeEducation.EducationQualificationID).Name;
            }
            else
            {
                return "";
            }
        }

        public IEnumerable<Employeecls> GetAllEmployees()
        {
            try
            {
                var employeedetails = employeeRepository.GetAllEmployeewithleft();
                var nationalityTypes = nationalityTypeRepository.GetAllNationalities();
                var bloodgroup = bloodGroupRepository.GetAllBloodGroup();
                var religionType = religionTypeRepository.GetAllReligionTypes();
                var res = (from a in employeedetails
                               //join b in nationalityTypes on a.NatiobalityID equals b.ID
                               //join c in bloodgroup on a.BloodGroupID equals c.ID
                               //join d in religionType on a.ReligionID equals d.ID
                           select new Employeecls
                           {
                               AlternativeMobile = a.AlternativeMobile,
                               BloodGroupID = a.BloodGroupID,
                               BloodGroupName = (a.BloodGroupID != null && a.BloodGroupID != 0) ?
                                GetBloodGroupName(a.BloodGroupID.Value) : "",
                               DateOfBirth = a.DateOfBirth != null ? a.DateOfBirth : null,
                               EmailId = a.EmailId,
                               EmpCode = a.EmpCode,
                               FirstName = a.FirstName,
                               Gender = (a.Gender != null && a.Gender != "") ? a.Gender : "",
                               ID = a.ID,
                               LandlineNumber = a.LandlineNumber,
                               LastName = a.LastName,
                               profile = a.Image,
                               JoinDate = GetEmployeeActiveTimeLine(a.ID),
                               MiddleName = a.MiddleName,
                               NatiobalityID = a.NatiobalityID,
                               NatiobalityName = (a.NatiobalityID != null && a.NatiobalityID != 0) ? GetNationalityName(a.NatiobalityID.Value) : "",
                               PrimaryMobile = a.PrimaryMobile,
                               ReligionID = a.ReligionID,
                               ReligionName = (a.ReligionID != null && a.ReligionID != 0) ? GetReligionName(a.ReligionID.Value) : "",
                               ModifiedDate = a.ModifiedDate,
                               IsLeft = a.Isleft != null ? a.Isleft.Value : false,
                               LeftDate = a.LeftDate
                           }).ToList();
                return res;
            }
            catch (Exception ex)
            {
                return new List<Employeecls>();
            }

        }

        public string GetBloodGroupName(long id)
        {
            BloodGroup bg = bloodGroupRepository.GetBloodGroupById(id);
            return bg != null ? bg.Name : "";

        }

        public string GetNationalityName(long id)
        {
            NationalityType bg = nationalityTypeRepository.GetNationalityById(id);
            return bg != null ? bg.Name : "";

        }

        public string GetReligionName(long id)
        {
            ReligionType bg = religionTypeRepository.GetReligionTypeById(id);
            return bg != null ? bg.Name : "";

        }

        private DateTime? GetEmployeeActiveTimeLine(long employeeId)
        {
            EmployeeTimeline et = employeeRepository.GetActiveEmployeeTimelineByEmployeeId(employeeId);

            DateTime? jd = et != null ? et.JoiningOn : null;


            return jd;
        }
        public EmployeeAccesscls GetAccessTypeData(long id)
        {
            var accesstype = accessRepository.GetAllAccess().Where(a => a.EmployeeID == id).FirstOrDefault();
            var employee = employeeRepository.GetEmployeeById(id);
            EmployeeAccesscls employeeAccesscls = new EmployeeAccesscls();
            employeeAccesscls.EmployeeID = id;
            employeeAccesscls.roles = roleRepository.GetAllRole();
            employeeAccesscls.AccessID = 0;
            employeeAccesscls.RoleID = 0;
            if (accesstype != null)
            {
                employeeAccesscls.AccessID = accesstype.ID;
                employeeAccesscls.RoleID = accesstype.RoleID;
                employeeAccesscls.Password = accesstype.Password;
                employeeAccesscls.Username = accesstype.Username;

            }

            return employeeAccesscls;
        }

        public EmployeeModuleSubModuleAccess GetModuleSubModuleAccess(long id)
        {
            try
            {
                EmployeeModuleSubModuleAccess od = new EmployeeModuleSubModuleAccess();
                od.ID = id;
                od.modules = (from a in moduleRepository.GetAllModule()
                              select new ModulesCls
                              {
                                  ID = a.ID,
                                  ModuleName = a.Name,
                                  subModules = (from b in subModuleRepository.GetAllSubModule()
                                                where b.ModuleID == a.ID
                                                select new SubModulesDtCls
                                                {
                                                    ID = b.ID,
                                                    SubModuleName = b.Name,
                                                    actionSubModules = (from c in actionRepository.GetAllAction()
                                                                        where c.SubModuleID == b.ID
                                                                        select new ActionSubModuleClass
                                                                        {
                                                                            ID = c.ID,
                                                                            ActionName = c.Name,
                                                                            ActionAccessId = actionAccessRepository.GetAllActionDepartment().Where(d => d.DepartmentID == id && d.ActionID == c.ID).FirstOrDefault() == null ? 0 : actionAccessRepository.GetAllActionDepartment().Where(d => d.DepartmentID == id && d.ActionID == c.ID).FirstOrDefault().ID,
                                                                            IsRequired = actionAccessRepository.GetAllActionDepartment().Where(d => d.DepartmentID == id && d.ActionID == c.ID).FirstOrDefault() == null ? false : true

                                                                        }).ToList()
                                                }).ToList()
                              }).ToList();
                return od;
            }
            catch
            {
                return null;
            }
        }
        public EmployeeModuleSubModuleAccess GetStudentModuleAccess(long roleid, long classid, long studentID)
        {
            try
            {
                var actionrole = actionAccessRepository.GetAllActionRole().Where(a => a.RoleID == 6 && a.StandardID == classid).ToList();
                var modules = moduleRepository.GetAllModule();
                var submodules = subModuleRepository.GetAllSubModule();

                EmployeeModuleSubModuleAccess od = new EmployeeModuleSubModuleAccess();
                od.modules = (from m in actionrole.Select(s => s.ModuleID).ToList().Distinct()
                              join a in modules on m equals a.ID
                              select new ModulesCls
                              {
                                  ID = a.ID,
                                  ModuleName = a.Name,
                                  subModules = (from n in actionrole.Select(s => s.SubModuleID).ToList().Distinct()
                                                join b in subModuleRepository.GetAllSubModule() on n equals b.ID
                                                where b.ModuleID == a.ID
                                                select new SubModulesDtCls
                                                {
                                                    ID = b.ID,
                                                    SubModuleName = b.Name,
                                                    actionSubModules = (from o in actionrole.Select(s => s.ActionID).ToList().Distinct()
                                                                        join c in actionRepository.GetAllAction() on o equals c.ID
                                                                        where c.SubModuleID == b.ID
                                                                        select new ActionSubModuleClass
                                                                        {
                                                                            ID = c.ID,
                                                                            ActionName = c.Name,
                                                                            ActionAccessId = actionAccessRepository.GetAllActionStudent().Where(d => d.RoleID == roleid && d.StudentID == studentID && d.ActionID == c.ID).FirstOrDefault() == null ? 0 : actionAccessRepository.GetAllActionStudent().Where(d => d.RoleID == roleid && d.StudentID == studentID && d.ActionID == c.ID).FirstOrDefault().ID,
                                                                            IsRequired = actionAccessRepository.GetAllActionStudent().Where(d => d.RoleID == roleid && d.StudentID == studentID && d.ActionID == c.ID).FirstOrDefault() == null ? false : true

                                                                        }).ToList()
                                                }).ToList()
                              }).ToList();
                return od;
            }
            catch
            {
                return null;
            }
        }
        public EmployeeModuleSubModuleAccess GetStudentParentModuleAccess(long roleid, long classid)
        {
            try
            {
                EmployeeModuleSubModuleAccess od = new EmployeeModuleSubModuleAccess();
                od.modules = (from a in moduleRepository.GetAllModule()
                              select new ModulesCls
                              {
                                  ID = a.ID,
                                  ModuleName = a.Name,
                                  subModules = (from b in subModuleRepository.GetAllSubModule()
                                                where b.ModuleID == a.ID
                                                select new SubModulesDtCls
                                                {
                                                    ID = b.ID,
                                                    SubModuleName = b.Name,
                                                    actionSubModules = (from c in actionRepository.GetAllAction()
                                                                        where c.SubModuleID == b.ID
                                                                        select new ActionSubModuleClass
                                                                        {
                                                                            ID = c.ID,
                                                                            ActionName = c.Name,
                                                                            ActionAccessId = actionAccessRepository.GetAllActionRole().Where(d => d.RoleID == roleid && d.StandardID == classid && d.ActionID == c.ID).FirstOrDefault() == null ? 0 : actionAccessRepository.GetAllActionRole().Where(d => d.RoleID == roleid && d.StandardID == classid && d.ActionID == c.ID).FirstOrDefault().ID,
                                                                            IsRequired = actionAccessRepository.GetAllActionRole().Where(d => d.RoleID == roleid && d.StandardID == classid && d.ActionID == c.ID).FirstOrDefault() == null ? false : true

                                                                        }).ToList()
                                                }).ToList()
                              }).ToList();
                return od;
            }
            catch
            {
                return null;
            }
        }


        public EmployeeModuleSubModuleAccess GetEmployeeModuleSubModuleAccess(long id, long eid)
        {
            try
            {
                var departments = (from a in employeeDesignationRepository.GetAllEmployeeDesignations().Where(a => a.EmployeeID == eid).ToList()
                                   join b in designationRepository.GetAllDesignation() on a.DesignationID equals b.ID
                                   select new
                                   {
                                       b.DepartmentID
                                   }).ToList();
                EmployeeModuleSubModuleAccess od = new EmployeeModuleSubModuleAccess();
                od.ID = id;
                od.modules = (from a in moduleRepository.GetAllModule()
                              select new ModulesCls
                              {
                                  ID = a.ID,
                                  ModuleName = a.Name,
                                  subModules = (from b in subModuleRepository.GetAllSubModule()
                                                where b.ModuleID == a.ID
                                                select new SubModulesDtCls
                                                {
                                                    ID = b.ID,
                                                    SubModuleName = b.Name,
                                                    actionSubModules = (from c in actionRepository.GetAllAction()
                                                                        where c.SubModuleID == b.ID
                                                                        select new ActionSubModuleClass
                                                                        {
                                                                            ID = c.ID,
                                                                            ActionName = c.Name,
                                                                            ActionAccessId = actionAccessRepository.GetAllActionAccess().Where(d => d.AccessID == id && d.ActionID == c.ID).FirstOrDefault() == null ? 0 : actionAccessRepository.GetAllActionAccess().Where(d => d.AccessID == id && d.ActionID == c.ID).FirstOrDefault().ID,
                                                                            IsRequired = actionAccessRepository.GetAllActionAccess().Where(d => d.AccessID == id && d.ActionID == c.ID).FirstOrDefault() == null ? false : true

                                                                        }).ToList()
                                                }).ToList()
                              }).ToList();

                foreach (var u in departments)
                {
                    var departmentaccess = actionAccessRepository.GetAllActionDepartment().Where(a => a.DepartmentID == u.DepartmentID).ToList();
                    foreach (var item in departmentaccess)
                    {
                        foreach (var a in od.modules)
                        {
                            foreach (var b in a.subModules)
                            {
                                var m = b.actionSubModules.Where(k => k.ID == item.ActionID).FirstOrDefault();
                                if (m != null)
                                {
                                    b.actionSubModules.Remove(m);
                                }
                                if (b.actionSubModules == null)
                                {
                                    a.subModules.Remove(b);
                                }
                            }
                        }
                    }
                }

                return od;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public IEnumerable<GivenAccesscls> givenAccesscls(long id)
        {
            try
            {
                var actionAccesses = actionAccessRepository.GetAllActionAccess().Where(a => a.AccessID == id).ToList();
                var modules = moduleRepository.GetAllModule();
                var subModules = subModuleRepository.GetAllSubModule();
                List<GivenAccesscls> mod = new List<GivenAccesscls>();
                var res = (from a in actionAccesses
                           join b in modules on a.ModuleID equals b.ID
                           select new
                           {
                               ID = b.ID,
                               ModuleName = b.Name,
                           }).ToList().Distinct();
                foreach (var a in res)
                {
                    GivenAccesscls given = new GivenAccesscls();
                    given.ID = a.ID;
                    given.ModuleName = a.ModuleName;
                    given.SubModule = string.Join(",", (from c in actionAccesses
                                                        join d in subModules on c.SubModuleID equals d.ID
                                                        where c.ModuleID == a.ID
                                                        select new
                                                        {
                                                            d.Name
                                                        }).Distinct().Select(m => m.Name).ToArray());
                    mod.Add(given);
                }
                return mod;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public IEnumerable<SidebarAccesscls> sidebarAccesscls(long id)
        {
            try
            {
                var actionAccesses = actionAccessRepository.GetAllActionAccess().Where(a => a.AccessID == id).ToList();
                var modules = moduleRepository.GetAllModule();
                var subModules = subModuleRepository.GetAllSubModule();
                List<SidebarAccesscls> mod = new List<SidebarAccesscls>();
                var res = (from a in actionAccesses
                           join b in modules on a.ModuleID equals b.ID
                           select new
                           {
                               ID = b.ID,
                               ModuleName = b.Name,
                           }).ToList().Distinct();
                foreach (var a in res)
                {
                    SidebarAccesscls given = new SidebarAccesscls();
                    given.ID = a.ID;
                    given.ModuleName = a.ModuleName;
                    given.sidebarSubmodulecls = (from c in actionAccesses
                                                 join d in subModules on c.SubModuleID equals d.ID
                                                 where c.ModuleID == a.ID
                                                 select new SidebarSubmodulecls
                                                 {
                                                     ID = d.ID,
                                                     SubmoduleName = d.Name,
                                                     Url = d.Url
                                                 }).Distinct().ToList();
                    mod.Add(given);
                }
                return mod;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Access CheckLoginDetails(string userName, string Password)
        {
            try
            {
                var res = accessRepository.GetAccessByUserNameAndPassword(userName, Password);
                if (res != null)
                {
                    return res;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }
        public bool checkaccessavailable(string submoduleName, long accessId, string action, string moduleName, long roleId)
        {
            try
            {
                if (roleId == 1)
                {
                    return true;
                }
                else
                {
                    var moduleId = moduleRepository.GetModuleByName(moduleName).ID;
                    var submoduleId = subModuleRepository.GetSubModuleByName(submoduleName).ID;
                    var actionId = actionRepository.GetAllAction().Where(a => a.SubModuleID == submoduleId && a.Name == action).FirstOrDefault().ID;

                    var employeeId = accessRepository.GetAccessById(accessId).EmployeeID;
                    if (roleId == 2 || roleId == 3 || roleId == 4)
                    {
                        var empdesignation = employeeDesignationRepository.GetAllEmployeeDesignationsByEmployeeId(employeeId).ToList();
                        var designation = designationRepository.GetAllDesignation();
                        var departmentaccess = actionAccessRepository.GetAllActionDepartment();

                        var deptres = (from a in empdesignation
                                       join b in designation on a.DesignationID equals b.ID
                                       join c in departmentaccess on b.DepartmentID equals c.DepartmentID
                                       where c.SubModuleID == submoduleId && c.ModuleID == moduleId && c.ActionID == actionId
                                       select c).ToList();



                        if (deptres.Count() == 0)
                        {
                            var access = actionAccessRepository.GetAllActionAccess().Where(a => a.AccessID == accessId && a.SubModuleID == submoduleId && a.ModuleID == moduleId && a.ActionID == actionId).FirstOrDefault();
                            if (access == null)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return true;
                        }
                    }
                    //else if (roleId == 6)
                    //{
                    //    string stdvalue = _session.GetString("ddlstudentid");


                    //    string stdvalue = "ddlstudentid";
                    //    long stdId = Convert.ToInt64(stdvalue);
                    //    var studentclass = commonsql.Get_view_All_Academic_Students().Where(a => a.StudentId == stdId && a.IsAvailable == true).FirstOrDefault();
                    //    var access = actionAccessRepository.GetAllActionRole().Where(a => a.RoleID == 6 && a.StandardID == studentclass.BoardStandardId && a.SubModuleID == submoduleId && a.ModuleID == moduleId && a.ActionID == actionId).FirstOrDefault();

                    //    if (access == null)
                    //    {
                    //        return false;
                    //    }
                    //    else
                    //    {
                    //        return true;
                    //    }
                    //}
                    else if (roleId == 5)
                    {
                        var access = actionAccessRepository.GetAllActionStudent().Where(a => a.RoleID == 5 && a.StudentID == employeeId && a.SubModuleID == submoduleId && a.ModuleID == moduleId && a.ActionID == actionId).FirstOrDefault();
                        if (access == null)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch(Exception e)
            {
                return false;
            }
        }
        public string GetDepartmentbyparentid(long id, long oid)
        {
            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            var a = departmentRepository.GetAllDepartment().Where(m => m.OrganizationID == oid && m.ID == id).FirstOrDefault();
            var departmentlist = departmentRepository.GetAllDepartment().Where(m => m.OrganizationID == oid).ToList();
            string deptname = a.Name;
            Department depts = a;
            while (depts.ParentID > 0)
            {
                depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                deptname = depts.Name + " --> " + deptname;
            }
            return deptname;
        }
        public IEnumerable<CommonMasterModel> GetDepartmentsByOrganisation(long id)
        {
            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            var departmentlist = departmentRepository.GetAllDepartment().Where(m => m.OrganizationID == id).ToList();
            foreach (var a in departmentlist)
            {
                string deptname = a.Name;
                Department depts = a;
                while (depts.ParentID > 0)
                {
                    depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                    deptname = depts.Name + "-->" + deptname;
                }
                CommonMasterModel om = new CommonMasterModel();
                om.ID = a.ID;
                om.Name = deptname;
                dept.Add(om);
            }
            return dept;
        }
        public IEnumerable<CommonMasterModel> GetDepartmentsByGroupID(long id)
        {
            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            var departmentlist = departmentRepository.GetAllDepartment().Where(m => m.GroupID == id).ToList();
            foreach (var a in departmentlist)
            {
                string deptname = a.Name;
                Department depts = a;
                while (depts.ParentID > 0)
                {
                    depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                    deptname = depts.Name + "-->" + deptname;
                }
                CommonMasterModel om = new CommonMasterModel();
                om.ID = a.ID;
                om.Name = deptname;
                dept.Add(om);
            }
            return dept;
        }
        public IEnumerable<CommonMasterModel> GetDepartments()
        {
            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            var departmentlist = departmentRepository.GetAllDepartment().ToList();
            foreach (var a in departmentlist)
            {
                string deptname = a.Name;
                Department depts = a;
                while (depts.ParentID > 0)
                {
                    depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                    deptname = depts.Name + "-->" + deptname;
                }
                CommonMasterModel om = new CommonMasterModel();
                om.ID = a.ID;
                om.Name = deptname;
                om.GroupID = a.GroupID;
                om.organisationID = a.OrganizationID;
                dept.Add(om);
            }
            return dept;
        }
        public IEnumerable<StudentAdressCls> GetAllAddressByUsingStudentId(long id)
        {
            try
            {
                var studentAddresses = studentAggregateRepository.GetAllStudentAddress().Where(a => a.StudentID == id).ToList();
                var address = addressRepository.GetAllAddress();
                var addressType = addressTypeRepository.GetAllAddressType();
                var city = cityRepository.GetAllCity();
                var state = stateRepository.GetAllState();
                var country = countryRepository.GetAllCountries();

                IEnumerable<StudentAdressCls> res = (from a in studentAddresses
                                                     join b in address on a.AddressID equals b.ID
                                                     join c in addressType on b.AddressTypeID equals c.ID
                                                     join d in city on b.CityID equals d.ID
                                                     join e in state on b.StateID equals e.ID
                                                     join f in country on b.CountryID equals f.ID
                                                     select new StudentAdressCls
                                                     {
                                                         AddressLine1 = b.AddressLine1,
                                                         AddressLine2 = b.AddressLine2,
                                                         AddressTypeID = b.AddressTypeID,
                                                         AddressTypeName = c.Name,
                                                         CityID = b.CityID,
                                                         CityName = d.Name,
                                                         CountryID = b.CountryID,
                                                         CountryName = f.Name,
                                                         ID = a.ID,
                                                         PostalCode = b.PostalCode,
                                                         StateID = b.StateID,
                                                         StateName = e.Name,
                                                         ModifiedDate = a.ModifiedDate,
                                                         InsertedDate = a.InsertedDate
                                                     }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<StudentModelClass> GetAllStudent()
        {
            try
            {
                var studentdetails = studentAggregateRepository.GetAllStudent();
                var nationalityTypes = nationalityTypeRepository.GetAllNationalities();
                var bloodgroup = bloodGroupRepository.GetAllBloodGroup();
                var religionType = religionTypeRepository.GetAllReligionTypes();
                var res = (from a in studentdetails
                           join b in nationalityTypes on a.NatiobalityID equals b.ID
                           join c in bloodgroup on a.BloodGroupID equals c.ID
                           join d in religionType on a.ReligionID equals d.ID
                           select new StudentModelClass
                           {
                               BloodGroupID = a.BloodGroupID,
                               BloodGroupName = c.Name,
                               DateOfBirth = a.DateOfBirth,
                               StudentCode = a.StudentCode,
                               FirstName = a.FirstName,
                               Gender = a.Gender,
                               ID = a.ID,
                               LastName = a.LastName,
                               MiddleName = a.MiddleName,
                               NatiobalityID = a.NatiobalityID,
                               NatiobalityName = b.Name,
                               ReligionID = a.ReligionID,
                               ReligionName = d.Name,
                               ModifiedDate = a.ModifiedDate
                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }

        }

        public IEnumerable<StudentParentsCls> GetAllParentsByUsingStudentId(long id)
        {
            try
            {
                var studentAddresses = studentAggregateRepository.GetAllParent().Where(a => a.StudentID == id).ToList();
                var parentRelationshipTypes = studentAggregateRepository.GetAllParentRelationshipType();
                var professionTypes = studentAggregateRepository.GetAllProfessionType();

                IEnumerable<StudentParentsCls> res = (from a in studentAddresses
                                                      join b in parentRelationshipTypes on a.ParentRelationshipID equals b.ID
                                                      join c in professionTypes on a.ProfessionTypeID equals c.ID
                                                      select new StudentParentsCls
                                                      {
                                                          ID = a.ID,
                                                          AlternativeMobile = a.AlternativeMobile,
                                                          EmailId = a.EmailId,
                                                          FirstName = a.FirstName,
                                                          LandlineNumber = a.LandlineNumber,
                                                          LastName = a.LastName,
                                                          MiddleName = a.MiddleName,
                                                          ParentRelationshipID = a.ParentRelationshipID,
                                                          ParentRelationshipName = b.Name,
                                                          PrimaryMobile = a.PrimaryMobile,
                                                          ProfessionTypeID = a.ProfessionTypeID,
                                                          ProfessionTypeName = c.Name,
                                                          StudentID = a.StudentID,
                                                          ModifiedDate = a.ModifiedDate,
                                                          Imagepath = a.Image != null ? Path.Combine("/ODMImages/ParentProfile/", a.Image) : null
                                                      }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<StudentClassDetails> GetAllStudentClassByStudentId(long id)
        {
            try
            {
                var studentstandards = studentAggregateRepository.GetAllStudentClass().Where(a => a.StudentID == id);
                var standards = standardRepository.GetAllStandard();
                var section = sectionRepository.GetAllSection();
                var wings = studentAggregateRepository.GetAllWing();
                var organisations = organizationRepository.GetAllOrganization();
                var boards = GetBoard();
                IEnumerable<StudentClassDetails> res = (from a in studentstandards
                                                        join b in standards on a.StandardID equals b.ID
                                                        join f in boards on b.BoardID equals f.ID
                                                        join c in section on a.SectionID equals c.ID
                                                        join d in wings on a.WingID equals d.ID
                                                        join e in organisations on a.OrganizationID equals e.ID
                                                        select new StudentClassDetails
                                                        {
                                                            ID = a.ID,
                                                            IsCurrent = a.IsCurrent,
                                                            OrganizationID = a.OrganizationID,
                                                            OrganizationName = e.Name,
                                                            SectionID = a.SectionID,
                                                            SectionName = c.Name,
                                                            StandardID = a.StandardID,
                                                            StandardName = b.Name,
                                                            StudentID = a.StudentID,
                                                            WingID = a.WingID,
                                                            WingName = d.Name,
                                                            ModifiedDate = a.ModifiedDate,
                                                            BoardID = b.BoardID,
                                                            BoardName = f.Name
                                                        }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<Organization> GetOrganizations()
        {
            try
            {
                var organisation = organizationRepository.GetAllOrganization().ToList();
                return organisation;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<Standard> GetStandard()
        {
            try
            {
                var Standard = standardRepository.GetAllStandard().ToList();
                return Standard;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<Section> GetSection()
        {
            try
            {
                var Section = sectionRepository.GetAllSection().ToList();
                return Section;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<Wing> GetWing()
        {
            try
            {
                var Wing = studentAggregateRepository.GetAllWing().ToList();
                return Wing;
            }
            catch (Exception e1)
            {
                return null;
            }

        }
        //public List<Schoolwingcls> GetSchoolWing()
        //{
        //    try
        //    {
        //        var schoolwings = studentAggregateRepository.GetAllSchoolWing();
        //        var organisations = GetOrganizations();
        //        var Wing = GetWing();
        //        var res = (from a in schoolwings
        //                   join c in Wing on a.WingID equals c.ID
        //                   //join b in organisations on a.OrganisationID equals b.ID
        //                   select new Schoolwingcls
        //                   {
        //                       ID = a.ID,
        //                       OrganisationID = a.OrganisationID,
        //                       OrganizationAcademicID = a.OrganizationAcademicID,
        //                       OrganisationIDByAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.OrganizationAcademicID).Select(d => d.OrganizationId).FirstOrDefault(),
        //                       OrganisationNameByAcademic = organizationRepository.GetAllOrganization().Where(e => e.ID == organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.OrganizationAcademicID).Select(d => d.OrganizationId).FirstOrDefault()).Select(e => e.Name).FirstOrDefault(),
        //                       WingID = a.WingID,

        //                       //OrganisationName = b.Name,
        //                       wingName = c.Name,
        //                       ModifiedDate = a.ModifiedDate,
        //                       Status = a.Status,
        //                   }).ToList();
        //        return res;
        //    }
        //    catch (Exception e1)
        //    {
        //        return null;
        //    }
        //}

        public List<Board> GetBoard()
        {
            try
            {
                var board = boardRepository.GetAllBoard().ToList();
                return board;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public bool Checkuser(string username)
        {
            var res = accessRepository.GetAllAccess().Where(a => a.Username == username).ToList().Count();
            if (res > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public IEnumerable<EmployeeAccesscls> GetStudentAccessDetails()
        {
            try
            {
                var access = accessRepository.GetAllAccess();
                var roles = roleRepository.GetAllRole();
                IEnumerable<EmployeeAccesscls> res = (from a in access
                                                      join b in roles on a.RoleID equals b.ID
                                                      select new EmployeeAccesscls
                                                      {
                                                          AccessID = a.ID,
                                                          EmployeeID = a.EmployeeID,
                                                          RoleID = a.RoleID,
                                                          RoleName = b.Name,
                                                          Password = a.Password,
                                                          Username = a.Username,
                                                          ModifiedDate = a.ModifiedDate
                                                      }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }

        }

        public List<MyGrievanceClass> GetallMyGrievance(long id, long roleId)
        {
            try
            {
                var grievancetype = grievanceRepository.GetAllGrievanceType();
                var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievance = grievanceRepository.GetAllGrievance();
                var grievanceTimelines = grievanceRepository.GetAllGrievanceTimeline();
                var roles = roleRepository.GetAllRole();
                List<MyGrievanceClass> res = (from a in grievance
                                              join b in grievancetype on a.GrievanceTypeID equals b.ID
                                              join d in roles on a.GrievanceCategoryID equals d.ID
                                              join e in grievancestatus on a.GrievanceStatusID equals e.ID
                                              select new MyGrievanceClass
                                              {
                                                  Description = a.Description,
                                                  GrievanceCategoryID = a.GrievanceCategoryID,
                                                  GrievanceCategory = d.Name,
                                                  GrievancePriorityID = a.GrievancePriorityID,
                                                  Priority = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.ID).FirstOrDefault().Name,
                                                  GrievanceRelatedId = a.GrievanceRelatedId,
                                                  GrievanceStatusID = a.GrievanceStatusID,
                                                  status = e.Name,
                                                  GrievanceTypeID = a.GrievanceTypeID,
                                                  GrievanceTypeName = b.Name,
                                                  ID = a.ID,
                                                  InsertedDate = a.InsertedDate,
                                                  Code = a.Code,
                                                  ModifiedDate = a.ModifiedDate
                                              }).ToList();

                res = res.Where(a => a.GrievanceCategoryID == roleId && a.GrievanceRelatedId == id).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public List<MyGrievanceClass> GetallMyGrievance()
        {
            try
            {
                var grievancetype = grievanceRepository.GetAllGrievanceType();
                var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievance = grievanceRepository.GetAllGrievance();
                var grievanceTimelines = grievanceRepository.GetAllGrievanceTimeline();
                var roles = roleRepository.GetAllRole();
                List<MyGrievanceClass> res = (from a in grievance
                                              join b in grievancetype on a.GrievanceTypeID equals b.ID
                                              join d in roles on a.GrievanceCategoryID equals d.ID
                                              join e in grievancestatus on a.GrievanceStatusID equals e.ID
                                              select new MyGrievanceClass
                                              {
                                                  Description = a.Description,
                                                  GrievanceCategoryID = a.GrievanceCategoryID,
                                                  GrievanceCategory = d.Name,
                                                  GrievancePriorityID = a.GrievancePriorityID,
                                                  Priority = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.ID).FirstOrDefault().Name,
                                                  GrievanceRelatedId = a.GrievanceRelatedId,
                                                  GrievanceStatusID = a.GrievanceStatusID,
                                                  status = e.Name,
                                                  GrievanceTypeID = a.GrievanceTypeID,
                                                  GrievanceTypeName = b.Name,
                                                  ID = a.ID,
                                                  InsertedDate = a.InsertedDate,
                                                  Code = a.Code,
                                                  ModifiedDate = a.ModifiedDate
                                              }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public string GetGrievancecode(long roleid)
        {
            try
            {
                var grievance = grievanceRepository.GetactiveinactiveGrievances().Where(a => a.GrievanceCategoryID == roleid).ToList();
                long count = 0;
                string code = "";
                if (roleid == 4)
                {
                    code = "E-GV-";
                }
                else if (roleid == 5)
                {
                    code = "S-GV-";
                }
                else if (roleid == 6)
                {
                    code = "P-GV-";
                }
                else
                {
                    code = "O-GV-";
                }
                if (grievance.Count() > 0)
                {
                    count = (grievance.LastOrDefault().ID + 1);
                }
                else
                {
                    count = 1;
                }
                if (count.ToString().Length == 1)
                {
                    code += "0000" + count;
                }
                else if (count.ToString().Length == 2)
                {
                    code += "000" + count;
                }
                else if (count.ToString().Length == 3)
                {
                    code += "00" + count;
                }
                else if (count.ToString().Length == 4)
                {
                    code += "0" + count;
                }
                else
                {
                    code += count;
                }
                return code;
            }
            catch
            {
                return "";
            }
        }

        public List<Employeeddlview> GetManageGrievanceemployee()
        {
            try
            {
                var employees = employeeRepository.GetAllEmployee();
                var submoduleid = subModuleRepository.GetSubModuleByName("Manage Grievance").ID;
                var departmentaccess = actionAccessRepository.GetAllActionDepartment().Where(a => a.SubModuleID == submoduleid).ToList();
                var designations = designationRepository.GetAllDesignation();
                var employeedepartments = employeeDesignationRepository.GetAllEmployeeDesignations();
                List<Employeeddlview> res = (from a in employees
                                             join b in employeedepartments on a.ID equals b.EmployeeID
                                             join c in designations on b.DesignationID equals c.ID
                                             join d in departmentaccess on c.DepartmentID equals d.DepartmentID
                                             select new Employeeddlview
                                             {
                                                 Code = a.EmpCode,
                                                 Id = a.ID,
                                                 Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                                             }).ToList().Distinct().ToList();
                var actionaccess = actionAccessRepository.GetAllActionAccess();
                var access = accessRepository.GetAllAccess();
                List<Employeeddlview> resl = (from a in employees
                                              join b in access on a.ID equals b.EmployeeID
                                              join c in actionaccess on b.ID equals c.AccessID
                                              where c.SubModuleID == submoduleid
                                              select new Employeeddlview
                                              {
                                                  Code = a.EmpCode,
                                                  Id = a.ID,
                                                  Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                                              }).ToList().Distinct().ToList();
                List<Employeeddlview> result = res.Union(resl).ToList().Distinct().ToList();
                return result;
            }
            catch (Exception e1)
            {

                return null;
            }
        }
        public List<Employeeddlview> GetAssignverifyGrievanceemployee()
        {
            try
            {
                var employees = employeeRepository.GetAllEmployee();
                var submoduleid = subModuleRepository.GetSubModuleByName("Assign & Verify Grievance").ID;
                var departmentaccess = actionAccessRepository.GetAllActionDepartment().Where(a => a.SubModuleID == submoduleid).ToList();
                var designations = designationRepository.GetAllDesignation();
                var employeedepartments = employeeDesignationRepository.GetAllEmployeeDesignations();
                List<Employeeddlview> res = (from a in employees
                                             join b in employeedepartments on a.ID equals b.EmployeeID
                                             join c in designations on b.DesignationID equals c.ID
                                             join d in departmentaccess on c.DepartmentID equals d.DepartmentID
                                             select new Employeeddlview
                                             {
                                                 Code = a.EmailId,
                                                 Id = a.ID,
                                                 Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                                             }).ToList().Distinct().ToList();
                var actionaccess = actionAccessRepository.GetAllActionAccess();
                var access = accessRepository.GetAllAccess();
                List<Employeeddlview> resl = (from a in employees
                                              join b in access on a.ID equals b.EmployeeID
                                              join c in actionaccess on b.ID equals c.AccessID
                                              where c.SubModuleID == submoduleid
                                              select new Employeeddlview
                                              {
                                                  Code = a.EmailId,
                                                  Id = a.ID,
                                                  Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                                              }).ToList().Distinct().ToList();
                List<Employeeddlview> result = res.Union(resl).ToList().Distinct().ToList();
                return result;
            }
            catch (Exception e1)
            {

                return null;
            }
        }

        public long AddSuperAdmin(string username, string password)
        {
            long accessId = 0;
            long roleId = roleRepository.CreateSuperAdminRole();

            if (roleId > 0)
            {
                long employeeId = employeeRepository.CreateSuperAdmin(roleId);

                if (employeeId > 0)
                {
                    accessId = accessRepository.CreateSuperAdminAccess(employeeId, username, password, roleId);
                }
            }


            return accessId;
        }
        public int GetRoleCount()
        {
            return roleRepository.GetAllRole().Count();
        }

        //private static List<TaskLayerclass> FillRecursiveTask(List<TaskLayerclass> flatObjects, long parentId)
        //{            
        //    List<TaskLayerclass> recursiveObjects = new List<TaskLayerclass>();
        //    foreach (var a in flatObjects.Where(x => x.ParentTaskID.Equals(parentId)))
        //    {
        //        recursiveObjects.Add(new TaskLayerclass
        //        {
        //            Id = a.Id,
        //            Title = a.Title,
        //            Description = a.Description,
        //            workName = a.workName,
        //            workSectionName = a.workSectionName,
        //            insertedOn = a.insertedOn,
        //            taskPrioritieName = a.taskPrioritieName,
        //            CompletionDate = a.CompletionDate,
        //            TaskStatus = a.TaskStatus,
        //            taskLayerclasses = FillRecursiveTask(flatObjects, a.Id)
        //        });
        //    }
        //    return recursiveObjects;
        //}
        //public IEnumerable<TaskLayerclass> GetallMyTasks(long empid)
        //{
        //    try
        //    {
        //        var tasks = taskAggregateRepository.GetAllTask();
        //        var accesstasks = taskAggregateRepository.GetAllAccessTasks();
        //        var employees = employeeRepository.GetAllEmployee();
        //        var works = taskAggregateRepository.GetAllWork();
        //        var worksection = taskAggregateRepository.GetAllWorkSection();
        //        var completion = taskAggregateRepository.GetAllTaskCompletionDetails();
        //        var statuses = taskAggregateRepository.GetAllTaskStatusDetails();
        //        var res = (from a in tasks
        //                   join b in accesstasks on a.ID equals b.TaskID
        //                   where a.InsertedId==empid && b.EmployeeID==empid && a.TaskType=="OWN"
        //                   select new TaskLayerclass
        //                   {
        //                       Id=a.ID,
        //                       Title=a.Title,
        //                       Description=a.Description,
        //                       workName=a.WorkID==null?"":taskAggregateRepository.GetWorkById(a.WorkID.Value).Title,
        //                       workSectionName=a.WorkSectionID==null?"":taskAggregateRepository.GetWorkSectionById(a.WorkSectionID.Value).Name,
        //                       insertedOn=a.InsertedDate,
        //                       taskPrioritieName=taskAggregateRepository.GetTaskPriorityById(completion.Where(m=>m.TaskId==a.ID).OrderByDescending(m=>m.ID).FirstOrDefault().TaskPriorityID).Name,
        //                       CompletionDate= completion.Where(m => m.TaskId == a.ID).OrderByDescending(m => m.ID).FirstOrDefault().CompletionDate,
        //                       TaskStatus= statuses.Where(m => m.TaskId == a.ID).OrderByDescending(m => m.ID).FirstOrDefault().TaskStatus

        //                   }).ToList();
        //        var e= FillRecursiveTask(res, 0); 
        //        return e;
        //    }
        //    catch 
        //    {
        //        return null;
        //    }
        //}

        public List<commonCls> GetDepartmentsByEmployeeID(long id)
        {
            try
            {
                var departments = GetDepartments();
                var employeedepartments = (from a in GetAllDesignationByUsingEmployeeId(id)
                                           select new { a.DepartmentID }).ToList().Distinct();
                var res = (from a in employeedepartments
                           join b in departments on a.DepartmentID equals b.ID
                           select new commonCls
                           {
                               id = b.ID,
                               name = b.Name,
                               groupid = b.GroupID
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<commonCls> GetSuperadminDepartments()
        {
            try
            {
                var departments = departmentRepository.GetAllDepartment();

                var res = (from a in departments

                           select new commonCls
                           {
                               id = a.ID,
                               name = a.Name,

                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<commonCls> GetEmployeesForTasks(long id)
        {
            try
            {
                var employees = employeeRepository.GetAllEmployee();

                var self = (from a in employees
                            where a.ID == id
                            select new commonCls
                            {
                                id = a.ID,
                                name = "me",
                                profile = a.Image
                            }).ToList();
                var exceptself = (from a in employees
                                  where a.ID != id
                                  select new commonCls
                                  {
                                      id = a.ID,
                                      name = a.FirstName + " " + a.LastName,
                                      profile = a.Image
                                  }).ToList();

                var res = self.Union(exceptself).ToList();

                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<commonCls> GetEmployeesForTaskswithleft(long id)
        {
            try
            {
                var employees = employeeRepository.GetAllEmployeewithleft();

                var self = (from a in employees
                            where a.ID == id
                            select new commonCls
                            {
                                id = a.ID,
                                name = "me",
                                profile = a.Image!=null? a.Image:""
                            }).ToList();
                var exceptself = (from a in employees
                                  where a.ID != id
                                  select new commonCls
                                  {
                                      id = a.ID,
                                      name = (a.FirstName!=null && a.LastName != null) ? a.FirstName + " " + a.LastName:"NA",
                                      profile = a.Image != null ? a.Image : ""
                                  }).ToList();

                var res = self.Union(exceptself).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        #region Task Management
        public List<commonCls> GetEmployeesForTasks(long id, long[] deptid)
        {
            var allempdept = GetDepartmentsByEmployeeID(id);
            List<CommonMasterModel> mast = new List<CommonMasterModel>();
            foreach (var a in allempdept)
            {
                var departments = GetallchilddepartmentbasedonParent(a.id);
                mast.AddRange(departments);
            }
            var det = mast.Distinct();
            if (deptid != null)
            {
                if (deptid.ToList().Count > 0)
                {
                    det = GetDepartments();
                }
            }
            var empdesg = employeeDesignationRepository.GetAllEmployeeDesignations();
            var desg = designationRepository.GetAllDesignation();
            var emp = employeeRepository.GetAllEmployee();

            var res = (from a in emp
                       join b in empdesg on a.ID equals b.EmployeeID
                       join c in desg on b.DesignationID equals c.ID
                       join d in det on c.DepartmentID equals d.ID
                       group a by a.ID into g
                       select new commonCls
                       {
                           id = g.FirstOrDefault().ID,
                           name = g.FirstOrDefault().FirstName + " " + g.FirstOrDefault().LastName,
                           profile = g.FirstOrDefault().Image
                       }).ToList().Distinct().ToList();

            return res;
        }
        public List<subtasklist_cls> GetAllMyParentTask(long employeeid)
        {
            try
            {
                var employees = GetEmployeesForTasks(employeeid);
                var todo = todoRepository.GetAllTodo() == null ? null : todoRepository.GetAllTodo().Where(a => a.AssignedId == employeeid && a.EmployeeID == employeeid).ToList();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = GetDepartments();
                if (todo != null)
                {
                    var res = (from a in todo
                               join b in employees on a.AssignedId equals b.id
                               join e in employees on a.EmployeeID equals e.id
                               join c in priority on a.TodoPriorityID equals c.ID
                               join d in departments on a.DepartmentID equals d.ID
                               select new subtasklist_cls
                               {
                                   todoid = a.ID,
                                   todoname = a.Name,
                                   status = a.Status,
                                   AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                                   tododescription = a.Description,
                                   DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                                   assignedto = b.name,
                                   assignedby = e.name,
                                   profile = b.profile,
                                   priority = c.Name,
                                   department = d.Name,
                                   assigntime = a.AssignedDate.Value.ToString("hh:mm tt")
                               }).ToList();
                    return res;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e1)
            {

                throw;
            }
        }
        public List<subtasklist_cls> GetAssignToOthers(long employeeid, string[] TodoStatusName, long[] TodoPriority)
        {
            try
            {
                var tasks = GetallTasks(employeeid);
                if (TodoStatusName.ToList().Count > 0)
                {
                    tasks = tasks.Where(a => TodoStatusName.Contains(a.status)).ToList();
                }
                if (TodoPriority.ToList().Count > 0)
                {
                    tasks = tasks.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                }
                if (tasks != null)
                {
                    List<subtasklist_cls> res = (from a in tasks
                                                 where a.AssignToID != employeeid && a.AssignToID != 0 && a.AssignByID == employeeid
                                                 select a).ToList();
                    List<subtasklist_cls> parents = res.Where(a => a.parentID == 0).ToList();
                    if (parents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in parents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = res.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    res.Remove(t);
                                }
                            }
                        }
                    }
                    if (res.Count > 0)
                    {
                        //foreach (subtasklist_cls a in res.Where(m => m.subtasks == null).ToList())
                        //{
                        //    if (TodoStatusName != null)
                        //    {
                        //        if (a.status != TodoStatusName)
                        //        {
                        //            res.Remove(a);
                        //        }
                        //    }
                        //    if (TodoPriority != null)
                        //    {
                        //        if (a.priorityID != TodoPriority)
                        //        {
                        //            res.Remove(a);
                        //        }
                        //    }
                        //}
                    }
                    return res;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<subtasklist_cls> GetAssignToMe(long employeeid, string[] TodoStatusName, long[] TodoPriority)
        {
            try
            {
                var tasks = GetallTasks(employeeid);
                if (TodoStatusName.ToList().Count > 0)
                {
                    tasks = tasks.Where(a => TodoStatusName.Contains(a.status)).ToList();
                }
                if (TodoPriority.ToList().Count > 0)
                {
                    tasks = tasks.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                }
                if (tasks != null)
                {
                    List<subtasklist_cls> res = (from a in tasks
                                                 where a.AssignToID == employeeid && a.AssignToID != 0 && a.AssignByID != employeeid
                                                 select a).ToList();
                    List<subtasklist_cls> parents = res.Where(a => a.parentID == 0).ToList();
                    if (parents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in parents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = res.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    res.Remove(t);
                                }
                            }
                        }
                    }

                    if (res.Count > 0)
                    {
                        //foreach (subtasklist_cls a in res.Where(m => m.subtasks == null).ToList())
                        //{

                        //}
                    }
                    return res;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<subtasklist_cls> GetallTasks(long employeeid)
        {
            try
            {
                var employees = GetEmployeesForTaskswithleft(employeeid);
                var todo = todoRepository.GetAllTodo();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = GetDepartments();
                var comments = todoRepository.GetAllTodoComment();
                var res = (from a in todo
                           join e in employees on a.EmployeeID equals e.id
                           join c in priority on a.TodoPriorityID equals c.ID
                           select new subtasklist_cls
                           {
                               todoid = a.ID,
                               todoname = a.Name,
                               status = a.Status,
                               AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                               tododescription = a.Description,
                               DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                               AssignByDepartmentID = a.AssignByDepartmentID,
                               AssignByDepartmentName = (a.AssignByDepartmentID == 0) ? "" : departments.Where(m => m.ID == a.AssignByDepartmentID).FirstOrDefault().Name,
                               assignedto = a.AssignedId != 0 ? employees.Where(m => m.id == a.AssignedId).FirstOrDefault().name : "",
                               assignedby = a.EmployeeID == 0 ? "" : employees.Where(m => m.id == a.EmployeeID).FirstOrDefault().name,
                               profile = a.AssignedId != 0 ? employees.Where(m => m.id == a.AssignedId).FirstOrDefault().profile : "",
                               priority = a.TodoPriorityID == 0 ? "" : priority.Where(m => m.ID == a.TodoPriorityID).FirstOrDefault().Name,
                               department = (a.DepartmentID == 0) ? "" : departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name,
                               assigntime = a.AssignedDate.Value.ToString("hh:mm tt"),
                               AssignByID = a.EmployeeID,
                               AssignDate = a.AssignedDate,
                               AssignToID = a.AssignedId,
                               DueDt = a.DueDate,
                               ModifiedDate = a.ModifiedDate,
                               parentID = a.ParentId,
                               DepartmentID = a.DepartmentID,
                               priorityID = a.TodoPriorityID,
                               completionDt = a.CompletionDate != null ? a.CompletionDate.Value.ToString("dd-MMM-yyyy") : "",
                               VerifyDt = a.VerifiedDate != null ? a.VerifiedDate.Value.ToString("dd-MMM-yyyy") : "",
                               commentcount = comments != null ? comments.Where(m => m.TodoID == a.ID && m.InsertedId != employeeid && m.IsRead == false).ToList().Count() : 0,
                              
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }


        public List<subtasklist_cls> GetallTasksForAndroid(long employeeid)
        {
            try
            {
                var employees = GetEmployeesForTaskswithleft(employeeid);
                var todo = todoRepository.GetAllTodo();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = GetDepartments();
                var comments = todoRepository.GetAllTodoComment();
                var res = (from a in todo
                           join e in employees on a.EmployeeID equals e.id
                           join c in priority on a.TodoPriorityID equals c.ID
                           select new subtasklist_cls
                           {
                               todoid = a.ID,
                               todoname = a.Name,
                               status = a.Status,
                               AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                               tododescription = a.Description,
                               DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                               AssignByDepartmentID = a.AssignByDepartmentID,
                               AssignByDepartmentName = (a.AssignByDepartmentID == 0) ? "" : departments.Where(m => m.ID == a.AssignByDepartmentID).FirstOrDefault().Name,
                               assignedto = a.AssignedId != 0 ? employees.Where(m => m.id == a.AssignedId).FirstOrDefault().name : "",
                               assignedby = a.EmployeeID == 0 ? "" : employees.Where(m => m.id == a.EmployeeID).FirstOrDefault().name,
                               profile = a.AssignedId != 0 ? employees.Where(m => m.id == a.AssignedId).FirstOrDefault().profile : "",
                               priority = a.TodoPriorityID == 0 ? "" : priority.Where(m => m.ID == a.TodoPriorityID).FirstOrDefault().Name,
                               department = (a.DepartmentID == 0) ? "" : departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name,
                               assigntime = a.AssignedDate.Value.ToString("hh:mm tt"),
                               AssignByID = a.EmployeeID,
                               AssignDate = a.AssignedDate,
                               AssignToID = a.AssignedId,
                               DueDt = a.DueDate,
                               ModifiedDate = a.ModifiedDate,
                               parentID = a.ParentId,
                               DepartmentID = a.DepartmentID,
                               priorityID = a.TodoPriorityID,
                               completionDt = a.CompletionDate != null ? a.CompletionDate.Value.ToString("dd-MMM-yyyy") : "",
                               VerifyDt = a.VerifiedDate != null ? a.VerifiedDate.Value.ToString("dd-MMM-yyyy") : "",
                               commentcount = comments != null ? comments.Where(m => m.TodoID == a.ID && m.InsertedId != employeeid && m.IsRead == false).ToList().Count() : 0,
                               subtasks = (from s in todo
                                           where s.ParentId == a.ID
                                           select new subtasklist_cls
                                           {
                                               todoid = s.ID,
                                               todoname = s.Name,
                                               status = s.Status,
                                               AssignedDate = s.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                                               tododescription = s.Description,
                                               DueDate = s.DueDate == null ? "" : s.DueDate.Value.ToString("dd-MMM-yyyy"),
                                               AssignByDepartmentID = s.AssignByDepartmentID,
                                               AssignByDepartmentName = (s.AssignByDepartmentID == 0) ? "" : departments.Where(m => m.ID == s.AssignByDepartmentID).FirstOrDefault().Name,
                                               assignedto = s.AssignedId != 0 ? employees.Where(m => m.id == s.AssignedId).FirstOrDefault().name : "",
                                               assignedby = s.EmployeeID == 0 ? "" : employees.Where(m => m.id == s.EmployeeID).FirstOrDefault().name,
                                               profile = s.AssignedId != 0 ? employees.Where(m => m.id == s.AssignedId).FirstOrDefault().profile : "",
                                               priority = s.TodoPriorityID == 0 ? "" : priority.Where(m => m.ID == s.TodoPriorityID).FirstOrDefault().Name,
                                               department = (s.DepartmentID == 0) ? "" : departments.Where(m => m.ID == s.DepartmentID).FirstOrDefault().Name,
                                               assigntime = s.AssignedDate.Value.ToString("hh:mm tt"),
                                               AssignByID = s.EmployeeID,
                                               AssignDate = s.AssignedDate,
                                               AssignToID = s.AssignedId,
                                               DueDt = s.DueDate,
                                               ModifiedDate = s.ModifiedDate,
                                               parentID = s.ParentId,
                                               DepartmentID = s.DepartmentID,
                                               priorityID = s.TodoPriorityID,
                                               completionDt = s.CompletionDate != null ? s.CompletionDate.Value.ToString("dd-MMM-yyyy") : "",
                                               VerifyDt = s.VerifiedDate != null ? s.VerifiedDate.Value.ToString("dd-MMM-yyyy") : ""
                                           }


                                           ).ToList()
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public List<Cls_TodoTimelines> GetTaskTimelines(long id)
        {
            var timelines = todoRepository.GetAllTodoTimeline().Where(a => a.TodoID == id).ToList();
            var employees = employeeRepository.GetAllEmployee();
            var res = (from a in timelines
                       join b in employees on a.InsertedId equals b.ID
                       select new Cls_TodoTimelines
                       {
                           InsertedDate = a.InsertedDate,
                           RelatedDate = a.RelatedDate,
                           RelatedId = a.RelatedId,
                           Status = a.Status,
                           TodoStatus = a.TodoStatus,
                           RelatedName = a.RelatedId != 0 ? employees.Where(c => c.ID == a.RelatedId).Select(c => new { Name = c.FirstName + " " + c.LastName }).FirstOrDefault().Name : "",
                           insertedBy = b.FirstName + " " + b.LastName
                       }).ToList();
            return res;
        }
        public List<CommonMasterModel> GetallchilddepartmentbasedonParent(long deptID)
        {
            var dept = departmentRepository.GetAllDepartment();
            List<CommonMasterModel> comm = new List<CommonMasterModel>();
            Department comdept = dept.Where(a => a.ID == deptID).FirstOrDefault();
            List<Department> childs = new List<Department>();
            comm.Add(new CommonMasterModel { ID = comdept.ID, Name = comdept.Name });
            for (int i = 0; i < comm.Count(); i++)
            {
                childs = dept.Where(a => a.ParentID == comm.ToList()[i].ID).ToList();
                if (childs.Count > 0)
                {
                    foreach (var df in childs)
                    {
                        comm.Add(new CommonMasterModel { ID = df.ID, Name = df.Name });
                    }
                }
            }
            return comm;
        }
        public viewalltaskclass GetDepartmentLeadAllTask(long accessId, long roleId, long employeeid, string[] TodoStatusName, long[] TodoPriority, long[] emploid, long[] departmentid, long? datetype, DateTime? f_date, DateTime? t_date, string m_date)
        {

            IEnumerable<CommonMasterModel> det = new List<CommonMasterModel>();
            var alltasks = GetallTasksForAndroid(employeeid).Where(a => a.DepartmentID != 0 && a.AssignByDepartmentID != 0).ToList();
            if (checkaccessavailable("View Tasks", accessId, "Management", "Task", roleId) == true)
            {
                det = GetDepartments();
            }
            else if (checkaccessavailable("View Tasks", accessId, "Departmentwise", "Task", roleId) == true)
            {
                var allempdept = GetDepartmentsByEmployeeID(employeeid);
                List<CommonMasterModel> mast = new List<CommonMasterModel>();
                foreach (var a in allempdept)
                {
                    var departments = GetallchilddepartmentbasedonParent(a.id);
                    mast.AddRange(departments);
                }
                det = mast.Distinct();
            }
            else
            {
                // emploid = employeeid;
                det = GetDepartments();
                alltasks = alltasks.Where(a => a.AssignByID == employeeid || a.AssignToID == employeeid).ToList();
            }




            if (TodoStatusName != null && TodoStatusName.ToList().Count > 0 && TodoStatusName.ToList()[0] != "string" )
            {
                alltasks = alltasks.Where(a => TodoStatusName.Contains(a.status)).ToList();
            }
            if (TodoPriority != null &&  TodoPriority.ToList().Count > 0 && TodoPriority.ToList()[0] != 0 )
            {
                alltasks = alltasks.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
            }
            if ( emploid != null &&  emploid.ToList().Count > 0 && emploid.ToList()[0] != 0 )
            {
                alltasks = alltasks.Where(a => emploid.Contains(a.AssignToID) || emploid.Contains(a.AssignByID)).ToList();
            }

            if ( departmentid != null &&  departmentid.ToList().Count > 0 && departmentid.ToList()[0] != 0 )
            {
                alltasks = alltasks.Where(a => departmentid.Contains(a.AssignByDepartmentID) || departmentid.Contains(a.DepartmentID)).ToList();
            }

            if (f_date != null && t_date != null)
            {
                alltasks = alltasks.Where(a => a.AssignDate.Value.Date >= f_date.Value.Date && a.AssignDate.Value.Date <= t_date.Value.Date).ToList();
            }
            if (m_date != null && m_date != "")
            {
                DateTime m_dt = Convert.ToDateTime(m_date);
                alltasks = alltasks.Where(a => a.AssignDate.Value.Month == m_dt.Month && a.AssignDate.Value.Year == m_dt.Year).ToList();
            }
            var tasks1 = (from a in alltasks
                          join b in det on a.DepartmentID equals b.ID
                          select a).ToList();
            var tasks2 = (from a in alltasks
                          join b in det on a.AssignByDepartmentID equals b.ID
                          select a).ToList();
            var tasks = tasks1.Union(tasks2).Distinct();




            Mytaskscls mytaskscls = new Mytaskscls();
            viewalltaskclass viewalltaskclass = new viewalltaskclass();

            if (tasks != null)
            {
                List<subtasklist_cls> res = (from a in tasks
                                             select a).ToList();
                mytaskscls.TodoList = res.ToList();
                //mytaskscls.OnholdList = res.Where(a => a.status == "ON-HOLD").ToList();
                //mytaskscls.InprogressList = res.Where(a => a.status == "IN-PROGRESS").ToList();
                //mytaskscls.CompletedList = res.Where(a => a.status == "COMPLETED").ToList();
                //mytaskscls.VerifiedList = res.Where(a => a.status == "VERIFY").ToList();

                //List<subtasklist_cls> PendingListparents = mytaskscls.PendingList.Where(a => a.parentID == 0).ToList();
                //List<subtasklist_cls> OnholdListparents = mytaskscls.OnholdList.Where(a => a.parentID == 0).ToList();
                //List<subtasklist_cls> InprogressListparents = mytaskscls.InprogressList.Where(a => a.parentID == 0).ToList();
                //List<subtasklist_cls> CompletedListparents = mytaskscls.CompletedList.Where(a => a.parentID == 0).ToList();
                //List<subtasklist_cls> VerifiedListparents = mytaskscls.VerifiedList.Where(a => a.parentID == 0).ToList();

                if (res.Count() > 0)
                {
                    foreach (subtasklist_cls pa in res.ToList())
                    {
                        pa.SubtaskCount = tasks.Where(m => m.parentID == pa.todoid).ToList().Count();

                        var ch = mytaskscls.TodoList.Where(a => a.parentID == pa.todoid).ToList();
                        if (ch.Count > 0)
                        {
                            foreach (var t in ch)
                            {
                                mytaskscls.TodoList.Remove(t);
                            }
                        }
                    }
                }
                //if (OnholdListparents.Count() > 0)
                //{
                //    foreach (subtasklist_cls pa in OnholdListparents.ToList())
                //    {
                //        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                //        var ch = mytaskscls.OnholdList.Where(a => a.parentID == pa.todoid).ToList();
                //        if (ch.Count > 0)
                //        {
                //            foreach (var t in ch)
                //            {
                //                mytaskscls.OnholdList.Remove(t);
                //            }
                //        }
                //    }
                //}
                //if (InprogressListparents.Count() > 0)
                //{
                //    foreach (subtasklist_cls pa in InprogressListparents.ToList())
                //    {
                //        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                //        var ch = mytaskscls.InprogressList.Where(a => a.parentID == pa.todoid).ToList();
                //        if (ch.Count > 0)
                //        {
                //            foreach (var t in ch)
                //            {
                //                mytaskscls.InprogressList.Remove(t);
                //            }
                //        }
                //    }
                //}
                //if (CompletedListparents.Count() > 0)
                //{
                //    foreach (subtasklist_cls pa in CompletedListparents.ToList())
                //    {
                //        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                //        var ch = mytaskscls.CompletedList.Where(a => a.parentID == pa.todoid).ToList();
                //        if (ch.Count > 0)
                //        {
                //            foreach (var t in ch)
                //            {
                //                mytaskscls.CompletedList.Remove(t);
                //            }
                //        }
                //    }
                //}
                //if (VerifiedListparents.Count() > 0)
                //{
                //    foreach (subtasklist_cls pa in VerifiedListparents.ToList())
                //    {
                //        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                //        var ch = mytaskscls.VerifiedList.Where(a => a.parentID == pa.todoid).ToList();
                //        if (ch.Count > 0)
                //        {
                //            foreach (var t in ch)
                //            {
                //                mytaskscls.VerifiedList.Remove(t);
                //            }
                //        }
                //    }
                //}
                //if (res.Count > 0)
                //{
                //    foreach (subtasklist_cls a in res.Where(m => m.subtasks == null).ToList())
                //    {

                //    }
                //}
                viewalltaskclass.tasks = mytaskscls;
                //viewalltaskclass.totaltask = tasks.Count();
                //viewalltaskclass.Completedtask = tasks.Where(a => a.status == "COMPLETED").ToList().Count();
                //viewalltaskclass.inprogresstask = tasks.Where(a => a.status == "IN-PROGRESS").ToList().Count();
                //viewalltaskclass.onholdtask = tasks.Where(a => a.status == "ON-HOLD").ToList().Count();
                //viewalltaskclass.Pendingtask = tasks.Where(a => a.status == "PENDING").ToList().Count();
                //viewalltaskclass.Verifiedtask = tasks.Where(a => a.status == "VERIFIED").ToList().Count();
                return viewalltaskclass;
            }
            else
            {
                viewalltaskclass = new viewalltaskclass();
                viewalltaskclass.tasks = null;
                viewalltaskclass.totaltask = 0;
                viewalltaskclass.Completedtask = 0;
                viewalltaskclass.inprogresstask = 0;
                viewalltaskclass.onholdtask = 0;
                viewalltaskclass.Pendingtask = 0;
                viewalltaskclass.Verifiedtask = 0;
                return viewalltaskclass;
            }

        }

        public List<subtasklist_cls> GetallTasksFormail()
        {
            try
            {
                var employees = employeeRepository.GetAllEmployeewithleft();
                var todo = todoRepository.GetAllTodo();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = GetDepartments();
                var res = (from a in todo
                           join e in employees on a.EmployeeID equals e.ID
                           join c in priority on a.TodoPriorityID equals c.ID
                           select new subtasklist_cls
                           {
                               todoid = a.ID,
                               todoname = a.Name,
                               status = a.Status,
                               AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                               tododescription = a.Description,
                               DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                               assignedto = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).Select(m => m.FirstName + " " + m.LastName).FirstOrDefault() : "",
                               assignedby = e.FirstName + " " + e.LastName,
                               profile = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).FirstOrDefault().EmailId : "",
                               priority = c.Name,
                               department = a.DepartmentID != 0 ? departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name : "",
                               assigntime = a.AssignedDate.Value.ToString("hh:mm tt"),
                               AssignByID = a.EmployeeID,
                               AssignDate = a.AssignedDate,
                               AssignToID = a.AssignedId,
                               DueDt = a.DueDate,
                               ModifiedDate = a.ModifiedDate,
                               parentID = a.ParentId,
                               DepartmentID = a.DepartmentID,
                               priorityID = a.TodoPriorityID
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<subtasklist_cls> GetMyTasks(long employeeid, string[] TodoStatusName, long[] TodoPriority)
        {
            try
            {
                var tasks = GetallTasks(employeeid);
                if (TodoStatusName.ToList().Count > 0)
                {
                    tasks = tasks.Where(a => TodoStatusName.Contains(a.status)).ToList();
                }
                if (TodoPriority.ToList().Count > 0)
                {
                    tasks = tasks.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                }
                //if (tasks != null)
                //{
                //    List<subtasklist_cls> parent = tasks.Where(a => a.AssignByID == employeeid && a.DepartmentID==0 && (a.AssignToID == employeeid || a.AssignToID == 0) && a.parentID == 0).ToList();
                //    foreach (subtasklist_cls a in parent.ToList())
                //    {
                //        a.subtasks = tasks.Where(m => m.parentID == a.todoid).ToList();                        
                //    }
                //    return parent;
                //}
                //return null;


                if (tasks != null)
                {



                    List<subtasklist_cls> res = (from a in tasks
                                                 where a.AssignByID == employeeid && (a.AssignToID == employeeid || a.AssignToID == 0) && a.parentID == 0 && a.DepartmentID == 0
                                                 select a).ToList();
                    List<subtasklist_cls> parents = res.Where(a => a.parentID == 0).ToList();
                    if (parents.Count() > 0)

                    {
                        foreach (subtasklist_cls pa in parents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = res.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    res.Remove(t);
                                }
                            }
                        }
                    }

                    if (res.Count > 0)
                    {
                        //foreach (subtasklist_cls a in res.Where(m => m.subtasks == null).ToList())
                        //{

                        //}
                    }
                    return res;
                }
                else
                {
                    return null;
                }





            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<attachmentclass> GetAttachments(long id, long empid)
        {
            try
            {
                var attachments = todoRepository.GetAllTodoAttachment();
                var employees = GetEmployeesForTasks(empid);
                var res = (from a in attachments
                           join b in employees on a.InsertedId equals b.id
                           where a.TodoID == id
                           select new attachmentclass
                           {
                               AttachmentName = a.AttachmentName,
                               Attachmenturl = a.Attachmenturl,
                               employeeid = a.InsertedId,
                               employeeName = b.name,
                               TodoID = a.TodoID,
                               id = a.ID,
                               insertedOn = a.InsertedDate,
                               modifiedOn = a.ModifiedDate
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }



        public List<todocomments> GetComments(long id, long empid)
        {
            try
            {
                var comments = todoRepository.GetAllTodoComment();
                var employees = GetEmployeesForTaskswithleft(empid);
                var res = (from a in comments
                           join b in employees on a.InsertedId equals b.id
                           where a.TodoID == id
                           select new todocomments
                           {
                               id = a.ID,
                               Description = a.Description,
                               EmployeeName = b.name,
                               EmployeeID = a.InsertedId,
                               TodoID = a.TodoID,
                               postedON = a.InsertedDate.ToString("dd-MMM-yyyy hh:mm aa"),
                               profile = b.profile,
                               insertedOn = a.InsertedDate,
                               modifiedOn = a.ModifiedDate
                           }).Distinct().ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }



        public List<subtasklist_cls> GetSubTaskDetails(long todoparentid, long employeeid)
        {
            try
            {
                var employees = GetEmployeesForTasks(employeeid);
                var todo = todoRepository.GetAllTodo() == null ? null : todoRepository.GetAllTodo().Where(a => a.ParentId == todoparentid).ToList();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = GetDepartments();
                if (todo != null)
                {
                    var res = (from a in todo
                               join b in employees on a.AssignedId equals b.id
                               join e in employees on a.EmployeeID equals e.id
                               join c in priority on a.TodoPriorityID equals c.ID
                               // join d in departments on a.DepartmentID equals d.ID
                               select new subtasklist_cls
                               {
                                   todoid = a.ID,
                                   todoname = a.Name,
                                   status = a.Status,
                                   AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                                   tododescription = a.Description,
                                   DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                                   assignedto = b.name,
                                   assignedby = e.name,
                                   profile = b.profile,
                                   priority = c.Name,
                                   department = a.DepartmentID == 0 ? "" : departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name
                               }).ToList();
                    return res;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e1)
            {

                throw;
            }
        }
        public IEnumerable<TodoPriority> Gettaskpriority()
        {
            try
            {
                var priority = todoRepository.GetAllTodoPriority();
                return priority;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        #endregion


        #region GetAllErpBugs

        public async Task<IReadOnlyList<ErpBugModel>> GetAllErpBugs()
        {
            try
            {
                var erpBugList = erpBugRepository.ListAllAsyncIncludeAll().Result.ToList();
                var moduleList = moduleRepository.GetAllModule();
                var subModuleList = subModuleRepository.GetAllSubModule();
                var actionList = actionRepository.GetAllAction();

                var res = (from a in erpBugList
                           join b in moduleList on a.ModuleID equals b.ID
                           join c in subModuleList on a.SubModuleID equals c.ID
                           join d in actionList on a.ActionID equals d.ID
                           select new ErpBugModel
                           {
                               ID = a.ID,
                               ModuleID = a.ModuleID,
                               ModuleName = b.Name,
                               SubModuleName = c.Name,
                               ActionName = d.Name,
                               ActionID = a.ActionID,
                               Title = a.Title,
                               Description = a.Description

                           }

                          ).ToList();
                return res;
            }
            catch
            {
                return null;
            }

        }


        #endregion

        #region EditErpBugs

        public ErpBugModel EditErpBugs(long id)
        {
            try
            {
                var erpBugList = erpBugRepository.ListAllAsyncIncludeAll().Result.ToList();
                var moduleList = moduleRepository.GetAllModule();
                var subModuleList = subModuleRepository.GetAllSubModule();
                var actionList = actionRepository.GetAllAction();

                var res = (from a in erpBugList
                           join b in moduleList on a.ModuleID equals b.ID
                           join c in subModuleList on a.SubModuleID equals c.ID
                           join d in actionList on a.ActionID equals d.ID
                           select new ErpBugModel
                           {
                               ID = a.ID,
                               ModuleID = a.ModuleID,
                               ModuleName = b.Name,
                               SubModuleID = a.SubModuleID,
                               SubModuleName = c.Name,
                               ActionName = d.Name,
                               ActionID = a.ActionID,
                               Title = a.Title,
                               Description = a.Description

                           }

                          ).FirstOrDefault(w => w.ID == id);
                return res;
            }
            catch
            {
                return null;
            }

        }


        #endregion

    }
}
