﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models; 

namespace WebAPI.Data
{
    public class AdmissionMethod
    {
        protected readonly IOrganizationAcademicRepository organizationAcademicRepository;
        protected readonly IAcademicStandardRepository academicStandardRepository;
        protected readonly IBoardStandardRepository boardStandardRepository;
        protected readonly IStandardRepository standardRepository;
        protected readonly IBoardRepository boardRepository;
        protected readonly IOrganizationRepository organizationRepository;
        protected readonly IAcademicStandardWingRepository academicStandardWingRepository;
        protected readonly IBoardStandardWingRepository boardStandardWingRepository;
        protected readonly IWingRepository wingRepository;
        protected readonly IAdmissionRepository admissionRepository;
        protected readonly IAdmissionStudentRepository admissionStudentRepository;
        protected readonly IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        protected readonly IAdmissionStudentContactRepository admissionStudentContactRepository;
        protected readonly IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        protected readonly IAcademicSessionRepository academicSessionRepository;
        //protected readonly StudentAdmissionMethod admissionStudentMethod;

        public AdmissionMethod(IOrganizationAcademicRepository organizationAcademicRepository, IAcademicStandardRepository academicStandardRepository,
            IBoardStandardRepository boardStandardRepository, IStandardRepository standardRepository, IBoardRepository boardRepository,
            IOrganizationRepository organizationRepository, IAcademicStandardWingRepository academicStandardWingRepository,
            IBoardStandardWingRepository boardStandardWingRepository, IWingRepository wingRepository, IAdmissionRepository admissionRepository,
            IAdmissionStudentRepository admissionStudentRepository, IAdmissionStudentStandardRepository admissionStudentStandardRepository,
            IAdmissionStudentContactRepository admissionStudentContactRepository,
           // StudentAdmissionMethod admissionStudentMethod,
            IAdmissionFormPaymentRepository admissionFormPaymentRepository,
            IAcademicSessionRepository academicSessionRepository)
        {
            //this.admissionStudentMethod = admissionStudentMethod;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.academicStandardRepository = academicStandardRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.standardRepository = standardRepository;
            this.boardRepository = boardRepository;
            this.organizationRepository = organizationRepository;
            this.academicStandardWingRepository = academicStandardWingRepository;
            this.boardStandardWingRepository = boardStandardWingRepository;
            this.wingRepository = wingRepository;
            this.admissionRepository = admissionRepository;
            this.admissionStudentRepository = admissionStudentRepository;
            this.admissionStudentStandardRepository = admissionStudentStandardRepository;
            this.admissionStudentContactRepository = admissionStudentContactRepository;
            this.admissionFormPaymentRepository = admissionFormPaymentRepository;
            this.academicSessionRepository = academicSessionRepository;
        }




        public List<academicClassModal> GetClassByAcademicSession(long AcademicSessionId)
        {

            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                    Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.Active == true).ToList().Select(a => a.ID);

            var boardStandards = academicStandardRepository.ListAllAsync().Result.
                    Where(a => orgAcademics.Contains(a.OrganizationAcademicId) && a.Active == true).ToList().Select(a => a.BoardStandardId);

            var standardsBoards = boardStandardRepository.ListAllAsync().
                    Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
            var standards = (from a in standardsBoards
                             select new academicClassModal
                             {
                                 boardClassName = standardRepository.GetStandardById(a.StandardId).Name,
                                 boardId = a.BoardId,
                                 boardName = boardRepository.GetBoardById(a.BoardId).Name,
                                 standardId = a.StandardId,
                                 boardStandardId = a.ID
                             }
                             ).Distinct().ToList();
            return standards;
        }

        public List<WingOrgModel> GetWingByBoardStandard(long BoardStandardId)
        {
            long BoardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().BoardId;
            long StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().StandardId;

            #region GetOrganizationNames


            var boardStandards = boardStandardRepository.ListAllAsync().Result.
                Where(a => a.Active == true && a.BoardId == BoardId & a.StandardId == StandardId).
                ToList().Select(a => a.ID);


            var organizationAcademics = academicStandardRepository.ListAllAsync().Result.
                Where(a => a.Active == true && boardStandards.Contains(a.BoardStandardId)).
                ToList().Select(a => a.OrganizationAcademicId);

            var OrganizationIds = organizationAcademicRepository.
                   ListAllAsync().Result.Where(a => organizationAcademics.
                   Contains(a.ID) && a.Active == true).ToList().Select(a => a.OrganizationId).Distinct();

            var organisation = organizationRepository.GetAllOrganization().
                Where(a => OrganizationIds.Contains(a.ID));

            #endregion

            List<WingOrgModel> schoolWingNames = new List<WingOrgModel>();

            foreach (var org in organisation)
            {
                var orgAcademicId = organizationAcademicRepository.
                    ListAllAsync().Result.Where(a => a.OrganizationId == org.ID).Select(a => a.ID).FirstOrDefault();
                var academicStandardId = academicStandardRepository.ListAllAsync()
                    .Result.Where(a => a.BoardStandardId == BoardStandardId && a.Active == true &&
                    a.OrganizationAcademicId == orgAcademicId).Select(a => a.ID).FirstOrDefault();

                var boardStandardWingId = academicStandardWingRepository.ListAllAsync().Result
                    .Where(a => a.AcademicStandardId == academicStandardId && a.Active == true).
                    Select(a => a.BoardStandardWingID).FirstOrDefault();

                var AcademicStandardWingId = academicStandardWingRepository.ListAllAsync().Result
                   .Where(a => a.AcademicStandardId == academicStandardId && a.Active == true).
                   Select(a => a.ID).FirstOrDefault();

                var WingId = boardStandardWingRepository.ListAllAsync().Result
                   .Where(a => a.ID == boardStandardWingId && a.Active == true).
                   Select(a => a.WingID).FirstOrDefault();

                var WingName = wingRepository.ListAllAsync().Result
                    .Where(a => a.ID == WingId && a.Active == true).
                    Select(a => a.Name).FirstOrDefault();

                if (WingName != null)
                {
                    WingOrgModel m = new WingOrgModel();
                    m.SchoolId = org.ID;
                    m.WingId = WingId;
                    m.Name = org.Name + "-" + WingName;
                    m.AcademicStandardWindId = AcademicStandardWingId;
                    schoolWingNames.Add(m);

                }
            }
            return schoolWingNames;
        }


        public List<WingOrgModel> GetWingByBoardStandardId(long BoardStandardId, long SessionId)
        {
            long BoardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().BoardId;
            long StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().StandardId;

            #region GetOrganizationNames


            var boardStandards = boardStandardRepository.ListAllAsync().Result.
                Where(a => a.Active == true && a.BoardId == BoardId & a.StandardId == StandardId).
                ToList().Select(a => a.ID);


            var organizationAcademics = academicStandardRepository.ListAllAsync().Result.
                Where(a => a.Active == true && boardStandards.Contains(a.BoardStandardId)).
                ToList().Select(a => a.OrganizationAcademicId);

            var OrganizationIds = organizationAcademicRepository.
                   ListAllAsync().Result.Where(a => organizationAcademics.
                   Contains(a.ID) && a.Active == true && a.AcademicSessionId == SessionId).ToList().Select(a => a.OrganizationId).Distinct();

            var organisation = organizationRepository.GetAllOrganization().
                Where(a => OrganizationIds.Contains(a.ID));

            #endregion

            List<WingOrgModel> schoolWingNames = new List<WingOrgModel>();

            foreach (var org in organisation)
            {
                var orgAcademicId = organizationAcademicRepository.
                    ListAllAsync().Result.Where(a => a.OrganizationId == org.ID && a.AcademicSessionId == SessionId).Select(a => a.ID).FirstOrDefault();
                var academicStandardId = academicStandardRepository.ListAllAsync()
                    .Result.Where(a => a.BoardStandardId == BoardStandardId && a.Active == true &&
                    a.OrganizationAcademicId == orgAcademicId).Select(a => a.ID).FirstOrDefault();

                var boardStandardWingId = academicStandardWingRepository.ListAllAsync().Result
                    .Where(a => a.AcademicStandardId == academicStandardId && a.Active == true).
                    Select(a => a.BoardStandardWingID).FirstOrDefault();

                var AcademicStandardWingId = academicStandardWingRepository.ListAllAsync().Result
                   .Where(a => a.AcademicStandardId == academicStandardId && a.Active == true).
                   Select(a => a.ID).FirstOrDefault();

                var WingId = boardStandardWingRepository.ListAllAsync().Result
                   .Where(a => a.ID == boardStandardWingId && a.Active == true).
                   Select(a => a.WingID).FirstOrDefault();

                var WingName = wingRepository.ListAllAsync().Result
                    .Where(a => a.ID == WingId && a.Active == true).
                    Select(a => a.Name).FirstOrDefault();

                if (WingName != null)
                {
                    WingOrgModel m = new WingOrgModel();
                    m.SchoolId = org.ID;
                    m.WingId = WingId;
                    m.Name = org.Name + "-" + WingName;
                    m.AcademicStandardWindId = AcademicStandardWingId;
                    schoolWingNames.Add(m);

                }
            }
            return schoolWingNames;
        }


        public long SaveStudentRegistration(AdmissionBasicStudentModel stumodel)
        {            
            //OdmErp.ApplicationCore.Entities.Admission adm = new OdmErp.ApplicationCore.Entities.Admission();
            var adm = admissionRepository.GetAllAdmission().Where(a => a.ID == stumodel.admissionId).FirstOrDefault();
            adm.AcademicSessionId = stumodel.academicSessionId;
            adm.AdmissionSlotId = stumodel.admissionSlotId;
            adm.AcademicSourceId = stumodel.academicSourceId;
            //adm.MobileNumber = stumodel.mobile;
            adm.Active = true;
            adm.IsAvailable = true;
            adm.Status = "ACTIVE";
            adm.InsertedDate = DateTime.Now;
            adm.ModifiedDate = DateTime.Now;
            adm.InsertedId = 0;
            adm.ModifiedId = 0;
            adm.FormNumber = stumodel.formNumber;
            admissionRepository.UpdateAdmission(adm);
            long addmisionid = adm.ID;

            AdmissionStudent student = new AdmissionStudent();
            student.AdmissionId = addmisionid;
            student.FirstName = stumodel.firstName;
            student.LastName = stumodel.lastName;
            student.DateOfBirth = null;
            student.Active = true;
            student.IsAvailable = true;
            student.Status = "ACTIVE";
            student.InsertedDate = DateTime.Now;
            student.ModifiedDate = DateTime.Now;
            student.InsertedId = 0;
            student.ModifiedId = 0;
            var std=admissionStudentRepository.AddAsync(student).Result;
            long addmisionstudentid = std.ID;

            AdmissionStudentStandard studentstandard = new AdmissionStudentStandard();
            studentstandard.AcademicStandardWindId = stumodel.academicStandardWindId;
            studentstandard.AdmissionStudentId = addmisionstudentid;
            studentstandard.Active = true;
            studentstandard.IsAvailable = true;
            studentstandard.Status = "ACTIVE";
            studentstandard.InsertedDate = DateTime.Now;
            studentstandard.ModifiedDate = DateTime.Now;
            studentstandard.InsertedId = 0;
            studentstandard.ModifiedId = 0;
            admissionStudentStandardRepository.AddAsync(studentstandard);

            AdmissionStudentContact stucontact = new AdmissionStudentContact();
            stucontact.AdmissionStudentId = addmisionstudentid;
            stucontact.FullName = stumodel.fullName;
            stucontact.EmailId = stumodel.emailId;
            stucontact.Mobile = stumodel.mobile;
            stucontact.Active = true;
            stucontact.IsAvailable = true;
            stucontact.Status = "ACTIVE";
            stucontact.InsertedDate = DateTime.Now;
            stucontact.ModifiedDate = DateTime.Now;
            stucontact.InsertedId = 0;
            stucontact.ModifiedId = 0;
            stucontact.ConatctType = "emergency";
            admissionStudentContactRepository.AddAsync(stucontact);

            return addmisionid;
        }

        public long SavePaymentDetails(PaymentModal payment)
        {
            AdmissionFormPayment formPayment = new AdmissionFormPayment();
            formPayment.InsertedDate = DateTime.Now;
            formPayment.ModifiedDate = DateTime.Now;
            formPayment.Active = true;
            formPayment.Status = "ACTIVE";
            formPayment.IsAvailable = true;
            formPayment.TransactionId = payment.transactionId;
            formPayment.PaymentModeId = payment.paymentModalId;
            formPayment.AdmissionId = payment.admissionId;
            formPayment.PaidAmount = payment.paidAmount;
            formPayment.PaidDate = DateTime.Now;
            formPayment.ReceivedId = payment.receivedId;

            admissionFormPaymentRepository.AddAsync(formPayment);

            return formPayment.ID;
        }

        public BesicRegistrationModal GetBesicRegistration(long admissionid)
        {
            var admission = admissionRepository.GetAdmissionById(admissionid);
            var sessions = academicSessionRepository.GetAllAcademicSession().Where(a => a.ID == admission.AcademicSessionId).FirstOrDefault();
            var admissionStudent = admissionRepository.GetAdmissionStudentById(admissionid);
            var admissionStudentStandard = admissionRepository.GetAdmissionStudentStandardByAdmissionStudentId(admissionStudent.AdmissionId);
            var admissionStudentContact = admissionRepository.GetAdmissionStudentContactByAdmissionStudentId(admissionStudent.AdmissionId);

            BesicRegistrationModal model = new BesicRegistrationModal
            {
                admissionId = admissionid,
                sessionId = admission.AcademicSessionId,
                academicSessionName = sessions.DisplayName,

    
                mobile = admission.MobileNumber,
                firstName = admissionStudent.FirstName,
                lastName = admissionStudent.LastName,
                fullName = admissionStudentContact.FullName,
                EmgencyMob = admissionStudentContact.Mobile,
                email = admissionStudentContact.EmailId,
                academicStandardWingId = admissionStudentStandard.AcademicStandardWindId
            };





            var data = new { };// admissionStudentMethod.GetStudentIdData(model.academicStandardWingId);

            model.school = organizationRepository.GetOrganizationById(admissionStudentStandard.OrganizationId).Name;
            model.board = boardRepository.GetBoardById(admissionStudentStandard.BoardId).Name;
            model.standard = standardRepository.GetStandardById(admissionStudentStandard.StandardId).Name;
            model.wing = wingRepository.GetByIdAsync(admissionStudentStandard.WingId).Result.Name;
            return model;

        }

        public (string boardName, string className,
            string schoolName, string wingName,
            long boardId,
            long standardId,
             long schoolId,
            long wingId
           ) GetAdmissionClassDetails(long acwingId)
        {
            var cadeStWing = academicStandardWingRepository.GetByIdAsync(acwingId).Result;
            var acStd = academicStandardRepository.GetByIdAsync(cadeStWing.AcademicStandardId).Result;
            var orgAcd = organizationAcademicRepository.GetByIdAsync(acStd.OrganizationAcademicId).Result;
            var org = organizationRepository.GetOrganizationById(orgAcd.OrganizationId);
            var stdBoard = boardStandardRepository.GetByIdAsync(acStd.BoardStandardId).Result;
            var wing = wingRepository.GetByIdAsync(cadeStWing.WingID.Value).Result;
            var bstd = boardRepository.GetBoardById(stdBoard.BoardId);
            var standard = standardRepository.GetStandardById(stdBoard.StandardId);

            return (bstd.Name, standard.Name, org.Name, wing.Name,
                bstd.ID, standard.ID, org.ID, wing.ID);
        }

    }
}
