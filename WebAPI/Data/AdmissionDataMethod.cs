﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;

using OdmErp.WebAPI.LeadSquared;
using OdmErp.WebAPI.Models;
using OdmErp.WebAPI.SendInBlue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static OdmErp.WebAPI.LeadSquared.LeadManage;
namespace WebAPI.Data
{
    public class AdmissionDataMethod
    {

        protected readonly IAdmissionRepository admissionRepository;
        protected readonly IOrganizationRepository organizationRepository;
        protected readonly IBoardRepository boardRepository;
        protected readonly IStandardRepository standardRepository;
        protected readonly IWingRepository wingRepository;
        protected readonly IAcademicStandardWingRepository academicStandardWingRepository;
        protected readonly IBoardStandardWingRepository boardStandardWingRepository;
        protected readonly IBoardStandardRepository boardStandardRepository;
        protected readonly IOrganizationAcademicRepository organizationAcademicRepository;
        protected readonly IAcademicStandardRepository academicStandardRepository;
        protected readonly IAcademicSessionRepository academicSessionRepository;

        protected readonly smssend smssend;
        protected readonly SendInBlueEmailManager sendInBlue;

        public AdmissionDataMethod(IAdmissionRepository admissionRepository,
            IOrganizationRepository organizationRepository, IBoardRepository boardRepository, 
            IStandardRepository standardRepository, IWingRepository wingRepository, 
            IAcademicStandardWingRepository academicStandardWingRepository,
            IBoardStandardWingRepository boardStandardWingRepository, IBoardStandardRepository boardStandardRepository,
            IOrganizationAcademicRepository organizationAcademicRepository, IAcademicStandardRepository academicStandardRepository,
            IAcademicSessionRepository academicSessionRepository, smssend smssend, SendInBlueEmailManager sendInBlue)
        {
            this.admissionRepository = admissionRepository;
            this.organizationRepository = organizationRepository;
            this.boardRepository = boardRepository;
            this.standardRepository = standardRepository;
            this.wingRepository = wingRepository;
            this.academicStandardWingRepository = academicStandardWingRepository;
            this.boardStandardWingRepository = boardStandardWingRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.academicStandardRepository = academicStandardRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.smssend = smssend;
            this.sendInBlue = sendInBlue;
        }

        public long SaveAdmission(string mobile, long sourceId)
        {
            var SessionId = admissionRepository.GetCurrentAdmissionSession();
            OdmErp.ApplicationCore.Entities.Admission adm =
                new OdmErp.ApplicationCore.Entities.Admission();
            adm.AcademicSessionId = SessionId.ID;
            adm.AdmissionSlotId = 0;
            adm.AcademicSourceId = sourceId;
            adm.MobileNumber = mobile;
            adm.Active = true;
            adm.IsAvailable = true;
            adm.Status = Admission_Status.REGISTERED;
            adm.InsertedDate = DateTime.Now;
            adm.ModifiedDate = DateTime.Now;
            adm.InsertedId = 0;
            adm.ModifiedId = 0;
            //adm.FormNumber = "";
            //adm.LeadId = Guid.NewGuid();
            adm.LeadReferenceId = Guid.NewGuid();
            admissionRepository.CreateAdmission(adm);
            long addmisionid = adm.ID;

            AdmissionStudent student = new AdmissionStudent();
            student.AdmissionId = addmisionid;
            student.FirstName = null;
            student.LastName = null;
            student.DateOfBirth = null;
            student.Active = true;
            student.IsAvailable = true;
            student.Status = EntityStatus.ACTIVE;
            student.InsertedDate = DateTime.Now;
            student.ModifiedDate = DateTime.Now;
            student.InsertedId = 0;
            student.ModifiedId = 0;
            admissionRepository.CreateAdmissionStudent(student);
            long addmisionstudentid = student.ID;


            string board = null;
            string org = null;
            string stand = null;
            string wing = null;

            AdmissionStudentStandard studentstandard = new AdmissionStudentStandard();


            studentstandard.AdmissionStudentId = addmisionstudentid;
            studentstandard.Active = true;
            studentstandard.IsAvailable = true;
            studentstandard.Status = EntityStatus.ACTIVE;
            studentstandard.InsertedDate = DateTime.Now;
            studentstandard.ModifiedDate = DateTime.Now;
            studentstandard.InsertedId = 0;
            studentstandard.ModifiedId = 0;
            admissionRepository.CreateAdmissionStudentStandard(studentstandard);
            AdmissionStudentContact stucontact = new AdmissionStudentContact();
            stucontact.AdmissionStudentId = addmisionstudentid;

            stucontact.Mobile = mobile;
            stucontact.Active = true;
            stucontact.IsAvailable = true;
            stucontact.Status = EntityStatus.ACTIVE;
            stucontact.InsertedDate = DateTime.Now;
            stucontact.ModifiedDate = DateTime.Now;
            stucontact.InsertedId = 0;
            stucontact.ModifiedId = 0;
            stucontact.ConatctType = "official";
            admissionRepository.CreateAdmissionStudentContact(stucontact);

            AdmissionTimeline timeline = new AdmissionTimeline();
            timeline.AdmissionId = addmisionid;
            timeline.Timeline = "REGISTRATION";
            timeline.Active = true;
            timeline.IsAvailable = true;
            timeline.Status = EntityStatus.ACTIVE;
            timeline.InsertedDate = DateTime.Now;
            timeline.ModifiedDate = DateTime.Now;
            timeline.InsertedId = 0;
            timeline.ModifiedId = 0;
            admissionRepository.CreateAdmissionTimeline(timeline);
            #region SMS and EMAIL



            //smssend.SendRegistrationSms(FirstName + " " + LastName, ContactMobile);


            //senndInBlue.SendRegistrationMail(

            //        sessionname, org, board, stand, wing, FirstName + " " + LastName, ContactName, ContactMobile, EmailId,
            //        EmailId);


            #endregion
            try
            {

                LeadCreateDto dto = new LeadCreateDto();
                dto.AdmissionId = adm.LeadReferenceId.ToString();
                dto.FirstName = student.FirstName;
                dto.LastName = student.LastName;
                dto.Name = stucontact.FullName;
                dto.Phone = stucontact.Mobile;
                dto.SchoolName = studentstandard.OrganizationId > 0 ? organizationRepository.
                    GetOrganizationById(studentstandard.OrganizationId).Name : "NA";
                dto.ClassName = studentstandard.StandardId > 0 ? standardRepository.
                    GetStandardById(studentstandard.StandardId).Name : "NA";
                dto.WingName = studentstandard.WingId > 0 ? wingRepository.
                    GetByIdAsync(studentstandard.WingId).Result.Name : "NA";
                dto.EmailAddress = stucontact.EmailId;
                dto.Source = admissionRepository.
                    GetAdmissionSourceById(adm.AcademicSourceId).Name;
                dto.Status = "NOT PAID";

                dto.CreatedOn = NullableDateTimeToStringDateTime(adm.InsertedDate).DateTime;



                LeadManage leadManage = new LeadManage();
                LeadResponse lead = leadManage.createLead(leadManage.CreateLeadData(dto));
                if (lead != null && lead.Message != null)
                {
                    adm.LeadId = Guid.Parse(lead.Message.Id);
                    admissionRepository.UpdateAdmission(adm);
                }
            }
            catch (Exception e)
            {

            }

            return adm.ID;
        }

        public OdmErp.ApplicationCore.Entities.Admission UpdateAdmissionDataAMobile(string FirstName,
        string LastName,
        string ContactName,
        string ContactMobile,
        string EmailId,
        string ClassId,
        string AcademicStandardWindId, long sourceId, long admissionId)
        {
            // admissionRepository.GetAllAdmission().Where(a => a.MobileNumber == ContactMobile).FirstOrDefault();

            var adm = admissionRepository.GetAdmissionById(admissionId);
            long addmisionid = admissionId;

            AdmissionStudent student = admissionRepository.GetAdmissionStudentById(addmisionid);
            student.AdmissionId = addmisionid;
            student.FirstName = FirstName;
            student.LastName = LastName;
            student.DateOfBirth = null;
            student.Active = true;
            student.IsAvailable = true;
            student.Status = EntityStatus.ACTIVE;
            student.InsertedDate = DateTime.Now;
            student.ModifiedDate = DateTime.Now;
            student.InsertedId = 0;
            student.ModifiedId = 0;
            admissionRepository.UpdateAdmissionStudent(student);
            long addmisionstudentid = student.ID;


            string board = null;
            string org = null;
            string stand = null;
            string wing = null;

            AdmissionStudentStandard studentstandard = admissionRepository.
                GetAdmissionStudentStandardByAdmissionStudentId(addmisionstudentid);
            if (AcademicStandardWindId != null)
            {
                var res = academicStandardWingRepository.
                   GetByIdAsync(Convert.ToInt64(AcademicStandardWindId))
                   .Result;
                studentstandard.AcademicStandardWindId = Convert.ToInt64(AcademicStandardWindId);

                if (res != null)
                {
                    var data = boardStandardWingRepository.
                        GetByIdAsync(res.BoardStandardWingID).Result;
                    var data1 = boardStandardRepository.
                        GetByIdAsync(data.BoardStandardID).Result;
                    var data2 = academicStandardRepository.
                        GetByIdAsync(res.AcademicStandardId).Result;
                    var data3 = organizationAcademicRepository.
                        GetByIdAsync(data2.OrganizationAcademicId).Result;
                    board = boardRepository.
                        GetBoardById(data1.BoardId).Name;
                    wing = wingRepository.
                       GetByIdAsync(data.WingID).Result.Name;
                    stand = standardRepository.
                      GetStandardById(data1.StandardId).Name;
                    org = organizationRepository.
                      GetOrganizationById(data3.OrganizationId).Name;

                    studentstandard.BoardId = data1.BoardId;
                    studentstandard.StandardId = data1.StandardId;
                    studentstandard.WingId = data.WingID;
                    studentstandard.OrganizationId = data3.OrganizationId;
                }
            }

            studentstandard.AdmissionStudentId = addmisionstudentid;
            studentstandard.Active = true;
            studentstandard.IsAvailable = true;
            studentstandard.Status = EntityStatus.ACTIVE;
            studentstandard.InsertedDate = DateTime.Now;
            studentstandard.ModifiedDate = DateTime.Now;
            studentstandard.InsertedId = 0;
            studentstandard.ModifiedId = 0;
            admissionRepository.UpdateAdmissionStudentStandard(studentstandard);
            AdmissionStudentContact stucontact = admissionRepository.
                GetAdmissionStudentContactByAdmissionStudentId(addmisionstudentid);
            stucontact.AdmissionStudentId = addmisionstudentid;
            stucontact.FullName = ContactName;
            stucontact.EmailId = EmailId;
            stucontact.Mobile = ContactMobile;
            stucontact.Active = true;
            stucontact.IsAvailable = true;
            stucontact.Status = EntityStatus.ACTIVE;
            stucontact.InsertedDate = DateTime.Now;
            stucontact.ModifiedDate = DateTime.Now;
            stucontact.InsertedId = 0;
            stucontact.ModifiedId = 0;
            stucontact.ConatctType = "official";
            admissionRepository.UpdateAdmissionStudentContact(stucontact);


            #region SMS and EMAIL


            string sessionname =
                 academicSessionRepository.
                 GetByIdAsync(adm.AcademicSessionId).Result.DisplayName;


            if (FirstName != null && LastName != null)
            {
                smssend.SendRegistrationSms(FirstName + " " + LastName, ContactMobile);
            }
            if (org != null)
            {
                sendInBlue.SendRegistrationMail(

                        sessionname, org, board, stand, wing, FirstName + " " + LastName, ContactName, ContactMobile, EmailId,
                        EmailId);
            }

            #endregion
            try
            {

                LeadCreateDto dto = new LeadCreateDto();
                dto.AdmissionId = adm.LeadReferenceId.ToString();
                dto.FirstName = student.FirstName;
                dto.LastName = student.LastName;
                dto.Name = stucontact.FullName;
                dto.Phone = stucontact.Mobile;
                dto.SchoolName = studentstandard.OrganizationId > 0 ? organizationRepository.
                    GetOrganizationById(studentstandard.OrganizationId).Name : "NA";
                dto.ClassName = studentstandard.StandardId > 0 ? standardRepository.
                    GetStandardById(studentstandard.StandardId).Name : "NA";
                dto.WingName = studentstandard.WingId > 0 ? wingRepository.
                    GetByIdAsync(studentstandard.WingId).Result.Name : "NA";
                dto.EmailAddress = stucontact.EmailId;
                dto.Source = admissionRepository.
                    GetAdmissionSourceById(adm.AcademicSourceId).Name;
                dto.Status = "NOT PAID";

                dto.CreatedOn = NullableDateTimeToStringDateTime(adm.InsertedDate).DateTime;



                LeadManage leadManage = new LeadManage();
                LeadResponse lead = leadManage.updateLead(leadManage.CreateLeadData(dto));

            }
            catch (Exception e)
            {

            }

            return adm;
        }
        public (string Date, string Time, string DateTime) NullableDateTimeToStringDateTime(DateTime? time)
        {
            return time == null ? ("NA", "NA", "NA") : (time.Value.ToString("dd-MMM-yyyy"),
                time.Value.ToString("hh:mm tt"), time.Value.ToString("dd-MMM-yyyy hh:mm tt"));

        }
    }
}
