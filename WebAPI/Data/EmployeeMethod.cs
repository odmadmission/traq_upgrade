﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Constants;
using WebAPI.Models;
using static WebAPI.Models.SupportModel;

namespace WebAPI.Data
{
    public class EmployeeMethod
    {

        protected readonly IEmployeeRepository employeeRepository;
        protected readonly IEmployeeAddressRepository employeeAddressRepository;
        protected readonly IEmployeeBankAccountRepository employeeBankAccountRepository;
        protected readonly IEmployeeDesignationRepository employeeDesignationRepository;
        protected readonly IEmployeeDocumentAttachmentRepository employeeDocumentAttachmentRepository;
        protected readonly IEmployeeEducationRepository employeeEducationRepository;
        protected readonly IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepository;
        protected readonly IEmployeeExperienceRepository employeeExperienceRepository;
        protected readonly IActionAccessRepository actionAccessRepository;
        protected readonly IAccessRepository accessRepository;
        protected readonly IAddressRepository addressRepository;
        protected readonly IAddressTypeRepository addressTypeRepository;
        protected readonly ICountryRepository countryRepository;
        protected readonly ICityRepository cityRepository;
        protected readonly IStateRepository stateRepository;
        protected readonly IParentRepository parentRepository;
        protected readonly ILogingRepository logingRepository;

        public EmployeeMethod(IEmployeeRepository employeeRepository, IEmployeeAddressRepository employeeAddressRepository,
            IEmployeeBankAccountRepository employeeBankAccountRepository, IEmployeeDesignationRepository employeeDesignationRepository,
            IEmployeeDocumentAttachmentRepository employeeDocumentAttachmentRepository,
            IEmployeeEducationRepository employeeEducationRepository,
            IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepository,
            IEmployeeExperienceRepository employeeExperienceRepository, IActionAccessRepository actionAccessRepository,
            IAccessRepository accessRepository, IAddressRepository addressRepository, IAddressTypeRepository addressTypeRepository,
            ICountryRepository countryRepository, ICityRepository cityRepository, IStateRepository stateRepository,
             ILogingRepository logingRepository)
        {
            this.logingRepository = logingRepository;
            this.employeeRepository = employeeRepository;
            this.employeeAddressRepository = employeeAddressRepository;
            this.employeeBankAccountRepository = employeeBankAccountRepository;
            this.employeeDesignationRepository = employeeDesignationRepository;
            this.employeeDocumentAttachmentRepository = employeeDocumentAttachmentRepository;
            this.employeeEducationRepository = employeeEducationRepository;
            this.employeeRequiredDocumentRepository = employeeRequiredDocumentRepository;
            this.employeeExperienceRepository = employeeExperienceRepository;
            this.actionAccessRepository = actionAccessRepository;
            this.accessRepository = accessRepository;
            this.addressRepository = addressRepository;
            this.addressTypeRepository = addressTypeRepository;
            this.countryRepository = countryRepository;
            this.cityRepository = cityRepository;
            this.stateRepository = stateRepository;
        }

        public LoginResponseModel CheckAccess(LoginRequestModel request)
        {
            LoginResponseModel response = new LoginResponseModel();

            Access access = accessRepository.GetAccessByUserName(request.UserName);
            if (access != null)
            {
                Employee employee = employeeRepository.GetEmployeeById(access.EmployeeID);
                response.commonModel = new CommonModel();
                response.commonModel.Mobile = employee.PrimaryMobile;
                response.commonModel.UniqueCode = access.Username;
                response.commonModel.EmployeeId = access.EmployeeID;
                if (access.Password == null || access.Password.Equals("null"))
                {
                    response.StatusCode = HttpStatusCode.LOGIN_NO_PASSWORD;
                    response.Status = "LOGIN_NO_PASSWORD";
                }

                else

                {
                    
                
                    response.StatusCode = HttpStatusCode.LOGIN_PASSWORD_AVAILABLE;
                    response.Status = "LOGIN_PASSWORD_AVAILABLE";
                }

            }
            else
            {
                response.StatusCode = HttpStatusCode.LOGIN_USERNAME_INVALID;
                response.Status = "LOGIN_USERNAME_INVALID";
            }

            return response;
        }

        public LoginResponseModel Login(LoginRequestModel request)
        {
            LoginResponseModel response = new LoginResponseModel();

            Access access = accessRepository.GetAccessByUserName(request.UserName);
            if (access != null)
            {
                if (access.Password == null || access.Password.Equals("null"))
                {
                    response.StatusCode = HttpStatusCode.LOGIN_NO_PASSWORD;
                    response.Status = "LOGIN_NO_PASSWORD";
                }
                else
                if (access.Password.Equals(request.Password))
                {
                    CommonModel common = new CommonModel();
                    response.commonModel = common;
                    //employee
                    if (access.RoleID == 4)
                    {
                        Employee employee = employeeRepository.GetEmployeeById(access.EmployeeID);
                        if (employee != null)
                        {


                            //EmployeeTimeline et = employeeRepository.GetEmployeeTimelineEmployeeById(employee.ID);
                            //if (et != null)
                            //{
                                common.AccessId = access.ID;
                                common.RoleId = access.RoleID;

                                common.EmployeeId = employee.ID;
                                common.UniqueCode = employee.EmpCode;
                                common.Mobile = employee.PrimaryMobile;

                                response.StatusCode = HttpStatusCode.LOGIN_SUCCESS;
                                response.Status = "LOGIN_SUCCESS";

                            //}
                            //else
                            //{
                            //    response.StatusCode = HttpStatusCode.EMPLOYEE_JOIN_DETAILS_NOT_FOUND;
                            //    response.Status = "EMPLOYEE_JOIN_DETAILS_NOT_FOUND";
                            //}
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.EMPLOYEE_NOT_FOUND;
                            response.Status = "EMPLOYEE_NOT_FOUND";
                        }

                    }
                    else
                   if (access.RoleID == 5)
                    {
                        //student

                    }
                    else if(access.RoleID == 1)
                    {
                        Employee employee = employeeRepository.GetEmployeeById(access.EmployeeID);
                        if (employee != null)
                        {
                            common.AccessId = access.ID;
                            common.RoleId = access.RoleID;

                            common.EmployeeId = employee.ID;
                            common.UniqueCode = employee.EmpCode;
                            common.Mobile = employee.PrimaryMobile;

                            response.StatusCode = HttpStatusCode.LOGIN_SUCCESS;
                            response.Status = "LOGIN_SUCCESS";
                        }
                        else {
                            response.StatusCode = HttpStatusCode.EMPLOYEE_NOT_FOUND;
                            response.Status = "EMPLOYEE_NOT_FOUND";
                        }

                    }
                    else
                    if (access.RoleID == 6)
                    {
                        Parent parent = parentRepository.GetParentById(access.EmployeeID);
                        if (parent != null)
                        {


                                common.AccessId = access.ID;
                                common.RoleId = access.RoleID;

                                common.EmployeeId = parent.ID;
                                //common.UniqueCode = parent.ParentCode;
                                common.Mobile = parent.PrimaryMobile;

                                response.StatusCode = HttpStatusCode.LOGIN_SUCCESS;
                                response.Status = "LOGIN_SUCCESS";

                          
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.PARENT_NOT_FOUND;
                            response.Status = "PARENT_NOT_FOUND";
                        }

                    }



                }
                else

                {
                    response.StatusCode = HttpStatusCode.LOGIN_INVALID_CREDENTIALS;
                    response.Status = "LOGIN_INVALID_CREDENTIALS";
                }

            }
            else
            {
                response.StatusCode = HttpStatusCode.LOGIN_USERNAME_INVALID;
                response.Status = "LOGIN_USERNAME_INVALID";
            }

            return response;
        }

        public BaseModel LogoutAccount(LogoutRequestModel request)
        {
            BaseModel response = new BaseModel();
            try
            {

                Login login = logingRepository.GetByIdAsync(request.LoginId).Result;
                if (login != null)
                {

                    login.ModifiedDate = DateTime.Now;
                    login.ModifiedId = request.EmployeeId;

                    login.Active = false;
                    login.Status = EntityStatus.INACTIVE;

                    logingRepository.UpdateAsync(login).Wait();

                    response.StatusCode = HttpStatusCode.SUCCESS;
                    response.Status = CommonText.SUCCESS;

                }
                else
                {
                    response.StatusCode = HttpStatusCode.LOGIN_NOT_FOUND;
                    response.Status = "LOGIN_NOT_FOUND";
                }

            }
            catch(Exception e)
            {
                response.StatusCode = HttpStatusCode.EXCEPTION;
                response.Status = CommonText.EXCEPTION;
                response.Message = e.Message;
                response.DisplayMessage = e.Message;
            }

            return response;
        }


        public IEnumerable<EmployeeAddressModel> GetEmployeeAddress(EmployeeBaseRequestModel employeeBaseRequestModel)
        {
            var empAddressList = employeeAddressRepository.GetAllEmployeeAddresses();
            var addressList = addressRepository.GetAllAddress();
            var addressTypeList = addressTypeRepository.GetAllAddressType();
            var empList = employeeRepository.GetAllEmployee();
            //var cityList = cityRepository.GetAllCity();
            //var stateList = stateRepository.GetAllState();
            //var countryList = countryRepository.GetAllCountries();

            var res = (from a in empAddressList
                       join b in addressList on a.AddressID equals b.ID
                       join c in addressTypeList on b.AddressTypeID equals c.ID
                       join d in empList on a.EmployeeID equals d.ID
                       select new EmployeeAddressModel
                       {
                           ID = a.ID,
                           AddressLine1 = b.AddressLine1,
                           AddressLine2 = b.AddressLine2,
                           PostalCode = b.PostalCode,
                           CityId = b.CityID,
                           StateId = b.StateID,
                           CountryId = b.CountryID,
                           AddressTypeId = b.AddressTypeID,
                           EmployeeId = a.EmployeeID
                       }).ToList();
            return res;
        }

        public IEnumerable<EmpBankAccounts> GetEmployeeBankAccount(EmployeeBaseRequestModel employeeBaseRequestModel)
        {
            var accessList = accessRepository.GetAccessById(employeeBaseRequestModel.AccessId);
            var empBankAccount = employeeBankAccountRepository.GetEmployeeBankAccountByEmployeeID(accessList.EmployeeID);

            var res = (from a in empBankAccount
                       select new EmpBankAccounts
                       {
                           ID = a.ID,
                           BankTypeId = a.BankTypeID,
                           BankNameId = a.BankNameID,
                           BankAccountNumber = a.AccountNumber,
                           BankAccountHolderName = a.AccountHolderName,
                           EmployeeId = a.EmployeeID,
                           IsPrimary = a.IsPrimary,
                           BranchName = a.BankBranchName,
                           IFSCcode = a.IfscCode
                       }).ToList();
            return res;
        }

        public IEnumerable<EmployeeExperienceModel> GetEmployeeExperience(EmployeeBaseRequestModel employeeBaseRequestModel)
        {
            var accessList = accessRepository.GetAccessById(employeeBaseRequestModel.AccessId);
            var empExperienceList = employeeExperienceRepository.GetExperienceByEmployeeId(accessList.EmployeeID);

            var res = (from a in empExperienceList
                       select new EmployeeExperienceModel
                       {
                           ID = a.ID,
                           OrganizationName = a.OrganizationName,
                           Desingnation = a.Designation,
                           SalaryPerAnum = a.SalaryPerAnum,
                           FromDate = a.FromDate,
                           ToDate = a.ToDate,
                           EmployeeId = a.EmployeeID
                       }).ToList();
            return res;
        }

        public IEnumerable<EmployeeEducationModel> GetEmployeeEducation(EmployeeBaseRequestModel employeeBaseRequestModel)
        {
            var accessList = accessRepository.GetAccessById(employeeBaseRequestModel.AccessId);
            var empEducationList = employeeEducationRepository.GetAllEmployeeEducationsByEmployeeId(accessList.EmployeeID);

            var res = (from a in empEducationList
                       select new EmployeeEducationModel
                       { 
                           ID = a.ID,
                           EducationQualificationId = a.EducationQualificationID,
                           EmployeeId = a.EmployeeID,
                           College = a.College,
                           Univarsity = a.University,
                           FromYear = a.FromYear,
                           ToYear = a.ToYear
                       }).ToList();
            return res;
        }

        public LoginResponseModel UpdatePassword(ForgotPasswordRequest forgotPasswordRequest)
        {
            LoginResponseModel response = new LoginResponseModel();

            try
            {
                var accessList = accessRepository.GetAccessByUserName(forgotPasswordRequest.UserName);
                bool proceed = true;
                if(forgotPasswordRequest.Type==PasswordType.CHANGE_PASSWORD)
                {
                    if (!accessList.Password.Equals(forgotPasswordRequest.OldPassword))
                    {
                        proceed = false;
                        response.StatusCode = HttpStatusCode.FAILURE;
                        response.Status = CommonText.FAILED;
                        response.DisplayMessage = DisplayText.PASSOWRD_UPDATE_OLD_WRONG_TEXT;
                    }
                    
                }


                if (proceed)
                {
                    if (forgotPasswordRequest.NewPassword == forgotPasswordRequest.ConfirmPassword)
                    {
                        accessList.Password = forgotPasswordRequest.NewPassword;
                        accessList.ModifiedId = accessList.EmployeeID;
                        accessList.ModifiedDate = DateTime.Now;
                        accessRepository.UpdateAccess(accessList);

                        response.StatusCode = HttpStatusCode.SUCCESS;
                        response.Status = CommonText.SUCCESS;
                        response.DisplayMessage = DisplayText.PASSOWRD_UPDATE_SUCCESS_TEXT;
                    }
                    else
                    {
                        response.StatusCode = HttpStatusCode.FAILURE;
                        response.Status = CommonText.FAILED;
                        response.DisplayMessage = DisplayText.PASSOWRD_UPDATE_FAILED_TEXT;
                    }
                }

            }
            catch(Exception e)
            {
                response.StatusCode = HttpStatusCode.EXCEPTION;
                response.Status = CommonText.EXCEPTION;
                response.Message = e.Message;
                response.DisplayMessage = e.Message;
            }

            
            return response;

        }

        //// updated eva


        public IEnumerable<NameIdViewModel> EmployeeStatus()
        {
            try
            {
                List<NameIdViewModel> viewModels = new List<NameIdViewModel>() {
                        new NameIdViewModel{TypeId = "All",Name = "ALL"},
                        new NameIdViewModel{ TypeId = "Current",Name = "Current"},
                        new NameIdViewModel{ TypeId = "Left",Name = "Left"}
                }.ToList();

                return viewModels;
            }
            catch
            {
                return null;
            }
        }

    }
}
