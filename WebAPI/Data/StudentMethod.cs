﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;
using static WebAPI.Models.StudentModel;

namespace WebAPI.Data
{
    public class StudentMethod
    {
        protected readonly IBloodGroupRepository bloodGroupRepository;
        protected readonly IReligionTypeRepository religionTypeRepository;
        protected readonly INationalityTypeRepository nationalityTypeRepository;
        protected readonly IStudentRepository studentRepository;
        protected readonly IProfessionTypeRepository professionTypeRepository;
        protected readonly IParentRelationshipTypeRepository parentRelationshipTypeRepository;
        protected readonly IParentRepository parentRepository;
        protected readonly IAccessRepository accessRepository;

        
        public StudentMethod(IBloodGroupRepository bloodGroupRepository, IReligionTypeRepository religionTypeRepository,
            INationalityTypeRepository nationalityTypeRepository, IStudentRepository studentRepository, IParentRepository parentRepository,
            IProfessionTypeRepository professionTypeRepository, IParentRelationshipTypeRepository parentRelationshipTypeRepository,
            IAccessRepository accessRepository)
        {
            this.bloodGroupRepository = bloodGroupRepository;
            this.religionTypeRepository = religionTypeRepository;
            this.nationalityTypeRepository = nationalityTypeRepository;
            this.studentRepository = studentRepository;
            this.professionTypeRepository = professionTypeRepository;
            this.parentRelationshipTypeRepository = parentRelationshipTypeRepository;
            this.parentRepository = parentRepository;
            this.accessRepository = accessRepository;
        }

        public IEnumerable<ParentProfileModel> GetParentProfileDetails(EmployeeBaseRequestModel employeeBaseRequestModel)
        {
            var accessList = accessRepository.GetAccessById(employeeBaseRequestModel.AccessId);
            var professionTypeList = professionTypeRepository.GetAllProfessionTypes();
            var parentRelationShipTypeList = parentRelationshipTypeRepository.GetAllParentRelationshipType();
            var parentList = parentRepository.GetAllParent();
            var studentList = studentRepository.GetAllStudent();

            var res = (from a in parentList.Where(A=>A.ID==accessList.EmployeeID)
                       join b in professionTypeList on a.ProfessionTypeID equals b.ID
                       join c in parentRelationShipTypeList on a.ParentRelationshipID equals c.ID
                       join d in studentList on a.StudentID equals d.ID
                      
                       select new ParentProfileModel
                       {
                           ID = a.ID,
                           FirstName = a.FirstName,
                           MiddleName = a.MiddleName,
                           LastName = a.LastName,
                           FullName = a.FirstName + "" + a.MiddleName + "" + a.LastName,
                           PrimaryMobile = a.PrimaryMobile,
                           AlternativeMobile = a.AlternativeMobile,
                           LandLineNumber = a.LandlineNumber,
                           Email = a.EmailId,
                           ProfessionTypeId = a.ProfessionTypeID,
                           ProfessionName = b.Name,
                           //Organization = a.Organization,
                           StudentId = a.StudentID,
                           StudentName = d.FirstName + "" + d.MiddleName + "" + d.LastName,
                           ParentRelationshipId = a.ParentRelationshipID,
                           ParentRelationshipName = c.Name

                       }).ToList();
            return res;
        }

        public IEnumerable<StudentProfileModel> GetStudentProfileDetails(long id)
        {
            var studentList = studentRepository.GetAllStudent();
            var bloodGroups = bloodGroupRepository.GetAllBloodGroup();
            var nationalityList = nationalityTypeRepository.GetAllNationalities();
            var religionList = religionTypeRepository.GetAllReligionTypes();

            var res = (from a in studentList
                       join b in bloodGroups on a.BloodGroupID equals b.ID
                       join c in nationalityList on a.NatiobalityID equals c.ID
                       join d in religionList on a.ReligionID equals d.ID
                       select new StudentProfileModel
                       {
                           ID = a.ID,
                           FirstName = a.FirstName,
                           MiddleName = a.MiddleName,
                           LastName = a.LastName,
                           FullName = a.FirstName + "" + a.MiddleName + "" + a.LastName,
                           StudentCode = a.StudentCode,
                           Gender = a.Gender,
                           DOB = a.DateOfBirth,
                           BloodGroupId = a.BloodGroupID,
                           BloodGroupName = b.Name,
                           NationalId = a.NatiobalityID,
                           NationalityName = c.Name,
                           ReligionId = a.ReligionID,
                           ReligionName = d.Name
                       });
            return res;
        }
    }
}
