﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebAPI.Constants;
using WebAPI.Models;

namespace WebAPI.Data
{
    public class SmsMethod
    {


        protected readonly IOtpMessageRepository otpMessageRepository;
        protected readonly ILogingRepository logingRepository;
     
        protected readonly IAcademicSessionRepository academicSessionRepository;
        protected readonly IAdmissionRepository admissionRepository;
        protected readonly IAdmissionStudentRepository admissionStudentRepository;
        protected readonly IAdmissionFormPaymentRepository admissionFormPaymentRepository;


        public SmsMethod(IOtpMessageRepository otpMessageRepository, ILogingRepository logingRepository,
            IAcademicSessionRepository academicSessionRepository, IAdmissionRepository admissionRepository,
            IAdmissionStudentRepository admissionStudentRepository, IAdmissionFormPaymentRepository admissionFormPaymentRepository)
        {
            this.otpMessageRepository = otpMessageRepository;
            this.logingRepository = logingRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.admissionRepository = admissionRepository;
            this.admissionStudentRepository = admissionStudentRepository;
            this.admissionFormPaymentRepository = admissionFormPaymentRepository;
        }

        public SmsResponseModel SendSms(SmsRequestModel request)
        {
            SmsResponseModel response = new SmsResponseModel();
            string otp = GenerateRandomNumber(6);
            string message = null;


           
            if (request.OtpType == OtpTypeText.OTP_LOGIN_APP)
            {
                 message = "Tracq login verification code : " + otp;
                
            }
            else
            if (request.OtpType == OtpTypeText.OTP_CREATE_PASSWORD)
            {
                 message = "Tracq create password verification code : " + otp;
            }
            else
            if (request.OtpType == OtpTypeText.OTP_FORGOT_PASSWORD)
            {
                 message = "Tracq forgot password verification code : " + otp;
            }
            else
            if (request.OtpType == OtpTypeText.OTP_CHANGE_PASSWORD)
            {
                message = "Tracq change password verification code : " + otp;
            }
            else
            if (request.OtpType == OtpTypeText.ADMISSION_REGISTRATION)
            {
                message = "Tracq admission registration verification code : " + otp;
            }
            if (message!=null)
            {
                OtpMessage otpMessage = new OtpMessage();
                otpMessage.AppType = request.AppType;
                otpMessage.OtpType = request.OtpType;
                otpMessage.InsertedDate = DateTime.Now;// request.CurrentDate;
                otpMessage.InsertedId = request.EmployeeId;
                otpMessage.Active = true;
                otpMessage.Status = EntityStatus.ACTIVE;
                otpMessage.AccessID = request.AccessId;
                otpMessage.ModifiedDate = DateTime.Now;
                otpMessage.ModifiedId = request.EmployeeId;
                otpMessage.Mobile = request.Mobile;
                otpMessage.ValidTill = DateTime.Now;//  request.CurrentDate.AddMinutes(5);
                otpMessage.SentOtpCode = otp;
                OtpMessage savedOtp = otpMessageRepository.AddAsync(otpMessage).Result;
                if (savedOtp != null)
                {
                    response.OtpId = savedOtp.ID;
                }

                if (response.OtpId>0&&SendToMobile(request.Mobile, message))
                {
                    
                    response.OtpCode = otp;

                    response.StatusCode = Constants.HttpStatusCode.SMS_SENT_SUCCESS;
                    response.Status = "SMS_SENT_SUCCESS";
                }
                else
                {
                    savedOtp.Status = EntityStatus.INACTIVE;
                    savedOtp.Active =false;
                    savedOtp.ModifiedDate = DateTime.Now;
                    savedOtp.ModifiedId = request.EmployeeId;
                    otpMessageRepository.UpdateAsync(savedOtp);

                    response.StatusCode = Constants.HttpStatusCode.SMS_SENT_FAILED;
                    response.Status = "SMS_SENT_FAILED";
                }
            }

           
            return response;
        }
        public SmsResponseModel SendOtp(SmsInputModel request)
        {
            SmsResponseModel response = new SmsResponseModel();
            string otp = GenerateRandomNumber(6);
            string message = null;



           
            if (request.OtpType == OtpTypeText.ADMISSION_REGISTRATION)
            {
                message = "Tracq admission registration verification code : " + otp;
            }
            if (message != null)
            {
                OtpMessage otpMessage = new OtpMessage();
                otpMessage.AppType = request.AppType;
                otpMessage.OtpType = request.OtpType;
                otpMessage.InsertedDate = DateTime.Now;// request.CurrentDate;
                otpMessage.InsertedId = 0;
                otpMessage.Active = true;
                otpMessage.Status = EntityStatus.ACTIVE;
                otpMessage.AccessID = 0;
                otpMessage.ModifiedDate = DateTime.Now;
                otpMessage.ModifiedId = 0;
                otpMessage.Mobile = request.Mobile;
                otpMessage.ValidTill = DateTime.Now;//  request.CurrentDate.AddMinutes(5);
                otpMessage.SentOtpCode = otp;
                OtpMessage savedOtp = admissionRepository.SaveOtpMessage(otpMessage);
                if (savedOtp != null)
                {
                    response.OtpId = savedOtp.ID;
                }

                if (response.OtpId > 0 && SendToMobile(request.Mobile, message))
                {

                    response.OtpCode = otp;

                    response.StatusCode = Constants.HttpStatusCode.SMS_SENT_SUCCESS;
                    response.Status = "SMS_SENT_SUCCESS";
                }
                else
                {
                    savedOtp.Status = EntityStatus.INACTIVE;
                    savedOtp.Active = false;
                    savedOtp.ModifiedDate = DateTime.Now;
                    savedOtp.ModifiedId =0;
                    admissionRepository.UpdateOtpMessage(savedOtp);

                    response.StatusCode = Constants.HttpStatusCode.SMS_SENT_FAILED;
                    response.Status = "SMS_SENT_FAILED";
                }
            }


            return response;
        }

        public  SmsResponseModel VerifySms(SmsRequestModel request)
        {
            SmsResponseModel response = new SmsResponseModel();

            try
            {
                OtpMessage otpMessage = admissionRepository.GetOtpMessageById(request.OtpId);
                if (otpMessage != null)
                {
                    if (otpMessage.Active)
                    {
                        if (otpMessage.SentOtpCode == request.RecievedOtp)
                        {
                          

                            if (request.OtpType == OtpTypeText.OTP_LOGIN_APP)
                            {
                                Login login = new Login();
                                login.InsertedDate = DateTime.Now;// request.CurrentDate;
                                login.InsertedId = request.EmployeeId;
                                login.ModifiedDate = DateTime.Now;// request.CurrentDate;
                                login.ModifiedId = request.EmployeeId;
                                login.Active = true;
                                login.Status = EntityStatus.ACTIVE;
                                login.AccessID = request.AccessId;
                                login.RelatedId = request.RelatedId;
                                login.RelatedName = request.RelatedName;
                                login.Data = request.DeviceData;
                                login.FCMID = request.FcmId;
                                login.Type = request.AppType;
                                Login savedLogin = admissionRepository.SaveLogin(login);

                                response.LoginId = savedLogin.ID;

                            }

                            otpMessage.Status = EntityStatus.INACTIVE;
                            otpMessage.Active = false;
                            otpMessage.RecievedOtpCode = request.RecievedOtp;
                            otpMessage.ModifiedDate = DateTime.Now;//  request.CurrentDate;
                            otpMessage.ModifiedId = request.EmployeeId;
                            admissionRepository.UpdateOtpMessage(otpMessage);
                            
                            if(request.OtpType == "ADMISSION_REGISTRATION")    // This one for getting admission id(at the time of admission registration)
                            {
                              

                                var admission = admissionRepository.GetAdmissionById(request.RelatedId);
                                
                                if (admission != null)
                                {
                                    response.AdmissionId = admission.ID;
                                    var paymentDetails = admissionRepository.GetAdmissionFormPaymentByAdmissionId(admission.ID);
                                    var admissionStudent = admissionRepository.
                                        GetAdmissionStudentById(admission.ID);
                                    if(admissionStudent == null)
                                    {
                                        response.IsBasicRegistrationDone = false;
                                    }
                                    else
                                    {
                                        response.IsBasicRegistrationDone = true;
                                    }
                                    if(paymentDetails != null)
                                    {
                                        response.IsPaymentDone = true;
                                    }
                                    else
                                    {
                                        response.IsPaymentDone = false;
                                    }
                                }
                                else
                                {
                                    response.AdmissionId = 0;
                                }
                            }
                           
                            response.StatusCode = Constants.HttpStatusCode.SMS_VERIFICATION_SUCCESS;
                            response.Status = "SMS_VERIFICATION_SUCCESS";


                        }
                        else
                        {
                            response.AdmissionId = 0;
                            response.StatusCode = Constants.HttpStatusCode.SMS_VERIFICATION_OTP_INCORRECT;
                            response.Status = "SMS_VERIFICATION_OTP_INCORRECT";
                        }
                    }
                    else
                    {
                        response.AdmissionId = 0;
                        response.StatusCode = Constants.HttpStatusCode.SMS_VERIFICATION_INACTIVE;
                        response.Status = "SMS_VERIFICATION_INACTIVE";
                    }

                }
                else
                {
                    response.AdmissionId = 0;
                    response.StatusCode = Constants.HttpStatusCode.SMS_VERIFICATION_OTP_NOT_FOUND;
                    response.Status = "SMS_VERIFICATION_OTP_NOT_FOUND";
                }
            }
            catch (Exception e)
            {
                response.StatusCode = Constants.HttpStatusCode.EXCEPTION;
                response.Status =CommonText.EXCEPTION;
                response.Message = e.Message;
                response.DisplayMessage = e.Message;
            }
            return response;

        }

        public bool SendToMobile(string mobile, string message)
        {
            string authKey = "232520Aycx7OOpF5b790d71";

            string mobileNumber = mobile;

            string senderId = "OTRACQ";

            string msg = HttpUtility.UrlEncode(message);

            //Prepare you post parameters
            StringBuilder sbPostData = new StringBuilder();
            sbPostData.AppendFormat("authkey={0}", authKey);
            sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
            sbPostData.AppendFormat("&message={0}", msg);
            sbPostData.AppendFormat("&sender={0}", senderId);
            sbPostData.AppendFormat("&route={0}", "4");

            try
            {
                //Call Send SMS API
                string sendSMSUri = "http://api.msg91.com/api/sendhttp.php";
                //Create HTTPWebrequest
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                //Prepare and Add URL Encoded data
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(sbPostData.ToString());
                //Specify post method
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (System.IO.Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                //Get the response
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string responseString = reader.ReadToEnd();

                //Close the response
                reader.Close();
                response.Close();
                return true;
            }
            catch (SystemException ex)
            {
                return false;
            }

        }
        private static string GenerateRandomNumber(int length)
        {
            try
            {
                const string valid = "1234567890";
                StringBuilder res = new StringBuilder();
                Random rnd = new Random();
                while (0 < length--)
                {
                    res.Append(valid[rnd.Next(valid.Length)]);
                }
                return res.ToString();
            }
            catch (Exception e1)
            {
                return null;
            }
        }

    }

}
