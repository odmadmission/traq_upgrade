﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
   public class View_Timetable
    {
        public string title { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }

        public bool allDay { get; set; }
        public bool editable { get; set; }
        public string backgroundColor { get; set; }
        public string textColor { get; set; }
    }
}
