﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_All_Academic_Standard
    {
        public long ID { get; set; }
        public string BoardName { get; set; }
        public long BoardStandardId { get; set; }
        public long StandardId { get; set; }
        public string StandardName { get; set; }
        public long OraganisationAcademicId { get; set; }
        public string OraganisationName { get; set; }
        public long AcademicSessionId { get; set; }
        public int CountAcademicWing { get; set; }
        public string SessionName { get; set; }   
        public bool IsAvailable { get; set; }
        public bool? Nucleus { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int CountAcademicSection { get; set; }
        public int CountAcademicSubjectGroup { get; set; }
        public string Status { get; set; }
    }
}
