﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_Admission_Progresssheet
    {
        public long ID { get; set; }
        public int personal { get; set; }
        public int addresses { get; set; }
        public int standarddet { get; set; }
        public int academic { get; set; }
        public int contact { get; set; }
        public int declaration { get; set; }
        public int parent { get; set; }
        public int proficiency { get; set; }
        public int reference { get; set; }
        public int transport { get; set; }

    }
}
