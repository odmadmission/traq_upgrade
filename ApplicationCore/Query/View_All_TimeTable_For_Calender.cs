﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
   public class View_All_TimeTable_For_Calender
    {
        public long typeId { get; set; }
        public string typeName { get; set; }
        public string title { get; set; }
        public DateTime start { get; set; }
        public string backgroundColor { get; set; }
        public string textColor { get; set; }
        public long AcademicSectionId { get; set; }
        public long AcademicStandardId { get; set; }
        public long? AcademicStandardStreamId { get; set; }
    }
}
