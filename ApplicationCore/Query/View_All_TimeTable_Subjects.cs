﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_All_TimeTable_Subjects
    {
        public long AcademicSectionId { get; set; }
        public long AcademicStandardId { get; set; }
        public DateTime AssignDate { get; set; }
        public string NameOfMonth { get; set; }
        public long AcdemicTimeTableId { get; set; }
        public long ClassTimingId { get; set; }
        public long SubjectId { get; set; }
        public long TeacherId { get; set; }
        public long ID { get; set; }
        public string NameOfDay { get; set; }
        public string classtime { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public string TeacherName { get; set; }
        public string SubjectName { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ForwardedBy { get; set; }
        public long PeriodEmployeeID { get; set; }
        public Nullable<long> TakenEmployeeId { get; set; }
        public string TakenEmployeeName { get; set; }
        public string Reason { get; set; }
        public Nullable<long> AcademicChapterId { get; set; }
        public Nullable<long> AcademicConceptId { get; set; }
        public Nullable<long> AcademicSyllabusId { get; set; }
        public Nullable<long> PeriodEmployeeStatusId { get; set; }
        public string chaptername { get; set; }
        public Nullable<long> chapterId { get; set; }
        public Nullable<long> conceptId { get; set; }
        public string conceptname { get; set; }
        public Nullable<long> syllabusId { get; set; }
        public string syllabusname { get; set; }
        public long EmployeeId { get; set; }
    }
}
