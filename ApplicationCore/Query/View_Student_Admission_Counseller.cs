﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_Student_Admission_Counseller
    {
      public long AdmissionId { get; set; }
        public DateTime InsertedDate{ get; set; }
        public DateTime ModifiedDate{ get; set; }
        public long InsertedId{ get; set; }
        public long ModifiedId{ get; set; }
        public bool IsAvailable{ get; set; }
        public string FormNumber{ get; set; }
        public long AcademicSessionId{ get; set; }
        public long AdmissionSlotId{ get; set; }
        public string MobileNumber{ get; set; }
        public string ProfileImage { get; set; }
        public string OrganisationName { get; set; }
        public bool IsAdmissionFormAmountPaid{ get; set; }
        public bool IsAdmission{ get; set; }
        public bool IsPersonalInterviewAppeared{ get; set; }
        public bool IsPersonalInterviewQualified{ get; set; }
        public bool IsDocumentsSubmitted{ get; set; }
        public long AcademicStudentID { get; set; }
        public string StudentName{ get; set; }
        public long AcademicStandardWindId{ get; set; }
        public bool Nucleus { get; set; }
        public long AcademicStandardId { get; set; }
        public long BoardStandardWingID { get; set; }
        public long BoardStandardId { get; set; }
        public long OrganizationAcademicId { get; set; }
        public long BoardId { get; set; }
        public long StandardId { get; set; }
        public string BoardName { get; set; }
        public string StandardName { get; set; }
        public long WingID { get; set; }
        public string WingName { get; set; }
        public long OrganizationId { get; set; }
        public string SessionName { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
