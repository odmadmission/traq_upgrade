﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_All_Subject_Optional_Category
    {
        public long SubjectOptionalCategoryID { get; set; }
        public long BoardStandardID { get; set; }
        public long SubjectGroupID { get; set; }
        public long SubjectOptionalTypeID { get; set; }
        public string SubjectOptionalTypeName { get; set; }
        public long WingID { get; set; }
        public long StandardId { get; set; }
        public long BoardId { get; set; }
        public string StandardName { get; set; }
        public string BoardName { get; set; }
        public string WingName { get; set; }
        public string SubjectGroupName { get; set; }
        public int NoOfSubject { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsAvailable { get; set; }

    }
}
