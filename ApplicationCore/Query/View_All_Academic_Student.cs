﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_All_Academic_Student
    {
        public long ID { get; set; }
        public string BoardName { get; set; }
        public long BoardStandardId { get; set; }
        public long StandardId { get; set; }
        public bool StandardStreamType { get; set; }
        public string StandardName { get; set; }
        public long OraganisationAcademicId { get; set; }
        public string OraganisationName { get; set; }
        public long AcademicSessionId { get; set; }
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }
        public string studentname { get; set; }
        public string studentcode { get; set; }
        public long StudentId { get; set; }
        public long AcademicStandardId { get; set; }
        public long AcademicStandardStreamId { get; set; }
        public string streamName { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
