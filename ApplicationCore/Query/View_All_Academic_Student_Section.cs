﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
   public class View_All_Academic_Student_Section
    {
        public long ID { get; set; }
        public string BoardName { get; set; }
        public long BoardStandardId { get; set; }
        public long StandardId { get; set; }
        public bool StandardStreamType { get; set; }
        public string StandardName { get; set; }
        public long OraganisationAcademicId { get; set; }
        public string OraganisationName { get; set; }
        public long AcademicSessionId { get; set; }
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }
        public string studentname { get; set; }
        public string studentcode { get; set; }
        public long StudentId { get; set; }
        public string streamName { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long AcademicStudentId { get; set; }
        public long AcademicSectionId { get; set; }
        public string RollNo { get; set; }
        public string SectionName { get; set; }
    }
}
