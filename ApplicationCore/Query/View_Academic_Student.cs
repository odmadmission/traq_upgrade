﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_Academic_Student
    {
        public long ID { get; set; }
        public long OraganisationAcademicId { get; set; }
        public long AcademicStandardId { get; set; }
        public long AcademicSubjectCombinationId { get; set; }
        public long AcademicStandardWingId { get; set; }
        public long StudentId { get; set; }
        public bool? Nucleus { get; set; }
        public bool? IsRequiredNucleus { get; set; }
        public string PrimaryMobile { get; set; }
        public string ZoomMailID { get; set; }
        public string EmailID { get; set; }
        public string WhatsappMobile { get; set; }
        public string OraganisationName { get; set; }
        public string SessionName { get; set; }
        public string StandardName { get; set; }
        public string SectionName { get; set; }
        public string RollNo { get; set; }
        public string WingName { get; set; }
        public string studentname { get; set; }
        public string studentcode { get; set; }
        public string BoardName { get; set; }
        public string SubjectGroupName { get; set; }
        public string optionalSubject { get; set; }
        public long BoardStandardId { get; set; }
        public long AcademicSessionId { get; set; }
        public long StandardId { get; set; }
        public long BoardId { get; set; }
        public long WingID { get; set; }
        public long AcademicSectionId { get; set; }
        public long SectionID { get; set; }
        public bool IsAvailable { get; set; }
       
        public DateTime ModifiedDate { get; set; }
    }
    public class View_Previous_Academic
    {
        public long ID { get; set; }
        public DateTime InsertedDate { get; set; }
        public string OrganizationName { get; set; }
        public long BoardID { get; set; }
        public long ClassID { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public long StudentID { get; set; }
        public string ReasonForChange { get; set; }
        public string GradeOrPercentage { get; set; }
        public long CityId { get; set; }
        public long StateId { get; set; }
        public string BoardName { get; set; }
        public string StandardName { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public long CountryID { get; set; }
        public string StudentCode { get; set; }
        public string StudentName { get; set; }



    }
}
