﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
   public class View_All_Academic_Subject
    {
       public long AcademicSessionId { get; set; }
        public long AcademicSubjectsID { get; set; }
        public long SubjectId { get; set; }
        public long BoardStandardId { get; set; }
        public string Subjectname { get; set; }
        public string SubjectCode { get; set; }
    }
}
