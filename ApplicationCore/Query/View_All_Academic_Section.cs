﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_All_Academic_Section
    {
        public long ID { get; set; }
        public long AcademicStandardId { get; set; }
       
        public long SectionId { get; set; }
        public long BoardStandardId { get; set; }
        public long OrganizationAcademicId { get; set; }
        public long AcademicStandardWingsID { get; set; }
        public string Sectionname { get; set; }
        public long BoardId { get; set; }
        public long StandardId { get; set; }
        public long AcademicSessionId { get; set; }
        public long OrganizationId { get; set; }
        public long WingID { get; set; }


        public long OrganizationAcademicID { get; set; }

        public string Sessionname { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public long GroupID { get; set; }
        public string Organisationname { get; set; }
        public string Wingname { get; set; }
        public string Boardname { get; set; }
        public string Standardname { get; set; }

        public long BoardStandardWingID { get; set; }
    }

    public class View_StudentPaymentData
    {
        public long ID { get; set; }
        public long AcademicStandardId { get; set; }
        public long AcademicStandardStreamId { get; set; }
        public long SectionId { get; set; }
        public long BoardStandardId { get; set; }
        public long OrganizationAcademicId { get; set; }
        public long SchoolWingId { get; set; }
        public string Sectionname { get; set; }
        public long BoardId { get; set; }
        public long StandardId { get; set; }
        public long AcademicSessionId { get; set; }
        public long OrganizationId { get; set; }
        public long WingID { get; set; }
        public long OrganizationAcademicID { get; set; }
        public string Sessionname { get; set; }
        public string OrganisationName { get; set; }
        public string Wingname { get; set; }
        public string Boardname { get; set; }
        public string Standardname { get; set; }
        public string Streamname { get; set; }
        public decimal SumAmount { get; set; }
        public string Image { get; set; }
        public string studentcode { get; set; }

    }

    public class getMandatoryPrice
    {
        public decimal price { get; set; }
        public long? ApplicabledataId { get; set; }
        public string Applicabledata { get; set; }
    }

    public class getfeePrice
    {
        public decimal price { get; set; }
       
    }

    public class getStudentPaymentData
    {
        public long PaymentCollectionId { get; set; }
        public long PaidUserId { get; set; }
        public DateTime PaidDate { get; set; }
        public string AmountPaid { get; set; }
        public string TransactionId { get; set; }
        public long PaymentTypeId { get; set; }
        public string PaymentStatus { get; set; }
        public long StudentId { get; set; }
    }
}
