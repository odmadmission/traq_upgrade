﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_All_Teachers
    {
        public long ID { get; set; }
        public long EmployeeId { get; set; }
        public DateTime? EndDate { get; set; }
        public long GroupId { get; set; }
        public DateTime? StartDate { get; set; }
        public long OrganizationId { get; set; }
        public string Teachername { get; set; }
        public string EmpCode { get; set; }
        public bool Isleft { get; set; }
        public string Teacherstatus { get; set; }
        public long AcademicSectionId { get; set; }
        public long AcademicSubjectId { get; set; }
        public long AcademicTeacherSubjectsId { get; set; }
        public string TeacherSubjectstatus { get; set; }
        public long AcademicSubjectsId { get; set; }
        public long SubjectId { get; set; }
        public string SubjectName { get; set; }
        public long BoardStandardId { get; set; }
    }
}
