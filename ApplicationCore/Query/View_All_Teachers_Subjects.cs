﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_All_Teachers_Subjects
    {
        public long ID { get; set; }
        public long AcademicSectionId { get; set; }
        public long AcademicSubjectId { get; set; }
        public long TeacherId { get; set; }
        public long AcademicSessionId { get; set; }
        public long SubjectId { get; set; }
        public long BoardStandardId { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public string SessionName { get; set; }
        public long EmployeeId { get; set; }
        public long GroupId { get; set; }
        public long OrganizationId { get; set; }
        public string EmployeeName { get; set; }
        public string EmpCode { get; set; }
        public long BoardId { get; set; }
        public long StandardId { get; set; }
        public long OrganizationAcademicId { get; set; }
        public string BoardName { get; set; }
        public string StandardName { get; set; }
        public string OrganizationName { get; set; }
        public long AcademicStandardId { get; set; }
        public string SectionName { get; set; }
    }
}
