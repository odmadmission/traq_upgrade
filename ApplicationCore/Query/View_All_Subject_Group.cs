﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
   public class View_All_Subject_Group
    {
        public long SubjectGroupID { get; set; }
        public string SubjectGroupName { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsAvailable { get; set; }
        public long BoardStandardId { get; set; }
      
        public string StandardName { get; set; }
        public long StandardID { get; set; }
        public string BoardName { get; set; }
        public long BoardID { get; set; }
     
        public int NoOfSubject { get; set; }
    }
}
