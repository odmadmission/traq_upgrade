﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Query
{
    public class View_All_Admission
    {
        public long ID { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long? InsertedId { get; set; }
        public long? AdmissionSourceId { get; set; }
        public long? ModifiedId { get; set; }
        public bool? IsAvailable { get; set; }
        public string FormNumber { get; set; }
        public long? AcademicSessionId { get; set; }
        public long? AdmissionSlotId { get; set; }
        public string Mobile { get; set; }
        public bool? IsAdmissionFormAmountPaid { get; set; }
        public bool? IsAdmission { get; set; }
        public bool? IsPersonalInterviewAppeared { get; set; }
        public bool? IsPersonalInterviewQualified { get; set; }
        public bool? IsDocumentsSubmitted { get; set; }
        public string Status { get; set; }
        public long? AcademicStudentID { get; set; }

        public long AdmissionStudentId { get; set; }
        public Guid? LeadReferenceId { get; set; }
        public string Fullname { get; set; }
        public string ProfileImage { get; set; }
        public string OrganisationName { get; set; }
        public long? AcademicStandardWindId { get; set; }
        public bool? Nucleus { get; set; }
        public long? AcademicStandardId { get; set; }
        public long? AcademicStandardWingId { get; set; }
        public long? BoardStandardId { get; set; }
        public long? OrganisationAcademicId { get; set; }
        public long? BoardId { get; set; }
        public long? StandardId { get; set; }
        public string BoardName { get; set; }
        public string StandardName { get; set; }
        public long? WingId { get; set; }
        public string WingName { get; set; }
        public long? OrganizationId { get; set; }
        public string AcademicSessionName { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string EmergencyName { get; set; }
        public string EmergencyEmail { get; set; }
        public string EmergencyMobile { get; set; }
        public long? StudentCounselorId { get; set; }
        public string StudentCounselorName { get; set; }
        public DateTime? StudentCounselorAssignDate { get; set; }
        public long? CounsellorStatusId { get; set; }
        public DateTime? StudentCounselorEndDate { get; set; }
        public string StudentCounselorRemarks { get; set; }
        public string StudentCounselorStatus { get; set; }
        public string CounsellorStatusName { get; set; }
        public string AcademicSourceName { get; set; }
        public long? AdmissionFeeId { get; set; }
        public string TransactionId { get; set; }
        public DateTime? PaidDate { get; set; }
        public DateTime? ExamDate { get; set; }
        public long? FormPaymentId { get; set; }
        public long? ReceivedId { get; set; }
        public long? PaymentAmount { get; set; }
        public string AdmissionFeeName { get; set; }
        public string ReceivedPaymentBy { get; set; }
        public string PaymentModeName { get; set; }
        public string KitStatus { get; set; }
        public DateTime? KitStatusDate { get; set; }
    }
}
