﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentReference", Schema = "admission")]
    public class AdmissionStudentReference:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "nvarchar(300)")]
        public string FullName { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string Mobile { get; set; }
       
       
        [Column(TypeName = "nvarchar(300)")]
        public string EmailId { get; set; }


        [Column(TypeName = "varchar(100)")]
        public string ProfessionTypeId { get; set; }

        [Column(TypeName = "nvarchar(800)")]
        public string Address { get; set; }

        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }

        [Column(TypeName = "nvarchar(10)")]
        public string Pincode { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string CountrytId { get; set; }

        [Column(TypeName = "nvarchar(20)")]
        public string ConatctType { get; set; }
    }
}
