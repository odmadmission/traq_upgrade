﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("MapClassToAdmission", Schema = "admission")]
    public class MapClassToAdmission : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }
        [Column(TypeName = "bigint")]
        public long BoardId { get; set; }
        [Column(TypeName = "bigint")]
        public long ClassId { get; set; }
        [Column(TypeName = "bigint")]
        public long WingId { get; set; }
    }
}
