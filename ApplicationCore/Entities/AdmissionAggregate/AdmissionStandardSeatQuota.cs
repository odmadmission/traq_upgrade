﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStandardSeatQuota", Schema = "admission")]
    public class AdmissionStandardSeatQuota:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }

        [Column(TypeName = "bigint")]
        public long OrganizationAcademicId { get; set; }

        [Column(TypeName = "bigint")]
        public long BoardId { get; set; }

        [Column(TypeName = "bigint")]
        public long BoardStandardId { get; set; }

        //unused

        [Column(TypeName = "bigint")]
        public long WindId { get; set; }

      
        //unused
        [Column(TypeName = "bigint")]
        public long? StreamId { get; set; }


        [Column(TypeName = "bigint")]
        public long NoOfSeats { get; set; }




    }
}
