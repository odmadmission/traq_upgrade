﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentLanguage", Schema = "admission")]
    public class AdmissionStudentLanguage:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }

        [Column(TypeName = "bigint")]
        public long LanguageId { get; set; }
    }
}
