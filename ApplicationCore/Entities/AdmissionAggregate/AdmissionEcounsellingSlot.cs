﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using OdmErp.ApplicationCore.Interfaces;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionEcounsellingSlot", Schema = "admission")]
    public  class AdmissionEcounsellingSlot : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionEcounsellingDateID { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string Name { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string No_Of_Registration { get; set; }
        [Column(TypeName = "time(7)")]
        public TimeSpan StartTime { get; set; }
        [Column(TypeName = "time(7)")]
        public Nullable<TimeSpan> EndTime { get; set; }
    }
}
