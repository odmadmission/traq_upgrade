﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionStudentKits", Schema = "admission")]
    public class AdmissionStudentKit : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionId { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string kitstatus { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime kitdate { get; set; }
    }
}
