﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionCampusTour", Schema = "admission")]
   public class AdmissionCampusTour : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionCampusTourDateId { get; set; }
        [Column(TypeName = "bigint")]
        public long AdmissionCampusTourSlotId { get; set; }
        [Column(TypeName = "bigint")]
        public long AdmissionId { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string smscontent { get; set; }
        [Column(TypeName = "bit")]
        public bool IssmsSend { get; set; }
        [Column(TypeName = "bit")]
        public bool Isvisited { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> VisitedDate { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string Remarks { get; set; }
    }
}
