﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionSource", Schema = "admission")]
    public class AdmissionSource:BaseEntity
    {

        [Column(TypeName = "nvarchar(300)")]
        public string Name { get; set; }


    }
}
