﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionPersonalInterviewAssign", Schema = "admission")]
    public class AdmissionPersonalInterviewAssign : BaseEntity,IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> PIDate { get; set; }


        [Column(TypeName = "time(7)")]
        public Nullable<TimeSpan> PITime { get; set; }

        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }


        [Column(TypeName = "bit")]
        public Nullable<bool> IsAppeared { get; set; }

        [Column(TypeName = "bigint")]
        public long? Score { get; set; }

        [Column(TypeName = "bigint")]
        public long? InterviewTakenId { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Remarks { get; set; }

        [Column(TypeName = "bit")]
        public Nullable<bool> IsQualified { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> AppearedDateTime { get; set; }


        [Column(TypeName = "bigint")]
        public long AssignedCounselorId { get; set; }

        


    }
}
