﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentProficiency", Schema = "admission")]
    public class AdmissionStudentProficiency: BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionStudentID { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Sports { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string PerformingArts { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string PositionHeld { get; set; }



    }
}
