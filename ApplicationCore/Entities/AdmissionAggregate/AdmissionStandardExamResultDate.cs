﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionStandardExamResultDate", Schema = "admission")]
   public class AdmissionStandardExamResultDate:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }
        [Column(TypeName = "bigint")]
        public long BoardStandardId { get; set; }

        [Column(TypeName = "bigint")]
        public long OrganisationAcademicId { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime ExamResultDate { get; set; }
    }
}
