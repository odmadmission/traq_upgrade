﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("CounsellorStatus", Schema = "admission")]
    public class CounsellorStatus :  BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "nvarchar(300)")]
        public string Name { get; set; }

    }
}
