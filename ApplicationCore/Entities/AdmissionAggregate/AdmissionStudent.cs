﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudent", Schema = "admission")]
    public class AdmissionStudent : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionId { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string FirstName { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string MiddleName { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string LastName { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Gender { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> DateOfBirth { get; set; }
        [Column(TypeName = "bigint")]
        public long BloodGroupID { get; set; }
        [Column(TypeName = "bigint")]
        public long NatiobalityID { get; set; }
        [Column(TypeName = "bigint")]
        public long ReligionID { get; set; }
        [Column(TypeName = "bigint")]
        public long CategoryID { get; set; }
        [Column(TypeName = "bigint")]
        public long MotherTongueID { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string PassportNO { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string AadharNO { get; set; }
       
        [Column(TypeName = "varchar(300)")]
        public string Image { get; set; }


    }
}
