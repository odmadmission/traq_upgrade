﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using OdmErp.ApplicationCore.Interfaces;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionEcounselling", Schema = "admission")]
 public   class AdmissionEcounselling : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionEcounsellingDateId { get; set; }
        [Column(TypeName = "bigint")]
        public long AdmissionEcounsellingSlotId { get; set; }
        [Column(TypeName = "bigint")]
        public long AdmissionId { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string smscontent { get; set; }
        [Column(TypeName = "bit")]
        public bool? IssmsSend { get; set; }
        [Column(TypeName = "bit")]
        public bool? Isvisited { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? VisitedDate { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string Remarks { get; set; }
    }
}
