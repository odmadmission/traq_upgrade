﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("MasterEmail", Schema = "admission")]
    public class MasterEmail : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "nvarchar(500)")]
        public string Provider { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Port { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Host { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string FromEmailAddress { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string Username { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Password { get; set; }

        [Column(TypeName = "bit")]
        public bool EnableSSl { get; set; }
    }
}
