﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionStudentDocument", Schema = "admission")]
    public class AdmissionStudentDocument:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }


        [Column(TypeName = "bigint")]
        public long AdmissionDocumentId { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string FileName { get; set; }

        [Column(TypeName = "bigint")]
        public long AdmissionDocumentAssignId { get; set; }

        [Column(TypeName = "bit")]
        public bool? IsSubmitted { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? SubmittedDate { get; set; }
    }
}
