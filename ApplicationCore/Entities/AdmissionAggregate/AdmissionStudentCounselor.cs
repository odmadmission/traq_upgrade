﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentCounselor", Schema = "admission")]
    public class AdmissionStudentCounselor:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }


        // AdmissionCounsellorId
        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime AssignedDate { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> EndDate { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Remarks { get; set; }


        //
        [Column(TypeName = "bigint")]
        public long? CounsellorStatusId { get; set; }

    }
}
