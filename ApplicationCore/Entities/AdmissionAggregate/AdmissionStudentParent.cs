﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentParent", Schema = "admission")]
    public class AdmissionStudentParent:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "nvarchar(300)")]
        public string FullName { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string PrimaryMobile { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string AlternativeMobile { get; set; }
       
        [Column(TypeName = "nvarchar(300)")]
        public string EmailId { get; set; }


        [Column(TypeName = "varchar(100)")]
        public string ProfessionTypeId { get; set; }

        

        [Column(TypeName = "nvarchar(20)")]
        public string ParentRelationship { get; set; }


        [Column(TypeName = "nvarchar(300)")]
        public string Image { get; set; }

        [Column(TypeName = "nvarchar(300)")]
        public string Company { get; set; }

        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }
    }
}
