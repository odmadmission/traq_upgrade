﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStandardSetting", Schema = "admission")]
    public class AdmissionStandardSetting:BaseEntity
    {
        [Required(ErrorMessage = "Please Enter Code")]
        [Column(TypeName = "nvarchar(50)")]
        public string Code { get; set; }
        [Required(ErrorMessage = "Please Select Session")]
        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }

        [Column(TypeName = "bit")]
        public bool? Nucleus { get; set; }
        [Required(ErrorMessage = "Please Select Class")]
        [Column(TypeName = "bigint")]
        public long BoardStandardId { get; set; }

        [Required(ErrorMessage = "Please Select Wing")]
        [Column(TypeName = "bigint")]
        public long? WingId { get; set; }

        [Required(ErrorMessage = "Please Select Organization")]
        [Column(TypeName = "bigint")]
        public long? OrganizationId { get; set; } //OrganisationAcademicid

        [Column(TypeName = "bit")]
        public bool? ExamRequired { get; set; }

        [Column(TypeName = "bit")]
        public bool? PIRequired { get; set; }

        [Column(TypeName = "bit")]
        public bool? DocumentRequired { get; set; }

        [Column(TypeName = "bit")]
        public bool? ExamDatesShow { get; set; }
       

    }
}
