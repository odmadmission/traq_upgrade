﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentAcademic", Schema = "admission")]
    public class AdmissionStudentAcademic:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionStudentID { get; set; }

        [Column(TypeName = "nvarchar(300)")]
        public string OrganizationName { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string CityId { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string StateId { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string CountryId { get; set; }

        [Column(TypeName = "nvarchar(10)")]
        public string AggregatePercentageOrGrade { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string AggregatePercentageOrGradeDocument { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string ReasonForChange { get; set; }
    }
}
