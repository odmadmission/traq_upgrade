﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentContact", Schema = "admission")]
    public class AdmissionStudentContact:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }
        [Column(TypeName = "nvarchar(200)")]
        public string FullName { get; set; }
        [Column(TypeName = "nvarchar(200)")]
        public string Mobile { get; set; }
        [Column(TypeName = "nvarchar(150)")]
        public string EmailId { get; set; }

        [Column(TypeName = "nvarchar(20)")]
        public string ConatctType { get; set; }
    }
}
