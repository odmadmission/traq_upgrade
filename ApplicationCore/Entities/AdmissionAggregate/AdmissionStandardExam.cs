﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{

    //unused
    [Table("AdmissionStandardExam", Schema = "admission")]
   public class AdmissionStandardExam : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long? AcademicSessionId { get; set; }
        [Column(TypeName = "bigint")]
        public long? BoardStandardId { get; set; }

    

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ExamDate { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string ExamTimeDisplayName { get; set; }


        [Column(TypeName = "time(7)")]
        public Nullable<TimeSpan> ExamStartTime { get; set; }

        [Column(TypeName = "time(7)")]
        public Nullable<TimeSpan> ExamEndTime { get; set; }

        [Column(TypeName = "time(7)")]
        public Nullable<TimeSpan> ReachTime { get; set; }

        [Column(TypeName = "bigint")]
        public long? OrganizationId { get; set; } //organisationacademic

        [Column(TypeName = "bigint")]
        public long? NoOfStudent { get; set; }

       [Column(TypeName = "bigint")]
        public long? WingId { get; set; } //Academicstandardwing

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ResultDate { get; set; }
        
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> PublishStartDate { get; set; }
        
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> PublishEndDate { get; set; }
        [Column(TypeName = "bit")]
        public bool? IsPublishDate { get; set; }

    }
}
