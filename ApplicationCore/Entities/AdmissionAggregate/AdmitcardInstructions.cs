﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmitcardInstructions", Schema = "admission")]
    public class AdmitcardInstructions:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long AdmitcardDetailsId { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string Instruction { get; set; }
    }
}
