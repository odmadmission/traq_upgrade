﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionFee", Schema = "admission")]
    public class AdmissionFee : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }
        [Column(TypeName = "bigint")]
        public long Ammount { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string Name { get; set; }
    }
}
