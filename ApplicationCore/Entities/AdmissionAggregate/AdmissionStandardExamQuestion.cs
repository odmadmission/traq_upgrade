﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStandardExamQuestion", Schema = "admission")]
   public class AdmissionStandardExamQuestion : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }
        [Column(TypeName = "bigint")]
        public long BoardStandardId { get; set; }
        [Column(TypeName = "nvarchar(300)")]
        public string QuestionUrl { get; set; }
        [Column(TypeName = "bigint")]
        public long OrganizationId { get; set; }


    }
}
