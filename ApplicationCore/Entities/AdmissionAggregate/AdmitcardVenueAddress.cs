﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmitcardVenueAddress", Schema = "admission")]
    public class AdmitcardVenueAddress:BaseEntity
    {
        
        [Column(TypeName = "varchar(500)")]
        public string Address { get; set; }
    }
}
