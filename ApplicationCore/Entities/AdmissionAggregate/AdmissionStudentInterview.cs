﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

//unused
namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentInterview", Schema = "admission")]
    public class AdmissionStudentInterview:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }

        [Column(TypeName = "DateTime")]
        public DateTime? InterviewDate { get; set; }

        [Column(TypeName = "DateTime")]
        public DateTime? AppearedDate { get; set; }

        [Column(TypeName = "bit")]
        public bool? IsAppeared { get; set; }

        [Column(TypeName = "bit")]
        public bool? IsQualified { get; set; }

        [Column(TypeName = "nvarchar(20)")]
        public string Score { get; set; }


    }
}
