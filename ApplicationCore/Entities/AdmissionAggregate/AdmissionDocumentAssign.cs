﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionDocumentAssign", Schema = "admission")]
    public class AdmissionDocumentAssign : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }

        [Column(TypeName = "bigint")]
        public long DocumentTypeId { get; set; }

        [Column(TypeName = "bigint")]
        public long DocumentSubTypeId { get; set; }

    }
}
