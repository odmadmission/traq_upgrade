﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionInterViewer", Schema = "admission")]
    public class AdmissionInterViewer : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? StartDate { get; set; }


        [Column(TypeName = "datetime")]
        public DateTime? EndDate { get; set; }
    }
}
