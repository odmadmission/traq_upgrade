﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmitcardDetails", Schema = "admission")]
    public class AdmitcardDetails : BaseEntity
    {
        [Column(TypeName = "varchar(500)")]
        public string LogoName { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string Address { get; set;}
        [Column(TypeName = "varchar(20)")]
        public string Tollfree { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string Title { get; set; } 
        [Column(TypeName = "varchar(max)")]
        public string Remarks { get; set; }
        [Column(TypeName = "bigint")]
        public long OrganisationAcademicId { get; set; }
    }
}
