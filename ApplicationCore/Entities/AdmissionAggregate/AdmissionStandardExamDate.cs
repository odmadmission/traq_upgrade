﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionStandardExamDate", Schema = "admission")]
    public class AdmissionStandardExamDate : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }
        [Column(TypeName = "bigint")]
        public long AcademicStandardId { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ExamDate { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ResultDate { get; set; }



    }
}
