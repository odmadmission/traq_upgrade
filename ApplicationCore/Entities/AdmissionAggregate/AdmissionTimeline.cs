﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionTimeline", Schema = "admission")]
    public class AdmissionTimeline : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long AdmissionId { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Timeline { get; set; }

    }
}
