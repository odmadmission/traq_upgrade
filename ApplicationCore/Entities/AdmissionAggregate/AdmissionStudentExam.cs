﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentExam", Schema = "admission")]
    public class AdmissionStudentExam:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }

        //unused
        [Column(TypeName = "DateTime")]
        public Nullable<DateTime> ExamDate { get; set; }
        //un used
        [Column(TypeName = "DateTime")]
        public Nullable<DateTime> AppearedDate { get; set; }
        [Column(TypeName = "bit")]
        public bool? IsAppeared { get; set; }

        [Column(TypeName = "bit")]
        public bool? IsQualified { get; set; }

        [Column(TypeName = "nvarchar(20)")]
        public string Score  { get; set; }
        [Column(TypeName = "bigint")]
        public long? AdmissionStandardExamId { get; set; }

        //unused
        [Column(TypeName = "datetime")]
        public DateTime? ResultDate { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string MarksheetFileName { get; set; }


        [Column(TypeName = "bigint")]
        public long? AdmissionStandardExamResultDateId { get; set; }

    }
}
