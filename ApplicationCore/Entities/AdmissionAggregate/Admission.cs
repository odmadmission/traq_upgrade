﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("Admission", Schema = "admission")]
    public class Admission:BaseEntity
    {
        [Column(TypeName = "varchar(40)")]
        public string FormNumber { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicSourceId { get; set; }

        [Column(TypeName = "bigint")]
        public long AdmissionSlotId { get; set; }



        [Column(TypeName = "varchar(15)")]
        public string MobileNumber { get; set; }

        [Column(TypeName = "bit")]
        public bool IsAdmissionFormAmountPaid { get; set; }

        [Column(TypeName = "bit")]
        public bool IsAdmission { get; set; }
      

        [Column(TypeName = "bit")]
        public bool IsPersonalInterviewAppeared { get; set; }

        [Column(TypeName = "bit")]
        public bool? IsExamQualified { get; set; }


        [Column(TypeName = "bit")]
        public bool IsPersonalInterviewQualified { get; set; }

        [Column(TypeName = "bit")]
        public bool IsDocumentsSubmitted { get; set; }


        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
      
        public Guid LeadReferenceId { get; set; }

        
        public Guid? LeadId { get; set; }

      
    }
}
