﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using OdmErp.ApplicationCore.Interfaces;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionCampusTourDate", Schema = "admission")]
   public class AdmissionCampusTourDate : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CampusTourDate { get; set; }
    }
}
