﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using OdmErp.ApplicationCore.Interfaces;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionEcounsellingTimeline", Schema = "admission")]
  public  class AdmissionEcounsellingTimeline : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionEcounsellingId { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string Remarks { get; set; }
    }
}
