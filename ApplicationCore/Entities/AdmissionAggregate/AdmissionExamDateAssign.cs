﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionExamDateAssign", Schema = "admission")]
    public class AdmissionExamDateAssign : BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }


        [Column(TypeName = "bigint")]
        public long AdmissionStandardExamId { get; set; }


        [Column(TypeName = "bigint")]
        public long? RollNo { get; set; }

    }
}
