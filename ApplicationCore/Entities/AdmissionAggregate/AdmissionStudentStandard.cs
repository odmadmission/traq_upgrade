﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentStandard", Schema = "admission")]
    public class AdmissionStudentStandard : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }
        [Column(TypeName = "bigint")]
        public long BoardId { get; set; }

        [Column(TypeName = "bigint")]
        public long StandardId { get; set; }

        [Column(TypeName = "bigint")]
        public long WingId { get; set; }

        [Column(TypeName = "bigint")]
        public long OrganizationId { get; set; }

        [Column(TypeName = "bit")]
        public bool Nucleus { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicStandardWindId { get; set; }

    }
}
