﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("MasterSMS", Schema = "admission")]
    public class MasterSMS : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "nvarchar(500)")]
        public string Host { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string Provider { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string ApiKey { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string SenderId { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Route { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Mobile { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Message { get; set; }
    }
}
