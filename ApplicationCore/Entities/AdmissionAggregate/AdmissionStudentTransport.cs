﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentTransport", Schema = "admission")]
    public class AdmissionStudentTransport:BaseEntity, IAggregateRoot
    {

        
            [Column(TypeName = "bigint")]
            public long AdmissionStudentId { get; set; }
            [Column(TypeName = "bit")]
            public bool IsTransport { get; set; }
            [Column(TypeName = "varchar(500)")]
            public string LocationId { get; set; }
            [Column(TypeName = "decimal(18, 2)")]
            public decimal Distance { get; set; }
            
        
    }
}
