
using OdmErp.ApplicationCore.Interfaces;
using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{

    [Table("AdmissionFormPayment", Schema = "admission")]
    public class AdmissionFormPayment:BaseEntity, IAggregateRoot

    {

        [Column(TypeName = "nvarchar(50)")]
        public string TransactionId { get; set; }

        [Column(TypeName = "bigint")]
        public long PaymentModeId { get; set; }

        [Column(TypeName = "bigint")]
        public long AdmissionId { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal PaidAmount { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime PaidDate { get; set; }


        [Column(TypeName = "bigint")]
        public long ReceivedId { get; set; }
        [Column(TypeName = "bigint")]
        public long AdmissionFeeId { get; set; }

        [Column(TypeName = "nvarchar(300)")]
        public string PaymentData { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        public string PaymentGatewayProxyId { get; set; }

    }
}
