﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentAddress", Schema = "admission")]
    public class AdmissionStudentAddress:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(400)")]
        public string AddressLine1 { get; set; }
        [Column(TypeName = "varchar(400)")]
        public string AddressLine2 { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string PostalCode { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string CityId { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string StateId { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string CountryId { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string AddressType { get; set; }

        [Column(TypeName = "bigint")]
        public long AdmissionStudentId { get; set; }
    }
}
