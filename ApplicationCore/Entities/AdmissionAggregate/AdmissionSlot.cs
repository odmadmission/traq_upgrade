﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionSlot", Schema = "admission")]
    public class AdmissionSlot : BaseEntity
    {
        [Column(TypeName = "nvarchar(50)")]
        public string SlotName { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? StartDate { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? EndDate { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }

    }
}
