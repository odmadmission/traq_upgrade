﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmissionStudentDeclaration", Schema = "admission")]
    public class AdmissionStudentDeclaration : BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bit")]
        public bool? IsDuaghter { get; set; }

        [Column(TypeName = "bit")]
        public bool? IsSon { get; set; }

        [Column(TypeName = "bigint")]
        public long? ClassId { get; set; }


        [Column(TypeName = "bigint")]
        public long? WingId { get; set; }

        [Column(TypeName = "bit")]
        public bool? IsFather { get; set; }

        [Column(TypeName = "bit")]
        public bool? IsMother { get; set; }

        [Column(TypeName = "bit")]
        public bool? IsGuardian { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string StudentName { get; set; }

        [Column(TypeName = "nvarchar(300)")]
        public string SignatureFileName { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string ParentName { get; set; }

        [Column(TypeName = "bigint")]
        public long AdmissionId { get; set; }

    }
}
