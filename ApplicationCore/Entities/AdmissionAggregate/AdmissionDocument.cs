﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    [Table("AdmissionDocument", Schema = "admission")]
    public class AdmissionDocument:BaseEntity,IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AdmissionSessionId { get; set; }

        [Column(TypeName = "bigint")]
        public long DocumentTypeId { get; set; }

        [Column(TypeName = "bigint")]
        public long DocumentSubTypeId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicStandardId { get; set; }
    }
}
