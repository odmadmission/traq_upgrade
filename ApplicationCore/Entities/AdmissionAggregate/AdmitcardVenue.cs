﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("AdmitcardVenue", Schema = "admission")]
    public class AdmitcardVenue :BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long AdmitcardDetailsId { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string OrganisationName { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string Address { get; set; }
    }
}
