﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace OdmErp.ApplicationCore.Entities.SessionFeesType
{
    public class SessionFeesType:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long SessionId { get; set; }
        [Column(TypeName = "bigint")]
        public long FeesTypeId { get; set; }
        [Column(TypeName = "bigint")]
        public long ApplicableId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Deadlinedate { get; set; }
        [Column(TypeName = "bit")]
        public bool IsMandatory { get; set; }
    }
    //public class SessionFeesTypeView
    //{
    //    public long SessionId { get; set; }
    //    public long FeesTypeId { get; set; }
    //    public long ParentFeesTypeId { get; set; }
    //    public long ApplicableId { get; set; }
    //    public string FeesTypeName { get; set; }
    //    public string SessionName { get; set; }
    //    public string ParentFeesType { get; set; }
    //    public string ApplicableTo { get; set; }
    //    public bool Mandatory { get; set; }
    //    public DateTime DeadlineDate { get; set; }
    //    public bool Active { get; set; }
    //    // public IPagedList<SessionFeesTypeViewModel> SessionFeesType { get; set; }
    //}
}
