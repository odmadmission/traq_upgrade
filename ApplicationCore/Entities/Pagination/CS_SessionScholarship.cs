﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities
{
    public class CS_SessionScholarship
    {
        public long id { get; set; }
        public string sessionName { get; set; }
        public string scholarshipName { get; set; }
        public string status { get; set; }
    }
}
