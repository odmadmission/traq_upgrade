﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
   public class PaginationSessionScholarshipDiscountPrice
    {
        public string draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<SessionScholarshipDiscountPrice> data { get; set; }
    }
}
