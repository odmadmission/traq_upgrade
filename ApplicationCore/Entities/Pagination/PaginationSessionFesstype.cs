﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.Pagination
{
    public class PaginationSessionFesstype
    {
        public string draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        //public List<SessionFeesType.SessionFeesTypeView> data { get; set; }
    }
}
