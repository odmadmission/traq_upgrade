﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class PaginationSessionScholarshipDiscountApplicable
    {
        public string draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<SessionScholarshipDiscountApplicable> data { get; set; }
    }
}
