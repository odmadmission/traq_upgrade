﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class PaginationSessionScholarshipDiscount
    {
        public string draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<SessionScholarshipDiscount> data { get; set; }
    }
}
