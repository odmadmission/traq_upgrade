﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities
{
    public class Access :BaseEntity
    {
        [Column(TypeName = "varchar(200)")]
        public string Username { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string Password { get; set; }
        [Column(TypeName = "bigint")]
        public long RoleID { get; set; }

        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }


        [Column(TypeName = "bigint")]
        public long AccessTypeID { get; set; }

    }
}
