﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
   public class EmployeeGrievance: BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }
        [Column(TypeName = "bigint")]
        public long GrievanceID { get; set; }
      


    }
}
