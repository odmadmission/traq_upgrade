﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievanceForward : BaseEntity,IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long GrievanceId { get; set; }
        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }
        [Column(TypeName = "bigint")]
        public long DepartmentId { get; set; }
        [Column(TypeName = "bigint")]
        public long? StatusId { get; set; }
        [Column(TypeName = "bit")]
        public bool? IsSubAssigned { get; set; }
    }
}
