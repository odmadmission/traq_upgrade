﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievanceAttachment: BaseEntity
    {


        [Column(TypeName = "bigint")]
        public long GrievanceID { get; set; }
      

        [Column(TypeName = "varchar(max)")]
        public string Path { get; set; }


        [Column(TypeName = "varchar(max)")]
        public string Name { get; set; }

        [Column(TypeName = "bigint")]
        public long RoleId { get; set; }


    }
}
