﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievancePriority:BaseEntity
    {

        [Column(TypeName = "varchar(max)")]
        public string Name { get; set; }


        [Column(TypeName = "varchar(max)")]
        public string TimeText { get; set; }

        [Column(TypeName = "bigint")]
        public int TimeValue { get; set; }
    }
}
