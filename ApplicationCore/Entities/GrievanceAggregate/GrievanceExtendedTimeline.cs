﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievanceExtendedTimeline : BaseEntity,IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long GrievanceId { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ExtendedDate { get; set; }

        [Column(TypeName = "time")]
        public Nullable<TimeSpan> ExtendedTime { get; set; }

        [Column(TypeName = "varchar(500)")]
        public string ExtendedReason { get; set; }
    }
}
