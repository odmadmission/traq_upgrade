﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievanceCommentAttachment: BaseEntity
    {


        [Column(TypeName = "bigint")]
        public long GrievanceCommentID { get; set; }


        [Column(TypeName = "varchar(max)")]
        public string Path { get; set; }


        [Column(TypeName = "varchar(max)")]
        public string Name { get; set; }




    }
}
