﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class DataTimeline : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long EntityId { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string EntityName { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string Timeline { get; set; }
    }
}
