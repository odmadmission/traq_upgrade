﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
   public class GrievanceStatus :BaseEntity
    {
     
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }

    }
}
