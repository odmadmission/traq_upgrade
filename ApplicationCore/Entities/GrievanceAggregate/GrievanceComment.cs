﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievanceComment:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long GrievanceID { get; set; }
        [Column(TypeName = "bigint")]
        public long RoleId { get; set; }

        [Column(TypeName = "varchar(max)")]
        public string Description { get; set; }

        [Column(TypeName = "bigint")]
        public long GrievanceStatusID { get; set; }

        [Column(TypeName = "bit")]
        public bool IsRead { get; set; }


    }
}


