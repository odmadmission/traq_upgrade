﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievanceExtendedTimelineAttachment :BaseEntity,IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long GrievanceId { get; set; }

        [Column(TypeName = "varchar(200)")]
        public string Path { get; set; }

        [Column(TypeName = "varchar(200)")]
        public string Name { get; set; }
    }
}
