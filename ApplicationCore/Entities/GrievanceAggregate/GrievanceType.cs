﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
   public class GrievanceType: BaseEntity
    {
        [Column(TypeName = "varchar(max)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        [Required(ErrorMessage = "Please Choose Category")]
        public long CategoryId { get; set; }
        [Column(TypeName = "bigint")]
        public long RoleID { get; set; }
    }
}
