﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievanceAccessTimeline : BaseEntity,IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long GrievanceAccessId { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> StartDate { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> EndDate { get; set; }
    }
}
