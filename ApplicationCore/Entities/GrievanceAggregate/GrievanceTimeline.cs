﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievanceTimeline:BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long GrievanceID { get; set; }
        [Column(TypeName = "bigint")]
        public long RoleId { get; set; }

        [Column(TypeName = "bigint")]
        public long GrievanceStatusID { get; set; }

        [Column(TypeName = "bit")]
        public bool IsForwarded { get; set; }

        [Column(TypeName = "bigint")]
        public long? ForwardToId { get; set; }

        [Column(TypeName = "bigint")]
        public long? ForwardToDept { get; set; } 
        [Column(TypeName = "varchar(MAX)")]
        public string Remarks { get; set; }

    }
}
