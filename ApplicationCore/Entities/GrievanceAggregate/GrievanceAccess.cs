﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievanceAccess : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long GrievanceCategoryId { get; set; }
        [Column(TypeName = "bigint")]
        public long GrievanceTypeId { get; set; }
        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }
    }
}
