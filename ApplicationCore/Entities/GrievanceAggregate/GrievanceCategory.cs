﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Linq;


namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
    public class GrievanceCategory:BaseEntity
    {

        [Column(TypeName = "varchar(max)")]
        public string Name { get; set; }

    }
}
