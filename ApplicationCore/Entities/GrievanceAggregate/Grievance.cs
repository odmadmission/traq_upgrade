﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.GrievanceAggregate
{
  public class Grievance : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long GrievanceCategoryID { get; set; }

        [Column(TypeName = "bigint")]
        public long GrievanceTypeID { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string Code { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Description { get; set; }

        [Column(TypeName = "bigint")]
        public long GrievanceStatusID { get; set; }

        [Column(TypeName = "DateTime")]
        public Nullable<DateTime> CompletionDate { get; set; }


        [Column(TypeName = "bigint")]
        public long CompletionId { get; set; }



        [Column(TypeName = "DateTime")]
        public Nullable<DateTime> VerificationDate { get; set; }


        [Column(TypeName = "bigint")]
        public long VerifiedId { get; set; }


        [Column(TypeName = "varchar(MAX)")]
        public string RequestType { get; set; }



        [Column(TypeName = "bigint")]
        public long GrievancePriorityID { get; set; }


        [Column(TypeName = "bigint")]
        public long GrievanceRelatedId { get; set; }

        [Column(TypeName = "bigint")]
        public long RoleId { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ProgressDate { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ScoreGivenDate { get; set; }

        [Column(TypeName = "decimal")]
        public decimal Score { get; set; }

        [Column(TypeName = "bigint")]
        public long VerifiedRoleId { get; set; }

        [Column(TypeName = "bit")]
        public bool IsRequestedForStudentDetails { get; set; }

        [Column(TypeName = "bigint")]
        public long RequestedId { get; set; }

        [Column(TypeName = "bit")]
        public bool IsApproveForStudentDetails { get; set; }

        [Column(TypeName = "bigint")]
        public long ApprovedId { get; set; }

        [Column(TypeName = "bit")]
        public bool IsRejectedForStudentDetails { get; set; }

        [Column(TypeName = "bit")]
        public bool IsGrievanceRejected { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string RejectReason { get; set; }

        [Column(TypeName = "nvarchar(300)")]
        public string VerifyRemark { get; set; }
    }
}



