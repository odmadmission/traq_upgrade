﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities
{
    public class Parent:BaseEntity
    {

        [Column(TypeName = "varchar(300)")]
        public string FirstName { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string MiddleName { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string LastName { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string PrimaryMobile { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string AlternativeMobile { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string LandlineNumber { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string EmailId { get; set; }


        [Column(TypeName = "bigint")]
        public long ProfessionTypeID { get; set; }

        //[Column(TypeName = "varchar(300)")]
        //public string Organization { get; set; }

        [Column(TypeName = "bigint")]
        public long StudentID { get; set; }

        [Column(TypeName = "bigint")]
        public long ParentRelationshipID { get; set; }


        [Column(TypeName = "varchar(300)")]
        public string Image { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string ParentOrganization { get; set; }


      
      


        //[Column(TypeName = "varchar(10)")]
        //public string ParentCode { get; set; }
    }
}
