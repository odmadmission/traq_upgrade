﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.StudentAggregate
{
    public class StudentAchievement:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long StudentID { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string Sports { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string PerformingArts { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string PositionHeld { get; set; }

    }
}
