﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.StudentAggregate
{
   public class StudentTransportation: BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long StudentID { get; set; }
        [Column(TypeName = "bit")]
        public bool IsTransport { get; set; }
        [Column(TypeName = "bigint")]
        public long LocationID { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Distance { get; set; }
        [Column(TypeName = "bigint")]
        public long DepartureTimeID { get; set; }

    }
}
