﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities.StudentAggregate
{
    
        public class StudentClass : BaseEntity
        {
        [Column(TypeName = "bigint")]
        public long StudentID { get; set; }
        [Column(TypeName = "bigint")]
        public long StandardID { get; set; }
        [Column(TypeName = "bigint")]
        public long SectionID { get; set; }
        //schoolwingid
        [Column(TypeName = "bigint")]
        public long WingID { get; set; }
        [Column(TypeName = "bigint")]
        public long OrganizationID { get; set; }
        [Column(TypeName = "bit")]
        public bool IsCurrent { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicSessionID { get; set; }
    }
}
