﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;


namespace OdmErp.ApplicationCore.Entities.StudentAggregate
{
   public class StudentDocumentAttachment : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long StudentRequiredDocumentID { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string DocumentPath { get; set; }
    }
}
