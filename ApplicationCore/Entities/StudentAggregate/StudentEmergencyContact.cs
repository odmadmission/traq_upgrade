﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.StudentAggregate
{
   public class StudentEmergencyContact: BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long StudentID { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string ContactPersonName { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string Mobile { get; set; }
        [Column(TypeName = "varchar(800)")]
        public string EmailId { get; set; }       
    }
}
