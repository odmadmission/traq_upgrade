﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.StudentAggregate
{
    public class StudentLanguagesKnown : BaseEntity,IAggregateRoot
    {
        public long StudentID { get; set; }
        public long LanguageID { get; set; }
    }
}
