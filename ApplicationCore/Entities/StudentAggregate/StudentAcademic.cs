﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.StudentAggregate
{
    public class StudentAcademic : BaseEntity
    {
        [Column(TypeName = "varchar(300)")]
        public string OrganizationName { get; set; }

        [Column(TypeName = "int")]
        public int BoardID { get; set; }

        [Column(TypeName = "int")]
        public int ClassID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime FromDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ToDate { get; set; }
        [Column(TypeName = "bigint")]
        public long StudentID { get; set; }


        [Column(TypeName = "nvarchar(500)")]
        public string ReasonForChange { get; set; }
        [Column(TypeName = "nvarchar(500)")]
        public string GradeOrPercentage { get; set; }
        [Column(TypeName = "int")]
        public int? CityId { get; set; }

        [Column(TypeName = "int")]
        public int? StateId { get; set; }

     

    }
}
