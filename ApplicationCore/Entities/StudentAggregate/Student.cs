﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities
{
    public class Student : BaseEntity
    {

        [Column(TypeName = "varchar(300)")]
        public string StudentCode { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string FirstName { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string MiddleName { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string LastName { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Gender { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateOfBirth { get; set; }
        [Column(TypeName = "bigint")]
        public long BloodGroupID { get; set; }
        [Column(TypeName = "bigint")]
        public long NatiobalityID { get; set; }
        [Column(TypeName = "bigint")]
        public long ReligionID { get; set; }
        [Column(TypeName = "bigint")]
        public long CategoryID { get; set; }
        [Column(TypeName = "bigint")]
        public long MotherTongueID { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string PassportNO { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string AadharNO { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string PrimaryMobile { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string WhatsappMobile { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string EmailID { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string ZoomMailID { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string Image { get; set; }




    }
}
