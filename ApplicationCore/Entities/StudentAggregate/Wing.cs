﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities.StudentAggregate
{
    public class Wing : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(max)")]
        public string Name { get; set; }
    }
}
