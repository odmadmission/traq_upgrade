﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities
{
    public class ActivityTimeLine
    {

        [Column(TypeName = "bigint")]
        public long ActvityId { get; set; }


        [Column(TypeName = "varchar(MAX)")]
        public string ActvityType { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Action { get; set; }


    }
}
