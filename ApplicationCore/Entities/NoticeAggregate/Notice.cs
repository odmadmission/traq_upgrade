﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
   public class Notice : BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "varchar(100)")]
        public string SendType { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string SendSubType { get; set; }
        [Column(TypeName = "varchar(MAX)")]      
        public string Message { get; set; }
      
    }
}
