﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class NoticeData : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long NoticeId { get; set; }
        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }
        [Column(TypeName = "bigint")]
        public long StudentId { get; set; }
    }
}
