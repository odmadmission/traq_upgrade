﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
  public  class UserGroupEmployee : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long UserGroupId { get; set; }

        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }
    }
}
