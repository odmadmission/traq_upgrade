﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
  public  class UserGroup : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string GroupName { get; set; }
        [Column(TypeName = "text")]
        public string Description { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Icon { get; set; }


    }
}
