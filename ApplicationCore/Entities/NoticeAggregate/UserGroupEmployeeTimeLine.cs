﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
  public  class UserGroupEmployeeTimeLine : BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long UserGroupEmployeeID { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? StartDate { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? EndDate { get; set; }

        [Column(TypeName = "bit")]
        public bool IsLeft { get; set; }
    }
}
