﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
     public class Login : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(200)")]
        public string Type { get; set; }
        [Column(TypeName = "bigint")]
        public long AccessID { get; set; }
        [Column(TypeName = "text")]
        public string FCMID { get; set; }
        [Column(TypeName = "text")]
        public string Data { get; set; }

        [Column(TypeName = "bigint")]
        public long RelatedId { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string RelatedName { get; set; }

    }
}
