﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class PeriodSyllabus:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long PeriodEmployeeStatusId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicSyllabusId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicConceptId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicChapterId { get; set; }

     

    }
}
