﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class AcademicStudent:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long StudentId { get; set; }
        [Column(TypeName = "bigint")]
        public long AcademicStandardId { get; set; }
        [Column(TypeName = "bigint")]
        public long? AcademicSubjectCombinationId { get; set; }
        [Column(TypeName = "bigint")]
        public long? AcademicStandardWingId { get; set; }
        [Column(TypeName = "bit")]
        public bool Nucleus { get; set; }
    }
}
