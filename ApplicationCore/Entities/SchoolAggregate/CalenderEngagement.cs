﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SchoolAggregate
{
    public class CalenderEngagement : BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long CalenderEngagementTypeId { get; set; }

        [Column(TypeName = "date")]
        public DateTime Day { get; set; }


        [Column(TypeName = "bigint")]
        public long AcademicCalenderId { get; set; }


        [Column(TypeName = "varchar(255)")]
        public string Name { get; set; }

    }
}
