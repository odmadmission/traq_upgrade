﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class PeriodEmployeeStatus:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long PeriodEmployeeId { get; set; }

        [Column(TypeName = "bigint")]
        public long TakenEmployeeId { get; set; }


        [Column(TypeName = "varchar(MAX)")]
        public string Reason { get; set; }

    }
}
