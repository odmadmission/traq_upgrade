﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class Subject:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "varchar(200)")]
        public string Name { get; set; }

        [Column(TypeName = "bit")]
        public bool IsSubSubject { get; set; }

        [Column(TypeName = "bigint")]
        public long ParentSubjectId { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string SubjectCode { get; set; }    

     
        [Column(TypeName = "bigint")]
        public long BoardStandardId { get; set; }

     

        [Column(TypeName = "bit")]
        public bool IsOptional { get; set; }
    }
}
