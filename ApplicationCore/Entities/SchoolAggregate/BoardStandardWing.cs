﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SchoolAggregate
{
   public class BoardStandardWing : BaseEntity, IAggregateRoot
    {
        public long BoardStandardID { get; set; }
        public long WingID { get; set; }
    }
}
