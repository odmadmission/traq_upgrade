﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class OrganizationAcademic:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }

        [Column(TypeName = "bigint")]
        public long OrganizationId { get; set; }
    }
}
