﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SchoolAggregate
{
    public class AcademicCalender:BaseEntity,IAggregateRoot
    {
        public long SchoolWingId { get; set; }

        public long AcademicStandardId { get; set; }

    }
}
