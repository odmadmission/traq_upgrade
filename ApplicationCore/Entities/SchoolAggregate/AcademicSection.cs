﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class AcademicSection:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicStandardId { get; set; }

        [Column(TypeName = "bigint")]
        public long SectionId { get; set; }

        //[Column(TypeName = "bigint")]
        //public long AcademicStandardStreamId { get; set; }

    }
}
