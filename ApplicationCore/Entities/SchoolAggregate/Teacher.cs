﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SchoolAggregate
{
    public class Teacher : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }
        [Column(TypeName = "bigint")]
        public long OrganizationId { get; set; }

        [Column(TypeName = "bigint")]
        public long GroupId { get; set; }

       
       
    }
}
