﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class Chapter:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(200)")]
        public string Name { get; set; }

        [Column(TypeName = "bit")]
        public bool IsSubChapter { get; set; }

        [Column(TypeName = "bigint")]
        public long ParentChapterId { get; set; }

        [Column(TypeName = "bigint")]
        public long SubjectId { get; set; }

    }
}
