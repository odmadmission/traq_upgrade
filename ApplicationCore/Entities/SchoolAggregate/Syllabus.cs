﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class Syllabus:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(MAX)")]
        public string Title { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

    }
}
