﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SchoolAggregate
{
    public class AcademicStandardWing : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicStandardId { get; set; }
        
        [Column(TypeName = "bigint")]
        public long? WingID { get; set; }
        [Column(TypeName = "bigint")]
        public long BoardStandardWingID { get; set; }
    }
}
