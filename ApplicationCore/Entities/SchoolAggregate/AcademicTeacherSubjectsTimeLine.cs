﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SchoolAggregate
{
  public  class AcademicTeacherSubjectsTimeLine : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicTeacherSubjectId { get; set; }

        [Column(TypeName = "datetime")]
        [Required]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> EndDate { get; set; }
       
        
    }
}
