﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class AcademicSession : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(200)")]
        public string DisplayName { get; set; }

        [Column(TypeName = "date")]
        public Nullable<DateTime> Start { get; set; }

        [Column(TypeName = "date")]
        public Nullable<DateTime> End { get; set; }

        [Column(TypeName = "bit")]
        public bool IsAdmission { get; set; }


    }
}
