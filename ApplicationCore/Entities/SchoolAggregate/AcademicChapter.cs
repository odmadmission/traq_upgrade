﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class AcademicChapter:BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long ChapterId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicSubjectId { get; set; }


    }
}
