﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class AcademicTeacher:BaseEntity,IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AcademicSectionId { get; set; }


        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }

        [Column(TypeName = "bigint")]
        public long TeacherId { get; set; }

        [Column(TypeName = "bigint")]
        public long WingId { get; set; }

      




    }
}
