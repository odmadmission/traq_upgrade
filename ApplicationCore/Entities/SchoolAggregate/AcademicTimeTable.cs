﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class AcademicTimeTable:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AcademicSectionId { get; set; }    
        [Column(TypeName = "varchar(100)")]
        public string Code { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string NameOfMonth { get; set; }
        [Column(TypeName = "bigint")]
        public long AcademicStandardId { get; set; }
        //[Column(TypeName = "bigint")]
        //public long? AcademicStandardStreamId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime AssignDate { get; set; }
    }
}
