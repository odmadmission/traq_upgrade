﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class Concept:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "varchar(MAX)")]
        public string Name { get; set; }

        [Column(TypeName = "bit")]
        public bool IsSubConcept { get; set; }

        [Column(TypeName = "bigint")]
        public long ParentConceptId { get; set; }

        [Column(TypeName = "bigint")]
        public long ChapterId { get; set; }
    }
}
