﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
   public class AcademicStudentSubjectOptional : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicStudentID { get; set; }
        [Column(TypeName = "bigint")]
        public long AcademicSubjectOptionalID { get; set; }
        [Column(TypeName = "bigint")]
        public long AcademicSubjectOptionalDetailID { get; set; }
        
    }
}
