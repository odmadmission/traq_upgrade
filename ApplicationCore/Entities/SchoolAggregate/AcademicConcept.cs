﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class AcademicConcept:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long ConceptId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicChapterId { get; set; }
    }
}
