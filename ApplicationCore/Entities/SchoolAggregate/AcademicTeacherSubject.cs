﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SchoolAggregate
{
  public  class AcademicTeacherSubject : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long TeacherId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicSubjectId { get; set; }
        [Column(TypeName = "bigint")]
        public long AcademicSectionId { get; set; }
    }
}
