﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SchoolAggregate
{
    public class CalenderEngagementType:BaseEntity,IAggregateRoot
    {

        [Column(TypeName = "varchar(250)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(10)")]
        public string Code { get; set; }

        [Column(TypeName = "bit")]
        public bool IsWorkingDay { get; set; }

        [Column(TypeName = "bit")]
        public bool IsHoliday { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string Type { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string BackgroundColor { get; set; }

        [Column(TypeName = "varchar(50)")]
        public string TextColor { get; set; }

    }
}
