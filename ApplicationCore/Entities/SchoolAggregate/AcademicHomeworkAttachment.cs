﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities

{
    public class AcademicHomeworkAttachment:BaseEntity,IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AcademicHomeworkId { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string AttachmentName { get; set; }

        [Column(TypeName = "varchar(255)")]
        public string Type { get; set; }
    }
}
