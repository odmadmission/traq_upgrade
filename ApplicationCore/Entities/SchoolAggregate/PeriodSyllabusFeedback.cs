﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class PeriodSyllabusFeedback:BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long PeriodSyllabusId { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string Feedback { get; set; }
        [Column(TypeName = "int")]
        public int Rating { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicStudentId { get; set; }
    }
}
