﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SchoolAggregate
{
    public class ClassTiming : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "time(7)")]
        public TimeSpan StartTime { get; set; }
        [Column(TypeName = "time(7)")]
        public TimeSpan EndTime { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string Name { get; set; }
    }
}
