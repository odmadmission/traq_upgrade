﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class AcademicStandard: BaseEntity, IAggregateRoot
    {
       

        [Column(TypeName = "bigint")]
        public long BoardStandardId { get; set; }

        [Column(TypeName = "bigint")]
        public long OrganizationAcademicId { get; set; }

        //[Column(TypeName = "bigint")]
        //public long BoardStandardWingID { get; set; }


    }
}
