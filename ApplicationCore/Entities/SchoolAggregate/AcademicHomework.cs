﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class AcademicHomework:BaseEntity,IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long AcademicChapterId { get; set; }

        [Column(TypeName = "text")]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        public string Desctiption { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime HomeworkDate { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicSectionId { get; set; }
    }
}
