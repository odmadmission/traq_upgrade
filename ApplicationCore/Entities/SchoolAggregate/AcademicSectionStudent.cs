﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class AcademicSectionStudent:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AcademicStudentId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicSectionId { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string StudentCode { get; set; }

    }
}
