﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class TimeTablePeriod:BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long AcdemicTimeTableId { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string NameOfDay { get; set; }
        [Column(TypeName = "bigint")]
        public long ClassTimingId { get; set; }
        
        [Column(TypeName = "varchar(100)")]
        public string Code { get; set; }


    }
}
