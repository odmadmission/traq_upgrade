﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class BoardStandard:BaseEntity,IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long BoardId { get; set; }

        [Column(TypeName = "bigint")]
        public long StandardId { get; set; }

        //[Column(TypeName = "bigint")]
        //public long StreamId { get; set; }

     
    }
}
