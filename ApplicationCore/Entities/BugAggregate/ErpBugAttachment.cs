﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class ErpBugAttachment : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(300)")]
        public long Attachment { get; set; }
        [Column(TypeName = "bigint")]
        public long ErpBugID { get; set; }
    }
}
