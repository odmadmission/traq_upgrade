﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class ErpBug : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long ModuleID { get; set; }
        [Column(TypeName = "bigint")]
        public long SubModuleID { get; set; }
        [Column(TypeName = "bigint")]
        public long ActionID { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string Title { get; set; }
        [Column(TypeName = "text")]
        public string Description { get; set; }
        [Column(TypeName = "bigint")]
        public Nullable<long> AssignToID { get; set; }
        [Column(TypeName = "bigint")]
        public Nullable<long> AssignFromID { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ComplitionDate { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> VarifiedDate { get; set; }

    }
}
