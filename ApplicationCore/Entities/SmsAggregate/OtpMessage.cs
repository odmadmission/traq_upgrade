﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class OtpMessage : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(200)")]
        public string OtpType { get; set; }
        [Column(TypeName = "varchar(15)")]
        public string Mobile { get; set; }

        [Column(TypeName = "bigint")]
        public long AccessID { get; set; }

        [Column(TypeName = "varchar(10)")]
        public string SentOtpCode { get; set; }

        [Column(TypeName = "varchar(10)")]
        public string RecievedOtpCode { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime ValidTill { get; set; }
        [Column(TypeName = "varchar(200)")]
        public string AppType { get; set; }
    }
}
