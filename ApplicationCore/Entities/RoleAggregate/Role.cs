﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities
{
    public class Role:BaseEntity
    {

        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }

    }
}
