﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.ModuleAggregate
{
   public class ActionAcademic : BaseEntity
    {
        [Column(TypeName = "varchar(200)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        public long ActionID { get; set; }
        [Column(TypeName = "bigint")]
        public long ModuleID { get; set; }
        [Column(TypeName = "bigint")]
        public long SubModuleID { get; set; }
        [Column(TypeName = "bigint")]
        public long AcademicStandardID { get; set; }
        [Column(TypeName = "bigint")]
        public long AcademicStandardStreamID { get; set; }
    }
}
