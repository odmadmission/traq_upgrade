﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities.ModuleAggregate
{

    public class ActionDepartment:BaseEntity
    {

        [Column(TypeName = "varchar(200)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        public long ActionID { get; set; }
        [Column(TypeName = "bigint")]
        public long ModuleID { get; set; }
        [Column(TypeName = "bigint")]
        public long SubModuleID { get; set; }
        [Column(TypeName = "bigint")]
        public long DepartmentID { get; set; }

    }
}
