﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities
{
    public class SubModule:BaseEntity
    {
        [Column(TypeName = "bit")]
        public bool IsAvailable { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        public long ModuleID { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string Url { get; set; }
       
        //[Column(TypeName = "varchar(300)")]
        //public string DisplayName { get; set; }


    }
}
