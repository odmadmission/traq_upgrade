﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities.ModuleAggregate
{
   public class EmployeeActionDepartment:BaseEntity
    {


        [Column(TypeName = "bigint")]
        public long ActionDepartmentID { get; set; }

        [Column(TypeName = "bigint")]
        public long AccessID { get; set; }



    }
}
