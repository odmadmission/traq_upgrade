﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities
{
    public class ActionAccess:BaseEntity
    {

        [Column(TypeName = "varchar(200)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        public long ActionID { get; set; }
        [Column(TypeName = "bigint")]
        public long ModuleID { get; set;  }
        [Column(TypeName = "bigint")]
        public long SubModuleID { get; set; }
        [Column(TypeName = "bigint")]
        public long AccessID { get; set; }



    }
}
