﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities.DocumentAggregate
{
   public class DocumentSubType:BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long DocumentTypeID { get; set; }


        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }

     

    }
}
