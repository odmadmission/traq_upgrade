﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class SessionScholarshipDiscount: BaseEntity
    {
        [Column(TypeName = "varchar(250)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string Description { get; set; }
        [Column(TypeName = "bigint")]
        public long SessionScholarshipId { get; set; }
    }
}
