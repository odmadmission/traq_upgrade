﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace OdmErp.ApplicationCore.Entities
{
    public class ScholarshipApply : BaseEntity
    {
        
        [Column(TypeName = "bigint")]
        public long SessionScholarshipDiscountId { get; set; }

        [Column(TypeName = "bigint")]
        public long StudentId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ApprovedDate { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> RejectedDate { get; set; }
        public string RejectedReason { get; set; }
        public long RequestSenderId { get; set; }
        public long ApprovedId { get; set; }
        public long RejectedId { get; set; }
    }
}
