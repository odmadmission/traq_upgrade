﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class ScholarshipApproval:BaseEntity
    {
        [Required]
        public long SessionScholarshipId { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable< DateTime> ApprovedDate { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> RejectedDate { get; set; }
        public string RejectedReason { get; set; }
        public long RequestSenderId { get; set; }
        public long ApprovedId { get; set; }
        public long RejectedId { get; set; }
    }
}
