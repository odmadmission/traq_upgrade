﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
   public class SessionScholarshipDiscountPrice : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long SessionScholarshipDiscountId { get; set; }

        [Column(TypeName = "bigint")]
        public long SessionFeesTypeId { get; set; }
        [Column(TypeName = "money")]
        public decimal DiscountPercentage { get; set; }
        public Guid UniqueId { get; set; }
    }
}
