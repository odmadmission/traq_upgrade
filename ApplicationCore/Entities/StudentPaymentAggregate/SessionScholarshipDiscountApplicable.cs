﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
   public class SessionScholarshipDiscountApplicable : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long SessionScholarshipDiscountPriceId { get; set; }

        [Column(TypeName = "bigint")]
        public long AcademicStandardId { get; set; }
        public Guid UniqueId { get; set; }

    }
}
