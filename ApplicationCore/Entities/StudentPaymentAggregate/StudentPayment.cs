﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.StudentPaymentAggregate
{
    public class StudentPayment:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long PaymentCollectionId { get; set; }
        [Column(TypeName = "bigint")]
        public long PaidUserId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime PaidDate { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal AmountPaid { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string TransactionId { get; set;}
        [Column(TypeName = "bigint")]
        public long PaymentTypeId { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string PaymentStatus { get; set; }
        [Column(TypeName = "bigint")]
        public long StudentId { get; set; }
    }
}
