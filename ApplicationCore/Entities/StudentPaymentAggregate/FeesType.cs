﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace OdmErp.ApplicationCore.Entities
{
    public class FeesType : BaseEntity
    {
        [Column(TypeName = "varchar(300)")]
        [Required(ErrorMessage = "Please Enter FeesType")]
        public string Name { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string Description { get; set; }

        [Column(TypeName = "bit")]
        public bool IsSubType { get; set; }

        [Column(TypeName = "bigint")]
        public long? ParentFeeTypeID { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string ParentFeeTypeName { get; set; }

        [Column(TypeName = "bit")]
        public bool HaveChild { get; set; }

        [Column(TypeName = "bit")]
        public bool HaveInstallment { get; set; }


        [Column(TypeName = "varchar(30)")]
        public string LateFeePercentageType { get; set; }


        [Column(TypeName = "varchar(30)")]
        public string LateFeeAmountType { get; set; }
    }
}
