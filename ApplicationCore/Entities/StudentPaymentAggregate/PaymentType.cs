﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace OdmErp.ApplicationCore.Entities.StudentPaymentAggregate
{
    public class PaymentType:BaseEntity
    {
        [Column(TypeName = "varchar(500)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string Descriptions { get; set; }

        [Column(TypeName = "varchar(5)")]
        public string BillDeskTransactionCode { get; set; }
    }
}
