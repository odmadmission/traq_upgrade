﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using OdmErp.ApplicationCore.Interfaces;

namespace OdmErp.ApplicationCore.Entities
{
    public class FeesTypeApplicable : BaseEntity
    {
        [Required(ErrorMessage = "Please  Enter  Name")]
        [Column(TypeName = "varchar(300)")]       
        public string Name { get; set; }
        
        [Column(TypeName = "varchar(500)")]
        public string Description { get; set; }
    }
}
