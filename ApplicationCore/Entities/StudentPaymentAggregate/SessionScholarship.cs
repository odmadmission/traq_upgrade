﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
   public class SessionScholarship: BaseEntity
    {
        [Column(TypeName = "varchar(250)")]
        public string Name { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "bigint")]
        public long SessionId { get; set; }

        [Column(TypeName = "bigint")]
        public long ScholarshipId { get; set; }
    }
}
