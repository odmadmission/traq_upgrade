﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using OdmErp.ApplicationCore.Interfaces;
namespace OdmErp.ApplicationCore.Entities
{
    public class FeesTypePriceData : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]       
        public long AcademicSessionId { get; set; }
        [Column(TypeName = "bigint")]
        public long FeesTypePriceId { get; set; }
        public List<SessionFeesTypeApplicableData> sessionFeesTypePriceApplicableData { get; set; } = new List<SessionFeesTypeApplicableData>();

    }
    //public class FeesTypePriceData:BaseEntity
    //{
    //    [Column(TypeName = "bigint")]
    //    public long AcademicSessionId { get; set; }
    //    [Column(TypeName = "bigint")]
    //    public long FeesTypePriceId { get; set; }
    //}

    public class SessionFeesTypeApplicableData : BaseEntity
    {       
        
        [Column(TypeName = "bigint")]
        public long FeesTypePriceId { get; set; }
        [Column(TypeName = "bigint")]
        public long ApplicabledataId { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }
        [Column(TypeName = "bigint")]
        public long FeesTypePriceDataID { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string SessionName { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string FeesTypeName { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string Applicabledata { get; set; }
        [Column(TypeName = "varchar(500)")]
        public string ParentFeesType { get; set; }
        [Column(TypeName = "bigint")]
        public long ApplicableToId { get; set; }
    }
}
