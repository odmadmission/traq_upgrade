﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace OdmErp.ApplicationCore.Entities.StudentPaymentAggregate
{
    public class PaymentCollectionType : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long AcademicSessionId { get; set; }


        //unnsed
        [Column(TypeName = "bigint")]
        public long OrganizationId { get; set; }
        //unnsed
        [Column(TypeName = "bigint")]
        public long WingId { get; set; }
        //unnsed
        [Column(TypeName = "bigint")]
        public long Boardid { get; set; }
        //unnsed
        [Column(TypeName = "bigint")]
        public long ClassId { get; set; }


        //unnsed
        [Column(TypeName = "decimal(18,2)")]
        public decimal SumAmount { get; set; }


        [Column(TypeName = "bigint")]
        public long NoOfInstallment { get; set; }

        [Column(TypeName = "bigint")]
        public long FeeTypeId { get; set; }


        public List<PaymentCollectionTypeData> paymentCollectionTypeData { get; set; } = new List<PaymentCollectionTypeData>();

    }
    public class PaymentCollectionTypeData : BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long PaymentCollectionId { get; set; }

        [Column(TypeName = "varchar(500)")]
        public string PaymentName { get; set; }

        //unnsed
        [Column(TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime ToDate { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime DeadLineDate { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal LatefeePercentage { get; set; }

    }
}