﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace OdmErp.ApplicationCore.Entities
{
   public class MessagesContent : BaseEntity
    {
        [Column(TypeName = "varchar(300)")]
        [Required(ErrorMessage = "Please Enter Type")]
        public string Type { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Template { get; set; }
    }
}
