﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace OdmErp.ApplicationCore.Entities
{
  public class ScholarshipApplicable : BaseEntity
    {
        [Column(TypeName = "varchar(300)")]
        [Required(ErrorMessage = "Please Enter ScholarshipApplicable Name")]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
