﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class Scholarship : BaseEntity
    {
        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please  Enter  Name")]
        [Column(TypeName = "varchar(500)")]
        public string Description { get; set; }
    }
}
