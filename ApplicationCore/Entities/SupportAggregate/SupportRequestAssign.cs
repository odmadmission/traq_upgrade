﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
     public class SupportRequestAssign : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long SupportRequestId { get; set; }

        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }
    }
}
