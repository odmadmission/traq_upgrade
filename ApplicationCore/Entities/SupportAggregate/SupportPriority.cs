﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
    public class SupportPriority : BaseEntity
    {
        [Column(TypeName ="varchar(MAX)")]
        public string Name { get; set; }
    }
}
