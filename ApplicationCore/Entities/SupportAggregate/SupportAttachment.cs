﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
   public class SupportAttachment : BaseEntity
    {
       
        [Column(TypeName = "bigint")]
        public long SupportRequestID { get; set; }


        [Column(TypeName = "varchar(max)")]
        public string Path {
            get;

            set;
        }



        [Column(TypeName = "varchar(max)")]
        public string Name { get; set; }

        [Column(TypeName = "varchar(max)")]
        public string Type { get; set; }
    }
}
