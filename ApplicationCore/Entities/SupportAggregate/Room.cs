﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
    public class Room:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long BuildingID { get; set; }

        [Column(TypeName = "bigint")]
        public long RoomCategoryID { get; set; }

        [Column(TypeName = "varchar(250)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        public long FloorID { get; set; }
    }
}
