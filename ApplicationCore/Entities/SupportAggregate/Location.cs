﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
   public class Location:BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long OrganizationID { get; set; }

        [Column(TypeName = "varchar(250)")]
        public string Address { get; set; }

        [Column(TypeName = "varchar(250)")]
        public string Name { get; set; }

        [Column(TypeName = "bigint")]
        public long CityID { get; set; }
    }
}
