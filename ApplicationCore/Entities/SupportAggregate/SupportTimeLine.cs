﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
  public class SupportTimeLine : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long SupportRequestID { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string comment { get; set; }

        [Column(TypeName = "bigint")]
        public long SupportStatusID { get; set; }

        [Column(TypeName = "bigint")]
        public long ReassignedToId { get; set; }
    }
}
