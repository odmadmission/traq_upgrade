﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
  public class SupportComment : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long SupportRequestID { get; set; }
        [Column(TypeName = "text")]
        public string Name { get; set; }
        [Column(TypeName = "bit")]
        public bool IsRead { get; set; }
    }
}
