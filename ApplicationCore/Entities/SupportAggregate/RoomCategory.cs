﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
   public class RoomCategory:BaseEntity
    {
        
        [Column(TypeName = "varchar(250)")]
        public string Name { get; set; }

        
    }
}
