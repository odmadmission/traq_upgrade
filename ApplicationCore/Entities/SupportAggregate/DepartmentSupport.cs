﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{


    public class DepartmentSupport : BaseEntity

    {
        [Column(TypeName = "varchar(MAX)")]
        public string Name { get; set; }

        [Column(TypeName = "bigint")]
        public long DepartmentID { get; set; }
        [Column(TypeName = "bigint")]
        public long SupportTypeID { get; set; }
    }
}
