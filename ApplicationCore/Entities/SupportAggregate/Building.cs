﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
    public class Building:BaseEntity
    {
        [Column(TypeName="varchar(250)")]
        public string Name { get; set; }

        [Column(TypeName = "bigint")]
        public long LocationID { get; set; }

        [Column(TypeName = "bigint")]
        public long BuildingTypeID { get; set; }
    }
}
