﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
    public class SupportCategory : BaseEntity
    {
        [Column(TypeName = "varchar(MAX)")]
        public string Name { get; set; }

        [Column(TypeName = "bigint")]
        public long SupportTypeID { get; set; }
    }
}
