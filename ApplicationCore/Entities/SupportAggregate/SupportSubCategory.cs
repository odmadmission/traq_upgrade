﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
    public class SupportSubCategory : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long CategoryID { get; set; }


        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string OthersName { get; set; }
    }
}
