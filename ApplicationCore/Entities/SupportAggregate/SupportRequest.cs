﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SupportAggregate
{
    public class SupportRequest : BaseEntity
    {






        [Column(TypeName = "bigint")]
        public long CategoryID { get; set; }
        [Column(TypeName = "bigint")]
        public long SubCategoryID { get; set; }

        [Column(TypeName = "bigint")]
        public long RoomID { get; set; }
        [Column(TypeName = "bigint")]
        public long BuildingID { get; set; }
        [Column(TypeName = "bigint")]
        public long DepartmentID { get; set; }
        [Column(TypeName = "bigint")]
        public long PriorityID { get; set; }
        [Column(TypeName = "text")]
        public string Description { get; set; }
        [Column(TypeName = "bigint")]
        public long StatusID { get; set; }
        [Column(TypeName = "bigint")]
        public long LocationID { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> CompletionDate { get; set; }
        [Column(TypeName = "bigint")]
        public long SupportTypeID { get; set; }

        [Column(TypeName = "bigint")]
        public long ApprovedID { get; set; }
        [Column(TypeName = "bigint")]
        public long RejectedID { get; set; }
        [Column(TypeName = "datetime")]

        public Nullable<DateTime> SentApprovalDate { get; set; }

        [Column(TypeName = "datetime")]

        public Nullable<DateTime> ApproveDate { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> RejectDate { get; set; }
        [Column(TypeName = "bit")]
        public bool IsApprove { get; set; }
        [Column(TypeName = "bit")]
        public bool IsReject { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> DueDate { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> RequestedCompletionDate { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string StatusName { get; set; }

        [Column(TypeName = "bit")]
        public bool ManagementApproval { get; set; }

        [Column(TypeName = "bit")]
        public bool SendApproval { get; set; }

        [Column(TypeName = "bigint")]
        public Nullable<long> ManagementApprovalID { get; set; }

        [Column(TypeName = "bigint")]
        public Nullable<long> SentApprovalID { get; set; }

        [Column(TypeName = "text")]
        public string Comment { get; set; }


        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ManagementApprovedDate { get; set; }


        [Column(TypeName = "datetime")]
        public Nullable<DateTime> ManagementRejectDate { get; set; }
        [Column(TypeName = "bigint")]
        public long ManagementRejectedID { get; set; }
        [Column(TypeName = "bit")]
        public bool ManagementReject { get; set; }


        [Column(TypeName = "bit")]
        public Nullable<bool> IsRead { get; set; }

        [Column(TypeName = "bit")]
        public Nullable<bool> IsManagementRead { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string Title { get;set;}
        [Column(TypeName = "varchar(500)")]
        public string TicketCode { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Score { get; set; }
        [Column(TypeName = "bigint")]
        public long? ScoreGivenId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ScoreGivenDate { get; set; }
    }
}
