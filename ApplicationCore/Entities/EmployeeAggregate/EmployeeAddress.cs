﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.EmployeeAggregate
{
    public class EmployeeAddress:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long AddressID { get; set; }

        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }


    }
}
