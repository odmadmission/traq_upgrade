﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OdmErp.ApplicationCore.Entities.EmployeeAggregate
{
    public class Employee:BaseEntity
    {
        [Column(TypeName = "varchar(300)")]
        public string EmpCode { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string FirstName { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string MiddleName { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string LastName { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string Gender { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> DateOfBirth { get; set; }
        [Column(TypeName = "bigint")]
        public Nullable<long> BloodGroupID { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string PrimaryMobile { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string AlternativeMobile { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string LandlineNumber { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string EmailId { get; set; }


        [Column(TypeName = "varchar(300)")]
        public string Image { get; set; }


        [Column(TypeName = "bigint")]
        public Nullable<long> NatiobalityID { get; set; }

        [Column(TypeName = "bigint")]
        public Nullable<long> ReligionID { get; set; }


        [Column(TypeName = "bigint")]
        public long EmployeeGroupID { get; set; }


        [Column(TypeName = "varchar(300)")]
        public string MaritalStatus { get; set; }
        [Column(TypeName = "bit")]
        public Nullable<bool> Isleft { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> LeftDate { get; set; }
    }
}