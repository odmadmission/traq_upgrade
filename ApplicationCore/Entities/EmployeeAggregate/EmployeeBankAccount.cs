﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities.EmployeeAggregate
{
    public class EmployeeBankAccount : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long BankTypeID { get; set; }
        [Column(TypeName = "bigint")]
        public long BankNameID { get; set; }
     
        [Column(TypeName = "varchar(300)")]
        public string AccountNumber { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string AccountHolderName { get; set; }

        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }
        [Column(TypeName = "bit")]
        public bool IsPrimary { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string BankBranchName { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string IfscCode { get; set; }
    }
}
