﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.EmployeeAggregate
{
    public class EmployeeRequiredDocument : BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long DocumentSubTypeID { get; set; }
        [Column(TypeName = "bit")]
        public bool IsMandatory { get; set; }
        [Column(TypeName = "bigint")]
        public long DocumentTypeID { get; set; }
        [Column(TypeName = "bigint")]
        public long RelatedID { get; set; }
        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }
        public bool IsVerified { get; set; }
        [Column(TypeName = "bigint")]
        public long VerifiedAccessID { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? VerifiedDate { get; set; }    
       
        [Column(TypeName = "bit")]
        public bool IsApproved { get; set; }
        [Column(TypeName = "bit")]
        public bool IsReject { get; set; }
    }
}
