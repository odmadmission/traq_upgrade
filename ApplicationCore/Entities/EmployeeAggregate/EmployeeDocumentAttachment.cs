﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.EmployeeAggregate
{
   public class EmployeeDocumentAttachment : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long EmployeeRequiredDocumentID { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string DocumentPath { get; set; }
    }
}
