﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OdmErp.ApplicationCore.Entities.EmployeeAggregate
{
    public class EmployeeExperience:BaseEntity
    {

        [Column(TypeName = "varchar(300)")]
        public string OrganizationName { get; set; }
        [Column(TypeName = "varchar(300)")]
        public string Designation { get; set; }
        [Column(TypeName = "int")]
        public int SalaryPerAnum  { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FromDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ToDate { get; set; }
        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }
      


    }
}