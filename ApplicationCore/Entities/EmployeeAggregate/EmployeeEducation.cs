﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities.EmployeeAggregate
{
    public class EmployeeEducation : BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long EducationQualificationID { get; set; }

        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }

        [Column(TypeName = "varchar(max)")]
        public string College { get; set; }
        [Column(TypeName = "varchar(max)")]
        public string University { get; set; }
        [Column(TypeName = "int")]
        public Nullable<int> FromYear { get; set; }
        [Column(TypeName = "int")]
        public Nullable<int> ToYear { get; set; }
    }
}
