﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.EmployeeAggregate
{
    public class EmployeeDesignation:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long DesignationID { get; set; }

        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }

    }
}
