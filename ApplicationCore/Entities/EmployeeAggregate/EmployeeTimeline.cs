﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.EmployeeAggregate
{
   public class EmployeeTimeline : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long EmployeeId { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> JoiningOn { get; set; }
        [Column(TypeName = "datetime")]
        public Nullable<DateTime> LeftOn { get; set; }
        [Column(TypeName = "bit")]
        public bool IsPrimary { get; set; }
    }
}
