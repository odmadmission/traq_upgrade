﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.MasterAggregate
{
    public class DepartmentAuthority:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long DepartmentID { get; set; }

        [Column(TypeName = "bigint")]
        public long AuthorityID { get; set; }


        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }
    }
}