﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OdmErp.ApplicationCore.Entities
{
    public class Designation:BaseEntity
    {

        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }

        //unused
        //[Column(TypeName = "bigint")]
        //public Nullable<long> DesignationLevelID { get; set; }

        [Column(TypeName = "bigint")]
        public long DepartmentID { get; set; }

    }
}