﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OdmErp.ApplicationCore.Entities
{

   
    public class Address:BaseEntity
    {
        [Column(TypeName = "varchar(400)")]
        public string AddressLine1 { get; set; }
        [Column(TypeName = "varchar(400)")]
        public string AddressLine2 { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string PostalCode { get; set; }
        [Column(TypeName = "bigint")]
        public long CityID { get; set; }
        [Column(TypeName = "bigint")]
        public long StateID { get; set; }
        [Column(TypeName = "bigint")]
        public long CountryID { get; set; }
        [Column(TypeName = "bigint")]
        public long AddressTypeID { get; set; }



        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }



    }
}