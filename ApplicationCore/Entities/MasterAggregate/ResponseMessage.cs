﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities
{
    public class ResponseMessage:BaseEntity
    {
        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        public long StateID { get; set; }



    }


}
