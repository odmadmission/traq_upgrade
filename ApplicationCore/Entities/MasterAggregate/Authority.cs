﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities.MasterAggregate
{
    public class Authority : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long AuthorityTypeID { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }

    }
}
