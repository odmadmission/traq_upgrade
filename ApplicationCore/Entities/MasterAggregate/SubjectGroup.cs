﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.MasterAggregate
{
    public class SubjectGroup : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        public long BoardStandardId { get; set; }
    }
}
