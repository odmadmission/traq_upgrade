﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities
{
    public class DesignationEducation:BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long EducationQualificationID { get; set; }

        [Column(TypeName = "bigint")]
        public long DesignationID { get; set; }


    }
}
