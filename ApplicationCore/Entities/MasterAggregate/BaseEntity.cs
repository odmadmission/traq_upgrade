﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class BaseEntity
    {
        [Key]
        public long ID { get; set; }
        [Column(TypeName = "datetime")]
        [Required]
        public DateTime InsertedDate { get; set; }
        [Column(TypeName = "datetime")]
        [Required]
        public DateTime ModifiedDate { get; set; }
        [Column(TypeName = "bigint")]
        public long InsertedId { get; set; }
        [Column(TypeName = "bigint")]
        public long ModifiedId { get; set; }
        [Column(TypeName = "bit")]
        public bool Active { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Status { get; set; }
        [Column(TypeName = "bit")]
        public bool IsAvailable { get; set; }

    }
}
