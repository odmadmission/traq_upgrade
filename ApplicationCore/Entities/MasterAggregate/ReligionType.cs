﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities
{
    public class ReligionType:BaseEntity
    {

        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }


    }
}
