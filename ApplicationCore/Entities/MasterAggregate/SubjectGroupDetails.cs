﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.MasterAggregate
{
    public class SubjectGroupDetails : BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long SubjectGroupID { get; set; }
        [Column(TypeName = "bigint")]
        public long SubjectID { get; set; }
    }
    public class AcademicSubjectGroup : BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "bigint")]
        public long SubjectGroupID { get; set; }
        [Column(TypeName = "bigint")]
        public long AcademicStandardID { get; set; }
    }
    public class SubjectOptionalType : BaseEntity, IAggregateRoot
    {

        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }

    }
    public class SubjectOptionalCategory : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long? SubjectOptionalTypeID { get; set; }
        [Column(TypeName = "bigint")]
        public long? BoardStandardID { get; set; }
        [Column(TypeName = "bigint")]
        public long? WingID { get; set; }
        [Column(TypeName = "bigint")]
        public long? SubjectGroupID { get; set; }

    }
    public class SubjectOptional : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long? SubjectOptionalCategoryID { get; set; }
        [Column(TypeName = "bigint")]
        public long? SubjectID { get; set; }

    }
    public class AcademicSubjectOptional : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long? SubjectOptionalCategoryID { get; set; }
        [Column(TypeName = "bigint")]
        public long? AcademicStandardID { get; set; }
        [Column(TypeName = "bigint")]
        public long? AcademicStandardWingID { get; set; }
        [Column(TypeName = "bigint")]
        public long? AcademicSubjectGroupID { get; set; }
    }
    public class AcademicSubjectOptionalDetails : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "bigint")]
        public long? SubjectOptionalID { get; set; }
        [Column(TypeName = "bigint")]
        public long? AcademicSubjectOptionalID { get; set; }
    }
    public class VehicleType : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }

    }
    public class Vehicle : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(300)")]
        public string Number { get; set; }
        [Column(TypeName = "bigint")]
        public long? VehicleTypeID { get; set; }
        [Column(TypeName = "bigint")]
        public long? OrganisationID { get; set; }
        [Column(TypeName = "bigint")]
        public long? GroupID { get; set; }

    }
    public class VehicleLocation : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        public long? VehicleID { get; set; }
        [Column(TypeName = "bigint")]
        public long CityID { get; set; }
    }
    public class DepartureTime : BaseEntity, IAggregateRoot
    {
        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }
        [Column(TypeName = "time(7)")]
        public TimeSpan PickupTime { get; set; }
        [Column(TypeName = "time(7)")]
        public TimeSpan DropTime { get; set; }
        [Column(TypeName = "bigint")]
        public long? LocationID { get; set; }
    }
}
