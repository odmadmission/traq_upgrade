﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OdmErp.ApplicationCore.Entities
{
    public class City:BaseEntity
    {

        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        public long StateID { get; set; }
        [Column(TypeName = "bigint")]
        public long CountryID { get; set; }

      
    }
}