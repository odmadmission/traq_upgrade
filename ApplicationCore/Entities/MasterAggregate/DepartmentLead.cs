﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.MasterAggregate
{
    public class DepartmentLead:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long DepartmentID { get; set; }
        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }
        
    }
}
