﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OdmErp.ApplicationCore.Entities
{
    public class Department : BaseEntity
    {
        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }
        [Column(TypeName = "bigint")]
        public long OrganizationID { get; set; }
        [Column(TypeName = "bigint")]
        public long? ParentID { get; set; }
        [Column(TypeName = "bigint")]
        public long DepartmentTypeID { get; set; }
        [Column(TypeName = "bit")]
        public bool IsPrimary { get; set; }

        [Column(TypeName = "bigint")]
        public long GroupID { get; set; }

    }
}