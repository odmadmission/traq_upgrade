﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    public class FilePathUrl:BaseEntity,IAggregateRoot
    {
        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }


        [Column(TypeName = "varchar(300)")]
        public string Type { get; set; }


        [Column(TypeName = "varchar(300)")]
        public string Path { get; set; }



    }
}
