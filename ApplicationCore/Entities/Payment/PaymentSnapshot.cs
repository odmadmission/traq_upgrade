﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("PaymentSnapshot", Schema = "payment")]
    public class PaymentSnapshot : BaseEntity
    {
        [Column(TypeName = "nvarchar(150)")]
        public string TxnAmount { get; set; }
        [Column(TypeName = "nvarchar(150)")]
        public string TxnReferenceNo { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string UniqueTxnId { get; set; }
        public Guid PaymentSnapshotId { get; set; }

        [Column(TypeName = "nvarchar(150)")]
              public string RelatedName { get; set; }

             [Column(TypeName = "bigint")]
            public long RelatedId { get; set; }

            [Column(TypeName = "nvarchar(max)")]
            public string RequestMsg { get; set; }

        [Column(TypeName = "nvarchar(max)")]
        public string ResponseMsg { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string ResponseChecksum { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string RequestChecksum { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string PaymentStatus { get; set; }

        [Column(TypeName = "nvarchar(150)")]
        public string AuthStatus { get; set; }
        [Column(TypeName = "nvarchar(150)")]
        public string TxnType { get; set; }

        [Column(TypeName = "datetime")]
        public Nullable<DateTime> TxnDate { get; set; }

    }
}
