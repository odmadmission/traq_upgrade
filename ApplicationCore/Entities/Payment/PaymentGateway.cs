﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("MasterPaymentGateway", Schema = "payment")]
    public class PaymentGateway : BaseEntity
    {
        [Column(TypeName = "varchar(300)")]
        public string ProviderName { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string Host { get; set; }

        [Column(TypeName = "varchar(10)")]
        public string HttpMethod { get; set; }

        [Column(TypeName = "varchar(10)")]
        public string RequestParameter { get; set; }

        [Column(TypeName = "varchar(10)")]
        public string ResponseParameter { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string ServerToServerCallback { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string MerchantId { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string CheckSum { get; set; }

        [Column(TypeName = "varchar(10)")]
        public string TypeField { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string CurrencyType { get; set; }

        [Column(TypeName = "varchar(10)")]
        public string TypeFieldTwo { get; set; }
    }
}
