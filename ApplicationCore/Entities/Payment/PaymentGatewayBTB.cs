﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities
{
    [Table("MasterPaymentGatewayBTB", Schema = "payment")]
    public class PaymentGatewayBTB : BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long PaymentGatewayId { get; set; }

        [Column(TypeName = "varchar(300)")]
        public string BtbParam { get; set; }

    }
}
