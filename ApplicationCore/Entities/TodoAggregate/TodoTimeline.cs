﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.TodoAggregate
{
    public class TodoTimeline:BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long TodoID { get; set; }

        [Column(TypeName = "varchar(200)")]
        public string TodoStatus { get; set; }

        [Column(TypeName = "bigint")]
        public long RelatedId { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime?   RelatedDate { get; set; }
    }
}
