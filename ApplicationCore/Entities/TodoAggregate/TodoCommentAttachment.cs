﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.TodoAggregate
{
    public class TodoCommentAttachment:BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long TodoCommentID { get; set; }

        [Column(TypeName = "varchar(max)")]
        public string AttachmentName { get; set; }
    }
}
