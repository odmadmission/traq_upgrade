﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.TodoAggregate
{
    public class TodoComment:BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long TodoID { get; set; }

        [Column(TypeName = "varchar(max)")]
        public string Description { get; set; }
        [Column(TypeName = "bit")]
        public bool IsRead { get; set; }

    }
}
