﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.TodoAggregate
{
   public class TodoStatusDetails : BaseEntity
    {
        [Column(TypeName = "varchar(300)")]
        public string StatusName { get; set; }
        [Column(TypeName = "bigint")]
        public long TodoId { get; set; }
        [Column(TypeName = "bigint")]
        public long UserId { get; set; }
    }
}
