﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
namespace OdmErp.ApplicationCore.Entities.TodoAggregate
{
    public class Todo:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long EmployeeID { get; set; }
        [Column(TypeName = "bigint")]
        public long AssignByDepartmentID { get; set; }
        [Column(TypeName = "bigint")]
        public long DepartmentID { get; set; }
        [Column(TypeName = "bigint")]
        public long TodoPriorityID { get; set; }

        [Column(TypeName = "bigint")]
        public long TodoStatusID { get; set; }


        [Column(TypeName = "varchar(max)")]
        public string Name { get; set; }

        [Column(TypeName = "varchar(max)")]
        public string Description { get; set; }


        [Column(TypeName = "datetime")]
        public DateTime? DueDate { get; set; }


        [Column(TypeName = "datetime")]
        public DateTime? AssignedDate { get; set; }


        [Column(TypeName = "datetime")]
        public DateTime? CompletionDate { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? VerifiedDate { get; set; }

        
        [Column(TypeName = "bigint")]
        public long AssignedId { get; set; }

        [Column(TypeName = "bigint")]
        public long ParentId { get; set; }
        [Column(TypeName = "bit")]
        public bool IsRead { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Score { get; set; }
        [Column(TypeName = "bigint")]
        public long? ScoreGivenId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ScoreGivenDate { get; set; }
    }
}
