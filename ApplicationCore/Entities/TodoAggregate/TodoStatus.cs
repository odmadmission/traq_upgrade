﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OdmErp.ApplicationCore.Entities.TodoAggregate
{
    public class TodoStatus: BaseEntity
    {

        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }
    }
}
