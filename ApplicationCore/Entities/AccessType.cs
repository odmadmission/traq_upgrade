﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities
{
    public class AccessType:BaseEntity
    {
        [Column(TypeName = "varchar(200)")]
        public string Name { get; set; }
    }
}
