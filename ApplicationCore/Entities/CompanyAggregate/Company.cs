﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.CompanyAggregate
{
   public class Company:BaseEntity
    {
        [Column(TypeName = "varchar(max)")]
        public string CompanyName { get; set; }
        [Column(TypeName ="varchar(max)")]
        public string Location { get; set; }
    }
}
