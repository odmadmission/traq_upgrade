﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SoftwareAggregate
{
   public class SWAttachment : BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long SupportID { get; set; }


        [Column(TypeName = "varchar(max)")]
        public string Path { get; set; }


        [Column(TypeName = "varchar(max)")]
        public string Name { get; set; }

        [Column(TypeName = "varchar(max)")]
        public string Type { get; set; }
    }
}
