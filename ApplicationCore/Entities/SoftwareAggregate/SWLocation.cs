﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SoftwareAggregate
{
   public class SWLocation:BaseEntity
    {

        [Column(TypeName = "bigint")]
        public long OrganizationID { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Address { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Name { get; set; }
    }
}
