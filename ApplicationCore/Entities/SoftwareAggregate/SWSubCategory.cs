﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SoftwareAggregate
{
    public class SWSubCategory:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long CategoryID { get; set; }


        [Column(TypeName = "varchar(300)")]
        public string Name { get; set; }
    }
}
