﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SoftwareAggregate
{
    public class SWDepartment:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long CategoryID { get; set; }
        [Column(TypeName = "bigint")]
        public long SubCategoryID { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string Mobile { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string Email { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string Password { get; set; }
    }
}
