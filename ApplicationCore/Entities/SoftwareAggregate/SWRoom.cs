﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SoftwareAggregate
{
    public class SWRoom:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long BuildingID { get; set; }

        [Column(TypeName = "bigint")]
        public long RoomCategoryID { get; set; }

        [Column(TypeName = "varchar(MAX)")]
        public string Name { get; set; }
    }
}
