﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SoftwareAggregate
{
    public class Software:BaseEntity
    {
        [Column(TypeName = "bigint")]
        public long CategoryID { get; set; }
        [Column(TypeName = "bigint")]
        public long SubCategoryID { get; set; }

        [Column(TypeName = "bigint")]
        public long RoomID { get; set; }
        [Column(TypeName = "bigint")]
        public long BuildingID { get; set; }
        [Column(TypeName = "bigint")]
        public long DepartmentID { get; set; }
        [Column(TypeName = "bigint")]
        public long PriorityID { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string Description { get; set; }
        [Column(TypeName = "bigint")]
        public long StatusID { get; set; }
        [Column(TypeName = "bigint")]
        public long LocationID { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string Comment { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public DateTime CompletionDate { get; set; }
    }
}
