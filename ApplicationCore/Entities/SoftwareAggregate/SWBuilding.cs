﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.ApplicationCore.Entities.SoftwareAggregate
{
    public class SWBuilding : BaseEntity
    {
        [Column(TypeName="varchar(MAX)")]
        public string Name { get; set; }

        [Column(TypeName = "bigint")]
        public long LocationID { get; set; }
    }
}
