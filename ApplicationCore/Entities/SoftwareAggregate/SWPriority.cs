﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace OdmErp.ApplicationCore.Entities.SoftwareAggregate
{
    public class SWPriority:BaseEntity
    {
        [Column(TypeName ="varchar(MAX)")]
        public string Name { get; set; }
    }
}
