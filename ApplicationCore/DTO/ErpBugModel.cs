﻿
using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.DTO
{
    public class OdmErpModel
    {
        [Key]
        public long ID { get; set; }
        public string ModuleName { get; set; }

        [Display(Name = "Module")]        
        public long ModuleID { get; set; }

       public string SubModuleName { get; set; }

        [Display(Name = "SubModule")]       
        public long SubModuleID { get; set; }
        public string ActionName { get; set; }
        [Display(Name = "Action")]      
        public long ActionID { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Please Fill The Title")]
        public string  Title { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please Fill The Description")]
        public string Description { get; set; }
        public bool Active { get; set; }

        //public IEnumerable<Module> Modules { get; set; }
        //public IEnumerable<SubModule> SubModules { get; set; }
       // public IEnumerable<Action> Actions { get; set; }
    }

    public class ErpBugAttachmentModel
    {
        public long ID { get; set; }
        public string Name { get; set; }

    }
}
