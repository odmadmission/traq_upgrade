﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.DTO
{
    public class AccessDto
    {

        private string EmployeeName { set; get; }
        private long EmployeeId { set; get; }
        private long AccessId { set; get; }

        private string GroupName { set; get; }
        private long GroupId { set; get; }
        private long OrganizationId { set; get; }
        private long DesignationId { set; get; }
        private string OrganizationName { set; get; }
        private string DesignationName { set; get; }

        private IEnumerable<ModuleDto> moduleDtos { set; get; }
    }
}
