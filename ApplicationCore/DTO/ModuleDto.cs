﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.DTO
{
    public class ModuleDto
    {

        private string Name { set; get; }
        private long ID { set; get; }

        private IEnumerable<AccessDto>  subModuleDtos{ set; get; }

    }
}
