﻿using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.StudentAggregate;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IStudentAggregateRepository
    {

        #region Student
        Student GetStudentByStudentCode(string studentcode);
        Student GetStudentById(long id);
        IEnumerable<Student> GetAllStudent();
        long CreateStudent(Student newStudent);
        bool UpdateStudent(Student changedStudent);
        bool DeleteStudent(long id);
        bool DeleteAllStudent();
        #endregion

        #region StudentAddress
      
        StudentAddress GetStudentAddressById(long id);
        IEnumerable<StudentAddress> GetAllStudentAddress();
        long CreateStudentAddress(StudentAddress newStudentAddress);
        bool UpdateStudentAddress(StudentAddress changedStudentAddress);
        bool DeleteStudentAddress(long id);
        bool DeleteAllStudentAddress();
        #endregion


        #region Parent

        Parent GetParentById(long id);
        IEnumerable<Parent> GetAllParent();
        long CreateParent(Parent newParent);
        bool UpdateParent(Parent changedParent);
        bool DeleteParent(long id);
        bool DeleteAllParent();
        #endregion

        #region ParentRelationshipType

        ParentRelationshipType GetParentRelationshipTypeById(long id);
        IEnumerable<ParentRelationshipType> GetAllParentRelationshipType();
        long CreateParentRelationshipType(ParentRelationshipType newParentRelationshipType);
        bool UpdateParentRelationshipType(ParentRelationshipType changedParentRelationshipType);
        bool DeleteParentRelationshipType(long id);
        bool DeleteAllParentRelationshipType();
        #endregion



        #region ProfessionType

        ProfessionType GetProfessionTypeById(long id);
        IEnumerable<ProfessionType> GetAllProfessionType();
        long CreateProfessionType(ProfessionType newProfessionType);
        bool UpdateProfessionType(ProfessionType changedProfessionType);
        bool DeleteProfessionType(long id);
        bool DeleteAllProfessionType();
        string GetStudentById();
        string GetParentById();
        #endregion


        #region Wing
        Wing GetWingById(long id);
        IEnumerable<Wing> GetAllWing();
        long CreateWing(Wing newWing);
        bool UpdateWing(Wing changedWing);
        bool DeleteWing(long id);
        bool DeleteAllWing();
        #endregion

        #region SchoolWing
        //SchoolWing GetSchoolWingById(long id);
        //IEnumerable<SchoolWing> GetAllSchoolWing();
        //long CreateSchoolWing(SchoolWing newschoolwing);
        //bool UpdateSchoolWing(SchoolWing changedschoolwing);
        //bool DeleteSchoolWing(long id);
        //bool DeleteAllSchoolWing();
        #endregion


        #region StudentClass

        StudentClass GetStudentClassById(long id);
        IEnumerable<StudentClass> GetAllStudentClass();
        long CreateStudentClass(StudentClass newStudentClass);
        bool UpdateStudentClass(StudentClass changedStudentClass);
        bool DeleteStudentClass(long id);
        bool DeleteAllStudentClass();
        #endregion


        #region student document
        IEnumerable<StudentAcademic> GetStudentAcademicByStudentId(long studentid);
        StudentAcademic GetStudentAcademicById(long id);
            IEnumerable<StudentAcademic> GetAllStudentAcademic();
            long CreateStudentAcademic(StudentAcademic newStudentClass);
            bool UpdateStudentAcademic(StudentAcademic changedStudentClass);
            bool DeleteStudentAcademic(long id);
            bool DeleteAllStudentAcademic();

        #endregion
        #region student Required document

        StudentRequiredDocument GetStudentRequiredDocumentById(long id);
        IEnumerable<StudentRequiredDocument> GetAllStudentRequiredDocument();
        long CreateStudentRequiredDocument(StudentRequiredDocument newStudentDoc);
        bool UpdateStudentRequiredDocument(StudentRequiredDocument changedStudentDoc);
        bool DeleteStudentRequiredDocument(long id);
        bool DeleteAllStudentRequiredDocument();

        #endregion
        #region student   Attachment

        StudentDocumentAttachment GetStudentDocumentAttachmentById(long id);
        IEnumerable<StudentDocumentAttachment> GetAllStudentDocumentAttachment();
        long CreateStudentDocumentAttachment(StudentDocumentAttachment newStudentAttachment);
        bool UpdateStudentDocumentAttachment(StudentDocumentAttachment changedStudentAttachment);
        bool DeleteStudentDocumentAttachment(long id);
        bool DeleteAllStudentDocumentAttachment();

        #endregion

       


    }
}
