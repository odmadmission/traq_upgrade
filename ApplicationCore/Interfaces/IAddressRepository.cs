﻿using OdmErp.ApplicationCore.Entities;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IAddressRepository
    {       
        Address GetAddressById(long id);
        IEnumerable<Address> GetAllAddress();
        long CreateAddress(Address newAddress);
        bool UpdateAddress(Address changedAddress);
        bool DeleteAddress(long id);
        bool DeleteAllAddress();

    }
}
