﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IDepartmentRepository
    {
        Department GetDepartmentByName(string name);
        Department GetDepartmentById(long id);
        IEnumerable<Department> GetAllDepartment();
        long CreateDepartment(Department newDepartment);
        bool UpdateDepartment(Department changedDepartment);
        bool DeleteDepartment(long id);
        bool DeleteAllDepartment();

        IEnumerable<Department> GetDepartmentByIdIn(List<long> id);

        DepartmentType GetDepartmentTypeByName(string name);
        DepartmentType GetDepartmentTypeById(long id);
        IEnumerable<DepartmentType> GetAllDepartmentType();
        long CreateDepartmentType(DepartmentType newDepartmentType);
        bool UpdateDepartmentType(DepartmentType changedDepartmentType);
        bool DeleteDepartmentType(long id);
        bool DeleteAllDepartmentType();

      
        long GetDepartmentCountByGroupId(long groupId);
        IEnumerable<Department> GetDepartmentListByGroupId(long groupId);

        long GetDepartmentCountByOrganizationId(long organizationId);
        IEnumerable<Department> GetDepartmentListByOrganizationId(long organizationId);


        IEnumerable<Department> GetDepartmentListByGroupIdIn(List<long> groupId);
        IEnumerable<Department> GetDepartmentListByOrganizationIdIn(List<long> organizationId);

    }
}
