﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.DTO;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IAccessRepository
    {
        Access GetAccessByUserNameAndPassword(string username, string password);
        Access GetAccessById(long id);
        IEnumerable<Access> GetAllAccess();
        IEnumerable<Access> GetAllAccesswithinactive();
        long CreateAccess(Access newAccess);
        long CreateSuperAdminAccess(long employeeId, string username, string password, long roleId);
        bool UpdateAccess(Access changedAccess);
        bool DeleteAccess(long id);
        bool DeleteAllAccess();
        Access GetAccessByUserName(string username);
        AccessDto GetAccessDtoByUserNameAndPassword(
            string username, 
            string password);

    }
}
