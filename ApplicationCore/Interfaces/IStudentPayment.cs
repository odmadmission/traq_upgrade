﻿using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities.StudentPaymentAggregate;
namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IStudentPayment
    {
        long CreateStudentPayment(StudentPayment newStudentpayment);
        IEnumerable<StudentPayment> GetAllStudentPayment();
    }
}
