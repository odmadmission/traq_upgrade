﻿
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IEntityCommonMethod<T> where T : BaseEntity, IAggregateRoot
    {
        Task<IReadOnlyList<T>> ListAllAsyncIncludeAll();

        Task<T> GetByIdAsyncIncludeAll(long id);

       

    }
}
