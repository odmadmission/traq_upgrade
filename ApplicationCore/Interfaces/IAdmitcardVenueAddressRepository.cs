﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface IAdmitcardVenueAddressRepository
    {
        AdmitcardVenueAddress GetAdmitcardVenueAddressByName(string name);
        AdmitcardVenueAddress GetAdmitcardVenueAddressById(long id);
        IEnumerable<AdmitcardVenueAddress> GetAllAdmitcardVenueAddress();
        long CreateAdmitcardVenueAddress(AdmitcardVenueAddress newAdmitcardVenueAddress);
        //  bool UpdateAdmitcardVenueAddress(AdmitcardInstructions changedAdmitcardVenueAddress);
        bool UpdateAdmitcardVenueAddress(AdmitcardVenueAddress changedAdmitcardVenueAddress);
        bool DeleteAdmitcardVenueAddress(long id);
        bool DeleteAllAdmitcardVenueAddress();
    }
}
