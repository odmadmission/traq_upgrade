﻿using OdmErp.ApplicationCore.Entities.StudentAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IParentRelationshipTypeRepository
    {
        ParentRelationshipType GetParentRelationshipTypeByName(string name);
        ParentRelationshipType GetParentRelationshipTypeById(long id);
        IEnumerable<ParentRelationshipType> GetAllParentRelationshipType();
    }
}
