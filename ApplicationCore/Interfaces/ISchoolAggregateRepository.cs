﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IUserGroupRepository : IAsyncRepository<UserGroup>, IEntityCommonMethod<UserGroup>
    {
        Task<IReadOnlyList<UserGroup>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IUserGroupTimeLineRepository : IAsyncRepository<UserGroupTimeLine>, IEntityCommonMethod<UserGroupTimeLine>
    {
        Task<IReadOnlyList<UserGroupTimeLine>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IUserGroupEmployeeRepository : IAsyncRepository<UserGroupEmployee>, IEntityCommonMethod<UserGroupEmployee>
    {
        Task<IReadOnlyList<UserGroupEmployee>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IUserGroupEmployeeTimeLineRepository : IAsyncRepository<UserGroupEmployeeTimeLine>, IEntityCommonMethod<UserGroupEmployeeTimeLine>
    {
        Task<IReadOnlyList<UserGroupEmployeeTimeLine>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface INoticeRepository : IAsyncRepository<Notice>, IEntityCommonMethod<Notice>
    {
        Task<IReadOnlyList<Notice>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface INoticeDataRepository : IAsyncRepository<NoticeData>, IEntityCommonMethod<NoticeData>
    {
        Task<IReadOnlyList<NoticeData>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAcademicHomeworkRepository : IAsyncRepository<AcademicHomework>, IEntityCommonMethod<AcademicHomework>
    {
        Task<IReadOnlyList<AcademicHomework>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAcademicHomeworkAttachmentRepository : IAsyncRepository<AcademicHomeworkAttachment>, IEntityCommonMethod<AcademicHomeworkAttachment>
    {
        Task<IReadOnlyList<AcademicHomeworkAttachment>> ListAllAsyncIncludeActiveNoTrack();
    }
}
