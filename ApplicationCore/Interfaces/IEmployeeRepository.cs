﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IEmployeeRepository 
    {
        Employee GetEmployeeByCode(string empcode);
        Employee GetEmployeeById(long id);
        IEnumerable<Employee> GetAllEmployee();
        IEnumerable<Employee> GetAllRequiredEmployee();
        IEnumerable<Employee> GetAllEmployeewithleft();
        IEnumerable<Employee> GetAllByEmployeeId(List<Int64> empIds);
        long CreateEmployee(Employee newEmployee);
        bool UpdateEmployee(Employee changedEmployee);
        bool DeleteEmployee(long id);
        bool DeleteAllEmployee();

        string GetEmployeeFullNameById(long id);
        long CreateSuperAdmin(long roleId);
         
        #region EmployeeTimeline
        EmployeeTimeline GetEmployeeTimelineById(long id);
        EmployeeTimeline GetEmployeeTimelineEmployeeById(long employeeid);
        IEnumerable<EmployeeTimeline> GetAllEmployeeTimeline();
        long CreateEmployeeTimeline(EmployeeTimeline newEmployeeTimeline);
        bool UpdateEmployeeTimeline(EmployeeTimeline changedEmployeeTimeline);
        bool DeleteEmployeeTimeline(long id);
        bool DeleteAllEmployeeTimeline();


        EmployeeTimeline GetActiveEmployeeTimelineByEmployeeId(long empid);

        #endregion
        
    }
}
