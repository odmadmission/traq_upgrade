﻿using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities;
namespace OdmErp.ApplicationCore.Interfaces
{
    public interface ICountryRepository
    {
        Country GetCountryByName(string name);
        Country GetCountryById(long id);
        IEnumerable<Country> GetAllCountries();
        long CreateCountry(Country newCountry);
        bool UpdateCountry(Country changedCountry);
        bool DeleteCountry(long id);
        bool DeleteAllCountries();

    }
}
