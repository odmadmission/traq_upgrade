﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IEmployeeRequiredDocumentRepository
    {
        EmployeeRequiredDocument GetEmployeeRequiredDocumentById(long id);
        IEnumerable<EmployeeRequiredDocument> GetAllEmployeeRequiredDocuments();
        long CreateEmployeeRequiredDocument(EmployeeRequiredDocument newEmployeeRequiredDocument);
        bool UpdateEmployeeRequiredDocument(EmployeeRequiredDocument changedEmployeeRequiredDocument);
        bool DeleteEmployeeRequiredDocument(long id);
        bool DeleteAllEmployeeRequiredDocuments();

    }
}
