﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using OdmErp.ApplicationCore.Entities.Pagination;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IFeesTypeRepository
    {
        //long CreateFeesType(FeesType newFeesType);
        //IEnumerable<FeesType> GetAllFeesType();
        //FeesType GetFeesTypeByName(string name);
        //FeesType GetFeesTypeById(long id);
        //bool UpdateFeesType(FeesType changedFeesType);
        //bool DeleteFeesType(long id);
        //bool DeleteAllFeesType();

        long CreateFeesType(FeesType newFeesType);
        IEnumerable<FeesType> GetAllFeesType();
        FeesType GetFeesTypeByName(string name);
        FeesType GetFeesTypeById(long id);
        bool UpdateFeesType(FeesType changedBankType);
        bool UpdateFeesTypeParent(long id,bool HaveChild);
        bool DeleteFeesType(long id);
        bool DeleteAllFeesType();
        PaginationFeesType GetPaginationAllFeesType(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue);
    }
}
