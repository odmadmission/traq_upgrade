﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IScholarshipRepository
    {
        long CreateScholarship(Scholarship newScholarship);
        IEnumerable<Scholarship> GetAllScholarshipData();
        PaginationScholarship GetPaginationScholarship(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue);
        IEnumerable<Scholarship> GetAllScholarship(int? page, int? noofData);
        Scholarship GetScholarshipByName(string name);
        Scholarship GetScholarshipById(long id);
        bool UpdateScholarship(Scholarship changedScholarship);
        bool DeleteScholarship(long id);
        bool DeleteAllScholarship();
    }
}
