﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface IScholarshipApprovalRepoaitory
    {
        long CreateScholarshipApproval(ScholarshipApproval newScholarshipApproval);
        IEnumerable<ScholarshipApproval> GetAllScholarshipApproval();
        ScholarshipApproval GetScholarshipApprovalByName(string name);
        //PaginationSessionScholarshipDiscount GetPaginationSessionScholarshipDiscount(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue);
        ScholarshipApproval GetScholarshipApprovalById(long id);
        bool UpdateScholarshipApproval(ScholarshipApproval changedScholarshipApproval);
        bool DeleteScholarshipApproval(long id);
        bool DeleteAllScholarshipApproval();
    }
}
