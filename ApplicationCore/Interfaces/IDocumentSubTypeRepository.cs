﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.DocumentAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IDocumentSubTypeRepository
    {
        DocumentSubType GetDocumentSubTypeByName(string name);
        DocumentSubType GetDocumentSubTypeById(long id);
        IEnumerable<DocumentSubType> GetAllDocumentSubTypes();
        long CreateDocumentSubType(DocumentSubType newDocumentSubType);
        bool UpdateDocumentSubType(DocumentSubType changedDocumentSubType);
        bool DeleteDocumentSubType(long id);
        bool DeleteAllDocumentTypes();

        IEnumerable<DocumentSubType> GetAllDocumentSubTypeByDocumentTypeId(long doctypeId);
        long GetAllDocumentSubTypeCountByDocumentTypeId(long doctypeId);

    }
}
