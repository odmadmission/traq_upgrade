﻿using OdmErp.ApplicationCore.Entities;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IAcademicSessionRepository : IAsyncRepository<AcademicSession>, IEntityCommonMethod<AcademicSession>
    {
       
        Task<IReadOnlyList<AcademicSession>> ListAllAsyncIncludeAllNoTrack();
        Task<IReadOnlyList<AcademicSession>> ListAllAsyncIncludeActiveNoTrack();

        long GetAcademicSubjectCountBySessionId(long sessionId);





        IEnumerable<AcademicSession> GetAllAcademicSession();




    }
}
