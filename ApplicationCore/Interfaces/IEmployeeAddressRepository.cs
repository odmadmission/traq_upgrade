﻿using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface IEmployeeAddressRepository
    {
        IEnumerable<EmployeeAddress> GetEmployeeAddressByEmployeeID(long empid);
        EmployeeAddress GetEmployeeAddressByAddressID(long addressid);
        EmployeeAddress GetEmployeeAddressById(long id);
        IEnumerable<EmployeeAddress> GetAllEmployeeAddresses();
        long CreateEmployeeAddress(EmployeeAddress newEmployeeAddress);
        bool UpdateEmployeeAddress(EmployeeAddress changedEmployeeAddress);
        bool DeleteEmployeeAddress(long id);
        bool DeleteAllEmployeeAddresses();
    }
}
