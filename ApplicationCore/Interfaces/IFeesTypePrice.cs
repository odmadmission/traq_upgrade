﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IFeesTypePrice
    {
        long CreateFeesTypePrice(FeesTypePriceData newFeesTypePrice);
        IEnumerable<FeesTypePriceData> GetAllFeesTypePrice(int? page, int? noofData);
        IEnumerable<SessionFeesTypeApplicableData> GetAllFeesTypePriceApplicableData();
        FeesTypePriceData GetFeesTypePriceByName(string name);
        FeesTypePriceData GetFeesTypePriceById(long id);
        long UpdateFeesTypePrice(FeesTypePriceData changedFeesTypePrice);
        long UpdateFeesTypePriceStatus(FeesTypePriceData changedFeesTypePrice);
        bool DeleteFeesTypePrice(long id);
        bool DeleteAllFeesTypePrice();

        bool DeleteSub(long id);
    }
}
