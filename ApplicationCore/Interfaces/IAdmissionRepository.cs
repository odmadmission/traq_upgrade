﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IAdmissionRepository
    {

        #region Login
        Login SaveLogin(Login login);


        #endregion

        #region OtpMessage
        OtpMessage GetOtpMessageById(long id);
        OtpMessage SaveOtpMessage(OtpMessage otpMessage);

        long UpdateOtpMessage(OtpMessage otpMessage);

        #endregion


        #region AdmissionStudent

        AdmissionStudent GetAdmissionStudentById(long id);
        AdmissionStudent CreateAdmissionStudent(AdmissionStudent admStudent);
        #endregion

        AdmissionFee GetAdmissionFeeById(string name,long id);

        #region AdmissionStudentStandard
        AdmissionStudentStandard GetAdmissionStudentStandardByAdmissionStudentId(long id);
        AdmissionStudentStandard CreateAdmissionStudentStandard(AdmissionStudentStandard admStudent);
        #endregion

        #region AdmissionStudentContact
        AdmissionStudentContact GetAdmissionStudentContactByAdmissionStudentId(long id);
        AdmissionStudentContact CreateAdmissionStudentContact(AdmissionStudentContact admStudent);
        #endregion

        long UpdateAdmissionStudent(AdmissionStudent admStudent);
        long UpdateAdmissionStudentStandard(AdmissionStudentStandard obj);

        long UpdateAdmissionStudentContact(AdmissionStudentContact obj);


        AcademicSession GetCurrentAdmissionSession();

        #region------------ Admission---------------------
        Admission GetAdmissionById(long id);

        Admission GetAdmissionByMobileAndSessionId(string mobile,long id);


        Admission GetAdmissionByLeadReferenceId(string id);
        IEnumerable<Admission> GetAllAdmission();
        long CreateAdmission(Admission newAdmission);
        bool UpdateAdmission(Admission changedAdmission);
        bool DeleteAdmission(long id);
        bool DeleteAllAdmission();

        AdmissionFormPayment GetAdmissionFormPaymentById(long id);
       // AdmissionFormPayment GetAdmissionFormPaymentByAdmissionId(string id);
        AdmissionFormPayment GetAdmissionFormPaymentByAdmissionId(long id);
        IEnumerable<AdmissionFormPayment> GetAllAdmissionFormPayment();
        long CreateAdmissionFormPayment(AdmissionFormPayment newAdmission);
        bool UpdateAdmissionFormPayment(AdmissionFormPayment changedAdmission);
        bool DeleteAdmissionFormPayment(long id);
        bool DeleteAllAdmissionFormPayment();

        #endregion
        #region--------- AdmissionSlot-----------------------
        AdmissionSlot GetAdmissionSlotById(long id);
        AdmissionSlot GetAdmissionSlotByName(string name);
        IEnumerable<AdmissionSlot> GetAllAdmissionSlot();
        long CreateAdmissionSlot(AdmissionSlot newAdmission);
        bool UpdateAdmissionSlot(AdmissionSlot changedAdmission);
        bool DeleteAdmissionSlot(long id);
        bool DeleteAllAdmissionSlot();

        #endregion
        #region----------- AdmissionDeclaration--------------
        AdmissionStudentDeclaration GetAdmissionStudentDeclarationById(long id);
        IEnumerable<AdmissionStudentDeclaration> GetAllAdmissionStudentDeclaration();
        long CreateAdmissionStudentDeclaration(AdmissionStudentDeclaration newAdmission);
        bool UpdateAdmissionStudentDeclaration(AdmissionStudentDeclaration changedAdmission);
        bool DeleteAdmissionStudentDeclaration(long id);
        bool DeleteAllAdmissionStudentDeclaration();

        #endregion
        #region----------- Admission Source--------------
        AdmissionSource GetAdmissionSourceById(long id);
        IEnumerable<AdmissionSource> GetAllAdmissionSource();
        long CreateAdmissionSource(AdmissionSource newAdmission);
        bool UpdateAdmissionSource(AdmissionSource changedAdmission);
        bool DeleteAdmissionSource(long id);
        bool DeleteAllAdmissionSource();

        #endregion
        #region----------- Admission Standard Setting--------------
        AdmissionStandardSetting GetAdmissionStandardSettingById(long id);
        IEnumerable<AdmissionStandardSetting> GetAllAdmissionStandardSetting();
        long CreateAdmissionStandardSetting(AdmissionStandardSetting newAdmission);
        bool UpdateAdmissionStandardSetting(AdmissionStandardSetting changedAdmission);
        bool DeleteAdmissionStandardSetting(long id);
        bool DeleteAllAdmissionStandardSetting();

        #endregion
        #region----------- Admission Source--------------
        AdmissionTimeline GetAdmissionTimelineById(long id);
        IEnumerable<AdmissionTimeline> GetAllAdmissionTimeline();
        long CreateAdmissionTimeline(AdmissionTimeline newAdmission);
        bool UpdateAdmissionTimeline(AdmissionTimeline changedAdmission);
        bool DeleteAdmissionTimeline(long id);
        bool DeleteAllAdmissionTimeline();

        #endregion

        #region--------- AdmissionStandardExamQuestion-----------------------
        AdmissionStandardExamQuestion GetAdmissionStandardExamQuestionById(long id);
        //AdmissionSlot GetAdmissionStandardExamQuestionByName(string name);
        IEnumerable<AdmissionStandardExamQuestion> GetAllAdmissionStandardExamQuestion();
        long CreateAdmissionStandardExamQuestion(AdmissionStandardExamQuestion newAdmissionStandardExamQuestion);
        bool UpdateAdmissionStandardExamQuestion(AdmissionStandardExamQuestion changedAdmissionStandardExamQuestion);
        //bool DeleteAdmissionStandardExamQuestion(long id);
        //bool DeleteAllAdmissionStandardExamQuestion();
        #endregion

    }
    #region-------------- New Table 1-------------------
    public interface IAdmissionStudentKitRepository : IAsyncRepository<AdmissionStudentKit>, IEntityCommonMethod<AdmissionStudentKit>
    {
        Task<IReadOnlyList<AdmissionStudentKit>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionCounsellorRepository : IAsyncRepository<AdmissionCounsellor>, IEntityCommonMethod<AdmissionCounsellor>
    {
        Task<IReadOnlyList<AdmissionCounsellor>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionVerifierRepository : IAsyncRepository<AdmissionVerifier>, IEntityCommonMethod<AdmissionVerifier>
    {
        Task<IReadOnlyList<AdmissionVerifier>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionInterViewerRepository : IAsyncRepository<AdmissionInterViewer>, IEntityCommonMethod<AdmissionInterViewer>
    {
        Task<IReadOnlyList<AdmissionInterViewer>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStandardSeatQuotaRepository : IAsyncRepository<AdmissionStandardSeatQuota>, IEntityCommonMethod<AdmissionStandardSeatQuota>
    {
        Task<IReadOnlyList<AdmissionStandardSeatQuota>> ListAllAsyncIncludeActiveNoTrack();
    }

    public interface IAdmissionDocumentAssignRepository : IAsyncRepository<AdmissionDocumentAssign>, IEntityCommonMethod<AdmissionDocumentAssign>
    {
     //   Task<IReadOnlyList<AdmissionStandardSeatQuota>> ListAllAsyncIncludeActiveNoTrack();
    }
    //public interface IAdmissionCounsellorRepository : IAsyncRepository<AdmissionCounsellor>, IEntityCommonMethod<AdmissionCounsellor>
    //{
    //    //   Task<IReadOnlyList<AdmissionStandardSeatQuota>> ListAllAsyncIncludeActiveNoTrack();
    //}
    public interface IAdmissionStudentRepository : IAsyncRepository<AdmissionStudent>, IEntityCommonMethod<AdmissionStudent>
    {
        Task<IReadOnlyList<AdmissionStudent>> ListAllAsyncIncludeActiveNoTrack();
        Task<AdmissionStudent> GetByAdmissionId(long admissionId);
    }
    public interface IAdmissionStudentAcademicRepository : IAsyncRepository<AdmissionStudentAcademic>, IEntityCommonMethod<AdmissionStudentAcademic>
    {
        Task<IReadOnlyList<AdmissionStudentAcademic>> ListAllAsyncIncludeActiveNoTrack();
        List<AdmissionStudentAcademic> AllAdmissionStudentAcademic();
    }
    public interface IAdmissionStudentAddressRepository : IAsyncRepository<AdmissionStudentAddress>, IEntityCommonMethod<AdmissionStudentAddress>
    {
        Task<IReadOnlyList<AdmissionStudentAddress>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStudentContactRepository : IAsyncRepository<AdmissionStudentContact>, IEntityCommonMethod<AdmissionStudentContact>
    {
        Task<AdmissionStudentContact> GetStudentById(long admStudentId);
        Task<IReadOnlyList<AdmissionStudentContact>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStudentCounselorRepository : IAsyncRepository<AdmissionStudentCounselor>, IEntityCommonMethod<AdmissionStudentCounselor>
    {
        Task<IReadOnlyList<AdmissionStudentCounselor>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStudentDocumentRepository : IAsyncRepository<AdmissionStudentDocument>, IEntityCommonMethod<AdmissionStudentDocument>
    {
        Task<IReadOnlyList<AdmissionStudentDocument>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStudentExamRepository : IAsyncRepository<AdmissionStudentExam>, IEntityCommonMethod<AdmissionStudentExam>
    {
        Task<IReadOnlyList<AdmissionStudentExam>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStudentInterviewRepository : IAsyncRepository<AdmissionStudentInterview>, IEntityCommonMethod<AdmissionStudentInterview>
    {
        Task<IReadOnlyList<AdmissionStudentInterview>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStudentLanguageRepository : IAsyncRepository<AdmissionStudentLanguage>, IEntityCommonMethod<AdmissionStudentLanguage>
    {
        Task<IReadOnlyList<AdmissionStudentLanguage>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStudentParentRepository : IAsyncRepository<AdmissionStudentParent>, IEntityCommonMethod<AdmissionStudentParent>
    {
        Task<IReadOnlyList<AdmissionStudentParent>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStudentProficiencyRepository : IAsyncRepository<AdmissionStudentProficiency>, IEntityCommonMethod<AdmissionStudentProficiency>
    {
        Task<IReadOnlyList<AdmissionStudentProficiency>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStudentReferenceRepository : IAsyncRepository<AdmissionStudentReference>, IEntityCommonMethod<AdmissionStudentReference>
    {
        Task<IReadOnlyList<AdmissionStudentReference>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStudentStandardRepository : IAsyncRepository<AdmissionStudentStandard>, IEntityCommonMethod<AdmissionStudentStandard>
    {
        Task<IReadOnlyList<AdmissionStudentStandard>> ListAllAsyncIncludeActiveNoTrack();

        Task<AdmissionStudentStandard> GetByStudentId(long admissionStudentId);
    }
    public interface IAdmissionStudentTransportRepository : IAsyncRepository<AdmissionStudentTransport>, IEntityCommonMethod<AdmissionStudentTransport>
    {
        Task<IReadOnlyList<AdmissionStudentTransport>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionFormPaymentRepository : IAsyncRepository<AdmissionFormPayment>, IEntityCommonMethod<AdmissionFormPayment>
    {
        Task<IReadOnlyList<AdmissionFormPayment>> ListAllAsyncIncludeActiveNoTrack();
        Task<AdmissionFormPayment> GetByAdmissionId(long id);


    }
    public interface IAdmissionStandardExamDateRepository : IAsyncRepository<AdmissionStandardExamDate>, IEntityCommonMethod<AdmissionStandardExamDate>
    {
        Task<IReadOnlyList<AdmissionStandardExamDate>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStandardExamRepository : IAsyncRepository<AdmissionStandardExam>, IEntityCommonMethod<AdmissionStandardExam>
    {
        Task<IReadOnlyList<AdmissionStandardExam>> ListAllAsyncIncludeActiveNoTrack();
    }
    #endregion-------------
    #region------------------New Table 2------------------
    public interface IAdmissionDocumentRepository : IAsyncRepository<AdmissionDocument>, IEntityCommonMethod<AdmissionDocument>
    {
        Task<IReadOnlyList<AdmissionDocument>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionDocumentVerificationAssignRepository : IAsyncRepository<AdmissionDocumentVerificationAssign>, IEntityCommonMethod<AdmissionDocumentVerificationAssign>
    {
        Task<IReadOnlyList<AdmissionDocumentVerificationAssign>> ListAllAsyncIncludeActiveNoTrack();
    }
    

    public interface IAdmissionExamDateAssignRepository : IAsyncRepository<AdmissionExamDateAssign>, IEntityCommonMethod<AdmissionExamDateAssign>
    {
        Task<IReadOnlyList<AdmissionExamDateAssign>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionPersonalInterviewAssignRepository : IAsyncRepository<AdmissionPersonalInterviewAssign>, IEntityCommonMethod<AdmissionPersonalInterviewAssign>
    {
        Task<IReadOnlyList<AdmissionPersonalInterviewAssign>> ListAllAsyncIncludeActiveNoTrack();
    }

    #endregion
    #region------------------New Tabl 3----------------
    public interface IMapClassToAdmissionRepository : IAsyncRepository<MapClassToAdmission>, IEntityCommonMethod<MapClassToAdmission>
    {
        Task<IReadOnlyList<MapClassToAdmission>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAdmissionStandardExamResultDateRepository : IAsyncRepository<AdmissionStandardExamResultDate>, IEntityCommonMethod<AdmissionStandardExamResultDate>
    {
        Task<IReadOnlyList<AdmissionStandardExamResultDate>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface ICounsellorStatusRepository : IAsyncRepository<CounsellorStatus>, IEntityCommonMethod<CounsellorStatus>
    {
        Task<IReadOnlyList<CounsellorStatus>> ListAllAsyncIncludeActiveNoTrack();
    }

    #endregion
    #region---------------fee-----------------------
    public interface IAdmissionFeeRepository : 
        IAsyncRepository<AdmissionFee>, IEntityCommonMethod<AdmissionFee>
    {
        Task<IReadOnlyList<AdmissionFee>> ListAllAsyncIncludeActiveNoTrack();

        Task<AdmissionFee> GetAdmissionFeeByName(string name,long sessionId);

    }
    #endregion
    #region -------email && Smg--------------
    public interface IMasterEmailRepository : IAsyncRepository<MasterEmail>, IEntityCommonMethod<MasterEmail>
    {
       
    }
    public interface IMasterSMSRepository : IAsyncRepository<MasterSMS>, IEntityCommonMethod<MasterSMS>
    {
       
    }
    #endregion----------------------------

    #region-----------------Admission Ecounselling ------------------------
    public interface IAdmissionEcounsellingRepository : IAsyncRepository<AdmissionEcounselling>, IEntityCommonMethod<AdmissionEcounselling>
    {
      
    }
    public interface IAdmissionEcounsellingDateRepository : IAsyncRepository<AdmissionEcounsellingDate>, IEntityCommonMethod<AdmissionEcounsellingDate>
    {

    }
    public interface IAdmissionEcounsellingSlotRepository : IAsyncRepository<AdmissionEcounsellingSlot>, IEntityCommonMethod<AdmissionEcounsellingSlot>
    {

    }
    public interface IAdmissionEcounsellingTimelineRepository : IAsyncRepository<AdmissionEcounsellingTimeline>, IEntityCommonMethod<AdmissionEcounsellingTimeline>
    {

    }
    #endregion
    #region-----------------Admission Cpampus Tour ------------------------
    public interface IAdmissionCampusTourRepository : IAsyncRepository<AdmissionCampusTour>, IEntityCommonMethod<AdmissionCampusTour>
    {

    }
    public interface IAdmissionCampusTourDateRepository : IAsyncRepository<AdmissionCampusTourDate>, IEntityCommonMethod<AdmissionCampusTourDate>
    {

    }
    public interface IAdmissionCampusTourSlotRepository : IAsyncRepository<AdmissionCampusTourSlot>, IEntityCommonMethod<AdmissionCampusTourSlot>
    {

    }
    public interface IAdmissionCampusTourTimelineRepository : IAsyncRepository<AdmissionCampusTourTimeline>, IEntityCommonMethod<AdmissionCampusTourTimeline>
    {

    }
    #endregion
}
