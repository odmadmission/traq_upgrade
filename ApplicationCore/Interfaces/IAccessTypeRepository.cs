﻿using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IAccessTypeRepository
    {

        AccessType GetAccessTypeByName(string name);
        AccessType GetAccessTypeById(long id);
        IEnumerable<AccessType> GetAllAccessType();
      
        long CreateAccessType(AccessType newAccessType);
        bool UpdateAccessType(AccessType changedAccessType);
        bool DeleteAccessType(long id);

        bool DeleteAllAccessType();

    }
}
