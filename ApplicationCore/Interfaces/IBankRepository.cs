﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IBankRepository
    {
        Bank GetBankByName(string name);
        Bank GetBankById(long id);
        IEnumerable<Bank> GetAllBank();
        long CreateBank(Bank newBank);
        bool UpdateBank(Bank changedBank);
        bool DeleteBank(long id);
        bool DeleteAllBank();
    }
}
