﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IStandardRepository
    {
        Standard GetStandardByName(string name);
        Standard GetStandardById(long id);
        IEnumerable<Standard> GetAllStandard();
        long CreateStandard(Standard newStandard);
        bool UpdateStandard(Standard changedStandard);
        bool DeleteStandard(long id);
        bool DeleteAllStandard();
      

    }
}
