﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface ISessionScholarship
    {
        long CreateSessionScholarship(SessionScholarship newSessionScholarship);
        IEnumerable<SessionScholarship> GetAllSessionScholarship();
        SessionScholarship GetSessionScholarshipByName(string name);
         PaginationSessionScholarship GetPaginationSessionScholarship(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue);
        SessionScholarship GetSessionScholarshipById(long id);
        bool UpdateSessionScholarship(SessionScholarship changedSessionScholarship);
        bool DeleteSessionScholarship(long id);
        bool DeleteAllSessionScholarship();
    }
}
