﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface ISessionScholarshipDiscountApplicableRepository
    {
        long CreateSessionScholarshipDiscountApplicable(SessionScholarshipDiscountApplicable newSessionScholarshipDiscountApplicable);
        IEnumerable<SessionScholarshipDiscountApplicable> GetAllSessionScholarshipDiscountApplicable();
        SessionScholarshipDiscountApplicable GetSessionScholarshipDiscountApplicableByName(string name);
        PaginationSessionScholarshipDiscountApplicable GetPaginationSessionScholarshipDiscountApplicable(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue);
        SessionScholarshipDiscountApplicable GetSessionScholarshipDiscountApplicableById(long id);
        bool UpdateSessionScholarshipDiscountApplicable(SessionScholarshipDiscountApplicable changedSessionScholarshipDiscountApplicable);
        bool DeleteSessionScholarshipDiscountApplicable(long id);
        bool DeleteAllSessionScholarshipDiscountApplicable();
    }
}
