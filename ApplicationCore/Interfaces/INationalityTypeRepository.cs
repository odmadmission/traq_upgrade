﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface INationalityTypeRepository
    {

        NationalityType GetNationalityByName(string name);
        NationalityType GetNationalityById(long id);
        IEnumerable<NationalityType> GetAllNationalities();
        long CreateNationality(NationalityType newNationality);
        bool UpdateNationality(NationalityType changedNationality);
        bool DeleteNationality(long id);
        bool DeleteAllNationalities();
    }
}
