﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface ISessionScholarshipDiscountRepository
    {
        long CreateSessionScholarshipDiscount(SessionScholarshipDiscount newSessionScholarshipDiscount);
        IEnumerable<SessionScholarshipDiscount> GetAllSessionScholarshipDiscount();
        SessionScholarshipDiscount GetSessionScholarshipDiscountByName(string name);
        PaginationSessionScholarshipDiscount GetPaginationSessionScholarshipDiscount(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue);
        SessionScholarshipDiscount GetSessionScholarshipDiscountById(long id);
        bool UpdateSessionScholarshipDiscount(SessionScholarshipDiscount changedSessionScholarshipDiscount);
        bool DeleteSessionScholarshipDiscount(long id);
        bool DeleteAllSessionScholarshipDiscount();
    }
}
