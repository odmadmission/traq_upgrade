﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IRoleRepository
    {
        Role GetRoleByName(string name);
        Role GetRoleById(long id);
        IEnumerable<Role> GetAllRole();
        long CreateRole(Role newRole);

        long CreateSuperAdminRole();
        bool UpdateRole(Role changedRole);
        bool DeleteRole(long id);
        bool DeleteAllRole();

    }
}
