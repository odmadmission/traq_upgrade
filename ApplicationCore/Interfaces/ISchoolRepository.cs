﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface ICategoryRepository : IAsyncRepository<Category>, IEntityCommonMethod<Category>
    {
        Task<Category> GetCategoryNameById(long id);
    }
    public interface IChapterRepository : IAsyncRepository<Chapter>, IEntityCommonMethod<Chapter>
    {
        long GetChapterCountByBoardStandardId(long SubjectId);
      

    }

    public interface IConceptRepository : IAsyncRepository<Concept>, IEntityCommonMethod<Concept>
    {
        long GetConceptCountBychapterid(long ChapterId);
    }

    public interface ISubjectRepository : IAsyncRepository<Subject>, IEntityCommonMethod<Subject>
    {
       
    }
    public interface IAcademicStandardWingRepository : IAsyncRepository<AcademicStandardWing>, IEntityCommonMethod<AcademicStandardWing>
    {

    }
    public interface IBoardStandardRepository : IAsyncRepository<BoardStandard>, IEntityCommonMethod<BoardStandard>
    {
        long GetStandardCountByBoardId(long BoardId);
        long GetBoardIdByBoardStandardId(long BoardStandardId);
        Task<IReadOnlyList<BoardStandard>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IOtpMessageRepository : IAsyncRepository<OtpMessage>, IEntityCommonMethod<OtpMessage>
    {

    }
    public interface IOrganizationAcademicRepository : IAsyncRepository<OrganizationAcademic>, IEntityCommonMethod<OrganizationAcademic>
    {
        long GetOrganizationAcademicCountBySessionId(long sessionId);
        Task<IReadOnlyList<OrganizationAcademic>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAcademicStandardRepository : IAsyncRepository<AcademicStandard>, IEntityCommonMethod<AcademicStandard>
    {
        Task<IReadOnlyList<AcademicStandard>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAcademicStudentRepository : IAsyncRepository<AcademicStudent>, IEntityCommonMethod<AcademicStudent>
    {
        Task<IReadOnlyList<AcademicStudent>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface ITimeTablePeriodRepository : IAsyncRepository<TimeTablePeriod>, IEntityCommonMethod<TimeTablePeriod>
    {
        Task<IReadOnlyList<TimeTablePeriod>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAcademicTimeTableRepository : IAsyncRepository<AcademicTimeTable>, IEntityCommonMethod<AcademicTimeTable>
    {
        Task<IReadOnlyList<AcademicTimeTable>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAcademicSectionStudentRepository : IAsyncRepository<AcademicSectionStudent>, IEntityCommonMethod<AcademicSectionStudent>
    {
        Task<IReadOnlyList<AcademicSectionStudent>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IAcademicSubjectRepository : IAsyncRepository<AcademicSubject>, IEntityCommonMethod<AcademicSubject>
    {
    }
    public interface ISyallbusRepository : IAsyncRepository<Syllabus>, IEntityCommonMethod<Syllabus>
    {
    }
    public interface IAcademicChapterRepository : IAsyncRepository<AcademicChapter>, IEntityCommonMethod<AcademicChapter>
    {
    }
    public interface IAcademicSyllabusRepository : IAsyncRepository<AcademicSyllabus>, IEntityCommonMethod<AcademicSyllabus>
    {
    }
    public interface IFilePathUrlRepository : IAsyncRepository<FilePathUrl>, IEntityCommonMethod<FilePathUrl>
    {


    }
    //public interface IBoardStandardStreamRepository : IAsyncRepository<BoardStandardStream>, IEntityCommonMethod<BoardStandardStream>
    //{
    //    long GetStreamCountByBoardStandardId(long BoardStandardId);
    //    long GetSubjectCountByBoardStandardId(long BoardStandardId);
    //    long GetSubjectsreamCountByBoardStandardId(long boardStandardSreamID);
    //    Task<IReadOnlyList<BoardStandardStream>> ListAllAsyncIncludeActiveNoTrack();


    //}

    public interface IAcademicSectionRepository : IAsyncRepository<AcademicSection>, IEntityCommonMethod<AcademicSection>
    {
        Task<IReadOnlyList<AcademicSection>> ListAllAsyncIncludeActiveNoTrack();
    }
    //public interface IAcademicStandardStreamRepository : IAsyncRepository<AcademicStandardStream>, IEntityCommonMethod<AcademicStandardStream>
    //{
    //    Task<IReadOnlyList<AcademicStandardStream>> ListAllAsyncIncludeActiveNoTrack();
    //}
    public interface IPeriodEmployeeRepository : IAsyncRepository<PeriodEmployee>, IEntityCommonMethod<PeriodEmployee>
    {
        Task<IReadOnlyList<PeriodEmployee>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IPeriodEmployeeStatusRepository : IAsyncRepository<PeriodEmployeeStatus>, IEntityCommonMethod<PeriodEmployeeStatus>
    {
        Task<IReadOnlyList<PeriodEmployeeStatus>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface IPeriodSyllabusRepository : IAsyncRepository<PeriodSyllabus>, IEntityCommonMethod<PeriodSyllabus>
    {
        Task<IReadOnlyList<PeriodSyllabus>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface ITeacherRepository : IAsyncRepository<Teacher>, IEntityCommonMethod<Teacher>
    {
        Task<IReadOnlyList<Teacher>> ListAllAsyncIncludeActiveNoTrack();
    }
    public interface ICalenderEngagementTypeRepository : IAsyncRepository<CalenderEngagementType>, IEntityCommonMethod<CalenderEngagementType>
    {


    }
    public interface ICalenderEngagementRepository : IAsyncRepository<CalenderEngagement>, IEntityCommonMethod<CalenderEngagement>
    {
        Task<CalenderEngagement> GetByAcademicCalenderIdAndDate(long calenderId,DateTime date);
        long GetDayCountByTypeId(long calenderEngagementTypeId);
    }

    public interface IAcademicCalenderRepository : IAsyncRepository<AcademicCalender>, IEntityCommonMethod<AcademicCalender>
    {


    }

    public interface IAcademicConceptRepository : IAsyncRepository<AcademicConcept>, IEntityCommonMethod<AcademicConcept>
    {

    }
    public interface IAcademicTeacherSubjectRepository : IAsyncRepository<AcademicTeacherSubject>, IEntityCommonMethod<AcademicTeacherSubject>
    {

        Task<IReadOnlyList<AcademicTeacherSubject>> ListAllAsyncIncludeAllNoTrack();

    }


    public interface IAcademicTeacherRepository : IAsyncRepository<AcademicTeacher>, IEntityCommonMethod<AcademicTeacher>
    {
        Task<IReadOnlyList<AcademicTeacher>> ListAllAsyncIncludeAllNoTrack();

    }
    public interface IAcademicTeacherSubjectsTimeLineRepository : IAsyncRepository<AcademicTeacherSubjectsTimeLine>, IEntityCommonMethod<AcademicTeacherSubjectsTimeLine>
    {


    }
    public interface IClassTimingRepository : IAsyncRepository<ClassTiming>, IEntityCommonMethod<ClassTiming>
    {

        Task<IReadOnlyList<ClassTiming>> ListAllAsyncIncludeAllNoTrack();

    }
    public interface ITeacherTimeLineRepository : IAsyncRepository<TeacherTimeLine>, IEntityCommonMethod<TeacherTimeLine>
    {        

    }
    public interface IAcademicTeacherTimeLineRepository : IAsyncRepository<AcademicTeacherTimeLine>, IEntityCommonMethod<AcademicTeacherTimeLine>
    {

    }

}
