﻿using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IEmployeeDocumentAttachmentRepository
    {
        EmployeeDocumentAttachment GetEmployeeDocumentAttachmentById(long id);
        IEnumerable<EmployeeDocumentAttachment> GetAllEmployeeDocumentAttachments();
        long CreateEmployeeDocumentAttachment(EmployeeDocumentAttachment newEmployeeDocumentAttachment);
        bool UpdateEmployeeDocumentAttachment(EmployeeDocumentAttachment changedEmployeeDocumentAttachment);
        bool DeleteEmployeeDocumentAttachment(long id);
        bool DeleteAllEmployeeDocumentAttachments();
    }
}
