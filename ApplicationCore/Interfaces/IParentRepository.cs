﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IParentRepository
    {
        Parent GetParentByPrimaryMobile(string mobile);
        Parent GetParentById(long id);
        IEnumerable<Parent> GetAllParent();
        long CreateParent(Parent newParent);
        bool UpdateParent(Parent changedParent);
        bool DeleteParent(long id);
        bool DeleteAllParent();

    }
}
