﻿using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities.StudentPaymentAggregate;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IPaymentType
    {
        long CreatePaymentType(PaymentType newpaymentType);
        IEnumerable<PaymentType> GetAllPaymentType();
        PaymentType GetPaymentTypeByName(string name);

        PaymentType GetPaymentTypeByBillDeskCode(string code);

        PaymentType GetPaymentTypeById(long id);
        bool UpdatePaymentType(PaymentType changedPaymentType);
        bool DeletePaymentType(long id);
        bool DeleteAllPaymentType();
       // IEnumerable<PaymentType> CheckPaymentType();
    }
}
