﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface IAdmitcardInstructionsRepository
    {
        // AdmicardInstructions
        AdmitcardInstructions GetAdmicardInstructionsByName(string name);
        AdmitcardInstructions GetAdmicardInstructionsById(long id);
        IEnumerable<AdmitcardInstructions> GetAllAdmicardInstructions();
        long CreateAdmicardInstructions(AdmitcardInstructions newAdmicardInstructions);
        bool UpdateAdmicardInstructions(AdmitcardInstructions changedAdmicardInstructions);
        bool DeleteAdmicardInstructions(long id);
        bool DeleteAllAdmicardInstructions();
    }
}
