﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IStateRepository
    {
        State GetStateByName(string name);
        State GetStateById(long id);
        IEnumerable<State> GetAllState();
        IEnumerable<State> GetAllStateByCountryId(long countryId);
        long CreateState(State newState);
        bool UpdateState(State changedState);
        bool DeleteState(long id);
        bool DeleteAllState();

        long CreateAllState(List<State> newState);

    }
}
