﻿using OdmErp.ApplicationCore.Entities.StudentAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IProfessionTypeRepository
    {
        ProfessionType GetProfessionTypesByName(string name);
        ProfessionType GetProfessionTypesById(long id);
        IEnumerable<ProfessionType> GetAllProfessionTypes();
        
    }
}
