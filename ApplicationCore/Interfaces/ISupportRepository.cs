﻿using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Entities.SupportAggregate;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface ISupportRepository
    {
        #region Building   
        Building GetByBuildingId(long id);
        IEnumerable<Building> GetAllBuilding();
        long CreateBuilding(Building obj);
        bool UpdateBuilding(Building changed);
        bool DeleteBuilding(long id);
        bool DeleteAllBuilding();
        #endregion

        #region SupportCategory
        SupportCategory GetByCategoryId(long id);
        IEnumerable<SupportCategory> GetAllCategory();
        long CreateCategory(SupportCategory obj);
        bool UpdateCategory(SupportCategory changed);
        bool DeleteCategory(long id);
        bool DeleteAllCategory();
        #endregion

        #region SupportPriority
        SupportPriority GetByPriorityId(long id);
        IEnumerable<SupportPriority> GetAllPriority();
        long CreatePriority(SupportPriority obj);
        bool UpdatePriority(SupportPriority changed);
        bool DeletePriority(long id);
        bool DeleteAllPriority();
        #endregion

        #region Room
        Room GetByRoomId(long id);
        IEnumerable<Room> GetAllRoom();
        long CreateRoom(Room obj);
        bool UpdateRoom(Room changed);
        bool DeleteRoom(long id);
        bool DeleteAllRoom();
        #endregion

        #region SupportStatus
        SupportStatus GetByStatusId(long id);
        IEnumerable<SupportStatus> GetAllStatus();
        long CreateStatus(SupportStatus obj);
        bool UpdateStatus(SupportStatus changed);
        bool DeleteStatus(long id);
        bool DeleteAllStatus();
        #endregion

        #region SupportSubCategory
        SupportSubCategory GetBySubCategoryId(long id);
        IEnumerable<SupportSubCategory> GetAllSubCategory();
        long CreateSubCategory(SupportSubCategory obj);
        bool UpdateSubCategory(SupportSubCategory changed);
        bool DeleteSubCategory(long id);
        bool DeleteAllSubCategory();
        #endregion

        #region SupportRequest
        SupportRequest GetBySupportRequestId(long id);

        SupportRequest GetBySupportRequestIdNoTracking(long id);

        IEnumerable<SupportRequest> GetAllSupportRequest();
        IEnumerable<SupportRequest> GetAllSupportRequestWithinactive();
        IEnumerable<Int64> GetSupportTypeIdsByDepartmentIdIn(List<Int64> id);
        IEnumerable<SupportRequest> GetAllBySupportTypeId(List<Int64> id);

        IEnumerable<SupportRequest> GetApprovalSupportRequestBySupportTypeId(List<Int64> id);

        IEnumerable<SupportRequest> GetAllSupportRequestByUserId(long userId);

        long CreateSupportRequest(SupportRequest obj);
        bool UpdateSupportRequest(SupportRequest changed);
        bool DeleteSupportRequest(long id);
        bool DeleteAllSupportRequest();
        IEnumerable<SupportRequest> GetBySupportRequestSupportTypeId(List<Int64> id);
        IEnumerable<SupportRequest> GetBySupportRequestByNotSupportTypeId(List<Int64> id);
        #endregion

        #region SupportAttachment
        SupportAttachment GetByAttachmentId(long id);
        IEnumerable<SupportAttachment> GetAllAttachment();
        long CreateAttachment(SupportAttachment obj);
        bool UpdateAttachment(SupportAttachment changed);
        bool DeleteAttachment(long id);
        bool DeleteAllAttachment();

        IEnumerable<SupportAttachment> GetAttachmentsBySupportIdAndType(long supportId,string type);

        #endregion

        #region Location
        Location GetByLocationId(long id);
        IEnumerable<Location> GetAllLocation();
        long CreateLocation(Location obj);
        bool UpdateLocation(Location changed);
        bool DeleteLocation(long id);
        bool DeleteAllLocation();
        #endregion

        #region RoomCategory
        RoomCategory GetByRoomCategoryId(long id);
        IEnumerable<RoomCategory> GetAllRoomCategory();
        long CreateRoomCategory(RoomCategory obj);
        bool UpdateRoomCategory(RoomCategory changed);
        bool DeleteRoomCategory(long id);
        bool DeleteAllRoomCategory();
        #endregion

        #region Floor
        Floor GetByFloorId(long id);
        IEnumerable<Floor> GetAllFloor();
        long CreateFloor(Floor obj);
        bool UpdateFloor(Floor changed);
        bool DeleteFloor(long id);
        bool DeleteAllFloor();
        #endregion

        #region DepartmentSupport
        DepartmentSupport GetByDepartmentSupportId(long id);
        IEnumerable<DepartmentSupport> GetAllDepartmentSupport();
        long CreateDepartmentSupport(DepartmentSupport obj);
        bool UpdateDepartmentSupport(DepartmentSupport changed);
        bool DeleteDepartmentSupport(long id);
        bool DeleteAllDepartmentSupport();
        IEnumerable<DepartmentSupport> GetSupportByDepartmentId(long id);
        #endregion

        #region SupportType
        SupportType GetBySupportTypeId(long id);
        IEnumerable<SupportType> GetAllSupportType();
        long CreateSupportType(SupportType obj);
        bool UpdateSupportType(SupportType changed);
        bool DeleteSupportType(long id);
        bool DeleteAllSupportType();

        IEnumerable<SupportType> GetAllSupportTypeById(List<long> ids);

        #endregion

        #region SupportTimeLine
        SupportTimeLine GetBySupportTimeLineId(long id);
        IEnumerable<SupportTimeLine> GetAllSupportTimeLine();
        long CreateSupportTimeLine(SupportTimeLine obj);
        bool UpdateSupportTimeLine(SupportTimeLine changed);
        bool DeleteSupportTimeLine(long id);
        bool DeleteAllSupportTimeLine();

        IEnumerable<SupportTimeLine> GetTimelineBySupportId(long supportId);

       

        #endregion

        #region SupportComment
        SupportComment GetBySupportCommentId(long id);
        IEnumerable<SupportComment> GetAllSupportComment();
        long CreateSupportComment(SupportComment obj);
        bool UpdateSupportComment(SupportComment changed);
        bool DeleteSupportComment(long id);
        bool DeleteAllSupportComment();

        SupportComment GetByLastTimelineStatus(long supportId, string status);

        int GetUnreadSupportCommentCountBySupportID(long supportRequestId);

        SupportComment GetLastCommentBySupportId(long supportId);

        int GetUnreadSupportCommentCountBySupportIdAndInsertedId(long id,long empId);

        #endregion

    }
}
