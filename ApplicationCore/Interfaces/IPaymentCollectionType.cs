﻿using OdmErp.ApplicationCore.Entities.StudentPaymentAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IPaymentCollectionType
    {
        long CreatePaymentCollectionType(PaymentCollectionType paymentCollectionType);
        IEnumerable<PaymentCollectionType> GetAllPaymentCollectionType();
        IEnumerable<PaymentCollectionTypeData> GetAllPaymentCollectionTypeData();
        PaymentCollectionType GetPaymentCollectionTypeByName(string name);
        PaymentCollectionType GetPaymentCollectionTypeById(long id);
        long UpdatePaymentCollectionType(PaymentCollectionType changedPaymentCollectionType);
        long UpdatePaymentCollectionTypeStatus(PaymentCollectionType changedPaymentCollectionType);
        bool DeletePaymentCollectionType(long id);
        bool DeleteAllPaymentCollectionType();
        bool DeleteSub(long id);
    }
}
