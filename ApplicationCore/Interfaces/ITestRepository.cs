﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
     public interface ITestRepository
    {
      
        IEnumerable<string> GetAllTablesName();
    }
    public class GetAllTablesNameFromDatabase
    {
        public string TABLE_NAME { get; set; }
    }
}
