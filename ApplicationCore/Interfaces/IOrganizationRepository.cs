﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IOrganizationRepository
    {
        Organization GetOrganizationByName(string name);
        Organization GetOrganizationById(long id);
        IEnumerable<Organization> GetAllOrganization();
        long CreateOrganization(Organization newOrganization);
        bool UpdateOrganization(Organization changedOrganization);
        bool DeleteOrganization(long id);
        bool DeleteAllOrganization();

        IEnumerable<Organization> GetAllOrganizationByGroupId(long groupId);
        long GetAllOrganizationCountByGroupId(long groupId);





    }
}
