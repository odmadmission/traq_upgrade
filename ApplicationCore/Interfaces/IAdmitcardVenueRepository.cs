﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface IAdmitcardVenueRepository
    {
        //AdmitcardVenue
        AdmitcardVenue GetAdmitcardVenueByName(string name);
        AdmitcardVenue GetAdmitcardVenueById(long id);
        IEnumerable<AdmitcardVenue> GetAllAdmitcardVenue();
        long CreateAdmitcardVenue(AdmitcardVenue newAdmitcardVenue);
        bool UpdateAdmitcardVenue(AdmitcardVenue changedAdmitcardVenue);
        bool DeleteAdmitcardVenue(long id);
        bool DeleteAllAdmitcardVenue();
    }
}
