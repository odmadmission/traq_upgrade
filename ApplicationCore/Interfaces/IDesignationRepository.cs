﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IDesignationRepository
    {
        Designation GetDesignationByName(string name);
        Designation GetDesignationById(long id);
        IEnumerable<Designation> GetAllDesignation();
        long CreateDesignation(Designation newDesignation);
        bool UpdateDesignation(Designation changedDesignation);
        bool DeleteDesignation(long id);
        bool DeleteAllDesignation();
        IEnumerable<long> GetDesignationByDepartmentId(long id);

        IEnumerable<Designation> GetDesignationListByDepartmentId(long id);

    }
}
