﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface ILogingRepository : IAsyncRepository<Login>, IEntityCommonMethod<Login>
    {

    }
}
