﻿using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface IEmployeeDesignationRepository
    {

       
        EmployeeDesignation GetEmployeeDesignationById(long id);
        IEnumerable<EmployeeDesignation> GetAllEmployeeDesignations();
        IEnumerable<EmployeeDesignation> GetAllEmployeeDesignationsByEmployeeId(long empid);

        IEnumerable<EmployeeDesignation> GetAllEmployeeDesignationsByDesignationId(List<Int64> desgIds);

        long CreateEmployeeDesignation(EmployeeDesignation newEmployeeDesignation);
        bool UpdateEmployeeDesignation(EmployeeDesignation changedEmployeeDesignation);
        bool DeleteEmployeeDesignation(long id);
        bool DeleteAllEmployeeDesignations();

    }
}
