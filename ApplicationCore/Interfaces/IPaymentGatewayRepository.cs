﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IPaymentGatewayRepository
    {
        #region Payment Gateway
        PaymentGateway GetPaymentGatewayById(long id);
        IEnumerable<PaymentGateway> GetAllPaymentGateway();
        long CreatePaymentGateway(PaymentGateway obj);
        bool UpdatePaymentGateway(PaymentGateway changed);
        bool DeletePaymentGateway(long id);
        bool DeleteAllPaymentGateway();

        #endregion


        #region PaymentGatewayBTB
        PaymentGatewayBTB GetPaymentGatewayBTBById(long id);
        IEnumerable<PaymentGatewayBTB> GetAllPaymentGatewayBTB();
        long CreatePaymentGatewayBTB(PaymentGatewayBTB obj);
        bool UpdatePaymentGatewayBTB(PaymentGatewayBTB changed);
        bool DeletePaymentGatewayBTB(long id);
        bool DeleteAllPaymentGatewayBTB();

        #endregion


        #region PaymentSnapshot
        PaymentSnapshot GetPaymentSnapshotById(long id);

        PaymentSnapshot GetPaymentSnapshotBySnapshotId(string id);
        PaymentSnapshot GetPaymentSnapshotByTransactionId(string transId);
        PaymentSnapshot GetPaymentSnapshotByAdmissionId(long reltid);

        IEnumerable<PaymentSnapshot> GetAllPaymentSnapshot();
        long CreatePaymentSnapshot(PaymentSnapshot obj);
        bool UpdatePaymentSnapshot(PaymentSnapshot changed);
        bool DeletePaymentSnapshot(long id);
        bool DeleteAllPaymentSnapshot();

        #endregion
    }
}
