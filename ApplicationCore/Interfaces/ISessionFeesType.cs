﻿using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities.SessionFeesType;
using OdmErp.ApplicationCore.Entities.Pagination;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface ISessionFeesType
    {
        long CreateSessionFeesType(SessionFeesType newSessionFeesType);
        IEnumerable<SessionFeesType> GetAllSessionFeesType(); 
        //  SesseionFeesType GetSessionFeesTypePriceByName(string name);
        SessionFeesType GetSessionFeesTypeById(long id);
        bool UpdateSessionFeesType(SessionFeesType changedSesseionFeesType);
        bool DeleteSessionFeesType(long id);
        bool DeleteAllSessionFeesType();
        IEnumerable<SessionFeesType> CheckSessionFeesType();
        //PaginationSessionFesstype GetPaginationAllSessionFeesType(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue);
    }
}
