﻿using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Entities.TodoAggregate;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IToDoRepository
    {
        #region Todo
        Todo GetTodoById(long id);
        IEnumerable<Todo> GetAllTodo();
        long CreateTodo(Todo newTodo);
        bool UpdateTodo(Todo changedTodo);
        bool DeleteTodo(long id);
        bool DeleteAllTodo();
        #endregion

        #region TodoAttachment
        TodoAttachment GetTodoAttachmentById(long id);
        IEnumerable<TodoAttachment> GetAllTodoAttachment();
        long CreateTodoAttachment(TodoAttachment newTodoAttachment);
        bool UpdateTodoAttachment(TodoAttachment changedTodoAttachment);
        bool DeleteTodoAttachment(long id);
        bool DeleteAllTodoAttachment();
        #endregion

        #region TodoComment
        TodoComment GetTodoCommentById(long id);
        IEnumerable<TodoComment> GetAllTodoComment();
        long CreateTodoComment(TodoComment newTodoComment);
        bool UpdateTodoComment(TodoComment changedTodoComment);
        bool DeleteTodoComment(long id);
        bool DeleteAllTodoComment();
        #endregion

        #region TodoCommentAttachment
        TodoCommentAttachment GetTodoCommentAttachmentById(long id);
        IEnumerable<TodoCommentAttachment> GetAllTodoCommentAttachment();
        long CreateTodoCommentAttachment(TodoCommentAttachment newTodoCommentAttachment);
        bool UpdateTodoCommentAttachment(TodoCommentAttachment changedTodoCommentAttachment);
        bool DeleteTodoCommentAttachment(long id);
        bool DeleteAllTodoCommentAttachment();
        #endregion

        #region TodoPriority
        TodoPriority GetTodoPriorityById(long id);
        IEnumerable<TodoPriority> GetAllTodoPriority();
        long CreateTodoPriority(TodoPriority newTodoPriority);
        bool UpdateTodoPriority(TodoPriority changedTodoPriority);
        bool DeleteTodoPriority(long id);
        bool DeleteAllTodoPriority();
        #endregion

        #region TodoStatus
        TodoStatus GetTodoStatusById(long id);
        IEnumerable<TodoStatus> GetAllTodoStatus();
        long CreateTodoStatus(TodoStatus newTodoStatus);
        bool UpdateTodoStatus(TodoStatus changedTodoStatus);
        bool DeleteTodoStatus(long id);
        bool DeleteAllTodoStatus();
        #endregion
        #region TodoStatusDetails
        TodoStatusDetails GetTodoStatusDetailsById(long id);
        IEnumerable<TodoStatusDetails> GetAllTodoStatusDetails();
        long CreateTodoStatusDetails(TodoStatusDetails newTodoStatusDetails);
        bool UpdateTodoStatusDetails(TodoStatusDetails changedTodoStatusDetails);
        bool DeleteTodoStatusDetails(long id);
        bool DeleteAllTodoStatusDetails();
        #endregion

        #region TodoTimeline
        TodoTimeline GetTodoTimelineById(long id);
        IEnumerable<TodoTimeline> GetAllTodoTimeline();
        long CreateTodoTimeline(TodoTimeline newTodoTimeline);
        bool UpdateTodoTimeline(TodoTimeline changedTodoTimeline);
        bool DeleteTodoTimeline(long id);
        bool DeleteAllTodoTimeline();
        #endregion


    }
}
