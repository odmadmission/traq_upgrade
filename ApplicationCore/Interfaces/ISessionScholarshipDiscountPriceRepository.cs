﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface ISessionScholarshipDiscountPriceRepository
    {
        long CreateSessionScholarshipDiscountPrice(SessionScholarshipDiscountPrice newSessionScholarshipDiscountPrice);
        IEnumerable<SessionScholarshipDiscountPrice> GetAllSessionScholarshipDiscountPrice();
        SessionScholarshipDiscountPrice GetSessionScholarshipDiscountPriceByName(string name);
        PaginationSessionScholarshipDiscountPrice GetPaginationSessionScholarshipDiscountPrice(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue);
        SessionScholarshipDiscountPrice GetSessionScholarshipDiscountPriceById(long id);
        bool UpdateSessionScholarshipDiscountPrice(SessionScholarshipDiscountPrice changedSessionScholarshipDiscountPrice);
        bool DeleteSessionScholarshipDiscountPrice(long id);
        bool DeleteAllSessionScholarshipDiscountPrice();
    }
}
