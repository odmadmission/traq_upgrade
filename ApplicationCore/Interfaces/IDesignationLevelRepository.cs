﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IDesignationLevelRepository
    {
        DesignationLevel GetDesignationLevelByName(string name);
        DesignationLevel GetDesignationLevelById(long id);
        IEnumerable<DesignationLevel> GetAllDesignationLevel();
        long CreateDesignationLevel(DesignationLevel newDesignationLevel);
        bool UpdateDesignationLevel(DesignationLevel changedDesignationLevel);
        bool DeleteDesignationLevel(long id);
        bool DeleteAllDesignationLevel();

    }
}
