﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IEducationQualificationRepository
    {
        EducationQualification GetEducationQualificationByName(string name);
        EducationQualification GetEducationQualificationById(long id);
        IEnumerable<EducationQualification> GetAllEducationQualification();
        IEnumerable<EducationQualification> GetAllEducationQualificationByTypeId(long qualificationId);
        long CreateEducationQualification(EducationQualification newEducationQualification);
        bool UpdateEducationQualification(EducationQualification changedEducationQualification);
        bool DeleteEducationQualification(long id);
        bool DeleteAllEducationQualification();

    }
}
