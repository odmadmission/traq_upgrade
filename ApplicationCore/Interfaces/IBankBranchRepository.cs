﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IBankBranchRepository
    {
        BankBranch GetBankBranchByName(string name);
        BankBranch GetBankBranchById(long id);
        IEnumerable<BankBranch> GetAllBankBranch();
        IEnumerable<BankBranch> GetAllBankBranchByBankId(long bankId);
        long CreateBankBranch(BankBranch newBankBranch);
        bool UpdateBankBranch(BankBranch changedBankBranch);
        bool DeleteBankBranch(long id);
        bool DeleteAllBankBranch();
    }
}
