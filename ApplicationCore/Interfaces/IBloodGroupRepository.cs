﻿using OdmErp.ApplicationCore.Entities;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IBloodGroupRepository
    {
        BloodGroup GetBloodGroupByName(string name);
        BloodGroup GetBloodGroupById(long id);
        IEnumerable<BloodGroup> GetAllBloodGroup();
        long CreateBloodGroup(BloodGroup newBloodGroup);
        bool UpdateBloodGroup(BloodGroup changedBloodGroup);
        bool DeleteBloodGroup(long id);
        bool DeleteAllBloodGroup();

    }
}
