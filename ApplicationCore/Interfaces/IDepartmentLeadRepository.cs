﻿using OdmErp.ApplicationCore.Entities.MasterAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
  public interface IDepartmentLeadRepository
    {

        #region DepartmentLead
        DepartmentLead GetByDepartmentLeadId(long id);
        IEnumerable<DepartmentLead> GetAllDepartmentLead();
        long CreateDepartmentLead(DepartmentLead obj);
        bool UpdateDepartmentLead(DepartmentLead changed);
        bool DeleteDepartmentLead(long id);
        bool DeleteAllDepartmentLead();

        IEnumerable<DepartmentLead> GetAllByEmployeeId(long employeeId);
        IEnumerable<DepartmentLead> GetAllEmployeeIdByDepartment(long deptId);

        #endregion

    }
}
