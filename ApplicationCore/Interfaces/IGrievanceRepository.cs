﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Entities.GrievanceAggregate;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IGrievanceRepository
    {
        #region Grievance Access

        IEnumerable<GrievanceAccess> GrievanceAccess();
        long CreateGrievanceAccess(GrievanceAccess access);
        bool UpdateGrivanceAccess(GrievanceAccess access);
        bool DeleteGrievanceAccess(long id);
        GrievanceAccess GetGrievanceAccessById(long id);

        #endregion

        #region GrievanceType
        GrievanceType GetGrievanceTypeById(long id);
        IEnumerable<GrievanceType> GetAllGrievanceType();
        long CreateGrievanceType(GrievanceType newGrievanceType);
        bool UpdateGrievanceType(GrievanceType changedGrievanceType);
        bool DeleteGrievanceType(long id);
        bool DeleteAllGrievanceType();
        #endregion

        #region GrievanceStatus
        GrievanceStatus GetGrievanceStatusById(long id);
        IEnumerable<GrievanceStatus> GetAllGrievanceStatus();
        long CreateGrievanceStatus(GrievanceStatus newGrievanceStatus);
        bool UpdateGrievanceStatus(GrievanceStatus changedGrievanceStatus);
        bool DeleteGrievanceStatus(long id);
        bool DeleteAllGrievanceStatus();
        #endregion


        #region Grievance
        Grievance GetGrievanceById(long id);
        IEnumerable<Grievance> GetAllGrievance();
        IEnumerable<Grievance> GetactiveinactiveGrievances();
        long CreateGrievance(Grievance newGrievance);
        bool UpdateGrievance(Grievance changedGrievance);
        bool DeleteGrievance(long id);
        bool DeleteAllGrievance();
        #endregion

        #region EmployeeGrievance
        EmployeeGrievance GetEmployeeGrievanceById(long id);
        IEnumerable<EmployeeGrievance> GetAllEmployeeGrievance();
        long CreateEmployeeGrievance(EmployeeGrievance newEmployeeGrievance);
        bool UpdateEmployeeGrievance(EmployeeGrievance changedEmployeeGrievance);
        bool DeleteEmployeeGrievance(long id);
        bool DeleteAllEmployeeGrievance();
        #endregion



        #region GrievanceAttachment
        GrievanceAttachment GetGrievanceAttachmentById(long id);
        IEnumerable<GrievanceAttachment> GetAllGrievanceAttachment();
        long CreateGrievanceAttachment(GrievanceAttachment newGrievanceAttachment);
        bool UpdateGrievanceAttachment(GrievanceAttachment changedGrievanceAttachment);
        bool DeleteGrievanceAttachment(long id);
        bool DeleteAllGrievanceAttachment();
        #endregion

        #region GrievanceInhouse Attachments

        GrievanceInhouseAttachment GetGrievanceInhouseAttachmentById(long id);
        IEnumerable<GrievanceInhouseAttachment> GrievanceInhouseAttachment(long grivid);
        long CreateGrievanceInhouseAttachment(GrievanceInhouseAttachment newGrievanceAttachment);
        //bool UpdateGrievanceInhouseAttachment(GrievanceInhouseAttachment changedGrievanceAttachment);
        bool DeleteGrievanceInhouseAttachment(long id,long userid); 

        #endregion

        #region GrievanceCategory
        GrievanceCategory GetGrievanceCategoryById(long id);
        IEnumerable<GrievanceCategory> GetAllGrievanceCategory();
        long CreateGrievanceCategory(GrievanceCategory newGrievanceCategory);
        bool UpdateGrievanceCategory(GrievanceCategory changedGrievanceCategory);
        bool DeleteGrievanceCategory(long id);
        bool DeleteAllGrievanceCategory();
        #endregion


        #region GrievancePriority
        GrievancePriority GetGrievancePriorityById(long id);
        IEnumerable<GrievancePriority> GetAllGrievancePriority();
        long CreateGrievancePriority(GrievancePriority newGrievancePriority);
        bool UpdateGrievancePriority(GrievancePriority changedGrievancePriority);
        bool DeleteGrievancePriority(long id);
        bool DeleteAllGrievancePriority();
        #endregion


        #region GrievanceTimeline
        GrievanceTimeline GetGrievanceTimelineById(long id);
        IEnumerable<GrievanceTimeline> GetAllGrievanceTimeline();
        IEnumerable<GrievanceTimeline> GetAllGrievanceTimelineByGrivId(long id);
        long CreateGrievanceTimeline(GrievanceTimeline newGrievanceTimeline);
        bool UpdateGrievanceTimeline(GrievanceTimeline changedGrievanceTimeline);
        bool DeleteGrievanceTimeline(long id);
        bool DeleteAllGrievanceTimeline();
        #endregion

        #region GrievanceInHouse Comments

        IEnumerable<GrievanceInhouseComment> GrievanceInhouseComments(long id);
        GrievanceInhouseComment GetGrievanceInhouseCommentById(long id);
        long CreateGrievanceInHouseComment(GrievanceInhouseComment inhouseComment);
        bool updaeGrievanceInHouseComment(GrievanceInhouseComment inhouseComment);
        bool DeleteGrievanceInHouseComment(long id,long userid);

        #endregion

        #region Grievance Comments
        GrievanceComment GetGrievanceCommentById(long id);
        IEnumerable<GrievanceComment> GetAllGrievanceComment();
        long CreateGrievanceComment(GrievanceComment newGrievanceComment);
        bool UpdateGrievanceComment(GrievanceComment changedGrievanceComment);
        bool DeleteGrievanceComment(long id);
        bool DeleteAllGrievanceComment();
        #endregion

        #region Grievance Comments Attachment
        GrievanceCommentAttachment GetGrievanceCommentAttachmentById(long id);
        IEnumerable<GrievanceCommentAttachment> GetAllGrievanceCommentAttachment();
        long CreateGrievanceCommentAttachment(GrievanceCommentAttachment newGrievanceCommentAttachment);
        bool UpdateGrievanceCommentAttachment(GrievanceCommentAttachment changedGrievanceCommentAttachment);
        bool DeleteGrievanceCommentAttachment(long id);
        bool DeleteAllGrievanceCommentAttachment();
        #endregion

       
    }
}
