﻿using OdmErp.ApplicationCore.Entities;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IAddressTypeRepository
    {
        AddressType GetAddressTypeByName(string name);
        AddressType GetAddressTypeById(long id);
        IEnumerable<AddressType> GetAllAddressType();
        long CreateAddressType(AddressType newAddressType);
        bool UpdateAddressType(AddressType changedAddressType);
        bool DeleteAddressType(long id);
        bool DeleteAllAddressType();

    }
}
