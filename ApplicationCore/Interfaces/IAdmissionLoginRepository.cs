﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
  public  interface IAdmissionLoginRepository
    {
        AdmissionLogin GetAdmissionLoginById(long id);
        IEnumerable<AdmissionLogin> GetAllAdmissionLogin();
        long CreateAdmissionLogin(AdmissionLogin newAdmissionLogin);
        bool UpdateAdmissionLogin(AdmissionLogin changedAdmissionLogin);
        bool DeleteAdmissionLogin(long id);
        bool DeleteAllAdmissionLogin();
    }
}
