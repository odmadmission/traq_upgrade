﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IModuleRepository
    {
        Module GetModuleByName(string name);
        Module GetModuleById(long id);
        IEnumerable<Module> GetAllModule();
        long CreateModule(Module newModule);
        bool UpdateModule(Module changedModule);
        bool DeleteModule(long id);
        bool DeleteAllModule();

    }
}
