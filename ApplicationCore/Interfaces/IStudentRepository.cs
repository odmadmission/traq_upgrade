﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IStudentRepository
    {
        Student GetStudentByStudentCode(string studentcode);
        Student GetStudentById(long id);
        IEnumerable<Student> GetAllStudent();
        long CreateStudent(Student newStudent);
        bool UpdateStudent(Student changedStudent);
        bool DeleteStudent(long id);
        bool DeleteAllStudent();

    }
    public interface IStudentLanguagesKnownRepository : IAsyncRepository<StudentLanguagesKnown>, IEntityCommonMethod<StudentLanguagesKnown>
    {
    }
}
