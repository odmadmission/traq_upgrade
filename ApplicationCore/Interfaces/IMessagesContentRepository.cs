﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;


namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IMessagesContentRepository
    {
        long CreateMessagesContent(MessagesContent newMessagesContent);

        MessagesContent GetMessagesContentByName(string name);
        MessagesContent GetMessagesContentById(long id);
        bool UpdateMessagesContent(MessagesContent changedBankType);
        bool DeleteMessagesContent(long id);
        bool DeleteAllMessagesContent();
        IEnumerable<MessagesContent> GetAllMessagesContent();
    }
}
