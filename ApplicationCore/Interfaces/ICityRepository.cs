﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface ICityRepository
    {
        City GetCityByName(string name);
        City GetCityById(long id);
        IEnumerable<City> GetAllCity();
        IEnumerable<City> GetAllCityByStateId(long stateId);
        long CreateCity(City newCity);
        bool UpdateCity(City changedCity);
        bool DeleteCity(long id);
        bool DeleteAllCity();

    }
}
