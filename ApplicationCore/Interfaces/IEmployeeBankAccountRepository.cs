﻿using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IEmployeeBankAccountRepository
    {
        IEnumerable<EmployeeBankAccount> GetEmployeeBankAccountByEmployeeID(long empid);
        EmployeeBankAccount GetEmployeeBankAccountById(long id);
        IEnumerable<EmployeeBankAccount> GetAllEmployeeBankAccountes();
        long CreateEmployeeBankAccount(EmployeeBankAccount newEmployeeBankAccount);
        bool UpdateEmployeeBankAccount(EmployeeBankAccount changedEmployeeBankAccount);
        bool DeleteEmployeeBankAccount(long id);
        bool DeleteAllEmployeeBankAccountes();
    }
}
