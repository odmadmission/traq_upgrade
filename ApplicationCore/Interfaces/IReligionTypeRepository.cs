﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IReligionTypeRepository
    {

        ReligionType GetReligionTypeByName(string name);
        ReligionType GetReligionTypeById(long id);
        IEnumerable<ReligionType> GetAllReligionTypes();
        long CreateReligionType(ReligionType newReligionType);
        bool UpdateReligionType(ReligionType changedReligionType);
        bool DeleteReligionType(long id);
        bool DeleteAllReligionTypes();
    }
}
