﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IBoardRepository
    {
        Board GetBoardByName(string name);
        Board GetBoardById(long id);
        IEnumerable<Board> GetAllBoard();
        long CreateBoard(Board newBoard);
        bool UpdateBoard(Board changedBoard);
        bool DeleteBoard(long id);
        bool DeleteAllBoard();

       

    }
}
