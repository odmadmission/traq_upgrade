﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.GrievanceAggregate;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface ILanguageRepository : IAsyncRepository<Language>, IEntityCommonMethod<Language>
    {
    }
    public interface IGrievanceForwardRepository : IAsyncRepository<GrievanceForward>, IEntityCommonMethod<GrievanceForward>
    {
    }
    public interface IAcademicStandardSettingsRepository : IAsyncRepository<AcademicStandardSettings>, IEntityCommonMethod<AcademicStandardSettings>
    {
    }
    public interface IGrievanceExtendedTimelinesRepository : IAsyncRepository<GrievanceExtendedTimeline>, IEntityCommonMethod<GrievanceExtendedTimeline>
    {
    } 
    public interface IGrievanceExtendedTimelineAttachmentRepository : IAsyncRepository<GrievanceExtendedTimelineAttachment>, IEntityCommonMethod<GrievanceExtendedTimelineAttachment>
    {
    } 
    public interface IGrievanceAccessTimelineRepository : IAsyncRepository<GrievanceAccessTimeline>, IEntityCommonMethod<GrievanceAccessTimeline>
    {
    }
    public interface IWingRepository : IAsyncRepository<Wing>, IEntityCommonMethod<Wing>
    {
    }
    public interface IStudentTransportationRepository : IAsyncRepository<StudentTransportation>, IEntityCommonMethod<StudentTransportation>
    {
    }
    public interface IBoardStandardWingRepository : IAsyncRepository<BoardStandardWing>, IEntityCommonMethod<BoardStandardWing>
    {
    }
    public interface ISubjectGroupRepository : IAsyncRepository<SubjectGroup>, IEntityCommonMethod<SubjectGroup>
    {
    }
    public interface IAcademicSubjectGroupRepository : IAsyncRepository<AcademicSubjectGroup>, IEntityCommonMethod<AcademicSubjectGroup>
    {
    }
    public interface IAcademicSubjectOptionalDetailsRepository : IAsyncRepository<AcademicSubjectOptionalDetails>, IEntityCommonMethod<AcademicSubjectOptionalDetails>
    {
    }
    public interface IStudentEmergencyContactRepository : IAsyncRepository<StudentEmergencyContact>, IEntityCommonMethod<StudentEmergencyContact>
    {
    }
    public interface ISubjectGroupDetailsRepository : IAsyncRepository<SubjectGroupDetails>, IEntityCommonMethod<SubjectGroupDetails>
    {
    }
    public interface IAcademicStudentSubjectOptionalRepository : IAsyncRepository<AcademicStudentSubjectOptional>, IEntityCommonMethod<AcademicStudentSubjectOptional>
    {
    }
    public interface ISubjectOptionalTypeRepository : IAsyncRepository<SubjectOptionalType>, IEntityCommonMethod<SubjectOptionalType>
    {
    }
    public interface ISubjectOptionalCategoryRepository : IAsyncRepository<SubjectOptionalCategory>, IEntityCommonMethod<SubjectOptionalCategory>
    {
    }
    public interface ISubjectOptionalRepository : IAsyncRepository<SubjectOptional>, IEntityCommonMethod<SubjectOptional>
    {
    }
    public interface IAcademicSubjectOptionalRepository : IAsyncRepository<AcademicSubjectOptional>, IEntityCommonMethod<AcademicSubjectOptional>
    {
    }
    public interface IVehicleTypeRepository : IAsyncRepository<VehicleType>, IEntityCommonMethod<VehicleType>
    {
    }
    public interface IVehicleRepository : IAsyncRepository<Vehicle>, IEntityCommonMethod<Vehicle>
    {
    }
    public interface IVehicleLocationRepository : IAsyncRepository<VehicleLocation>, IEntityCommonMethod<VehicleLocation>
    {
    }
    public interface IDepartureTimeRepository : IAsyncRepository<DepartureTime>, IEntityCommonMethod<DepartureTime>
    {
    }
    public interface IContactTypeRepository : IAsyncRepository<ContactType>, IEntityCommonMethod<ContactType>
    {
    }
}
