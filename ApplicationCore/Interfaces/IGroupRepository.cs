﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IGroupRepository
    {
        Group GetGroupByName(string name);
        Group GetGroupById(long id);
        IEnumerable<Group> GetAllGroup();
        long CreateGroup(Group newGroup);
        bool UpdateGroup(Group changedGroup);
        bool DeleteGroup(long id);
        bool DeleteAllGroup();

    }
}
