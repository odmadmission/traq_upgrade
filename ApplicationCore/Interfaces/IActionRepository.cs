﻿using OdmErp.ApplicationCore.Entities;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IActionRepository
    {
        Action GetActionByName(string name);
        Action GetActionById(long id);
        IEnumerable<Action> GetAllAction();
        long CreateAction(Action newAction);
        bool UpdateAction(Action changedAction);
        bool DeleteAction(long id);
        bool DeleteAllAction();

    }
}
