﻿using OdmErp.ApplicationCore.Entities.CompanyAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface ICompanyRepository
    {
        Company GetCompanyByName(string name);
        Company GetCompanyById(long id);
        IEnumerable<Company> GetAllCompany();
        long CreateCompany(Company newCompany);
        bool UpdateCompany(Company changedCompany);
        bool DeleteCompany(long id);
        bool DeleteAllCompany();
    }
}
