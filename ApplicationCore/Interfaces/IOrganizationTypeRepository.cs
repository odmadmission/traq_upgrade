﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IOrganizationTypeRepository
    {
        OrganizationType GetOrganizationTypeByName(string name);
        OrganizationType GetOrganizationTypeById(long id);
        IEnumerable<OrganizationType> GetAllOrganizationType();
        long CreateOrganizationType(OrganizationType newOrganizationType);
        bool UpdateOrganizationType(OrganizationType changedOrganizationType);
        bool DeleteOrganizationType(long id);
        bool DeleteAllOrganizationType();

    }
}
