﻿using OdmErp.ApplicationCore.Entities.GrievanceAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IDataTimelineRepository
    {
        long CreateDataTimeline(DataTimeline timeline);
        bool UpdateDataTimeline(DataTimeline timeline);
        bool DeleteDataTimeline(long id);
    }
}
