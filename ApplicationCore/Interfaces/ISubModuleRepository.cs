﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface ISubModuleRepository
    {
        SubModule GetSubModuleByName(string name);
        SubModule GetSubModuleById(long id);
        IEnumerable<SubModule> GetAllSubModule();
        long CreateSubModule(SubModule newSubModule);
        bool UpdateSubModule(SubModule changedSubModule);
        bool DeleteSubModule(long id);
        bool DeleteAllSubModule();

    }
}
