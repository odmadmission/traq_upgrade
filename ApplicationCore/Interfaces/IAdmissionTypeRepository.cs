﻿using OdmErp.ApplicationCore.Entities;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IAdmissionTypeRepository
    {
        AdmissionType GetAdmissionTypeByName(string name);
        AdmissionType GetAdmissionTypeById(long id);
        IEnumerable<AdmissionType> GetAllAdmissionType();
        long CreateAdmissionType(AdmissionType newAdmissionType);
        bool UpdateAdmissionType(AdmissionType changedAdmissionType);
        bool DeleteAdmissionType(long id);
        bool DeleteAllAdmissionType();

    }
}
