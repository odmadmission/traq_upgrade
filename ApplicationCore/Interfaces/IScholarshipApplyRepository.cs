﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface IScholarshipApplyRepository
    {
        long CreateScholarshipApply(ScholarshipApply newScholarshipApply);
        IEnumerable<ScholarshipApply> GetAllScholarshipApply();
        ScholarshipApply GetScholarshipApplyByName(string name);
        ScholarshipApply GetScholarshipApplyById(long id);
        bool UpdateScholarshipApply(ScholarshipApply changedScholarshipApply);
        bool DeleteScholarshipApply(long id);
        bool DeleteAllScholarshipApply();
    }
}
