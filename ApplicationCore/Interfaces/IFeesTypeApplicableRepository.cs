﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.Pagination;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface  IFeesTypeApplicableRepository
    {
        long CreateFeesTypeApplicable(FeesTypeApplicable newFeesTypeApplicable);
        IEnumerable<FeesTypeApplicable> GetAllFeesTypeApplicable(int? page, int? noofData);
        FeesTypeApplicable GetFeesTypeApplicableByName(string name);
        FeesTypeApplicable GetFeesTypeApplicableById(long id);
        string GetFeesTypeApplicableNameById(long id);
        bool UpdateFeesTypeApplicable(FeesTypeApplicable changedBankType);
        bool DeleteFeesTypeApplicable(long id);
        bool DeleteAllFeesTypeApplicable();
        PaginationFeestypeApplicable GetPaginationAllFeesTypeApplicable(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue);
    }
}
