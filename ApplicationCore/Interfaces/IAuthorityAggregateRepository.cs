﻿using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.MasterAggregate;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IAuthorityAggregateRepository
    {

        Authority GetAuthorityById(long id);
        IEnumerable<Authority> GetAllAuthority();
        long CreateAuthority(Authority newAuthority);
        bool UpdateAuthority(Authority changedAuthority);
        bool DeleteAuthority(long id);
        bool DeleteAllAuthority();


        AuthorityType GetAuthorityTypeById(long id);
        IEnumerable<AuthorityType> GetAllAuthorityType();
        long CreateAuthorityType(AuthorityType newAuthorityType);
        bool UpdateAuthorityType(AuthorityType changedAuthorityType);
        bool DeleteAuthorityType(long id);
        bool DeleteAllAuthorityType();



        DepartmentAuthority GetDepartmentAuthorityById(long id);
        IEnumerable<DepartmentAuthority> GetAllDepartmentAuthority();
        long CreateDepartmentAuthority(DepartmentAuthority newDepartmentAuthority);
        bool UpdateDepartmentAuthority(DepartmentAuthority changedDepartmentAuthority);
        bool DeleteDepartmentAuthority(long id);
        bool DeleteAllDepartmentAuthority();



    }
}
