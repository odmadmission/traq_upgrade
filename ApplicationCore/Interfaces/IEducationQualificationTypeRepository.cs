﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IEducationQualificationTypeRepository
    {
        EducationQualificationType GetEducationQualificationTypeByName(string name);
        EducationQualificationType GetEducationQualificationTypeById(long id);
        IEnumerable<EducationQualificationType> GetAllEducationQualificationType();
        long CreateEducationQualificationType(EducationQualificationType newEducationQualificationType);
        bool UpdateEducationQualificationType(EducationQualificationType changedEducationQualificationType);
        bool DeleteEducationQualificationType(long id);
        bool DeleteAllEducationQualificationType();

    }
}
