﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface ISectionRepository
    {
        Section GetSectionByName(string name);
        Section GetSectionById(long id);
        IEnumerable<Section> GetAllSection();
        long CreateSection(Section newSection);
        bool UpdateSection(Section changedSection);
        bool DeleteSection(long id);
        bool DeleteAllSection();

    }
}
