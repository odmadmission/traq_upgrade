﻿using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IEmployeeEducationRepository
    {

        EmployeeEducation GetEmployeeEducationById(long id);
        IEnumerable<EmployeeEducation> GetAllEmployeeEducations();
        IEnumerable<EmployeeEducation> GetAllEmployeeEducationsByEmployeeId(long empId);
        long CreateEmployeeEducation(EmployeeEducation newEmployeeEducation);
        bool UpdateEmployeeEducation(EmployeeEducation changedEmployeeEducation);
        bool DeleteEmployeeEducation(long id);
        bool DeleteAllEmployeeEducations();
    }
}
