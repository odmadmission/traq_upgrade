﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IEmployeeExperienceRepository
    {
        IEnumerable<EmployeeExperience> GetExperienceByEmployeeId(long employeeid);
        EmployeeExperience GetExperienceById(long id);
        IEnumerable<EmployeeExperience> GetAllExperience();
        long CreateExperience(EmployeeExperience newExperience);
        bool UpdateExperience(EmployeeExperience changedExperience);
        bool DeleteExperience(long id);
        bool DeleteAllExperience();

    }
}
