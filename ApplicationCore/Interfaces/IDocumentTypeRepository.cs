﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.DocumentAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IDocumentTypeRepository
    {
        DocumentType GetDocumentTypeByName(string name);
        DocumentType GetDocumentTypeById(long id);
        IEnumerable<DocumentType> GetAllDocumentTypes();
        long CreateDocumentType(DocumentType newDocumentType);
        bool UpdateDocumentType(DocumentType changedDocumentType);
        bool DeleteDocumentType(long id);
        bool DeleteAllDocumentTypes();

    }
}
