﻿
using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface IScholarshipApplicableRepository
    {
        long CreateScholarshipApplicable(ScholarshipApplicable newScholarshipApplicable);
        IEnumerable<ScholarshipApplicable> GetAllScholarshipApplicable();
        ScholarshipApplicable GetScholarshipApplicableByName(string name);
        ScholarshipApplicable GetScholarshipApplicableById(long id);
        bool UpdateScholarshipApplicable(ScholarshipApplicable changedBankType);
        bool DeleteScholarshipApplicable(long id);
        bool DeleteAllScholarshipApplicable();
    }
}
