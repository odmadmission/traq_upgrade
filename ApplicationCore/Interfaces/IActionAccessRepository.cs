﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.ModuleAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IActionAccessRepository
    {
        #region ActionRole
        ActionRole GetActionRoleById(long id);
        IEnumerable<ActionRole> GetAllActionRole();
        long CreateActionRole(ActionRole newActionRole);
        bool UpdateActionRole(ActionRole changedActionRole);
        bool DeleteActionRole(long id);
        bool DeleteAllActionRole();
        #endregion
        #region ActionStudent
        ActionStudent GetActionStudentById(long id);
        IEnumerable<ActionStudent> GetAllActionStudent();
        long CreateActionStudent(ActionStudent newActionStudent);
        bool UpdateActionStudent(ActionStudent changedActionStudent);
        bool DeleteActionStudent(long id);
        bool DeleteAllActionStudent();
        #endregion
        #region ActionAccess

        ActionAccess GetActionAccessByName(string name);
        ActionAccess GetActionAccessById(long id);
        IEnumerable<ActionAccess> GetAllActionAccess();
        long CreateActionAccess(ActionAccess newActionAccess);
        bool UpdateActionAccess(ActionAccess changedActionAccess);
        bool DeleteActionAccess(long id);
        bool DeleteAllActionAccess();

        #endregion


        #region ActionDepartment

        ActionDepartment GetActionDepartmentById(long id);
        IEnumerable<ActionDepartment> GetAllActionDepartment();
        long CreateActionDepartment(ActionDepartment newActionDepartment);
        bool UpdateActionDepartment(ActionDepartment changedActionDepartment);
        bool DeleteActionDepartment(long id);
        bool DeleteAllActionDepartment();

        IEnumerable<long> GetAllDepartmentIdBySubmoduleId(long submoduleId);

        #endregion


        #region EmployeeActionDepartment

        EmployeeActionDepartment GetEmployeeActionDepartmentById(long id);
        IEnumerable<EmployeeActionDepartment> GetAllEmployeeActionDepartment();
        long CreateEmployeeActionDepartment(EmployeeActionDepartment newEmployeeActionDepartment);
        bool UpdateEmployeeActionDepartment(EmployeeActionDepartment changedEmployeeActionDepartment);
        bool DeleteEmployeeActionDepartment(long id);
        bool DeleteAllEmployeeActionDepartment();

        #endregion
        #region ActionAcademic

        ActionAcademic GetActionAcademicById(long id);
        IEnumerable<ActionAcademic> GetAllActionAcademic();
        long CreateActionAcademic(ActionAcademic newActionDepartment);
        bool UpdateActionAcademic(ActionAcademic changedActionDepartment);
        bool DeleteActionAcademic(long id);
        bool DeleteAllActionAcademic();

        
        #endregion
    }
}
