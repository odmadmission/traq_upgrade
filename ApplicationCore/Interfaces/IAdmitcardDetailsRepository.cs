﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
   public interface IAdmitcardDetailsRepository
    {
        AdmitcardDetails GetAdmitcardDetailsByName(string name);
        AdmitcardDetails GetAdmitcardDetailsById(long id);
        IEnumerable<AdmitcardDetails> GetAllAdmitcardDetails();
        long CreateAdmitcardDetails(AdmitcardDetails newAdmitcardDetails);
        bool UpdateAdmitcardDetails(AdmitcardDetails changedAdmitcardDetails);
        bool DeleteAdmitcardDetails(long id);
        bool DeleteAllAdmitcardDetails();
    }
}
