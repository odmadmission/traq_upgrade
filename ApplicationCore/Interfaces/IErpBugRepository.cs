﻿using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IErpBugRepository : IAsyncRepository<ErpBug>, IEntityCommonMethod<ErpBug>
    {
        
    }
    public interface IErpBugAttachmentRepository : IAsyncRepository<ErpBugAttachment>, IEntityCommonMethod<ErpBugAttachment>
    {

    }
}
