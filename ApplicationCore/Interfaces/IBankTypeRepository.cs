﻿
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdmErp.ApplicationCore.Interfaces
{
    public interface IBankTypeRepository
    {
        BankType GetBankTypeByName(string name);
        BankType GetBankTypeById(long id);
        IEnumerable<BankType> GetAllBankType();
        long CreateBankType(BankType newBankType);
        bool UpdateBankType(BankType changedBankType);
        bool DeleteBankType(long id);
        bool DeleteAllBankType();

    }  
}
