﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.ChartModels
{
    public class ApexChartOptions
    {
        public Int64[] series { get; set; }
        public string[] labels { get; set; }

        public ApexChartSeriesChart chart { get; set; }

        public ApexChartLegend legend { get; set; }
        public ApexChartResponsive responsive { get; set; }

        public long max { get; set; }


        public string[] MonthNamesAsCategories { get; set; }

        public ApexChartMonthWiseSeries[] MonthSeries { get; set; }

        public ApexChartTimeLine[] timeline { get; set; }

    }
    public class ApexChartMonthWiseSeries
    {

        public int name { get; set; }
        public Int64[] data { get; set; }
    }

    public class ApexChartTimeLine
    {

        public string x { get; set; }
        public Int64[] y { get; set; }
        public string fillColor  { get; set; }
        
    }


    public class ApexChartSeriesChart
    {

        public int width { get; set; }
        public string type { get; set; }
    }
    public class ApexChartResponsive
    {

        public int breakpoints { get; set; }
        public ApexChartOptions options { get; set; }
    }
    public class ApexChartLegend
    {
        public string position { get; set; }
    }
}
