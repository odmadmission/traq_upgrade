﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.ModuleAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ActionAccessRepository : IActionAccessRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public ActionAccessRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        #region Action Role


        public long CreateActionRole(ActionRole newActionRole)
        {
            try
            {
                newActionRole.InsertedDate = DateTime.Now;
                newActionRole.ModifiedDate = DateTime.Now;
                _DbContext.Set<ActionRole>().Add(newActionRole);
                _DbContext.SaveChanges();
                return newActionRole.ID;
            }
            catch(Exception e)
            {
                return 0;
            }
        }

        public bool DeleteAllActionRole()
        {
            try
            {
                List<ActionRole> res = _DbContext.Set<ActionRole>().ToListAsync().Result;
                foreach (ActionRole ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.ActionRoles.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteActionRole(long id)
        {
            try
            {
                ActionRole ob = _DbContext.Set<ActionRole>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.ActionRoles.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<ActionRole> GetAllActionRole()
        {
            try
            {
                IEnumerable<ActionRole> res = _DbContext.Set<ActionRole>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public ActionRole GetActionRoleById(long id)
        {
            try
            {
                ActionRole ob = _DbContext.Set<ActionRole>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public ActionRole GetActionRoleByName(string name)
        {
            try
            {
                ActionRole ob = _DbContext.Set<ActionRole>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateActionRole(ActionRole changedActionRole)
        {
            try
            {
                changedActionRole.ModifiedDate = DateTime.Now;
                changedActionRole.Status = "UPDATED";
                _DbContext.ActionRoles.Attach(changedActionRole);
                var entry = _DbContext.Entry(changedActionRole);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
        #region Action Student


        public long CreateActionStudent(ActionStudent newActionStudent)
        {
            try
            {
                newActionStudent.InsertedDate = DateTime.Now;
                newActionStudent.ModifiedDate = DateTime.Now;
                _DbContext.Set<ActionStudent>().Add(newActionStudent);
                _DbContext.SaveChanges();
                return newActionStudent.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllActionStudent()
        {
            try
            {
                List<ActionStudent> res = _DbContext.Set<ActionStudent>().ToListAsync().Result;
                foreach (ActionStudent ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.actionStudents.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteActionStudent(long id)
        {
            try
            {
                ActionStudent ob = _DbContext.Set<ActionStudent>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.actionStudents.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<ActionStudent> GetAllActionStudent()
        {
            try
            {
                IEnumerable<ActionStudent> res = _DbContext.Set<ActionStudent>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public ActionStudent GetActionStudentById(long id)
        {
            try
            {
                ActionStudent ob = _DbContext.Set<ActionStudent>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public ActionStudent GetActionStudentByName(string name)
        {
            try
            {
                ActionStudent ob = _DbContext.Set<ActionStudent>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateActionStudent(ActionStudent changedActionStudent)
        {
            try
            {
                changedActionStudent.ModifiedDate = DateTime.Now;
                changedActionStudent.Status = "UPDATED";
                _DbContext.actionStudents.Attach(changedActionStudent);
                var entry = _DbContext.Entry(changedActionStudent);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region ActionAccess
        public long CreateActionAccess(ActionAccess newActionAccess)
        {
            try
            {
                newActionAccess.InsertedDate = DateTime.Now;
                newActionAccess.ModifiedDate = DateTime.Now;
                _DbContext.Set<ActionAccess>().Add(newActionAccess);
                _DbContext.SaveChanges();
                return newActionAccess.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllActionAccess()
        {
            try
            {
                List<ActionAccess> res = _DbContext.Set<ActionAccess>().ToListAsync().Result;
                foreach (ActionAccess ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.ActionAccesses.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteActionAccess(long id)
        {
            try
            {
                ActionAccess ob = _DbContext.Set<ActionAccess>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.ActionAccesses.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<ActionAccess> GetAllActionAccess()
        {
            try
            {
                IEnumerable<ActionAccess> res = _DbContext.Set<ActionAccess>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public ActionAccess GetActionAccessById(long id)
        {
            try
            {
                ActionAccess ob = _DbContext.Set<ActionAccess>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public ActionAccess GetActionAccessByName(string name)
        {
            try
            {
                ActionAccess ob = _DbContext.Set<ActionAccess>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateActionAccess(ActionAccess changedActionAccess)
        {
            try
            {
                changedActionAccess.ModifiedDate = DateTime.Now;
                changedActionAccess.Status = "UPDATED";
                _DbContext.ActionAccesses.Attach(changedActionAccess);
                var entry = _DbContext.Entry(changedActionAccess);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
        #region ActionDepartment
        public ActionDepartment GetActionDepartmentById(long id)
        {
            try
            {
                ActionDepartment ob = _DbContext.Set<ActionDepartment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<ActionDepartment> GetAllActionDepartment()
        {
            try
            {
                IEnumerable<ActionDepartment> res = _DbContext.Set<ActionDepartment>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateActionDepartment(ActionDepartment newActionDepartment)
        {
            try
            {
                newActionDepartment.InsertedDate = DateTime.Now;
                newActionDepartment.ModifiedDate = DateTime.Now;
                _DbContext.Set<ActionDepartment>().Add(newActionDepartment);
                _DbContext.SaveChanges();
                return newActionDepartment.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateActionDepartment(ActionDepartment changedActionDepartment)
        {
            try
            {
                changedActionDepartment.ModifiedDate = DateTime.Now;
                changedActionDepartment.Status = "UPDATED";
                _DbContext.ActionDepartments.Attach(changedActionDepartment);
                var entry = _DbContext.Entry(changedActionDepartment);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteActionDepartment(long id)
        {

            try
            {
                ActionDepartment ob = _DbContext.Set<ActionDepartment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.ActionDepartments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllActionDepartment()
        {
            try
            {
                List<ActionDepartment> res = _DbContext.Set<ActionDepartment>().ToListAsync().Result;
                foreach (ActionDepartment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.ActionDepartments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
        #region EmployeeActionDepartment
        public EmployeeActionDepartment GetEmployeeActionDepartmentById(long id)
        {
            try
            {
                EmployeeActionDepartment ob = _DbContext.Set<EmployeeActionDepartment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<EmployeeActionDepartment> GetAllEmployeeActionDepartment()
        {
            try
            {
                IEnumerable<EmployeeActionDepartment> res = _DbContext.Set<EmployeeActionDepartment>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateEmployeeActionDepartment(EmployeeActionDepartment newEmployeeActionDepartment)
        {
            try
            {
                newEmployeeActionDepartment.InsertedDate = DateTime.Now;
                newEmployeeActionDepartment.ModifiedDate = DateTime.Now;
                _DbContext.Set<EmployeeActionDepartment>().Add(newEmployeeActionDepartment);
                _DbContext.SaveChanges();
                return newEmployeeActionDepartment.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateEmployeeActionDepartment(EmployeeActionDepartment changedEmployeeActionDepartment)
        {
            try
            {
                changedEmployeeActionDepartment.ModifiedDate = DateTime.Now;
                changedEmployeeActionDepartment.Status = "UPDATED";
                _DbContext.EmployeeActionDepartments.Attach(changedEmployeeActionDepartment);
                var entry = _DbContext.Entry(changedEmployeeActionDepartment);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployeeActionDepartment(long id)
        {
            try
            {
                EmployeeActionDepartment ob = _DbContext.Set<EmployeeActionDepartment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.EmployeeActionDepartments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllEmployeeActionDepartment()
        {
            try
            {
                List<EmployeeActionDepartment> res = _DbContext.Set<EmployeeActionDepartment>().ToListAsync().Result;
                foreach (EmployeeActionDepartment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.EmployeeActionDepartments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<long> GetAllDepartmentIdBySubmoduleId(long submoduleId)
        {
            try
            {
                IEnumerable<long> ob = _DbContext.Set<ActionDepartment>().Where(a => a.SubModuleID == submoduleId).Select(a => a.DepartmentID).ToList();
                return ob;
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region ActionAcademic
        public ActionAcademic GetActionAcademicById(long id)
        {
            try
            {
                ActionAcademic ob = _DbContext.Set<ActionAcademic>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<ActionAcademic> GetAllActionAcademic()
        {
            try
            {
                IEnumerable<ActionAcademic> res = _DbContext.Set<ActionAcademic>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public long CreateActionAcademic(ActionAcademic newActionAcademic)
        {
            try
            {
                newActionAcademic.InsertedDate = DateTime.Now;
                newActionAcademic.ModifiedDate = DateTime.Now;
                _DbContext.Set<ActionAcademic>().Add(newActionAcademic);
                _DbContext.SaveChanges();
                return newActionAcademic.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateActionAcademic(ActionAcademic changedActionAcademic)
        {
            try
            {
                changedActionAcademic.ModifiedDate = DateTime.Now;
                changedActionAcademic.Status = "UPDATED";
                _DbContext.actionAcademics.Attach(changedActionAcademic);
                var entry = _DbContext.Entry(changedActionAcademic);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteActionAcademic(long id)
        {

            try
            {
                ActionAcademic ob = _DbContext.Set<ActionAcademic>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.actionAcademics.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllActionAcademic()
        {
            try
            {
                List<ActionAcademic> res = _DbContext.Set<ActionAcademic>().ToListAsync().Result;
                foreach (ActionAcademic ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.actionAcademics.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
