﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Interfaces;
using System.Data.SqlClient;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class LoginRepository : EfRepository<Login>, ILogingRepository
    {

        public LoginRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Login> GetByIdAsyncIncludeAll(long id)
        {
            var res = _dbContext.Logins.FirstOrDefaultAsync(a => a.ID == id);
            return await res;
        }

        public async Task<IReadOnlyList<Login>> ListAllAsyncIncludeAll()
        {
            var res = await _dbContext.Logins.ToListAsync();
            return   res;
        }

        

    }
}
