﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class StandardRepository : IStandardRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public StandardRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateStandard(Standard newStandard)
        {
            try
            {
                newStandard.InsertedDate = DateTime.Now;
                newStandard.ModifiedDate = DateTime.Now;
                _DbContext.Set<Standard>().Add(newStandard);
                _DbContext.SaveChanges();
                return newStandard.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllStandard()
        {
            try
            {
                List<Standard> res = _DbContext.Set<Standard>().ToListAsync().Result;
                foreach (Standard ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Standards.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteStandard(long id)
        {
            try
            {
                Standard ob = _DbContext.Set<Standard>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Standards.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Standard> GetAllStandard()
        {
            try
            {
                IEnumerable<Standard> res = _DbContext.Set<Standard>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Standard GetStandardById(long id)
        {
            try
            {
                Standard ob = _DbContext.Set<Standard>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Standard GetStandardByName(string name)
        {
            try
            {
                Standard ob = _DbContext.Set<Standard>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateStandard(Standard changedStandard)
        {
            try
            {
                changedStandard.ModifiedDate = DateTime.Now;
                //changedStandard.Status = "UPDATED";
                _DbContext.Standards.Attach(changedStandard);
                var entry = _DbContext.Entry(changedStandard);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
       
    }
}
