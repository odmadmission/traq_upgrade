﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class OrganizationTypeRepository : IOrganizationTypeRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public OrganizationTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateOrganizationType(OrganizationType newOrganizationType)
        {
            try
            {
                newOrganizationType.InsertedDate = DateTime.Now;
                newOrganizationType.ModifiedDate = DateTime.Now;
                _DbContext.Set<OrganizationType>().Add(newOrganizationType);
                _DbContext.SaveChanges();
                return newOrganizationType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllOrganizationType()
        {
            try
            {
                List<OrganizationType> res = _DbContext.Set<OrganizationType>().ToListAsync().Result;
                foreach (OrganizationType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.OrganizationTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteOrganizationType(long id)
        {
            try
            {
                OrganizationType ob = _DbContext.Set<OrganizationType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.OrganizationTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<OrganizationType> GetAllOrganizationType()
        {
            try
            {
                IEnumerable<OrganizationType> res = _DbContext.Set<OrganizationType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public OrganizationType GetOrganizationTypeById(long id)
        {
            try
            {
                OrganizationType ob = _DbContext.Set<OrganizationType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public OrganizationType GetOrganizationTypeByName(string name)
        {
            try
            {
                OrganizationType ob = _DbContext.Set<OrganizationType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateOrganizationType(OrganizationType changedOrganizationType)
        {
            try
            {
                changedOrganizationType.ModifiedDate = DateTime.Now;
                changedOrganizationType.Status = "UPDATED";
                _DbContext.OrganizationTypes.Attach(changedOrganizationType);
                var entry = _DbContext.Entry(changedOrganizationType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
