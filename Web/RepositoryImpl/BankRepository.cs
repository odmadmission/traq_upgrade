﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class BankRepository : IBankRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public BankRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateBank(Bank newBank)
        {
            try
            {
                newBank.InsertedDate = DateTime.Now;
                newBank.ModifiedDate = DateTime.Now;
                _DbContext.Set<Bank>().Add(newBank);
                _DbContext.SaveChanges();
                return newBank.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllBank()
        {
            try
            {
                List<Bank> res = _DbContext.Set<Bank>().ToListAsync().Result;
                foreach (Bank ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Banks.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteBank(long id)
        {
            try
            {
                Bank ob = _DbContext.Set<Bank>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Banks.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Bank> GetAllBank()
        {
            try
            {
                IEnumerable<Bank> res = _DbContext.Set<Bank>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Bank GetBankById(long id)
        {
            try
            {
                Bank ob = _DbContext.Set<Bank>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Bank GetBankByName(string name)
        {
            try
            {
                Bank ob = _DbContext.Set<Bank>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateBank(Bank changedBank)
        {
            try
            {
                changedBank.ModifiedDate = DateTime.Now;
                changedBank.Status = "UPDATED";
                _DbContext.Banks.Attach(changedBank);
                var entry = _DbContext.Entry(changedBank);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    
    }
}
