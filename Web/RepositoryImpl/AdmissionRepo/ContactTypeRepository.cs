﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AddmissionRepository
{
    public class ContactTypeRepository : EfRepository<ContactType>,IContactTypeRepository
    {
        public ContactTypeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<ContactType> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.ContactTypes.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<ContactType>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.ContactTypes.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
