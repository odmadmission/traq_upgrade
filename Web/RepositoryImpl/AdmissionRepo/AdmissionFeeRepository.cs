﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
  public class AdmissionFeeRepository : EfRepository<AdmissionFee>, IAdmissionFeeRepository
    {
        public AdmissionFeeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionFee> GetAdmissionFeeByName(string name,long sessionId)
        {
            try
            {
                var res = _dbContext.AdmissionFees.FirstOrDefaultAsync(a => a.Name == name&&a.Active==true
                &&a.AcademicSessionId== sessionId);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<AdmissionFee> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionFees.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionFee>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionFees.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionFee>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionFees.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
