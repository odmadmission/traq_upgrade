﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AdmitcardInstructionsRepository : IAdmitcardInstructionsRepository
    {
        //AdmicardInstructions
        private readonly ApplicationDbContext _DbContext;

        public AdmitcardInstructionsRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateAdmicardInstructions(AdmitcardInstructions newAdmicardInstructions)
        {
            try
            {
                newAdmicardInstructions.InsertedDate = DateTime.Now;
                newAdmicardInstructions.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmitcardInstructions>().Add(newAdmicardInstructions);
                _DbContext.SaveChanges();
                return newAdmicardInstructions.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAdmicardInstructions(long id)
        {
            try
            {
                AdmitcardInstructions ob = _DbContext.Set<AdmitcardInstructions>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmicardInstructions.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAdmicardInstructions()
        {
            try
            {
                List<AdmitcardInstructions> res = _DbContext.Set<AdmitcardInstructions>().ToListAsync().Result;
                foreach (AdmitcardInstructions ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AdmicardInstructions.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public AdmitcardInstructions GetAdmicardInstructionsById(long id)
        {
            try
            {
                AdmitcardInstructions ob = _DbContext.Set<AdmitcardInstructions>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public AdmitcardInstructions GetAdmicardInstructionsByName(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AdmitcardInstructions> GetAllAdmicardInstructions()
        {
            try
            {
                IEnumerable<AdmitcardInstructions> res = _DbContext.Set<AdmitcardInstructions>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public bool UpdateAdmicardInstructions(AdmitcardInstructions changedAdmicardInstructions)
        {
            try
            {
                changedAdmicardInstructions.ModifiedDate = DateTime.Now;
                changedAdmicardInstructions.Status = "UPDATED";
                _DbContext.AdmicardInstructions.Attach(changedAdmicardInstructions);
                var entry = _DbContext.Entry(changedAdmicardInstructions);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
    }
}
