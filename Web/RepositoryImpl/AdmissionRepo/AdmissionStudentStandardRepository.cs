﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionStudentStandardRepository : 
        EfRepository<AdmissionStudentStandard>, IAdmissionStudentStandardRepository
    {
        public AdmissionStudentStandardRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionStudentStandard> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionStudentStandards.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<AdmissionStudentStandard> GetByStudentId(long admissionStudentId)
        {
            try
            {
                var res = _dbContext.AdmissionStudentStandards.
                    FirstOrDefaultAsync(a => a.AdmissionStudentId == admissionStudentId);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStudentStandard>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionStudentStandards.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStudentStandard>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionStudentStandards.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
