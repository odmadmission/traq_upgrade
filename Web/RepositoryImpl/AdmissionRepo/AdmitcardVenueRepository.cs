﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AdmitcardVenueRepository : IAdmitcardVenueRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public AdmitcardVenueRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateAdmitcardVenue(AdmitcardVenue newAdmitcardVenue)
        {
            try
            {
                newAdmitcardVenue.InsertedDate = DateTime.Now;
                newAdmitcardVenue.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmitcardVenue>().Add(newAdmitcardVenue);
                _DbContext.SaveChanges();
                return newAdmitcardVenue.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAdmitcardVenue(long id)
        {
            try
            {
                AdmitcardVenue ob = _DbContext.Set<AdmitcardVenue>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmitcardVenues.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAdmitcardVenue()
        {
            try
            {
                List<AdmitcardVenue> res = _DbContext.Set<AdmitcardVenue>().ToListAsync().Result;
                foreach (AdmitcardVenue ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AdmitcardVenues.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public AdmitcardVenue GetAdmitcardVenueById(long id)
        {
            try
            {
                AdmitcardVenue ob = _DbContext.Set<AdmitcardVenue>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public AdmitcardVenue GetAdmitcardVenueByName(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AdmitcardVenue> GetAllAdmitcardVenue()
        {
            try
            {
                IEnumerable<AdmitcardVenue> res = _DbContext.Set<AdmitcardVenue>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public bool UpdateAdmitcardVenue(AdmitcardVenue changedAdmitcardVenue)
        {
            try
            {
                changedAdmitcardVenue.ModifiedDate = DateTime.Now;
                changedAdmitcardVenue.Status = "UPDATED";
                _DbContext.AdmitcardVenues.Attach(changedAdmitcardVenue);
                var entry = _DbContext.Entry(changedAdmitcardVenue);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
