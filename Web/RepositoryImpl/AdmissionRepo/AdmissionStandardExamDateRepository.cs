﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.ApplicationCore.Entities.AdmissionAggregate
{
    public class AdmissionStandardExamDateRepository : EfRepository<AdmissionStandardExamDate>, IAdmissionStandardExamDateRepository
    {
        public AdmissionStandardExamDateRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionStandardExamDate> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionStandardExamDate.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch (Exception e)
            {

                string msg = e.Message;
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStandardExamDate>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionStandardExamDate.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch(Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStandardExamDate>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionStandardExamDate.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch(Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }
    }
}
