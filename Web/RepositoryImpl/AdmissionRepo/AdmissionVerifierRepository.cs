﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionVerifierRepository : EfRepository<AdmissionVerifier>,
        IAdmissionVerifierRepository
    {
        public AdmissionVerifierRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionVerifier> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionVerifier.Where(a => a.ID == id && a.Active == true).FirstOrDefaultAsync();
                return await res;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionVerifier>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionVerifier.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionVerifier>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionVerifier.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
