﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class MasterEmailRepository : EfRepository<MasterEmail>, IMasterEmailRepository
    {
        public MasterEmailRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<MasterEmail> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.MasterEmail.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<MasterEmail>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.MasterEmail.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
