﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionFormPaymentRepository : EfRepository<AdmissionFormPayment>, IAdmissionFormPaymentRepository
    {
        public AdmissionFormPaymentRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionFormPayment> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionFormPayments.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

      

        public async Task<IReadOnlyList<AdmissionFormPayment>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionFormPayments.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionFormPayment>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionFormPayments.Where(a=>a.Active==true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

       

      public async  Task<AdmissionFormPayment> GetByAdmissionId(long id)
        {
            try
            {
                var res = _dbContext.AdmissionFormPayments.FirstOrDefaultAsync(a => a.AdmissionId == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
