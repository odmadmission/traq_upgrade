﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
   public class AdmissionStudentRepository : EfRepository<AdmissionStudent>, IAdmissionStudentRepository
    {
        public AdmissionStudentRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionStudent> GetByAdmissionId(long admissionId)
        {
            try
            {
                var res = _dbContext.AdmissionStudents.FirstOrDefaultAsync(a => a.AdmissionId ==
                admissionId);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<AdmissionStudent> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionStudents.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<AdmissionStudent>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionStudents.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionStudent>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionStudents.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
