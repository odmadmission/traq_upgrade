﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionDocumentAssignRepository : 
        EfRepository<AdmissionDocumentAssign>, IAdmissionDocumentAssignRepository
    {
        public AdmissionDocumentAssignRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public Task<AdmissionDocumentAssign> GetByIdAsyncIncludeAll(long id)
        {
            throw new NotImplementedException();
        }

        Task<IReadOnlyList<AdmissionDocumentAssign>> IEntityCommonMethod<AdmissionDocumentAssign>.ListAllAsyncIncludeAll()
        {
            throw new NotImplementedException();
        }
    }
}
