﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AdmitcardVenueAddressRepository : 
        IAdmitcardVenueAddressRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public AdmitcardVenueAddressRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateAdmitcardVenueAddress(AdmitcardVenueAddress newAdmitcardVenueAddress)
        {
            try
            {
                newAdmitcardVenueAddress.InsertedDate = DateTime.Now;
                newAdmitcardVenueAddress.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmitcardVenueAddress>().Add(newAdmitcardVenueAddress);
                _DbContext.SaveChanges();
                return newAdmitcardVenueAddress.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAdmitcardVenueAddress(long id)
        {
            try
            {
                AdmitcardVenueAddress ob = _DbContext.Set<AdmitcardVenueAddress>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmitcardVenueAddress.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAdmitcardVenueAddress()
        {
           
            try
            {
                List<AdmitcardVenueAddress> res = _DbContext.Set<AdmitcardVenueAddress>().ToListAsync().Result;
                foreach (AdmitcardVenueAddress ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AdmitcardVenueAddress.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public AdmitcardVenueAddress GetAdmitcardVenueAddressById(long id)
        {
            try
            {
                AdmitcardVenueAddress ob = _DbContext.Set<AdmitcardVenueAddress>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }

        }

        public AdmitcardVenueAddress GetAdmitcardVenueAddressByName(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AdmitcardVenueAddress> GetAllAdmitcardVenueAddress()
        {
            try
            {
                IEnumerable<AdmitcardVenueAddress> res = _DbContext.Set<AdmitcardVenueAddress>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public bool UpdateAdmitcardVenueAddress(AdmitcardVenueAddress changedAdmitcardVenueAddress)
        {
            try
            {
                changedAdmitcardVenueAddress.ModifiedDate = DateTime.Now;
                changedAdmitcardVenueAddress.Status = "UPDATED";
                _DbContext.AdmitcardVenueAddress.Attach(changedAdmitcardVenueAddress);
                var entry = _DbContext.Entry(changedAdmitcardVenueAddress);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

      
    }
}
