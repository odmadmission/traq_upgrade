﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo
{
    public class AdmissionInterViewerRepository : EfRepository<AdmissionInterViewer>,
        IAdmissionInterViewerRepository
    {
        public AdmissionInterViewerRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AdmissionInterViewer> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AdmissionInterViewer.Where(a => a.ID == id && a.Active == true).FirstOrDefaultAsync();
                return await res;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionInterViewer>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AdmissionInterViewer.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        public async Task<IReadOnlyList<AdmissionInterViewer>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AdmissionInterViewer.Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
