﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class OrganizationRepository : IOrganizationRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public OrganizationRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateOrganization(Organization newOrganization)
        {
            try
            {
                newOrganization.InsertedDate = DateTime.Now;
                newOrganization.ModifiedDate = DateTime.Now;
                _DbContext.Set<Organization>().Add(newOrganization);
                _DbContext.SaveChanges();
                return newOrganization.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllOrganization()
        {
            try
            {
                List<Organization> res = _DbContext.Set<Organization>().ToListAsync().Result;
                foreach (Organization ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Organizations.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteOrganization(long id)
        {
            try
            {
                Organization ob = _DbContext.Set<Organization>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Organizations.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Organization> GetAllOrganization()
        {
            try
            {
                IEnumerable<Organization> res = _DbContext.Set<Organization>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Organization> GetAllOrganizationByGroupId(long groupId)
        {
            try
            {
                IEnumerable<Organization> ob = _DbContext.Set<Organization>().Where(a => a.GroupID == groupId).ToList();
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public long GetAllOrganizationCountByGroupId(long groupId)
        {
            try
            {
                long ob = _DbContext.Set<Organization>().Where(a => a.GroupID == groupId).Count();
                return ob;
            }
            catch
            {
                return 0;
            }
        }

        public Organization GetOrganizationById(long id)
        {
            try
            {
                Organization ob = _DbContext.Set<Organization>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Organization GetOrganizationByName(string name)
        {
            try
            {
                Organization ob = _DbContext.Set<Organization>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateOrganization(Organization changedOrganization)
        {
            try
            {
                changedOrganization.ModifiedDate = DateTime.Now;
                changedOrganization.Status = "UPDATED";
                _DbContext.Organizations.Attach(changedOrganization);
                var entry = _DbContext.Entry(changedOrganization);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
