﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AddressRepository : IAddressRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public AddressRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateAddress(Address newAddress)
        {
            try
            {
                newAddress.InsertedDate = DateTime.Now;
                newAddress.ModifiedDate = DateTime.Now;
                _DbContext.Set<Address>().Add(newAddress);
                _DbContext.SaveChanges();
                return newAddress.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllAddress()
        {
            try
            {
                List<Address> res = _DbContext.Set<Address>().ToListAsync().Result;
                foreach (Address ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Addresses.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAddress(long id)
        {
            try
            {
                Address ob = _DbContext.Set<Address>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Addresses.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Address> GetAllAddress()
        {
            try
            {
                IEnumerable<Address> res = _DbContext.Set<Address>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Address GetAddressById(long id)
        {
            try
            {
                Address ob = _DbContext.Set<Address>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
  

        public bool UpdateAddress(Address changedAddress)
        {
            try
            {
                changedAddress.ModifiedDate = DateTime.Now;
                changedAddress.Status = "UPDATED";
                _DbContext.Addresses.Attach(changedAddress);
                var entry = _DbContext.Entry(changedAddress);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
