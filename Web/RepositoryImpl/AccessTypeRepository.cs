﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AccessTypeRepository : IAccessTypeRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public AccessTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateAccessType(AccessType newAccessType)
        {
            try
            {
                newAccessType.InsertedDate = DateTime.Now;
                newAccessType.ModifiedDate = DateTime.Now;
                _DbContext.Set<AccessType>().Add(newAccessType);
                _DbContext.SaveChanges();
                return newAccessType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAccessType(long id)
        {
            try
            {
                AccessType ob = _DbContext.Set<AccessType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AccessTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllAccessType()
        {
            try
            {
                List<AccessType> res = _DbContext.Set<AccessType>().ToListAsync().Result;
                foreach (AccessType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AccessTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public AccessType GetAccessTypeById(long id)
        {
            try
            {
                AccessType ob = _DbContext.Set<AccessType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public AccessType GetAccessTypeByName(string name)
        {
            try
            {
                AccessType ob = _DbContext.Set<AccessType>().
                    FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<AccessType> GetAllAccessType()
        {
            try
            {
                IEnumerable<AccessType> res = _DbContext.Set<AccessType>().
                   Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateAccessType(AccessType changedAccessType)
        {
            try
            {
                changedAccessType.ModifiedDate = DateTime.Now;
                changedAccessType.Status = "UPDATED";
                _DbContext.AccessTypes.Attach(changedAccessType);
                var entry = _DbContext.Entry(changedAccessType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
