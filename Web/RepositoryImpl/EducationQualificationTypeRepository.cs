﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class EducationQualificationTypeRepository : IEducationQualificationTypeRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public EducationQualificationTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateEducationQualificationType(EducationQualificationType newEducationQualificationType)
        {
            try
            {
                newEducationQualificationType.InsertedDate = DateTime.Now;
                newEducationQualificationType.ModifiedDate = DateTime.Now;
                _DbContext.Set<EducationQualificationType>().Add(newEducationQualificationType);
                _DbContext.SaveChanges();
                return newEducationQualificationType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllEducationQualificationType()
        {
            try
            {
                List<EducationQualificationType> res = _DbContext.Set<EducationQualificationType>().ToListAsync().Result;
                foreach (EducationQualificationType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.EducationQualificationTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEducationQualificationType(long id)
        {
            try
            {
                EducationQualificationType ob = _DbContext.Set<EducationQualificationType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.EducationQualificationTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<EducationQualificationType> GetAllEducationQualificationType()
        {
            try
            {
                IEnumerable<EducationQualificationType> res = _DbContext.Set<EducationQualificationType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public EducationQualificationType GetEducationQualificationTypeById(long id)
        {
            try
            {
                EducationQualificationType ob = _DbContext.Set<EducationQualificationType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public EducationQualificationType GetEducationQualificationTypeByName(string name)
        {
            try
            {
                EducationQualificationType ob = _DbContext.Set<EducationQualificationType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEducationQualificationType(EducationQualificationType changedEducationQualificationType)
        {
            try
            {
                changedEducationQualificationType.ModifiedDate = DateTime.Now;
                changedEducationQualificationType.Status = "UPDATED";
                _DbContext.EducationQualificationTypes.Attach(changedEducationQualificationType);
                var entry = _DbContext.Entry(changedEducationQualificationType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
