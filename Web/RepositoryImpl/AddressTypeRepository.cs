﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AddressTypeRepository : IAddressTypeRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public AddressTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateAddressType(AddressType newAddressType)
        {
            try
            {
                newAddressType.InsertedDate = DateTime.Now;
                newAddressType.ModifiedDate = DateTime.Now;
                _DbContext.Set<AddressType>().Add(newAddressType);
                _DbContext.SaveChanges();
                return newAddressType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllAddressType()
        {
            try
            {
                List<AddressType> res = _DbContext.Set<AddressType>().ToListAsync().Result;
                foreach (AddressType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AddressTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAddressType(long id)
        {
            try
            {
                AddressType ob = _DbContext.Set<AddressType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AddressTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<AddressType> GetAllAddressType()
        {
            try
            {
                IEnumerable<AddressType> res = _DbContext.Set<AddressType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public AddressType GetAddressTypeById(long id)
        {
            try
            {
                AddressType ob = _DbContext.Set<AddressType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public AddressType GetAddressTypeByName(string name)
        {
            try
            {
                AddressType ob = _DbContext.Set<AddressType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateAddressType(AddressType changedAddressType)
        {
            try
            {
                changedAddressType.ModifiedDate = DateTime.Now;
                changedAddressType.Status = "UPDATED";
                _DbContext.AddressTypes.Attach(changedAddressType);
                var entry = _DbContext.Entry(changedAddressType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
