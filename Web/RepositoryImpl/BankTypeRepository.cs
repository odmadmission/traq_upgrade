﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class BankTypeRepository : IBankTypeRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public BankTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateBankType(BankType newBankType)
        {
            try
            {
                newBankType.InsertedDate = DateTime.Now;
                newBankType.ModifiedDate = DateTime.Now;
                _DbContext.Set<BankType>().Add(newBankType);
                _DbContext.SaveChanges();
                return newBankType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllBankType()
        {
            try
            {
                List<BankType> res = _DbContext.Set<BankType>().ToListAsync().Result;
                foreach (BankType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.BankTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteBankType(long id)
        {
            try
            {
                BankType ob = _DbContext.Set<BankType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.BankTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<BankType> GetAllBankType()
        {
            try
            {
                IEnumerable<BankType> res = _DbContext.Set<BankType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public BankType GetBankTypeById(long id)
        {
            try
            {
                BankType ob = _DbContext.Set<BankType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public BankType GetBankTypeByName(string name)
        {
            try
            {
                BankType ob = _DbContext.Set<BankType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateBankType(BankType changedBankType)
        {
            try
            {
                changedBankType.ModifiedDate = DateTime.Now;
                changedBankType.Status = "UPDATED";
                _DbContext.BankTypes.Attach(changedBankType);
                var entry = _DbContext.Entry(changedBankType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
