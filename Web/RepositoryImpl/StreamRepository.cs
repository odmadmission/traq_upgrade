﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class StreamRepository : IStreamRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public StreamRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        //public long CreateStream(Stream newStream)
        //{
        //    try
        //    {
        //        newStream.InsertedDate = DateTime.Now;
        //        newStream.ModifiedDate = DateTime.Now;
        //        _DbContext.Set<Stream>().Add(newStream);
        //        _DbContext.SaveChanges();
        //        return newStream.ID;
        //    }
        //    catch
        //    {
        //        return 0;
        //    }
        //}

        //public bool DeleteAllStream()
        //{
        //    try
        //    {
        //        List<Stream> res = _DbContext.Set<Stream>().ToListAsync().Result;
        //        foreach (Stream ob in res)
        //        {
        //            ob.Active = false;
        //            ob.ModifiedDate = DateTime.Now;
        //            ob.Status = "DELETED";
        //            _DbContext.Streams.Attach(ob);
        //            var entry = _DbContext.Entry(ob);
        //            entry.State = EntityState.Modified;
        //            entry.Property(e => e.InsertedDate).IsModified = false;
        //            entry.Property(e => e.InsertedId).IsModified = false;
        //            _DbContext.SaveChanges();
        //        }
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        //public bool DeleteStream(long id)
        //{
        //    try
        //    {
        //        Stream ob = _DbContext.Set<Stream>().FirstOrDefaultAsync(a => a.ID == id).Result;
        //        ob.Active = false;
        //        ob.ModifiedDate = DateTime.Now;
        //        ob.Status = "DELETED";
        //        _DbContext.Streams.Attach(ob);
        //        var entry = _DbContext.Entry(ob);
        //        entry.State = EntityState.Modified;
        //        entry.Property(e => e.InsertedDate).IsModified = false;
        //        entry.Property(e => e.InsertedId).IsModified = false;
        //        _DbContext.SaveChanges();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        //public IEnumerable<Stream> GetAllStream()
        //{
        //    try
        //    {
        //        IEnumerable<Stream> res = _DbContext.Set<Stream>().Where(a => a.Active == true).ToListAsync().Result;
        //        return res;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        //public Stream GetStreamById(long id)
        //{
        //    try
        //    {
        //        Stream ob = _DbContext.Set<Stream>().FirstOrDefaultAsync(a => a.ID == id).Result;
        //        return ob;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}
        //public Stream GetStreamByName(string name)
        //{
        //    try
        //    {
        //        Stream ob = _DbContext.Set<Stream>().FirstOrDefaultAsync(a => a.Name == name).Result;
        //        return ob;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        //public bool UpdateStream(Stream changedStream)
        //{
        //    try
        //    {
        //        changedStream.ModifiedDate = DateTime.Now;
        //        //changedStream.Status = "UPDATED";
        //        _DbContext.Streams.Attach(changedStream);
        //        var entry = _DbContext.Entry(changedStream);
        //        entry.State = EntityState.Modified;
        //        entry.Property(e => e.InsertedDate).IsModified = false;
        //        entry.Property(e => e.InsertedId).IsModified = false;
        //        _DbContext.SaveChanges();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
    }
}
