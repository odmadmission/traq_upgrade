﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class DepartmentRepository : IDepartmentRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public DepartmentRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateDepartment(Department newDepartment)
        {
            try
            {
                newDepartment.InsertedDate = DateTime.Now;
                newDepartment.ModifiedDate = DateTime.Now;
                _DbContext.Set<Department>().Add(newDepartment);
                _DbContext.SaveChanges();
                return newDepartment.ID;
            }
            catch
            {
                return 0;
            }
        }

        public long CreateDepartmentType(DepartmentType newDepartmentType)
        {
            try
            {
                newDepartmentType.InsertedDate = DateTime.Now;
                newDepartmentType.ModifiedDate = DateTime.Now;
                _DbContext.Set<DepartmentType>().Add(newDepartmentType);
                _DbContext.SaveChanges();
                return newDepartmentType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllDepartment()
        {
            try
            {
                List<Department> res = _DbContext.Set<Department>().ToListAsync().Result;
                foreach (Department ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Departments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllDepartmentType()
        {
            try
            {
                List<DepartmentType> res = _DbContext.Set<DepartmentType>().ToListAsync().Result;
                foreach (DepartmentType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.DepartmentTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDepartment(long id)
        {
            try
            {
                Department ob = _DbContext.Set<Department>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Departments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDepartmentType(long id)
        {
            try
            {
                DepartmentType ob = _DbContext.Set<DepartmentType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.DepartmentTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Department> GetAllDepartment()
        {
            try
            {
                IEnumerable<Department> res = _DbContext.Set<Department>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public IEnumerable<DepartmentType> GetAllDepartmentType()
        {
            try
            {
                IEnumerable<DepartmentType> res = _DbContext.Set<DepartmentType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Department GetDepartmentById(long id)
        {
            try
            {
                Department ob = _DbContext.Set<Department>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Department> GetDepartmentByIdIn(List<long> id)
        {
           try
            {
                IEnumerable<Department> ob = _DbContext.Set<Department>().Where(a =>  id.Contains(a.ID)).ToListAsync().Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public Department GetDepartmentByName(string name)
        {
            try
            {
                Department ob = _DbContext.Set<Department>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public long GetDepartmentCountByGroupId(long groupId)
        {
            try
            {
                long ob = _DbContext.Set<Department>().Where(a=>a.GroupID==groupId).Count();
                return ob;
            }
            catch
            {
                return 0;
            }
        }

        public long GetDepartmentCountByOrganizationId(long organizationId)
        {
            try
            {
                long ob = _DbContext.Set<Department>().Where(a => a.OrganizationID == organizationId).Count();
                return ob;
            }
            catch
            {
                return 0;
            }
        }

        public IEnumerable<Department> GetDepartmentListByGroupId(long groupId)
        {
            try
            {
                IEnumerable<Department> res = _DbContext.Set<Department>().Where(a => a.GroupID==groupId).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Department> GetDepartmentListByGroupIdIn(List<long> groupId)
        {
          
                return null;
            
        }

        public IEnumerable<Department> GetDepartmentListByOrganizationId(long organizationId)
        {
            try
            {
                IEnumerable<Department> res = _DbContext.Set<Department>().Where(a => a.OrganizationID == organizationId).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Department> GetDepartmentListByOrganizationIdIn(List<long> organizationId)
        {
            try
            {
                IEnumerable<Department> res = _DbContext.Set<Department>().Where(a => organizationId.Contains(a.OrganizationID ) 
                &&a.Active==true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public DepartmentType GetDepartmentTypeById(long id)
        {
            try
            {
                DepartmentType ob = _DbContext.Set<DepartmentType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public DepartmentType GetDepartmentTypeByName(string name)
        {
            try
            {
                DepartmentType ob = _DbContext.Set<DepartmentType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateDepartment(Department changedDepartment)
        {
            try
            {
                changedDepartment.ModifiedDate = DateTime.Now;
                changedDepartment.Status = EntityStatus.ACTIVE;
                _DbContext.Departments.Attach(changedDepartment);
                var entry = _DbContext.Entry(changedDepartment);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateDepartmentType(DepartmentType changedDepartmentType)
        {
            try
            {
                changedDepartmentType.ModifiedDate = DateTime.Now;
                changedDepartmentType.Status = EntityStatus.ACTIVE;
                _DbContext.DepartmentTypes.Attach(changedDepartmentType);
                var entry = _DbContext.Entry(changedDepartmentType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
