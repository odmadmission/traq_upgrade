﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class EmployeeAddressRepository : IEmployeeAddressRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public EmployeeAddressRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateEmployeeAddress(EmployeeAddress newEmployeeAddress)
        {
            try
            {
                newEmployeeAddress.InsertedDate = DateTime.Now;
                newEmployeeAddress.ModifiedDate = DateTime.Now;
                _DbContext.Set<EmployeeAddress>().Add(newEmployeeAddress);
                _DbContext.SaveChanges();
                return newEmployeeAddress.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllEmployeeAddresses()
        {
            try
            {
                List<EmployeeAddress> res = _DbContext.Set<EmployeeAddress>().ToListAsync().Result;
                foreach (EmployeeAddress ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.EmployeeAddresses.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployeeAddress(long id)
        {
            try
            {
                EmployeeAddress ob = _DbContext.Set<EmployeeAddress>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.EmployeeAddresses.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<EmployeeAddress> GetAllEmployeeAddresses()
        {
            try
            {
                IEnumerable<EmployeeAddress> res = _DbContext.Set<EmployeeAddress>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeAddress> GetEmployeeAddressByEmployeeID(long empid)
        {
            try
            {
                IEnumerable<EmployeeAddress> res = _DbContext.Set<EmployeeAddress>().Where(a => a.Active == true && a.EmployeeID== empid).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public EmployeeAddress GetEmployeeAddressByAddressID(long addressid)
        {
            try
            {
                EmployeeAddress res = _DbContext.Set<EmployeeAddress>().Where(a => a.Active == true && a.AddressID == addressid).FirstOrDefault();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public EmployeeAddress GetEmployeeAddressById(long id)
        {
            try
            {
                EmployeeAddress ob = _DbContext.Set<EmployeeAddress>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
      

        public bool UpdateEmployeeAddress(EmployeeAddress changedEmployeeAddress)
        {
            try
            {
                changedEmployeeAddress.ModifiedDate = DateTime.Now;
                changedEmployeeAddress.Status = "UPDATED";
                _DbContext.EmployeeAddresses.Attach(changedEmployeeAddress);
                var entry = _DbContext.Entry(changedEmployeeAddress);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
