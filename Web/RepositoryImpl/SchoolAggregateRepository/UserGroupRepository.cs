﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolAggregateRepository
{
   public class UserGroupRepository : EfRepository<UserGroup>, IUserGroupRepository
    {
        public UserGroupRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<UserGroup> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.UserGroups.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<UserGroup>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.UserGroups.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<UserGroup>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.UserGroups.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
