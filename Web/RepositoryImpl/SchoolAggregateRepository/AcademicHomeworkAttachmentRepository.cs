﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolAggregateRepository
{
    public class AcademicHomeworkAttachmentRepository : EfRepository<AcademicHomeworkAttachment>, IAcademicHomeworkAttachmentRepository
    {
        public AcademicHomeworkAttachmentRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AcademicHomeworkAttachment> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicHomeworkAttachments.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicHomeworkAttachment>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicHomeworkAttachments.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicHomeworkAttachment>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicHomeworkAttachments.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
