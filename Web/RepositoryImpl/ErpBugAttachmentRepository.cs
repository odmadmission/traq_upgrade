﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.Data;
namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ErpBugAttachmentRepository : EfRepository<ErpBugAttachment>, IErpBugAttachmentRepository
    {
        public ErpBugAttachmentRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

       
        public Task<ErpBugAttachment> GetByIdAsyncIncludeAll(long id)
        {
            throw new NotImplementedException();
        }

        public Task<IReadOnlyList<ErpBugAttachment>> ListAllAsyncIncludeAll()
        {
            throw new NotImplementedException();
        }

    }
}
