﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class EducationQualificationRepository : IEducationQualificationRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public EducationQualificationRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateEducationQualification(EducationQualification newEducationQualification)
        {
            try
            {
                newEducationQualification.InsertedDate = DateTime.Now;
                newEducationQualification.ModifiedDate = DateTime.Now;
                _DbContext.Set<EducationQualification>().Add(newEducationQualification);
                _DbContext.SaveChanges();
                return newEducationQualification.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllEducationQualification()
        {
            try
            {
                List<EducationQualification> res = _DbContext.Set<EducationQualification>().ToListAsync().Result;
                foreach (EducationQualification ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.EducationQualifications.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEducationQualification(long id)
        {
            try
            {
                EducationQualification ob = _DbContext.Set<EducationQualification>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.EducationQualifications.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<EducationQualification> GetAllEducationQualification()
        {
            try
            {
                IEnumerable<EducationQualification> res = _DbContext.Set<EducationQualification>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EducationQualification> GetAllEducationQualificationByTypeId(long qualificationId)
        {
            try
            {
                IEnumerable<EducationQualification> res = _DbContext.Set<EducationQualification>().Where(a => a.Active == true && a.EducationQualificationTypeID==qualificationId).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public EducationQualification GetEducationQualificationById(long id)
        {
            try
            {
                EducationQualification ob = _DbContext.Set<EducationQualification>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public EducationQualification GetEducationQualificationByName(string name)
        {
            try
            {
                EducationQualification ob = _DbContext.Set<EducationQualification>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEducationQualification(EducationQualification changedEducationQualification)
        {
            try
            {
                changedEducationQualification.ModifiedDate = DateTime.Now;
                changedEducationQualification.Status = "UPDATED";
                _DbContext.EducationQualifications.Attach(changedEducationQualification);
                var entry = _DbContext.Entry(changedEducationQualification);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
