﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AdmissionTypeRepository : IAdmissionTypeRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public AdmissionTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateAdmissionType(AdmissionType newAdmissionType)
        {
            try
            {
                newAdmissionType.InsertedDate = DateTime.Now;
                newAdmissionType.ModifiedDate = DateTime.Now;
                _DbContext.Set<AdmissionType>().Add(newAdmissionType);
                _DbContext.SaveChanges();
                return newAdmissionType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllAdmissionType()
        {
            try
            {
                List<AdmissionType> res = _DbContext.Set<AdmissionType>().ToListAsync().Result;
                foreach (AdmissionType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.AdmissionTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAdmissionType(long id)
        {
            try
            {
                AdmissionType ob = _DbContext.Set<AdmissionType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.AdmissionTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<AdmissionType> GetAllAdmissionType()
        {
            try
            {
                IEnumerable<AdmissionType> res = _DbContext.Set<AdmissionType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public AdmissionType GetAdmissionTypeById(long id)
        {
            try
            {
                AdmissionType ob = _DbContext.Set<AdmissionType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public AdmissionType GetAdmissionTypeByName(string name)
        {
            try
            {
                AdmissionType ob = _DbContext.Set<AdmissionType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateAdmissionType(AdmissionType changedAdmissionType)
        {
            try
            {
                changedAdmissionType.ModifiedDate = DateTime.Now;
                changedAdmissionType.Status = "UPDATED";
                _DbContext.AdmissionTypes.Attach(changedAdmissionType);
                var entry = _DbContext.Entry(changedAdmissionType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
