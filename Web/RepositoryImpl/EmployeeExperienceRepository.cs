﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class EmployeeExperienceRepository : IEmployeeExperienceRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public EmployeeExperienceRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateExperience(EmployeeExperience newExperience)
        {
            try
            {
                newExperience.InsertedDate = DateTime.Now;
                newExperience.ModifiedDate = DateTime.Now;
                _DbContext.Set<EmployeeExperience>().Add(newExperience);
                _DbContext.SaveChanges();
                return newExperience.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllExperience()
        {
            try
            {
                List<EmployeeExperience> res = _DbContext.Set<EmployeeExperience>().ToListAsync().Result;
                foreach (EmployeeExperience ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.EmployeeExperiences.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteExperience(long id)
        {
            try
            {
                EmployeeExperience ob = _DbContext.Set<EmployeeExperience>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.EmployeeExperiences.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<EmployeeExperience> GetAllExperience()
        {
            try
            {
                IEnumerable<EmployeeExperience> res = _DbContext.Set<EmployeeExperience>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public EmployeeExperience GetExperienceById(long id)
        {
            try
            {
                EmployeeExperience ob = _DbContext.Set<EmployeeExperience>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeExperience> GetExperienceByEmployeeId(long employeeid)
        {
            try
            {
                IEnumerable<EmployeeExperience> ob = _DbContext.Set<EmployeeExperience>().Where(a => a.EmployeeID == employeeid && a.Active==true).ToList();
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateExperience(EmployeeExperience changedExperience)
        {
            try
            {
                changedExperience.ModifiedDate = DateTime.Now;
                changedExperience.Status = "UPDATED";
                _DbContext.EmployeeExperiences.Attach(changedExperience);
                var entry = _DbContext.Entry(changedExperience);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
