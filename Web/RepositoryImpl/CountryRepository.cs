﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class CountryRepository : ICountryRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public CountryRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateCountry(Country newCountry)
        {
            try
            {
                newCountry.InsertedDate = DateTime.Now;
                newCountry.ModifiedDate = DateTime.Now;
                _DbContext.Set<Country>().Add(newCountry);
                _DbContext.SaveChanges();
                return newCountry.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllCountries()
        {
            try
            {
                List<Country> res = _DbContext.Set<Country>().ToListAsync().Result;
                foreach (Country ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Countries.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteCountry(long id)
        {
            try
            {
                Country ob = _DbContext.Set<Country>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Countries.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Country> GetAllCountries()
        {
            try
            {
                IEnumerable<Country> res = _DbContext.Set<Country>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Country GetCountryById(long id)
        {
            try
            {
                Country ob = _DbContext.Set<Country>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Country GetCountryByName(string name)
        {
            try
            {
                Country ob = _DbContext.Set<Country>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateCountry(Country changedCountry)
        {
            try
            {
                changedCountry.ModifiedDate = DateTime.Now;
                changedCountry.Status = "UPDATED";
                _DbContext.Countries.Attach(changedCountry);
                var entry = _DbContext.Entry(changedCountry);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
