﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class EmployeeEducationRepository : IEmployeeEducationRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public EmployeeEducationRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateEmployeeEducation(EmployeeEducation newEmployeeEducation)
        {
            try
            {
                newEmployeeEducation.InsertedDate = DateTime.Now;
                newEmployeeEducation.ModifiedDate = DateTime.Now;
                _DbContext.Set<EmployeeEducation>().Add(newEmployeeEducation);
                _DbContext.SaveChanges();
                return newEmployeeEducation.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllEmployeeEducations()
        {
            try
            {
                List<EmployeeEducation> res = _DbContext.Set<EmployeeEducation>().ToListAsync().Result;
                foreach (EmployeeEducation ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.EmployeeEducations.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployeeEducation(long id)
        {
            try
            {
                EmployeeEducation ob = _DbContext.Set<EmployeeEducation>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.EmployeeEducations.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<EmployeeEducation> GetAllEmployeeEducations()
        {
            try
            {
                IEnumerable<EmployeeEducation> res = _DbContext.Set<EmployeeEducation>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeEducation> GetAllEmployeeEducationsByEmployeeId(long empid)
        {
            try
            {
                IEnumerable<EmployeeEducation> res = _DbContext.Set<EmployeeEducation>().Where(a => a.Active == true && a.EmployeeID == empid).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }      

        public EmployeeEducation GetEmployeeEducationById(long id)
        {
            try
            {
                EmployeeEducation ob = _DbContext.Set<EmployeeEducation>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }


        public bool UpdateEmployeeEducation(EmployeeEducation changedEmployeeEducation)
        {
            try
            {
                changedEmployeeEducation.ModifiedDate = DateTime.Now;
                changedEmployeeEducation.Status = "UPDATED";
                _DbContext.EmployeeEducations.Attach(changedEmployeeEducation);
                var entry = _DbContext.Entry(changedEmployeeEducation);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}