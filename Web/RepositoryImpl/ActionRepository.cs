﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ActionRepository : IActionRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public ActionRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateAction(ApplicationCore.Entities.Action newAction)
        {
            try
            {
                newAction.InsertedDate = DateTime.Now;
                newAction.ModifiedDate = DateTime.Now;
                _DbContext.Set<ApplicationCore.Entities.Action>().Add(newAction);
                _DbContext.SaveChanges();
                return newAction.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllAction()
        {
            try
            {
                List<ApplicationCore.Entities.Action> res = _DbContext.Set<ApplicationCore.Entities.Action>().ToListAsync().Result;
                foreach (ApplicationCore.Entities.Action ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Actions.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAction(long id)
        {
            try
            {
                ApplicationCore.Entities.Action ob = _DbContext.Set<ApplicationCore.Entities.Action>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Actions.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<ApplicationCore.Entities.Action> GetAllAction()
        {
            try
            {
                IEnumerable<ApplicationCore.Entities.Action> res = _DbContext.Set<ApplicationCore.Entities.Action>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public ApplicationCore.Entities.Action GetActionById(long id)
        {
            try
            {
                ApplicationCore.Entities.Action ob = _DbContext.Set<ApplicationCore.Entities.Action>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public ApplicationCore.Entities.Action GetActionByName(string name)
        {
            try
            {
                ApplicationCore.Entities.Action ob = _DbContext.Set<ApplicationCore.Entities.Action>().FirstOrDefaultAsync(a => a.Name == name && a.Active == true).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateAction(ApplicationCore.Entities.Action changedAction)
        {
            try
            {
                changedAction.ModifiedDate = DateTime.Now;
                changedAction.Status = "UPDATED";
                _DbContext.Actions.Attach(changedAction);
                var entry = _DbContext.Entry(changedAction);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
