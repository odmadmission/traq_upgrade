﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ParentRelationshipTypeRepository : IParentRelationshipTypeRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public ParentRelationshipTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public IEnumerable<ParentRelationshipType> GetAllParentRelationshipType()
        {
            try
            {
                IEnumerable<ParentRelationshipType> res = _DbContext.Set<ParentRelationshipType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public ParentRelationshipType GetParentRelationshipTypeById(long id)
        {
            throw new NotImplementedException();
        }

        public ParentRelationshipType GetParentRelationshipTypeByName(string name)
        {
            throw new NotImplementedException();
        }
    }
}
