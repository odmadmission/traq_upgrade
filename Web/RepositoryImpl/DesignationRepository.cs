﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class DesignationRepository : IDesignationRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public DesignationRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateDesignation(Designation newDesignation)
        {
            try
            {
                newDesignation.InsertedDate = DateTime.Now;
                newDesignation.ModifiedDate = DateTime.Now;
                _DbContext.Set<Designation>().Add(newDesignation);
                _DbContext.SaveChanges();
                return newDesignation.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllDesignation()
        {
            try
            {
                List<Designation> res = _DbContext.Set<Designation>().ToListAsync().Result;
                foreach (Designation ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Designations.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteDesignation(long id)
        {
            try
            {
                Designation ob = _DbContext.Set<Designation>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Designations.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Designation> GetAllDesignation()
        {
            try
            {
                IEnumerable<Designation> res = _DbContext.Set<Designation>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<long> GetDesignationByDepartmentId(long id)
        {
            try
            {
                IEnumerable<long> ob = _DbContext.Set<Designation>().Where(a => a.DepartmentID == id&&a.Active==true).Select(a=>a.ID).ToList();
                return ob;

            }
            catch
            {
                return null;
            }
        }

       

        public Designation GetDesignationById(long id)
        {
            try
            {
                Designation ob = _DbContext.Set<Designation>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Designation GetDesignationByName(string name)
        {
            try
            {
                Designation ob = _DbContext.Set<Designation>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Designation> GetDesignationListByDepartmentId(long id)
        {
            try
            {
                IEnumerable<Designation> ob = _DbContext.Set<Designation>().
                    Where(a => a.DepartmentID == id&&a.Active==true).ToList();
                return ob;

            }
            catch
            {
                return null;
            }
        }

        public bool UpdateDesignation(Designation changedDesignation)
        {
            try
            {
                changedDesignation.ModifiedDate = DateTime.Now;
                changedDesignation.Status = "UPDATED";
                _DbContext.Designations.Attach(changedDesignation);
                var entry = _DbContext.Entry(changedDesignation);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
