﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ProfessionTypeRepository : IProfessionTypeRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public ProfessionTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public IEnumerable<ProfessionType> GetAllProfessionTypes()
        {
            try
            {
                IEnumerable<ProfessionType> res = _DbContext.Set<ProfessionType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public ProfessionType GetProfessionTypesById(long id)
        {
            throw new NotImplementedException();
        }

        public ProfessionType GetProfessionTypesByName(string name)
        {
            throw new NotImplementedException();
        }
    }
}
