﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
 public class EmployeeBankAccountRepository : IEmployeeBankAccountRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public EmployeeBankAccountRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateEmployeeBankAccount(EmployeeBankAccount newEmployeeBankAccount)
        {
            try
            {
                newEmployeeBankAccount.InsertedDate = DateTime.Now;
                newEmployeeBankAccount.ModifiedDate = DateTime.Now;
                _DbContext.Set<EmployeeBankAccount>().Add(newEmployeeBankAccount);
                _DbContext.SaveChanges();
                return newEmployeeBankAccount.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllEmployeeBankAccountes()
        {
            try
            {
                List<EmployeeBankAccount> res = _DbContext.Set<EmployeeBankAccount>().ToListAsync().Result;
                foreach (EmployeeBankAccount ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "ACCESS";
                    _DbContext.EmployeeBankAccounts.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployeeBankAccount(long id)
        {
            try
            {
                EmployeeBankAccount ob = _DbContext.Set<EmployeeBankAccount>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.EmployeeBankAccounts.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<EmployeeBankAccount> GetAllEmployeeBankAccountes()
        {
            try
            {
                IEnumerable<EmployeeBankAccount> res = _DbContext.Set<EmployeeBankAccount>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public EmployeeBankAccount GetEmployeeBankAccountById(long id)
        {
            try
            {
                EmployeeBankAccount ob = _DbContext.Set<EmployeeBankAccount>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeBankAccount> GetEmployeeBankAccountByEmployeeID(long empid)
        {
            try
            {
                IEnumerable<EmployeeBankAccount> ob = _DbContext.Set<EmployeeBankAccount>().Where(a => a.Active == true && a.EmployeeID==empid).ToList();
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEmployeeBankAccount(EmployeeBankAccount changedEmployeeBankAccount)
        {
            try
            {
                changedEmployeeBankAccount.ModifiedDate = DateTime.Now;
             
                _DbContext.EmployeeBankAccounts.Attach(changedEmployeeBankAccount);
                var entry = _DbContext.Entry(changedEmployeeBankAccount);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

