﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.CompanyAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public CompanyRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateCompany(Company newCompany)
        {
            try
            {
                newCompany.InsertedDate = DateTime.Now;
                newCompany.ModifiedDate = DateTime.Now;
                _DbContext.Set<Company>().Add(newCompany);
                _DbContext.SaveChanges();
                return newCompany.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllCompany()
        {
            throw new NotImplementedException();
            //try
            //{
            //    List<Company> res = _DbContext.Set<Company>().ToListAsync().Result;
            //    foreach (Company ob in res)
            //    {
            //        ob.Active = false;
            //        ob.ModifiedDate = DateTime.Now;
            //        ob.Status = "DELETED";
            //        _DbContext.com.Attach(ob);
            //        var entry = _DbContext.Entry(ob);
            //        entry.State = EntityState.Modified;
            //        entry.Property(e => e.InsertedDate).IsModified = false;
            //        entry.Property(e => e.InsertedId).IsModified = false;
            //        _DbContext.SaveChanges();
            //    }
            //    return true;
            //}
            //catch
            //{
            //    return false;
            //}
        }

        public bool DeleteCompany(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Company> GetAllCompany()
        {
            throw new NotImplementedException();
        }

        public Company GetCompanyById(long id)
        {
            throw new NotImplementedException();
        }

        public Company GetCompanyByName(string name)
        {
            throw new NotImplementedException();
        }

        public bool UpdateCompany(Company changedCompany)
        {
            throw new NotImplementedException();
        }
    }
}
