﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class MessagesContentRepository : IMessagesContentRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public MessagesContentRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateMessagesContent(MessagesContent newMessagesContent)
        {
            try
            {
                newMessagesContent.InsertedDate = DateTime.Now;
                newMessagesContent.ModifiedDate = DateTime.Now;
                _DbContext.Set<MessagesContent>().Add(newMessagesContent);
                _DbContext.SaveChanges();
                return newMessagesContent.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool UpdateMessagesContent(MessagesContent changeMessagesContent)
        {
            try
            {
                changeMessagesContent.ModifiedDate = DateTime.Now;
                changeMessagesContent.Status = "UPDATED";
                _DbContext.MessagesContents.Attach(changeMessagesContent);
                var entry = _DbContext.Entry(changeMessagesContent);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public MessagesContent GetMessagesContentById(long id)
        {
            try
            {
                MessagesContent ob = _DbContext.Set<MessagesContent>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public MessagesContent GetMessagesContentByName(string name)
        {
            try
            {
                MessagesContent ob = _DbContext.Set<MessagesContent>().FirstOrDefaultAsync(a => a.Type == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public bool DeleteAllMessagesContent()
        {
            try
            {
                List<MessagesContent> res = _DbContext.Set<MessagesContent>().ToListAsync().Result;
                foreach (MessagesContent ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.MessagesContents.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteMessagesContent(long id)
        {
            try
            {
                MessagesContent ob = _DbContext.Set<MessagesContent>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.MessagesContents.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<MessagesContent> GetAllMessagesContent()
        {
            try
            {
                IEnumerable<MessagesContent> res = _DbContext.Set<MessagesContent>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
