﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ScholarshipApplyRepository : IScholarshipApplyRepository
    {
        private readonly ApplicationDbContext _DbContext;
        public ScholarshipApplyRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateScholarshipApply(ScholarshipApply newScholarshipApply)
        {
            try
            {
                newScholarshipApply.InsertedDate = DateTime.Now;
                newScholarshipApply.ModifiedDate = DateTime.Now;
                _DbContext.Set<ScholarshipApply>().Add(newScholarshipApply);
                _DbContext.SaveChanges();
                return newScholarshipApply.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteAllScholarshipApply()
        {
            try
            {
                List<ScholarshipApply> res = _DbContext.Set<ScholarshipApply>().ToListAsync().Result;
                foreach (ScholarshipApply ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.ScholarshipApplys.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteScholarshipApply(long id)
        {
            try
            {
                ScholarshipApply ob = _DbContext.Set<ScholarshipApply>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.ScholarshipApplys.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<ScholarshipApply> GetAllScholarshipApply()
        {
            try
            {
                IEnumerable<ScholarshipApply> res = _DbContext.Set<ScholarshipApply>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ScholarshipApply GetScholarshipApplyById(long id)
        {
            try
            {
                ScholarshipApply ob = _DbContext.Set<ScholarshipApply>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public ScholarshipApply GetScholarshipApplyByName(string name)
        {
            throw new NotImplementedException();
        }

        public bool UpdateScholarshipApply(ScholarshipApply changedScholarshipApply)
        {
            try
            {
                changedScholarshipApply.ModifiedDate = DateTime.Now;
                //changedSessionScholarship.Status = "INACTIVE";
                _DbContext.ScholarshipApplys.Attach(changedScholarshipApply);
                var entry = _DbContext.Entry(changedScholarshipApply);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
