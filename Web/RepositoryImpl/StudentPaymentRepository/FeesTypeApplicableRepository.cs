﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities.Pagination;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class FeesTypeApplicableRepository : IFeesTypeApplicableRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public FeesTypeApplicableRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateFeesTypeApplicable(FeesTypeApplicable newFeesTypeApplicable)
        {
            try
            {
                newFeesTypeApplicable.InsertedDate = DateTime.Now;
                newFeesTypeApplicable.ModifiedDate = DateTime.Now;
                _DbContext.Set<FeesTypeApplicable>().Add(newFeesTypeApplicable);
                _DbContext.SaveChanges();
                return newFeesTypeApplicable.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public IEnumerable<FeesTypeApplicable> GetAllFeesTypeApplicable(int? page, int? noofData)
        {
            try
            {
                IEnumerable<FeesTypeApplicable> res = _DbContext.Set<FeesTypeApplicable>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool UpdateFeesTypeApplicable(FeesTypeApplicable changeFeesTypeApplicable)
        {
            try
            {
                changeFeesTypeApplicable.ModifiedDate = DateTime.Now;
               // changeFeesTypeApplicable.Status = "ACTIVE";
                _DbContext.FeesTypeApplicables.Attach(changeFeesTypeApplicable);
                var entry = _DbContext.Entry(changeFeesTypeApplicable);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateFeesTypeApplicable1(FeesTypeApplicable changeFeesTypeApplicable)
        {
            try
            {
                changeFeesTypeApplicable.ModifiedDate = DateTime.Now;
                changeFeesTypeApplicable.Status = "INACTIVE";
                _DbContext.FeesTypeApplicables.Attach(changeFeesTypeApplicable);
                var entry = _DbContext.Entry(changeFeesTypeApplicable);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public FeesTypeApplicable GetFeesTypeApplicableById(long id)
        {
            try
            {
                FeesTypeApplicable ob = _DbContext.Set<FeesTypeApplicable>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public string GetFeesTypeApplicableNameById(long id)
        {
            try
            {
                FeesTypeApplicable ob = _DbContext.Set<FeesTypeApplicable>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob.Name;
            }
            catch
            {
                return null;
            }
        }

        public FeesTypeApplicable GetFeesTypeApplicableByName(string name)
        {
            try
            {
                FeesTypeApplicable ob = _DbContext.Set<FeesTypeApplicable>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public bool DeleteAllFeesTypeApplicable()
        {
            try
            {
                List<FeesTypeApplicable> res = _DbContext.Set<FeesTypeApplicable>().ToListAsync().Result;
                foreach (FeesTypeApplicable ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.FeesTypeApplicables.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteFeesTypeApplicable(long id)
        {
            try
            {
                FeesTypeApplicable ob = _DbContext.Set<FeesTypeApplicable>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.FeesTypeApplicables.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public PaginationFeestypeApplicable GetPaginationAllFeesTypeApplicable(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue)
        {

            var v = (from a in _DbContext.FeesTypeApplicables where a.Active == true && a.Name.Contains(searchValue) select a);
            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                if (sortColumnDir == "asc")
                {
                    if (sortColumn == "Name")
                    {
                        v = v.OrderByDescending(a => a.Name);
                    }


                }
                //v = v.OrderBy(sortColumn + " " + sortColumnDir);
            }

            totalRecords = v.Count();
            var data = v.Skip(skip).Take(pageSize).ToList();
            //var fd = JsonSerializer.Serialize(data);
            PaginationFeestypeApplicable str = new PaginationFeestypeApplicable { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data };
            //var dfdf = JsonSerializer.Serialize(str);
            // return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });
            return str;
        }


    }
   
}
