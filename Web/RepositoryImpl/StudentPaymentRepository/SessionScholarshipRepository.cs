﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class SessionScholarshipRepository : ISessionScholarship
    {
        private readonly ApplicationDbContext _DbContext;
        public SessionScholarshipRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateSessionScholarship(SessionScholarship newSessionScholarship)
        {
            try
            {
                newSessionScholarship.InsertedDate = DateTime.Now;
                newSessionScholarship.ModifiedDate = DateTime.Now;
                _DbContext.Set<SessionScholarship>().Add(newSessionScholarship);
                _DbContext.SaveChanges();
                return newSessionScholarship.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteAllSessionScholarship()
        {
            try
            {
                List<SessionScholarship> res = _DbContext.Set<SessionScholarship>().ToListAsync().Result;
                foreach (SessionScholarship ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.SessionScholarships.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteSessionScholarship(long id)
        {
            try
            {
                SessionScholarship ob = _DbContext.Set<SessionScholarship>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.SessionScholarships.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SessionScholarship> GetAllSessionScholarship()
        {
            try
            {
                IEnumerable<SessionScholarship> res = _DbContext.Set<SessionScholarship>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public PaginationSessionScholarship GetPaginationSessionScholarship(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue)
        {
            var session = from a in _DbContext.AcademicSessions where a.Active == true select a;
            var scholarshipsList = from a in _DbContext.Scholarships where a.Active == true select a;

            var v = (from a in _DbContext.SessionScholarships join b in session on a.SessionId equals b.ID join c in scholarshipsList on a.ScholarshipId equals c.ID where c.Name.Contains(searchValue) select new CS_SessionScholarship { id = a.ID, sessionName = b.DisplayName, scholarshipName = c.Name, status = a.Status });
            // where a.Active == true && a.Name.Contains(searchValue) select a);
            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                if (sortColumnDir == "asc")
                {
                    if (sortColumn == "Name")
                    {
                        v = v.OrderByDescending(a => a.scholarshipName);
                    }


                }
                //v = v.OrderBy(sortColumn + " " + sortColumnDir);
            }

            totalRecords = v.Count();
            var data = v.Skip(skip).Take(pageSize).ToList();
            //var fd = JsonSerializer.Serialize(data);
            PaginationSessionScholarship str = new PaginationSessionScholarship { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data };
            //var dfdf = JsonSerializer.Serialize(str);
            // return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });
            return str;
        }

        public SessionScholarship GetSessionScholarshipById(long id)
        {
            try
            {
                SessionScholarship ob = _DbContext.Set<SessionScholarship>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public SessionScholarship GetSessionScholarshipByName(string name)
        {
            try
            {
                SessionScholarship ob = _DbContext.Set<SessionScholarship>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateSessionScholarship(SessionScholarship changedSessionScholarship)
        {
            try
            {
                changedSessionScholarship.ModifiedDate = DateTime.Now;
                //changedSessionScholarship.Status = "INACTIVE";
                _DbContext.SessionScholarships.Attach(changedSessionScholarship);
                var entry = _DbContext.Entry(changedSessionScholarship);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
