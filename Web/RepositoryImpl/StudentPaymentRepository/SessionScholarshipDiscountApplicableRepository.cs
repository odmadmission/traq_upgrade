﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class SessionScholarshipDiscountApplicableRepository : ISessionScholarshipDiscountApplicableRepository
    {
        private readonly ApplicationDbContext _DbContext;
        public SessionScholarshipDiscountApplicableRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }
        public long CreateSessionScholarshipDiscountApplicable(SessionScholarshipDiscountApplicable newSessionScholarshipDiscountApplicable)
        {
            try
            {
                newSessionScholarshipDiscountApplicable.InsertedDate = DateTime.Now;
                newSessionScholarshipDiscountApplicable.ModifiedDate = DateTime.Now;
                _DbContext.Set<SessionScholarshipDiscountApplicable>().Add(newSessionScholarshipDiscountApplicable);
                _DbContext.SaveChanges();
                return newSessionScholarshipDiscountApplicable.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteAllSessionScholarshipDiscountApplicable()
        {
            try
            {
                List<SessionScholarshipDiscountApplicable> res = _DbContext.Set<SessionScholarshipDiscountApplicable>().ToListAsync().Result;
                foreach (SessionScholarshipDiscountApplicable ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.SessionScholarshipDiscountApplicables.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteSessionScholarshipDiscountApplicable(long id)
        {
            try
            {
                SessionScholarshipDiscountApplicable ob = _DbContext.Set<SessionScholarshipDiscountApplicable>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.SessionScholarshipDiscountApplicables.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<SessionScholarshipDiscountApplicable> GetAllSessionScholarshipDiscountApplicable()
        {
            try
            {

                // return View(onePageOfEmps);
                IEnumerable<SessionScholarshipDiscountApplicable> res = _DbContext.Set<SessionScholarshipDiscountApplicable>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public PaginationSessionScholarshipDiscountApplicable GetPaginationSessionScholarshipDiscountApplicable(string draw, string sortColumn, string sortColumnDir, int pageSize, int skip, int totalRecords, string searchValue)
        {
            throw new NotImplementedException();
        }

        public SessionScholarshipDiscountApplicable GetSessionScholarshipDiscountApplicableById(long id)
        {
            try
            {
                SessionScholarshipDiscountApplicable ob = _DbContext.Set<SessionScholarshipDiscountApplicable>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public SessionScholarshipDiscountApplicable GetSessionScholarshipDiscountApplicableByName(string name)
        {
            throw new NotImplementedException();
        }

        public bool UpdateSessionScholarshipDiscountApplicable(SessionScholarshipDiscountApplicable changedSessionScholarshipDiscountApplicable)
        {
            try
            {
                changedSessionScholarshipDiscountApplicable.ModifiedDate = DateTime.Now;
                changedSessionScholarshipDiscountApplicable.Status = changedSessionScholarshipDiscountApplicable.Status;
                _DbContext.SessionScholarshipDiscountApplicables.Attach(changedSessionScholarshipDiscountApplicable);
                var entry = _DbContext.Entry(changedSessionScholarshipDiscountApplicable);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
