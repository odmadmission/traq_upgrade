﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class ScholarshipApplicableRepository : IScholarshipApplicableRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public ScholarshipApplicableRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateScholarshipApplicable(ScholarshipApplicable newScholarshipApplicable)
        {
            try
            {
                newScholarshipApplicable.InsertedDate = DateTime.Now;
                newScholarshipApplicable.ModifiedDate = DateTime.Now;
                _DbContext.Set<ScholarshipApplicable>().Add(newScholarshipApplicable);
                _DbContext.SaveChanges();
                return newScholarshipApplicable.ID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public IEnumerable<ScholarshipApplicable> GetAllScholarshipApplicable()
        {
            try
            {
                IEnumerable<ScholarshipApplicable> res = _DbContext.Set<ScholarshipApplicable>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool UpdateScholarshipApplicable(ScholarshipApplicable changeScholarshipApplicable)
        {
            try
            {
                changeScholarshipApplicable.ModifiedDate = DateTime.Now;
                changeScholarshipApplicable.Status = "UPDATED";
                _DbContext.ScholarshipApplicables.Attach(changeScholarshipApplicable);
                var entry = _DbContext.Entry(changeScholarshipApplicable);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public ScholarshipApplicable GetScholarshipApplicableById(long id)
        {
            try
            {
                ScholarshipApplicable ob = _DbContext.Set<ScholarshipApplicable>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public ScholarshipApplicable GetScholarshipApplicableByName(string name)
        {
            try
            {
                ScholarshipApplicable ob = _DbContext.Set<ScholarshipApplicable>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public bool DeleteAllScholarshipApplicable()
        {
            try
            {
                List<ScholarshipApplicable> res = _DbContext.Set<ScholarshipApplicable>().ToListAsync().Result;
                foreach (ScholarshipApplicable ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.ScholarshipApplicables.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteScholarshipApplicable(long id)
        {
            try
            {
                ScholarshipApplicable ob = _DbContext.Set<ScholarshipApplicable>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.ScholarshipApplicables.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
