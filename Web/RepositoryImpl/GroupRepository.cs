﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class GroupRepository : IGroupRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public GroupRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateGroup(Group newGroup)
        {
            try
            {
                newGroup.InsertedDate = DateTime.Now;
                newGroup.ModifiedDate = DateTime.Now;
                _DbContext.Set<Group>().Add(newGroup);
                _DbContext.SaveChanges();
                return newGroup.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllGroup()
        {
            try
            {
                List<Group> res = _DbContext.Set<Group>().ToListAsync().Result;
                foreach (Group ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Groups.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGroup(long id)
        {
            try
            {
                Group ob = _DbContext.Set<Group>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Groups.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Group> GetAllGroup()
        {
            try
            {
                IEnumerable<Group> res = _DbContext.Set<Group>().OrderByDescending(a=>a.Active).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Group GetGroupById(long id)
        {
            try
            {
                Group ob = _DbContext.Set<Group>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Group GetGroupByName(string name)
        {
            try
            {
                Group ob = _DbContext.Set<Group>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateGroup(Group changedGroup)
        {
            try
            {
                changedGroup.ModifiedDate = DateTime.Now;

                _DbContext.Groups.Attach(changedGroup);
                var entry = _DbContext.Entry(changedGroup);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
