﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.GrievanceAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class GrievanceRepository : IGrievanceRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public GrievanceRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        #region Grievance Comments

        public long CreateGrievanceComment(GrievanceComment newGrievanceComment)
        {
            try
            {
                newGrievanceComment.InsertedDate = DateTime.Now;
                newGrievanceComment.ModifiedDate = DateTime.Now;
                _DbContext.Set<GrievanceComment>().Add(newGrievanceComment);
                _DbContext.SaveChanges();
                return newGrievanceComment.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllGrievanceComment()
        {
            try
            {
                List<GrievanceComment> res = _DbContext.Set<GrievanceComment>().ToListAsync().Result;
                foreach (GrievanceComment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.GrievanceComments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGrievanceComment(long id)
        {
            try
            {
                GrievanceComment ob = _DbContext.Set<GrievanceComment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.GrievanceComments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<GrievanceComment> GetAllGrievanceComment()
        {
            try
            {
                IEnumerable<GrievanceComment> res = _DbContext.Set<GrievanceComment>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public GrievanceComment GetGrievanceCommentById(long id)
        {
            try
            {
                GrievanceComment ob = _DbContext.Set<GrievanceComment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
     

        public bool UpdateGrievanceComment(GrievanceComment changedGrievanceComment)
        {
            try
            {
                changedGrievanceComment.ModifiedDate = DateTime.Now;
                changedGrievanceComment.Status = "UPDATED";
                _DbContext.GrievanceComments.Attach(changedGrievanceComment);
                var entry = _DbContext.Entry(changedGrievanceComment);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Grievance Comment Attachments

        public long CreateGrievanceCommentAttachment(GrievanceCommentAttachment newGrievanceCommentAttachment)
        {
            try
            {
                newGrievanceCommentAttachment.InsertedDate = DateTime.Now;
                newGrievanceCommentAttachment.ModifiedDate = DateTime.Now;
                _DbContext.Set<GrievanceCommentAttachment>().Add(newGrievanceCommentAttachment);
                _DbContext.SaveChanges();
                return newGrievanceCommentAttachment.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllGrievanceCommentAttachment()
        {
            try
            {
                List<GrievanceCommentAttachment> res = _DbContext.Set<GrievanceCommentAttachment>().ToListAsync().Result;
                foreach (GrievanceCommentAttachment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.GrievanceCommentAttachments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGrievanceCommentAttachment(long id)
        {
            try
            {
                GrievanceCommentAttachment ob = _DbContext.Set<GrievanceCommentAttachment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.GrievanceCommentAttachments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<GrievanceCommentAttachment> GetAllGrievanceCommentAttachment()
        {
            try
            {
                IEnumerable<GrievanceCommentAttachment> res = _DbContext.Set<GrievanceCommentAttachment>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public GrievanceCommentAttachment GetGrievanceCommentAttachmentById(long id)
        {
            try
            {
                GrievanceCommentAttachment ob = _DbContext.Set<GrievanceCommentAttachment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public GrievanceCommentAttachment GetGrievanceCommentAttachmentByName(string name)
        {
            try
            {
                GrievanceCommentAttachment ob = _DbContext.Set<GrievanceCommentAttachment>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateGrievanceCommentAttachment(GrievanceCommentAttachment changedGrievanceCommentAttachment)
        {
            try
            {
                changedGrievanceCommentAttachment.ModifiedDate = DateTime.Now;
                changedGrievanceCommentAttachment.Status = "UPDATED";
                _DbContext.GrievanceCommentAttachments.Attach(changedGrievanceCommentAttachment);
                var entry = _DbContext.Entry(changedGrievanceCommentAttachment);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion
        public long CreateEmployeeGrievance(EmployeeGrievance newEmployeeGrievance)
        {
            //try
            //{
            newEmployeeGrievance.InsertedDate = DateTime.Now;
            newEmployeeGrievance.ModifiedDate = DateTime.Now;
            _DbContext.Set<EmployeeGrievance>().Add(newEmployeeGrievance);
            _DbContext.SaveChanges();
            return newEmployeeGrievance.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }

        public long CreateGrievance(Grievance newGrievance)
        {
            //try
            //{
            newGrievance.InsertedDate = DateTime.Now;
            newGrievance.ModifiedDate = DateTime.Now;
            _DbContext.Set<Grievance>().Add(newGrievance);
            _DbContext.SaveChanges();
            return newGrievance.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }

        public long CreateGrievancePriority(GrievancePriority newGrievancePriority)
        {   
            newGrievancePriority.InsertedDate = DateTime.Now;
            newGrievancePriority.ModifiedDate = DateTime.Now;
            _DbContext.Set<GrievancePriority>().Add(newGrievancePriority);
            _DbContext.SaveChanges();
            return newGrievancePriority.ID;
        }

        public long CreateGrievanceStatus(GrievanceStatus newGrievanceStatus)
        {
            try
            {
                newGrievanceStatus.InsertedDate = DateTime.Now;
                newGrievanceStatus.ModifiedDate = DateTime.Now;
                _DbContext.Set<GrievanceStatus>().Add(newGrievanceStatus);
                _DbContext.SaveChanges();
                return newGrievanceStatus.ID;
            }
            catch
            {
                return 0;
            }
        }

        public long CreateGrievanceTimeline(GrievanceTimeline newGrievanceTimeline)
        {
            newGrievanceTimeline.InsertedDate = DateTime.Now;
            newGrievanceTimeline.ModifiedDate = DateTime.Now;
            _DbContext.Set<GrievanceTimeline>().Add(newGrievanceTimeline);
            _DbContext.SaveChanges();
            return newGrievanceTimeline.ID;
        }

        public long CreateGrievanceType(GrievanceType newGrievanceType)
        {
            try
            {
                newGrievanceType.InsertedDate = DateTime.Now;
                newGrievanceType.ModifiedDate = DateTime.Now;
                _DbContext.Set<GrievanceType>().Add(newGrievanceType);
                _DbContext.SaveChanges();
                return newGrievanceType.ID;
            }
            catch
            {
                return 0;
            }
        }
        public bool DeleteAllEmployeeGrievance()
        {
            try
            {
                List<EmployeeGrievance> res = _DbContext.Set<EmployeeGrievance>().ToListAsync().Result;
                foreach (EmployeeGrievance ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.EmployeeGrievances.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllGrievance()
        {
            try
            {
                List<Grievance> res = _DbContext.Set<Grievance>().ToListAsync().Result;
                foreach (Grievance ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.Grievances.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllGrievancePriority()
        {
            try
            {
                List<GrievancePriority> res = _DbContext.Set<GrievancePriority>().ToListAsync().Result;
                foreach (GrievancePriority ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.GrievancePriorities.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllGrievanceStatus()
        {
            try
            {
                List<GrievanceStatus> res = _DbContext.Set<GrievanceStatus>().ToListAsync().Result;
                foreach (GrievanceStatus ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.GrievanceStatus.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllGrievanceTimeline()
        {
            try
            {
                List<GrievanceTimeline> res = _DbContext.Set<GrievanceTimeline>().ToListAsync().Result;
                foreach (GrievanceTimeline ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.GrievanceTimelines.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllGrievanceType()
        {
            try
            {
                List<GrievanceType> res = _DbContext.Set<GrievanceType>().ToListAsync().Result;
                foreach (GrievanceType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.GrievanceTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployeeGrievance(long id)
        {
            try
            {
                EmployeeGrievance ob = _DbContext.Set<EmployeeGrievance>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.EmployeeGrievances.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGrievance(long id)
        {
            try
            {
                Grievance ob = _DbContext.Set<Grievance>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.Grievances.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGrievancePriority(long id)
        {
            try
            {
                GrievancePriority ob = _DbContext.Set<GrievancePriority>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.GrievancePriorities.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGrievanceStatus(long id)
        {
            try
            {
                GrievanceStatus ob = _DbContext.Set<GrievanceStatus>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.GrievanceStatus.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGrievanceTimeline(long id)
        {
            try
            {
                GrievanceTimeline ob = _DbContext.Set<GrievanceTimeline>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.GrievanceTimelines.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGrievanceType(long id)
        {
            try
            {
                GrievanceType ob = _DbContext.Set<GrievanceType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.GrievanceTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<EmployeeGrievance> GetAllEmployeeGrievance()
        {
            try
            {
                IEnumerable<EmployeeGrievance> res = _DbContext.Set<EmployeeGrievance>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Grievance> GetAllGrievance()
        {
            try
            {
                IEnumerable<Grievance> res = _DbContext.Set<Grievance>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception e)
            {
                return null;
            }
        }
        public IEnumerable<Grievance> GetactiveinactiveGrievances()
        {
            try
            {
                IEnumerable<Grievance> res = _DbContext.Set<Grievance>().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<GrievancePriority> GetAllGrievancePriority()
        {
            try
            {
                IEnumerable<GrievancePriority> res = _DbContext.
                    Set<GrievancePriority>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<GrievanceStatus> GetAllGrievanceStatus()
        {
            try
            {
                IEnumerable<GrievanceStatus> res = _DbContext.Set<GrievanceStatus>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<GrievanceTimeline> GetAllGrievanceTimeline()
        {
            try
            {
                IEnumerable<GrievanceTimeline> res = _DbContext.Set<GrievanceTimeline>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<GrievanceType> GetAllGrievanceType()
        {
            try
            {
                IEnumerable<GrievanceType> res = _DbContext.Set<GrievanceType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public EmployeeGrievance GetEmployeeGrievanceById(long id)
        {
            try
            {
                EmployeeGrievance ob = _DbContext.Set<EmployeeGrievance>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public Grievance GetGrievanceById(long id)
        {
            try
            {
                Grievance ob = _DbContext.Set<Grievance>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public GrievancePriority GetGrievancePriorityById(long id)
        {
            try
            {
                GrievancePriority ob = _DbContext.Set<GrievancePriority>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public GrievanceStatus GetGrievanceStatusById(long id)
        {
            try
            {
                GrievanceStatus ob = _DbContext.Set<GrievanceStatus>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public GrievanceTimeline GetGrievanceTimelineById(long id)
        {
            try
            {
                GrievanceTimeline ob = _DbContext.Set<GrievanceTimeline>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public GrievanceType GetGrievanceTypeById(long id)
        {
            try
            {
                GrievanceType ob = _DbContext.Set<GrievanceType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEmployeeGrievance(EmployeeGrievance changedEmployeeGrievance)
        {
            try
            {
                changedEmployeeGrievance.ModifiedDate = DateTime.Now;
                changedEmployeeGrievance.Status = EntityStatus.ACTIVE;
                _DbContext.EmployeeGrievances.Attach(changedEmployeeGrievance);
                var entry = _DbContext.Entry(changedEmployeeGrievance);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateGrievance(Grievance changedGrievance)
        {
            try
            {
                changedGrievance.ModifiedDate = DateTime.Now;
                changedGrievance.Status = EntityStatus.ACTIVE;
                _DbContext.Grievances.Attach(changedGrievance);
                var entry = _DbContext.Entry(changedGrievance);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateGrievancePriority(GrievancePriority changedGrievancePriority)
        {
            try
            {
                changedGrievancePriority.ModifiedDate = DateTime.Now;
                changedGrievancePriority.Status = EntityStatus.ACTIVE;
                _DbContext.GrievancePriorities.Attach(changedGrievancePriority);
                var entry = _DbContext.Entry(changedGrievancePriority);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateGrievanceStatus(GrievanceStatus changedGrievanceStatus)
        {
            try
            {
                changedGrievanceStatus.ModifiedDate = DateTime.Now;
                changedGrievanceStatus.Status = EntityStatus.ACTIVE;
                _DbContext.GrievanceStatus.Attach(changedGrievanceStatus);
                var entry = _DbContext.Entry(changedGrievanceStatus);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateGrievanceTimeline(GrievanceTimeline changedGrievanceTimeline)
        {
            try
            {
                changedGrievanceTimeline.ModifiedDate = DateTime.Now;
                changedGrievanceTimeline.Status = EntityStatus.ACTIVE;
                _DbContext.GrievanceTimelines.Attach(changedGrievanceTimeline);
                var entry = _DbContext.Entry(changedGrievanceTimeline);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateGrievanceType(GrievanceType changedGrievanceType)
        {
            try
            {
                changedGrievanceType.ModifiedDate = DateTime.Now;
                changedGrievanceType.Status = EntityStatus.ACTIVE;
                _DbContext.GrievanceTypes.Attach(changedGrievanceType);
                var entry = _DbContext.Entry(changedGrievanceType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


        //-----------------------------------------------------------------
        public long CreateGrievanceAttachment(GrievanceAttachment newGrievanceAttachment)
        {
            //try
            //{
            newGrievanceAttachment.InsertedDate = DateTime.Now;
            newGrievanceAttachment.ModifiedDate = DateTime.Now;
            _DbContext.Set<GrievanceAttachment>().Add(newGrievanceAttachment);
            _DbContext.SaveChanges();
            return newGrievanceAttachment.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }
        public bool DeleteAllGrievanceAttachment()
        {
            try
            {
                List<GrievanceAttachment> res = _DbContext.Set<GrievanceAttachment>().ToListAsync().Result;
                foreach (GrievanceAttachment ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.GrievanceAttachment.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGrievanceAttachment(long id)
        {
            try
            {
                GrievanceAttachment ob = _DbContext.Set<GrievanceAttachment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.GrievanceAttachment.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<GrievanceAttachment> GetAllGrievanceAttachment()
        {
            try
            {
                IEnumerable<GrievanceAttachment> res = _DbContext.Set<GrievanceAttachment>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public GrievanceAttachment GetGrievanceAttachmentById(long id)
        {
            try
            {
                GrievanceAttachment ob = _DbContext.Set<GrievanceAttachment>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateGrievanceAttachment(GrievanceAttachment changedGrievanceAttachment)
        {
            try
            {
                changedGrievanceAttachment.ModifiedDate = DateTime.Now;
                changedGrievanceAttachment.Status = EntityStatus.ACTIVE;
                _DbContext.GrievanceAttachment.Attach(changedGrievanceAttachment);
                var entry = _DbContext.Entry(changedGrievanceAttachment);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //---------------------------------------------------
        public long CreateGrievanceCategory(GrievanceCategory newGrievanceCategory)
        {
            //try
            //{
            newGrievanceCategory.InsertedDate = DateTime.Now;
            newGrievanceCategory.ModifiedDate = DateTime.Now;
            _DbContext.Set<GrievanceCategory>().Add(newGrievanceCategory);
            _DbContext.SaveChanges();
            return newGrievanceCategory.ID;
            //}
            //catch
            //{
            //    return 0;
            //}
        }
        public bool DeleteAllGrievanceCategory()
        {
            try
            {
                List<GrievanceCategory> res = _DbContext.Set<GrievanceCategory>().ToListAsync().Result;
                foreach (GrievanceCategory ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = EntityStatus.DELETED;
                    _DbContext.GrievanceCategory.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGrievanceCategory(long id)
        {
            try
            {
                GrievanceCategory ob = _DbContext.Set<GrievanceCategory>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.GrievanceCategory.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<GrievanceCategory> GetAllGrievanceCategory()
        {
            try
            {
                IEnumerable<GrievanceCategory> res = _DbContext.Set<GrievanceCategory>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public GrievanceCategory GetGrievanceCategoryById(long id)
        {
            try
            {
                GrievanceCategory ob = _DbContext.Set<GrievanceCategory>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateGrievanceCategory(GrievanceCategory changedGrievanceCategory)
        {
            try
            {
                changedGrievanceCategory.ModifiedDate = DateTime.Now;
                changedGrievanceCategory.Status = EntityStatus.ACTIVE;
                _DbContext.GrievanceCategory.Attach(changedGrievanceCategory);
                var entry = _DbContext.Entry(changedGrievanceCategory);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region Grievance Access

        public IEnumerable<GrievanceAccess> GrievanceAccess()
        {
            try
            {
                IEnumerable<GrievanceAccess> res = _DbContext.Set<GrievanceAccess>().AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateGrievanceAccess(GrievanceAccess access)
        {
            access.InsertedDate = DateTime.Now;
            access.ModifiedDate = DateTime.Now;
            _DbContext.Set<GrievanceAccess>().Add(access);
            _DbContext.SaveChanges();
            return access.ID;
        }

        public bool UpdateGrivanceAccess(GrievanceAccess access)
        {
            try
            {
                access.ModifiedDate = DateTime.Now;
                //access.Status = EntityStatus.UPDATED;
                _DbContext.GrievanceAccess.Attach(access);
                var entry = _DbContext.Entry(access);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteGrievanceAccess(long id)
        {
            try
            {
                GrievanceAccess ob = _DbContext.Set<GrievanceAccess>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = EntityStatus.DELETED;
                _DbContext.GrievanceAccess.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public GrievanceAccess GetGrievanceAccessById(long id)
        {
            try
            {
                GrievanceAccess ob = _DbContext.Set<GrievanceAccess>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<GrievanceTimeline> GetAllGrievanceTimelineByGrivId(long id)
        {
            try
            {
                IEnumerable<GrievanceTimeline> res = _DbContext.Set<GrievanceTimeline>().Where(a => a.GrievanceID == id && a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public GrievanceInhouseAttachment GetGrievanceInhouseAttachmentById(long id)
        {
            try
            {
                GrievanceInhouseAttachment attachment = _DbContext.Set<GrievanceInhouseAttachment>().AsNoTracking().ToList().Where(a => a.Active == true && a.ID == id).FirstOrDefault();
                return attachment;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;    
            }
        }

        public IEnumerable<GrievanceInhouseAttachment> GrievanceInhouseAttachment(long grivid)
        {
            try
            {
                IEnumerable<GrievanceInhouseAttachment> attachments = _DbContext.Set<GrievanceInhouseAttachment>().AsNoTracking().ToList().Where(a => a.Active == true && a.GrievanceID == grivid).ToList();
                return attachments;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        public long CreateGrievanceInhouseAttachment(GrievanceInhouseAttachment newGrievanceAttachment)
        {
            try
            {
                newGrievanceAttachment.InsertedDate = DateTime.Now;
                newGrievanceAttachment.ModifiedDate = DateTime.Now;
                newGrievanceAttachment.Active = true;
                newGrievanceAttachment.Status = "ACTIVE";
                newGrievanceAttachment.IsAvailable = true;

                _DbContext.GrievanceInhouseAttachment.Add(newGrievanceAttachment);
                _DbContext.SaveChanges();

                return newGrievanceAttachment.ID;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return 0;
            }
        }
                
        public bool DeleteGrievanceInhouseAttachment(long id,long userid)
        {
            try
            {
                GrievanceInhouseAttachment attachment = _DbContext.Set<GrievanceInhouseAttachment>().AsNoTracking().ToList().Where(a => a.ID == id).FirstOrDefault();
                attachment.ModifiedDate = DateTime.Now;
                attachment.ModifiedId = userid;
                attachment.Active = false;
                attachment.Status = "DELETED";
                attachment.IsAvailable = false;

                _DbContext.GrievanceInhouseAttachment.Update(attachment);
                _DbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return false;
            }
        }

        public IEnumerable<GrievanceInhouseComment> GrievanceInhouseComments(long id)
        {
            try
            {
                IEnumerable<GrievanceInhouseComment> inhouseComments = _DbContext.Set<GrievanceInhouseComment>().AsNoTracking().ToList()==null?null: _DbContext.Set<GrievanceInhouseComment>().AsNoTracking().ToList().Where(a => a.Active == true && a.GrievanceID == id).ToList();
                return inhouseComments;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        public GrievanceInhouseComment GetGrievanceInhouseCommentById(long id)
        {
            try
            {
                GrievanceInhouseComment inhouseComments = _DbContext.Set<GrievanceInhouseComment>().AsNoTracking().ToList().Where(a => a.Active == true && a.ID == id).FirstOrDefault();
                return inhouseComments;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return null;
            }
        }

        public long CreateGrievanceInHouseComment(GrievanceInhouseComment inhouseComment)
        {
            try
            {
                inhouseComment.InsertedDate = DateTime.Now;
                inhouseComment.ModifiedDate = DateTime.Now;
                inhouseComment.Active = true;
                inhouseComment.Status = "ACTIVE";
                inhouseComment.IsAvailable = true;

                _DbContext.GrievanceInhouseComment.Add(inhouseComment);
                _DbContext.SaveChanges();

                return inhouseComment.ID;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return 0;     
            }
        }

        public bool updaeGrievanceInHouseComment(GrievanceInhouseComment inhouseComment)
        {
            try
            {
                inhouseComment.ModifiedDate = DateTime.Now;
                inhouseComment.Active = true;
                inhouseComment.Status = "UPDATED";
                inhouseComment.IsAvailable = true;

                _DbContext.GrievanceInhouseComment.Update(inhouseComment);
                _DbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return false;
            }
        }

        public bool DeleteGrievanceInHouseComment(long id, long userid)
        {
            try
            {
                GrievanceInhouseComment comment = _DbContext.Set<GrievanceInhouseComment>().AsNoTracking().ToList().Where(a => a.ID == id).FirstOrDefault();
                comment.ModifiedDate = DateTime.Now;
                comment.ModifiedId = userid;
                comment.Active = false;
                comment.Status = "DELETED";
                comment.IsAvailable = false;

                _DbContext.GrievanceInhouseComment.Update(comment);
                _DbContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return false;
            }
        }

        #endregion
    }
}
