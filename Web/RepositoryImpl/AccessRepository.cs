﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class AccessRepository : IAccessRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public AccessRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateAccess(Access newAccess)
        {
            try
            {
                newAccess.InsertedDate = DateTime.Now;
                newAccess.ModifiedDate = DateTime.Now;
                _DbContext.Set<Access>().Add(newAccess);
                _DbContext.SaveChanges();
                return newAccess.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllAccess()
        {
            try
            {
                List<Access> res = _DbContext.Set<Access>().ToListAsync().Result;
                foreach (Access ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Accesses.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAccess(long id)
        {
            try
            {
                Access ob = _DbContext.Set<Access>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Accesses.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Access> GetAllAccess()
        {
            try
            {
                IEnumerable<Access> res = _DbContext.Set<Access>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception e1)
            {
                return null;
            }
        }
        public IEnumerable<Access> GetAllAccesswithinactive()
        {
            try
            {
                IEnumerable<Access> res = _DbContext.Set<Access>().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public Access GetAccessById(long id)
        {
            try
            {
                Access ob = _DbContext.Set<Access>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Access GetAccessByUserNameAndPassword(string username,string password)
        {
            try
            {
                Access ob = _DbContext.Set<Access>().FirstOrDefaultAsync(a => a.Username == username && a.Password==password).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public Access GetAccessByUserName(string username)
        {
            try
            {
                Access ob = _DbContext.Set<Access>().FirstOrDefaultAsync(a => a.Username == username).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateAccess(Access changedAccess)
        {
            try
            {
                changedAccess.ModifiedDate = DateTime.Now;
                changedAccess.Status = "UPDATED";
                _DbContext.Accesses.Attach(changedAccess);
                var entry = _DbContext.Entry(changedAccess);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public AccessDto GetAccessDtoByUserNameAndPassword(string username, string password)
        {
            


            return null;

        }

        public long CreateSuperAdminAccess(long employeeId,string username,string password,long roleId)
        {
            try
            {

                Access newAccess = new Access();
                newAccess.Username = username;
                newAccess.Password = password;
                newAccess.RoleID = roleId;
                newAccess.EmployeeID = employeeId;
                newAccess.InsertedId = 0;
                newAccess.Active = true;
                newAccess.Status = EntityStatus.ACTIVE;
                newAccess.ModifiedId = 0;
                newAccess.InsertedDate = DateTime.Now;
                newAccess.ModifiedDate = DateTime.Now;
                _DbContext.Set<Access>().Add(newAccess);
                _DbContext.SaveChanges();
                return newAccess.ID;
            }
            catch
            {
                return 0;
            }
        }

    }
}
