﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ReligionTypeRepository : IReligionTypeRepository
    {

        private readonly ApplicationDbContext _DbContext;

        public ReligionTypeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateReligionType(ReligionType newReligionType)
        {
            try
            {
                newReligionType.InsertedDate = DateTime.Now;
                newReligionType.ModifiedDate = DateTime.Now;
                _DbContext.Set<ReligionType>().Add(newReligionType);
                _DbContext.SaveChanges();
                return newReligionType.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllReligionTypes()
        {
            try
            {
                List<ReligionType> res = _DbContext.Set<ReligionType>().ToListAsync().Result;
                foreach (ReligionType ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.ReligionTypes.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteReligionType(long id)
        {
            try
            {
                ReligionType ob = _DbContext.Set<ReligionType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.ReligionTypes.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<ReligionType> GetAllReligionTypes()
        {
            try
            {
                IEnumerable<ReligionType> res = _DbContext.Set<ReligionType>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public ReligionType GetReligionTypeById(long id)
        {
            try
            {
                ReligionType ob = _DbContext.Set<ReligionType>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public ReligionType GetReligionTypeByName(string name)
        {
            try
            {
                ReligionType ob = _DbContext.Set<ReligionType>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateReligionType(ReligionType changedReligionType)
        {
            try
            {
                changedReligionType.ModifiedDate = DateTime.Now;
                changedReligionType.Status = "UPDATED";
                _DbContext.ReligionTypes.Attach(changedReligionType);
                var entry = _DbContext.Entry(changedReligionType);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
