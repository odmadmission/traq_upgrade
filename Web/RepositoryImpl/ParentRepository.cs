﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class ParentRepository : IParentRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public ParentRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateParent(Parent newParent)
        {
            try
            {
                newParent.InsertedDate = DateTime.Now;
                newParent.ModifiedDate = DateTime.Now;
                _DbContext.Set<Parent>().Add(newParent);
                _DbContext.SaveChanges();
                return newParent.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllParent()
        {
            try
            {
                List<Parent> res = _DbContext.Set<Parent>().ToListAsync().Result;
                foreach (Parent ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Parents.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteParent(long id)
        {
            try
            {
                Parent ob = _DbContext.Set<Parent>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Parents.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Parent> GetAllParent()
        {
            try
            {
                IEnumerable<Parent> res = _DbContext.Set<Parent>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch(Exception e )
            {
                var msg = e.Message;
                return null;
            }
        }

        public Parent GetParentById(long id)
        {
            try
            {
                Parent ob = _DbContext.Set<Parent>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Parent GetParentByPrimaryMobile(string mobile)
        {
            try
            {
                Parent ob = _DbContext.Set<Parent>().FirstOrDefaultAsync(a => a.PrimaryMobile == mobile).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateParent(Parent changedParent)
        {
            try
            {
                changedParent.ModifiedDate = DateTime.Now;
                changedParent.Status = "UPDATED";
                _DbContext.Parents.Attach(changedParent);
                var entry = _DbContext.Entry(changedParent);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
