﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class EmployeeRequiredDocumentRepository : IEmployeeRequiredDocumentRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public EmployeeRequiredDocumentRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateEmployeeRequiredDocument(EmployeeRequiredDocument newEmployeeRequiredDocument)
        {
            try
            {
                newEmployeeRequiredDocument.InsertedDate = DateTime.Now;
                newEmployeeRequiredDocument.ModifiedDate = DateTime.Now;
                _DbContext.Set<EmployeeRequiredDocument>().Add(newEmployeeRequiredDocument);
                _DbContext.SaveChanges();
                return newEmployeeRequiredDocument.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllEmployeeRequiredDocuments()
        {
            try
            {
                List<EmployeeRequiredDocument> res = _DbContext.Set<EmployeeRequiredDocument>().ToListAsync().Result;
                foreach (EmployeeRequiredDocument ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.EmployeeRequiredDocuments.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployeeRequiredDocument(long id)
        {
            try
            {
                EmployeeRequiredDocument ob = _DbContext.Set<EmployeeRequiredDocument>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.EmployeeRequiredDocuments.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<EmployeeRequiredDocument> GetAllEmployeeRequiredDocuments()
        {
            try
            {
                IEnumerable<EmployeeRequiredDocument> res = _DbContext.Set<EmployeeRequiredDocument>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public EmployeeRequiredDocument GetEmployeeRequiredDocumentById(long id)
        {
            try
            {
                EmployeeRequiredDocument ob = _DbContext.Set<EmployeeRequiredDocument>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEmployeeRequiredDocument(EmployeeRequiredDocument changedEmployeeRequiredDocument)
        {
            try
            {
                changedEmployeeRequiredDocument.ModifiedDate = DateTime.Now;
                changedEmployeeRequiredDocument.Status = "UPDATED";
                _DbContext.EmployeeRequiredDocuments.Attach(changedEmployeeRequiredDocument);
                var entry = _DbContext.Entry(changedEmployeeRequiredDocument);
                entry.State = EntityState.Modified;
              
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

