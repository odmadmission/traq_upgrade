﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public EmployeeRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateEmployee(Employee newEmployee)
        {
            try
            {
                newEmployee.InsertedDate = DateTime.Now;
                newEmployee.ModifiedDate = DateTime.Now;
                newEmployee.IsAvailable = true;
                _DbContext.Set<Employee>().Add(newEmployee);
                _DbContext.SaveChanges();
                return newEmployee.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllEmployee()
        {
            try
            {
                List<Employee> res = _DbContext.Set<Employee>().ToListAsync().Result;
                foreach (Employee ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Employees.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployee(long id)
        {
            try
            {
                Employee ob = _DbContext.Set<Employee>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Employees.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public IEnumerable<Employee> GetAllEmployee()
        {
            try
            {
                IEnumerable<Employee> res = _DbContext.Set<Employee>().Where(a => a.Active == true && a.Isleft == false).AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch(Exception e)
            {
                return null;
            }
        }
        public IEnumerable<Employee> GetAllRequiredEmployee()
        {
            try
            {
                IEnumerable<Employee> res = _DbContext.Set<Employee>().Where(a => a.Active == true && a.Isleft==false || a.Isleft == null).AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<Employee> GetAllEmployeewithleft()
        {
            try
            {
                IEnumerable<Employee> res = _DbContext.Set<Employee>().Where(a => a.Active == true).AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public Employee GetEmployeeById(long id)
        {
            try
            {
                Employee ob = _DbContext.Set<Employee>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Employee GetEmployeeByCode(string empcode)
        {
            try
            {
                Employee ob = _DbContext.Set<Employee>().AsNoTracking().FirstOrDefaultAsync(a => a.EmpCode == empcode).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEmployee(Employee changedEmployee)
        {
            try
            {
                changedEmployee.ModifiedDate = DateTime.Now;
                changedEmployee.Status = "UPDATED";
            _DbContext.Employees.Attach(changedEmployee);
            var entry = _DbContext.Entry(changedEmployee);
            entry.State = EntityState.Modified;
            if (changedEmployee.Isleft == null)
            {
                entry.Property(e => e.Isleft).IsModified = false;
                entry.Property(e => e.LeftDate).IsModified = false;
            }
            if (changedEmployee.Image == null)
            {
                entry.Property(e => e.Image).IsModified = false;
            }
            entry.Property(e => e.InsertedDate).IsModified = false;
            entry.Property(e => e.InsertedId).IsModified = false;
            _DbContext.SaveChanges();
                return true;
            }
            catch (Exception e1)
            {
                return false;
            }
        }


        public long CreateSuperAdmin(long roleId)
        {
            try
            {

                Employee newEmployee = new Employee();
                newEmployee.FirstName = "Super";
                newEmployee.LastName = "Admin";
                newEmployee.IsAvailable = true;
                newEmployee.InsertedId = 0;
                newEmployee.ModifiedId = 0;
                newEmployee.Status = EntityStatus.ACTIVE;
                newEmployee.Active = true;
                newEmployee.DateOfBirth = DateTime.Now;
                newEmployee.BloodGroupID = 0;
                newEmployee.EmployeeGroupID = 0;
                newEmployee.ReligionID = 0;
                newEmployee.NatiobalityID = 0;

                newEmployee.InsertedDate = DateTime.Now;
                newEmployee.ModifiedDate = DateTime.Now;
                _DbContext.Set<Employee>().Add(newEmployee);
                _DbContext.SaveChanges();
                return newEmployee.ID;
            }
            catch
            {
                return 0;

            }

        }

        public IEnumerable<Employee> GetAllByEmployeeId(List<long> empIds)
        {
            try
            {
                IEnumerable<Employee> res = _DbContext.Set<Employee>().Where(a => a.Active == true&& empIds.Contains(a.ID)).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;

            }
        }

        public string GetEmployeeFullNameById(long id)
        {
            try
            {
                Employee ob = _DbContext.Set<Employee>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob.FirstName+" "+ob.LastName;
            }
            catch
            {
                return null;
            }
        }
        #region EmployeeTimeline
        public EmployeeTimeline GetEmployeeTimelineById(long id)
        {
            try
            {
                EmployeeTimeline ob = _DbContext.Set<EmployeeTimeline>().AsNoTracking().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public EmployeeTimeline GetEmployeeTimelineEmployeeById(long employeeid)
        {
            try
            {
                EmployeeTimeline ob = _DbContext.Set<EmployeeTimeline>().AsNoTracking().FirstOrDefaultAsync(a => a.EmployeeId == employeeid && a.IsPrimary==true).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<EmployeeTimeline> GetAllEmployeeTimeline()
        {
            try
            {
                IEnumerable<EmployeeTimeline> res = _DbContext.Set<EmployeeTimeline>().Where(a => a.Active == true).AsNoTracking().ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public long CreateEmployeeTimeline(EmployeeTimeline newEmployeeTimeline)
        {
            try
            {
                newEmployeeTimeline.InsertedDate = DateTime.Now;
                newEmployeeTimeline.ModifiedDate = DateTime.Now;
                newEmployeeTimeline.IsAvailable = true;
                _DbContext.Set<EmployeeTimeline>().Add(newEmployeeTimeline);
                _DbContext.SaveChanges();
                return newEmployeeTimeline.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool UpdateEmployeeTimeline(EmployeeTimeline changedEmployeeTimeline)
        {
            try
            {
                changedEmployeeTimeline.ModifiedDate = DateTime.Now;
                changedEmployeeTimeline.Status = "UPDATED";
                _DbContext.EmployeeTimelines.Attach(changedEmployeeTimeline);
                var entry = _DbContext.Entry(changedEmployeeTimeline);
                entry.State = EntityState.Modified;
                
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch (Exception e1)
            {
                return false;
            }
        }

        public bool DeleteEmployeeTimeline(long id)
        {
            try
            {
                EmployeeTimeline ob = _DbContext.Set<EmployeeTimeline>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.EmployeeTimelines.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;

                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAllEmployeeTimeline()
        {
            try
            {
                List<EmployeeTimeline> res = _DbContext.Set<EmployeeTimeline>().ToListAsync().Result;
                foreach (EmployeeTimeline ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.EmployeeTimelines.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public EmployeeTimeline GetActiveEmployeeTimelineByEmployeeId(long empid)
        {

            try
            {
                EmployeeTimeline ob = _DbContext.Set<EmployeeTimeline>().AsNoTracking().FirstOrDefaultAsync(a => a.EmployeeId == empid && a.IsPrimary == true).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        #endregion

       
    }
}
