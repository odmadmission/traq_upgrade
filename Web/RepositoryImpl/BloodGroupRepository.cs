﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class BloodGroupRepository : IBloodGroupRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public BloodGroupRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateBloodGroup(BloodGroup newBloodGroup)
        {
            try
            {
                newBloodGroup.InsertedDate = DateTime.Now;
                newBloodGroup.ModifiedDate = DateTime.Now;
                _DbContext.Set<BloodGroup>().Add(newBloodGroup);
                _DbContext.SaveChanges();
                return newBloodGroup.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllBloodGroup()
        {
            try
            {
                List<BloodGroup> res = _DbContext.Set<BloodGroup>().ToListAsync().Result;
                foreach (BloodGroup ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.BloodGroups.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteBloodGroup(long id)
        {
            try
            {
                BloodGroup ob = _DbContext.Set<BloodGroup>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.BloodGroups.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<BloodGroup> GetAllBloodGroup()
        {
            try
            {
                IEnumerable<BloodGroup> res = _DbContext.Set<BloodGroup>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public BloodGroup GetBloodGroupById(long id)
        {
            try
            {
                BloodGroup ob = _DbContext.Set<BloodGroup>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public BloodGroup GetBloodGroupByName(string name)
        {
            try
            {
                BloodGroup ob = _DbContext.Set<BloodGroup>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateBloodGroup(BloodGroup changedBloodGroup)
        {
            try
            {
                changedBloodGroup.ModifiedDate = DateTime.Now;
                changedBloodGroup.Status = "UPDATED";
                _DbContext.BloodGroups.Attach(changedBloodGroup);
                var entry = _DbContext.Entry(changedBloodGroup);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
