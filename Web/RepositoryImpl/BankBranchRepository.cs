﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OdmErp.Infrastructure.RepositoryImpl
{
   public class BankBranchRepository : IBankBranchRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public BankBranchRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateBankBranch(BankBranch newBankBranch)
        {
            try
            {
                newBankBranch.InsertedDate = DateTime.Now;
                newBankBranch.ModifiedDate = DateTime.Now;
                _DbContext.Set<BankBranch>().Add(newBankBranch);
                _DbContext.SaveChanges();
                return newBankBranch.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllBankBranch()
        {
            try
            {
                List<BankBranch> res = _DbContext.Set<BankBranch>().ToListAsync().Result;
                foreach (BankBranch ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.BankBranches.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteBankBranch(long id)
        {
            try
            {
                BankBranch ob = _DbContext.Set<BankBranch>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.BankBranches.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<BankBranch> GetAllBankBranch()
        {
            try
            {
                IEnumerable<BankBranch> res = _DbContext.Set<BankBranch>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<BankBranch> GetAllBankBranchByBankId(long bankId)
        {
            try
            {
                IEnumerable<BankBranch> res = _DbContext.Set<BankBranch>().Where(a => a.Active == true && a.BankID==bankId).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public BankBranch GetBankBranchById(long id)
        {
            try
            {
                BankBranch ob = _DbContext.Set<BankBranch>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public BankBranch GetBankBranchByName(string name)
        {
            try
            {
                BankBranch ob = _DbContext.Set<BankBranch>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateBankBranch(BankBranch changedBankBranch)
        {
            try
            {
                changedBankBranch.ModifiedDate = DateTime.Now;
                changedBankBranch.Status = "UPDATED";
                _DbContext.BankBranches.Attach(changedBankBranch);
                var entry = _DbContext.Entry(changedBankBranch);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }    
    }
}
