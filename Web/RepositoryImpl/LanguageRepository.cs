﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.GrievanceAggregate;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl
{

    public class LanguageRepository : EfRepository<Language>, ILanguageRepository
    {
        public LanguageRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<Language> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.languages.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<Language>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.languages.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }

    public class AcademicStandardSettingsRepository : EfRepository<AcademicStandardSettings>, IAcademicStandardSettingsRepository
    {
        public AcademicStandardSettingsRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<AcademicStandardSettings> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicStandardSettings.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicStandardSettings>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.AcademicStandardSettings.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }

    public class GrievanceForwardRepository : EfRepository<GrievanceForward>, IGrievanceForwardRepository
    {
        public GrievanceForwardRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<GrievanceForward> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.GrievanceForwards.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<GrievanceForward>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.GrievanceForwards.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class GrievanceExtendedTimelineRepository : EfRepository<GrievanceExtendedTimeline>, IGrievanceExtendedTimelinesRepository
    {
        public GrievanceExtendedTimelineRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<GrievanceExtendedTimeline> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.GrievanceExtendedTimelines.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<GrievanceExtendedTimeline>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.GrievanceExtendedTimelines.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class GrievanceExtendedTimelineAttachmentRepository : EfRepository<GrievanceExtendedTimelineAttachment>, IGrievanceExtendedTimelineAttachmentRepository
    {
        public GrievanceExtendedTimelineAttachmentRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<GrievanceExtendedTimelineAttachment> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.GrievanceExtendedTimelineAttachments.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<GrievanceExtendedTimelineAttachment>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.GrievanceExtendedTimelineAttachments.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class GrievanceAccessTimelineRepository : EfRepository<GrievanceAccessTimeline>, IGrievanceAccessTimelineRepository
    {
        public GrievanceAccessTimelineRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<GrievanceAccessTimeline> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.GrievanceAccessTimelines.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<GrievanceAccessTimeline>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.GrievanceAccessTimelines.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class WingRepository : EfRepository<Wing>, IWingRepository
    {
        public WingRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<Wing> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.Wing.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<Wing>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.Wing.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class StudentTransportationRepository : EfRepository<StudentTransportation>, IStudentTransportationRepository
    {
        public StudentTransportationRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<StudentTransportation> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.StudentTransportations.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<StudentTransportation>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.StudentTransportations.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class StudentEmergencyContactRepository : EfRepository<StudentEmergencyContact>, IStudentEmergencyContactRepository
    {
        public StudentEmergencyContactRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<StudentEmergencyContact> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.StudentEmergencyContacts.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<StudentEmergencyContact>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.StudentEmergencyContacts.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class AcademicStudentSubjectOptionalRepository : EfRepository<AcademicStudentSubjectOptional>, IAcademicStudentSubjectOptionalRepository
    {
        public AcademicStudentSubjectOptionalRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<AcademicStudentSubjectOptional> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.academicStudentSubjectOptionals.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicStudentSubjectOptional>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.academicStudentSubjectOptionals.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class BoardStandardWingRepository : EfRepository<BoardStandardWing>, IBoardStandardWingRepository
    {
        public BoardStandardWingRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<BoardStandardWing> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.BoardStandardWings.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<BoardStandardWing>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.BoardStandardWings.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class SubjectGroupRepository : EfRepository<SubjectGroup>, ISubjectGroupRepository
    {
        public SubjectGroupRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<SubjectGroup> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.SubjectGroups.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<SubjectGroup>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.SubjectGroups.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class AcademicSubjectGroupRepository : EfRepository<AcademicSubjectGroup>, IAcademicSubjectGroupRepository
    {
        public AcademicSubjectGroupRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<AcademicSubjectGroup> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicSubjectGroups.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicSubjectGroup>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.AcademicSubjectGroups.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }

    public class SubjectGroupDetailsRepository : EfRepository<SubjectGroupDetails>, ISubjectGroupDetailsRepository
    {
        public SubjectGroupDetailsRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<SubjectGroupDetails> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.SubjectGroupDetailss.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<SubjectGroupDetails>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.SubjectGroupDetailss.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class SubjectOptionalTypeRepository : EfRepository<SubjectOptionalType>, ISubjectOptionalTypeRepository
    {
        public SubjectOptionalTypeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<SubjectOptionalType> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.SubjectOptionalTypes.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<SubjectOptionalType>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.SubjectOptionalTypes.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class SubjectOptionalCategoryRepository : EfRepository<SubjectOptionalCategory>, ISubjectOptionalCategoryRepository
    {
        public SubjectOptionalCategoryRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<SubjectOptionalCategory> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.SubjectOptionalCategories.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<SubjectOptionalCategory>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.SubjectOptionalCategories.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }

    public class SubjectOptionalRepository : EfRepository<SubjectOptional>, ISubjectOptionalRepository
    {
        public SubjectOptionalRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<SubjectOptional> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.SubjectOptionals.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<SubjectOptional>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.SubjectOptionals.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class AcademicSubjectOptionalRepository : EfRepository<AcademicSubjectOptional>, IAcademicSubjectOptionalRepository
    {
        public AcademicSubjectOptionalRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<AcademicSubjectOptional> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicSubjectOptionals.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicSubjectOptional>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.AcademicSubjectOptionals.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class AcademicSubjectOptionalDetailsRepository : EfRepository<AcademicSubjectOptionalDetails>, IAcademicSubjectOptionalDetailsRepository
    {
        public AcademicSubjectOptionalDetailsRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<AcademicSubjectOptionalDetails> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicSubjectOptionalDetailss.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicSubjectOptionalDetails>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.AcademicSubjectOptionalDetailss.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class VehicleTypeRepository : EfRepository<VehicleType>, IVehicleTypeRepository
    {
        public VehicleTypeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<VehicleType> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.VehicleTypes.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<VehicleType>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.VehicleTypes.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class VehicleRepository : EfRepository<Vehicle>, IVehicleRepository
    {
        public VehicleRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<Vehicle> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.Vehicles.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<Vehicle>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.Vehicles.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class VehicleLocationRepository : EfRepository<VehicleLocation>, IVehicleLocationRepository
    {
        public VehicleLocationRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<VehicleLocation> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.VehicleLocations.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<VehicleLocation>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.VehicleLocations.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class DepartureTimeRepository : EfRepository<DepartureTime>, IDepartureTimeRepository
    {
        public DepartureTimeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<DepartureTime> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.DepartureTimes.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<DepartureTime>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.DepartureTimes.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
    public class AcademicStandardWingRepository : EfRepository<AcademicStandardWing>, IAcademicStandardWingRepository
    {
        public AcademicStandardWingRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<AcademicStandardWing> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.academicStandardWings.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicStandardWing>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = await _dbContext.academicStandardWings.ToListAsync();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
}
