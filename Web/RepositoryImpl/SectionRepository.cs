﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace OdmErp.Infrastructure.RepositoryImpl
{
    public class SectionRepository : ISectionRepository
    {
        private readonly ApplicationDbContext _DbContext;

        public SectionRepository(ApplicationDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public long CreateSection(Section newSection)
        {
            try
            {
                newSection.InsertedDate = DateTime.Now;
                newSection.ModifiedDate = DateTime.Now;
                _DbContext.Set<Section>().Add(newSection);
                _DbContext.SaveChanges();
                return newSection.ID;
            }
            catch
            {
                return 0;
            }
        }

        public bool DeleteAllSection()
        {
            try
            {
                List<Section> res = _DbContext.Set<Section>().ToListAsync().Result;
                foreach (Section ob in res)
                {
                    ob.Active = false;
                    ob.ModifiedDate = DateTime.Now;
                    ob.Status = "DELETED";
                    _DbContext.Sections.Attach(ob);
                    var entry = _DbContext.Entry(ob);
                    entry.State = EntityState.Modified;
                    entry.Property(e => e.InsertedDate).IsModified = false;
                    entry.Property(e => e.InsertedId).IsModified = false;
                    _DbContext.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteSection(long id)
        {
            try
            {
                Section ob = _DbContext.Set<Section>().FirstOrDefaultAsync(a => a.ID == id).Result;
                ob.Active = false;
                ob.ModifiedDate = DateTime.Now;
                ob.Status = "DELETED";
                _DbContext.Sections.Attach(ob);
                var entry = _DbContext.Entry(ob);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<Section> GetAllSection()
        {
            try
            {
                IEnumerable<Section> res = _DbContext.Set<Section>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch
            {
                return null;
            }
        }

        public Section GetSectionById(long id)
        {
            try
            {
                Section ob = _DbContext.Set<Section>().FirstOrDefaultAsync(a => a.ID == id).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }
        public Section GetSectionByName(string name)
        {
            try
            {
                Section ob = _DbContext.Set<Section>().FirstOrDefaultAsync(a => a.Name == name).Result;
                return ob;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateSection(Section changedSection)
        {
            try
            {
                changedSection.ModifiedDate = DateTime.Now;
                changedSection.Status = "UPDATED";
                _DbContext.Sections.Attach(changedSection);
                var entry = _DbContext.Entry(changedSection);
                entry.State = EntityState.Modified;
                entry.Property(e => e.InsertedDate).IsModified = false;
                entry.Property(e => e.InsertedId).IsModified = false;
                _DbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
