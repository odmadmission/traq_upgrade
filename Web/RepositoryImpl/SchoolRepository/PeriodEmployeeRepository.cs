﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class PeriodEmployeeRepository : EfRepository<PeriodEmployee>, IPeriodEmployeeRepository
    {
        public PeriodEmployeeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<PeriodEmployee> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.PeriodEmployees.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }


        public async Task<IReadOnlyList<PeriodEmployee>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.PeriodEmployees.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<PeriodEmployee>> ListAllAsyncIncludeAllNoTrack()
        {
            try
            {
                var res = _dbContext.PeriodEmployees.AsNoTracking().ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<PeriodEmployee>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.PeriodEmployees.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

    }
}

