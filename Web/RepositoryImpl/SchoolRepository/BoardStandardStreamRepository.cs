﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
    //public class BoardStandardStreamRepository : EfRepository<BoardStandardStream>, IBoardStandardStreamRepository
    //{
    //    public BoardStandardStreamRepository(ApplicationDbContext dbContext) : base(dbContext)
    //    {
    //    }

    //    public async Task<BoardStandardStream> GetByIdAsyncIncludeAll(long id)
    //    {
    //        try
    //        {
    //            var res = _dbContext.BoardStandardStreams.FirstOrDefaultAsync(a => a.ID == id);
    //            return await res;
    //        }
    //        catch
    //        {
    //            return null;
    //        }
    //    }

    //    public async Task<IReadOnlyList<BoardStandardStream>> ListAllAsyncIncludeAll()
    //    {
    //        try
    //        {
    //            var res = _dbContext.BoardStandardStreams.ToListAsync();
    //            return await res;
    //        }
    //        catch
    //        {
    //            return null;
    //        }
    //    }
    //    public long GetStreamCountByBoardStandardId(long BoardStandardId)
    //    {
    //        try
    //        {
    //            long res = _dbContext.BoardStandardStreams.ToListAsync().Result.Where(a => a.BoardStandardId == BoardStandardId).Count();

    //            return res;
    //        }
    //        catch
    //        {
    //            return 0;
    //        }
    //    }
    //    public long GetSubjectCountByBoardStandardId(long BoardStandardId)
    //    {
    //        try
    //        {
    //            long res = _dbContext.Subjects.ToListAsync().Result.Where(a => a.BoardStandardId == BoardStandardId).Count();

    //            return res;
    //        }
    //        catch
    //        {
    //            return 0;
    //        }
    //    }

    //    public long GetSubjectsreamCountByBoardStandardId(long boardStandardSreamID)
    //    {
    //        try
    //        {
    //            long res = _dbContext.Subjects.ToListAsync().Result.Where(a => a.BoardStandardStreamId == boardStandardSreamID).Count();

    //            return res;
    //        }
    //        catch
    //        {
    //            return 0;
    //        }
    //    }
    //    public async Task<IReadOnlyList<BoardStandardStream>> ListAllAsyncIncludeActiveNoTrack()
    //    {
    //        try
    //        {
    //            var res = _dbContext.BoardStandardStreams.AsNoTracking().Where(a => a.Active == true).ToListAsync();
    //            return await res;
    //        }
    //        catch
    //        {
    //            return null;
    //        }
    //    }
    //}
}
