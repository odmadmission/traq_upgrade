﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;

using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class AcademicSectionRepository : EfRepository<AcademicSection>, IAcademicSectionRepository
    {
      
        public AcademicSectionRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            
        }

        public async Task<AcademicSection> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicSections.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<AcademicSection>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicSections.AsNoTracking().Where(a=>a.Active==true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<AcademicSection>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicSections.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

       

    }
}
