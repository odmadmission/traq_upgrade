﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   //public class AcademicStandardStreamRepository : EfRepository<AcademicStandardStream>, IAcademicStandardStreamRepository
   // {
   //     public AcademicStandardStreamRepository(ApplicationDbContext dbContext) : base(dbContext)
   //     {
   //     }

   //     public async Task<AcademicStandardStream> GetByIdAsyncIncludeAll(long id)
   //     {
   //         try
   //         {
   //             var res = _dbContext.AcademicStandardStreams.FirstOrDefaultAsync(a => a.ID == id);
   //             return await res;
   //         }
   //         catch
   //         {
   //             return null;
   //         }
   //     }

   //     public async Task<IReadOnlyList<AcademicStandardStream>> ListAllAsyncIncludeAll()
   //     {
   //         try
   //         {
   //             var res = _dbContext.AcademicStandardStreams.ToListAsync();
   //             return await res;
   //         }
   //         catch
   //         {
   //             return null;
   //         }
   //     }
   //     public async Task<IReadOnlyList<AcademicStandardStream>> ListAllAsyncIncludeActiveNoTrack()
   //     {
   //         try
   //         {
   //             var res = _dbContext.AcademicStandardStreams.AsNoTracking().Where(a => a.Active == true).ToListAsync();
   //             return await res;
   //         }
   //         catch
   //         {
   //             return null;
   //         }
   //     }
   // }
}
