﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class OrganizationAcademicRepository: EfRepository<OrganizationAcademic>, IOrganizationAcademicRepository
    {
        public OrganizationAcademicRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<OrganizationAcademic> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.OrganizationAcademics.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public long GetOrganizationAcademicCountBySessionId(long sessionId)
        {
            try
            {
                long res = _dbContext.OrganizationAcademics.ToListAsync().Result.Where(a => a.AcademicSessionId == sessionId).Count();

                return res;
            }
            catch
            {
                return 0;
            }
        }

        public async Task<IReadOnlyList<OrganizationAcademic>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.OrganizationAcademics.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<OrganizationAcademic>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.OrganizationAcademics.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;              
            }
            catch
            {
                return null;
            }
        }
    }
}
