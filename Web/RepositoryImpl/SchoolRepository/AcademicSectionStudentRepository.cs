﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
    public class AcademicSectionStudentRepository : EfRepository<AcademicSectionStudent>, IAcademicSectionStudentRepository
    {
        public AcademicSectionStudentRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }


        public async Task<AcademicSectionStudent> GetByIdAsyncIncludeAll(long id)
        {

            try
            {
                var res = _dbContext.AcademicSectionStudents.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicSectionStudent>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicSectionStudents.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<AcademicSectionStudent>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicSectionStudents.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}

