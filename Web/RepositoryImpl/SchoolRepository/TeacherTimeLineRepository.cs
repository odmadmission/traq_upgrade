﻿using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class TeacherTimeLineRepository : EfRepository<TeacherTimeLine>, ITeacherTimeLineRepository
    {
        public TeacherTimeLineRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<TeacherTimeLine> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.TeacherTimeLines.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<TeacherTimeLine>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.TeacherTimeLines.ToListAsync();
                return await res;
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}
