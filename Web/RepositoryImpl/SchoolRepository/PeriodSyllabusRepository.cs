﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
   public class PeriodSyllabusRepository : EfRepository<PeriodSyllabus>, IPeriodSyllabusRepository
    {
        public PeriodSyllabusRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<PeriodSyllabus> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.PeriodSyllabus.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }


        public async Task<IReadOnlyList<PeriodSyllabus>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.PeriodSyllabus.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<PeriodSyllabus>> ListAllAsyncIncludeAllNoTrack()
        {
            try
            {
                var res = _dbContext.PeriodSyllabus.AsNoTracking().ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<PeriodSyllabus>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.PeriodSyllabus.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

    }
}

