﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
    public class CalenderEngagementTypeRepository : EfRepository<CalenderEngagementType>, ICalenderEngagementTypeRepository
    {
        public CalenderEngagementTypeRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<CalenderEngagementType> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.CalenderEngagementTypes.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<CalenderEngagementType>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.CalenderEngagementTypes.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }

    public class CalenderEngagementRepository : EfRepository<CalenderEngagement>, ICalenderEngagementRepository
    {
        public CalenderEngagementRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<CalenderEngagement> GetByAcademicCalenderIdAndDate(long calenderId, DateTime date)
        {
            try
            {
                var res = _dbContext.CalenderEngagements.FirstOrDefaultAsync(a => a.AcademicCalenderId == calenderId&&a.Day==date.Date);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<CalenderEngagement> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.CalenderEngagements.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public long GetDayCountByTypeId(long calenderEngagementTypeId)
        {
            try
            {
                var res = _dbContext.CalenderEngagements.CountAsync(a => a.CalenderEngagementTypeId == calenderEngagementTypeId).Result;
                return  res;
            }
            catch
            {
                return 0;
            }
        }

        public async Task<IReadOnlyList<CalenderEngagement>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.CalenderEngagements.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }



    public class AcademicCalenderRepository : EfRepository<AcademicCalender>, IAcademicCalenderRepository
    {
        public AcademicCalenderRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AcademicCalender> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicCalenders.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

      

        public async Task<IReadOnlyList<AcademicCalender>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicCalenders.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
