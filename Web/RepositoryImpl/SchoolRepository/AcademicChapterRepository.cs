﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
    public class AcademicChapterRepository : EfRepository<AcademicChapter>, IAcademicChapterRepository
    {
        public AcademicChapterRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<AcademicChapter> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicChapters.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicChapter>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicChapters.ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
    }
}
