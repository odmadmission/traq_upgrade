﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Infrastructure.RepositoryImpl.SchoolRepository
{
    public class AcademicSessionRepository : EfRepository<AcademicSession>, IAcademicSessionRepository
    {
        private readonly ApplicationDbContext _DbContext;
        public AcademicSessionRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _DbContext = dbContext;
        }

        public long GetAcademicSubjectCountBySessionId(long sessionId)
        {
            try
            {
                long res = _dbContext.AcademicSubjects.ToListAsync().Result.Where(a => a.AcademicSessionId == sessionId).Count();

                return res;
            }
            catch
            {
                return 0;
            }
        }
        public IEnumerable<AcademicSession> GetAllAcademicSession()
        {
            try
            {
                IEnumerable<AcademicSession> res = _dbContext.Set<AcademicSession>().Where(a => a.Active == true).ToListAsync().Result;
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public async Task<AcademicSession> GetByIdAsyncIncludeAll(long id)
        {
            try
            {
                var res = _dbContext.AcademicSessions.FirstOrDefaultAsync(a => a.ID == id);
                return await res;
            }
            catch
            {
                return null;
            }
        }


        public async Task<IReadOnlyList<AcademicSession>> ListAllAsyncIncludeAll()
        {
            try
            {
                var res = _dbContext.AcademicSessions.ToListAsync();
                return await res;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public async Task<IReadOnlyList<AcademicSession>> ListAllAsyncIncludeAllNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicSessions.AsNoTracking().ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<IReadOnlyList<AcademicSession>> ListAllAsyncIncludeActiveNoTrack()
        {
            try
            {
                var res = _dbContext.AcademicSessions.AsNoTracking().Where(a => a.Active == true).ToListAsync();
                return await res;
            }
            catch
            {
                return null;
            }
        }

    }
}
