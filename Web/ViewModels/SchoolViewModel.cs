﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.ViewModels
{
    public class SchoolOrganizationAcademicViewModel:BaseViewModel
    {

        public IEnumerable<SchoolAcademicStandardViewModel> classes { get; set; }

    }

    public class SchoolAcademicStandardViewModel : BaseViewModel
    {

      

    }
    public class NameIdCountViewModel
    {
        public bool Active { get; set; }
        public long ID { get; set; }
        public long OrgCount { get; set; }

        public long DeptCount { get; set; }
        public string Name { get; set; }
        public DateTime ModifiedDate { get; set; }

        public string ModifiedName { get; set; }

        public string Status { get; set; }

        public long DocSubCount { get; set; }



        public List<long> ids { get; set; }
    }

   

    public class NameBoardCountViewModel
    {
        public bool Active { get; set; }
        public long ID { get; set; }
        public long BoardCount { get; set; }

        public long BoardID { get; set; }
        public string Name { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedText { get; set; }
        public string ModifiedName { get; set; }


        public string Status { get; set; }


    }
    public class NameStreamCountViewModel
    {
        public bool Active { get; set; }
        public long ID { get; set; }
        public long StreamCount { get; set; }

        public long BoardID { get; set; }
        public string StandardName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedText { get; set; }
        public string ModifiedName { get; set; }

        public string BoardName { get; set; }
        public string Status { get; set; }

        public bool StreamType { get; set; }

        public long SubjectCount { get; set; }
    }

    public class NameStreamSubjectCountViewModel
    {
        public bool Active { get; set; }
        public long ID { get; set; }
        public long StreamCount { get; set; }

        public long BoardID { get; set; }
        public string StandardName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedText { get; set; }
        public string ModifiedName { get; set; }

        public string BoardName { get; set; }
        public string Status { get; set; }

        public bool StreamType { get; set; }

        public long SubjectCount { get; set; }
    }


    public class CountSubjectStreamWiseViewModel
    {
        public long ID { get; set; }
        public long BoardStandardId { get; set; }
        public long StreamId { get; set; }
        public long BoardId { get; set; }
        public long StandardId { get; set; }

        public string StreamName { get; set; }
        public string BoardStandardName { get; set; }

        public long BoardStandarStreamSubCnt { get; set; }

        public string ModifiedText { get; set; }
        public DateTime ModifiedDate { get; set; }

        public string Status { get; set; }
    }
    public class CountChapterCountViewModel
    {
        public long ID { get; set; }


        public string Name { get; set; }

        public long SubjectId { get; set; }
        public string SubjectName { get; set; }

        public string SubjectCode { get; set; }

        public bool IsSubChapter { get; set; }

        public long ParentChapterId { get; set; }
        public string ParentChapterName { get; set; }
        public string Chapter { get; set; }

    }
    public class NameAcademicSubjectCountViewModel
    {
        public long ID { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool Active { get; set; }
        public string Status { get; set; }
        public long SubjectId { get; set; }
        public long AcademicSessinId { get; set; }
        public long AcademicSubjectCount { get; set; }
        public long OrganizationAcademicCount { get; set; }
        public string Name { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsAdmission { get; set; }
    }

    public class NameOrganizationAcademicCountViewModel
    {
        public long ID { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool Active { get; set; }
        public string Status { get; set; }
        public long AcademicSessinId { get; set; }
        public long OrganizationId { get; set; }
        public long OrganizationAcademicCount { get; set; }
        //  public long OrganizationAcademicCount { get; set; }
        public bool IsAvailable { get; set; }

        public long BoardId { get; set; }
        public long BoardStandardId { get; set; }
        public long BoardStandardStreamId { get; set; }
        public string ModifiedText { get; set; }

        public long CountChapter { get; set; }



    }
    public class DataModel
    {

        public IEnumerable<NameIdViewModel> Streams { get; set; }

        public IEnumerable<NameIdViewModel> Subjects { get; set; }

    }
    public class NameIdViewModel
    {
        public bool Selected { get; set; }
        public long ID { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

    }
    public class AssignEmployeeModel
    { 
        public long ID { get; set; }
        public string Name { get; set; }
        public Nullable<DateTime> AssignedDate { get; set; }

    }



    public class AcademicDataModel
    {

        public IEnumerable<NameIdViewModel> data { get; set; }

        public string Start { get; set; }
        public string End { get; set; }
    }

    public class CalenderDataModel
    {

        public long SessionID { get; set; }
        public long OrganizationID { get; set; }
        public long WingID { get; set; }

        public long CalenderEngagementTypeID { get; set; }

        public DateTime CalenderDate { get; set; }

        public string Name { get; set; }
        public long Id { get; set; }

        public long[] BoardID { get; set; }
        public long[] StandardID { get; set; }
    }

    public class CalenderEventData
    {
        public string groupId { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }

        public bool allDay { get; set; }
        public bool editable { get; set; }
        public string backgroundColor { get; set; }
        public string textColor { get; set; }

        public ExtendedProperties extendedProps { get; set; }
       

    }

    public class ExtendedProperties
    {
        public ExtendedProperties(long typeId, string type, string name, string date)
        {
            TypeId = typeId;
            Type = type;
            Name = name;
            Date = date;
        }

        public long TypeId { get; set; }

        public string Type { get; set; }
        public string Name { get; set; }

        public string Date { get; set; }
    }

   
}
