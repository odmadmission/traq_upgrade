﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.ViewModels
{
    public class EmployeeDepartmentNameViewModel
    {

        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }
        public string EmployeeName { get; set; }


        public long DepartmentId;
        public long DesignationId;


    }
}
