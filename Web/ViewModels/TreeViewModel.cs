﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.ViewModels
{
    public class TreeViewModel
    {
        public TreeViewChart chart { get; set; }
     
        public NodeStructure nodeStructure { get; set; }

    }

    public class TreeViewConnector
    {
        public string type { get; set; }

        public TreeViewStyle style { get; set; }
    }
    public class TreeViewChart
    {
        public string container { get; set; }

        public string rootOrientation { get; set; }

        public int levelSeparation { get; set; }

        public string nodeAlign { get; set; }

        public TreeViewConnector connectors { get; set; }
    }


    public class TreeViewStyle
    {
        public TreeViewStyle(string stoke)
        {
            this.stoke = stoke;
        }

        public string stoke { get; set; }
    }

    public class NodeStructure
    {
        public NodeStructureText text { get; set; }
        public List<NodeStructure> children { get; set; }

        public bool stackChildren { get; set; }

        public TreeViewConnector connectors { get; set; }
    }

    public class NodeStructureText
    {

        public NodeStructureText()
        {

        }
        public NodeStructureText(string n)
        {
            name = n;
        }

      

        public string name { get; set; }
     
        
    }

   

   
}
