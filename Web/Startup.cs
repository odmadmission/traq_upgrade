﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.RepositoryImpl;
using Microsoft.AspNetCore.Session;
using System.IO;
using Microsoft.Extensions.FileProviders;
using OdmErp.Web.Models;
using OdmErp.Web.Models.Background;
using Microsoft.AspNetCore.Http.Features;
using OdmErp.Infrastructure.RepositoryImpl.SchoolRepository;
using OdmErp.ApplicationCore.Entities;
using OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository;
using OdmErp.Infrastructure;
using OdmErp.Web.Areas.Tasks.Models;
using OdmErp.Web.Areas.Support.Models;
using OdmErp.Infrastructure.RepositoryImpl.SchoolAggregateRepository;
using Microsoft.AspNetCore.Internal;
using Newtonsoft.Json;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using OdmErp.Web.Areas.SchoolBellQ.Models;
using OdmErp.Infrastructure.RepositoryImpl.AddmissionRepository;
using OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo;
using OdmErp.Web.Areas.Admission.Models;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using DinkToPdf;
using DinkToPdf.Contracts;
using System.Runtime.Loader;
using System.Reflection;
using OdmErp.Web.BillDesk;
using OdmErp.Web.SendInBlue;

//using Microsoft.Extensions.Hosting;

namespace OdmErp.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            hostingEnvironment = env;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment hostingEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services )
        {
            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
           // services.AddControllersWithViews();
            //var architectureFolder = (IntPtr.Size == 8) ? "64 bit" : "32 bit";
            //var wkHtmlToPdfPath = Path.Combine(_hostingEnvironment.ContentRootPath, $"wkhtmltox\\v0.12.4\\{architectureFolder}\\libwkhtmltox");
          //  var hh = new CustomAssemblyLoadContext();
          //  hh.LoadUnmanagedLibrary(Path.Combine(Directory.GetCurrentDirectory(), "libwkhtmltox.dll"));
            //context.LoadUnmanagedLibrary(wkHtmlToPdfPath);
            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            //services.AddHttpContextAccessor();
            services.Configure<FormOptions>(x => x.ValueCountLimit = 10000);
            /* =============================Repository & Services==================================== */
            services.AddTransient<ICountryRepository, CountryRepository>();
            services.AddTransient<IAdmissionTypeRepository, AdmissionTypeRepository>();
            services.AddTransient<IBloodGroupRepository, BloodGroupRepository>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IBoardRepository, BoardRepository>();
            services.AddTransient<IDesignationLevelRepository, DesignationLevelRepository>();
            services.AddTransient<IEducationQualificationTypeRepository, EducationQualificationTypeRepository>();
            services.AddTransient<IGroupRepository, GroupRepository>();
            services.AddTransient<IModuleRepository, ModuleRepository>();
            services.AddTransient<IOrganizationTypeRepository, OrganizationTypeRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<ISectionRepository, SectionRepository>();
            services.AddTransient<IStudentRepository, StudentRepository>();
            services.AddTransient<IEmployeeDocumentAttachmentRepository, EmployeeDocumentAttachmentRepository>();
            // services.AddSingleton<Microsoft.Extensions.Hosting.IHostedService, TimedHostedService>();
            //services.AddTransient<IHostedService, TimedHostedService>();
            services.AddHostedService<TimedHostedService>();


            //   services.AddHostedService<BackgroundService>();
            // var hostBuilder = new Microsoft.Extensions.Hosting.HostBuilder().ConfigureServices(services => services.AddHostedService<WorkerService>());
            services.AddTransient<RazorpayManager, RazorpayManager>();
            services.AddTransient<IStateRepository, StateRepository>();
            services.AddTransient<IDepartmentRepository, DepartmentRepository>();
            services.AddTransient<IDesignationRepository, DesignationRepository>();
            services.AddTransient<IEducationQualificationRepository, EducationQualificationRepository>();
            services.AddTransient<IStandardRepository, StandardRepository>();
            services.AddTransient<IStreamRepository, StreamRepository>();
            services.AddTransient<ISubModuleRepository, SubModuleRepository>();
            services.AddTransient<IOrganizationRepository, OrganizationRepository>();
            services.AddTransient<ICityRepository, CityRepository>();
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();
            services.AddTransient<INationalityTypeRepository, NationalityTypeRepository>();
            services.AddTransient<IAddressTypeRepository, AddressTypeRepository>();
            services.AddTransient<IReligionTypeRepository, ReligionTypeRepository>();
            services.AddTransient<IEmployeeDesignationRepository, EmployeeDesignationRepository>();
            services.AddTransient<IAddressRepository, AddressRepository>();
            services.AddTransient<IEmployeeAddressRepository, EmployeeAddressRepository>();
            services.AddTransient<IEmployeeExperienceRepository, EmployeeExperienceRepository>();
            services.AddTransient<IBankRepository, BankRepository>();
            services.AddTransient<IBankBranchRepository, BankBranchRepository>();
            services.AddTransient<IDocumentTypeRepository, DocumentTypeRepository>();
            services.AddTransient<IDocumentSubTypeRepository, DocumentSubTypeRepository>();
            services.AddTransient<IEmployeeEducationRepository, EmployeeEducationRepository>();
            services.AddTransient<IEmployeeBankAccountRepository, EmployeeBankAccountRepository>();
            services.AddTransient<IEmployeeRequiredDocumentRepository, EmployeeRequiredDocumentRepository>();
            services.AddTransient<IActionRepository, ActionRepository>();
            services.AddTransient<IAccessTypeRepository, AccessTypeRepository>();

         //   services.AddTransient<IAccessRepository, AccessRepository>();

            services.AddTransient<IAccessRepository, AccessRepository>();
            services.AddTransient<IActionAccessRepository, ActionAccessRepository>();
            services.AddTransient<IAuthorityAggregateRepository, AuthorityAggregateRepository>();
            services.AddTransient<IStudentAggregateRepository, StudentAggregateRepository>();

            services.AddTransient<IAdmissionCounsellorRepository, AdmissionCounsellorRepository>();
           
            services.AddTransient<SendInBlueEmailManager, SendInBlueEmailManager>();

            services.AddTransient<CommonMethods, CommonMethods>();
            services.AddTransient<commonsqlquery, commonsqlquery>();
            //services.AddTransient<calender, calender>();
            services.AddTransient<CommonTaskMethods, CommonTaskMethods>();

            services.AddTransient<IAdmissionStudentCounselorRepository, AdmissionStudentCounselorRepository>();

            services.AddTransient<IGrievanceRepository, GrievanceRepository>();
            services.AddTransient<IToDoRepository, TodoRepository>();

            services.AddTransient<IPaymentGatewayRepository, PaymentGatewayRepositoryImpl>();
            services.AddTransient<IAdmissionStudentKitRepository, AdmissionStudentKitRepository>();

            services.AddTransient<IParentRepository, ParentRepository>();
            services.AddTransient<IAdmissionDocumentAssignRepository, AdmissionDocumentAssignRepository>();
            services.AddTransient<IAdmissionStudentDocumentRepository, 
                AdmissionStudentDocumentRepository>();

            services.AddTransient<IAdmissionDocumentRepository,
               AdmissionDocumentRepository>();

            services.AddTransient<IAdmissionExamDateAssignRepository,
               AdmissionExamDateAssignRepository>();

            services.AddTransient<IAdmissionDocumentVerificationAssignRepository,
               AdmissionDocumentVerificationAssignRepository>();

            services.AddTransient<AdmissionSqlQuery,
            AdmissionSqlQuery>();


            services.AddTransient<IAdmissionStandardExamRepository,
             AdmissionStandardExamRepository>();

            services.AddTransient<IAdmissionPersonalInterviewAssignRepository,
             AdmissionPersonalInterviewAssignRepository>();


            services.AddTransient<GrievanceModalClass, GrievanceModalClass>();
            services.AddTransient<smssend, smssend>();
            services.AddTransient<ISupportRepository, SupportRepository>();
            services.AddTransient<ITestRepository, TestRepository>();
            //   services.AddTransient<IErpBugRepository, ErpBugRepository>();
            services.AddTransient<IErpBugAttachmentRepository, ErpBugAttachmentRepository>();
            services.AddTransient<IDepartmentLeadRepository, DepartmentLeadRepository>();

            services.AddTransient<IErpBugRepository, ErpBugRepository>();
            services.AddTransient<IBankTypeRepository, BankTypeRepository>();
            services.AddTransient<ILogingRepository, LoginRepository>();
            services.AddTransient<IProfessionTypeRepository, ProfessionTypeRepository>();
            services.AddTransient<IParentRelationshipTypeRepository, ParentRelationshipTypeRepository>();

            // School Belq Startup
            services.AddTransient<CommonSchoolModel, CommonSchoolModel>();
            services.AddTransient<ISubjectRepository, SubjectRepository>();
            services.AddTransient<IBoardStandardRepository, BoardStandardRepository>();
            services.AddTransient<IChapterRepository, ChapterRepository>();
            services.AddTransient<IConceptRepository, ConceptRepository>();
            services.AddTransient<IAcademicSessionRepository, AcademicSessionRepository>();
            services.AddTransient<IOrganizationAcademicRepository, OrganizationAcademicRepository>();
            services.AddTransient<IAcademicStandardRepository, AcademicStandardRepository>();
            services.AddTransient<IAcademicSubjectRepository, AcademicSubjectRepository>();
            services.AddTransient<ISyallbusRepository, SyallbusRepository>();
            services.AddTransient<IAcademicChapterRepository, AcademicChapterRepository>();
            services.AddTransient<IAcademicStudentRepository, AcademicStudentRepository>();
            services.AddTransient<IAcademicSectionStudentRepository, AcademicSectionStudentRepository>();
          //  services.AddTransient<IBoardStandardStreamRepository, BoardStandardStreamRepository>();
            services.AddTransient<IAcademicSectionRepository, AcademicSectionRepository>();
           // services.AddTransient<IAcademicStandardStreamRepository, AcademicStandardStreamRepository>();
            services.AddTransient<ITeacherRepository, TeacherRepository>();
            services.AddTransient<IAcademicTeacherRepository, AcademicTeacherRepository>();
            services.AddTransient<ICalenderEngagementRepository, CalenderEngagementRepository>();
            services.AddTransient<ICalenderEngagementTypeRepository, CalenderEngagementTypeRepository>();
            services.AddTransient<IAcademicConceptRepository, AcademicConceptRepository>();
            services.AddTransient<IAcademicTeacherSubjectRepository, AcademicTeacherSubjectRepository>();
            services.AddTransient<IAcademicTeacherSubjectsTimeLineRepository, AcademicTeacherSubjectsTimeLineRepository>();
            services.AddTransient<IClassTimingRepository, ClassTimingRepository>();

            services.AddTransient<IAcademicCalenderRepository, AcademicCalenderRepository>();
            services.AddTransient<IAcademicTimeTableRepository, AcademicTimeTableRepository>();
            services.AddTransient<IAcademicSyllabusRepository, AcademicSyllabusRepository>();
            services.AddTransient<ITimeTablePeriodRepository, TimeTablePeriodRepository>();
            services.AddTransient<IPeriodEmployeeRepository, PeriodEmployeeRepository>();
            services.AddTransient<IPeriodEmployeeStatusRepository, PeriodEmployeeStatusRepository>();
            services.AddTransient<IPeriodSyllabusRepository, PeriodSyllabusRepository>();

            services.AddTransient<StudentCommonMethod, StudentCommonMethod>();

            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IFeesTypeRepository, FeesTypeRepository>();
            services.AddTransient<IScholarshipApplicableRepository, ScholarshipApplicableRepository>();

            services.AddTransient<IFeesTypeApplicableRepository, FeesTypeApplicableRepository>();
            services.AddTransient<IFeesTypePrice, FeesTypePriceRepository>();

            services.AddTransient<ISessionFeesType, OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository.SessionFessTypeRepository>();

            services.AddTransient<ITeacherTimeLineRepository, TeacherTimeLineRepository>();
            services.AddTransient<IAcademicTeacherTimeLineRepository, AcademicTeacherTimeLineRepository>();
            
            services.AddTransient<IAdmissionVerifierRepository, AdmissionVerifierRepository>();
            services.AddTransient<IAdmissionInterViewerRepository, AdmissionInterViewerRepository>();

            services.AddTransient<IMessagesContentRepository, MessagesContentRepository>();

            services.AddTransient<IDataTimelineRepository, DataTimelineRepository>();


            services.AddTransient<IMessagesContentRepository, MessagesContentRepository>();

            //Scholarship

            services.AddTransient<IMessagesContentRepository, MessagesContentRepository>();
            //Scholarship

            services.AddTransient<IMessagesContentRepository, MessagesContentRepository>();
            services.AddTransient<IScholarshipRepository, ScholarshipRepository>();
            services.AddTransient<ISessionScholarship, SessionScholarshipRepository>();
            services.AddTransient<ISessionScholarshipDiscountRepository, SessionScholarshipDiscountRepository>();
            services.AddTransient<ISessionScholarshipDiscountPriceRepository, SessionScholarshipDiscountPriceRepository>();
            services.AddTransient<ISessionScholarshipDiscountApplicableRepository, SessionScholarshipDiscountApplicableRepository>();
            services.AddTransient<IScholarshipApprovalRepoaitory, ScholarshipApprovalRepoaitory>();
            services.AddTransient<IScholarshipApplyRepository, ScholarshipApplyRepository>();

            services.AddTransient<IPaymentCollectionType, PaymentCollectionTypeRepository>();
            services.AddTransient<IPaymentType, PaymentTypeRepository>();
            services.AddTransient<IStudentPayment, StudentPaymentRepo>();
            services.AddTransient<PaymentServices, PaymentServices>();
            services.AddTransient<PaymentManager, PaymentManager>();
            #region Parismita
            services.AddTransient<TodoModelClass, TodoModelClass>();
            services.AddTransient<TodoSmsnMails, TodoSmsnMails>();
            services.AddTransient<TodoMethod, TodoMethod>();
            services.AddTransient<TaskServices, TaskServices>();
            services.AddTransient<AdmissionService, AdmissionService>();
            services.AddTransient<IGrievanceForwardRepository, GrievanceForwardRepository>();
            services.AddTransient<IGrievanceExtendedTimelineAttachmentRepository, GrievanceExtendedTimelineAttachmentRepository>();
            services.AddTransient<IGrievanceAccessTimelineRepository, GrievanceAccessTimelineRepository>();
            services.AddTransient<IGrievanceExtendedTimelinesRepository, GrievanceExtendedTimelineRepository>();
            services.AddTransient<IOtpMessageRepository, OtpMessageRepository>();
            services.AddTransient<SupportModelClass, SupportModelClass>();
            services.AddTransient<SupportSmsnMails, SupportSmsnMails>();
            services.AddTransient<SupportMethod, SupportMethod>();
            services.AddTransient<IContactTypeRepository, ContactTypeRepository>();
            services.AddTransient<SupportServices, SupportServices>();
            services.AddTransient<OdmErp.Web.Areas.Student.Models.StudentModelClass, OdmErp.Web.Areas.Student.Models.StudentModelClass>();
            services.AddTransient<OdmErp.Web.Areas.Student.Models.StudentMethod, OdmErp.Web.Areas.Student.Models.StudentMethod>();
            services.AddTransient<OdmErp.Web.Areas.Student.Models.StudentSmsnMails, OdmErp.Web.Areas.Student.Models.StudentSmsnMails>();
            services.AddTransient<OdmErp.Web.Areas.Student.Models.StudentSqlQuery, OdmErp.Web.Areas.Student.Models.StudentSqlQuery>();
            services.AddTransient<OdmErp.Web.Areas.Student.Models.StudentServices, OdmErp.Web.Areas.Student.Models.StudentServices>();
            services.AddTransient<IStudentLanguagesKnownRepository, StudentLanguagesKnownRepository>();
            services.AddTransient<ILanguageRepository, LanguageRepository>();
            services.AddTransient<IBoardStandardWingRepository, BoardStandardWingRepository>();
            services.AddTransient<IWingRepository, WingRepository>();
            services.AddTransient<ISubjectGroupRepository, SubjectGroupRepository>();
            services.AddTransient<ISubjectGroupDetailsRepository, SubjectGroupDetailsRepository>();
            services.AddTransient<ISubjectOptionalTypeRepository, SubjectOptionalTypeRepository>();
            services.AddTransient<ISubjectOptionalCategoryRepository, SubjectOptionalCategoryRepository>();
            services.AddTransient<IAcademicSubjectOptionalRepository, AcademicSubjectOptionalRepository>();
            services.AddTransient<ISubjectOptionalRepository, SubjectOptionalRepository>();
            services.AddTransient<IStudentEmergencyContactRepository, StudentEmergencyContactRepository>();
            services.AddTransient<IVehicleTypeRepository, VehicleTypeRepository>();
            services.AddTransient<IVehicleRepository, VehicleRepository>();
            services.AddTransient<IVehicleLocationRepository, VehicleLocationRepository>();
            services.AddTransient<IDepartureTimeRepository, DepartureTimeRepository>();
            services.AddTransient<IAcademicSubjectGroupRepository, AcademicSubjectGroupRepository>();
            services.AddTransient<IAcademicStandardWingRepository, AcademicStandardWingRepository>();
            services.AddTransient<IAcademicSubjectOptionalDetailsRepository, AcademicSubjectOptionalDetailsRepository>();
            services.AddTransient<IAcademicStudentSubjectOptionalRepository, AcademicStudentSubjectOptionalRepository>();
            services.AddTransient<IStudentTransportationRepository, StudentTransportationRepository>();
            services.AddTransient<IAcademicStandardSettingsRepository, AcademicStandardSettingsRepository>();
            services.AddTransient<TimeTableMethod, TimeTableMethod>();
            services.AddTransient<TimeTableSQLQuery, TimeTableSQLQuery>();
            services.AddTransient<TimetableModel, TimetableModel>();

            #endregion

            #region------------------------------notice and Home work--------------------------------
            services.AddTransient<IUserGroupRepository, UserGroupRepository>();
            services.AddTransient<IUserGroupTimeLineRepository, UserGroupTimeLineRepository>();
            services.AddTransient<IUserGroupEmployeeRepository, UserGroupEmployeeRepository>();
            services.AddTransient<IUserGroupEmployeeTimeLineRepository, UserGroupEmployeeTimeLineRepository>();
            services.AddTransient<INoticeRepository, NoticeRepository>();
            services.AddTransient<INoticeDataRepository, NoticeDataRepository>();
            //services.AddTransient<IAcademicHomeworkRepository, AcademicHomeworkRepository>();
            //services.AddTransient<IAcademicHomeworkAttachmentRepository, AcademicHomeworkAttachmentRepository>();


            #endregion
            #region--------------------------Admission------------------------------
            services.AddTransient<IContactTypeRepository, ContactTypeRepository>();
            services.AddTransient<AdmissionStudentCommonMethod, AdmissionStudentCommonMethod>();
            services.AddTransient<IAdmissionRepository, AdmissionRepository>();
            services.AddTransient<IAdmissionStandardSeatQuotaRepository, AdmissionStandardSeatQuotaRepository>();
            services.AddTransient<IAdmissionStudentRepository, AdmissionStudentRepository>();
            services.AddTransient<IAdmissionStudentAcademicRepository, AdmissionStudentAcademicRepository>();
            services.AddTransient<IAdmissionStudentAddressRepository, AdmissionStudentAddressRepository>();
            services.AddTransient<IAdmissionStudentContactRepository, AdmissionStudentContactRepository>();
            services.AddTransient<IAdmissionStudentCounselorRepository,AdmissionStudentCounselorRepository>();
            services.AddTransient<IAdmissionStudentDocumentRepository, AdmissionStudentDocumentRepository>();
            services.AddTransient<IAdmissionStudentExamRepository, AdmissionStudentExamRepository>();
            services.AddTransient<IAdmissionStudentInterviewRepository,AdmissionStudentInterviewRepository>();
            services.AddTransient<IAdmissionStudentLanguageRepository,AdmissionStudentLanguageRepository>();
            services.AddTransient<IAdmissionStudentParentRepository,AdmissionStudentParentRepository>();
            services.AddTransient<IAdmissionStudentProficiencyRepository,AdmissionStudentProficiencyRepository>();
            services.AddTransient<IAdmissionStudentReferenceRepository,AdmissionStudentReferenceRepository>();
            services.AddTransient<IAdmissionStudentStandardRepository,AdmissionStudentStandardRepository>();
            services.AddTransient<IAdmissionStudentTransportRepository,AdmissionStudentTransportRepository>();
            services.AddTransient<IAdmissionFormPaymentRepository, AdmissionFormPaymentRepository>();
            services.AddTransient<StudentAdmissionMethod, StudentAdmissionMethod>();
            services.AddTransient<IAdmissionStandardExamRepository, AdmissionStandardExamRepository>();
            services.AddTransient<IAdmissionStandardExamDateRepository, AdmissionStandardExamDateRepository>();
            services.AddTransient<IMapClassToAdmissionRepository, MapClassToAdmissionRepository>();
            services.AddTransient<IAdmissionStandardExamResultDateRepository, AdmissionStandardExamResultDateRepository>();
            services.AddTransient<IAdmissionDocumentRepository, AdmissionDocumentRepository>();
            services.AddTransient<IAdmissionPersonalInterviewAssignRepository, AdmissionPersonalInterviewAssignRepository>();
            services.AddTransient<IAdmissionDocumentVerificationAssignRepository, AdmissionDocumentVerificationAssignRepository>();

            services.AddTransient<ICounsellorStatusRepository, CounsellorStatusRepository>();
            services.AddTransient<IAdmissionFeeRepository, AdmissionFeeRepository>();

            services.AddTransient<IAdmitcardVenueRepository, AdmitcardVenueRepository>();
            services.AddTransient<IAdmitcardDetailsRepository, AdmitcardDetailsRepository>();
            services.AddTransient<IAdmitcardInstructionsRepository, AdmitcardInstructionsRepository>();
            services.AddTransient<IAdmitcardVenueAddressRepository, AdmitcardVenueAddressRepository>();

            services.AddTransient<AdmissionSmsnMails, AdmissionSmsnMails>();
            services.AddTransient<IMasterEmailRepository, MasterEmailRepository>();
            services.AddTransient<IMasterSMSRepository, MasterSMSRepository>();
            #endregion-----------------------------------------------------------

            #region--------------Admissio Ecounselling And Campus Tour--------
            services.AddTransient<IAdmissionEcounsellingRepository, AdmissionEcounsellingRepository>();
            services.AddTransient<IAdmissionEcounsellingDateRepository, AdmissionEcounsellingDateRepository>();
            services.AddTransient<IAdmissionEcounsellingSlotRepository, AdmissionEcounsellingSlotRepository>();
            services.AddTransient<IAdmissionEcounsellingTimelineRepository, AdmissionEcounsellingTimelineRepository>();

            services.AddTransient<IAdmissionCampusTourRepository, AdmissionCampusTourRepository>();
            services.AddTransient<IAdmissionCampusTourDateRepository, AdmissionCampusTourDateRepository>();
            services.AddTransient<IAdmissionCampusTourSlotRepository, AdmissionCampusTourSlotRepository>();
            services.AddTransient<IAdmissionCampusTourTimelineRepository, AdmissionCampusTourTimelineRepository>();
            #endregion

            /* =============================Repository & Services==================================== */

            /* =============================Session==================================== */
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);//You can set Time
                options.Cookie.HttpOnly = true;

            });
            services.AddMvc().AddJsonOptions(opt =>
    opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/";
                options.SlidingExpiration = true;
            });
            /* =============================Session==================================== */

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            /* =============================Connection String==================================== */
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DevConnection"),
                    assembly => assembly.MigrationsAssembly("Infrastructure"));
            });
            /* =============================Connection String==================================== */
           
            services.AddSingleton<IFileProvider>(new PhysicalFileProvider(Path.Combine("", hostingEnvironment.WebRootPath)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Accounts/EmployeeLogin");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            //app.UseCookiePolicy();

            /* =============================Session==================================== */
            app.UseSession();
            /* =============================Session==================================== */
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers();
            //});
            app.UseMvc(routes =>
            {
                //routes.MapRoute(
                // name: "api",
                // template: "api/{controller}/{action}/{id?}");

                //routes.MapRoute(
                //    name: "areas",
                //    template: "{area}/{controller}/{action}/{id?}");


                routes.MapRoute(
                    name: "areas",
                    template: "{area}/{controller}/{action}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Accounts}/{action=EmployeeLogin}/{id?}");
            });
        }
    }
    public class CustomAssemblyLoadContext : AssemblyLoadContext
    {
        public IntPtr LoadUnmanagedLibrary(string absolutePath)
        {
            return LoadUnmanagedDll(absolutePath);
        }
        protected override IntPtr LoadUnmanagedDll(String unmanagedDllName)
        {
            return LoadUnmanagedDllFromPath(unmanagedDllName);
        }
        protected override Assembly Load(AssemblyName assemblyName)
        {
            throw new NotImplementedException();
        }
    }
}
