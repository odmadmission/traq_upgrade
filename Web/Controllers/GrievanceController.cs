﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Operations;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Entities.GrievanceAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Web.Controllers;
using OdmErp.Web.DTO;
using OdmErp.Web.Models;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using static OdmErp.Web.Models.CommonMethods;

namespace Web.Controllers
{
    //[Area("Grievance")]
    public class GrievanceController : Controller
    { 
        private IGrievanceRepository grievanceRepository;
        private IEmployeeRepository employeeRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IParentRepository parentRepository;
        private IRoleRepository roleRepository;
        public CommonMethods commonMethods;
        public smssend smssend;
        private IDepartmentRepository departmentRepository;
        private IDesignationRepository designationRepository;
        private IOrganizationRepository organisationRepository;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        public commonsqlquery commonsqlquery;
        public GrievanceModalClass grievancemodalclass;
        public IGrievanceForwardRepository grievanceForwardRepository;
        public IGrievanceExtendedTimelinesRepository grievanceExtendedTimelinesRepository;
        public IGrievanceAccessTimelineRepository grievanceAccessTimelineRepository;
        public IGrievanceExtendedTimelineAttachmentRepository grievanceExtendedTimelineAttachmentRepository;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly IDataTimelineRepository dataTimelineRepository;
        private readonly IFileProvider fileprovider;
        private readonly ApplicationDbContext _DbContext;

        long userId = 0;
        public GrievanceController(IGrievanceExtendedTimelineAttachmentRepository grievanceExtendedTimelineAttachmentRepo, IGrievanceAccessTimelineRepository grievanceAccessTimelineRepo, IGrievanceExtendedTimelinesRepository grievanceExtendedTimelinesRepo, IGrievanceForwardRepository grievanceForwardRepo, IFileProvider filepro, IHostingEnvironment hostingEnv, IGrievanceRepository grievanceReposito, IEmployeeRepository empRepository, IStudentAggregateRepository studentAggregate, IDataTimelineRepository dataTimelineRepository,
            IParentRepository parentReposito, CommonMethods commonMetho, IRoleRepository roleRepo, GrievanceModalClass grievancemodal, smssend sms, commonsqlquery commonsql, IDepartmentRepository departmentReposito, IDesignationRepository designationReposito, IOrganizationRepository organisationReposito, IEmployeeDesignationRepository employeeDesignationReposito, ApplicationDbContext dbContext)
        {
            grievanceExtendedTimelineAttachmentRepository = grievanceExtendedTimelineAttachmentRepo;
            grievanceAccessTimelineRepository = grievanceAccessTimelineRepo;
            grievanceExtendedTimelinesRepository = grievanceExtendedTimelinesRepo;
            grievanceForwardRepository = grievanceForwardRepo;
            commonsqlquery = commonsql;
            fileprovider = filepro;
            hostingEnvironment = hostingEnv;
            smssend = sms;
            grievanceRepository = grievanceReposito;
            employeeRepository = empRepository;
            studentAggregateRepository = studentAggregate;
            parentRepository = parentReposito;
            commonMethods = commonMetho;
            roleRepository = roleRepo;
            grievancemodalclass = grievancemodal;
            departmentRepository = departmentReposito;
            designationRepository = designationReposito;
            organisationRepository = organisationReposito;
            employeeDesignationRepository = employeeDesignationReposito;
            _DbContext = dbContext;
            this.dataTimelineRepository = dataTimelineRepository;
        }


        public IActionResult Index()
        {
            return View();
        }

        #region Grievance Priority

        //GrievancTypes List 
        public IActionResult GrievancePriority()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Grievance Priority", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(grievanceRepository.GetAllGrievancePriority());
        }

        public IActionResult CreateOrEditGrievancePriority(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Grievance Priority", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = ViewMethod.CREATE;
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Grievance Priority", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = ViewMethod.UPDATE;
              
                var grievancePriority = grievanceRepository.GetGrievancePriorityById(id.Value);
                od.ID = grievancePriority.ID;
                od.Name = grievancePriority.Name;
            }
            return View(od);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateGrievancePriority([Bind("ID,Name")] GrievancePriority grievancePriority)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                grievancePriority.ModifiedId = userId;
                grievancePriority.Active = true;
                if (grievancePriority.ID == 0)
                {
                    grievancePriority.InsertedId = userId;
                    grievancePriority.Status = EntityStatus.ACTIVE;
                    grievanceRepository.CreateGrievancePriority(grievancePriority);
                }
                else
                {
                    grievanceRepository.UpdateGrievancePriority(grievancePriority);
                }

                return RedirectToAction(nameof(GrievancePriority));
            }
            return View(grievancePriority);
        }


        // POST: GrievanceTypes/Delete/5
        [ActionName("DeleteGrievancePriority")]
        public async Task<IActionResult> GrievancePriorityDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Grievance Priority", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (GrievancePriorityExists(id.Value) == true)
            {
                var country = grievanceRepository.DeleteGrievancePriority(id.Value);
            }
            return RedirectToAction(nameof(GrievancePriority));
        }

        private bool GrievancePriorityExists(long id)
        {
            if (grievanceRepository.GetGrievancePriorityById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Grievance Access

        public IActionResult GrievanceAccess()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Grievance Access", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var grivAccess = grievanceRepository.GrievanceAccess();
            var grivType = grievanceRepository.GetAllGrievanceCategory();
            var grivSubType = grievanceRepository.GetAllGrievanceType();
            var employee = employeeRepository.GetAllEmployee();
            var accessTimeline = grievanceAccessTimelineRepository.ListAllAsyncIncludeAll().Result;

            if (grivAccess != null)
            {
                var res = (from a in grivAccess
                           join b in grivType on a.GrievanceCategoryId equals b.ID
                           //join c in grivSubType on a.GrievanceTypeId equals c.ID
                           join d in employee on a.EmployeeId equals d.ID
                           join f in accessTimeline on a.ID equals f.GrievanceAccessId
                           select new GrievanceAccessModelClass
                           {
                               ID = a.ID,
                               InsertedDate = a.InsertedDate,
                               GrievanceCategoryId = b.ID,
                               GrievanceCategoryName = b.Name,
                               GrievanceTypeId = a.GrievanceTypeId == 0 ? 0 : (grivSubType != null ? grivSubType.Where(s => s.ID == a.GrievanceTypeId).FirstOrDefault().ID : 0),
                               GrievanceTypeName = a.GrievanceTypeId == 0 ? "" : (grivSubType != null ? grivSubType.Where(s => s.ID == a.GrievanceTypeId).FirstOrDefault().Name : "NA"),
                               EmployeeId = d.ID,
                               EmployeeName = d.EmpCode + " - " + d.FirstName + " " + d.LastName,
                               StartDate = f.StartDate,
                               EndDate = f.EndDate,
                               Active = a.Active,
                               Status = a.Status
                           }).ToList();

                return View(res);
            }
            else
            {
                return View(null);
            }
        }
        public IActionResult CreatOrEditGrievanceAccess(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            GrievanceAccessModelClass access = new GrievanceAccessModelClass();
            access.GrievType = grievanceRepository.GetAllGrievanceCategory();
            access.GrivSubType = grievanceRepository.GetAllGrievanceType();
            ViewBag.Employees = employeeRepository.GetAllEmployee().Select(a => new { ID = a.ID, Name = a.FirstName + " " + a.LastName });

            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Grievance Access", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = ViewMethod.CREATE;
                access.ID = 0;
                access.GrievanceCategoryId = 0;
                access.GrievanceTypeId = 0;
            }
            else
            {
                if (commonMethods.checkaccessavailable("Grievance Access", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = ViewMethod.UPDATE;
                var grievanceAccess = grievanceRepository.GetGrievanceAccessById(id.Value);
                access.ID = grievanceAccess.ID;
                access.GrievanceCategoryId = grievanceAccess.GrievanceCategoryId;
                access.GrievanceTypeId = grievanceAccess.GrievanceTypeId;
                access.EmployeeId = grievanceAccess.EmployeeId;
            }
            return View(access);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateGrievanceAccess([Bind("ID,GrievanceCategoryId,GrievanceTypeId,EmployeeId")] GrievanceAccess grievanceAccess)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            var res = grievanceRepository.GrievanceAccess().Where(a => a.Active == true && a.GrievanceCategoryId == grievanceAccess.GrievanceCategoryId && a.GrievanceTypeId == grievanceAccess.GrievanceTypeId && a.EmployeeId == grievanceAccess.EmployeeId).FirstOrDefault();
            if (res == null)
            {
                if (ModelState.IsValid)
                {
                    grievanceAccess.ModifiedId = userId;
                    grievanceAccess.Active = true;
                    if (grievanceAccess.ID == 0)
                    {
                        grievanceAccess.InsertedId = userId;
                        grievanceAccess.IsAvailable = true;
                        grievanceAccess.Status = EntityStatus.ACTIVE;
                        long id = grievanceRepository.CreateGrievanceAccess(grievanceAccess);

                        if (id > 0)
                        {
                            GrievanceAccessTimeline accessTimeline = new GrievanceAccessTimeline();
                            accessTimeline.InsertedDate = DateTime.Now;
                            accessTimeline.InsertedId = 1;
                            accessTimeline.ModifiedDate = DateTime.Now;
                            accessTimeline.ModifiedId = 1;
                            accessTimeline.Active = true;
                            accessTimeline.Status = "ACTIVE";
                            accessTimeline.IsAvailable = true;
                            accessTimeline.GrievanceAccessId = id;
                            accessTimeline.StartDate = DateTime.Now;
                            accessTimeline.EndDate = null;
                            await grievanceAccessTimelineRepository.AddAsync(accessTimeline);
                            // _DbContext.SaveChanges();
                        }
                    }
                    else
                    {
                        var grivAccess = grievanceRepository.GetGrievanceAccessById(grievanceAccess.ID);
                        if (grivAccess.GrievanceCategoryId == grievanceAccess.GrievanceCategoryId && grivAccess.GrievanceTypeId == grievanceAccess.GrievanceTypeId && grivAccess.EmployeeId != grievanceAccess.EmployeeId)
                        {
                            grievanceAccess.InsertedId = userId;
                            grievanceAccess.Status = EntityStatus.ACTIVE;
                            grievanceAccess.IsAvailable = true;
                            grievanceAccess.ID = 0;
                            long id = grievanceRepository.CreateGrievanceAccess(grievanceAccess);

                            if (id > 0)
                            {
                                GrievanceAccessTimeline accessTimeline = new GrievanceAccessTimeline();
                                accessTimeline.InsertedDate = DateTime.Now;
                                accessTimeline.InsertedId = 1;
                                accessTimeline.ModifiedDate = DateTime.Now;
                                accessTimeline.ModifiedId = 1;
                                accessTimeline.Active = true;
                                accessTimeline.Status = "ACTIVE";
                                accessTimeline.GrievanceAccessId = id;
                                accessTimeline.StartDate = DateTime.Now;
                                accessTimeline.EndDate = null;
                                await grievanceAccessTimelineRepository.AddAsync(accessTimeline);
                                // _DbContext.SaveChanges();
                            }
                            grivAccess.Active = false;
                            grivAccess.Status = "DELETED";
                            grievanceRepository.UpdateGrivanceAccess(grivAccess);

                            var grivTimeline = _DbContext.GrievanceAccessTimelines.Where(a => a.ID == grivAccess.ID && a.Active == true).FirstOrDefault();
                            grivTimeline.EndDate = DateTime.Now;
                            grivTimeline.Active = false;
                            grivTimeline.Status = "DELETED";
                            await grievanceAccessTimelineRepository.UpdateAsync(grivTimeline);
                            // _DbContext.SaveChanges();
                        }
                        else
                        {
                            grievanceAccess.Status = "UPDATED";
                            grievanceRepository.UpdateGrivanceAccess(grievanceAccess);
                        }

                    }

                    return RedirectToAction(nameof(GrievanceAccess));
                }
            }
            return RedirectToAction(nameof(GrievanceAccess));
        }

        public async Task<IActionResult> DeleteGrievanceAccess(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Grievance Access", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (grievanceRepository.GetGrievanceAccessById(id.Value) != null)
            {
                var country = grievanceRepository.DeleteGrievanceAccess(id.Value);
            }
            return RedirectToAction(nameof(GrievanceAccess));
        }

        public async Task<IActionResult> GrievanceAccessStatusChange(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Grievance Access", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var grivAccess = grievanceRepository.GetGrievanceAccessById(id.Value);
            if (grivAccess != null)
            {
                if (grivAccess.Active == false)
                {
                    grivAccess.Status = EntityStatus.ACTIVE;
                    grivAccess.Active = true;
                    grivAccess.ModifiedDate = DateTime.Now;
                    grivAccess.ModifiedId = userId;
                    grievanceRepository.UpdateGrivanceAccess(grivAccess);

                    if (grivAccess.ID > 0)
                    {
                        GrievanceAccessTimeline accessTimeline = grievanceAccessTimelineRepository.ListAllAsync().Result.Where(a => a.GrievanceAccessId == grivAccess.ID && a.Active == false).FirstOrDefault();

                        accessTimeline.ModifiedDate = DateTime.Now;
                        accessTimeline.ModifiedId = userId;
                        accessTimeline.Active = true;
                        accessTimeline.Status = "ACTIVE";
                        accessTimeline.StartDate = DateTime.Now;
                        accessTimeline.EndDate = null;
                        await grievanceAccessTimelineRepository.UpdateAsync(accessTimeline);
                        // _DbContext.SaveChanges();
                    }
                }
                else
                {
                    grivAccess.Status = EntityStatus.DELETED;
                    grivAccess.Active = false;
                    grivAccess.ModifiedDate = DateTime.Now;
                    grivAccess.ModifiedId = userId;
                    grievanceRepository.UpdateGrivanceAccess(grivAccess);

                    if (grivAccess.ID > 0)
                    {
                        GrievanceAccessTimeline accessTimeline = grievanceAccessTimelineRepository.ListAllAsync().Result.Where(a => a.GrievanceAccessId == grivAccess.ID && a.Active == true).FirstOrDefault();

                        accessTimeline.ModifiedDate = DateTime.Now;
                        accessTimeline.ModifiedId = userId;
                        accessTimeline.Active = false;
                        accessTimeline.Status = "DELETED";
                        accessTimeline.EndDate = DateTime.Now;
                        await grievanceAccessTimelineRepository.UpdateAsync(accessTimeline);
                        // _DbContext.SaveChanges();
                    }
                }

            }
            return RedirectToAction(nameof(GrievanceAccess));
        }

        #endregion

        #region Grievance Bulk

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public IActionResult DownloadGrievanceAccess()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            //if (commonMethods.checkaccessavailable("Raise", accessId, "Download", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            //moduledt.Columns.Add("Sl No"); 
            moduledt.Columns.Add("Grievance Type");
            moduledt.Columns.Add("Grievance SubType");
            moduledt.Columns.Add("Authorized Person");
            moduledt.Columns.Add("Status");
            // moduledt.Columns.Add("Modified On");

            var grivAccess = grievanceRepository.GrievanceAccess().ToList();
            var grivType = grievanceRepository.GetAllGrievanceCategory();
            var grivSubType = grievanceRepository.GetAllGrievanceType();
            var empList = employeeRepository.GetAllEmployee();
            var result = (from a in grivAccess
                          join b in grivType on a.GrievanceCategoryId equals b.ID
                          join c in grivSubType on a.GrievanceTypeId equals c.ID
                          join d in empList on a.EmployeeId equals d.ID
                          select a).ToList();
            int i = 0;
            foreach (var a in result)
            {
                DataRow dr = moduledt.NewRow();
                // dr["Sl No"] = ++i;               
                dr["Grievance Type"] = grivType.Where(m => m.ID == a.GrievanceCategoryId).FirstOrDefault().Name;
                dr["Grievance SubType"] = grivSubType.Where(m => m.ID == a.GrievanceTypeId).FirstOrDefault().Name;
                dr["Authorized Person"] = GetEmployeeNameByEmpId(a.EmployeeId);
                dr["Status"] = a.IsAvailable == true ? "ACTIVE"  : "INACTIVE";
                // dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "GrievanceAccess";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"GrievanceAccess.xlsx");
        }
        public IActionResult DownloadGrievanceAccessSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;

            XLWorkbook oWB = new XLWorkbook();
            var wsData = oWB.Worksheets.Add("Data");
            var grivanceType = grievanceRepository.GetAllGrievanceCategory();
            var grivanceSubType = grievanceRepository.GetAllGrievanceType();
            wsData.Cell(1, 1).Value = "GrievanceType";
            int i = 1;
            foreach (var a in grivanceType)
            {
                wsData.Cell(++i, 1).Value = a.Name;
            }
            wsData.Range("A2:A" + i).AddToNamed("GrievanceType");
            wsData.Cell(1, 2).Value = "GrievanceSubType";
            i = 1;
            foreach (var a in grivanceSubType)
            {
                wsData.Cell(++i, 2).Value = a.Name;
            }
            wsData.Range("B2:B" + i).AddToNamed("GrievanceSubType");

            var empList = GetAllEmployeeName();
            wsData.Cell(1, 3).Value = "Employee";
            i = 1;
            foreach (var a in empList)
            {
                wsData.Cell(++i, 3).Value = a.FullName;
            }
            wsData.Range("C2:C" + i).AddToNamed("Employee");

            //DataTable grivdt = new DataTable();
            //grivdt.Columns.Add("GrievanceType");
            //var grivanceType = grievanceRepository.GetAllGrievanceCategory();
            //foreach (var a in grivanceType)
            //{
            //    DataRow dr = grivdt.NewRow();
            //    dr["GrievanceType"] = a.Name;
            //    grivdt.Rows.Add(dr);
            //}
            //grivdt.TableName = "GrievanceType";

            //grivdt.Columns.Add("GrievanceSubType");
            //var grivanceSubType = grievanceRepository.GetAllGrievanceType();
            //foreach (var a in grivanceSubType)
            //{
            //    DataRow dr = grivdt.NewRow();
            //    dr["GrievanceSubType"] = a.Name;
            //    grivdt.Rows.Add(dr);
            //}
            //grivdt.TableName = "GrievanceSubType";

            //grivdt.Columns.Add("Employee");
            //var empList = GetAllEmployeeName();
            //foreach (var a in empList)
            //{
            //    DataRow dr = grivdt.NewRow();
            //    dr["Employee"] = a;
            //    grivdt.Rows.Add(dr);
            //}
            //grivdt.TableName = "Employee";

            //int lastCellNo1 = grivdt.Rows.Count + 1;
            ////ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            //oWB.AddWorksheet(grivdt);
            //var worksheet1 = oWB.Worksheet(1);

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("GrievanceType");
            validationTable.Columns.Add("GrievanceSubType");
            validationTable.Columns.Add("Employee");

            validationTable.TableName = "GrievanceAccess_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(wsData.Range("GrievanceType"), true);
            worksheet.Column(2).SetDataValidation().List(wsData.Range("GrievanceSubType"), true);
            worksheet.Column(3).SetDataValidation().List(wsData.Range("Employee"), true);

            wsData.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"GrievanceAccessSample.xlsx");

        }
        public IActionResult UploadGrievanceAccess(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var grivType = grievanceRepository.GetAllGrievanceCategory();
                    var grivSubType = grievanceRepository.GetAllGrievanceType();
                    var empList = GetAllEmployeeName();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        GrievanceAccess grievanceAccess = new GrievanceAccess();
                        string grivTypeName = firstWorksheet.Cells[i, 1].Value.ToString();
                        string grivSubTypeName = firstWorksheet.Cells[i, 2].Value.ToString();
                        string empName = firstWorksheet.Cells[i, 3].Value.ToString();

                        long grivTypeId = grivType.Where(a => a.Name == grivTypeName).FirstOrDefault().ID;
                        long grivSubTypeId = grivSubType.Where(a => a.Name == grivSubTypeName).FirstOrDefault().ID;
                        long empId = empList.Where(a => a.FullName == empName).FirstOrDefault().ID;

                        if (grievanceRepository.GrievanceAccess().Where(s => s.GrievanceCategoryId == grivTypeId && s.GrievanceTypeId == grivSubTypeId && s.EmployeeId == empId).FirstOrDefault() == null)
                        {
                            grievanceAccess.GrievanceCategoryId = grivTypeId;
                            grievanceAccess.GrievanceTypeId = grivSubTypeId;
                            grievanceAccess.EmployeeId = empId;
                            grievanceAccess.Active = true;
                            grievanceAccess.ModifiedId = userId;
                            grievanceAccess.Status = EntityStatus.ACTIVE;
                            grievanceAccess.InsertedDate = DateTime.Now;
                            grievanceAccess.InsertedId = userId;
                            grievanceAccess.ModifiedDate = DateTime.Now;
                            grievanceAccess.ModifiedId = userId;

                            grievanceRepository.CreateGrievanceAccess(grievanceAccess);
                            //_DbContext.SaveChanges();
                        }
                    }
                }
            }
            return RedirectToAction(nameof(GrievanceAccess));
        }
        public string GetEmployeeNameByEmpId(long id)
        {
            var emp = employeeRepository.GetEmployeeById(id);
            var FullName = emp.FirstName + " " + emp.LastName;
            return FullName;
        }
        public class Empclass
        {
            public long ID { get; set; }
            public string FullName { get; set; }
        }
        public List<Empclass> GetAllEmployeeName()
        {
            var emp = employeeRepository.GetAllEmployee().Select(a => new Empclass { ID = a.ID, FullName = a.FirstName + " " + a.LastName }).ToList();
            return emp;
        }
        public IActionResult DownloadGrievanceSubTypeSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;

            XLWorkbook oWB = new XLWorkbook();

            DataTable grivdt = new DataTable();
            grivdt.Columns.Add("Name");
            var grivanceType = grievanceRepository.GetAllGrievanceCategory();
            foreach (var a in grivanceType)
            {
                DataRow dr = grivdt.NewRow();
                dr["Name"] = a.Name;
                grivdt.Rows.Add(dr);
            }
            grivdt.TableName = "GrievanceSubType";


            int lastCellNo1 = grivdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(grivdt);
            var worksheet1 = oWB.Worksheet(1);
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("GrievanceType");
            validationTable.Columns.Add("Name");
            validationTable.TableName = "GrievanceSubType_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"GrievanceSubTypeSample.xlsx");
        }
        public IActionResult UploadGrievanceSubType(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var grivType = grievanceRepository.GetAllGrievanceCategory();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        GrievanceType grievanceType = new GrievanceType();

                        var type = firstWorksheet.Cells[i, 1].Value;
                        var subType = firstWorksheet.Cells[i, 2].Value;

                        string grivTypeName = type.ToString();
                        string grivSubTypeName = subType != null ? subType.ToString() : null;

                        long grivTypeId = grivType.Where(a => a.Name == grivTypeName).FirstOrDefault().ID;

                        if (grievanceRepository.GetAllGrievanceType().Where(s => s.Name == grivSubTypeName && s.CategoryId == grivTypeId).FirstOrDefault() == null)
                        {
                            grievanceType.Name = grivSubTypeName;
                            grievanceType.CategoryId = grivTypeId;
                            grievanceType.Active = true;
                            grievanceType.ModifiedId = userId;
                            grievanceType.Status = EntityStatus.ACTIVE;

                            grievanceRepository.CreateGrievanceType(grievanceType);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(GrievanceSubType));
        }
        public IActionResult DownloadGrievanceSubType()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            //if (commonMethods.checkaccessavailable("Raise", accessId, "Download", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            //moduledt.Columns.Add("Sl No");          
            moduledt.Columns.Add("Name");
            moduledt.Columns.Add("Grievance Type");
            //moduledt.Columns.Add("Availability");
            // moduledt.Columns.Add("Modified On");

            var grivType = grievanceRepository.GetAllGrievanceCategory();
            var grivSubType = grievanceRepository.GetAllGrievanceType();

            var result = (from a in grivSubType
                          join b in grivType on a.CategoryId equals b.ID
                          select a).ToList();
            int i = 0;
            foreach (var a in result)
            {
                DataRow dr = moduledt.NewRow();
                // dr["Sl No"] = ++i;               
                dr["Name"] = a.Name;
                dr["Grievance Type"] = grivType.Where(m => m.ID == a.CategoryId).FirstOrDefault().Name;
                //dr["Availability"] = a.IsAvailable;
                // dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "GrievanceSubType";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"GrievanceSubType.xlsx");
        }

        public IActionResult DownloadGrievanceTypeSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;

            XLWorkbook oWB = new XLWorkbook();

            DataTable grivdt = new DataTable();
            grivdt.Columns.Add("Name");
            //var support = grievanceRepository.GetAllGrievanceCategory();
            //foreach (var a in support)
            //{
            //    DataRow dr = grivdt.NewRow();
            //    dr["Name"] = a.Name;
            //    grivdt.Rows.Add(dr);
            //}
            grivdt.TableName = "GrievanceType";


            int lastCellNo1 = grivdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(grivdt);
            var worksheet1 = oWB.Worksheet(1);
            DataTable validationTable = new DataTable();
            // validationTable.Columns.Add("GrievanceType");
            validationTable.Columns.Add("Name");
            validationTable.TableName = "GrievanceType_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            //worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            //worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"GrievanceTypeSample.xlsx");
        }
        public IActionResult UploadGrievanceType(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var grivType = grievanceRepository.GetAllGrievanceCategory();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        GrievanceCategory category = new GrievanceCategory();
                        string grievancetypename = firstWorksheet.Cells[i, 1].Value.ToString();
                        //string supporttypename = firstWorksheet.Cells[i, 2].Value.ToString();

                        var grievanceTypeId = grievanceRepository.GetAllGrievanceCategory().Where(m => m.Name == grievancetypename).FirstOrDefault();

                        if (grievanceTypeId == null)
                        {
                            category.InsertedId = userId;
                            category.Status = EntityStatus.ACTIVE;
                            category.Active = true;
                            category.ModifiedId = userId;
                            category.Name = grievancetypename;
                            grievanceRepository.CreateGrievanceCategory(category);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(GrievanceType));
        }
        public IActionResult DownloadGrievanceType()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            //if (commonMethods.checkaccessavailable("Raise", accessId, "Download", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            //moduledt.Columns.Add("Sl No");
            moduledt.Columns.Add("Grievance Type");
            //moduledt.Columns.Add("Availability");
            // moduledt.Columns.Add("Modified On");

            var grivType = grievanceRepository.GetAllGrievanceCategory();
            int i = 0;
            foreach (var a in grivType)
            {
                DataRow dr = moduledt.NewRow();
                //  dr["Sl No"] = ++i;
                dr["Grievance Type"] = a.Name;
                //dr["Availability"] = a.Active;
                //  dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "GrievaceType";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"GrievanceType.xlsx");
        }

        #endregion

        #region Grievance SubType

        //GrievancTypes List 
        public IActionResult GrievanceSubType()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Grievance Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var types = grievanceRepository.GetAllGrievanceType();
            var grivtType = grievanceRepository.GetAllGrievanceCategory();
            if (types == null)
            {
                return View(null);
            }
            else
            {
                IEnumerable<GrievanceTypeClass> res = (from a in types
                                                       join b in grivtType on a.CategoryId equals b.ID
                                                       select new GrievanceTypeClass
                                                       {
                                                           ID = a.ID,
                                                           Name = a.Name,
                                                           InsertedDate = a.InsertedDate,
                                                           CategoryId = b.ID,
                                                           GrievanceName = b.Name,
                                                           ModifiedDate = a.ModifiedDate
                                                       }).ToList();
                return View(res);
            }
        }

        public IActionResult CreateOrEditGrievanceSubType(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            GrievanceTypeClass od = new GrievanceTypeClass();
            od.Types = grievanceRepository.GetAllGrievanceCategory();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Grievance Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = ViewMethod.CREATE;
                od.ID = 0;
                od.Name = "";
                od.RoleID = 0;
            }
            else
            {
                if (commonMethods.checkaccessavailable("Grievance Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = ViewMethod.UPDATE;
                var grievanceType = grievanceRepository.GetGrievanceTypeById(id.Value);
                od.ID = grievanceType.ID;
                od.Name = grievanceType.Name;
                od.CategoryId = grievanceType.CategoryId;
                //od.RoleID = grievanceType.RoleID;
            }
            return View(od);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateGrievanceSubType([Bind("ID,Name,CategoryId")] GrievanceType grievanceType)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                var res = grievanceRepository.GetAllGrievanceType();
                grievanceType.ModifiedId = userId;
                grievanceType.Active = true;
                if (grievanceType.ID == 0)
                {
                    var duplicate = res == null ? null : res.Where(a => a.Active == true && a.Name.ToLower() == grievanceType.Name.ToLower() && a.CategoryId == grievanceType.CategoryId).ToList();
                    if (duplicate.Count() == 0)
                    {
                        grievanceType.InsertedId = userId;
                        grievanceType.Status = EntityStatus.ACTIVE;
                        grievanceRepository.CreateGrievanceType(grievanceType);


                    }
                    else
                    {
                        grievanceRepository.UpdateGrievanceType(grievanceType);
                    }
                    return RedirectToAction(nameof(GrievanceSubType));
                }
                else
                {
                    var duplicate = grievanceRepository.GetAllGrievanceType().Where(a => a.Active == true && a.Name.ToLower() == grievanceType.Name.ToLower() && a.ID != grievanceType.ID && a.CategoryId == grievanceType.CategoryId).ToList();
                    if (duplicate.Count() == 0)
                    {
                        grievanceRepository.UpdateGrievanceType(grievanceType);
                    }
                    return RedirectToAction(nameof(GrievanceSubType));
                }
            }

            return View(grievanceType);
        }


        // POST: GrievanceTypes/Delete/5
        [ActionName("DeleteGrievanceSubType")]
        public async Task<IActionResult> GrievanceSubTypeDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Grievance Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (GrievanceSubTypeExists(id.Value) == true)
            {
                var country = grievanceRepository.DeleteGrievanceType(id.Value);
            }
            return RedirectToAction(nameof(GrievanceSubType));
        }

        private bool GrievanceSubTypeExists(long id)
        {
            if (grievanceRepository.GetGrievanceTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region EmployeeGrievance


        //EmployeeGrievance List 
        public IActionResult EmployeeGrievance()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;


            //if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            //var greType = grievanceRepository.GetAllGrievanceType().ToList();
            var empgre = grievanceRepository.GetAllEmployeeGrievance().ToList();
            var employee = employeeRepository.GetAllEmployee().ToList();
            var greType = grievanceRepository.GetAllGrievanceType().ToList();
            var res = (from a in empgre
                       join b in employee on a.EmployeeID equals b.ID
                       join c in empgre on a.GrievanceID equals c.GrievanceID
                       join d in greType on c.GrievanceID equals d.ID
                       select new EmployeeGrievance
                       {
                           ID = a.ID,
                           EmployeeID = a.EmployeeID,
                           GrievanceID = b.ID,
                           //  Name = d.Name,
                           //  FirstName = b.FirstName
                       }).ToList();
            return View(res);

            //var res = (from a in greType
            //           join b in empgre on a.ID equals b.ID
            //           join c in employee on b.EmployeeID equals c.ID
            //           select new EmployeeGrievance
            //           {
            //               ID = a.ID,
            //               Name = a.Name,
            //               EmployeeID = b.EmployeeID,
            //               FirstName = c.FirstName,
            //               GrievanceTypeID = b.GrievanceTypeID



            //           }).ToList();


            //  return View(grievanceRepository.GetAllEmployeeGrievance());
        }




        public IActionResult CreateOrEditEmployeeGrievance(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            EmpGrievancecls od = new EmpGrievancecls();
            od.grievanceType = (from a in grievanceRepository.GetAllGrievanceType().ToList()

                                select new CommonMasterModel
                                {
                                    ID = a.ID,
                                    Name = a.Name
                                }).ToList();
            od.employees = (from a in employeeRepository.GetAllEmployee().ToList()

                            select new CommonMasterModel
                            {
                                ID = a.ID,
                                Name = a.FirstName + " " + a.LastName
                            }).ToList();


            if (id == null)
            {
                //if (commonMethods.checkaccessavailable("Educational Qualification", accessId, "Create", "Master", roleId) == false)
                //{
                //    return RedirectToAction("AuthenticationFailed", "Accounts");
                //}

                ViewBag.status = "Create";
                od.ID = 0;

                od.GrievanceTypeID = 0;
            }
            else
            {
                //if (commonMethods.checkaccessavailable("Educational Qualification", accessId, "Edit", "Master", roleId) == false)
                //{
                //    return RedirectToAction("AuthenticationFailed", "Accounts");
                //}

                ViewBag.status = "Update";
                var employeeGrievance = grievanceRepository.GetEmployeeGrievanceById(id.Value);
                od.ID = employeeGrievance.ID;

                od.GrievanceTypeID = employeeGrievance.GrievanceID;
            }



            return View(od);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeGrievance([Bind("ID,Name,GrievanceTypeID,EmployeeID")] EmployeeGrievance grievance)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                grievance.ModifiedId = userId;
                grievance.Active = true;

                if (grievance.ID == 0)
                {
                    grievance.InsertedId = userId;

                    grievance.Status = "CREATED";
                    grievanceRepository.CreateEmployeeGrievance(grievance);

                }
                else
                {
                    grievanceRepository.UpdateEmployeeGrievance(grievance);
                }

                return RedirectToAction(nameof(EmployeeGrievance));
            }
            return View(grievance);
        }

        [ActionName("DeleteEmployeeGrievance")]
        public async Task<IActionResult> EmployeeGrievanceDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("City", accessId, "Delete", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (EmployeeGrievanceExists(id.Value) == true)
            {
                var Section = grievanceRepository.DeleteEmployeeGrievance(id.Value);
            }
            return RedirectToAction(nameof(EmployeeGrievance));
        }
        private bool EmployeeGrievanceExists(long id)
        {
            if (grievanceRepository.GetEmployeeGrievanceById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Grievance Comment

        public IActionResult GrievanceCommnets(long ID)
        {
            //TempData["id"] = ID;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            var comments = grievanceRepository.GetAllGrievanceComment().Where(a => a.Active == true).ToList();
            var commentattachments = grievanceRepository.GetAllGrievanceCommentAttachment().Where(a => a.Active == true).ToList();
            //var grievances = _DbContext.Grievances.Where(a => a.Active == true).ToList();
            var commentList = (from a in comments
                               where a.GrievanceID == ID
                               select new GrievanceCommentModel
                               {
                                   ID = a.ID,
                                   InsertedId = a.InsertedId,
                                   CommentBy = grievancemodalclass.getRoleById(a.RoleId, a.InsertedId),
                                   CommentedOn = a.InsertedDate,
                                   Comment = a.Description,
                                   Profile = grievancemodalclass.getProfileById(a.RoleId, a.InsertedId),
                                   EmpId = a.InsertedId,
                                   UserId = userId,
                                   CommentedAttachments = commentattachments.Where(m => m.GrievanceCommentID == a.ID).ToList()
                                   //GrievanceStatusId = b.GrievanceStatusID
                               }).ToList();


            if (ID > 0)
            {
                ViewBag.GrievanceId = ID;
                ViewBag.GrievanceStatusId = grievanceRepository.GetGrievanceById(ID).GrievanceStatusID;

                var result = comments.Where(s => s.GrievanceID == ID).ToList();
                foreach (var k in result)
                {
                    if (k.InsertedId != userId)
                    {
                        k.IsRead = true;
                        grievanceRepository.UpdateGrievanceComment(k);
                        //_DbContext.SaveChanges();
                    }
                }
            }
            return View(commentList);
        }

        [HttpPost]
        public IActionResult SaveGrievanceComment(string comment, long grievanceid, long grievancestatusid)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            var attachfile = HttpContext.Request.Form.Files["file1"];
            GrievanceComment grivComment = new GrievanceComment();
            grivComment.InsertedId = userId;
            grivComment.ModifiedId = userId;
            grivComment.RoleId = roleId;
            grivComment.InsertedDate = DateTime.Now;
            grivComment.ModifiedDate = DateTime.Now;
            grivComment.Active = true;
            grivComment.Status = "Active";
            grivComment.IsAvailable = true;
            grivComment.GrievanceID = grievanceid;
            grivComment.Description = comment;
            grivComment.GrievanceStatusID = grievancestatusid;
            grievanceRepository.CreateGrievanceComment(grivComment);
            //_DbContext.GrievanceComments.Add(grivComment);
            //_DbContext.SaveChanges();
            var id = grivComment.ID;

            if (attachfile != null)
            {
                FileInfo fi = new FileInfo(attachfile.FileName);
                var newFilename = "Grievance" + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\GrievanceAttachment\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    attachfile.CopyTo(stream);
                } 
                GrievanceCommentAttachment attachment = new GrievanceCommentAttachment();
                attachment.Active = true;
                attachment.GrievanceCommentID = id;
                attachment.InsertedDate = DateTime.Now;
                attachment.InsertedId = userId;
                attachment.IsAvailable = true;
                attachment.ModifiedDate = DateTime.Now;
                attachment.ModifiedId = userId;
                attachment.Name = attachfile.FileName;

                attachment.Path = pathToSave;
                attachment.Status = "ACTIVE";
                grievanceRepository.CreateGrievanceCommentAttachment(attachment); 
            }
            //if (file.Count() > 0)
            //{
            //    foreach (var fil in file)
            //    {
            //        FileInfo fi = new FileInfo(fil.FileName);
            //        var newFilename = id + "_" + String.Format("{0:d}",
            //                          (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
            //        var webPath = hostingEnvironment.WebRootPath;
            //        string path = Path.Combine("", webPath + @"\ODMImages\GrievanceAttachment\" + newFilename);
            //        var pathToSave = newFilename;
            //        using (var stream = new FileStream(path, FileMode.Create))
            //        {
            //            fil.CopyToAsync(stream);
            //        }
            //        GrievanceCommentAttachment attachment = new GrievanceCommentAttachment();
            //        attachment.Active = true;
            //        attachment.GrievanceCommentID = id;
            //        attachment.InsertedDate = DateTime.Now;
            //        attachment.InsertedId = userId;
            //        attachment.IsAvailable = true;
            //        attachment.ModifiedDate = DateTime.Now;
            //        attachment.ModifiedId = userId;
            //        attachment.Name = fil.FileName;

            //        attachment.Path = newFilename;
            //        attachment.Status = "ACTIVE";
            //        grievanceRepository.CreateGrievanceCommentAttachment(attachment);
            //    }
            //}
            return RedirectToAction(nameof(GrievanceCommnets), new { ID = grievanceid });
        }

        public IActionResult RemoveComment(long grivids)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            var comment = grievanceRepository.GetGrievanceCommentById(grivids);
            if (comment != null)
            {
                comment.ModifiedDate = DateTime.Now;
                comment.ModifiedId = userId;
                comment.Active = false;
                comment.Status = "DELETED";

                grievanceRepository.UpdateGrievanceComment(comment);
                //_DbContext.SaveChanges();
            }
            return RedirectToAction(nameof(GrievanceCommnets), new { ID = comment.GrievanceID });
        }
        #endregion

        #region GrievanceInhouse Comment

        public IActionResult GrievanceInHouseCommnets(long ID)
        {
            //TempData["id"] = ID;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            var comments = grievanceRepository.GrievanceInhouseComments(ID).Where(a => a.Active == true).ToList();
            //var commentattachments = grievanceRepository.GrievanceInhouseAttachment().Where(a => a.Active == true).ToList();
            //var grievances = _DbContext.Grievances.Where(a => a.Active == true).ToList();
            var commentList = (from a in comments
                               where a.GrievanceID == ID
                               select new GrievanceCommentModel
                               {
                                   ID = a.ID,
                                   InsertedId = a.InsertedId,
                                   CommentBy = grievancemodalclass.getRoleById(a.RoleId, a.InsertedId),
                                   CommentedOn = a.InsertedDate,
                                   Comment = a.Description,
                                   Profile = grievancemodalclass.getProfileById(a.RoleId, a.InsertedId),
                                   EmpId = a.InsertedId,
                                   UserId = userId,
                                   InhouseCommentedAttachments = grievanceRepository.GrievanceInhouseAttachment(a.ID).ToList().Count() > 0 ? grievanceRepository.GrievanceInhouseAttachment(a.ID).ToList() : null,
                                   //GrievanceStatusId = b.GrievanceStatusID
                               }).ToList();


            if (ID > 0)
            {
                ViewBag.GrievanceId = ID;
                ViewBag.GrievanceStatusId = grievanceRepository.GetGrievanceById(ID).GrievanceStatusID;

                var result = comments.Where(s => s.GrievanceID == ID).ToList();
                foreach (var k in result)
                {
                    if (k.InsertedId != userId)
                    {
                        k.IsRead = true;
                        grievanceRepository.updaeGrievanceInHouseComment(k);
                        //_DbContext.SaveChanges();
                    }
                }
            }
            return View(commentList);
        }

        [HttpPost]
        public IActionResult SaveGrievanceInHouseComment(string comment, long grievanceid, long grievancestatusid, List<IFormFile> file)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            GrievanceInhouseComment grivComment = new GrievanceInhouseComment();
            grivComment.InsertedId = userId;
            grivComment.ModifiedId = userId;
            grivComment.RoleId = roleId;
            grivComment.GrievanceID = grievanceid;
            grivComment.Description = comment;
            //grivComment.GrievanceStatusID = grievancestatusid;
            grievanceRepository.CreateGrievanceInHouseComment(grivComment);
            //_DbContext.GrievanceComments.Add(grivComment);
            //_DbContext.SaveChanges();
            var id = grivComment.ID;
            if (file.Count() > 0)
            {
                foreach (var fil in file)
                {
                    FileInfo fi = new FileInfo(fil.FileName);
                    var newFilename = id + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\GrievanceInHouseAttachment\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        fil.CopyToAsync(stream);
                    }
                    GrievanceInhouseAttachment attachment = new GrievanceInhouseAttachment();
                    attachment.GrievanceInHouseCommentID = id;
                    attachment.InsertedId = userId;
                    attachment.ModifiedId = userId;
                    attachment.Name = fil.FileName;

                    attachment.Path = newFilename;
                    grievanceRepository.CreateGrievanceInhouseAttachment(attachment);
                }
            }
            return RedirectToAction(nameof(GrievanceInHouseCommnets), new { ID = grievanceid });
        }

        public IActionResult RemoveInHouseComment(long id)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            var comment = grievanceRepository.GetGrievanceInhouseCommentById(id);
            if (comment != null)
            {
                grievanceRepository.updaeGrievanceInHouseComment(comment);
                //_DbContext.SaveChanges();
            }
            return RedirectToAction(nameof(GrievanceInHouseCommnets), new { ID = comment.GrievanceID });
        }
        #endregion

        #region GrievanceInhouse Attachments

        public IActionResult GrievanceInhouseAttachment(long grivid)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            var result = grievanceRepository.GrievanceInhouseAttachment(grivid);

            return View(result);
        }

        public IActionResult SaveGrievanceInhouseAttachment(long id, IFormFile file)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (file != null)
            {
                GrievanceInhouseAttachment Attachment = new GrievanceInhouseAttachment();
                Attachment.InsertedId = userId;
                Attachment.ModifiedId = userId;
                Attachment.Active = true;
                Attachment.Status = "Uploaded";
                Attachment.IsAvailable = true;
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = id + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\Grievance\" + newFilename);
                var pathToSave = newFilename;
                //using (var stream = new FileStream(path, FileMode.Create))
                //{
                //    await file.CopyToAsync(stream);
                //}
                Attachment.Path = pathToSave;
                grievanceRepository.CreateGrievanceInhouseAttachment(Attachment);
            }

            return RedirectToAction(nameof(GrievanceInhouseAttachment));
        }
        public IActionResult DeleteGrievanceInhouseAttachment(long id)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;

            grievanceRepository.DeleteGrievanceInhouseAttachment(id, userId);

            return RedirectToAction(nameof(GrievanceInhouseAttachment));
        }
        #endregion

        #region Grievance Attachments

        public IActionResult GrievanceAttachments(long id)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            GrievanceAttachmentNewModel model = new GrievanceAttachmentNewModel();
            var grivAttachments = grievanceRepository.GetAllGrievanceAttachment().Where(a => a.GrievanceID == id).ToList();
            var empList = employeeRepository.GetAllRequiredEmployee();
            if (grivAttachments.Count() > 0)
            {
                var res = (from a in grivAttachments
                           join b in empList on a.InsertedId equals b.ID
                           select new GrievanceAttachmentModel
                           {
                               ID = a.ID,
                               GrievanceId = id,
                               AttachmentName = a.Name,
                               AttachmentPath = a.Path,
                               InsertedDate = a.InsertedDate,
                               ModifiedDate = a.ModifiedDate,
                               PostedBy = a.InsertedId == userId ? "me" : grievancemodalclass.getRoleById(a.RoleId, a.InsertedId)
                           }).ToList();

                model.Attachments = res;
            }

            model.GrievanceId = id;

            return View(model);
        }
        [HttpPost]
        public IActionResult SaveGrievanceAttachments(long grivid)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            var attachFile = HttpContext.Request.Form.Files["myfile"];
            GrievanceAttachment attachments = new GrievanceAttachment();
            attachments.Active = true;
            string attachmentname = "Grievance";

            if (attachFile != null)
            {
                FileInfo fi = new FileInfo(attachFile.FileName);
                var newFilename = attachmentname + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\Grievance\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    attachFile.CopyTo(stream);
                }
                attachments.Path = pathToSave;
                //attach.file = "/ODMImages/Tempfile/" + pathToSave;
            }
            //attachments.AttachmentName = attachmentname;
            //if (myfile != null)
            //{
            //    // Create a File Info 
            //    FileInfo fi = new FileInfo(myfile.FileName);
            //    var newFilename = attachmentname + "_" + String.Format("{0:d}",
            //                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
            //    var webPath = hostingEnvironment.WebRootPath;
            //    string path = Path.Combine("", webPath + @"\ODMImages\Grievance\" + newFilename);
            //    var pathToSave = newFilename;
            //    using (var stream = new FileStream(path, FileMode.Create))
            //    {
            //        myfile.CopyToAsync(stream);
            //    }
            //    attachments.Path = pathToSave;
            //}
            attachments.InsertedId = userId;
            attachments.IsAvailable = true;
            attachments.ModifiedId = userId;
            attachments.RoleId = roleId;
            attachments.InsertedDate = DateTime.Now;
            attachments.ModifiedDate = DateTime.Now;
            attachments.Status = "Uploaded";
            attachments.GrievanceID = grivid;

            grievanceRepository.CreateGrievanceAttachment(attachments);
            //_DbContext.SaveChanges();

            return RedirectToAction(nameof(GrievanceAttachments), new { id = grivid });
        }

        public IActionResult DeleteGrievanceAttachments(long grivids)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            var grivAttachment = grievanceRepository.GetAllGrievanceAttachment().Where(a => a.ID == grivids).FirstOrDefault();
            grivAttachment.ModifiedId = userId;
            grivAttachment.ModifiedDate = DateTime.Now;
            grivAttachment.Active = false;
            grivAttachment.Status = "DELETED";
            grivAttachment.IsAvailable = false;

            grievanceRepository.UpdateGrievanceAttachment(grivAttachment);

            return RedirectToAction(nameof(GrievanceAttachments), new { id = grivAttachment.GrievanceID });
        }


        #endregion

        #region GrievanceStatus

        //GrievanceStatus List 
        public IActionResult GrievanceStatus()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            return View(grievanceRepository.GetAllGrievanceStatus());
        }

        public IActionResult CreateOrEditGrievanceStatus(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                //if (commonMethods.checkaccessavailable("Country", accessId, "Create", "Master", roleId) == false)
                //{
                //    return RedirectToAction("AuthenticationFailed", "Accounts");
                //}
                ViewBag.status = ViewMethod.CREATE;
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                //if (commonMethods.checkaccessavailable("Country", accessId, "Edit", "Master", roleId) == false)
                //{
                //    return RedirectToAction("AuthenticationFailed", "Accounts");
                //}
                ViewBag.status = ViewMethod.UPDATE;
                var grievanceStatus = grievanceRepository.GetGrievanceStatusById(id.Value);
                od.ID = grievanceStatus.ID;
                od.Name = grievanceStatus.Name;
            }
            return View(od);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateGrievanceStatus([Bind("ID,Name")] GrievanceStatus grievanceStatus)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                grievanceStatus.ModifiedId = userId;
                grievanceStatus.Active = true;
                if (grievanceStatus.ID == 0)
                {
                    grievanceStatus.InsertedId = userId;
                    grievanceStatus.Status = EntityStatus.ACTIVE;
                    grievanceRepository.CreateGrievanceStatus(grievanceStatus);
                }
                else
                {
                    grievanceRepository.UpdateGrievanceStatus(grievanceStatus);
                }

                return RedirectToAction(nameof(GrievanceStatus));
            }
            return View(grievanceStatus);
        }


        // POST: GrievanceTypes/Delete/5
        [ActionName("DeleteGrievanceStatus")]
        public async Task<IActionResult> GrievanceStatusDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("Country", accessId, "Delete", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            if (GrievanceStatusExists(id.Value) == true)
            {
                var grievancestatus = grievanceRepository.DeleteGrievanceStatus(id.Value);
            }
            return RedirectToAction(nameof(GrievanceStatus));
        }

        private bool GrievanceStatusExists(long id)
        {
            if (grievanceRepository.GetGrievanceStatusById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion

        #region Raise Grievances

        public IActionResult AllGrievances(long? typeid, long? subtypeid, long? statusid, string fromdate, string todate)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;

            ViewBag.Type = grievanceRepository.GetAllGrievanceCategory();
            ViewBag.SubType = grievanceRepository.GetAllGrievanceType();
            ViewBag.Status = commonMethods.GrievanceStatusList();

            if (commonMethods.checkaccessavailable("Raise", accessId, "List", "Grievance", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(commonMethods.GetallMyGrievance(userId, roleId, typeid, subtypeid, statusid, fromdate, todate));
        }
        public async Task<IActionResult> SendReminderfrmparents(long id,string act)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            backgroundtaskcls on = new backgroundtaskcls();
            on.Id = id;
            SendEmailInBackgroundThread(on);
            return RedirectToAction(act).WithSuccess("Success","Reminder Send Successfully");
        }
        public async Task<IActionResult> SendReminderfromparents(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            backgroundtaskcls on = new backgroundtaskcls();
            on.Id = id;
            SendEmailInBackgroundThread(on);
            return RedirectToAction(nameof(AllGrievances)).WithSuccess("Success", "Reminder Send Successfully");

        }
        public class backgroundtaskcls
        {
            public long Id { get; set; }
        }
        public async Task<JsonResult> GetSubTypeByTypeId(long? id)
        {
            var res = (from a in grievanceRepository.GetAllGrievanceType()
                       where a.CategoryId == id.Value && a.Active == true
                       select new GrievanceStatusModel
                       {
                           ID = a.ID,
                           StatusName = a.Name
                       }).ToList();
            return Json(res);
        }
        public void SendEmailInBackgroundThread(backgroundtaskcls ob)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmailforparentremainder));
            bgThread.IsBackground = true;
            bgThread.Start(ob);
        }
        public void sendbackgroundmailforparentremainder(Object obj1)
        {
            backgroundtaskcls objt = (backgroundtaskcls)obj1;
            var grievances = grievanceRepository.GetAllGrievance().Where(a => a.ID == objt.Id).FirstOrDefault();


            var students = studentAggregateRepository.GetAllStudent().ToList();
            var parents = studentAggregateRepository.GetAllParent().ToList();
            var employee = employeeRepository.GetAllEmployee().ToList();
            string sms = "Please update status of my grievance. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy");
            string name = "";
            string code = "";
            string email = "";
            string phone = "";
            string category = "";
            string whom = "";
            string typename = grievanceRepository.GetAllGrievanceCategory().Where(p => p.ID == grievances.GrievanceCategoryID).FirstOrDefault().Name;
            //string typename = grievanceRepository.GetGrievanceTypeById(type).Name;

            var employees = commonMethods.GetAssignverifyGrievanceemployee().Where(a => a.typeid == grievances.GrievanceCategoryID && a.subtypeid == 0).ToList().Union(commonMethods.GetAssignverifyGrievanceemployee().Where(a => a.typeid == grievances.GrievanceCategoryID && a.subtypeid == grievances.GrievanceTypeID).ToList());
            var forwardemployee = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.Active == true && a.GrievanceId == grievances.ID).Select(a => a.EmployeeId).ToList();
            if(forwardemployee.Count() > 0)
            {
                var result = (from a in employee
                              where forwardemployee.Contains(a.ID)
                              select new Employeeddlview
                              {
                                  typeid = grievances.GrievanceCategoryID,
                                  subtypeid = grievances.GrievanceTypeID,
                                  Code = a.EmailId,
                                  Id = a.ID,
                                  Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                              }).ToList();
                employees = employees.Union(result);
            }
            if (grievances.RoleId == 5)
            {
                var s = students.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.StudentCode;
                email = students.Where(a => a.ID == s.ID && a.EmailID != null).FirstOrDefault().EmailID;
                phone = students.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 5)
                {
                    category = "Student";
                }
                whom = "Student";
            }
            else if (grievances.RoleId == 6)
            {
                var s = parents.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = students.Where(a => a.ID == s.StudentID).FirstOrDefault().StudentCode;
                email = parents.Where(a => a.ID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                phone = parents.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 6)
                {
                    category = "Parent";
                }

                whom = "Parent";
            }
            else
            {
                var s = employee.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.EmpCode;
                email = s.EmailId;
                phone = s.PrimaryMobile;
                category = "Employee";
                whom = "Employee";
            }
            var desc = "Please update status of my grievance. Grievance Code:-" + grievances.Code + "\n Raised On: -" + grievances.InsertedDate.ToString("dd - MMM - yyyy") + "\n By:-" + name;
            foreach (var a in employees)
            {
                smssend.SendReminder(desc, "Grievance Reminder By " + name + "(code-" + grievances.Code + ")", a.Code);
            }
        }


        public async Task<IActionResult> Sendremindertoemployee(long id, string act)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            backgroundtaskcls on = new backgroundtaskcls();
            on.Id = id;
            SendEmailToEmployeeInBackgroundThread(on);
            return RedirectToAction(nameof(act)).WithSuccess("Success", "Reminder Send Successfully");

        }
        public void SendEmailToEmployeeInBackgroundThread(backgroundtaskcls ob)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmailtoemployeeremainder));
            bgThread.IsBackground = true;
            bgThread.Start(ob);
        }
        public IActionResult ReminderFromParent(long grivid)
        {
            var grievance = grievanceRepository.GetGrievanceById(grivid);
            var grivaccess = grievancemodalclass.GrievanceAuthorizedPerson(grievance.ID).ToList();
            var grivaccessHead = grievancemodalclass.GrievanceAuthorizedDeptHeadPerson(grievance.ID).ToList();
            //var verifier = grievancemodalclass.getRoleById(grievance.VerifiedRoleId, grievance.VerifiedId);
            if (grivaccessHead.Count() > 0)
            {
                grivaccess = grivaccess.Union(grivaccessHead).ToList();
            }
            foreach (var a in grivaccess)
            {
                var desc = "Dear " + a.Name + ",\n Sir, As per urgent basis Please solve the Grievance as soon as possible. " + "Grievance Code:-" + grievance.Code + "\n Raised On:-" + grievance.InsertedDate.ToString("dd-MMM-yyyy");
                smssend.SendReminder(desc, "Grievance Reminder from parent (code-" + grievance.Code + ")", a.Code);
            }

            return RedirectToAction(nameof(AllGrievances)).WithSuccess("Success","Reminder sended Successfully");
        }
        public void SendReminderToEmployees(long grivid)
        {
            var grievance = grievanceRepository.GetGrievanceById(grivid);
            var employee = employeeRepository.GetAllEmployee();
            string raisedName = grievancemodalclass.getRoleById(grievance.RoleId, grievance.InsertedId);
            string studentCode = "";
            string email = "";
            string mobile = "";
            string Category = "";
            string grievanceType = "";
            string subType = "";

            if (grievance.RoleId == 6)
            {
                var parent = parentRepository.GetParentById(grievance.InsertedId);
                studentCode = studentAggregateRepository.GetStudentById(parent.StudentID).StudentCode;
                email = parent.EmailId;
                mobile = parent.PrimaryMobile;
                Category = "Parent";
                grievanceType = grievanceRepository.GetGrievanceCategoryById(grievance.GrievanceCategoryID).Name;
                subType = grievanceRepository.GetGrievanceTypeById(grievance.GrievanceTypeID).Name;
            }
            if(grievance.RoleId == 5)
            {
                var student = studentAggregateRepository.GetStudentById(grievance.InsertedId);
                studentCode = student.StudentCode;
                email = student.EmailID;
                mobile = student.PrimaryMobile;
                Category = "Student";
                grievanceType = grievanceRepository.GetGrievanceCategoryById(grievance.GrievanceCategoryID).Name;
                subType = grievanceRepository.GetGrievanceTypeById(grievance.GrievanceTypeID).Name;
            }
            if (grievance.GrievanceStatusID != 4 && grievance.GrievanceStatusID !=6)
            {
                var forwardemployee = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.Active == true && a.GrievanceId == grievance.ID).Select(a => a.EmployeeId).ToList();
                List<Employeeddlview> employees = new List<Employeeddlview>();
                if (forwardemployee.Count() > 0)
                {
                    var result = (from a in employee
                                  where forwardemployee.Contains(a.ID)
                                  select new Employeeddlview
                                  {
                                      Code = a.EmailId,
                                      Id = a.ID,
                                      Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                                  }).ToList();
                    employees = result;
                }
                var grivaccess = grievancemodalclass.GrievanceAuthorizedPerson(grievance.ID).ToList();
                if (grivaccess.Count() > 0)
                {
                    employees = employees.Union(grivaccess).ToList();
                }

                var desc = "Please solve it as soon as possible. Grievance Code:-" + grievance.Code + "\n Raised On: -" + grievance.InsertedDate.ToString("dd - MMM - yyyy") + "\n Raised By:-" + raisedName;
                foreach (var a in employees)
                {
                    smssend.SendReminderToEmployee(desc, "Grievance Reminder (code-" + grievance.Code + ")", a.Code, raisedName, studentCode, email, mobile, Category, grievanceType, subType);
                }
            }
            else if(grievance.GrievanceStatusID == 4)
            {
                string comName = "";
                if(grievance.CompletionId > 0)
                {
                    comName = grievancemodalclass.getRoleById(4, grievance.CompletionId);
                }
                var desc = "Dear " + raisedName + ",\n Your grievance is completed by " + comName + ". Grievance Code:-" + grievance.Code + "\n Raised On:-" + grievance.InsertedDate.ToString("dd-MMM-yyyy") + ". Please verify the grievance by giving score.";
               
                smssend.SendReminder(desc, "Grievance Completed (code-" + grievance.Code + ")", email);
               
            }
            else if(grievance.GrievanceStatusID == 6)
            {
                //var forwardemployee = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.Active == true && a.GrievanceId == grievance.ID).Select(a => a.EmployeeId).ToList();
                //List<Employeeddlview> employees = new List<Employeeddlview>();
                //if (forwardemployee.Count() > 0)
                //{
                //    var result = (from a in employee
                //                  where forwardemployee.Contains(a.ID)
                //                  select new Employeeddlview
                //                  {
                //                      Code = a.EmailId,
                //                      Id = a.ID,
                //                      Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                //                  }).ToList();
                //    employees = result;
                //}
                var grivaccess = grievancemodalclass.GrievanceAuthorizedPerson(grievance.ID).ToList();
                var grivaccessHead = grievancemodalclass.GrievanceAuthorizedDeptHeadPerson(grievance.ID).ToList();
                var verifier = grievancemodalclass.getRoleById(grievance.VerifiedRoleId, grievance.VerifiedId);
                if (grivaccessHead.Count() > 0)
                {
                    grivaccess = grivaccess.Union(grivaccessHead).ToList();
                }
                
                foreach (var a in grivaccess)
                {
                    var desc = "Dear " + a.Name + ",\n Grievance is verified by " + verifier + ". Grievance Code:-" + grievance.Code + "\n Raised On:-" + grievance.InsertedDate.ToString("dd-MMM-yyyy");
                    smssend.SendReminder(desc, "Grievance Verified (code-" + grievance.Code + ")", a.Code);
                }
            }
            
        }
        public IActionResult SendReminderToEmployeesForManagement(long grivid)
        {
            SendReminderToEmployees(grivid);
            return RedirectToAction(nameof(ManageGrievance)).WithSuccess("Success", "Reminder Send Successfully");
        }
        public IActionResult SendReminderToEmployeesForAllGrievance(long grivid)
        {
            SendReminderToEmployees(grivid);
            return RedirectToAction(nameof(GetAllGrievance)).WithSuccess("Success", "Reminder Send Successfully");
        }
        public IActionResult SendReminderToSingleEmployees(long id, long empid)
        {
            var grievance = grievanceRepository.GetGrievanceById(id);
            string raisedName = grievancemodalclass.getRoleById(grievance.RoleId, grievance.InsertedId);
            string studentCode = "";
            string email = "";
            string mobile = "";
            string Category = "";
            string grievanceType = "";
            string subType = "";

            if (grievance.RoleId == 6)
            {
                var parent = parentRepository.GetParentById(grievance.InsertedId);
                studentCode = studentAggregateRepository.GetStudentById(parent.StudentID).StudentCode;
                email = parent.EmailId;
                mobile = parent.PrimaryMobile;
                Category = "Parent";
                grievanceType = grievanceRepository.GetGrievanceCategoryById(grievance.GrievanceCategoryID).Name;
                subType = grievanceRepository.GetGrievanceTypeById(grievance.GrievanceTypeID).Name;
            }
            if (grievance.RoleId == 5)
            {
                var student = studentAggregateRepository.GetStudentById(grievance.InsertedId);
                studentCode = student.StudentCode;
                email = student.EmailID;
                mobile = student.PrimaryMobile;
                Category = "Student";
                grievanceType = grievanceRepository.GetGrievanceCategoryById(grievance.GrievanceCategoryID).Name;
                subType = grievanceRepository.GetGrievanceTypeById(grievance.GrievanceTypeID).Name;
            }


            var employee = employeeRepository.GetEmployeeById(empid);

            var desc = "Please solve it as soon as possible. Grievance Code:-" + grievance.Code + "\n Raised On: -" + grievance.InsertedDate.ToString("dd - MMM - yyyy") + "\n Raised By:-" + raisedName;
             
            smssend.SendReminderToEmployee(desc, "Grievance Reminder (code-" + grievance.Code + ")", employee.EmailId, raisedName, studentCode, email, mobile, Category, grievanceType, subType);
            

            return RedirectToAction(nameof(ViewManageGrievance),new { id = id }).WithSuccess("Success", "Reminder Send Successfully"); 
        }

        public void sendbackgroundmailtoemployeeremainder(Object obj1)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            backgroundtaskcls objt = (backgroundtaskcls)obj1;
            var grievances = grievanceRepository.GetAllGrievance().Where(a => a.ID == objt.Id).FirstOrDefault();


            var students = studentAggregateRepository.GetAllStudent();
            var parents = studentAggregateRepository.GetAllParent();
            var employee = employeeRepository.GetAllEmployee();
            string sms = "Please solve it as soon as possible. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy")+"\n Regards,\n ODM Public School";
            string name = "";
            string code = "";
            string email = "";
            string phone = "";
            string category = "";
            string whom = "";
            string typename = grievanceRepository.GetAllGrievanceCategory().Where(p => p.ID == grievances.GrievanceCategoryID).FirstOrDefault().Name;
            //string typename = grievanceRepository.GetGrievanceTypeById(type).Name;

          //  var employees = commonMethods.GetAssignverifyGrievanceemployee().Where(a => a.typeid == grievances.GrievanceCategoryID && a.subtypeid == 0).ToList().Union(commonMethods.GetAssignverifyGrievanceemployee().Where(a => a.typeid == grievances.GrievanceCategoryID && a.subtypeid == grievances.GrievanceTypeID).ToList());
            var forwardemployee = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.Active == true && a.GrievanceId == grievances.ID && a.InsertedId==userId).Select(a => a.EmployeeId).ToList();
            List<Employeeddlview> employees=new List<Employeeddlview>();
            if (forwardemployee.Count() > 0)
            {
                var result = (from a in employee
                              where forwardemployee.Contains(a.ID)
                              select new Employeeddlview
                              {
                                  typeid = grievances.GrievanceCategoryID,
                                  subtypeid = grievances.GrievanceTypeID,
                                  Code = a.EmailId,
                                  Id = a.ID,
                                  Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                              }).ToList();
                employees = result;
            }
            if (grievances.RoleId == 5)
            {
                var s = students.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.StudentCode;
                email = students.Where(a => a.ID == s.ID && a.EmailID != null).FirstOrDefault().EmailID;
                phone = students.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 5)
                {
                    category = "Student";
                }
                whom = "Student";
            }
            else if (grievances.RoleId == 6)
            {
                var s = parents.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = students.Where(a => a.ID == s.StudentID).FirstOrDefault().StudentCode;
                email = parents.Where(a => a.ID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                phone = parents.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 6)
                {
                    category = "Parent";
                }

                whom = "Parent";
            }
            else
            {
                var s = employee.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.EmpCode;
                email = s.EmailId;
                phone = s.PrimaryMobile;
                category = "Employee";
                whom = "Employee";
            }
            var desc = "Please solve it as soon as possible. Grievance Code:-" + grievances.Code + "\n Raised On: -" + grievances.InsertedDate.ToString("dd - MMM - yyyy") + "\n By:-" + name;
            foreach (var a in employees)
            {
                smssend.SendReminder(desc, "Grievance Reminder By " + name + "(code-" + grievances.Code + ")", a.Code);
            }
        }
 

        public async Task<IActionResult> Sendremindertoparent(long id, string act)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            backgroundtaskcls on = new backgroundtaskcls();
            on.Id = id;
            SendEmailToparentInBackgroundThread(on);
            return RedirectToAction(nameof(act)).WithSuccess("Success", "Reminder Send Successfully");

        }
        public void SendEmailToparentInBackgroundThread(backgroundtaskcls ob)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmailtoparentremainder));
            bgThread.IsBackground = true;
            bgThread.Start(ob);
        }
        public void sendbackgroundmailtoparentremainder(Object obj1)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            backgroundtaskcls objt = (backgroundtaskcls)obj1;
            var grievances = grievanceRepository.GetAllGrievance().Where(a => a.ID == objt.Id).FirstOrDefault();


            var students = studentAggregateRepository.GetAllStudent();
            var parents = studentAggregateRepository.GetAllParent();
            var employee = employeeRepository.GetAllEmployee();
            string sms = "Dear Parents,\n Your query is solved, Please don't forget to verify & rate our grievance team effort.\n Regards,\n ODM Public School";
            string name = "";
            string code = "";
            string email = "";
            string phone = "";
            string category = "";
            string whom = "";
            string typename = grievanceRepository.GetAllGrievanceCategory().Where(p => p.ID == grievances.GrievanceCategoryID).FirstOrDefault().Name;
            //string typename = grievanceRepository.GetGrievanceTypeById(type).Name;

            //  var employees = commonMethods.GetAssignverifyGrievanceemployee().Where(a => a.typeid == grievances.GrievanceCategoryID && a.subtypeid == 0).ToList().Union(commonMethods.GetAssignverifyGrievanceemployee().Where(a => a.typeid == grievances.GrievanceCategoryID && a.subtypeid == grievances.GrievanceTypeID).ToList());
            if (grievances.RoleId == 5)
            {
                var s = students.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.StudentCode;
                email = students.Where(a => a.ID == s.ID && a.EmailID != null).FirstOrDefault().EmailID;
                phone = students.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 5)
                {
                    category = "Student";
                }
                whom = "Student";
            }
            else if (grievances.RoleId == 6)
            {
                var s = parents.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = students.Where(a => a.ID == s.StudentID).FirstOrDefault().StudentCode;
                email = parents.Where(a => a.ID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                phone = parents.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 6)
                {
                    category = "Parent";
                }

                whom = "Parent";
            }
            else
            {
                var s = employee.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.EmpCode;
                email = s.EmailId;
                phone = s.PrimaryMobile;
                category = "Employee";
                whom = "Employee";
            }
            var desc = "Dear Parents,\n Your query is solved, Please don't forget to verify & rate our grievance team effort.";
           
                smssend.SendReminder(desc, "Grievance Reminder By " + name + "(code-" + grievances.Code + ")", email);
           
        }





        // POST: GrievanceTypes/Delete/5
        [ActionName("DeleteGrievance")]
        public async Task<IActionResult> GrievanceDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Raise", accessId, "Delete", "Grievance", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (GrievanceExists(id.Value) == true)
            {
                var grievance = grievanceRepository.DeleteGrievance(id.Value);
            }
            return RedirectToAction(nameof(AllGrievances));
        }

        private bool GrievanceExists(long id)
        {
            if (grievanceRepository.GetGrievanceById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public IActionResult CreateOrEditGrievance(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            var types = grievanceRepository.GetAllGrievanceCategory();
            var subType = grievanceRepository.GetAllGrievanceType();
            var roles = roleRepository.GetAllRole();
            //IEnumerable<GrievanceType> res = (from a in types
            //                                       join b in roles on a.RoleID equals b.ID
            //                                       select new GrievanceType
            //                                       {
            //                                           ID = a.ID,
            //                                           Name = a.Name + "(" + b.Name + ")"                                                                                                         
            //                                       }).ToList();
            MyGrievanceClass myGrievance = new MyGrievanceClass();
            myGrievance.GrivType = grievanceRepository.GetAllGrievanceCategory();
            myGrievance.GrivSubTypes = grievanceRepository.GetAllGrievanceType();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Raise", accessId, "Create", "Grievance", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                myGrievance.grievanceAttachments = null;
                ViewBag.status = ViewMethod.CREATE;
                myGrievance.ID = 0;
                myGrievance.status = null;
                myGrievance.GrievanceStatusID = 1;
                myGrievance.GrievanceTypeID = 0;
                myGrievance.GrievanceSubTypeID = 0;
                myGrievance.Description = "";
            }

            else
            {
                if (commonMethods.checkaccessavailable("Raise", accessId, "Edit", "Grievance", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = ViewMethod.UPDATE;
                var grievance = grievanceRepository.GetGrievanceById(id.Value);
                myGrievance.ID = grievance.ID;
                myGrievance.grievanceAttachments = grievanceRepository.GetAllGrievanceAttachment().Where(a => a.GrievanceID == id.Value).ToList();
                myGrievance.GrievanceTypeID = grievance.GrievanceCategoryID;
                myGrievance.GrievanceCategoryID = grievance.GrievanceTypeID;
                myGrievance.GrievanceStatusID = grievance.GrievanceStatusID;
                myGrievance.Description = grievance.Description;
            }
            HttpContext.Session.Remove("attachments");
            return View(myGrievance);
        }
        public async Task<JsonResult> SaveGrievanceAttachment(string Name, IFormFile file)
        {
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            List<grievanceattachments> grievanceattachments = new List<grievanceattachments>();
            if (HttpContext.Session.GetString("attachments") != null)
            {
                grievanceattachments = JsonConvert.DeserializeObject<List<grievanceattachments>>(HttpContext.Session.GetString("attachments"));
            }
            grievanceattachments grievanceattach = new grievanceattachments();
            if (file != null)
            {
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = Name + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\Grievance\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                grievanceattach.path = path;
                grievanceattach.file = @"\ODMImages\Grievance\" + pathToSave;
            }
            grievanceattach.RoleId = roleId;
            grievanceattach.Name = Name;
            grievanceattach.id = grievanceattachments.Count + 1;
            grievanceattachments.Add(grievanceattach);

            HttpContext.Session.SetString("attachments", JsonConvert.SerializeObject(grievanceattachments));
            return Json(grievanceattachments);
        }
        public async Task<JsonResult> RemoveGrievanceAttachment(int id)
        {
            List<grievanceattachments> grievanceattachments = new List<grievanceattachments>();
            if (HttpContext.Session.GetString("attachments") != null)
            {
                var attach = HttpContext.Session.GetString("attachments");

                grievanceattachments = JsonConvert.DeserializeObject<List<grievanceattachments>>(attach);
            }
            grievanceattachments.Remove(grievanceattachments.Where(a=>a.id==id).FirstOrDefault());
            HttpContext.Session.SetString("attachments", JsonConvert.SerializeObject(grievanceattachments));
            return Json(grievanceattachments);
        }
        public async Task<JsonResult> SaveGrievance(long type, long subtype, string description)
        {
            try
            {
                userId = HttpContext.Session.GetInt32("userId").Value;
                long roleId = HttpContext.Session.GetInt32("roleId").Value;
                Grievance grievance = new Grievance();
                grievance.GrievanceCategoryID = type;
                grievance.GrievanceTypeID = subtype;
                grievance.Code = commonMethods.GetGrievancecode(roleId);
                grievance.Description = description;
                grievance.ModifiedId = userId;
                grievance.GrievanceStatusID = 1;
                grievance.InsertedId = userId;
                grievance.Status = EntityStatus.ACTIVE;
                grievance.Active = true;
                grievance.RoleId = roleId;
                grievance.VerifiedRoleId = 0;
                grievance.IsRequestedForStudentDetails = false;
                grievance.RequestedId = 0;
                grievance.IsApproveForStudentDetails = false;
                grievance.ApprovedId = 0;
                grievance.IsRejectedForStudentDetails = false;
                grievance.IsGrievanceRejected = false; 
                grievance.ProgressDate = DateTime.Now;
                if (roleId == 6)
                {
                    long studentid = parentRepository.GetAllParent().Where(a => a.ID == userId).FirstOrDefault().StudentID;
                    grievance.GrievanceRelatedId = studentid;
                    grievance.RequestType = "ParentLOGIN";
                }
                else if (roleId == 5)
                {
                    grievance.GrievanceRelatedId = userId;
                    grievance.RequestType = "StudentLOGIN";
                }
                else if (roleId == 1)
                {
                    grievance.GrievanceRelatedId = userId;
                    grievance.RequestType = "SuperAdminLOGIN";
                }
                else
                {
                    grievance.GrievanceRelatedId = userId;
                    grievance.RequestType = "EmployeeLOGIN";
                }



                long id = grievanceRepository.CreateGrievance(grievance);
                long relatedid = grievance.GrievanceRelatedId;

                GrievanceTimeline tm = new GrievanceTimeline();
                tm.GrievanceID = id;
                tm.Active = true;
                tm.GrievanceStatusID = 1;
                tm.RoleId = roleId;
                tm.InsertedId = userId;
                tm.ModifiedId = userId;
                tm.Status = EntityStatus.ACTIVE;
                grievanceRepository.CreateGrievanceTimeline(tm);

                List<grievanceattachments> grievanceattachments = new List<grievanceattachments>();
                if (HttpContext.Session.GetString("attachments") != null)
                {
                    grievanceattachments = JsonConvert.DeserializeObject<List<grievanceattachments>>(HttpContext.Session.GetString("attachments"));
                }
                if (grievanceattachments.Count() > 0)
                {
                    foreach (var a in grievanceattachments)
                    {
                        string getinfo = Path.GetFileName(a.path);
                        string filepath = hostingEnvironment.WebRootPath + "\\ODMImages\\Grievance\\" + id;
                        if (!Directory.Exists(filepath))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(filepath);
                        }

                        System.IO.File.Copy(a.path, filepath + "\\" + getinfo);
                        GrievanceAttachment grievanceAttachment = new GrievanceAttachment();
                        grievanceAttachment.Active = true;
                        grievanceAttachment.GrievanceID = id;
                        grievanceAttachment.InsertedId = userId;
                        grievanceAttachment.ModifiedId = userId;
                        grievanceAttachment.Name = a.Name;
                        grievanceAttachment.RoleId = roleId;
                        grievanceAttachment.Path = getinfo;
                        grievanceAttachment.Status = "CREATED";
                        grievanceRepository.CreateGrievanceAttachment(grievanceAttachment);
                    }
                    HttpContext.Session.Remove("attachments");
                }
                var students = studentAggregateRepository.GetAllStudent();
                var parents = studentAggregateRepository.GetAllParent();
                var employee = employeeRepository.GetAllEmployee();
                string sms = "We are in receipt of your grievance and we regret the inconvenience caused. Our team will be trying to fix this grievance as soon as possible and get back to you.\n\n Regards \n ODM Management";
                string name = "";
                string code = "";
                string email = "";
                string phone = "";
                string category = "";
                string whom = "";
                string typename = grievanceRepository.GetAllGrievanceCategory().Where(p => p.ID == type).FirstOrDefault().Name;
                //string typename = grievanceRepository.GetGrievanceTypeById(type).Name;
                if (roleId == 6)
                {
                    string mobile = parents.Where(a => a.ID == userId).FirstOrDefault().PrimaryMobile;
                    if (mobile != null)
                    {
                        smssend.Sendsmstoemployee(mobile, sms);
                    }
                }
                else if (roleId == 5)
                {
                    string mobile = students.Where(a => a.ID == userId).FirstOrDefault().PrimaryMobile;
                    if (mobile != null)
                    {
                        smssend.Sendsmstoemployee(mobile, sms);
                    }
                }
                else
                {
                    if (roleId == 1)
                    {

                    }
                    else
                    {
                        string mobile = employee.Where(a => a.ID == userId).FirstOrDefault().PrimaryMobile;
                        if (mobile != null)
                        {
                            smssend.Sendsmstoemployee(mobile, sms);
                        }
                    }
                }
                var employees = commonMethods.GetAssignverifyGrievanceemployee().Where(a => a.typeid == type).ToList().Union(commonMethods.GetAssignverifyGrievanceemployee().Where(a => a.typeid == type && a.subtypeid == subtype).ToList());
                if (roleId == 5)
                {
                    var s = students.Where(a => a.ID == relatedid).FirstOrDefault();
                    name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                    code = s.StudentCode;
                    email = students.Where(a => a.ID == s.ID && a.EmailID != null).FirstOrDefault().EmailID;
                    phone = students.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                    if (roleId == 5)
                    {
                        category = "Student";
                    }
                    whom = "Student";
                }
                else if (roleId == 6)
                {
                    var s = parents.Where(a => a.ID == relatedid).FirstOrDefault();
                    name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                    code = students.Where(a => a.ID == s.StudentID).FirstOrDefault().StudentCode;
                    email = parents.Where(a => a.ID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                    phone = parents.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                    if (roleId == 6)
                    {
                        category = "Parent";
                    }

                    whom = "Parent";
                }
                else
                {
                    var s = employee.Where(a => a.ID == relatedid).FirstOrDefault();
                    name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                    code = s.EmpCode;
                    email = s.EmailId;
                    phone = s.PrimaryMobile;
                    category = "Employee";
                    whom = "Employee";
                }
                foreach (var a in employees)
                {
                    smssend.newgrievance(name, email, phone, code, category, typename, "New Grievance Request", whom, a.Code);
                }
                //var url = "/Grievance/AllGrievances";
                return Json(new { redirectToUrl = "/Grievance/AllGrievances" });
            }
            catch (Exception e1)
            {
                return Json(0);
            }
        }
        public IActionResult ViewMyGrievance(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            ViewBag.Employee = GetAllEmployeeName();
            if (commonMethods.checkaccessavailable("Raise", accessId, "View", "Grievance", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (HttpContext.Session.GetString("attachments") != null)
            {
                HttpContext.Session.Remove("attachments");
            }

            var res = grievancemodalclass.GetGrievanceDetails(id, userId, roleId);
            return View(res);
        }
        #endregion

        #region Grievance Forward

        [HttpPost]
        public IActionResult SaveForwardGrievance(long grivid, long? fwdEmpId, long? fwdDeptId)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            long id = 0;
            GrievanceForward forward = new GrievanceForward();
            // var forwardGriv = _DbContext.GrievanceForwards.Where(a => a.Active == true && a.GrievanceId == grivid).ToList();

            //if (forwardGriv.Count > 0)
            //{
            //    foreach (var griv in forwardGriv)
            //    {
            //        griv.IsAvailable = false;
            //        _DbContext.GrievanceForwards.Update(griv);
            //        _DbContext.SaveChanges();
            //    }
            //}
            if (fwdEmpId != null && fwdDeptId != 0)                   //Grievance is forward to Employee
            {
                // var deptId = commonMethods.GetDepartmentsByEmployeeID(fwdEmpId.Value);
                forward.InsertedDate = DateTime.Now;
                forward.InsertedId = userId;
                forward.ModifiedDate = DateTime.Now;
                forward.ModifiedId = userId;
                forward.Active = true;
                forward.Status = "Created";
                forward.IsAvailable = true;
                forward.GrievanceId = grivid;
                forward.EmployeeId = fwdEmpId.Value;
                forward.DepartmentId = fwdDeptId.Value;
                forward.IsSubAssigned = false;
                forward.StatusId = 1;

                id = grievanceForwardRepository.AddAsync(forward).Result.ID;
                // _DbContext.SaveChanges();
            }
            if (fwdDeptId != 0 && fwdEmpId == null)                   //Grievance is forward to Department
            {
                var accesstype = grievanceRepository.GrievanceAccess().Where(a => a.GrievanceCategoryId == fwdDeptId.Value && a.GrievanceTypeId == 0).FirstOrDefault();
                if (accesstype != null)
                {
                    forward.InsertedDate = DateTime.Now;
                    forward.InsertedId = userId;
                    forward.ModifiedDate = DateTime.Now;
                    forward.ModifiedId = userId;
                    forward.Active = true;
                    forward.Status = "Created";
                    forward.IsAvailable = true;
                    forward.GrievanceId = grivid;
                    forward.EmployeeId = accesstype.EmployeeId;
                    forward.DepartmentId = fwdDeptId.Value;
                    forward.IsSubAssigned = false;
                    forward.StatusId = 1;
                    id = grievanceForwardRepository.AddAsync(forward).Result.ID;
                }
                else
                {
                    ViewBag.deptHeadId = 0;
                }
            }
            if (id > 0)              //Create grievance timeline
            {
                GrievanceTimeline timeline = new GrievanceTimeline();
                timeline.InsertedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedDate = DateTime.Now;
                timeline.ModifiedId = userId;
                timeline.Active = true;
                timeline.Status = "ACTIVE";
                timeline.IsAvailable = true;
                timeline.RoleId = roleId;
                timeline.GrievanceID = grivid;
                timeline.GrievanceStatusID = 7;      // StatusId is 7 means the grievance is forwarded
                timeline.IsForwarded = true;
                timeline.ForwardToId = fwdEmpId != null ? fwdEmpId.Value : 0;
                timeline.ForwardToDept = fwdDeptId != null ? fwdDeptId.Value : 0;
                grievanceRepository.CreateGrievanceTimeline(timeline);
                //_DbContext.SaveChanges();
            }
            if(id > 0)
            {
                var grivFor = grievanceForwardRepository.GetByIdAsyncIncludeAll(id).Result;
                var grivaccess = _DbContext.GrievanceAccess.Where(a => a.GrievanceCategoryId == grivFor.DepartmentId && a.GrievanceTypeId == 0).Select(a => a.EmployeeId).FirstOrDefault();
                var employee = employeeRepository.GetAllEmployee().Where(a => a.ID == grivaccess).Select(a => new EmailModal { ID = a.ID,FirstName = a.FirstName,LastName = a.LastName,Email = a.EmailId}).FirstOrDefault();
                var grievance = grievanceRepository.GetGrievanceById(grivid);
                var forwardEmp = employeeRepository.GetEmployeeById(userId);
                var raisedEmp = new EmailModal();

                if (grievance.RoleId == 6)
                {
                    raisedEmp = parentRepository.GetAllParent().Where(a => a.ID == grievance.InsertedId).Select(a => new EmailModal { ID = a.ID, FirstName = a.FirstName, LastName = a.LastName, Email = a.EmailId }).FirstOrDefault();
                }
                else if (grievance.RoleId == 5)
                {
                    raisedEmp = studentAggregateRepository.GetAllStudent().Where(a => a.ID == grievance.InsertedId).Select(a => new EmailModal { ID = a.ID, FirstName = a.FirstName, LastName = a.LastName, Email = a.EmailID }).FirstOrDefault();
                }

                string raisedName = grievancemodalclass.getRoleById(grievance.RoleId, grievance.InsertedId);

                List<EmailModal> ee = new List<EmailModal>();
                ee.Add(employee);
                ee.Add(raisedEmp);
                var desc = "";

                foreach (var a in ee)
                {
                    if (a.ID == grievance.InsertedId)
                    {
                        desc = "Dear " + raisedEmp.FirstName + " " + raisedEmp.LastName + "\n, Your grievance  is Forwarded to " + employee.FirstName + " " + employee.LastName + " By " + forwardEmp.FirstName + " " + forwardEmp.LastName + ". Grievance Code:- " + grievance.Code + "\n Raised On: -" + grievance.InsertedDate.ToString("dd - MMM - yyyy") + ". Please have patience.";
                    }
                    else
                    {
                        desc = "Dear " + employee.FirstName + " " + employee.LastName + "\n, A new grievance  is Forwarded to you By " + forwardEmp.FirstName + " " + forwardEmp.LastName + ". Grievance Code:- " + grievance.Code + "\n Raised On: -" + grievance.InsertedDate.ToString("dd - MMM - yyyy") + "\n By:-" + raisedName + ". Please solve it as soon as possible.";
                    }

                    smssend.GrievanceSubAssign(desc, "Grievance Forwarded By " + forwardEmp.FirstName + " " + forwardEmp.LastName + "(code-" + grievance.Code + ")", a.Email);
                }
            }

            if (commonMethods.checkaccessavailable("All Grievance", accessId, "List", "Grievance", roleId) == false)
            {
                return RedirectToAction(nameof(ViewAllGrievance), new { id = grivid });
            }
            return RedirectToAction(nameof(ViewManageGrievance), new { id = grivid });
        }
        [HttpPost]
        public IActionResult subassigngrievance(long said, long? fEmpId, long? caid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            var emp = grievancemodalclass.getEmployeeById(userId);
            long id = 0;
            GrievanceForward forward = new GrievanceForward();
            // var forwardGriv = _DbContext.GrievanceForwards.Where(a => a.Active == true && a.GrievanceId == grivid).ToList();

            //if (forwardGriv.Count > 0)
            //{
            //    foreach (var griv in forwardGriv)
            //    {
            //        griv.IsAvailable = false;
            //        _DbContext.GrievanceForwards.Update(griv);
            //        _DbContext.SaveChanges();
            //    }
            //}
            if (fEmpId != 0 && caid != 0)                   //Grievance is forward to Employee
            {
                // var deptId = commonMethods.GetDepartmentsByEmployeeID(fwdEmpId.Value);
                forward.InsertedDate = DateTime.Now;
                forward.InsertedId = userId;
                forward.ModifiedDate = DateTime.Now;
                forward.ModifiedId = userId;
                forward.Active = true;
                forward.Status = "Created";
                forward.IsAvailable = true;
                forward.GrievanceId = said;
                forward.EmployeeId = fEmpId.Value;
                forward.DepartmentId = caid.Value;
                forward.IsSubAssigned = true;
                forward.StatusId = 1;

                id = grievanceForwardRepository.AddAsync(forward).Result.ID;
                // _DbContext.SaveChanges();
            }

            if (id > 0)              //Create grievance timeline
            {
                GrievanceTimeline timeline = new GrievanceTimeline();
                timeline.InsertedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedDate = DateTime.Now;
                timeline.ModifiedId = userId;
                timeline.Active = true;
                timeline.Status = "ACTIVE";
                timeline.IsAvailable = true;
                timeline.GrievanceID = said;
                timeline.GrievanceStatusID = 9;      // StatusId is 7 means the grievance is subassigned
                timeline.IsForwarded = true;
                timeline.RoleId = roleId;
                timeline.ForwardToId = fEmpId != 0 ? fEmpId.Value : 0;
                timeline.ForwardToDept = caid != 0 ? caid.Value : 0;
                grievanceRepository.CreateGrievanceTimeline(timeline);
                //_DbContext.SaveChanges();
            }
            if(id > 0)
            {
                var forwards = grievanceForwardRepository.GetByIdAsyncIncludeAll(id).Result;
                var employee = employeeRepository.GetAllEmployee().Where(a => a.ID == forwards.EmployeeId).Select(a => new EmailModal { ID = a.ID, FirstName = a.FirstName, LastName = a.LastName, Email = a.EmailId }).FirstOrDefault();
                var grievance = grievanceRepository.GetGrievanceById(forwards.GrievanceId); 
                var raisedEmp = new EmailModal();

                if (grievance.RoleId == 6)
                {
                    raisedEmp = parentRepository.GetAllParent().Where(a => a.ID == grievance.InsertedId).Select(a => new EmailModal { ID = a.ID, FirstName = a.FirstName,LastName = a.LastName,Email = a.EmailId}).FirstOrDefault();
                }
                else if(grievance.RoleId == 5)
                {
                    raisedEmp = studentAggregateRepository.GetAllStudent().Where(a => a.ID == grievance.InsertedId).Select(a => new EmailModal { ID = a.ID,FirstName = a.FirstName,LastName = a.LastName,Email = a.EmailID}).FirstOrDefault();
                }
               
                string raisedName = grievancemodalclass.getRoleById(grievance.RoleId, grievance.InsertedId);

                List<EmailModal> ee = new List<EmailModal>();
                ee.Add(employee);
                ee.Add(raisedEmp);
                var desc = "";

                foreach(var a in ee)
                {
                    if (a.ID == grievance.InsertedId)
                    {
                        desc = "Dear " + raisedEmp.FirstName + " " + raisedEmp.LastName + "\n, Your grievance  is Sub-assigned to " + employee.FirstName + " " + employee.LastName + " By " + emp + ". Grievance Code:- " + grievance.Code + "\n Raised On: -" + grievance.InsertedDate.ToString("dd - MMM - yyyy") + ". Please have patience.";
                    }
                    else
                    {
                        desc = "Dear " + employee.FirstName + " " + employee.LastName + "\n, A new grievance  is Sub-assigned to you By " + emp + ". Grievance Code:- " + grievance.Code + "\n Raised On: -" + grievance.InsertedDate.ToString("dd - MMM - yyyy") + "\n By:-" + raisedName + ". Please solve it as soon as possible.";
                    }

                    smssend.GrievanceSubAssign(desc, "Grievance Sub-Assigned By " + emp + "(code-" + grievance.Code + ")", a.Email);
                }

            }

            if (commonMethods.checkaccessavailable("All Grievance", accessId, "List", "Grievance", roleId) == true)
            {
                return RedirectToAction(nameof(ViewAllGrievance), new { id = said });
            }
            return RedirectToAction(nameof(ViewManageGrievance), new { id = said });
        }

        public class EmailModal
        {
            public long ID { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
        }

        public IActionResult RemoveSubAssigned(long subassignedid)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            var subAssign = grievanceForwardRepository.GetByIdAsync(subassignedid).Result;
            subAssign.ModifiedDate = DateTime.Now;
            subAssign.ModifiedId = userId;
            subAssign.Active = false;
            subAssign.Status = "Updated";
            subAssign.IsSubAssigned = false;

            grievanceForwardRepository.UpdateAsync(subAssign);


            if (commonMethods.checkaccessavailable("All Grievance", accessId, "List", "Grievance", roleId) == true)
            {
                return RedirectToAction(nameof(ViewAllGrievance), new { id = subAssign.GrievanceId });
            }
            return RedirectToAction(nameof(ViewManageGrievance), new { id = subAssign.GrievanceId });
        }

        #endregion

        #region Extended Timeline

        [HttpPost]
        public IActionResult SaveGrievanceExtendedTimeLine(long grivid, Nullable<DateTime> extdDate, Nullable<TimeSpan> extdTime, string reason, IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            GrievanceExtendedTimeline extendedTimeline = new GrievanceExtendedTimeline();

            extendedTimeline.InsertedDate = DateTime.Now;
            extendedTimeline.InsertedId = userId;
            extendedTimeline.ModifiedDate = DateTime.Now;
            extendedTimeline.ModifiedId = userId;
            extendedTimeline.Active = true;
            extendedTimeline.Status = "ACTIVE";
            extendedTimeline.IsAvailable = true;
            extendedTimeline.GrievanceId = grivid;
            extendedTimeline.ExtendedDate = extdDate;
            extendedTimeline.ExtendedTime = extdTime;
            extendedTimeline.ExtendedReason = reason;

            long ids = grievanceExtendedTimelinesRepository.AddAsync(extendedTimeline).Result.ID;
            //  _DbContext.SaveChanges();

            if (file != null)                       //save the extendedtimeline attachment
            {
                GrievanceExtendedTimelineAttachment timelineAttachment = new GrievanceExtendedTimelineAttachment();
                timelineAttachment.InsertedDate = DateTime.Now;
                // timelineAttachment.InsertedDate = DateTime.Now;
                timelineAttachment.InsertedId = userId;
                timelineAttachment.ModifiedDate = DateTime.Now;
                timelineAttachment.ModifiedId = userId;
                timelineAttachment.Active = true;
                timelineAttachment.Status = "Uploaded";
                timelineAttachment.IsAvailable = true;
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = grivid + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\Grievance\" + newFilename);
                var pathToSave = newFilename;
                //using (var stream = new FileStream(path, FileMode.Create))
                //{
                //    await file.CopyToAsync(stream);
                //}
                timelineAttachment.Path = pathToSave;
                grievanceExtendedTimelineAttachmentRepository.AddAsync(timelineAttachment);
                //  _DbContext.SaveChanges();


            }

            GrievanceTimeline timeline = new GrievanceTimeline();      // Create grievance timeline
            timeline.InsertedDate = DateTime.Now;
            timeline.InsertedId = userId;
            timeline.ModifiedDate = DateTime.Now;
            timeline.ModifiedId = userId;
            timeline.Active = true;
            timeline.Status = "ACTIVE";
            timeline.IsAvailable = true;
            timeline.GrievanceID = grivid;
            timeline.GrievanceStatusID = 8;                          // StatusId 8 means grievance timeline is extneded
            timeline.RoleId = roleId;                            

            grievanceRepository.CreateGrievanceTimeline(timeline);
            SendMailForExtendedTimeLine(grivid, userId);

            if (commonMethods.checkaccessavailable("All Grievance", accessId, "List", "Grievance", roleId) == true)
            {
                return RedirectToAction(nameof(ViewAllGrievance), new { id = grivid }).WithSuccess("Success","Grievance timeline is extended successfully");
            }

            return RedirectToAction(nameof(ViewManageGrievance), new { id = grivid }).WithSuccess("Success", "Grievance timeline is extended successfully");

        }

        public void SendMailForExtendedTimeLine(long grivid,long userid)
        {
            var grievance = grievanceRepository.GetGrievanceById(grivid);
            var employee = employeeRepository.GetAllEmployee();
            string raisedName = grievancemodalclass.getRoleById(grievance.RoleId, grievance.InsertedId);
            string studentCode = "";
            string email = "";
            string mobile = "";
            string Category = "";
            string grievanceType = "";
            string subType = "";

            if (grievance.RoleId == 6)
            {
                var parent = parentRepository.GetParentById(grievance.InsertedId);
                studentCode = studentAggregateRepository.GetStudentById(parent.StudentID).StudentCode;
                email = parent.EmailId;
                mobile = parent.PrimaryMobile;
                Category = "Parent";
                grievanceType = grievanceRepository.GetGrievanceCategoryById(grievance.GrievanceCategoryID).Name;
                subType = grievanceRepository.GetGrievanceTypeById(grievance.GrievanceTypeID).Name;
            }
            if (grievance.RoleId == 5)
            {
                var student = studentAggregateRepository.GetStudentById(grievance.InsertedId);
                studentCode = student.StudentCode;
                email = student.EmailID;
                mobile = student.PrimaryMobile;
                Category = "Student";
                grievanceType = grievanceRepository.GetGrievanceCategoryById(grievance.GrievanceCategoryID).Name;
                subType = grievanceRepository.GetGrievanceTypeById(grievance.GrievanceTypeID).Name;
            }

            var name = grievancemodalclass.getRoleById(4,userid);

            var desc = "Dear " + raisedName + ",\n Grievance timeline is extended by " + name + ". Grievance Code:-" + grievance.Code + "\n Raised On:-" + grievance.InsertedDate.ToString("dd-MMM-yyyy");
            smssend.SendReminder(desc, "Grievance TimeLine Extended (code-" + grievance.Code + ")",email);
            
        }

        public void SendEmailextendedtimelineInBackgroundThread(backgroundtaskcls ob)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmailextendedtimelineremainder));
            bgThread.IsBackground = true;
            bgThread.Start(ob);
        }
        public void sendbackgroundmailextendedtimelineremainder(Object obj1)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            backgroundtaskcls objt = (backgroundtaskcls)obj1;
            var grievances = grievanceRepository.GetAllGrievance().Where(a => a.ID == objt.Id).FirstOrDefault();


            var students = studentAggregateRepository.GetAllStudent();
            var parents = studentAggregateRepository.GetAllParent();
            var employee = employeeRepository.GetAllEmployee();
            string sms = "Dear Parents,\n This will take some time to fix as management committee has not discussed yet. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy") + "\n Regards,\n ODM Public School";
            string name = "";
            string code = "";
            string email = "";
            string phone = "";
            string category = "";
            string whom = "";
            string typename = grievanceRepository.GetAllGrievanceCategory().Where(p => p.ID == grievances.GrievanceCategoryID).FirstOrDefault().Name;
           
            if (grievances.RoleId == 5)
            {
                var s = students.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.StudentCode;
                email = students.Where(a => a.ID == s.ID && a.EmailID != null).FirstOrDefault().EmailID;
                phone = students.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 5)
                {
                    category = "Student";
                }
                whom = "Student";
            }
            else if (grievances.RoleId == 6)
            {
                var s = parents.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = students.Where(a => a.ID == s.StudentID).FirstOrDefault().StudentCode;
                email = parents.Where(a => a.ID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                phone = parents.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 6)
                {
                    category = "Parent";
                }

                whom = "Parent";
            }
            else
            {
                var s = employee.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.EmpCode;
                email = s.EmailId;
                phone = s.PrimaryMobile;
                category = "Employee";
                whom = "Employee";
            }
            var desc = "Dear Parents,\n This will take some time to fix as management committee has not discussed yet. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy");
            //foreach (var a in employees)
            //{
                smssend.SendReminder(desc, "Grievance Extended Time (code-" + grievances.Code + ")", email);
            //}
        }




        #endregion

        #region Email Notification

        public void EmailNotificationForDeadlineMissed(long grivid)
        {
            var grievance = grievanceRepository.GetGrievanceById(grivid);
            var employee = employeeRepository.GetAllEmployee();
            string raisedName = grievancemodalclass.getRoleById(grievance.RoleId, grievance.InsertedId);
            string studentCode = "";
            string email = "";
            string mobile = "";
            string Category = "";
            string grievanceType = "";
            string subType = "";

            if (grievance.RoleId == 6)
            {
                var parent = parentRepository.GetParentById(grievance.InsertedId);
                studentCode = studentAggregateRepository.GetStudentById(parent.StudentID).StudentCode;
                email = parent.EmailId;
                mobile = parent.PrimaryMobile;
                Category = "Parent";
                grievanceType = grievanceRepository.GetGrievanceCategoryById(grievance.GrievanceCategoryID).Name;
                subType = grievanceRepository.GetGrievanceTypeById(grievance.GrievanceTypeID).Name;
            }
            if (grievance.RoleId == 5)
            {
                var student = studentAggregateRepository.GetStudentById(grievance.InsertedId);
                studentCode = student.StudentCode;
                email = student.EmailID;
                mobile = student.PrimaryMobile;
                Category = "Student";
                grievanceType = grievanceRepository.GetGrievanceCategoryById(grievance.GrievanceCategoryID).Name;
                subType = grievanceRepository.GetGrievanceTypeById(grievance.GrievanceTypeID).Name;
            }
            if (grievance.GrievanceStatusID != 4 && grievance.GrievanceStatusID != 6)
            {
                var forwardemployee = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.Active == true && a.GrievanceId == grievance.ID).Select(a => a.EmployeeId).ToList();
                List<Employeeddlview> employees = new List<Employeeddlview>();
                if (forwardemployee.Count() > 0)
                {
                    var result = (from a in employee
                                  where forwardemployee.Contains(a.ID)
                                  select new Employeeddlview
                                  {
                                      Code = a.EmailId,
                                      Id = a.ID,
                                      Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                                  }).ToList();
                    employees = result;
                }
                var grivaccess = grievancemodalclass.GrievanceAuthorizedPerson(grievance.ID).ToList();
                if (grivaccess.Count() > 0)
                {
                    employees = employees.Union(grivaccess).ToList();
                }

                foreach (var a in employees)
                {
                    var desc = "Hello " + a.Name + ",\nYou have missed the timeline for, Grievance Type - " + grievanceType + ", Grievance Sub-Type - " + subType + ". Please solve it as soon as possible";

                    smssend.SendReminderToEmployee(desc, "Grievance Reminder (code-" + grievance.Code + ")", a.Code, raisedName, studentCode, email, mobile, Category, grievanceType, subType);
                }
            }

        }
       
        #endregion

        #region Verify Grievance

        [HttpPost]
        public IActionResult VerifyGrievance(long hdnid, decimal txtscore,string remark)
        {
            var userId = HttpContext.Session.GetInt32("userId").Value;
            var roleId = HttpContext.Session.GetInt32("roleId").Value;
            var griv = grievanceRepository.GetGrievanceById(hdnid);
            if (griv != null)
            {
                griv.ModifiedDate = DateTime.Now;
                griv.ModifiedId = userId;
                griv.VerificationDate = DateTime.Now;
                griv.VerifiedId = userId;
                griv.ScoreGivenDate = DateTime.Now;
                griv.Score = txtscore;
                griv.VerifyRemark = remark;
                griv.VerifiedRoleId = roleId;
                griv.GrievanceStatusID = 6;             // StatusId 6 means Grievance is verified
                grievanceRepository.UpdateGrievance(griv);

                GrievanceTimeline timeline = new GrievanceTimeline();
                timeline.InsertedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedDate = DateTime.Now;
                timeline.ModifiedId = userId;
                timeline.RoleId = roleId;
                timeline.Active = true;
                timeline.Status = "ACTIVE";
                timeline.IsAvailable = true;
                timeline.GrievanceID = hdnid;
                timeline.GrievanceStatusID = 6;   // StatusId 6 means Grievance is verified

                grievanceRepository.CreateGrievanceTimeline(timeline);
                //   _DbContext.SaveChanges();
                SendReminderToEmployees(hdnid);

                return RedirectToAction(nameof(AllGrievances));
            }

            return null;
        }

        public void SendEmailInVerifyBackgroundThread(backgroundtaskcls ob)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmailfoVerifyremainder));
            bgThread.IsBackground = true;
            bgThread.Start(ob);
        }
        public void sendbackgroundmailfoVerifyremainder(Object obj1)
        {
            backgroundtaskcls objt = (backgroundtaskcls)obj1;
            var grievances = grievanceRepository.GetAllGrievance().Where(a => a.ID == objt.Id).FirstOrDefault();


            var students = studentAggregateRepository.GetAllStudent();
            var parents = studentAggregateRepository.GetAllParent();
            var employee = employeeRepository.GetAllEmployee();
            string sms = "Grievance Verified Successfully. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy");
            string name = "";
            string code = "";
            string email = "";
            string phone = "";
            string category = "";
            string whom = "";
            string typename = grievanceRepository.GetAllGrievanceCategory().Where(p => p.ID == grievances.GrievanceCategoryID).FirstOrDefault().Name;
            //string typename = grievanceRepository.GetGrievanceTypeById(type).Name;

            var employees = commonMethods.GetAssignverifyGrievanceemployee().Where(a => a.typeid == grievances.GrievanceCategoryID && a.subtypeid == 0).ToList().Union(commonMethods.GetAssignverifyGrievanceemployee().Where(a => a.typeid == grievances.GrievanceCategoryID && a.subtypeid == grievances.GrievanceTypeID).ToList());
            var forwardemployee = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.Active == true && a.GrievanceId == grievances.ID).Select(a => a.EmployeeId).ToList();
            if (forwardemployee.Count() > 0)
            {
                var result = (from a in employee
                              where forwardemployee.Contains(a.ID)
                              select new Employeeddlview
                              {
                                  typeid = grievances.GrievanceCategoryID,
                                  subtypeid = grievances.GrievanceTypeID,
                                  Code = a.EmailId,
                                  Id = a.ID,
                                  Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                              }).ToList();
                employees = employees.Union(result);
            }
            if (grievances.RoleId == 5)
            {
                var s = students.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.StudentCode;
                email = students.Where(a => a.ID == s.ID && a.EmailID != null).FirstOrDefault().EmailID;
                phone = students.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 5)
                {
                    category = "Student";
                }
                whom = "Student";
            }
            else if (grievances.RoleId == 6)
            {
                var s = parents.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = students.Where(a => a.ID == s.StudentID).FirstOrDefault().StudentCode;
                email = parents.Where(a => a.ID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                phone = parents.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 6)
                {
                    category = "Parent";
                }

                whom = "Parent";
            }
            else
            {
                var s = employee.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.EmpCode;
                email = s.EmailId;
                phone = s.PrimaryMobile;
                category = "Employee";
                whom = "Employee";
            }
            var desc = "Grievance Verified Successfully. Grievance Code:-" + grievances.Code + "\n Raised On: -" + grievances.InsertedDate.ToString("dd - MMM - yyyy") + "\n By:-" + name;
            foreach (var a in employees)
            {
                smssend.SendReminder(desc, "Grievance Verified By " + name + "(code-" + grievances.Code + ")", a.Code);
            }
        }

        #endregion

        #region Requested For Student Details

        public IActionResult RequestForStudentDetails(long grivid)
        {
            var userId = HttpContext.Session.GetInt32("userId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            var grievance = grievanceRepository.GetGrievanceById(grivid);
            grievance.IsRequestedForStudentDetails = true;
            grievance.RequestedId = userId;

            grievanceRepository.UpdateGrievance(grievance);            

            //GrievanceTimeline timeline = new GrievanceTimeline();   //Timelines
            //timeline.InsertedDate = DateTime.Now;
            //timeline.InsertedId = userId;
            //timeline.ModifiedDate = DateTime.Now;
            //timeline.ModifiedId = userId;
            //timeline.Active = true;
            //timeline.Status = "Active";
            //timeline.IsAvailable = true;
            //timeline.GrievanceID = grivid;
            //timeline.RoleId = roleId;
            //timeline.GrievanceStatusID = grievance.GrievanceStatusID;
            //timeline.IsForwarded = false;


            return RedirectToAction(nameof(ManageGrievance)).WithSuccess("Success", "Your request is sent to Grievance Management Department");

        }

        public IActionResult ApproveForStudentDetails(long grivid)
        {
            var userId = HttpContext.Session.GetInt32("userId").Value;

            var grievance = grievanceRepository.GetGrievanceById(grivid);
            grievance.IsApproveForStudentDetails = true;
            grievance.ApprovedId = userId;

            grievanceRepository.UpdateGrievance(grievance);

            return RedirectToAction(nameof(ViewAllGrievance), new { id = grivid}).WithSuccess("Success", "Requested Grievance is Approved By You");

        }
        public IActionResult RejectForStudentDetails(long grivid)
        {
            var userId = HttpContext.Session.GetInt32("userId").Value;

            var grievance = grievanceRepository.GetGrievanceById(grivid);
            grievance.IsRejectedForStudentDetails = true;
            grievance.ApprovedId = userId;

            grievanceRepository.UpdateGrievance(grievance);

            return RedirectToAction(nameof(ViewAllGrievance), new { id = grivid }).WithSuccess("Success", "Requested Grievance is Rejected By You");

        }

        #endregion

        #region Grievance Rejected

        public IActionResult RejectGrievance(long grivid, string reason)
        {
            var userId = HttpContext.Session.GetInt32("userId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            var grievance = grievanceRepository.GetGrievanceById(grivid);
            grievance.ModifiedId = userId;
            grievance.ModifiedDate = DateTime.Now;
            grievance.GrievanceStatusID = 5;    // StatusId 5 means grievance is rejected
            grievance.IsGrievanceRejected = true;
            grievance.RejectReason = reason;

            grievanceRepository.UpdateGrievance(grievance);

            GrievanceTimeline grievanceTimeline = new GrievanceTimeline();
            grievanceTimeline.InsertedId = userId;
            grievanceTimeline.InsertedDate = DateTime.Now;
            grievanceTimeline.ModifiedId = userId;
            grievanceTimeline.ModifiedDate = DateTime.Now;
            grievanceTimeline.Active = true;
            grievanceTimeline.Status = "ACTIVE";
            grievanceTimeline.IsAvailable = true;
            grievanceTimeline.GrievanceID = grivid;
            grievanceTimeline.RoleId = roleId;
            grievanceTimeline.GrievanceStatusID = grievance.GrievanceStatusID;
            grievanceTimeline.IsForwarded = false;

            grievanceRepository.CreateGrievanceTimeline(grievanceTimeline);

            return RedirectToAction(nameof(ManageGrievance));
        }

        #endregion

        #region Manage Grievance

        public IActionResult ManageGrievance(long? roleId, long? typeid, long? subtypeid, long? statusid, string fromdate, string todate)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            if (roleId == null)
            {
                roleId = HttpContext.Session.GetInt32("roleId").Value;
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            var access = grievanceRepository.GrievanceAccess();
            var types = grievanceRepository.GetAllGrievanceCategory();
            var subtypes = grievanceRepository.GetAllGrievanceType();
            long deptHead = 0;
            if (commonMethods.checkaccessavailable("Manage Grievance", accessId, "Management", "Grievance", roleId.Value) == false)
            {
                access = access.Where(a => a.Active == true && a.EmployeeId == userId).ToList();
                types = types.Where(a => access.Select(s => s.GrievanceCategoryId).ToList().Contains(a.ID)).ToList();
                subtypes = subtypes.Where(a => access.Select(s => s.GrievanceTypeId).ToList().Contains(a.ID)).ToList();
                deptHead = 1;
            }


            ViewBag.IsDeptHead = deptHead == 1 ? "No" : "Yes";      //Check for dept head
            ViewBag.Type = types;
            ViewBag.SubType = subtypes;
            ViewBag.Status = commonMethods.GrievanceStatusList();
            ViewBag.Category = commonMethods.GrievanceCategoryList();

            if (commonMethods.checkaccessavailable("Manage Grievance", accessId, "Manage", "Grievance", roleId.Value) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(commonMethods.GetallManageGrievance(userId, roleId.Value, typeid, subtypeid, statusid, fromdate, todate,accessId));
        } 
        public async Task<JsonResult> GetSubTypes(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            userId = HttpContext.Session.GetInt32("userId").Value;
            var access = grievanceRepository.GrievanceAccess();
            if (commonMethods.checkaccessavailable("Manage Grievance", accessId, "Management", "Grievance", roleId) == false)
            {
                access = access.Where(a => a.Active == true && a.EmployeeId == userId && a.GrievanceCategoryId == id).ToList();
                if (access.Where(a => a.GrievanceTypeId == 0).ToList().Count > 0)
                {
                    var SubType = grievanceRepository.GetAllGrievanceType().Where(a => a.Active == true && a.CategoryId == id).ToList();
                    return Json(SubType);
                }
                else
                {
                    var SubType = grievanceRepository.GetAllGrievanceType().Where(a => access.Select(s => s.GrievanceTypeId).ToList().Contains(a.ID) && a.CategoryId == id).ToList();
                    return Json(SubType);
                }
            }
            else
            {
                var SubType = grievanceRepository.GetAllGrievanceType().Where(a => a.Active == true && a.CategoryId == id).ToList();
                return Json(SubType);
            }
        }
        public IActionResult ViewManageGrievance(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            var gvaccess = grievanceRepository.GrievanceAccess().Where(a => a.Active == true).ToList();
            var emp = employeeRepository.GetAllEmployee();
            var empres = emp.Where(a => a.Active == true && gvaccess.Select(m => m.EmployeeId).Distinct().ToList().Contains(a.ID)).Select(a => new Empclass { ID = a.ID, FullName = a.FirstName + " " + a.LastName }).ToList();

            ViewBag.Employee = empres;

            ViewBag.Type = grievanceRepository.GetAllGrievanceCategory();
            if (commonMethods.checkaccessavailable("Manage Grievance", accessId, "Manage", "Grievance", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (HttpContext.Session.GetString("attachments") != null)
            {
                HttpContext.Session.Remove("attachments");
            }
            ViewBag.Status = commonMethods.GrievanceStatusList();
            var res = grievancemodalclass.GetGrievanceDetails(id, userId,roleId);
            var ssm = gvaccess.Where(a => a.GrievanceCategoryId == res.GrievanceCategoryID).Select(a => a.EmployeeId).Distinct().ToList();
            var emprest = emp.Where(a => a.Active == true && ssm.Contains(a.ID)).Select(a => new Empclass { ID = a.ID, FullName = a.FirstName + " " + a.LastName }).ToList();

            ViewBag.subAssignEmployee = emprest;
            return View(res);
        }
        public async Task<JsonResult> GetGrievanceCategory(long empid)
        {
            var gvaccess = grievanceRepository.GrievanceAccess().Where(a => a.Active == true && a.EmployeeId == empid).Select(a => a.GrievanceCategoryId).Distinct().ToList();
            var catg = grievanceRepository.GetAllGrievanceCategory().Where(a => a.Active == true && gvaccess.Contains(a.ID)).ToList();
            return Json(catg);

        }
        public async Task<IActionResult> inprogressGrievancestatusupdate(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;

            Grievance gv = grievanceRepository.GetAllGrievance().Where(a => a.ID == id).FirstOrDefault();
            gv.GrievanceStatusID = 3;
            gv.ModifiedId = userId;
            grievanceRepository.UpdateGrievance(gv);
            GrievanceTimeline tm = new GrievanceTimeline();
            tm.GrievanceID = id;
            tm.RoleId = roleId;
            tm.IsAvailable = true;
            tm.Active = true;
            tm.GrievanceStatusID = 3;
            tm.InsertedId = userId;
            tm.ModifiedId = userId;
            tm.Status = EntityStatus.ACTIVE;
            grievanceRepository.CreateGrievanceTimeline(tm);
            var grievanceforward = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.GrievanceId == id && a.EmployeeId == userId && a.Active == true).ToList();
            if (grievanceforward.Count > 0)
            {
                foreach (var a in grievanceforward)
                {
                    GrievanceForward forward = a;
                    forward.StatusId = 3;
                    forward.ModifiedDate = DateTime.Now;
                    forward.ModifiedId = userId;
                    await grievanceForwardRepository.UpdateAsync(forward);
                }
            }
            string Status = "IN-PROGRESS";
            SendEmailForStatusChange(id, Status,null);
            return RedirectToAction(nameof(ViewManageGrievance), new { id = id }).WithSuccess("Success", "Status change is updated Successfully"); 
        }

        public void SendEmailinprogressInBackgroundThread(backgroundtaskcls ob)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmailinprogressremainder));
            bgThread.IsBackground = true;
            bgThread.Start(ob);
        }
        public void sendbackgroundmailinprogressremainder(Object obj1)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            backgroundtaskcls objt = (backgroundtaskcls)obj1;
            var grievances = grievanceRepository.GetAllGrievance().Where(a => a.ID == objt.Id).FirstOrDefault();


            var students = studentAggregateRepository.GetAllStudent();
            var parents = studentAggregateRepository.GetAllParent();
            var employee = employeeRepository.GetAllEmployee();
            string sms = "Dear Parents,\n Your query is under progress, Please have patience, We will get back to you as soon as possible. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy") + "\n Regards,\n ODM Educational Group";
            string name = "";
            string code = "";
            string email = "";
            string phone = "";
            string category = "";
            string whom = "";
            string typename = grievanceRepository.GetAllGrievanceCategory().Where(p => p.ID == grievances.GrievanceCategoryID).FirstOrDefault().Name;

            if (grievances.RoleId == 5)
            {
                var s = students.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.StudentCode;
                email = students.Where(a => a.ID == s.ID && a.EmailID != null).FirstOrDefault().EmailID;
                phone = students.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 5)
                {
                    category = "Student";
                }
                whom = "Student";
            }
            else if (grievances.RoleId == 6)
            {
                var s = parents.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = students.Where(a => a.ID == s.StudentID).FirstOrDefault().StudentCode;
                email = parents.Where(a => a.ID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                phone = parents.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 6)
                {
                    category = "Parent";
                }

                whom = "Parent";
            }
            else
            {
                var s = employee.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.EmpCode;
                email = s.EmailId;
                phone = s.PrimaryMobile;
                category = "Employee";
                whom = "Employee";
            }
            var desc = "Dear Parents,\n Your query is under progress, Please have patience, We will get back to you as soon as possible. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy");
            //foreach (var a in employees)
            //{
            smssend.SendReminder(desc, "Grievance In-Progress (code-" + grievances.Code + ")", email);
            //}
        }


        [HttpPost]
        public async Task<IActionResult> onholdGrievancestatusupdate(long grid, string remark)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;

            Grievance gv = grievanceRepository.GetAllGrievance().Where(a => a.ID == grid).FirstOrDefault();
            gv.GrievanceStatusID = 2;
            gv.ModifiedId = userId;
            grievanceRepository.UpdateGrievance(gv);
            GrievanceTimeline tm = new GrievanceTimeline();
            tm.GrievanceID = grid;
            tm.RoleId = roleId;
            tm.Remarks = remark;
            tm.IsAvailable = true;
            tm.Active = true;
            tm.GrievanceStatusID = 2;
            tm.InsertedId = userId;
            tm.ModifiedId = userId;
            tm.Status = EntityStatus.ACTIVE;
            grievanceRepository.CreateGrievanceTimeline(tm);
            var grievanceforward = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.GrievanceId == grid && a.EmployeeId == userId).FirstOrDefault();
            if (grievanceforward != null)
            {
                GrievanceForward forward = grievanceforward;
                forward.StatusId = 2;
                forward.ModifiedDate = DateTime.Now;
                forward.ModifiedId = userId;
                await grievanceForwardRepository.UpdateAsync(forward);
            }

            string Status = "ON-HOLD";
            SendEmailForStatusChange(grid, Status, remark);

            return RedirectToAction(nameof(ViewManageGrievance), new { id = grid }).WithSuccess("Success","Status is changed Successfully");
        }
        public void SendEmailonholdInBackgroundThread(backgroundtaskcls ob)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmailonholdremainder));
            bgThread.IsBackground = true;
            bgThread.Start(ob);
        }
        public void sendbackgroundmailonholdremainder(Object obj1)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            backgroundtaskcls objt = (backgroundtaskcls)obj1;
            var grievances = grievanceRepository.GetAllGrievance().Where(a => a.ID == objt.Id).FirstOrDefault();


            var students = studentAggregateRepository.GetAllStudent();
            var parents = studentAggregateRepository.GetAllParent();
            var employee = employeeRepository.GetAllEmployee();
            string sms = "Dear Parents,\n Your query is on-hold, Please have patience, We will get back to you as soon as possible. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy") + "\n Regards,\n ODM Educational Group";
            string name = "";
            string code = "";
            string email = "";
            string phone = "";
            string category = "";
            string whom = "";
            string typename = grievanceRepository.GetAllGrievanceCategory().Where(p => p.ID == grievances.GrievanceCategoryID).FirstOrDefault().Name;

            if (grievances.RoleId == 5)
            {
                var s = students.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.StudentCode;
                email = students.Where(a => a.ID == s.ID && a.EmailID != null).FirstOrDefault().EmailID;
                phone = students.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 5)
                {
                    category = "Student";
                }
                whom = "Student";
            }
            else if (grievances.RoleId == 6)
            {
                var s = parents.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = students.Where(a => a.ID == s.StudentID).FirstOrDefault().StudentCode;
                email = parents.Where(a => a.ID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                phone = parents.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 6)
                {
                    category = "Parent";
                }

                whom = "Parent";
            }
            else
            {
                var s = employee.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.EmpCode;
                email = s.EmailId;
                phone = s.PrimaryMobile;
                category = "Employee";
                whom = "Employee";
            }
            var desc = "Dear Parents,\n Your query is on-hold, Please have patience, We will get back to you as soon as possible. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy");
            //foreach (var a in employees)
            //{
            smssend.SendReminder(desc, "Grievance On-Hold (code-" + grievances.Code + ")", email);
            //}
        }


        public async Task<IActionResult> completedGrievancestatusupdate(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;

            Grievance gv = grievanceRepository.GetAllGrievance().Where(a => a.ID == id).FirstOrDefault();
            gv.CompletionDate = DateTime.Now;
            gv.CompletionId = userId;
            gv.GrievanceStatusID = 4;
            gv.ModifiedId = userId;
            grievanceRepository.UpdateGrievance(gv);
            GrievanceTimeline tm = new GrievanceTimeline();
            tm.GrievanceID = id;
            tm.RoleId = roleId;
            tm.IsAvailable = true;
            tm.Active = true;
            tm.GrievanceStatusID = 4;
            tm.InsertedId = userId;
            tm.ModifiedId = userId;
            tm.Status = EntityStatus.ACTIVE;
            grievanceRepository.CreateGrievanceTimeline(tm);
            var grievanceforward = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.GrievanceId == id && a.EmployeeId == userId).FirstOrDefault();
            if (grievanceforward != null)
            {
                GrievanceForward forward = grievanceforward;
                forward.StatusId = 4;
                forward.ModifiedDate = DateTime.Now;
                forward.ModifiedId = userId;
                await grievanceForwardRepository.UpdateAsync(forward);
            }
            string Status = "COMPLETED";
            SendEmailForStatusChange(id, Status,null);
            return RedirectToAction(nameof(ViewManageGrievance), new { id = id }).WithSuccess("Success","Status change is updated Successfully");
        }
        public void SendEmailForStatusChange(long grivid, string status,string remark)
        {
            var grievance = grievanceRepository.GetGrievanceById(grivid);
            var employee = employeeRepository.GetAllEmployee();
            string raisedName = grievancemodalclass.getRoleById(grievance.RoleId, grievance.InsertedId);
            string studentCode = "";
            string email = "";
            string mobile = "";
            string Category = "";
            string grievanceType = "";
            string subType = "";

            if (grievance.RoleId == 6)
            {
                var parent = parentRepository.GetParentById(grievance.InsertedId);
                studentCode = studentAggregateRepository.GetStudentById(parent.StudentID).StudentCode;
                email = parent.EmailId;
                mobile = parent.PrimaryMobile;
                Category = "Parent";
                grievanceType = grievanceRepository.GetGrievanceCategoryById(grievance.GrievanceCategoryID).Name;
                subType = grievanceRepository.GetGrievanceTypeById(grievance.GrievanceTypeID).Name;
            }
            if (grievance.RoleId == 5)
            {
                var student = studentAggregateRepository.GetStudentById(grievance.InsertedId);
                studentCode = student.StudentCode;
                email = student.EmailID;
                mobile = student.PrimaryMobile;
                Category = "Student";
                grievanceType = grievanceRepository.GetGrievanceCategoryById(grievance.GrievanceCategoryID).Name;
                subType = grievanceRepository.GetGrievanceTypeById(grievance.GrievanceTypeID).Name;
            }

            string comName = "";
            var desc = "";
            if (grievance.ModifiedId > 0)
            {
                comName = grievancemodalclass.getRoleById(4, grievance.ModifiedId);
            }

            if (remark != null)     // Email description for status on hold
            {
                desc = "Dear " + raisedName + ",\n Your grievance is marked as " + status + " due to \"" + remark + "\" By " + comName + ". Grievance Code:-" + grievance.Code + "\n Raised On:-" + grievance.InsertedDate.ToString("dd-MMM-yyyy") + ". Please have patience for further Update.";
            }
            else                   // Email description for status in-progress and completed
            {
                desc = "Dear " + raisedName + ",\n Your grievance is marked as " + status + " By " + comName + ". Grievance Code:-" + grievance.Code + "\n Raised On:-" + grievance.InsertedDate.ToString("dd-MMM-yyyy") + ". Please have patience for further Update.";
            }
            
            smssend.SendReminder(desc, "Grievance " + status + " (code-" + grievance.Code + ")", email);
            
        }
        public void SendEmailcompletedInBackgroundThread(backgroundtaskcls ob)
        {
            Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmailcompletedremainder));
            bgThread.IsBackground = true;
            bgThread.Start(ob);
        }
        public void sendbackgroundmailcompletedremainder(Object obj1)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            backgroundtaskcls objt = (backgroundtaskcls)obj1;
            var grievances = grievanceRepository.GetAllGrievance().Where(a => a.ID == objt.Id).FirstOrDefault();


            var students = studentAggregateRepository.GetAllStudent();
            var parents = studentAggregateRepository.GetAllParent();
            var employee = employeeRepository.GetAllEmployee();
            string sms = "Dear Parents,\n Your query is solved, Please verify the grievance by giving score. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy") + "\n Regards,\n ODM Educational Group";
            string name = "";
            string code = "";
            string email = "";
            string phone = "";
            string category = "";
            string whom = "";
            string typename = grievanceRepository.GetAllGrievanceCategory().Where(p => p.ID == grievances.GrievanceCategoryID).FirstOrDefault().Name;

            if (grievances.RoleId == 5)
            {
                var s = students.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.StudentCode;
                email = students.Where(a => a.ID == s.ID && a.EmailID != null).FirstOrDefault().EmailID;
                phone = students.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 5)
                {
                    category = "Student";
                }
                whom = "Student";
            }
            else if (grievances.RoleId == 6)
            {
                var s = parents.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = students.Where(a => a.ID == s.StudentID).FirstOrDefault().StudentCode;
                email = parents.Where(a => a.ID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                phone = parents.Where(a => a.ID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (grievances.RoleId == 6)
                {
                    category = "Parent";
                }

                whom = "Parent";
            }
            else
            {
                var s = employee.Where(a => a.ID == grievances.GrievanceRelatedId).FirstOrDefault();
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.EmpCode;
                email = s.EmailId;
                phone = s.PrimaryMobile;
                category = "Employee";
                whom = "Employee";
            }
            var desc = "Dear Parents,\n Your query is solved, Please verify the grievance by giving score. Grievance Code:-" + grievances.Code + "\n Raised On:-" + grievances.InsertedDate.ToString("dd-MMM-yyyy");
            //foreach (var a in employees)
            //{
            smssend.SendReminder(desc, "Grievance Completed (code-" + grievances.Code + ")", email);
            //}
        }





        [HttpPost]
        public IActionResult SaveViewGrievanceInHouseComment(string comment, long grievanceid, long grievancestatusid)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            var attachfile = HttpContext.Request.Form.Files["file"];
            GrievanceInhouseComment grivComment = new GrievanceInhouseComment();
            grivComment.InsertedId = userId;
            grivComment.ModifiedId = userId;
            grivComment.RoleId = roleId;
            grivComment.InsertedDate = DateTime.Now;
            grivComment.ModifiedDate = DateTime.Now;
            grivComment.Active = true;
            grivComment.Status = "Active";
            grivComment.IsAvailable = true;
            grivComment.GrievanceID = grievanceid;
            grivComment.Description = comment;
            grivComment.GrievanceStatusID = grievancestatusid;
            grievanceRepository.CreateGrievanceInHouseComment(grivComment);
            //_DbContext.GrievanceComments.Add(grivComment);
            //_DbContext.SaveChanges();
            var id = grivComment.ID;

            if (attachfile != null)
            {
                FileInfo fi = new FileInfo(attachfile.FileName);
                var newFilename = "Grievance" + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\GrievanceInHouseAttachment\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    attachfile.CopyTo(stream);
                }
                GrievanceInhouseAttachment attachment = new GrievanceInhouseAttachment();
                attachment.Active = true;
                attachment.GrievanceID = grievanceid;
                attachment.GrievanceInHouseCommentID = id;
                attachment.InsertedDate = DateTime.Now;
                attachment.InsertedId = userId;
                attachment.IsAvailable = true;
                attachment.ModifiedDate = DateTime.Now;
                attachment.ModifiedId = userId;
                attachment.Name = attachfile.FileName;

                attachment.Path = pathToSave;
                attachment.Status = "ACTIVE";
                grievanceRepository.CreateGrievanceInhouseAttachment(attachment); 
            }
           
            return RedirectToAction(nameof(ViewManageGrievance), new { ID = grievanceid });
        }
        [HttpPost]
        public IActionResult SaveViewGrievanceComment(string comment1, long grievanceid1, long grievancestatusid1)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            var attachfile = HttpContext.Request.Form.Files["file1"];
            GrievanceComment grivComment = new GrievanceComment();
            grivComment.InsertedId = userId;
            grivComment.ModifiedId = userId;
            grivComment.RoleId = roleId;
            grivComment.InsertedDate = DateTime.Now;
            grivComment.ModifiedDate = DateTime.Now;
            grivComment.Active = true;
            grivComment.Status = "Active";
            grivComment.IsAvailable = true;
            grivComment.GrievanceID = grievanceid1;
            grivComment.Description = comment1;
            grivComment.GrievanceStatusID = grievancestatusid1;
            grievanceRepository.CreateGrievanceComment(grivComment);
            //_DbContext.GrievanceComments.Add(grivComment);
            //_DbContext.SaveChanges();
            var id = grivComment.ID; 

            if (attachfile != null)
            {
                FileInfo fi = new FileInfo(attachfile.FileName);
                var newFilename = "Grievance" + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\GrievanceAttachment\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    attachfile.CopyTo(stream);
                }
                GrievanceCommentAttachment attachment = new GrievanceCommentAttachment();
                attachment.Active = true;
                attachment.GrievanceCommentID = id;
                attachment.InsertedDate = DateTime.Now;
                attachment.InsertedId = userId;
                attachment.IsAvailable = true;
                attachment.ModifiedDate = DateTime.Now;
                attachment.ModifiedId = userId;
                attachment.Name = attachfile.FileName;
                attachment.Path = pathToSave;
                attachment.Status = "ACTIVE";
                grievanceRepository.CreateGrievanceCommentAttachment(attachment);
            }
            
            return RedirectToAction(nameof(ViewManageGrievance), new { ID = grievanceid1 });
        }

        #endregion

        #region All Grievance

        public IActionResult GetAllGrievance(long? roleId, long? typeid, long? subtypeid, long? statusid, string fromdate, string todate)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            if (roleId == null)
            {
                roleId = HttpContext.Session.GetInt32("roleId").Value;
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            var access = grievanceRepository.GrievanceAccess();
            var types = grievanceRepository.GetAllGrievanceCategory();
            var subtypes = grievanceRepository.GetAllGrievanceType();
            //if (commonMethods.checkaccessavailable("All Grievance", accessId, "Management", "Grievance", roleId.Value) == false)
            //{
            //    access = access.Where(a => a.Active == true && a.EmployeeId == userId).ToList();
            //    types = types.Where(a => access.Select(s => s.GrievanceCategoryId).ToList().Contains(a.ID)).ToList();
            //    subtypes = subtypes.Where(a => access.Select(s => s.GrievanceTypeId).ToList().Contains(a.ID)).ToList();
            //}



            ViewBag.Type = types;
            ViewBag.SubType = subtypes;
            ViewBag.Status = commonMethods.GrievanceStatusList();
            ViewBag.Category = commonMethods.GrievanceCategoryList();

            if (commonMethods.checkaccessavailable("All Grievance", accessId, "List", "Grievance", roleId.Value) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(commonMethods.GetallGrievances(userId, roleId.Value, typeid, subtypeid, statusid, fromdate, todate, accessId));

        }
        public class EmpClassModal
        {
            public long ID { get; set; }
            public string EmpName { get; set; }
            public long DeptId { get; set; }
            public string DeptName { get; set; }
        }
        public List<EmpClassModal> GetallEmployeeByDeptId()
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            var deptId = commonMethods.GetDepartmentsByEmployeeID(userId).Select(a => a.id);
            var empList = commonMethods.GetAllDeptwithEmployee().Where(a => deptId.Contains(a.DepartmentID)).ToList();
            //var emp = employeeRepository.get.GetAllEmployee();
            var res = (from a in empList
                       select new EmpClassModal
                       {
                           ID = a.EmployeeID,
                           EmpName = a.EmployeeID == userId ? "me" : employeeRepository.GetEmployeeFullNameById(a.EmployeeID),
                           DeptId = a.DepartmentID,
                           DeptName = a.DepartmentName
                       }).ToList();

            return res;
        }
        public IActionResult ViewAllGrievance(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            ViewBag.Employee = GetAllEmployeeName();
            ViewBag.Type = grievanceRepository.GetAllGrievanceCategory();
            if (commonMethods.checkaccessavailable("All Grievance", accessId, "View", "Grievance", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (HttpContext.Session.GetString("attachments") != null)
            {
                HttpContext.Session.Remove("attachments");
            }
            ViewBag.subAssignEmployee = GetallEmployeeByDeptId();
            var res = grievancemodalclass.GetAllGrievanceDetails(id, userId,roleId);
            return View(res);
        }

        #endregion

        #region Assign & Manage Grievance
        public IActionResult AssignManageGrievances()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            ViewBag.employees = commonMethods.GetManageGrievanceemployee();
            if (commonMethods.checkaccessavailable("Assign & Verify Grievance", accessId, "List", "Grievance", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(grievancemodalclass.GetAllGrievanceForView());
        }
        public IActionResult unassignedGrievance(long id)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Assign & Verify Grievance", accessId, "Un Assigned", "Grievance", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            Grievance gv = grievanceRepository.GetAllGrievance().Where(a => a.ID == id).FirstOrDefault();
            gv.GrievanceStatusID = 1;
            gv.ModifiedId = userId;
            grievanceRepository.UpdateGrievance(gv);
            EmployeeGrievance emp = grievanceRepository.GetAllEmployeeGrievance().Where(a => a.GrievanceID == id).FirstOrDefault();
            GrievanceTimeline tm = new GrievanceTimeline();
            tm.GrievanceID = id;
            tm.Active = true;
            tm.GrievanceStatusID = 1;
            tm.InsertedId = userId;
            tm.ModifiedId = emp.EmployeeID;
            tm.Status = EntityStatus.ACTIVE;
            grievanceRepository.CreateGrievanceTimeline(tm);
            grievanceRepository.DeleteEmployeeGrievance(emp.ID);
            return RedirectToAction(nameof(AssignManageGrievances));
        }
        public async Task<JsonResult> AssignGrievanceToEmployee(long empid, string gid, string desc)
        {
            try
            {
                userId = HttpContext.Session.GetInt32("userId").Value;
                long accessId = HttpContext.Session.GetInt32("accessId").Value;
                long roleId = HttpContext.Session.GetInt32("roleId").Value;
                if (commonMethods.checkaccessavailable("Assign & Verify Grievance", accessId, "Assigned", "Grievance", roleId) == false)
                {
                    return Json(2);
                }
                string mobile = "";
                string[] gvid = gid.Split(',');
                string name = "";
                string code = "";
                string email = "";
                string phone = "";
                string category = "";
                string whom = "";
                string typename = "";
                string sms = "Your grievance has been assigned to the concerned department.We assure you that they are working on fixing the same.We will hopefully resolve the same.\n\n Regards \n ODM Management";
                foreach (string item in gvid)
                {

                    EmployeeGrievance emp = new EmployeeGrievance();
                    emp.EmployeeID = empid;
                    emp.GrievanceID = Convert.ToInt64(item);
                    emp.Active = true;
                    emp.Status = ViewMethod.CREATE;
                    emp.ModifiedId = userId;
                    emp.InsertedId = userId;
                    grievanceRepository.CreateEmployeeGrievance(emp);
                    Grievance gv = grievanceRepository.GetAllGrievance().Where(a => a.ID == Convert.ToInt64(item)).FirstOrDefault();
                    gv.GrievanceStatusID = 2;
                    grievanceRepository.UpdateGrievance(gv);
                    GrievanceTimeline tm = new GrievanceTimeline();
                    tm.GrievanceID = Convert.ToInt64(item);
                    tm.Active = true;
                    tm.GrievanceStatusID = 2;
                    tm.InsertedId = userId;
                    tm.ModifiedId = userId;
                    tm.Status = EntityStatus.ACTIVE;
                    grievanceRepository.CreateGrievanceTimeline(tm);
                    if (gv.GrievanceCategoryID == 6)
                    {
                        string bb = parentRepository.GetAllParent().Where(a => a.ID == gv.GrievanceRelatedId).FirstOrDefault().PrimaryMobile;
                        if (bb != null)
                        {
                            smssend.Sendsmstoemployee(bb, sms);
                        }
                    }
                    else if (gv.GrievanceCategoryID == 5)
                    {
                        var parents = parentRepository.GetAllParent().Where(a => a.StudentID == gv.GrievanceRelatedId).ToList();
                        if (parents.Count > 0)
                        {
                            foreach (var a in parents)
                            {
                                string bb = a.PrimaryMobile;
                                if (mobile != null)
                                {
                                    smssend.Sendsmstoemployee(bb, sms);
                                }
                            }
                        }
                    }
                    else
                    {
                        string bb = employeeRepository.GetAllEmployee().Where(a => a.ID == gv.GrievanceRelatedId).FirstOrDefault().PrimaryMobile;
                        if (mobile != null)
                        {
                            smssend.Sendsmstoemployee(bb, sms);
                        }
                    }
                    GrievanceComment grievanceComment = new GrievanceComment();
                    grievanceComment.Active = true;
                    grievanceComment.Description = desc;
                    grievanceComment.GrievanceID = Convert.ToInt64(item);
                    grievanceComment.GrievanceStatusID = 2;
                    grievanceComment.InsertedId = userId;
                    grievanceComment.ModifiedId = userId;
                    grievanceComment.Status = "CREATED";
                    long commid = grievanceRepository.CreateGrievanceComment(grievanceComment);

                    typename = grievanceRepository.GetGrievanceTypeById(gv.GrievanceTypeID).Name;
                    if (gv.GrievanceCategoryID == 6 && gv.GrievanceCategoryID == 5)
                    {
                        var s = studentAggregateRepository.GetStudentById(gv.GrievanceRelatedId);
                        name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                        code = s.StudentCode;
                        email = parentRepository.GetAllParent().Where(a => a.StudentID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                        phone = parentRepository.GetAllParent().Where(a => a.StudentID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                        if (roleId == 6)
                        {
                            category = "Parent";
                        }
                        else if (roleId == 5)
                        {
                            category = "Student";
                        }
                        whom = "Student";
                    }
                    else
                    {
                        var s = employeeRepository.GetEmployeeById(gv.GrievanceRelatedId);
                        name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                        code = s.EmpCode;
                        email = s.EmailId;
                        phone = s.PrimaryMobile;
                        category = "Employee";
                        whom = "Employee";
                    }
                    var employees = employeeRepository.GetEmployeeById(empid);
                    smssend.assigngrievance(name, email, phone, code, category, typename, "New Grievance Assigned", whom, employees.EmailId);


                }


                return Json(1);
            }
            catch (Exception e1)
            {

                return Json(e1);
            }
        }

        public IActionResult VerifyGrievance(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Assign & Verify Grievance", accessId, "Verify", "Grievance", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (HttpContext.Session.GetString("attachments") != null)
            {
                HttpContext.Session.Remove("attachments");
            }

            var res = grievancemodalclass.GetAllGrievanceForView().Where(a => a.ID == id).FirstOrDefault();
            return View(res);
        }
        public async Task<JsonResult> GrievanceVerified(string desc, long gid)
        {
            try
            {
                userId = HttpContext.Session.GetInt32("userId").Value;
                long accessId = HttpContext.Session.GetInt32("accessId").Value;
                long roleId = HttpContext.Session.GetInt32("roleId").Value;
                if (commonMethods.checkaccessavailable("Assign & Verify Grievance", accessId, "Verify", "Grievance", roleId) == false)
                {
                    return Json(2);
                }
                string sms = "We are glad to inform you that your grievance has been successfully resolved by our team. Do remember, that true service to you is our ultimate aim.Do check your email for all the details.\n\n Regards \n ODM Management";
                Grievance gv = grievanceRepository.GetAllGrievance().Where(a => a.ID == gid).FirstOrDefault();
                gv.GrievanceStatusID = 5;
                gv.ModifiedId = userId;
                gv.VerifiedId = userId;
                gv.VerificationDate = DateTime.Now;
                grievanceRepository.UpdateGrievance(gv);
                if (gv.GrievanceCategoryID == 6)
                {
                    string mobile = parentRepository.GetAllParent().Where(a => a.ID == gv.GrievanceRelatedId).FirstOrDefault().PrimaryMobile;
                    if (mobile != null)
                    {
                        smssend.Sendsmstoemployee(mobile, sms);
                    }
                }
                else if (gv.GrievanceCategoryID == 5)
                {
                    var parents = parentRepository.GetAllParent().Where(a => a.StudentID == gv.GrievanceRelatedId).ToList();
                    if (parents.Count > 0)
                    {
                        foreach (var a in parents)
                        {
                            string mobile = a.PrimaryMobile;
                            if (mobile != null)
                            {
                                smssend.Sendsmstoemployee(mobile, sms);
                            }
                        }
                    }
                }
                else
                {
                    string mobile = employeeRepository.GetAllEmployee().Where(a => a.ID == gv.GrievanceRelatedId).FirstOrDefault().PrimaryMobile;
                    if (mobile != null)
                    {
                        smssend.Sendsmstoemployee(mobile, sms);
                    }
                }






                GrievanceTimeline tm = new GrievanceTimeline();
                tm.GrievanceID = gid;
                tm.Active = true;
                tm.GrievanceStatusID = 5;
                tm.InsertedId = userId;
                tm.ModifiedId = userId;
                tm.Status = EntityStatus.ACTIVE;
                grievanceRepository.CreateGrievanceTimeline(tm);
                GrievanceComment grievanceComment = new GrievanceComment();
                grievanceComment.Active = true;
                grievanceComment.Description = desc;
                grievanceComment.GrievanceID = gid;
                grievanceComment.GrievanceStatusID = 5;
                grievanceComment.InsertedId = userId;
                grievanceComment.ModifiedId = userId;
                grievanceComment.Status = "CREATED";
                long commid = grievanceRepository.CreateGrievanceComment(grievanceComment);
                List<grievanceattachments> grievanceattachments = new List<grievanceattachments>();
                if (HttpContext.Session.GetString("attachments") != null)
                {
                    grievanceattachments = JsonConvert.DeserializeObject<List<grievanceattachments>>(HttpContext.Session.GetString("attachments"));
                }
                if (grievanceattachments.Count() > 0)
                {
                    foreach (var a in grievanceattachments)
                    {

                        string getinfo = Path.GetFileName(a.path);
                        string filepath = hostingEnvironment.WebRootPath + "\\ODMImages\\Grievance\\" + gid;
                        if (!Directory.Exists(filepath))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(filepath);
                        }

                        System.IO.File.Copy(a.path, filepath + "\\" + getinfo);
                        GrievanceCommentAttachment grievanceCommentAttachment = new GrievanceCommentAttachment();
                        grievanceCommentAttachment.Active = true;
                        grievanceCommentAttachment.GrievanceCommentID = commid;
                        grievanceCommentAttachment.InsertedId = userId;
                        grievanceCommentAttachment.ModifiedId = userId;
                        grievanceCommentAttachment.Name = a.Name;
                        grievanceCommentAttachment.Path = getinfo;
                        grievanceCommentAttachment.Status = "CREATED";
                        grievanceRepository.CreateGrievanceCommentAttachment(grievanceCommentAttachment);
                    }

                    HttpContext.Session.Remove("attachments");
                }
                var gvres = grievancemodalclass.GetAllGrievanceForView().Where(a => a.ID == gid).FirstOrDefault();
                string table = "<table align='center' style='width:100%;'>";
                table += "<tr><td>Id</td><td>Complaint</td><td>Raised By</td><td>Raised On</td></tr>";
                table += "<tr><td>" + gvres.Code + "</td><td>" + gvres.Description + "</td><td>" + gvres.GrievanceRelatedName + "</td><td>" + gvres.CreatedDate.Value.ToString("dd-MMM-yyyy hh:mm tt") + "</td></tr>";
                table += "<tr><td colspan='4'><h5>Comments</h5><hr/></td></tr>";
                table += "<tr><td colspan='4'>";
                foreach (var a in gvres.grievancecommentscls)
                {
                    table += "<div><strong> Posted By:</strong>" + a.CommentBy + "<br/>";
                    table += "<strong>Posted Date Time :</strong>" + a.CommentedOn.Value.ToString("dd-MMM-yyyy hh:mm tt") + "<br/>";
                    //table += "<strong>Status :</strong>" + a.GrievanceStatusName + "</div>";
                    table += "<div><label><strong>Comment</strong></label><br/><label>" + a.Comment + "</label></div><hr/>";
                }
                table += "</td></tr>";
                table += "</table>";
                string email = "";
                if (gvres.GrievanceCategoryID == 6)
                {
                    email = parentRepository.GetAllParent().Where(a => a.ID == gvres.GrievanceRelatedId).FirstOrDefault().EmailId;
                    smssend.verifygrievance(table, "Grievance Resolved", email);
                }
                else if (gvres.GrievanceCategoryID == 5)
                {
                    var parents = parentRepository.GetAllParent().Where(a => a.StudentID == gvres.GrievanceRelatedId).ToList();
                    if (parents.Count > 0)
                    {
                        foreach (var a in parents)
                        {
                            smssend.verifygrievance(table, "Grievance Resolved", a.EmailId);
                        }
                    }
                }
                else
                {
                    email = employeeRepository.GetAllEmployee().Where(a => a.ID == gvres.GrievanceRelatedId).FirstOrDefault().EmailId;
                    if (email != null)
                    {
                        smssend.verifygrievance(table, "Grievance Resolved", email);
                    }
                }


                return Json(1);
            }
            catch
            {

                return Json(0);
            }
        }
        public async Task<JsonResult> GrievanceReassigned(string desc, long gid)
        {
            try
            {
                userId = HttpContext.Session.GetInt32("userId").Value;
                long accessId = HttpContext.Session.GetInt32("accessId").Value;
                long roleId = HttpContext.Session.GetInt32("roleId").Value;
                if (commonMethods.checkaccessavailable("Assign & Verify Grievance", accessId, "Assigned", "Grievance", roleId) == false)
                {
                    return Json(2);
                }
                Grievance gv = grievanceRepository.GetAllGrievance().Where(a => a.ID == gid).FirstOrDefault();
                gv.GrievanceStatusID = 2;
                gv.ModifiedId = userId;
                gv.CompletionId = 0;
                gv.CompletionDate = null;
                grievanceRepository.UpdateGrievance(gv);
                GrievanceTimeline tm = new GrievanceTimeline();
                tm.GrievanceID = gid;
                tm.Active = true;
                tm.GrievanceStatusID = 2;
                tm.InsertedId = userId;
                tm.ModifiedId = userId;
                tm.Status = EntityStatus.ACTIVE;
                grievanceRepository.CreateGrievanceTimeline(tm);
                GrievanceComment grievanceComment = new GrievanceComment();
                grievanceComment.Active = true;
                grievanceComment.Description = desc;
                grievanceComment.GrievanceID = gid;
                grievanceComment.GrievanceStatusID = 2;
                grievanceComment.InsertedId = userId;
                grievanceComment.ModifiedId = userId;
                grievanceComment.Status = "CREATED";
                long commid = grievanceRepository.CreateGrievanceComment(grievanceComment);
                List<grievanceattachments> grievanceattachments = new List<grievanceattachments>();
                if (HttpContext.Session.GetString("attachments") != null)
                {
                    grievanceattachments = JsonConvert.DeserializeObject<List<grievanceattachments>>(HttpContext.Session.GetString("attachments"));
                }
                if (grievanceattachments.Count() > 0)
                {
                    foreach (var a in grievanceattachments)
                    {

                        string getinfo = Path.GetFileName(a.path);
                        string filepath = hostingEnvironment.WebRootPath + "\\ODMImages\\Grievance\\" + gid;
                        if (!Directory.Exists(filepath))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(filepath);
                        }

                        System.IO.File.Copy(a.path, filepath + "\\" + getinfo);
                        GrievanceCommentAttachment grievanceCommentAttachment = new GrievanceCommentAttachment();
                        grievanceCommentAttachment.Active = true;
                        grievanceCommentAttachment.GrievanceCommentID = commid;
                        grievanceCommentAttachment.InsertedId = userId;
                        grievanceCommentAttachment.ModifiedId = userId;
                        grievanceCommentAttachment.Name = a.Name;
                        grievanceCommentAttachment.Path = getinfo;
                        grievanceCommentAttachment.Status = "CREATED";
                        grievanceRepository.CreateGrievanceCommentAttachment(grievanceCommentAttachment);
                    }

                    HttpContext.Session.Remove("attachments");
                }


                return Json(1);
            }
            catch
            {

                return Json(0);
            }
        }
        #endregion

        #region Manage Grievance

        public IActionResult ManageGrievances()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Manage Grievance", accessId, "Manage", "Grievance", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            var res = grievancemodalclass.GetAllGrievanceForView().Where(a => a.grievanceempcls != null).ToList();

            return View(res.Where(a => a.grievanceempcls.EmployeeID == userId).ToList());
        }
        public IActionResult GrievanceForEmployee(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Manage Grievance", accessId, "Manage", "Grievance", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (HttpContext.Session.GetString("attachments") != null)
            {
                HttpContext.Session.Remove("attachments");
            }

            var res = grievancemodalclass.GetAllGrievanceForView().Where(a => a.ID == id).FirstOrDefault();
            return View(res);
        }
        public IActionResult inprogressGrievance(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;

            Grievance gv = grievanceRepository.GetAllGrievance().Where(a => a.ID == id).FirstOrDefault();
            gv.GrievanceStatusID = 3;
            gv.ModifiedId = userId;
            gv.CompletionId = userId;
            gv.CompletionDate = DateTime.Now;
            grievanceRepository.UpdateGrievance(gv);
            GrievanceTimeline tm = new GrievanceTimeline();
            tm.GrievanceID = id;
            tm.Active = true;
            tm.GrievanceStatusID = 3;
            tm.InsertedId = userId;
            tm.ModifiedId = userId;
            tm.Status = EntityStatus.ACTIVE;
            grievanceRepository.CreateGrievanceTimeline(tm);
            return RedirectToAction(nameof(ManageGrievance));
        }
        public async Task<JsonResult> Grievancecompleted(string desc, long gid)
        {
            try
            {
                userId = HttpContext.Session.GetInt32("userId").Value;
                long roleId = HttpContext.Session.GetInt32("roleId").Value;
                Grievance gv = grievanceRepository.GetAllGrievance().Where(a => a.ID == gid).FirstOrDefault();
                gv.GrievanceStatusID = 4;
                gv.ModifiedId = userId;
                gv.CompletionId = userId;
                gv.CompletionDate = DateTime.Now;
                grievanceRepository.UpdateGrievance(gv);
                string name = "";

                if (gv.GrievanceCategoryID == 6 && gv.GrievanceCategoryID == 5)
                {
                    var s = studentAggregateRepository.GetStudentById(gv.GrievanceRelatedId);
                    name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                }
                else
                {
                    var s = employeeRepository.GetEmployeeById(gv.GrievanceRelatedId);
                    name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                }
                string employeename = grievancemodalclass.getEmployeeById(userId);
                var gemployees = commonMethods.GetAssignverifyGrievanceemployee();
                foreach (var a in gemployees)
                {
                    smssend.resolvegrievance(gv.Code, gv.Description, name, employeename, "Grievance Resolved", a.Code);
                }


                GrievanceTimeline tm = new GrievanceTimeline();
                tm.GrievanceID = gid;
                tm.Active = true;
                tm.GrievanceStatusID = 4;
                tm.InsertedId = userId;
                tm.ModifiedId = userId;
                tm.Status = EntityStatus.ACTIVE;
                grievanceRepository.CreateGrievanceTimeline(tm);
                GrievanceComment grievanceComment = new GrievanceComment();
                grievanceComment.Active = true;
                grievanceComment.Description = desc;
                grievanceComment.GrievanceID = gid;
                grievanceComment.GrievanceStatusID = 4;
                grievanceComment.InsertedId = userId;
                grievanceComment.ModifiedId = userId;
                grievanceComment.Status = "CREATED";
                long commid = grievanceRepository.CreateGrievanceComment(grievanceComment);
                List<grievanceattachments> grievanceattachments = new List<grievanceattachments>();
                if (HttpContext.Session.GetString("attachments") != null)
                {
                    grievanceattachments = JsonConvert.DeserializeObject<List<grievanceattachments>>(HttpContext.Session.GetString("attachments"));
                }
                if (grievanceattachments.Count() > 0)
                {
                    foreach (var a in grievanceattachments)
                    {

                        string getinfo = Path.GetFileName(a.path);
                        string filepath = hostingEnvironment.WebRootPath + "\\ODMImages\\Grievance\\" + gid;
                        if (!Directory.Exists(filepath))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(filepath);
                        }

                        System.IO.File.Copy(a.path, filepath + "\\" + getinfo);
                        GrievanceCommentAttachment grievanceCommentAttachment = new GrievanceCommentAttachment();
                        grievanceCommentAttachment.Active = true;
                        grievanceCommentAttachment.GrievanceCommentID = commid;
                        grievanceCommentAttachment.InsertedId = userId;
                        grievanceCommentAttachment.ModifiedId = userId;
                        grievanceCommentAttachment.Name = a.Name;
                        grievanceCommentAttachment.Path = getinfo;
                        grievanceCommentAttachment.Status = "CREATED";
                        grievanceRepository.CreateGrievanceCommentAttachment(grievanceCommentAttachment);
                    }

                    HttpContext.Session.Remove("attachments");
                }


                return Json(1);
            }
            catch
            {

                return Json(0);
            }
        }

        #endregion

        #region Public Grievance Form

        public IActionResult GrievanceForm()
        {
            return View();
        }
        public IActionResult StaffGrievanceForm()
        {
            publicgrievancecls publicgrievancecls = new publicgrievancecls();
            publicgrievancecls.staffcode = "";
            publicgrievancecls.staffname = "";
            ViewBag.errormsg = "";
            return View(publicgrievancecls);
        }
        [HttpGet]
        public IActionResult RaiseGrievanceByStaff(publicgrievancecls publicgrievancecls)
        {
            var employee = employeeRepository.GetAllEmployee().Where(a => a.EmpCode == publicgrievancecls.staffcode.Trim()).FirstOrDefault();
            if (employee == null)
            {
                ViewBag.errormsg = "Invalid Employee Code";
                return RedirectToAction(nameof(StaffGrievanceForm));
            }
            else
            {
                var designation = commonMethods.GetAllDesignationByUsingEmployeeId(employee.ID);

                staffGrievanceDetails staffGrievance = new staffGrievanceDetails();
                staffGrievance.EmployeeName = employee.FirstName + " " + (employee.MiddleName == null ? employee.LastName : (employee.MiddleName + " " + employee.LastName));
                staffGrievance.Empcode = employee.EmpCode;
                staffGrievance.EmployeeId = employee.ID;
                staffGrievance.employeeDesignations = designation;
                staffGrievance.GrievanceTypeID = 0;
                staffGrievance.Description = "";
                staffGrievance.types = grievanceRepository.GetAllGrievanceType().Where(a => a.RoleID == 4).ToList();

                return View(staffGrievance);
            }
        }
        public async Task<JsonResult> SaveStaffGrievance(long type, string description, long empid)
        {
            try
            {
                long roleId = 4;
                Grievance grievance = new Grievance();
                grievance.Description = description;
                grievance.GrievanceTypeID = type;
                grievance.ModifiedId = empid;
                grievance.RequestType = "PUBLIC";
                grievance.Active = true;
                grievance.GrievanceRelatedId = empid;
                grievance.GrievanceCategoryID = roleId;
                grievance.GrievanceStatusID = 1;
                grievance.InsertedId = empid;
                grievance.Code = commonMethods.GetGrievancecode(roleId);
                grievance.Status = EntityStatus.ACTIVE;
                long id = grievanceRepository.CreateGrievance(grievance);
                long relatedid = grievance.GrievanceRelatedId;
                GrievanceTimeline tm = new GrievanceTimeline();
                tm.GrievanceID = id;
                tm.Active = true;
                tm.GrievanceStatusID = 1;
                tm.InsertedId = empid;
                tm.ModifiedId = empid;
                tm.Status = EntityStatus.ACTIVE;
                grievanceRepository.CreateGrievanceTimeline(tm);
                List<grievanceattachments> grievanceattachments = new List<grievanceattachments>();
                if (HttpContext.Session.GetString("attachments") != null)
                {
                    grievanceattachments = JsonConvert.DeserializeObject<List<grievanceattachments>>(HttpContext.Session.GetString("attachments"));
                }
                if (grievanceattachments.Count() > 0)
                {
                    foreach (var a in grievanceattachments)
                    {

                        string getinfo = Path.GetFileName(a.path);
                        string filepath = hostingEnvironment.WebRootPath + "\\ODMImages\\Grievance\\" + id;
                        if (!Directory.Exists(filepath))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(filepath);
                        }

                        System.IO.File.Copy(a.path, filepath + "\\" + getinfo);
                        GrievanceAttachment grievanceAttachment = new GrievanceAttachment();
                        grievanceAttachment.Active = true;
                        grievanceAttachment.GrievanceID = id;
                        grievanceAttachment.InsertedId = empid;
                        grievanceAttachment.ModifiedId = empid;
                        grievanceAttachment.Name = a.Name;
                        grievanceAttachment.Path = getinfo;
                        grievanceAttachment.Status = "CREATED";
                        grievanceRepository.CreateGrievanceAttachment(grievanceAttachment);
                    }
                    HttpContext.Session.Remove("attachments");
                }
                string sms = "We are in receipt of your grievance and we regret the inconvenience caused. Our team will be trying to fix this grievance as soon as possible and get back to you.\n\n Regards \n ODM Management";
                string name = "";
                string code = "";
                string email = "";
                string phone = "";
                string category = "";
                string whom = "";
                string typename = grievanceRepository.GetGrievanceTypeById(type).Name;

                string mobile = employeeRepository.GetAllEmployee().Where(a => a.ID == empid).FirstOrDefault().PrimaryMobile;
                if (mobile != null)
                {
                    smssend.Sendsmstoemployee(mobile, sms);
                }

                var employees = commonMethods.GetAssignverifyGrievanceemployee();

                var s = employeeRepository.GetEmployeeById(relatedid);
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.EmpCode;
                email = s.EmailId;
                phone = s.PrimaryMobile;
                category = "Employee";
                whom = "Employee";

                foreach (var a in employees)
                {
                    smssend.newgrievance(name, email, phone, code, category, typename, "New Grievance Request", whom, a.Code);
                }
                return Json(1);
            }
            catch (Exception e1)
            {
                return Json(0);
            }
        }
        public async Task<JsonResult> SaveStudentGrievance(long type, string description, long empid)
        {
            try
            {
                long roleId = 5;
                Grievance grievance = new Grievance();
                grievance.Description = description;
                grievance.GrievanceTypeID = type;
                grievance.ModifiedId = empid;
                grievance.RequestType = "PUBLIC";
                grievance.Active = true;
                grievance.GrievanceRelatedId = empid;
                grievance.GrievanceCategoryID = roleId;
                grievance.GrievanceStatusID = 1;
                grievance.InsertedId = empid;
                grievance.Code = commonMethods.GetGrievancecode(roleId);
                grievance.Status = EntityStatus.ACTIVE;
                long id = grievanceRepository.CreateGrievance(grievance);
                long relatedid = grievance.GrievanceRelatedId;
                GrievanceTimeline tm = new GrievanceTimeline();
                tm.GrievanceID = id;
                tm.Active = true;
                tm.GrievanceStatusID = 1;
                tm.InsertedId = empid;
                tm.ModifiedId = empid;
                tm.Status = EntityStatus.ACTIVE;
                grievanceRepository.CreateGrievanceTimeline(tm);
                List<grievanceattachments> grievanceattachments = new List<grievanceattachments>();
                if (HttpContext.Session.GetString("attachments") != null)
                {
                    grievanceattachments = JsonConvert.DeserializeObject<List<grievanceattachments>>(HttpContext.Session.GetString("attachments"));
                }
                if (grievanceattachments.Count() > 0)
                {
                    foreach (var a in grievanceattachments)
                    {

                        string getinfo = Path.GetFileName(a.path);
                        string filepath = hostingEnvironment.WebRootPath + "\\ODMImages\\Grievance\\" + id;
                        if (!Directory.Exists(filepath))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(filepath);
                        }

                        System.IO.File.Copy(a.path, filepath + "\\" + getinfo);
                        GrievanceAttachment grievanceAttachment = new GrievanceAttachment();
                        grievanceAttachment.Active = true;
                        grievanceAttachment.GrievanceID = id;
                        grievanceAttachment.InsertedId = empid;
                        grievanceAttachment.ModifiedId = empid;
                        grievanceAttachment.Name = a.Name;
                        grievanceAttachment.Path = getinfo;
                        grievanceAttachment.Status = "CREATED";
                        grievanceRepository.CreateGrievanceAttachment(grievanceAttachment);
                    }
                    HttpContext.Session.Remove("attachments");
                }
                string sms = "We are in receipt of your grievance and we regret the inconvenience caused. Our team will be trying to fix this grievance as soon as possible and get back to you.\n\n Regards \n ODM Management";
                string name = "";
                string code = "";
                string email = "";
                string phone = "";
                string category = "";
                string whom = "";
                string typename = grievanceRepository.GetGrievanceTypeById(type).Name;
                if (roleId == 5)
                {
                    var parents = parentRepository.GetAllParent().Where(a => a.StudentID == userId).ToList();
                    if (parents.Count > 0)
                    {
                        foreach (var a in parents)
                        {
                            string mobile = a.PrimaryMobile;
                            if (mobile != null)
                            {
                                smssend.Sendsmstoemployee(mobile, sms);
                            }
                        }
                    }
                }

                var employees = commonMethods.GetAssignverifyGrievanceemployee();
                var s = studentAggregateRepository.GetStudentById(relatedid);
                name = s.FirstName + " " + (s.MiddleName == null ? s.LastName : (s.MiddleName + " " + s.LastName));
                code = s.StudentCode;
                email = parentRepository.GetAllParent().Where(a => a.StudentID == s.ID && a.EmailId != null).FirstOrDefault().EmailId;
                phone = parentRepository.GetAllParent().Where(a => a.StudentID == s.ID && a.PrimaryMobile != null).FirstOrDefault().PrimaryMobile;
                if (roleId == 6)
                {
                    category = "Parent";
                }
                else if (roleId == 5)
                {
                    category = "Student";
                }
                whom = "Student";

                foreach (var a in employees)
                {
                    smssend.newgrievance(name, email, phone, code, category, typename, "New Grievance Request", whom, a.Code);
                }
                return Json(1);
            }
            catch (Exception e1)
            {
                return Json(0);
            }
        }
        public IActionResult StudentGrievanceForm()
        {
            return View();
        }
        [HttpGet]
        public IActionResult RaiseGrievanceByStudent(publicgrievancecls publicgrievancecls)
        {
            var stdId = studentAggregateRepository.GetAllStudent().Where(a => a.StudentCode == publicgrievancecls.studentcode.Trim()).FirstOrDefault();
            if (stdId == null)
            {
                ViewBag.errormsg = "Invalid Student Id";
                return RedirectToAction(nameof(StudentGrievanceForm));
            }
            else
            {
                var studentclass = commonsqlquery.Get_view_All_Academic_Students().Where(a => a.StudentId == stdId.ID && a.IsAvailable == true).FirstOrDefault();
                studentGrievanceDetails staffGrievance = new studentGrievanceDetails();
                staffGrievance.StudentName = stdId.FirstName + " " + (stdId.MiddleName == null ? stdId.LastName : (stdId.MiddleName + " " + stdId.LastName));
                staffGrievance.Studentcode = stdId.StudentCode;
                staffGrievance.StudentId = stdId.ID;
                staffGrievance.wing = studentclass.WingName;
                staffGrievance.school = studentclass.OraganisationName;
                staffGrievance.section = studentclass.SessionName;

                staffGrievance.GrievanceTypeID = 0;
                staffGrievance.Description = "";
                staffGrievance.types = grievanceRepository.GetAllGrievanceType().Where(a => a.RoleID == 5 || a.RoleID == 6).ToList();

                return View(staffGrievance);
            }
        }

        public IActionResult SuccessfullyRaised()
        {
            return View();
        }
        #endregion

        #region Grievance

        //Grievance List 
        public IActionResult Grievance()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            ViewBag.GrievanceTypes = grievanceRepository.GetAllGrievanceType();
            ViewBag.GrievanceStatus = grievanceRepository.GetAllGrievanceStatus();
            ViewBag.employees = employeeRepository.GetAllEmployee();

            var student = studentAggregateRepository.GetAllStudent();
            var parent = parentRepository.GetAllParent();
            var grievancetype = grievanceRepository.GetAllGrievanceType();
            var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
            var grievance = grievanceRepository.GetAllGrievance();

            var res = (from a in grievance
                           //join b in student on a.StudentID equals b.ID
                           // join c in parent on a.ParentID equals c.ID
                       join d in grievancetype on a.GrievanceTypeID equals d.ID
                       join e in grievancestatus on a.GrievanceStatusID equals e.ID
                       select new StudentGrievancecls
                       {
                           //  StudentName = b.FirstName + " " + b.MiddleName + " " + b.LastName,
                           // ParentName = c.FirstName + " " + c.MiddleName + " " + c.LastName,
                           GrievanceTypeName = d.Name,
                           GrievanceStatusName = e.Name,
                           Description = a.Description,
                           ModifiedDate = a.ModifiedDate,
                           ID = a.ID
                       }).ToList();

            // return View(grievanceRepository.GetAllGrievance());

            return View(res);
        }



        public JsonResult GetAllStatus()
        {
            var res = grievanceRepository.GetAllGrievanceStatus();
            return Json(res);
        }

        #endregion

        #region GrievanceType

        //GrievancTypes List 
        public IActionResult GrievanceType()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Grievance Category", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(grievanceRepository.GetAllGrievanceCategory());
        }

        public IActionResult CreateOrEditGrievanceType(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Grievance Category", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = ViewMethod.CREATE;
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Grievance Category", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = ViewMethod.UPDATE;
                var grievanceCategory = grievanceRepository.GetGrievanceCategoryById(id.Value);
                od.ID = grievanceCategory.ID;
                od.Name = grievanceCategory.Name;
            }
            return View(od);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateGrievanceType([Bind("ID,Name")] GrievanceCategory grievanceCategory)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                grievanceCategory.ModifiedId = userId;
                grievanceCategory.Active = true;
                if (grievanceCategory.ID == 0)
                {
                    var duplicate = grievanceRepository.GetAllGrievanceCategory().Where(a => a.Active == true && a.Name == grievanceCategory.Name).ToList();
                    if (duplicate.Count() == 0)
                    {
                        grievanceCategory.InsertedId = userId;
                        grievanceCategory.Status = EntityStatus.ACTIVE;
                        var ids = grievanceRepository.CreateGrievanceCategory(grievanceCategory);

                        if(ids > 0)
                        {
                            DataTimeline dataTimeline = new DataTimeline();
                            dataTimeline.InsertedId = userId;
                            dataTimeline.ModifiedId = userId;
                            dataTimeline.EntityId = ids;
                            dataTimeline.EntityName = grievanceCategory.Name;
                            dataTimeline.Timeline = "Created on " + DateTime.Now + " by " + GetEmployeeNameByEmpId(userId);
                            dataTimelineRepository.CreateDataTimeline(dataTimeline);
                        }
                    }
                    else
                    {
                        return RedirectToAction(nameof(GrievanceType));
                    }
                }
                else
                {
                    var duplicate = grievanceRepository.GetAllGrievanceCategory().Where(a => a.Active == true && a.Name == grievanceCategory.Name).ToList();
                    if (duplicate.Count() == 0)
                    {
                        grievanceRepository.UpdateGrievanceCategory(grievanceCategory);
                    }
                }

                return RedirectToAction(nameof(GrievanceType));
            }
            return View(grievanceCategory);
        }


        // POST: GrievanceTypes/Delete/5
        [ActionName("DeleteGrievanceType")]
        public async Task<IActionResult> GrievanceTypeDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Grievance Category", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (GrievanceTypeExists(id.Value) == true)
            {
                var grievanceCategory = grievanceRepository.DeleteGrievanceCategory(id.Value);
            }
            return RedirectToAction(nameof(GrievanceType));
        }

        private bool GrievanceTypeExists(long id)
        {
            if (grievanceRepository.GetGrievanceCategoryById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion

        #region GrievanceForEmployee



        #endregion

        #region GrievanceForStudent

        public IActionResult GrievanceForStudent(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            var res = grievancemodalclass.GetAllGrievanceForView().Where(a => a.GrievanceCategoryID == roleId && a.ID == id).FirstOrDefault();
            return View(res);
        }

        #endregion

        #region GrievanceForParent

        public IActionResult GrievanceForParent(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            var res = grievancemodalclass.GetAllGrievanceForView().Where(a => a.GrievanceCategoryID == roleId && a.ID == id).FirstOrDefault();
            return View(res);
        }

        #endregion

        #region GrievancePublicForm

        public IActionResult GrievancePublicForm()
        {
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;

            GrievancePublicFormCls Grievancepubfrm = new GrievancePublicFormCls();

            //if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            // var res = grievancemodalclass.GetAllGrievanceForView().Where(a=>a.grievanceempcls.EmployeeID==id).FirstOrDefault();
            //var res = grievancemodalclass.GetAllGrievanceForView().Where(a => a.GrievanceCategoryID == roleId && a.ID == id).FirstOrDefault();
            return View("GrievancePublicForm");
        }
        #endregion
        public IActionResult AddGrievance()
        {
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;

            GrievancePublicFormCls Grievancepubfrm = new GrievancePublicFormCls();

            //if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            // var res = grievancemodalclass.GetAllGrievanceForView().Where(a=>a.grievanceempcls.EmployeeID==id).FirstOrDefault();
            //var res = grievancemodalclass.GetAllGrievanceForView().Where(a => a.GrievanceCategoryID == roleId && a.ID == id).FirstOrDefault();
            return View();
        }


        public JsonResult GetEmployee(string id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            var emp = employeeRepository.GetAllEmployee();
            var empdes = employeeDesignationRepository.GetAllEmployeeDesignations();
            var des = designationRepository.GetAllDesignation();
            var org = organisationRepository.GetAllOrganization();
            var dept = departmentRepository.GetAllDepartment();

            var res = (from a in emp
                       join b in empdes on a.ID equals b.EmployeeID
                       join c in des on b.DesignationID equals c.ID
                       join d in dept on c.DepartmentID equals d.ID
                       join e in org on d.OrganizationID equals e.ID

                       select new GrievancePublicFormCls
                       {
                           EmployeeName = a.FirstName + " " + a.MiddleName + " " + a.LastName,
                           Organisation = e.Name,
                           Department = d.Name,
                           EmpCode = a.EmpCode,
                           EmployeeID = a.ID
                       }).ToList().Where(w => w.EmpCode == id).FirstOrDefault();

            return Json(res);
        }

        public JsonResult GetStudent(string id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            var stud = studentAggregateRepository.GetAllStudent();
            //var org = organisationRepository.GetAllOrganization();


            var res = (from a in stud

                       select new GrievancePublicFormCls
                       {
                           StudentName = a.FirstName + " " + a.MiddleName + " " + a.LastName,
                           StudentID = a.ID,
                           StudentCode = a.StudentCode
                       }).ToList().Where(w => w.StudentCode == id).FirstOrDefault();

            return Json(res);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SavePublicGrievance([Bind("ID,Name")] Grievance grievance)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                grievance.ModifiedId = userId;
                grievance.Active = true;
                if (grievance.ID == 0)
                {
                    grievance.InsertedId = userId;
                    grievance.Status = EntityStatus.ACTIVE;
                    grievanceRepository.CreateGrievance(grievance);
                }
                else
                {
                    grievanceRepository.UpdateGrievance(grievance);
                }

                return RedirectToAction(nameof(Grievance));
            }
            return View(grievance);
        }


    }
}


