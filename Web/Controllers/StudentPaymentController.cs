﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Web.Models;
using OdmErp.Web.Models.StudentPayment;
using Web.Controllers;
using X.PagedList;
using X.PagedList.Mvc;
using OfficeOpenXml;
using ClosedXML.Excel;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using System.Data;
using OdmErp.ApplicationCore.Entities.Pagination;

namespace OdmErp.Web.Controllers
{
    public class StudentPaymentController : AppController
    {
        private CommonMethods commonMethods;
        private IMessagesContentRepository messagesContentRepository;
        private IFeesTypeApplicableRepository feesTypeApplicableRepository;
        private IFeesTypeRepository feesTypeRepository;
        private IScholarshipApplicableRepository scholarshipApplicableRepository;

        private ICountryRepository countryRepository;
        private IAdmissionTypeRepository admissionTypeRepository;
        private IBankTypeRepository bankTypeRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private IBoardRepository boardRepository;
        private IBoardStandardRepository boardStandardRepository;
        private IDesignationLevelRepository designationLevelRepository;
        private IEducationQualificationTypeRepository educationQualificationTypeRepository;
        private IGroupRepository groupRepository;
        private IModuleRepository moduleRepository;
        private IOrganizationTypeRepository organizationTypeRepository;
        private IRoleRepository roleRepository;
        private ISectionRepository sectionRepository;

        private IStateRepository stateRepository;
        private IDepartmentRepository departmentRepository;
        private IDesignationRepository designationRepository;
        private IEducationQualificationRepository educationQualificationRepository;
        private IStandardRepository standardRepository;
        private IStreamRepository streamRepository;
        private ISubModuleRepository subModuleRepository;
        private ICityRepository cityRepository;
        private IOrganizationRepository organizationRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IReligionTypeRepository religionTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private IBankRepository bankRepository;
        private IBankBranchRepository bankBranchRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IAccessTypeRepository accessTypeRepository;
        private IActionRepository actionRepository;
        private IAuthorityAggregateRepository authorityAggregateRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IActionAccessRepository actionAccessRepository;
        private IDepartmentLeadRepository departmentLeadRepository;
        private IEmployeeRepository employeeRepository;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IHttpContextAccessor _httpContextAccessor;
        public Extensions.SessionExtensions sessionExtensions;
        public IAcademicSessionRepository academicSessionRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IClassTimingRepository classTimingRepository;
        public commonsqlquery commonsql;

        private readonly IFileProvider fileProvider;
        private readonly IHostingEnvironment hostingEnvironment;

        public StudentPaymentController(IFileProvider fileprovider, IHostingEnvironment env, IMessagesContentRepository messageContentRepo,IFeesTypeApplicableRepository feesTypeApplicableRepo, IFeesTypeRepository feesTypeRepo, IScholarshipApplicableRepository scholarshipApplicableRepo, ICountryRepository countryRepo, IAdmissionTypeRepository admissionTypeRepo, IBloodGroupRepository bloodGroupRepo,
            IBoardRepository boardRepo, IDesignationLevelRepository designationLevelRepo, IEducationQualificationTypeRepository educationQualificationTypeRepo, IGroupRepository groupRepo,
            IModuleRepository moduleRepo, IOrganizationTypeRepository organizationTypeRepo, IRoleRepository roleRepo, ISectionRepository sectionRepo,
            IStateRepository stateRepo, IDepartmentRepository departmentRepo, IDesignationRepository designationRepo, IEducationQualificationRepository educationQualificationRepo, IStandardRepository standardRepo,
            IStreamRepository streamRepo, ISubModuleRepository subModuleRepo, ICityRepository cityRepo, IOrganizationRepository organizationRepo, INationalityTypeRepository nationalityTypeRepo, IReligionTypeRepository religionTypeRepo,
            IAddressTypeRepository addressTypeRepo, IBankRepository bankRepo, IBankBranchRepository bankBranchRepo, IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo,
            IAccessTypeRepository accessTypeRepo, IActionRepository actionRepo, CommonMethods commonMeth, IAuthorityAggregateRepository authorityAggregateRepo, IStudentAggregateRepository studentAggregateRepo, IActionAccessRepository actionAccessRepo,
            IDepartmentLeadRepository departmentlead, IHttpContextAccessor httpContextAccessor, IEmployeeRepository emprepo, IEmployeeDesignationRepository empdesrepo, IBankTypeRepository bankTypeRepo,
            IBoardStandardRepository boardStandardRepository, IClassTimingRepository classTimingRepo, IAcademicSessionRepository academicSessionRepository, commonsqlquery commonsqlquery, IOrganizationAcademicRepository organizationAcademicRepository
          )
        {
            fileProvider = fileprovider;
            hostingEnvironment = env;
            messagesContentRepository = messageContentRepo;
            feesTypeApplicableRepository = feesTypeApplicableRepo;
            scholarshipApplicableRepository = scholarshipApplicableRepo;
            feesTypeRepository = feesTypeRepo;
            countryRepository = countryRepo;
            admissionTypeRepository = admissionTypeRepo;
            bloodGroupRepository = bloodGroupRepo;
            boardRepository = boardRepo;
            designationLevelRepository = designationLevelRepo;
            educationQualificationTypeRepository = educationQualificationTypeRepo;
            groupRepository = groupRepo;
            moduleRepository = moduleRepo;
            organizationTypeRepository = organizationTypeRepo;
            roleRepository = roleRepo;
            sectionRepository = sectionRepo;
            bankTypeRepository = bankTypeRepo;
            stateRepository = stateRepo;
            departmentRepository = departmentRepo;
            designationRepository = designationRepo;
            educationQualificationRepository = educationQualificationRepo;
            standardRepository = standardRepo;
            streamRepository = streamRepo;
            subModuleRepository = subModuleRepo;
            cityRepository = cityRepo;
            organizationRepository = organizationRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            religionTypeRepository = religionTypeRepo;
            addressTypeRepository = addressTypeRepo;
            bankRepository = bankRepo;
            bankBranchRepository = bankBranchRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            accessTypeRepository = accessTypeRepo;
            actionRepository = actionRepo;
            commonMethods = commonMeth;
            authorityAggregateRepository = authorityAggregateRepo;
            studentAggregateRepository = studentAggregateRepo;
            actionAccessRepository = actionAccessRepo;
            _httpContextAccessor = httpContextAccessor;
            departmentLeadRepository = departmentlead;
            employeeRepository = emprepo;
            employeeDesignationRepository = empdesrepo;
            this.boardStandardRepository = boardStandardRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            commonsql = commonsqlquery;
            classTimingRepository = classTimingRepo;

        }
        #region FeesTypeApplicable
        public IActionResult FeesTypeApplicable(int? page, int? noofData)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesTypeApplicable", accessId, "List", "StudentPayment", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var pageNumber = page ?? 1;
            var PageSize = noofData ?? 10;
            var onePageOfEmps = feesTypeApplicableRepository.GetAllFeesTypeApplicable(pageNumber, PageSize).ToPagedList();//_context.Emps.ToPagedList(pageNumber, PageSize);
             return  View("FeesTypeApplicable", onePageOfEmps);
                                      // return View(feesTypeApplicableRepository.GetAllFeesTypeApplicable());
        }

        [HttpPost]
        public IActionResult LoadData()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            //Get Sort columns value
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;
            PaginationFeestypeApplicable objpg = feesTypeApplicableRepository.GetPaginationAllFeesTypeApplicable(draw, sortColumn, sortColumnDir, pageSize, skip, totalRecords, searchValue);
            return Json(objpg);
        }

        //public IActionResult CreateOrEditFeeTypeApplicable()
        //{
        //    CSFeesTypeApplicable obj = new CSFeesTypeApplicable();
        //    obj.ID = 0;
        //    obj.Name = "";
        //    return View(obj);
        //}
        [HttpGet]
        public JsonResult CreateOrEditFeeTypeApplicable(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            ApplicationCore.Entities.FeesTypeApplicable od = new ApplicationCore.Entities.FeesTypeApplicable();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Create", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Edit", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var fessTypeApplicale = feesTypeApplicableRepository.GetFeesTypeApplicableById(id.Value);
                od.ID = fessTypeApplicale.ID;
                od.Name = fessTypeApplicale.Name;
                od.Description = fessTypeApplicale.Description;

            }
            return Json(od);
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SaveOrUpdateFeesTypeApplicable(CS_FeesTypeApplicable feesTypeApplicable)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                OdmErp.ApplicationCore.Entities.FeesTypeApplicable feesTypeApplicable1 = new FeesTypeApplicable();

                feesTypeApplicable1.Name = feesTypeApplicable.Name;
                feesTypeApplicable1.Description = feesTypeApplicable.Description;



                feesTypeApplicable1.Active = true;
                if (feesTypeApplicable1.ID == 0)
                {
                    feesTypeApplicable1.InsertedId = userId;
                    feesTypeApplicable1.Status = EntityStatus.ACTIVE;
                    feesTypeApplicable.ReturnMsg = "Records added Successfully.";
                    if (FeesTypeApplicableExists(feesTypeApplicable.Name) == true)
                    {
                        feesTypeApplicableRepository.CreateFeesTypeApplicable(feesTypeApplicable1);
                    }
                    else
                    {
                        feesTypeApplicable.ReturnMsg = "Records already Exists !!!";
                    }
                    //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Saved successfully");
                    //return Json(feesTypeApplicable.ReturnMsg);
                }
                //else
                //{
                //    //TODO
                //    feesTypeApplicable1.ModifiedId = userId;
                //    feesTypeApplicableRepository.UpdateFeesTypeApplicable(feesTypeApplicable1);
                //    feesTypeApplicable.ReturnMsg = "Records Updated Successfully.";
                //    //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Updated successfully");
                //    //return Json("");
                //}


            }
            return Json(feesTypeApplicable); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        [HttpPost]
        public JsonResult Update(CS_FeesTypeApplicable feesTypeApplicable)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                OdmErp.ApplicationCore.Entities.FeesTypeApplicable feesTypeApplicable1 = new FeesTypeApplicable();
                feesTypeApplicable1.ID = feesTypeApplicable.ID;
                feesTypeApplicable1.Name = feesTypeApplicable.Name;
                feesTypeApplicable1.Description = feesTypeApplicable.Description;



                feesTypeApplicable1.Active = true;

                //TODO
                feesTypeApplicable1.ModifiedId = userId;
                feesTypeApplicableRepository.UpdateFeesTypeApplicable(feesTypeApplicable1);
                feesTypeApplicable.ReturnMsg = "Records Updated Successfully.";
                //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Updated successfully");
                //return Json("");



            }
            return Json(feesTypeApplicable); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }


        //[ActionName("DeleteFeesTypeApplicable")]
        public JsonResult DeleteFeesTypeApplicable(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Delete", "StudentPayment", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            if (FeesTypeApplicableExists1(id.Value) == true)
            {
                var feesTypeApplicable = feesTypeApplicableRepository.DeleteFeesTypeApplicable(id.Value);
            }
            return Json("Deleted successfully");
        }

        public JsonResult DeleteFeesTypeApplicableAll()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Delete", "StudentPayment", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }

            var feesTypeApplicable = feesTypeApplicableRepository.DeleteAllFeesTypeApplicable();

            return Json("Deleted successfully");
        }
        private bool FeesTypeApplicableExists1(long id)
        {
            if (feesTypeApplicableRepository.GetFeesTypeApplicableById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool FeesTypeApplicableExists(string res)
        {
            if (feesTypeApplicableRepository.GetFeesTypeApplicableByName(res) != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public JsonResult ActiveDeactiveFeesTypeApplicable(string id, string IsActive)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Update", "FeesType Applicable", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {

                var sessioschalar = feesTypeApplicableRepository.GetFeesTypeApplicableById(Convert.ToInt64(id));
                OdmErp.ApplicationCore.Entities.FeesTypeApplicable objScholarship = new FeesTypeApplicable();
                sessioschalar.ID = Convert.ToInt64(id);
                sessioschalar.Status = IsActive;

                sessioschalar.Active = true;

                //TODO
                sessioschalar.ModifiedId = userId;
                feesTypeApplicableRepository.UpdateFeesTypeApplicable(sessioschalar);
                objScholar.ReturnMsg = sessioschalar.Status.ToUpper() + " Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        private string Get_FeesTypeApplicable()
        {
            return "";
        }



        #endregion

        #region FeesType
        public IActionResult FeesType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesTypeApplicable", accessId, "List", "StudentPayment", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(feesTypeRepository.GetAllFeesType());
        }

        [HttpPost]
        public IActionResult LoadDataFeesType111()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            //Get Sort columns value
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;
            PaginationFeesType objpg = feesTypeRepository.GetPaginationAllFeesType(draw, sortColumn, sortColumnDir, pageSize, skip, totalRecords, searchValue);
            return Json(objpg);
        }

        [HttpGet]
        public JsonResult CreateOrEditFeeType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            ApplicationCore.Entities.FeesType od = new ApplicationCore.Entities.FeesType();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("FeesType ", accessId, "Create", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("FeesType ", accessId, "Edit", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Update";
                var fessType = feesTypeRepository.GetFeesTypeById(id.Value);
                od.ID = fessType.ID;
                od.Name = fessType.Name;
                od.Description = fessType.Description;
                //od.IsMandatory = fessType.IsMandatory;
                od.IsSubType = fessType.IsSubType;
                // od.DeadLineDate = fessType.DeadLineDate;
                //od.FeesTypeApplicableID = fessType.FeesTypeApplicableID;
                od.ParentFeeTypeID = fessType.ParentFeeTypeID;
                od.ParentFeeTypeName = fessType.ParentFeeTypeName;
                od.HaveInstallment = fessType.HaveInstallment;
                od.LateFeePercentageType = fessType.LateFeePercentageType;
                od.LateFeeAmountType = fessType.LateFeeAmountType;
            }
            return Json(od);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SaveOrUpdateFeesType(CS_FeesType feesType)
        {
            string ReturnMsg = "Error !!!";
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                OdmErp.ApplicationCore.Entities.FeesType feesType1 = new FeesType();

                feesType1.Name = feesType.Name;
                feesType1.Description = feesType.Description;
                feesType1.IsSubType = feesType.IsSubType;
                if (feesType.ParentFeeTypeID == null)
                {
                    feesType1.ParentFeeTypeID = 0;
                }
                else
                {
                    feesType1.ParentFeeTypeID = feesType.ParentFeeTypeID;
                }
                
                feesType1.ParentFeeTypeName = feesType.ParentFeeTypeName;
                feesType1.Status = "ACTIVE";
                feesType1.Active = true;
                feesType1.HaveInstallment = feesType.HaveInstallment;
                feesType1.LateFeePercentageType = feesType.LateFeePercentageType;
                feesType1.LateFeeAmountType = feesType.LateFeeAmountType;
                if (feesType1.ID == 0)
                {
                    feesType1.InsertedId = userId;
                    feesType1.Status = EntityStatus.ACTIVE;
                    
                    if (FeesTypeExists(feesType.Name) == true)
                    {
                        feesTypeRepository.CreateFeesType(feesType1);
                        ReturnMsg = "Records added Successfully.";
                    }
                    else
                    {
                       ReturnMsg = "Records already Exists !!!";
                    }
                    
                }
            }
            return Json(ReturnMsg); 
        }

        [HttpPost]
        public JsonResult UpdateFeesType(FeesType feesType)
        {
            string ReturnMsg = ""; 
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                OdmErp.ApplicationCore.Entities.FeesType feesType1 = new FeesType();
                feesType1.ID = feesType.ID;
                feesType1.Name = feesType.Name;
                feesType1.Description = feesType.Description;
                feesType1.IsSubType = feesType.IsSubType;
                feesType1.ParentFeeTypeID = feesType.ParentFeeTypeID;
                feesType1.ParentFeeTypeName = feesType.ParentFeeTypeName;
                feesType1.Active = true;
                feesType1.HaveInstallment = feesType.HaveInstallment;
                feesType1.Status = "ACTIVE";
                feesType1.ModifiedId = userId;
                feesType1.LateFeePercentageType = feesType.LateFeePercentageType;
                feesType1.LateFeeAmountType = feesType.LateFeeAmountType;
                feesTypeRepository.UpdateFeesType(feesType1);
                ReturnMsg = "Records Updated Successfully.";
            }
            return Json(ReturnMsg);
        }

        [HttpPost]
        public JsonResult UpdateFeesTypeParent(long id,bool HaveChild)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                OdmErp.ApplicationCore.Entities.FeesType feesType1 = new FeesType();
                feesType1.ID = id;
                feesType1.HaveChild = HaveChild;
                feesTypeRepository.UpdateFeesTypeParent(id,HaveChild);
            }
            return Json("done"); 
        }


        public JsonResult DeleteFeesType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesType ", accessId, "Delete", "FeesTypePrice", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            if (FeesTypeExists1(id.Value) == true)
            {
                var feesTypePrice = feesTypeRepository.DeleteFeesType(id.Value);
            }
            return Json("Deleted successfully");
        }

        public JsonResult DeleteFeesTypePriceAll()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesType Price", accessId, "Delete", "FeesTypePrice", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }

            var feesTypeApplicable = feesTypeRepository.DeleteAllFeesType();

            return Json("Deleted successfully");
        }

        private bool FeesTypeExists1(long id)
        {
            if (feesTypeRepository.GetFeesTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool FeesTypeExists(string res)
        {
            if (feesTypeRepository.GetFeesTypeByName(res) != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<FeesType> Get_FeesType()
        {

            List<FeesType> feesType = new List<FeesType>();
            IEnumerable<FeesType> myGenericEnumerable = (IEnumerable<FeesType>)feesType;
            myGenericEnumerable = feesTypeRepository.GetAllFeesType();
            var res = (from a in myGenericEnumerable.ToList()
                       select new FeesType
                       {
                           ID = a.ID,
                           Name = a.Name,
                           ParentFeeTypeName=a.ParentFeeTypeName
                       }).ToList();
            feesType = res;

            return feesType;
        }

        public JsonResult ActiveDeactiveFeesType(string id, string IsActive)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("FeesType", accessId, "Update", "FeesType", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {

                var sessioschalar = feesTypeRepository.GetFeesTypeById(Convert.ToInt64(id));
                OdmErp.ApplicationCore.Entities.FeesType objScholarship = new FeesType();
                sessioschalar.ID = Convert.ToInt64(id);
                sessioschalar.Status = IsActive;

                sessioschalar.Active = true;

                //TODO
                sessioschalar.ModifiedId = userId;
                feesTypeRepository.UpdateFeesType(sessioschalar);
                objScholar.ReturnMsg = "Records " + sessioschalar.Status.ToLower() + " Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        #endregion

        #region ScholarshipApplicable
        public IActionResult ScholarshipApplicable()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("ScholarshipApplicable", accessId, "List", "StudentPayment", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(scholarshipApplicableRepository.GetAllScholarshipApplicable());
            // GetAllFeesTypeApplicable());
        }
        //public IActionResult ScholarshipApplicable(int? page, int? noofData)
        //{
        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //    long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    if (commonMethods.checkaccessavailable("FeesTypePrice", accessId, "List", "FessTypePrice", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }
        //    var pageNumber = page ?? 1;
        //    var PageSize = noofData ?? 10;
        //    var onePageOfEmps = scholarshipApplicableRepository.GetAllScholarshipApplicable(pageNumber, PageSize).ToPagedList();//_context.Emps.ToPagedList(pageNumber, PageSize);
        //    return View("ScholarshipApplicable", onePageOfEmps);
        //   // return View(feesTypePriceRepository.GetAllFeesTypePrice());
        //}
        //public IActionResult CreateOrEditFeeTypeApplicable()
        //{
        //    CSFeesTypeApplicable obj = new CSFeesTypeApplicable();
        //    obj.ID = 0;
        //    obj.Name = "";
        //    return View(obj);
        //}
        [HttpGet]
        public JsonResult CreateOrEditScholarshipApplicable(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            ApplicationCore.Entities.ScholarshipApplicable od = new ApplicationCore.Entities.ScholarshipApplicable();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Scholarship Applicable", accessId, "Create", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Scholarship Applicable", accessId, "Edit", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var scholarshipApplicable = scholarshipApplicableRepository.GetScholarshipApplicableById(id.Value);
                od.ID = scholarshipApplicable.ID;
                od.Name = scholarshipApplicable.Name;
                od.Description = scholarshipApplicable.Description;

            }
            return Json(od);
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SaveOrUpdateScholarshipApplicable(CS_ScholarshipApplicable scholarshipApplicable)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                OdmErp.ApplicationCore.Entities.ScholarshipApplicable scholarshipApplicable1 = new ScholarshipApplicable();

                scholarshipApplicable1.Name = scholarshipApplicable.Name;
                scholarshipApplicable1.Description = scholarshipApplicable.Description;



                scholarshipApplicable1.Active = true;
                if (scholarshipApplicable1.ID == 0)
                {
                    scholarshipApplicable1.InsertedId = userId;
                    scholarshipApplicable1.Status = EntityStatus.ACTIVE;
                    scholarshipApplicable.ReturnMsg = "Records added Successfully.";
                    if (ScholarshipApplicableExists(scholarshipApplicable.Name) == true)
                    {
                        scholarshipApplicableRepository.CreateScholarshipApplicable(scholarshipApplicable1);
                        //.CreateFeesTypeApplicable(scholarshipApplicable1);
                    }
                    else
                    {
                        scholarshipApplicable.ReturnMsg = "Records already Exists !!!";
                    }
                    //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Saved successfully");
                    //return Json(feesTypeApplicable.ReturnMsg);
                }
                //else
                //{
                //    //TODO
                //    feesTypeApplicable1.ModifiedId = userId;
                //    feesTypeApplicableRepository.UpdateFeesTypeApplicable(feesTypeApplicable1);
                //    feesTypeApplicable.ReturnMsg = "Records Updated Successfully.";
                //    //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Updated successfully");
                //    //return Json("");
                //}


            }
            return Json(scholarshipApplicable); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        [HttpPost]
        public JsonResult UpdateScholarship(CS_ScholarshipApplicable scholarshipApplicable)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                ScholarshipApplicable scholarshipApplicable1 = new ScholarshipApplicable();

                scholarshipApplicable1.ID = scholarshipApplicable.ID;
                scholarshipApplicable1.Name = scholarshipApplicable.Name;
                scholarshipApplicable1.Description = scholarshipApplicable.Description;



                scholarshipApplicable1.Active = true;

                //TODO
                scholarshipApplicable1.ModifiedId = userId;
                scholarshipApplicableRepository.UpdateScholarshipApplicable(scholarshipApplicable1);
                //UpdateFeesTypeApplicable(scholarshipApplicable1);
                scholarshipApplicable.ReturnMsg = "Records Updated Successfully.";
                //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Updated successfully");
                //return Json("");



            }
            return Json(scholarshipApplicable); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }


        //[ActionName("DeleteFeesTypeApplicable")]
        public JsonResult DeleteScholarshipApplicable(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Scholarship Applicable", accessId, "Delete", "StudentPayment", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            if (ScholarshipApplicableExists1(id.Value) == true)
            {
                var scholarshipApplicable = scholarshipApplicableRepository.DeleteScholarshipApplicable(id.Value);

            }
            return Json("Deleted successfully");
        }

        public JsonResult DeleteScholarshipApplicableAll()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Scholarship Applicable", accessId, "Delete", "StudentPayment", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }

            var ScholarshipApplicable = scholarshipApplicableRepository.DeleteAllScholarshipApplicable();
            //DeleteAllFeesTypeApplicable();

            return Json("Deleted successfully");
        }
        private bool ScholarshipApplicableExists1(long id)
        {

            if (scholarshipApplicableRepository.GetScholarshipApplicableById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool ScholarshipApplicableExists(string res)
        {

            if (scholarshipApplicableRepository.GetScholarshipApplicableByName(res) != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region MessagesContent
        public IActionResult MessagesContent()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("MessageContent", accessId, "List", "StudentPayment", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(messagesContentRepository.GetAllMessagesContent());
            // GetAllFeesTypeApplicable());
        }
        //public IActionResult ScholarshipApplicable(int? page, int? noofData)
        //{
        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //    long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    if (commonMethods.checkaccessavailable("FeesTypePrice", accessId, "List", "FessTypePrice", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }
        //    var pageNumber = page ?? 1;
        //    var PageSize = noofData ?? 10;
        //    var onePageOfEmps = scholarshipApplicableRepository.GetAllScholarshipApplicable(pageNumber, PageSize).ToPagedList();//_context.Emps.ToPagedList(pageNumber, PageSize);
        //    return View("ScholarshipApplicable", onePageOfEmps);
        //   // return View(feesTypePriceRepository.GetAllFeesTypePrice());
        //}
        //public IActionResult CreateOrEditFeeTypeApplicable()
        //{
        //    CSFeesTypeApplicable obj = new CSFeesTypeApplicable();
        //    obj.ID = 0;
        //    obj.Name = "";
        //    return View(obj);
        //}
        [HttpGet]
        public JsonResult CreateOrEditMessagesContent(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            ApplicationCore.Entities.MessagesContent od = new ApplicationCore.Entities.MessagesContent();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("MessagesContent", accessId, "Create", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Type = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Messages Content", accessId, "Edit", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var messagesContent = messagesContentRepository.GetMessagesContentById(id.Value);
                od.ID = messagesContent.ID;
                od.Type = messagesContent.Type;
                od.Category = messagesContent.Category;
                od.SubCategory = messagesContent.SubCategory;
                od.Title = messagesContent.Title;
                od.Body = messagesContent.Body;
                od.Template = messagesContent.Template;

            }
            return Json(od);
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SaveOrUpdateMessagesContent(CS_MessagesContent messagesContent)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                OdmErp.ApplicationCore.Entities.MessagesContent messagesContent1 = new MessagesContent();

                messagesContent1.Type = messagesContent.Type;
                messagesContent1.Category = messagesContent.Category;
                messagesContent1.SubCategory = messagesContent.SubCategory;
                messagesContent1.Title = messagesContent.Title;
                messagesContent1.Body = messagesContent.Body;
                messagesContent1.Template = messagesContent.Template;

                //messagesContent1.Active = true;
                //if (messagesContent1.ID == 0)
                //{
                //    messagesContent1.InsertedId = userId;
                //    messagesContent1.Status = EntityStatus.ACTIVE;
                //    messagesContent.ReturnMsg = "Records added Successfully.";
                //}
                messagesContent1.Active = true;
                if (messagesContent1.ID == 0)
                {
                    messagesContent1.InsertedId = userId;
                    messagesContent1.Status = EntityStatus.ACTIVE;
                    messagesContent.ReturnMsg = "Records added Successfully.";
                    if (MessagesContentExists(messagesContent.Type) == true)
                    {
                        messagesContentRepository.CreateMessagesContent(messagesContent1);
                        //.CreateFeesTypeApplicable(scholarshipApplicable1);
                    }
                    else
                    {
                        messagesContent.ReturnMsg = "Records already Exists !!!";
                    }
                    //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Saved successfully");
                    //return Json(feesTypeApplicable.ReturnMsg);
                }


            }
            return Json(messagesContent); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        [HttpPost]
        public JsonResult UpdateMessagesContent(CS_MessagesContent messagesContent)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                MessagesContent messagesContent1 = new MessagesContent();

                messagesContent1.ID = messagesContent.ID;
                messagesContent1.Type = messagesContent.Type;
                messagesContent1.Category = messagesContent.Category;
                messagesContent1.SubCategory = messagesContent.SubCategory;
                messagesContent1.Title = messagesContent.Title;
                messagesContent1.Body = messagesContent.Body;
                messagesContent1.Template = messagesContent.Template;

                messagesContent1.Active = true;

                //TODO
                messagesContent1.ModifiedId = userId;
                messagesContentRepository.UpdateMessagesContent(messagesContent1);
                //UpdateFeesTypeApplicable(scholarshipApplicable1);
                messagesContent.ReturnMsg = "Records Updated Successfully.";
                //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Updated successfully");
                //return Json("");



            }
            return Json(messagesContent); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }


        //[ActionName("DeleteMessageContent")]
        public JsonResult DeleteMessagesContent(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Messages Content", accessId, "Delete", "StudentPayment", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            if (MessagesContentExists1(id.Value) == true)
            {
                var messagesContent = messagesContentRepository.DeleteMessagesContent(id.Value);

            }
            return Json("Deleted successfully");
        }

        public JsonResult DeleteMessagesContentAll()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Messages Content", accessId, "Delete", "StudentPayment", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }

            var MessagesContent = messagesContentRepository.DeleteAllMessagesContent();

            return Json("Deleted successfully");
        }
        private bool MessagesContentExists1(long id)
        {

            if (messagesContentRepository.GetMessagesContentById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool MessagesContentExists(string res)
        {

            if (messagesContentRepository.GetMessagesContentByName(res) != null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        #endregion


        #region FeesType Applicable Download & Upload Excel
        public IActionResult downloadFeesTypeApplicableSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Upload", "StudentPayment", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name",
                "Description"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("FeesType Applicable"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }
                using (var cells = worksheet.Cells[1, 2]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"FeesType Applicable.xlsx");
        }
        public IActionResult DownloadBloodGroup()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name",
                "Description"
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("FeesType Applicable"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in bloodGroupRepository.GetAllBloodGroup())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    //worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    //worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"FeesType Applicable.xlsx");
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IActionResult UploadFeesTypeApplicable(IFormFile file)
        {
            long Yn = 0; string ReturnMsg = ""; string ExistsYN = "";
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Upload", "StudentPayment", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "FeesType Applicable_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                //var webPath = "";
                //if (string.IsNullOrWhiteSpace(hostingEnvironment.WebRootPath))
                //{
                //    webPath= hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                //}
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        FeesTypeApplicable feestypeApp = new FeesTypeApplicable();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        
                        if (CheckExistance(name) == false)
                        {
                            feestypeApp.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            feestypeApp.Description= firstWorksheet.Cells[i, 2].Value.ToString();
                            feestypeApp.ModifiedId = userId;
                            feestypeApp.Active = true;
                            feestypeApp.InsertedId = userId;
                            feestypeApp.Status = EntityStatus.ACTIVE;
                         Yn=  feesTypeApplicableRepository.CreateFeesTypeApplicable(feestypeApp);
                        }
                        else
                        {
                            ExistsYN = "Name already Exists !!!";
                        }
                    }
                    if (Yn > 1)
                    {
                        ReturnMsg = "Excel uploaded successfully";
                    }
                    else
                    {
                        ReturnMsg = "Excel uploaded Failed !!!  " + ExistsYN;
                    }
                }
            }
            
                return Json(ReturnMsg);
        }

        private bool CheckExistance(string names)
        {
            var name_exists = feesTypeApplicableRepository.GetAllFeesTypeApplicable(0, 0).Where(x => x.Name == names && x.Active == true).Select(x => x.Name).FirstOrDefault();
            if (names== name_exists)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion Download & Upload Excel

        #region FeesType Download & Upload Excel
        public IActionResult downloadFeesTypeSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("FeesType", accessId, "Upload", "StudentPayment", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            XLWorkbook oWB = new XLWorkbook();
            var wsData = oWB.Worksheets.Add("Data");
            wsData.Cell(1, 1).Value = "Name";
            wsData.Cell(1, 2).Value = "Description";
            wsData.Cell(1, 3).Value = "IsSubType";
            wsData.Cell(1, 4).Value = "ParentFeesType";
            int z= 1;
            string[] arr ={"0", "1"};
            for(int a=0;a<arr.Length;a++)
            {
                wsData.Cell(++z, 3).Value = a;
            }
            wsData.Range("C2:C" + z).AddToNamed("IsSubType");
            var parentdata = feesTypeRepository.GetAllFeesType();
            z = 1;
            foreach (var a in parentdata)
            {
                wsData.Cell(++z, 4).Value =a.Name;
            }
            wsData.Range("D2:D" + z).AddToNamed("ParentFeesType");

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Name");
            validationTable.Columns.Add("Description");
            validationTable.Columns.Add("IsSubType");
            validationTable.Columns.Add("ParentFeesType");
            //validationTable.Columns.Add("SubCategory");
            //validationTable.Columns.Add("Building");
            //validationTable.Columns.Add("Room");
            //validationTable.Columns.Add("Priority");
            //validationTable.Columns.Add("Name");
            //validationTable.Columns.Add("Description");
            //validationTable.Columns.Add("Status");
            validationTable.TableName = "FeesType_Deatils";
            var worksheet = oWB.AddWorksheet(validationTable);
            //worksheet.Column(1).SetDataValidation().List(wsData.Range("Name"), true);
            //worksheet.Column(2).SetDataValidation().List(wsData.Range("Description"), true);
            worksheet.Column(3).SetDataValidation().List(wsData.Range("IsSubType"), true);
            worksheet.Column(4).SetDataValidation().List(wsData.Range("ParentFeesType"), true);
            //worksheet.Column(3).SetDataValidation().InCellDropdown = true;
            //worksheet.Column(3).SetDataValidation().Operator = XLOperator.Between;
            //worksheet.Column(3).SetDataValidation().AllowedValues = XLAllowedValues.List;
            //worksheet.Column(3).SetDataValidation().List("=INDIRECT(SUBSTITUTE(B1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
            //worksheet.Column(4).SetDataValidation().List(wsData1.Range("Building"), true);
            //worksheet.Column(5).SetDataValidation().InCellDropdown = true;
            //worksheet.Column(5).SetDataValidation().Operator = XLOperator.Between;
            //worksheet.Column(5).SetDataValidation().AllowedValues = XLAllowedValues.List;
            //worksheet.Column(5).SetDataValidation().List("=INDIRECT(SUBSTITUTE(D1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
            //worksheet.Column(6).SetDataValidation().List(wsData1.Range("Priority"), true);
            wsData.Hide();
           // wsData1.Hide();
            // worksheet7.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"FeesType.xlsx");
        }
        
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IActionResult UploadFeesType(IFormFile file)
        {
            long Yn = 0; string ReturnMsg = "";
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("FeesType", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                    int totalRows = firstWorksheet.Dimension.Rows;
                   // var request = supportRepository.GetAllSupportRequest();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        FeesType fesstype = new FeesType();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        string description = firstWorksheet.Cells[i, 2].Value.ToString();
                        bool issubtype = Convert.ToBoolean(firstWorksheet.Cells[i, 3].Value);
                        string parentname = firstWorksheet.Cells[i, 4].Value.ToString();
                        var parentfeestypeid = Convert.ToInt64(feesTypeRepository.GetAllFeesType().Where(m => m.Name == parentname).FirstOrDefault().ID);

                        fesstype.Name = name;
                        fesstype.Description = description;
                        fesstype.IsSubType = issubtype;
                        fesstype.ParentFeeTypeID = parentfeestypeid;
                        fesstype.ParentFeeTypeName = parentname;
                        fesstype.ModifiedId = userId;
                        fesstype.Active = true;
                        fesstype.InsertedId = userId;
                        fesstype.Status = EntityStatus.ACTIVE;
                        
                        long id = feesTypeRepository.CreateFeesType(fesstype);
                        if (id > 1)
                        {
                            ReturnMsg = "Excel uploaded successfully";
                        }
                        else
                        {
                            ReturnMsg = "Excel uploaded Failed !!!";
                        }
                        //}
                    }
                }
                #region Oldcode
                //// Create a File Info 
                //FileInfo fi = new FileInfo(file.FileName);
                //var newFilename = "FeesType_" + String.Format("{0:d}",
                //                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                ////var webPath = "";
                ////if (string.IsNullOrWhiteSpace(hostingEnvironment.WebRootPath))
                ////{
                ////    webPath= hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                ////}
                //var webPath = hostingEnvironment.WebRootPath;
                //string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                //var pathToSave = newFilename;
                //using (var stream = new FileStream(path, FileMode.Create))
                //{
                //    file.CopyToAsync(stream);
                //}
                //FileInfo fil = new FileInfo(path);
                //using (ExcelPackage excelPackage = new ExcelPackage(fil))
                //{

                //    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                //    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                //    int totalRows = firstWorksheet.Dimension.Rows;
                //    for (int i = 2; i <= totalRows; i++)
                //    {
                //        FeesType feestypeApp = new FeesType();
                //        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                //        string Parentname= Convert.ToInt32(firstWorksheet.Cells[i, 4].Value).ToString();
                //        var ParentFeeTypeID = feesTypeRepository.GetAllFeesType().Where(m => m.Name == Parentname).FirstOrDefault().ID;
                //        if (feesTypeRepository.GetFeesTypeByName(name) == null)
                //        {
                //            feestypeApp.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                //            feestypeApp.Description = firstWorksheet.Cells[i, 2].Value.ToString();
                //            feestypeApp.IsSubType= Convert.ToBoolean(firstWorksheet.Cells[i, 3].Value);
                //            feestypeApp.ParentFeeTypeID = ParentFeeTypeID;
                //            feestypeApp.ModifiedId = userId;
                //            feestypeApp.Active = true;
                //            feestypeApp.InsertedId = userId;
                //            feestypeApp.Status = EntityStatus.ACTIVE;
                //            Yn = feesTypeRepository.CreateFeesType(feestypeApp);
                //        }
                //    }
                //    if (Yn > 1)
                //    {
                //        ReturnMsg = "Excel uploaded successfully";
                //    }
                //    else
                //    {
                //        ReturnMsg = "Excel uploaded Failed !!!";
                //    }
                //}
                #endregion
            }

            return Json(ReturnMsg);
        }

        #endregion Download & Upload Excel
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        #region Report
        public ActionResult StudentPaymentReport()
        {
            return View();
        }
        #endregion
    }
}
