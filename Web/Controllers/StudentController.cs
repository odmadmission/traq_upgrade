﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.ApplicationCore.Query;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Models;
using OdmErp.Web.ViewModels;
using System.Threading;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using ClosedXML.Excel;
using System.Data;
using OfficeOpenXml;
using System.Collections;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Web.Controllers
{
    public class StudentController : AppController
    {


        private readonly IFileProvider fileProvider;
        private readonly IHostingEnvironment hostingEnvironment;

        private IAddressRepository addressRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private ICityRepository cityRepository;
        private IStateRepository stateRepository;
        private ICountryRepository countryRepository;
        private IReligionTypeRepository religionTypeRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IAccessRepository accessRepository;
        private IRoleRepository roleRepository;
        public CommonMethods commonMethods;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IOrganizationRepository organizationRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IBoardStandardRepository BoardStandardRepository;
        public IStandardRepository StandardRepository;
        public IAcademicSessionRepository academicSessionRepository;
       // public IAcademicStandardStreamRepository academicStandardStreamRepository;
        public IBoardRepository boardRepository;
        public IStreamRepository streamRepository;
        //public IBoardStandardStreamRepository boardStandardStreamRepository;
        public IAcademicStudentRepository academicStudentRepository;
        public CommonSchoolModel commonSchoolModel;
        public commonsqlquery commonsqlquery;
        public IAcademicSectionRepository academicSectionRepository;
        public ISectionRepository sectionRepository;
        public IAcademicSectionStudentRepository academicSectionStudentRepository;

        public IAcademicCalenderRepository academicCalenderRepository;
        public IAcademicTimeTableRepository academicTimeTableRepository;
        public ITimeTablePeriodRepository timeTablePeriodRepository;
        public IPeriodEmployeeRepository periodEmployeeRepository;
        public IPeriodEmployeeStatusRepository periodEmployeeStatusRepository;
        public IPeriodSyllabusRepository periodSyllabusRepository;
        public IClassTimingRepository classTimingRepository;
        public IAcademicSubjectRepository academicSubjectRepository;
        public IAcademicChapterRepository academicChapterRepository;
        public IAcademicConceptRepository academicConceptRepository;
        public IChapterRepository chapterRepository;
        public IConceptRepository conceptRepository;
        public ISyallbusRepository syallbusRepository;
        public IAcademicSyllabusRepository academicSyllabusRepository;
        private IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;

        private StudentCommonMethod studentCommonMethod;
        public smssend Smssend;


        public ICategoryRepository categoryRepository;
        public ISubjectRepository SubjectRepository;
        public ITeacherRepository teacherRepository;
        public IEmployeeRepository employeeRepository;


        public StudentController(IFileProvider fileprovider, IHostingEnvironment env, IStudentAggregateRepository studentAggregateRepo, IBloodGroupRepository bloodGroupRepo, INationalityTypeRepository nationalityTypeRepo,
            IAddressTypeRepository addressTypeRepo, ICityRepository cityRepo, IStateRepository stateRepo, ICountryRepository countryRepo, IReligionTypeRepository religionTypeRepo, CommonMethods common, IAddressRepository addressRepo, IAccessRepository accessRepo,
            IRoleRepository roleRepo, IOrganizationAcademicRepository organizationAcademicRepo, IOrganizationRepository organizationRepo, IAcademicStandardRepository academicStandardRepo, IBoardStandardRepository BoardStandardRepo, IStandardRepository StandardRepo,
            IAcademicSessionRepository academicSessionRepo, CommonSchoolModel commonSchool, IBoardRepository boardRepo, IStreamRepository streamRepo, IAcademicStudentRepository academicStudentRepo, commonsqlquery commonsql,
            IAcademicSectionRepository academicSectionRepo, ISectionRepository sectionRepo, IAcademicSectionStudentRepository academicSectionStudentRepo,
            IAcademicCalenderRepository academicCalenderRepo, IAcademicChapterRepository academicChapterRepo,ICategoryRepository categoryRepository,
        IAcademicTimeTableRepository academicTimeTableRepo, IAcademicConceptRepository academicConceptRepo, smssend Smssend,
        IPeriodEmployeeRepository periodEmployeeRepo, IAcademicSubjectRepository academicSubjectRepo, IAcademicSyllabusRepository academicSyllabusRepo,
            IPeriodEmployeeStatusRepository periodEmployeeStatusRepo, IChapterRepository chapterRepo, IConceptRepository conceptRepo, ISyallbusRepository syallbusRepo,
            IPeriodSyllabusRepository periodSyllabusRepo, IClassTimingRepository classTimingRepo, ITimeTablePeriodRepository timeTablePeriodReo, IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepo,
            IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo, StudentCommonMethod studentCommonMet, ISubjectRepository subjectRepository, IEmployeeRepository employeeRepository, ITeacherRepository teacherRepository)
        {
            academicSectionStudentRepository = academicSectionStudentRepo;
            academicSectionRepository = academicSectionRepo;
            sectionRepository = sectionRepo;
            fileProvider = fileprovider;
            hostingEnvironment = env;
            BoardStandardRepository = BoardStandardRepo;
            studentAggregateRepository = studentAggregateRepo;
            bloodGroupRepository = bloodGroupRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            addressTypeRepository = addressTypeRepo;
            cityRepository = cityRepo;
            stateRepository = stateRepo;
            countryRepository = countryRepo;
            religionTypeRepository = religionTypeRepo;
            commonMethods = common;
            addressRepository = addressRepo;
            accessRepository = accessRepo;
            roleRepository = roleRepo;
            organizationAcademicRepository = organizationAcademicRepo;
            organizationRepository = organizationRepo;
            academicStandardRepository = academicStandardRepo;
            StandardRepository = StandardRepo;
            academicSessionRepository = academicSessionRepo;
            boardRepository = boardRepo;
            //academicStandardStreamRepository = academicStandardStreamRepo;
            //boardStandardStreamRepository = boardStandardStreamRepo;
            streamRepository = streamRepo;
            academicStudentRepository = academicStudentRepo;
            commonSchoolModel = commonSchool;
            commonsqlquery = commonsql;
            academicCalenderRepository = academicCalenderRepo;
            academicTimeTableRepository = academicTimeTableRepo;
            periodEmployeeRepository = periodEmployeeRepo;
            periodEmployeeStatusRepository = periodEmployeeStatusRepo;
            periodSyllabusRepository = periodSyllabusRepo;
            classTimingRepository = classTimingRepo;
            timeTablePeriodRepository = timeTablePeriodReo;
            academicSubjectRepository = academicSubjectRepo;
            academicChapterRepository = academicChapterRepo;
            academicConceptRepository = academicConceptRepo;
            chapterRepository = chapterRepo;
            conceptRepository = conceptRepo;
            syallbusRepository = syallbusRepo;
            academicSyllabusRepository = academicSyllabusRepo;
            employeeRequiredDocumentRepository = employeeRequiredDocumentRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            this.Smssend = Smssend;
            studentCommonMethod = studentCommonMet;

            this.categoryRepository = categoryRepository;
            SubjectRepository = subjectRepository;
            this.teacherRepository = teacherRepository;
            this.employeeRepository = employeeRepository;
        }
        #region ---------------------Student----------------------
        public IActionResult Index()
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            StudentModelClass studentcls = new StudentModelClass();
            studentcls.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
            studentcls.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
            studentcls.religionTypes = religionTypeRepository.GetAllReligionTypes();
            studentcls.categoryTypes = categoryRepository.ListAllAsyncIncludeAll().Result;
            studentcls.StudentList = commonMethods.GetAllStudent();
            studentcls.accesscls = commonMethods.GetStudentAccessDetails().Where(a => (a.RoleID == 5 || a.RoleID == 6)).ToList();
           
            var studentAddresses = studentAggregateRepository.GetAllParent().ToList();
            var parentRelationshipTypes = studentAggregateRepository.GetAllParentRelationshipType();
            var professionTypes = studentAggregateRepository.GetAllProfessionType();
            IEnumerable<StudentParentsCls> res = (from a in studentAddresses
                                                  join b in parentRelationshipTypes on a.ParentRelationshipID equals b.ID
                                                  join c in professionTypes on a.ProfessionTypeID equals c.ID
                                                  select new StudentParentsCls
                                                  {
                                                      ID = a.ID,
                                                      AlternativeMobile = a.AlternativeMobile,
                                                      EmailId = a.EmailId,
                                                      FirstName = a.FirstName,
                                                      LandlineNumber = a.LandlineNumber,
                                                      LastName = a.LastName,
                                                      MiddleName = a.MiddleName,
                                                      ParentRelationshipID = a.ParentRelationshipID,
                                                      ParentRelationshipName = b.Name,
                                                      PrimaryMobile = a.PrimaryMobile,
                                                      ProfessionTypeID = a.ProfessionTypeID,
                                                      ProfessionTypeName = c.Name,
                                                      StudentID = a.StudentID,
                                                      ModifiedDate = a.ModifiedDate,
                                                      Imagepath = a.Image != null ? Path.Combine("/ODMImages/ParentProfile/", a.Image) : null
                                                  }).ToList();
           
            studentcls.studentParentsCls = res.ToList();
            studentcls.studentAdresses = commonMethods.GetAllAddressByUsingStudent().ToList();
            studentcls.studentClassDetails = commonMethods.GetAllStudentClassByStudent().Where(a => a.IsCurrent == true).FirstOrDefault();
            studentcls.StudentDocumentAccess = studentCommonMethod.GetAllDocumentByUsingStudent().Where(a => a.DocumentTypeName == "EDUCATIONAL").ToList(); ;
            studentcls.StudentPersonalDocumentAccess = studentCommonMethod.GetAllDocumentByUsingStudent().Where(a => a.DocumentTypeName == "PERSONAL").ToList();
            studentcls.StudentAcademicAccess = studentCommonMethod.GetAllStudentAcademicListByStudent().ToList();
            ViewBag.status = "Create";           
            return View(studentcls);

            
        }
        public IActionResult CreateOrEditStudent(long? id)
        {
            CheckLoginStatus();

            StudentModelClass studentModelClass = new StudentModelClass();
            studentModelClass.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
            studentModelClass.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
            studentModelClass.religionTypes = religionTypeRepository.GetAllReligionTypes();
            studentModelClass.categoryTypes = categoryRepository.ListAllAsyncIncludeAll().Result;
            ViewBag.status = "Create";
            Random generator = new Random();
            String s = generator.Next(0, 999999).ToString("D6");
            var StudentCode = "ODMEG-" + s;
            if (id != null && id!=0)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                var studentdetails = studentAggregateRepository.GetStudentById(id.Value);
                studentModelClass.BloodGroupID = studentdetails.BloodGroupID;
                studentModelClass.DateOfBirth = studentdetails.DateOfBirth;
                studentModelClass.StudentCode = studentdetails.StudentCode;
                studentModelClass.FirstName = studentdetails.FirstName;
                studentModelClass.Gender = studentdetails.Gender;
                studentModelClass.ID = studentdetails.ID;
                studentModelClass.LastName = studentdetails.LastName;
                studentModelClass.MiddleName = studentdetails.MiddleName;
                studentModelClass.NatiobalityID = studentdetails.NatiobalityID;
                studentModelClass.CategoryID = studentdetails.CategoryID;
                studentModelClass.ReligionID = studentdetails.ReligionID;
                var webPath = hostingEnvironment.WebRootPath;
                if (studentdetails.Image != null)
                {
                    string path = Path.Combine("/ODMImages/StudentProfile/", studentdetails.Image);
                    ViewBag.Profileimage = path;
                }
                else
                {
                    ViewBag.Profileimage = null;
                }
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("PERSONAL").ID;
                studentModelClass.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                      where a.DocumentTypeID == documentTypeId
                                                      select new documentTypesCls
                                                      {
                                                          TypeId = a.DocumentTypeID,
                                                          ID = a.ID,
                                                          Name = a.Name,
                                                          IsRequired = studentCommonMethod.studentRequiredDocument(studentdetails.ID, a.DocumentTypeID, a.ID, studentdetails.ID),                                                          
                                                      }).ToList();
                ViewBag.status = "Update";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }


                studentModelClass.BloodGroupID = 0;
                studentModelClass.DateOfBirth = DateTime.Now.Date;
                studentModelClass.StudentCode = StudentCode;
                studentModelClass.FirstName = "";
                studentModelClass.Gender = null;
                studentModelClass.ID = 0;
                studentModelClass.LastName = "";
                studentModelClass.MiddleName = "";
                studentModelClass.NatiobalityID = 0;
                studentModelClass.CategoryID = 0;
                studentModelClass.ReligionID = 0;
                ViewBag.Profileimage = null;
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("PERSONAL").ID;
                studentModelClass.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                               where a.DocumentTypeID == documentTypeId
                                               select new documentTypesCls
                                               {
                                                   TypeId = a.DocumentTypeID,
                                                   ID = a.ID,
                                                   Name = a.Name,
                                                   IsRequired = false
                                               }).ToList();
                ViewBag.status = "Create";
            }

            return View(studentModelClass);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeProfile([Bind("ID,StudentCode,FirstName,MiddleName,LastName,Gender,DateOfBirth," +
            "BloodGroupID,NatiobalityID,ReligionID,CategoryID")] Student student,[FromForm(Name = "Upload")] IFormFile file, [Bind("documentTypesCls")] List<documentTypesCls> documentTypesCls)
        {

            CheckLoginStatus();
            if (ModelState.IsValid)
            {                
               
                student.ModifiedId = userId;
                student.Active = true;
                if (file != null)
                {
                    // Create a File Info 
                    FileInfo fi = new FileInfo(file.FileName);
                    var newFilename = student.StudentCode + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\StudentProfile\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                    student.Image = pathToSave;
                }
                if (student.ID == 0)
                {
                    var std = studentAggregateRepository.GetAllStudent().Where(a => a.StudentCode == student.StudentCode).FirstOrDefault();
                    if (std != null)
                    {
                        return View(student).WithDanger("Error", "Student code already exist");     
                    }
                    student.InsertedId = userId;
                    student.Status = "CREATED";                    
                    long sid = studentAggregateRepository.CreateStudent(student);

                    Access access = new Access();
                    access.EmployeeID = sid;
                    access.Active = true;
                    access.InsertedId = userId;
                    access.Password = "";
                    access.Username = student.StudentCode;
                    access.RoleID = 5;
                    access.ModifiedId = userId; 
                    accessRepository.CreateAccess(access);                   
                    foreach (var item in documentTypesCls)
                    {
                        if (item.IsRequired == true)
                        {
                            StudentRequiredDocument studentRequired = new StudentRequiredDocument();
                            studentRequired.Active = true;
                            studentRequired.DocumentSubTypeID = item.ID;
                            studentRequired.DocumentTypeID = item.TypeId;
                            studentRequired.StudentID = sid;
                            studentRequired.InsertedId = userId;
                            studentRequired.IsApproved = false;
                            studentRequired.IsMandatory = true;
                            studentRequired.IsReject = false;
                            studentRequired.IsVerified = false;
                            studentRequired.ModifiedId = userId;
                            studentRequired.RelatedID = sid;
                            studentRequired.Status = "CREATED";
                            studentRequired.VerifiedAccessID = 0;
                            studentAggregateRepository.CreateStudentRequiredDocument(studentRequired);
                        }
                    }
                    return RedirectToAction(nameof(ViewStudentDetails), new { id = sid });
                }
                else
                {
                    var std = studentAggregateRepository.GetAllStudent().Where(a => a.StudentCode == student.StudentCode && a.ID != student.ID).FirstOrDefault();
                    if (std != null)
                    {
                        return View(student).WithDanger("Error", "Student code already exist");
                    }
                    studentAggregateRepository.UpdateStudent(student);
                    Access access = accessRepository.GetAllAccess().Where(a => a.EmployeeID == student.ID && a.RoleID == 5).FirstOrDefault();
                    access.Username = student.StudentCode;
                    access.ModifiedId = userId;
                    accessRepository.UpdateAccess(access);

                    foreach (var item in documentTypesCls)
                    {
                        StudentRequiredDocument studentRequired = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.DocumentTypeID == item.TypeId && a.DocumentSubTypeID == item.ID && a.StudentID == student.ID).FirstOrDefault();
                        if (item.IsRequired == true)
                        {
                            if (studentRequired == null)
                            {
                                studentRequired = new StudentRequiredDocument();
                                studentRequired.Active = true;
                                studentRequired.DocumentSubTypeID = item.ID;
                                studentRequired.DocumentTypeID = item.TypeId;
                                studentRequired.StudentID = student.ID;
                                studentRequired.InsertedId = userId;
                                studentRequired.IsApproved = false;
                                studentRequired.IsMandatory = true;
                                studentRequired.IsReject = false;
                                studentRequired.IsVerified = false;
                                studentRequired.ModifiedId = userId;
                                studentRequired.RelatedID = student.ID;
                                studentRequired.Status = "CREATED";
                                studentRequired.VerifiedAccessID = 0;
                                studentAggregateRepository.CreateStudentRequiredDocument(studentRequired);
                            }
                            else
                            {
                                studentRequired.Active = true;
                                studentRequired.IsApproved = false;
                                studentRequired.IsMandatory = true;
                                studentRequired.IsReject = false;
                                studentRequired.IsVerified = false;
                                studentRequired.ModifiedId = userId;
                                studentRequired.RelatedID = student.ID;
                                studentRequired.Status = "CREATED";
                                studentRequired.VerifiedAccessID = 0;
                                studentAggregateRepository.UpdateStudentRequiredDocument(studentRequired);
                            }
                        }
                        else
                        {
                            if (studentRequired != null)
                            {
                                studentRequired.Active = false;
                                studentRequired.IsApproved = false;
                                studentRequired.IsMandatory = true;
                                studentRequired.IsReject = false;
                                studentRequired.IsVerified = false;
                                studentRequired.ModifiedId = userId;
                                studentRequired.RelatedID = student.ID;
                                studentRequired.Status = "CREATED";
                                studentRequired.VerifiedAccessID = 0;
                                studentAggregateRepository.UpdateStudentRequiredDocument(studentRequired);
                            }

                        }

                    }
                    return RedirectToAction(nameof(ViewStudentDetails), new { id = student.ID });
                }


            }
            return View(student);
        }
        [ActionName("DeleteStudent")]
        public async Task<IActionResult> StudentDeleteConfirmed(long id)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var student = studentAggregateRepository.DeleteStudent(id);
            return RedirectToAction(nameof(Index));
        }
        public IActionResult ViewStudentDetails(long? id)
        {
            StudentModelClass studentcls = new StudentModelClass();
            studentcls.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
            studentcls.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
            studentcls.religionTypes = religionTypeRepository.GetAllReligionTypes();
            studentcls.categoryTypes = categoryRepository.ListAllAsyncIncludeAll().Result;
            ViewBag.status = "Create";
            if (id != null)
            {
                var studentdetails = studentAggregateRepository.GetStudentById(id.Value);
                studentcls.BloodGroupID = studentdetails.BloodGroupID;
                studentcls.BloodGroupName = bloodGroupRepository.GetBloodGroupById(studentdetails.BloodGroupID).Name;
                studentcls.DateOfBirth = studentdetails.DateOfBirth;
                studentcls.StudentCode = studentdetails.StudentCode;
                studentcls.FirstName = studentdetails.FirstName;
                studentcls.Gender = studentdetails.Gender;
                studentcls.ID = studentdetails.ID;
                studentcls.LastName = studentdetails.LastName;
                studentcls.MiddleName = studentdetails.MiddleName;
                studentcls.NatiobalityID = studentdetails.NatiobalityID;
                studentcls.NatiobalityName = nationalityTypeRepository.GetNationalityById(studentdetails.NatiobalityID).Name;
                studentcls.ReligionID = studentdetails.ReligionID;
                studentcls.ReligionName = religionTypeRepository.GetReligionTypeById(studentdetails.ReligionID).Name;
                studentcls.CategoryName = categoryRepository.GetCategoryNameById(studentdetails.CategoryID).Result.Name;
                var webPath = hostingEnvironment.WebRootPath;
                if (studentdetails.Image != null)
                {
                    string path = Path.Combine("/ODMImages/StudentProfile/", studentdetails.Image);
                    ViewBag.Profileimage = path;
                }
                else
                {
                    ViewBag.Profileimage = null;
                }
                studentcls.studentAdresses = commonMethods.GetAllAddressByUsingStudentId(studentdetails.ID);
                studentcls.studentParentsCls = commonMethods.GetAllParentsByUsingStudentId(studentdetails.ID);
                studentcls.studentClassDetails = commonMethods.GetAllStudentClassByStudentId(studentdetails.ID).Where(a => a.IsCurrent == true).FirstOrDefault();
                studentcls.accesscls = commonMethods.GetStudentAccessDetails().Where(a => a.EmployeeID == studentdetails.ID && (a.RoleID == 5 || a.RoleID == 6)).ToList();
                studentcls.StudentDocumentAccess = studentCommonMethod.GetAllDocumentByUsingStudentId(studentdetails.ID).Where(a => a.DocumentTypeName == "EDUCATIONAL").ToList(); 
                studentcls.StudentPersonalDocumentAccess = studentCommonMethod.GetAllDocumentByUsingStudentId(studentdetails.ID).Where(a=>a.DocumentTypeName == "PERSONAL").ToList();
                studentcls.StudentAcademicAccess = studentCommonMethod.GetAllStudentAcademicByStudentId(studentdetails.ID);

                ViewBag.status = "Update";
            }
            return View(studentcls);
        }
        #endregion-------------------------------student--------------------------------
        #region --------Student Address-------------------
        public IActionResult StudentAddressDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            ViewBag.StudentId = id;
            ViewBag.StudentName = studentAggregateRepository.GetStudentById(id).FirstName;
            var addressTypes = addressTypeRepository.GetAllAddressType();
            var address = commonMethods.GetAllAddressByUsingStudentId(id);
            var res = (from a in addressTypes
                       select new StudentTypewithAddresscls
                       {
                           typeId = a.ID,
                           typeName = a.Name,
                           studentAdress = address.Where(m => m.AddressTypeID == a.ID).FirstOrDefault()
                       }).ToList();

            return View(res);



        }
        public IActionResult CreateOrEditStudentAddress(long? id, long eid, long typeid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            ViewBag.StudentName = studentAggregateRepository.GetStudentById(eid).FirstName;
            StudentAdressCls studentAdressCls = new StudentAdressCls();
            studentAdressCls.addressTypes = addressTypeRepository.GetAllAddressType();
            studentAdressCls.countries = countryRepository.GetAllCountries();
            studentAdressCls.StudentId = eid;
            studentAdressCls.AddressTypeID = typeid;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var studentAddress = studentAggregateRepository.GetStudentAddressById(id.Value);
                studentAdressCls.StudentAddressId = studentAddress.ID;

                var address = addressRepository.GetAddressById(studentAddress.AddressID);
                studentAdressCls.cities = cityRepository.GetAllCityByStateId(address.StateID);
                studentAdressCls.states = stateRepository.GetAllStateByCountryId(address.CountryID);
                studentAdressCls.AddressLine1 = address.AddressLine1;
                studentAdressCls.AddressLine2 = address.AddressLine2;
                studentAdressCls.CityID = address.CityID;
                studentAdressCls.StateID = address.StateID;
                studentAdressCls.CountryID = address.CountryID;
                studentAdressCls.PostalCode = address.PostalCode;
                studentAdressCls.ID = address.ID;
                ViewBag.status = "Update";

            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                studentAdressCls.AddressLine1 = "";
                studentAdressCls.AddressLine2 = "";
                studentAdressCls.CityID = 0;
                studentAdressCls.StateID = 0;
                studentAdressCls.CountryID = 0;
                studentAdressCls.ID = 0;
                studentAdressCls.PostalCode = "";
                studentAdressCls.StudentAddressId = 0;
                ViewBag.status = "Create";
            }
            return View(studentAdressCls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentAddress([Bind("ID,AddressLine1,AddressLine2,AddressTypeID,CityID,CountryID,PostalCode,StateID")]Address address, long StudentAddressId, long StudentId)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                address.ModifiedId = userId;
                address.Active = true;
                if (address.ID == 0 && StudentAddressId == 0)
                {
                    address.InsertedId = userId;
                    address.Status = "CREATED";
                    long id = addressRepository.CreateAddress(address);
                    StudentAddress studentAddress = new StudentAddress();
                    studentAddress.AddressID = id;
                    studentAddress.StudentID = StudentId;
                    studentAddress.Active = true;
                    studentAddress.InsertedId = userId;
                    studentAddress.ModifiedId = userId;
                    studentAddress.Status = "CREATED";
                    studentAggregateRepository.CreateStudentAddress(studentAddress);
                }
                else
                {
                    addressRepository.UpdateAddress(address);
                }

                return RedirectToAction(nameof(StudentAddressDetails), new { id = StudentId });
            }
            return View(address);
        }
        [ActionName("DeletestudentAddress")]
        public async Task<IActionResult> StudentAddressDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var studentAddress = studentAggregateRepository.DeleteStudentAddress(id.Value);
            return RedirectToAction(nameof(StudentAddressDetails), new { id = eid });
        }
        #endregion

        #region-------------------- Student Parents--------------
        public IActionResult StudentParentsDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.StudentId = id;
            var res = commonMethods.GetAllParentsByUsingStudentId(id);
            return View(res);
        }
        public IActionResult CreateOrEditStudentParents(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            StudentParentsCls studentParentsCls = new StudentParentsCls();
            studentParentsCls.ProfessionTypes = (from a in studentAggregateRepository.GetAllProfessionType()
                                                 select new CommonMasterModel
                                                 {
                                                     ID = a.ID,
                                                     Name = a.Name
                                                 }).ToList();
            studentParentsCls.ParentRelationships = (from a in studentAggregateRepository.GetAllParentRelationshipType()
                                                     select new CommonMasterModel
                                                     {
                                                         ID = a.ID,
                                                         Name = a.Name
                                                     }).ToList();
            studentParentsCls.StudentID = eid;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var studentparents = studentAggregateRepository.GetParentById(id.Value);
                studentParentsCls.ID = studentparents.ID;
                studentParentsCls.AlternativeMobile = studentparents.AlternativeMobile;

                studentParentsCls.AlternativeMobile = studentparents.AlternativeMobile;
                studentParentsCls.EmailId = studentparents.EmailId;
                studentParentsCls.FirstName = studentparents.FirstName;
                studentParentsCls.LandlineNumber = studentparents.LandlineNumber;
                studentParentsCls.LastName = studentparents.LastName;
                studentParentsCls.MiddleName = studentparents.MiddleName;
                studentParentsCls.ParentRelationshipID = studentparents.ParentRelationshipID;
                studentParentsCls.PrimaryMobile = studentparents.PrimaryMobile;
                studentParentsCls.ProfessionTypeID = studentparents.ProfessionTypeID;
                studentParentsCls.ParentOrganization = studentparents.ParentOrganization;
                studentParentsCls.Imagepath = studentparents.Image != null ? Path.Combine("/ODMImages/ParentProfile/", studentparents.Image) : null;
                ViewBag.status = "Update";

            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                studentParentsCls.ID = 0;
                studentParentsCls.AlternativeMobile = "";
                studentParentsCls.EmailId = "";
                studentParentsCls.FirstName = "";
                studentParentsCls.LandlineNumber = "";
                studentParentsCls.LastName = "";
                studentParentsCls.MiddleName = "";
                studentParentsCls.ParentRelationshipID = 0;
                studentParentsCls.PrimaryMobile = "";
                studentParentsCls.ProfessionTypeID = 0;
                studentParentsCls.ParentOrganization = "";
                studentParentsCls.Imagepath = null;
                ViewBag.status = "Create";
            }
            return View(studentParentsCls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentParents([Bind("ID,FirstName,MiddleName,LastName,ParentRelationshipID,PrimaryMobile,ProfessionTypeID,LandlineNumber," +
            "AlternativeMobile,EmailId,StudentID,ParentOrganization")]Parent parent, IFormFile file)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                parent.ModifiedId = userId;
                parent.Active = true;
                if (file != null)
                {
                    // Create a File Info 
                    FileInfo fi = new FileInfo(file.FileName);
                    var newFilename = parent.StudentID + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\ParentProfile\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                    parent.Image = pathToSave;
                }
                if (parent.ID == 0 && parent.ID == 0)
                {
                    parent.InsertedId = userId;
                    parent.Status = "CREATED";
                    long id = studentAggregateRepository.CreateParent(parent);

                    if (parent.PrimaryMobile != null)
                    {
                        Access access = accessRepository.GetAllAccess().Where(a => a.Username == parent.PrimaryMobile).FirstOrDefault();
                        if (access == null)
                        {
                            access = new Access();
                            access.EmployeeID = id;
                            access.Active = true;
                            access.InsertedId = userId;
                            access.Password = "";
                            access.Username = parent.PrimaryMobile;
                            access.RoleID = 6;
                            access.ModifiedId = userId;
                            long ids = accessRepository.CreateAccess(access);

                            Access acces = accessRepository.GetAccessById(ids);
                            var emp = studentAggregateRepository.GetParentById(acces.EmployeeID);
                                Thread t1 = new Thread(delegate ()
                                {
                                    Smssend.Parentcreated(emp.FirstName, emp.EmailId, emp.PrimaryMobile);
                                });
                                t1.Start();
                            

                        }
                    }
                }
                else
                {
                    studentAggregateRepository.UpdateParent(parent);
                    if (parent.PrimaryMobile != null)
                    {
                        Access access = accessRepository.GetAllAccess().Where(a => a.EmployeeID == parent.ID).FirstOrDefault();
                        if (access == null)
                        {
                            access = accessRepository.GetAllAccess().Where(a => a.Username == parent.PrimaryMobile).FirstOrDefault();
                            if (access == null)
                            {
                                access = new Access();
                                access.EmployeeID = parent.ID;
                                access.Active = true;
                                access.InsertedId = userId;
                                access.Password = "";
                                access.Username = parent.PrimaryMobile;
                                access.RoleID = 6;
                                access.ModifiedId = userId;
                                accessRepository.CreateAccess(access);
                            }
                        }
                        else
                        {
                            access.Username = parent.PrimaryMobile;
                            access.ModifiedId = userId;
                            accessRepository.UpdateAccess(access);
                        }
                    }
                }

                return RedirectToAction(nameof(StudentParentsDetails), new { id = parent.StudentID });
            }
            return View(parent);
        }
        [ActionName("DeletestudentParents")]
        public async Task<IActionResult> StudentParentsDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var student = studentAggregateRepository.DeleteParent(id.Value);
            return RedirectToAction(nameof(StudentParentsDetails), new { id = eid });
        }
        #endregion

        #region---------------- Student Standard----------------------
        public IActionResult StudentStandardDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.StudentId = id;
            var res = commonMethods.GetAllStudentClassByStudentId(id);
            return View(res);
        }
        public IActionResult CreateOrEditStudentStandards(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            StudentClassDetails studentClass = new StudentClassDetails();
            studentClass.organizations = commonMethods.GetOrganizations();
            studentClass.sections = commonMethods.GetSection();
            studentClass.boards = commonMethods.GetBoard();
            studentClass.StudentID = eid;
            ViewBag.StudentId = id;

            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var studentclasss = commonMethods.GetAllStudentClassByStudentId(eid).Where(a => a.ID == id).FirstOrDefault();
                studentClass.ID = studentclasss.ID;
                studentClass.BoardID = studentclasss.BoardID;
                studentClass.IsCurrent = studentclasss.IsCurrent;
                studentClass.OrganizationID = studentclasss.OrganizationID;
                studentClass.SectionID = studentclasss.SectionID;
                studentClass.StandardID = studentclasss.StandardID;
                studentClass.WingID = studentclasss.WingID;
                //studentClass.wings = commonMethods.GetSchoolWing().Where(a => a.OrganisationID == studentClass.OrganizationID).ToList();
                studentClass.standards = commonMethods.GetStandard().Where(a => a.BoardID == studentClass.BoardID).ToList();
                ViewBag.status = "Update";

            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                studentClass.ID = 0;
                studentClass.BoardID = 0;
                studentClass.IsCurrent = false;
                studentClass.OrganizationID = 0;
                studentClass.SectionID = 0;
                studentClass.StandardID = 0;
                studentClass.WingID = 0;
                ViewBag.status = "Create";
            }
            return View(studentClass);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentStandards([Bind("ID,IsCurrent,OrganizationID,SectionID,StandardID,WingID,StudentID")]StudentClass studentClass)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                if (studentClass.IsCurrent == true)
                {
                    var st = studentAggregateRepository.GetAllStudentClass();
                    foreach (var s in st)
                    {
                        s.IsCurrent = false;
                        studentAggregateRepository.UpdateStudentClass(s);
                    }
                }
                studentClass.ModifiedId = userId;
                studentClass.Active = true;
                if (studentClass.ID == 0)
                {
                    studentClass.InsertedId = userId;
                    studentClass.Status = "CREATED";
                    long id = studentAggregateRepository.CreateStudentClass(studentClass);
                }
                else
                {
                    studentAggregateRepository.UpdateStudentClass(studentClass);
                }

                return RedirectToAction(nameof(StudentStandardDetails), new { id = studentClass.StudentID });
            }
            return View(studentClass);
        }
        [ActionName("DeletestudentStandard")]
        public async Task<IActionResult> StudentStandardsDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var student = studentAggregateRepository.DeleteStudentClass(id.Value);
            return RedirectToAction(nameof(StudentStandardDetails), new { id = eid });
        }
        #endregion

        #region -----------------------Log In Access-----------------------
        public IActionResult StudentloginaccessDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.StudentId = id;
            var res = commonMethods.GetStudentAccessDetails().Where(a => a.EmployeeID == id && (a.RoleID == 5 || a.RoleID == 6)).ToList();
            return View(res);
        }
        public JsonResult CheckUserName(string userName) 
        {
            var res = commonMethods.Checkuser(userName);
            return Json(res);
        }
        public IActionResult CreateOrEditStudentloginaccess(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            EmployeeAccesscls employee = new EmployeeAccesscls();
            employee.roles = roleRepository.GetAllRole();
            employee.EmployeeID = eid;
            ViewBag.StudentId = id;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var accesscls = commonMethods.GetStudentAccessDetails().Where(a => a.EmployeeID == eid && a.AccessID == id && (a.RoleID == 5 || a.RoleID == 6)).FirstOrDefault();
                employee.AccessID = accesscls.AccessID;
                employee.Password = accesscls.Password;
                employee.RoleID = accesscls.RoleID;
                employee.Username = accesscls.Username;
                ViewBag.status = "Update";

            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                employee.AccessID = 0;
                employee.Password = "";
                employee.RoleID = 0;
                employee.Username = "";
                ViewBag.status = "Create";
            }
            return View(employee);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentAccessDetails([Bind("AccessID,RoleID,EmployeeID,Username,Password")]EmployeeAccesscls employeeLogInCls)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                Access access = new Access();
                access.EmployeeID = employeeLogInCls.EmployeeID;
                access.Active = true;
                access.ID = employeeLogInCls.AccessID;
                access.InsertedId = userId;
                access.Password = employeeLogInCls.Password;
                access.Username = employeeLogInCls.Username;
                access.RoleID = employeeLogInCls.RoleID;
                if (access.ID == 0)
                {
                    access.ModifiedId = userId;
                    accessRepository.CreateAccess(access);
                }
                else
                {
                    access.ModifiedId = userId;
                    accessRepository.UpdateAccess(access);
                }

                return RedirectToAction(nameof(StudentloginaccessDetails), new { id = employeeLogInCls.EmployeeID });
            }
            return View(nameof(StudentloginaccessDetails), new { id = employeeLogInCls.EmployeeID });
        }

        [ActionName("Deletestudentloginaccess")]
        public async Task<IActionResult> StudentloginaccessDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var student = accessRepository.DeleteAccess(id.Value);
            return RedirectToAction(nameof(StudentloginaccessDetails), new { id = eid });
        }
        #endregion

        #region-------------------------- Academic Students-------------------------
        public IActionResult AcademicStudent()
        {
            //var allacademicstudents = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result;
            //var session = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a=>a.IsAvailable==true).ToList();
            //var 

            return View(commonsqlquery.Get_view_All_Academic_Students().Where(a => a.IsAvailable == true));
        }
        public IActionResult CreateOrEditAcademicStudent()
        {
            AcademicStudentcls academicStudentcls = new AcademicStudentcls();
            academicStudentcls.academicSessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.IsAvailable == true).ToList();
            academicStudentcls.boards = boardRepository.GetAllBoard();
            return View(academicStudentcls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateAcademicStudent(AcademicStudentcls academicStudentcls)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {
                var academicstudents = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result;             
                foreach (var a in academicStudentcls.StudentId)
                {

                    var chk = academicstudents != null ? academicstudents.Where(m => m.StudentId == a && m.AcademicStandardId == academicStudentcls.AcademicStandardId).FirstOrDefault():null;
                    if (chk == null)
                    {
                        AcademicStudent academic = new AcademicStudent();
                        academic.AcademicStandardId = academicStudentcls.AcademicStandardId;
                        //academic.AcademicStandardStreamId = academicStudentcls.AcademicStandardStreamId;
                        academic.Active = true;
                        academic.InsertedId = userId;
                        academic.IsAvailable = true;
                        academic.ModifiedId = userId;
                        academic.Status = "Created";
                        academic.StudentId = a;
                        academic.InsertedDate = DateTime.Now;
                        academic.ModifiedDate = DateTime.Now;
                        await academicStudentRepository.AddAsync(academic);
                    }
                }
            }
            return RedirectToAction(nameof(AcademicStudent)).WithSuccess("Data", "Saved Successfully");
        }
        public JsonResult GetOrganisationByAcademicSession(long AcademicSessionId)                 //Filter the selected Standard
        {
            try
            {
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicSessionId == AcademicSessionId);
                var OrganisationList = organizationRepository.GetAllOrganization();
                var res = (from a in OrganisationAcademic
                           join b in OrganisationList on a.OrganizationId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }).ToList();
                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        //public JsonResult GetWingBySchoolWing(long OraganisationAcademicId)
        //{
        //    try
        //    {
        //        var SchoolWing = commonMethods.GetSchoolWing().Where(a => a.OrganizationAcademicID == OraganisationAcademicId);
        //        var wingList = commonMethods.GetWing();
        //        var res = (from a in SchoolWing
        //                   join b in wingList on a.WingID equals b.ID
        //                   select new
        //                   {
        //                       id = a.ID,
        //                       name = b.Name
        //                   }).ToList();
        //        return Json(res);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}
        public JsonResult GetClassByBoard(long BoardId, long OrganizationAcademicId)
        {
            try
            {
                var academicstandard = academicStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.OrganizationAcademicId == OrganizationAcademicId).ToList();
                var BoardStandardList = BoardStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.BoardId == BoardId);
                var StandardList = StandardRepository.GetAllStandard();
                var res = (from a in academicstandard
                           join b in BoardStandardList on a.BoardStandardId equals b.ID
                           join c in StandardList on b.StandardId equals c.ID
                           select new
                           {
                               id = a.ID,
                               name = c.Name
                           }).ToList();
                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        public JsonResult GetStreamAvailableByClass(long AcademicStandardId)
        {
            try
            {
                var academicstandard = academicStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID == AcademicStandardId).ToList();
                var BoardStandardList = BoardStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                var StandardList = StandardRepository.GetAllStandard();
                var res = (from a in academicstandard
                           join b in BoardStandardList on a.BoardStandardId equals b.ID
                           join c in StandardList on b.StandardId equals c.ID
                           select new
                           {
                               isstream = c.StreamType
                           }).FirstOrDefault();
                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        //public JsonResult GetStreamByAcademicStandard(long AcademicStandardId)
        //{
        //    try
        //    {
        //        var academicstandardstream = academicStandardStreamRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicStandardId == AcademicStandardId).ToList();
        //        var BoardStandardStream = boardStandardStreamRepository.ListAllAsyncIncludeActiveNoTrack().Result;
        //        var stream = streamRepository.GetAllStream();
        //        var res = (from a in academicstandardstream
        //                   join b in BoardStandardStream on a.BoardStandardStreamId equals b.ID
        //                   join c in stream on b.StreamId equals c.ID
        //                   select new
        //                   {
        //                       id = a.ID,
        //                       name = c.Name
        //                   }).ToList();
        //        return Json(res);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}
        public JsonResult GetStudentsByAcademicStandard(long AcademicSessionId, long OrganistionAcademicId, long BoardId, long BoardStandardId)
        {
            try
            {
                var sessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID < AcademicSessionId).LastOrDefault();
                var organisation = organizationAcademicRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID == OrganistionAcademicId).FirstOrDefault();

                var boardstandardlist = BoardStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                var academicstandardlist = academicStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                var boardstandarddet = boardstandardlist.Where(a => a.ID == academicstandardlist.Where(b => b.ID == BoardStandardId).Select(b => b.BoardStandardId).FirstOrDefault()).FirstOrDefault();
                var standards = StandardRepository.GetAllStandard().Where(a => a.ID < boardstandarddet.StandardId).LastOrDefault();
                var boardstandard = standards == null ? null : boardstandardlist.Where(a => a.BoardId == BoardId && a.StandardId == standards.ID).FirstOrDefault();
                var students = studentAggregateRepository.GetAllStudent();
                var allacademicstudents = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                var newres = students.Select(a => a.ID).Except(allacademicstudents.Select(a => a.StudentId));
                var newstudents = (from a in students
                                   where newres.Contains(a.ID)
                                   select new
                                   {
                                       id = a.ID,
                                       name = a.FirstName + " " + a.LastName,
                                       code = a.StudentCode,
                                       status = "N"
                                   }).ToList();
                if (boardstandard != null && sessions != null)
                {
                    var organisationAcademy = organizationAcademicRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.OrganizationId == organisation.OrganizationId && a.AcademicSessionId == sessions.ID).FirstOrDefault();
                    var academicstandard = academicstandardlist.Where(a => a.BoardStandardId == boardstandard.ID && a.OrganizationAcademicId == organisationAcademy.ID).FirstOrDefault();
                    if (academicstandard != null)
                    {
                        var academicstudents = allacademicstudents.Where(a => a.AcademicStandardId == academicstandard.ID).ToList();
                        var commonres = (from a in students
                                         join b in academicstudents on a.ID equals b.StudentId
                                         select new
                                         {
                                             id = a.ID,
                                             name = a.FirstName + " " + a.LastName,
                                             code = a.StudentCode,
                                             status = "O"
                                         }).ToList();
                        var res = newstudents.Union(commonres);
                        return Json(res);
                    }
                    else
                    {
                        return Json(newstudents);
                    }
                }
                else
                {
                    return Json(newstudents);
                }
            }
            catch
            {
                return Json(null);
            }
        }
        #endregion
        #region--------------------- Academic Student Section---------------------
        public IActionResult AcademicStudentSection()
        {
            //var allacademicstudents = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result;
            //var session = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a=>a.IsAvailable==true).ToList();
            //var 

            return View(commonsqlquery.Get_view_All_Academic_Student_Sections());
        }
        public IActionResult CreateOrEditAcademicStudentSection()
        {
            AcademicStudentcls academicStudentcls = new AcademicStudentcls();
            academicStudentcls.academicSessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.IsAvailable == true).ToList();
            academicStudentcls.boards = boardRepository.GetAllBoard();
            return View(academicStudentcls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateAcademicStudentSection(AcademicStudentcls academicStudentcls)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {
                var academicSectionstudents = academicSectionStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                foreach (var a in academicStudentcls.StudentId)
                {
                    var chk = academicSectionstudents.ToList().Count() > 0 ? academicSectionstudents.Where(m => m.AcademicStudentId == a && m.AcademicSectionId == academicStudentcls.AcademicSectionId).FirstOrDefault() :null ;
                    if (chk == null)
                    {
                        AcademicSectionStudent academic = new AcademicSectionStudent();
                        academic.AcademicStudentId = a;
                        academic.AcademicSectionId = academicStudentcls.AcademicSectionId;
                        academic.Active = true;
                        academic.InsertedId = userId;
                        academic.IsAvailable = true;
                        academic.ModifiedId = userId;
                        academic.Status =EntityStatus.ACTIVE;
                        academic.InsertedDate = DateTime.Now;
                        academic.ModifiedDate = DateTime.Now;
                      await  academicSectionStudentRepository.AddAsync(academic);
                    }
                    else
                    {
                        AcademicSectionStudent academic = chk;
                        academic.AcademicSectionId = academicStudentcls.AcademicSectionId;
                        academic.IsAvailable = true;
                        academic.ModifiedId = userId;
                        academic.Status = EntityStatus.ACTIVE;
                        academic.ModifiedDate = DateTime.Now;
                        await academicSectionStudentRepository.UpdateAsync(academic);
                    }
                }
            }
            return RedirectToAction(nameof(AcademicStudentSection)).WithSuccess("success", "Saved Successfully");
        }
        public JsonResult GetSectionsByAcademicStandard(long AcademicStandardId)
        {
            try
            {
                var academicsection = academicSectionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicStandardId == AcademicStandardId).ToList();
                var section = sectionRepository.GetAllSection();
                var res = (from a in academicsection
                           join b in section on a.SectionId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }).ToList();
                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        public JsonResult GetAllStudentsByAcademicStandard(long AcademicStandardId, long? AcademicStandardStreamId)
        {
            try
            {
                var students = studentAggregateRepository.GetAllStudent();
                var allacademicstudents = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicStandardId == AcademicStandardId).ToList();
                var allacademicsectionstudents = commonsqlquery.Get_view_All_Academic_Student_Sections();
                var commonres = (from a in allacademicstudents
                                 join b in students on a.StudentId equals b.ID
                                 select new
                                 {
                                     id = a.ID,
                                     name = b.FirstName + " " + b.LastName,
                                     code = b.StudentCode,
                                     status = "O",
                                     section = allacademicsectionstudents.ToList().Count() == 0 ? "" : (allacademicsectionstudents.Where(m => m.AcademicStudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : allacademicsectionstudents.Where(m => m.AcademicStudentId == a.ID && m.IsAvailable == true).FirstOrDefault().SectionName)
                                 }).ToList();

                return Json(commonres);

            }
            catch (Exception e)
            {
                return Json(null);
            }
        }
        #endregion
        #region------------------- Student Roll No----------------------------
        public IActionResult AllotAcademicRollNo()
        {
            AcademicStudentcls academicStudentcls = new AcademicStudentcls();
            academicStudentcls.academicSessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.IsAvailable == true).ToList();
            academicStudentcls.boards = boardRepository.GetAllBoard();
            return View(academicStudentcls);
        }
        public JsonResult GetAllStudentsByAcademicSection(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId)
        {
            try
            {
                var students = studentAggregateRepository.GetAllStudent();

                var allacademicstudents = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicStandardId == AcademicStandardId).ToList();
                var allacademicsectionstudents = academicSectionStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicSectionId == AcademicSectionId).ToList();
                var commonres = (from a in allacademicsectionstudents
                                 join c in allacademicstudents on a.AcademicStudentId equals c.ID
                                 join b in students on c.StudentId equals b.ID
                                 select new
                                 {
                                     id = a.ID,
                                     name = b.FirstName + " " + b.LastName,
                                     code = b.StudentCode,
                                     status = "O"
                                 }).ToList();

                return Json(commonres);

            }
            catch
            {
                return Json(null);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateAcademicStudentRollno(AcademicStudentcls academicStudentcls)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {
                var academicSectionstudents = academicSectionStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                for (var i = 0; i < academicStudentcls.AcademicStudentId.Count(); i++)
                {
                    var chk = academicSectionstudents.ToList().Count() == 0 ? null : academicSectionstudents.Where(m => m.ID == academicStudentcls.AcademicStudentId.ToList()[i]).FirstOrDefault();
                    if (chk != null)
                    {
                        AcademicSectionStudent academic = chk;
                        academic.StudentCode = academicStudentcls.RollNo.ToList()[i];
                        academic.ModifiedId = userId;
                        academic.ModifiedDate = DateTime.Now;
                      await  academicSectionStudentRepository.UpdateAsync(academic);
                    }

                }
            }
            return RedirectToAction(nameof(AcademicStudentSection)).WithSuccess("RollNo", "Alloted Successfully");
        }

        #endregion
        #region----------------------- Time Table----------------------
        public IActionResult StudentTimeTable()
        {
            CheckLoginStatus();
           
            AcademicStudentcls academicStudentcls = new AcademicStudentcls();
            academicStudentcls.academicSessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.IsAvailable == true).ToList();
            academicStudentcls.boards = boardRepository.GetAllBoard();
            return View(academicStudentcls);
        }
        public IActionResult TeacherTimeTable()
        {
            CheckLoginStatus();
          
            AcademicStudentcls academicStudentcls = new AcademicStudentcls();
            academicStudentcls.academicSessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.IsAvailable == true).ToList();
            academicStudentcls.boards = boardRepository.GetAllBoard();
            return View(academicStudentcls);
        }
        public IActionResult AcademicStudentTimeTable()
        {
            CheckLoginStatus();

            if (roleId == 6)
            {
                string stdvalue = HttpContext.Session.GetString("ddlstudentid");
                userId = Convert.ToInt64(stdvalue);
            }
            var s = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(a => a.StudentId == userId && a.IsAvailable==true).FirstOrDefault();
            view_time_table_cls table_Cls = new view_time_table_cls();
            table_Cls.AcademicSectionId = s.AcademicSectionId;
           // table_Cls.AcademicStandardId = s.AcademicStandardId;
           // table_Cls.AcademicStandardStreamId = s.AcademicStandardStreamId;
            return View(table_Cls);
        }
        public IActionResult CreateOrEditStudentTimeTable()
        {
            CheckLoginStatus();
            AcademicStudentcls academicStudentcls = new AcademicStudentcls();
            academicStudentcls.academicSessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.IsAvailable == true).ToList();
            academicStudentcls.boards = boardRepository.GetAllBoard();
            academicStudentcls.ClassTiming = classTimingRepository.ListAllAsyncIncludeAll().Result.Where(s => s.Active = true).ToList();
            var res = (from a in teacherRepository.ListAllAsyncIncludeAll().Result
                       join b in employeeRepository.GetAllEmployee() on a.EmployeeId equals b.ID
                       select new
                       {
                           id = a.ID,
                           name = b.FirstName
                       }
                       ).ToList();
          
            ViewBag.TeacherNameList = new[] { new SelectListItem { Text = "Select Teacher", Value = " " } }.Concat(new SelectList(res, "id", "name"));
          
           //if(id.Value!=0 && id!=null)
           // {
           //     var academictimetable = academicTimeTableRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID == id).FirstOrDefault();
           //     var timetableperiod = timeTablePeriodRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcdemicTimeTableId == id.Value).FirstOrDefault();
           //     var periodemployee = periodEmployeeRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.TimeTablePeriodId == timetableperiod.ID).FirstOrDefault();
           //     academicStudentcls.date = academictimetable.AssignDate;
           //     academicStudentcls.AcademicStandardId = academictimetable.AcademicStandardId;
           //     academicStudentcls.AcademicStandardStreamId = academictimetable.AcademicStandardStreamId.Value;
           //     academicStudentcls.ddlclasstiming = timetableperiod.ClassTimingId;
           //     academicStudentcls.ddlsubjects = periodemployee.SubjectId;
           //     academicStudentcls.ddlteacher = periodemployee.EmployeeId;
           //     academicStudentcls.ID = academictimetable.ID;
           // }
           // else
           // {
           //     academicStudentcls.ID = 0;
           // }   
            return View(academicStudentcls);
        }

        public IActionResult ViewTimeTable(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {
            CheckLoginStatus();
            view_time_table_cls table_Cls = new view_time_table_cls();
            table_Cls.AcademicSectionId = AcademicSectionId;
            table_Cls.AcademicStandardId = AcademicStandardId;
            table_Cls.AcademicStandardStreamId = AcademicStandardStreamId;
            table_Cls.date = date;
            AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
            table_Cls._Academic_Standard_With_Stream = commonsqlquery.Get_View_All_Academic_Section().Where(a => a.ID == AcademicSectionId).FirstOrDefault();
            var timetableperiods = commonsqlquery.Get_View_All_TimeTable_Subjects().Where(a => a.AcademicSectionId == AcademicSectionId && a.AcademicStandardId == AcademicStandardId && a.AssignDate.Date == date.Date).ToList();
            table_Cls.timetable = timetableperiods;
            table_Cls.teachers = commonsqlquery.Get_View_All_Teachers().Where(a => a.OrganizationId == table_Cls._Academic_Standard_With_Stream.OrganizationId && a.GroupId == table_Cls._Academic_Standard_With_Stream.GroupID).ToList();
            table_Cls.subjects = commonsqlquery.Get_View_All_Academic_Subject().Where(a => a.BoardStandardId == table_Cls._Academic_Standard_With_Stream.BoardStandardId).ToList();
            var classtiming = classTimingRepository.ListAllAsyncIncludeAllNoTrack().Result;
            table_Cls.timingcls = (from a in classtiming
                                   select new ClassTimingcls
                                   {
                                       EndTime = a.EndTime,
                                       ID = a.ID,
                                       IsAvailable = a.IsAvailable,
                                       ModifiedDate = a.ModifiedDate,
                                       Name = a.Name,
                                       StartTime = a.StartTime,
                                       Active = a.Active
                                   }).ToList();
            return View(table_Cls);
        }
        public IActionResult ViewTeacherTimeTable(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {
            CheckLoginStatus();
            view_time_table_cls table_Cls = new view_time_table_cls();
            table_Cls.AcademicSectionId = AcademicSectionId;
            table_Cls.AcademicStandardId = AcademicStandardId;
            table_Cls.AcademicStandardStreamId = AcademicStandardStreamId;
            table_Cls.date = date;
            AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
            table_Cls._Academic_Standard_With_Stream = commonsqlquery.Get_View_All_Academic_Section().Where(a => a.ID == AcademicSectionId).FirstOrDefault();
            var timetableperiods = commonsqlquery.Get_View_All_TimeTable_Subjects().Where(a => a.AcademicSectionId == AcademicSectionId && a.AcademicStandardId == AcademicStandardId && a.AssignDate.Date == date.Date).ToList();
            table_Cls.timetable = timetableperiods.Where(a => a.TeacherId == userId).ToList();
            table_Cls.teachers = commonsqlquery.Get_View_All_Teachers().Where(a => a.OrganizationId == table_Cls._Academic_Standard_With_Stream.OrganizationId && a.GroupId == table_Cls._Academic_Standard_With_Stream.GroupID).ToList();

            return View(table_Cls);
        }
       
        public IActionResult ViewAcademicStudentTimeTable(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {
            CheckLoginStatus();
            
            if(roleId==6)
            {
                string stdvalue = HttpContext.Session.GetString("ddlstudentid");
                userId = Convert.ToInt64(stdvalue);
            }
            
            view_time_table_cls table_Cls = new view_time_table_cls();
            table_Cls.AcademicSectionId = AcademicSectionId;
            table_Cls.AcademicStandardId = AcademicStandardId;
            table_Cls.AcademicStandardStreamId = AcademicStandardStreamId;
            table_Cls.date = date;
            AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
            table_Cls._Academic_Standard_With_Stream = commonsqlquery.Get_View_All_Academic_Section().Where(a => a.ID == AcademicSectionId).FirstOrDefault();
            var timetableperiods = commonsqlquery.Get_View_All_TimeTable_Subjects().Where(a => a.AcademicSectionId == AcademicSectionId && a.AcademicStandardId == AcademicStandardId && a.AssignDate.Date == date.Date).ToList();
            table_Cls.timetable = timetableperiods;
            var academicstudent = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicStandardId == AcademicStandardId && a.StudentId == userId).ToList();
            return View(table_Cls);
        }



        public async Task<IActionResult> SaveOrUpdateTimeTable(long? timetableid, long AcademicSectionId, long AcademicStandardId, DateTime date, long ddlclasstiming, long ddlsubjects, long ddlteacher)
        {
            CheckLoginStatus();

            //AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
            var timetableperiods = academicTimeTableRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicSectionId == AcademicSectionId && a.AcademicStandardId == AcademicStandardId && a.AssignDate.Date == date.Date).ToList();
            if (timetableid == 0 || timetableid==null)
            {
                if (timetableperiods.Count > 0)
                {
                    TimeTablePeriod timeTablePeriod = timeTablePeriodRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ClassTimingId == ddlclasstiming && a.AcdemicTimeTableId == timetableperiods.ToList()[0].ID).FirstOrDefault();
                    if (timeTablePeriod == null)
                    {
                        timeTablePeriod = new TimeTablePeriod();
                        timeTablePeriod.AcdemicTimeTableId = timetableperiods.ToList()[0].ID;
                        timeTablePeriod.Active = true;
                        timeTablePeriod.ClassTimingId = ddlclasstiming;
                        timeTablePeriod.Code = "";
                        timeTablePeriod.InsertedDate = DateTime.Now;
                        timeTablePeriod.ModifiedDate = DateTime.Now;
                        timeTablePeriod.InsertedId = userId;
                        timeTablePeriod.ModifiedId = userId;
                        timeTablePeriod.IsAvailable = true;
                        timeTablePeriod.NameOfDay = date.ToString("dddd");
                        timeTablePeriod.Status = EntityStatus.ACTIVE;
                        var res = timeTablePeriodRepository.AddAsync(timeTablePeriod);
                        PeriodEmployee periodEmployee = new PeriodEmployee();
                        periodEmployee.Active = true;
                        periodEmployee.EmployeeId = ddlteacher;
                        periodEmployee.InsertedDate = DateTime.Now;
                        periodEmployee.InsertedId = userId;
                        periodEmployee.IsAvailable = true;
                        periodEmployee.IsForwared = false;
                        periodEmployee.ModifiedDate = DateTime.Now;
                        periodEmployee.ModifiedId = userId;
                        periodEmployee.ParentPeriodEmployeeId = 0;
                        periodEmployee.Status = EntityStatus.ACTIVE;
                        periodEmployee.SubjectId = ddlsubjects;
                        periodEmployee.TimeTablePeriodId = res.Result.ID;
                      await  periodEmployeeRepository.AddAsync(periodEmployee);
                        return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") }).WithSuccess("Success", "Submitted Successfully");

                    }
                    else
                    {
                        return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") }).WithWarning("Warning", "Already Exist");
                    }
                }
                else
                {
                    AcademicTimeTable academicTimeTable = new AcademicTimeTable();
                    academicTimeTable.AcademicSectionId = AcademicSectionId;
                    academicTimeTable.AcademicStandardId = AcademicStandardId;
                    //academicTimeTable.AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
                    academicTimeTable.Active = true;
                    academicTimeTable.AssignDate = date;
                    var academictt = academicTimeTableRepository.ListAllAsyncIncludeAll().Result.ToList().LastOrDefault();
                    if (academictt == null)
                    {
                        academicTimeTable.Code = "ODM-TT-01";
                    }
                    else
                    {
                        academicTimeTable.Code = "ODM-TT-0" + (academictt.ID + 1);
                    }

                    academicTimeTable.NameOfMonth = date.ToString("MMMM");
                    academicTimeTable.InsertedDate = DateTime.Now;
                    academicTimeTable.ModifiedDate = DateTime.Now;
                    academicTimeTable.InsertedId = userId;
                    academicTimeTable.ModifiedId = userId;
                    academicTimeTable.IsAvailable = true;
                    academicTimeTable.Status = EntityStatus.ACTIVE;
                    var acd= academicTimeTableRepository.AddAsync(academicTimeTable).Result;                 
                   TimeTablePeriod timeTablePd = new TimeTablePeriod();
                    timeTablePd.AcdemicTimeTableId = acd.ID;
                    timeTablePd.Active = true;
                    timeTablePd.ClassTimingId = ddlclasstiming;
                    timeTablePd.Code = acd.Code;
                    timeTablePd.InsertedDate = DateTime.Now;
                    timeTablePd.ModifiedDate = DateTime.Now;
                    timeTablePd.InsertedId = userId;
                    timeTablePd.ModifiedId = userId;
                    timeTablePd.IsAvailable = true;
                    timeTablePd.NameOfDay = date.ToString("dddd");
                    timeTablePd.Status = EntityStatus.ACTIVE;
                    var res = timeTablePeriodRepository.AddAsync(timeTablePd).Result;
                    PeriodEmployee periodEmployee = new PeriodEmployee();
                    periodEmployee.Active = true;
                    periodEmployee.EmployeeId = ddlteacher;
                    periodEmployee.InsertedDate = DateTime.Now;
                    periodEmployee.InsertedId = userId;
                    periodEmployee.IsAvailable = true;
                    periodEmployee.IsForwared = false;
                    periodEmployee.ModifiedDate = DateTime.Now;
                    periodEmployee.ModifiedId = userId;
                    periodEmployee.ParentPeriodEmployeeId = 0;
                    periodEmployee.Status = EntityStatus.ACTIVE;
                    periodEmployee.SubjectId = ddlsubjects;
                    periodEmployee.TimeTablePeriodId = res.ID;
                    await periodEmployeeRepository.AddAsync(periodEmployee);
                    return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") }).WithSuccess("Success", "Submitted Successfully");
                }
            }
            else
            {
                TimeTablePeriod timeTablePeriod = timeTablePeriodRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ClassTimingId == ddlclasstiming && a.AcdemicTimeTableId == timetableperiods.ToList()[0].ID && a.ID != timetableid).FirstOrDefault();
                if (timeTablePeriod == null)
                {
                    timeTablePeriod = timeTablePeriodRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID != timetableid).FirstOrDefault();
                    timeTablePeriod.ClassTimingId = ddlclasstiming;
                    timeTablePeriod.ModifiedDate = DateTime.Now;
                    timeTablePeriod.ModifiedId = userId;
                    var res = timeTablePeriodRepository.UpdateAsync(timeTablePeriod);
                    PeriodEmployee periodEmployee = periodEmployeeRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.TimeTablePeriodId == timetableid).FirstOrDefault();

                    periodEmployee.EmployeeId = ddlteacher;
                    periodEmployee.ModifiedDate = DateTime.Now;
                    periodEmployee.ModifiedId = userId;
                    periodEmployee.SubjectId = ddlsubjects;
                    await periodEmployeeRepository.UpdateAsync(periodEmployee);
                    return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") }).WithSuccess("Success", "Submitted Successfully");

                }
                else
                {
                    return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") }).WithWarning("Warning", "Already Exist");
                }
            }
        }
        public async Task<IActionResult> ForwardSubject(long? timetableid, long ddlteacher, long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {
            CheckLoginStatus();


            PeriodEmployee periodEmployee = periodEmployeeRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID == timetableid).FirstOrDefault();
            if (periodEmployee == null)
            {
                periodEmployee.Active = false;
                periodEmployee.ModifiedDate = DateTime.Now;
                periodEmployee.ModifiedId = userId;
               await periodEmployeeRepository.UpdateAsync(periodEmployee);

                PeriodEmployee periodEmp = new PeriodEmployee();
                periodEmp.Active = true;
                periodEmp.EmployeeId = ddlteacher;
                periodEmp.InsertedDate = DateTime.Now;
                periodEmp.InsertedId = userId;
                periodEmp.IsAvailable = true;
                periodEmp.IsForwared = true;
                periodEmp.ModifiedDate = DateTime.Now;
                periodEmp.ModifiedId = userId;
                periodEmp.ParentPeriodEmployeeId = periodEmployee.EmployeeId;
                periodEmp.Status = EntityStatus.ACTIVE; ;
                periodEmp.SubjectId = periodEmployee.SubjectId;
                periodEmp.TimeTablePeriodId = periodEmployee.ID;
                await periodEmployeeRepository.AddAsync(periodEmp);
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") }).WithSuccess("Success", "Submitted Successfully");

            }
            else
            {
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") }).WithWarning("Warning", "Already Exist");
            }



        }


        public JsonResult Getchapters(long subjectId, long academicsessionId)
        {
            var Academicsubject = academicSubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.SubjectId == subjectId && a.AcademicSessionId == academicsessionId && a.Active == true).FirstOrDefault();
            var Academicchapter = academicChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSubjectId == Academicsubject.ID && a.Active == true).ToList();
            var chapters = chapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
            var res = (from a in Academicchapter
                       join b in chapters on a.ChapterId equals b.ID
                       select new
                       {
                           a.ID,
                           a.AcademicSubjectId,
                           b.Name
                       }).ToList();

            return Json(res);

        }
        public JsonResult Getconcept(long academicchapterId)
        {
            var Academicconcept = academicConceptRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicChapterId == academicchapterId && a.Active == true).ToList();
            var concepts = conceptRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
            var res = (from a in Academicconcept
                       join b in concepts on a.ConceptId equals b.ID
                       select new
                       {
                           a.ID,
                           a.AcademicChapterId,
                           b.Name
                       }).ToList();

            return Json(res);

        }

        public JsonResult GetSyllabus(long academicconceptId)
        {
            var Academicsyllabus = academicSyllabusRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicConceptId == academicconceptId && a.Active == true).ToList();
            var syllabus = syallbusRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
            var res = (from a in Academicsyllabus
                       join b in syllabus on a.SyllabusId equals b.ID
                       select new
                       {
                           a.ID,
                           a.AcademicConceptId,
                           b.Title
                       }).ToList();

            return Json(res);

        }

        public async Task<IActionResult> SyllabusUpdate(long periodemployeeid, long ddlchapter,long ddlconcept,long ddlsyllabus,string txtdesc, long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {
            CheckLoginStatus();
            PeriodEmployeeStatus periodEmployeeStatus = periodEmployeeStatusRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.PeriodEmployeeId == periodemployeeid && a.Active==true).FirstOrDefault();
            if (periodEmployeeStatus == null)
            {
                periodEmployeeStatus = new PeriodEmployeeStatus();
                periodEmployeeStatus.IsAvailable = true;
                periodEmployeeStatus.PeriodEmployeeId = periodemployeeid;
                periodEmployeeStatus.Reason = txtdesc;
                periodEmployeeStatus.TakenEmployeeId = userId;
                periodEmployeeStatus.Active = true;
                periodEmployeeStatus.InsertedDate = DateTime.Now;
                periodEmployeeStatus.InsertedId = userId;
                periodEmployeeStatus.ModifiedDate = DateTime.Now;
                periodEmployeeStatus.ModifiedId = userId;
               var res= periodEmployeeStatusRepository.AddAsync(periodEmployeeStatus);

                PeriodSyllabus periodSyllabus = new PeriodSyllabus();
                periodSyllabus.Active = true;
                periodSyllabus.AcademicChapterId = ddlchapter;
                periodSyllabus.InsertedDate = DateTime.Now;
                periodSyllabus.InsertedId = userId;
                periodSyllabus.IsAvailable = true;
                periodSyllabus.AcademicConceptId = ddlconcept;
                periodSyllabus.ModifiedDate = DateTime.Now;
                periodSyllabus.ModifiedId = userId;
                periodSyllabus.AcademicSyllabusId = ddlsyllabus;
                periodSyllabus.Status = EntityStatus.ACTIVE;
                periodSyllabus.PeriodEmployeeStatusId = res.Result.ID;
               await periodSyllabusRepository.AddAsync(periodSyllabus);
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") }).WithSuccess("Success", "Submitted Successfully");

            }
            else
            {
                periodEmployeeStatus.Reason = txtdesc;
                periodEmployeeStatus.TakenEmployeeId = userId;
                periodEmployeeStatus.ModifiedDate = DateTime.Now;
                periodEmployeeStatus.ModifiedId = userId;
               await periodEmployeeStatusRepository.UpdateAsync(periodEmployeeStatus);

                PeriodSyllabus periodSyllabus = periodSyllabusRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a=>a.PeriodEmployeeStatusId== periodEmployeeStatus.ID).FirstOrDefault();
                periodSyllabus.AcademicChapterId = ddlchapter;
                periodSyllabus.AcademicConceptId = ddlconcept;
                periodSyllabus.ModifiedDate = DateTime.Now;
                periodSyllabus.ModifiedId = userId;
                periodSyllabus.AcademicSyllabusId = ddlsyllabus;
               await periodSyllabusRepository.UpdateAsync(periodSyllabus);
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") }).WithSuccess("Success", "Submitted Successfully");
            }



        }
        public async Task<IActionResult> RatingUpdate(long periodemployeeid, long ddlchapter, long ddlconcept, long ddlsyllabus, string txtdesc, long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {
            CheckLoginStatus();
            PeriodEmployeeStatus periodEmployeeStatus = periodEmployeeStatusRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.PeriodEmployeeId == periodemployeeid && a.Active == true).FirstOrDefault();
            if (periodEmployeeStatus == null)
            {
                periodEmployeeStatus = new PeriodEmployeeStatus();
                periodEmployeeStatus.IsAvailable = true;
                periodEmployeeStatus.PeriodEmployeeId = periodemployeeid;
                periodEmployeeStatus.Reason = txtdesc;
                periodEmployeeStatus.TakenEmployeeId = userId;
                periodEmployeeStatus.Active = true;
                periodEmployeeStatus.InsertedDate = DateTime.Now;
                periodEmployeeStatus.InsertedId = userId;
                periodEmployeeStatus.ModifiedDate = DateTime.Now;
                periodEmployeeStatus.ModifiedId = userId;
                var res = periodEmployeeStatusRepository.AddAsync(periodEmployeeStatus);

                PeriodSyllabus periodSyllabus = new PeriodSyllabus();
                periodSyllabus.Active = true;
                periodSyllabus.AcademicChapterId = ddlchapter;
                periodSyllabus.InsertedDate = DateTime.Now;
                periodSyllabus.InsertedId = userId;
                periodSyllabus.IsAvailable = true;
                periodSyllabus.AcademicConceptId = ddlconcept;
                periodSyllabus.ModifiedDate = DateTime.Now;
                periodSyllabus.ModifiedId = userId;
                periodSyllabus.AcademicSyllabusId = ddlsyllabus;
                periodSyllabus.Status = EntityStatus.ACTIVE;
                periodSyllabus.PeriodEmployeeStatusId = res.Result.ID;
              await  periodSyllabusRepository.AddAsync(periodSyllabus);
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") }).WithSuccess("Success", "Submitted Successfully");

            }
            else
            {
                periodEmployeeStatus.Reason = txtdesc;
                periodEmployeeStatus.TakenEmployeeId = userId;
                periodEmployeeStatus.ModifiedDate = DateTime.Now;
                periodEmployeeStatus.ModifiedId = userId;
               await periodEmployeeStatusRepository.UpdateAsync(periodEmployeeStatus);

                PeriodSyllabus periodSyllabus = periodSyllabusRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.PeriodEmployeeStatusId == periodEmployeeStatus.ID).FirstOrDefault();
                periodSyllabus.AcademicChapterId = ddlchapter;
                periodSyllabus.AcademicConceptId = ddlconcept;
                periodSyllabus.ModifiedDate = DateTime.Now;
                periodSyllabus.ModifiedId = userId;
                periodSyllabus.AcademicSyllabusId = ddlsyllabus;
               await periodSyllabusRepository.UpdateAsync(periodSyllabus);
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") }).WithSuccess("Success", "Submitted Successfully");
            }



        }


        public async Task<IActionResult> DeletestudentTimetable(long id, long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {

            TimeTablePeriod timeTablePeriod = timeTablePeriodRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID != id).FirstOrDefault();
            timeTablePeriod.Active = false;
            timeTablePeriod.ModifiedDate = DateTime.Now;
            timeTablePeriod.ModifiedId = userId;
            var res = timeTablePeriodRepository.UpdateAsync(timeTablePeriod);
            PeriodEmployee periodEmployee = periodEmployeeRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.TimeTablePeriodId == id).FirstOrDefault();

            periodEmployee.Active = false;
            periodEmployee.ModifiedDate = DateTime.Now;
            periodEmployee.ModifiedId = userId;
           await periodEmployeeRepository.UpdateAsync(periodEmployee);
            return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") }).WithSuccess("Success", "Submitted Successfully");

        }

        public JsonResult GetCalenderEngagementEventsForTimeTable(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId)
        {
            AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
            var data = commonsqlquery.GetTimeTableList(AcademicSectionId, AcademicStandardId, AcademicStandardStreamId);
            //ToString("yyyy-MM-dd'T'HH:mm:ss.fffffffZ"))
            var res = (from a in data
                       select new CalenderEventData
                       {
                           allDay = a.allDay,
                           backgroundColor = a.backgroundColor,
                           editable = a.editable,
                           end = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.end),
                           start = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.start),
                           textColor = a.textColor,
                           title = a.title
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetCalenderEngagementEventsForAcademicTimeTable(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId)
        {
            AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
            var data = commonsqlquery.GetTimeTableList(AcademicSectionId, AcademicStandardId, AcademicStandardStreamId);
            //ToString("yyyy-MM-dd'T'HH:mm:ss.fffffffZ"))
            var res = (from a in data
                       select new CalenderEventData
                       {
                           allDay = a.allDay,
                           backgroundColor = a.backgroundColor,
                           editable = a.editable,
                           end = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.end),
                           start = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.start),
                           textColor = a.textColor,
                           title = a.title
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetCalenderEngagementEventsForzteacherTimeTable(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId)
        {
            CheckLoginStatus();
            AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
            var data = commonsqlquery.GetForTeacherTimeTableList(AcademicSectionId, AcademicStandardId, AcademicStandardStreamId, userId);
            //ToString("yyyy-MM-dd'T'HH:mm:ss.fffffffZ"))
            var res = (from a in data
                       select new CalenderEventData
                       {
                           allDay = a.allDay,
                           backgroundColor = a.backgroundColor,
                           editable = a.editable,
                           end = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.end),
                           start = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.start),
                           textColor = a.textColor,
                           title = a.title
                       }).ToList();
            return Json(res);
        }

        #endregion
        public async Task<JsonResult> GetStateByCountryId(long id)
        {
            var res = (from a in stateRepository.GetAllState().ToList()
                       where a.CountryID == id
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        public async Task<JsonResult> GetCityByStateId(long id)
        {
            var res = (from a in cityRepository.GetAllCity().ToList()
                       where a.StateID == id
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        //public JsonResult GetWingByOrganisationId(long id)
        //{
        //    var res = commonMethods.GetSchoolWing().Where(a => a.OrganisationID == id).ToList();
        //    return Json(res);
        //}
        public JsonResult GetStandardByBoardId(long id)
        {
            var res = commonMethods.GetStandard().Where(a => a.BoardID == id).ToList();
            return Json(res);
        }



        #region------------------------ Student Document Details-------------------------------------
        public IActionResult StudentDocumentDetails(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            if (commonMethods.checkaccessavailable("Student Document", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.StudentId = id;
            if (commonMethods.checkaccessavailable("Student Document", accessId, "Verify Document", "Student", roleId) == true)
            {
                ViewBag.verifiedaccess = true;
            }
            else
            {
                ViewBag.verifiedaccess = false;
            }
            if (commonMethods.checkaccessavailable("Student Document", accessId, "Approve Document", "Student", roleId) == true)
            {
                ViewBag.approveaccess = true;
            }
            else
            {
                ViewBag.approveaccess = false;
            }
            if (commonMethods.checkaccessavailable("Student Document", accessId, "Reject Document", "Student", roleId) == true)
            {
                ViewBag.rejectaccess = true;
            }
            else
            {
                ViewBag.rejectaccess = false;
            }
            var StudentRequiredDocumentList = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.StudentID == id).ToList();
            var documentTypeList = documentTypeRepository.GetAllDocumentTypes();
            var documentSubTypeList = documentSubTypeRepository.GetAllDocumentSubTypes();
            var documentsList = studentAggregateRepository.GetAllStudentDocumentAttachment();

            var res = (from a in StudentRequiredDocumentList
                       join b in documentTypeList on a.DocumentTypeID equals b.ID
                       join c in documentSubTypeList on a.DocumentSubTypeID equals c.ID
                       select new StudentDocumentModelClass
                       {
                           ID = a.ID,
                           totaldocument = documentsList.Where(m => m.StudentRequiredDocumentID == a.ID).ToList().Count,

                           DocumentSubTypeID = a.DocumentSubTypeID,
                           DocumentSubTypeName = c.Name,
                           DocumentTypeID = a.DocumentTypeID,
                           DocumentTypeName = b.Name,
                           StudentID = a.StudentID,
                           IsApproved = a.IsApproved,
                           IsMandatory = a.IsMandatory,
                           IsReject = a.IsReject,
                           IsVerified = a.IsVerified,
                           RelatedID = a.RelatedID,
                           VerifiedAccessID = a.VerifiedAccessID,
                           VerifiedDate = a.VerifiedDate
                       }).ToList();

            return View(res);
        }

        public IActionResult CreateOrEditStudentDocumentDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student Document", accessId, "Upload", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var doc = studentAggregateRepository.GetStudentRequiredDocumentById(id);
            StudentDocumentUploadCls documentUploadCls = new StudentDocumentUploadCls();
            documentUploadCls.attachments = studentAggregateRepository.GetAllStudentDocumentAttachment().Where(a => a.StudentRequiredDocumentID == id).ToList();
            documentUploadCls.isapproved = doc.IsApproved;
            documentUploadCls.isverified = doc.IsVerified;
            documentUploadCls.ID = id;
            documentUploadCls.StudentID = doc.StudentID;

            return View(documentUploadCls);
        }
        public IActionResult RemoveAttachments(long id, long did)
        {
            studentAggregateRepository.DeleteStudentDocumentAttachment(id);
            return RedirectToAction(nameof(CreateOrEditStudentDocumentDetails), new { id = did }).WithSuccess("Data", "Deleted successfully"); ;
        }
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentDocument(long ID, IEnumerable<IFormFile> files)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (files != null)
            {
                if (files.Count() > 0)
                {
                    foreach (IFormFile file in files)
                    {
                        StudentDocumentAttachment employeeDocument = new StudentDocumentAttachment();
                        employeeDocument.Active = true;
                        employeeDocument.StudentRequiredDocumentID = ID;
                        employeeDocument.InsertedId = userId;
                        employeeDocument.ModifiedId = userId;
                        employeeDocument.Status = "Upload";
                        FileInfo fi = new FileInfo(file.FileName);
                        var newFilename = ID + "_" + String.Format("{0:d}",
                                          (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                        var webPath = hostingEnvironment.WebRootPath;
                        string path = Path.Combine("", webPath + @"\ODMImages\StudentDocument\" + newFilename);
                        var pathToSave = newFilename;
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }
                        employeeDocument.DocumentPath = pathToSave;
                        studentAggregateRepository.CreateStudentDocumentAttachment(employeeDocument);
                    }
                }
            }

            return RedirectToAction(nameof(CreateOrEditStudentDocumentDetails), new { id = ID }).WithSuccess("Data", "Saved successfully");
        }
        public IActionResult verifyDocument(long id, bool verify)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            StudentRequiredDocument ob = studentAggregateRepository.GetStudentRequiredDocumentById(id);
            if (verify == true)
            {
                ob.IsVerified = true;
                ob.VerifiedDate = DateTime.Now;
                ob.VerifiedAccessID = userId;
                ob.ModifiedId = userId;
            }
            else
            {
                ob.IsVerified = false;
                ob.VerifiedDate = null;
                ob.VerifiedAccessID = 0;
                ob.ModifiedId = userId;
            }
            studentAggregateRepository.UpdateStudentRequiredDocument(ob);
            return RedirectToAction(nameof(StudentDocumentDetails), new { id = ob.StudentID }).WithSuccess("Document", "Verified successfully");
        }
        public IActionResult approveDocument(long id, bool verify)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            StudentRequiredDocument ob = studentAggregateRepository.GetStudentRequiredDocumentById(id);
            if (verify == true)
            {
                ob.IsApproved = true;
                ob.ModifiedId = userId;
            }
            else
            {
                ob.IsApproved = false;
                ob.ModifiedId = userId;
            }
            studentAggregateRepository.UpdateStudentRequiredDocument(ob);
            return RedirectToAction(nameof(StudentDocumentDetails), new { id = ob.StudentID }).WithSuccess("Document", "Approved successfully");
        }
        public IActionResult rejectDocument(long id, bool verify)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            StudentRequiredDocument ob = studentAggregateRepository.GetStudentRequiredDocumentById(id);
            if (verify == true)
            {
                ob.IsReject = true;
                ob.IsVerified = false;
                ob.VerifiedDate = null;
                ob.VerifiedAccessID = 0;
                ob.IsApproved = false;
                ob.ModifiedId = userId;
            }
            else
            {
                ob.IsReject = false;
                ob.ModifiedId = userId;
            }
            studentAggregateRepository.UpdateStudentRequiredDocument(ob);
            return RedirectToAction(nameof(StudentDocumentDetails), new { id = ob.StudentID }).WithSuccess("Document", "Rejected successfully");
        }
        #endregion

        #region ------------------Student Academic Details-------------------------
        public IActionResult StudentAcademicDetails(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Experience", accessId, "List", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            ViewBag.StudentId = id;
            var res = studentCommonMethod.GetAllStudentAcademicByStudentId(id.Value);
            return View(res);
        }
        public IActionResult CreateOrEditStudentAcademic(long? id, long sid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            StudentAcademicModelClass studentacademic = new StudentAcademicModelClass();
            studentacademic.StudentID = sid;
            ViewBag.BoardList = boardRepository.GetAllBoard();
            ViewBag.ClassList = StandardRepository.GetAllStandard();

            if (id != null && id!=0)
            {
                if (commonMethods.checkaccessavailable("Student Document", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var employeeExp = studentAggregateRepository.GetStudentAcademicByStudentId(sid).ToList().Where(a => a.ID == id.Value).FirstOrDefault();
                studentacademic.BoardID = employeeExp.BoardID;
                studentacademic.FromDate = employeeExp.FromDate;
                studentacademic.ToDate = employeeExp.ToDate;
                studentacademic.ClassID = employeeExp.ClassID;
                studentacademic.OrganizationName = employeeExp.OrganizationName;
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("EDUCATIONAL").ID;
                studentacademic.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                       where a.DocumentTypeID == documentTypeId 
                                                       select new documentTypesCls
                                                       {
                                                           TypeId = a.DocumentTypeID,
                                                           ID = a.ID,
                                                           Name = a.Name,
                                                           IsRequired = studentCommonMethod.studentRequiredDocument(sid, a.DocumentTypeID,a.ID, employeeExp.ID)
                                                       }).ToList();
                ViewBag.status = "Update";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Student Document", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                studentacademic.BoardID = 0;
                studentacademic.FromDate = DateTime.Now.Date;
                studentacademic.ToDate = DateTime.Now.Date;
                studentacademic.ClassID = 0;
                studentacademic.OrganizationName = "";
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("EDUCATIONAL").ID;
                studentacademic.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                       where a.DocumentTypeID == documentTypeId
                                                       select new documentTypesCls
                                                       {
                                                           TypeId = a.DocumentTypeID,
                                                           ID = a.ID,
                                                           Name = a.Name,
                                                           IsRequired = false
                                                       }).ToList();
                var docsubtypelist = documentSubTypeRepository.GetAllDocumentSubTypes().Select(a=>a.ID).ToList();

                var studentdoclist = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.StudentID == sid).Select(a => a.DocumentSubTypeID).ToList();

                var resuit = docsubtypelist.Except(studentdoclist);



                ViewBag.status = "Create";
            }
            return View(studentacademic);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentAcademic([Bind("ID,BoardID,StudentID,FromDate,ToDate,ClassID,OrganizationName")] StudentAcademic studentAcademic, [Bind("documentTypesCls")] List<documentTypesCls> documentTypesCls)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                studentAcademic.ModifiedId = userId;
                studentAcademic.Active = true;
                if (studentAcademic.ID == 0)
                {
                    studentAcademic.InsertedId = userId;
                    studentAcademic.Status = EntityStatus.ACTIVE;
                    long sid = studentAggregateRepository.CreateStudentAcademic(studentAcademic);
                    foreach (var item in documentTypesCls)
                    {
                        if (item.IsRequired == true)
                        {
                            StudentRequiredDocument studentRequired = new StudentRequiredDocument();
                            studentRequired.Active = true;
                            studentRequired.DocumentSubTypeID = item.ID;
                            studentRequired.DocumentTypeID = item.TypeId;
                            studentRequired.StudentID = studentAcademic.StudentID;
                            studentRequired.InsertedId = userId;
                            studentRequired.IsApproved = false;
                            studentRequired.IsMandatory = true;
                            studentRequired.IsReject = false;
                            studentRequired.IsVerified = false;
                            studentRequired.ModifiedId = userId;
                            studentRequired.RelatedID = sid;
                            studentRequired.Status = EntityStatus.ACTIVE;
                            studentRequired.VerifiedAccessID = 0;
                            studentAggregateRepository.CreateStudentRequiredDocument(studentRequired);
                        }
                    }
                    return RedirectToAction(nameof(StudentAcademicDetails), new { id = studentAcademic.StudentID }).WithSuccess("Data", "Saved Successfully");
                }
                else
                {
                    studentAggregateRepository.UpdateStudentAcademic(studentAcademic);
                    foreach (var item in documentTypesCls)
                    {
                        StudentRequiredDocument employeeRequired = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.DocumentTypeID == item.TypeId && a.DocumentSubTypeID == item.ID && a.StudentID == studentAcademic.StudentID).FirstOrDefault();
                        if (item.IsRequired == true)
                        {
                            if (employeeRequired == null)
                            {
                                employeeRequired = new StudentRequiredDocument();
                                employeeRequired.Active = true;
                                employeeRequired.DocumentSubTypeID = item.ID;
                                employeeRequired.DocumentTypeID = item.TypeId;
                                employeeRequired.StudentID = studentAcademic.StudentID;
                                employeeRequired.InsertedId = userId;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = studentAcademic.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                studentAggregateRepository.CreateStudentRequiredDocument(employeeRequired);
                            }
                            else
                            {
                                employeeRequired.Active = true;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = studentAcademic.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                studentAggregateRepository.UpdateStudentRequiredDocument(employeeRequired);
                            }
                        }
                        else
                        {
                            if (employeeRequired != null)
                            {
                                employeeRequired.Active = false;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = studentAcademic.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                studentAggregateRepository.UpdateStudentRequiredDocument(employeeRequired);
                            }

                        }

                    }
                    return RedirectToAction(nameof(StudentAcademicDetails), new { id = studentAcademic.StudentID }).WithSuccess("Data", "Updated Successfully");
                }



            }
            return View(studentAcademic).WithDanger("error", "Not Saved");
        }
        //   [ActionName("DeleteEmployeeExperience")]
        public async Task<IActionResult> StudentAcademicDeleteConfirmed(long? id, long sid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student Document", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var studentdet = studentAggregateRepository.GetStudentAcademicById(id.Value);
            var documentTypeId = documentTypeRepository.GetDocumentTypeByName("EDUCATIONAL").ID;

            var documents = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.DocumentTypeID == documentTypeId && a.StudentID == studentdet.StudentID && a.RelatedID == studentdet.ID).ToList();
            foreach(var a in documents)
            {
                studentAggregateRepository.DeleteStudentRequiredDocument(a.ID);
            }

            var employeeExperience = studentAggregateRepository.DeleteStudentAcademic(id.Value);
           




            return RedirectToAction(nameof(StudentAcademicDetails), new { id = sid }).WithSuccess("Data", "Deleted successfully");
        }
        #endregion

        #region--------------student Bulk Upload----------------

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }       
        public IActionResult DownloadStudent()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            moduledt.Columns.Add("Student Code");
            moduledt.Columns.Add("First Name");
            moduledt.Columns.Add("Middle Name");
            moduledt.Columns.Add("Last Name");
            moduledt.Columns.Add("Gender");
            moduledt.Columns.Add("Date Of Birth");
            moduledt.Columns.Add("Blood Group");
            moduledt.Columns.Add("Nationality");
            moduledt.Columns.Add("Religion");
            moduledt.Columns.Add("Category");
            var student = studentAggregateRepository.GetAllStudent();
            var bloodgroups = bloodGroupRepository.GetAllBloodGroup();
            var NationalityList = nationalityTypeRepository.GetAllNationalities();
            var ReligionList = religionTypeRepository.GetAllReligionTypes();
            var CategoryList = categoryRepository.ListAllAsyncIncludeAll();
            if (student != null)
            {
                foreach (var a in student)
                {
                    DataRow dr = moduledt.NewRow();
                    dr["Student Code"] = a.StudentCode;
                    dr["First Name"] = a.FirstName;
                    dr["Middle Name"] = a.MiddleName;
                    dr["Last Name"] = a.LastName;
                    dr["Gender"] = a.Gender;
                    dr["Date Of Birth"] = a.DateOfBirth != null ? a.DateOfBirth.ToString("dd-MMM-yyyy") : "";
                    dr["Blood Group"] = (a.BloodGroupID == null || a.BloodGroupID == 0) ? "" : bloodgroups.Where(m => m.ID == a.BloodGroupID).FirstOrDefault().Name;
                    dr["Nationality"] = (a.NatiobalityID == null || a.NatiobalityID == 0) ? "" : NationalityList.Where(m => m.ID == a.NatiobalityID).FirstOrDefault().Name;
                    dr["Religion"] = (a.ReligionID == null || a.ReligionID == 0) ? "" : ReligionList.Where(m => m.ID == a.ReligionID).FirstOrDefault().Name;
                    dr["Category"] = (a.CategoryID == null || a.CategoryID == 0) ? "" : CategoryList.Result.Where(m => m.ID == a.CategoryID).FirstOrDefault().Name;
                    moduledt.Rows.Add(dr);
                }
            }
            moduledt.TableName = "Students";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StudentsList.xlsx");
        }

        public IActionResult DownloadStudentSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            XLWorkbook oWB = new XLWorkbook();

            var NationalityList = nationalityTypeRepository.GetAllNationalities().ToList();
            var ReligionList = religionTypeRepository.GetAllReligionTypes().ToList();
            var CategoryList = categoryRepository.ListAllAsyncIncludeAll().Result.ToList();
            var BloodGroupList = bloodGroupRepository.GetAllBloodGroup().ToList();

            DataTable genderdt = new DataTable();
            genderdt.Columns.Add("Gender");
            DataRow dr2 = genderdt.NewRow();
            dr2["Gender"] = "Male";
            genderdt.Rows.Add(dr2);
            DataRow dr1 = genderdt.NewRow();
            dr1["Gender"] = "Female";
            genderdt.Rows.Add(dr1);
            genderdt.TableName = "Gender";

            DataTable blooddt = new DataTable();
            blooddt.Columns.Add("BloodName");
            foreach (var a in BloodGroupList)
            {
                DataRow dr = blooddt.NewRow();
                dr["BloodName"] = a.Name;
                blooddt.Rows.Add(dr);
            }
            blooddt.TableName = "Blood Group";          

            DataTable nationalitydt = new DataTable();
            nationalitydt.Columns.Add("NationalityName");           
            foreach (var a in NationalityList)
            {
                DataRow dr = nationalitydt.NewRow();
                dr["NationalityName"] = a.Name;
                nationalitydt.Rows.Add(dr);
            }
            nationalitydt.TableName = "Nationality";

            DataTable religiondt = new DataTable();
            religiondt.Columns.Add("ReligionName");
            foreach (var a in ReligionList)
            {
                DataRow dr = religiondt.NewRow();
                dr["ReligionName"] = a.Name;
                religiondt.Rows.Add(dr);
            }
            religiondt.TableName = "Religion";


            DataTable categorydt = new DataTable();
            categorydt.Columns.Add("CategoryName");
            foreach (var a in CategoryList)
            {
                DataRow dr = categorydt.NewRow();
                dr["CategoryName"] = a.Name;
                categorydt.Rows.Add(dr);
            }
            categorydt.TableName = "Category";

         
            int lastCellNo1 = genderdt.Rows.Count + 1;
            int lastCellNo2 = blooddt.Rows.Count + 1;
            int lastCellNo3 = nationalitydt.Rows.Count + 1;
            int lastCellNo4 = religiondt.Rows.Count + 1;
            int lastCellNo5 = categorydt.Rows.Count + 1;

            oWB.AddWorksheet(genderdt);
            oWB.AddWorksheet(blooddt);           
            oWB.AddWorksheet(nationalitydt);
            oWB.AddWorksheet(religiondt);
            oWB.AddWorksheet(categorydt);
           
            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            var worksheet3 = oWB.Worksheet(3);
            var worksheet4 = oWB.Worksheet(4);
            var worksheet5 = oWB.Worksheet(5);

            DataTable validationTable = new DataTable();         
            validationTable.Columns.Add("First Name");
            validationTable.Columns.Add("Middle Name");
            validationTable.Columns.Add("Last Name");
            validationTable.Columns.Add("Gender");
            validationTable.Columns.Add("Date Of Birth");
            validationTable.Columns.Add("Blood Group");
            validationTable.Columns.Add("Nationality");
            validationTable.Columns.Add("Religion");
            validationTable.Columns.Add("Category");
            validationTable.TableName = "Students";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(4).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(6).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(7).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
            worksheet.Column(8).SetDataValidation().List(worksheet4.Range("A2:A" + lastCellNo4), true);
            worksheet.Column(9).SetDataValidation().List(worksheet5.Range("A2:A" + lastCellNo5), true);
            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();
            worksheet4.Hide();
            worksheet5.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StudentSample.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadStudent(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                if (file != null)
                {

                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[5];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var request = studentAggregateRepository.GetAllStudent();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            Student student = new Student();                           
                            Random generator = new Random();
                            String s = generator.Next(0, 999999).ToString("D6");
                            var StudentCode = "ODMEG-" + s;

                            if (request.Where(a => a.StudentCode == StudentCode).FirstOrDefault() == null)
                            {
                                student.StudentCode = StudentCode;
                                student.ModifiedId = userId;                             

                                if (firstWorksheet.Cells[i, 1] != null && firstWorksheet.Cells[i, 1].Value != null)
                                {
                                    student.FirstName = firstWorksheet.Cells[i, 1].Value.ToString();

                                }
                                if (firstWorksheet.Cells[i, 2] != null && firstWorksheet.Cells[i, 2].Value != null)
                                {
                                    student.MiddleName = firstWorksheet.Cells[i, 2].Value.ToString();

                                }                                
                                student.LastName = firstWorksheet.Cells[i, 3].Value.ToString();
                                student.Gender = firstWorksheet.Cells[i, 4].Value.ToString();
                                string dateTime = firstWorksheet.Cells[i, 5].Value.ToString();
                                student.DateOfBirth = Convert.ToDateTime(dateTime);

                                student.BloodGroupID = bloodGroupRepository.GetBloodGroupByName(
                                    firstWorksheet.Cells[i, 6].Value.ToString()).ID;

                                student.NatiobalityID = nationalityTypeRepository.GetNationalityByName(
                                   firstWorksheet.Cells[i, 7].Value.ToString()).ID;
                                student.ReligionID = religionTypeRepository.GetReligionTypeByName(
                                 firstWorksheet.Cells[i, 8].Value.ToString()).ID;
                                student.CategoryID = categoryRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Name == firstWorksheet.Cells[i, 9].Value.ToString()).FirstOrDefault().ID;                           
                                student.Active = true;
                                student.InsertedId = userId;                               
                                student.IsAvailable = true;
                                student.ModifiedId = userId;
                                student.Status = EntityStatus.ACTIVE;
                                long id = studentAggregateRepository.CreateStudent(student);
                                if (id > 0)
                                {
                                    Access accesss = new Access();
                                    accesss.RoleID = 5;
                                    accesss.Username = student.StudentCode;
                                    accesss.EmployeeID = id;
                                    accesss.Active = true;
                                    accesss.InsertedDate = DateTime.Now;
                                    accesss.InsertedId = userId;
                                    accesss.ModifiedDate = DateTime.Now;
                                    accesss.ModifiedId = userId;
                                    accessRepository.CreateAccess(accesss);                                    
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(Index)).WithSuccess("success", "Excel uploaded successfully"); ;
        }

        public IActionResult DownloadStudentParentSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            XLWorkbook oWB = new XLWorkbook();
            var ProfessionList = studentAggregateRepository.GetAllProfessionType().ToList();
            var StudentList = studentAggregateRepository.GetAllStudent().ToList();
            var ParentRelationshipList = studentAggregateRepository.GetAllParentRelationshipType().ToList();


            DataTable professiondt = new DataTable();
            professiondt.Columns.Add("Profession Type");
            foreach (var a in ProfessionList)
            {
                DataRow dr = professiondt.NewRow();
                dr["Profession Type"] = a.Name;
                professiondt.Rows.Add(dr);
            }
            professiondt.TableName = "Profession Type";

            DataTable parentRelationsdt = new DataTable();
            parentRelationsdt.Columns.Add("ParentRelationship");
            foreach (var a in ParentRelationshipList)
            {
                DataRow dr = parentRelationsdt.NewRow();
                dr["ParentRelationship"] = a.Name;
                parentRelationsdt.Rows.Add(dr);
            }
            parentRelationsdt.TableName = "ParentRelationship";

            DataTable studentdt = new DataTable();
            studentdt.Columns.Add("Student");
            foreach (var a in StudentList)
            {
                DataRow dr = studentdt.NewRow();
                dr["Student"] = a.StudentCode;
                studentdt.Rows.Add(dr);
            }
            studentdt.TableName = "Student";        

          
            int lastCellNo1 = professiondt.Rows.Count + 1;
            int lastCellNo2 = studentdt.Rows.Count + 1;
            int lastCellNo3 = parentRelationsdt.Rows.Count + 1;
                  

            oWB.AddWorksheet(professiondt);          
            oWB.AddWorksheet(studentdt);
            oWB.AddWorksheet(parentRelationsdt);


            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            var worksheet3 = oWB.Worksheet(3);         


            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("First Name");
            validationTable.Columns.Add("Middle Name");
            validationTable.Columns.Add("Last Name");
            validationTable.Columns.Add("Primary Mobile");
            validationTable.Columns.Add("Alternative Mobile");
            validationTable.Columns.Add("Landline Number");
            validationTable.Columns.Add("EmailId");
            validationTable.Columns.Add("Profession Type");
            validationTable.Columns.Add("Student");
            validationTable.Columns.Add("ParentRelationship");
            validationTable.Columns.Add("ParentOrganization");    
            
            validationTable.TableName = "StudentParent_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(8).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(9).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(10).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
           
            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();
          
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StudentParentSample.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadStudentParent(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                if (file != null)
                {

                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[3];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var request = studentAggregateRepository.GetAllParent();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            Parent parent = new Parent();                          
                            string PrimaryNumber = firstWorksheet.Cells[i, 4].Value.ToString();

                            if (PrimaryNumber != null)
                            {
                                     parent.ModifiedId = userId;                                
                                    parent.FirstName = firstWorksheet.Cells[i, 1].Value.ToString();                                
                                if (firstWorksheet.Cells[i, 2] != null && firstWorksheet.Cells[i, 2].Value != null)
                                {
                                    parent.MiddleName = firstWorksheet.Cells[i, 2].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 3] != null && firstWorksheet.Cells[i, 3].Value != null)
                                {
                                    parent.LastName = firstWorksheet.Cells[i, 3].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 4] != null && firstWorksheet.Cells[i, 4].Value != null)
                                {
                                    parent.PrimaryMobile = firstWorksheet.Cells[i, 4].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 5] != null && firstWorksheet.Cells[i, 5].Value != null)
                                {
                                    parent.AlternativeMobile = firstWorksheet.Cells[i, 5].Value.ToString();

                                }
                                if (firstWorksheet.Cells[i, 6] != null && firstWorksheet.Cells[i, 6].Value != null)
                                {
                                    parent.LandlineNumber = firstWorksheet.Cells[i, 6].Value.ToString();

                                }
                                parent.EmailId = firstWorksheet.Cells[i, 7].Value.ToString();
                                string ProfessionTypeName= firstWorksheet.Cells[i, 8].Value.ToString();
                                string StudentCode = firstWorksheet.Cells[i, 9].Value.ToString();
                                string ParentRelationshipName = firstWorksheet.Cells[i, 10].Value.ToString();
                                parent.ProfessionTypeID = studentAggregateRepository.GetAllProfessionType().Where(a => a.Name == ProfessionTypeName).Select(a => a.ID).FirstOrDefault();
                                parent.StudentID = studentAggregateRepository.GetAllStudent().Where(a => a.StudentCode == StudentCode).Select(a => a.ID).FirstOrDefault(); ;
                                parent.ParentRelationshipID = studentAggregateRepository.GetAllParentRelationshipType().Where(a => a.Name == ParentRelationshipName).Select(a => a.ID).FirstOrDefault();
                                if (firstWorksheet.Cells[i, 11] != null && firstWorksheet.Cells[i, 11].Value != null)
                                {
                                    parent.ParentOrganization = firstWorksheet.Cells[i, 11].Value.ToString();

                                }
                                parent.Active = true;
                                parent.InsertedId = userId;
                                parent.IsAvailable = true;
                                parent.ModifiedId = userId;
                                parent.Status = EntityStatus.ACTIVE;
                                long id = studentAggregateRepository.CreateParent(parent);
                                if (id > 0)
                                {
                                    Access accesss = new Access();
                                    accesss.RoleID = 6;
                                    accesss.Username = parent.PrimaryMobile;
                                    accesss.EmployeeID = id;
                                    accesss.Active = true;
                                    accesss.InsertedDate = DateTime.Now;
                                    accesss.InsertedId = userId;
                                    accesss.ModifiedDate = DateTime.Now;
                                    accesss.ModifiedId = userId;
                                    accessRepository.CreateAccess(accesss);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(Index)).WithSuccess("success", "Excel uploaded successfully"); ;
        }

        public IActionResult DownloadStudentParent()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
           
            moduledt.Columns.Add("First Name");
            moduledt.Columns.Add("Middle Name");
            moduledt.Columns.Add("Last Name");
            moduledt.Columns.Add("Primary Mobile");
            moduledt.Columns.Add("Alternative Mobile");
            moduledt.Columns.Add("Landline Number");
            moduledt.Columns.Add("EmailId");
            moduledt.Columns.Add("Profession Type");
            moduledt.Columns.Add("Student Code");
            moduledt.Columns.Add("Student Name");
            moduledt.Columns.Add("ParentRelationship");
            moduledt.Columns.Add("ParentOrganization");


            var studentparent = studentAggregateRepository.GetAllParent();
            var ProfessionList = studentAggregateRepository.GetAllProfessionType().ToList();
            var StudentList = studentAggregateRepository.GetAllStudent().ToList();
            var ParentRelationshipList = studentAggregateRepository.GetAllParentRelationshipType().ToList();


            if (studentparent != null)
            {
                foreach (var a in studentparent)
                {
                    DataRow dr = moduledt.NewRow();
                    dr["First Name"] = a.FirstName;                  
                    dr["Middle Name"] = a.MiddleName;
                    dr["Last Name"] = a.LastName;
                    dr["Primary Mobile"] = a.PrimaryMobile;
                    dr["Alternative Mobile"] = a.AlternativeMobile;
                    dr["Landline Number"] = a.LandlineNumber;
                    dr["EmailId"] = a.EmailId;
                    dr["Profession Type"] = (a.ProfessionTypeID == null || a.ProfessionTypeID == 0) ? "" : ProfessionList.Where(m => m.ID == a.ProfessionTypeID).FirstOrDefault().Name;
                    dr["Student Code"] = (a.StudentID == null || a.StudentID == 0) ? "" : StudentList.Where(m => m.ID == a.StudentID).FirstOrDefault().StudentCode;
                    dr["Student Name"] = (a.StudentID == null || a.StudentID == 0) ? "" : StudentList.Where(m => m.ID == a.StudentID).FirstOrDefault().FirstName;

                    dr["ParentRelationship"] = (a.ParentRelationshipID == null || a.ParentRelationshipID == 0) ? "" : ParentRelationshipList.Where(m => m.ID == a.ParentRelationshipID).FirstOrDefault().Name;
                    dr["ParentOrganization"] = a.ParentOrganization;
                    moduledt.Rows.Add(dr);
                }
            }
            moduledt.TableName = "StudentsParent";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StudentsParentList.xlsx");
        }
        #endregion

        
    }
}