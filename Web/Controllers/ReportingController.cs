﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Web.Models;

namespace Web.Controllers
{
    public class ReportingController : Controller
    {
        private IAuthorityAggregateRepository authorityAggregateRepository;
        private IOrganizationRepository organizationRepository;
        private IDepartmentRepository departmentRepository;
        private IEmployeeRepository employeeRepository;

        private CommonMethods commonMethods;
        public ReportingController(CommonMethods common, IAuthorityAggregateRepository authorityAggregateRepo, IEmployeeRepository employeeRepo, IDepartmentRepository departmentRepo,
            IOrganizationRepository organizationRepo)
        {
            commonMethods = common;
            authorityAggregateRepository = authorityAggregateRepo;
            employeeRepository = employeeRepo;
            departmentRepository = departmentRepo;
            organizationRepository = organizationRepo;

        }
        public IActionResult Index()
        {
            return View();
        }
        #region DepartmentAuthority
        public IActionResult DepartmentAuthority()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Department Authority", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var departmentAuthority = authorityAggregateRepository.GetAllDepartmentAuthority();
            var authorityType = authorityAggregateRepository.GetAllAuthorityType();
            var department = departmentRepository.GetAllDepartment();
            var authorities = authorityAggregateRepository.GetAllAuthority();
            var organisation = organizationRepository.GetAllOrganization();
            var employees = employeeRepository.GetAllEmployee();
            if (departmentAuthority!=null)
            {
                var res = (from a in departmentAuthority
                           join b in authorities on a.AuthorityID equals b.ID
                           join c in department on a.DepartmentID equals c.ID
                           join d in organisation on c.OrganizationID equals d.ID
                           join e in employees on a.EmployeeID equals e.ID
                           join f in authorityType on b.AuthorityTypeID equals f.ID
                           select new DepartmentAuthoritycls
                           {
                               ID = a.ID,
                               DepartmentID = a.DepartmentID,
                               DepartmentName = commonMethods.GetDepartmentbyparentid(c.ID, c.OrganizationID),
                               ModifiedDate = a.ModifiedDate,
                               AuthorityID = a.AuthorityID,
                               AuthorityName = b.Name,
                               OrganisationName = d.Name,
                               AuthorityTypeName = f.Name,
                               EmployeeName = e.FirstName + " " + (e.MiddleName != "" ? (e.MiddleName + " " + e.LastName) : e.LastName)
                           }).ToList();
                return View(res);
            }
            return View(null);
        }

        public IActionResult CreateOrEditDepartmentAuthority(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            DepartmentAuthoritycls od = new DepartmentAuthoritycls();

            od.Organisations = (from a in organizationRepository.GetAllOrganization().ToList()
                                select new CommonMasterModel
                                {
                                    ID = a.ID,
                                    Name = a.Name
                                }).ToList();
            od.Employees= (from a in employeeRepository.GetAllEmployee().ToList()
                           select new CommonMasterModel
                           {
                               ID = a.ID,
                               Name = a.FirstName + " " + (a.MiddleName != "" ? (a.MiddleName + " " + a.LastName) : a.LastName)
                           }).ToList();
            od.authorityTypes= (from a in authorityAggregateRepository.GetAllAuthorityType().ToList()
                                select new CommonMasterModel
                                {
                                    ID = a.ID,
                                    Name = a.Name
                                }).ToList();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Department Authority", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.AuthorityID = 0;
                od.AuthorityTypeID = 0;
                od.DepartmentID = 0;
                od.EmployeeID = 0;
                od.OrganisationID = 0;
            }
            else
            {
                if (commonMethods.checkaccessavailable("Department Authority", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var departmentAuthority = authorityAggregateRepository.GetDepartmentAuthorityById(id.Value);
                od.ID = departmentAuthority.ID;
                od.AuthorityID = departmentAuthority.AuthorityID;
                var auth = authorityAggregateRepository.GetAuthorityById(departmentAuthority.AuthorityID).AuthorityTypeID;

                od.Authoritys = (from a in authorityAggregateRepository.GetAllAuthority()
                                 where a.AuthorityTypeID == auth
                                 select new CommonMasterModel {
                                     ID=a.ID,
                                     Name=a.Name
                                 }).ToList();
                od.AuthorityTypeID = auth;
                var org = departmentRepository.GetDepartmentById(departmentAuthority.DepartmentID).OrganizationID;
                od.Departments = commonMethods.GetDepartments();
                od.DepartmentID = departmentAuthority.DepartmentID;
                od.EmployeeID = departmentAuthority.EmployeeID;
                od.OrganisationID = org;

            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateDepartmentAuthority([Bind("ID,AuthorityID,DepartmentID,EmployeeID")] DepartmentAuthority departmentAuthority)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                departmentAuthority.ModifiedId = userId;
                departmentAuthority.Active = true;
                if (departmentAuthority.ID == 0)
                {
                    departmentAuthority.InsertedId = userId;
                    departmentAuthority.Status = "CREATED";
                    authorityAggregateRepository.CreateDepartmentAuthority(departmentAuthority);
                }
                else
                {
                    authorityAggregateRepository.UpdateDepartmentAuthority(departmentAuthority);
                }

                return RedirectToAction(nameof(DepartmentAuthority));
            }
            return View(departmentAuthority);
        }

        [ActionName("DeleteDepartmentAuthority")]
        public async Task<IActionResult> DepartmentAuthorityDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Department Authority", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (DepartmentAuthorityExists(id.Value) == true)
            {
                var Section = authorityAggregateRepository.DeleteDepartmentAuthority(id.Value);
            }
            return RedirectToAction(nameof(DepartmentAuthority));
        }
        public async Task<JsonResult> GetAuthorityByTypeId(long id)
        {
            var res = (from a in authorityAggregateRepository.GetAllAuthority().ToList()
                       where a.AuthorityTypeID == id
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        public async Task<JsonResult> GetDepartmentByOrgId(long id)
        {
            var res = (from a in departmentRepository.GetAllDepartment().ToList()
                       where a.OrganizationID == id
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = commonMethods.GetDepartmentbyparentid(a.ID, a.OrganizationID)
                       }).ToList();
            return Json(res);
        }
        private bool DepartmentAuthorityExists(long id)
        {
            if (authorityAggregateRepository.GetDepartmentAuthorityById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}