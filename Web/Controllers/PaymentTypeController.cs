﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Web.Models;
using OdmErp.Web.Models.StudentPayment;
using OfficeOpenXml;
using ClosedXML.Excel;
using System.IO;
using Web.Controllers;
using OdmErp.ApplicationCore.Entities.StudentPaymentAggregate;
using OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository;

namespace OdmErp.Web.Controllers
{
    public class PaymentTypeController : AppController
    {
        public Extensions.SessionExtensions sessionExtensions;
        public commonsqlquery commonsql;
        // long userId = 0;
        private CommonMethods commonMethods;
        public IPaymentType paymentTypeRepository;
        public PaymentTypeController(IPaymentType paymentTypeRepository, commonsqlquery commonsql, CommonMethods commonMethods)
        {
            this.paymentTypeRepository = paymentTypeRepository;
            this.commonsql = commonsql;
            this.commonMethods = commonMethods;
        }
        public IActionResult PaymentType()
        {
            CheckLoginStatus();
             long accessId = HttpContext.Session.GetInt32("accessId").Value;
             long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("PaymentType", accessId, "List", "PaymentType", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(paymentTypeRepository.GetAllPaymentType());
        }
        //public IActionResult downloadBankSample()
        //{
        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                       // long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    userId = HttpContext.Session.GetInt32("userId").Value;
        //    if (commonMethods.checkaccessavailable("Bank", accessId, "Upload", "Master", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }
        //    var comlumHeadrs = new string[]
        // {
        //        "Name"
        // };

        //    byte[] result;

        //    using (var package = new ExcelPackage())
        //    {
        //        var worksheet = package.Workbook.Worksheets.Add("Bank"); //Worksheet name
        //        using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
        //        {
        //            cells.Style.Font.Bold = true;
        //        }

        //        //First add the headers
        //        for (var i = 0; i < comlumHeadrs.Count(); i++)
        //        {
        //            worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
        //        }
        //        result = package.GetAsByteArray();
        //    }

        //    return File(result, "application/ms-excel", $"Bank.xlsx");
        //}
        //public IActionResult DownloadBank()
        //{
        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                       // long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    if (commonMethods.checkaccessavailable("Bank", accessId, "Download", "Master", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }
        //    var comlumHeadrs = new string[]
        //   {
        //       // "Sl No",
        //        "Name"
        //       // "Inserted On",
        //       // "Modified On"
        //   };

        //    byte[] result;

        //    using (var package = new ExcelPackage())
        //    {
        //        var worksheet = package.Workbook.Worksheets.Add("Bank"); //Worksheet name
        //        using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
        //        {
        //            cells.Style.Font.Bold = true;
        //        }

        //        //First add the headers
        //        for (var i = 0; i < comlumHeadrs.Count(); i++)
        //        {
        //            worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
        //        }

        //        //Add values
        //        int m = 1;
        //        var j = 2;
        //        foreach (var obj in bankRepository.GetAllBank())
        //        {
        //            // worksheet.Cells["A" + j].Value = m;
        //            worksheet.Cells["A" + j].Value = obj.Name;
        //            // worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
        //            // worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
        //            j++;
        //            m++;
        //        }
        //        result = package.GetAsByteArray();
        //    }

        //    return File(result, "application/ms-excel", $"Bank.xlsx");
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public IActionResult UploadBank(IFormFile file)
        //{
        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                       // long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    userId = HttpContext.Session.GetInt32("userId").Value;
        //    if (commonMethods.checkaccessavailable("Bank", accessId, "Upload", "Master", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }

        //    if (file != null)
        //    {
        //        // Create a File Info 
        //        FileInfo fi = new FileInfo(file.FileName);
        //        var newFilename = "Bank_" + String.Format("{0:d}",
        //                          (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
        //        var webPath = hostingEnvironment.WebRootPath;
        //        string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
        //        var pathToSave = newFilename;
        //        using (var stream = new FileStream(path, FileMode.Create))
        //        {
        //            file.CopyToAsync(stream);
        //        }
        //        FileInfo fil = new FileInfo(path);
        //        using (ExcelPackage excelPackage = new ExcelPackage(fil))
        //        {
        //            //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
        //            ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
        //            int totalRows = firstWorksheet.Dimension.Rows;
        //            for (int i = 2; i <= totalRows; i++)
        //            {
        //                Bank bank = new Bank();
        //                string name = firstWorksheet.Cells[i, 1].Value.ToString();
        //                if (bankRepository.GetBankByName(name) == null)
        //                {
        //                    bank.Name = firstWorksheet.Cells[i, 1].Value.ToString();
        //                    bank.ModifiedId = userId;
        //                    bank.Active = true;
        //                    bank.InsertedId = userId;
        //                    bank.Status = EntityStatus.ACTIVE;
        //                    bankRepository.CreateBank(bank);
        //                }
        //            }
        //        }
        //    }
        //    return RedirectToAction(nameof(Bank)).WithSuccess("success", "Excel uploaded successfully"); ;
        //}
        public IActionResult CreateOrEditPaymentType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            Cls_PaymentType od = new Cls_PaymentType();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("PaymentType", accessId, "Create", "PaymentType", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.Descriptions = "";
                od.BillDeskTransactionCode = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("PaymentType", accessId, "Edit", "PaymentType", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var PaymentType = paymentTypeRepository.GetPaymentTypeById(id.Value);
                od.ID = PaymentType.ID;
                od.Name = PaymentType.Name;
                od.Descriptions = PaymentType.Descriptions;
                od.BillDeskTransactionCode = PaymentType.BillDeskTransactionCode;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdatePaymentType([Bind("ID,Name,Descriptions,BillDeskTransactionCode")] PaymentType paymentType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                paymentType.ModifiedId = userId;
                paymentType.Active = true;
                if (paymentType.ID == 0)
                {
                    paymentType.InsertedId = userId;
                    paymentType.Status = EntityStatus.ACTIVE;
                    paymentTypeRepository.CreatePaymentType(paymentType);
                    return RedirectToAction(nameof(PaymentType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    paymentTypeRepository.UpdatePaymentType(paymentType);
                    return RedirectToAction(nameof(PaymentType)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(paymentType).WithDanger("error", "Not Saved"); ;
        }

        [ActionName("DeletePaymentType")]
        public async Task<IActionResult> PaymentTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("PaymentType", accessId, "Delete", "PaymentType", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (PaymentTypeExists(id.Value) == true)
            {
                var Bank = paymentTypeRepository.DeletePaymentType(id.Value);
            }
            return RedirectToAction(nameof(PaymentType)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool PaymentTypeExists(long id)
        {
            if (paymentTypeRepository.GetPaymentTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
