﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Web.Models;
using OdmErp.Web.Models.StudentPayment;
using Web.Controllers;
using X.PagedList.Mvc;
using X.PagedList;
using OdmErp.Web.ViewModels;
using OdmErp.ApplicationCore.Entities.SessionFeesType;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OfficeOpenXml;
using ClosedXML.Excel;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using System.Data;

namespace OdmErp.Web.Controllers
{
    public class FeesTypePriceController : AppController
    {
        private CommonMethods commonMethods;
        private IFeesTypePrice feesTypePriceRepository;
        private ICountryRepository countryRepository;
        private IAdmissionTypeRepository admissionTypeRepository;
        private IBankTypeRepository bankTypeRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private IBoardRepository boardRepository;
        private IBoardStandardRepository boardStandardRepository;
        private IDesignationLevelRepository designationLevelRepository;
        private IEducationQualificationTypeRepository educationQualificationTypeRepository;
        private IGroupRepository groupRepository;
        private IModuleRepository moduleRepository;
        private IOrganizationTypeRepository organizationTypeRepository;
        private IRoleRepository roleRepository;
        private ISectionRepository sectionRepository;
        private CommonSchoolModel commonschoolmodel;
        private IStateRepository stateRepository;
        private IDepartmentRepository departmentRepository;
        private IDesignationRepository designationRepository;
        private IEducationQualificationRepository educationQualificationRepository;
        private IStandardRepository standardRepository;
        private IStreamRepository streamRepository;
        private ISubModuleRepository subModuleRepository;
        private ICityRepository cityRepository;
        private IOrganizationRepository organizationRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IReligionTypeRepository religionTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private IBankRepository bankRepository;
        private IBankBranchRepository bankBranchRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IAccessTypeRepository accessTypeRepository;
        private IActionRepository actionRepository;
        private IAuthorityAggregateRepository authorityAggregateRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IActionAccessRepository actionAccessRepository;
        private IDepartmentLeadRepository departmentLeadRepository;
        private IEmployeeRepository employeeRepository;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IHttpContextAccessor _httpContextAccessor;
        public Extensions.SessionExtensions sessionExtensions;

        public IAcademicSessionRepository academicSessionRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IClassTimingRepository classTimingRepository;
        public commonsqlquery commonsql;
        private IFeesTypeRepository feesTypeRepository;
        public ISessionFeesType sessionFeesTypeRepo;
        public IFeesTypeApplicableRepository applicableRepo;

        private readonly IFileProvider fileProvider;
        private readonly IHostingEnvironment hostingEnvironment;
        public FeesTypePriceController(IFeesTypePrice feesTypePriceRepo, IFeesTypeApplicableRepository applicableRepository, ISessionFeesType sessionFeesTypeRepository, CommonSchoolModel comnschoolmodels, IFeesTypeRepository feesTypeRepo, IAdmissionTypeRepository admissionTypeRepo, IBloodGroupRepository bloodGroupRepo,
           IBoardRepository boardRepo, IDesignationLevelRepository designationLevelRepo, IEducationQualificationTypeRepository educationQualificationTypeRepo, IGroupRepository groupRepo,
           IModuleRepository moduleRepo, IOrganizationTypeRepository organizationTypeRepo, IRoleRepository roleRepo, ISectionRepository sectionRepo,
           IStateRepository stateRepo, IDepartmentRepository departmentRepo, IDesignationRepository designationRepo, IEducationQualificationRepository educationQualificationRepo, IStandardRepository standardRepo,
           IStreamRepository streamRepo, ISubModuleRepository subModuleRepo, ICityRepository cityRepo, INationalityTypeRepository nationalityTypeRepo, IReligionTypeRepository religionTypeRepo,
           IAddressTypeRepository addressTypeRepo, IBankRepository bankRepo, IBankBranchRepository bankBranchRepo, IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo,
           IAccessTypeRepository accessTypeRepo, IActionRepository actionRepo, CommonMethods commonMeth, IAuthorityAggregateRepository authorityAggregateRepo, IStudentAggregateRepository studentAggregateRepo, IActionAccessRepository actionAccessRepo,
           IDepartmentLeadRepository departmentlead, IHttpContextAccessor httpContextAccessor, IEmployeeRepository emprepo, IEmployeeDesignationRepository empdesrepo, IBankTypeRepository bankTypeRepo,
           IBoardStandardRepository boardStandardRepository, IClassTimingRepository classTimingRepo, IAcademicSessionRepository academicSessionRepository, commonsqlquery commonsqlquery, IOrganizationAcademicRepository organizationAcademicRepository,
           IOrganizationRepository organizationRepository, CommonSchoolModel commonsclmodl, IFileProvider fileProvider, IHostingEnvironment hostingEnvironment
         )
        {
            feesTypeRepository = feesTypeRepo;
            feesTypePriceRepository = feesTypePriceRepo;
            // countryRepository = countryRepo;
            admissionTypeRepository = admissionTypeRepo;
            bloodGroupRepository = bloodGroupRepo;
            boardRepository = boardRepo;
            designationLevelRepository = designationLevelRepo;
            educationQualificationTypeRepository = educationQualificationTypeRepo;
            groupRepository = groupRepo;
            moduleRepository = moduleRepo;
            organizationTypeRepository = organizationTypeRepo;
            roleRepository = roleRepo;
            sectionRepository = sectionRepo;
            bankTypeRepository = bankTypeRepo;
            stateRepository = stateRepo;
            departmentRepository = departmentRepo;
            designationRepository = designationRepo;
            educationQualificationRepository = educationQualificationRepo;
            standardRepository = standardRepo;
            streamRepository = streamRepo;
            subModuleRepository = subModuleRepo;
            cityRepository = cityRepo;
            this.organizationRepository = organizationRepository;
            nationalityTypeRepository = nationalityTypeRepo;
            religionTypeRepository = religionTypeRepo;
            addressTypeRepository = addressTypeRepo;
            bankRepository = bankRepo;
            bankBranchRepository = bankBranchRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            accessTypeRepository = accessTypeRepo;
            actionRepository = actionRepo;
            commonMethods = commonMeth;
            authorityAggregateRepository = authorityAggregateRepo;
            studentAggregateRepository = studentAggregateRepo;
            actionAccessRepository = actionAccessRepo;
            _httpContextAccessor = httpContextAccessor;
            departmentLeadRepository = departmentlead;
            employeeRepository = emprepo;
            employeeDesignationRepository = empdesrepo;
            this.boardStandardRepository = boardStandardRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            commonsql = commonsqlquery;
            classTimingRepository = classTimingRepo;
            sessionFeesTypeRepo = sessionFeesTypeRepository;
            commonschoolmodel = comnschoolmodels;
            applicableRepo = applicableRepository;
            commonschoolmodel = commonsclmodl;
        }
        public IActionResult FeesTypePrice(int? page, int? noofData)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesTypePrice", accessId, "List", "FessTypePrice", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var onePageOfEmps = commonschoolmodel.GetAllFeesTypePrices();
            return View("FeesTypePrice", onePageOfEmps);
        }
        [HttpGet]
        public JsonResult EditFeeTypePrice(long? id)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            ApplicationCore.Entities.FeesTypePriceData od = new ApplicationCore.Entities.FeesTypePriceData();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Create", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Create";
                //od.ID = 0;
                //od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Edit", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Update";
                var fessTypePrice = feesTypePriceRepository.GetFeesTypePriceById(id.Value);
                od.ID = id.Value;
                od.AcademicSessionId = fessTypePrice.AcademicSessionId;
                od.FeesTypePriceId = fessTypePrice.FeesTypePriceId;
                od.sessionFeesTypePriceApplicableData = fessTypePrice.sessionFeesTypePriceApplicableData;


            }
            return Json(od);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SaveOrUpdateFeesTypePrice(CS_FessTypePriceData feesTypePrice)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                OdmErp.ApplicationCore.Entities.FeesTypePriceData feesTypePrice1 = new FeesTypePriceData();

                feesTypePrice1.ID = feesTypePrice.ID;
                feesTypePrice1.AcademicSessionId = feesTypePrice.AcademicSessionId;
                feesTypePrice1.FeesTypePriceId = feesTypePrice.FeesTypePriceId;
                feesTypePrice1.InsertedId = userId;

                List<SessionFeesTypeApplicableData> newchild = new List<SessionFeesTypeApplicableData>();
                List<CS_SessionFeeTypePriceApplicableData> ObjChildData = new List<CS_SessionFeeTypePriceApplicableData>();
                ObjChildData = feesTypePrice.sessionFeesTypePriceApplicableData;

                foreach (var value in ObjChildData)
                {
                    feesTypePrice1.sessionFeesTypePriceApplicableData.Add(new SessionFeesTypeApplicableData() { FeesTypePriceId=value.FeesTypePriceId, InsertedId = userId, SessionName = value.SessionName, FeesTypeName = value.FeesTypeName, ApplicabledataId = value.ApplicabledataId, Price = value.Price, ApplicableToId = value.ApplicableToId });
                }

                var result = false;
                foreach(var a in feesTypePrice1.sessionFeesTypePriceApplicableData)
                {
                    if(CheckFeesTypeExists(feesTypePrice1.FeesTypePriceId,a.ApplicabledataId,a.Price)==true)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                long i = 0;
                if (result == true)
                {
                    i = feesTypePriceRepository.CreateFeesTypePrice(feesTypePrice1);
                }
                else {
                    feesTypePrice.ReturnMsg = "Error !!!";
                }
                if (i == 1)
                {
                    feesTypePrice.ReturnMsg = "Records added Successfully.";
                }
                else
                {
                    feesTypePrice.ReturnMsg = "Error !!!";
                }

            }
            return Json(feesTypePrice);
        }

        public JsonResult Update(CS_FessTypePriceData feesTypePrice)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                //OdmErp.ApplicationCore.Entities.SessionFeesTypePrice feesTypePrice1 = new SessionFeesTypePrice();
                //feesTypePrice1.ID = feesTypePrice.ID;
                //feesTypePrice1.AcademicSessionId = feesTypePrice.AcademicSessionId;
                //feesTypePrice1.SessionFeesTypeId = feesTypePrice.SessionFeesTypeId;
                //feesTypePrice1.Active = true;
                //SessionFeeTypePriceApplicableData newchild = new SessionFeeTypePriceApplicableData();
                //List<CS_SessionFeeTypePriceApplicableData> ObjChildData = new List<CS_SessionFeeTypePriceApplicableData>();
                //ObjChildData = feesTypePrice.sessionFeeTypePriceApplicableData;

                //foreach (var a in ObjChildData)
                //{
                //    newchild.ApplicabledataId = a.ApplicabledataId;
                //    newchild.FeesTypePriceId = a.FeesTypePriceId;
                //    feesTypePrice1.sessionFeeTypePriceApplicableData.Add(newchild);
                //}               

                //TODO
                // feesTypePrice1.ModifiedId = userId;
                // feesTypePriceRepository.UpdateFeesTypePrice(feesTypePrice1);
                //feesTypePrice.ReturnMsg = "Records Updated Successfully.";
                //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Updated successfully");
                //return Json("");
                OdmErp.ApplicationCore.Entities.FeesTypePriceData feesTypePrice1 = new FeesTypePriceData();

                feesTypePrice1.ID = feesTypePrice.ID;
                feesTypePrice1.AcademicSessionId = feesTypePrice.AcademicSessionId;
                feesTypePrice1.FeesTypePriceId = feesTypePrice.FeesTypePriceId;
                feesTypePrice1.InsertedId = userId;

                List<SessionFeesTypeApplicableData> newchild = new List<SessionFeesTypeApplicableData>();
                List<CS_SessionFeeTypePriceApplicableData> ObjChildData = new List<CS_SessionFeeTypePriceApplicableData>();
                ObjChildData = feesTypePrice.sessionFeesTypePriceApplicableData;

                foreach (var value in ObjChildData)
                {
                    //feesTypePrice1.ID = value.FeesTypePriceDataID;
                    feesTypePrice1.sessionFeesTypePriceApplicableData.Add(new SessionFeesTypeApplicableData() { ID = value.ID, FeesTypePriceId = value.FeesTypePriceId, InsertedId = userId, SessionName = value.SessionName, FeesTypeName = value.FeesTypeName,ApplicabledataId = value.ApplicabledataId, Price = value.Price, ApplicableToId = value.ApplicableToId });
                }

                long i = feesTypePriceRepository.UpdateFeesTypePrice(feesTypePrice1);
                if (i == 1)
                {
                    feesTypePrice.ReturnMsg = "Records updated Successfully.";
                }
                else
                {
                    feesTypePrice.ReturnMsg = "Records updated Faild.!!!";
                }


            }
            return Json(feesTypePrice); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        public JsonResult DeleteFeesTypePrice(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesType Price", accessId, "Delete", "FeesTypePrice", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            CS_FessTypePriceData obj = new CS_FessTypePriceData();
            var feesTypePrice = false;
            if (FeesTypePriceExists1(id.Value) == true)
            {
                feesTypePrice = feesTypePriceRepository.DeleteFeesTypePrice(id.Value);
            }

            if (feesTypePrice == true)
            {
                obj.ReturnMsg = "Deleted successfully";
            }
            else
            {
                obj.ReturnMsg = "Delete Failed.!!!";
            }

            return Json(obj.ReturnMsg);
        }

        public JsonResult DeleteFeesTypePriceAll()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesType Price", accessId, "Delete", "FeesTypePrice", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }

            var feesTypeApplicable = feesTypePriceRepository.DeleteAllFeesTypePrice();

            return Json("Deleted successfully");
        }

        private bool FeesTypePriceExists1(long id)
        {
            if (feesTypePriceRepository.GetFeesTypePriceById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool FeesTypePriceExists(string res)
        {
            if (feesTypePriceRepository.GetFeesTypePriceByName(res) != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        private bool CheckFeesTypeExists(long FeestypeId, long ApplicableId, decimal price)
        {
                var res = feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(s => s.FeesTypePriceId == FeestypeId && s.ApplicabledataId == ApplicableId && s.Price == price && s.Active == true).ToList();
                //.Where(x => x.SessionId == id).ToList();
                if (res.Count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
           
          
        }


        public List<FeesType> Get_FeesType111()
        {
            IEnumerable<FeesType> enumerable;
            List<FeesType> feesType = new List<FeesType>();
            enumerable = feesTypeRepository.GetAllFeesType();
            feesType = enumerable.ToList();
            return feesType;

        }

        public List<SessionFeesTypeViewModel> Get_SessionFeesType(long? id)
        {
            //CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //                   // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("FeesType", accessId, "List", "StudentPayment", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            List<SessionFeesTypeViewModel> feesType = new List<SessionFeesTypeViewModel>();
            IEnumerable<SessionFeesTypeViewModel> myGenericEnumerable = (IEnumerable<SessionFeesTypeViewModel>)feesType;
            myGenericEnumerable = commonschoolmodel.GetAllSessionFeesType().Where(x => x.SessionId == id ).ToList();
            var res = (from a in myGenericEnumerable.ToList()
                       select new SessionFeesTypeViewModel
                       {
                           ID = a.FeesTypeId,
                           FeesTypeName = a.FeesTypeName,
                           ParentFeesType = a.ParentFeesType,// == null ? "N A" : a.ParentFeesType,
                           ApplicableId = a.ApplicableId
                       }).ToList();
            feesType = res;

            return feesType;
        }

        public JsonResult Get_SessionApplicable_Edit(long? id)
        {
            SessionFeesTypeViewModel od = new SessionFeesTypeViewModel();
            var sessionapplicable = feesTypeRepository.GetFeesTypeById(id.Value);
            var sessionid = sessionFeesTypeRepo.GetAllSessionFeesType().Where(x => x.FeesTypeId == id).Select(x => x.SessionId).FirstOrDefault();
            // var applicabletoid = sessionFeesTypeRepo.GetAllSessionFeesType().Where(x => x.FeesTypeId == id).Select(x => x.ApplicableId).FirstOrDefault();
            var applicabletoid = feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(c => c.FeesTypePriceId == id).Select(c => c.ApplicableToId).FirstOrDefault();
            od.ApplicableId = applicabletoid;
            od.ApplicableTo = applicableRepo.GetAllFeesTypeApplicable(0, 0).Where(x => x.ID == applicabletoid).Select(x => x.Name).FirstOrDefault();
            od.SessionId = sessionid;
            return Json(od);
        }

        public JsonResult Get_SessionApplicable(long? id)
        {
            SessionFeesTypeViewModel od = new SessionFeesTypeViewModel();
            var sessionapplicable = feesTypeRepository.GetFeesTypeById(id.Value);
            var sessionid= sessionFeesTypeRepo.GetAllSessionFeesType().Where(x => x.FeesTypeId == id).Select(x => x.SessionId).FirstOrDefault();
            var applicabletoid = sessionFeesTypeRepo.GetAllSessionFeesType().Where(x => x.FeesTypeId == id).Select(x => x.ApplicableId).FirstOrDefault();
            // var applicabletoid = feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(c => c.FeesTypePriceDataID == id).Select(c => c.ApplicableToId).FirstOrDefault();
            od.ApplicableId = applicabletoid;
            od.ApplicableTo = applicableRepo.GetAllFeesTypeApplicable(0, 0).Where(x => x.ID == applicabletoid).Select(x => x.Name).FirstOrDefault();
            od.SessionId = sessionid;

            return Json(od);
        }

        public List<AcademicSession> AcademicSession()
        {
            List<AcademicSession> feesType = new List<AcademicSession>();
            IEnumerable<AcademicSession> myGenericEnumerable = (IEnumerable<AcademicSession>)feesType;
            myGenericEnumerable = academicSessionRepository.GetAllAcademicSession().Where(c=>c.IsAvailable==true).ToList();
            var res = (from a in myGenericEnumerable.ToList()
                       select new AcademicSession
                       {
                           ID = a.ID,
                           DisplayName = a.DisplayName,
                           IsAvailable = a.IsAvailable
                       }).ToList();
            feesType = res;
            return feesType;

        }

        public List<Organization> GetOrganizationData(string id)
        {

            var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicSessionId == Convert.ToInt64(id));
            var OrganisationList = organizationRepository.GetAllOrganization();
            //var res = (from a in OrganisationAcademic
            //           join b in OrganisationList on a.OrganizationId equals b.ID
            //           select new Organization
            //           {
            //               ID = a.ID,
            //               Name = b.Name
            //           }).ToList();
           // return Json(res);

            List<Organization> organizationObj = new List<Organization>();
            //IEnumerable<Organization> myGenericEnumerable = (IEnumerable<Organization>)organizationObj;
            //myGenericEnumerable = organizationRepository.GetAllOrganization();
            var res = (from a in OrganisationAcademic
                       join b in OrganisationList on a.OrganizationId equals b.ID
                       select new Organization
                       {
                           ID = a.ID,
                           Name = b.Name
                       }).ToList();
            organizationObj = res;
            return organizationObj;
        }

        public List<Wing> GetWingData()
        {
            List<Wing> wingObj = new List<Wing>();
            IEnumerable<Wing> myGenericEnumerable = (IEnumerable<Wing>)wingObj;
            myGenericEnumerable = studentAggregateRepository.GetAllWing();
            var res = (from a in myGenericEnumerable.ToList()
                       select new Wing
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            wingObj = res;
            return wingObj;
        }

        public List<Standard> GetClassData()
        {
            List<Standard> standardObj = new List<Standard>();
            IEnumerable<Standard> myGenericEnumerable = (IEnumerable<Standard>)standardObj;
            myGenericEnumerable = standardRepository.GetAllStandard();
            var res = (from a in myGenericEnumerable.ToList()
                       select new Standard
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            standardObj = res;
            return standardObj;
        }
        public List<Board> GetBoardData()
        {
            List<Board> boardObj = new List<Board>();
            IEnumerable<Board> myGenericEnumerable = (IEnumerable<Board>)boardObj;
            myGenericEnumerable = boardRepository.GetAllBoard();
            var res = (from a in myGenericEnumerable.ToList()
                       select new Board
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            boardObj = res;
            return boardObj;
        }
        public List<Student> GetStudentData()
        {
            List<Student> studentdObj = new List<Student>();
            IEnumerable<Student> myGenericEnumerable = (IEnumerable<Student>)studentdObj;
            myGenericEnumerable = studentAggregateRepository.GetAllStudent();
            var res = (from a in myGenericEnumerable.ToList()
                       select new Student
                       {
                           ID = a.ID,
                           FirstName = a.StudentCode+" - "+ a.FirstName + " " + a.MiddleName + " " + a.LastName
                       }).ToList();
            studentdObj = res;
            return studentdObj;
        }

        public JsonResult DeleteSub(long? id)
        {
            //    CheckLoginStatus();                              
            //    if (commonMethods.checkaccessavailable("FeesType Price", accessId, "DeleteSub", "FeesTypePrice", roleId) == false)
            //    {
            //        //return RedirectToAction("AuthenticationFailed", "Accounts");
            //        Json(new
            //        {
            //            redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
            //            isRedirect = true
            //        });
            //    }
            CS_SessionFeeTypePriceApplicableData obj = new CS_SessionFeeTypePriceApplicableData();
            var feesTypePrice = false;
            feesTypePrice = feesTypePriceRepository.DeleteSub(id.Value);
            if (feesTypePrice == true)
            {
                obj.ReturnMsg = "Deleted successfully";
            }
            else
            {
                obj.ReturnMsg = "Delete Failed.!!!";
            }

            return Json(obj);
        }

        #region FeesType Download & Upload Excel
        public IActionResult downloadFeesTypePriceSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("FeesTypePrice", accessId, "Upload", "FeesTypePrice", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            XLWorkbook oWB = new XLWorkbook();
            var wsData = oWB.Worksheets.Add("Data");
            wsData.Cell(1, 1).Value = "Session";
            wsData.Cell(1, 2).Value = "FeesType";
            wsData.Cell(1, 3).Value = "ApplicableTo";
            wsData.Cell(1, 4).Value = "Price";
            //int z = 1;
            //string[] arr = { "0", "1" };
            //for (int a = 0; a < arr.Length; a++)
            //{
            //    wsData.Cell(++z, 5).Value = a;
            //}
            //wsData.Range("E2:E" + z).AddToNamed("IsMandatory");


            var session = academicSessionRepository.GetAllAcademicSession().Where(s => s.IsAvailable == true).ToList();
            var SessionIds = academicSessionRepository.GetAllAcademicSession().Where(s => s.IsAvailable == true).Select(s => s.ID).FirstOrDefault();
            var feestype = commonschoolmodel.GetAllSessionFeesType().Where(x => x.SessionId == SessionIds).ToList();
            int g = 1;
            foreach (var a in session)
            {
                wsData.Cell(++g, 1).Value = a.DisplayName;
            }
            wsData.Range("A2:A" + g).AddToNamed("Session");
            int k = 1;
            foreach (var d in feestype)
            {
                //wsData.Cell(++k, 2).Value = d.Name;
                wsData.Cell(++k, 2).Value = "_C" + d.ID + "_" + d.FeesTypeName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
            }
            wsData.Range("B2:B" + k).AddToNamed("FeesType");
            int j = 3;
            foreach (var a in feestype)
            {               
                var applicabletoid = sessionFeesTypeRepo.GetAllSessionFeesType().Where(x => x.FeesTypeId == a.ID).Select(x => x.ApplicableId).FirstOrDefault();               
                var ApplicableTo = applicableRepo.GetAllFeesTypeApplicable(0, 0).Where(x => x.ID == applicabletoid).Select(x => x.Name).FirstOrDefault();
                wsData.Cell(1, ++j).Value = "_C" + a.ID + "_" + a.FeesTypeName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                if (ApplicableTo== "Organization" || ApplicableTo == "Organizations")
                {
                    var organization= organizationRepository.GetAllOrganization();
                    int p = 2;
                    foreach (var b in organization) //.Where(m => m.ID == applicabletoid).ToList()
                    {
                        //wsData.Cell(++p, j).Value =b.Name;
                        wsData.Cell(++p, j).Value = "S" + b.ID + "_" + b.Name;
                    }
                    //wsData.Range(wsData.Cell(2, j), wsData.Cell(p, j)).AddToNamed(a.Name);
                    wsData.Range(wsData.Cell(3, j), wsData.Cell(p, j)).AddToNamed("_C" + a.ID + "_" + a.FeesTypeName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }
                else if (ApplicableTo == "Board" || ApplicableTo == "Boards")
                {
                    var board = boardRepository.GetAllBoard();
                    int p = 1;
                    foreach (var c in board) //.Where(m => m.ID == applicabletoid).ToList()
                    {
                        //wsData.Cell(++p, j).Value =b.Name;
                        wsData.Cell(++p, j).Value = "S" + c.ID + "_" + c.Name;
                    }
                    //wsData.Range(wsData.Cell(2, j), wsData.Cell(p, j)).AddToNamed(a.Name);
                    wsData.Range(wsData.Cell(3, j), wsData.Cell(p, j)).AddToNamed("_C" + a.ID + "_" + a.FeesTypeName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }
                else if (ApplicableTo == "Class" || ApplicableTo == "Classes")
                {
                    var classdata = standardRepository.GetAllStandard();
                    int p = 1;
                    foreach (var x in classdata) //.Where(m => m.ID == applicabletoid).ToList()
                    {
                        //wsData.Cell(++p, j).Value =b.Name;
                        wsData.Cell(++p, j).Value = "S" + x.ID + "_" + x.Name;
                    }
                    //wsData.Range(wsData.Cell(2, j), wsData.Cell(p, j)).AddToNamed(a.Name);
                    wsData.Range(wsData.Cell(3, j), wsData.Cell(p, j)).AddToNamed("_C" + a.ID + "_" + a.FeesTypeName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }
                else if (ApplicableTo == "Wing" || ApplicableTo == "Wings")
                {
                    var wings = studentAggregateRepository.GetAllWing();
                    int p = 1;
                    foreach (var n in wings) //.Where(m => m.ID == applicabletoid).ToList()
                    {                       
                        wsData.Cell(++p, j).Value = "S" + n.ID + "_" + n.Name;
                    }                   
                    wsData.Range(wsData.Cell(3, j), wsData.Cell(p, j)).AddToNamed("_C" + a.ID + "_" + a.FeesTypeName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }
                else if (ApplicableTo == "Student" || ApplicableTo == "Students")
                {
                    var student = studentAggregateRepository.GetAllStudent();
                    int p = 1;
                    foreach (var s in student) //.Where(m => m.ID == applicabletoid).ToList()
                    {                       
                        wsData.Cell(++p, j).Value = "S" + s.ID + "_" + s.FirstName+" "+s.MiddleName+" "+s.LastName;
                    }                   
                    wsData.Range(wsData.Cell(3, j), wsData.Cell(p, j)).AddToNamed("_C" + a.ID + "_" + a.FeesTypeName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }

            }
           





            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Session");
            validationTable.Columns.Add("FeesType");
            validationTable.Columns.Add("ApplicableTo");
            validationTable.Columns.Add("Price");
            validationTable.TableName = "FeesTypePriceData";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(wsData.Range("Session"), true);
            worksheet.Column(2).SetDataValidation().List(wsData.Range("FeesType"), true);
            worksheet.Column(3).SetDataValidation().InCellDropdown = true;
            worksheet.Column(3).SetDataValidation().Operator = XLOperator.Between;
            worksheet.Column(3).SetDataValidation().AllowedValues = XLAllowedValues.List;
            worksheet.Column(3).SetDataValidation().List("=INDIRECT(SUBSTITUTE(B1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
           

            worksheet.Column(1).SetDataValidation().InCellDropdown = true;
            worksheet.Column(2).SetDataValidation().InCellDropdown = true;
         
            wsData.Hide();
            // wsData1.Hide();
            // worksheet7.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"FeesTypePrice.xlsx");
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IActionResult UploadFeesTypePrice(IFormFile file)
        {
            long Yn = 0; string ReturnMsg = "";
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("FeesTypePrice", accessId, "Upload", "FeesTypePrice", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    // var request = supportRepository.GetAllSupportRequest();
                    FeesTypePriceData sessionfesstype = new FeesTypePriceData();
                    for (int i = 2; i <= totalRows; i++)
                    {

                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        var sessionId = Convert.ToInt64(academicSessionRepository.GetAllAcademicSession().Where(s => s.IsAvailable = true && s.DisplayName == name).Select(s => s.ID).FirstOrDefault());
                        string feestypename = firstWorksheet.Cells[i, 2].Value.ToString();//.Replace("C", "").Replace(i.ToString(), "").Replace("_", "");
                        string[] feestypenames = feestypename.Split("_");
                        var feestypeids = Convert.ToInt64(feesTypeRepository.GetAllFeesType().Where(y => y.IsAvailable = true && y.Name == feestypenames[2].ToString()).Select(y => y.ID).FirstOrDefault());

                        sessionfesstype.AcademicSessionId = sessionId;
                        sessionfesstype.FeesTypePriceId = feestypeids;
                        sessionfesstype.InsertedId = userId;
                        var applicabletoid = sessionFeesTypeRepo.GetAllSessionFeesType().Where(x => x.FeesTypeId == feestypeids).Select(x => x.ApplicableId).FirstOrDefault();
                        var ApplicableTo = applicableRepo.GetAllFeesTypeApplicable(0, 0).Where(x => x.ID == applicabletoid).Select(x => x.Name).FirstOrDefault();
                        List<SessionFeesTypeApplicableData> newchild = new List<SessionFeesTypeApplicableData>();
                        List<CS_SessionFeeTypePriceApplicableData> ObjChildData = new List<CS_SessionFeeTypePriceApplicableData>();
                        string[] Applicabledatanames = firstWorksheet.Cells[i, 3].Value.ToString().Split("_");
                        string Applicabledatanamedata = Applicabledatanames[1].ToString();
                        long organizationdataId = 0;
                        if (ApplicableTo == "Organization" || ApplicableTo == "Organizations")
                        {
                            //Applicabledatanamedata = firstWorksheet.Cells[i, 3].Value.ToString().Replace("S", "").Replace(i.ToString(), "").Replace("_", "");
                            organizationdataId = organizationRepository.GetAllOrganization().Where(m => m.Name == Applicabledatanamedata).Select(m => m.ID).FirstOrDefault();
                        }
                        else if (ApplicableTo == "Board" || ApplicableTo == "Boards")
                        {
                            //Applicabledatanamedata = firstWorksheet.Cells[i, 3].Value.ToString().Split("_");
                            organizationdataId = boardRepository.GetAllBoard().Where(m => m.Name == Applicabledatanamedata).Select(m => m.ID).FirstOrDefault();
                        }
                        else if (ApplicableTo == "Class" || ApplicableTo == "Classes")
                        {
                            // Applicabledataname = firstWorksheet.Cells[i, 3].Value.ToString().Replace("S", "").Replace(i.ToString(), "").Replace("_", "");
                            organizationdataId = standardRepository.GetAllStandard().Where(m => m.Name == Applicabledatanamedata).Select(m => m.ID).FirstOrDefault();
                        }
                        else if (ApplicableTo == "Wing" || ApplicableTo == "Wings")
                        {
                            //Applicabledataname = firstWorksheet.Cells[i, 3].Value.ToString().Replace("S", "").Replace(i.ToString(), "").Replace("_", "");
                            organizationdataId = studentAggregateRepository.GetAllWing().Where(m => m.Name == Applicabledatanamedata).Select(m => m.ID).FirstOrDefault();
                        }
                        else if (ApplicableTo == "Student" || ApplicableTo == "Students")
                        {
                            // Applicabledataname = firstWorksheet.Cells[i, 3].Value.ToString().Replace("S", "").Replace(i.ToString(), "").Replace("_", "");
                            organizationdataId = studentAggregateRepository.GetAllStudent().Where(m => m.FirstName + "" + m.MiddleName + "" + m.LastName == Applicabledatanamedata).Select(m => m.ID).FirstOrDefault();
                        }

                        decimal prices = Convert.ToDecimal(firstWorksheet.Cells[i, 4].Value);


                        sessionfesstype.sessionFeesTypePriceApplicableData.Add(new SessionFeesTypeApplicableData() { InsertedId = userId, SessionName = name, FeesTypeName = feestypename[2].ToString(), ApplicabledataId = organizationdataId, Price = prices, ApplicableToId = applicabletoid });

                        if (CheckFeesTypeExists(feestypeids, organizationdataId, prices) == false)
                        {
                            Yn = 0;
                        }
                        else
                        {
                            Yn = 1;
                        }

                    }
                    if (Yn == 1)
                    {
                        long id = feesTypePriceRepository.CreateFeesTypePrice(sessionfesstype);
                        if (id > 0)
                        {
                            ReturnMsg = "Excel uploaded successfully";
                        }
                        else
                        {
                            ReturnMsg = "Excel uploaded Failed !!!";
                        }
                    }
                    else
                    {
                        ReturnMsg = "Records already Exists!!!";
                    }

                }                
            }
            return Json(ReturnMsg);
        }

        #endregion Download & Upload Excel
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        public JsonResult ActiveDeactiveFeesTypePrice(string id, string IsActive)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("FeesType Price", accessId, "Update", "FeesType Price", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {

                var sessioschalar = feesTypePriceRepository.GetFeesTypePriceById(Convert.ToInt64(id));
                OdmErp.ApplicationCore.Entities.SessionFeesTypeApplicableData objScholarship = new SessionFeesTypeApplicableData();
                sessioschalar.ID = Convert.ToInt64(id);
                sessioschalar.Status = IsActive;
                if (IsActive == "ACTIVE")
                {
                    sessioschalar.Active = true;
                }
                else
                {
                    sessioschalar.Active = false;
                }               

                //TODO
                sessioschalar.ModifiedId = userId;
                feesTypePriceRepository.UpdateFeesTypePriceStatus(sessioschalar);
                objScholar.ReturnMsg = sessioschalar.Status.ToUpper() + " Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

    }
}
