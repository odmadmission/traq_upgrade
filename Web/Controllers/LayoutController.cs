﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class LayoutController : Controller
    {
        public IActionResult Master()
        {
            return View();
        }

        public IActionResult Common()
        {
            return View();
        }

        public IActionResult Support()
        {
            return View();
        }
        public IActionResult HR()
        {
            return View();
        }
        public IActionResult MasterSchool()
        {
            return View();
        }
        public IActionResult MasterSchoolCommon()
        {
            return View();
        }
        public IActionResult Task()
        {
            return View();
        }
        public IActionResult Grievance()
        {
            return View();
        }

        public IActionResult HRManagement()
        {
            return View();
        }

        public IActionResult Student()
        {
            return View();
        }
        public IActionResult Academic()
        {
            return View();
        }

        public IActionResult SupportMaster()
        {
            return View();
        }
        public IActionResult TaskMaster()
        {
            return View();
        }

        public IActionResult GrievanceMaster()
        {
            return View();
        }

        public IActionResult Test()
        {
            return View();
        }

        public IActionResult Bugs()
        {
            return View();
        }

        public IActionResult SchoolMaster()
        {
            return View(nameof(School));
        }

        public IActionResult School()
        {
            return View(nameof(SchoolMaster));
        }
        public IActionResult SchoolLayout()
        {
            return View(nameof(SchoolLayout));
        }

        public IActionResult Login()
        {
            return View();
        }
        public IActionResult StudentPayment()
        {
            return View();
        }
        public IActionResult AdmissionLayout()
        {
            return View();
        }

        public IActionResult AdmissionPageLayout()
        {
            return View();
        }



    }
}