﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using OdmErp.Web.DTO;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
    //[Authorize]
    public class AppController : Controller
    {


        public long accessId = 0;
        public long userId = 0;
        public long roleId = 0;
        public string loginId;

        public IActionResult CheckLoginStatus()
        {
            

            if (HttpContext != null && HttpContext.Session != null)
            {




                accessId = HttpContext.Session.GetInt32(LoginSessionData.ACCESS_ID)

                    != null ? HttpContext.Session.GetInt32(LoginSessionData.ACCESS_ID).Value : 0;
                roleId = HttpContext.Session.GetInt32(LoginSessionData.ROLE_ID)
                   != null ? HttpContext.Session.GetInt32(LoginSessionData.ROLE_ID).Value : 0;
                userId = HttpContext.Session.GetInt32(LoginSessionData.USER_ID)
                    != null ? HttpContext.Session.GetInt32(LoginSessionData.USER_ID).Value : 0;

                if (accessId == 0)
                {
                    return RedirectToAction("EmployeeLogin", "Accounts");
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }


        public IActionResult AdmissionLoginStatus()
        {



            if (HttpContext!=null&&HttpContext.Session != null)
            {



                loginId = HttpContext.Session.GetString(LoginSessionData.LOGIN_ID);

                if (loginId == null)
                {
                    return RedirectToAction("Expired", "Student", new { area="Admission"});
                }
                else
                {
                    return null;
                }

            }
            else
            {
                return RedirectToAction("Expired", "Student", new { area = "Admission" });
            }

        }


        public string NullableDateTimeToStringDate(DateTime? time)
        {
            return time == null ? ViewConstants.NOT_APPLICABLE : time.Value.ToString("dd-MMM-yyyy");

        }

        public string DateTimeToStringDate(DateTime time)
        {
            return time == null ? ViewConstants.NOT_APPLICABLE : time.ToString("dd-MMM-yyyy");

        }

        public (string Date,string Time,string DateTime) NullableDateTimeToStringDateTime(DateTime? time)
        {
            return time == null ? (ViewConstants.NOT_APPLICABLE, ViewConstants.NOT_APPLICABLE, ViewConstants.NOT_APPLICABLE) : (time.Value.ToString("dd-MMM-yyyy"),
                time.Value.ToString("hh:mm tt"), time.Value.ToString("dd-MMM-yyyy hh:mm tt"));

        }

        public (string, string, string) DateTimeToStringDateTime(DateTime time)
        {
            return time == null ? (ViewConstants.NOT_APPLICABLE, ViewConstants.NOT_APPLICABLE, ViewConstants.NOT_APPLICABLE) : (time.ToString("dd-MMM-yyyy"),
                  time.ToString("hh:mm tt"), time.ToString("dd-MMM-yyyy hh:mm tt"));


        }


        public void GetStatusList()
        {


            ViewData["ALLSTATUS"] = new List<SelectListItem>
             {                
             new SelectListItem
                   {
                       Text="ACTIVE",
                       Value = "ACTIVE"
                   }
             ,new SelectListItem
                   {
                       Text="INACTIVE",
                       Value = "INACTIVE"
                    }
             };



    }
    }
}