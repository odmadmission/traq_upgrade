﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.ApplicationCore.Interfaces; 
using Microsoft.AspNetCore.Mvc.Rendering;
using OdmErp.ApplicationCore.Entities;
using OdmErp.Web.Models;

namespace Web.Controllers
{
    public class ErpBugController : AppController
    {
        private readonly IErpBugRepository erpBugRepository;
        private readonly IErpBugAttachmentRepository erpBugAttachmentRepository;
        private readonly IModuleRepository moduleRepository;
        private readonly ISubModuleRepository subModuleRepository;
        private readonly IActionRepository actionRepository;
        private readonly IEmployeeRepository employeeRepository;
        private readonly CommonMethods commonMethods;
        private readonly IEmployeeDesignationRepository employeeDesignationRepository;
        private readonly IDepartmentRepository departmentRepository;
        private readonly IDesignationRepository designationRepository;
         

        public ErpBugController(IErpBugRepository erpBugRepository, IErpBugAttachmentRepository erpBugAttachmentRepository,
            IModuleRepository moduleRepository, ISubModuleRepository subModuleRepository, IActionRepository actionRepository,
             IEmployeeRepository employeeRepository, CommonMethods commonMethods, IEmployeeDesignationRepository employeeDesignationRepository,
             IDepartmentRepository departmentRepository, IDesignationRepository designationRepository)
        {
            this.erpBugRepository = erpBugRepository;
            this.erpBugAttachmentRepository = erpBugAttachmentRepository;
            this.moduleRepository = moduleRepository;
            this.subModuleRepository = subModuleRepository;
            this.actionRepository = actionRepository;
            this.employeeRepository = employeeRepository;
            this.commonMethods = commonMethods;
            this.employeeDesignationRepository = employeeDesignationRepository;
            this.departmentRepository = departmentRepository;
            this.designationRepository = designationRepository;
        } 

        public async Task<IActionResult> Index()
        {
            var res = await commonMethods.GetAllErpBugs(); 
           
            return View(res);
        }
        
        public IActionResult Create()
        {             

            ViewData["Modules"]  = new[] { new SelectListItem { Text = "Select Module", Value = "0" } }.Concat(new SelectList(moduleRepository.GetAllModule(), "ID", "Name"));
            ViewData["SubModules"] = new[] { new SelectListItem { Text = "Select SubModule", Value = "0" } }.Concat(new SelectList(subModuleRepository.GetAllSubModule(), "ID", "Name"));
            ViewData["Actions"] = new[] { new SelectListItem { Text = "Select Action", Value = "0" } }.Concat(new SelectList(actionRepository.GetAllAction(), "ID", "Name"));


            return View(); 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateErpBug([Bind("ModuleID,SubModuleID,ActionID,Title,Description")] ErpBug erpBugs)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {

                erpBugs.InsertedDate = DateTime.Now;
                erpBugs.ModifiedDate = DateTime.Now;
                erpBugs.InsertedId = userId;
                erpBugs.ModifiedId = userId;
                erpBugs.Active = true;
                erpBugs.Status = "ACTIVE";
                erpBugs.IsAvailable = false;

                await erpBugRepository.AddAsync(erpBugs);
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(long? id)
        {
            ViewData["Modules"] = new[] { new SelectListItem { Text = "Select Module", Value = "0" } }.Concat(new SelectList(moduleRepository.GetAllModule(), "ID", "Name"));            
            ViewData["SubModules"] = new[] { new SelectListItem { Text = "Select SubModule", Value = "0" } }.Concat(new SelectList(subModuleRepository.GetAllSubModule().Distinct(), "ID", "Name"));
            ViewData["Actions"] = new[] { new SelectListItem { Text = "Select Action", Value = "0" } }.Concat(new SelectList(actionRepository.GetAllAction(), "ID", "Name"));
            ViewData["Employees"] = new[] { new SelectListItem { Text = "Select Employee", Value = "0" } }.Concat(new SelectList(employeeRepository.GetAllEmployee(), "ID", "FirstName"));

            if (id == null)
            {
                return NotFound();
            }

            //var result = commonMethods.EditErpBugs(id.Value);
            var result = commonMethods.EditErpBugs(id.Value);

            if (result == null)
            {
                return NotFound();
            }

            return View(result);
        }

        public JsonResult GetActionsBySubModule(long id)
        {
            try
            {
                var res = actionRepository.GetAllAction().Where(a => a.SubModuleID == id).ToList();
                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }

        public JsonResult GetAllEmployeeByDepartment()
        {
            try
            {
                var empDesingnationList = employeeDesignationRepository.GetAllEmployeeDesignations();
                var empList = employeeRepository.GetAllEmployee();
                var desingnationList = designationRepository.GetAllDesignation();
                var departmentList = departmentRepository.GetAllDepartment();

                var res = ( from a in empDesingnationList
                            join b in empList on a.EmployeeID equals b.ID
                            join c in desingnationList on a.DesignationID equals c.ID
                            join d in departmentList on c.DepartmentID equals d.ID 
                            where d.ID == 3
                            select a
                          ).ToList();
                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
    }
}