﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Web.Models;

namespace Web.Controllers
{
    public class DashboardController : Controller
    {
        public IAccessRepository accessRepository;
        private IBloodGroupRepository bloodGroupRepository;
        public ICategoryRepository categoryRepository;
        private IReligionTypeRepository religionTypeRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        public CommonMethods commonMethods;
        private readonly IHostingEnvironment hostingEnvironment;
        private OdmErp.Web.Areas.Student.Models.StudentMethod studentCommonMethod;
        private OdmErp.Web.Areas.Student.Models.StudentSqlQuery sqlQuery;
        public DashboardController(OdmErp.Web.Areas.Student.Models.StudentSqlQuery sql,IAccessRepository accessRepository, IBloodGroupRepository bloodGroupRepository, ICategoryRepository categoryRepository,
            IReligionTypeRepository religionTypeRepository, INationalityTypeRepository nationalityTypeRepository, IStudentAggregateRepository studentAggregateRepository,
            CommonMethods commonMethods, IHostingEnvironment hostingEnvironment, OdmErp.Web.Areas.Student.Models.StudentMethod studentCommonMethod)
        {
            sqlQuery = sql;
            this.accessRepository = accessRepository;
            this.bloodGroupRepository = bloodGroupRepository;
            this.categoryRepository = categoryRepository;
            this.religionTypeRepository = religionTypeRepository;
            this.nationalityTypeRepository = nationalityTypeRepository;
            this.studentAggregateRepository = studentAggregateRepository;
            this.commonMethods = commonMethods;
            this.hostingEnvironment = hostingEnvironment;
            this.studentCommonMethod = studentCommonMethod;

        }
        public IActionResult Index()
        {
            
            return View();
        }
        public IActionResult ParentDashboard()
        {

            return View();
        } public IActionResult StudentDashboard()
        {

            return View();
        }

        public IActionResult Accounts()    
        {

            return View().WithInfo("Information", "Please make sure your Child to whom You are going to see his/her profile or manage their password!"); ;
        }
        public IActionResult ChangePasswords()
        {
            logincls login = new logincls();
            login.StudentId = Convert.ToInt64(this.HttpContext.Session.GetString("ddlstudentid"));

            var access = accessRepository.GetAllAccess().Where(a => a.EmployeeID == login.StudentId && a.RoleID == 5).FirstOrDefault();
            if (access.Password == null || access.Password == "")
            {
                login.isExist = false;
                ViewBag.Status = "Create Passoword";
            }
            else
            {
                login.isExist = true;
                ViewBag.Status = "Change Password";
            }

            return View(login);
        }

        public IActionResult StudentViewDetails()
        {
            long? id = Convert.ToInt64(this.HttpContext.Session.GetString("ddlstudentid"));

            OdmErp.Web.Areas.Student.Models.StudentModelClass studentcls = new OdmErp.Web.Areas.Student.Models.StudentModelClass();
            studentcls.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
            studentcls.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
            studentcls.religionTypes = religionTypeRepository.GetAllReligionTypes();
            studentcls.categoryTypes = categoryRepository.ListAllAsyncIncludeAll().Result;
            ViewBag.status = "Create";
            if (id != null)
            {
                var studentdetails = studentAggregateRepository.GetStudentById(id.Value);
                studentcls.BloodGroupID = studentdetails.BloodGroupID;
                studentcls.BloodGroupName = bloodGroupRepository.GetBloodGroupById(studentdetails.BloodGroupID).Name;
                studentcls.DateOfBirth = studentdetails.DateOfBirth;
                studentcls.StudentCode = studentdetails.StudentCode;
                studentcls.FirstName = studentdetails.FirstName;
                studentcls.Gender = studentdetails.Gender;
                studentcls.ID = studentdetails.ID;
                studentcls.LastName = studentdetails.LastName;
                studentcls.MiddleName = studentdetails.MiddleName;
                studentcls.NatiobalityID = studentdetails.NatiobalityID;
                studentcls.NatiobalityName = nationalityTypeRepository.GetNationalityById(studentdetails.NatiobalityID).Name;
                studentcls.ReligionID = studentdetails.ReligionID;
                studentcls.ReligionName = religionTypeRepository.GetReligionTypeById(studentdetails.ReligionID).Name;
                studentcls.CategoryName = categoryRepository.GetCategoryNameById(studentdetails.CategoryID).Result.Name;
                var webPath = hostingEnvironment.WebRootPath;
                if (studentdetails.Image != null)
                {
                    string path = Path.Combine("/ODMImages/StudentProfile/", studentdetails.Image);
                    ViewBag.Profileimage = path;
                }
                else
                {
                    ViewBag.Profileimage = null;
                }
                var studentAddresses = studentAggregateRepository.GetAllParent().ToList();
                var parentRelationshipTypes = studentAggregateRepository.GetAllParentRelationshipType();
                var professionTypes = studentAggregateRepository.GetAllProfessionType();
                IEnumerable<OdmErp.Web.Areas.Student.Models.StudentParentsCls> res = (from a in studentAddresses
                                                             join b in parentRelationshipTypes on a.ParentRelationshipID equals b.ID
                                                             join c in professionTypes on a.ProfessionTypeID equals c.ID
                                                             select new OdmErp.Web.Areas.Student.Models.StudentParentsCls
                                                             {
                                                                 ID = a.ID,
                                                                 AlternativeMobile = a.AlternativeMobile,
                                                                 EmailId = a.EmailId,
                                                                 FirstName = a.FirstName,
                                                                 LandlineNumber = a.LandlineNumber,
                                                                 LastName = a.LastName,
                                                                 MiddleName = a.MiddleName,
                                                                 ParentRelationshipID = a.ParentRelationshipID,
                                                                 ParentRelationshipName = b.Name,
                                                                 PrimaryMobile = a.PrimaryMobile,
                                                                 ProfessionTypeID = a.ProfessionTypeID,
                                                                 ProfessionTypeName = c.Name,
                                                                 StudentID = a.StudentID,
                                                                 ModifiedDate = a.ModifiedDate,
                                                                 Imagepath = a.Image != null ? Path.Combine("/ODMImages/ParentProfile/", a.Image) : null
                                                             }).ToList();

                studentcls.studentParentsCls = res.ToList();
                studentcls.studentAdresses = studentCommonMethod.GetAllAddressByUsingStudent().ToList();
                studentcls.studentClassDetails = sqlQuery.Get_View_Academic_Student() == null ? null : sqlQuery.Get_View_Academic_Student().Where(a => a.IsAvailable == true).FirstOrDefault();
                studentcls.StudentDocumentAccess = studentCommonMethod.GetAllDocumentByUsingStudent()==null?null: studentCommonMethod.GetAllDocumentByUsingStudent().Where(a => a.DocumentTypeName == "EDUCATIONAL")==null?null: studentCommonMethod.GetAllDocumentByUsingStudent().Where(a => a.DocumentTypeName == "EDUCATIONAL").ToList(); ;
                studentcls.StudentPersonalDocumentAccess = studentCommonMethod.GetAllDocumentByUsingStudent()==null?null: studentCommonMethod.GetAllDocumentByUsingStudent().Where(a => a.DocumentTypeName == "PERSONAL")==null?null: studentCommonMethod.GetAllDocumentByUsingStudent().Where(a => a.DocumentTypeName == "PERSONAL").ToList();
                studentcls.StudentAcademicAccess = studentCommonMethod.GetAllStudentAcademicListByStudent()==null?null: studentCommonMethod.GetAllStudentAcademicListByStudent().ToList();
              //  ViewBag.status = "Create";
                ViewBag.status = "Update";
            }
            return View(studentcls);
        }

    }
}