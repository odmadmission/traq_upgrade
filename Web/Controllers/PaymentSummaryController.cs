﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities.StudentPaymentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Web.Models;
using OdmErp.Web.Models.StudentPayment;
using Web.Controllers;
using X.PagedList.Mvc;
using X.PagedList;
using OdmErp.Web.ViewModels;
using OdmErp.ApplicationCore.Entities.SessionFeesType;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OfficeOpenXml;
using ClosedXML.Excel;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using System.Data;
using OdmErp.Infrastructure.RepositoryImpl.StudentPaymentRepository;

namespace OdmErp.Web.Controllers
{
    public class PaymentSummaryController : AppController
    {
        private ISessionFeesType sessionfeestyperepo;
        public IFeesTypeRepository feestypeRepo;
        private IFeesTypePrice feesTypePriceRepository;
        private IPaymentCollectionType paymentCollectionRepo;
        private CommonMethods commonMethods;
        public CommonSchoolModel CommonSchoolModel;
        public Extensions.SessionExtensions sessionExtensions;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IOrganizationRepository organizationRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IBoardStandardRepository boardStandardRepository;
        public IStandardRepository standardRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        public IAcademicStudentRepository academicStudentRepository;
        public commonsqlquery commonsqlquery;
        private readonly IHostingEnvironment env;
        private IStudentPayment studentpaymentrepo;
        private IPaymentType paymenttype;
        public PaymentSummaryController(IFeesTypePrice feesTypePriceRepo, IPaymentCollectionType paymentCollectionRepo, CommonMethods commonMethods,
            CommonSchoolModel CommonSchoolModel, IOrganizationAcademicRepository organizationAcademicRepository, IOrganizationRepository organizationRepository,
            IAcademicStandardRepository academicStandardRepository, IBoardStandardRepository boardStandardRepository, IStandardRepository standardRepository,
            IStudentAggregateRepository studentAggregateRepository, IAcademicStudentRepository academicStudentRepository, commonsqlquery commonsqlquery,
            IHostingEnvironment env, IFeesTypeRepository feestypeRepository, ISessionFeesType sessionfeestyperepo, IStudentPayment studentpaymentrepo, IPaymentType paymenttype)
        {
            feesTypePriceRepository = feesTypePriceRepo;
            this.paymentCollectionRepo = paymentCollectionRepo;
            this.commonMethods = commonMethods;
            this.CommonSchoolModel = CommonSchoolModel;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.organizationRepository = organizationRepository;
            this.academicStandardRepository = academicStandardRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.standardRepository = standardRepository;
            this.studentAggregateRepository = studentAggregateRepository;
            this.academicStudentRepository = academicStudentRepository;
            this.commonsqlquery = commonsqlquery;
            this.env = env;
            this.feestypeRepo = feestypeRepository;
            this.sessionfeestyperepo = sessionfeestyperepo;
            this.studentpaymentrepo = studentpaymentrepo;
            this.paymenttype = paymenttype;
        }
        public IActionResult PaymentSummary()
        {
            //CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("PaymentCollectionType", accessId, "List", "PaymentCollectionType", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            return View();
        }
        public IActionResult ParentPaymentSummaryShow()
        {
            //CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("Payment", accessId, "List", "Payment", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            return View();
        }
        public JsonResult GetStudentDetails(long studentid)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("PaymentCollectionType", accessId, "List", "PaymentCollectionType", roleId) == false)
            {
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            try
            {
                var students = studentAggregateRepository.GetAllStudent().Where(s => s.ID == studentid).ToList();
                var allacademicsectionstudents = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).ToList();
                var paymentCollectionTypes = CommonSchoolModel.GetAllPaymentCollectionTypes().Where(e => e.Active == true).ToList();
                var paymentCollectionTypesdata = paymentCollectionRepo.GetAllPaymentCollectionTypeData().Where(x => x.Active == true).ToList();

                var results = (from a in paymentCollectionTypes
                               join b in paymentCollectionTypesdata on a.ID equals b.PaymentCollectionId
                               join c in allacademicsectionstudents on a.ClassId equals c.StandardId
                               select new
                               {
                                   TotalAmount = a.SumAmount,
                                   academicyearid = a.AcademicSessionId,
                                   orgnizationid = a.OrganizationId,
                                   wingid = a.WingId,
                                   boardid = a.Boardid,
                                   classid = c.StandardId,
                                   studentid = c.StudentId,
                                   organization = c.OraganisationName,
                                   AcademicSession = c.SessionName,
                                   wingname = c.WingName,
                                   boardname = c.BoardName,
                                   classname = c.StandardName

                               }).ToList();
                return Json(results);
            }
            catch
            {
                return Json(null);
            }

        }

        public JsonResult ParentPaymentSummaryView(long studentid)
        {
            //CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("PaymentCollectionType", accessId, "List", "PaymentCollectionType", roleId) == false)
            //{
            //    Json(new
            //    {
            //        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
            //        isRedirect = true
            //    });
            //}
            try
            {
                var res = commonsqlquery.Get_View_StudentPaymentData(studentid).ToList();

                var result = (from a in res
                              select new
                              {
                                  ID = a.ID,
                                  AcademicStandardId = a.AcademicStandardId,
                                  AcademicStandardStreamId = a.AcademicStandardStreamId,
                                  SectionId = a.SectionId,
                                  BoardStandardId = a.BoardStandardId,
                                  OrganizationAcademicId = a.OrganizationAcademicId,
                                  SchoolWingId = a.SchoolWingId,
                                  Sectionname = a.Sectionname,
                                  BoardId = a.BoardId,
                                  StandardId = a.StandardId,
                                  AcademicSessionId = a.AcademicSessionId,
                                  OrganizationId = a.OrganizationId,
                                  WingID = a.WingID,
                                  OrganizationAcademicID = a.OrganizationAcademicID,
                                  Sessionname = a.Sessionname,
                                  OrganisationName = a.OrganisationName,
                                  Wingname = a.Wingname,
                                  Boardname = a.Boardname,
                                  Standardname = a.Standardname,
                                  Streamname = a.Streamname,
                                  SumAmount = a.SumAmount,
                                  Image = env.WebRootFileProvider.GetFileInfo("~\\ODMImages\\StudentProfile\\" + a.Image)?.PhysicalPath,
                                  RollNo = a.studentcode
                              }).ToList();
                //    var students = studentAggregateRepository.GetAllStudent().Where(s => s.ID == studentid).ToList();
                //    var allacademicsectionstudents = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i=>i.StudentId==studentid).ToList();
                //    var paymentCollectionTypes = CommonSchoolModel.GetAllPaymentCollectionTypes().Where(e => e.Active == true).ToList();
                //    var paymentCollectionTypesdata = paymentCollectionRepo.GetAllPaymentCollectionTypeData().Where(x => x.Active == true).ToList();

                //    var results = (from a in paymentCollectionTypes
                //                   join b in paymentCollectionTypesdata on a.ID equals b.PaymentCollectionId 
                //                   join c in allacademicsectionstudents on a.ClassId equals c.StandardId 
                //                   select new
                //                   {

                //                       paymentname = b.PaymentName,
                //                       fromdate = Convert.ToDateTime(b.FromDate).ToShortDateString(),
                //                       todate = Convert.ToDateTime(b.ToDate).ToShortDateString(),
                //                       deadlinedate = Convert.ToDateTime(b.DeadLineDate).ToShortDateString(),
                //                       amount = b.Amount,
                //                       paystatus = "Paid",
                //                       academicyearid = a.AcademicSessionId,
                //                       orgnizationid = a.OrganizationId,
                //                       wingid = a.WingId,
                //                       boardid = a.Boardid,
                //                       classid = c.StandardId,
                //                       studentid = c.StudentId,
                //                       organization=c.OraganisationName,
                //                       AcademicSession=c.SessionName,
                //                       wingname=c.WingName,
                //                       boardname=c.BoardName

                //                   }).ToList();
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }

        }

        public JsonResult ParentPaymentSummaryFeesTypedata(long studentid)
        {
            //CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("PaymentCollectionType", accessId, "List", "PaymentCollectionType", roleId) == false)
            //{
            //    Json(new
            //    {
            //        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
            //        isRedirect = true
            //    });
            //}
            try
            {

                var students = studentAggregateRepository.GetAllStudent().Where(s => s.ID == studentid).ToList();
                var allacademicsectionstudents = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).ToList();
                var Getclassid = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).Select(i => i.StandardId).FirstOrDefault();
                var GetFeesTypeid = feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(s => s.ApplicabledataId == Getclassid && s.Active == true).Select(o => o.FeesTypePriceId).FirstOrDefault();
                var paymentCollectionTypes = paymentCollectionRepo.GetAllPaymentCollectionType().Where(e => e.Active == true).ToList();
                var paymentCollectionTypesdata = paymentCollectionRepo.GetAllPaymentCollectionTypeData().Where(x => x.Active == true).ToList();
                var fessTypePriceData = feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(s => s.ApplicabledataId == Getclassid && s.Active == true).ToList();
                //var feestype = fees.Where(x => x.FeesTypeId == GetFeesTypeid).ToList();
                var FeesTypeList = feestypeRepo.GetAllFeesType().Where(x => x.Active == true).ToList();

                var results = (from a in paymentCollectionTypes
                               //join b in fessTypePriceData on a.FeeTypeId equals b.FeesTypePriceId
                               join e in FeesTypeList on a.FeeTypeId equals e.ID
                               select new
                               {
                                   Price = a.SumAmount, //feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(s => s.ApplicabledataId == c.StandardId && s.Active == true).Select(r => r.Price).FirstOrDefault(),
                                   FeesTypeName = feestypeRepo.GetAllFeesType().Where(x => x.Active == true && x.ID == a.FeeTypeId).Select(x => x.Name).FirstOrDefault(),
                                   ParentFeesType = feestypeRepo.GetAllFeesType().Where(x => x.Active == true && x.ID == e.ParentFeeTypeID).Select(x => x.Name).FirstOrDefault(),
                                   //Mandatory = CommonSchoolModel.GetAllSessionFeesType().Where(x => x.Active == true && x.FeesTypeId==a.FeesTypePriceId).Select(x=>x.Mandatory).FirstOrDefault(),
                                   Mandatory = sessionfeestyperepo.GetAllSessionFeesType().Where(x => x.Active == true && x.FeesTypeId == a.FeeTypeId).Select(x => x.IsMandatory).FirstOrDefault(),
                                   PaymentCId = a.ID, //CommonSchoolModel.GetAllPaymentCollectionTypes().Where(r => r.Active == true && r.FeeTypeId==a.FeesTypePriceId).Select(p=>p.ID).FirstOrDefault(),
                               }).ToList();


                //var results = (from a in fessTypePriceData
                //               join e in FeesTypeList on a.FeesTypePriceId equals e.ID
                //               join p in paymentCollectionTypes on a.FeesTypePriceId equals p.FeeTypeId
                //               select new
                //               {
                //                   Price = a.Price, //feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(s => s.ApplicabledataId == c.StandardId && s.Active == true).Select(r => r.Price).FirstOrDefault(),
                //                   FeesTypeName =  feestypeRepo.GetAllFeesType().Where(x => x.Active == true && x.ID == a.FeesTypePriceId ).Select(x => x.Name).FirstOrDefault(),
                //                   ParentFeesType= feestypeRepo.GetAllFeesType().Where(x => x.Active == true && x.ID == e.ParentFeeTypeID).Select(x => x.Name).FirstOrDefault(),
                //                   //Mandatory = CommonSchoolModel.GetAllSessionFeesType().Where(x => x.Active == true && x.FeesTypeId==a.FeesTypePriceId).Select(x=>x.Mandatory).FirstOrDefault(),
                //                   Mandatory = sessionfeestyperepo.GetAllSessionFeesType().Where(x => x.Active == true && x.FeesTypeId == a.FeesTypePriceId).Select(x => x.IsMandatory).FirstOrDefault(),
                //                   PaymentCId= p.ID , //CommonSchoolModel.GetAllPaymentCollectionTypes().Where(r => r.Active == true && r.FeeTypeId==a.FeesTypePriceId).Select(p=>p.ID).FirstOrDefault(),
                //               }).ToList();


               

                return Json(results);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }

        }


        public JsonResult ParentPaymentSummaryViewData(long studentid , long paymentCId)
        {
            //CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("PaymentCollectionType", accessId, "List", "PaymentCollectionType", roleId) == false)
            //{
            //    Json(new
            //    {
            //        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
            //        isRedirect = true
            //    });
            //}
            try
            {

                var students = studentAggregateRepository.GetAllStudent().Where(s => s.ID == studentid).ToList();
               // var allacademicsectionstudents = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).ToList();
                var Getclassid = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).Select(i => i.StandardId).FirstOrDefault();
                var GetFeesTypeid = feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(s => s.ApplicabledataId == Getclassid && s.Active == true).Select(o => o.FeesTypePriceId).FirstOrDefault();
                //var paymentCollectionTypes = CommonSchoolModel.GetAllPaymentCollectionTypes().Where(e => e.Active == true).ToList();

                var paymentCollectionTypes = paymentCollectionRepo.GetAllPaymentCollectionType().Where(e => e.Active == true).ToList();
                var paymentCollectionTypesdata = paymentCollectionRepo.GetAllPaymentCollectionTypeData().Where(x => x.Active == true).ToList();
                var fessTypePriceData = feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(s => s.ApplicabledataId == Getclassid && s.Active == true).ToList();
                var feestype = CommonSchoolModel.GetAllSessionFeesType().Where(x => x.FeesTypeId == GetFeesTypeid).ToList();
               // var StudentPayment= commonsqlquery.getStudentPaymentData()

                var results = (from a in paymentCollectionTypes
                               join b in paymentCollectionTypesdata on a.ID equals b.PaymentCollectionId
                              // join c in allacademicsectionstudents on a.ClassId equals c.StandardId
                               //join d in fessTypePriceData on a.ClassId equals d.ApplicabledataId
                               //join e in
                               select new
                               {
                                   PaymentCollectionDataId=b.PaymentCollectionId,
                                   PaymentCollectionId = b.ID,
                                   paymentname = b.PaymentName,
                                   fromdate = Convert.ToDateTime(b.FromDate).ToShortDateString(),
                                   todate = Convert.ToDateTime(b.ToDate).ToShortDateString(),
                                   deadlinedate = Convert.ToDateTime(b.DeadLineDate).ToShortDateString(),
                                   amount = b.Amount,
                                   paystatus = commonsqlquery.getStudentPaymentData(b.ID,studentid).Where(s => s.PaymentCollectionId == b.ID && s.StudentId == studentid).Select(s => s.PaymentStatus).FirstOrDefault()=="null"?"Due": commonsqlquery.getStudentPaymentData(b.ID, studentid).Where(s => s.PaymentCollectionId == b.ID && s.StudentId== studentid).Select(s => s.PaymentStatus).FirstOrDefault(),
                                   academicyearid = a.AcademicSessionId,
                                   orgnizationid = a.OrganizationId,
                                   wingid = a.WingId,
                                   boardid = a.Boardid,
                                   classid = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).Select(f => f.StandardId).FirstOrDefault(),
                                   Studentid = studentid,
                                   organization = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).Select(f=> f.OraganisationName).FirstOrDefault(),
                                   AcademicSession = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).Select(f => f.SessionName).FirstOrDefault(),
                                   wingname = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).Select(f => f.WingName).FirstOrDefault(),                                 
                                   boardname = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).Select(f => f.BoardName).FirstOrDefault(),                                   
                                   FeesTypePrice = feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(s => s.ApplicabledataId == commonsqlquery.Get_view_All_Academic_Student_Sections().Where(i => i.StudentId == studentid).Select(f => f.StandardId).FirstOrDefault() && s.Active == true).Select(r => r.Price).FirstOrDefault(),
                                   FeesType = CommonSchoolModel.GetAllSessionFeesType().Where(x => x.FeesTypeId == GetFeesTypeid).Select(g => g.FeesTypeName).FirstOrDefault()
                               }).Where(t=>t.PaymentCollectionDataId == paymentCId && t.Studentid==studentid).ToList();

                return Json(results);
            }
            catch (Exception ex)
            {
                return Json(ex.Message.ToString());
            }

        }

        public JsonResult PayStudentInstallment(StudentPayment studentpayment)
        {
            //CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("PaymentCollectionType", accessId, "List", "PaymentCollectionType", roleId) == false)
            //{
            //    Json(new
            //    {
            //        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
            //        isRedirect = true
            //    });
            //}
            long i = 0; string RetrunMsg = "";
            try
            {
                userId = HttpContext.Session.GetInt32("userId").Value;
                if (ModelState.IsValid)
                {

                    StudentPayment studentpayment1 = new StudentPayment();

                    studentpayment1.AmountPaid = studentpayment.AmountPaid;
                    studentpayment1.PaymentCollectionId = studentpayment.PaymentCollectionId;
                    studentpayment1.PaidDate= DateTime.Now;
                    studentpayment1.PaidUserId = userId;
                    studentpayment1.PaymentTypeId = studentpayment.PaymentTypeId;
                    studentpayment1.PaymentStatus = "Paid";
                    studentpayment1.TransactionId = "1234567890";
                    studentpayment1.Active = true;
                    studentpayment1.StudentId = studentpayment.StudentId;

                    if (studentpayment1.ID == 0)
                    {
                        studentpayment1.InsertedId = userId;
                        studentpayment1.Status = EntityStatus.ACTIVE;
                        // studentpayment1.ReturnMsg = "Records added Successfully.";
                       i= studentpaymentrepo.CreateStudentPayment(studentpayment1);
                        if(i!=0)
                        {
                            RetrunMsg = "Records added Successfully";
                        }
                        else
                        {
                            RetrunMsg = "Records added Failed !!!";
                        }
                    }
                    
                }
                
            }
            catch(Exception ex)
            {

            }
            return Json(RetrunMsg);
        }


        public List<PaymentType> GetPaymentType()
        {
            //CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("PaymentCollectionType", accessId, "List", "PaymentCollectionType", roleId) == false)
            //{
            //    Json(new
            //    {
            //        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
            //        isRedirect = true
            //    });
            //}
            List<PaymentType> ptype = new List<PaymentType>();
            try {
               
                IEnumerable<PaymentType> myGenericEnumerable = (IEnumerable<PaymentType>)ptype;
                myGenericEnumerable = paymenttype.GetAllPaymentType().Where(c => c.Active == true).ToList();
                var res = (from a in myGenericEnumerable.ToList()
                           select new PaymentType
                           {
                               ID = a.ID,
                               Name = a.Name
                           }).ToList();
                ptype = res;
                return ptype;

            }
            catch { }
            return ptype;
        }
    }


}
