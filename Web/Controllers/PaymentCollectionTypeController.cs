﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities.StudentPaymentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Web.Models;
using OdmErp.Web.Models.StudentPayment;
using Web.Controllers;
using X.PagedList.Mvc;
using X.PagedList;
using OdmErp.Web.ViewModels;
using OdmErp.ApplicationCore.Entities.SessionFeesType;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OfficeOpenXml;
using ClosedXML.Excel;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using System.Data;
using OdmErp.ApplicationCore.Entities;

namespace OdmErp.Web.Controllers
{
    public class PaymentCollectionTypeController : AppController
    {
        private IFeesTypePrice feesTypePriceRepository;
        private IPaymentCollectionType paymentCollectionRepo;
        private CommonMethods commonMethods;
        public CommonSchoolModel CommonSchoolModel;

        public Extensions.SessionExtensions sessionExtensions;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IOrganizationRepository organizationRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IBoardStandardRepository boardStandardRepository;
        public IStandardRepository standardRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        public IAcademicStudentRepository academicStudentRepository;
        public IScholarshipApplyRepository scholarshipApplyRepository;
        public commonsqlquery commonsqlquery;
        private readonly IHostingEnvironment env;
        private readonly IPaymentGatewayRepository paymentGatewayRepository;
        public PaymentCollectionTypeController(IFeesTypePrice feesTypePriceRepo, IPaymentCollectionType paymentCollectionRepo, CommonMethods commonMethods,
            CommonSchoolModel CommonSchoolModel, IOrganizationAcademicRepository organizationAcademicRepository, IOrganizationRepository organizationRepository,
            IAcademicStandardRepository academicStandardRepository, IBoardStandardRepository boardStandardRepository, IStandardRepository standardRepository,
            IStudentAggregateRepository studentAggregateRepository, IAcademicStudentRepository academicStudentRepository, IScholarshipApplyRepository scholarshipApplyRepo, commonsqlquery commonsqlquery,
            IHostingEnvironment env, IPaymentGatewayRepository paymentGatewayRepository)
        {
            feesTypePriceRepository = feesTypePriceRepo;
            this.paymentCollectionRepo = paymentCollectionRepo;
            this.commonMethods = commonMethods;
            this.CommonSchoolModel = CommonSchoolModel;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.organizationRepository = organizationRepository;
            this.academicStandardRepository = academicStandardRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.standardRepository = standardRepository;
            this.studentAggregateRepository = studentAggregateRepository;
            this.academicStudentRepository = academicStudentRepository;
            scholarshipApplyRepository = scholarshipApplyRepo;
            this.commonsqlquery = commonsqlquery;
            this.env = env;
            this.paymentGatewayRepository = paymentGatewayRepository;
        }
        public IActionResult PaymentCollectionType()
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("PaymentCollectionType", accessId, "List", "PaymentCollectionType", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var onePageOfEmps = CommonSchoolModel.GetAllPaymentCollectionTypes().ToList();
            return View("PaymentCollectionType", onePageOfEmps);
            //return View();
        }


        public List<SessionFeesTypeViewModel> Get_SessionFeesType(long? id)
        {
            //CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //                   // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("FeesType", accessId, "List", "StudentPayment", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            List<SessionFeesTypeViewModel> feesType = new List<SessionFeesTypeViewModel>();
            IEnumerable<SessionFeesTypeViewModel> myGenericEnumerable = (IEnumerable<SessionFeesTypeViewModel>)feesType;
            myGenericEnumerable = CommonSchoolModel.GetAllFeesType().Where(x => x.SessionId == id && x.ParentFeesTypeId==0 && x.HaveInstallment == true).ToList();
            var res = (from a in myGenericEnumerable.ToList()
                       select new SessionFeesTypeViewModel
                       {
                           ID = a.FeesTypeId,
                           FeesTypeName = a.FeesTypeName,
                           ParentFeesType = a.ParentFeesType,// == null ? "N A" : a.ParentFeesType,
                           ApplicableId = a.ApplicableId
                       }).ToList();
            feesType = res;

            return feesType;
        }


        public JsonResult GetFeePriceData(long? id)
        {
            decimal price = 0;
            try
            {
                if (id != 0)
                {
                    price = commonsqlquery.Get_getFeePrice(id).Select(i => i.price).FirstOrDefault();
                }
                else
                {
                    price = 0;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return Json(price);
        }


        public JsonResult GetPriceData(long? id, string type)
        {
            var price = commonsqlquery.Get_getMandatoryPrice(id,type).Where(i => i.ApplicabledataId == id && i.Applicabledata==type).Select(i => i.price).FirstOrDefault();

            //var applicabletoid = feesTypePriceRepository.GetAllFeesTypePriceApplicableData().Where(c => c.ApplicabledataId == id && c.Applicabledata == type && c.Active == true).ToList();
            //var SessionFeestypedata = CommonSchoolModel.GetAllSessionFeesType().Where(w => w.Mandatory == true && w.Active == true).ToList();
            ////.Select(c => c.Price).FirstOrDefault()

            //var results = (from a in applicabletoid
            //               join b in SessionFeestypedata on
            //               new { FeesTypeId = a.FeesTypePriceId, ApplicableToId = a.ApplicableToId } equals new { FeesTypeId = b.FeesTypeId, ApplicableToId = b.ApplicableId }
            //               select new
            //               {
            //                   price = a.Price
            //               });


            //from b in _dbContext.Burden
            //join bl in _dbContext.BurdenLookups on
            //new { Organization_Type = b.Organization_Type_ID, Cost_Type = b.Cost_Type_ID } equals
            //new { Organization_Type = bl.Organization_Type_ID, Cost_Type = bl.Cost_Type_ID }



            //select new
            //{

            //    paymentname = b.PaymentName,
            //    fromdate = Convert.ToDateTime(b.FromDate).ToShortDateString(),
            //    todate = Convert.ToDateTime(b.ToDate).ToShortDateString(),
            //    deadlinedate = Convert.ToDateTime(b.DeadLineDate).ToShortDateString(),
            //    amount = b.Amount,
            //    paystatus = "Paid",
            //    academicyearid = a.AcademicSessionId,
            //    orgnizationid = a.OrganizationId,
            //    wingid = a.WingId,
            //    boardid = a.Boardid,
            //    classid = a.ClassId,
            //    studentid = d.ID


            //}).ToList();




            return Json(price);
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SaveOrUpdatePaymentCollectionType(PaymentCollectionType paymentCollectionType)
        {
            string Return_Msg = "";
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                PaymentCollectionType paymentCollectionType1 = new PaymentCollectionType();

                paymentCollectionType1.ID = paymentCollectionType.ID;
                paymentCollectionType1.AcademicSessionId = paymentCollectionType.AcademicSessionId;
                paymentCollectionType1.OrganizationId = paymentCollectionType.OrganizationId;
                paymentCollectionType1.WingId = paymentCollectionType.WingId;
                paymentCollectionType1.Boardid = paymentCollectionType.Boardid;
                paymentCollectionType1.ClassId = paymentCollectionType.ClassId;
                paymentCollectionType1.SumAmount = paymentCollectionType.SumAmount;
                paymentCollectionType1.NoOfInstallment = paymentCollectionType.NoOfInstallment;
                paymentCollectionType1.InsertedId = userId;
                paymentCollectionType1.Status = paymentCollectionType.Status;
                paymentCollectionType1.FeeTypeId = paymentCollectionType.FeeTypeId;

                //List<PaymentCollectionType> newchild = new List<PaymentCollectionType>();
                List<PaymentCollectionTypeData> ObjChildData = new List<PaymentCollectionTypeData>();
                ObjChildData = paymentCollectionType.paymentCollectionTypeData;

                foreach (var value in ObjChildData)
                {
                    paymentCollectionType1.paymentCollectionTypeData.Add(new PaymentCollectionTypeData() { InsertedId = userId, PaymentName = value.PaymentName, Amount = value.Amount, FromDate = value.FromDate, ToDate = value.ToDate, DeadLineDate = value.DeadLineDate, LatefeePercentage = value.LatefeePercentage });
                }
                long i = 0;
                if (CheckPaymenctCollectionExists(paymentCollectionType.AcademicSessionId, paymentCollectionType.FeeTypeId) == true)
                {
                    i = paymentCollectionRepo.CreatePaymentCollectionType(paymentCollectionType1);
                    if (i == 1)
                    {
                        Return_Msg = "Records Saved Successfully.";
                    }
                    else
                    {
                        Return_Msg = "Records Saved Failed.!!!";
                    }
                }
                else
                {
                    Return_Msg = "Records already Exists.!!!";
                }
               
            }
            return Json(Return_Msg);
        }

        //private bool CheckPaymenctCollectionExists(decimal Amount, DateTime FromDate, DateTime Todate)
        //{

        //    var res = paymentCollectionRepo.GetAllPaymentCollectionTypeData().Where(s => s.Amount == Amount && s.FromDate == FromDate && s.ToDate == Todate && s.Active == true).ToList();
        //    //.Where(x => x.SessionId == id).ToList();
        //    if (res.Count > 0)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}

        private bool CheckPaymenctCollectionExists11(long AcademicYearId, long organizationId, long wingId, long boardId, long AcademicStandardId,decimal TotalAmount)
        {

            var res = paymentCollectionRepo.GetAllPaymentCollectionType().Where(s => s.AcademicSessionId == AcademicYearId && s.OrganizationId == organizationId && s.WingId == wingId && s.Boardid== boardId && s.ClassId==AcademicStandardId && s.SumAmount==TotalAmount && s.Active == true).ToList();
            //.Where(x => x.SessionId == id).ToList();
            if (res.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckPaymenctCollectionExists(long AcademicYearId, long feetypeid)
        {

            var res = paymentCollectionRepo.GetAllPaymentCollectionType().Where(s => s.AcademicSessionId == AcademicYearId && s.FeeTypeId == feetypeid && s.Active == true).ToList();
            //.Where(x => x.SessionId == id).ToList();
            if (res.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }



        public JsonResult Update(PaymentCollectionType paymentCollectionType)
        {
            string ReturnMsg = "";
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {


                PaymentCollectionType paymentCollectionType1 = new PaymentCollectionType();

                paymentCollectionType1.ID = paymentCollectionType.ID;
                paymentCollectionType1.AcademicSessionId = paymentCollectionType.AcademicSessionId;
                paymentCollectionType1.OrganizationId = paymentCollectionType.OrganizationId;
                paymentCollectionType1.WingId = paymentCollectionType.WingId;
                paymentCollectionType1.Boardid = paymentCollectionType.Boardid;
                paymentCollectionType1.ClassId = paymentCollectionType.ClassId;
                paymentCollectionType1.SumAmount = paymentCollectionType.SumAmount;
                paymentCollectionType1.NoOfInstallment = paymentCollectionType.NoOfInstallment;
                paymentCollectionType1.InsertedId = userId;
                paymentCollectionType1.FeeTypeId = paymentCollectionType.FeeTypeId;

                // List<PaymentCollectionTypeData> newchild = new List<PaymentCollectionTypeData>();
                List<PaymentCollectionTypeData> ObjChildData = new List<PaymentCollectionTypeData>();
                ObjChildData = paymentCollectionType.paymentCollectionTypeData;

                foreach (var value in ObjChildData)
                {
                    paymentCollectionType1.paymentCollectionTypeData.Add(new PaymentCollectionTypeData() { InsertedId = userId, ID = value.ID, PaymentName = value.PaymentName, Amount = value.Amount, FromDate = value.FromDate, ToDate = value.ToDate, DeadLineDate = value.DeadLineDate,LatefeePercentage=value.LatefeePercentage });
                }

                long i = paymentCollectionRepo.UpdatePaymentCollectionType(paymentCollectionType1);
                if (i == 1)
                {
                    ReturnMsg = "Records updated Successfully.";
                }
                else
                {
                    ReturnMsg = "Records updated Faild.!!!";
                }


            }
            return Json(ReturnMsg); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }


        [HttpGet]
        public JsonResult GetPaymentCollectionTypeById(long? id)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            PaymentCollectionType od = new PaymentCollectionType();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Create", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Create";
                //od.ID = 0;
                //od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("FeesType Applicable", accessId, "Edit", "StudentPayment", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Update";
                var paymentcollectiontype = paymentCollectionRepo.GetPaymentCollectionTypeById(id.Value);
                od.ID = id.Value;
                od.AcademicSessionId = paymentcollectiontype.AcademicSessionId;
                od.OrganizationId = paymentcollectiontype.OrganizationId;
                od.WingId = paymentcollectiontype.WingId;
                od.Boardid = paymentcollectiontype.Boardid;
                od.ClassId = paymentcollectiontype.ClassId;
                od.SumAmount = paymentcollectiontype.SumAmount;
                od.NoOfInstallment = paymentcollectiontype.NoOfInstallment;
                od.FeeTypeId = paymentcollectiontype.FeeTypeId;
                od.paymentCollectionTypeData = paymentcollectiontype.paymentCollectionTypeData;
            }
            return Json(od);
        }

        public JsonResult DeletePaymentCollectionType(long? id)
        {
            string ReturnMsg = "";
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("PaymentCollectionType", accessId, "Delete", "PaymentCollectionType", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            PaymentCollectionType obj = new PaymentCollectionType();
            var val = false;
            if (FeesTypePriceExists1(id.Value) == true)
            {
                val = paymentCollectionRepo.DeletePaymentCollectionType(id.Value);
            }

            if (val == true)
            {
                ReturnMsg = "Deleted successfully";
            }
            else
            {
                ReturnMsg = "Delete Failed.!!!";
            }

            return Json(ReturnMsg);
        }

        public JsonResult DeleteFeesTypePriceAll()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesType Price", accessId, "Delete", "FeesTypePrice", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }

            var feesTypeApplicable = feesTypePriceRepository.DeleteAllFeesTypePrice();

            return Json("Deleted successfully");
        }

        private bool FeesTypePriceExists1(long id)
        {
            if (paymentCollectionRepo.GetPaymentCollectionTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public JsonResult GetOrganisationByAcademicSession(long AcademicSessionId)                 //Filter the selected Standard
        {
            try
            {
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicSessionId == AcademicSessionId);
                var OrganisationList = organizationRepository.GetAllOrganization();
                var res = (from a in OrganisationAcademic
                           join b in OrganisationList on a.OrganizationId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }).ToList();
                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        //public JsonResult GetWingBySchoolWing(long OraganisationAcademicId)
        //{
        //    try
        //    {
        //        var SchoolWing = commonMethods.GetSchoolWing().Where(a => a.OrganizationAcademicID == OraganisationAcademicId).ToList();
        //        var wingList = commonMethods.GetWing();
        //        var res = (from a in SchoolWing
        //                   join b in wingList on a.WingID equals b.ID
        //                   select new
        //                   {
        //                       id = a.ID,
        //                       name = b.Name
        //                   }).ToList();
        //        return Json(res);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}

        public JsonResult GetClassByBoard(long BoardId, long OrganizationAcademicId, long schoolwingId)
        {
            try
            {
                var academicstandard = academicStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.OrganizationAcademicId == OrganizationAcademicId).ToList();
                var BoardStandardList = boardStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.BoardId == BoardId);
                var StandardList = standardRepository.GetAllStandard();
                var res = (from a in academicstandard
                           join b in BoardStandardList on a.BoardStandardId equals b.ID
                           join c in StandardList on b.StandardId equals c.ID
                           select new
                           {
                               id = a.ID,
                               name = c.Name
                           }).ToList();
                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        public JsonResult GetAllStudentsByAcademicStandard(long AcademicStandardId, long? AcademicStandardStreamId)
        {
            try
            {
                var students = studentAggregateRepository.GetAllStudent().Where(s => s.Active == true).ToList();
                var allacademicstudents = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicStandardId == AcademicStandardId).ToList();
                var allacademicsectionstudents = commonsqlquery.Get_view_All_Academic_Student_Sections();
                var paymentCollectionTypes = CommonSchoolModel.GetAllPaymentCollectionTypes().Where(e => e.Active == true && e.ClassId == AcademicStandardId).ToList();
                var paymentCollectionTypesdata = paymentCollectionRepo.GetAllPaymentCollectionTypeData().Where(x => x.Active == true).ToList();
                var paymentCollectionTypesdata_new = paymentCollectionRepo.GetAllPaymentCollectionTypeData().Where(x => x.Active == true).ToList();


                var query =
                (from t in paymentCollectionTypesdata
                 join r in paymentCollectionTypes on t.PaymentCollectionId equals r.ID
                 where t.PaymentCollectionId == r.ID && r.ClassId == AcademicStandardId
                 group t by new
                 {
                     t.PaymentName
                 } into g
                 orderby g.Key.PaymentName
                 select new
                 {
                     payTypeName = g.Key.PaymentName
                 }).ToList();

                var features = new List<string>();
                foreach (var a in query)
                {
                    var sdde = a.payTypeName;
                    features.Add(a.payTypeName);
                }

                var query1 =
                   (from t1 in paymentCollectionTypes
                    where t1.ClassId == AcademicStandardId
                    group t1 by new
                    {
                        t1.Organization,
                        t1.Wing,
                        t1.Board,
                        t1.Class
                    } into g
                    orderby g.Key.Organization, g.Key.Wing, g.Key.Board, g.Key.Class
                    select new
                    {
                        values = g.Key.Organization + " , " + g.Key.Wing + " , " + g.Key.Board + " , " + g.Key.Class

                    }).ToList();


                var features1 = new List<string>();
                foreach (var a1 in query1)
                {
                    var sdde = a1.values;
                    features1.Add(a1.values);
                }




                var commonres = (from a in paymentCollectionTypes
                                     // join b in paymentCollectionTypesdata on a.ID equals b.PaymentCollectionId
                                 join c in allacademicstudents on a.ClassId equals c.AcademicStandardId
                                 join d in students on c.StudentId equals d.ID
                                 select new
                                 {


                                     photo = env.WebRootFileProvider.GetFileInfo("~\\ODMImages\\StudentProfile\\" + d.Image)?.PhysicalPath,
                                     id = a.ID,
                                     typeDataId = (from e in paymentCollectionTypesdata_new where e.PaymentCollectionId == a.ID select e.ID),
                                     name = d.FirstName + " " + d.LastName,
                                     inr = a.SumAmount.ToString(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")),
                                     payTypeName = features,
                                     standard = features1,
                                     studentid = d.ID,
                                     academicSessionId = a.AcademicSessionId,
                                     academicSession = a.AcademicSession,
                                     organization = a.Organization,
                                     organizationid = a.OrganizationId,
                                     wing = a.Wing,
                                     wingid = a.WingId,
                                     boardid = a.Boardid,
                                     board = a.Board,
                                     classname = a.Class,
                                     classid = a.ClassId,
                                     sumamount = a.SumAmount,
                                     section = allacademicsectionstudents.ToList().Count() == 0 ? "" : (allacademicsectionstudents.Where(m => m.AcademicStudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : allacademicsectionstudents.Where(m => m.AcademicStudentId == a.ID && m.IsAvailable == true).FirstOrDefault().SectionName),
                                     studentcode = d.StudentCode,
                                     studentWithScholarship = scholarshipApplyRepository.GetAllScholarshipApply().Where(e => e.AcademicSessionId == a.AcademicSessionId && e.StudentId == d.ID).FirstOrDefault(),
                                     // sentBy = (a.InsertedId == 0 ? null : GetUserNameById(a.InsertedId)),

                                     //code = d.StudentCode,
                                     //status = "O",
                                     //standard = standardRepository.GetAllStandard().Where(t => t.ID == AcademicStandardId).Select(t => t.Name).FirstOrDefault(),
                                     //section = allacademicsectionstudents.ToList().Count() == 0 ? "" : (allacademicsectionstudents.Where(m => m.AcademicStudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : allacademicsectionstudents.Where(m => m.AcademicStudentId == a.ID && m.IsAvailable == true).FirstOrDefault().SectionName)

                                 }).ToList();




                //var commonres = (from a in allacademicstudents
                //                 join b in students on a.StudentId equals b.ID
                //                 join c in paymentCollectionTypes on AcademicStandardId equals c.ClassId
                //                 join d in paymentCollectionTypesdata on c.ID equals d.PaymentCollectionId
                //                 select new
                //                 {
                //                     // var path = env.WebRootFileProvider.GetFileInfo("images/foo.png")?.PhysicalPath

                //                     photo = env.WebRootFileProvider.GetFileInfo("~\\ODMImages\\StudentProfile\\" + b.Image)?.PhysicalPath,
                //                     id = a.ID,
                //                     name = b.FirstName + " " + b.LastName,
                //                     inr = d.Amount,
                //                     payTypeName = d.PaymentName,
                //                     code = b.StudentCode,
                //                     status = "O",
                //                     standard = standardRepository.GetAllStandard().Where(t => t.ID == AcademicStandardId).Select(t => t.Name).FirstOrDefault(),
                //                     section = allacademicsectionstudents.ToList().Count() == 0 ? "" : (allacademicsectionstudents.Where(m => m.AcademicStudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : allacademicsectionstudents.Where(m => m.AcademicStudentId == a.ID && m.IsAvailable == true).FirstOrDefault().SectionName)
                //                 }).ToList();

                return Json(commonres);

            }
            catch (Exception e)
            {
                return Json(null);
            }
        }
        public JsonResult GetAllStudentsData(long AcademicYearId,long organizationId,long wingId,long boardId, long AcademicStandardId,long studentid)
        {
            try
            {
                var students = studentAggregateRepository.GetAllStudent().Where(s=>s.ID==studentid && s.Active==true).ToList();
                var allacademicsectionstudents = commonsqlquery.Get_view_All_Academic_Student_Sections().Where(p=>p.StandardId==AcademicStandardId && p.StudentId==studentid).ToList();
                var paymentCollectionTypes = CommonSchoolModel.GetAllPaymentCollectionTypes().Where(e => e.Active == true && e.AcademicSessionId==AcademicYearId && e.OrganizationId== organizationId && e.WingId== wingId && e.Boardid==boardId && e.ClassId == AcademicStandardId).ToList();
                var paymentCollectionTypesdata = paymentCollectionRepo.GetAllPaymentCollectionTypeData().Where(x => x.Active == true).ToList();

                var results = (from a in paymentCollectionTypes
                               join b in paymentCollectionTypesdata on a.ID equals b.PaymentCollectionId
                               join c in allacademicsectionstudents on a.ClassId equals c.StandardId
                               join d in students on studentid equals d.ID
                               select new
                               {
                                   
                                   paymentname = b.PaymentName,
                                   fromdate = Convert.ToDateTime(b.FromDate).ToShortDateString(),
                                   todate = Convert.ToDateTime(b.ToDate).ToShortDateString(),
                                   deadlinedate = Convert.ToDateTime(b.DeadLineDate).ToShortDateString(),
                                   amount = b.Amount,
                                   paystatus = commonsqlquery.getStudentPaymentData(b.ID,d.ID).Where(s => s.PaymentCollectionId == b.ID && s.StudentId==d.ID).Select(s => s.PaymentStatus).FirstOrDefault() == "null" ? "Due" : commonsqlquery.getStudentPaymentData(b.ID,d.ID).Where(s => s.PaymentCollectionId == b.ID && s.StudentId == d.ID).Select(s => s.PaymentStatus).FirstOrDefault(),
                                   academicyearid=a.AcademicSessionId,
                                   orgnizationid=a.OrganizationId,
                                   wingid=a.WingId,
                                   boardid=a.Boardid,
                                   classid=a.ClassId,
                                   studentid=d.ID


                               }).ToList();
                return Json(results);
            }
            catch {
                return Json(null);
            }

        }

        public JsonResult ActiveDeactivePaymentCollectionType(string id, string IsActive)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("FeesType Price", accessId, "Update", "FeesType Price", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {

                var sessioschalar = paymentCollectionRepo.GetPaymentCollectionTypeById(Convert.ToInt64(id));
                OdmErp.ApplicationCore.Entities.StudentPaymentAggregate.PaymentCollectionTypeData objScholarship = new PaymentCollectionTypeData();
                sessioschalar.ID = Convert.ToInt64(id);
                sessioschalar.Status = IsActive;
                sessioschalar.Active = true;

                //TODO
                sessioschalar.ModifiedId = userId;
                paymentCollectionRepo.UpdatePaymentCollectionTypeStatus(sessioschalar);
                objScholar.ReturnMsg = sessioschalar.Status.ToLower() + " Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        #region ---------------------Master Payment Gateway---------------------------

        public IActionResult MasterPaymentGateway()
        {
            var payment = paymentGatewayRepository.GetAllPaymentGateway();
            return View(payment);
        }
        public IActionResult CreateOrEditMasterPaymentGateway(long? id)
        {
            PaymentGateway paymentGateway = new PaymentGateway();
            if(id != null)
            {
                var pay = paymentGatewayRepository.GetPaymentGatewayById(id.Value);
                paymentGateway.ProviderName = pay.ProviderName;
                paymentGateway.Host = pay.Host;
                paymentGateway.HttpMethod = pay.HttpMethod;
                paymentGateway.RequestParameter = pay.RequestParameter;
                paymentGateway.ResponseParameter = pay.ResponseParameter;
                paymentGateway.ServerToServerCallback = pay.ServerToServerCallback;
                paymentGateway.MerchantId = pay.MerchantId;
                paymentGateway.CheckSum = pay.CheckSum;
                paymentGateway.TypeField = pay.TypeField;
                paymentGateway.CurrencyType = pay.CurrencyType;
                paymentGateway.TypeFieldTwo = pay.TypeFieldTwo;

                ViewBag.status = "Update";
            }
            else
            {
                paymentGateway.ProviderName = "";
                paymentGateway.Host = "";
                paymentGateway.HttpMethod = "";
                paymentGateway.RequestParameter = "";
                paymentGateway.ResponseParameter = "";
                paymentGateway.ServerToServerCallback = "";
                paymentGateway.MerchantId = "";
                paymentGateway.CheckSum = "";
                paymentGateway.TypeField = "";
                paymentGateway.CurrencyType = "";
                paymentGateway.TypeFieldTwo = "";

                ViewBag.status = "Create";
            }

            return View(paymentGateway);
        }
        public IActionResult SaveOrUpdateMasterPaymentGateway(PaymentGateway payment)
        {
            CheckLoginStatus();
            PaymentGateway gateway = new PaymentGateway();
            if (payment.ID > 0)
            {
                var pay = paymentGatewayRepository.GetPaymentGatewayById(payment.ID);
                gateway.ModifiedDate = DateTime.Now;
                gateway.ModifiedId = userId;
                gateway.Status = EntityStatus.UPDATED;
                gateway.ProviderName = payment.ProviderName;
                gateway.Host = payment.Host;
                gateway.HttpMethod = payment.HttpMethod;
                gateway.RequestParameter = payment.RequestParameter;
                gateway.ResponseParameter = payment.ResponseParameter;
                gateway.ServerToServerCallback = payment.ServerToServerCallback;
                gateway.MerchantId = payment.MerchantId;
                gateway.CheckSum = payment.CheckSum;
                gateway.TypeField = payment.TypeField;
                gateway.CurrencyType = payment.CurrencyType;
                gateway.TypeFieldTwo = payment.TypeFieldTwo;

                paymentGatewayRepository.UpdatePaymentGateway(gateway);
            }
            else
            {
                gateway.InsertedDate = DateTime.Now;
                gateway.InsertedId = userId;
                gateway.ModifiedDate = DateTime.Now;
                gateway.ModifiedId = userId;
                gateway.Active = true;
                gateway.Status = EntityStatus.ACTIVE;
                gateway.IsAvailable = true;
                gateway.ProviderName = payment.ProviderName;
                gateway.Host = payment.Host;
                gateway.HttpMethod = payment.HttpMethod;
                gateway.RequestParameter = payment.RequestParameter;
                gateway.ResponseParameter = payment.ResponseParameter;
                gateway.ServerToServerCallback = payment.ServerToServerCallback;
                gateway.MerchantId = payment.MerchantId;
                gateway.CheckSum = payment.CheckSum;
                gateway.TypeField = payment.TypeField;
                gateway.CurrencyType = payment.CurrencyType;
                gateway.TypeFieldTwo = payment.TypeFieldTwo;

                paymentGatewayRepository.CreatePaymentGateway(gateway);
            }

            return RedirectToAction(nameof(MasterPaymentGateway));
        }

        #endregion

        #region -----------------MasterPaymentGatewayBTB-------------------
              

        public IActionResult PaymentGatewayBTB()
        {
            var btb = paymentGatewayRepository.GetAllPaymentGatewayBTB().ToList();
            var gateway = paymentGatewayRepository.GetAllPaymentGateway().ToList();

            var res = (from a in btb
                       join b in gateway on a.PaymentGatewayId equals b.ID
                       select new PaymentGatewayBTBModel
                       {
                           ID = a.ID,
                           ProviderName = b.ProviderName,
                           Host = b.Host,
                           HttpMethod = b.HttpMethod,
                           BtbParam = a.BtbParam
                       }).ToList();

            return View(res);
        }
        public IActionResult CreateOrEditPaymentGatewayBTB(long? id)
        {
            PaymentGatewayBTBModel model = new PaymentGatewayBTBModel();
            ViewBag.GateWay = paymentGatewayRepository.GetAllPaymentGateway();
            if (id != null)
            {
                var btb = paymentGatewayRepository.GetPaymentGatewayBTBById(id.Value);
                model.BtbParam = btb.BtbParam;
                model.PaymentGatewayId = btb.PaymentGatewayId;

                ViewBag.Status = "Update";
            }
            else
            {
                model.PaymentGatewayId = 0;
                model.BtbParam = "";
                ViewBag.Status = "Create";
            }

            return View(model);
        }
        public IActionResult SaveOrUpdatePaymentGatewayBTB(PaymentGatewayBTBModel payment)
        {
            CheckLoginStatus();
            PaymentGatewayBTB gatewayBTB = new PaymentGatewayBTB();
            if (payment.ID > 0)
            {
                var btb = paymentGatewayRepository.GetPaymentGatewayBTBById(payment.ID);
                gatewayBTB.PaymentGatewayId = payment.PaymentGatewayId;
                gatewayBTB.BtbParam = payment.BtbParam;
                gatewayBTB.ModifiedId = userId;
                gatewayBTB.ModifiedDate = DateTime.Now;

                paymentGatewayRepository.UpdatePaymentGatewayBTB(gatewayBTB);
            }
            else
            {
                gatewayBTB.InsertedDate = DateTime.Now;
                gatewayBTB.InsertedId = userId;
                gatewayBTB.ModifiedId = userId;
                gatewayBTB.ModifiedDate = DateTime.Now;
                gatewayBTB.Active = true;
                gatewayBTB.Status = EntityStatus.ACTIVE;
                gatewayBTB.IsAvailable = true;
                gatewayBTB.PaymentGatewayId = payment.PaymentGatewayId;
                gatewayBTB.BtbParam = payment.BtbParam;

                paymentGatewayRepository.CreatePaymentGatewayBTB(gatewayBTB);
            }

            return RedirectToAction(nameof(PaymentGatewayBTB));
        }
        #endregion
    }
}

