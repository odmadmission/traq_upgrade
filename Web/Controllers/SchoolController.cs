﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Models;

using OdmErp.Web.Models.SchoolBelq;
using OdmErp.Infrastructure.RepositoryImpl.SchoolRepository;
using OfficeOpenXml;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Collections;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using Microsoft.AspNetCore.Routing;
using OdmErp.Web.ViewModels;
using static OdmErp.Web.ViewModels.CountChapterCountViewModel;

using DocumentFormat.OpenXml.Drawing.Diagrams;
using DataModel = OdmErp.Web.ViewModels.DataModel;
using OdmErp.ApplicationCore.Entities.StudentAggregate;

using OdmErp.ApplicationCore.Entities.ModuleAggregate;
using DocumentFormat.OpenXml.Office2010.Excel;
using DocumentFormat.OpenXml.Wordprocessing;
using OdmErp.ApplicationCore.Query;
using OdmErp.ApplicationCore.Entities.MasterAggregate;

namespace Web.Controllers
{
    public class SchoolController : AppController
    {
        public IBoardStandardRepository BoardStandardRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IBoardRepository BoardRepository;
        public IStreamRepository StreamRepository;
        public IStandardRepository StandardRepository;
        public IChapterRepository ChapterRepository;
        public IConceptRepository conceptRepository;
        public ISubjectRepository SubjectRepository;
        public CommonSchoolModel CommonSchoolModel;
        public IAcademicSessionRepository academicSessionRepository;
        private IHttpContextAccessor _httpContextAccessor;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IOrganizationRepository organizationRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IAcademicSubjectRepository academicSubjectRepository;
        public ISyallbusRepository syallbusRepository;
        public IAcademicChapterRepository academicChapterRepository;
        // public IBoardStandardStreamRepository boardStandardStreamRepository;
        public IOrganizationTypeRepository organizationTypeRepository;
        public CommonMethods commonMethods;
        public ISectionRepository sectionRepository;
        public IAcademicSectionRepository academicSectionRepository;
        //  public IAcademicStandardStreamRepository academicStandardStreamRepository;
        public IActionAccessRepository actionAccessRepository;

        public ITeacherRepository teacherRepository;
        public IEmployeeRepository employeeRepository;

        public IStudentAggregateRepository studentAggregateRepository;


        public IAcademicTeacherRepository academicTeacherRepository;


        public ICalenderEngagementTypeRepository calenderEngagementTypeRepository;
        public ICalenderEngagementRepository calenderEngagementRepository;
        public IAcademicCalenderRepository academicCalenderRepository;

        public IAcademicConceptRepository academicConceptRepository;
        public IAcademicTeacherSubjectRepository academicTeacherSubjectRepository;
        public IAcademicTeacherSubjectsTimeLineRepository academicTeacherSubjectsTimeLineRepository;

        public commonsqlquery commonsqlquery;

        public IGroupRepository groupRepository;

        public ICategoryRepository categoryRepository;
        public IAcademicTeacherTimeLineRepository academicTeacherTimeLineRepository;
        public ITeacherTimeLineRepository teacherTimeLineRepository;
        public IWingRepository wingRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IAcademicSubjectGroupRepository academicSubjectGroupRepository;
        public IAcademicSubjectOptionalRepository academicSubjectOptionalRepository;
        public ISubjectOptionalRepository subjectOptionalRepository;
        public IAcademicSubjectOptionalDetailsRepository academicSubjectOptionalDetailsRepository;
        public IAcademicStandardSettingsRepository academicStandardSettingsRepository;

        public OdmErp.Web.Areas.Student.Models.StudentMethod studentMethod;
        public OdmErp.Web.Areas.Student.Models.StudentSqlQuery sqlQuery;


        public SchoolController(IAcademicStandardSettingsRepository academicStandardSettingsRepo,IAcademicSubjectOptionalRepository academicSubjectOptionalRepo, OdmErp.Web.Areas.Student.Models.StudentSqlQuery sqlQ, OdmErp.Web.Areas.Student.Models.StudentMethod studentMet, IAcademicSubjectGroupRepository academicSubjectGroupRepo, IWingRepository wingRepo, IBoardStandardWingRepository boardStandardWingRepo, IBoardStandardRepository boardStandardRepository, IBoardRepository boardRepository,
            IStreamRepository streamRepository, IStandardRepository standardRepository, IChapterRepository chapterRepository,
            ISubjectRepository subjectRepository, CommonSchoolModel commonSchoolModel, IHttpContextAccessor _HttpContextAccessor,
            IAcademicSessionRepository academicSessionRepository, IOrganizationAcademicRepository organizationAcademicRepository,
            IOrganizationRepository organizationRepository, IAcademicStandardRepository academicStandardRepository,
            IAcademicSubjectRepository academicSubjectRepository, ISyallbusRepository syallbusRepository,
            IAcademicChapterRepository academicChapterRepository, IAcademicStandardWingRepository academicStandardWingRepo,

            IConceptRepository conceptRepository, IOrganizationTypeRepository organizationTypeRepository, CommonMethods commonMethods,
            ISectionRepository sectionRepository, IAcademicSubjectOptionalDetailsRepository academicSubjectOptionalDetailsRepo, IAcademicSectionRepository academicSectionRepository,

             ICalenderEngagementTypeRepository cet, ICalenderEngagementRepository ce,
             IEmployeeRepository employeeRepository, ITeacherRepository teacherRepository, IAcademicTeacherSubjectRepository academicTeacherSubjectRepository,
             IAcademicTeacherSubjectsTimeLineRepository academicTeacherSubjectsTimeLineRepository, IActionAccessRepository actionAccessRepo,



            IStudentAggregateRepository sar,
            IAcademicCalenderRepository acr,

           commonsqlquery commonsql,

            ICategoryRepository categoryRepository,

            IGroupRepository groupRepository, ISubjectOptionalRepository subjectOptionalRepo,

             IAcademicConceptRepository academicConceptRepository,
             IAcademicTeacherRepository academicTeacherRepository, IAcademicTeacherTimeLineRepository academicTeacherTimeLineRepository, ITeacherTimeLineRepository teacherTimeLineRepository


            )

        {

            academicStandardSettingsRepository = academicStandardSettingsRepo;
            academicSubjectOptionalDetailsRepository = academicSubjectOptionalDetailsRepo;
            academicSubjectOptionalRepository = academicSubjectOptionalRepo;
            subjectOptionalRepository = subjectOptionalRepo;
            sqlQuery = sqlQ;
            studentMethod = studentMet;
            academicSubjectGroupRepository = academicSubjectGroupRepo;
            academicStandardWingRepository = academicStandardWingRepo;
            boardStandardWingRepository = boardStandardWingRepo;
            academicCalenderRepository = acr;
            studentAggregateRepository = sar;
            wingRepository = wingRepo;
            commonsqlquery = commonsql;

            this.calenderEngagementTypeRepository = cet;
            this.calenderEngagementRepository = ce;
            BoardStandardRepository = boardStandardRepository;
            BoardRepository = boardRepository;
            StreamRepository = streamRepository;
            StandardRepository = standardRepository;
            ChapterRepository = chapterRepository;
            SubjectRepository = subjectRepository;
            CommonSchoolModel = commonSchoolModel;
            this.academicSessionRepository = academicSessionRepository;
            _httpContextAccessor = _HttpContextAccessor;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.organizationRepository = organizationRepository;
            this.academicStandardRepository = academicStandardRepository;
            this.academicSubjectRepository = academicSubjectRepository;
            this.syallbusRepository = syallbusRepository;
            this.academicChapterRepository = academicChapterRepository;
            //  this.boardStandardStreamRepository = boardStandardStreamRepository;
            this.conceptRepository = conceptRepository;
            this.organizationTypeRepository = organizationTypeRepository;
            this.commonMethods = commonMethods;
            this.sectionRepository = sectionRepository;
            this.academicSectionRepository = academicSectionRepository;

            //this.academicStandardStreamRepository = academicStandardStreamRepository;
            this.employeeRepository = employeeRepository;
            this.teacherRepository = teacherRepository;
            this.academicTeacherSubjectRepository = academicTeacherSubjectRepository;
            this.academicTeacherSubjectsTimeLineRepository = academicTeacherSubjectsTimeLineRepository;

            this.categoryRepository = categoryRepository;
            //  this.academicStandardStreamRepository = academicStandardStreamRepository;
            this.academicConceptRepository = academicConceptRepository;
            this.academicTeacherSubjectRepository = academicTeacherSubjectRepository;
            this.teacherRepository = teacherRepository;
            this.employeeRepository = employeeRepository;
            this.academicTeacherRepository = academicTeacherRepository;
            this.groupRepository = groupRepository;
            actionAccessRepository = actionAccessRepo;
            this.academicTeacherTimeLineRepository = academicTeacherTimeLineRepository;
            this.teacherTimeLineRepository = teacherTimeLineRepository;
        }



        #region BoardStandard
        public async Task<IActionResult> BoardStandard(long BoardID)
        {
            ViewBag.BoradId = BoardID;
            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardID).Select(a => a.Name).FirstOrDefault();
            var subjects = SubjectRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var groups = CommonSchoolModel.GetAllBoardStandard().Where(a => a.BoardID == BoardID);
            var res = (from a in groups.ToList()
                       select new NameStreamCountViewModel
                       {
                           ID = a.ID,
                           BoardName = a.BoardName,
                           BoardID = a.BoardID,
                           StandardName = a.StandardName,
                           ModifiedDate = a.ModifiedDate,
                           ModifiedText = NullableDateTimeToStringDate(a.ModifiedDate),
                           StreamType = StandardRepository.GetStandardById(a.StandardID).StreamType,
                           StreamCount = boardStandardWingRepository.ListAllAsync().Result.Where(m => m.BoardStandardID == a.ID).ToList().Count(),
                           SubjectCount = subjects.Where(m => m.BoardStandardId == a.ID).ToList().Count(),
                           Status = a.Status,

                           Active = a.Active
                       }).ToList();

            return View(res);
        }

        [ActionName("BoardStandardByID")]
        public async Task<IActionResult> BoardStandardByID(long boardStandardID)
        {
            var res = CommonSchoolModel.GetAllBoardStandard().Where(a => a.ID == boardStandardID);

            return View("BoardStandardByID", res);
            // return View();
        }

        public IActionResult CreateBoardStandard(long BoardID)
        {
            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardID).Select(a => a.Name).FirstOrDefault();

            BoardStandard brd = new BoardStandard();
            brd.BoardId = BoardID;
            ViewData["BoradId"] = BoardID;
            ViewData["Standards"] = new SelectList(StandardRepository.GetAllStandard(), "ID", "Name");
            //ViewData["Streams"] = new SelectList(StreamRepository.GetAllStream(), "ID", "Name");

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateBoardStandard(long[] StandardID, long BoardID, long StreamID)
        {
            BoardStandardViewModel boardStandard = new BoardStandardViewModel();
            boardStandard.BoardID = BoardID;
            boardStandard.StreamID = StreamID;
            boardStandard.StandardId = StandardID;
            ArrayList myList = new ArrayList(StandardID);
            if (ModelState.IsValid)
            {
                foreach (long str in myList)
                {
                    BoardStandard board = new BoardStandard()
                    {
                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = "ACTIVE",
                        IsAvailable = false,
                        BoardId = boardStandard.BoardID,
                        StandardId = str,
                        //StreamId = boardStandard.StreamID
                    };

                    await BoardStandardRepository.AddAsync(board);

                }
            }

            return RedirectToAction(nameof(BoardStandard), new { boardStandard.BoardID }).WithSuccess("Success", "Saved Successfully");
        }


        public IActionResult EditBoardStandard(long? id)
        {

            //ViewData["BoradId"] = BoardId;            

            ViewData["Boards"] = new SelectList(BoardRepository.GetAllBoard(), "ID", "Name");
            ViewData["Standards"] = new SelectList(StandardRepository.GetAllStandard(), "ID", "Name");
            //ViewData["Streams"] = new[] { new SelectListItem { Text = "Select Stream", Value = "0" } }.Concat(new SelectList(StreamRepository.GetAllStream(), "ID", "Name"));
            GetStatusList();

            var res = CommonSchoolModel.GetAllBoardStandard().FirstOrDefault(a => a.ID == id);
            return View(res);

            // var res = BoardStandardRepository.GetByIdAsyncIncludeAll(id.Value);
            // return View(res);       

        }
        [HttpPost]
        public async Task<IActionResult> EditBoardStandard([Bind("ID,BoardID,StreamID,StandardID,Status")] BoardStandardViewModel boardStandard)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {

                BoardStandard board = new BoardStandard()
                {
                    ID = boardStandard.ID,
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = boardStandard.Status,
                    IsAvailable = false,
                    BoardId = boardStandard.BoardID,
                    StandardId = boardStandard.StandardID,
                    //StreamId = boardStandard.StreamID
                };

                await BoardStandardRepository.UpdateAsync(board);
            }

            return RedirectToAction("BoardStandard", "School", new { boardStandard.BoardID }).WithSuccess("Update", "Updated Successfully");
        }
        #endregion

        #region Subject

        public IActionResult Subject(long BoardId, long BoardStnrdId, string Class)
        {
            ViewData["BoardId"] = BoardId;
            ViewData["BoardStandardId"] = BoardStnrdId;
            //ViewData["BoardStaandStreamId"] = boardStaandStream;

            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
            var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStnrdId).Select(a => a.StandardId).FirstOrDefault();
            ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid).Select(a => a.Name).FirstOrDefault();

            ViewData["StreamClass"] = Class;
            //if (boardStaandStream != 0)
            //{
            //var streamid = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == boardStaandStream).Select(a => a.StreamId).FirstOrDefault();
            //ViewBag.SteamName = StreamRepository.GetAllStream().Where(a => a.ID == streamid).Select(a => a.Name).FirstOrDefault();

            //    var res = CommonSchoolModel.GetAllStreamSubjects().Where(a => a.BoardStandardStreamId == boardStaandStream);
            //    return View(res);
            //}
            //else
            //{
            var res = CommonSchoolModel.GetAllSubjects().Where(a => a.BoardStandardId == BoardStnrdId);
            return View(res);
            //}
        }


        public IActionResult CreateSubject(long BoardStandardID, long BoardId, long BoardStaandStreamId)
        {
            ViewData["BoardId"] = BoardId;
            ViewData["BoardStandardId"] = BoardStandardID;
            //ViewData["BoardStaandStreamId"] = BoardStaandStreamId;
            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
            var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardID).Select(a => a.StandardId).FirstOrDefault();
            ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid).Select(a => a.Name).FirstOrDefault();


            //if (BoardStaandStreamId != 0)
            //{
            //    ViewData["Subject"] = "StreamClassSubject";
            //    var streamid = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStaandStreamId).Select(a => a.StreamId).FirstOrDefault();
            //    ViewBag.SteamName = StreamRepository.GetAllStream().Where(a => a.ID == streamid).Select(a => a.Name).FirstOrDefault();

            //}
            //else
            //{
            ViewData["Subject"] = "ClassSubject";
            //}
            //if (BoardStaandStreamId != 0)
            //{
            //    ViewData["ParentSubject"] = new[] { new SelectListItem { Text = "Select Parent Subject", Value = "0" } }.Concat(new SelectList(SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardStandardStreamId == BoardStaandStreamId), "ID", "Name"));
            //}
            //else
            //{
            ViewData["ParentSubject"] = new[] { new SelectListItem { Text = "Select Parent Subject", Value = "0" } }.Concat(new SelectList(SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardStandardId == BoardStandardID), "ID", "Name"));
            //}



            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateSubject(long BoardStandardId, long BoardId, string Name, string SubjectCode, string Subject, long ParentSubjectId, bool IsOptional)
        {
            CheckLoginStatus();
            ViewBag.StreamClass = Subject;
            SubjectViewModel subjectViewModel = new SubjectViewModel();
            subjectViewModel.Name = Name;
            subjectViewModel.BoardId = BoardId;
            subjectViewModel.BoardStandardId = BoardStandardId;
            //subjectViewModel.BoardStandardStreamId = BoardStandardStreamId;
            subjectViewModel.SubjectCode = SubjectCode;
            subjectViewModel.ParentSubjectId = ParentSubjectId;
            subjectViewModel.IsOptional = IsOptional;

            if (subjectViewModel.ParentSubjectId > 0)
            {
                subjectViewModel.IsSubSubject = true;
            }
            else
            {
                subjectViewModel.IsSubSubject = false;
            }


            if (ModelState.IsValid)
            {
                Subject subject = new Subject()
                {
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = EntityStatus.ACTIVE,
                    IsAvailable = true,
                    Name = subjectViewModel.Name,
                    SubjectCode = subjectViewModel.SubjectCode,
                    ParentSubjectId = subjectViewModel.ParentSubjectId,
                    IsSubSubject = subjectViewModel.IsSubSubject,
                    BoardStandardId = subjectViewModel.BoardStandardId,
                    //BoardStandardStreamId = subjectViewModel.BoardStandardStreamId,
                    IsOptional = subjectViewModel.IsOptional

                };

                await SubjectRepository.AddAsync(subject);
            }
            var routeValues = new RouteValueDictionary { { "BoardStnrdId", subjectViewModel.BoardStandardId }, { "BoardId", subjectViewModel.BoardId }, { "Class", Subject } };

            return RedirectToAction("Subject", "School", routeValues).WithSuccess("Success", "Saved Successfully");

        }

        public IActionResult EditSubject(long BoardStandardID, long BoardId, long BoardStandardStreamId, long? id)
        {
            ViewData["BoardId"] = BoardId;
            ViewData["BoardStandardId"] = BoardStandardID;
            //ViewData["BoardStaandStreamId"] = BoardStandardStreamId;
            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
            var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardID).Select(a => a.StandardId).FirstOrDefault();
            ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid).Select(a => a.Name).FirstOrDefault();

            //if (BoardStandardStreamId != 0)
            //{
            //    ViewData["Subject"] = "StreamClassSubject";
            //    var streamid = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardStreamId).Select(a => a.StreamId).FirstOrDefault();
            //    ViewBag.SteamName = StreamRepository.GetAllStream().Where(a => a.ID == streamid).Select(a => a.Name).FirstOrDefault();

            //}
            //else
            //{
            ViewData["Subject"] = "ClassSubject";
            //}
            //if (BoardStandardStreamId != 0)
            //{
            //    ViewData["ParentSubject"] = new[] { new SelectListItem { Text = "Select Parent Subject", Value = "0" } }.Concat(new SelectList(SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardStandardStreamId == BoardStandardStreamId), "ID", "Name"));
            //}
            //else
            //{
            ViewData["ParentSubject"] = new[] { new SelectListItem { Text = "Select Parent Subject", Value = "0" } }.Concat(new SelectList(SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardStandardId == BoardStandardID), "ID", "Name"));
            //}
            GetStatusList();
            var res = SubjectRepository.GetByIdAsyncIncludeAll(id.Value).Result;
            SubjectViewModel subject = new SubjectViewModel();
            subject.ID = res.ID;
            subject.Name = res.Name;
            subject.SubjectCode = res.SubjectCode;
            subject.BoardStandardId = BoardStandardID;
            subject.BoardId = BoardId;
            //subject.BoardStandardStreamId = BoardStandardStreamId;
            subject.IsSubSubject = res.IsSubSubject;
            subject.ParentSubjectId = res.ParentSubjectId;
            subject.IsOptional = res.IsOptional;

            return View(subject);
        }

        [HttpPost]
        public async Task<IActionResult> EditSubject(long BoardStandardId, long BoardId, long id, string Name, string SubjectCode, string Status, string Subject, long ParentSubjectId, bool IsOptional)
        {
            CheckLoginStatus();
            GetStatusList();
            SubjectViewModel subjectViewModel = new SubjectViewModel();
            subjectViewModel.ID = id;
            subjectViewModel.Name = Name;
            subjectViewModel.SubjectCode = SubjectCode;
            subjectViewModel.BoardStandardId = BoardStandardId;
            subjectViewModel.BoardId = BoardId;
            subjectViewModel.Status = Status;
            //subjectViewModel.BoardStandardStreamId = BoardStandardStreamId;
            subjectViewModel.ParentSubjectId = ParentSubjectId;
            subjectViewModel.IsOptional = IsOptional;
            if (subjectViewModel.ParentSubjectId > 0)
            {
                subjectViewModel.IsSubSubject = true;
            }
            else
            {
                subjectViewModel.IsSubSubject = false;
            }

            if (ModelState.IsValid)
            {
                Subject subject = new Subject()
                {
                    ID = id,
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = subjectViewModel.Status,
                    IsAvailable = true,
                    ParentSubjectId = subjectViewModel.ParentSubjectId,
                    IsSubSubject = subjectViewModel.IsSubSubject,
                    Name = subjectViewModel.Name,
                    SubjectCode = subjectViewModel.SubjectCode,
                    BoardStandardId = subjectViewModel.BoardStandardId,
                    //BoardStandardStreamId = subjectViewModel.BoardStandardStreamId,
                    IsOptional = subjectViewModel.IsOptional,
                };

                await SubjectRepository.UpdateAsync(subject);
            }
            var routeValues = new RouteValueDictionary { { "BoardStnrdId", subjectViewModel.BoardStandardId }, { "BoardId", subjectViewModel.BoardId }, { "Class", Subject } };

            return RedirectToAction("Subject", "School", routeValues).WithSuccess("Update", "Updated Successfully");


        }


        #endregion    

        #region Academic Session

        public IActionResult CreateAcademicSession()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAcademicSession([Bind("DisplayName,IsAvailable,IsAdmission")] AcademicSession acasession, long? id)
        {
            CheckLoginStatus();

            if (acasession.IsAvailable == true || acasession.IsAdmission == true)
            {
                var res = academicSessionRepository.ListAllAsyncIncludeAll().Result;
                if (res != null)
                {
                    for (int i = 0; i < res.ToList().Count(); i++)
                    {
                        AcademicSession asm = res.ToList()[i];
                        asm.ModifiedId = userId;
                        asm.ModifiedDate = DateTime.Now;
                        if (acasession.IsAvailable == true)
                        {
                            asm.IsAvailable = false;
                        }
                        if (acasession.IsAdmission == true)
                        {
                            asm.IsAdmission = false;
                        }
                        await academicSessionRepository.UpdateAsync(asm);
                    }
                }

            }
            acasession.InsertedId = userId;
            acasession.ModifiedId = userId;
            acasession.InsertedDate = DateTime.Now;
            acasession.ModifiedDate = DateTime.Now;
            acasession.Active = true;
            acasession.Status = EntityStatus.ACTIVE;
            acasession.IsAvailable = acasession.IsAvailable;
            acasession.IsAdmission = acasession.IsAdmission;

            await academicSessionRepository.AddAsync(acasession);
            return RedirectToAction(nameof(AcademicSession)).WithSuccess("Success", "Saved Successfully");
        }
        public async Task<IActionResult> AcademicSession()
        {
            var academicSessionsList = await academicSessionRepository.ListAllAsyncIncludeAll();

            var res = (from a in academicSessionsList
                       select new NameAcademicSubjectCountViewModel
                       {
                           ID = a.ID,
                           InsertedDate = a.InsertedDate,
                           ModifiedDate = a.ModifiedDate,
                           Active = a.Active,
                           IsAvailable = a.IsAvailable,
                           Status = a.Status,
                           AcademicSubjectCount = academicSessionRepository.GetAcademicSubjectCountBySessionId(a.ID),
                           OrganizationAcademicCount = organizationAcademicRepository.GetOrganizationAcademicCountBySessionId(a.ID),
                           Name = a.DisplayName,
                           IsAdmission = a.IsAdmission
                       });
            return View(res);
        }
        public async Task<IActionResult> EditAcademicSession(long? id)  //Get Academic Session
        {
            var result = academicSessionRepository.GetByIdAsyncIncludeAll(id.Value);

            if (result == null)
            {
                return NotFound();
            }

            return View(await result);
        }

        [ActionName("EditAcademicSession")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAcademicSession([Bind("ID,IsAvailable,DisplayName,IsAdmission")] AcademicSession acasession)  //Edit Academic Session
        {
            CheckLoginStatus();

            if (acasession.IsAvailable == true)
            {
                var res = academicSessionRepository.ListAllAsyncIncludeAllNoTrack().Result.Where(a => a.IsAvailable == true || a.IsAdmission == true && a.ID != acasession.ID);
                if (res != null)
                {
                    for (int i = 0; i < res.ToList().Count(); i++)
                    {
                        AcademicSession asm = res.ToList()[i];
                        asm.ModifiedId = userId;
                        asm.ModifiedDate = DateTime.Now;
                        if (acasession.IsAvailable == true)
                        {
                            asm.IsAvailable = false;
                        }
                        if(acasession.IsAdmission == true)
                        {
                            asm.IsAdmission = false;
                        }
                        await academicSessionRepository.UpdateAsync(asm);
                    }
                }

            }

            AcademicSession asm2 = new AcademicSession();
            asm2.ID = acasession.ID;
            asm2.DisplayName = acasession.DisplayName;
            asm2.InsertedId = userId;
            asm2.ModifiedId = userId;
            asm2.InsertedDate = DateTime.Now;
            asm2.ModifiedDate = DateTime.Now;
            asm2.Active = true;
            asm2.Status = EntityStatus.ACTIVE;
            asm2.IsAvailable = acasession.IsAvailable;
            asm2.IsAdmission = acasession.IsAdmission;
            await academicSessionRepository.UpdateAsync(asm2);
            return RedirectToAction(nameof(AcademicSession)).WithSuccess("Update", "Updated Successfully");
        }

        public IActionResult DownloadAcademicSession()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            var comlumHeadrs = new string[]
           {
                //"Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Academic Session"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;

                foreach (var session in academicSessionRepository.ListAllAsyncIncludeAll().Result)
                {
                    //worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = session.DisplayName;
                    //worksheet.Cells["C" + j].Value = country.InsertedDate.ToString("dd/MM/yyyy");
                    //worksheet.Cells["D" + j].Value = country.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"AcademicSession.xlsx");
        }


        public IActionResult downloadAcademicSessionSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
                               // userId = HttpContext.Session.GetInt32("userId").Value;

            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Academic Session"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Academic Session.xlsx");
        }
        public IActionResult UploadAcademicSession(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;


            if (file != null)
            {
                // string path1 = file.FileName;
                byte[] filestr;
                System.IO.Stream stream = file.OpenReadStream();

                using (XLWorkbook workBook = new XLWorkbook(stream))
                {
                    //Read the first Sheet from Excel file.
                    IXLWorksheet workSheet = workBook.Worksheet(1);
                    var allsession = academicSessionRepository.ListAllAsyncIncludeAll().Result;
                    foreach (IXLRow row in workSheet.Rows().Skip(1))
                    {
                        AcademicSession acsession = new AcademicSession();
                        if (row.Cell(1).Value != null && row.Cell(1).Value.ToString() != "")
                        {
                            string countryname = row.Cell(1).Value.ToString();
                            if (allsession.Where(a => a.DisplayName == countryname).FirstOrDefault() == null)
                            {
                                acsession.DisplayName = countryname;
                                acsession.ModifiedId = userId;
                                acsession.Active = true;
                                acsession.InsertedId = userId;
                                acsession.Status = "CREATED";
                                acsession.ModifiedDate = DateTime.Now;
                                acsession.InsertedDate = DateTime.Now;
                                academicSessionRepository.AddAsync(acsession);

                            }
                        }
                    }
                    workBook.Dispose();
                }
                _httpContextAccessor.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

            }

            return RedirectToAction(nameof(AcademicSession)).WithSuccess("success", "Excel uploaded successfully");
        }
        #endregion

        #region  academic standard

        public async Task<IActionResult> AcademicStandard()
        {
            var res = commonsqlquery.Get_view_All_Academic_Standards();
            return View(res);
        }
        public IActionResult AccessAcademicStandard(long standardid, long streamid)
        {
            return View(commonMethods.GetModuleSubModuleActionAcademicAccess(standardid, streamid));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateAcademicModuleSubModuleDetails(EmployeeModuleSubModuleAccess employeeModuleSubModuleAccess)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                foreach (var a in employeeModuleSubModuleAccess.modules)
                {
                    if (a.subModules != null)
                    {
                        foreach (var b in a.subModules)
                        {
                            if (b.actionSubModules != null)
                            {
                                foreach (var c in b.actionSubModules)
                                {
                                    if (c.IsRequired == true && c.ActionAccessId == 0)
                                    {
                                        ActionAcademic action = new ActionAcademic();
                                        action.ActionID = c.ID;
                                        action.Active = true;
                                        action.InsertedId = userId;
                                        action.ModifiedId = userId;
                                        action.ModuleID = a.ID;
                                        action.Status = EntityStatus.ACTIVE;
                                        action.SubModuleID = b.ID;
                                        action.AcademicStandardID = employeeModuleSubModuleAccess.standardid;
                                        action.AcademicStandardStreamID = employeeModuleSubModuleAccess.standardstreamid;
                                        actionAccessRepository.CreateActionAcademic(action);
                                    }
                                    else if (c.ActionAccessId != 0)
                                    {
                                        ActionAcademic actionAccess = actionAccessRepository.GetActionAcademicById(c.ActionAccessId);
                                        if (actionAccess != null)
                                        {
                                            if (c.IsRequired == false)
                                            {
                                                actionAccessRepository.DeleteActionAcademic(actionAccess.ID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return RedirectToAction(nameof(AccessAcademicStandard), new { standardid = employeeModuleSubModuleAccess.standardid, streamid = employeeModuleSubModuleAccess.standardstreamid }).WithSuccess("success", "Saved successfully"); ;
            }
            return RedirectToAction(nameof(AccessAcademicStandard), new { standardid = employeeModuleSubModuleAccess.standardid, streamid = employeeModuleSubModuleAccess.standardstreamid }).WithSuccess("success", "Saved successfully"); ;
        }

        public IActionResult CreateAcademicStandard()
        {
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = " " } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result, "ID", "DisplayName"));
            ViewData["BoardName"] = new[] { new SelectListItem { Text = "Select Board", Value = " " } }.Concat(new SelectList(BoardRepository.GetAllBoard(), "ID", "Name"));


            return View();
        }
        [HttpPost]
        [ActionName("CreateAcademicStandard")]
        public async Task<IActionResult> CreateAcademicStandard(long OraganisationAcademicId, long[] MultiBoardStandardId,bool Nucleus40)
        {
            CheckLoginStatus();
            AcademicStandardViewModel standardviewmodel = new AcademicStandardViewModel();
            standardviewmodel.OraganisationAcademicId = OraganisationAcademicId;

            ArrayList myList = new ArrayList(MultiBoardStandardId);
            foreach (long str in myList)
            {
                AcademicStandard academicstandard = new AcademicStandard()
                {
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = EntityStatus.ACTIVE,
                    IsAvailable = true,
                    OrganizationAcademicId = standardviewmodel.OraganisationAcademicId,
                    BoardStandardId = str,
                    //SchoolWingId = standardviewmodel.SchoolWingId

                };
                var id= academicStandardRepository.AddAsync(academicstandard).Result.ID;
                AcademicStandardSettings ov = new AcademicStandardSettings();
                ov.AcademicStandardId = id;
                ov.Active = true;
                ov.InsertedDate = DateTime.Now;
                ov.ModifiedDate = DateTime.Now;
                ov.InsertedId = userId;
                ov.ModifiedId = userId;
                ov.Nucleus = Nucleus40;
                ov.Status = EntityStatus.ACTIVE;
                await academicStandardSettingsRepository.AddAsync(ov);

            }
            return RedirectToAction(nameof(AcademicStandard)).WithSuccess("Sucess", "Saved Sucessfully");
        }

        public IActionResult EditAcademicStandard(long? id)
        {
            GetStatusList();
            var schoolwingcls = CommonSchoolModel.GetAllAcademicStandard().Where(a => a.ID == id.Value).FirstOrDefault();
            AcademicStandardViewModel standardviewmodel = new AcademicStandardViewModel();
            standardviewmodel.ID = schoolwingcls.ID;
            standardviewmodel.BoardStandardId = schoolwingcls.BoardStandardId;
            standardviewmodel.OraganisationAcademicId = schoolwingcls.OraganisationAcademicId;
            standardviewmodel.StandardName = schoolwingcls.StandardName;
            standardviewmodel.OraganisationName = schoolwingcls.OraganisationName;
            standardviewmodel.Status = schoolwingcls.Status;
            standardviewmodel.Nucleus40 = academicStandardSettingsRepository.ListAllAsync().Result==null?false: academicStandardSettingsRepository.ListAllAsync().Result.Where(a => a.AcademicStandardId == standardviewmodel.ID && a.Active == true).FirstOrDefault()==null?false: academicStandardSettingsRepository.ListAllAsync().Result.Where(a => a.AcademicStandardId == standardviewmodel.ID && a.Active == true).FirstOrDefault().Nucleus.Value;

            return View(standardviewmodel);
        }
        [HttpPost]
        public IActionResult EditAcademicStandard(long ID, string Status, long OraganisationAcademicId, long BoardStandardId,bool Nucleus40)
        {
            CheckLoginStatus();
            AcademicStandardViewModel standardviewmodel = new AcademicStandardViewModel();
            standardviewmodel.Status = Status;
            standardviewmodel.ID = ID;
            standardviewmodel.BoardStandardId = BoardStandardId;
            standardviewmodel.OraganisationAcademicId = OraganisationAcademicId;
            //standardviewmodel.SchoolWingId = SchoolWingId;


            AcademicStandard academicstandard = new AcademicStandard()
            {
                ID = standardviewmodel.ID,
                InsertedId = userId,
                ModifiedId = userId,
                InsertedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                Active = true,
                Status = standardviewmodel.Status,
                IsAvailable = true,
                BoardStandardId = standardviewmodel.BoardStandardId,
                OrganizationAcademicId = standardviewmodel.OraganisationAcademicId,
                //SchoolWingId = standardviewmodel.SchoolWingId,
            };
            academicStandardRepository.UpdateAsync(academicstandard);
            AcademicStandardSettings ov = academicStandardSettingsRepository.ListAllAsync().Result==null?null: academicStandardSettingsRepository.ListAllAsync().Result.Where(a=>a.AcademicStandardId== ID && a.Active==true).FirstOrDefault();
            if (ov != null)
            {
                ov.AcademicStandardId = standardviewmodel.ID;
                ov.Active = true;
                ov.ModifiedDate = DateTime.Now;
                ov.ModifiedId = userId;
                ov.Nucleus = Nucleus40;
                ov.Status = EntityStatus.ACTIVE;
                academicStandardSettingsRepository.UpdateAsync(ov);
            }
            else
            {
                ov = new AcademicStandardSettings();
                ov.AcademicStandardId = standardviewmodel.ID;
                ov.Active = true;
                ov.InsertedDate = DateTime.Now;
                ov.ModifiedDate = DateTime.Now;
                ov.InsertedId = userId;
                ov.ModifiedId = userId;
                ov.Nucleus = Nucleus40;
                ov.Status = EntityStatus.ACTIVE;
                academicStandardSettingsRepository.AddAsync(ov);
            }
            return RedirectToAction(nameof(AcademicStandard)).WithSuccess("Sucess", "Updated Sucessfully");
        }





        public JsonResult GetOrganisationByAcademicSession(long AcademicSessionId)                 //Filter the selected Standard
        {
            try
            {
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == AcademicSessionId);
                var OrganisationList = organizationRepository.GetAllOrganization();
                var academicstandard = academicStandardRepository.ListAllAsyncIncludeAll().Result;

                var res = (from a in OrganisationAcademic
                           join b in OrganisationList on a.OrganizationId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }
                          ).ToList();



                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        //public JsonResult GetWingBySchoolWing(long OraganisationAcademicId)                 //Filter the selected Standard
        //{
        //    try
        //    {
        //        var SchoolWing = commonMethods.GetSchoolWing().Where(a => a.OrganizationAcademicID == OraganisationAcademicId);

        //        var wingList = commonMethods.GetWing();

        //        var res = (from a in SchoolWing
        //                   join b in wingList on a.WingID equals b.ID
        //                   select new
        //                   {
        //                       id = a.ID,
        //                       name = b.Name
        //                   }
        //                  ).ToList();



        //        return Json(res);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}



        public JsonResult GetClassByBoard(long BoardId, long OraganisationAcademicId)                 //Filter the selected Standard
        {
            try
            {
                var academicstandard = academicStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.OrganizationAcademicId == OraganisationAcademicId && a.Active == true).Select(a => a.BoardStandardId);
                var BoardStandardList = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardId == BoardId && !academicstandard.Contains(a.ID));
                var StandardList = StandardRepository.GetAllStandard();


                var res = (from a in BoardStandardList
                           join b in StandardList on a.StandardId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }
                          ).ToList();



                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }




        #endregion
        #region Academic Standard Wing

        public IActionResult AcademicStandardWing(long AcademicStandardId)
        {
            ViewBag.AcademicStandardId = AcademicStandardId;
            var res = CommonSchoolModel.GetacademicstandardWings(AcademicStandardId).OrderByDescending(a => a.ID).ToList();
            return View(res);
        }
        public IActionResult CreateOrEditAcademicStandardWing(long AcademicStandardId)
        {
            ViewData["AcademicStandardId"] = AcademicStandardId;
            GetStatusList();
            var academicStandand = academicStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicStandardId).FirstOrDefault();
            var boardstandardwing = CommonSchoolModel.GetWingsListByAcademicStandardId(AcademicStandardId, academicStandand.BoardStandardId);
            AcademicStandardWingcls AcademicStreammodel = new AcademicStandardWingcls();
            AcademicStreammodel.AcademicStandardID = AcademicStandardId;
            AcademicStreammodel.ID = 0;
            AcademicStreammodel.wings = boardstandardwing;
            ViewBag.Status = "Create";


            return View(AcademicStreammodel);

        }
        [HttpPost]

        public async Task<IActionResult> CreateOrEditAcademicStandardWing(long AcademicStandardID, long[] wingids)
        {
            CheckLoginStatus();

            var routeValues = new RouteValueDictionary { { "AcademicStandardId", AcademicStandardID } };

            ArrayList myList = new ArrayList(wingids);
            foreach (long str in myList)
            {
                var chk = academicStandardWingRepository.ListAllAsync().Result.Where(a => a.Active == true && a.BoardStandardWingID == str && a.AcademicStandardId == AcademicStandardID).FirstOrDefault();


                var wingId =  boardStandardWingRepository.GetByIdAsync(str).Result.WingID;

                if (chk == null)
                {
                    AcademicStandardWing academicstream = new AcademicStandardWing()
                    {
                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = EntityStatus.ACTIVE,
                        IsAvailable = true,
                        BoardStandardWingID = str,
                        WingID = wingId,
                        AcademicStandardId = AcademicStandardID,
                    };
                    await academicStandardWingRepository.AddAsync(academicstream);
                }

            }
            return RedirectToAction(nameof(AcademicStandardWing), routeValues).WithSuccess("Sucess", "Saved Sucessfully");

        }
        public IActionResult DeleteAcademicStandardWing(long AcademicStandardId, long ID)
        {
            CheckLoginStatus();
            AcademicStandardWing res = academicStandardWingRepository.GetByIdAsync(ID).Result;
            res.Active = false;
            res.ModifiedDate = DateTime.Now;
            res.ModifiedId = userId;
            res.Status = EntityStatus.INACTIVE;
            academicStandardWingRepository.UpdateAsync(res);
            return RedirectToAction(nameof(AcademicStandardWing), new { AcademicStandardId = AcademicStandardId });
        }

        #endregion
        #region Academic Subject Group

        public IActionResult AcademicSubjectGroup(long AcademicStandardId)
        {
            ViewBag.AcademicStandardId = AcademicStandardId;
            var academicsubjectgroup = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.AcademicStandardID == AcademicStandardId && a.Active == true).ToList();
            var subjectgroups = studentMethod.GetSubjectGroupList();
            var res = (from a in academicsubjectgroup
                       join b in subjectgroups on a.SubjectGroupID equals b.SubjectGroupID
                       select new academicsubjectgroupcls
                       {
                           ID = a.ID,
                           AcademicStandardID = a.AcademicStandardID,
                           ModifiedDate = a.ModifiedDate,
                           SubjectGroupID = a.SubjectGroupID,
                           SubjectGroup = b
                       }).ToList();
            return View(res);
        }
        public IActionResult CreateOrEditAcademicSubjectGroup(long AcademicStandardId)
        {
            ViewData["AcademicStandardId"] = AcademicStandardId;
            GetStatusList();
            var academicStandand = academicStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicStandardId).FirstOrDefault();
            var academicsubjectgroup = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStandardID == AcademicStandardId).ToList();
            var subjectgroups = studentMethod.GetSubjectGroupList().Where(a => a.BoardStandardId == academicStandand.BoardStandardId).ToList();
            var subjectgroupids = subjectgroups.Select(a => a.SubjectGroupID).Except(academicsubjectgroup.Select(a => a.SubjectGroupID)).ToList();
            var res = subjectgroups.Where(a => subjectgroupids.Contains(a.SubjectGroupID)).Select(a => new CommonMasterModel { ID = a.SubjectGroupID, Name = a.SubjectGroupName }).ToList();


            var boardstandardwing = CommonSchoolModel.GetWingsListByAcademicStandardId(AcademicStandardId, academicStandand.BoardStandardId);
            AcademicStandardWingcls AcademicStreammodel = new AcademicStandardWingcls();
            AcademicStreammodel.AcademicStandardID = AcademicStandardId;
            AcademicStreammodel.ID = 0;
            AcademicStreammodel.wings = res;
            ViewBag.Status = "Create";


            return View(AcademicStreammodel);

        }
        [HttpPost]

        public async Task<IActionResult> CreateOrEditAcademicSubjectGroup(long AcademicStandardID, long[] wingids)
        {
            CheckLoginStatus();

            var routeValues = new RouteValueDictionary { { "AcademicStandardId", AcademicStandardID } };

            ArrayList myList = new ArrayList(wingids);
            foreach (long str in myList)
            {
                var chk = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.Active == true && a.SubjectGroupID == str && a.AcademicStandardID == AcademicStandardID).FirstOrDefault();
                if (chk == null)
                {
                    AcademicSubjectGroup academicstream = new AcademicSubjectGroup()
                    {
                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = EntityStatus.ACTIVE,
                        IsAvailable = true,
                        SubjectGroupID = str,
                        AcademicStandardID = AcademicStandardID,
                    };
                    await academicSubjectGroupRepository.AddAsync(academicstream);
                }

            }
            return RedirectToAction(nameof(AcademicSubjectGroup), routeValues).WithSuccess("Sucess", "Saved Sucessfully");

        }
        public IActionResult DeleteAcademicSubjectGroup(long AcademicStandardId, long ID)
        {
            CheckLoginStatus();
            AcademicSubjectGroup res = academicSubjectGroupRepository.GetByIdAsync(ID).Result;
            res.Active = false;
            res.ModifiedDate = DateTime.Now;
            res.ModifiedId = userId;
            res.Status = EntityStatus.INACTIVE;
            academicSubjectGroupRepository.UpdateAsync(res);
            return RedirectToAction(nameof(AcademicSubjectGroup), new { AcademicStandardId = AcademicStandardId });
        }

        #endregion
        #region  Subject Optional

        public async Task<IActionResult> AcademicSubjectOptional()
        {
            var academicsubjectoptional = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var SubjectOptionalCategorys = sqlQuery.Get_All_Subject_Optional_Categories().ToList();
            var subjects = studentMethod.Getsubjects();
            var subjectoptional = subjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var allsubjects = SubjectRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var res = (from a in academicsubjectoptional
                       join c in SubjectOptionalCategorys on a.SubjectOptionalCategoryID equals c.SubjectOptionalCategoryID
                       select new academicsubjectoptionalcls
                       {
                           ID = a.ID,
                           SubjectOptionalID = a.SubjectOptionalCategoryID,
                           ModifiedDate = a.ModifiedDate,
                           AcademicStandardID = a.AcademicStandardID,
                           AcademicStandardWingID = a.AcademicStandardWingID,
                           AcademicSubjectGroupID = a.AcademicSubjectGroupID,
                           _Subject_Optional_Category = c,

                       }).ToList();

            return View(res);
        }
        public JsonResult GetAcademicStandard(long BoardId, long OraganisationAcademicId, long AcademicSessionId)
        {
            var boardstandard = BoardStandardRepository.ListAllAsync().Result.Where(a => a.BoardId == BoardId && a.Active == true).ToList();
            var academicstandard = commonsqlquery.Get_view_All_Academic_Standards().Where(a => a.AcademicSessionId == AcademicSessionId && a.OraganisationAcademicId == OraganisationAcademicId).ToList();
            var res = (from a in academicstandard
                       join b in boardstandard on a.BoardStandardId equals b.ID
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.StandardName
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetAcademicWing(long AcademicStandardId)
        {
            var academicstandardwing = CommonSchoolModel.GetacademicstandardWings(AcademicStandardId).ToList();

            var res = (from a in academicstandardwing
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetAcademicSubjectGroup(long AcademicStandardId)
        {
            var academicsubjectgroup = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.AcademicStandardID == AcademicStandardId && a.Active == true).ToList();
            var subjectgroups = studentMethod.GetSubjectGroupList();

            var res = (from a in academicsubjectgroup
                       join b in subjectgroups on a.SubjectGroupID equals b.SubjectGroupID
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = b.SubjectGroupName
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetSubjectoptions(long subjectcategoryid)
        {
            var optionlist = studentMethod.GetSubjectOptionalList(subjectcategoryid);
            var res = (from a in optionlist
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.SubjectName
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetAcademicSubjectOption(long AcademicStandardId, long AcademicWingId, long AcademicSubjectGroupId)
        {
            var academicstandard = commonsqlquery.Get_view_All_Academic_Standards().Where(a => a.ID == AcademicStandardId).FirstOrDefault();
            var optioncategory = studentMethod.GetSubjectOptionalCategoryList();

            if (AcademicStandardId != 0)
            {
                optioncategory = optioncategory.Where(a => a.BoardStandardID == academicstandard.BoardStandardId).ToList();
            }
            if (AcademicWingId != 0)
            {
                var academicwing = academicStandardWingRepository.ListAllAsync().Result.Where(a => a.ID == AcademicWingId).FirstOrDefault();
                optioncategory = optioncategory.Where(a => a.WingID == academicwing.BoardStandardWingID).ToList();
            }
            if (AcademicSubjectGroupId != 0)
            {
                var academicsubjectgroup = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.ID == AcademicSubjectGroupId && a.Active == true).FirstOrDefault();
                optioncategory = optioncategory.Where(a => a.SubjectGroupID == academicsubjectgroup.SubjectGroupID).ToList();
            }
            var res = (from a in optioncategory
                       select new CommonMasterModel
                       {
                           ID = a.SubjectOptionalCategoryID,
                           Name = a.SubjectOptionalTypeName
                       }).ToList();
            return Json(res);
        }
        public IActionResult CreateOrEditSubjectOptional(long? id)
        {
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = " " } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true), "ID", "DisplayName"));
            ViewData["BoardName"] = new[] { new SelectListItem { Text = "Select Board", Value = " " } }.Concat(new SelectList(BoardRepository.GetAllBoard(), "ID", "Name"));
            if (id != null)
            {
                var category = academicSubjectOptionalRepository.GetByIdAsync(id.Value).Result;
                var categorydet = academicSubjectOptionalDetailsRepository.ListAllAsync().Result.Where(a => a.Active == true).Select(a => a.SubjectOptionalID).ToList();
                var academicstandard = academicStandardRepository.GetByIdAsync(category.AcademicStandardID.Value).Result;
                var orgacd = organizationAcademicRepository.GetByIdAsync(academicstandard.OrganizationAcademicId).Result;
                var boardid = BoardStandardRepository.GetByIdAsync(academicstandard.BoardStandardId).Result.BoardId;
                ViewBag.catg = category;
                ViewBag.catgdet = categorydet;
                ViewBag.academic = academicstandard;
                ViewBag.org = orgacd;
                ViewBag.boardid = boardid;
            }

            return View();
        }
        [HttpPost]
        [ActionName("CreateAcademicSubjectOptional")]
        public async Task<IActionResult> CreateAcademicSubjectOptional(academicssubjectoptiondetcls ob)
        {
            CheckLoginStatus();
            AcademicSubjectOptional chkacademicsubjectopt = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStandardID == ob.AcademicStandardId && a.AcademicStandardWingID == ob.AcademicSubjectWingId && a.AcademicSubjectGroupID == ob.AcademicSubjectGroupId && a.SubjectOptionalCategoryID == ob.subjectcategoryids).FirstOrDefault();
            if (chkacademicsubjectopt == null)
            {
                AcademicSubjectOptional sopt = new AcademicSubjectOptional();
                sopt.AcademicStandardID = ob.AcademicStandardId;
                sopt.AcademicStandardWingID = ob.AcademicSubjectWingId;
                sopt.AcademicSubjectGroupID = ob.AcademicSubjectGroupId;
                sopt.SubjectOptionalCategoryID = ob.subjectcategoryids;
                sopt.Status = EntityStatus.ACTIVE;

                sopt.Active = true;
                sopt.InsertedDate = DateTime.Now;
                sopt.InsertedId = userId;
                sopt.ModifiedId = userId;
                sopt.ModifiedDate = DateTime.Now;
                sopt.IsAvailable = true;
                long id = academicSubjectOptionalRepository.AddAsync(sopt).Result.ID;

                ArrayList myList = new ArrayList(ob.subjectoptionids);
                foreach (long str in myList)
                {
                    AcademicSubjectOptionalDetails academicstandard = new AcademicSubjectOptionalDetails();

                    academicstandard.InsertedId = userId;
                    academicstandard.ModifiedId = userId;
                    academicstandard.InsertedDate = DateTime.Now;
                    academicstandard.ModifiedDate = DateTime.Now;
                    academicstandard.Active = true;
                    academicstandard.Status = EntityStatus.ACTIVE;
                    academicstandard.IsAvailable = true;
                    academicstandard.AcademicSubjectOptionalID = id;
                    academicstandard.SubjectOptionalID = str;
                    await academicSubjectOptionalDetailsRepository.AddAsync(academicstandard);
                }
                return RedirectToAction(nameof(AcademicSubjectOptional)).WithSuccess("Sucess", "Saved Sucessfully");
            }
            else
            {
                chkacademicsubjectopt.AcademicStandardID = ob.AcademicStandardId;
                chkacademicsubjectopt.AcademicStandardWingID = ob.AcademicSubjectWingId;
                chkacademicsubjectopt.AcademicSubjectGroupID = ob.AcademicSubjectGroupId;
                chkacademicsubjectopt.Active = true;
                chkacademicsubjectopt.ModifiedId = userId;
                chkacademicsubjectopt.ModifiedDate = DateTime.Now;
                chkacademicsubjectopt.IsAvailable = true;
                await academicSubjectOptionalRepository.UpdateAsync(chkacademicsubjectopt);
                long id = chkacademicsubjectopt.ID;
                ArrayList myList = new ArrayList(ob.subjectoptionids);
                foreach (long str in myList)
                {
                    AcademicSubjectOptionalDetails academicstandard = academicSubjectOptionalDetailsRepository.ListAllAsync().Result.Where(a => a.AcademicSubjectOptionalID == id && a.SubjectOptionalID == str).FirstOrDefault();
                    if (academicstandard == null)
                    {
                        academicstandard = new AcademicSubjectOptionalDetails();
                        academicstandard.InsertedId = userId;
                        academicstandard.ModifiedId = userId;
                        academicstandard.InsertedDate = DateTime.Now;
                        academicstandard.ModifiedDate = DateTime.Now;
                        academicstandard.Active = true;
                        academicstandard.Status = EntityStatus.ACTIVE;
                        academicstandard.IsAvailable = true;
                        academicstandard.AcademicSubjectOptionalID = id;
                        academicstandard.SubjectOptionalID = str;
                        await academicSubjectOptionalDetailsRepository.AddAsync(academicstandard);
                    }
                }
                var removeoptions = academicSubjectOptionalDetailsRepository.ListAllAsync().Result.Where(a => a.AcademicSubjectOptionalID == id && !myList.Contains(a.SubjectOptionalID)).ToList();
                foreach (AcademicSubjectOptionalDetails re in removeoptions)
                {
                    re.ModifiedId = userId;
                    re.ModifiedDate = DateTime.Now;
                    re.Active = false;
                    re.Status = EntityStatus.INACTIVE;
                    await academicSubjectOptionalDetailsRepository.UpdateAsync(re);
                }
                return RedirectToAction(nameof(AcademicSubjectOptional)).WithSuccess("Sucess", "Updated Sucessfully");

            }
        }

        public async Task<IActionResult> DeleteAcademicSubjectOptional(long id)
        {
            CheckLoginStatus();
            AcademicSubjectOptional ob = academicSubjectOptionalRepository.GetByIdAsync(id).Result;
            ob.Active = false;
            ob.ModifiedDate = DateTime.Now;
            ob.ModifiedId = userId;
            academicSubjectOptionalRepository.UpdateAsync(ob);
            return RedirectToAction(nameof(AcademicSubjectOptional)).WithSuccess("Success", "Deleted Successfully");
        }

        #endregion

        #region Academic Section

        public IActionResult AcademicSection(long AcademicStandardId, long AcademicStandardStreamId)
        {
            ViewData["AcademicStandardId"] = AcademicStandardId;
            //ViewData["AcademicStandardStreamId"] = AcademicStandardStreamId;

            //if (AcademicStandardStreamId > 0)
            //{
            //    var res = CommonSchoolModel.GetAllAcademicStandardSectionbyStream(AcademicStandardStreamId).Where(a => a.AcademicStandardId == AcademicStandardId);
            //    return View(res);
            //}
            //else
            //{
            var res = CommonSchoolModel.GetAllAcademicSection();
            return View(res == null ? null : res.Where(a => a.AcademicStandardId == AcademicStandardId));
            //}

        }
        public JsonResult GetAcademicsection(long AcademicStandardId)
        {
            var res = CommonSchoolModel.GetAllAcademicSection() == null ? null : CommonSchoolModel.GetAllAcademicSection().Where(a => a.AcademicStandardId == AcademicStandardId).Select(f => new commonCls { id = f.ID, name = f.SessionName }).ToList();
            return Json(res);
        }
        public IActionResult CreateOrEditAcademicSection(long AcademicStandardId, long AcademicStandardStreamId, long ID)
        {
            ViewData["AcademicStandardId"] = AcademicStandardId;
            ViewData["AcademicStandardStreamId"] = AcademicStandardStreamId;

            GetStatusList();
            AcademicSectionViewModel AcademicSectionmodel = new AcademicSectionViewModel();
            if (ID != 0 && ID.ToString() != null)
            {
                var res = CommonSchoolModel.GetAllAcademicSection().Where(a => a.ID == ID).FirstOrDefault();
                AcademicSectionmodel.AcademicStandardId = AcademicStandardId;
                AcademicSectionmodel.ID = res.ID;
                AcademicSectionmodel.Status = res.Status;
                AcademicSectionmodel.AcademicStandardId = res.AcademicStandardId;
                AcademicSectionmodel.AcademicStandardStreamId = res.AcademicStandardStreamId;
                AcademicSectionmodel.SectionId = res.SectionId;
                AcademicSectionmodel.SessionName = res.SessionName;
                ViewBag.Status = "Update";

            }
            else
            {
                var academicsectionlist = academicSectionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicStandardId).Select(a => a.SectionId);
                ViewData["Section"] = new[] { new SelectListItem { Text = "Select Section", Value = " " } }.Concat(new SelectList(sectionRepository.GetAllSection().Where(a => !academicsectionlist.Contains(a.ID)).ToList(), "ID", "Name"));

                AcademicSectionmodel.AcademicStandardId = AcademicStandardId;
                AcademicSectionmodel.AcademicStandardStreamId = AcademicStandardStreamId;
                AcademicSectionmodel.ID = 0;
                ViewBag.Status = "Create";
            }

            return View(AcademicSectionmodel);
        }
        [HttpPost]
        [ActionName("CreateOrEditAcademicSection")]
        public async Task<IActionResult> CreateOrEditAcademicSection(long[] MultiSectionId, long AcademicStandardId, long AcademicStandardStreamId, long ID, string Status, long SectionId)
        {
            ViewData["AcademicStandardId"] = AcademicStandardId;
            ViewData["AcademicStandardStreamId"] = AcademicStandardStreamId;
            CheckLoginStatus();
            AcademicSectionViewModel AcademicSectionmodel = new AcademicSectionViewModel();
            AcademicSectionmodel.AcademicStandardId = AcademicStandardId;
            AcademicSectionmodel.AcademicStandardStreamId = AcademicStandardStreamId;
            AcademicSectionmodel.ID = ID;
            AcademicSectionmodel.Status = Status;

            var routeValues = new RouteValueDictionary { { "AcademicStandardId", AcademicSectionmodel.AcademicStandardId }, { "AcademicStandardStreamId", AcademicSectionmodel.AcademicStandardStreamId } };
            if (ID == 0)
            {
                ArrayList myList = new ArrayList(MultiSectionId);
                foreach (long str in myList)
                {
                    AcademicSection academicsection = new AcademicSection()
                    {
                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = EntityStatus.ACTIVE,
                        IsAvailable = true,
                        SectionId = str,
                        AcademicStandardId = AcademicSectionmodel.AcademicStandardId,
                        //AcademicStandardStreamId = AcademicSectionmodel.AcademicStandardStreamId,
                    };
                    await academicSectionRepository.AddAsync(academicsection);
                }
                return RedirectToAction(nameof(AcademicSection), routeValues).WithSuccess("Sucess", "Saved Sucessfully");
            }
            else
            {
                AcademicSection academicsection = new AcademicSection()
                {
                    ID = AcademicSectionmodel.ID,
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = AcademicSectionmodel.Status,
                    IsAvailable = true,
                    SectionId = SectionId,
                    AcademicStandardId = AcademicSectionmodel.AcademicStandardId,
                    //AcademicStandardStreamId = AcademicSectionmodel.AcademicStandardStreamId,
                };
                await academicSectionRepository.UpdateAsync(academicsection);
            }
            return RedirectToAction(nameof(AcademicSection), routeValues).WithSuccess("Sucess", "Updated Sucessfully");
        }


        #endregion

        //#region Academic Standard Stream

        //public IActionResult AcademicStandardStream(long AcademicStandardId)
        //{
        //    ViewData["AcademicStandardId"] = AcademicStandardId;
        //    var res = CommonSchoolModel.GetAllAcademicstandardStream().Where(a => a.AcademicStandardId == AcademicStandardId);
        //    return View(res);
        //}
        //public IActionResult CreateOrEditAcademicStandardStream(long AcademicStandardId, long ID)
        //{
        //    ViewData["AcademicStandardId"] = AcademicStandardId;
        //    GetStatusList();
        //    AcademicStandardStreamViewModel AcademicStreammodel = new AcademicStandardStreamViewModel();
        //    if (ID != 0 && ID.ToString() != null)
        //    {
        //        var res = CommonSchoolModel.GetAllAcademicstandardStream().Where(a => a.ID == ID).FirstOrDefault();
        //        AcademicStreammodel.AcademicStandardId = AcademicStandardId;
        //        AcademicStreammodel.ID = res.ID;
        //        AcademicStreammodel.Status = res.Status;
        //        AcademicStreammodel.AcademicStandardId = res.AcademicStandardId;
        //        AcademicStreammodel.StreamId = res.StreamId;
        //        AcademicStreammodel.StreamName = res.StreamName;
        //        AcademicStreammodel.BoardStandardStreamId = res.BoardStandardStreamId;
        //        ViewBag.Status = "Update";


        //    }
        //    else
        //    {
        //        var academicStandardStreamlist = academicStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicStandardId).Select(a => a.BoardStandardStreamId).ToList();


        //        var BoardstandardIdByacademicStandard = academicStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicStandardId).Select(a => a.BoardStandardId).FirstOrDefault();

        //        var boardstandardstream = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardStandardId == BoardstandardIdByacademicStandard && !academicStandardStreamlist.Contains(a.ID)).ToList();
        //        var stream = StreamRepository.GetAllStream();
        //        var StreamList = (from a in boardstandardstream
        //                          join b in stream on a.StreamId equals b.ID
        //                          select new
        //                          {
        //                              ID = a.ID,
        //                              Name = b.Name
        //                          }
        //               ).ToList();
        //        ViewData["Stream"] = new[] { new SelectListItem { Text = "Select Stream", Value = " " } }.Concat(new SelectList(StreamList, "ID", "Name"));

        //        AcademicStreammodel.AcademicStandardId = AcademicStandardId;
        //        AcademicStreammodel.ID = 0;
        //        ViewBag.Status = "Create";
        //    }

        //    return View(AcademicStreammodel);

        //}
        //[HttpPost]

        //public async Task<IActionResult> CreateOrEditAcademicStandardStream(long[] MultiBoardStandardStreamId, long AcademicStandardId, long ID, string Status, long BoardStandardStreamId)
        //{
        //    CheckLoginStatus();
        //    AcademicStandardStreamViewModel AcademicStreammodel = new AcademicStandardStreamViewModel();
        //    AcademicStreammodel.AcademicStandardId = AcademicStandardId;
        //    AcademicStreammodel.ID = ID;
        //    AcademicStreammodel.Status = Status;
        //    AcademicStreammodel.BoardStandardStreamId = BoardStandardStreamId;
        //    var routeValues = new RouteValueDictionary { { "AcademicStandardId", AcademicStreammodel.AcademicStandardId } };
        //    if (ID == 0)
        //    {
        //        ArrayList myList = new ArrayList(MultiBoardStandardStreamId);
        //        foreach (long str in myList)
        //        {
        //            AcademicStandardStream academicstream = new AcademicStandardStream()
        //            {
        //                InsertedId = userId,
        //                ModifiedId = userId,
        //                InsertedDate = DateTime.Now,
        //                ModifiedDate = DateTime.Now,
        //                Active = true,
        //                Status = EntityStatus.ACTIVE,
        //                IsAvailable = true,
        //                BoardStandardStreamId = str,
        //                AcademicStandardId = AcademicStreammodel.AcademicStandardId,
        //            };
        //            await academicStandardStreamRepository.AddAsync(academicstream);
        //        }
        //        return RedirectToAction(nameof(AcademicStandardStream), routeValues).WithSuccess("Sucess", "Saved Sucessfully");
        //    }
        //    else
        //    {
        //        AcademicStandardStream academicstream = new AcademicStandardStream()
        //        {
        //            ID = AcademicStreammodel.ID,
        //            InsertedId = userId,
        //            ModifiedId = userId,
        //            InsertedDate = DateTime.Now,
        //            ModifiedDate = DateTime.Now,
        //            Active = true,
        //            Status = AcademicStreammodel.Status,
        //            IsAvailable = true,
        //            BoardStandardStreamId = AcademicStreammodel.BoardStandardStreamId,
        //            AcademicStandardId = AcademicStreammodel.AcademicStandardId,
        //        };
        //        await academicStandardStreamRepository.UpdateAsync(academicstream);
        //    }
        //    return RedirectToAction(nameof(AcademicStandardStream), routeValues).WithSuccess("Sucess", "Updated Sucessfully");
        //}








        //#endregion

        #region Organization Academic 

        public IActionResult OraganizationAcademic(long SessionId) //List Show
        {
            ViewData["SessionId"] = SessionId;
            ViewBag.AcademicSessionName = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == SessionId).Select(a => a.DisplayName).FirstOrDefault();

            var res = CommonSchoolModel.GetAllOrganisationAcademic().Where(a => a.AcademicSessionID == SessionId);

            //var res = (from a in organizationAcademicList
            //           select new NameOrganizationAcademicCountViewModel
            //           {
            //               ID = a.ID,
            //               InsertedDate = a.InsertedDate,
            //               ModifiedDate = a.ModifiedDate,
            //               Active = a.Active,
            //               Status = a.Status,
            //               IsAvailable = a.IsAvailable,
            //               AcademicSessinId = a.AcademicSessionID,
            //               OrganizationId = a.OrganisationID,
            //               OrganizationAcademicCount = organizationAcademicRepository.GetOrganizationAcademicCountBySessionId(a.ID)
            //           });

            return View(res);
        }

        public IActionResult CreateOrganizationAcademic(long SessionId)
        {
            ViewData["SessionId"] = SessionId;
            ViewBag.AcademicSessionName = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == SessionId).Select(a => a.DisplayName).FirstOrDefault();


            // ViewData["AcademicSession"] = new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true), "ID", "DisplayName");

            //var typeid = organizationTypeRepository.GetAllOrganizationType().Where(a=>a.Name == "School").Select(a=>a.ID).FirstOrDefault();

            //ViewData["Oraganization"] = new[] { new SelectListItem { Text = "Select Oraganization", Value = " " } }.Concat(new SelectList(organizationRepository.GetAllOrganization().Where(a=>a.OrganizationTypeID == typeid), "ID", "Name"));

            return View();
        }


        [HttpGet]
        public JsonResult GetOrganisationSelectedType(long SessionId)
        {
            try
            {
                //var typeid = organizationTypeRepository.GetAllOrganizationType().Where(a => a.Name == "School").Select(a => a.ID).FirstOrDefault();

                var OrganizationList = organizationRepository.GetAllOrganization();//.Where(a => a.OrganizationTypeID == typeid);
                var OrganizationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == SessionId);

                var res = (from a in OrganizationList
                           join b in OrganizationAcademic on a.ID equals b.OrganizationId
                           select a
                          ).ToList();

                var list = OrganizationList.Except(res);

                return Json(list);
            }
            catch
            {
                return Json(null);
            }
        }


        [HttpPost]
        public async Task<IActionResult> CreateOrganizationAcademic(long[] OrganisationId, long AcademicSessionID)
        {
            CheckLoginStatus();

            OrganizationAcademicViewModel organisationacademic = new OrganizationAcademicViewModel();
            organisationacademic.OrganisationId = OrganisationId;
            organisationacademic.AcademicSessionID = AcademicSessionID;

            ArrayList myList = new ArrayList(OrganisationId);
            foreach (long str in myList)
            {
                OrganizationAcademic orgaacademic = new OrganizationAcademic()
                {
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = "ACTIVE",
                    IsAvailable = false,
                    OrganizationId = str,
                    AcademicSessionId = organisationacademic.AcademicSessionID

                };
                await organizationAcademicRepository.AddAsync(orgaacademic);
            }
            var routeValues = new RouteValueDictionary { { "SessionId", organisationacademic.AcademicSessionID } };


            return RedirectToAction("OraganizationAcademic", "School", routeValues).WithSuccess("Success", "Saved Successfully");

        }


        public async Task<IActionResult> EditOrganizationAcademic(long? id, long SessionId)
        {
            ViewData["SessionId"] = SessionId;
            ViewBag.AcademicSessionName = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == SessionId).Select(a => a.DisplayName).FirstOrDefault();

            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = "0" } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true), "ID", "DisplayName"));
            ViewData["Oraganization"] = new SelectList(CommonSchoolModel.GetAllOrganisationAcademic().Where(a => a.ID == id), "ID", "OrganisationName");
            GetStatusList();
            var res = CommonSchoolModel.GetAllOrganisationAcademic().FirstOrDefault(a => a.ID == id);
            return View(res);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("EditOrganizationAcademic")]
        public async Task<IActionResult> EditOrganizationAcademic([Bind("ID,AcademicSessionID,Status,OrganisationID")] OrganizationAcademicViewModel organisationacademic)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {
                OrganizationAcademic orgaacademic = new OrganizationAcademic()
                {
                    ID = organisationacademic.ID,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = organisationacademic.Status,
                    IsAvailable = false,
                    OrganizationId = organisationacademic.OrganisationID,
                    AcademicSessionId = organisationacademic.AcademicSessionID
                };

                await organizationAcademicRepository.UpdateAsync(orgaacademic);

                var routeValues = new RouteValueDictionary { { "SessionId", organisationacademic.AcademicSessionID } };

                return RedirectToAction("OraganizationAcademic", "School", routeValues).WithSuccess("Update", "Updated Successfully");
            }
            return View();

        }

        //public async Task<IActionResult> DeleteOraganizationAcademic(long? id, OrganizationAcademic organisationacademic)  //Edit Academic Session
        //{

        //    await organizationAcademicRepository.DeleteAsync(organisationacademic);
        //    return RedirectToAction(nameof(OraganizationAcademic));
        //}


        #endregion

        #region  Academic Subject
        public IActionResult AcademicSubject() //List Show
        {
            var res = CommonSchoolModel.GetAllAcademicSubject();
            return View(res);
        }

        public IActionResult CreateAcademicSubject()
        {
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = "0" } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true), "ID", "DisplayName"));
            ViewData["BoardID"] = new[] { new SelectListItem { Text = "Select Board", Value = "0" } }.Concat(new SelectList(BoardRepository.GetAllBoard(), "ID", "Name"));

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAcademicSubject([Bind("SubjectIDArray,AcademicSessionID")] AcademicSubjectViewModel academicsub)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {
                foreach (long id in academicsub.SubjectIDArray)
                {
                    AcademicSubject sub = new AcademicSubject()
                    {
                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = EntityStatus.ACTIVE,
                        IsAvailable = false,
                        SubjectId = id,
                        AcademicSessionId = academicsub.AcademicSessionID
                    };

                    await academicSubjectRepository.AddAsync(sub);
                }

                return RedirectToAction(nameof(AcademicSubject)).WithSuccess("Success", "Saved Successfully");
            }
            return View();

        }


        public async Task<IActionResult> EditAcademicSubject(long? id)
        {
            //  
            // ViewData["Subject"] = new[] { new SelectListItem { Text = "Select Board", Value = "0" } }.Concat(new SelectList(subjectRepository.ListAllAsyncIncludeAll().Result, "ID", "Name"));

            var res = CommonSchoolModel.GetAllAcademicSubject().FirstOrDefault(a => a.ID == id);
            ViewData["AcademicSession"] = new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true), "ID", "DisplayName", res.AcademicSessionID);
            ViewData["Subject"] = new[] { new SelectListItem { Text = "Select Subject", Value = "0" } }.Concat(new SelectList(SubjectRepository.ListAllAsyncIncludeAll().Result, "ID", "Name", res.SubjectID));

            GetStatusList();

            return View(res);
        }


        [HttpPost]
        [ActionName("EditAcademicSubject")]
        public async Task<IActionResult> EditAcademicSubject([Bind("ID,SubjectID,AcademicSessionID,Status")] AcademicSubjectViewModel academicsub)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {
                AcademicSubject sub = new AcademicSubject()
                {
                    ID = academicsub.ID,
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,


                    IsAvailable = false,
                    SubjectId = academicsub.SubjectID,
                    AcademicSessionId = academicsub.AcademicSessionID
                };
                if (academicsub.Status == EntityStatus.ACTIVE)
                {
                    sub.Active = true;
                    sub.Status = EntityStatus.ACTIVE;

                }
                else
                {
                    sub.Active = false;
                    sub.Status = EntityStatus.INACTIVE;
                }
                await academicSubjectRepository.UpdateAsync(sub);
                return RedirectToAction(nameof(AcademicSubject)).WithSuccess("Update", "Updated Successfully");
            }
            return View();

        }
        #endregion

        #region Syallbus
        public async Task<IActionResult> Syallbus()
        {
            var res = await syallbusRepository.ListAllAsyncIncludeAll();

            return View(res);
        }

        public async Task<IActionResult> CreateOrEditSyallbus(long? id)
        {
            if (id != null && id != 0)
            {
                ViewBag.Message = "Update";
                var res = syallbusRepository.GetByIdAsyncIncludeAll(id.Value).Result;
                SyallbusViewModel sysmodel = new SyallbusViewModel();
                sysmodel.ID = res.ID;
                sysmodel.Titel = res.Title;
                sysmodel.Description = res.Description;
                sysmodel.IsAvailable = res.IsAvailable;
                return View(sysmodel);
            }
            else
            {
                ViewBag.Message = "Create";
                return View();
            }



        }
        [HttpPost]
        [ActionName("CreateOrEditSyallbus")]

        public async Task<IActionResult> CreateOrEditSyallbus([Bind("ID,Titel,Description,IsAvailable")] SyallbusViewModel syallbus)
        {
            CheckLoginStatus();


            if (syallbus.ID.ToString() != null && syallbus.ID != 0)
            {

                Syllabus syall = new Syllabus()
                {
                    ID = syallbus.ID,
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = EntityStatus.ACTIVE,
                    IsAvailable = syallbus.IsAvailable,
                    Title = syallbus.Titel,
                    Description = syallbus.Description,

                };

                await syallbusRepository.UpdateAsync(syall);

                return RedirectToAction(nameof(Syallbus)).WithSuccess("Update", "Updated Successfully");

            }
            else
            {
                Syllabus syall = new Syllabus()
                {
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = EntityStatus.ACTIVE,
                    IsAvailable = syallbus.IsAvailable,
                    Title = syallbus.Titel,
                    Description = syallbus.Description,

                };

                await syallbusRepository.AddAsync(syall);

                return RedirectToAction(nameof(Syallbus)).WithSuccess("Success", "Saved Successfully");

            }



        }





        #endregion

        #region AcademicChapter
        public IActionResult AcademicChapter(long AcademicSubjectId) //List Show
        {
            ViewData["AcademicSubjectId"] = AcademicSubjectId;
            var subjectid = academicSubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicSubjectId).Select(a => a.SubjectId).FirstOrDefault();
            ViewData["SubjectName"] = SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == subjectid).Select(a => a.Name).FirstOrDefault();

            var res = CommonSchoolModel.GetAllAcademicChapter().Where(a => a.AcademicSubjectId == AcademicSubjectId);
            return View(res);
        }
        [HttpGet]

        public async Task<IActionResult> CreateOrEditAcademicChapter(long AcademicSubjectId, long? ID)
        {
            GetStatusList();
            ViewData["AcademicSubjectId"] = AcademicSubjectId;
            var subjectid = academicSubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicSubjectId).Select(a => a.SubjectId).FirstOrDefault();
            ViewData["SubjectName"] = SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == subjectid).Select(a => a.Name).FirstOrDefault();

            AcademicChapterViewModel AcademicChapterModel = new AcademicChapterViewModel();
            if (ID != 0 && ID != null)
            {
                var res = CommonSchoolModel.GetAllAcademicChapter().Where(a => a.ID == ID).FirstOrDefault();
                AcademicChapterModel.ID = res.ID;
                AcademicChapterModel.ChapterId = res.ChapterId;
                AcademicChapterModel.ChapterName = res.ChapterName;
                AcademicChapterModel.AcademicSubjectId = res.AcademicSubjectId;
                AcademicChapterModel.Status = res.Status;
                ViewBag.Status = "Update";
            }
            else
            {
                AcademicChapterModel.ID = 0;
                AcademicChapterModel.AcademicSubjectId = AcademicSubjectId;
                ViewBag.Status = "Create";

            }

            var academicchapterlist = academicChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSubjectId == AcademicSubjectId).Select(a => a.ChapterId);

            ViewData["ChapterList"] = new[] { new SelectListItem { Text = "Select Chapter", Value = "0" } }.Concat(new SelectList(ChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => !academicchapterlist.Contains(a.ID)).ToList(), "ID", "Name"));

            return View(AcademicChapterModel);
        }

        [HttpPost]
        [ActionName("CreateOrEditAcademicChapter")]
        public async Task<IActionResult> CreateOrEditAcademicChapter(long AcademicSubjectId, long id, long[] ChapterArrayId, long ChapterId, string Status)
        {
            CheckLoginStatus();
            GetStatusList();

            var routeValues = new RouteValueDictionary { { "AcademicSubjectId", AcademicSubjectId } };

            AcademicChapterViewModel AcademicChapterModel = new AcademicChapterViewModel();
            AcademicChapterModel.AcademicSubjectId = AcademicSubjectId;
            AcademicChapterModel.ChapterId = ChapterId;
            AcademicChapterModel.ID = id;
            AcademicChapterModel.Status = Status;

            if (id == 0)
            {

                ArrayList myList = new ArrayList(ChapterArrayId);
                foreach (long str in myList)
                {
                    AcademicChapter academicchapter = new AcademicChapter()
                    {
                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = EntityStatus.ACTIVE,
                        IsAvailable = true,
                        AcademicSubjectId = AcademicChapterModel.AcademicSubjectId,
                        ChapterId = str,

                    };
                    await academicChapterRepository.AddAsync(academicchapter);
                }
                return RedirectToAction("AcademicChapter", "School", routeValues).WithSuccess("Sucess", "saved Sucessfully");
            }
            else
            {
                AcademicChapter academicchapter = new AcademicChapter()
                {
                    ID = AcademicChapterModel.ID,
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = AcademicChapterModel.Status,
                    IsAvailable = true,
                    AcademicSubjectId = AcademicSubjectId,
                    ChapterId = ChapterId,

                };
                await academicChapterRepository.UpdateAsync(academicchapter);
            }


            return RedirectToAction("AcademicChapter", "School", routeValues).WithSuccess("Sucess", "Updated Sucessfully");
        }
        #endregion


        #region Academic Concept
        public IActionResult AcademicConcept(long AcademicSubjectId, long AcademicChapterId) //List Show
        {
            ViewData["AcademicSubjectId"] = AcademicSubjectId;
            ViewData["AcademicChapterId"] = AcademicChapterId;

            var subjectid = academicSubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicSubjectId).Select(a => a.SubjectId).FirstOrDefault();
            ViewData["SubjectName"] = SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == subjectid).Select(a => a.Name).FirstOrDefault();

            var Chapterid = academicChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicChapterId).Select(a => a.ChapterId).FirstOrDefault();
            ViewData["ChapterName"] = ChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == Chapterid).Select(a => a.Name).FirstOrDefault();

            var res = CommonSchoolModel.GetAllAcademicConcept().Where(a => a.AcademicChapterId == AcademicChapterId);
            return View(res);
        }

        public async Task<IActionResult> CreateOrEditAcademicConcept(long AcademicSubjectId, long AcademicChapterId, long? ID)
        {
            ViewData["AcademicSubjectId"] = AcademicSubjectId;
            ViewData["AcademicChapterId"] = AcademicChapterId;

            var subjectid = academicSubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicSubjectId).Select(a => a.SubjectId).FirstOrDefault();
            ViewData["SubjectName"] = SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == subjectid).Select(a => a.Name).FirstOrDefault();

            var Chapterid = academicChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicChapterId).Select(a => a.ChapterId).FirstOrDefault();
            ViewData["ChapterName"] = ChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == Chapterid).Select(a => a.Name).FirstOrDefault();

            var academicconceptlist = academicConceptRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicChapterId == AcademicChapterId).Select(a => a.ConceptId);
            ViewData["ConceptList"] = new[] { new SelectListItem { Text = "Select Concept", Value = " " } }.Concat(new SelectList(conceptRepository.ListAllAsyncIncludeAll().Result.Where(a => !academicconceptlist.Contains(a.ID) && a.ChapterId==Chapterid).ToList(), "ID", "Name"));
            GetStatusList();


            AcademicConceptViewModel AcademicConceptModel = new AcademicConceptViewModel();
            if (ID != 0 && ID != null)
            {
                var res = CommonSchoolModel.GetAllAcademicConcept().Where(a => a.ID == ID).FirstOrDefault();
                AcademicConceptModel.ID = res.ID;
                AcademicConceptModel.ConceptId = res.ConceptId;
                AcademicConceptModel.ConceptName = res.ConceptName;
                AcademicConceptModel.AcademicChapterId = res.AcademicChapterId;
                AcademicConceptModel.AcademicSubjectId = AcademicSubjectId;
                AcademicConceptModel.Status = res.Status;
                ViewBag.Status = "Update";
            }
            else
            {
                AcademicConceptModel.ID = 0;
                AcademicConceptModel.AcademicSubjectId = AcademicSubjectId;
                AcademicConceptModel.AcademicChapterId = AcademicChapterId;
                ViewBag.Status = "Create";

            }
            return View(AcademicConceptModel);

        }

        [HttpPost]
        [ActionName("CreateOrEditAcademicConcept")]
        public async Task<IActionResult> CreateOrEditAcademicConcept(long AcademicSubjectId, long AcademicChapterId, long id, long[] ConceptArrayId, long ConceptId, string Status)
        {
            CheckLoginStatus();


            var routeValues = new RouteValueDictionary { { "AcademicSubjectId", AcademicSubjectId }, { "AcademicChapterId", AcademicChapterId } };

            if (id == 0)
            {
                ArrayList myList = new ArrayList(ConceptArrayId);
                foreach (long str in myList)
                {
                    AcademicConcept academicconcept = new AcademicConcept()
                    {
                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = EntityStatus.ACTIVE,
                        IsAvailable = true,
                        AcademicChapterId = AcademicChapterId,
                        ConceptId = str,

                    };
                    await academicConceptRepository.AddAsync(academicconcept);
                }
                return RedirectToAction("AcademicConcept", "School", routeValues).WithSuccess("Sucess", "saved Sucessfully");
            }
            else
            {
                AcademicConcept academicconcept = new AcademicConcept()
                {
                    ID = id,
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = Status,
                    IsAvailable = true,
                    AcademicChapterId = AcademicChapterId,
                    ConceptId = ConceptId,

                };
                await academicConceptRepository.UpdateAsync(academicconcept);
            }

            return RedirectToAction("AcademicConcept", "School", routeValues).WithSuccess("Sucess", "Updated Sucessfully");
        }



        #endregion


        #region WebApis
        [HttpGet]
        public JsonResult GetStandardByBoardType(long BoardId)                 //Filter the selected Standard
        {
            try
            {
                var boardStandardList = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardId == BoardId);
                var standardList = StandardRepository.GetAllStandard();

                var res = (from a in standardList
                           join b in boardStandardList on a.ID equals b.StandardId
                           select a
                          ).ToList();

                var list = standardList.Except(res);

                return Json(list);
            }
            catch
            {
                return Json(null);
            }
        }

        public JsonResult GetEmployeeFilterForAcademicTeacher(long BoardId)                 //Filter the selected Standard
        {
            try
            {
                var boardStandardList = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardId == BoardId);
                var standardList = StandardRepository.GetAllStandard();

                var res = (from a in standardList
                           join b in boardStandardList on a.ID equals b.StandardId
                           select a
                          ).ToList();

                var list = standardList.Except(res);

                return Json(list);
            }
            catch
            {
                return Json(null);
            }
        }
        public async Task<JsonResult> GetStreamByStreamType(long? id)
        {
            try
            {
                // var standardList = StandardRepository.GetAllStandard().FirstOrDefault(a => a.ID == id);

                var BoardList = BoardRepository.GetBoardById(id.Value);
                var standardList = StandardRepository.GetStandardById(id.Value);
                var BoardstandardList = BoardStandardRepository.GetByIdAsync(id.Value).Result;
                //var BoardStandardStreamList = boardStandardStreamRepository.GetByIdAsync(id.Value).Result;
                var SubjectList = SubjectRepository.GetByIdAsync(id.Value);

                if (standardList.StreamType == true)
                {
                    var streamList = "";
                    //var streamList = (from a in SubjectList
                    //                  join b in BoardstandardList on a.BoardStandardId equals b.ID
                    //           select new SubjectViewModel
                    //           {
                    //               ID = a.ID,
                    //               Name = a.Name,
                    //               Status = a.Status,
                    //               // ParentSubjectId = a.ParentSubjectId,
                    //               SubjectCode = a.SubjectCode,
                    //               BoardId = b.BoardId,
                    //               BoardStandardId = b.ID
                    //           }

                    //    ).ToList();

                    return Json(streamList);
                }
                else
                {
                    return Json(null);
                }
            }
            catch
            {
                return Json(null);
            }
        }


        public async Task<JsonResult> GetGroupOrOrganizationBySelectedId(long id)
        {
            try
            {
                //var ListItem ="".ToList();

                if (id == 1)
                {
                    var ListItem = organizationRepository.GetAllOrganization();
                    return Json(ListItem);
                }
                else
                {
                    var ListItem = groupRepository.GetAllGroup();
                    return Json(ListItem);
                }

            }
            catch
            {
                return Json(null);
            }
        }

        //public async Task<JsonResult> GetEmployeeBySelectedOrganizationIdOrGroupId(long orgId, long groupId)
        //{
        //    try
        //    {
        //        if (groupId == 0)
        //        {
        //            //var orgEmp = 
        //        }
        //        else
        //        { 

        //        }
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}

        //public JsonResult GetStreamByStandardType(long id)
        //{
        //    try
        //    {
        //        if (standard.StreamType == true)
        //        {
        //            var streamList = StreamRepository.GetAllStream();
        //            return Json(streamList);
        //        }

        //        var streamList = StreamRepository.GetAllStream();
        //        var standardList = StandardRepository.GetAllStandard().Where(a => a.ID == id);



        //        var res = (from a in standardList
        //                   join b in streamList on a.StreamType equals true
        //                   select b).ToList();

        //        var res = (from a in streamList
        //                   join b in standardList where b.StreamType == true
        //                   select a).ToList();



        //        return Json(null);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}

        #endregion

        //#region BoardStandardStream
        //public IActionResult BoardStandardStream(long BoardStnrdId, long BoardId) //BoardStandardStream List Show
        //{
        //    ViewData["BoardId"] = BoardId;
        //    ViewData["BoardStandardId"] = BoardStnrdId;
        //    ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
        //    var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStnrdId).Select(a => a.StandardId).FirstOrDefault();
        //    ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid).Select(a => a.Name).FirstOrDefault();

        //    var groups = CommonSchoolModel.GetAllBoardStandardStream().Where(a => a.BoardStandardId == BoardStnrdId);
        //    var res = (from a in groups.ToList()
        //               select new CountSubjectStreamWiseViewModel
        //               {
        //                   ID = a.ID,
        //                   StreamId = a.StreamId,
        //                   BoardStandardId = a.BoardStandardId,
        //                   BoardId = a.BoardId,
        //                   StandardId = a.StandardId,
        //                   StreamName = a.StreamName,
        //                   ModifiedDate = a.ModifiedDate,
        //                   Status = a.Status,
        //                   BoardStandarStreamSubCnt = boardStandardStreamRepository.GetSubjectsreamCountByBoardStandardId(a.ID),
        //               }).ToList();

        //    return View(res);

        //}
        //public IActionResult CreateOrEditBoardStandardStream(long BoardStandardID, long BoardId, long? id)
        //{
        //    ViewData["BoardId"] = BoardId;
        //    ViewData["BoardStandardId"] = BoardStandardID;

        //    ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
        //    var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardID).Select(a => a.StandardId).FirstOrDefault();
        //    ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid).Select(a => a.Name).FirstOrDefault();



        //    ViewData["Streams"] = new SelectList(StreamRepository.GetAllStream(), "ID", "Name");
        //    if (id != null && id != 0)
        //    {
        //        GetStatusList();
        //        ViewBag.Message = "Update";
        //        var res = boardStandardStreamRepository.GetByIdAsyncIncludeAll(id.Value).Result;
        //        BoardStandardStreamViewModel brdmodel = new BoardStandardStreamViewModel();
        //        brdmodel.ID = res.ID;
        //        brdmodel.StreamId = res.StreamId;
        //        brdmodel.BoardStandardId = res.BoardStandardId;
        //        return View(brdmodel);
        //    }
        //    else
        //    {
        //        ViewBag.Message = "Create";
        //        return View();
        //    }
        //}
        //[HttpPost]
        //[ActionName("CreateOrEditBoardStandardStream")]
        //public async Task<IActionResult> CreateOrEditBoardStandardStream(long ID, string Status, long BoardStandardId, long StreamId, long BoardId)
        //{
        //    CheckLoginStatus();
        //    BoardStandardStreamViewModel brdstrdstrm = new BoardStandardStreamViewModel();
        //    brdstrdstrm.BoardStandardId = BoardStandardId;
        //    brdstrdstrm.StreamId = StreamId;
        //    brdstrdstrm.BoardId = BoardId;
        //    brdstrdstrm.ID = ID;
        //    brdstrdstrm.Status = Status;
        //    var routeValues = new RouteValueDictionary { { "BoardStnrdId", brdstrdstrm.BoardStandardId }, { "BoardId", brdstrdstrm.BoardId } };

        //    if (ID.ToString() != null && ID != 0)
        //    {
        //        GetStatusList();
        //        BoardStandardStream brdupdate = new BoardStandardStream()
        //        {
        //            ID = brdstrdstrm.ID,
        //            InsertedId = userId,
        //            ModifiedId = userId,
        //            InsertedDate = DateTime.Now,
        //            ModifiedDate = DateTime.Now,
        //            Active = true,
        //            Status = brdstrdstrm.Status,
        //            IsAvailable = true,
        //            StreamId = brdstrdstrm.StreamId,
        //            BoardStandardId = brdstrdstrm.BoardStandardId
        //        };
        //        await boardStandardStreamRepository.UpdateAsync(brdupdate);
        //        return RedirectToAction("BoardStandardStream", "School", routeValues).WithSuccess("Update", "Updated Successfully");

        //    }
        //    else
        //    {
        //        BoardStandardStream brd = new BoardStandardStream()
        //        {
        //            InsertedId = userId,
        //            ModifiedId = userId,
        //            InsertedDate = DateTime.Now,
        //            ModifiedDate = DateTime.Now,
        //            Active = true,
        //            Status = EntityStatus.ACTIVE,
        //            IsAvailable = true,
        //            StreamId = brdstrdstrm.StreamId,
        //            BoardStandardId = brdstrdstrm.BoardStandardId
        //        };
        //        await boardStandardStreamRepository.AddAsync(brd);
        //        return RedirectToAction("BoardStandardStream", "School", routeValues).WithSuccess("Success", "Saved Successfully");

        //    }

        //}
        //#endregion
        #region BoardStandardWing
        public IActionResult BoardStandardWing(long BoardStnrdId, long BoardId) //BoardStandardStream List Show
        {
            ViewData["BoardId"] = BoardId;
            ViewData["BoardStandardId"] = BoardStnrdId;
            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
            var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStnrdId && a.Active == true).Select(a => a.StandardId).FirstOrDefault();
            ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid && a.Active == true).Select(a => a.Name).FirstOrDefault();
            var wings = wingRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var groups = boardStandardWingRepository.ListAllAsync().Result.Where(a => a.BoardStandardID == BoardStnrdId && a.Active == true).ToList();
            var res = (from a in groups
                       join b in wings on a.WingID equals b.ID
                       select new CountSubjectStreamWiseViewModel
                       {
                           ID = a.ID,
                           StreamId = a.WingID,
                           BoardStandardId = a.BoardStandardID,
                           StreamName = b.Name,
                           ModifiedDate = a.ModifiedDate,
                           Status = a.Status
                       }).ToList();

            return View(res);

        }
        public IActionResult CreateOrEditBoardStandardWing(long BoardStandardID, long BoardId, long? id)
        {
            ViewData["BoardId"] = BoardId;
            ViewData["BoardStandardId"] = BoardStandardID;

            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
            var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardID).Select(a => a.StandardId).FirstOrDefault();
            ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid).Select(a => a.Name).FirstOrDefault();


            GetStatusList();
            ViewBag.Streams = new SelectList(wingRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList(), "ID", "Name");
            if (id != null && id != 0)
            {

                ViewBag.Message = "Update";
                var res = boardStandardWingRepository.GetByIdAsyncIncludeAll(id.Value).Result;
                var wings = wingRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == res.WingID).FirstOrDefault();

                BoardStandardStreamViewModel brdmodel = new BoardStandardStreamViewModel();
                brdmodel.ID = res.ID;
                brdmodel.StreamId = wings.ID;
                brdmodel.BoardStandardId = res.BoardStandardID;
                return View(brdmodel);
            }
            else
            {
                ViewBag.Message = "Create";
                BoardStandardStreamViewModel brdmodel = new BoardStandardStreamViewModel();
                brdmodel.ID = 0;
                brdmodel.StreamId = 0;
                brdmodel.BoardStandardId = 0;
                return View();
            }
        }
        [HttpPost]
        [ActionName("CreateOrEditBoardStandardWing")]
        public async Task<IActionResult> CreateOrEditBoardStandardWing(string Status, long BoardStandardId, long[] StreamId, long BoardId)
        {
            CheckLoginStatus();
            BoardStandardStreamViewModel brdstrdstrm = new BoardStandardStreamViewModel();
            brdstrdstrm.BoardStandardId = BoardStandardId;
            // brdstrdstrm.StreamId = StreamId;
            brdstrdstrm.BoardId = BoardId;
            // brdstrdstrm.ID = ID;
            brdstrdstrm.Status = Status;
            var routeValues = new RouteValueDictionary { { "BoardStnrdId", brdstrdstrm.BoardStandardId }, { "BoardId", brdstrdstrm.BoardId } };

            if (StreamId.Count() > 0)
            {
                GetStatusList();
                foreach (var wing in StreamId)
                {
                    BoardStandardWing brdupdate = boardStandardWingRepository.ListAllAsync().Result.Where(a => a.BoardStandardID == BoardStandardId && a.WingID == wing && a.Active == true).FirstOrDefault();
                    if (brdupdate != null)
                    {

                        brdupdate.ModifiedId = userId;
                        brdupdate.ModifiedDate = DateTime.Now;
                        brdupdate.Active = brdstrdstrm.Status == "ACTIVE" ? true : false;
                        brdupdate.Status = brdstrdstrm.Status;
                        brdupdate.IsAvailable = true;
                        brdupdate.WingID = wing;
                        brdupdate.BoardStandardID = brdstrdstrm.BoardStandardId;
                        await boardStandardWingRepository.UpdateAsync(brdupdate);
                    }
                    else
                    {
                        brdupdate = new BoardStandardWing();
                        brdupdate.InsertedDate = DateTime.Now;
                        brdupdate.InsertedId = userId;
                        brdupdate.ModifiedId = userId;
                        brdupdate.ModifiedDate = DateTime.Now;
                        brdupdate.Active = brdstrdstrm.Status == "ACTIVE" ? true : false; ;
                        brdupdate.Status = brdstrdstrm.Status;
                        brdupdate.IsAvailable = true;
                        brdupdate.WingID = wing;
                        brdupdate.BoardStandardID = brdstrdstrm.BoardStandardId;
                        await boardStandardWingRepository.AddAsync(brdupdate);
                    }
                }
                return RedirectToAction("BoardStandardWing", "School", routeValues).WithSuccess("Submit", "Submitted Successfully");
            }
            else
            {
                return RedirectToAction("CreateOrEditBoardStandardWing", new { BoardStandardID = BoardStandardId, BoardId = BoardId }).WithWarning("Warning", "Please select wings");
            }

        }
        #endregion
        #region Chapter

        public IActionResult Chapter(long BoardId, long BoardStnrdId, long SubjectId)
        {
            ViewData["BoardId"] = BoardId;
            ViewData["BoardStandardId"] = BoardStnrdId;
            //ViewData["BoardStaandStreamId"] = boardStaandStreamId;
            ViewData["SubjectId"] = SubjectId;

            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
            var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStnrdId).Select(a => a.StandardId).FirstOrDefault();
            ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid).Select(a => a.Name).FirstOrDefault();
            //if (boardStaandStreamId != 0)
            //{
            //    var streamid = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == boardStaandStreamId).Select(a => a.StreamId).FirstOrDefault();
            //    ViewBag.StreamName = StreamRepository.GetAllStream().Where(a => a.ID == streamid).Select(a => a.Name).FirstOrDefault();

            //}
            ViewBag.SubjectName = SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == SubjectId).Select(a => a.Name).FirstOrDefault();



            var res = CommonSchoolModel.GetAllChapters().Where(a => a.SubjectId == SubjectId);
            return View(res);

        }


        public IActionResult CreateOrEditChapter(long? id, long BoardId, long BoardStnrdId, long BoardStaandStreamId, long SubjectId)
        {
            CheckLoginStatus();
            GetStatusList();
            ViewData["BoardId"] = BoardId;
            ViewData["BoardStandardId"] = BoardStnrdId;
            //ViewData["BoardStaandStreamId"] = BoardStaandStreamId;
            ViewData["SubjectId"] = SubjectId;

            // Navigation Name Bar
            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
            var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStnrdId).Select(a => a.StandardId).FirstOrDefault();
            ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid).Select(a => a.Name).FirstOrDefault();
            //if (BoardStaandStreamId != 0)
            //{
            //    var streamid = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStaandStreamId).Select(a => a.StreamId).FirstOrDefault();
            //    ViewBag.StreamName = StreamRepository.GetAllStream().Where(a => a.ID == streamid).Select(a => a.Name).FirstOrDefault();

            //}
            ViewBag.SubjectName = SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == SubjectId).Select(a => a.Name).FirstOrDefault();





            //if (BoardStaandStreamId != 0)
            //{
            //    ViewData["Chapter"] = "StreamClassSubjectChaper";
            //}
            //else
            //{
            ViewData["Chapter"] = "ClassSubjectChapter";
            //}



            //if (BoardStaandStreamId != 0)
            //{
            //    ViewData["Subject"] = new[] { new SelectListItem { Text = "Select Subject", Value = "0" } }.Concat(new SelectList(CommonSchoolModel.GetAllStreamSubjects().Where(a => a.BoardStandardStreamId == BoardStaandStreamId), "ID", "Name"));

            //}
            //else
            //{
            ViewData["Subject"] = new[] { new SelectListItem { Text = "Select Subject", Value = "0" } }.Concat(new SelectList(CommonSchoolModel.GetAllSubjects().Where(a => a.BoardStandardId == BoardStnrdId), "ID", "Name"));

            //}

            ViewData["Chapter"] = new[] { new SelectListItem { Text = "Select Chapter", Value = "0" } }.Concat(new SelectList(ChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.SubjectId == SubjectId), "ID", "Name"));

            ChapterViewModel chapterViewModel = new ChapterViewModel();
            if (id == null)
            {
                ViewBag.Status = "Create";
            }
            else
            {
                ViewBag.Status = "Update";
                var chapter = CommonSchoolModel.GetAllChapters().FirstOrDefault(a => a.ID == id);
                chapterViewModel.ID = chapter.ID;
                chapterViewModel.Name = chapter.Name;
                chapterViewModel.SubjectId = chapter.SubjectId;
                chapterViewModel.IsSubChapter = chapter.IsSubChapter;
            }

            return View(chapterViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateChapter(long ID, string Name, long ParentChapterId, string Status, long BoardId, long BoardStandardId, long SubjectId, string Chapter)
        {


            ChapterViewModel chapterViewModel = new ChapterViewModel();
            chapterViewModel.ID = ID;
            chapterViewModel.Name = Name;
            chapterViewModel.SubjectId = SubjectId;
            chapterViewModel.ParentChapterId = ParentChapterId;
            chapterViewModel.Chapter = Chapter;
            chapterViewModel.BoardId = BoardId;
            chapterViewModel.BoardStandardId = BoardStandardId;
            //chapterViewModel.BoardStandardStreamId = BoardStandardStreamId;
            chapterViewModel.Status = Status;
            chapterViewModel.IsSubChapter = false;
            if (chapterViewModel.ParentChapterId > 0)
            {
                chapterViewModel.IsSubChapter = true;
            }


            var routeValues = new RouteValueDictionary { { "BoardId", chapterViewModel.BoardId }, { "BoardStnrdId", chapterViewModel.BoardStandardId }, { "BoardStaandStreamId", chapterViewModel.BoardStandardStreamId }, { "SubjectId", chapterViewModel.SubjectId } };




            if (ModelState.IsValid)
            {
                if (chapterViewModel.ID == 0)
                {
                    Chapter chapter = new Chapter
                    {
                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = EntityStatus.ACTIVE,
                        IsAvailable = true,
                        Name = chapterViewModel.Name,
                        SubjectId = chapterViewModel.SubjectId,
                        IsSubChapter = chapterViewModel.IsSubChapter,
                        ParentChapterId = chapterViewModel.ParentChapterId
                    };

                    await ChapterRepository.AddAsync(chapter);
                }
                else
                {
                    Chapter chapter = new Chapter
                    {
                        ID = chapterViewModel.ID,
                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = chapterViewModel.Status,
                        IsAvailable = true,
                        Name = chapterViewModel.Name,
                        SubjectId = chapterViewModel.SubjectId,
                        IsSubChapter = chapterViewModel.IsSubChapter,
                        ParentChapterId = chapterViewModel.ParentChapterId
                    };

                    await ChapterRepository.UpdateAsync(chapter);
                }
            }


            return RedirectToAction("Chapter", "School", routeValues);
        }


        //public async Task<IActionResult> DeleteChapter(long? id)
        //{
        //    var res = ChapterRepository.GetByIdAsyncIncludeAll(id.Value).Result;
        //    await ChapterRepository.DeleteAsync(res);

        //    return RedirectToAction(nameof(Chapter));   
        //}

        #endregion

        #region concept

        public IActionResult Concept(long BoardId, long BoardStnrdId, long boardStaandStreamId, long SubjectId, long ChapterId)
        {
            ViewData["BoardId"] = BoardId;
            ViewData["BoardStandardId"] = BoardStnrdId;
            //ViewData["BoardStaandStreamId"] = boardStaandStreamId;
            ViewData["SubjectId"] = SubjectId;
            ViewData["ChapterId"] = ChapterId;


            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
            var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStnrdId).Select(a => a.StandardId).FirstOrDefault();
            ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid).Select(a => a.Name).FirstOrDefault();
            //if (boardStaandStreamId != 0)
            //{
            //    var streamid = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == boardStaandStreamId).Select(a => a.StreamId).FirstOrDefault();
            //    ViewBag.StreamName = StreamRepository.GetAllStream().Where(a => a.ID == streamid).Select(a => a.Name).FirstOrDefault();

            //}
            ViewBag.SubjectName = SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == SubjectId).Select(a => a.Name).FirstOrDefault();
            ViewBag.ChapterName = ChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == ChapterId).Select(a => a.Name).FirstOrDefault();




            var res = CommonSchoolModel.GetAllConcept().Where(a => a.ChapterId == ChapterId);
            return View(res);

        }

        public IActionResult CreateOrEditConcept(long? id, long BoardId, long BoardStnrdId, long BoardStaandStreamId, long SubjectId, long ChapterId)
        {
            CheckLoginStatus();
            GetStatusList();
            ViewData["BoardId"] = BoardId;
            ViewData["BoardStandardId"] = BoardStnrdId;
            //ViewData["BoardStaandStreamId"] = BoardStaandStreamId;
            ViewData["SubjectId"] = SubjectId;
            ViewData["ChapterId"] = ChapterId;
            ViewBag.BoardName = BoardRepository.GetAllBoard().Where(a => a.ID == BoardId).Select(a => a.Name).FirstOrDefault();
            var Standardid = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStnrdId).Select(a => a.StandardId).FirstOrDefault();
            ViewBag.ClassName = StandardRepository.GetAllStandard().Where(a => a.ID == Standardid).Select(a => a.Name).FirstOrDefault();
            //if (BoardStaandStreamId != 0)
            //{
            //    var streamid = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStaandStreamId).Select(a => a.StreamId).FirstOrDefault();
            //    ViewBag.StreamName = StreamRepository.GetAllStream().Where(a => a.ID == streamid).Select(a => a.Name).FirstOrDefault();

            //}
            ViewBag.SubjectName = SubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == SubjectId).Select(a => a.Name).FirstOrDefault();
            ViewBag.ChapterName = ChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == ChapterId).Select(a => a.Name).FirstOrDefault();

            if (BoardStaandStreamId != 0)
            {
                ViewData["Chapter"] = "StreamClassSubjectChaper";
            }
            else
            {
                ViewData["Chapter"] = "ClassSubjectChapter";
            }

            ViewData["Concept"] = new[] { new SelectListItem { Text = "Select Concept", Value = "0" } }.Concat(new SelectList(conceptRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ChapterId == ChapterId), "ID", "Name"));

            CoceptViewModel conceptViewModel = new CoceptViewModel();
            if (id == null)
            {
                ViewBag.Status = "Create";
            }
            else
            {
                ViewBag.Status = "Update";
                var concept = CommonSchoolModel.GetAllConcept().FirstOrDefault(a => a.ID == id);
                conceptViewModel.ID = concept.ID;
                conceptViewModel.Name = concept.Name;
                conceptViewModel.IsSubConcept = concept.IsSubConcept;
                conceptViewModel.ParentConceptId = concept.ParentConceptId;
                conceptViewModel.Status = concept.Status;
            }

            return View(conceptViewModel);
        }



        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateConcept(long ID, string Name, long ParentConceptId, string Status, long BoardId, long BoardStandardId, long BoardStandardStreamId, long SubjectId, long ChapterId)
        {
            CheckLoginStatus();
            CoceptViewModel conceptViewModel = new CoceptViewModel();
            conceptViewModel.ID = ID;
            conceptViewModel.Name = Name;
            conceptViewModel.SubjectId = SubjectId;
            conceptViewModel.IsSubConcept = false;
            conceptViewModel.ParentConceptId = ParentConceptId;
            conceptViewModel.BoardId = BoardId;
            conceptViewModel.BoardStandardId = BoardStandardId;
            conceptViewModel.BoardStandardStreamId = BoardStandardStreamId;
            conceptViewModel.ChapterId = ChapterId;
            conceptViewModel.Status = Status;
            if (conceptViewModel.ParentConceptId > 0)
            {
                conceptViewModel.IsSubConcept = true;
            }

            var routeValues = new RouteValueDictionary { { "BoardId", conceptViewModel.BoardId }, { "BoardStnrdId", conceptViewModel.BoardStandardId }, { "BoardStaandStreamId", conceptViewModel.BoardStandardStreamId }, { "SubjectId", conceptViewModel.SubjectId }, { "ChapterId", conceptViewModel.ChapterId } };


            if (conceptViewModel.ID == 0)
            {
                Concept Concept = new Concept
                {
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = EntityStatus.ACTIVE,
                    IsAvailable = true,
                    Name = conceptViewModel.Name,
                    ChapterId = conceptViewModel.ChapterId,
                    IsSubConcept = conceptViewModel.IsSubConcept,
                    ParentConceptId = conceptViewModel.ParentConceptId
                };

                await conceptRepository.AddAsync(Concept);
                return RedirectToAction("Concept", "School", routeValues).WithSuccess("Sucess", "Saved Sucessfully.");
            }
            else
            {
                Concept Concept = new Concept
                {
                    ID = conceptViewModel.ID,
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = conceptViewModel.Status,
                    IsAvailable = true,
                    Name = conceptViewModel.Name,
                    ChapterId = conceptViewModel.ChapterId,
                    IsSubConcept = conceptViewModel.IsSubConcept,
                    ParentConceptId = conceptViewModel.ParentConceptId
                };

                await conceptRepository.UpdateAsync(Concept);
                return RedirectToAction("Concept", "School", routeValues).WithSuccess("Update", "Updated Sucessfully.");
            }


        }



        #endregion

        #region Teacher

        public async Task<IActionResult> Teachers()
        {
            var teacherList = teacherRepository.ListAllAsyncIncludeAll().Result;
            var teachertimelineList = teacherTimeLineRepository.ListAllAsyncIncludeAll().Result;
            var empList = employeeRepository.GetAllEmployee();
            var organisationlist = organizationRepository.GetAllOrganization();
            var grouplist = groupRepository.GetAllGroup();


            var res = (from a in teacherList
                       join b in empList on a.EmployeeId equals b.ID
                       join c in teachertimelineList on a.ID equals c.TeacherID
                       join d in organisationlist on a.OrganizationId equals d.ID
                       join e in grouplist on a.GroupId equals e.ID
                       where c.IsAvailable == true
                       select new TeacherViewModel
                       {
                           ID = a.ID,
                           EmpId = a.EmployeeId,
                           EmpName = b.FirstName + " " + b.MiddleName + " " + b.LastName,
                           EmpCode = b.EmpCode,
                           Status = a.Status,
                           ModifiedDate = a.ModifiedDate,
                           StartDateTime = NullableDateTimeToStringDate(c.StartDate),
                           EndDateTime = NullableDateTimeToStringDate(c.EndDate),
                           IsAvailable = c.EndDate != null ? (c.EndDate.Value.Date < DateTime.Now.Date ? false : true) : true,
                           GroupId = a.GroupId,
                           GroupName = e.Name,
                           OrganizationId = a.OrganizationId,
                           OrganizationName = d.Name,
                           StartDate = c.StartDate.Value,
                           EndDate = c.EndDate,
                           InsertedDate = a.InsertedDate,
                           ModifiedDateText = NullableDateTimeToStringDate(a.ModifiedDate)
                       });
            return View(res);
        }

        public IActionResult CreateTeacher()
        {
            //ViewBag.Employees = new[] { new SelectListItem { Text = "Select Employee", Value = "0" } }.Concat(new SelectList(employeeRepository.GetAllEmployee(), "ID", "FirstName"));
            //ViewBag.Organizations = new[] { new SelectListItem { Text = "Select Organization", Value = "0" } }.Concat(new SelectList(organizationRepository.GetAllOrganization(), "ID", "Name"));
            // ViewBag.Types = new[] { new SelectListItem { Text = "Group Type", Value = "2" }, new SelectListItem { Text = "Organization Type", Value = "1" } };
            ViewBag.Groups = new[] { new SelectListItem { Text = "Select Groups", Value = "0" } }.Concat(new SelectList(groupRepository.GetAllGroup(), "ID", "Name"));

            return View();
        }
        public JsonResult Getorganisation(long id)
        {
            return Json(organizationRepository.GetAllOrganizationByGroupId(id));
        }
        public async Task<IActionResult> SaveTeacher(long? id, long[] EmpId, long GroupId, long? organizationid, DateTime? StartDate, DateTime? EndDate)
        {
            CheckLoginStatus();

            ArrayList myList = new ArrayList(EmpId);
            foreach (long str in myList)
            {
                Teacher teacher = new Teacher();

                teacher.InsertedId = userId;
                teacher.ModifiedId = userId;
                teacher.InsertedDate = DateTime.Now;
                teacher.ModifiedDate = DateTime.Now;
                teacher.Active = true;
                teacher.Status = EntityStatus.ACTIVE;
                teacher.IsAvailable = true;
                teacher.OrganizationId = organizationid.Value;
                teacher.EmployeeId = str;
                teacher.GroupId = GroupId;
                await teacherRepository.AddAsync(teacher);
                long teacherid = teacher.ID;

                TeacherTimeLine timeline = new TeacherTimeLine();
                timeline.InsertedDate = DateTime.Now;
                timeline.TeacherID = teacherid;
                timeline.ModifiedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedId = userId;
                timeline.StartDate = StartDate;
                timeline.IsAvailable = true;
                timeline.Status = EntityStatus.ACTIVE;
                timeline.Active = true;
                timeline.EndDate = EndDate;
                await teacherTimeLineRepository.AddAsync(timeline);

            }
            return RedirectToAction(nameof(Teachers));
        }


        public async Task<IActionResult> EditTeacher(long? id)
        {
            GetStatusList();
            TeacherModel teacher = new TeacherModel();
            var res = teacherRepository.GetByIdAsyncIncludeAll(id.Value).Result;
            var timeline = teacherTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(a => a.TeacherID == id.Value);
            var emp = employeeRepository.GetEmployeeById(res.EmployeeId);
            ViewBag.EmployeeName = emp.FirstName + " " + emp.LastName + "-" + emp.EmpCode;


            teacher.ID = res.ID;
            teacher.EmployeeId = res.EmployeeId;
            teacher.StartDate = timeline.FirstOrDefault().StartDate;
            teacher.EndDate = timeline.FirstOrDefault().EndDate;
            teacher.OrganizationId = res.OrganizationId;
            teacher.GroupId = res.GroupId;
            teacher.IsAvailable = res.IsAvailable;
            ViewBag.Groups = new[] { new SelectListItem { Text = "Select Groups", Value = "0" } }.Concat(new SelectList(groupRepository.GetAllGroup(), "ID", "Name"));
            ViewBag.Organizations = new[] { new SelectListItem { Text = "Select Organization", Value = "0" } }.Concat(new SelectList(organizationRepository.GetAllOrganization().Where(a => a.GroupID == res.GroupId).ToList(), "ID", "Name"));

            return View(teacher);
        }
        public async Task<IActionResult> UpdateTeacher(long id, long GroupId, long? organizationid, DateTime? StartDate, DateTime? EndDate)
        {
            Teacher teacher = teacherRepository.GetByIdAsync(id).Result;
            teacher.ModifiedId = userId;
            teacher.ModifiedDate = DateTime.Now;
            teacher.OrganizationId = organizationid.Value;
            teacher.GroupId = GroupId;
            await teacherRepository.UpdateAsync(teacher);

            TeacherTimeLine timeline = teacherTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(a => a.TeacherID == id).FirstOrDefault();
            timeline.ModifiedDate = DateTime.Now;
            timeline.ModifiedId = userId;
            timeline.StartDate = StartDate;
            timeline.EndDate = EndDate;
            await teacherTimeLineRepository.UpdateAsync(timeline);

            return RedirectToAction(nameof(Teachers));
        }

        #endregion

        #region CalenderEngagementType

        public IActionResult CalenderEngagementType()
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("CalenderEngagementType", accessId, "List", "School", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }


            var res = (from a in calenderEngagementTypeRepository.ListAllAsync().Result
                       select new NameIdCountViewModel
                       {
                           ID = a.ID,
                           Name = a.Name,
                           OrgCount = calenderEngagementRepository.GetDayCountByTypeId(a.ID),
                           Active = a.Active,
                           Status = a.Status


                       }).ToList();


            return View(res);
        }



        public IActionResult CreateOrEditCalenderEngagementType(long? id)
        {
            CheckLoginStatus();
            GetStatusList();
            CalenderEngagementType od = new CalenderEngagementType();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("CalenderEngagementType", accessId, "Create", "School", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("CalenderEngagementType", accessId, "Edit", "School", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                od = calenderEngagementTypeRepository.GetByIdAsync(id.Value).Result;

            }
            return View(od);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateCalenderEngagementType([Bind("ID,Name,Status,InsertedId,ModifiedId,Active,InsertedDate,ModifiedDate")] CalenderEngagementType cetype)
        {

            CheckLoginStatus();
            if (ModelState.IsValid)
            {
                cetype.ModifiedId = userId;
                cetype.Active = true;
                if (cetype.ID == 0)
                {
                    cetype.InsertedDate = DateTime.Now;
                    cetype.ModifiedDate = DateTime.Now;
                    cetype.ModifiedId = userId;
                    cetype.InsertedId = userId;
                    cetype.Active = true;
                    cetype.Status = EntityStatus.ACTIVE;
                    await calenderEngagementTypeRepository.AddAsync(cetype);
                    return RedirectToAction(nameof(CalenderEngagementType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    cetype.ModifiedId = userId;
                    cetype.ModifiedDate = DateTime.Now;
                    if (cetype.Status == EntityStatus.INACTIVE)
                    {

                        cetype.Active = false;
                    }
                    else
                    {

                        cetype.Active = true;
                    }

                    await calenderEngagementTypeRepository.UpdateAsync(cetype);
                    return RedirectToAction(nameof(CalenderEngagementType)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(cetype).WithDanger("error", "Not Saved");

        }




        #endregion

        #region Jquery
        //[HttpGet]
        //public JsonResult GetStandardByBoardType(long BoardId)        
        //{
        //    try
        //    {
        //        var boardStandardList = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardId == BoardId);
        //        var standardList = StandardRepository.GetAllStandard();

        //        var res = (from a in standardList
        //                   join b in boardStandardList on a.ID equals b.StandardId
        //                   select a
        //                  ).ToList();

        //        var list = standardList.Except(res);

        //        return Json(list);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}

        public JsonResult GetUnselectedEmployee()
        {
            try
            {
                var teacherList = teacherRepository.ListAllAsyncIncludeAll().Result.Select(a => a.EmployeeId);
                var empList = employeeRepository.GetAllEmployee().Where(a => !teacherList.Contains(a.ID)).ToList();

                //var res = (from a in teacherList
                //           join b in empList on a.EmployeeId equals b.ID
                //           select a
                //           ).ToList();

                //var list = empList.Except(res);

                return Json(empList);




            }
            catch
            {
                return Json(null);
            }
        }

        //public async Task<JsonResult> GetStreamByStreamType(long? id)
        //{
        //    try
        //    {
        //        // var standardList = StandardRepository.GetAllStandard().FirstOrDefault(a => a.ID == id);

        //        var BoardList = BoardRepository.GetBoardById(id.Value);
        //        var standardList = StandardRepository.GetStandardById(id.Value);
        //        var BoardstandardList = BoardStandardRepository.GetByIdAsync(id.Value).Result;
        //        var BoardStandardStreamList = boardStandardStreamRepository.GetByIdAsync(id.Value).Result;
        //        var SubjectList = SubjectRepository.GetByIdAsync(id.Value);

        //        if (standardList.StreamType == true)
        //        {
        //            var streamList = "";
        //            //var streamList = (from a in SubjectList
        //            //                  join b in BoardstandardList on a.BoardStandardId equals b.ID
        //            //           select new SubjectViewModel
        //            //           {
        //            //               ID = a.ID,
        //            //               Name = a.Name,
        //            //               Status = a.Status,
        //            //               // ParentSubjectId = a.ParentSubjectId,
        //            //               SubjectCode = a.SubjectCode,
        //            //               BoardId = b.BoardId,
        //            //               BoardStandardId = b.ID
        //            //           }

        //            //    ).ToList();

        //            return Json(streamList);
        //        }
        //        else
        //        {
        //            return Json(null);
        //        }
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}


        //public JsonResult GetStreamByStandardType(long id)
        //{
        //    try
        //    {
        //        if (standard.StreamType == true)
        //        {
        //            var streamList = StreamRepository.GetAllStream();
        //            return Json(streamList);
        //        }

        //        var streamList = StreamRepository.GetAllStream();
        //        var standardList = StandardRepository.GetAllStandard().Where(a => a.ID == id);



        //        var res = (from a in standardList
        //                   join b in streamList on a.StreamType equals true
        //                   select b).ToList();

        //        var res = (from a in streamList
        //                   join b in standardList where b.StreamType == true
        //                   select a).ToList();



        //        return Json(null);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}



        #endregion

        #region CalenderEngagement

        public IActionResult CalenderEngagement()
        {


            return View();

        }


        public IActionResult CreateCalenderEngagement()
        {
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = "0" } }.
                Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result, "ID", "DisplayName"));
            ViewData["Types"] = new[] { new SelectListItem { Text = "Select Type", Value = "0" } }.Concat(
                new SelectList(calenderEngagementTypeRepository.ListAllAsyncIncludeAll().Result, "ID", "Name"));

            return View();

        }

        #endregion

        #region GetMethods

        public JsonResult GetAllClass(long BoardId)
        {
            var res = (from a in BoardStandardRepository.ListAllAsync().Result.Where(a => a.BoardId == BoardId).ToList()
                       join b in StandardRepository.GetAllStandard()
                       on a.StandardId equals b.ID
                       select new
                       {

                           ID = a.ID,
                           Name = b.Name,
                           BoardStandardId = a.ID
                       }


                ).ToList();

            return Json(res);
        }




        public JsonResult GetOrganizationsBySessionID(long SessionId)
        {
            AcademicDataModel data = new AcademicDataModel();

            var res = (from a in academicSessionRepository.ListAllAsync().Result.Where(a => a.ID == SessionId).ToList()
                       join b in organizationAcademicRepository.ListAllAsync().Result
                       on a.ID equals b.AcademicSessionId
                       join c in organizationRepository.GetAllOrganization().ToList()
                         on b.OrganizationId equals c.ID
                       select new NameIdViewModel
                       {

                           ID = b.ID,
                           Name = c.Name,


                       }


                ).ToList();

            AcademicSession session = academicSessionRepository.ListAllAsync().Result.Where(a => a.ID == SessionId).FirstOrDefault();
            data.data = res;
            data.Start = session.Start.Value.ToString("yyyy-MM-dd");
            data.End = session.End.Value.ToString("yyyy-MM-dd");
            return Json(data);
        }

        //public JsonResult GetWingsByOrganizationAcademicID(long OrganizationAcademicId)
        //{
        //    var res = (from a in organizationAcademicRepository.ListAllAsync().Result.Where(a => a.ID == OrganizationAcademicId).ToList()
        //               join b in studentAggregateRepository.GetAllSchoolWing().ToList()
        //               on a.ID equals b.OrganizationAcademicID
        //               join c in studentAggregateRepository.GetAllWing().ToList()
        //                 on b.WingID equals c.ID
        //               select new
        //               {

        //                   ID = b.ID,
        //                   Name = c.Name

        //               }


        //        ).ToList();

        //    return Json(res);
        //}



        //public JsonResult GetAllStream(long BoardStandardId)
        //{
        //    DataModel data = new DataModel();

        //    long cid = BoardStandardRepository.GetByIdAsync(BoardStandardId).Result.StandardId;

        //    var clas = StandardRepository.GetStandardById(cid);
        //    //if (clas != null && !clas.StreamType)
        //    //{
        //    //    data.Subjects = GetAllSubject(BoardStandardId, 0);
        //    //}
        //    //else
        //    //{
        //    data.Streams = (from a in boardStandardStreamRepository.ListAllAsync().Result.Where(a => a.BoardStandardId == BoardStandardId).ToList()
        //                    join b in StreamRepository.GetAllStream()
        //                    on a.StreamId equals b.ID
        //                    select new NameIdViewModel
        //                    {
        //                        ID = a.ID,
        //                        Name = b.Name
        //                    }


        //   ).ToList();
        //    //}



        //    return Json(data);
        //}

        public JsonResult GetAllStreamSubjects(long BoardStandardId, long BoardStandardStreamId)
        {

            return Json(GetAllSubject(BoardStandardId, BoardStandardStreamId));
        }
        public List<NameIdViewModel> GetAllSubject(long BoardStandardId, long BoardStandardStreamId)
        {
            if (BoardStandardStreamId == 0 && BoardStandardId > 0)
            {

                var subids = SubjectRepository.ListAllAsync().Result.
                 Where(a => a.BoardStandardId == BoardStandardId).Select(a => a.ID).ToList();
                var ACSUBIDS = academicSubjectRepository.ListAllAsync().Result.
              Where(a => subids.Contains(a.SubjectId)).Select(a => a.ID).ToList();

                var res = (from a in SubjectRepository.ListAllAsync().Result.Where(x => !ACSUBIDS.Contains(x.ID)).ToList()


                           select new NameIdViewModel
                           {

                               ID = a.ID,
                               Name = a.Name
                           }


               ).ToList();
                return res;
            }
            //else if (BoardStandardStreamId != 0 && BoardStandardId > 0)
            //{
            //    var subids = SubjectRepository.ListAllAsync().Result.
            //     Where(a => a.BoardStandardId == BoardStandardId &&
            //     a.BoardStandardStreamId == BoardStandardStreamId).Select(a => a.ID).ToList();


            //    var ACSUBIDS = academicSubjectRepository.ListAllAsync().Result.
            //    Where(a => subids.Contains(a.SubjectId)).Select(a => a.ID).ToList();

            //    var res = (from a in SubjectRepository.ListAllAsync().Result.
            //               Where(x => !ACSUBIDS.Contains(x.ID) && x.BoardStandardId == BoardStandardId &&
            //              x.BoardStandardStreamId == BoardStandardStreamId).ToList()
            //               select new NameIdViewModel
            //               {

            //                   ID = a.ID,
            //                   Name = a.Name
            //               }


            //   ).ToList();

            //    return res;
            //}
            return null;

        }

        public JsonResult GetAllStreamSubjectsList(long BoardStandardId)
        {
            var academicsubjectList = academicSubjectRepository.ListAllAsync().Result.ToList();
            if (BoardStandardId > 0)
            {
                var subjectlist = SubjectRepository.ListAllAsync().Result.
               Where(a => a.BoardStandardId == BoardStandardId).ToList();

                var list = (from a in subjectlist
                            join b in academicsubjectList on a.ID equals b.SubjectId
                            select a
                            ).ToList();
                var res = subjectlist.Except(list);

                return Json(res);
            }
            //else if (BoardStandardId > 0 && BoardStandardStreamId > 0)
            //{
            //    var subjectlist = SubjectRepository.ListAllAsync().Result.
            //   Where(a => a.BoardStandardId == BoardStandardId && a.BoardStandardStreamId == BoardStandardStreamId).ToList();

            //    var list = (from a in subjectlist
            //                join b in academicsubjectList on a.ID equals b.SubjectId
            //                select a).ToList();
            //    var res = subjectlist.Except(list);

            //    return Json(res);
            //}

            return Json(null);

        }

        #endregion



        #region CalenderAjaxMethods

        public async Task<JsonResult> SaveCalenderEngagement(CalenderDataModel model)
        {
            long status = 0;
            try
            {
                CheckLoginStatus();

                List<long> academicCalenderIds = new List<long>();

                foreach (long bid in model.BoardID)
                {
                    foreach (long sid in model.StandardID)
                    {
                        BoardStandard bs = BoardStandardRepository.ListAllAsync().Result.Where(a => a.BoardId == bid && a.StandardId == sid).FirstOrDefault();

                        //var sw = studentAggregateRepository.GetAllSchoolWing().
                        //    Where(a => a.OrganizationAcademicID == model.OrganizationID && a.WingID == a.WingID).FirstOrDefault();

                        AcademicStandard academicStandard = academicStandardRepository.ListAllAsync().Result.Where(a => a.BoardStandardId == bs.ID).FirstOrDefault();


                        if (academicStandard != null)
                        {



                            AcademicCalender calender = academicCalenderRepository.ListAllAsync().Result.Where(a => a.AcademicStandardId == academicStandard.ID).FirstOrDefault();

                            if (calender == null)
                            {
                                calender = new AcademicCalender();
                                //calender.SchoolWingId = academicStandard.SchoolWingId;
                                calender.AcademicStandardId = academicStandard.ID;
                                calender.ModifiedId = userId;
                                calender.ModifiedDate = DateTime.Now;
                                calender.InsertedDate = DateTime.Now;
                                calender.InsertedId = userId;
                                calender.Status = EntityStatus.ACTIVE;
                                calender.Active = true;

                                await academicCalenderRepository.AddAsync(calender);

                            }


                            CalenderEngagement calenderEngagement = await calenderEngagementRepository.GetByAcademicCalenderIdAndDate(calender.AcademicStandardId, model.CalenderDate.Date);

                            if (calenderEngagement != null && calenderEngagement.ID > 0)
                            {
                                //calenderEngagement = await calenderEngagementRepository.GetByIdAsync(model.Id);
                                calenderEngagement.ModifiedId = userId;
                                calenderEngagement.ModifiedDate = DateTime.Now;
                                calenderEngagement.CalenderEngagementTypeId = model.CalenderEngagementTypeID;
                                calenderEngagement.Name = model.Name;
                                calender.Status = EntityStatus.ACTIVE;
                                calender.Active = true;

                                await calenderEngagementRepository.UpdateAsync(calenderEngagement);

                                status = 1;
                            }
                            else
                            {

                                calenderEngagement = new CalenderEngagement();
                                calenderEngagement.ModifiedId = userId;
                                calenderEngagement.ModifiedDate = DateTime.Now;
                                calenderEngagement.InsertedDate = DateTime.Now;
                                calenderEngagement.InsertedId = userId;
                                calenderEngagement.Status = EntityStatus.ACTIVE;
                                calenderEngagement.Active = true;

                                calenderEngagement.AcademicCalenderId = calender.ID;
                                calenderEngagement.CalenderEngagementTypeId = model.CalenderEngagementTypeID;
                                calenderEngagement.Day = model.CalenderDate;
                                calenderEngagement.Name = model.Name;

                                CalenderEngagement saved = await calenderEngagementRepository.AddAsync(calenderEngagement);

                                status = saved.ID;
                            }

                        }
                    }
                }


            }
            catch (Exception e)
            {
                throw e;
            }



            return Json(status);
        }


        public JsonResult GetCalenderEngagementEvents(CalenderDataModel model)
        {
            var id = academicCalenderRepository.ListAllAsync().Result.Where(a => a.SchoolWingId == model.WingID).Select(a => a.ID).ToList();
            var data = (from a in calenderEngagementRepository.ListAllAsync().Result.Where(a => id.Contains(a.AcademicCalenderId))
                        join b in calenderEngagementTypeRepository.ListAllAsync().Result
                        on a.CalenderEngagementTypeId equals b.ID
                        select new
                        {
                            typeId = b.ID,
                            typeName = b.Name,
                            title = a.Name,
                            start = a.Day.ToString("yyyy-MM-dd"),
                            backgroundColor = b.BackgroundColor,
                            textColor = b.TextColor,


                        }

                      ).GroupBy(x => new { x.start, x.title, x.typeId, x.typeName, x.backgroundColor, x.textColor }).Select(y => new CalenderEventData
                      {

                          title = String.Join(",", y.Key.title),
                          start = y.Key.start,
                          end = y.Key.start,
                          allDay = true,
                          editable = true,
                          backgroundColor = y.Key.backgroundColor,
                          textColor =
                                  y.Key.textColor,
                          extendedProps = new ExtendedProperties
                                    (
                                        y.Key.typeId,
                                     y.Key.typeName,
                                      y.Key.title,
                                        y.Key.start
                                    )


                      }).ToList();



            return Json(data);
        }


        #endregion
        #region Academic Tearcher Subject


        public async Task<IActionResult> AcademicTearcherSubject()
        {
            var res = CommonSchoolModel.GetAllAcademicTearcherSubject();
            return View(res);
        }
        public async Task<IActionResult> CreateAcademicTearcherSubject()
        {
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = " " } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true), "ID", "DisplayName"));
            ViewData["BoardName"] = new[] { new SelectListItem { Text = "Select Board", Value = " " } }.Concat(new SelectList(BoardRepository.GetAllBoard(), "ID", "Name"));

            return View();
        }
        public async Task<JsonResult> GetTeacherList(long orgid)
        {
            var teacher = teacherRepository.ListAllAsync().Result;
            var teachertimeline = teacherTimeLineRepository.ListAllAsync().Result;
            var academicorganisation = organizationAcademicRepository.ListAllAsync().Result.Where(a => a.ID == orgid).FirstOrDefault().OrganizationId;
            var organisation = organizationRepository.GetAllOrganization();
            var employees = employeeRepository.GetAllEmployee();
            var res = (from a in teacher
                       join d in teachertimeline on a.ID equals d.TeacherID
                       join b in employees on a.EmployeeId equals b.ID
                       join c in organisation on a.OrganizationId equals c.ID
                       where c.ID == academicorganisation && (d.EndDate != null ? (d.EndDate.Value.Date < DateTime.Now.Date ? false : true) : true)
                       select new
                       {
                           id = a.ID,
                           name = b.FirstName + " " + b.LastName
                       }).ToList();
            return Json(res);
        }
        public async Task<JsonResult> GetSubjects(long academicstandardid)
        {
            var academicstandard = studentMethod.GetAcademicStandard().Where(a => a.ID == academicstandardid).FirstOrDefault();
            var academicsubjects = academicSubjectRepository.ListAllAsync().Result.Where(a => a.AcademicSessionId == academicstandard.AcademicSessionId && a.Active == true).ToList();
            var subjects = SubjectRepository.ListAllAsync().Result.Where(a => a.Active == true && a.BoardStandardId== academicstandard.BoardStandardId).ToList();
            var res = (from a in academicsubjects
                       join b in subjects on a.SubjectId equals b.ID
                       select new
                       {
                           id = a.ID,
                           name = b.Name
                       }).ToList();
            return Json(res);
        }
        [HttpPost]
        [ActionName("SaveAcademicTearcherSubject")]
        public async Task<IActionResult> SaveAcademicTearcherSubject(long[] ArrayAcademicSubjectId, long[] AcademicSectionId, long TeacherId)
        {
            CheckLoginStatus();
            ArrayList mysecList = new ArrayList(AcademicSectionId);
            ArrayList mysubList = new ArrayList(ArrayAcademicSubjectId);
            foreach (long a in mysecList)
            {
                foreach (long str in mysubList)
                {
                    AcademicTeacherSubject academicteachersubject = new AcademicTeacherSubject()
                    {
                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = EntityStatus.ACTIVE,
                        IsAvailable = true,
                        AcademicSectionId = a,
                        AcademicSubjectId = str,
                        TeacherId = TeacherId
                    };

                    await academicTeacherSubjectRepository.AddAsync(academicteachersubject);
                    var academicteachersubjectid = academicteachersubject.ID;

                    AcademicTeacherSubjectsTimeLine timeline = new AcademicTeacherSubjectsTimeLine()
                    {

                        InsertedId = userId,
                        ModifiedId = userId,
                        InsertedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Active = true,
                        Status = EntityStatus.ACTIVE,
                        IsAvailable = true,
                        AcademicTeacherSubjectId = academicteachersubjectid,
                        StartDate = DateTime.Now,
                        EndDate = null

                    };

                    await academicTeacherSubjectsTimeLineRepository.AddAsync(timeline);
                }
            }
            return RedirectToAction(nameof(AcademicTearcherSubject)).WithSuccess("Sucess", "Saved Sucessfully");

        }


        public IActionResult EditAcademicTearcherSubject(long id)
        {
            GetStatusList();
            var res = CommonSchoolModel.GetAllAcademicTearcherSubject().Where(a => a.ID == id).FirstOrDefault();

            return View(res);
        }
        [HttpPost]
        public async Task<IActionResult> EditAcademicTearcherSubject(long ID, string Status)
        {
            CheckLoginStatus();
            var res = academicTeacherSubjectRepository.ListAllAsyncIncludeAllNoTrack().Result.Where(a => a.ID == ID).FirstOrDefault();
            var subjecttimeline = academicTeacherSubjectsTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicTeacherSubjectId == ID).FirstOrDefault();

            if (ID != 0)
            {
                AcademicTeacherSubject academicteachersubject = academicTeacherSubjectRepository.GetByIdAsync(ID).Result;

                academicteachersubject.Active = Status=="ACTIVE"?true:false;
                academicteachersubject.ModifiedId = userId;
                academicteachersubject.ModifiedDate = DateTime.Now;
                academicteachersubject.Status = Status;

                await academicTeacherSubjectRepository.UpdateAsync(academicteachersubject);
                var academicteachersubjectstatus = academicteachersubject.Status;
                if (academicteachersubjectstatus == "INACTIVE")
                {

                    AcademicTeacherSubjectsTimeLine timeline = academicTeacherSubjectsTimeLineRepository.GetByIdAsync(subjecttimeline.ID).Result;

                    timeline.ModifiedId = userId;
                    timeline.ModifiedDate = DateTime.Now;
                    timeline.Status = EntityStatus.INACTIVE;
                    timeline.EndDate = DateTime.Now;
                    timeline.Active = Status == "ACTIVE" ? true : false;
                    await academicTeacherSubjectsTimeLineRepository.UpdateAsync(timeline);
                }
            }
            return RedirectToAction(nameof(AcademicTearcherSubject)).WithSuccess("Sucess", "Saved Sucessfully");

        }


        public JsonResult GetOrganisationByAcademicSessionId(long AcademicSessionId)                 //Filter the selected Standard
        {
            try
            {
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == AcademicSessionId);
                var OrganisationList = organizationRepository.GetAllOrganization();
                var res = (from a in OrganisationAcademic
                           join b in OrganisationList on a.OrganizationId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }
                        ).ToList();

                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        //public JsonResult GetWingByOrganisationAcademicId(long OrganisationAcademicId)                 //Filter the selected Standard
        //{
        //    try
        //    {
        //        var Schoolwing = commonMethods.GetSchoolWing().Where(a => a.OrganizationAcademicID == OrganisationAcademicId);
        //        var WingList = commonMethods.GetWing();
        //        var res = (from a in Schoolwing
        //                   join b in WingList on a.WingID equals b.ID
        //                   select new
        //                   {
        //                       id = a.ID,
        //                       name = b.Name
        //                   }
        //                ).ToList();

        //        return Json(res);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}


        public JsonResult GetClassBySchoolWingId(long OrganizationAcademicId, long BoardId)
        {
            try
            {
                var academicstandard = academicStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.OrganizationAcademicId == OrganizationAcademicId).ToList();
                var BoardStandardList = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardId == BoardId);
                var StandardList = StandardRepository.GetAllStandard();
                var res = (from a in academicstandard
                           join b in BoardStandardList on a.BoardStandardId equals b.ID
                           join c in StandardList on b.StandardId equals c.ID
                           select new
                           {
                               id = a.ID,
                               name = c.Name
                           }).ToList();
                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }

        //Get steam List By Class

        //public JsonResult GetStreamByAcademicStandardId(long AcademicStandardId)                 //Filter the selected Standard
        //{
        //    try
        //    {
        //        var academicstandardstreamid = academicStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicStandardId == AcademicStandardId);
        //        var boardstandardstream = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result;
        //        var stream = StreamRepository.GetAllStream();

        //        var res = (from a in academicstandardstreamid
        //                   join b in boardstandardstream on a.BoardStandardStreamId equals b.ID
        //                   join c in stream on b.StreamId equals c.ID
        //                   select new
        //                   {
        //                       id = a.ID,
        //                       name = c.Name
        //                   }
        //                ).ToList();

        //        return Json(res);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}

        //Get Section List By Stream
        //public JsonResult GetSectionByAcademicStandarStreamdId(long AcademicStandarStreamdId)                 //Filter the selected Standard
        //{
        //    try
        //    {
        //        var academicsectionList = academicSectionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicStandardStreamId == AcademicStandarStreamdId);
        //        var SectionList = sectionRepository.GetAllSection();
        //        var res = (from a in academicsectionList
        //                   join b in SectionList on a.SectionId equals b.ID

        //                   select new
        //                   {
        //                       id = a.ID,
        //                       name = b.Name
        //                   }
        //                ).ToList();

        //        return Json(res);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}

        //Get Section List By Class
        public JsonResult GetSectionByAcademicStandardId(long AcademicStandardId)                 //Filter the selected Standard
        {
            try
            {
                var academicsection = academicSectionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicStandardId == AcademicStandardId);

                var SectionList = sectionRepository.GetAllSection();
                var res = (from a in academicsection
                           join b in SectionList on a.SectionId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }
                        ).ToList();

                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        //Get Employee  List By section
        public JsonResult GetEmployeeByAcademicSectiondId(long AcademicSectiondId)                 //Filter the selected Standard
        {
            try
            {
                var AcademicTeacherList = academicTeacherRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSectionId == AcademicSectiondId);
                var EmployeeList = employeeRepository.GetAllEmployee();
                var res = (from a in AcademicTeacherList
                           join b in EmployeeList on a.EmployeeId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.FirstName
                           }
                        ).ToList();

                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        public JsonResult GetSubjectByAcademicSectiondId(long AcademicSessionId, long TeacherId, long AcademicSectiondId)                 //Filter the selected Standard
        {
            try
            {
                var academicteachersubjectlist = academicTeacherSubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.TeacherId == TeacherId && a.AcademicSectionId == AcademicSectiondId).Select(a => a.AcademicSubjectId);
                var AcademicSubjectList = academicSubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => !academicteachersubjectlist.Contains(a.ID) && a.AcademicSessionId == AcademicSessionId);
                var SubjectList = SubjectRepository.ListAllAsyncIncludeAll().Result;
                var res = (from a in AcademicSubjectList
                           join b in SubjectList on a.SubjectId equals b.ID
                           select new
                           {
                               acdemicsubjectid = a.ID,
                               name = b.Name,
                               code = b.SubjectCode,
                               boardstandardid = b.BoardStandardId,
                               //boardname=BoardRepository.GetAllBoard().Where(f=>f.ID== BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == b.BoardStandardId).Select(d => d.BoardId).FirstOrDefault()).Select(f=>f.Name).FirstOrDefault(),
                               //classname=StandardRepository.GetAllStandard().Where(f=>f.ID== BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == b.BoardStandardId).Select(d => d.StandardId).FirstOrDefault()).Select(f => f.Name).FirstOrDefault(),
                           }
                        ).ToList();

                return Json(res);
            }
            catch
            {
                return Json(null);
            }

        }


        public JsonResult GetCalenderEngagementTypes(long SelectedId)
        {

            var data =
                calenderEngagementTypeRepository.ListAllAsyncIncludeAll().Result;

            var standards = (from a in StandardRepository.GetAllStandard().ToList()

                             select new
                             {

                                 ID = a.ID,
                                 Name = a.Name
                             }


              ).ToList();

            var boards = (from a in BoardRepository.GetAllBoard().ToList()
                          select new
                          {

                              ID = a.ID,
                              Name = a.Name,

                          }


              ).ToList();


            var model = new
            {
                types = data,
                boards = boards,
                standards = standards
            };

            return Json(model);
        }

        #endregion




        #region AcademicTeacher

        public async Task<IActionResult> AcademicTeachers(long AcademicStandardId)
        {
            ViewBag.AcademicStandardId = AcademicStandardId;

            var academicTeachers = await academicTeacherRepository.ListAllAsyncIncludeAll();
            var academicTeacherTimeLine = await academicTeacherTimeLineRepository.ListAllAsyncIncludeAll();
            var empList = employeeRepository.GetAllEmployee();

            var res = (from a in academicTeachers
                       join b in empList on a.EmployeeId equals b.ID
                       join c in academicTeacherTimeLine on a.ID equals c.AcademicTeacherID
                       select new AcademicTeacherViewModel
                       {
                           ID = a.ID,
                           EmpName = b.FirstName + " " + b.MiddleName + " " + b.LastName,
                           StartDate = c.StartDate.Value,
                           EndDate = c.EndDate.Value,
                           Status = a.Status
                       }).ToList();

            return View(res);
        }

        public async Task<IActionResult> CreateAcademicTeacher()
        {

            ViewBag.emp = new[] { new SelectListItem { Text = "Select Employee", Value = "0" } }.Concat(new SelectList(employeeRepository.GetAllEmployee(), "ID", "FirstName"));


            return View();
        }

        public async Task<IActionResult> SaveAcademicTeacher(long? EmployeeId)
        {
            CheckLoginStatus();
            try
            {

                AcademicTeacher academicTeacher = new AcademicTeacher
                {
                    InsertedId = userId,
                    ModifiedId = userId,
                    InsertedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    Active = true,
                    Status = EntityStatus.ACTIVE,
                    IsAvailable = true,
                    EmployeeId = EmployeeId.Value,

                };
                await academicTeacherRepository.AddAsync(academicTeacher);

                AcademicTeacherTimeLine timeline = new AcademicTeacherTimeLine();
                timeline.InsertedDate = DateTime.Now;
                timeline.ModifiedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedId = userId;
                timeline.IsAvailable = true;
                timeline.Status = EntityStatus.ACTIVE;
                timeline.Active = true;
                timeline.AcademicTeacherID = academicTeacher.ID;
                timeline.StartDate = DateTime.Now;
                timeline.EndDate = DateTime.Now;
                await academicTeacherTimeLineRepository.AddAsync(timeline);

                return RedirectToAction(nameof(AcademicTeachers));
            }
            catch
            {
                return null;
            }
        }


        #endregion

        #region Parent Access
        public IActionResult ParentAccess(long id)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Parent Access", accessId, "Access", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.standardid = id;
            return View(commonMethods.GetStudentParentModuleAccess(6, id));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveOrUpdatearentModuleSubModuleDetails(EmployeeModuleSubModuleAccess employeeModuleSubModuleAccess, long standardid)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            var board = BoardStandardRepository.GetBoardIdByBoardStandardId(standardid);
            if (ModelState.IsValid)
            {
                foreach (var a in employeeModuleSubModuleAccess.modules)
                {
                    if (a.subModules != null)
                    {
                        foreach (var b in a.subModules)
                        {
                            if (b.actionSubModules != null)
                            {
                                foreach (var c in b.actionSubModules)
                                {
                                    if (c.IsRequired == true && c.ActionAccessId == 0)
                                    {
                                        ActionRole action = new ActionRole();
                                        action.ActionID = c.ID;
                                        action.Active = true;
                                        action.InsertedId = userId;
                                        action.ModifiedId = userId;
                                        action.ModuleID = a.ID;
                                        action.Status = EntityStatus.ACTIVE;
                                        action.SubModuleID = b.ID;
                                        action.RoleID = 6;
                                        action.StandardID = standardid;
                                        action.Name = "";
                                        actionAccessRepository.CreateActionRole(action);
                                    }
                                    else if (c.ActionAccessId != 0)
                                    {
                                        ActionRole actionAccess = actionAccessRepository.GetActionRoleById(c.ActionAccessId);
                                        if (actionAccess != null)
                                        {
                                            if (c.IsRequired == false)
                                            {
                                                actionAccessRepository.DeleteActionRole(actionAccess.ID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return RedirectToAction(nameof(BoardStandard), new { BoardID = board });
            }
            return RedirectToAction(nameof(BoardStandard), new { BoardID = board });
        }

        #endregion

        #region Category

        public async Task<IActionResult> Category()
        {
            try
            {
                var category = await categoryRepository.ListAllAsyncIncludeAll();
                if (category != null)
                {
                    return View(category);
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        public async Task<IActionResult> CreateOrEditCategory(long? id)
        {
            try
            {
                OdmErp.ApplicationCore.Entities.SchoolAggregate.Category cat = new OdmErp.ApplicationCore.Entities.SchoolAggregate.Category();

                if (id == null)
                {
                    ViewBag.status = "Create";
                    cat.ID = 0;
                    cat.Name = "";
                }
                else
                {
                    ViewBag.status = "Update";
                    var category = await categoryRepository.GetByIdAsyncIncludeAll(id.Value);
                    cat.ID = category.ID;
                    cat.Name = category.Name;
                }

                return View(cat);
            }
            catch
            {
                return null;
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateCategory(long? id, string name)
        {
            try
            {
                CheckLoginStatus();


                if (id == 0)
                {
                    var duplicate = categoryRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true && a.Name == name).ToList();
                    if (duplicate.Count() == 0)
                    {
                        OdmErp.ApplicationCore.Entities.SchoolAggregate.Category cat = new OdmErp.ApplicationCore.Entities.SchoolAggregate.Category();
                        cat.InsertedDate = DateTime.Now;
                        cat.InsertedId = userId;
                        cat.ModifiedDate = DateTime.Now;
                        cat.ModifiedId = userId;
                        cat.Active = true;
                        cat.Status = "ACTIVE";
                        cat.IsAvailable = true;
                        cat.Name = name;

                        await categoryRepository.AddAsync(cat);

                        return RedirectToAction(nameof(Category));
                    }
                    else
                    {
                        return RedirectToAction(nameof(Category));
                    }
                }
                else
                {
                    var duplicate = categoryRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true && a.ID != id && a.Name == name).ToList();
                    if (duplicate.Count() == 0)
                    {
                        var category = await categoryRepository.GetByIdAsyncIncludeAll(id.Value);
                        category.Name = name;
                        category.Status = "UPDATED";
                        category.ModifiedDate = DateTime.Now;

                        await categoryRepository.UpdateAsync(category);

                        return RedirectToAction(nameof(Category));
                    }
                    else
                    {
                        return RedirectToAction(nameof(Category));
                    }
                }

            }
            catch
            {
                return null;
            }
        }

        public async Task<IActionResult> DeleteCategory(long id)
        {
            try
            {
                var category = await categoryRepository.GetByIdAsyncIncludeAll(id);
                category.Status = "DELETED";
                category.Active = false;
                category.IsAvailable = false;

                await categoryRepository.UpdateAsync(category);

                return RedirectToAction(nameof(Category));
            }
            catch
            {
                return null;
            }
        }



        #endregion

    }

}