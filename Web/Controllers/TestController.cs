﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure;
using OdmErp.Infrastructure.Data;

namespace Web.Controllers
{
    public class TestController : Controller
    {
        private ITestRepository testRepository;

        private ApplicationDbContext applicationDbContext;

        public TestController(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public TestController(ITestRepository testRepository)
        {
            this.testRepository = testRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult TablesName()
        {
            var result = testRepository.GetAllTablesName() ;

            return View(result);
        }


        

    }
}