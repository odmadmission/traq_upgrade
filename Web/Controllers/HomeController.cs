﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OdmErp.Web.Models;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.ApplicationCore.Interfaces;

namespace OdmErp.Web.Controllers
{
   

    public class HomeController : Controller
    {
        private ICountryRepository countryRepository;
        public HomeController(ICountryRepository countryRepo)
        {
            countryRepository = countryRepo;
        }

        public IActionResult Index()
        {
            //commit

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
