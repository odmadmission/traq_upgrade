﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Infrastructure.RepositoryImpl.SchoolRepository;
using OdmErp.Web.Models;
using OdmErp.Web.Models.StudentPayment;
using Remotion.Linq.Clauses;
using Web.Controllers;
using X.PagedList;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using OfficeOpenXml;
using Microsoft.SqlServer.Management.XEvent;
using OdmErp.Web.Areas.Student.Models;
using static OdmErp.Web.Areas.Admission.Controllers.BasicRegistrationController;

namespace OdmErp.Web.Controllers
{
    public class ScholarshipController : AppController
    {
        private IScholarshipRepository scholarshipRepository;
        private ISessionScholarship sessionScholarship;
        private CommonMethods commonMethods;
        public Extensions.SessionExtensions sessionExtensions;
        public IAcademicSessionRepository academicSessionRepository;
        public IFeesTypeRepository feeTypeRepository;
        public IGroupRepository groupRepository;
        public IOrganizationRepository organizationRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public CommonSchoolModel CommonSchoolModel;
        private PaymentServices paymentServices;
        public IAcademicStandardRepository academicStandardRepository;
        public IBoardStandardRepository BoardStandardRepository;
        public IBoardRepository boardRepository;
        public IAccessRepository accessRepository;
        public IEmployeeRepository employeeRepository;
        public IStandardRepository StandardRepository;
        public ISessionScholarshipDiscountRepository sessionScholarshipDiscountRepository;
        public ISessionScholarshipDiscountPriceRepository sessionScholarshipDiscountPriceRepository;
        public ISessionScholarshipDiscountApplicableRepository sessionScholarshipDiscountApplicableRepository;
        public IScholarshipApprovalRepoaitory scholarshipApprovalRepository;
        public IScholarshipApplyRepository scholarshipApplyRepository;
        public IPaymentCollectionType paymentCollectionTypeRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        public IAcademicStudentRepository academicStudentRepository;
        public IAdmissionRepository admissionRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public ISessionFeesType sessionfeestypeRepository;
        public IWingRepository wingRepository;
        public commonsqlquery commonsql;
        public StudentSqlQuery studentSqlQuery;
        private readonly IFileProvider fileProvider;
        private readonly IHostingEnvironment hostingEnvironment;
        public ScholarshipController(IEmployeeRepository employeeRepo, IAccessRepository accessRepo, IScholarshipRepository scholarshipRepo, CommonSchoolModel commonSchoolModel, ISessionScholarship sessionScholarshipRepo, commonsqlquery commonsqlquery, CommonMethods commonMeth, IAcademicSessionRepository academicSessionRepo, IFeesTypeRepository feeTypeRepo, IGroupRepository groupRepo, IOrganizationRepository organizationRepo,
            IOrganizationAcademicRepository organizationAcademicRepo, IAcademicStandardRepository academicStandardRepo, IBoardStandardRepository boardStandardRepo,
            IStandardRepository StandardRepo, IBoardRepository boardRepo, ISessionScholarshipDiscountRepository sessionScholarshipDiscountRepo,
            ISessionScholarshipDiscountPriceRepository sessionScholarshipDiscountPriceRepo,
            ISessionScholarshipDiscountApplicableRepository sessionScholarshipDiscountApplicableRepo, IScholarshipApprovalRepoaitory scholarshipApprovalRepo, IScholarshipApplyRepository scholarshipApplyRepo, IPaymentCollectionType paymentCollectionTypeRepo, IStudentAggregateRepository studentAggregateRepo, IAcademicStudentRepository academicStudentRepo, IAdmissionRepository admissionRepo, IBoardStandardWingRepository boardStandardWingRepo, IAcademicStandardWingRepository academicStandardWingRepo, IWingRepository wingRepo, ISessionFeesType sessionfeestypeRepo, IFileProvider fileProvider, IHostingEnvironment hostingEnvironment, PaymentServices paymentServ, StudentSqlQuery studentSql)
        {
            employeeRepository = employeeRepo;
            accessRepository = accessRepo;
            studentSqlQuery = studentSql;
            scholarshipRepository = scholarshipRepo;
            sessionScholarship = sessionScholarshipRepo;
            commonsql = commonsqlquery;
            commonMethods = commonMeth;
            CommonSchoolModel = commonSchoolModel;
            academicSessionRepository = academicSessionRepo;
            feeTypeRepository = feeTypeRepo;
            groupRepository = groupRepo;
            organizationRepository = organizationRepo;
            organizationAcademicRepository = organizationAcademicRepo;
            academicStandardRepository = academicStandardRepo;
            BoardStandardRepository = boardStandardRepo;
            StandardRepository = StandardRepo;
            boardRepository = boardRepo;
            sessionScholarshipDiscountRepository = sessionScholarshipDiscountRepo;
            sessionScholarshipDiscountPriceRepository = sessionScholarshipDiscountPriceRepo;
            sessionScholarshipDiscountApplicableRepository = sessionScholarshipDiscountApplicableRepo;
            scholarshipApprovalRepository = scholarshipApprovalRepo;
            scholarshipApplyRepository = scholarshipApplyRepo;
            paymentCollectionTypeRepository = paymentCollectionTypeRepo;
            studentAggregateRepository = studentAggregateRepo;
            academicStudentRepository = academicStudentRepo;
            admissionRepository = admissionRepo;
            boardStandardWingRepository = boardStandardWingRepo;
            academicStandardWingRepository = academicStandardWingRepo;
            wingRepository = wingRepo;
            sessionfeestypeRepository = sessionfeestypeRepo;
            this.fileProvider = fileProvider;
            this.hostingEnvironment = hostingEnvironment;
            paymentServices = paymentServ;
        }
        #region -----Scholarship-----
        public IActionResult Index()
        {

            return View();
        }
        private bool CheckExistance(string names)
        {
            var name_exists = scholarshipRepository.GetScholarshipByName(names);
            if (name_exists != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        [HttpPost]
        public IActionResult LoadData()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            //Get Sort columns value
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;
            PaginationScholarship objpg = scholarshipRepository.GetPaginationScholarship(draw, sortColumn, sortColumnDir, pageSize, skip, totalRecords, searchValue);
            return Json(objpg);
        }
        [HttpGet]
        public JsonResult CreateOrEditScholarship(long? id)
        {
            CheckLoginStatus();
            ApplicationCore.Entities.Scholarship scholar = new ApplicationCore.Entities.Scholarship();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Scholarship", accessId, "Create", "Scholarship", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Create";
                scholar.ID = 0;
                scholar.Name = "";
            }
            else
            {
                //if (commonMethods.checkaccessavailable("Scholarship", accessId, "Edit", "Scholarship", roleId) == false)
                //{
                //    //return Redirect("AuthenticationFailed", "Accounts");
                //}

                ViewBag.status = "Update";
                var scholarship = scholarshipRepository.GetScholarshipById(id.Value);
                scholar.ID = scholarship.ID;
                scholar.Name = scholarship.Name;
                scholar.Description = scholarship.Description;
                scholar.Status = scholarship.Status;

            }
            return Json(scholar);
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SaveOrUpdateScholarship(CS_Scholarship scholarship)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            try
            {
                userId = HttpContext.Session.GetInt32("userId").Value;
                //CS_Scholarship objScholarship = new CS_Scholarship();
                if (ModelState.IsValid)
                {

                    OdmErp.ApplicationCore.Entities.Scholarship scholar = new Scholarship();

                    scholar.Name = scholarship.Name;
                    scholar.Description = scholarship.Description;
                    scholar.Active = true;
                    scholar.IsAvailable = true;
                    if (scholar.ID == 0)
                    {
                        scholar.InsertedId = userId;
                        scholar.Status = scholarship.Status;

                        scholarship.ReturnMsg = scholar.Name + " added Successfully.";
                        if (ScholarshipExists(scholar.Name) == true)
                        {
                            scholarshipRepository.CreateScholarship(scholar);
                        }
                        else
                        {
                            scholarship.ReturnMsg = scholar.Name + " already Exists !!!";
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                scholarship.ReturnMsg = ex.Message;
            }

            return Json(scholarship); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }
        public JsonResult Update(CS_Scholarship scholarship)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            try
            {
                userId = HttpContext.Session.GetInt32("userId").Value;
                if (ModelState.IsValid)
                {

                    OdmErp.ApplicationCore.Entities.Scholarship objScholarship = new Scholarship();
                    objScholarship.ID = scholarship.ID;
                    objScholarship.Name = scholarship.Name;
                    objScholarship.Description = scholarship.Description;
                    objScholarship.Status = scholarship.Status;


                    objScholarship.Active = true;

                    //TODO
                    objScholarship.ModifiedId = userId;

                    if (ScholarshipExists(objScholarship.Name) == true)
                    {
                        scholarshipRepository.UpdateScholarship(objScholarship);
                        scholarship.ReturnMsg = scholarship.Name + " Updated Successfully.";
                    }
                    else
                    {
                        scholarship.ReturnMsg = scholarship.Name + " already Exists !!!";
                    }


                    //scholarship.ReturnMsg = "Records Updated Successfully.";
                    //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Updated successfully");
                    //return Json("");



                }
            }
            catch (Exception Ex)
            {
                scholarship.ReturnMsg = Ex.Message;
            }

            return Json(scholarship); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }
        public JsonResult ActiveDeactiveScholarship(string id, string IsActive)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {

                var sessioschalar = scholarshipRepository.GetScholarshipById(Convert.ToInt64(id));
                OdmErp.ApplicationCore.Entities.Scholarship objScholarship = new Scholarship();
                sessioschalar.ID = Convert.ToInt64(id);
                sessioschalar.Status = IsActive;

                sessioschalar.Active = true;

                //TODO
                sessioschalar.ModifiedId = userId;
                scholarshipRepository.UpdateScholarship(sessioschalar);
                objScholar.ReturnMsg = sessioschalar.Name + " " + sessioschalar.Status.ToLower() + " Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }
        public JsonResult DeleteScholarship(long? id)
        {
            CheckLoginStatus();
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Delete", "StudentPayment", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            if (ScholarshipExists(id.Value) == true)
            {
                var feesTypePrice = scholarshipRepository.DeleteScholarship(id.Value);
            }
            return Json("Deleted successfully");
        }
        private bool ScholarshipExists(long id)
        {
            if (scholarshipRepository.GetScholarshipById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool ScholarshipExists(string res)
        {
            if (scholarshipRepository.GetScholarshipByName(res) != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region -----SessionScholarship-----
        private bool SessionScholarshipExists(long id)
        {
            if (sessionScholarship.GetSessionScholarshipById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool SessionScholarshipExists(string res)
        {
            if (sessionScholarship.GetSessionScholarshipByName(res) != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public IActionResult SessionScholarship()
        {

            return View();

        }

        [HttpPost]
        public IActionResult LoadSessionScholarshipData()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            //Get Sort columns value
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int totalRecords = 0;
            PaginationSessionScholarship objpg = sessionScholarship.GetPaginationSessionScholarship(draw, sortColumn, sortColumnDir, pageSize, skip, totalRecords, searchValue);
            return Json(objpg);
        }


        public List<AcademicSession> Get_Session()
        {
            IEnumerable<AcademicSession> objAcademicSession = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true);
            var academicse = (from a in objAcademicSession.ToList()
                              select new AcademicSession
                              {
                                  ID = a.ID,
                                  DisplayName = a.DisplayName
                              }).ToList();

            return academicse;
        }
        public JsonResult Get_SessionWithDefault()
        {
            IEnumerable<AcademicSession> objAcademicSession = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true);
            var academicse = (from a in objAcademicSession.ToList()
                              select new
                              {
                                  ID = a.ID,
                                  DisplayName = a.DisplayName
                              }).ToList();

            var defaultSession = objAcademicSession.ToList().Where(a => a.IsAvailable == true).FirstOrDefault();

            return Json(new
            {
                AcademicSession = academicse,
                isRedirect = defaultSession.ID
            });
            // return Json( academicse);
        }
        public List<Scholarship> Get_AllScholarship()
        {

            IEnumerable<Scholarship> objScholarship = scholarshipRepository.GetAllScholarshipData().Where(a => a.IsAvailable == true);
            var scholarship = (from a in objScholarship.ToList()
                               select new Scholarship
                               {
                                   ID = a.ID,
                                   Name = a.Name
                               }).ToList();


            return scholarship;
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SaveOrUpdateSessionScholaship(string sessionid, string[] schorshipId)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            var sdfsf = schorshipId;
            userId = HttpContext.Session.GetInt32("userId").Value;
            OdmErp.ApplicationCore.Entities.Scholarship scholar = new Scholarship();
            CS_Scholarship scholarship = new CS_Scholarship();
            var sessionscholarships = sessionScholarship.GetAllSessionScholarship();

            for (int i = 0; i < schorshipId.Length; i++)
            {
                var sessionExist = (from a in sessionscholarships where a.SessionId == Convert.ToInt64(sessionid) && a.ScholarshipId == Convert.ToInt64(schorshipId[i]) select a).Count();
                if (sessionExist > 0)
                {
                    scholarship.ReturnMsg = "Some Scholarships are associated with Academic Session.";
                    return Json(scholarship);
                }

            }
            for (int j = 0; j < schorshipId.Length; j++)
            {
                SessionScholarship objsessionscholship = new SessionScholarship();
                objsessionscholship.ScholarshipId = Convert.ToInt64(schorshipId[j]);
                objsessionscholship.SessionId = Convert.ToInt64(sessionid);
                objsessionscholship.Active = true;
                objsessionscholship.IsAvailable = true;
                objsessionscholship.InsertedId = userId;
                objsessionscholship.Status = EntityStatus.ACTIVE;
                sessionScholarship.CreateSessionScholarship(objsessionscholship);
            }

            scholarship.ReturnMsg = "Records added Successfully.";
            //}           
            return Json(scholarship); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }
        public JsonResult UpdateSessionScholarship(string id, string IsActive, string ScholarshipId)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (ModelState.IsValid)
            {
                var sessioschalar = sessionScholarship.GetSessionScholarshipById(Convert.ToInt64(id));
                OdmErp.ApplicationCore.Entities.SessionScholarship objSessionScholarship = new SessionScholarship();
                sessioschalar.ID = Convert.ToInt64(id);
                sessioschalar.Status = IsActive;


                var ListSessionScholarship = (sessionScholarship.GetAllSessionScholarship().Where(a => a.SessionId == sessioschalar.SessionId && a.ScholarshipId == Convert.ToInt64(ScholarshipId))).Count();
                if (ListSessionScholarship > 0)
                {
                    objScholar.ReturnMsg = "Already the Scholarship is associated with the Session.";
                }
                else
                {
                    sessioschalar.Active = true;
                    sessioschalar.ScholarshipId = Convert.ToInt64(ScholarshipId);
                    //TODO
                    sessioschalar.ModifiedId = userId;
                    sessionScholarship.UpdateSessionScholarship(sessioschalar);
                    objScholar.ReturnMsg = "Records Updated Successfully.";

                }

            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }
        //ActiveDeactiveSessionScholarship
        public JsonResult ActiveDeactiveSessionScholarship(string id, string IsActive)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {

                var sessioschalar = sessionScholarship.GetSessionScholarshipById(Convert.ToInt64(id));
                OdmErp.ApplicationCore.Entities.SessionScholarship objSessionScholarship = new SessionScholarship();
                sessioschalar.ID = Convert.ToInt64(id);
                sessioschalar.Status = IsActive;

                sessioschalar.Active = true;

                //TODO
                sessioschalar.ModifiedId = userId;
                sessionScholarship.UpdateSessionScholarship(sessioschalar);
                objScholar.ReturnMsg = "Records Updated Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }
        [HttpGet]
        public JsonResult EditSessionScholarship(long? id)
        {
            CheckLoginStatus();
            ApplicationCore.Entities.SessionScholarship Sessionscholar = new ApplicationCore.Entities.SessionScholarship();
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Scholarship", accessId, "Create", "Scholarship", roleId) == false)
                {

                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Update";
                var sessionscholarships = sessionScholarship.GetAllSessionScholarship();
                var academicSessionsList = academicSessionRepository.GetAllAcademicSession();

                var scholarships = scholarshipRepository.GetAllScholarshipData();
                var allsessionscolarshipdata = (from a in sessionscholarships
                                                where a.ID == id.Value
                                                join b in academicSessionsList on a.SessionId equals b.ID
                                                join c in scholarships on a.ScholarshipId equals c.ID
                                                select new
                                                {
                                                    id = a.ID,
                                                    sessionName = b.DisplayName,
                                                    scholarshipName = c.Name,
                                                    scholarshipId = a.ScholarshipId,
                                                    status = a.Status
                                                }).ToList();
                //   return View(allsessionscolarshipdata);
                return Json(allsessionscolarshipdata);
            }
            else
            {
                return Json(null);
            }


        }

        #endregion

        #region SessionScholarshipDiscount
        public IActionResult SessionScholarshipDiscount()
        {

            return View();
        }
        public List<Models.StudentPayment.CS_SessionScholarship> Get_SessionSchalaship()
        {
            var sessionScholaarship = sessionScholarship.GetAllSessionScholarship();
            var session = academicSessionRepository.GetAllAcademicSession();
            var scholarship = scholarshipRepository.GetAllScholarshipData();
            List<Models.StudentPayment.CS_SessionScholarship> obj = (from a in sessionScholaarship where a.Status == "ACTIVE" join b in session on a.SessionId equals b.ID join c in scholarship on a.ScholarshipId equals c.ID select new Models.StudentPayment.CS_SessionScholarship { id = a.ID, sessionName = b.DisplayName, scholarshipName = c.Name, sessionId = b.ID, status = a.Status }).ToList();

            return obj;
        }
        public List<AcademicSession> AcademicSession()
        {
            //List<AcademicSession> feesType = new List<AcademicSession>();
            // IEnumerable<AcademicSession> academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true);
            // myGenericEnumerable = academicSessionRepository.GetAllAcademicSession().Where(c => c.IsAvailable == true).ToList();
            var res = (from a in academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true).ToList()
                       select new AcademicSession
                       {
                           ID = a.ID,
                           DisplayName = a.DisplayName,
                           IsAvailable = a.IsAvailable
                       }).ToList();
            return res;

        }
        public List<Board> GetBoardData()
        {
            //List<Board> boardObj = new List<Board>();
            //IEnumerable<Board> myGenericEnumerable = (IEnumerable<Board>)boardObj;
            //myGenericEnumerable = boardRepository.GetAllBoard();
            var res = (from a in boardRepository.GetAllBoard()
                       select new Board
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            //boardObj = res;
            return res;
        }
        public JsonResult GetOrganisationByAcademicSession(long AcademicSessionId)                 //Filter the selected Standard
        {
            try
            {
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == AcademicSessionId);
                var OrganisationList = organizationRepository.GetAllOrganization();
                var academicstandard = academicStandardRepository.ListAllAsyncIncludeAll().Result;

                var res = (from a in OrganisationAcademic
                           join b in OrganisationList on a.OrganizationId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }
                          ).ToList();



                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        public List<FeesType> Get_FeeTypes(long sessionid)
        {
            var sessionFeeType = sessionfeestypeRepository.GetAllSessionFeesType().Where(a => a.SessionId == sessionid).ToList();
            var feeTypeList = feeTypeRepository.GetAllFeesType().Where(a => a.Active == true && a.ParentFeeTypeID == 0).ToList();
            var feeTypes = (from a in sessionFeeType
                            join b in feeTypeList on a.FeesTypeId equals b.ID
                            select new FeesType
                            {
                                ID = a.ID,
                                Name = b.Name,
                                ParentFeeTypeName = b.ParentFeeTypeName

                            }).ToList();
    
            return feeTypes;
        }
        public OdmErp.Web.Models.StudentPayment.CS_SessionScholarship GetSessionById(long SessionScholarshipId)
        {
            var obj = (from a in sessionScholarship.GetAllSessionScholarship().Where(a => a.Status == "ACTIVE" && a.ID == SessionScholarshipId) join b in academicSessionRepository.GetAllAcademicSession() on a.SessionId equals b.ID select new OdmErp.Web.Models.StudentPayment.CS_SessionScholarship { sessionId = b.ID }).FirstOrDefault();
            return obj;
        }
        public List<Group> Get_Groups()
        {
            var groupList = groupRepository.GetAllGroup().Where(a => a.Status == "ACTIVE").ToList();
            return groupList;
        }
        public List<Board> GetAllBoard()
        {
            var groupList = boardRepository.GetAllBoard().Where(a => a.Status == "ACTIVE").ToList();
            return groupList;
        }



        //public JsonResult GetWingBySchoolWing(long OraganisationAcademicId)
        //{
        //    try
        //    {
        //        var SchoolWing = commonMethods.GetSchoolWing().Where(a => a.OrganizationAcademicID == OraganisationAcademicId);
        //        var wingList = commonMethods.GetWing();
        //        var res = (from a in SchoolWing
        //                   join b in wingList on a.WingID equals b.ID
        //                   select new
        //                   {
        //                       id = a.ID,
        //                       name = b.Name
        //                   }).ToList();
        //        return Json(res);
        //    }
        //    catch
        //    {
        //        return Json(null);
        //    }
        //}
        public JsonResult GetBoardSchoolByOrgId(long OraganisationAcademicId)
        {
            try
            {
                //var orgAcademics = academicStandardRepository.ListAllAsync().Result.ToList().Where(a=>a.OrganizationAcademicId== OraganisationAcademicId).Select(a=>a.ID);
                var orgAcademics = academicStandardRepository.ListAllAsync().Result.
                   Where(a => a.OrganizationAcademicId == OraganisationAcademicId && a.Active == true).ToList();
                var boardStandards = BoardStandardRepository.ListAllAsync().Result;
                var BoardWithclass = (from a in orgAcademics
                                      join b in boardStandards on a.BoardStandardId equals b.ID
                                      select new
                                      {
                                          b.ID,
                                          ClassName = StandardRepository.GetStandardById(b.StandardId).Name,
                                          b.StandardId,
                                          boardName = boardRepository.GetBoardById(b.BoardId).Name,
                                          b.BoardId,
                                          boardStandardName = StandardRepository.GetStandardById(b.StandardId).Name + "--" + boardRepository.GetBoardById(b.BoardId).Name
                                      }).ToList();
                //var standardsBoards = BoardStandardRepository.ListAllAsync().
                //    Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
                //var standards = (from a in standardsBoards
                //                 //where a.ID == id
                //                 select new
                //                 {
                //                     boardClass = StandardRepository.GetStandardById(a.StandardId).Name,
                //                     boardId = a.BoardId,
                //                     boardName = boardRepository.GetBoardById(a.BoardId).Name,
                //                     standardId = a.StandardId,
                //                     boardStandardId = a.ID,
                //                     boardStandardName = boardRepository.GetBoardById(a.BoardId).Name + "--" + StandardRepository.GetStandardById(a.StandardId).Name
                //                 }
                //            ).ToList();
                //var SchoolWing = commonMethods.GetSchoolWing().Where(a => a.OrganizationAcademicID == OraganisationAcademicId);
                //var wingList = commonMethods.GetWing();
                //var res = (from a in SchoolWing
                //           join b in wingList on a.WingID equals b.ID
                //           select new
                //           {
                //               id = a.ID,
                //               name = b.Name
                //           }).ToList();
                return Json(BoardWithclass);
            }
            catch
            {
                return Json(null);
            }
        }
        public JsonResult GetWingByBoardStandard(long BoardStandardId, long OraganisationAcademicId, long AcademicSessionId)
        {

            //return Json(new { schooolwingname = schoolWingNames, nucleus = nucleusBool });
            var standardsBoards = BoardStandardRepository.ListAllAsync().
              Result.Where(a => a.ID == BoardStandardId && a.Active == true).ToList();
            //var nucleusBool = admissionRepository.GetAllAdmissionStandardSetting().Where(a => a.BoardStandardId == BoardStandardId).Select(a => a.Nucleus);
            var standardsBoardswing = boardStandardWingRepository.ListAllAsync().
                 Result.Where(a => a.BoardStandardID == BoardStandardId && a.Active == true).ToList();
            List<WingOrgModel> list = new List<WingOrgModel>();
            var academicstandard = academicStandardRepository.ListAllAsync().Result;
            var academicstandardwing = academicStandardWingRepository.ListAllAsync().Result;
            var organisation = organizationRepository.GetAllOrganization();
            //var session = academicSessionRepository.ListAllAsync().Result.Where(a => a.IsAdmission == true && a.IsAvailable == true).FirstOrDefault();
            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                 Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.Active == true).ToList();
            var wings = wingRepository.ListAllAsync().Result;
            var boardStandards = academicstandard.
                   Where(a => a.OrganizationAcademicId == OraganisationAcademicId && a.Active == true && a.BoardStandardId == BoardStandardId).FirstOrDefault();
            if (boardStandards != null)
            {
                foreach (var st in standardsBoardswing)
                {
                    var academicwing = academicstandardwing.Where(a => a.AcademicStandardId == boardStandards.ID && a.Active == true && a.BoardStandardWingID == st.ID).FirstOrDefault();
                    if (academicwing != null)
                    {
                        var wingname = wings.Where(a => a.ID == st.WingID).FirstOrDefault().Name;
                        // var organisationname = organisation.Where(a => a.ID == OraganisationAcademicId).FirstOrDefault().Name;
                        WingOrgModel od = new WingOrgModel();
                        od.AcademicStandardWindId = academicwing.ID;
                        od.Name = wingname;
                        list.Add(od);
                    }
                }
            }
            //foreach (var org in orgAcademics)
            //{
            //    var boardStandards = academicstandard.
            //        Where(a => a.OrganizationAcademicId == org.ID && a.Active == true && a.BoardStandardId == BoardStandardId ).FirstOrDefault();
            //    if (boardStandards != null)
            //    {
            //        foreach (var st in standardsBoardswing)
            //        {
            //            var academicwing = academicstandardwing.Where(a => a.AcademicStandardId == boardStandards.ID && a.Active == true && a.BoardStandardWingID == st.ID).FirstOrDefault();
            //            if (academicwing != null)
            //            {
            //                var wingname = wings.Where(a => a.ID == st.WingID).FirstOrDefault().Name;
            //                var organisationname = organisation.Where(a => a.ID == org.OrganizationId).FirstOrDefault().Name;
            //                WingOrgModel od = new WingOrgModel();
            //                od.AcademicStandardWindId = academicwing.ID;
            //                od.Name = organisationname + "-" + wingname;
            //                od.SchoolId = org.ID;
            //                od.WingId = st.WingID;
            //                list.Add(od);
            //            }
            //        }
            //    }
            //}
            return Json(new { schooolwingname = list });

            //    }
            //        catch (Exception ex)
            //        {
            //            return Json(0);
            //}

        }
        public JsonResult GetClassByBoard(long BoardId, long OrganizationAcademicId)
        {
            try
            {
                var academicstandard = academicStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.OrganizationAcademicId == OrganizationAcademicId && a.Active == true).Select(a => a.BoardStandardId);
                var BoardStandardList = BoardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardId == BoardId && !academicstandard.Contains(a.ID));
                var StandardList = StandardRepository.GetAllStandard();


                var res = (from a in BoardStandardList
                           join b in StandardList on a.StandardId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }
                          ).ToList();



                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }

        public JsonResult SaveSessionScholashipDiscountSingle(string sessionScholarshipId, string discountName, string discountDesc)
        {
            try
            {
                SessionScholarshipDiscount objSessionScholarshipDiscount = new SessionScholarshipDiscount();
                objSessionScholarshipDiscount.Name = discountName;
                objSessionScholarshipDiscount.Description = discountDesc;
                objSessionScholarshipDiscount.SessionScholarshipId = Convert.ToInt64(sessionScholarshipId);
                objSessionScholarshipDiscount.Active = true;
                objSessionScholarshipDiscount.IsAvailable = true;
                objSessionScholarshipDiscount.InsertedId = userId;
                objSessionScholarshipDiscount.Status = EntityStatus.ACTIVE;
                long sessiondiscountId = sessionScholarshipDiscountRepository.CreateSessionScholarshipDiscount(objSessionScholarshipDiscount);
                if (sessiondiscountId > 0)
                {
                    return Json("Data Added Sucessfully.");
                }
                else
                {
                    return Json("Data is not added");
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return Json(msg);
            }

        }
        [HttpPost]
        public JsonResult SaveSessionScholashipDiscount(string sessionScholarshipId, string discountName, string discountDesc, string DiscountPercentage, string[] feeTypeIdArray, string[] classId)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Add", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });

            }

            try
            {
                SessionScholarshipDiscount objSessionScholarshipDiscount = new SessionScholarshipDiscount();
                objSessionScholarshipDiscount.Name = discountName;
                objSessionScholarshipDiscount.Description = discountDesc;
                objSessionScholarshipDiscount.SessionScholarshipId = Convert.ToInt64(sessionScholarshipId);
                objSessionScholarshipDiscount.Active = true;
                objSessionScholarshipDiscount.IsAvailable = true;
                objSessionScholarshipDiscount.InsertedId = userId;
                objSessionScholarshipDiscount.Status = EntityStatus.ACTIVE;
                //sessionScholarship.CreateSessionScholarship(objsessionscholship);
                long sessiondiscountId = sessionScholarshipDiscountRepository.CreateSessionScholarshipDiscount(objSessionScholarshipDiscount);
                if (sessiondiscountId > 0)
                {
                    for (int i = 0; i < feeTypeIdArray.Length; i++)
                    {
                        SessionScholarshipDiscountPrice objSSDP = new SessionScholarshipDiscountPrice();
                        objSSDP.SessionScholarshipDiscountId = sessiondiscountId;
                        objSSDP.DiscountPercentage = Convert.ToInt64(DiscountPercentage);
                        objSSDP.SessionFeesTypeId = Convert.ToInt64(feeTypeIdArray[i]);
                        objSSDP.Active = true;
                        objSSDP.IsAvailable = true;
                        objSSDP.InsertedId = userId;
                        objSSDP.Status = EntityStatus.ACTIVE;
                        long sessionScholarshipDiscountPriceId = sessionScholarshipDiscountPriceRepository.CreateSessionScholarshipDiscountPrice(objSSDP);
                        for (int j = 0; j < classId.Length; j++)
                        {
                            SessionScholarshipDiscountApplicable objSSDA = new SessionScholarshipDiscountApplicable();
                            objSSDA.SessionScholarshipDiscountPriceId = sessionScholarshipDiscountPriceId;
                            objSSDA.AcademicStandardId = Convert.ToInt64(classId[j]);
                            objSSDA.Active = true;
                            objSSDA.IsAvailable = true;
                            objSSDA.InsertedId = userId;
                            objSSDA.Status = EntityStatus.ACTIVE;
                            long sessionScholarshipDiscountApplicableId = sessionScholarshipDiscountApplicableRepository.CreateSessionScholarshipDiscountApplicable(objSSDA);
                        }
                    }

                }
                return Json("Data Added Sucessfully.");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        public JsonResult UpdateSessionScholashipDiscount(string sessionScholarshipId, string sessionScholarshipDiscountid, string discountName, string discountDesc, string DiscountPercentage, string[] feeTypeIdArray, string[] classId)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Add", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });

            }
            try
            {
                var sessionScholarshipdiscountid = Convert.ToInt64(sessionScholarshipDiscountid);
                var getSessionDetails = sessionScholarshipDiscountRepository.GetSessionScholarshipDiscountById(sessionScholarshipdiscountid);
                // SessionScholarshipDiscount objSessionScholarshipDiscount = new SessionScholarshipDiscount();
                getSessionDetails.Description = discountDesc;
                getSessionDetails.Name = discountName;
                getSessionDetails.ModifiedDate = DateTime.Now;
                getSessionDetails.ModifiedId = userId;
                bool entry = sessionScholarshipDiscountRepository.UpdateSessionScholarshipDiscount(getSessionDetails);
                var allsessionscholarshipdiscountprice = sessionScholarshipDiscountPriceRepository.GetAllSessionScholarshipDiscountPrice().Where(a => a.SessionScholarshipDiscountId == sessionScholarshipdiscountid).ToList();
                //string[] feetypelist=null;
                List<string> DBfeetypelist = new List<string>();
                List<string> Inputfeetypelist = new List<string>();
                for (int j = 0; j < allsessionscholarshipdiscountprice.Count(); j++)
                {
                    DBfeetypelist.Add(allsessionscholarshipdiscountprice[j].SessionFeesTypeId.ToString());

                }
                for (int l = 0; l < feeTypeIdArray.Count(); l++)
                {
                    Inputfeetypelist.Add(feeTypeIdArray[l]);
                }

                var removed = DBfeetypelist.Except(Inputfeetypelist).ToList();
                var added = Inputfeetypelist.Except(DBfeetypelist).ToList();
                var unchanged = DBfeetypelist.Intersect(Inputfeetypelist).ToList();
                for (int a = 0; a < removed.Count(); a++)
                {
                    var removedval = Convert.ToInt64(removed[a]);
                    var getSessionscholarshipPrice = sessionScholarshipDiscountPriceRepository.GetAllSessionScholarshipDiscountPrice().Where(b => b.SessionFeesTypeId == removedval && b.Status == "ACTIVE" && b.SessionScholarshipDiscountId == sessionScholarshipdiscountid).SingleOrDefault();
                    getSessionscholarshipPrice.Status = "DEACTIVE";
                    getSessionscholarshipPrice.DiscountPercentage = Convert.ToInt64(DiscountPercentage);
                    getSessionscholarshipPrice.ModifiedDate = DateTime.Now;
                    getSessionscholarshipPrice.ModifiedId = userId;
                    sessionScholarshipDiscountPriceRepository.UpdateSessionScholarshipDiscountPrice(getSessionscholarshipPrice);
                    var getAllscholarshipdiscountapplicable = sessionScholarshipDiscountApplicableRepository.GetAllSessionScholarshipDiscountApplicable().Where(d => d.SessionScholarshipDiscountPriceId == getSessionscholarshipPrice.ID).ToList();
                    for (int k = 0; k < getAllscholarshipdiscountapplicable.Count; k++)
                    {
                        var objss = sessionScholarshipDiscountApplicableRepository.GetSessionScholarshipDiscountApplicableById(getAllscholarshipdiscountapplicable[k].ID);
                        objss.Status = "DEACTIVE";
                        objss.ModifiedDate = DateTime.Now;
                        objss.ModifiedId = userId;
                        sessionScholarshipDiscountApplicableRepository.UpdateSessionScholarshipDiscountApplicable(objss);
                    }
                }
                for (int b = 0; b < added.Count(); b++)
                {
                    SessionScholarshipDiscountPrice ssdp = new SessionScholarshipDiscountPrice();
                    ssdp.Active = true;
                    ssdp.DiscountPercentage = Convert.ToInt64(DiscountPercentage);
                    ssdp.SessionFeesTypeId = Convert.ToInt64(added[b]);
                    ssdp.SessionScholarshipDiscountId = sessionScholarshipdiscountid;
                    ssdp.IsAvailable = true;
                    ssdp.InsertedId = userId;
                    ssdp.Status = "ACTIVE";
                    long sessionScholarshipDiscountPriceId = sessionScholarshipDiscountPriceRepository.CreateSessionScholarshipDiscountPrice(ssdp);
                    for (int j = 0; j < classId.Length; j++)
                    {
                        SessionScholarshipDiscountApplicable objSSDA = new SessionScholarshipDiscountApplicable();
                        objSSDA.SessionScholarshipDiscountPriceId = sessionScholarshipDiscountPriceId;
                        objSSDA.AcademicStandardId = Convert.ToInt64(classId[j]);
                        objSSDA.Active = true;
                        objSSDA.IsAvailable = true;
                        objSSDA.InsertedId = userId;
                        objSSDA.Status = EntityStatus.ACTIVE;
                        long sessionScholarshipDiscountApplicableId = sessionScholarshipDiscountApplicableRepository.CreateSessionScholarshipDiscountApplicable(objSSDA);
                    }
                }
                for (int c = 0; c < unchanged.Count(); c++)
                {
                    var unchangedval = Convert.ToInt64(unchanged[c]);
                    var getSessionscholarshipPrice = sessionScholarshipDiscountPriceRepository.GetAllSessionScholarshipDiscountPrice().Where(b => b.SessionFeesTypeId == unchangedval && b.Status == "ACTIVE" && b.SessionScholarshipDiscountId == sessionScholarshipdiscountid).SingleOrDefault();
                    if (getSessionscholarshipPrice != null)
                    {
                        getSessionscholarshipPrice.DiscountPercentage = Convert.ToInt64(DiscountPercentage);
                        getSessionscholarshipPrice.ModifiedDate = DateTime.Now;
                        getSessionscholarshipPrice.ModifiedId = userId;
                        sessionScholarshipDiscountPriceRepository.UpdateSessionScholarshipDiscountPrice(getSessionscholarshipPrice);
                        for (int m = 0; m < classId.Length; m++)
                        {
                            var ssdaCount = sessionScholarshipDiscountApplicableRepository.GetAllSessionScholarshipDiscountApplicable().Where(z => z.SessionScholarshipDiscountPriceId == getSessionscholarshipPrice.ID && z.AcademicStandardId == Convert.ToInt64(classId[m]) && z.Status == "ACTIVE").ToList();
                            if (ssdaCount.Count == 0)
                            {
                                SessionScholarshipDiscountApplicable objSSDA = new SessionScholarshipDiscountApplicable();
                                objSSDA.SessionScholarshipDiscountPriceId = getSessionscholarshipPrice.ID;
                                objSSDA.AcademicStandardId = Convert.ToInt64(classId[m]);
                                objSSDA.Active = true;
                                objSSDA.IsAvailable = true;
                                objSSDA.InsertedId = userId;
                                objSSDA.Status = EntityStatus.ACTIVE;
                                long sessionScholarshipDiscountApplicableId = sessionScholarshipDiscountApplicableRepository.CreateSessionScholarshipDiscountApplicable(objSSDA);
                            }

                        }
                    }
                    else
                    {
                        SessionScholarshipDiscountPrice ssdp = new SessionScholarshipDiscountPrice();
                        ssdp.Active = true;
                        ssdp.DiscountPercentage = Convert.ToInt64(DiscountPercentage);
                        ssdp.SessionFeesTypeId = unchangedval;
                        ssdp.SessionScholarshipDiscountId = sessionScholarshipdiscountid;
                        ssdp.IsAvailable = true;
                        ssdp.InsertedId = userId;
                        ssdp.Status = "ACTIVE";
                        long sessionScholarshipDiscountPriceId = sessionScholarshipDiscountPriceRepository.CreateSessionScholarshipDiscountPrice(ssdp);
                        for (int j = 0; j < classId.Length; j++)
                        {
                            SessionScholarshipDiscountApplicable objSSDA1 = new SessionScholarshipDiscountApplicable();
                            objSSDA1.SessionScholarshipDiscountPriceId = sessionScholarshipDiscountPriceId;
                            objSSDA1.AcademicStandardId = Convert.ToInt64(classId[j]);
                            objSSDA1.Active = true;
                            objSSDA1.IsAvailable = true;
                            objSSDA1.InsertedId = userId;
                            objSSDA1.Status = EntityStatus.ACTIVE;
                            long sessionScholarshipDiscountApplicableId = sessionScholarshipDiscountApplicableRepository.CreateSessionScholarshipDiscountApplicable(objSSDA1);
                        }
                    }


                }
                return Json("Data Added Sucessfully.");


            }
            catch (Exception ex)
            {
                return Json("Data is not updated.");
            }

        }
        public List<CommonMasterModel> GetAcademicWingList(long AcademicStandardId)
        {
            var academicstandardwing = CommonSchoolModel.GetacademicstandardWings(AcademicStandardId).ToList();

            var res = (from a in academicstandardwing
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return res;
        }

        public IActionResult GetClassFeetypeBySessionScholarshipId(string SessionScholarshipId, string SessionId)
        {
            var sessionScholarshipDiscountId = Convert.ToInt64(SessionScholarshipId);

            var AcademicSessionId = Convert.ToInt64(SessionId);
            var scholarship = scholarshipRepository.GetAllScholarshipData().Where(a => a.Status == "ACTIVE");
            //var sessionScholarshipDetails = sessionScholarship.GetSessionScholarshipById(sessionScholarshipId);
            var sessionScholarshipDiscount = sessionScholarshipDiscountRepository.GetSessionScholarshipDiscountById(sessionScholarshipDiscountId);
            var sessionScholarshipDiscountPrice = sessionScholarshipDiscountPriceRepository.GetAllSessionScholarshipDiscountPrice().Where(a => a.Status == "ACTIVE").ToList();
            var sessionScholarshipDiscountApplicableList = sessionScholarshipDiscountApplicableRepository.GetAllSessionScholarshipDiscountApplicable().Where(a => a.Status == "ACTIVE").ToList();
            var feeTypes = Get_FeeTypes(AcademicSessionId);
            var academicStandarSingle = academicStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result/*.Where(a => a.ID == 1).FirstOrDefault()*/ ;
            var boardData = BoardStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result;
            var organizationData = organizationAcademicRepository.ListAllAsyncIncludeAll().Result;
            var OrganisationList1 = organizationRepository.GetAllOrganization();
            var wingList = commonMethods.GetWing();
            var StandardList1 = StandardRepository.GetAllStandard();
            var boardList = boardRepository.GetAllBoard().ToList();
            var alldata = (from b in sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount()
                           where b.ID == sessionScholarshipDiscountId
                           select new
                           {

                               sessionScholarshipId = b.SessionScholarshipId,
                               sessionScholarshipDiscountId = b.ID,
                               sessionScholarshipDiscountName = b.Name,
                               sessionScholarshipDiscountDesc = b.Description,
                               allFeetype = (from c in sessionScholarshipDiscountPrice
                                             where c.SessionScholarshipDiscountId == b.ID
                                             join d in feeTypes on c.SessionFeesTypeId equals d.ID
                                             select new
                                             {
                                                 sessionScholarshipDiscountPriceId = c.ID,
                                                 feeId = c.SessionFeesTypeId,
                                                 discountPrice = c.DiscountPercentage,
                                                 feeTypeName = d.Name,
                                                 feetypeParent = d.ParentFeeTypeName,
                                                 allApplicable = (from e in sessionScholarshipDiscountApplicableList
                                                                  where e.SessionScholarshipDiscountPriceId == c.ID
                                                                  select new
                                                                  {
                                                                      sessionScholarshipDiscountApplicableId = e.ID,
                                                                      academicStandard = e.AcademicStandardId,
                                                                      e.Status,
                                                                      joindata = (from x in academicStandarSingle
                                                                                  where (x.ID == e.AcademicStandardId)
                                                                                  join y in boardData on x.BoardStandardId equals y.ID
                                                                                  join z in organizationData on x.OrganizationAcademicId equals z.ID
                                                                                  where z.AcademicSessionId == AcademicSessionId
                                                                                  // join w in SchoolWingData on z.ID equals w.OrganizationAcademicID
                                                                                  // join u in wingList on w.WingID equals u.ID
                                                                                  select new
                                                                                  {
                                                                                      y.BoardId,
                                                                                      y.StandardId,
                                                                                      z.ID,
                                                                                      z.OrganizationId,

                                                                                      // w.WingID,
                                                                                      //GetBoardName=from c in boardList
                                                                                      // GetBoardName = (from p in boardList where p.ID == y.BoardId select p.Name).SingleOrDefault(),
                                                                                      //GetWingName = (from q in GetAcademicWingList(y.StandardId) select q.Name).SingleOrDefault(),

                                                                                      //w.WingID,
                                                                                      //GetBoardName=from c in boardList
                                                                                      GetBoardName = (from p in boardList where p.ID == y.BoardId select p.Name).SingleOrDefault(),
                                                                                      //GetWingName = (from q in wingList where q.ID == w.WingID select q.Name).SingleOrDefault(),
                                                                                      GetOrganizationName = (from r in OrganisationList1 where r.ID == z.OrganizationId select r.Name).SingleOrDefault(),
                                                                                      GetStandardId = (from f in StandardList1 where f.ID == y.StandardId select f.ID).SingleOrDefault(),
                                                                                      GetStandardName = (from f in StandardList1 where f.ID == y.StandardId select f.Name).SingleOrDefault(),
                                                                                  })
                                                                  })


                                             })

                           });

            var allApplicable1 = (from e in sessionScholarshipDiscountApplicableList
                                  where e.SessionScholarshipDiscountPriceId == 2
                                  select new
                                  {
                                      sessionScholarshipDiscountApplicableId = e.ID,
                                      academicStandard = e.AcademicStandardId,

                                  });

            return Json(alldata);
        }
        public IActionResult GetScholarshiBySession(string SessionId)
        {
            var sessionId = Convert.ToInt64(SessionId);

            var sessionScholaarship = sessionScholarship.GetAllSessionScholarship();
            var session = academicSessionRepository.GetAllAcademicSession();
            var scholarship = scholarshipRepository.GetAllScholarshipData().Where(a => a.Status == "ACTIVE");
            var sessionScholarshipDiscount = sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount();
            var obj = (from a in sessionScholaarship
                       where a.Status == "ACTIVE" && a.SessionId == sessionId
                       join b in session on a.SessionId equals b.ID
                       join c in scholarship on a.ScholarshipId equals c.ID
                       select
                       new
                       {
                           id = a.ID,
                           sessionName = b.DisplayName,
                           scholarshipName = c.Name,
                           sessionId = b.ID,
                           status = a.Status,
                           SessionScholarshipDiscounts = (from e in sessionScholarshipDiscount where e.SessionScholarshipId == a.ID && e.Status == "ACTIVE" select e)

                       }).ToList();

            return Json(obj);
        }
        public IActionResult GetScholarshiBySessionDefault(string SessionId)
        {
            var sessionId = Convert.ToInt64(SessionId);

            var sessionScholaarship = sessionScholarship.GetAllSessionScholarship();
            var session = academicSessionRepository.GetAllAcademicSession();
            var scholarship = scholarshipRepository.GetAllScholarshipData().Where(a => a.Status == "ACTIVE");
            var sessionScholarshipDiscount = sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount();
            var obj = (from a in sessionScholaarship
                       where a.Status == "ACTIVE" && a.SessionId == sessionId
                       join b in session on a.SessionId equals b.ID
                       join c in scholarship on a.ScholarshipId equals c.ID
                       select
                       new
                       {
                           id = a.ID,
                           sessionName = b.DisplayName,
                           scholarshipName = c.Name,
                           sessionId = b.ID,
                           status = a.Status,
                           SessionScholarshipDiscounts = (from e in sessionScholarshipDiscount where e.SessionScholarshipId == a.ID && e.Status == "ACTIVE" select e),
                           selectDefault = (from f in sessionScholarshipDiscount where f.SessionScholarshipId == a.ID && f.Status == "ACTIVE" select f).FirstOrDefault()

                       }).ToList();

            return Json(obj);
        }
        public IActionResult GetFeeTypeClassDetails(string SessionScholarshipDiscountId, string SessionId)
        {
            var SSDI = Convert.ToInt64(SessionScholarshipDiscountId);
            var AcademicSessionId = Convert.ToInt64(SessionId);
            var boardList = boardRepository.GetAllBoard().ToList();
            var academicStandarSingle = academicStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result/*.Where(a => a.ID == 1).FirstOrDefault()*/ ;
            var boardData = BoardStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result;
            var organizationData = organizationAcademicRepository.ListAllAsyncIncludeAll().Result;
            //var SchoolWingData = commonMethods.GetSchoolWing();
            var OrganisationList1 = organizationRepository.GetAllOrganization();
            var wingList = commonMethods.GetWing();
            var StandardList1 = StandardRepository.GetAllStandard();
            var feeTypes = Get_FeeTypes(AcademicSessionId);
            var SSDAList = sessionScholarshipDiscountApplicableRepository.GetAllSessionScholarshipDiscountApplicable();
            var sessionScholarshipDiscountPrice = sessionScholarshipDiscountPriceRepository.GetAllSessionScholarshipDiscountPrice().Where(a => a.Status == "ACTIVE").ToList();
            var alldata = (from a in sessionScholarshipDiscountPrice
                           where a.Status == "ACTIVE" && a.SessionScholarshipDiscountId == SSDI
                           join c in feeTypes on a.SessionFeesTypeId equals c.ID
                           select
                           new
                           {
                               sessionScholarshipDiscountPriceId = a.ID,
                               discountPrice = a.DiscountPercentage,
                               feeTypeId = a.SessionFeesTypeId,
                               feeId = c.ID,
                               parentFeeType = c.ParentFeeTypeName,
                               feeTypeName = c.Name,
                           });
            return Json(alldata);
        }

        public IActionResult GetFeeTypeClassDetailsWithDefault(string SessionScholarshipDiscountId, string SessionId)
        {
            var SSDI = Convert.ToInt64(SessionScholarshipDiscountId);
            var AcademicSessionId = Convert.ToInt64(SessionId);
            var boardList = boardRepository.GetAllBoard().ToList();
            var academicStandarSingle = academicStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result/*.Where(a => a.ID == 1).FirstOrDefault()*/ ;
            var boardData = BoardStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result;
            var organizationData = organizationAcademicRepository.ListAllAsyncIncludeAll().Result;
            var OrganisationList1 = organizationRepository.GetAllOrganization();
            var wingList = commonMethods.GetWing();
            var StandardList1 = StandardRepository.GetAllStandard();
            var feeTypes = Get_FeeTypes(AcademicSessionId);
            var SSDAList = sessionScholarshipDiscountApplicableRepository.GetAllSessionScholarshipDiscountApplicable();
            var sessionScholarshipDiscountPrice = sessionScholarshipDiscountPriceRepository.GetAllSessionScholarshipDiscountPrice().Where(a => a.Status == "ACTIVE").ToList();
            var alldata = (from a in sessionScholarshipDiscountPrice
                           where a.Status == "ACTIVE" && a.SessionScholarshipDiscountId == SSDI
                           join c in feeTypes on a.SessionFeesTypeId equals c.ID
                           select
                           new
                           {
                               sessionScholarshipDiscountPriceId = a.ID,
                               discountPrice = a.DiscountPercentage,
                               feeTypeId = a.SessionFeesTypeId,
                               feeId = c.ID,
                               parentFeeType = c.ParentFeeTypeName,
                               feeTypeName = c.Name,
                           });
            return Json(alldata);
        }
        public IActionResult GetClassDetailsByFeetypeDiscount(string SessionScholarshipDiscountPriceId, string SessionId)
        {
            var SSDPId = Convert.ToInt64(SessionScholarshipDiscountPriceId);
            var AcademicSessionId = Convert.ToInt64(SessionId);
            var boardList = boardRepository.GetAllBoard().ToList();
            var academicStandarSingle = academicStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result/*.Where(a => a.ID == 1).FirstOrDefault()*/ ;
            var boardData = BoardStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result;
            var organizationData = organizationAcademicRepository.ListAllAsyncIncludeAll().Result;
            //var SchoolWingData = commonMethods.GetSchoolWing();
            var OrganisationList1 = organizationRepository.GetAllOrganization();
            var wingList = commonMethods.GetWing();
            var StandardList1 = StandardRepository.GetAllStandard();
            var feeTypes = Get_FeeTypes(AcademicSessionId);
            var SSDAList = sessionScholarshipDiscountApplicableRepository.GetAllSessionScholarshipDiscountApplicable().Where(a => a.Status == "ACTIVE").ToList();
            var sessionScholarshipDiscountPrice = sessionScholarshipDiscountPriceRepository.GetAllSessionScholarshipDiscountPrice().Where(a => a.Status == "ACTIVE").ToList();
            var AllAccademicDetails = (from b in SSDAList
                                       where b.ID == SSDPId
                                       select
                                   new
                                   {
                                       SSDAId = b.ID,
                                       academicStandId = b.AcademicStandardId,
                                       joindata = (from x in academicStandarSingle
                                                   where (x.ID == b.AcademicStandardId)
                                                   join y in boardData on x.BoardStandardId equals y.ID
                                                   join z in organizationData on x.OrganizationAcademicId equals z.ID
                                                   where z.AcademicSessionId == AcademicSessionId
                                                   select new
                                                   {
                                                       y.BoardId,
                                                       y.StandardId,
                                                       z.ID,
                                                       z.OrganizationId,
                                                       //w.WingID,
                                                       //GetBoardName=from c in boardList
                                                       GetBoardName = (from p in boardList where p.ID == y.BoardId select p.Name).SingleOrDefault(),
                                                       GetWingName = (from q in GetAcademicWingList(y.StandardId) select q.Name).SingleOrDefault(),
                                                       GetOrganizationName = (from r in OrganisationList1 where r.ID == z.OrganizationId select r.Name).SingleOrDefault(),
                                                       GetStandardName = (from d in StandardList1 where d.ID == y.StandardId select d.Name).SingleOrDefault(),
                                                   })

                                   });
            return Json(AllAccademicDetails);
        }


        //ActiveDeactiveSessionScholarshipDiscountApplicable

        public JsonResult ActiveDeactiveSessionScholarshipDiscountApplicable(string id, string IsActive)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {

                var sessioschalarDiscountApplicable = sessionScholarshipDiscountApplicableRepository.GetSessionScholarshipDiscountApplicableById(Convert.ToInt64(id));
                //OdmErp.ApplicationCore.Entities.SessionScholarshipDiscount objSessionScholarship = new ApplicationCore.Entities.SessionScholarshipDiscount();
                sessioschalarDiscountApplicable.ID = Convert.ToInt64(id);
                sessioschalarDiscountApplicable.Status = IsActive;

                sessioschalarDiscountApplicable.Active = true;

                //TODO
                sessioschalarDiscountApplicable.ModifiedId = userId;
                sessionScholarshipDiscountApplicableRepository.UpdateSessionScholarshipDiscountApplicable(sessioschalarDiscountApplicable);
                objScholar.ReturnMsg = "Records Deactivaed Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }


        public JsonResult ActiveDeactiveSessionScholarshipDiscount(string id, string IsActive)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {

                var sessioschalarDiscount = sessionScholarshipDiscountRepository.GetSessionScholarshipDiscountById(Convert.ToInt64(id));
                //OdmErp.ApplicationCore.Entities.SessionScholarshipDiscount objSessionScholarship = new ApplicationCore.Entities.SessionScholarshipDiscount();
                sessioschalarDiscount.ID = Convert.ToInt64(id);
                sessioschalarDiscount.Status = IsActive;

                sessioschalarDiscount.Active = true;

                //TODO
                sessioschalarDiscount.ModifiedId = userId;
                sessionScholarshipDiscountRepository.UpdateSessionScholarshipDiscount(sessioschalarDiscount);
                objScholar.ReturnMsg = "Records Deactivaed Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        public JsonResult ActiveDeactiveSessionScholarshipDiscountPrice(string id, string IsActive)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {

                var sessioschalarDiscount = sessionScholarshipDiscountPriceRepository.GetSessionScholarshipDiscountPriceById(Convert.ToInt64(id));
                //OdmErp.ApplicationCore.Entities.SessionScholarshipDiscount objSessionScholarship = new ApplicationCore.Entities.SessionScholarshipDiscount();
                sessioschalarDiscount.ID = Convert.ToInt64(id);
                sessioschalarDiscount.Status = IsActive;

                sessioschalarDiscount.Active = true;

                //TODO
                sessioschalarDiscount.ModifiedId = userId;
                sessionScholarshipDiscountPriceRepository.UpdateSessionScholarshipDiscountPrice(sessioschalarDiscount);
                objScholar.ReturnMsg = "Records Deactivaed Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        public IActionResult downloadSchalarshipSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Upload", "Scholarship", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            XLWorkbook oWB = new XLWorkbook();
            var wsData = oWB.Worksheets.Add("Data");
            wsData.Cell(1, 1).Value = "Session";
            wsData.Cell(1, 2).Value = "Scholarship";
            //wsData.Cell(1, 3).Value = "Discount Name";
            //wsData.Cell(1, 4).Value = "Discount Description";
            //wsData.Cell(1, 5).Value = "FeeType";
            //wsData.Cell(1, 6).Value = "Discount Percentage";
            //wsData.Cell(1, 7).Value = "Organization";
            //wsData.Cell(1, 8).Value = "Wing";
            //wsData.Cell(1, 9).Value = "Board";
            //wsData.Cell(1, 10).Value = "Class";
            var session = academicSessionRepository.GetAllAcademicSession().Where(s => s.IsAvailable == true).ToList();
            var sessionId = academicSessionRepository.GetAllAcademicSession().Where(s => s.IsAvailable == true).Select(s => s.ID).FirstOrDefault();
            //var sessionscholarship =(from a in  sessionScholarship.GetAllSessionScholarship().Where(a=>a.SessionId== sessionId)
            //                        join b in scholarshipRepository.GetAllScholarshipData().Where(b=>b.Status=="ACTIVE") on a.ScholarshipId equals b.ID
            //                        select new
            //                        {
            //                            a.ID,
            //                            b.Name
            //                        }).ToList();
            // var SessionIds = academicSessionRepository.GetAllAcademicSession().Where(s => s.IsAvailable == true).Select(s => s.ID).FirstOrDefault();
            // var feeTypes = feeTypeRepository.GetAllFeesType().Where(a => a.Status == "ACTIVE").ToList();
            // var feestype = commonschoolmodel.GetAllSessionFeesType().Where(x => x.SessionId == SessionIds).ToList();
            var sessionscholarship = (from q in sessionScholarship.GetAllSessionScholarship().Where(q => q.SessionId == sessionId)
                                      join b in scholarshipRepository.GetAllScholarshipData().Where(b => b.Status == "ACTIVE") on q.ScholarshipId equals b.ID
                                      select new
                                      {
                                          q.ID,
                                          b.Name
                                      }).ToList();
            int g = 1;
            foreach (var a in session)
            {
                wsData.Cell(++g, 1).Value = a.DisplayName;
            }
            wsData.Range("A2:A" + g).AddToNamed("Session");

            int k = 1;
            foreach (var d in sessionscholarship)
            {
                //wsData.Cell(++k, 2).Value = d.Name;
                wsData.Cell(++k, 2).Value = /*d.Name;*/ "_C" + d.ID + "_" + d.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
            }
            wsData.Range("B2:B" + k).AddToNamed("Scholarship");

            int j = 3;
            foreach (var e in sessionscholarship)
            {
                var sessionscholarship1 = (from q in sessionScholarship.GetAllSessionScholarship().Where(q => q.SessionId == e.ID)
                                           join b in scholarshipRepository.GetAllScholarshipData().Where(b => b.Status == "ACTIVE") on q.ScholarshipId equals b.ID
                                           select new
                                           {
                                               q.ID,
                                               b.Name
                                           }).ToList();
                wsData.Cell(1, ++j).Value = "_C" + e.ID + "_" + e.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                int l = 2;
                foreach (var d in sessionscholarship)
                {
                    wsData.Cell(++l, j).Value = "S" + d.ID + "_" + d.Name;
                }
                wsData.Range(wsData.Cell(3, j), wsData.Cell(l, j)).AddToNamed("_C" + e.ID + "_" + e.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
            }
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Session");
            validationTable.Columns.Add("Scholarship");
            validationTable.TableName = "ScholarshipData";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(wsData.Range("Session"), true);
            worksheet.Column(2).SetDataValidation().List(wsData.Range("Scholarship"), true);
            worksheet.Column(1).SetDataValidation().InCellDropdown = true;
            worksheet.Column(1).SetDataValidation().Operator = XLOperator.Between;
            worksheet.Column(1).SetDataValidation().AllowedValues = XLAllowedValues.List;

            worksheet.Column(2).SetDataValidation().InCellDropdown = true;
            worksheet.Column(2).SetDataValidation().Operator = XLOperator.Between;
            worksheet.Column(2).SetDataValidation().AllowedValues = XLAllowedValues.List;
            worksheet.Column(2).SetDataValidation().List("=INDIRECT(SUBSTITUTE(B1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);


            //worksheet.Column(1).SetDataValidation().InCellDropdown = true;
            //worksheet.Column(2).SetDataValidation().InCellDropdown = true;

            wsData.Hide();
            // wsData1.Hide();
            // worksheet7.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"Scholarship.xlsx");



            //  return File(workbookBytes, "application/ms-excel", $"FeesTypePrice.xlsx");
            // return Json(1);
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        #endregion

        #region ApproveScholarship
        public JsonResult SendForApprove(string id)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {
                var sessionScholarshipId = Convert.ToInt64(id);
                var checkIsAvalableScholarship = scholarshipApprovalRepository.GetAllScholarshipApproval().Where(a => a.SessionScholarshipId == sessionScholarshipId).FirstOrDefault();
                if (checkIsAvalableScholarship == null)
                {
                    ScholarshipApproval objScholarshipApproval = new ScholarshipApproval();
                    objScholarshipApproval.SessionScholarshipId = Convert.ToInt64(id);
                    objScholarshipApproval.RequestSenderId = userId;
                    objScholarshipApproval.Active = true;
                    objScholarshipApproval.IsAvailable = true;
                    objScholarshipApproval.InsertedId = userId;
                    objScholarshipApproval.Status = EntityStatus.ACTIVE;
                    scholarshipApprovalRepository.CreateScholarshipApproval(objScholarshipApproval);
                }
                else
                {
                    objScholar.ReturnMsg = "Session Scholarship is already present.";
                }


                //TODO

                //objScholar.ReturnMsg = "Records Deactivaed Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }


        public IActionResult ScholarshipApproval()
        {

            return View();

        }
        public IActionResult GetScholarshipApproval(string SessionId)
        {
            var sessionScholarshipId = Convert.ToInt64(SessionId);

            var sessionScholaarship = sessionScholarship.GetAllSessionScholarship();
            var session = academicSessionRepository.GetAllAcademicSession();
            var scholarship = scholarshipRepository.GetAllScholarshipData().Where(a => a.Status == "ACTIVE");
            var sessionScholarshipDiscount = sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount();
            var scholarshipApproval = scholarshipApprovalRepository.GetAllScholarshipApproval();
            var alldata = (from a in scholarshipApproval
                           where a.Status == "ACTIVE"
                           join b in sessionScholaarship.Where(d => d.SessionId == sessionScholarshipId) on a.SessionScholarshipId equals b.ID
                           join c in scholarship on b.ScholarshipId equals c.ID
                           select new
                           {
                               scholarshipApproval = a.ID,
                               sessionScholarshipId = b.ID,
                               scholarshipName = c.Name,
                               scholarshipAdddate = c.InsertedDate,
                               sentdate = a.InsertedDate,
                               a.ApprovedDate,
                               a.ApprovedId,
                               a.RejectedDate,
                               a.RejectedId,
                               a.RejectedReason,
                               noOfDiscounts = (from d in sessionScholarshipDiscount where d.SessionScholarshipId == b.ID select d).ToList().Count()
                           }).ToList();


            //var obj = (from a in sessionScholaarship
            //           where a.Status == "ACTIVE" && a.SessionId == sessionScholarshipId
            //           join b in session on a.SessionId equals b.ID
            //           join c in scholarship on a.ScholarshipId equals c.ID
            //           select
            //           new
            //           {
            //               id = a.ID,
            //               sessionName = b.DisplayName,
            //               scholarshipName = c.Name,
            //               sessionId = b.ID,
            //               status = a.Status,
            //               SessionScholarshipDiscounts = (from e in sessionScholarshipDiscount where e.SessionScholarshipId == a.ID && e.Status == "ACTIVE" select e),
            //               selectDefault = (from f in sessionScholarshipDiscount where f.SessionScholarshipId == a.ID && f.Status == "ACTIVE" select f).FirstOrDefault()

            //           }).ToList();

            return Json(alldata);
        }
        public string GetUserNameById(long id)
        {
            var accessdetails = accessRepository.GetAccessById(id);
            var empDtails = employeeRepository.GetEmployeeById(accessdetails.EmployeeID);
            return empDtails.FirstName + " " + empDtails.LastName;
        }
        public IActionResult GetScholarshipApprovalById(string scholarshipApprovalId, string SessionId)
        {
            var ScholarshipApprovalId = Convert.ToInt64(scholarshipApprovalId);
            var sessionId = Convert.ToInt64(SessionId);
            var sessionScholaarship = sessionScholarship.GetAllSessionScholarship();
            var session = academicSessionRepository.GetAllAcademicSession().Where(a => a.ID == sessionId).SingleOrDefault().DisplayName;
            var scholarship = scholarshipRepository.GetAllScholarshipData().Where(a => a.Status == "ACTIVE");
            var sessionScholarshipDiscount = sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount();
            var scholarshipApproval = scholarshipApprovalRepository.GetAllScholarshipApproval();
            var alldata = (from a in scholarshipApproval
                           where a.ID == ScholarshipApprovalId
                           where a.Status == "ACTIVE"
                           join b in sessionScholaarship.Where(d => d.SessionId == sessionId) on a.SessionScholarshipId equals b.ID
                           join c in scholarship on b.ScholarshipId equals c.ID
                           select new
                           {
                               sessionName = session,
                               scholarshipApproval = a.ID,
                               sessionScholarshipId = b.ID,
                               scholarshipName = c.Name,
                               scholarshipAdddate = c.InsertedDate,
                               sentdate = a.InsertedDate,
                               sentBy = (a.InsertedId == 0 ? null : GetUserNameById(a.InsertedId)),
                               a.ApprovedDate,
                               approvedBy = (a.ApprovedId == 0 ? null : GetUserNameById(a.ApprovedId)),
                               a.RejectedDate,
                               rejectBy = (a.RejectedId == 0 ? null : GetUserNameById(a.RejectedId)),
                               a.RejectedReason,
                               noOfDiscounts = (from d in sessionScholarshipDiscount where d.SessionScholarshipId == b.ID select d).ToList().Count(),
                               SessionScholarshipDiscounts = (from g in sessionScholarshipDiscount where g.SessionScholarshipId == b.ID && g.Status == "ACTIVE" select g),
                               selectDefault = (from f in sessionScholarshipDiscount where f.SessionScholarshipId == b.ID && f.Status == "ACTIVE" select f).FirstOrDefault()
                           }).ToList();


            return Json(alldata);
        }

        //GetScholarshipWithDefault

        public IActionResult GetScholarshipWithDefault(string SessionScholarshipDiscountId, string SessionId)
        {

            var sessionId = Convert.ToInt64(SessionId);
            var scholarshipDiscountId = Convert.ToInt64(SessionScholarshipDiscountId);
            var sessionScholaarship = sessionScholarship.GetAllSessionScholarship();
            var session = academicSessionRepository.GetAllAcademicSession().Where(a => a.ID == sessionId).SingleOrDefault().DisplayName;
            var scholarship = scholarshipRepository.GetAllScholarshipData().Where(a => a.Status == "ACTIVE");
            var sessionScholarshipDiscount = sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount();
            var scholarshipApproval = scholarshipApprovalRepository.GetAllScholarshipApproval();
            var alldata = (from b in sessionScholaarship
                           join c in sessionScholarshipDiscount.Where(d => d.ID == scholarshipDiscountId) on b.ID equals c.SessionScholarshipId
                           select new
                           {
                               sessionScholarshipId = b.ID,
                               scholarshipName = c.Name,
                               selectDefault = (from f in sessionScholarshipDiscount where f.SessionScholarshipId == b.ID && f.Status == "ACTIVE" select f).FirstOrDefault()
                           }).ToList();


            return Json(alldata);
        }
        public IActionResult GetScholarshipDiscountWithDefault(string SessionScholarshipId, string SessionId)
        {

            var sessionId = Convert.ToInt64(SessionId);
            var sessionScholarshipId = Convert.ToInt64(SessionScholarshipId);
            var sessionScholarshipDiscount = sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount();
            var alldata = (from c in sessionScholarshipDiscount.Where(d => d.SessionScholarshipId == sessionScholarshipId)
                           select new
                           {
                               sessionScholarshipDiscountId = c.ID,
                               c.Name,


                           }).ToList();

            var selectDefault = alldata.FirstOrDefault();
            return Json(new
            {
                scholarshipDiscountData = alldata,
                isRedirect = selectDefault.sessionScholarshipDiscountId
            });
            //return Json(alldata);
        }
        public JsonResult UpdateScholarshipApproval(string[] ids, string ActiveStatus, string reason)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (ModelState.IsValid)
            {
                if (ActiveStatus == "approve")
                {
                    for (int i = 0; i < ids.Length; i++)
                    {
                        var scholarshipapprovalObj = scholarshipApprovalRepository.GetScholarshipApprovalById(Convert.ToInt64(ids[i]));
                        ScholarshipApproval objsessionApproval = new ScholarshipApproval();
                        scholarshipapprovalObj.ApprovedDate = DateTime.Now;
                        scholarshipapprovalObj.ApprovedId = userId;
                        scholarshipapprovalObj.RejectedDate = null;
                        scholarshipapprovalObj.RejectedId = 0;
                        scholarshipapprovalObj.RejectedReason = null;
                        scholarshipapprovalObj.ModifiedId = userId;
                        scholarshipApprovalRepository.UpdateScholarshipApproval(scholarshipapprovalObj);
                    }
                }
                else
                {
                    for (int i = 0; i < ids.Length; i++)
                    {
                        var scholarshipapprovalObj = scholarshipApprovalRepository.GetScholarshipApprovalById(Convert.ToInt64(ids[i]));
                        ScholarshipApproval objsessionApproval = new ScholarshipApproval();
                        scholarshipapprovalObj.ApprovedDate = null;
                        scholarshipapprovalObj.ApprovedId = 0;
                        scholarshipapprovalObj.RejectedDate = DateTime.Now;
                        scholarshipapprovalObj.RejectedId = userId;
                        scholarshipapprovalObj.RejectedReason = reason;
                        scholarshipapprovalObj.ModifiedId = userId;
                        scholarshipApprovalRepository.UpdateScholarshipApproval(scholarshipapprovalObj);
                    }
                }
                objScholar.ReturnMsg = "Records Updated Successfully.";


            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }
        #endregion

        #region --Payment Summary--
        public IActionResult GetScholarshipDeailsByClass(string classId, string SessionId, string studentId)
        {
            object defaultData;
            var standardid = Convert.ToInt64(classId);
            var sessionid = Convert.ToInt64(SessionId);

            var scholarship = scholarshipRepository.GetAllScholarshipData().Where(a => a.Status == "ACTIVE");
            var sessionscholarship = sessionScholarship.GetAllSessionScholarship().Where(a => a.SessionId == sessionid && a.Status == "ACTIVE");
            //var sessionScholarshipDetails = sessionScholarship.GetSessionScholarshipById(sessionScholarshipId);
            var sessionScholarshipDiscount = sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount().Where(a => a.Status == "ACTIVE");
            var sessionScholarshipDiscountPrice = sessionScholarshipDiscountPriceRepository.GetAllSessionScholarshipDiscountPrice().Where(a => a.Status == "ACTIVE").ToList();
            var sessionScholarshipDiscountApplicableList = sessionScholarshipDiscountApplicableRepository.GetAllSessionScholarshipDiscountApplicable().Where(a => a.Status == "ACTIVE" && a.AcademicStandardId == standardid).ToList();
            var scholarshipApproval = scholarshipApprovalRepository.GetAllScholarshipApproval();
            var alldata = (from a in sessionScholarshipDiscountApplicableList
                           join b in sessionScholarshipDiscountPrice on a.SessionScholarshipDiscountPriceId equals b.ID
                           join c in sessionScholarshipDiscount on b.SessionScholarshipDiscountId equals c.ID
                           join d in sessionscholarship on c.SessionScholarshipId equals d.ID
                           join e in scholarshipApproval on d.ID equals e.SessionScholarshipId
                           where e.ApprovedId > 0
                           select new
                           {
                               //d.ID,
                               d.ID
                           }).Distinct().ToList();
            var newdata = (from f in alldata
                           join g in sessionscholarship on f.ID equals g.ID
                           join i in scholarship on g.ScholarshipId equals i.ID
                           select new
                           {
                               g.ID,
                               i.Name
                           }).OrderBy(a => a.ID).ToList();
            if (studentId == "0")
            {
                defaultData = newdata.FirstOrDefault();
            }
            else
            {
                var stdId = Convert.ToInt64(studentId);
                var scholarshipScholarshipDiscountByStudentId = scholarshipApplyRepository.GetAllScholarshipApply().Where(a => a.Status == "ACTIVE" && a.StudentId == stdId && a.AcademicSessionId == sessionid).Select(a => a.SessionScholarshipDiscountId).FirstOrDefault();
                var SessionScholarshipId = sessionScholarshipDiscountRepository.GetSessionScholarshipDiscountById(scholarshipScholarshipDiscountByStudentId).SessionScholarshipId;
                var data1 = (from g in sessionscholarship
                             where g.ID == SessionScholarshipId
                             join i in scholarship on g.ScholarshipId equals i.ID
                             select new
                             {
                                 g.ID,
                                 i.Name
                             }).OrderBy(a => a.ID).ToList();
                defaultData = data1.FirstOrDefault();

            }
            // var 

            return Json(new
            {
                sessionData = newdata,
                isRedirect = defaultData,
            });

            //var getData = (from a in scholarshipApproval
            //               where a.ApprovedId > 0 
            //               join b in sessionscholarship on a.SessionScholarshipId equals b.ID where b.SessionId ==sessionid
            //               //join c in sessionScholarshipDiscount on b.ID equals c.SessionScholarshipId
            //               //join d in sessionScholarshipDiscountPrice on c.ID equals d.SessionScholarshipDiscountId
            //               //join e in sessionScholarshipDiscountApplicableList on d.ID equals e.SessionScholarshipDiscountPriceId
            //               select new
            //               {
            //                  a.SessionScholarshipId,
            //                  //b.ID,
            //                   //sessionData=from d in sessionScholarshipDiscount where d.SessionScholarshipId==b.ID 
            //                   //            join e in sessionScholarshipDiscountPrice on d.ID equals e.SessionScholarshipDiscountId
            //                   //            join f in 


            //               }).ToList();
            //return Json(1);
        }

        public IActionResult GetPaymentCollectionTypeByClass(string classId, string SessionId, string sessionScholarshipDiscountId)
        {
            var classid = Convert.ToInt64(classId);
            var sessionId = Convert.ToInt64(SessionId);
            var totalPrice = Convert.ToDecimal(0);
            var sessionscholarshipdiscount = Convert.ToInt64(sessionScholarshipDiscountId);
            var collectionData = paymentCollectionTypeRepository.GetAllPaymentCollectionType().Where(a => a.Active == true && a.ClassId == classid && a.AcademicSessionId == sessionId).FirstOrDefault();
            var DiscountPrice = sessionScholarshipDiscountPriceRepository.GetAllSessionScholarshipDiscountPrice().Where(b => b.SessionScholarshipDiscountId == sessionscholarshipdiscount && b.Status == "ACTIVE").FirstOrDefault().DiscountPercentage;
            if (collectionData != null)
            {
                totalPrice = collectionData.SumAmount;

            }

            var discountAmunt = (totalPrice * DiscountPrice) / 100;
            var paidAmount = totalPrice - discountAmunt;
            return Json(new
            {
                totalAmount = totalPrice,
                discountAmount = discountAmunt,
                toBepaidAmount = paidAmount

            });
        }


        public IActionResult GetScholarshipDiscountDetailsWithDefault(string SessionScholarshipId, string SessionId, string studentId)
        {
            object selectDefault;
            var sessionid = Convert.ToInt64(SessionId);
            var scholarship = scholarshipRepository.GetAllScholarshipData().Where(a => a.Status == "ACTIVE");
            var sessionscholarship = sessionScholarship.GetAllSessionScholarship().Where(a => a.SessionId == sessionid && a.Status == "ACTIVE");
            //var sessionId = Convert.ToInt64(SessionId);
            var sessionScholarshipId = Convert.ToInt64(SessionScholarshipId);
            var sessionScholarshipDiscount = sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount();
            var alldata = (from c in sessionScholarshipDiscount.Where(d => d.SessionScholarshipId == sessionScholarshipId)
                           select new
                           {
                               sessionScholarshipDiscountId = c.ID,
                               c.Name,


                           }).ToList();

            //var selectDefault = alldata.FirstOrDefault();
            if (studentId == "0")
            {
                selectDefault = null;
            }
            else
            {
                var stdId = Convert.ToInt64(studentId);
                var scholarshipScholarshipDiscountByStudentId = scholarshipApplyRepository.GetAllScholarshipApply().Where(a => a.Status == "ACTIVE" && a.StudentId == stdId && a.AcademicSessionId == sessionid).Select(a => a.SessionScholarshipDiscountId).FirstOrDefault();
                var data1 = (from b in sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount()
                             where b.ID == scholarshipScholarshipDiscountByStudentId
                             select new
                             {
                                 sessionScholarshipDiscountId = b.ID,
                                 b.Name
                             }).OrderBy(a => a.sessionScholarshipDiscountId).ToList();
                //var data1 = (from a in scholarshipScholarshipDiscountByStudentId
                //             select new
                //             {
                //                 a.
                //             });


                //var data1 = (from g in sessionscholarship
                //             where g.ID == SessionScholarshipId1
                //             join i in scholarship on g.ScholarshipId equals i.ID
                //             select new
                //             {
                //                 g.ID,
                //                 i.Name
                //             }).OrderBy(a => a.ID).ToList();
                selectDefault = data1.FirstOrDefault();

            }
            return Json(new
            {
                scholarshipDiscountData = alldata,
                isRedirect = selectDefault,
            });
            //return Json(alldata);
        }

        public JsonResult SaveScholarshipApply(string scholarshipDiscountId, string[] StudentIds, string SessionId)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Save", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            var sessionId = Convert.ToInt64(SessionId);
            var scholarshipDiscountid = Convert.ToInt64(scholarshipDiscountId);
            if (StudentIds != null)
            {
                foreach (string a in StudentIds)
                {
                    var studentid = Convert.ToInt64(a);
                    var scholarshipApplyAvail = scholarshipApplyRepository.GetAllScholarshipApply().Where(b => b.Status == "ACTIVE" && b.SessionScholarshipDiscountId == scholarshipDiscountid && b.StudentId == studentid).ToList().Count;
                    if (scholarshipApplyAvail == 0)
                    {
                        ScholarshipApply objScholarshipApply = new ScholarshipApply();
                        objScholarshipApply.StudentId = studentid;
                        objScholarshipApply.SessionScholarshipDiscountId = scholarshipDiscountid;
                        objScholarshipApply.AcademicSessionId = sessionId;
                        objScholarshipApply.InsertedId = userId;
                        objScholarshipApply.InsertedDate = DateTime.Now;
                        objScholarshipApply.RequestSenderId = userId;
                        objScholarshipApply.Active = true;
                        objScholarshipApply.IsAvailable = true;
                        objScholarshipApply.InsertedId = userId;
                        objScholarshipApply.Status = EntityStatus.ACTIVE;
                        scholarshipApplyRepository.CreateScholarshipApply(objScholarshipApply);
                    }
                }
                objScholar.ReturnMsg = "Scholarship Apply Successfully!";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        //UpdateScholarshipApply
        public JsonResult UpdateScholarshipApply(string scholarshipDiscountId, string StudentIds, string SessionId)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            var sessionId = Convert.ToInt64(SessionId);
            var scholarshipDiscountid = Convert.ToInt64(scholarshipDiscountId);
            if (StudentIds != null)
            {
                var studentid = Convert.ToInt64(StudentIds);
                var scholarshipApplyAvail = scholarshipApplyRepository.GetAllScholarshipApply().Where(b => b.Status == "ACTIVE" && b.SessionScholarshipDiscountId == scholarshipDiscountid && b.StudentId == studentid).SingleOrDefault();
                if (scholarshipApplyAvail != null)
                {
                    // ScholarshipApply objScholarshipApply = new ScholarshipApply();
                    scholarshipApplyAvail.ModifiedId = userId;
                    scholarshipApplyAvail.ModifiedDate = DateTime.Now;
                    scholarshipApplyAvail.Active = true;
                    scholarshipApplyAvail.IsAvailable = true;
                    scholarshipApplyAvail.InsertedId = userId;
                    scholarshipApplyAvail.Status = "DEACTIVE";
                    scholarshipApplyRepository.UpdateScholarshipApply(scholarshipApplyAvail);
                }
                objScholar.ReturnMsg = "Scholarship Apply Successfully!";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        #endregion

        #region --Upload Excel Scholarship--


        public IActionResult downloadScholarshipSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Upload", "Scholarship", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var comlumHeadrs = new string[]
           {
                "Name",
                "Description"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Scholarship"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }
                using (var cells = worksheet.Cells[1, 2]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Scholarship.xlsx");
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IActionResult UploadScholarship(IFormFile file)
        {
            long Yn = 0; string ReturnMsg = ""; string ExistsYN = "";
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Upload", "Scholarship", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "Scholarship_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                //var webPath = "";
                //if (string.IsNullOrWhiteSpace(hostingEnvironment.WebRootPath))
                //{
                //    webPath= hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                //}
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {

                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Scholarship objScholarship = new Scholarship();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();

                        if (CheckExistance(name) == false)
                        {
                            objScholarship.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            objScholarship.Description = firstWorksheet.Cells[i, 2].Value.ToString();
                            objScholarship.ModifiedId = userId;
                            objScholarship.Active = true;
                            objScholarship.InsertedId = userId;
                            objScholarship.IsAvailable = true;
                            objScholarship.Status = EntityStatus.ACTIVE;
                            Yn = scholarshipRepository.CreateScholarship(objScholarship);
                        }
                        else
                        {
                            ExistsYN = "Name already Exists !!!";
                        }
                    }
                    if (Yn >= 1)
                    {
                        ReturnMsg = "Excel uploaded successfully";
                    }
                    else
                    {
                        ReturnMsg = "Excel uploaded Failed !!!  " + ExistsYN;
                    }
                }
            }

            return Json(ReturnMsg);
        }


        #endregion



        #region --Student Scholarship Approval--
        public IActionResult StudentScholarshipApproval()
        {

            return View();

        }

        public IActionResult GetStudentScholarshipApproval(string ClassId, string sessionId)
        {
            var classId = Convert.ToInt64(ClassId);
            var SessionId = Convert.ToInt64(sessionId);
            var sessionScholaarship = sessionScholarship.GetAllSessionScholarship();
            var session = academicSessionRepository.GetAllAcademicSession();
            var scholarship = scholarshipRepository.GetAllScholarshipData().Where(a => a.Status == "ACTIVE");
            var sessionScholarshipDiscount = sessionScholarshipDiscountRepository.GetAllSessionScholarshipDiscount();
            var StudentscholarshipApply = scholarshipApplyRepository.GetAllScholarshipApply();
            // var students = studentAggregateRepository.GetAllStudent();
            var allacademicstudents = studentSqlQuery.Get_View_Academic_Student().Where(a => a.AcademicStandardId == classId).ToList();
            //var allacademicsectionstudents = commonsql.Get_view_All_Academic_Student_Sections();
            var alldata = (from a in StudentscholarshipApply
                           where a.Status == "ACTIVE" && a.AcademicSessionId == SessionId
                           join b in sessionScholarshipDiscount on a.SessionScholarshipDiscountId equals b.ID
                           join c in allacademicstudents on a.StudentId equals c.StudentId
                           //join d in students on c.StudentId equals d.ID
                           select new
                           {
                               studentScholarshipApproval = a.ID,
                               sessionScholarshipId = b.ID,
                               scholarshipDiscountName = b.Name,
                               scholarshipAdddate = a.InsertedDate,
                               sentdate = a.InsertedDate,
                               a.ApprovedDate,
                               a.ApprovedId,
                               a.RejectedDate,
                               a.RejectedId,
                               a.RejectedReason,
                               name = c.studentname
                           }).ToList();

            return Json(alldata);
        }

        public JsonResult UpdateStudentScholarshipApproval(string[] ids, string ActiveStatus, string reason)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Scholarship", accessId, "Update", "Scholarship", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (ModelState.IsValid)
            {
                if (ActiveStatus == "approve")
                {
                    for (int i = 0; i < ids.Length; i++)
                    {
                        var scholarshipapprovalObj = scholarshipApplyRepository.GetScholarshipApplyById(Convert.ToInt64(ids[i]));
                        ScholarshipApproval objsessionApproval = new ScholarshipApproval();
                        scholarshipapprovalObj.ApprovedDate = DateTime.Now;
                        scholarshipapprovalObj.ApprovedId = userId;
                        scholarshipapprovalObj.RejectedDate = null;
                        scholarshipapprovalObj.RejectedId = 0;
                        scholarshipapprovalObj.RejectedReason = null;
                        scholarshipapprovalObj.ModifiedId = userId;
                        scholarshipApplyRepository.UpdateScholarshipApply(scholarshipapprovalObj);
                    }
                }
                else
                {
                    for (int i = 0; i < ids.Length; i++)
                    {
                        var scholarshipapprovalObj = scholarshipApplyRepository.GetScholarshipApplyById(Convert.ToInt64(ids[i]));
                        ScholarshipApproval objsessionApproval = new ScholarshipApproval();
                        scholarshipapprovalObj.ApprovedDate = null;
                        scholarshipapprovalObj.ApprovedId = 0;
                        scholarshipapprovalObj.RejectedDate = DateTime.Now;
                        scholarshipapprovalObj.RejectedId = userId;
                        scholarshipapprovalObj.RejectedReason = reason;
                        scholarshipapprovalObj.ModifiedId = userId;
                        scholarshipApplyRepository.UpdateScholarshipApply(scholarshipapprovalObj);
                    }
                }
                objScholar.ReturnMsg = "Records Updated Successfully.";


            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }


        #endregion

        #region --Upload Excel Session Scholarship--
        public IActionResult downloadSessionScholarshipSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("FeesType", accessId, "Upload", "StudentPayment", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            XLWorkbook oWB = new XLWorkbook();
            var allSession = Get_Session().ToList();
            var allScholarship = scholarshipRepository.GetAllScholarshipData().Where(f => f.Status == "ACTIVE");
            DataTable sessiondt = new DataTable();
            sessiondt.Columns.Add("Session");
            foreach (var a in allSession)
            {
                DataRow dr = sessiondt.NewRow();
                dr["Session"] = a.DisplayName;
                sessiondt.Rows.Add(dr);
            }
            sessiondt.TableName = "SessionList";

            DataTable Scholarshipdt = new DataTable();
            Scholarshipdt.Columns.Add("Scholarship");
            foreach (var a in allScholarship)
            {
                DataRow dr = Scholarshipdt.NewRow();
                dr["Scholarship"] = a.Name;
                Scholarshipdt.Rows.Add(dr);
            }
            Scholarshipdt.TableName = "ScholarshipList";
            int lastCellNo1 = sessiondt.Rows.Count + 1;
            int lastCellNo2 = Scholarshipdt.Rows.Count + 1;

            oWB.AddWorksheet(sessiondt);
            oWB.AddWorksheet(Scholarshipdt);
            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Session");
            validationTable.Columns.Add("Scholarship");
            validationTable.TableName = "SessionScholarships";

            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(2).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);

            worksheet1.Hide();
            worksheet2.Hide();

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"SessionScholarship.xlsx");
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public IActionResult UploadSessionScholarship(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            long Yn = 0; string ReturnMsg = ""; string ExistsYN = "";
            try
            {
                if (commonMethods.checkaccessavailable("Scholarship", accessId, "List", "Scholarship", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                if (file != null)
                {

                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[2];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var request = sessionScholarship.GetAllSessionScholarship();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            SessionScholarship objSessionScholarship = new SessionScholarship();
                            Random generator = new Random();
                            objSessionScholarship.SessionId = Get_Session().Where(a => a.DisplayName == (firstWorksheet.Cells[i, 1].Value.ToString())).FirstOrDefault().ID;
                            objSessionScholarship.ScholarshipId = scholarshipRepository.GetScholarshipByName(firstWorksheet.Cells[i, 2].Value.ToString()).ID;
                            var isPresent = (request.Where(b => b.SessionId == objSessionScholarship.SessionId && b.ScholarshipId == objSessionScholarship.ScholarshipId)).ToList().Count;
                            if (isPresent == 0)
                            {
                                objSessionScholarship.Active = true;
                                objSessionScholarship.InsertedId = userId;
                                objSessionScholarship.IsAvailable = true;
                                objSessionScholarship.ModifiedId = userId;
                                objSessionScholarship.Status = EntityStatus.ACTIVE;
                                Yn = sessionScholarship.CreateSessionScholarship(objSessionScholarship);
                            }
                            else
                            {
                                ExistsYN = "Name already Exists !!!";
                            }


                        }
                        if (Yn >= 1)
                        {
                            ReturnMsg = "Excel uploaded successfully";
                        }
                        else
                        {
                            ReturnMsg = "Excel uploaded Failed !!!  " + ExistsYN;
                        }

                        //ReturnMsg = "Excel uploaded successfully";
                    }
                }
            }
            catch (Exception e)
            {
                ReturnMsg = "Excel uploaded Failed !!!  ";
            }
            return Json(ReturnMsg);
        }

        #endregion

        public ActionResult TestData()
        {
            paymentServices.SendPaymentRemindermail();
            return null;
        }
    }
}
