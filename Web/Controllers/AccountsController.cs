﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OdmErp.Web.Extensions;
using OdmErp.Web.Models;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using OdmErp.Web.Models.Background;
using Microsoft.Extensions.Hosting;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.ApplicationCore.Entities;
using System.Threading;
using System.Text.RegularExpressions;
//using System.Threading;

namespace OdmErp.Web.Controllers
{
    public class AccountsController : AppController
    {
        private DateTime currentTime = DateTime.Now;
        private CommonMethods commonMethods;
        public smssend Smssend;
        private readonly TimedHostedService _recureHostedService;
        public IEmployeeRepository employeeRepository;
        public IStudentAggregateRepository studentAggregateRepository;
        public IAccessRepository accessRepository;
        public IHttpContextAccessor contextAccessor;
        public commonsqlquery commonsql;
        public IActionAccessRepository actionAccessRepository;
        public IModuleRepository moduleRepository;
        public ISubModuleRepository subModuleRepository;
        public AccountsController(IHttpContextAccessor context, IStudentAggregateRepository studentAggregateRepo, CommonMethods commonMet, IEmployeeRepository employeeRepo, smssend Sms, IAccessRepository accessRepo, IHostedService hostedService,
            commonsqlquery commonsqlquery, IActionAccessRepository actionAccessRepo, IModuleRepository moduleRepo, ISubModuleRepository subModuleRepo)
        {
            contextAccessor = context;
            commonMethods = commonMet;
            _recureHostedService = hostedService as TimedHostedService;
            accessRepository = accessRepo;
            Smssend = Sms;
            employeeRepository = employeeRepo;
            studentAggregateRepository = studentAggregateRepo;
            commonsql = commonsqlquery;
            actionAccessRepository = actionAccessRepo;
            moduleRepository = moduleRepo;
            subModuleRepository = subModuleRepo;
        }
        #region Employee Login
        public IActionResult EmployeeLogin()
        {

            logincls login = new logincls();
            if (contextAccessor.HttpContext.Request.Cookies["username"] != null && contextAccessor.HttpContext.Request.Cookies["pas"] != null)
            {
                var username = contextAccessor.HttpContext.Request.Cookies["username"].ToString();
                var Password = contextAccessor.HttpContext.Request.Cookies["pas"].ToString();
                login.userName = username;
                login.Password = Password;
                login.isExist = true;
                var access = accessRepository.GetAllAccess().Where(a => a.Username == login.userName && a.Password == login.Password && (a.RoleID == 1 || a.RoleID == 2 || a.RoleID == 3 || a.RoleID==4)).FirstOrDefault();
                if (access != null)
                {
                    HttpContext.Session.SetInt32("userId", Convert.ToInt32(access.EmployeeID));
                    HttpContext.Session.SetInt32("accessId", Convert.ToInt32(access.ID));
                    HttpContext.Session.SetInt32("roleId", Convert.ToInt32(access.RoleID));

                    CookieOptions option = new CookieOptions();
                    option.Expires = DateTime.Now.AddDays(5);
                    Response.Cookies.Append("username", login.userName, option);
                    Response.Cookies.Append("pas", login.Password, option);
                    return RedirectToAction("Index", "Dashboard");
                }
            }


            login.accessid = 0;
            login.confirmpassword = "";
            return View(login);
        }

        public IActionResult checkusername(logincls login)
        {
            if (login.Password != null)
            {
                var access = accessRepository.GetAllAccess().Where(a => a.Username == login.userName && a.Password == login.Password && (a.RoleID == 1 || a.RoleID == 2 || a.RoleID == 3 || a.RoleID == 4)).FirstOrDefault();
                if (access != null)
                {
                    HttpContext.Session.SetInt32("userId", Convert.ToInt32(access.EmployeeID));
                    HttpContext.Session.SetInt32("accessId", Convert.ToInt32(access.ID));
                    HttpContext.Session.SetInt32("roleId", Convert.ToInt32(access.RoleID));
                    if (login.isRemember == true)
                    {
                        CookieOptions option = new CookieOptions();
                        option.Expires = DateTime.Now.AddDays(5);
                        Response.Cookies.Append("username", login.userName, option);
                        Response.Cookies.Append("pas", login.Password, option);


                    }
                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {

                    login.isExist = true;
                    return View(nameof(EmployeeLogin), login).WithDanger("Error", "Invalid Username/Password. Try Again!");
                }
            }
            else
            {
                var access = accessRepository.GetAllAccess().Where(a => a.Username == login.userName && (a.RoleID == 1 || a.RoleID == 2 || a.RoleID == 3 || a.RoleID == 4)).FirstOrDefault();
                logincls logincls = new logincls();
                if (access != null)
                {
                    if (access.Password != null && access.Password != "")
                    {
                        logincls.isExist = true;
                        logincls.userName = login.userName;
                        return View(nameof(EmployeeLogin), logincls);
                    }
                    else
                    {
                        logincls.isExist = true;
                        logincls.userName = login.userName;
                        logincls.userId = access.EmployeeID;
                        var emp = employeeRepository.GetEmployeeById(access.EmployeeID);
                        string otp = smssend.random_password(6);
                        Thread t1 = new Thread(delegate ()
                        {
                            Smssend.Sendotp(otp, emp.PrimaryMobile);
                        });
                        t1.Start();

                        logincls.mobile = emp.PrimaryMobile;
                        logincls.accessid = access.ID;
                        HttpContext.Session.SetString("OTP", otp.ToString());
                        return RedirectToAction(nameof(OTPValidation), logincls);
                    }
                }
                else
                {
                    return View(nameof(EmployeeLogin), login).WithDanger("Error", "Invalid Username. Try Again!");
                }
            }
        }
        public IActionResult OTPSend(string username)
        {
            logincls logincls = new logincls();
            var access = accessRepository.GetAllAccess().Where(a => a.Username == username && (a.RoleID == 1 || a.RoleID == 2 || a.RoleID == 3 || a.RoleID == 4)).FirstOrDefault();
            if (access != null)
            {
                logincls.isExist = true;
                logincls.userName = username;
                logincls.userId = access.EmployeeID;
                var emp = employeeRepository.GetEmployeeById(access.EmployeeID);
                string otp = smssend.random_password(6);
                Thread t1 = new Thread(delegate ()
                {
                    Smssend.Sendotp(otp, emp.PrimaryMobile);
                });
                t1.Start();
                logincls.mobile = emp.PrimaryMobile;
                logincls.accessid = access.ID;
                HttpContext.Session.SetString("OTP", otp.ToString());
                return RedirectToAction(nameof(OTPValidation), logincls).WithSuccess("Success", "OTP Send Successfully");
            }
            else
            {
                return View(nameof(ForgotPassword)).WithDanger("Error", "Incorrect Username.Try Again!");
            }
        }
        public IActionResult OTPValidation(logincls logincls)
        {
            ViewBag.ErrorMessage = "";
            return View(logincls);
        }
        public IActionResult CheckOTPValidation(logincls logincls)
        {
            ViewBag.ErrorMessage = "";
            string otp = HttpContext.Session.GetString("OTP");
            if (otp == logincls.otp)
            {
                return RedirectToAction(nameof(CreatePassword), logincls);
            }
            else
            {
                return View(nameof(OTPValidation), logincls).WithDanger("Error", "Incorrect OTP");
            }

        }
        public JsonResult ResendOTP(string mobile)
        {
            ViewBag.ErrorMessage = "OTP send successfully";
            string otp = smssend.random_password(6);
            Thread t1 = new Thread(delegate ()
            {
                Smssend.Sendotp(otp, mobile);
            });
            t1.Start();

            HttpContext.Session.SetString("OTP", otp.ToString());
            return Json(1);
        }
        public IActionResult CreatePassword(logincls logincls)
        {
            ViewBag.ErrorMessage = "";

            return View(logincls);
        }
        public IActionResult SavePassword(logincls logincls)
        {
            if (logincls.newPassword == logincls.confirmpassword)
            {

                Access access = accessRepository.GetAccessById(logincls.accessid);
                if (access.Password == null || access.Password == "")
                {
                    var emp = employeeRepository.GetEmployeeById(access.EmployeeID);
                    Thread t1 = new Thread(delegate ()
                    {
                        Smssend.accountcreated(emp.FirstName, emp.EmailId, emp.EmpCode);
                        //Smssend.SendSms(emp.FirstName, emp.PrimaryMobile);
                    });
                    t1.Start();
                }
                access.Password = logincls.newPassword;
                accessRepository.UpdateAccess(access);

                HttpContext.Session.SetInt32("userId", Convert.ToInt32(access.EmployeeID));
                HttpContext.Session.SetInt32("accessId", Convert.ToInt32(access.ID));
                HttpContext.Session.SetInt32("roleId", Convert.ToInt32(access.RoleID));


                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
            }
            return View(nameof(CreatePassword), logincls).WithDanger("Error", "Passwords are not same");
        }
        public IActionResult Login()
        {
            ViewBag.ErrorMessage = "";
            return View();
        }
        public IActionResult ChangePassword()
        {

            return View();
        }
        public IActionResult SaveChangePassword(string password, string oldpassword)
        {
            CheckLoginStatus();
            var userId = HttpContext.Session.GetInt32("userId").Value;
            var chk = accessRepository.GetAllAccess().Where(a => a.EmployeeID == userId && a.Password == oldpassword && (a.RoleID == 1 || a.RoleID == 2 || a.RoleID == 3 || a.RoleID == 4)).FirstOrDefault();
            if (chk == null)
            {
                if (roleId == 5 || roleId == 6)
                {
                    var pass = accessRepository.GetAllAccess().Where(a => a.EmployeeID == userId && a.Password == oldpassword && (roleId == 5 || roleId == 6)).FirstOrDefault();
                    if (pass == null)
                    {
                        return View(nameof(ChangePassword)).WithDanger("Error", "Old Password is not correct");
                    }
                    else
                    {
                        pass.Password = password;
                        pass.ModifiedId = userId;
                        accessRepository.UpdateAccess(pass);
                        HttpContext.Session.Clear();
                        Response.Cookies.Delete("username");
                        Response.Cookies.Delete("pas");
                        if (roleId == 5)
                        {
                            var emp = studentAggregateRepository.GetAllParent().Where(a => a.StudentID == userId).ToList();
                            foreach (var a in emp)
                            {
                                Thread t1 = new Thread(delegate ()
                                {
                                    Smssend.passwordchange(a.FirstName, a.EmailId, a.PrimaryMobile);
                                    
                                });
                                t1.Start();
                            }
                            return RedirectToAction("StudentLogin", "Accounts").WithSuccess("Success", "Changed Successfully");
                        }
                        else 
                        {
                            var emp = studentAggregateRepository.GetParentById(userId);
                            Thread t1 = new Thread(delegate ()
                            {
                                Smssend.passwordchange(emp.FirstName, emp.EmailId, emp.PrimaryMobile);
                                Smssend.SendPasswordChangeSms(emp.FirstName, emp.PrimaryMobile);
                            });
                            t1.Start();
                            return RedirectToAction("ParentLogin", "Accounts").WithSuccess("Success", "Changed Successfully");
                        }
                    }
                }

                return View(nameof(ChangePassword)).WithDanger("Error", "Old Password is not correct");
            }
            else
            {
               
                chk.Password = password;
                chk.ModifiedId = userId;
                accessRepository.UpdateAccess(chk);
                HttpContext.Session.Clear();
                Response.Cookies.Delete("username");
                Response.Cookies.Delete("pas");
                if (roleId == 5)
                {
                    var emp = studentAggregateRepository.GetAllParent().Where(a => a.StudentID == userId).ToList();
                    foreach (var a in emp)
                    {
                        Thread t1 = new Thread(delegate ()
                        {
                            Smssend.passwordchange(a.FirstName, a.EmailId, a.PrimaryMobile);
                        });
                        t1.Start();
                    }
                    return RedirectToAction("StudentLogin", "Accounts").WithSuccess("Success", "Changed Successfully");
                }
                else if (roleId == 6)
                {
                    var emp = studentAggregateRepository.GetParentById(userId);
                    Thread t1 = new Thread(delegate ()
                    {
                        Smssend.passwordchange(emp.FirstName, emp.EmailId, emp.PrimaryMobile);
                    });
                    t1.Start();
                    return RedirectToAction("ParentLogin", "Accounts").WithSuccess("Success", "Changed Successfully");
                }
                else
                {
                    var emp = employeeRepository.GetEmployeeById(userId);
                    Thread t1 = new Thread(delegate ()
                    {
                        Smssend.passwordchange(emp.FirstName, emp.EmailId, emp.EmpCode);
                    });
                    t1.Start();
                    return RedirectToAction("EmployeeLogin", "Accounts").WithSuccess("Success", "Changed Successfully");
                }
                 
                
            }

        }
        public IActionResult Index()
        {

            //_recureHostedService.StartAsync(new System.Threading.CancellationToken());
            if (commonMethods.GetRoleCount() > 0)
            {
                return View();
            }
            else
            {

                return View(nameof(CreateSuperAdmin));
            }
        }

        public IActionResult CreateSuperAdmin()
        {

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveSuperAdmin([Bind("UserName,Password")]loginCls login)
        {

            if (!ModelState.IsValid)
            {
                ViewBag.ErrorMessage = "Please enter Username and Password";
                return View(nameof(CreateSuperAdmin));
            }
            else
            {
                long id = commonMethods.AddSuperAdmin(login.UserName, login.Password);
                if (id > 0)
                    return RedirectToAction("Index", "Accounts");
                else
                    return View(nameof(CreateSuperAdmin));


            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult LoginAccess([Bind("UserName,Password")]loginCls login)
        {
            var res = commonMethods.CheckLoginDetails(login.UserName, login.Password);
            if (res == null)
            {
                //saveDummy();
                // return RedirectToAction("Index", "Dashboard");

                ViewBag.ErrorMessage = "Invalid Username and Password. Please try again...";
                return View(nameof(Index));
            }
            else
            {

                HttpContext.Session.SetInt32("userId", Convert.ToInt32(res.EmployeeID));
                HttpContext.Session.SetInt32("accessId", Convert.ToInt32(res.ID));
                HttpContext.Session.SetInt32("roleId", Convert.ToInt32(res.RoleID));
                long roleId = HttpContext.Session.GetInt32("userId").Value;

                return RedirectToAction("Index", "Dashboard");


            }
        }
        private void saveDummy()
        {
            HttpContext.Session.SetInt32("userId", 1);
            HttpContext.Session.SetInt32("accessId", 1);
            HttpContext.Session.SetInt32("roleId", 1);
        }

        public IActionResult ForgotPassword()
        {
            return View();
        }
        public IActionResult AuthenticationFailed()
        {
            return View();
        }
        public IActionResult Logout()
        {
            CheckLoginStatus();
            HttpContext.Session.Clear();
            Response.Cookies.Delete("username");
            Response.Cookies.Delete("pas");
            if (roleId == 2 || roleId == 3 || roleId == 4)
            {              
                return RedirectToAction("EmployeeLogin", "Accounts");
            }
            else if (roleId == 5)
            {
                return RedirectToAction("StudentLogin", "Accounts");
            }
            else if (roleId == 6)
            {
                return RedirectToAction("ParentLogin", "Accounts");
            }
            else
            {
                return RedirectToAction("EmployeeLogin", "Accounts");
            }
        }
        #endregion

        #region Parent Login
        public IActionResult ParentLogin()
        {


            logincls login = new logincls();
            if (contextAccessor.HttpContext.Request.Cookies["username"] != null && contextAccessor.HttpContext.Request.Cookies["pas"] != null)
            {
                var username = contextAccessor.HttpContext.Request.Cookies["username"].ToString();
                var Password = contextAccessor.HttpContext.Request.Cookies["pas"].ToString();
                login.userName = username;
                login.Password = Password;
                login.isExist = true;
                var access = accessRepository.GetAllAccess().Where(a => a.Username == login.userName && a.Password == login.Password && a.RoleID == 6).FirstOrDefault();
                if (access != null)
                {
                    HttpContext.Session.SetInt32("userId", Convert.ToInt32(access.EmployeeID));
                    HttpContext.Session.SetInt32("accessId", Convert.ToInt32(access.ID));
                    HttpContext.Session.SetInt32("roleId", Convert.ToInt32(access.RoleID));

                    CookieOptions option = new CookieOptions();
                    option.Expires = DateTime.Now.AddDays(5);
                    Response.Cookies.Append("username", login.userName, option);
                    Response.Cookies.Append("pas", login.Password, option);
                    return RedirectToAction("ParentDashboard", "Dashboard");
                }
            }


            login.accessid = 0;
            login.confirmpassword = "";
            return View(login);
        }

        public IActionResult checkParentusername(logincls login)
        {           
                if (login.Password != null)
                {
                    var access = accessRepository.GetAllAccess().Where(a => a.Username == login.userName && a.Password == login.Password && a.RoleID == 6).FirstOrDefault();
                    if (access != null)
                    {
                        HttpContext.Session.SetInt32("userId", Convert.ToInt32(access.EmployeeID));
                        HttpContext.Session.SetInt32("accessId", Convert.ToInt32(access.ID));
                        HttpContext.Session.SetInt32("roleId", Convert.ToInt32(access.RoleID));
                        if (login.isRemember == true)
                        {
                            CookieOptions option = new CookieOptions();
                            option.Expires = DateTime.Now.AddDays(5);
                            Response.Cookies.Append("username", login.userName, option);
                            Response.Cookies.Append("pas", login.Password, option);


                        }
                        return RedirectToAction("ParentDashboard", "Dashboard");
                    }
                    else
                    {

                        login.isExist = true;
                        return View(nameof(ParentLogin), login).WithDanger("Error", "Invalid Username/Password. Try Again!");
                    }
                }
                else
                {
                    var access = accessRepository.GetAllAccess().Where(a => a.Username == login.userName && a.RoleID == 6).FirstOrDefault();
                    logincls logincls = new logincls();
                    if (access != null)
                    {
                        if (access.Password != null && access.Password != "")
                        {
                            logincls.isExist = true;
                            logincls.userName = login.userName;
                            return View(nameof(ParentLogin), logincls);
                        }
                        else
                        {
                            logincls.isExist = true;
                            logincls.userName = login.userName;
                            logincls.userId = access.EmployeeID;
                            var emp = studentAggregateRepository.GetParentById(access.EmployeeID);
                            string otp = smssend.random_password(6);
                            Thread t1 = new Thread(delegate ()
                            {
                                Smssend.Sendotp(otp, emp.PrimaryMobile);
                            });
                            t1.Start();

                            logincls.mobile = emp.PrimaryMobile;
                            logincls.accessid = access.ID;
                            HttpContext.Session.SetString("OTP", otp.ToString());
                            return RedirectToAction(nameof(ParentOTPValidation), logincls);
                        }
                    }
                    else
                    {
                        return View(nameof(ParentLogin), login).WithDanger("Error", "Invalid Username. Try Again!");
                    }
                } 
            
        }
        public IActionResult ParentOTPSend(string username)
        {
            logincls logincls = new logincls();
            var access = accessRepository.GetAllAccess().Where(a => a.Username == username && a.RoleID == 6).FirstOrDefault();
            if (access != null)
            {
                logincls.isExist = true;
                logincls.userName = username;
                logincls.userId = access.EmployeeID;
                var emp = studentAggregateRepository.GetParentById(access.EmployeeID);
                string otp = smssend.random_password(6);
                //Thread t1 = new Thread(delegate ()
                //{
                    Smssend.Sendotp(otp, emp.PrimaryMobile);
                //});
               // t1.Start();
                logincls.mobile = emp.PrimaryMobile;
                logincls.accessid = access.ID;
                HttpContext.Session.SetString("OTP", otp.ToString());
                return RedirectToAction(nameof(ParentOTPValidation), logincls).WithSuccess("Success", "OTP Send Successfully");
            }
            else
            {
                return View(nameof(ParentForgotPassword)).WithDanger("Error", "Incorrect Username.Try Again!");
            }
        }
        public IActionResult ParentOTPValidation(logincls logincls)
        {
            ViewBag.ErrorMessage = "";
            return View(logincls);
        }
        public IActionResult CheckParentOTPValidation(logincls logincls)
        {
            ViewBag.ErrorMessage = "";
            string otp = HttpContext.Session.GetString("OTP");
            if (otp == logincls.otp)
            {
                return RedirectToAction(nameof(ParentCreatePassword), logincls);
            }
            else
            {
                return View(nameof(ParentOTPValidation), logincls).WithDanger("Error", "Incorrect OTP");
            }

        }
        public JsonResult ParentResendOTP(string mobile)
        {
            ViewBag.ErrorMessage = "OTP send successfully";
            string otp = smssend.random_password(6);
            //Thread t1 = new Thread(delegate ()
            //{
                Smssend.Sendotp(otp, mobile);
            //});
           // t1.Start();

            HttpContext.Session.SetString("OTP", otp.ToString());
            return Json(1);
        }
        public IActionResult ParentCreatePassword(logincls logincls)
        {
            ViewBag.ErrorMessage = "";

            return View(logincls);
        }
    public IActionResult ParentSavePassword(logincls logincls)
    {
        var validation = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
        if (Regex.IsMatch(logincls.newPassword, validation))
        {
            if (logincls.newPassword == logincls.confirmpassword)
            {

                Access access = accessRepository.GetAccessById(logincls.accessid);
                if (access.Password == null || access.Password == "")
                {
                    var emp = studentAggregateRepository.GetParentById(access.EmployeeID);
                    Thread t1 = new Thread(delegate ()
                    {
                        Smssend.accountcreated(emp.FirstName, emp.EmailId, emp.PrimaryMobile);
                        Smssend.SendSms(emp.FirstName, emp.PrimaryMobile);
                    });
                    t1.Start();
                }
                access.Password = logincls.newPassword;
                accessRepository.UpdateAccess(access);

                HttpContext.Session.SetInt32("userId", Convert.ToInt32(access.EmployeeID));
                HttpContext.Session.SetInt32("accessId", Convert.ToInt32(access.ID));
                HttpContext.Session.SetInt32("roleId", Convert.ToInt32(access.RoleID));


                return RedirectToAction("ParentDashboard", "Dashboard");
            }
            else
            {
            }
            return View(nameof(ParentCreatePassword), logincls).WithDanger("Error", "Passwords are not same");
        }
        else
        {
            return View(nameof(ParentCreatePassword), logincls).WithDanger("Error", "Password should be contain minimum eight charecter and one upper case and one number and one special charecter");
        }
    }

        public IActionResult ParentChangePassword()
        {

            return View();
        }
        public IActionResult ParentSaveChangePassword(string password, string oldpassword)
        {
            var validation = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
            if (Regex.IsMatch(password, validation))
            {

                var userId = HttpContext.Session.GetInt32("userId").Value;
                var chk = accessRepository.GetAllAccess().Where(a => a.EmployeeID == userId && a.Password == oldpassword && a.RoleID == 6).FirstOrDefault();
                if (chk == null)
                {
                    return View(nameof(ParentChangePassword)).WithDanger("Error", "Old Password is not correct");
                }
                else
                {
                    var emp = studentAggregateRepository.GetParentById(userId);
                    Thread t1 = new Thread(delegate ()
                    {
                        Smssend.passwordchange(emp.FirstName, emp.EmailId, emp.PrimaryMobile);
                    });
                    t1.Start();
                    chk.Password = password;
                    chk.ModifiedId = userId;
                    accessRepository.UpdateAccess(chk);
                    HttpContext.Session.Clear();
                    Response.Cookies.Delete("username");
                    Response.Cookies.Delete("pas");
                    return RedirectToAction("ParentLogin", "Accounts").WithSuccess("Success", "Changed Successfully");
                }
            }
            else
            {
                return RedirectToAction("ParentChangePassword", "Accounts").WithDanger("Error", "Password should be contain minimum eight charecter and one upper case and one number and one special charecter");
            }

        }
        public IActionResult ParentForgotPassword()
        {
            return View();
        }
        public IActionResult ParentLogout()
        {
            HttpContext.Session.Clear();
            Response.Cookies.Delete("username");
            Response.Cookies.Delete("pas");
            return RedirectToAction("ParentLogin", "Accounts");
        }

        public JsonResult Getmodulesubmoduleaccess(long studentId)
        {
            HttpContext.Session.SetString("ddlstudentid", studentId.ToString());
            var studentclass = commonsql.Get_view_All_Academic_Students().Where(a => a.StudentId == studentId && a.IsAvailable == true).FirstOrDefault();
            var roleaccess = actionAccessRepository.GetAllActionRole().Where(a => a.RoleID == 6 && a.StandardID == studentclass.BoardStandardId).ToList();

            var modules = moduleRepository.GetAllModule();
            var subModules = subModuleRepository.GetAllSubModule();
            var moduleslist = (from a in roleaccess
                       join d in modules on a.ModuleID equals d.ID
                       select d).ToList().Distinct();
            var res = (from a in moduleslist
                       select new {
                           ID = a.ID,
                           ModuleName = a.Name,
                           submodules = (from e in roleaccess
                                         join f in subModules on e.SubModuleID equals f.ID
                                         where e.ModuleID == a.ID
                                         select new
                                         {
                                             ID = f.ID,
                                             SubmoduleName = f.Name,
                                             Url = f.Url
                                         }).ToList().Distinct()
                       }).ToList();
            
            return Json(res);
        }

        #endregion

        #region Student Login
        public IActionResult StudentLogin()
        {

            logincls login = new logincls();
            if (contextAccessor.HttpContext.Request.Cookies["username"] != null && contextAccessor.HttpContext.Request.Cookies["pas"] != null)
            {
                var username = contextAccessor.HttpContext.Request.Cookies["username"].ToString();
                var Password = contextAccessor.HttpContext.Request.Cookies["pas"].ToString();
                login.userName = username;
                login.Password = Password;
                login.isExist = true;
                var access = accessRepository.GetAllAccess().Where(a => a.Username == login.userName && a.Password == login.Password && a.RoleID==5).FirstOrDefault();
                if (access != null)
                {
                    HttpContext.Session.SetInt32("userId", Convert.ToInt32(access.EmployeeID));
                    HttpContext.Session.SetInt32("accessId", Convert.ToInt32(access.ID));
                    HttpContext.Session.SetInt32("roleId", Convert.ToInt32(access.RoleID));

                    CookieOptions option = new CookieOptions();
                    option.Expires = DateTime.Now.AddDays(5);
                    Response.Cookies.Append("username", login.userName, option);
                    Response.Cookies.Append("pas", login.Password, option);
                    return RedirectToAction("StudentDashboard", "Dashboard");
                }
            }


            login.accessid = 0;
            login.confirmpassword = "";
            return View(login);
        }

        public IActionResult checkStudentusername(logincls login)
        {
            CheckLoginStatus();
            if (login.Password != null)
            {
                var access = accessRepository.GetAllAccess().Where(a => a.Username == login.userName && a.Password == login.Password && a.RoleID == 5).FirstOrDefault();
                if (access != null)
                {
                    HttpContext.Session.SetInt32("userId", Convert.ToInt32(access.EmployeeID));
                    HttpContext.Session.SetInt32("accessId", Convert.ToInt32(access.ID));
                    HttpContext.Session.SetInt32("roleId", Convert.ToInt32(access.RoleID));
                    if (login.isRemember == true)
                    {
                        CookieOptions option = new CookieOptions();
                        option.Expires = DateTime.Now.AddDays(5);
                        Response.Cookies.Append("username", login.userName, option);
                        Response.Cookies.Append("pas", login.Password, option);


                    }
                    return RedirectToAction("StudentDashboard", "Dashboard");
                }
                else
                {

                    login.isExist = true;
                    return View(nameof(StudentLogin), login).WithDanger("Error", "Invalid Username/Password. Try Again!");
                }
            }
            else
            {
                var access = accessRepository.GetAllAccess().Where(a => a.Username == login.userName && a.RoleID == 5).FirstOrDefault();
                logincls logincls = new logincls();
                if (access != null && roleId == 6)
                {
                    if (access.Password != null && access.Password != "")
                    {
                        logincls.isExist = true;
                        logincls.userName = login.userName;
                        return View(nameof(StudentLogin), logincls);
                    }
                    else
                    {
                        logincls.isExist = true;
                        logincls.userName = login.userName;
                        logincls.userId = access.EmployeeID;
                        var emp = studentAggregateRepository.GetAllParent().Where(a => a.StudentID == access.EmployeeID).ToList();
                        string otp = smssend.random_password(6);
                        foreach (var a in emp)
                        {
                            if (a.PrimaryMobile != null)
                            {
                                Thread t1 = new Thread(delegate ()
                                {
                                    Smssend.Sendotp(otp, a.PrimaryMobile);
                                });
                                t1.Start();
                            }
                        }
                        logincls.mobilelist = emp.Select(a => a.PrimaryMobile).ToArray();
                        logincls.accessid = access.ID;
                        HttpContext.Session.SetString("OTP", otp.ToString());
                        return RedirectToAction(nameof(StudentOTPValidation), logincls);
                    }
                }
                else
                {
                    if (access == null)
                    {
                        return View(nameof(StudentLogin), login).WithDanger("Error", "Invalid Username. Try Again!");
                    }
                    else
                    {
                        if (access.Password == "")
                        {
                            return View(nameof(StudentLogin), login).WithDanger("Error", "You don't have Password... Please ask your parents");
                        }
                        else
                        {
                            logincls.isExist = true;
                            logincls.userName = login.userName;
                            return View(nameof(StudentLogin), logincls);
                        }
                    }
                }
            }
        }
        public IActionResult StudentOTPSend(string username)
        {
            logincls logincls = new logincls();
            var access = accessRepository.GetAllAccess().Where(a => a.Username == username && a.RoleID == 5).FirstOrDefault();
            if (access != null)
            {
                logincls.isExist = true;
                logincls.userName = username;
                logincls.userId = access.EmployeeID;
               // var emp = studentAggregateRepository.GetParentById(access.EmployeeID);
                var emp = studentAggregateRepository.GetAllParent().Where(a => a.StudentID == access.EmployeeID).ToList();
                string otp = smssend.random_password(6);
                foreach (var a in emp)
                {
                    Thread t1 = new Thread(delegate ()
                    {
                        Smssend.Sendotp(otp, a.PrimaryMobile);
                    });
                    t1.Start();
                }
                //logincls.mobile = emp.PrimaryMobile;
                logincls.mobilelist = emp.Select(a => a.PrimaryMobile).ToArray();
                logincls.accessid = access.ID;
                HttpContext.Session.SetString("OTP", otp.ToString());
                return RedirectToAction(nameof(StudentOTPValidation), logincls).WithSuccess("Success", "OTP Send Successfully");
            }
            else
            {
                return View(nameof(StudentForgotPassword)).WithDanger("Error", "Incorrect Username.Try Again!");
            }
        }
        public IActionResult StudentOTPValidation(logincls logincls)
        {
            ViewBag.ErrorMessage = "";
            return View(logincls);
        }
        public IActionResult CheckStudentOTPValidation(logincls logincls)
        {
            ViewBag.ErrorMessage = "";
            string otp = HttpContext.Session.GetString("OTP");
            if (otp == logincls.otp)
            {
                return RedirectToAction(nameof(StudentCreatePassword), logincls);
            }
            else
            {
                return View(nameof(StudentOTPValidation), logincls).WithDanger("Error", "Incorrect OTP");
            }

        }
        public JsonResult StudentResendOTP(string[] mobilelist)
        {
            ViewBag.ErrorMessage = "OTP send successfully";
            string otp = smssend.random_password(6);
            foreach (var a in mobilelist)
            {
                Thread t1 = new Thread(delegate ()
                {
                    Smssend.Sendotp(otp, a);
                });
                t1.Start();
            }
            HttpContext.Session.SetString("OTP", otp.ToString());
            return Json(1);
        }
        public IActionResult StudentCreatePassword(logincls logincls)
        {
            ViewBag.ErrorMessage = "";

            return View(logincls);
        }
        public IActionResult StudentSavePassword(logincls logincls)
        {
            if (logincls.newPassword == logincls.confirmpassword)
            { 

                Access access = accessRepository.GetAccessById(logincls.accessid);
                if (access.Password == null || access.Password == "")
                {
                    var emp = studentAggregateRepository.GetStudentById(access.EmployeeID);
                    var parents = studentAggregateRepository.GetAllParent().Where(a => a.StudentID == access.EmployeeID).Select(a => a.EmailId).ToList();
                    foreach (var a in parents)
                    {
                        if (a != null)
                        {
                            Thread t1 = new Thread(delegate ()
                            {
                                Smssend.accountcreated(emp.FirstName, a, emp.StudentCode);
                            });
                            t1.Start();
                        }
                    }
                }
                access.Password = logincls.newPassword;
                accessRepository.UpdateAccess(access);

                HttpContext.Session.SetInt32("userId", Convert.ToInt32(access.EmployeeID));
                HttpContext.Session.SetInt32("accessId", Convert.ToInt32(access.ID));
                HttpContext.Session.SetInt32("roleId", Convert.ToInt32(access.RoleID));


                return RedirectToAction("StudentDashboard", "Dashboard");
            }
            else
            {
            }
            return View(nameof(StudentCreatePassword), logincls).WithDanger("Error", "Passwords are not same");
        }

        public IActionResult StudentSaveChangePasswords(logincls logincls)
        {
            var validation = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
            if (Regex.IsMatch(logincls.newPassword, validation))
            {
                CheckLoginStatus();
                if (logincls.oldpassword == null)
                {
                    if (logincls.newPassword == logincls.confirmpassword)
                    {

                        Access access = accessRepository.GetAllAccess().Where(a => a.EmployeeID == logincls.StudentId && a.RoleID == 5).FirstOrDefault();
                        if (access.Password == null || access.Password == "")
                        {
                            var emp = studentAggregateRepository.GetStudentById(logincls.StudentId);
                            var parents = studentAggregateRepository.GetAllParent().Where(a => a.StudentID == access.EmployeeID).ToList();
                            foreach (var a in parents)
                            {
                                if (a != null)
                                {
                                    Thread t1 = new Thread(delegate ()
                                    {
                                        Smssend.Passwordcreated(a.FirstName, emp.FirstName, a.EmailId, emp.StudentCode);
                                        Smssend.SendStudentPasswordCreateSms(a.FirstName, emp.FirstName, a.PrimaryMobile);
                                    });
                                    t1.Start();
                                }
                            }
                        }
                        access.Password = logincls.newPassword;
                        accessRepository.UpdateAccess(access);

                        HttpContext.Session.SetInt32("userId", Convert.ToInt32(access.EmployeeID));
                        HttpContext.Session.SetInt32("accessId", Convert.ToInt32(access.ID));
                        HttpContext.Session.SetInt32("roleId", Convert.ToInt32(access.RoleID));


                        return RedirectToAction("StudentDashboard", "Dashboard").WithSuccess("Success", "Password Created Successfully");
                    }
                    else
                    {
                        return RedirectToAction("ChangePasswords", "Dashboard").WithDanger("Error", "New assword and Confirm password are not same");
                    }
                }
                else
                {
                    Access access = accessRepository.GetAllAccess().Where(a => a.EmployeeID == logincls.StudentId && a.RoleID == 5).FirstOrDefault();
                    var chk = accessRepository.GetAllAccess().Where(a => a.EmployeeID == access.EmployeeID && a.Password == logincls.oldpassword && a.RoleID == 5).FirstOrDefault();
                    if (chk == null)
                    {
                        return RedirectToAction("ChangePasswords", "Dashboard").WithDanger("Error", "Old Password is not correct");
                    }
                    else
                    {
                        var student = studentAggregateRepository.GetStudentById(logincls.StudentId);
                        var emp = studentAggregateRepository.GetAllParent().Where(a => a.StudentID == logincls.StudentId).FirstOrDefault();
                        Thread t1 = new Thread(delegate ()
                        {
                            Smssend.Studentpasswordchange(emp.FirstName, student.FirstName, emp.EmailId, emp.PrimaryMobile);
                            Smssend.SendStudentPasswordChangeSms(emp.FirstName, student.FirstName, emp.PrimaryMobile);
                        });
                        t1.Start();
                        chk.Password = logincls.newPassword;
                        chk.ModifiedId = userId;
                        accessRepository.UpdateAccess(chk);
                        HttpContext.Session.Clear();
                        Response.Cookies.Delete("username");
                        Response.Cookies.Delete("pas");
                        return RedirectToAction("StudentDashboard", "Dashboard").WithSuccess("Success", "Password Changed Successfully");
                    }
                }

            }
            else
            {
                return RedirectToAction("ChangePasswords", "Dashboard").WithDanger("Error", "Password should be contain minimum eight charecter and one upper case and one number and one special charecter");
            }
            
        }

        public IActionResult StudentChangePassword()
        {

            return View();
        }
        public IActionResult StudentSaveChangePassword(string password, string oldpassword)
        {

            var userId = HttpContext.Session.GetInt32("userId").Value;
            var chk = accessRepository.GetAllAccess().Where(a => a.EmployeeID == userId && a.Password == oldpassword && a.RoleID == 5).FirstOrDefault();
            if (chk == null)
            {
                return View(nameof(StudentChangePassword)).WithDanger("Error", "Old Password is not correct");
            }
            else
            {
                var emp = studentAggregateRepository.GetParentById(userId);
                Thread t1 = new Thread(delegate ()
                {
                    Smssend.passwordchange(emp.FirstName, emp.EmailId, emp.PrimaryMobile);
                });
                t1.Start();
                chk.Password = password;
                chk.ModifiedId = userId;
                accessRepository.UpdateAccess(chk);
                HttpContext.Session.Clear();
                Response.Cookies.Delete("username");
                Response.Cookies.Delete("pas");
                return RedirectToAction("StudentLogin", "Accounts").WithSuccess("Success", "Changed Successfully");
            }

        }
        public IActionResult StudentForgotPassword()
        {
            return View();
        }
        public IActionResult StudentLogout()
        {
            HttpContext.Session.Clear();
            Response.Cookies.Delete("username");
            Response.Cookies.Delete("pas");
            return RedirectToAction("StudentLogin", "Accounts");
        }
        #endregion
    }
}