﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;

namespace Web.Controllers
{
    public class LoginController : AppController
    {
        public ILogingRepository logingRepository;
        public IAccessRepository accessRepository;
        
        public LoginController(ILogingRepository logingRepository, IAccessRepository accessRepository)
        {
            this.logingRepository = logingRepository;
            this.accessRepository = accessRepository;
        }

        public IActionResult Index()
        {
            var result = logingRepository.ListAllAsyncIncludeAll().Result.ToList();
            return View(result);
        }

        public IActionResult Create()
        {
            ViewData["Logins"] = new[] { new SelectListItem { Text = "Select Action", Value = "0" } }.Concat(new SelectList(accessRepository.GetAllAccess(), "ID", "ID"));

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateLogin([Bind("Type,AccessID,FCMID,Data")] Login login)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {

                login.InsertedDate = DateTime.Now;
                login.ModifiedDate = DateTime.Now;
                login.InsertedId = userId;
                login.ModifiedId = userId;
                login.Active = true;
                login.Status = "ACTIVE";
                login.IsAvailable = false;

                await logingRepository.AddAsync(login);
            }

            return RedirectToAction(nameof(Index)); 
        }

        public IActionResult Details(long id)
        {
            var result = logingRepository.GetByIdAsyncIncludeAll(id);
            return View(result.Result);
        }

        public IActionResult Delete(long id)
        {
            var result = logingRepository.GetByIdAsyncIncludeAll(id);

            logingRepository.DeleteAsync(result.Result);
            return RedirectToAction(nameof(Index));
        }
    }
}