﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Web.Models;
using OdmErp.Web.Models.StudentPayment;
using Web.Controllers;
using X.PagedList.Mvc;
using X.PagedList;
using OfficeOpenXml;
using ClosedXML.Excel;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using System.Data;
using OdmErp.ApplicationCore.Entities.SessionFeesType;
using OdmErp.ApplicationCore.Entities.Pagination;

namespace OdmErp.Web.Controllers
{

    public class SessionFeesTypeController : AppController
    {
        private CommonMethods commonMethods;
        private IFeesTypeApplicableRepository feesTypeApplicableRepository;
        private IFeesTypeRepository feesTypeRepository;
        private IScholarshipApplicableRepository scholarshipApplicableRepository;
        public CommonSchoolModel CommonSchoolModel;
        private ICountryRepository countryRepository;
        private IAdmissionTypeRepository admissionTypeRepository;
        private IBankTypeRepository bankTypeRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private IBoardRepository boardRepository;
        private IBoardStandardRepository boardStandardRepository;
        private IDesignationLevelRepository designationLevelRepository;
        private IEducationQualificationTypeRepository educationQualificationTypeRepository;
        private IGroupRepository groupRepository;
        private IModuleRepository moduleRepository;
        private IOrganizationTypeRepository organizationTypeRepository;
        private IRoleRepository roleRepository;
        private ISectionRepository sectionRepository;

        private IStateRepository stateRepository;
        private IDepartmentRepository departmentRepository;
        private IDesignationRepository designationRepository;
        private IEducationQualificationRepository educationQualificationRepository;
        private IStandardRepository standardRepository;
        private IStreamRepository streamRepository;
        private ISubModuleRepository subModuleRepository;
        private ICityRepository cityRepository;
        private IOrganizationRepository organizationRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IReligionTypeRepository religionTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private IBankRepository bankRepository;
        private IBankBranchRepository bankBranchRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IAccessTypeRepository accessTypeRepository;
        private IActionRepository actionRepository;
        private IAuthorityAggregateRepository authorityAggregateRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IActionAccessRepository actionAccessRepository;

        private IDepartmentLeadRepository departmentLeadRepository;
        private IEmployeeRepository employeeRepository;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IHttpContextAccessor _httpContextAccessor;
        public Extensions.SessionExtensions sessionExtensions;

        public IAcademicSessionRepository academicSessionRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IClassTimingRepository classTimingRepository;
        public commonsqlquery commonsql;
        public ISessionFeesType sessionfeestype;
        private IFeesTypePrice feestypePriceRepository;

        private readonly IFileProvider fileProvider;
        private readonly IHostingEnvironment hostingEnvironment;

        public SessionFeesTypeController(IFeesTypeApplicableRepository feesTypeApplicableRepo, IFeesTypeRepository feesTypeRepo, IScholarshipApplicableRepository scholarshipApplicableRepo, ICountryRepository countryRepo, IAdmissionTypeRepository admissionTypeRepo, IBloodGroupRepository bloodGroupRepo,
                   IBoardRepository boardRepo, IDesignationLevelRepository designationLevelRepo, IEducationQualificationTypeRepository educationQualificationTypeRepo, IGroupRepository groupRepo,
                   IModuleRepository moduleRepo, IOrganizationTypeRepository organizationTypeRepo, IRoleRepository roleRepo, ISectionRepository sectionRepo,
                   IStateRepository stateRepo, IDepartmentRepository departmentRepo, IDesignationRepository designationRepo, IEducationQualificationRepository educationQualificationRepo, IStandardRepository standardRepo,
                   IStreamRepository streamRepo, ISubModuleRepository subModuleRepo, ICityRepository cityRepo, IOrganizationRepository organizationRepo, INationalityTypeRepository nationalityTypeRepo, IReligionTypeRepository religionTypeRepo,
                   IAddressTypeRepository addressTypeRepo, IBankRepository bankRepo, IBankBranchRepository bankBranchRepo, IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo,
                   IAccessTypeRepository accessTypeRepo, IActionRepository actionRepo, CommonMethods commonMeth, CommonSchoolModel commonSchoolModel, IAuthorityAggregateRepository authorityAggregateRepo, IStudentAggregateRepository studentAggregateRepo, IActionAccessRepository actionAccessRepo,
                   IDepartmentLeadRepository departmentlead, IHttpContextAccessor httpContextAccessor, IEmployeeRepository emprepo, IEmployeeDesignationRepository empdesrepo, IBankTypeRepository bankTypeRepo,
                   IBoardStandardRepository boardStandardRepository, IClassTimingRepository classTimingRepo, IAcademicSessionRepository academicSessionRepository, commonsqlquery commonsqlquery, IOrganizationAcademicRepository organizationAcademicRepository
            , ISessionFeesType sessionfeestyperepo, IFileProvider fileProvider, IHostingEnvironment hostingEnvironment, IFeesTypePrice feestypePriceRepository
                 )
        {
            feesTypeApplicableRepository = feesTypeApplicableRepo;
            scholarshipApplicableRepository = scholarshipApplicableRepo;
            feesTypeRepository = feesTypeRepo;
            countryRepository = countryRepo;
            admissionTypeRepository = admissionTypeRepo;
            bloodGroupRepository = bloodGroupRepo;
            boardRepository = boardRepo;
            designationLevelRepository = designationLevelRepo;
            educationQualificationTypeRepository = educationQualificationTypeRepo;
            groupRepository = groupRepo;
            moduleRepository = moduleRepo;
            organizationTypeRepository = organizationTypeRepo;
            roleRepository = roleRepo;
            sectionRepository = sectionRepo;
            bankTypeRepository = bankTypeRepo;
            stateRepository = stateRepo;
            departmentRepository = departmentRepo;
            designationRepository = designationRepo;
            educationQualificationRepository = educationQualificationRepo;
            standardRepository = standardRepo;
            streamRepository = streamRepo;
            subModuleRepository = subModuleRepo;
            cityRepository = cityRepo;
            organizationRepository = organizationRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            religionTypeRepository = religionTypeRepo;
            addressTypeRepository = addressTypeRepo;
            bankRepository = bankRepo;
            bankBranchRepository = bankBranchRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            accessTypeRepository = accessTypeRepo;
            actionRepository = actionRepo;
            commonMethods = commonMeth;
            authorityAggregateRepository = authorityAggregateRepo;
            studentAggregateRepository = studentAggregateRepo;
            actionAccessRepository = actionAccessRepo;
            _httpContextAccessor = httpContextAccessor;
            departmentLeadRepository = departmentlead;
            employeeRepository = emprepo;
            employeeDesignationRepository = empdesrepo;
            this.boardStandardRepository = boardStandardRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            commonsql = commonsqlquery;
            classTimingRepository = classTimingRepo;
            sessionfeestype = sessionfeestyperepo;
            CommonSchoolModel = commonSchoolModel;
            this.fileProvider = fileProvider;
            this.hostingEnvironment = hostingEnvironment;
            this.feestypePriceRepository = feestypePriceRepository;
        }
        public IActionResult SessionFeesType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("SessionFeesType", accessId, "List", "SessionFeesType", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
           
            var onePageOfEmps = CommonSchoolModel.GetAllSessionFeesType().ToPagedList();
            return View("SessionFeesType", onePageOfEmps);
        }


        //[HttpPost]
        //public IActionResult LoadDataSessionFeesType()
        //{
        //    var draw = Request.Form["draw"].FirstOrDefault();
        //    var start = Request.Form["start"].FirstOrDefault();
        //    var length = Request.Form["length"].FirstOrDefault();
        //    //Get Sort columns value
        //    var searchValue = Request.Form["search[value]"].FirstOrDefault();
        //    var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
        //    var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
        //    int pageSize = length != null ? Convert.ToInt32(length) : 0;
        //    int skip = start != null ? Convert.ToInt32(start) : 0;
        //    int totalRecords = 0;
        //    PaginationSessionFesstype objpg = sessionfeestype.GetPaginationAllSessionFeesType(draw, sortColumn, sortColumnDir, pageSize, skip, totalRecords, searchValue);
        //    return Json(objpg);
        //}



        [HttpGet]
        public JsonResult CreateOrEditSessionFeeType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            ApplicationCore.Entities.SessionFeesType.SessionFeesType od = new ApplicationCore.Entities.SessionFeesType.SessionFeesType();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("SessionFeesType", accessId, "Create", "SessionFeesTypePrice", roleId) == false)
                {
                    //return Redirect("AuthenticationFailed", "Accounts");
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Create";
                od.ID = 0;
            }
            else
            {
                if (commonMethods.checkaccessavailable("SessionFeesType", accessId, "Edit", "SessionFeesTypePrice", roleId) == false)
                {
                    Json(new
                    {
                        redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                        isRedirect = true
                    });
                }

                ViewBag.status = "Update";
                var sessionfessType = sessionfeestype.GetSessionFeesTypeById(id.Value);
                od.ID = sessionfessType.ID;
                od.SessionId = sessionfessType.SessionId;
                od.FeesTypeId = sessionfessType.FeesTypeId;
                od.ApplicableId = sessionfessType.ApplicableId;
                od.Deadlinedate = sessionfessType.Deadlinedate;
                od.IsMandatory = sessionfessType.IsMandatory;

            }
            return Json(od);
        }
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult SaveOrUpdateSessionFeesType(CS_SessionFeesType sessionFeesType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                OdmErp.ApplicationCore.Entities.SessionFeesType.SessionFeesType sessionFeesTypePrice = new OdmErp.ApplicationCore.Entities.SessionFeesType.SessionFeesType();

                sessionFeesTypePrice.SessionId = sessionFeesType.SessionId;
                sessionFeesTypePrice.ApplicableId = sessionFeesType.ApplicableId;
                sessionFeesTypePrice.FeesTypeId = sessionFeesType.FeesTypeId;
                sessionFeesTypePrice.Deadlinedate = sessionFeesType.Deadlinedate;
                sessionFeesTypePrice.IsMandatory = sessionFeesType.IsMandatory;
                sessionFeesTypePrice.Active = true;
                if (sessionFeesTypePrice.ID == 0)
                {
                    sessionFeesTypePrice.InsertedId = userId;
                    sessionFeesTypePrice.Status = EntityStatus.ACTIVE;


                    if (FeesTypePriceExists(sessionFeesTypePrice.SessionId, sessionFeesTypePrice.FeesTypeId) == true)
                    {

                        long i = sessionfeestype.CreateSessionFeesType(sessionFeesTypePrice);
                        if (i > 0)
                        {
                            sessionFeesType.ReturnMsg = "Records added Successfully.";
                        }
                        else
                        {
                            sessionFeesType.ReturnMsg = "Records added Faild.!!!";
                        }
                    }
                    else
                    {
                        sessionFeesType.ReturnMsg = "Records already Exists!!!";
                    }
                   
                    //}
                    //else
                    //{
                    //    feesTypeApplicable.ReturnMsg = "Records already Exists !!!";
                    //}
                    //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Saved successfully");
                    //return Json(feesTypeApplicable.ReturnMsg);
                }
               
            }
            return Json(sessionFeesType); 
        }

        public JsonResult Update(CS_SessionFeesType UpdateSessionfeesTypePrice)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                OdmErp.ApplicationCore.Entities.SessionFeesType.SessionFeesType sessionfeesTypePrice = new OdmErp.ApplicationCore.Entities.SessionFeesType.SessionFeesType();
                sessionfeesTypePrice.ID = UpdateSessionfeesTypePrice.ID;
                sessionfeesTypePrice.SessionId = UpdateSessionfeesTypePrice.SessionId;
                sessionfeesTypePrice.FeesTypeId = UpdateSessionfeesTypePrice.FeesTypeId;
                sessionfeesTypePrice.ApplicableId = UpdateSessionfeesTypePrice.ApplicableId;
                sessionfeesTypePrice.Deadlinedate = UpdateSessionfeesTypePrice.Deadlinedate;
                sessionfeesTypePrice.IsMandatory = UpdateSessionfeesTypePrice.IsMandatory;
                sessionfeesTypePrice.Active = true;

                //TODO
                sessionfeesTypePrice.ModifiedId = userId;
                bool i=sessionfeestype.UpdateSessionFeesType(sessionfeesTypePrice);
                if (i == true)
                {
                }
                UpdateSessionfeesTypePrice.ReturnMsg = "Records Updated Successfully.";
                //return RedirectToAction(nameof(FeesTypeApplicable)).WithSuccess("success", "Updated successfully");
                //return Json("");
            }
            return Json(UpdateSessionfeesTypePrice); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

        public JsonResult DeleteSessionFeesType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("SessionFeesType", accessId, "Delete", "SessionFeesTypePrice", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            if (CheckForDelete(id.Value) == true)
            {
                var feesTypePrice = sessionfeestype.DeleteSessionFeesType(id.Value);
            }
            return Json("Deleted successfully");
        }

        public JsonResult DeleteFeesTypePriceAll()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("FeesType Price", accessId, "Delete", "FeesTypePrice", roleId) == false)
            {
                //return RedirectToAction("AuthenticationFailed", "Accounts");
                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }

            var feesTypeApplicable = sessionfeestype.DeleteAllSessionFeesType();

            return Json("Deleted successfully");
        }

        private bool FeesTypePriceExists(long sessionId, long FeestypeId)
        {

            var res = sessionfeestype.CheckSessionFeesType().Where(s => s.SessionId == sessionId && s.FeesTypeId == FeestypeId && s.Active==true).ToList();
                //.Where(x => x.SessionId == id).ToList();
            if (res.Count > 0 )
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckForDelete(long id)
        {
            if(CreateOrEditSessionFeeType(id)!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public List<FeesTypeApplicable> Get_FeesTypeApplicable()
        {

            List<FeesTypeApplicable> feesType = new List<FeesTypeApplicable>();
            IEnumerable<FeesTypeApplicable> myGenericEnumerable = (IEnumerable<FeesTypeApplicable>)feesType;
            myGenericEnumerable = feesTypeApplicableRepository.GetAllFeesTypeApplicable(0, 0);
            var res = (from a in myGenericEnumerable.ToList()
                       select new FeesTypeApplicable
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            feesType = res;

            return feesType;
        }

        public List<FeesType> Get_All_FeesType()
        {

            List<FeesType> feesType1 = new List<FeesType>();
            IEnumerable<FeesType> myGenericEnumerable1 = (IEnumerable<FeesType>)feesType1;
            myGenericEnumerable1 = feesTypeRepository.GetAllFeesType().Where(a => a.ParentFeeTypeID == 0 && a.Active == true).ToList();
            var res1 = (from a in myGenericEnumerable1.ToList()
                       select new FeesType
                       {
                           ID = a.ID,
                           Name = a.Name,
                           //ParentFeeTypeName = a.ParentFeeTypeName==null?"N A":a.ParentFeeTypeName,
                           ParentFeeTypeName= a.ParentFeeTypeName,
                       }).ToList();
            feesType1 = res1;

            return feesType1;
        }


        #region FeesType Download & Upload Excel
        public IActionResult downloadSessionFeesTypeSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("SessionFeesType", accessId, "Upload", "SessionFeesType", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            XLWorkbook oWB = new XLWorkbook();
            var wsData = oWB.Worksheets.Add("Data");
            wsData.Cell(1, 1).Value = "Session";
            wsData.Cell(1, 2).Value = "FeesType";
            wsData.Cell(1, 3).Value = "ApplicableTo";
            wsData.Cell(1, 4).Value = "DeadLineDate";
            wsData.Cell(1, 5).Value = "IsMandatory";
            int z = 1;
            string[] arr = { "0", "1" };
            for (int a = 0; a < arr.Length; a++)
            {
                wsData.Cell(++z, 5).Value = a;
            }
            wsData.Range("E2:E" + z).AddToNamed("IsMandatory");

            var feestype = feesTypeRepository.GetAllFeesType();
            var session = academicSessionRepository.GetAllAcademicSession(); //.Where(s => s.IsAvailable = true).Select(s => s.DisplayName).FirstOrDefault();
           
            int g = 1;
            foreach (var a in session)
            {
                wsData.Cell(++g, 1).Value = a.DisplayName;
            }
            wsData.Range("A2:A" + g).AddToNamed("Session");
           int k = 1;
            foreach (var d in feestype)
            {
                wsData.Cell(++k, 2).Value = d.Name;
            }
            wsData.Range("B2:B" + k).AddToNamed("FeesType");
            int t = 1;
            var Applicableto = feesTypeApplicableRepository.GetAllFeesTypeApplicable(0, 0);
            foreach (var p in Applicableto)
            {
                wsData.Cell(++t, 3).Value = p.Name;
            }
            wsData.Range("C2:C" + t).AddToNamed("ApplicableTo");


            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Session");
            validationTable.Columns.Add("FeesType");
            validationTable.Columns.Add("ApplicableTo");
            validationTable.Columns.Add("DeadLineDate");
            validationTable.Columns.Add("IsMandatory");
            //validationTable.Columns.Add("SubCategory");
            //validationTable.Columns.Add("Building");
            //validationTable.Columns.Add("Room");
            //validationTable.Columns.Add("Priority");
            //validationTable.Columns.Add("Name");
            //validationTable.Columns.Add("Description");
            //validationTable.Columns.Add("Status");
            validationTable.TableName = "SessionFeesType";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(wsData.Range("Session"), true);
            worksheet.Column(2).SetDataValidation().List(wsData.Range("FeesType"), true);
            worksheet.Column(3).SetDataValidation().List(wsData.Range("ApplicableTo"), true);
            //worksheet.Column(4).SetDataValidation().List(wsData.Range("DeadLineDate"), true);
            worksheet.Column(5).SetDataValidation().List(wsData.Range("IsMandatory"), true);

            worksheet.Column(1).SetDataValidation().InCellDropdown = true;
            worksheet.Column(2).SetDataValidation().InCellDropdown = true;
            worksheet.Column(3).SetDataValidation().InCellDropdown = true;
            worksheet.Column(5).SetDataValidation().InCellDropdown = true;
            //worksheet.Column(3).SetDataValidation().Operator = XLOperator.Between;
            //worksheet.Column(3).SetDataValidation().AllowedValues = XLAllowedValues.List;
            //worksheet.Column(3).SetDataValidation().List("=INDIRECT(SUBSTITUTE(B1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
            //worksheet.Column(4).SetDataValidation().List(wsData1.Range("Building"), true);
            //worksheet.Column(5).SetDataValidation().InCellDropdown = true;
            //worksheet.Column(5).SetDataValidation().Operator = XLOperator.Between;
            //worksheet.Column(5).SetDataValidation().AllowedValues = XLAllowedValues.List;
            //worksheet.Column(5).SetDataValidation().List("=INDIRECT(SUBSTITUTE(D1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
            //worksheet.Column(6).SetDataValidation().List(wsData1.Range("Priority"), true);
            wsData.Hide();
            // wsData1.Hide();
            // worksheet7.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SeesionFeesType.xlsx");
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IActionResult UploadSessionFeesType(IFormFile file)
        {
            long Yn = 0; string ReturnMsg = "";
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("SessionFeesType", accessId, "Upload", "SessionFeesType", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    // var request = supportRepository.GetAllSupportRequest();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        SessionFeesType sessionfesstype = new SessionFeesType();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        var sessionId= Convert.ToInt64(academicSessionRepository.GetAllAcademicSession().Where(s => s.IsAvailable = true  && s.DisplayName==name).Select(s => s.ID).FirstOrDefault());
                        string feestypename = firstWorksheet.Cells[i, 2].Value.ToString();
                        var feestypeids= Convert.ToInt64(feesTypeRepository.GetAllFeesType().Where(y => y.IsAvailable = true && y.Name == feestypename).Select(y => y.ID).FirstOrDefault());
                        var applicabletoname = firstWorksheet.Cells[i, 3].Value.ToString();
                        var applicabletoid= Convert.ToInt64(feesTypeApplicableRepository.GetAllFeesTypeApplicable(0, 0).Where(c => c.IsAvailable = true && c.Name == applicabletoname).Select(c => c.ID).FirstOrDefault());
                        var deadlinedate = firstWorksheet.Cells[i, 4].Value.ToString();
                        bool Ismandatory = Convert.ToBoolean(firstWorksheet.Cells[i, 5].Value);

                        sessionfesstype.SessionId = sessionId;
                        sessionfesstype.FeesTypeId = feestypeids;
                        sessionfesstype.ApplicableId = applicabletoid;
                        sessionfesstype.Deadlinedate = Convert.ToDateTime(deadlinedate);
                        sessionfesstype.IsMandatory = Ismandatory;
                        sessionfesstype.ModifiedId = userId;
                        sessionfesstype.Active = true;
                        sessionfesstype.InsertedId = userId;
                        sessionfesstype.Status = EntityStatus.ACTIVE;
                        if (FeesTypePriceExists(sessionId, feestypeids) == true)
                        {
                            long id = sessionfeestype.CreateSessionFeesType(sessionfesstype);
                            if (id > 1)
                            {
                                ReturnMsg = "Excel uploaded successfully";
                            }
                            else
                            {
                                ReturnMsg = "Excel uploaded Failed !!!";
                            }
                        }
                        else
                        {
                            ReturnMsg = "Records already Exists!!!";
                        }
                    }
                }
                #region Oldcode
                //// Create a File Info 
                //FileInfo fi = new FileInfo(file.FileName);
                //var newFilename = "FeesType_" + String.Format("{0:d}",
                //                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                ////var webPath = "";
                ////if (string.IsNullOrWhiteSpace(hostingEnvironment.WebRootPath))
                ////{
                ////    webPath= hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                ////}
                //var webPath = hostingEnvironment.WebRootPath;
                //string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                //var pathToSave = newFilename;
                //using (var stream = new FileStream(path, FileMode.Create))
                //{
                //    file.CopyToAsync(stream);
                //}
                //FileInfo fil = new FileInfo(path);
                //using (ExcelPackage excelPackage = new ExcelPackage(fil))
                //{

                //    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                //    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                //    int totalRows = firstWorksheet.Dimension.Rows;
                //    for (int i = 2; i <= totalRows; i++)
                //    {
                //        FeesType feestypeApp = new FeesType();
                //        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                //        string Parentname= Convert.ToInt32(firstWorksheet.Cells[i, 4].Value).ToString();
                //        var ParentFeeTypeID = feesTypeRepository.GetAllFeesType().Where(m => m.Name == Parentname).FirstOrDefault().ID;
                //        if (feesTypeRepository.GetFeesTypeByName(name) == null)
                //        {
                //            feestypeApp.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                //            feestypeApp.Description = firstWorksheet.Cells[i, 2].Value.ToString();
                //            feestypeApp.IsSubType= Convert.ToBoolean(firstWorksheet.Cells[i, 3].Value);
                //            feestypeApp.ParentFeeTypeID = ParentFeeTypeID;
                //            feestypeApp.ModifiedId = userId;
                //            feestypeApp.Active = true;
                //            feestypeApp.InsertedId = userId;
                //            feestypeApp.Status = EntityStatus.ACTIVE;
                //            Yn = feesTypeRepository.CreateFeesType(feestypeApp);
                //        }
                //    }
                //    if (Yn > 1)
                //    {
                //        ReturnMsg = "Excel uploaded successfully";
                //    }
                //    else
                //    {
                //        ReturnMsg = "Excel uploaded Failed !!!";
                //    }
                //}
                #endregion
            }

            return Json(ReturnMsg);
        }

        #endregion Download & Upload Excel
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }


        public JsonResult ActiveDeactiveSessionFeesType(string id, string IsActive)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("FeesType", accessId, "Update", "FeesType", roleId) == false)
            {

                Json(new
                {
                    redirectUrl = Url.Action("AuthenticationFailed", "Accounts"),
                    isRedirect = true
                });
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            CS_Scholarship objScholar = new CS_Scholarship();
            if (id != null)
            {

                var sessioschalar = sessionfeestype.GetSessionFeesTypeById(Convert.ToInt64(id));
                OdmErp.ApplicationCore.Entities.SessionFeesType.SessionFeesType objScholarship = new SessionFeesType();
                sessioschalar.ID = Convert.ToInt64(id);
                sessioschalar.Status = IsActive;

                sessioschalar.Active = true;

                //TODO
                sessioschalar.ModifiedId = userId;
                sessionfeestype.UpdateSessionFeesType(sessioschalar);
                objScholar.ReturnMsg = sessioschalar.Status.ToUpper() + " Successfully.";
            }
            return Json(objScholar); //feesTypeApplicable).WithDanger("error", "Not Saved"
        }

    }
}
