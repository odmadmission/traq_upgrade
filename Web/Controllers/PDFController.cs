﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Web.Models;

namespace OdmErp.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PDFController : ControllerBase
    {
        private readonly IConverter _converter;
        private readonly ApplicationDbContext context;
        public IAdmissionExamDateAssignRepository admissionExamDateAssignRepository;
        public IAdmissionStandardExamRepository admissionStandardExamRepository;
        public IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository;
        public IEmployeeRepository employeeRepository;
        public IAdmissionDocumentRepository admissionDocumentRepository;
        public IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepository;
        public IAdmissionStudentDocumentRepository admissionStudentDocumentRepository;
        public IDocumentSubTypeRepository documentSubTypeRepository;
        public IDocumentTypeRepository documentTypeRepository;
        public IPaymentType paymentType;
        public IBoardStandardRepository boardStandardRepository;
        public IAdmissionExamDateAssignRepository admissionExamDateAssign;
        public IAdmissionStudentExamRepository admissionStudentExamRepository;
        public IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository;
        public IAdmissionRepository admissionRepository;
        public IAcademicSessionRepository academicSessionRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IOrganizationRepository organizationRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IBoardRepository boardRepository;
        public IStandardRepository standardRepository;
        public IWingRepository wingRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IAdmissionStudentRepository admissionStudentRepository;
        public IAdmissionStudentAcademicRepository admissionStudentAcademicRepository;
        public IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository;
        public IAdmissionCounsellorRepository admissionCounsellorRepository;
        public IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssign;
        public IAdmissionDocumentAssignRepository admissionDocumentAssignRepository;
        public ICounsellorStatusRepository counsellorStatusRepository;
        public IAdmissionInterViewerRepository admissionInterViewerRepository;
        public IAdmissionVerifierRepository admissionVerifierRepository;
        public CommonMethods commonMethods;
        public PDFController(IConverter _conv,IAdmissionStudentDocumentRepository admissionStudentDocumentRepo,
            IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo,
            IAdmissionDocumentRepository admissionDocumentRepo,
            CommonMethods commonMethods,
            IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepo,
            IEmployeeRepository employeeRepo,
            IAdmissionExamDateAssignRepository admissionExamDateAssignRepo,
            IAdmissionStandardExamRepository admissionStandardExamRepo, IAdmissionDocumentAssignRepository adsRepo,
            IAcademicSessionRepository asrepo, IStandardRepository srepo, IBoardRepository brpo, IWingRepository wpo,
            IOrganizationAcademicRepository oarepo, IOrganizationRepository orepo, IAcademicStandardWingRepository aswrepo,
            IAcademicStandardRepository astdrepo, IAdmissionRepository admrepo, IBoardStandardRepository bostdrepo,
             IBoardStandardWingRepository boardStandardWingRepository,
              IAdmissionStudentRepository admStdrepo, IAdmissionStudentAcademicRepository admStdAcdrepo,
             IAdmissionStudentStandardRepository admStdStadrepo, IAdmissionFormPaymentRepository admFormParEpo,
             IPaymentType paty, IBoardStandardRepository boardStandardRepository, IAcademicStandardSettingsRepository academicStandardSettingsRepository,
         IAdmissionExamDateAssignRepository admissionExamDateAssign, IAdmissionStudentExamRepository admissionStudentExamRepository, IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository,
         IAdmissionPersonalInterviewAssignRepository apiasign, IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository,
         IAdmissionCounsellorRepository admissionCounsellorRepository, ICounsellorStatusRepository counsellorStatusRepo,
         IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository, IAdmissionInterViewerRepository admissionInterViewerRepository, IAdmissionVerifierRepository admissionVerifierRepository
            )
        {
            _converter = _conv;
            this.commonMethods = commonMethods;
            this.admissionInterViewerRepository = admissionInterViewerRepository;
            this.admissionStudentCounsellorRepository = admissionStudentCounsellorRepository;
            this.admissionCounsellorRepository = admissionCounsellorRepository;
            this.admissionDocumentAssignRepository = adsRepo;
            this.admissionPersonalInterviewAssign = apiasign;
            this.admissionPersonalInterviewAssignRepository = admissionPersonalInterviewAssignRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.admissionExamDateAssign = admissionExamDateAssign;
            this.admissionStudentExamRepository = admissionStudentExamRepository;
            this.admissionStandardExamResultDateRepository = admissionStandardExamResultDateRepository;
            paymentType = paty;
            admissionStudentStandardRepository = admStdStadrepo;
            admissionStudentAcademicRepository = admStdAcdrepo;
            admissionStudentRepository = admStdrepo;
            this.boardStandardWingRepository = boardStandardWingRepository;
            academicSessionRepository = asrepo;
            academicStandardRepository = astdrepo;
            academicStandardWingRepository = aswrepo;
            organizationRepository = orepo;
            organizationAcademicRepository = oarepo;
            boardRepository = brpo;
            standardRepository = srepo;
            wingRepository = wpo;
            admissionRepository = admrepo;
            this.boardStandardRepository = bostdrepo;
            this.admissionFormPaymentRepository = admFormParEpo;
            admissionStudentDocumentRepository = admissionStudentDocumentRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            admissionDocumentRepository = admissionDocumentRepo;
            admissionDocumentVerificationAssignRepository = admissionDocumentVerificationAssignRepo;
            employeeRepository = employeeRepo;
            admissionExamDateAssignRepository = admissionExamDateAssignRepo;
            admissionStandardExamRepository = admissionStandardExamRepo;
            this.counsellorStatusRepository = counsellorStatusRepo;
            this.admissionVerifierRepository = admissionVerifierRepository;
        }

        [HttpGet]
        public async Task<IActionResult> CreatePDF(string id)
        {
            //long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
           // long AdmissionId = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            var admissionSingle = admissionRepository.GetAdmissionById(Convert.ToInt64(id));
            //var sessionName = academicSessionRepository.GetByIdAsync(admissionSingle.AcademicSessionId).Result.DisplayName;
            var admissionStudentSingle = await admissionStudentRepository.GetByIdAsyncIncludeAll(Convert.ToInt64(id));
            var admissionstudentStandardSingle = (from c in await admissionStudentStandardRepository.ListAllAsyncIncludeAll()
                                                  where c.AdmissionStudentId == Convert.ToInt64(admissionStudentSingle.ID)
                                                  join d in await academicStandardWingRepository.ListAllAsyncIncludeAll() on c.AcademicStandardWindId equals d.ID
                                                  join e in await boardStandardWingRepository.ListAllAsyncIncludeAll() on d.BoardStandardWingID equals e.ID
                                                  join f in await boardStandardRepository.ListAllAsyncIncludeAll() on e.BoardStandardID equals f.ID
                                                  join g in standardRepository.GetAllStandard() on f.StandardId equals g.ID
                                                  select new
                                                  {
                                                     studentId= c.ID,
                                                      c.AcademicStandardWindId,
                                                      c.AdmissionStudentId,
                                                      c.StandardId,
                                                      c.Nucleus,
                                                      orgName = organizationRepository.GetOrganizationById(c.OrganizationId).Name,
                                                      wingName = wingRepository.GetByIdAsync(c.WingId).Result.Name,
                                                      standard = standardRepository.GetStandardById(c.StandardId).Name,
                                                      board = boardRepository.GetBoardById(f.BoardId).Name,
                                                      e.BoardStandardID

                                                  }).FirstOrDefault();
           // admissionstudentStandardSingle.studentId
            var sessionName = academicSessionRepository.GetByIdAsync(admissionSingle.AcademicSessionId).Result.DisplayName;
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "AdmitCard",
                //Out = @"D:\AdmitCard.pdf"
            };
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = TemplateGenerator.GetHTMLString(sessionName,admissionSingle.FormNumber, admissionStudentSingle.FirstName, admissionStudentSingle.MiddleName, admissionStudentSingle.LastName, admissionStudentSingle.DateOfBirth, admissionstudentStandardSingle.orgName, admissionstudentStandardSingle.board, admissionstudentStandardSingle.wingName, admissionstudentStandardSingle.standard,admissionSingle.InsertedDate, admissionStudentSingle.Image),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet =Path.Combine(Directory.GetCurrentDirectory(), "Models", "admitCard.css") },
                HeaderSettings = { FontName = "monospace", FontSize = 12, Right = "Page [page] of [toPage]", Line = true },
                FooterSettings = { FontName = "monospace", FontSize = 12, Line = true, Center = "Report Footer" }
            };
            var pdf = new HtmlToPdfDocument
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };
           var img= _converter.Convert(pdf);
            return File(img,"application/pdf");
        }
    }
}

        
