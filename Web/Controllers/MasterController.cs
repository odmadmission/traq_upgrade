﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using OdmErp.Web.Extensions;
using Microsoft.AspNetCore.Http;
using OdmErp.Web.Models;
using System.Linq;
using OdmErp.ApplicationCore.Entities.DocumentAggregate;
using Action = OdmErp.ApplicationCore.Entities.Action;
using Microsoft.AspNetCore.Authorization;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Entities.ModuleAggregate;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OfficeOpenXml;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Text;
using OdmErp.Infrastructure.DTO;
using Web.Controllers;
using ClosedXML.Excel;
using System.Data;
using OdmErp.Web.ViewModels;
using Stream = System.IO.Stream;
using OdmErp.Web.DTO;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections;
using Microsoft.AspNetCore.Routing;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;


namespace OdmErp.Web.Controllers
{

    public class MasterController : AppController
    {
        private readonly IFileProvider fileProvider;
        private readonly IHostingEnvironment hostingEnvironment;

        private ICountryRepository countryRepository;
        private IAdmissionTypeRepository admissionTypeRepository;
        private IBankTypeRepository bankTypeRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private IBoardRepository boardRepository;
        private IBoardStandardRepository boardStandardRepository;
        private IDesignationLevelRepository designationLevelRepository;
        private IEducationQualificationTypeRepository educationQualificationTypeRepository;
        private IGroupRepository groupRepository;
        private IModuleRepository moduleRepository;
        private IOrganizationTypeRepository organizationTypeRepository;
        private IRoleRepository roleRepository;
        private ISectionRepository sectionRepository;
        private readonly IOtpMessageRepository otpMessageRepository;
        private IStateRepository stateRepository;
        private IDepartmentRepository departmentRepository;
        private IDesignationRepository designationRepository;
        private IEducationQualificationRepository educationQualificationRepository;
        private IStandardRepository standardRepository;
        private IStreamRepository streamRepository;
        private ISubModuleRepository subModuleRepository;
        private ICityRepository cityRepository;
        private IOrganizationRepository organizationRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IReligionTypeRepository religionTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private IBankRepository bankRepository;
        private IBankBranchRepository bankBranchRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IAccessTypeRepository accessTypeRepository;
        private IActionRepository actionRepository;
        private IAuthorityAggregateRepository authorityAggregateRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IActionAccessRepository actionAccessRepository;

        private IDepartmentLeadRepository departmentLeadRepository;
        private IEmployeeRepository employeeRepository;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IHttpContextAccessor _httpContextAccessor;
        public Extensions.SessionExtensions sessionExtensions;

        public IAcademicSessionRepository academicSessionRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IClassTimingRepository classTimingRepository;
        public ILanguageRepository languageRepository;
        public commonsqlquery commonsql;
        public IContactTypeRepository contactTypeRepository;
        // long userId = 0;
        private CommonMethods commonMethods;
        public MasterController(ILanguageRepository languageRepo, IFileProvider fileprovider, IHostingEnvironment env, ICountryRepository countryRepo, IAdmissionTypeRepository admissionTypeRepo, IBloodGroupRepository bloodGroupRepo,
            IBoardRepository boardRepo, IDesignationLevelRepository designationLevelRepo, IEducationQualificationTypeRepository educationQualificationTypeRepo, IGroupRepository groupRepo,
            IModuleRepository moduleRepo, IOrganizationTypeRepository organizationTypeRepo, IRoleRepository roleRepo, ISectionRepository sectionRepo,
            IStateRepository stateRepo, IDepartmentRepository departmentRepo, IDesignationRepository designationRepo, IEducationQualificationRepository educationQualificationRepo, IStandardRepository standardRepo,
            IStreamRepository streamRepo, ISubModuleRepository subModuleRepo, ICityRepository cityRepo, IOrganizationRepository organizationRepo, INationalityTypeRepository nationalityTypeRepo, IReligionTypeRepository religionTypeRepo, IOtpMessageRepository otpMessageRepository ,
            IAddressTypeRepository addressTypeRepo, IBankRepository bankRepo, IBankBranchRepository bankBranchRepo, IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo,
            IAccessTypeRepository accessTypeRepo, IActionRepository actionRepo, CommonMethods commonMeth, IAuthorityAggregateRepository authorityAggregateRepo, IStudentAggregateRepository studentAggregateRepo, IActionAccessRepository actionAccessRepo,
            IDepartmentLeadRepository departmentlead, IHttpContextAccessor httpContextAccessor, IEmployeeRepository emprepo, IEmployeeDesignationRepository empdesrepo, IBankTypeRepository bankTypeRepo, 
            IBoardStandardRepository boardStandardRepository, IClassTimingRepository classTimingRepo, IAcademicSessionRepository academicSessionRepository, commonsqlquery commonsqlquery,
            IOrganizationAcademicRepository organizationAcademicRepository, IContactTypeRepository contactTypeRepository)
        {
            languageRepository = languageRepo;
            fileProvider = fileprovider;
            hostingEnvironment = env;
            countryRepository = countryRepo;
            admissionTypeRepository = admissionTypeRepo;
            bloodGroupRepository = bloodGroupRepo;
            boardRepository = boardRepo;
            designationLevelRepository = designationLevelRepo;
            educationQualificationTypeRepository = educationQualificationTypeRepo;
            groupRepository = groupRepo;
            moduleRepository = moduleRepo;
            organizationTypeRepository = organizationTypeRepo;
            roleRepository = roleRepo;
            sectionRepository = sectionRepo;
            bankTypeRepository = bankTypeRepo;
            stateRepository = stateRepo;
            departmentRepository = departmentRepo;
            designationRepository = designationRepo;
            educationQualificationRepository = educationQualificationRepo;
            standardRepository = standardRepo;
            streamRepository = streamRepo;
            subModuleRepository = subModuleRepo;
            cityRepository = cityRepo;
            organizationRepository = organizationRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            religionTypeRepository = religionTypeRepo;
            addressTypeRepository = addressTypeRepo;
            bankRepository = bankRepo;
            bankBranchRepository = bankBranchRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            accessTypeRepository = accessTypeRepo;
            actionRepository = actionRepo;
            commonMethods = commonMeth;
            authorityAggregateRepository = authorityAggregateRepo;
            studentAggregateRepository = studentAggregateRepo;
            actionAccessRepository = actionAccessRepo;
            _httpContextAccessor = httpContextAccessor;
            departmentLeadRepository = departmentlead;
            employeeRepository = emprepo;
            employeeDesignationRepository = empdesrepo;
            this.boardStandardRepository = boardStandardRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            commonsql = commonsqlquery;
            classTimingRepository = classTimingRepo;
            this.contactTypeRepository = contactTypeRepository;
            this.otpMessageRepository = otpMessageRepository;
        }

        public IActionResult Index()
        {
            return View();
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        #region Country
        // GET: Countries
        public IActionResult Country()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var res = (from a in countryRepository.GetAllCountries()
                       select new NameIdCountViewModel
                       {
                           ID=a.ID,
                           Name=a.Name,
                           ModifiedDate=a.ModifiedDate,
                           OrgCount=stateRepository.GetAllStateByCountryId(a.ID).Count()
                       }
                     ).ToList();

            return View(res);
        }
      
       
        public IActionResult DownloadCountry()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Country", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
                //"Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Country"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var country in countryRepository.GetAllCountries())
                {
                    //worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = country.Name;
                    //worksheet.Cells["C" + j].Value = country.InsertedDate.ToString("dd/MM/yyyy");
                    //worksheet.Cells["D" + j].Value = country.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Country.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadCountry(IFormFile file)
        {
            CheckLoginStatus();
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Country", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

         
            if (file != null)
            {
                // string path1 = file.FileName;
                byte[] filestr;
                Stream stream = file.OpenReadStream();

                using (XLWorkbook workBook = new XLWorkbook(stream))
                {
                    //Read the first Sheet from Excel file.
                    IXLWorksheet workSheet = workBook.Worksheet(1);
                    var countries = countryRepository.GetAllCountries();
                    foreach (IXLRow row in workSheet.Rows().Skip(1))
                    {
                        Country country = new Country();
                        if (row.Cell(1).Value != null && row.Cell(1).Value.ToString() != "")
                        {
                            string countryname = row.Cell(1).Value.ToString();
                            if (countries.Where(a => a.Name == countryname).FirstOrDefault() == null)
                            {
                                country.Name = countryname;
                                country.ModifiedId = userId;
                                country.Active = true;
                                country.InsertedId = userId;
                                country.Status =EntityStatus.ACTIVE;
                                countryRepository.CreateCountry(country);
                            }
                        }
                    }
                    workBook.Dispose();
                }
                _httpContextAccessor.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

            }

            return RedirectToAction(nameof(Country)).WithSuccess("success", "Excel uploaded successfully");
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditCountry(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            Models.CommonMasterModel od = new Models.CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Country", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Country", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var country = countryRepository.GetCountryById(id.Value);
                od.ID = country.ID;
                od.Name = country.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateCountry([Bind("ID,Name")] Country country)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                country.ModifiedId = userId;
                country.Active = true;
                if (country.ID == 0)
                {
                    country.InsertedId = userId;
                    country.Status = EntityStatus.ACTIVE;
                    countryRepository.CreateCountry(country);
                    return RedirectToAction(nameof(Country)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    countryRepository.UpdateCountry(country);
                    return RedirectToAction(nameof(Country)).WithSuccess("success", "Updated successfully");
                }
            }
            return View(country).WithDanger("error", "Not Saved");
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteCountry")]
        public async Task<IActionResult> CountryDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Country", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (CountryExists(id.Value) == true)
            {
                var country = countryRepository.DeleteCountry(id.Value);
            }
            return RedirectToAction(nameof(Country)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool CountryExists(long id)
        {
            if (countryRepository.GetCountryById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Admission Type
        public IActionResult AdmissionType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Admission Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(admissionTypeRepository.GetAllAdmissionType());
        }
        public IActionResult downloadAdmissionTypeSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Admission Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Admission Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Admission Type.xlsx");
        }
        public IActionResult DownloadAdmissionType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Admission Type", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Admission Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var country in admissionTypeRepository.GetAllAdmissionType())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = country.Name;
                    // worksheet.Cells["C" + j].Value = country.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = country.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Admission Type.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadAdmissionType(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Admission Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "Admission Type_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        AdmissionType admission = new AdmissionType();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (admissionTypeRepository.GetAdmissionTypeByName(name) == null)
                        {
                            admission.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            admission.ModifiedId = userId;
                            admission.Active = true;
                            admission.InsertedId = userId;
                            admission.Status = EntityStatus.ACTIVE;
                            admissionTypeRepository.CreateAdmissionType(admission);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(AdmissionType)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditAdmissionType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Admission Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Admission Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var admissionType = admissionTypeRepository.GetAdmissionTypeById(id.Value);
                od.ID = admissionType.ID;
                od.Name = admissionType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateAdmissionType([Bind("ID,Name")] AdmissionType admissionType)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                admissionType.ModifiedId = userId;
                admissionType.Active = true;
                if (admissionType.ID == 0)
                {
                    admissionType.InsertedId = userId;
                    admissionType.Status = EntityStatus.ACTIVE;
                    admissionTypeRepository.CreateAdmissionType(admissionType);
                    return RedirectToAction(nameof(AdmissionType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    admissionTypeRepository.UpdateAdmissionType(admissionType);
                    return RedirectToAction(nameof(AdmissionType)).WithSuccess("success", "Updated successfully");
                }
            }
            return View(admissionType).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteAdmissionType")]
        public async Task<IActionResult> AdmissionTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Admission Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (AdmissionTypeExists(id.Value) == true)
            {
                var country = admissionTypeRepository.DeleteAdmissionType(id.Value);
            }
            return RedirectToAction(nameof(AdmissionType)).WithSuccess("success", "Deleted successfully");
        }

        private bool AdmissionTypeExists(long id)
        {
            if (admissionTypeRepository.GetAdmissionTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Bank Type
        public IActionResult BankType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Bank Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(bankTypeRepository.GetAllBankType());
        }
        public IActionResult downloadBankTypeSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Bank Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Bank Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Bank Type.xlsx");
        }
        public IActionResult DownloadBankType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Bank Type", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
                //"Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Bank Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var country in bankTypeRepository.GetAllBankType())
                {
                    //  worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = country.Name;
                    // worksheet.Cells["C" + j].Value = country.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = country.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Bank Type.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadBankType(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Bank Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            //if (file != null)
            //{
            //    // Create a File Info 
            //    FileInfo fi = new FileInfo(file.FileName);
            //    var newFilename = "Bank Type_" + String.Format("{0:d}",
            //                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
            //    var webPath = hostingEnvironment.WebRootPath;
            //    string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
            //    var pathToSave = newFilename;
            //    using (var stream = new FileStream(path, FileMode.Create))
            //    {
            //        file.CopyToAsync(stream);
            //    }
            //    FileInfo fil = new FileInfo(path);
            //    using (ExcelPackage excelPackage = new ExcelPackage(fil))
            //    {
            //        //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
            //        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
            //        int totalRows = firstWorksheet.Dimension.Rows;
            //        for (int i = 2; i <= totalRows; i++)
            //        {
            //            BankType Bank = new BankType();
            //            string name = firstWorksheet.Cells[i, 1].Value.ToString();
            //            if (bankTypeRepository.GetBankTypeByName(name) == null)
            //            {
            //                Bank.Name = firstWorksheet.Cells[i, 1].Value.ToString();
            //                Bank.ModifiedId = userId;
            //                Bank.Active = true;
            //                Bank.InsertedId = userId;
            //                Bank.Status = EntityStatus.ACTIVE;
            //                bankTypeRepository.CreateBankType(Bank);
            //            }
            //        }
            //    }
            //}

            if (file != null)
            {
                // string path1 = file.FileName;
                byte[] filestr;
                Stream stream = file.OpenReadStream();

                using (XLWorkbook workBook = new XLWorkbook(stream))
                {
                    //Read the first Sheet from Excel file.
                    IXLWorksheet workSheet = workBook.Worksheet(1);
                    var countries = bankTypeRepository.GetAllBankType();
                    foreach (IXLRow row in workSheet.Rows().Skip(1))
                    {
                        BankType Bank = new BankType();
                        if (row.Cell(1).Value != null && row.Cell(1).Value.ToString() != "")
                        {
                            string countryname = row.Cell(1).Value.ToString();
                            if (countries.Where(a => a.Name == countryname).FirstOrDefault() == null)
                            {
                                Bank.Name = countryname;
                                Bank.ModifiedId = userId;
                                Bank.Active = true;
                                Bank.InsertedId = userId;
                                Bank.Status =EntityStatus.ACTIVE;
                                bankTypeRepository.CreateBankType(Bank);
                            }
                        }
                    }
                    workBook.Dispose();
                }
                _httpContextAccessor.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

            }


            return RedirectToAction(nameof(BankType)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditBankType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Bank Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Bank Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var BankType = bankTypeRepository.GetBankTypeById(id.Value);
                od.ID = BankType.ID;
                od.Name = BankType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateBankType([Bind("ID,Name")] BankType BankType)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                BankType.ModifiedId = userId;
                BankType.Active = true;
                if (BankType.ID == 0)
                {
                    BankType.InsertedId = userId;
                    BankType.Status = EntityStatus.ACTIVE;
                    bankTypeRepository.CreateBankType(BankType);
                    return RedirectToAction(nameof(BankType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    bankTypeRepository.UpdateBankType(BankType);
                    return RedirectToAction(nameof(BankType)).WithSuccess("success", "Updated successfully");
                }
            }
            return View(BankType).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteBankType")]
        public async Task<IActionResult> BankTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Bank Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (BankTypeExists(id.Value) == true)
            {
                var country = bankTypeRepository.DeleteBankType(id.Value);
            }
            return RedirectToAction(nameof(BankType)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool BankTypeExists(long id)
        {
            if (bankTypeRepository.GetBankTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Blood Group
        public IActionResult BloodGroup()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Blood Group", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(bloodGroupRepository.GetAllBloodGroup());
        }
        public IActionResult downloadBloodGroupSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Blood Group", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Blood Group"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Blood Group.xlsx");
        }
        public IActionResult DownloadBloodGroup()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Blood Group", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Blood Group"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in bloodGroupRepository.GetAllBloodGroup())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    //worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    //worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Blood Group.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadBloodGroup(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Blood Group", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "Blood Group_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        BloodGroup blood = new BloodGroup();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (bloodGroupRepository.GetBloodGroupByName(name) == null)
                        {
                            blood.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            blood.ModifiedId = userId;
                            blood.Active = true;
                            blood.InsertedId = userId;
                            blood.Status = EntityStatus.ACTIVE;
                            bloodGroupRepository.CreateBloodGroup(blood);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(BloodGroup)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditBloodGroup(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Blood Group", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Blood Group", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var bloodGroup = bloodGroupRepository.GetBloodGroupById(id.Value);
                od.ID = bloodGroup.ID;
                od.Name = bloodGroup.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateBloodGroup([Bind("ID,Name")] BloodGroup bloodGroup)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                bloodGroup.ModifiedId = userId;
                bloodGroup.Active = true;
                if (bloodGroup.ID == 0)
                {
                    bloodGroup.InsertedId = userId;
                    bloodGroup.Status = EntityStatus.ACTIVE;
                    bloodGroupRepository.CreateBloodGroup(bloodGroup);
                    return RedirectToAction(nameof(BloodGroup)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    bloodGroupRepository.UpdateBloodGroup(bloodGroup);
                    return RedirectToAction(nameof(BloodGroup)).WithSuccess("success", "Updated successfully");
                }


            }
            return View(bloodGroup).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteBloodGroup")]
        public async Task<IActionResult> BloodGroupDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Blood Group", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (BloodGroupExists(id.Value) == true)
            {
                var country = bloodGroupRepository.DeleteBloodGroup(id.Value);
            }
            return RedirectToAction(nameof(BloodGroup)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool BloodGroupExists(long id)
        {
            if (bloodGroupRepository.GetBloodGroupById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Board
        public IActionResult Board()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Board", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var groups = boardRepository.GetAllBoard();
            var res = (from a in groups.ToList()
                       select new NameBoardCountViewModel
                       {
                          ID = a.ID,
                           Name = a.Name,
                           ModifiedDate = a.ModifiedDate,
                           ModifiedText=NullableDateTimeToStringDate(a.ModifiedDate),
                           BoardCount = boardStandardRepository.GetStandardCountByBoardId(a.ID),
                           Status = a.Status,
                           Active = a.Active
                       }).ToList();

            return View(res);



        }
        public IActionResult downloadBoardSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Board", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Board"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Board.xlsx");
        }
        public IActionResult DownloadBoard()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Board", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
              //  "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Board"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in boardRepository.GetAllBoard())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    // worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    //  worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Board.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadBoard(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Board", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "Board_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Board board = new Board();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (boardRepository.GetBoardByName(name) == null)
                        {
                            board.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            board.ModifiedId = userId;
                            board.Active = true;
                            board.InsertedId = userId;
                            board.Status = EntityStatus.ACTIVE;
                            boardRepository.CreateBoard(board);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(Board)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditBoard(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Board", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewData["ALLSTATUS"] = new List<SelectListItem> {new SelectListItem {Text="ACTIVE",Value = "ACTIVE" }};
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
               
            }
            else
            {
                if (commonMethods.checkaccessavailable("Board", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
               

                ViewBag.status = "Update";
                var board = boardRepository.GetBoardById(id.Value);
                od.ID = board.ID;
                od.Name = board.Name;
               GetStatusList();
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateBoard([Bind("ID,Name,Status")] Board board)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                board.ModifiedId = userId;
                board.Active = true;
                if (board.ID == 0)
                {
                    board.InsertedId = userId;
                    board.Status = EntityStatus.ACTIVE;
                    boardRepository.CreateBoard(board);
                    return RedirectToAction(nameof(Board)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    board.Status = board.Status;
                    boardRepository.UpdateBoard(board);
                    return RedirectToAction(nameof(Board)).WithSuccess("success", "Updated successfully");
                }


            }
            return View(board).WithDanger("error", "Not Saved");
        }

        //[ActionName("DeleteBoard")]
        //public async Task<IActionResult> BoardDeleteConfirmed(long? id)
        //{
        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                       // long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    if (commonMethods.checkaccessavailable("Board", accessId, "Delete", "Master", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }

        //    if (BoardExists(id.Value) == true)
        //    {
        //        var country = boardRepository.DeleteBoard(id.Value);
        //    }
        //    return RedirectToAction(nameof(Board)).WithSuccess("success", "Deleted successfully"); ;
        //}

        //private bool BoardExists(long id)
        //{
        //    if (boardRepository.GetBoardById(id) != null)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        #endregion
        #region Designation Level
        public IActionResult DesignationLevel()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Designation Level", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(designationLevelRepository.GetAllDesignationLevel());
        }
        public IActionResult downloadDesignationLevelSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Designation Level", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Designation Level"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Designation Level.xlsx");
        }
        public IActionResult DownloadDesignationLevel()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Designation Level", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Designation Level"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in designationLevelRepository.GetAllDesignationLevel())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    // worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Designation Level.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadDesignationLevel(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Designation Level", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "DesignationLevel_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        DesignationLevel designation = new DesignationLevel();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (designationLevelRepository.GetDesignationLevelByName(name) == null)
                        {
                            designation.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            designation.ModifiedId = userId;
                            designation.Active = true;
                            designation.InsertedId = userId;
                            designation.Status = EntityStatus.ACTIVE;
                            designationLevelRepository.CreateDesignationLevel(designation);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(DesignationLevel)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditDesignationLevel(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Designation Level", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Designation Level", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var DesignationLevel = designationLevelRepository.GetDesignationLevelById(id.Value);
                od.ID = DesignationLevel.ID;
                od.Name = DesignationLevel.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateDesignationLevel([Bind("ID,Name")] DesignationLevel designationLevel)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                designationLevel.ModifiedId = userId;
                designationLevel.Active = true;
                if (designationLevel.ID == 0)
                {
                    designationLevel.InsertedId = userId;
                    designationLevel.Status = EntityStatus.ACTIVE;
                    designationLevelRepository.CreateDesignationLevel(designationLevel);
                    return RedirectToAction(nameof(DesignationLevel)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    designationLevelRepository.UpdateDesignationLevel(designationLevel);
                    return RedirectToAction(nameof(DesignationLevel)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(designationLevel).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteDesignationLevel")]
        public async Task<IActionResult> DesignationLevelDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Designation Level", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (DesignationLevelExists(id.Value) == true)
            {
                var country = designationLevelRepository.DeleteDesignationLevel(id.Value);
            }
            return RedirectToAction(nameof(DesignationLevel)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool DesignationLevelExists(long id)
        {
            if (designationLevelRepository.GetDesignationLevelById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Education Qualification Type
        public IActionResult EducationQualificationType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Education Qualification Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(educationQualificationTypeRepository.GetAllEducationQualificationType());
        }
        public IActionResult downloadEducationQualificationTypeSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Education Qualification Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Education Qualification Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Education Qualification Type.xlsx");
        }
        public IActionResult DownloadEducationQualificationType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Education Qualification Type", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Education Qualification Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in educationQualificationTypeRepository.GetAllEducationQualificationType())
                {
                    //  worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    //  worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    //  worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Education Qualification Type.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadEducationQualificationType(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Education Qualification Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "EducationQualificationType_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        EducationQualificationType educationQualification = new EducationQualificationType();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (educationQualificationTypeRepository.GetEducationQualificationTypeByName(name) == null)
                        {
                            educationQualification.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            educationQualification.ModifiedId = userId;
                            educationQualification.Active = true;
                            educationQualification.InsertedId = userId;
                            educationQualification.Status = EntityStatus.ACTIVE;
                            educationQualificationTypeRepository.CreateEducationQualificationType(educationQualification);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(EducationQualificationType)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditEducationQualificationType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Education Qualification Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Education Qualification Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var educationQualificationType = educationQualificationTypeRepository.GetEducationQualificationTypeById(id.Value);
                od.ID = educationQualificationType.ID;
                od.Name = educationQualificationType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEducationQualificationType([Bind("ID,Name")] EducationQualificationType educationQualificationType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                educationQualificationType.ModifiedId = userId;
                educationQualificationType.Active = true;
                if (educationQualificationType.ID == 0)
                {
                    educationQualificationType.InsertedId = userId;
                    educationQualificationType.Status = EntityStatus.ACTIVE;
                    educationQualificationTypeRepository.CreateEducationQualificationType(educationQualificationType);
                    return RedirectToAction(nameof(EducationQualificationType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    educationQualificationTypeRepository.UpdateEducationQualificationType(educationQualificationType);
                    return RedirectToAction(nameof(EducationQualificationType)).WithSuccess("success", "Updated successfully");
                }


            }
            return View(educationQualificationType).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteEducationQualificationType")]
        public async Task<IActionResult> EducationQualificationTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Education Qualification Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (EducationQualificationTypeExists(id.Value) == true)
            {
                var country = educationQualificationTypeRepository.DeleteEducationQualificationType(id.Value);
            }
            return RedirectToAction(nameof(EducationQualificationType)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool EducationQualificationTypeExists(long id)
        {
            if (educationQualificationTypeRepository.GetEducationQualificationTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Group
        public IActionResult Group()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Group", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var groups = groupRepository.GetAllGroup();
            var res = (from a in groups.ToList()
                       select new NameIdCountViewModel
                       {
                           ID = a.ID,
                           Name = a.Name,
                           ModifiedDate = a.ModifiedDate,
                           ModifiedName = employeeRepository.GetEmployeeFullNameById(a.ModifiedId),
                           OrgCount = organizationRepository.GetAllOrganizationCountByGroupId(a.ID),
                           DeptCount = departmentRepository.GetDepartmentCountByGroupId(a.ID),
                           Status = a.Status,
                           Active = a.Active
                       }).ToList();

            return View(res);
        }

        public IActionResult downloadGroupSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Group", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Group"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Group.xlsx");
        }
        public IActionResult DownloadGroup()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Group", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Group"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var country in groupRepository.GetAllGroup())
                {
                    //worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = country.Name;
                    // worksheet.Cells["C" + j].Value = country.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = country.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Group.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadGroup(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Group", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "Group_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Group group = new Group();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (groupRepository.GetGroupByName(name) == null)
                        {
                            group.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            group.ModifiedId = userId;
                            group.Active = true;
                            group.InsertedId = userId;
                            group.Status = EntityStatus.ACTIVE;
                            groupRepository.CreateGroup(group);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(Group)).WithSuccess("success", "Excel uploaded successfully"); ;
        }

        public IActionResult CreateOrEditGroup(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Group", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Group", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";

                var group = groupRepository.GetGroupById(id.Value);
                od.ID = group.ID;
                od.Name = group.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateGroup([Bind("ID,Name")] Group group)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                group.ModifiedId = userId;
                group.Active = true;
                if (group.ID == 0)
                {
                    group.InsertedId = userId;
                    group.Status = EntityStatus.ACTIVE;


                    groupRepository.CreateGroup(group);
                    return RedirectToAction(nameof(Group)).WithSuccess("success", "Saved successfully");

                }
                else
                {

                    groupRepository.UpdateGroup(group);
                    return RedirectToAction(nameof(Group)).WithSuccess("success", "Updated successfully");

                }


            }
            return View(group).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteGroup")]
        public async Task<IActionResult> GroupDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Group", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }


            if (GroupExists(id.Value) == true)
            {
                var country = groupRepository.DeleteGroup(id.Value);
            }
            return RedirectToAction(nameof(Group)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool GroupExists(long id)
        {
            if (groupRepository.GetGroupById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        [ActionName("ChangeGroupStatus")]
        public async Task<IActionResult> GroupStatusChange(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Group", accessId, "ChangeStatus", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            Group group = groupRepository.GetGroupById(id.Value);

            if (group != null)
            {

                if (group.Active)
                {
                    group.Active = false;
                    group.Status = EntityStatus.INACTIVE;
                    group.ModifiedDate = DateTime.Now;
                    group.ModifiedId = userId;

                }
                else
                {
                    group.Active = true;
                    group.Status = EntityStatus.ACTIVE;
                    group.ModifiedDate = DateTime.Now;
                    group.ModifiedId = userId;
                }

                var country = groupRepository.UpdateGroup(group);
            }
            return RedirectToAction(nameof(Group));
        }


        [ActionName("ChangeDocumentTypeStatus")]
        public async Task<IActionResult> DocumentTypeStatusChange(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
                               //if (commonMethods.checkaccessavailable("Group", accessId, "ChangeStatus", "Master", roleId) == false)
                               //{
                               //    return RedirectToAction("AuthenticationFailed", "Accounts");
                               //}

            DocumentType group = documentTypeRepository.GetDocumentTypeById(id.Value);

            if (group != null)
            {

                if (group.Active)
                {
                    group.Active = false;
                    group.Status = EntityStatus.INACTIVE;
                    group.ModifiedDate = DateTime.Now;
                    group.ModifiedId = userId;

                }
                else
                {
                    group.Active = true;
                    group.Status = EntityStatus.ACTIVE;
                    group.ModifiedDate = DateTime.Now;
                    group.ModifiedId = userId;
                }

                var country = documentTypeRepository.UpdateDocumentType(group);
            }
            return RedirectToAction(nameof(DocumentType));
        }


        #endregion
        #region Module
        public IActionResult Module()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Module", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(moduleRepository.GetAllModule());
        }
        public IActionResult downloadModuleSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Module", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Group"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Module.xlsx");
        }
        public IActionResult DownloadModule()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Module", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Module"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var module in moduleRepository.GetAllModule())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = module.Name;
                    // worksheet.Cells["C" + j].Value = module.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = module.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Module.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadModule(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Module", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "Module_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Module module = new Module();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (moduleRepository.GetModuleByName(name) == null)
                        {
                            module.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            module.ModifiedId = userId;
                            module.Active = true;
                            module.InsertedId = userId;
                            module.Status = EntityStatus.ACTIVE;
                            moduleRepository.CreateModule(module);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(Module)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditModule(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Module", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Module", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var module = moduleRepository.GetModuleById(id.Value);
                od.ID = module.ID;
                od.Name = module.Name;
                od.Url = module.Url;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateModule([Bind("ID,Name,Url")] Module module)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                module.ModifiedId = userId;
                module.Active = true;
                if (module.ID == 0)
                {
                    module.InsertedId = userId;
                    module.Status = EntityStatus.ACTIVE;
                    moduleRepository.CreateModule(module);
                    return RedirectToAction(nameof(Module)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    moduleRepository.UpdateModule(module);
                    return RedirectToAction(nameof(Module)).WithSuccess("success", "Updated successfully");
                }


            }
            return View(module).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteModule")]
        public async Task<IActionResult> ModuleDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Module", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (ModuleExists(id.Value) == true)
            {
                var country = moduleRepository.DeleteModule(id.Value);
            }
            return RedirectToAction(nameof(Module)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool ModuleExists(long id)
        {
            if (moduleRepository.GetModuleById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Class Timing
        public IActionResult ClassTiming()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Class Timing", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var classtiming = classTimingRepository.ListAllAsyncIncludeAllNoTrack().Result;
            if (classtiming!=null)
            {
                var res = (from a in classtiming
                           select new ClassTimingcls
                           {
                               EndTime = a.EndTime,
                               ID = a.ID,
                               IsAvailable = a.IsAvailable,
                               ModifiedDate = a.ModifiedDate,
                               Name = a.Name,
                               StartTime = a.StartTime,
                               Active = a.Active
                           }).ToList();
                return View(res);
            }
            else
            {
                return View(null);
            }
        }        
        public IActionResult CreateOrEditClassTiming(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            ClassTimingcls od = new ClassTimingcls();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Class Timing", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";


            }
            else
            {
                if (commonMethods.checkaccessavailable("Class Timing", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var classtimes = classTimingRepository.GetByIdAsync(id.Value).Result;
                od.ID = classtimes.ID;
                od.Name = classtimes.Name;
                od.StartTime = classtimes.StartTime;
                od.EndTime = classtimes.EndTime;
                od.IsAvailable = classtimes.IsAvailable;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateClassTiming([Bind("ID,Name,StartTime,EndTime,IsAvailable")] ClassTiming module)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                module.ModifiedId = userId;
                module.ModifiedDate = DateTime.Now;
                module.Active = true;
                if (module.ID == 0)
                {
                    module.InsertedId = userId;
                    module.Status = EntityStatus.ACTIVE;
                    
                    module.InsertedDate = DateTime.Now;
                    await classTimingRepository.AddAsync(module);
                    return RedirectToAction(nameof(ClassTiming)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    var s = classTimingRepository.ListAllAsyncIncludeAllNoTrack().Result.Where(a=>a.ID==module.ID).FirstOrDefault();
                    module.InsertedId = s.InsertedId;
                    module.InsertedDate = s.InsertedDate;
                    await classTimingRepository.UpdateAsync(module);
                    return RedirectToAction(nameof(ClassTiming)).WithSuccess("success", "Updated successfully");
                }


            }
            return View(module).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteClassTiming")]
        public async Task<IActionResult> ClassTimingDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Class Timing", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (ClassTimingExists(id.Value) == true)
            {
                var country = classTimingRepository.DeleteAsync(classTimingRepository.GetByIdAsync(id.Value).Result);
            }
            return RedirectToAction(nameof(ClassTiming)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool ClassTimingExists(long id)
        {
            if (classTimingRepository.GetByIdAsync(id).Result != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Language
        public IActionResult Language()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Language", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(languageRepository.ListAllAsync().Result);
        }
       
        public IActionResult CreateOrEditLanguage(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Language", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Language", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var Language = languageRepository.GetByIdAsync(id.Value).Result;
                od.ID = Language.ID;
                od.Name = Language.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateLanguage([Bind("ID,Name")] Language language)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                language.ModifiedId = userId;
                language.Active = true;
                if (language.ID == 0)
                {
                    language.InsertedId = userId;
                    language.InsertedDate = DateTime.Now;
                    language.ModifiedDate = DateTime.Now;
                    language.Status = EntityStatus.ACTIVE;
                    await languageRepository.AddAsync(language);
                    return RedirectToAction(nameof(Language)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                   
                    await languageRepository.UpdateAsync(language);
                    return RedirectToAction(nameof(Language)).WithSuccess("success", "Updated successfully");
                }


            }
            return View(language).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteLanguage")]
        public async Task<IActionResult> LanguageDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Language", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (LanguageExists(id.Value) == true)
            {
                Language language = languageRepository.GetByIdAsync(id.Value).Result;
                language.Active = false;              
                language.ModifiedDate = DateTime.Now;
                language.ModifiedId = userId;
                await languageRepository.UpdateAsync(language);
            }
            return RedirectToAction(nameof(Language)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool LanguageExists(long id)
        {
            if (languageRepository.GetByIdAsync(id).Result != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region Organization Type
        public IActionResult OrganizationType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Organization Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(organizationTypeRepository.GetAllOrganizationType());
        }
        public IActionResult downloadOrganizationTypeSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Organization Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Organization Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Organization Type.xlsx");
        }
        public IActionResult DownloadOrganizationType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Organization Type", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Organization Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in organizationTypeRepository.GetAllOrganizationType())
                {
                    //  worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    //  worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Organization Type.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadOrganizationType(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Organization Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "OrganizationType_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        OrganizationType organization = new OrganizationType();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (organizationTypeRepository.GetOrganizationTypeByName(name) == null)
                        {
                            organization.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            organization.ModifiedId = userId;
                            organization.Active = true;
                            organization.InsertedId = userId;
                            organization.Status = EntityStatus.ACTIVE;
                            organizationTypeRepository.CreateOrganizationType(organization);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(OrganizationType)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditOrganizationType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Organization Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Organization Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var organizationType = organizationTypeRepository.GetOrganizationTypeById(id.Value);
                od.ID = organizationType.ID;
                od.Name = organizationType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateOrganizationType([Bind("ID,Name")] OrganizationType organizationType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                organizationType.ModifiedId = userId;
                organizationType.Active = true;
                if (organizationType.ID == 0)
                {
                    organizationType.InsertedId = userId;
                    organizationType.Status = EntityStatus.ACTIVE;


                    organizationTypeRepository.CreateOrganizationType(organizationType);
                    return RedirectToAction(nameof(OrganizationType)).WithSuccess("success", "Saved successfully");
                }
                else
                {

                    organizationTypeRepository.UpdateOrganizationType(organizationType);
                    return RedirectToAction(nameof(OrganizationType)).WithSuccess("success", "Updated successfully");
                }



            }
            return View(organizationType).WithDanger("error", "Not Saved"); ;
        }

        [ActionName("DeleteOrganizationType")]
        public async Task<IActionResult> OrganizationTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Organization Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (OrganizationTypeExists(id.Value) == true)
            {
                var organizationType = organizationTypeRepository.DeleteOrganizationType(id.Value);
            }
            return RedirectToAction(nameof(OrganizationType)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool OrganizationTypeExists(long id)
        {
            if (organizationTypeRepository.GetOrganizationTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Role
        public IActionResult Role()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Role", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(roleRepository.GetAllRole());
        }
        public IActionResult downloadRoleSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Role", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Role"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Role.xlsx");
        }
        public IActionResult DownloadRole()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Role", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Role"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in roleRepository.GetAllRole())
                {
                    //  worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    // worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Role.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadRole(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Role", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "Role_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Role role = new Role();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (roleRepository.GetRoleByName(name) == null)
                        {
                            role.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            role.ModifiedId = userId;
                            role.Active = true;
                            role.InsertedId = userId;
                            role.Status = EntityStatus.ACTIVE;
                            roleRepository.CreateRole(role);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(Role)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditRole(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Role", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Role", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var Role = roleRepository.GetRoleById(id.Value);
                od.ID = Role.ID;
                od.Name = Role.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateRole([Bind("ID,Name")] Role role)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                role.ModifiedId = userId;
                role.Active = true;
                if (role.ID == 0)
                {
                    role.InsertedId = userId;
                    role.Status = EntityStatus.ACTIVE;
                    roleRepository.CreateRole(role);
                    return RedirectToAction(nameof(Role)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    roleRepository.UpdateRole(role);
                    return RedirectToAction(nameof(Role)).WithSuccess("success", "Updated successfully");
                }



            }
            return View(role).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteRole")]
        public async Task<IActionResult> RoleDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Role", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (RoleExists(id.Value) == true)
            {
                var Role = roleRepository.DeleteRole(id.Value);
            }
            return RedirectToAction(nameof(Role)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool RoleExists(long id)
        {
            if (roleRepository.GetRoleById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Section
        public IActionResult Section()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Section", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(sectionRepository.GetAllSection());
        }
        public IActionResult downloadSectionSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Section", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Section"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Section.xlsx");
        }
        public IActionResult DownloadSection()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Section", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
              //  "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Section"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in sectionRepository.GetAllSection())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    //  worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Section.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadSection(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Section", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "Section_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Section section = new Section();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (sectionRepository.GetSectionByName(name) == null)
                        {
                            section.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            section.ModifiedId = userId;
                            section.Active = true;
                            section.InsertedId = userId;
                            section.Status = EntityStatus.ACTIVE;
                            sectionRepository.CreateSection(section);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(Section)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditSection(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Section", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Section", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var section = sectionRepository.GetSectionById(id.Value);
                od.ID = section.ID;
                od.Name = section.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateSection([Bind("ID,Name")] Section section)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                section.ModifiedId = userId;
                section.Active = true;
                if (section.ID == 0)
                {
                    section.InsertedId = userId;
                    section.Status = EntityStatus.ACTIVE;
                    sectionRepository.CreateSection(section);
                    return RedirectToAction(nameof(Section)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    sectionRepository.UpdateSection(section);
                    return RedirectToAction(nameof(Section)).WithSuccess("success", "Updated successfully");
                }
            }
            return View(section).WithDanger("error", "Not Saved"); ;
        }

        [ActionName("DeleteSection")]
        public async Task<IActionResult> SectionDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Section", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (SectionExists(id.Value) == true)
            {
                var Section = sectionRepository.DeleteSection(id.Value);
            }
            return RedirectToAction(nameof(Section)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool SectionExists(long id)
        {
            if (sectionRepository.GetSectionById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Nationality
        public IActionResult Nationality()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Nationality", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(nationalityTypeRepository.GetAllNationalities());
        }
        public IActionResult downloadNationalitySample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Nationality", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Nationality"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Nationality.xlsx");
        }
        public IActionResult DownloadNationality()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Nationality", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Nationality"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in nationalityTypeRepository.GetAllNationalities())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    // worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Nationality.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadNationality(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Nationality", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            //if (file != null)
            //{
            //    // Create a File Info 
            //    FileInfo fi = new FileInfo(file.FileName);
            //    var newFilename = "Nationality_" + String.Format("{0:d}",
            //                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
            //    var webPath = hostingEnvironment.WebRootPath;
            //    string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
            //    var pathToSave = newFilename;
            //    using (var stream = new FileStream(path, FileMode.Create))
            //    {
            //        file.CopyToAsync(stream);
            //    }
            //    FileInfo fil = new FileInfo(path);
            //    using (ExcelPackage excelPackage = new ExcelPackage(fil))
            //    {
            //        //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
            //        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
            //        int totalRows = firstWorksheet.Dimension.Rows;
            //        for (int i = 2; i <= totalRows; i++)
            //        {
            //            NationalityType nationality = new NationalityType();
            //            string name = firstWorksheet.Cells[i, 1].Value.ToString();
            //            if (nationalityTypeRepository.GetNationalityByName(name) == null)
            //            {
            //                nationality.Name = firstWorksheet.Cells[i, 1].Value.ToString();
            //                nationality.ModifiedId = userId;
            //                nationality.Active = true;
            //                nationality.InsertedId = userId;
            //                nationality.Status = EntityStatus.ACTIVE;
            //                nationalityTypeRepository.CreateNationality(nationality);
            //            }
            //        }
            //    }
            //}

            if (file != null)
            {
                // string path1 = file.FileName;
                byte[] filestr;
                Stream stream = file.OpenReadStream();

                using (XLWorkbook workBook = new XLWorkbook(stream))
                {
                    //Read the first Sheet from Excel file.
                    IXLWorksheet workSheet = workBook.Worksheet(1);
                    var nationalities = nationalityTypeRepository.GetAllNationalities();
                    foreach (IXLRow row in workSheet.Rows().Skip(1))
                    {
                        NationalityType nationality = new NationalityType();
                        if (row.Cell(1).Value != null && row.Cell(1).Value.ToString() != "")
                        {
                            string nationalityname = row.Cell(1).Value.ToString();
                            if (nationalities.Where(a => a.Name == nationalityname).FirstOrDefault() == null)
                            {
                                nationality.Name = nationalityname;
                                nationality.ModifiedId = userId;
                                nationality.Active = true;
                                nationality.InsertedId = userId;
                                nationality.Status =EntityStatus.ACTIVE;
                                nationalityTypeRepository.CreateNationality(nationality);
                            }
                        }
                    }
                    workBook.Dispose();
                }
                _httpContextAccessor.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

            }

            return RedirectToAction(nameof(Nationality)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditNationality(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Nationality", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Nationality", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var nationalityType = nationalityTypeRepository.GetNationalityById(id.Value);
                od.ID = nationalityType.ID;
                od.Name = nationalityType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateNationality([Bind("ID,Name")] NationalityType nationalityType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                nationalityType.ModifiedId = userId;
                nationalityType.Active = true;
                if (nationalityType.ID == 0)
                {
                    nationalityType.InsertedId = userId;
                    nationalityType.Status = EntityStatus.ACTIVE;
                    nationalityTypeRepository.CreateNationality(nationalityType);
                    return RedirectToAction(nameof(Nationality)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    nationalityTypeRepository.UpdateNationality(nationalityType);
                    return RedirectToAction(nameof(Nationality)).WithSuccess("success", "Updated successfully");
                }



            }
            return View(nationalityType).WithDanger("error", "Not Saved"); ;
        }

        [ActionName("DeleteNationality")]
        public async Task<IActionResult> NationalityDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Nationality", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (NationalityExists(id.Value) == true)
            {
                var nationalityType = nationalityTypeRepository.DeleteNationality(id.Value);
            }
            return RedirectToAction(nameof(Nationality)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool NationalityExists(long id)
        {
            if (nationalityTypeRepository.GetNationalityById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Religion
        public IActionResult Religion()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Religion", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(religionTypeRepository.GetAllReligionTypes());
        }
        public IActionResult downloadReligionSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Religion", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Religion"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Religion.xlsx");
        }
        public IActionResult DownloadReligion()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Religion", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
                //"Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Religion"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in religionTypeRepository.GetAllReligionTypes())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    // worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Religion.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadReligion(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Religion", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // string path1 = file.FileName;
                byte[] filestr;
                Stream stream = file.OpenReadStream();

                using (XLWorkbook workBook = new XLWorkbook(stream))
                {
                    //Read the first Sheet from Excel file.
                    IXLWorksheet workSheet = workBook.Worksheet(1);
                    var countries = religionTypeRepository.GetAllReligionTypes();
                    foreach (IXLRow row in workSheet.Rows().Skip(1))
                    {
                        ReligionType country = new ReligionType();
                        if (row.Cell(1).Value != null && row.Cell(1).Value.ToString() != "")
                        {
                            string countryname = row.Cell(1).Value.ToString();
                            if (countries.Where(a => a.Name == countryname).FirstOrDefault() == null)
                            {
                                country.Name = countryname;
                                country.ModifiedId = userId;
                                country.Active = true;
                                country.InsertedId = userId;
                                country.Status =EntityStatus.ACTIVE;
                                religionTypeRepository.CreateReligionType(country);
                            }
                        }
                    }
                    workBook.Dispose();
                }
                _httpContextAccessor.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

            }
            return RedirectToAction(nameof(Religion)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditReligion(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Religion", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Religion", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var religionType = religionTypeRepository.GetReligionTypeById(id.Value);
                od.ID = religionType.ID;
                od.Name = religionType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateReligion([Bind("ID,Name")] ReligionType religionType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                religionType.ModifiedId = userId;
                religionType.Active = true;
                if (religionType.ID == 0)
                {
                    religionType.InsertedId = userId;
                    religionType.Status = EntityStatus.ACTIVE;
                    religionTypeRepository.CreateReligionType(religionType);
                    return RedirectToAction(nameof(Religion)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    religionTypeRepository.UpdateReligionType(religionType);
                    return RedirectToAction(nameof(Religion)).WithSuccess("success", "Saved successfully");
                }
            }
            return View(religionType).WithDanger("error", "Not Saved"); ;
        }

        [ActionName("DeleteReligion")]
        public async Task<IActionResult> ReligionDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Religion", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (ReligionExists(id.Value) == true)
            {
                var religionType = religionTypeRepository.DeleteReligionType(id.Value);
            }
            return RedirectToAction(nameof(Religion)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool ReligionExists(long id)
        {
            if (religionTypeRepository.GetReligionTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region AddressType
        public IActionResult AddressType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Address Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(addressTypeRepository.GetAllAddressType());
        }
        public IActionResult downloadAddressTypeSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Address Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Address Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Address Type.xlsx");
        }
        public IActionResult DownloadAddressType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Address Type", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Address Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in addressTypeRepository.GetAllAddressType())
                {
                    //worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    // worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Address Type.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadAddressType(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Address Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "AddressType_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        AddressType address = new AddressType();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (addressTypeRepository.GetAddressTypeByName(name) == null)
                        {
                            address.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            address.ModifiedId = userId;
                            address.Active = true;
                            address.InsertedId = userId;
                            address.Status = EntityStatus.ACTIVE;
                            addressTypeRepository.CreateAddressType(address);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(AddressType)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditAddressType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Address Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Address Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var addressType = addressTypeRepository.GetAddressTypeById(id.Value);
                od.ID = addressType.ID;
                od.Name = addressType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateAddressType([Bind("ID,Name")] AddressType addressType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                addressType.ModifiedId = userId;
                addressType.Active = true;
                if (addressType.ID == 0)
                {
                    addressType.InsertedId = userId;
                    addressType.Status = EntityStatus.ACTIVE;
                    addressTypeRepository.CreateAddressType(addressType);
                    return RedirectToAction(nameof(AddressType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    addressTypeRepository.UpdateAddressType(addressType);
                    return RedirectToAction(nameof(AddressType)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(addressType).WithDanger("error", "Not Saved"); ;
        }

        [ActionName("DeleteAddressType")]
        public async Task<IActionResult> AddressTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Address Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (AddressTypeExists(id.Value) == true)
            {
                var addressType = addressTypeRepository.DeleteAddressType(id.Value);
            }
            return RedirectToAction(nameof(AddressType)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool AddressTypeExists(long id)
        {
            if (addressTypeRepository.GetAddressTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Bank
        public IActionResult Bank()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Bank", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(bankRepository.GetAllBank());
        }
        public IActionResult downloadBankSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Bank", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Bank"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Bank.xlsx");
        }
        public IActionResult DownloadBank()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Bank", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Bank"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in bankRepository.GetAllBank())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    // worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Bank.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadBank(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Bank", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "Bank_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Bank bank = new Bank();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (bankRepository.GetBankByName(name) == null)
                        {
                            bank.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            bank.ModifiedId = userId;
                            bank.Active = true;
                            bank.InsertedId = userId;
                            bank.Status = EntityStatus.ACTIVE;
                            bankRepository.CreateBank(bank);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(Bank)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditBank(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Bank", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Bank", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var bank = bankRepository.GetBankById(id.Value);
                od.ID = bank.ID;
                od.Name = bank.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateBank([Bind("ID,Name")] Bank bank)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                bank.ModifiedId = userId;
                bank.Active = true;
                if (bank.ID == 0)
                {
                    bank.InsertedId = userId;
                    bank.Status = EntityStatus.ACTIVE;
                    bankRepository.CreateBank(bank);
                    return RedirectToAction(nameof(Bank)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    bankRepository.UpdateBank(bank);
                    return RedirectToAction(nameof(Bank)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(bank).WithDanger("error", "Not Saved"); ;
        }

        [ActionName("DeleteBank")]
        public async Task<IActionResult> BankDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Bank", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (BankExists(id.Value) == true)
            {
                var Bank = bankRepository.DeleteBank(id.Value);
            }
            return RedirectToAction(nameof(Bank)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool BankExists(long id)
        {
            if (bankRepository.GetBankById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region DocumentType
        public IActionResult DocumentType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Document Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var groups = documentTypeRepository.GetAllDocumentTypes();
            var res = (from a in groups.ToList()
                       select new NameIdCountViewModel
                       {
                           ID = a.ID,
                           Name = a.Name,
                           ModifiedDate = a.ModifiedDate,
                           ModifiedName = employeeRepository.GetEmployeeFullNameById(a.ModifiedId),
                           DocSubCount = documentSubTypeRepository.GetAllDocumentSubTypeCountByDocumentTypeId(a.ID),
                           Status = a.Status,
                           Active = a.Active
                       }).ToList();

            return View(res);

            //return View(documentTypeRepository.GetAllDocumentTypes());
        }
        public IActionResult downloadDocumentTypeSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Document Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Document Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Document Type.xlsx");
        }
        public IActionResult DownloadDocumentType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Document Type", accessId, "Download", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var comlumHeadrs = new string[]
           {
               // "Sl No",
                "Name"
               // "Inserted On",
               // "Modified On"
           };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Document Type"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1, 1, 5]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }

                //Add values
                int m = 1;
                var j = 2;
                foreach (var obj in documentTypeRepository.GetAllDocumentTypes())
                {
                    // worksheet.Cells["A" + j].Value = m;
                    worksheet.Cells["A" + j].Value = obj.Name;
                    // worksheet.Cells["C" + j].Value = obj.InsertedDate.ToString("dd/MM/yyyy");
                    // worksheet.Cells["D" + j].Value = obj.ModifiedDate.ToString("dd/MM/yyyy");
                    j++;
                    m++;
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Document Type.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadDocumentType(IFormFile file)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Document Type", accessId, "Upload", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "DocumentType_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ExcelFiles\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    file.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                using (ExcelPackage excelPackage = new ExcelPackage(fil))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    for (int i = 2; i <= totalRows; i++)
                    {
                        DocumentType document = new DocumentType();
                        string name = firstWorksheet.Cells[i, 1].Value.ToString();
                        if (documentTypeRepository.GetDocumentTypeByName(name) == null)
                        {
                            document.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            document.ModifiedId = userId;
                            document.Active = true;
                            document.InsertedId = userId;
                            document.Status = EntityStatus.ACTIVE;
                            documentTypeRepository.CreateDocumentType(document);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(DocumentType)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        public IActionResult CreateOrEditDocumentType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Document Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Document Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var documentType = documentTypeRepository.GetDocumentTypeById(id.Value);
                od.ID = documentType.ID;
                od.Name = documentType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateDocumentType([Bind("ID,Name")] DocumentType documentType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                documentType.ModifiedId = userId;
                documentType.Active = true;
                if (documentType.ID == 0)
                {
                    documentType.InsertedId = userId;
                    documentType.Status = EntityStatus.ACTIVE;
                    documentTypeRepository.CreateDocumentType(documentType);
                    return RedirectToAction(nameof(DocumentType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    documentTypeRepository.UpdateDocumentType(documentType);
                    return RedirectToAction(nameof(DocumentType)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(documentType).WithDanger("error", "Not Saved"); ;
        }

        [ActionName("DeleteDocumentType")]
        public async Task<IActionResult> DocumentTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Document Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (DocumentTypeExists(id.Value) == true)
            {
                var DocumentType = documentTypeRepository.DeleteDocumentType(id.Value);
            }
            return RedirectToAction(nameof(DocumentType)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool DocumentTypeExists(long id)
        {
            if (documentTypeRepository.GetDocumentTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region AccessType
        public IActionResult AccessType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Access Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(accessTypeRepository.GetAllAccessType());
        }

        public IActionResult CreateOrEditAccessType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Access Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Access Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var accessType = accessTypeRepository.GetAccessTypeById(id.Value);
                od.ID = accessType.ID;
                od.Name = accessType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateAccessType([Bind("ID,Name")] AccessType accessType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                accessType.ModifiedId = userId;
                accessType.Active = true;
                if (accessType.ID == 0)
                {
                    accessType.InsertedId = userId;
                    accessType.Status = EntityStatus.ACTIVE;
                    accessTypeRepository.CreateAccessType(accessType);
                    return RedirectToAction(nameof(AccessType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    accessTypeRepository.UpdateAccessType(accessType);
                    return RedirectToAction(nameof(AccessType)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(accessType).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteAccessType")]
        public async Task<IActionResult> AccessTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Access Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (AccessTypeExists(id.Value) == true)
            {
                var AccessType = accessTypeRepository.DeleteAccessType(id.Value);
            }
            return RedirectToAction(nameof(AccessType)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool AccessTypeExists(long id)
        {
            if (accessTypeRepository.GetAccessTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region AuthorityType
        public IActionResult AuthorityType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Authority Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(authorityAggregateRepository.GetAllAuthorityType());
        }

        public IActionResult CreateOrEditAuthorityType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Authority Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Authority Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var authority = authorityAggregateRepository.GetAuthorityTypeById(id.Value);
                od.ID = authority.ID;
                od.Name = authority.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateAuthorityType([Bind("ID,Name")] AuthorityType authority)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                authority.ModifiedId = userId;
                authority.Active = true;
                if (authority.ID == 0)
                {
                    authority.InsertedId = userId;
                    authority.Status = EntityStatus.ACTIVE;
                    authorityAggregateRepository.CreateAuthorityType(authority);
                    return RedirectToAction(nameof(AuthorityType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    authorityAggregateRepository.UpdateAuthorityType(authority);
                    return RedirectToAction(nameof(AuthorityType)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(authority).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteAuthorityType")]
        public async Task<IActionResult> AuthorityTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Authority Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (AuthorityTypeExists(id.Value) == true)
            {
                var authority = authorityAggregateRepository.DeleteAuthorityType(id.Value);
            }
            return RedirectToAction(nameof(AuthorityType)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool AuthorityTypeExists(long id)
        {
            if (authorityAggregateRepository.GetAuthorityTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Parent Relationship Type
        public IActionResult ParentRelationshipType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Parent Relationship Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(studentAggregateRepository.GetAllParentRelationshipType());
        }

        public IActionResult CreateOrEditParentRelationshipType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Parent Relationship Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Parent Relationship Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var parentRelationshipType = studentAggregateRepository.GetParentRelationshipTypeById(id.Value);
                od.ID = parentRelationshipType.ID;
                od.Name = parentRelationshipType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateParentRelationshipType([Bind("ID,Name")] ParentRelationshipType parentRelationshipType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                parentRelationshipType.ModifiedId = userId;
                parentRelationshipType.Active = true;
                if (parentRelationshipType.ID == 0)
                {
                    parentRelationshipType.InsertedId = userId;
                    parentRelationshipType.Status = EntityStatus.ACTIVE;
                    studentAggregateRepository.CreateParentRelationshipType(parentRelationshipType);
                    return RedirectToAction(nameof(ParentRelationshipType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    studentAggregateRepository.UpdateParentRelationshipType(parentRelationshipType);
                    return RedirectToAction(nameof(ParentRelationshipType)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(parentRelationshipType).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteParentRelationshipType")]
        public async Task<IActionResult> ParentRelationshipTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Parent Relationship Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (ParentRelationshipTypeExists(id.Value) == true)
            {
                var authority = studentAggregateRepository.DeleteParentRelationshipType(id.Value);
            }
            return RedirectToAction(nameof(ParentRelationshipType)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool ParentRelationshipTypeExists(long id)
        {
            if (studentAggregateRepository.GetParentRelationshipTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Profession Type
        public IActionResult ProfessionType()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Profession Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            return View(studentAggregateRepository.GetAllProfessionType());
        }

        public IActionResult CreateOrEditProfessionType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Profession Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Profession Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var professionType = studentAggregateRepository.GetProfessionTypeById(id.Value);
                od.ID = professionType.ID;
                od.Name = professionType.Name;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateProfessionType([Bind("ID,Name")] ProfessionType professionType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                professionType.ModifiedId = userId;
                professionType.Active = true;
                if (professionType.ID == 0)
                {
                    professionType.InsertedId = userId;
                    professionType.Status = EntityStatus.ACTIVE;
                    studentAggregateRepository.CreateProfessionType(professionType);
                    return RedirectToAction(nameof(ProfessionType)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    studentAggregateRepository.UpdateProfessionType(professionType);
                    return RedirectToAction(nameof(ProfessionType)).WithSuccess("success", "Updated successfully");
                }
            }
            return View(professionType).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteProfessionType")]
        public async Task<IActionResult> ProfessionTypeDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Profession Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (ProfessionTypeExists(id.Value) == true)
            {
                var authority = studentAggregateRepository.DeleteProfessionType(id.Value);
            }
            return RedirectToAction(nameof(ProfessionType)).WithSuccess("success", "Deleted successfully");
        }

        private bool ProfessionTypeExists(long id)
        {
            if (studentAggregateRepository.GetProfessionTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Wing

        public IActionResult Wing()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Wing", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(studentAggregateRepository.GetAllWing());
        }

        public IActionResult CreateOrEditWing(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            GetStatusList();
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Wing", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "CREATE";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Wing", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "UPDATE";
                var wing = studentAggregateRepository.GetWingById(id.Value);
                od.ID = wing.ID;
                od.Name = wing.Name;
                od.Status = wing.Status;
            }
            return View(od);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateWing([Bind("ID,Name,Status")] Wing wing)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                wing.ModifiedId = userId;
                wing.Active = true;
                if (wing.ID == 0)
                {
                    wing.InsertedId = userId;
                    wing.Status = EntityStatus.ACTIVE;
                    studentAggregateRepository.CreateWing(wing);
                    return RedirectToAction(nameof(Wing)).WithSuccess("success", "Saved successfully");
                }
                else
                {                    
                    studentAggregateRepository.UpdateWing(wing);
                    return RedirectToAction(nameof(Wing)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(wing).WithDanger("error", "Not Saved");
        }


        // POST: GrievanceTypes/Delete/5
        [ActionName("DeleteWing")]
        public async Task<IActionResult> WingDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
                               //if (commonMethods.checkaccessavailable("Country", accessId, "Delete", "Master", roleId) == false)
                               //{
                               //    return RedirectToAction("AuthenticationFailed", "Accounts");
                               //}
            if (WingExists(id.Value) == true)
            {
                var wing = studentAggregateRepository.DeleteWing(id.Value);
            }
            return RedirectToAction(nameof(Wing)).WithSuccess("success", "Deleted successfully");
        }

        private bool WingExists(long id)
        {
            if (studentAggregateRepository.GetWingById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion


        #region State
        public IActionResult State()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("State", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var state = stateRepository.GetAllState().ToList();
            var country = countryRepository.GetAllCountries().ToList();
            var res = (from a in state
                       join b in country on a.CountryID equals b.ID
                       select new Statecls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           CountryID = a.CountryID,
                           CountryName = b.Name,
                           ModifiedDate = a.ModifiedDate
                       }).ToList();
            return View(res);
        }

        public IActionResult CreateOrEditState(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            Statecls od = new Statecls();
            od.countries = (from a in countryRepository.GetAllCountries().ToList()
                            select new CommonMasterModel
                            {
                                ID = a.ID,
                                Name = a.Name
                            }).ToList();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("State", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.CountryID = 0;
            }
            else
            {
                if (commonMethods.checkaccessavailable("State", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var state = stateRepository.GetStateById(id.Value);
                od.ID = state.ID;
                od.Name = state.Name;
                od.CountryID = state.CountryID;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateState([Bind("ID,Name,CountryID")] State state)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                state.ModifiedId = userId;
                state.Active = true;
                if (state.ID == 0)
                {
                    state.InsertedId = userId;
                    state.Status = EntityStatus.ACTIVE;
                    stateRepository.CreateState(state);
                    return RedirectToAction(nameof(State)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    stateRepository.UpdateState(state);
                    return RedirectToAction(nameof(State)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(state).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteState")]
        public async Task<IActionResult> StateDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("State", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (StateExists(id.Value) == true)
            {
                var Section = stateRepository.DeleteState(id.Value);
            }
            return RedirectToAction(nameof(State)).WithSuccess("success", "Deleted successfully");
        }

        private bool StateExists(long id)
        {
            if (stateRepository.GetStateById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region StateExcel
        public IActionResult downloadStateSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("State", accessId, "Upload", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable countrydt = new DataTable();
            countrydt.Columns.Add("Name");
            var country = countryRepository.GetAllCountries();
            foreach (var a in country)
            {
                DataRow dr = countrydt.NewRow();
                dr["Name"] = a.Name;
                countrydt.Rows.Add(dr);
            }
            countrydt.TableName = "Country";
            int lastCellNo1 = countrydt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(countrydt);
            var worksheet1 = oWB.Worksheet(1);
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Country");
            validationTable.Columns.Add("Name");
            validationTable.TableName = "State_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"State.xlsx");
        }
        public IActionResult DownloadState()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //// long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            // if (commonMethods.checkaccessavailable("State", accessId, "Download", "Master", staffTypeID) == false)
            // {
            //     return RedirectToAction("AuthenticationFailed", "Accounts");
            // }
            XLWorkbook oWB = new XLWorkbook();
            DataTable countrydt = new DataTable();
            // countrydt.Columns.Add("Sl No");
            countrydt.Columns.Add("Country Name");
            countrydt.Columns.Add("State Name");
            // countrydt.Columns.Add("Inserted On");
            // countrydt.Columns.Add("Modified On");
            var state = stateRepository.GetAllState();
            var country = countryRepository.GetAllCountries();
            var ress = (from a in state
                        join b in country on a.CountryID equals b.ID
                        select a).ToList();
            int i = 0;
            foreach (var a in ress)
            {
                DataRow dr = countrydt.NewRow();
                // dr["Sl No"] = ++i;
                dr["Country Name"] = country.Where(m => m.ID == a.CountryID).FirstOrDefault().Name;
                dr["State Name"] = a.Name;
                // dr["Inserted On"] = a.InsertedDate.ToString("dd-MMM-yyyy");
                // dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                countrydt.Rows.Add(dr);
            }
            countrydt.TableName = "State";
            oWB.AddWorksheet(countrydt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"State.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadState(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("State", accessId, "Upload", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            List<State> stateList = new List<State>();
            if (file != null)
            {
                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {

                   

                    long count = excelPackage.Workbook.Worksheets.Count();
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var state = stateRepository.GetAllState();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        if (firstWorksheet.Cells[i, 1].Value != null && firstWorksheet.Cells[i, 2].Value != null)
                        {
                            State states = new State();

                            string Countryname = firstWorksheet.Cells[i, 1].Value.ToString();
                            string Statename = firstWorksheet.Cells[i, 2].Value.ToString();
                            Country countryid = countryRepository.GetCountryByName(Countryname);
                            if (countryid != null)
                            {
                                if (state.Where(a => a.Name == Statename &&
                             a.CountryID == countryid.ID).FirstOrDefault() == null)
                                {
                                    states.Name = Statename;
                                    states.ModifiedId = userId;
                                    states.CountryID = countryid.ID;
                                    states.Active = true;
                                    states.InsertedId = userId;
                                    states.Status = EntityStatus.ACTIVE;

                                    stateList.Add(states);


                                }
                            }
                            
                        }
                    }

                   long countSaved= stateRepository.CreateAllState(stateList);
                }
            }
            return RedirectToAction("State", "Master").WithSuccess("success", stateList.Count()+" states uploaded successfully"); ;
        }
        #endregion



        #region Department
        public IActionResult Department(long id, string type)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Department", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var Department = type == "Group" ? departmentRepository.GetDepartmentListByGroupId(id).ToList() :
               departmentRepository.GetDepartmentListByOrganizationId(id).ToList();
            var organization = organizationRepository.GetAllOrganization().ToList();
            var groups = groupRepository.GetAllGroup().ToList();
            var res = (from a in Department

                       select new Departmentcls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           OrganizationID = a.OrganizationID,
                           OrganizationName = a.OrganizationID > 0 ?
                           organizationRepository.GetOrganizationById(a.OrganizationID)
                           .Name : null,
                           Active=a.Active,
                           DesignationCount=designationRepository.GetDesignationByDepartmentId(a.ID).Count(),
                           GroupID = a.GroupID,
                           GroupName = a.GroupID > 0 ?
                           groupRepository.GetGroupById(a.GroupID)
                           .Name : a.OrganizationID > 0 ?
                           groupRepository.GetGroupById(organizationRepository.
                           GetOrganizationById(a.OrganizationID).GroupID).Name : "",
                           
                           ModifiedDate = a.ModifiedDate,
                           ParentName = (a.ParentID != null && a.ParentID != 0) ?
                           GetDepartmentbyparentid(a.ParentID.Value, a.OrganizationID > 0 ?
                           a.OrganizationID : a.GroupID) : ""
                       }).ToList();

            if (type == "Group")
            {
                Group g = groupRepository.GetGroupById(id);

                ViewBag.Group = g.Name;
                ViewBag.GroupId = g.ID;

            }
            else
            {
                Organization g = organizationRepository.GetOrganizationById(id);
                //if (g.GroupID != 0)
                //{
                Group o = groupRepository.GetGroupById(g.GroupID);
                //}
                ViewBag.Organization = g.Name;
                ViewBag.OrganizationId = g.ID;

                ViewBag.Group = o.Name;
                ViewBag.GroupId = o.ID;



            }
            ViewBag.TypeId = id;
            ViewBag.Type = type;
            return View(res);
        }
        public IActionResult DepartmentAccess(long id, long eid, string type)
        {
            ViewBag.DepartmentId = id;
            ViewBag.eid = eid;
            ViewBag.type = type;
            return View(commonMethods.GetModuleSubModuleAccess(id));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateDepartmentModuleSubModuleDetails(EmployeeModuleSubModuleAccess employeeModuleSubModuleAccess)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                foreach (var a in employeeModuleSubModuleAccess.modules)
                {
                    if (a.subModules != null)
                    {
                        foreach (var b in a.subModules)
                        {
                            if (b.actionSubModules != null)
                            {
                                foreach (var c in b.actionSubModules)
                                {
                                    if (c.IsRequired == true && c.ActionAccessId == 0)
                                    {
                                        ActionDepartment action = new ActionDepartment();
                                        action.ActionID = c.ID;
                                        action.Active = true;
                                        action.InsertedId = userId;
                                        action.ModifiedId = userId;
                                        action.ModuleID = a.ID;
                                        action.Status = EntityStatus.ACTIVE;
                                        action.SubModuleID = b.ID;
                                        action.DepartmentID = employeeModuleSubModuleAccess.ID;
                                        actionAccessRepository.CreateActionDepartment(action);
                                    }
                                    else if (c.ActionAccessId != 0)
                                    {
                                        ActionDepartment actionAccess = actionAccessRepository.GetActionDepartmentById(c.ActionAccessId);
                                        if (actionAccess != null)
                                        {
                                            if (c.IsRequired == false)
                                            {
                                                actionAccessRepository.DeleteActionDepartment(actionAccess.ID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return RedirectToAction(nameof(DepartmentAccess), new { id = employeeModuleSubModuleAccess.ID }).WithSuccess("success", "Saved successfully"); ;
            }
            return RedirectToAction(nameof(DepartmentAccess), new { id = employeeModuleSubModuleAccess.ID }).WithSuccess("success", "Saved successfully"); ;
        }

        public string GetDepartmentbyparentid(long id, long oid)
        {

            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            var a = departmentRepository.GetDepartmentById(id);
            var departmentlist = departmentRepository.GetAllDepartment().Where(m => m.OrganizationID == oid).ToList();
            string deptname = a.Name;
            Department depts = a;
            while (depts.ParentID > 0)
            {
                depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                deptname = depts.Name + "-->" + deptname;
            }
            return deptname;
        }
        public JsonResult GetDepartment(long id)
        {
            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            var departmentlist = departmentRepository.GetAllDepartment().Where(m => m.OrganizationID == id).ToList();
            foreach (var a in departmentlist)
            {
                string deptname = a.Name;
                Department depts = a;
                while (depts.ParentID > 0)
                {
                    depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                    deptname = depts.Name + "-->" + deptname;
                }
                CommonMasterModel om = new CommonMasterModel();
                om.ID = a.ID;
                om.Name = deptname;
                dept.Add(om);
            }
            return Json(dept);
        }

        public JsonResult GetDepartmentByIdAndType(long id, string type, string deptId)
        {
            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            if (type == "1")
            {
                var departmentlist = departmentRepository.GetAllDepartment().Where(m => m.GroupID == id
                && m.ID != Convert.ToInt64(deptId)).ToList();
                foreach (var a in departmentlist)
                {
                    string deptname = a.Name;
                    Department depts = a;
                    while (depts.ParentID > 0)
                    {
                        depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                        deptname = depts.Name + "-->" + deptname;
                    }
                    CommonMasterModel om = new CommonMasterModel();
                    om.ID = a.ID;
                    om.Name = deptname;
                    dept.Add(om);
                }
                return Json(dept);
            }
            else
            {
                var departmentlist = departmentRepository.GetAllDepartment().Where(m => m.OrganizationID == id && m.ID != Convert.ToInt64(deptId)).ToList();
                foreach (var a in departmentlist)
                {
                    string deptname = a.Name;
                    Department depts = a;
                    while (depts.ParentID > 0)
                    {
                        depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                        deptname = depts.Name + "-->" + deptname;
                    }
                    CommonMasterModel om = new CommonMasterModel();
                    om.ID = a.ID;
                    om.Name = deptname;
                    dept.Add(om);
                }
                return Json(dept);
            }
        }
        public IActionResult CreateOrEditDepartment(long? id, long tid, string type)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            Departmentcls od = new Departmentcls();
            var departmentlist = commonMethods.GetDepartments();
            ViewBag.TypeId = tid;
            ViewBag.Type = type;
            if (type == "Group")
            {
                od.GroupID = tid;
                od.departments = departmentlist.Where(a => a.GroupID == tid).ToList();
            }
            else if (type == "Organization")
            {
                od.OrganizationID = tid;
                od.departments = departmentlist.Where(a => a.organisationID == tid).ToList();
            }

            //od.organizations = (from a in organizationRepository.GetAllOrganization().ToList()
            //                    select new CommonMasterModel
            //                    {
            //                        ID = a.ID,
            //                        Name = a.Name
            //                    }).ToList();

            //od.groups = (from a in groupRepository.GetAllGroup().ToList()
            //             select new CommonMasterModel
            //             {
            //                 ID = a.ID,
            //                 Name = a.Name
            //             }).ToList();


            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Department", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                //od.OrganizationID = 0;
                //od.GroupID = 0;
                od.IsPrimary = true;

            }
            else
            {
                if (commonMethods.checkaccessavailable("Department", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var Department = departmentRepository.GetDepartmentById(id.Value);
                od.ID = Department.ID;
                od.Name = Department.Name;
                od.OrganizationID = Department.OrganizationID;
                od.GroupID = Department.GroupID;
                od.IsPrimary = Department.IsPrimary;
                if (Department.OrganizationID > 0)
                    od.DepartmentType = "2";
                else
                    od.DepartmentType = "1";

                if (Department.ParentID != null && Department.ParentID > 0)
                {
                    od.ParentID = Department.ParentID.Value;
                }
                od.departments = od.departments.Where(a => a.ID != id.Value).ToList();


            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateDepartment([Bind("ID,Name,OrganizationID,GroupID,ParentID")] Department department)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            //if (ModelState.IsValid)
            //{
            department.ModifiedId = userId;
            department.Active = true;
            if (department.ID == 0)
            {
                department.InsertedId = userId;
                department.Status = EntityStatus.ACTIVE;
                departmentRepository.CreateDepartment(department);
                return RedirectToAction(nameof(Department), new { id = (department.GroupID != 0 ? department.GroupID : department.OrganizationID), type = (department.GroupID != 0 ? "Group" : "Organization") }).WithSuccess("success", "Saved successfully");
            }
            else
            {
                departmentRepository.UpdateDepartment(department);
                return RedirectToAction(nameof(Department), new { id = (department.GroupID != 0 ? department.GroupID : department.OrganizationID), type = (department.GroupID != 0 ? "Group" : "Organization") }).WithSuccess("success", "Updated successfully");
            }

            //}
            //  return View(department);
        }

        [ActionName("DeleteDepartment")]
        public async Task<IActionResult> DepartmentDeleteConfirmed(long? id, long tid, long type)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Department", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (DepartmentExists(id.Value) == true)
            {
                var Section = departmentRepository.DeleteDepartment(id.Value);
            }
            return RedirectToAction(nameof(Department), new { id = tid, type = type }).WithSuccess("success", "Deleted successfully");
        }

        private bool DepartmentExists(long id)
        {
            if (departmentRepository.GetDepartmentById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion



        #region DepartmentExcel
        public IActionResult downloadDepartmentSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("State", accessId, "Upload", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable countrydt = new DataTable();
            countrydt.Columns.Add("Name");
            var country = organizationRepository.GetAllOrganization();
            foreach (var a in country)
            {
                DataRow dr = countrydt.NewRow();
                dr["Name"] = a.Name;
                countrydt.Rows.Add(dr);
            }
            countrydt.TableName = "Organization";
            int lastCellNo1 = countrydt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(countrydt);
            var worksheet1 = oWB.Worksheet(1);
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Organization");
            validationTable.Columns.Add("Name");
            validationTable.TableName = "Department_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Department.xlsx");
        }
        public IActionResult DownloadDepartment()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            //if (commonMethods.checkaccessavailable("State", accessId, "Download", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable countrydt = new DataTable();
            //countrydt.Columns.Add("Sl No");
            countrydt.Columns.Add("Organization Name");
            countrydt.Columns.Add("Department Name");
            // countrydt.Columns.Add("Inserted On");
            // countrydt.Columns.Add("Modified On");
            var department = departmentRepository.GetAllDepartment();
            var org = organizationRepository.GetAllOrganization();
            var ress = (from a in department
                        join b in org on a.OrganizationID equals b.ID
                        select a).ToList();
            int i = 0;
            foreach (var a in ress)
            {
                DataRow dr = countrydt.NewRow();
                //dr["Sl No"] = ++i;
                dr["Organization Name"] = org.Where(m => m.ID == a.OrganizationID).FirstOrDefault().Name;
                dr["Department Name"] = a.Name;
                // dr["Inserted On"] = a.InsertedDate.ToString("dd-MMM-yyyy");
                //dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                countrydt.Rows.Add(dr);
            }
            countrydt.TableName = "Department";
            oWB.AddWorksheet(countrydt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Department.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadDepartment(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("State", accessId, "Upload", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {
                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {

                    long count = excelPackage.Workbook.Worksheets.Count();
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var dept = departmentRepository.GetAllDepartment();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Department states = new Department();
                        string Countryname = firstWorksheet.Cells[i, 1].Value.ToString();
                        string Statename = firstWorksheet.Cells[i, 2].Value.ToString();
                        var countryid = organizationRepository.GetAllOrganization().Where(m => m.Name == Countryname).FirstOrDefault().ID;
                        if (dept.Where(a => a.Name == Statename &&
                        a.OrganizationID == countryid).FirstOrDefault() == null)
                        {
                            states.Name = firstWorksheet.Cells[i, 2].Value.ToString();
                            states.ModifiedId = userId;
                            states.OrganizationID = countryid;
                            states.Active = true;
                            states.InsertedId = userId;
                            states.Status = EntityStatus.ACTIVE;
                            departmentRepository.CreateDepartment(states);
                        }
                    }
                }
            }
            return RedirectToAction("Department", "Master").WithSuccess("success", "Excel uploaded successfully"); ;
        }
        #endregion




        #region EducationalQualification
        public IActionResult EducationalQualification()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Educational Qualification", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var educationalQualification = educationQualificationRepository.GetAllEducationQualification().ToList();
            var educationalQualificationtype = educationQualificationTypeRepository.GetAllEducationQualificationType().ToList();

            var res = (from a in educationalQualification
                       join b in educationalQualificationtype on a.EducationQualificationTypeID equals b.ID
                       select new EducationalQualificationcls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           EducationQualificationTypeID = a.EducationQualificationTypeID,
                           EducationalQualificationTypeName = b.Name,
                           ModifiedDate = a.ModifiedDate
                       }).ToList();
            return View(res);
        }

        public IActionResult CreateOrEditEducationalQualification(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            EducationalQualificationcls od = new EducationalQualificationcls();
            od.educationalQualificationType = (from a in educationQualificationTypeRepository.GetAllEducationQualificationType().ToList()
                                               select new CommonMasterModel
                                               {
                                                   ID = a.ID,
                                                   Name = a.Name
                                               }).ToList();

            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Educational Qualification", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.EducationQualificationTypeID = 0;
            }
            else
            {
                if (commonMethods.checkaccessavailable("Educational Qualification", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var educationalQualification = educationQualificationRepository.GetEducationQualificationById(id.Value);
                od.ID = educationalQualification.ID;
                od.Name = educationalQualification.Name;
                od.EducationQualificationTypeID = educationalQualification.EducationQualificationTypeID;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEducationalQualification([Bind("ID,Name,EducationQualificationTypeID")] EducationQualification educationQualification)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                educationQualification.ModifiedId = userId;
                educationQualification.Active = true;
                if (educationQualification.ID == 0)
                {
                    educationQualification.InsertedId = userId;
                    educationQualification.Status = EntityStatus.ACTIVE;
                    educationQualificationRepository.CreateEducationQualification(educationQualification);
                    return RedirectToAction(nameof(EducationalQualification)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    educationQualificationRepository.UpdateEducationQualification(educationQualification);
                    return RedirectToAction(nameof(EducationalQualification)).WithSuccess("success", "Updated successfully");
                }
            }
            return View(educationQualification).WithDanger("error", "Not Saved"); ;
        }

        [ActionName("DeleteEducationalQualification")]
        public async Task<IActionResult> EducationalQualificationDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Educational Qualification", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (EducationalQualificationExists(id.Value) == true)
            {
                var Section = educationQualificationRepository.DeleteEducationQualification(id.Value);
            }
            return RedirectToAction(nameof(EducationalQualification)).WithSuccess("success", "Deleted successfully");
        }

        private bool EducationalQualificationExists(long id)
        {

            if (educationQualificationRepository.GetEducationQualificationById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Standard
        public IActionResult Standard()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Standard", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var Standard = standardRepository.GetAllStandard().ToList();
            var board = boardRepository.GetAllBoard().ToList();

            var res = (from a in Standard

                       select new Standardcls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           BoardID = a.BoardID,
                           StreamType = a.StreamType,
                           ModifiedDate = a.ModifiedDate,
                           Status = a.Status
                       }).ToList();
            return View(res);
        }

        public IActionResult CreateOrEditStandard(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            GetStatusList();
            Standardcls od = new Standardcls();
            od.boards = (from a in boardRepository.GetAllBoard().ToList()
                         select new CommonMasterModel
                         {
                             ID = a.ID,
                             Name = a.Name
                         }).ToList();

            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Standard", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.StreamType = false;
                od.BoardID = 0;
            }
            else
            {
                if (commonMethods.checkaccessavailable("Standard", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var standard = standardRepository.GetStandardById(id.Value);
                od.ID = standard.ID;
                od.Name = standard.Name;
                od.StreamType = standard.StreamType;
                od.BoardID = standard.BoardID;
                od.Status = standard.Status;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[ActionName("CreateOrEditStandard")]
        public async Task<IActionResult> SaveOrUpdateStandard([Bind("ID,Name,BoardID,StreamType,Status")] Standard standard)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;           
         
                standard.ModifiedId = userId;
                standard.Active = true;
                if (standard.ID == 0)
                {
                    standard.InsertedId = userId;
                    standard.Status = EntityStatus.ACTIVE;
                    standardRepository.CreateStandard(standard);
                    return RedirectToAction(nameof(Standard)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    standardRepository.UpdateStandard(standard);
                    return RedirectToAction(nameof(Standard)).WithSuccess("success", "Updated successfully");
                }


            
            //return View(standard).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteStandard")]
        public async Task<IActionResult> StandardDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Standard", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (StandardExists(id.Value) == true)
            {
                var Section = standardRepository.DeleteStandard(id.Value);
            }
            return RedirectToAction(nameof(Standard)).WithSuccess("success", "Deleted successfully");
        }

        private bool StandardExists(long id)
        {
            if (standardRepository.GetStandardById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        //#region Stream
        //public IActionResult Stream()
        //{
        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                       // long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    if (commonMethods.checkaccessavailable("Stream", accessId, "List", "Master", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }

        //    var stream = streamRepository.GetAllStream().ToList();
        //    var standard = standardRepository.GetAllStandard().ToList();

        //    var res = (from a in stream
        //                   //join b in standard on a.StandardID equals b.ID
        //               select new Streamcls
        //               {
        //                   ID = a.ID,
        //                   Name = a.Name,
        //                   //StandardID = a.StandardID,
        //                   //StandardName = b.Name,
        //                   ModifiedDate = a.ModifiedDate,
        //                   status = a.Status
        //               }).ToList();
        //    return View(res);
        //}

        //public IActionResult CreateOrEditStream(long? id)
        //{
        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                       // long roleId = HttpContext.Session.GetInt32("roleId").Value;

        //    GetStatusList();
        //    Streamcls od = new Streamcls();
        //    od.Standards = (from a in standardRepository.GetAllStandard().ToList()
        //                    select new CommonMasterModel
        //                    {
        //                        ID = a.ID,
        //                        Name = a.Name
        //                    }).ToList();

        //    if (id == null)
        //    {
        //        if (commonMethods.checkaccessavailable("Stream", accessId, "Create", "Master", roleId) == false)
        //        {
        //            return RedirectToAction("AuthenticationFailed", "Accounts");
        //        }
        //        ViewBag.status = "Create";
        //        od.ID = 0;
        //        od.Name = "";
        //        od.StandardID = 0;
        //    }
        //    else
        //    {
        //        if (commonMethods.checkaccessavailable("Stream", accessId, "Edit", "Master", roleId) == false)
        //        {
        //            return RedirectToAction("AuthenticationFailed", "Accounts");
        //        }

        //        ViewBag.status = "Update";
        //        var stream = streamRepository.GetStreamById(id.Value);
        //        od.ID = stream.ID;
        //        od.Name = stream.Name;
        //        od.StandardID = stream.StandardID;
        //        od.status = stream.Status;
        //    }
        //    return View(od);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> SaveOrUpdateStream([Bind("ID,Name,StandardID,Status")] ApplicationCore.Entities.Stream stream)
        //{
        //    userId = HttpContext.Session.GetInt32("userId").Value;
        //    if (ModelState.IsValid)
        //    {
        //        stream.ModifiedId = userId;
        //        stream.Active = true;
        //        if (stream.ID == 0)
        //        {
        //            stream.InsertedId = userId;
        //            stream.Status = EntityStatus.ACTIVE;
        //            streamRepository.CreateStream(stream);
        //            return RedirectToAction(nameof(Stream)).WithSuccess("success", "Saved successfully");
        //        }
        //        else
        //        {
        //            streamRepository.UpdateStream(stream);
        //            return RedirectToAction(nameof(Stream)).WithSuccess("success", "Updated successfully");
        //        }

        //    }
        //    return View(stream).WithDanger("error", "Not Saved");
        //}

        //[ActionName("DeleteStream")]
        //public async Task<IActionResult> StreamDeleteConfirmed(long? id)
        //{
        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                       // long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    if (commonMethods.checkaccessavailable("Stream", accessId, "Delete", "Master", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }

        //    if (StreamExists(id.Value) == true)
        //    {
        //        var Section = streamRepository.DeleteStream(id.Value);
        //    }
        //    return RedirectToAction(nameof(Stream)).WithSuccess("success", "Deleted successfully");
        //}

        //private bool StreamExists(long id)
        //{
        //    if (streamRepository.GetStreamById(id) != null)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
        //#endregion
        #region SubModule
        public IActionResult SubModule()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Sub Module", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var SubModule = subModuleRepository.GetAllSubModule();
            var module = moduleRepository.GetAllModule();
            if (SubModule != null)
            {
                var res = (from a in SubModule
                           join b in module on a.ModuleID equals b.ID
                           select new SubModulecls
                           {
                               ID = a.ID,
                               Name = a.Name,
                               ModuleID = a.ModuleID,
                               ModuleName = b.Name,
                               ModifiedDate = a.ModifiedDate
                           }).ToList();
                return View(res);
            }
            else
            {
                return View(null);
            }

        }

        public IActionResult CreateOrEditSubModule(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            SubModulecls od = new SubModulecls();
            od.Modules = (from a in moduleRepository.GetAllModule().ToList()
                          select new CommonMasterModel
                          {
                              ID = a.ID,
                              Name = a.Name
                          }).ToList();

            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Sub Module", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.ModuleID = 0;
            }
            else
            {
                if (commonMethods.checkaccessavailable("Sub Module", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var subModule = subModuleRepository.GetSubModuleById(id.Value);
                od.ID = subModule.ID;
                od.Name = subModule.Name;
                od.ModuleID = subModule.ModuleID;
                od.Url = subModule.Url;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateSubModule([Bind("ID,Name,Url,ModuleID")] SubModule subModule)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                subModule.ModifiedId = userId;
                subModule.Active = true;
                if (subModule.ID == 0)
                {
                    subModule.InsertedId = userId;
                    subModule.Status = EntityStatus.ACTIVE;
                    subModuleRepository.CreateSubModule(subModule);
                    return RedirectToAction(nameof(SubModule)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    subModuleRepository.UpdateSubModule(subModule);
                    return RedirectToAction(nameof(SubModule)).WithSuccess("success", "Updated successfully");
                }



            }
            return View(subModule).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteSubModule")]
        public async Task<IActionResult> SubModuleDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Sub Module", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (SubModuleExists(id.Value) == true)
            {
                var Section = subModuleRepository.DeleteSubModule(id.Value);
            }
            return RedirectToAction(nameof(SubModule)).WithSuccess("success", "Deleted successfully");
        }

        private bool SubModuleExists(long id)
        {
            if (subModuleRepository.GetSubModuleById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Sub Module Excel      
        public IActionResult downloadSubModuleSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Sub Module", accessId, "Upload", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            moduledt.Columns.Add("Name");
            var modules = moduleRepository.GetAllModule();
            foreach (var a in modules)
            {
                DataRow dr = moduledt.NewRow();
                dr["Name"] = a.Name;
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "Module";
            int lastCellNo1 = moduledt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(moduledt);
            var worksheet1 = oWB.Worksheet(1);
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Module");
            validationTable.Columns.Add("Name");
            validationTable.Columns.Add("Url");
            validationTable.TableName = "Sub_Module_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SubModule.xlsx");
        }
        public IActionResult DownloadSubModule()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            //if (commonMethods.checkaccessavailable("Sub Module", accessId, "Download", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            //  moduledt.Columns.Add("Sl No");
            moduledt.Columns.Add("Module Name");
            moduledt.Columns.Add("Sub Module Name");
            moduledt.Columns.Add("Url");
            // moduledt.Columns.Add("Inserted On");
            // moduledt.Columns.Add("Modified On");
            var submodules = subModuleRepository.GetAllSubModule();
            var modules = moduleRepository.GetAllModule();
            int i = 0;
            foreach (var a in submodules)
            {
                DataRow dr = moduledt.NewRow();
                // dr["Sl No"] = ++i;
                dr["Module Name"] = modules.Where(m => m.ID == a.ModuleID).FirstOrDefault().Name;
                dr["Sub Module Name"] = a.Name;
                dr["Url"] = a.Url;
                // dr["Inserted On"] = a.InsertedDate.ToString("dd-MMM-yyyy");
                // dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "SubModule";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SubModule.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadSubModule(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Sub Module", accessId, "Upload", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {
                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var submodule = subModuleRepository.GetAllSubModule();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        SubModule subModule = new SubModule();
                        if (firstWorksheet.Cells[i, 1].Value != null)
                        {
                            string Modulename = firstWorksheet.Cells[i, 1].Value.ToString();
                            string subModulename = firstWorksheet.Cells[i, 2].Value.ToString();
                            if (Modulename != "")
                            {
                                var moduleid = moduleRepository.GetModuleByName(Modulename).ID;
                                if (submodule.Where(a => a.Name == subModulename && a.ModuleID == moduleid).FirstOrDefault() == null)
                                {
                                    subModule.Name = firstWorksheet.Cells[i, 2].Value.ToString();
                                    subModule.ModifiedId = userId;
                                    subModule.Url = firstWorksheet.Cells[i, 3].Value.ToString();
                                    subModule.ModuleID = moduleid;
                                    subModule.Active = true;
                                    subModule.InsertedId = userId;
                                    subModule.Status = EntityStatus.ACTIVE;
                                    subModuleRepository.CreateSubModule(subModule);
                                }
                            }
                        }
                    }
                }
            }
            return RedirectToAction("SubModule", "Master").WithSuccess("success", "Excel uploaded successfully"); ;
        }

        #endregion

        #region BankBranch
        public IActionResult BankBranch()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Bank Branch", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var bankbranch = bankBranchRepository.GetAllBankBranch();
            var bank = bankRepository.GetAllBank();
            var res = (from a in bankbranch
                       join b in bank on a.BankID equals b.ID
                       select new Bankbranchcls
                       {
                           BankID = a.BankID,
                           BankName = b.Name,
                           ID = a.ID,
                           IfscCode = a.IfscCode,
                           ModifiedDate = a.ModifiedDate,
                           Name = a.Name
                       }).ToList();
            return View(res);
        }

        public IActionResult CreateOrEditBankBranch(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            Bankbranchcls od = new Bankbranchcls();
            od.banks = (from a in bankRepository.GetAllBank().ToList()
                        select new CommonMasterModel
                        {
                            ID = a.ID,
                            Name = a.Name
                        }).ToList();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Bank Branch", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Bank Branch", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var bankBranch = bankBranchRepository.GetBankBranchById(id.Value);
                od.ID = bankBranch.ID;
                od.Name = bankBranch.Name;
                od.BankID = bankBranch.BankID;
                od.IfscCode = bankBranch.IfscCode;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateBankBranch([Bind("ID,Name,IfscCode,BankID")] BankBranch bankBranch)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                bankBranch.ModifiedId = userId;
                bankBranch.Active = true;
                if (bankBranch.ID == 0)
                {
                    bankBranch.InsertedId = userId;
                    bankBranch.Status = EntityStatus.ACTIVE;
                    bankBranchRepository.CreateBankBranch(bankBranch);
                    return RedirectToAction(nameof(BankBranch)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    bankBranchRepository.UpdateBankBranch(bankBranch);
                    return RedirectToAction(nameof(BankBranch)).WithSuccess("success", "Updated successfully");
                }



            }
            return View(bankBranch).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteBankBranch")]
        public async Task<IActionResult> BankBranchDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Bank Branch", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (BankBranchExists(id.Value) == true)
            {
                var BankBranch = bankBranchRepository.DeleteBankBranch(id.Value);
            }
            return RedirectToAction(nameof(BankBranch)).WithSuccess("success", "Deleted successfully");
        }

        private bool BankBranchExists(long id)
        {
            if (bankBranchRepository.GetBankBranchById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region DocumentSubType
        public IActionResult DocumentSubType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Document Sub Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }


            var DocumentSubType = id != null ? documentSubTypeRepository.
                GetAllDocumentSubTypeByDocumentTypeId(id.Value).ToList() :
                documentSubTypeRepository.GetAllDocumentSubTypes().ToList();

            // var DocumentSubType = documentSubTypeRepository.GetAllDocumentSubTypes();
            var documentType = documentTypeRepository.GetAllDocumentTypes();
            var res = (from a in DocumentSubType
                       join b in documentType on a.DocumentTypeID equals b.ID
                       select new DocumentSubTypecls
                       {
                           DocumentTypeID = a.DocumentTypeID,
                           DocumentTypeName = b.Name,
                           ID = a.ID,
                           ModifiedDate = a.ModifiedDate,
                           Name = a.Name,
                           Status = a.Status,
                           Active = a.Active,
                           ModifiedName = employeeRepository.GetEmployeeFullNameById(a.ModifiedId)
                       }).ToList();

            if (id != null)
            {
                DocumentType group = documentTypeRepository.GetDocumentTypeById(id.Value);

                ViewBag.Group = group.Name;
                ViewBag.GroupId = id;
            }
            else
            {
                ViewBag.GroupId = 0;
            }

            return View(res);
        }

        public IActionResult CreateOrEditDocumentSubType(long? id, long? doctypeId)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;


            //var groups = doctypeId == null ? documentTypeRepository.GetAllDocumentTypes().ToList() :
            //  documentTypeRepository.GetAllDocumentTypes().Where(a => a.ID == doctypeId).ToList();



            DocumentSubTypecls od = new DocumentSubTypecls();
            //od.documentTypes = (from a in documentTypeRepository.GetAllDocumentTypes().ToList()
            //                    select new CommonMasterModel
            //                    {
            //                        ID = a.ID,
            //                        Name = a.Name
            //                    }).ToList();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Document Sub Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.DocumentTypeID = doctypeId.Value;
            }
            else
            {
                if (commonMethods.checkaccessavailable("Document Sub Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var documentSubType = documentSubTypeRepository.GetDocumentSubTypeById(id.Value);
                od.ID = documentSubType.ID;
                od.Name = documentSubType.Name;
                od.DocumentTypeID = documentSubType.DocumentTypeID;
            }

            if (doctypeId != null)
            {
                DocumentType group = documentTypeRepository.GetDocumentTypeById(doctypeId.Value);

                ViewBag.Group = group.Name;
                ViewBag.GroupId = doctypeId;
            }
            else
            {
                ViewBag.GroupId = 0;
            }

            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateDocumentSubType([Bind("ID,Name,DocumentTypeID")] DocumentSubType documentSubType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                documentSubType.ModifiedId = userId;
                documentSubType.Active = true;
                if (documentSubType.ID == 0)
                {
                    documentSubType.InsertedId = userId;
                    documentSubType.Status = EntityStatus.ACTIVE;
                    documentSubTypeRepository.CreateDocumentSubType(documentSubType);
                    return RedirectToAction(nameof(DocumentSubType), new { id = documentSubType.DocumentTypeID }).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    documentSubTypeRepository.UpdateDocumentSubType(documentSubType);
                    return RedirectToAction(nameof(DocumentSubType), new { id = documentSubType.DocumentTypeID }).WithSuccess("success", "Updated successfully");
                }



            }
            return View(documentSubType).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteDocumentSubType")]
        public async Task<IActionResult> DocumentSubTypeDeleteConfirmed(long? id, long typeid)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Document Sub Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (DocumentSubTypeExists(id.Value) == true)
            {
                var DocumentSubType = documentSubTypeRepository.DeleteDocumentSubType(id.Value);
            }
            return RedirectToAction(nameof(DocumentSubType), new { id = typeid }).WithSuccess("success", "Deleted successfully");
        }

        private bool DocumentSubTypeExists(long id)
        {
            if (documentSubTypeRepository.GetDocumentSubTypeById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Action
        public IActionResult Action()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Action", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var action = actionRepository.GetAllAction();
            var subModule = subModuleRepository.GetAllSubModule();
            var modules = moduleRepository.GetAllModule();
            var res = (from a in action
                       join b in subModule on a.SubModuleID equals b.ID
                       join c in modules on b.ModuleID equals c.ID
                       select new Actioncls
                       {
                           ID = a.ID,
                           ModifiedDate = a.ModifiedDate,
                           Name = a.Name,
                           ModuleID = b.ModuleID,
                           ModuleName = c.Name,
                           SubModuleID = a.SubModuleID,
                           SubModuleName = b.Name
                       }).ToList();
            return View(res);
        }

        public IActionResult CreateOrEditAction(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            Actioncls od = new Actioncls();
            od.modules = (from a in moduleRepository.GetAllModule().ToList()
                          select new CommonMasterModel
                          {
                              ID = a.ID,
                              Name = a.Name
                          }).ToList();

            od.submodules = (from a in subModuleRepository.GetAllSubModule().ToList()
                             where a.ModuleID == od.ModuleID
                             select new SubModule
                             {
                                 ID = a.ID,
                                 Name = a.Name
                             }).ToList();

            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Action", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.ModuleID = od.modules.FirstOrDefault().ID;
                od.submodules = (from a in subModuleRepository.GetAllSubModule().ToList()
                                 where a.ModuleID == od.modules.FirstOrDefault().ID
                                 select new SubModule
                                 {
                                     ID = a.ID,
                                     Name = a.Name
                                 }).ToList();
            }
            else
            {
                if (commonMethods.checkaccessavailable("Action", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var action = actionRepository.GetActionById(id.Value);
                od.ID = action.ID;
                od.Name = action.Name;
                od.SubModuleID = action.SubModuleID;
                od.ModuleID = subModuleRepository.GetSubModuleById(action.SubModuleID).ModuleID;
                od.submodules = subModuleRepository.GetAllSubModule().Where(a => a.ModuleID == od.ModuleID).ToList();

            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateAction([Bind("ID,Name,SubModuleID")]Actioncls actioncls)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                Action action = new ApplicationCore.Entities.Action();
                action.ID = actioncls.ID;
                action.Name = actioncls.Name;
                action.SubModuleID = actioncls.SubModuleID;
                action.ModifiedId = userId;
                action.Active = true;
                if (action.ID == 0)
                {
                    action.InsertedId = userId;
                    action.Status = EntityStatus.ACTIVE;
                    actionRepository.CreateAction(action);
                    return RedirectToAction(nameof(Action)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    actionRepository.UpdateAction(action);
                    return RedirectToAction(nameof(Action)).WithSuccess("success", "Updated successfully");
                }

            }
            return View(actioncls).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteAction")]
        public async Task<IActionResult> ActionDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Action", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (ActionExists(id.Value) == true)
            {
                var Action = actionRepository.DeleteAction(id.Value);
            }
            return RedirectToAction(nameof(Action)).WithSuccess("success", "Deleted successfully");
        }

        private bool ActionExists(long id)
        {
            if (actionRepository.GetActionById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public JsonResult GetSubModulesByModuleId(long id)
        {
            try
            {
                var res = subModuleRepository.GetAllSubModule().Where(a => a.ModuleID == id).ToList();
                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        #endregion

        #region ActionExcel
        public IActionResult downloadActionSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Action", accessId, "Upload", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();

            DataTable moduledt = new DataTable();
            moduledt.Columns.Add("ModuleName");
            var module = moduleRepository.GetAllModule();
            foreach (var a in module)
            {
                DataRow dr = moduledt.NewRow();
                dr["ModuleName"] = a.Name;
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "Module";

            DataTable submoduledt = new DataTable();
            submoduledt.Columns.Add("SubModuleName");
            var submodule = subModuleRepository.GetAllSubModule();
            foreach (var b in submodule)
            {
                DataRow dr = submoduledt.NewRow();
                dr["SubModuleName"] = b.Name;
                submoduledt.Rows.Add(dr);
            }
            submoduledt.TableName = "SubModule";

            int lastCellNo1 = moduledt.Rows.Count + 1;
            int lastCellNo2 = submoduledt.Rows.Count + 1;

            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(moduledt);
            oWB.AddWorksheet(submoduledt);
            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Module");
            validationTable.Columns.Add("SubModule");
            validationTable.Columns.Add("Name");
            validationTable.TableName = "Action_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(2).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet1.Hide();
            worksheet2.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", $"Action.xlsx");
        }
        public IActionResult DownloadAction()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            //if (commonMethods.checkaccessavailable("City", accessId, "Download", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable actiondt = new DataTable();
            // actiondt.Columns.Add("Sl No");
            actiondt.Columns.Add("Module Name");
            actiondt.Columns.Add("Sub Module Name");
            actiondt.Columns.Add("Action Name");
            //  actiondt.Columns.Add("Inserted On");
            //  actiondt.Columns.Add("Modified On");
            var module = moduleRepository.GetAllModule();
            var submodule = subModuleRepository.GetAllSubModule();
            var action = actionRepository.GetAllAction();
            var ress = (from a in action
                        join b in submodule on a.SubModuleID equals b.ID
                        join c in module on b.ModuleID equals c.ID
                        select a).ToList();


            int i = 0;
            foreach (var a in ress)
            {
                var modulename = subModuleRepository.GetSubModuleById(a.SubModuleID).ModuleID;
                // var modulename1 = moduleRepository.GetModuleById(submodulebymodule).Name;

                DataRow dr = actiondt.NewRow();
                //dr["Sl No"] = ++i;
                dr["Module Name"] = module.Where(m => m.ID == modulename).FirstOrDefault().Name;
                dr["Sub Module Name"] = submodule.Where(m => m.ID == a.SubModuleID).FirstOrDefault().Name;
                dr["Action Name"] = a.Name;
                // dr["Inserted On"] = a.InsertedDate.ToString("dd-MMM-yyyy");
                // dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                actiondt.Rows.Add(dr);
            }
            actiondt.TableName = "Action";
            oWB.AddWorksheet(actiondt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Action.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadAction(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("City", accessId, "Upload", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[2];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var action = actionRepository.GetAllAction().ToList();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Action cities = new Action();
                        string Countryname = firstWorksheet.Cells[i, 1].Value.ToString();
                        string Statename = firstWorksheet.Cells[i, 2].Value.ToString();
                        string Cityname = firstWorksheet.Cells[i, 3].Value.ToString();
                        var countryid = moduleRepository.GetAllModule().Where(m => m.Name == Countryname).FirstOrDefault().ID;
                        var stateid = subModuleRepository.GetAllSubModule().Where(m => m.Name == Statename).FirstOrDefault().ID;
                        if (action.Where(a => a.Name == Cityname && a.SubModuleID == stateid).FirstOrDefault() == null)
                        {
                            cities.Name = Cityname;
                            cities.ModifiedId = userId;
                            cities.SubModuleID = stateid;
                            cities.Active = true;
                            cities.InsertedId = userId;
                            cities.Status = EntityStatus.ACTIVE;
                            actionRepository.CreateAction(cities);
                        }
                    }
                }
            }
            return RedirectToAction("Action", "Master").WithSuccess("success", "Excel uploaded successfully"); ;
        }
        #endregion

        #region Authority
        public IActionResult Authority()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Authority", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var Authority = authorityAggregateRepository.GetAllAuthority().ToList();
            var authorityType = authorityAggregateRepository.GetAllAuthorityType().ToList();
            var res = (from a in Authority
                       join b in authorityType on a.AuthorityTypeID equals b.ID
                       select new Authoritycls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           AuthorityTypeID = a.AuthorityTypeID,
                           AuthorityTypeName = b.Name,
                           ModifiedDate = a.ModifiedDate
                       }).ToList();
            return View(res);
        }

        public IActionResult CreateOrEditAuthority(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            Authoritycls od = new Authoritycls();
            od.authorityTypes = (from a in authorityAggregateRepository.GetAllAuthorityType().ToList()
                                 select new CommonMasterModel
                                 {
                                     ID = a.ID,
                                     Name = a.Name
                                 }).ToList();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Authority", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.AuthorityTypeID = 0;
            }
            else
            {
                if (commonMethods.checkaccessavailable("Authority", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var authority = authorityAggregateRepository.GetAuthorityById(id.Value);
                od.ID = authority.ID;
                od.Name = authority.Name;
                od.AuthorityTypeID = authority.AuthorityTypeID;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateAuthority([Bind("ID,Name,AuthorityTypeID")] Authority authority)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                authority.ModifiedId = userId;
                authority.Active = true;
                if (authority.ID == 0)
                {
                    authority.InsertedId = userId;
                    authority.Status = EntityStatus.ACTIVE;
                    authorityAggregateRepository.CreateAuthority(authority);
                    return RedirectToAction(nameof(Authority)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    authorityAggregateRepository.UpdateAuthority(authority);
                    return RedirectToAction(nameof(Authority)).WithSuccess("success", "Updated successfully");
                }



            }
            return View(authority).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteAuthority")]
        public async Task<IActionResult> AuthorityDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Authority", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (AuthorityExists(id.Value) == true)
            {
                var Section = authorityAggregateRepository.DeleteAuthority(id.Value);
            }
            return RedirectToAction(nameof(Authority)).WithSuccess("success", "Deleted successfully");
        }

        private bool AuthorityExists(long id)
        {
            if (authorityAggregateRepository.GetAuthorityById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        //#region School Wing
        //public IActionResult SchoolWing(long OrganizationAcademicID, long AcademicSessionId)
        //{
        //    ViewData["OrganizationAcademicId"] = OrganizationAcademicID;
        //    ViewData["AcademicSessionId"] = AcademicSessionId;
        //    var OrganisationId = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == OrganizationAcademicID).Select(a => a.OrganizationId).FirstOrDefault();
        //    ViewData["OrganisationName"] = organizationRepository.GetAllOrganization().Where(a => a.ID == OrganisationId).Select(a => a.Name).FirstOrDefault();
           
        //    ViewBag.AcademicSessionName = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicSessionId).Select(a => a.DisplayName).FirstOrDefault();


        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                       // long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    if (commonMethods.checkaccessavailable("School Wing", accessId, "List", "Master", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }

        //    var res = commonMethods.GetSchoolWing().Where(a=>a.OrganizationAcademicID == OrganizationAcademicID);


        //    return View(res);
        //}

        //public IActionResult CreateOrEditSchoolWing(long? id,long OrganisationAcademicID, long AcademicSessionId)
        //{
        //    ViewData["OrganizationAcademicId"] = OrganisationAcademicID;
        //    var OrganisationId = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == OrganisationAcademicID).Select(a => a.OrganizationId).FirstOrDefault();
        //    ViewData["OrganisationName"] = organizationRepository.GetAllOrganization().Where(a => a.ID == OrganisationId).Select(a => a.Name).FirstOrDefault();
        //    ViewData["AcademicSessionId"] = AcademicSessionId;
        //    ViewBag.AcademicSessionName = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AcademicSessionId).Select(a => a.DisplayName).FirstOrDefault();

        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                       // long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    GetStatusList();
        //    Schoolwingcls od = new Schoolwingcls();
           
        //    var schollwingid = studentAggregateRepository.GetAllSchoolWing().Where(b=>b.OrganizationAcademicID== OrganisationAcademicID).Select(a => a.WingID);

        //    od.wings = commonMethods.GetWing().Where(a=> !schollwingid.Contains(a.ID)).ToList();
        //    if (id == null)
        //    {
        //        if (commonMethods.checkaccessavailable("School Wing", accessId, "Create", "Master", roleId) == false)
        //        {
        //            return RedirectToAction("AuthenticationFailed", "Accounts");
        //        }

        //        ViewBag.status = "Create";
        //        od.ID = 0;
        //        od.OrganizationAcademicID = OrganisationAcademicID;
        //        od.WingID = 0;
              
        //        od.AcademicSessionId = AcademicSessionId;
        //    }
        //    else
        //    {
        //        if (commonMethods.checkaccessavailable("School Wing", accessId, "Edit", "Master", roleId) == false)
        //        {
        //            return RedirectToAction("AuthenticationFailed", "Accounts");
        //        }

        //        ViewBag.status = "Update";
        //        var schoolwingcls = commonMethods.GetSchoolWing().Where(a => a.ID == id.Value).FirstOrDefault();
        //        od.ID = schoolwingcls.ID;
        //        od.OrganizationAcademicID = schoolwingcls.OrganizationAcademicID;
        //        od.WingID = schoolwingcls.WingID;
        //        od.AcademicSessionId = AcademicSessionId;
        //        od.Status = schoolwingcls.Status;
        //        od.wingName = schoolwingcls.wingName;

        //    }
        //    return View(od);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> SaveOrUpdateSchoolWing(long ID, long OrganizationAcademicID, long[] MultiWingID,long AcademicSessionId,string Status,long WingID)
        //{
        //    CheckLoginStatus();
        //    Schoolwingcls schoolwingmodel = new Schoolwingcls();
        //    schoolwingmodel.OrganizationAcademicID = OrganizationAcademicID;
        //    schoolwingmodel.MultiWingID = MultiWingID;
        //    schoolwingmodel.WingID = WingID;
        //    schoolwingmodel.AcademicSessionId = AcademicSessionId;
        //    schoolwingmodel.ID = ID;
        //    schoolwingmodel.Status = Status;

        //    var routeValues = new RouteValueDictionary { { "OrganizationAcademicId", schoolwingmodel.OrganizationAcademicID },{ "AcademicSessionId", schoolwingmodel.AcademicSessionId } };
           
           
        //        if (schoolwingmodel.ID == 0)
        //        {
        //        ArrayList myList = new ArrayList(MultiWingID);
        //        foreach (long str in myList)
        //        {
        //            SchoolWing schoolWing = new SchoolWing();
        //            schoolWing.ModifiedId = userId;
        //            schoolWing.Active = true;
        //            schoolWing.InsertedId = userId;
        //            schoolWing.Status = EntityStatus.ACTIVE;
        //            schoolWing.WingID = str;
        //            schoolWing.OrganisationID = 0;
        //            schoolWing.OrganizationAcademicID = schoolwingmodel.OrganizationAcademicID;
        //            studentAggregateRepository.CreateSchoolWing(schoolWing);                   
        //        }
        //        return RedirectToAction("SchoolWing", "Master", routeValues).WithSuccess("Success", "Saved successfully");

        //        }
        //        else
        //        {
        //        SchoolWing schoolWing = new SchoolWing();
        //        schoolWing.ID = schoolwingmodel.ID;
        //        schoolWing.ModifiedId = userId;
        //        schoolWing.Active = true;
        //        schoolWing.Status = schoolwingmodel.Status;
        //        schoolWing.OrganisationID = 0;
        //        schoolWing.OrganizationAcademicID =schoolwingmodel.OrganizationAcademicID;
        //        schoolWing.WingID = schoolwingmodel.WingID;
        //        studentAggregateRepository.UpdateSchoolWing(schoolWing);
        //         return RedirectToAction("SchoolWing", "Master", routeValues).WithSuccess("Success", "Updated successfully");
        //        }



        //    //return View(schoolWing).WithDanger("error", "Not Saved");
        //}

        ////[ActionName("DeleteSchoolWing")]
        ////public async Task<IActionResult> SchoolWingDeleteConfirmed(long? id)
        ////{
        ////    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        ////    long roleId = HttpContext.Session.GetInt32("roleId").Value;
        ////    if (commonMethods.checkaccessavailable("School Wing", accessId, "Delete", "Master", roleId) == false)
        ////    {
        ////        return RedirectToAction("AuthenticationFailed", "Accounts");
        ////    }

        ////    if (SchoolWingExists(id.Value) == true)
        ////    {
        ////        var Action = studentAggregateRepository.DeleteSchoolWing(id.Value);
        ////    }
        ////    return RedirectToAction(nameof(SchoolWing)).WithSuccess("success", "Deleted successfully");
        ////}

        ////private bool SchoolWingExists(long id)
        ////{
        ////    if (studentAggregateRepository.GetSchoolWingById(id) != null)
        ////    {
        ////        return true;
        ////    }
        ////    else
        ////    {
        ////        return false;
        ////    }
        ////}
        //#endregion


        #region Designation
        //public IActionResult Designation()
        //{
        //    CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //                       // long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    if (commonMethods.checkaccessavailable("Designation", accessId, "List", "Master", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }
        //    var Designation = designationRepository.GetAllDesignation().ToList();
        //    //var designationlevel = designationLevelRepository.GetAllDesignationLevel().ToList();
        //    var department = departmentRepository.GetAllDepartment().ToList();

        //    var res = (from a in Designation
        //                   // join b in designationlevel on a.DesignationLevelID equals b.ID
        //               select new Designationcls
        //               {
        //                   ID = a.ID,
        //                   Name = a.Name,
        //                   //DesignationLevelID = a.DesignationLevelID,
        //                   // DesignationLevelName = b.Name,
        //                   ModifiedDate = a.ModifiedDate,
        //                   DepartmentID = a.DepartmentID,
        //                   DepartmentName = commonMethods.GetDepartmentbyparentid(a.DepartmentID, department.Where(c => c.ID == a.DepartmentID).FirstOrDefault().OrganizationID)
        //               }).ToList();
        //    return View(res);
        //}
        public IActionResult Designation(long? deptId,string type)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Designation", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            ViewBag.Type = type;
            ViewBag.DeptId = deptId;

            var Designation = designationRepository.GetDesignationListByDepartmentId(deptId.Value).ToList(); 
            //var designationlevel = designationLevelRepository.GetAllDesignationLevel().ToList();
            var department = departmentRepository.GetAllDepartment().ToList();

            Department dept = departmentRepository.GetDepartmentById(deptId.Value);
         
            if (dept.ParentID!=null&&dept.ParentID>0)
            {
                Department parentDept = departmentRepository.GetDepartmentById(dept.ParentID.Value);
                ViewBag.Dept = parentDept.Name+">>"+dept.Name;
            }
            else
            {
                ViewBag.Dept = dept.Name;
            }

           
            ViewBag.DeptId = dept.ID;
            if (type == "Group")
            {

           
                Group g = groupRepository.GetGroupById(dept.GroupID);

                ViewBag.Group = g.Name;
                ViewBag.GroupId = g.ID;

            }
            else
            {
                Organization g = organizationRepository.GetOrganizationById(dept.OrganizationID);
                //if (g.GroupID != 0)
                //{
                Group o = groupRepository.GetGroupById(g.GroupID);
                //}
                ViewBag.Organization = g.Name;
                ViewBag.OrganizationId = g.ID;

                ViewBag.Group = o.Name;
                ViewBag.GroupId = o.ID;



            }


            var res = (from a in Designation
                           // join b in designationlevel on a.DesignationLevelID equals b.ID
                       select new Designationcls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           //DesignationLevelID = a.DesignationLevelID,
                           // DesignationLevelName = b.Name,
                           ModifiedDate = a.ModifiedDate,
                           DepartmentID = a.DepartmentID,
                           DepartmentName = commonMethods.
                           GetDepartmentbyparentid(a.DepartmentID, department.Where(c => c.ID == a.DepartmentID).FirstOrDefault().OrganizationID)
                       }).ToList();
            return View(res);
        }
        public IActionResult CreateOrEditDesignation(long? id,long? deptId,string type)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            Designationcls od = new Designationcls();
            //od.departments = commonMethods.GetDepartments();
            //od.designationLevels = (from a in designationLevelRepository.GetAllDesignationLevel().ToList()
            //                        select new CommonMasterModel
            //                        {
            //                            ID = a.ID,
            //                            Name = a.Name
            //                        }).ToList();


            Department dept = departmentRepository.GetDepartmentById(deptId.Value);

            if (dept.ParentID != null && dept.ParentID > 0)
            {
                Department parentDept = departmentRepository.GetDepartmentById(dept.ParentID.Value);
                ViewBag.Dept = parentDept.Name + ">>" + dept.Name;
            }
            else
            {
                ViewBag.Dept = dept.Name;
            }


            ViewBag.DeptId = dept.ID;
            ViewBag.Type = type;
            if (type == "Group")
            {


                Group g = groupRepository.GetGroupById(dept.GroupID);

                ViewBag.Group = g.Name;
                ViewBag.GroupId = g.ID;

            }
            else
            {
                Organization g = organizationRepository.GetOrganizationById(dept.OrganizationID);
                //if (g.GroupID != 0)
                //{
                Group o = groupRepository.GetGroupById(g.GroupID);
                //}
                ViewBag.Organization = g.Name;
                ViewBag.OrganizationId = g.ID;

                ViewBag.Group = o.Name;
                ViewBag.GroupId = o.ID;



            }


            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Designation", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.DesignationLevelID = 0;
               
                od.DepartmentID = deptId.Value;
            }
            else
            {
                if (commonMethods.checkaccessavailable("Designation", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var designation = designationRepository.GetDesignationById(id.Value);
                od.ID = designation.ID;
                od.Name = designation.Name;
                //od.DesignationLevelID = designation.DesignationLevelID;
                od.DepartmentID = designation.DepartmentID;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateDesignation([Bind("ID,Name,DesignationLevelID,DepartmentID")] Designation designation)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;

            if (ModelState.IsValid)
            {
                Department dept = departmentRepository.GetDepartmentById(designation.DepartmentID);
                string t = "";
                designation.ModifiedId = userId;
                designation.Active = true;
                if (designation.ID == 0)
                {
                    designation.InsertedId = userId;
                    designation.Status = EntityStatus.ACTIVE;
                    designationRepository.CreateDesignation(designation);


                   
                   
                    if(dept.OrganizationID!=null&& dept.OrganizationID>0)
                    {
                        t = "Organization";
                    }
                    else
                    {
                        t = "Group";

                    }

                    return RedirectToAction(nameof(Designation),new { deptId= dept.ID, type=t}).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    designationRepository.UpdateDesignation(designation);
                    return RedirectToAction(nameof(Designation), new { deptId = dept.ID, type = t }).WithSuccess("success", "Updated successfully");
                }



            }
            return View(designation).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteDesignation")]
        public async Task<IActionResult> DesignationDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Designation", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            long deptId = designationRepository.GetDesignationById(id.Value).DepartmentID;
            if (deptId>0)
            {
                var Section = designationRepository.DeleteDesignation(id.Value);
                
            }

            Department dept = departmentRepository.GetDepartmentById(deptId);
            string type = "";
            if (dept.OrganizationID != null && dept.OrganizationID > 0)
            {
                type = "Organization";
            }
            else
            {
                type = "Group";

            }

            return RedirectToAction(nameof(Designation), new { deptId = dept.ID, type = type }).WithSuccess("success", "Deleted successfully");
        }

        private bool DesignationExists(long id)
        {
            if (designationRepository.GetDesignationById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public JsonResult GetDesignationsByDepartmentId(long deptId)
        {
            var designations = designationRepository.GetAllDesignation().
                Where(a => a.DepartmentID
            == deptId && a.Active == true).ToList();
            return new JsonResult(designations);
        }
        #endregion
        #region City
        public IActionResult City()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("City", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var City = cityRepository.GetAllCity().AsQueryable();
            var state = stateRepository.GetAllState().AsQueryable();
            var country = countryRepository.GetAllCountries().AsQueryable();

            var res = (from a in City
                       join b in state on a.StateID equals b.ID
                       join c in country on a.CountryID equals c.ID
                       select new Citycls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           StateID = a.StateID,
                           StateName = b.Name,
                           ModifiedDate = a.ModifiedDate,
                           CountryID = a.CountryID,
                           CountryName = c.Name
                       })
                   .ToList();


            return View(res);
        }


        //public JsonResult CustomServerSideSearchAction(DataTableAjaxPostModel model)
        //{
        //    // action inside a standard controller
        //    int filteredResultsCount;
        //    int totalResultsCount;
        //    var res = YourCustomSearchFunc(model, out filteredResultsCount, out totalResultsCount);

        //    var result = new List<YourCustomSearchClass>(res.Count);
        //    foreach (var s in res)
        //    {
        //        // simple remapping adding extra info to found dataset
        //        result.Add(new YourCustomSearchClass
        //        {
        //            EmployerId = User.ClaimsUserId(),
        //            Id = s.Id,
        //            Pin = s.Pin,
        //            Firstname = s.Firstname,
        //            Lastname = s.Lastname,
        //            RegistrationStatusId = DoSomethingToGetIt(s.Id),
        //            Address3 = s.Address3,
        //            Address4 = s.Address4
        //        });
        //    };

        //    return Json(new
        //    {
        //        // this is what datatables wants sending back
        //        draw = model.draw,
        //        recordsTotal = totalResultsCount,
        //        recordsFiltered = filteredResultsCount,
        //        data = result
        //    });
        //}

        [HttpPost]
        public JsonResult GetCities(DataTableAjaxPostModel model)
        {
            var City = cityRepository.GetAllCity().AsQueryable();
            var state = stateRepository.GetAllState().AsQueryable();
            var country = countryRepository.GetAllCountries().AsQueryable();

            //var res = (from a in City
            //           join b in state on a.StateID equals b.ID
            //           join c in country on a.CountryID equals c.ID
            //           select new Citycls
            //           {
            //               ID = a.ID,
            //               Name = a.Name,
            //               StateID = a.StateID,
            //               StateName = b.Name,
            //               ModifiedDate = a.ModifiedDate,
            //               CountryID = a.CountryID,
            //               CountryName = c.Name
            //           }).Skip(model.start)
            //       .Take(model.length)
            //       .ToList();


            var res = (from a in City
                       join b in state on a.StateID equals b.ID
                       join c in country on a.CountryID equals c.ID
                       select new CityViewModel
                       {
                          ID=a.ID,
                           Name = a.Name,
                         
                           State = b.Name,
                           Modified = a.ModifiedDate,
                         
                           Country= c.Name
                       }).Skip(model.start)
                  .Take(model.length)
                  .ToList();


            return Json(new
            {
                // this is what datatables wants sending back
                draw = model.draw,
                recordsTotal = res.Count,
                recordsFiltered = res.Count,
                data = res
            });

           // return Json(res);

        }

        public IActionResult CreateOrEditCity(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            Citycls od = new Citycls();

            od.Countries = (from a in countryRepository.GetAllCountries().ToList()
                            select new CommonMasterModel
                            {
                                ID = a.ID,
                                Name = a.Name
                            }).ToList();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("City", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.StateID = 0;
                od.CountryID = od.Countries.FirstOrDefault().ID;
                od.States = (from a in stateRepository.GetAllState().ToList()
                             where a.CountryID == od.Countries.FirstOrDefault().ID
                             select new CommonMasterModel
                             {
                                 ID = a.ID,
                                 Name = a.Name
                             }).ToList();
            }
            else
            {
                if (commonMethods.checkaccessavailable("City", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var city = cityRepository.GetCityById(id.Value);
                od.ID = city.ID;
                od.Name = city.Name;
                od.States = (from a in stateRepository.GetAllState().ToList()
                             where a.CountryID == city.CountryID
                             select new CommonMasterModel
                             {
                                 ID = a.ID,
                                 Name = a.Name
                             }).ToList();
                od.StateID = city.StateID;
                od.CountryID = city.CountryID;

            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateCity([Bind("ID,Name,StateID,CountryID")] City city)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                city.ModifiedId = userId;
                city.Active = true;
                if (city.ID == 0)
                {
                    city.InsertedId = userId;
                    city.Status = EntityStatus.ACTIVE;
                    cityRepository.CreateCity(city);
                    return RedirectToAction(nameof(City)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    cityRepository.UpdateCity(city);
                    return RedirectToAction(nameof(City)).WithSuccess("success", "Updated successfully");
                }



            }
            return View(city).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteCity")]
        public async Task<IActionResult> CityDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("City", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (CityExists(id.Value) == true)
            {
                var Section = cityRepository.DeleteCity(id.Value);
            }
            return RedirectToAction(nameof(City)).WithSuccess("success", "Deleted successfully");
        }
        public async Task<JsonResult> GetStateByCountryId(long id)
        {
            var res = (from a in stateRepository.GetAllState().ToList()
                       where a.CountryID == id
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        private bool CityExists(long id)
        {
            if (cityRepository.GetCityById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region CityExcel
        public IActionResult downloadCitySample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("City", accessId, "Upload", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();

            DataTable countrydt = new DataTable();
            countrydt.Columns.Add("CountryName");
            var country = countryRepository.GetAllCountries();
            foreach (var a in country)
            {
                DataRow dr = countrydt.NewRow();
                dr["CountryName"] = a.Name;
                countrydt.Rows.Add(dr);
            }
            countrydt.TableName = "Country";

            DataTable statedt = new DataTable();
            statedt.Columns.Add("StateName");
            var state = stateRepository.GetAllState();
            foreach (var b in state)
            {
                DataRow dr = statedt.NewRow();
                dr["StateName"] = b.Name;
                statedt.Rows.Add(dr);
            }
            statedt.TableName = "State";

            int lastCellNo1 = countrydt.Rows.Count + 1;
            int lastCellNo2 = statedt.Rows.Count + 1;

            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(countrydt);
            oWB.AddWorksheet(statedt);
            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Country");
            validationTable.Columns.Add("State");
            validationTable.Columns.Add("Name");
            validationTable.TableName = "City_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(2).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet1.Hide();
            worksheet2.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", $"City.xlsx");
        }
        public IActionResult DownloadCity()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            // long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            //if (commonMethods.checkaccessavailable("City", accessId, "Download", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable countrydt = new DataTable();
            //countrydt.Columns.Add("Sl No");
            countrydt.Columns.Add("Country Name");
            countrydt.Columns.Add("State Name");
            countrydt.Columns.Add("City Name");
            //  countrydt.Columns.Add("Inserted On");
            // countrydt.Columns.Add("Modified On");
            var state = stateRepository.GetAllState();
            var country = countryRepository.GetAllCountries();
            var city = cityRepository.GetAllCity();
            var ress = (from a in city
                        join b in state on a.StateID equals b.ID
                        join c in country on b.CountryID equals c.ID
                        select a).ToList();
            int i = 0;
            foreach (var a in ress)
            {
                DataRow dr = countrydt.NewRow();
                // dr["Sl No"] = ++i;
                dr["Country Name"] = country.Where(m => m.ID == a.CountryID).FirstOrDefault().Name;
                dr["State Name"] = state.Where(m => m.ID == a.StateID).FirstOrDefault().Name;
                dr["City Name"] = a.Name;
                //dr["Inserted On"] = a.InsertedDate.ToString("dd-MMM-yyyy");
                // dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                countrydt.Rows.Add(dr);
            }
            countrydt.TableName = "City";
            oWB.AddWorksheet(countrydt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"City.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadCity(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //  long staffTypeID = HttpContext.Session.GetInt32("staffTypeID").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("City", accessId, "Upload", "Master", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var city = cityRepository.GetAllCity().ToList();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        City cities = new City();
                        string Countryname = firstWorksheet.Cells[i, 1].Value.ToString();
                        string Statename = firstWorksheet.Cells[i, 2].Value.ToString();
                        string Cityname = firstWorksheet.Cells[i, 3].Value.ToString();
                        var countryid = countryRepository.GetAllCountries().Where(m => m.Name == Countryname).FirstOrDefault().ID;
                        var stateid = stateRepository.GetAllState().Where(m => m.Name == Statename).FirstOrDefault().ID;
                        if (city.Where(a => a.Name == Cityname && a.CountryID == countryid && a.StateID == stateid).FirstOrDefault() == null)
                        {
                            cities.Name = Cityname;
                            cities.ModifiedId = userId;
                            cities.CountryID = countryid;
                            cities.StateID = stateid;
                            cities.Active = true;
                            cities.InsertedId = userId;
                            cities.Status = EntityStatus.ACTIVE;
                            cityRepository.CreateCity(cities);
                        }
                    }
                }
            }
            return RedirectToAction("City", "Master").WithSuccess("success", "Excel uploaded successfully"); ;
        }
        #endregion



        #region Organisation

        [ActionName("ChangeOrganizationStatus")]
        public async Task<IActionResult> OrganizationStatusChange(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Organization", accessId, "ChangeStatus", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            Organization group = organizationRepository.GetOrganizationById(id.Value);

            if (group != null)
            {

                if (group.Active)
                {
                    group.Active = false;
                    group.Status = EntityStatus.INACTIVE;
                    group.ModifiedDate = DateTime.Now;
                    group.ModifiedId = userId;

                }
                else
                {
                    group.Active = true;
                    group.Status = EntityStatus.ACTIVE;
                    group.ModifiedDate = DateTime.Now;
                    group.ModifiedId = userId;
                }

                var country = organizationRepository.UpdateOrganization(group);
            }
            return RedirectToAction(nameof(Organisation));
        }


        public IActionResult Organisation(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Organisation", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var Organisation = id != null ? organizationRepository.
                GetAllOrganizationByGroupId(id.Value).ToList() :
                organizationRepository.GetAllOrganization().ToList();
            var groups = groupRepository.GetAllGroup().ToList();
            var organisationtype = organizationTypeRepository.GetAllOrganizationType().ToList();



            var res = (from a in Organisation
                       join b in groups on a.GroupID equals b.ID
                       join c in organisationtype on a.OrganizationTypeID equals c.ID
                       select new Organisationcls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           GroupID = a.GroupID,
                           GroupName = b.Name,
                           ModifiedDate = a.ModifiedDate,
                           OrganizationTypeID = a.OrganizationTypeID,
                           OrganizationTypeName = c.Name,
                           DepartmentCount = departmentRepository.GetDepartmentCountByOrganizationId(a.ID),
                           Status = a.Status,
                           Active = a.Active,
                           ModifiedName = employeeRepository.GetEmployeeFullNameById(a.ModifiedId)
                       }).ToList();

            if (id != null)
            {
                Group group = groupRepository.GetGroupById(id.Value);

                ViewBag.Group = group.Name;
                ViewBag.GroupId = id;
                ViewBag.GroupStatus = group.Active;

            }
            else
            {
                ViewBag.GroupId = 0;
            }

            return View(res);
        }

        public IActionResult CreateOrEditOrganisation(long? id, long GroupId)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            var groups = GroupId == null ? groupRepository.GetAllGroup().ToList() :
                groupRepository.GetAllGroup().Where(a => a.ID == GroupId).ToList();



            Organisationcls od = new Organisationcls();
            //od.groups = (from a in groups.ToList()
            //             select new CommonMasterModel
            //             {
            //                 ID = a.ID,
            //                 Name = a.Name
            //             }).ToList();
            od.OrganizationTypes = (from a in organizationTypeRepository.GetAllOrganizationType().ToList()
                                    select new CommonMasterModel
                                    {
                                        ID = a.ID,
                                        Name = a.Name
                                    }).ToList();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Organisation", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
                od.GroupID = GroupId;
                od.OrganizationTypeID = 0;


            }
            else
            {
                if (commonMethods.checkaccessavailable("Organisation", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var organization = organizationRepository.GetOrganizationById(id.Value);
                od.ID = organization.ID;
                od.Name = organization.Name;
                od.GroupID = organization.GroupID;
                od.OrganizationTypeID = organization.OrganizationTypeID;
            }

            if (GroupId != null)
            {
                Group group = groupRepository.GetGroupById(GroupId);

                ViewBag.Group = group.Name;
                ViewBag.GroupId = GroupId;
            }
            else
            {
                ViewBag.GroupId = 0;
            }


            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateOrganisation([Bind("ID,Name,GroupID,OrganizationTypeID")] Organization organization)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                organization.ModifiedId = userId;
                organization.Active = true;
                if (organization.ID == 0)
                {
                    organization.InsertedId = userId;
                    organization.Status = EntityStatus.ACTIVE;


                    organizationRepository.CreateOrganization(organization);
                    return RedirectToAction(nameof(Organisation), new { id = organization.GroupID }).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    organizationRepository.UpdateOrganization(organization);
                    return RedirectToAction(nameof(Organisation), new { id = organization.GroupID }).WithSuccess("success", "Updated successfully");
                }



            }
            return View(organization).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteOrganisation")]
        public async Task<IActionResult> OrganisationDeleteConfirmed(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Organisation", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (OrganisationExists(id.Value) == true)
            {
                var Section = organizationRepository.DeleteOrganization(id.Value);
            }
            return RedirectToAction(nameof(Organisation)).WithSuccess("success", "Deleted successfully");
        }

        private bool OrganisationExists(long id)
        {
            if (organizationRepository.GetOrganizationById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Student Access
        public IActionResult StudentAccess()
        {
            CheckLoginStatus();
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Student Access", accessId, "Access", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            string studentId = HttpContext.Session.GetString("ddlstudentid");
            long stdId = Convert.ToInt64(studentId);
            // var parent = studentAggregateRepository.GetParentById(userId);
            var studentclass = commonsql.Get_view_All_Academic_Students().Where(a => a.StudentId == stdId && a.IsAvailable == true).FirstOrDefault();
            ViewBag.studentid = stdId;
            return View(commonMethods.GetStudentModuleAccess(5, studentclass.BoardStandardId, stdId));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentModuleSubModuleDetails(EmployeeModuleSubModuleAccess employeeModuleSubModuleAccess, long studentid)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                foreach (var a in employeeModuleSubModuleAccess.modules)
                {
                    if (a.subModules != null)
                    {
                        foreach (var b in a.subModules)
                        {
                            if (b.actionSubModules != null)
                            {
                                foreach (var c in b.actionSubModules)
                                {
                                    if (c.IsRequired == true && c.ActionAccessId == 0)
                                    {
                                        ActionStudent action = new ActionStudent();
                                        action.ActionID = c.ID;
                                        action.Active = true;
                                        action.InsertedId = userId;
                                        action.ModifiedId = userId;
                                        action.ModuleID = a.ID;
                                        action.Status = EntityStatus.ACTIVE;
                                        action.SubModuleID = b.ID;
                                        action.RoleID = 5;
                                        action.StudentID = studentid;
                                        actionAccessRepository.CreateActionStudent(action);
                                    }
                                    else if (c.ActionAccessId != 0)
                                    {
                                        ActionStudent actionAccess = actionAccessRepository.GetActionStudentById(c.ActionAccessId);
                                        if (actionAccess != null)
                                        {
                                            if (c.IsRequired == false)
                                            {
                                                actionAccessRepository.DeleteActionStudent(actionAccess.ID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return RedirectToAction(nameof(StudentAccess)).WithSuccess("success", "Saved successfully");
            }
            return RedirectToAction(nameof(StudentAccess)).WithSuccess("success", "Saved successfully");
        }

        #endregion

        

        #region DepartmentLead
        public IActionResult DepartmentLead()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Department Lead", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var dept = departmentRepository.GetAllDepartment().ToList();
            var emp = employeeRepository.GetAllEmployee().ToList();
            var deptlead = departmentLeadRepository.GetAllDepartmentLead().ToList();

            var res = (from a in deptlead
                       join b in dept on a.DepartmentID equals b.ID
                       join c in emp on a.EmployeeID equals c.ID
                       select new DepartmentLeadcls
                       {
                           ID = a.ID,
                           EmployeeName = c.FirstName + " " + c.LastName,
                           ModifiedDate = a.ModifiedDate,
                           DepartmentName = b.Name,
                           IsAvailable = a.IsAvailable,
                           
                       }).ToList();
            return View(res);
        }

        public IActionResult CreateOrEditDepartmentLead(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            DepartmentLeadcls od = new DepartmentLeadcls();

            var emp = employeeRepository.GetAllEmployee().ToList();
            var empdesignation = employeeDesignationRepository.GetAllEmployeeDesignations().ToList();
            var designation = designationRepository.GetAllDesignation().ToList();



            od.departments = (from a in departmentRepository.GetAllDepartment().ToList()
                              select new Department
                              {
                                  ID = a.ID,
                                  Name = a.Name
                              }).ToList();

            //od.employees = (from b in employeeRepository.GetAllEmployee().ToList()
            //                select new CommonMasterModel
            //                {
            //                    ID = b.ID,
            //                    Name = b.FirstName + " " + b.LastName,
            //                }
            //              ).ToList();

            if (id == null)
            {
                if (commonMethods.checkaccessavailable("Department Lead", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;

                od.DepartmentID = 0;
                od.EmployeeID = 0;

            }
            else
            {
                if (commonMethods.checkaccessavailable("Department Lead", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                var dept = departmentRepository.GetAllDepartment().ToList();

                ViewBag.status = "Update";
                var city = departmentLeadRepository.GetByDepartmentLeadId(id.Value);


                od.employees = (from a in emp
                                join b in empdesignation on a.ID equals b.EmployeeID
                                join c in designation on b.DesignationID equals c.ID
                                join d in dept on c.DepartmentID equals d.ID
                                where d.ID == city.DepartmentID
                                select new CommonMasterModel
                                {
                                    ID = a.ID,
                                    Name = a.FirstName + " " + a.LastName,
                                }
                          ).Distinct().ToList();
                od.ID = city.ID;
                od.DepartmentID = city.DepartmentID;
                od.EmployeeID = city.EmployeeID;
                od.IsAvailable = city.IsAvailable;

            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateDepartmentLead([Bind("ID,DepartmentID,EmployeeID,IsAvailable")] DepartmentLead deptlead)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                deptlead.ModifiedId = userId;
                deptlead.Active = true;
                if (deptlead.ID == 0)
                {
                    deptlead.InsertedId = userId;
                    deptlead.Status = EntityStatus.ACTIVE;
                    departmentLeadRepository.CreateDepartmentLead(deptlead);
                    return RedirectToAction(nameof(DepartmentLead)).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    departmentLeadRepository.UpdateDepartmentLead(deptlead);
                    return RedirectToAction(nameof(DepartmentLead)).WithSuccess("success", "Updated successfully");
                }



            }
            return View(deptlead).WithDanger("error", "Not Saved");
        }

        [ActionName("DeleteDepartmentLead")]
        public async Task<IActionResult> DepartmentLeadDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Department Lead", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (DepartmentLeadExists(id.Value) == true)
            {
                var Section = departmentLeadRepository.DeleteDepartmentLead(id.Value);
            }
            return RedirectToAction(nameof(DepartmentLead)).WithSuccess("success", "Deleted successfully");
        }
        public List<Employee> GetEmpByDeptId(long id)
        {
            var emp = employeeRepository.GetAllEmployee().ToList();
            var empdesignation = employeeDesignationRepository.GetAllEmployeeDesignations().ToList();
            var designation = designationRepository.GetAllDesignation().ToList();
            var dept = departmentRepository.GetAllDepartment().ToList();

            var res = (from a in emp
                       join b in empdesignation on a.ID equals b.EmployeeID
                       join c in designation on b.DesignationID equals c.ID
                       join d in dept on c.DepartmentID equals d.ID
                       where d.ID == id
                       select a).Distinct().ToList();
            return res;
        }
        private bool DepartmentLeadExists(long id)
        {
            if (departmentLeadRepository.GetByDepartmentLeadId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        #region

        public IActionResult GroupTreeView()
        {
            return View();
        }



        [HttpGet]
        public JsonResult GetGroupwiseTreeData()
        {

            TreeViewModel model = new TreeViewModel();

            TreeViewChart chart = new TreeViewChart();
            chart.container = "#tree-simple";
            chart.levelSeparation = 45;
            chart.rootOrientation = "WEST";
            chart.nodeAlign = "BOTTOM";
            TreeViewConnector con = new TreeViewConnector();
            con.type = "step";

            chart.connectors = con;

            NodeStructure nodeStructure = new NodeStructure();
            NodeStructureText nText = new NodeStructureText();
            nText.name = "Groups";
            nodeStructure.text = nText;

            List<NodeStructure> childObjs =
                (from a in groupRepository.GetAllGroup().ToList()
                 select new NodeStructure
                 {
                     text = new NodeStructureText(a.Name),
                     children = GetOrganizationNodes(a.ID)

                 }).ToList();

            nodeStructure.children = childObjs;
            model.chart = chart;

            model.nodeStructure = nodeStructure;

            return new JsonResult(model);

        }


        public List<NodeStructure> GetOrganizationNodes(long groupId)
        {
            List<NodeStructure> orgNodes =
               (from a in organizationRepository.GetAllOrganizationByGroupId(groupId).ToList()
                select new NodeStructure
                {
                    text = new NodeStructureText(a.Name),
                    children = GetDepartmentNodesByOrganizationId(a.ID),


                }).ToList();



            List<NodeStructure> deptNodes = GetDepartmentNodesByGroupId(groupId);

            orgNodes.AddRange(deptNodes);

            return orgNodes;
        }

        public List<NodeStructure> GetDepartmentNodesByGroupId(long groupId)
        {
            List<NodeStructure> deptNodes =
               (from a in departmentRepository.GetDepartmentListByGroupId(groupId).Where(a => a.ParentID == null || a.ParentID == 0).ToList()
                select new NodeStructure
                {
                    text = new NodeStructureText(a.Name),
                    children = GetSubDepartmentNodesByGroupIdDeptId(groupId, a.ID),


                }).ToList();

            return deptNodes;
        }
        public List<NodeStructure> GetDepartmentNodesByOrganizationId(long orgId)
        {
            return
               (from a in departmentRepository.GetDepartmentListByOrganizationId(orgId).Where(a => a.ParentID == null || a.ParentID == 0).ToList()
                select new NodeStructure
                {
                    text = new NodeStructureText(a.Name),
                    children = GetSubDepartmentNodesByDeptId(orgId, a.ID),


                }).ToList();
        }

        public List<NodeStructure> GetSubDepartmentNodesByDeptId(long orgId, long deptId)
        {
            return
               (from a in departmentRepository.
                GetDepartmentListByOrganizationId(orgId).Where(a => a.ParentID == deptId).ToList()
                select new NodeStructure
                {
                    text = new NodeStructureText(a.Name),
                    children = GetSubDepartmentNodesByDeptId(orgId, a.ID),

                }).ToList();
        }

        public List<NodeStructure> GetSubDepartmentNodesByGroupIdDeptId(long groupId, long deptId)
        {
            return
               (from a in departmentRepository.
                GetDepartmentListByGroupId(groupId).Where(a => a.ParentID == deptId).ToList()
                select new NodeStructure
                {
                    text = new NodeStructureText(a.Name),
                    children = GetSubDepartmentNodesByGroupIdDeptId(groupId, a.ID),



                }).ToList();
        }


        public TreeViewConnector GetConnector()
        {

            TreeViewConnector con = new TreeViewConnector();

            TreeViewStyle style = new TreeViewStyle("#8080FF");
            con.style = style;
            return con;
        }
        #endregion

        #region----------------contacttype-----------------------
        public IActionResult ContactType()
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("ContactType", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }           
            var res = contactTypeRepository.ListAllAsyncIncludeAll().Result.ToList();

            return View(res);



        }
        public IActionResult CreateOrEditContactType(long? id)
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;

            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (commonMethods.checkaccessavailable("ContactType", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewData["ALLSTATUS"] = new List<SelectListItem> { new SelectListItem { Text = "ACTIVE", Value = "ACTIVE" } };
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";

            }
            else
            {
                if (commonMethods.checkaccessavailable("ContactType", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Update";
                var type = contactTypeRepository.ListAllAsyncIncludeAll().Result.Where(a=>a.ID==id).FirstOrDefault();
                od.ID = type.ID;
                od.Name = type.Name;
                od.Status = type.Status;
                GetStatusList();
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateContactType([Bind("ID,Name")] ContactType type)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                type.ModifiedId = userId;
                type.Active = true;
                if (type.ID == 0)
                {
                    type.ModifiedDate = DateTime.Now;
                    type.InsertedDate = DateTime.Now;                  
                    type.InsertedId = userId;
                    type.IsAvailable = true;
                    type.Active = true;
                    type.Status = EntityStatus.ACTIVE;
                    await contactTypeRepository.AddAsync(type);
                   return RedirectToAction(nameof(ContactType)).WithSuccess("ContactType", "Saved successfully");
                }
                else
                {

                    type.ModifiedDate = DateTime.Now;                  
                    await contactTypeRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(ContactType)).WithSuccess("ContactType", "Updated successfully");
                }
            }
            return View(type).WithDanger("error", "Not Saved");
        }
        public async Task<IActionResult> DeleteContactType(int id)
        {
            ContactType type = contactTypeRepository.ListAllAsyncIncludeAll().Result.Where(a=>a.ID==id).FirstOrDefault();
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            await contactTypeRepository.UpdateAsync(type);

            return RedirectToAction(nameof(ContactType)).WithSuccess("ContactType", "Saved successfully");
        }
        public IActionResult DownloadContactTypeSample()
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            XLWorkbook oWB = new XLWorkbook();
            System.Data.DataTable validationTable = new System.Data.DataTable();

            validationTable.Columns.Add("Name");
            validationTable.TableName = "ContactType_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"ContactTypeSample.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadContactType(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            try
            {
                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            string Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            var sourcelist = contactTypeRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Name == Name && a.Active==true).FirstOrDefault();
                            if (sourcelist == null)
                            {
                                ContactType type = new ContactType();
                                type.Name = firstWorksheet.Cells[i, 1].Value.ToString();
                                type.Active = true;
                                type.InsertedId = userId;
                                type.InsertedDate = DateTime.Now;
                                type.ModifiedDate = DateTime.Now;
                                type.IsAvailable = true;
                                type.ModifiedId = userId;
                                type.Status = EntityStatus.ACTIVE;
                                contactTypeRepository.AddAsync(type);
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(ContactType)).WithSuccess("Contact Type", "Excel Uploaded Successfully"); ;
        }
        #endregion---------------------------------------------

        #region Otp Message & 

        public IActionResult OtpMessages()
        {
            var otp = otpMessageRepository.ListAllAsyncIncludeAll().Result;
            return View(otp);
        }

        //public IActionResult MasterPayment()
        //{
        //    var payment = 
        //}

        #endregion
    }
}