﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Models;
using OfficeOpenXml;

namespace Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IFileProvider fileProvider;
        private readonly IHostingEnvironment hostingEnvironment;



        private IEmployeeRepository employeeRepository;
        private IDepartmentRepository departmentRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private ICityRepository cityRepository;
        private IStateRepository stateRepository;
        private ICountryRepository countryRepository;
        private IDesignationLevelRepository designationLevelRepository;
        private IDesignationRepository designationRepository;
        private IGroupRepository groupRepository;
        private IOrganizationRepository organizationRepository;
        private IReligionTypeRepository religionTypeRepository;
        private IBankTypeRepository bankTypeRepository;
        private IEmployeeDocumentAttachmentRepository employeeDocumentAttachmentRepository;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IAddressRepository addressRepository;
        private IEmployeeAddressRepository employeeAddressRepository;
        private IEmployeeExperienceRepository employeeExperienceRepository;
        private IEmployeeBankAccountRepository employeeBankAccountRepository;
        private IBankRepository bankRepository;
        private IBankBranchRepository bankBranchRepository;
        private IEmployeeEducationRepository employeeEducationRepository;
        private IEducationQualificationRepository educationQualificationRepository;
        private IEducationQualificationTypeRepository educationQualificationTypeRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepository;
        private IAccessTypeRepository accessTypeRepository;
        private IAccessRepository accessRepository;
        private IActionAccessRepository actionAccessRepository;

        long userId = 0;
        private CommonMethods commonMethods;
        public EmployeeController(IFileProvider fileprovider, IHostingEnvironment env, CommonMethods commonMethod,
            IEmployeeRepository employeeRepo, IDepartmentRepository departmentRepo, IBloodGroupRepository bloodGroupRepo,
            INationalityTypeRepository nationalityTypeRepo, IAddressTypeRepository addressTypeRepo, ICityRepository cityRepo,
            IStateRepository stateRepo, ICountryRepository countryRepo, IDesignationLevelRepository designationLevelRepo, IDesignationRepository designationRepo,
            IGroupRepository groupRepo, IOrganizationRepository organizationRepo, IReligionTypeRepository religionTypeRepo, IEmployeeDesignationRepository employeeDesignationRepo,
            IAddressRepository addressRepo, IEmployeeAddressRepository employeeAddressRepo, IEmployeeExperienceRepository employeeExperienceRepo,
            IEmployeeBankAccountRepository employeeBankAccountRepo, IBankRepository bankRepo, IBankBranchRepository bankBranchRepo, IEmployeeEducationRepository employeeEducationRepo,
            IEducationQualificationRepository educationQualificationRepo, IEducationQualificationTypeRepository educationQualificationTypeRepo, IDocumentTypeRepository documentTypeRepo,
            IDocumentSubTypeRepository documentSubTypeRepo, IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepo, IAccessTypeRepository accessTypeRepo, IAccessRepository accessRepo,
            IActionAccessRepository actionAccessRepo, IBankTypeRepository bankTypeRepo, IEmployeeDocumentAttachmentRepository employeeDocumentAttachmentRepo)
        {
            fileProvider = fileprovider;
            hostingEnvironment = env;
            commonMethods = commonMethod;
            bankTypeRepository = bankTypeRepo;
            employeeRepository = employeeRepo;
            departmentRepository = departmentRepo;
            bloodGroupRepository = bloodGroupRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            addressTypeRepository = addressTypeRepo;
            cityRepository = cityRepo;
            stateRepository = stateRepo;
            countryRepository = countryRepo;
            designationLevelRepository = designationLevelRepo;
            designationRepository = designationRepo;
            groupRepository = groupRepo;
            organizationRepository = organizationRepo;
            religionTypeRepository = religionTypeRepo;
            employeeDesignationRepository = employeeDesignationRepo;
            addressRepository = addressRepo;
            employeeAddressRepository = employeeAddressRepo;
            employeeExperienceRepository = employeeExperienceRepo;
            employeeBankAccountRepository = employeeBankAccountRepo;
            bankRepository = bankRepo;
            bankBranchRepository = bankBranchRepo;
            employeeEducationRepository = employeeEducationRepo;
            educationQualificationRepository = educationQualificationRepo;
            educationQualificationTypeRepository = educationQualificationTypeRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            employeeRequiredDocumentRepository = employeeRequiredDocumentRepo;
            accessTypeRepository = accessTypeRepo;
            accessRepository = accessRepo;
            actionAccessRepository = actionAccessRepo;
            employeeDocumentAttachmentRepository = employeeDocumentAttachmentRepo;

        }

        #region EmployeeBulkUpload

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public IActionResult downloadEmployeeSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Employee", accessId, "Upload", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            XLWorkbook oWB = new XLWorkbook();
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Employee Code");
            validationTable.Columns.Add("First Name");
            validationTable.Columns.Add("Last Name");
            validationTable.Columns.Add("Mobile");
            validationTable.Columns.Add("Email Id");
            validationTable.TableName = "Employee_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Employee.xlsx");
        }
        public IActionResult DownloadEmployee()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee", accessId, "Download", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            moduledt.Columns.Add("Employee Code");
            moduledt.Columns.Add("First Name");
            moduledt.Columns.Add("Middle Name");
            moduledt.Columns.Add("Last Name");
            moduledt.Columns.Add("Gender");
            moduledt.Columns.Add("Date Of Birth");
            moduledt.Columns.Add("Blood Group");
            moduledt.Columns.Add("Mobile");
            moduledt.Columns.Add("Email Id");
         
            moduledt.Columns.Add("Join On");
         
            var employee = employeeRepository.GetAllEmployee();
            var bloodgroups = bloodGroupRepository.GetAllBloodGroup();
            var joiningdetails = employeeRepository.GetAllEmployeeTimeline();
            if (employee != null)
            {
                foreach (var a in employee)
                {
                    DataRow dr = moduledt.NewRow();
                    dr["Employee Code"] = a.EmpCode;
                    dr["First Name"] = a.FirstName;
                    dr["Middle Name"] = a.MiddleName;
                    dr["Last Name"] = a.LastName;
                    dr["Gender"] = a.Gender;
                    dr["Date Of Birth"] = a.DateOfBirth != null ? a.DateOfBirth.Value.ToString("dd-MMM-yyyy") : "";
                    dr["Blood Group"] = (a.BloodGroupID == null || a.BloodGroupID == 0) ? "" : bloodgroups.Where(m => m.ID == a.BloodGroupID).FirstOrDefault().Name;
                    dr["Mobile"] = a.PrimaryMobile;
                    dr["Email Id"] = a.EmailId;
                   // dr["Status"] = a.Isleft == true ? "Left" : "Current";
                    var jj = joiningdetails.Where(m => m.EmployeeId == a.ID && m.IsPrimary == true).FirstOrDefault();
                    dr["Join On"] = jj != null ? jj.JoiningOn.Value.ToString("dd-MMM-yyyy") : "";
                   // dr["Left On"] = jj != null ? (jj.LeftOn != null ? jj.LeftOn.Value.ToString("dd-MMM-yyyy") : "") : "";
                    moduledt.Rows.Add(dr);
                }
            }
            moduledt.TableName = "Employees";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Employees.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadEmployee(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {
                if (commonMethods.checkaccessavailable("Employee", accessId, "Upload", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                if (file != null)
                {

                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var request = employeeRepository.GetAllEmployee();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            Employee employee = new Employee();
                            string empcode = firstWorksheet.Cells[i, 1].Value.ToString();

                            if (request.Where(a => a.EmpCode == empcode).FirstOrDefault() == null)
                            {
                                employee.EmpCode = firstWorksheet.Cells[i, 1].Value.ToString();
                                employee.ModifiedId = userId;
                                employee.FirstName = firstWorksheet.Cells[i, 2].Value.ToString();

                                if (firstWorksheet.Cells[i, 3] != null && firstWorksheet.Cells[i, 3].Value != null)
                                {
                                    employee.MiddleName = firstWorksheet.Cells[i, 3].Value.ToString();

                                }


                                employee.LastName = firstWorksheet.Cells[i, 4].Value.ToString();
                                employee.Gender = firstWorksheet.Cells[i, 5].Value.ToString();


                                string dateTime = firstWorksheet.Cells[i, 6].Value.ToString();

                                employee.DateOfBirth = Convert.ToDateTime(dateTime);
                                employee.BloodGroupID = bloodGroupRepository.GetBloodGroupByName(
                                    firstWorksheet.Cells[i, 7].Value.ToString()).ID;
                                employee.PrimaryMobile = firstWorksheet.Cells[i, 8].Value.ToString();
                                employee.EmailId = firstWorksheet.Cells[i, 9].Value.ToString();
                                employee.Active = true;
                                employee.InsertedId = userId;
                                employee.Isleft = false;
                                employee.IsAvailable = true;
                                employee.ModifiedId = userId;
                                employee.Status = EntityStatus.ACTIVE;
                                long id = employeeRepository.CreateEmployee(employee);
                                if (id > 0)
                                {
                                    Access accesss = new Access();
                                    accesss.RoleID = 4;
                                    accesss.Username = employee.EmpCode;
                                    accesss.EmployeeID = id;
                                    accesss.Active = true;
                                    accesss.InsertedDate = DateTime.Now;
                                    accesss.InsertedId = userId;
                                    accesss.ModifiedDate = DateTime.Now;
                                    accesss.ModifiedId = userId;

                                    accessRepository.CreateAccess(accesss);
                                    if (id > 0 && firstWorksheet.Cells[i, 10].Value != null)
                                    {
                                        EmployeeTimeline det = new EmployeeTimeline();
                                        det.Status = EntityStatus.ACTIVE;
                                        det.EmployeeId = id;
                                        det.IsPrimary = true;
                                        det.IsAvailable = true;
                                        det.JoiningOn = Convert.ToDateTime(firstWorksheet.Cells[i, 10].Value.ToString());
                                        det.Active = true;
                                        det.InsertedDate = DateTime.Now;
                                        det.InsertedId = userId;
                                        det.ModifiedDate = DateTime.Now;
                                        det.ModifiedId = userId;

                                        employeeRepository.CreateEmployeeTimeline(det);



                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(Index)).WithSuccess("success", "Excel uploaded successfully"); ;
        }


        public IActionResult downloadEmployeeBankDetailsSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Employee", accessId, "Upload", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            moduledt.Columns.Add("Name");
            var banktypes = bankTypeRepository.GetAllBankType();
            foreach (var a in banktypes)
            {
                DataRow dr = moduledt.NewRow();
                dr["Name"] = a.Name;
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "Bank Type";
            int lastCellNo1 = moduledt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(moduledt);
            var worksheet1 = oWB.Worksheet(1);
            DataTable bankdt = new DataTable();
            bankdt.Columns.Add("Name");
            var banks = bankRepository.GetAllBank();
            foreach (var a in banks)
            {
                DataRow dr = bankdt.NewRow();
                dr["Name"] = a.Name;
                bankdt.Rows.Add(dr);
            }
            bankdt.TableName = "Bank";
            int lastCellNo2 = bankdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(bankdt);
            var worksheet2 = oWB.Worksheet(2);

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Employee Code");
            validationTable.Columns.Add("Bank Type");
            validationTable.Columns.Add("Bank Name");
            validationTable.Columns.Add("Branch Name");
            validationTable.Columns.Add("IFSC Code");
            validationTable.Columns.Add("Account Number");
            validationTable.Columns.Add("Account Holder Name");

            validationTable.TableName = "Employee_Bank_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(2).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(3).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet1.Hide();
            worksheet2.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Employee Bank Details.xlsx");
        }
        public IActionResult DownloadEmployeeBankDetails()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee", accessId, "Download", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            moduledt.Columns.Add("Employee Code");
            moduledt.Columns.Add("First Name");
            moduledt.Columns.Add("Last Name");
            moduledt.Columns.Add("Mobile");
            moduledt.Columns.Add("Email Id");
            moduledt.Columns.Add("Bank Type");
            moduledt.Columns.Add("Bank Name");
            moduledt.Columns.Add("Branch Name");
            moduledt.Columns.Add("IFSC Code");
            moduledt.Columns.Add("Account Number");
            moduledt.Columns.Add("Account Holder Name");
            var employee = employeeRepository.GetAllEmployee();
            var employeebank = employeeBankAccountRepository.GetAllEmployeeBankAccountes();
            var banktypes = bankTypeRepository.GetAllBankType();
            var banks = bankRepository.GetAllBank();
            if (employeebank != null)
            {
                foreach (var a in employeebank)
                {
                    DataRow dr = moduledt.NewRow();
                    var emp = employee.Where(m => m.ID == a.EmployeeID).FirstOrDefault();
                    dr["Employee Code"] = emp.EmpCode;
                    dr["First Name"] = emp.FirstName;
                    dr["Last Name"] = emp.LastName;
                    dr["Mobile"] = emp.PrimaryMobile;
                    dr["Email Id"] = emp.EmailId;

                    dr["Bank Type"] = banktypes.Where(m => m.ID == a.BankTypeID).FirstOrDefault().Name;
                    dr["Bank Name"] = banks.Where(m => m.ID == a.BankNameID).FirstOrDefault().Name;
                    dr["Branch Name"] = a.BankBranchName;

                    dr["IFSC Code"] = a.IfscCode;
                    dr["Account Number"] = a.AccountNumber;
                    dr["Account Holder Name"] = a.AccountHolderName;

                    moduledt.Rows.Add(dr);
                }
            }
            moduledt.TableName = "Employees_Bank_Details";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Employees_Bank_Details.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadEmployeeBankDetails(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;

            if (commonMethods.checkaccessavailable("Employee", accessId, "Upload", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[2];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var request = employeeRepository.GetAllEmployee();
                    var banktypes = bankTypeRepository.GetAllBankType();
                    var banks = bankRepository.GetAllBank();

                    for (int i = 2; i <= totalRows; i++)
                    {
                        string banktypename = firstWorksheet.Cells[i, 2].Value.ToString();
                        var banktypeid = bankTypeRepository.GetAllBankType().Where(m => m.Name == banktypename).FirstOrDefault().ID;

                        string bankname = firstWorksheet.Cells[i, 3].Value.ToString();
                        var bankid = bankRepository.GetAllBank().Where(m => m.Name == bankname).FirstOrDefault().ID;

                        EmployeeBankAccount employee = new EmployeeBankAccount();
                        string empcode = firstWorksheet.Cells[i, 1].Value.ToString();

                        if (request.Where(a => a.EmpCode == empcode).FirstOrDefault() != null)
                        {
                            employee.EmployeeID = request.Where(a => a.EmpCode == empcode).FirstOrDefault().ID;
                            employee.ModifiedId = userId;
                            employee.BankTypeID = banktypeid;
                            employee.BankNameID = bankid;


                            // employee.BankTypeID = banktypes.Where(m => m.ID == Convert.ToInt64(firstWorksheet.Cells[i, 2].Value.ToString())).FirstOrDefault().ID;
                            // employee.BankNameID = banks.Where(m => m.ID == Convert.ToInt64(firstWorksheet.Cells[i, 3].Value.ToString())).FirstOrDefault().ID;

                            employee.BankBranchName = firstWorksheet.Cells[i, 4].Value.ToString();
                            employee.IfscCode = firstWorksheet.Cells[i, 5].Value.ToString();
                            employee.AccountNumber = firstWorksheet.Cells[i, 6].Value.ToString();
                            employee.AccountHolderName = firstWorksheet.Cells[i, 7].Value.ToString();
                            employee.Active = true;
                            employee.InsertedId = userId;
                            employee.Status = EntityStatus.ACTIVE;
                            employeeBankAccountRepository.CreateEmployeeBankAccount(employee);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(Index)).WithSuccess("success", "Excel uploaded successfully"); ;
        }




        #endregion
        #region Employee

        public IActionResult Index(int? choosedepartmenttypes, long? organisation, long? departments, int? types)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee", accessId, "List", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.organisations = organizationRepository.GetAllOrganization();
            ViewBag.ctypeid = choosedepartmenttypes;
            ViewBag.orgid = organisation;
            ViewBag.deptid = departments;
            ViewBag.typeid = types;
            var deptswithemp = commonMethods.GetAllDeptwithEmployee();
            var res = commonMethods.GetAllEmployees().Where(a=>a.ID!=1).ToList();
            if (choosedepartmenttypes == 1)
            {
                res = (from a in res
                       join b in deptswithemp on a.ID equals b.EmployeeID
                       where b.GroupID != 0 && b.OrganizationID == 0
                       select a).Distinct().ToList();
            }
            else if (choosedepartmenttypes == 2)
            {
                res = (from a in res
                       join b in deptswithemp on a.ID equals b.EmployeeID
                       where b.GroupID == 0 && b.OrganizationID != 0
                       select a).Distinct().ToList();
            }
            if (organisation > 0)
            {
                res = (from a in res
                       join b in deptswithemp on a.ID equals b.EmployeeID
                       where b.OrganizationID == organisation
                       select a).Distinct().ToList();
            }
            if (departments > 0)
            {
                res = (from a in res
                       join b in deptswithemp on a.ID equals b.EmployeeID
                       where b.DepartmentID == departments
                       select a).Distinct().ToList();
            }
            if (types == 1)
            {
                res = (from a in res
                       join b in deptswithemp on a.ID equals b.EmployeeID
                       where a.IsLeft == false
                       select a).Distinct().ToList();
            }
            else if (types == 2)
            {
                res = (from a in res
                       join b in deptswithemp on a.ID equals b.EmployeeID
                       where a.IsLeft == true
                       select a).Distinct().ToList();
            }
            return View(res);
        }
        public IActionResult CreateOrEditEmployee(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            Employeecls employeecls = new Employeecls();
            employeecls.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
            employeecls.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
            employeecls.religionTypes = religionTypeRepository.GetAllReligionTypes();
            ViewBag.status = "Create";
            if (commonMethods.checkaccessavailable("Employee", accessId, "Edit Mobile", "HR", roleId) == true)
            {
                ViewBag.mobileaccess = true;
            }
            else
            {
                ViewBag.mobileaccess = false;
            }
            if (commonMethods.checkaccessavailable("Employee", accessId, "Edit Email", "HR", roleId) == true)
            {
                ViewBag.mailaccess = true;
            }
            else
            {
                ViewBag.mailaccess = false;
            }
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Employee", accessId, "Edit", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                var employeedetails = employeeRepository.GetEmployeeById(id.Value);
                employeecls.AlternativeMobile = employeedetails.AlternativeMobile;
                employeecls.BloodGroupID = employeedetails.BloodGroupID;
                employeecls.DateOfBirth = employeedetails.DateOfBirth != null ? employeedetails.DateOfBirth : null;
                employeecls.EmailId = employeedetails.EmailId;
                employeecls.EmpCode = employeedetails.EmpCode;
                employeecls.FirstName = employeedetails.FirstName;
                employeecls.Gender = employeedetails.Gender != null && employeedetails.Gender != "" ? employeedetails.Gender : "";
                employeecls.ID = employeedetails.ID;
                employeecls.LandlineNumber = employeedetails.LandlineNumber;
                employeecls.LastName = employeedetails.LastName;
                employeecls.profile = employeedetails.Image;
                employeecls.MiddleName = employeedetails.MiddleName;
                employeecls.NatiobalityID = employeedetails.NatiobalityID != null && employeedetails.NatiobalityID != 0 ? employeedetails.NatiobalityID : 0;
                employeecls.PrimaryMobile = employeedetails.PrimaryMobile;
                employeecls.ReligionID = employeedetails.ReligionID != null && employeedetails.ReligionID != 0 ? employeedetails.ReligionID : 0;
                var webPath = hostingEnvironment.WebRootPath;
                if (employeedetails.Image != null)
                {
                    string path = Path.Combine("/ODMImages/EmployeeProfile/", employeedetails.Image);
                    ViewBag.Profileimage = path;
                }
                else
                {
                    ViewBag.Profileimage = null;
                }
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("PERSONAL").ID;
                employeecls.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                where a.DocumentTypeID == documentTypeId
                                                select new documentTypesCls
                                                {
                                                    TypeId = a.DocumentTypeID,
                                                    ID = a.ID,
                                                    Name = a.Name,
                                                    IsRequired = commonMethods.employeeRequiredDocument(employeedetails.ID, a.DocumentTypeID, a.ID)
                                                }).ToList();
                ViewBag.status = "Update";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Employee", accessId, "Create", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                employeecls.AlternativeMobile = "";
                employeecls.BloodGroupID = 0;
                employeecls.DateOfBirth = null;
                employeecls.EmailId = "";
                employeecls.EmpCode = "";
                employeecls.FirstName = "";
                employeecls.Gender = null;
                employeecls.ID = 0;
                employeecls.LandlineNumber = "";
                employeecls.LastName = "";
                employeecls.MiddleName = "";
                employeecls.NatiobalityID = 0;
                employeecls.PrimaryMobile = "";
                employeecls.ReligionID = 0;
                ViewBag.Profileimage = null;
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("PERSONAL").ID;
                employeecls.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                where a.DocumentTypeID == documentTypeId
                                                select new documentTypesCls
                                                {
                                                    TypeId = a.DocumentTypeID,
                                                    ID = a.ID,
                                                    Name = a.Name,
                                                    IsRequired = false
                                                }).ToList();
                ViewBag.status = "Create";
            }

            return View(employeecls);
        }
        public JsonResult GetDepartmentsByGroupWise()
        {
            var ob = commonMethods.GetDepartments().Where(a => a.GroupID != 0).ToList();

            return Json(ob);
        }
        public JsonResult GetDepartmentsByOrganisationWise(long id)
        {
            var ob = commonMethods.GetDepartments().ToList();
            if (id > 0)
            {
                ob = ob.Where(a => a.organisationID == id).ToList();
            }
            return Json(ob);
        }

        public JsonResult GetEmpCode(string code, long id)
        {

            try
            {
                var res = employeeRepository.GetAllEmployee().Where(a => a.EmpCode == code).FirstOrDefault();
                if (id != 0)
                {
                    if (res.ID == id)
                    {
                        return Json(false);
                    }
                    else
                    {
                        return Json(true);
                    }
                }
                if (res == null)
                {
                    return Json(false);
                }
                else
                {
                    return Json(true);
                }
            }
            catch
            {
                return Json(false);
            }

        }


        public JsonResult GetPrimaryMobile(string mob, long id)
        {

            try
            {
                var res = employeeRepository.GetAllEmployee().Where(a => a.PrimaryMobile == mob).FirstOrDefault();
                if (id != 0)
                {
                    if (res.ID == id)
                    {
                        return Json(false);
                    }
                    else
                    {
                        return Json(true);
                    }
                }

                if (res == null)
                {
                    return Json(false);
                }
                else
                {
                    return Json(true);
                }
            }
            catch
            {
                return Json(false);
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeProfile([Bind("ID,EmpCode,FirstName,MiddleName,LastName,Gender,DateOfBirth," +
            "BloodGroupID,PrimaryMobile,AlternativeMobile,LandlineNumber,EmailId,NatiobalityID,ReligionID")] Employee employee,
           [FromForm(Name = "Upload")] IFormFile file, [Bind("documentTypesCls")]List<documentTypesCls> documentTypesCls)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            //if (ModelState.IsValid)
            //{
            //var empcode = employeeRepository.GetAllEmployee().Where(a => a.EmpCode == employee.EmpCode).FirstOrDefault();
            //var mobile = employeeRepository.GetAllEmployee().Where(a => a.PrimaryMobile == employee.PrimaryMobile).FirstOrDefault();

            //if (empcode == null && mobile == null)
            //{
            var emps = employeeRepository.GetAllEmployee();
            employee.ModifiedId = userId;
            employee.Active = true;

            String newFilename = null;
            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                 newFilename = employee.EmpCode + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\EmployeeProfile\" + newFilename);
                var pathToSave = newFilename;
                employee.Image = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                employee.Image = pathToSave;
            }
            if (employee.ID == 0)
            {
                var empcode = emps.Where(a => a.EmpCode == employee.EmpCode).FirstOrDefault();
                var mobile = emps.Where(a => a.PrimaryMobile == employee.PrimaryMobile).FirstOrDefault();

                if (empcode == null && mobile == null)
                {
                    employee.Isleft = false;
                    employee.InsertedId = userId;
                    employee.Status = EntityStatus.ACTIVE;

                    
                    long eid = employeeRepository.CreateEmployee(employee);
                    Access access = new Access();
                    access.EmployeeID = eid;
                    access.Active = true;
                    access.InsertedId = userId;
                    access.Password = "";
                    access.Username = employee.EmpCode;
                    access.RoleID = 4;
                    access.ModifiedId = userId;
                    accessRepository.CreateAccess(access);
                    foreach (var item in documentTypesCls)
                    {
                        if (item.IsRequired == true)
                        {
                            EmployeeRequiredDocument employeeRequired = new EmployeeRequiredDocument();
                            employeeRequired.Active = true;
                            employeeRequired.DocumentSubTypeID = item.ID;
                            employeeRequired.DocumentTypeID = item.TypeId;
                            employeeRequired.EmployeeID = eid;
                            employeeRequired.InsertedId = userId;
                            employeeRequired.IsApproved = false;
                            employeeRequired.IsMandatory = true;
                            employeeRequired.IsReject = false;
                            employeeRequired.IsVerified = false;
                            employeeRequired.ModifiedId = userId;
                            employeeRequired.RelatedID = eid;
                            employeeRequired.Status = EntityStatus.ACTIVE;
                            employeeRequired.VerifiedAccessID = 0;
                            employeeRequiredDocumentRepository.CreateEmployeeRequiredDocument(employeeRequired);
                        }
                    }

                    return RedirectToAction(nameof(ViewEmployeeDetails), new { id = eid }).WithSuccess("success", "Saved successfully"); ;
                }

            }
            else
            {
                var empcode = emps.Where(a => a.EmpCode == employee.EmpCode && a.ID != employee.ID).FirstOrDefault();
                var mobile = emps.Where(a => a.PrimaryMobile == employee.PrimaryMobile && a.ID != employee.ID).FirstOrDefault();

                if (empcode == null && mobile == null)
                {

                    employeeRepository.UpdateEmployee(employee);

                    foreach (var item in documentTypesCls)
                    {
                        EmployeeRequiredDocument employeeRequired = employeeRequiredDocumentRepository.GetAllEmployeeRequiredDocuments().
                            Where(a => a.DocumentTypeID == item.TypeId && a.DocumentSubTypeID == item.ID && a.EmployeeID == employee.ID).FirstOrDefault();
                        if (item.IsRequired == true)
                        {
                            if (employeeRequired == null)
                            {
                                employeeRequired = new EmployeeRequiredDocument();
                                employeeRequired.Active = true;
                                employeeRequired.DocumentSubTypeID = item.ID;
                                employeeRequired.DocumentTypeID = item.TypeId;
                                employeeRequired.EmployeeID = employee.ID;
                                employeeRequired.InsertedId = userId;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employee.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.CreateEmployeeRequiredDocument(employeeRequired);
                            }
                            else
                            {
                                employeeRequired.Active = true;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employee.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(employeeRequired);
                            }
                        }
                        else
                        {
                            if (employeeRequired != null)
                            {
                                employeeRequired.Active = false;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employee.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(employeeRequired);
                            }

                        }

                    }

                    return RedirectToAction(nameof(ViewEmployeeDetails), new { id = employee.ID }).WithSuccess("success", "Updated successfully");
                }

            }
            //}

            //else
            //{
            //    return RedirectToAction(nameof(CreateOrEditEmployee));
            //}

            //}
            return View(employee).WithDanger("error", "Not Saved"); ;
        }



        [ActionName("DeleteEmployee")]
        public async Task<IActionResult> EmployeeDeleteConfirmed(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee", accessId, "Delete", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var employeeAddress = employeeRepository.DeleteEmployee(id);
            return RedirectToAction(nameof(Index)).WithSuccess("success", "Deleted successfully");
        }
        public IActionResult LeftEmployee(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee", accessId, "Left", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var employee = employeeRepository.GetEmployeeById(id);
            employee.Isleft = true;
            employee.LeftDate = DateTime.Now;
            employeeRepository.UpdateEmployee(employee);
            var access = accessRepository.GetAllAccesswithinactive().Where(a => a.EmployeeID == id).FirstOrDefault();
            access.IsAvailable = false;
            access.Active = false;
            accessRepository.UpdateAccess(access);
            EmployeeTimeline timeline = employeeRepository.GetEmployeeTimelineEmployeeById(id);
            timeline.LeftOn = DateTime.Now;
            timeline.ModifiedId = userId;
            timeline.Status = "Update";
            employeeRepository.UpdateEmployeeTimeline(timeline);
            return RedirectToAction(nameof(Index)).WithSuccess("success", "Left successfully"); ;
        }
        public IActionResult HasLeftEmployee(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee", accessId, "Left", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var employee = employeeRepository.GetEmployeeById(id);
            employee.Isleft = false;
            employee.LeftDate = null;
            employeeRepository.UpdateEmployee(employee);
            var access = accessRepository.GetAllAccesswithinactive().Where(a => a.EmployeeID == id).FirstOrDefault();
            access.IsAvailable = true;
            access.Active = true;
            accessRepository.UpdateAccess(access);
            EmployeeTimeline timeline = employeeRepository.GetEmployeeTimelineEmployeeById(id);
            timeline.LeftOn = null;
            timeline.ModifiedId = userId;
            timeline.Status = "Update";
            employeeRepository.UpdateEmployeeTimeline(timeline);
            return RedirectToAction(nameof(Index)).WithSuccess("success", "Entered successfully"); ;
        }
        public IActionResult ViewEmployeeDetails(long? id)
        {
            Employeecls employeecls = new Employeecls();
            employeecls.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
            employeecls.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
            employeecls.religionTypes = religionTypeRepository.GetAllReligionTypes();



            ViewBag.status = "Create";
            if (id != null)
            {


                Access access = new Access();


                var employeedetails = employeeRepository.GetEmployeeById(id.Value);
                employeecls.AlternativeMobile = employeedetails.AlternativeMobile;
                employeecls.BloodGroupID = employeedetails.BloodGroupID;
                employeecls.BloodGroupName = employeedetails.BloodGroupID != null && employeedetails.BloodGroupID != 0 ? bloodGroupRepository.GetBloodGroupById(employeedetails.BloodGroupID.Value).Name : "";
                employeecls.DateOfBirth = employeedetails.DateOfBirth != null ? employeedetails.DateOfBirth : null;
                employeecls.EmailId = employeedetails.EmailId;
                employeecls.EmpCode = employeedetails.EmpCode;
                employeecls.FirstName = employeedetails.FirstName;
                employeecls.Gender = employeedetails.Gender;
                employeecls.ID = employeedetails.ID;
                employeecls.LandlineNumber = employeedetails.LandlineNumber;
                employeecls.LastName = employeedetails.LastName;
                employeecls.MiddleName = employeedetails.MiddleName;
                employeecls.NatiobalityID = employeedetails.NatiobalityID;
                employeecls.NatiobalityName = (employeedetails.NatiobalityID != null && employeedetails.NatiobalityID != 0) ? nationalityTypeRepository.GetNationalityById(employeedetails.NatiobalityID.Value).Name : "";
                employeecls.PrimaryMobile = employeedetails.PrimaryMobile;
                employeecls.ReligionID = employeedetails.ReligionID;
                employeecls.ReligionName = employeedetails.ReligionID != null && employeedetails.ReligionID != 0 ? religionTypeRepository.GetReligionTypeById(employeedetails.ReligionID.Value).Name : "";
                var webPath = hostingEnvironment.WebRootPath;
                ViewBag.EmployeeName = employeecls.FirstName + employeecls.LandlineNumber;
                if (employeedetails.Image != null)
                {
                    string path = Path.Combine("/ODMImages/EmployeeProfile/", employeedetails.Image);
                    ViewBag.Profileimage = path;
                }
                else
                {
                    ViewBag.Profileimage = null;
                }
                var addressTypes = addressTypeRepository.GetAllAddressType();
                var address = commonMethods.GetAllAddressByUsingEmployeeId(employeedetails.ID);
                var employeeaddress = (from a in addressTypes
                                       select new EmployeeTypewithAddresscls
                                       {
                                           typeId = a.ID,
                                           typeName = a.Name,
                                           employeeAdress = address.Where(m => m.AddressTypeID == a.ID).FirstOrDefault()
                                       }).ToList();
                employeecls.employeeadresslist = employeeaddress;
                employeecls.employeeDesignationlist = commonMethods.GetAllDesignationByUsingEmployeeId(employeedetails.ID);
                employeecls.employeeExperiencelist = employeeExperienceRepository.GetExperienceByEmployeeId(employeedetails.ID);

                var banktypes = bankTypeRepository.GetAllBankType();
                var employeebank = commonMethods.GetAllBankByUsingEmployeeId(employeedetails.ID);
                var banks = (from a in banktypes
                             select new EmployeeTypewithAddresscls
                             {
                                 typeId = a.ID,
                                 typeName = a.Name,
                                 bankCls = employeebank.Where(m => m.BankTypeID == a.ID).FirstOrDefault()
                             }).ToList();

                employeecls.employeeBanks = banks;
                var qualificationtypes = educationQualificationTypeRepository.GetAllEducationQualificationType();
                var employeeEducations = commonMethods.GetAllEducationByUsingEmployeeId(employeedetails.ID);
                var education = (from a in qualificationtypes
                                 select new EmployeeTypewithAddresscls
                                 {
                                     typeId = a.ID,
                                     typeName = a.Name,
                                     employeeEducation = employeeEducations.Where(m => m.EducationQualificationTypeID == a.ID).FirstOrDefault()
                                 }).ToList();
                employeecls.employeeEducationlist = education;
                employeecls.employeeTimelines = employeeRepository.GetAllEmployeeTimeline() == null ? null : employeeRepository.GetAllEmployeeTimeline().Where(a => a.EmployeeId == employeedetails.ID && a.IsPrimary == true).ToList();
                employeecls.requiredDocumentCls = commonMethods.GetAllDocumentByUsingEmployeeId(employeedetails.ID);
                employeecls.employeeAccesscls = commonMethods.GetAccessTypeData(employeedetails.ID);
                if (employeecls.employeeAccesscls.AccessID != 0)
                {
                    employeecls.employeeModuleSubModuleAccess = commonMethods.givenAccesscls(employeecls.employeeAccesscls.AccessID).ToList();
                }
                var total = 12 + (employeedetails.Image != null ? 4 : 0) + (employeecls.employeeadresslist.Count() > 0 ? 14 : 0) + (employeecls.employeeDesignationlist.Count() > 0 ? 14 : 0) + (employeecls.employeeExperiencelist.Count() > 0 ? 14 : 0) + (employeecls.employeeBanks.Count() > 0 ? 14 : 0) + (employeecls.employeeEducationlist.Count() > 0 ? 14 : 0) + (employeecls.requiredDocumentCls.Count() > 0 ? 14 : 0);
                ViewBag.count = total;
                ViewBag.status = "Update";
            }
            return View(employeecls);
        }

        #endregion
        #region Employee Address
        public IActionResult EmployeeAddressDetails(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Address", accessId, "List", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            ViewBag.EmployeeId = id;
            ViewBag.EmployeeName = employeeRepository.GetEmployeeById(id.Value).FirstName;
            var addressTypes = addressTypeRepository.GetAllAddressType();
            var address = commonMethods.GetAllAddressByUsingEmployeeId(id.Value);
            var res = (from a in addressTypes
                       select new EmployeeTypewithAddresscls
                       {
                           typeId = a.ID,
                           typeName = a.Name,
                           employeeAdress = address.Where(m => m.AddressTypeID == a.ID).FirstOrDefault()
                       }).ToList();

            return View(res);
        }

        public IActionResult CreateOrEditEmployeeAddress(long? id, long eid, long typeid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            EmployeeAdressCls employeeAdressCls = new EmployeeAdressCls();
            // employeeAdressCls.addressTypes = addressTypeRepository.GetAllAddressType();
            employeeAdressCls.countries = countryRepository.GetAllCountries();
            employeeAdressCls.EmployeeId = eid;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Employee Address", accessId, "Edit", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var employeeAddress = employeeAddressRepository.GetEmployeeAddressById(id.Value);
                employeeAdressCls.EmployeeAddressId = employeeAddress.ID;
                employeeAdressCls.ID = employeeAddress.AddressID;
                var address = addressRepository.GetAddressById(employeeAddress.AddressID);
                employeeAdressCls.cities = cityRepository.GetAllCityByStateId(address.StateID);
                employeeAdressCls.states = stateRepository.GetAllStateByCountryId(address.CountryID);
                employeeAdressCls.AddressLine1 = address.AddressLine1;
                employeeAdressCls.AddressLine2 = address.AddressLine2;
                employeeAdressCls.AddressTypeID = address.AddressTypeID;
                employeeAdressCls.CityID = address.CityID;
                employeeAdressCls.StateID = address.StateID;
                employeeAdressCls.CountryID = address.CountryID;
                employeeAdressCls.PostalCode = address.PostalCode;
                ViewBag.EmployeeId = id.Value;
                ViewBag.EmployeeName = employeeRepository.GetEmployeeById(id.Value).FirstName;
                ViewBag.status = "Update";

            }
            else
            {
                if (commonMethods.checkaccessavailable("Employee Address", accessId, "Create", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                employeeAdressCls.AddressLine1 = "";
                employeeAdressCls.AddressLine2 = "";
                employeeAdressCls.AddressTypeID = typeid;
                employeeAdressCls.CityID = 0;
                employeeAdressCls.StateID = 0;
                employeeAdressCls.CountryID = 0;
                employeeAdressCls.ID = 0;
                employeeAdressCls.EmployeeAddressId = 0;

                ViewBag.status = "Create";
            }
            return View(employeeAdressCls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeAddress([Bind("ID,AddressLine1,AddressLine2,AddressTypeID,CityID,CountryID,PostalCode,StateID")]Address address, long EmployeeAddressId, long EmployeeId)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                address.ModifiedId = userId;
                address.Active = true;
                if (address.ID == 0 && EmployeeAddressId == 0)
                {
                    address.InsertedId = userId;
                    address.Status = EntityStatus.ACTIVE;
                    long id = addressRepository.CreateAddress(address);
                    EmployeeAddress employeeAddress = new EmployeeAddress();
                    employeeAddress.AddressID = id;
                    employeeAddress.EmployeeID = EmployeeId;
                    employeeAddress.Active = true;
                    employeeAddress.InsertedId = userId;
                    employeeAddress.ModifiedId = userId;
                    employeeAddress.Status = EntityStatus.ACTIVE;
                    employeeAddressRepository.CreateEmployeeAddress(employeeAddress);
                    return RedirectToAction(nameof(EmployeeAddressDetails), new { id = EmployeeId }).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    addressRepository.UpdateAddress(address);
                    return RedirectToAction(nameof(EmployeeAddressDetails), new { id = EmployeeId }).WithSuccess("success", "Updated successfully");
                }



            }
            return View(address).WithDanger("error", "Not Saved"); ;
        }
        [ActionName("DeleteEmployeeAddress")]
        public async Task<IActionResult> EmployeeAddressDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Address", accessId, "Delete", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var employeeAddress = employeeAddressRepository.DeleteEmployeeAddress(id.Value);
            return RedirectToAction(nameof(EmployeeAddressDetails), new { id = eid }).WithSuccess("success", "Deleted successfully"); ;
        }
        #endregion
        #region Employee Designation
        public IActionResult EmployeeDesignationDetails(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Designation", accessId, "List", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            ViewBag.EmployeeId = id;
            var res = commonMethods.GetAllDesignationByUsingEmployeeId(id.Value);
            return View(res);
        }
        public IActionResult CreateOrEditEmployeeDesignation(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            EmployeeDesignationCls employeeDesignationCls = new EmployeeDesignationCls();
            //  employeeDesignationCls.designationLevels = commonMethods.GetParentDesignationLevels();
            employeeDesignationCls.groups = groupRepository.GetAllGroup();
            employeeDesignationCls.organizations = organizationRepository.GetAllOrganization();
            employeeDesignationCls.EmployeeID = eid;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Employee Designation", accessId, "Edit", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var employeeDesignation = commonMethods.GetAllDesignationByUsingEmployeeId(eid).ToList().Where(a => a.ID == id.Value).FirstOrDefault();
                employeeDesignationCls.DepartmentID = employeeDesignation.DepartmentID;
                employeeDesignationCls.DesignationId = employeeDesignation.DesignationId;
                // employeeDesignationCls.DesignationLevelID = employeeDesignation.DesignationLevelID;
                employeeDesignationCls.GroupID = employeeDesignation.GroupID;
                employeeDesignationCls.ID = employeeDesignation.ID;
                employeeDesignationCls.OrganizationID = employeeDesignation.OrganizationID;

                if (employeeDesignation.OrganizationID > 0)
                {
                    employeeDesignationCls.departments = commonMethods.
                        GetDepartmentsByOrganisation(employeeDesignation.OrganizationID).ToList();
                    employeeDesignationCls.DepartmentType = "2";
                }
                else
                {
                    employeeDesignationCls.departments = commonMethods.
                   GetDepartmentsByGroupID(employeeDesignation.GroupID).ToList();
                    employeeDesignationCls.DepartmentType = "1";

                }


                employeeDesignationCls.groups = groupRepository.GetAllGroup().ToList();

                employeeDesignationCls.organizations = organizationRepository.GetAllOrganization().Where(a => a.GroupID == employeeDesignation.GroupID).ToList();
                employeeDesignationCls.designations = designationRepository.GetAllDesignation().Where(a => a.DepartmentID == employeeDesignation.DepartmentID).ToList();
                ViewBag.status = "Update";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Employee Designation", accessId, "Create", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                employeeDesignationCls.DepartmentID = 0;
                employeeDesignationCls.DesignationId = 0;
                employeeDesignationCls.DesignationLevelID = 0;
                employeeDesignationCls.GroupID = 0;
                employeeDesignationCls.ID = 0;
                employeeDesignationCls.OrganizationID = 0;
                employeeDesignationCls.DepartmentType = "0";
                ViewBag.status = "Create";
            }
            return View(employeeDesignationCls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeDesignation([Bind("ID,DesignationID,EmployeeID,DesignationLevelID,DepartmentID")]EmployeeDesignation employeeDesignation)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                employeeDesignation.ModifiedId = userId;
                employeeDesignation.Active = true;
                if (employeeDesignation.ID == 0)
                {
                    employeeDesignation.InsertedId = userId;
                    employeeDesignation.Status = EntityStatus.ACTIVE;
                    employeeDesignationRepository.CreateEmployeeDesignation(employeeDesignation);
                    return RedirectToAction(nameof(EmployeeDesignationDetails), new { id = employeeDesignation.EmployeeID }).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    employeeDesignationRepository.UpdateEmployeeDesignation(employeeDesignation);
                    return RedirectToAction(nameof(EmployeeDesignationDetails), new { id = employeeDesignation.EmployeeID }).WithSuccess("success", "Updated successfully");
                }



            }
            return View(employeeDesignation);
        }
        [ActionName("DeleteEmployeeDesignation")]
        public async Task<IActionResult> EmployeeDesignationDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Designation", accessId, "Delete", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var employeeAddress = employeeDesignationRepository.DeleteEmployeeDesignation(id.Value);
            return RedirectToAction(nameof(EmployeeDesignationDetails), new { id = eid }).WithSuccess("success", "Deleted successfully");
        }
        #endregion
        #region Employee Experience
        public IActionResult EmployeeExperienceDetails(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Experience", accessId, "List", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            ViewBag.EmployeeId = id;
            var res = employeeExperienceRepository.GetExperienceByEmployeeId(id.Value);
            return View(res);
        }
        public IActionResult CreateOrEditEmployeeExperience(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            EmployeeExperienceCls employeeExperience = new EmployeeExperienceCls();
            employeeExperience.EmployeeID = eid;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Employee Experience", accessId, "Edit", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var employeeExp = employeeExperienceRepository.GetExperienceByEmployeeId(eid).ToList().Where(a => a.ID == id.Value).FirstOrDefault();
                employeeExperience.Designation = employeeExp.Designation;
                employeeExperience.FromDate = employeeExp.FromDate;
                employeeExperience.ToDate = employeeExp.ToDate;
                employeeExperience.SalaryPerAnum = employeeExp.SalaryPerAnum;
                employeeExperience.OrganizationName = employeeExp.OrganizationName;
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("EXPERIENCE").ID;
                employeeExperience.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                       where a.DocumentTypeID == documentTypeId
                                                       select new documentTypesCls
                                                       {
                                                           TypeId = a.DocumentTypeID,
                                                           ID = a.ID,
                                                           Name = a.Name,
                                                           IsRequired = commonMethods.employeeRequiredDocument(eid, a.DocumentTypeID, a.ID)
                                                       }).ToList();
                ViewBag.status = "Update";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Employee Experience", accessId, "Create", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                employeeExperience.Designation = "";
                employeeExperience.FromDate = DateTime.Now.Date;
                employeeExperience.ToDate = DateTime.Now.Date;
                employeeExperience.SalaryPerAnum = 0;
                employeeExperience.OrganizationName = "";
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("EXPERIENCE").ID;
                employeeExperience.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                       where a.DocumentTypeID == documentTypeId
                                                       select new documentTypesCls
                                                       {
                                                           TypeId = a.DocumentTypeID,
                                                           ID = a.ID,
                                                           Name = a.Name,
                                                           IsRequired = false
                                                       }).ToList();
                ViewBag.status = "Create";
            }
            return View(employeeExperience);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeExperience([Bind("ID,Designation,EmployeeID,FromDate,ToDate,SalaryPerAnum,OrganizationName")]EmployeeExperience employeeExperience, [Bind("documentTypesCls")]List<documentTypesCls> documentTypesCls)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                employeeExperience.ModifiedId = userId;
                employeeExperience.Active = true;
                if (employeeExperience.ID == 0)
                {
                    employeeExperience.InsertedId = userId;
                    employeeExperience.Status = EntityStatus.ACTIVE;
                    long eid = employeeExperienceRepository.CreateExperience(employeeExperience);
                    foreach (var item in documentTypesCls)
                    {
                        if (item.IsRequired == true)
                        {
                            EmployeeRequiredDocument employeeRequired = new EmployeeRequiredDocument();
                            employeeRequired.Active = true;
                            employeeRequired.DocumentSubTypeID = item.ID;
                            employeeRequired.DocumentTypeID = item.TypeId;
                            employeeRequired.EmployeeID = employeeExperience.EmployeeID;
                            employeeRequired.InsertedId = userId;
                            employeeRequired.IsApproved = false;
                            employeeRequired.IsMandatory = true;
                            employeeRequired.IsReject = false;
                            employeeRequired.IsVerified = false;
                            employeeRequired.ModifiedId = userId;
                            employeeRequired.RelatedID = eid;
                            employeeRequired.Status = EntityStatus.ACTIVE;
                            employeeRequired.VerifiedAccessID = 0;
                            employeeRequiredDocumentRepository.CreateEmployeeRequiredDocument(employeeRequired);
                        }
                    }
                    return RedirectToAction(nameof(EmployeeExperienceDetails), new { id = employeeExperience.EmployeeID }).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    employeeExperienceRepository.UpdateExperience(employeeExperience);
                    foreach (var item in documentTypesCls)
                    {
                        EmployeeRequiredDocument employeeRequired = employeeRequiredDocumentRepository.GetAllEmployeeRequiredDocuments().Where(a => a.DocumentTypeID == item.TypeId && a.DocumentSubTypeID == item.ID && a.EmployeeID == employeeExperience.EmployeeID).FirstOrDefault();
                        if (item.IsRequired == true)
                        {
                            if (employeeRequired == null)
                            {
                                employeeRequired = new EmployeeRequiredDocument();
                                employeeRequired.Active = true;
                                employeeRequired.DocumentSubTypeID = item.ID;
                                employeeRequired.DocumentTypeID = item.TypeId;
                                employeeRequired.EmployeeID = employeeExperience.EmployeeID;
                                employeeRequired.InsertedId = userId;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employeeExperience.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.CreateEmployeeRequiredDocument(employeeRequired);
                            }
                            else
                            {
                                employeeRequired.Active = true;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employeeExperience.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(employeeRequired);
                            }
                        }
                        else
                        {
                            if (employeeRequired != null)
                            {
                                employeeRequired.Active = false;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employeeExperience.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(employeeRequired);
                            }

                        }

                    }
                    return RedirectToAction(nameof(EmployeeExperienceDetails), new { id = employeeExperience.EmployeeID }).WithSuccess("success", "Updated successfully");
                }



            }
            return View(employeeExperience).WithDanger("error", "Not Saved");
        }
        //   [ActionName("DeleteEmployeeExperience")]
        public async Task<IActionResult> EmployeeExperienceDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Experience", accessId, "Delete", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var employeeExperience = employeeExperienceRepository.DeleteExperience(id.Value);
            return RedirectToAction(nameof(EmployeeExperienceDetails), new { id = eid }).WithSuccess("success", "Deleted successfully");
        }
        #endregion
        #region Employee Bank Details
        public IActionResult EmployeeBankDetails(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Bank Account", accessId, "List", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            ViewBag.EmployeeId = id;
            var banktypes = bankTypeRepository.GetAllBankType();
            var employeebank = commonMethods.GetAllBankByUsingEmployeeId(id.Value);
            var res = (from a in banktypes
                       select new EmployeeTypewithAddresscls
                       {
                           typeId = a.ID,
                           typeName = a.Name,
                           bankCls = employeebank.Where(m => m.BankTypeID == a.ID).FirstOrDefault()
                       }).ToList();
            return View(res);
        }
        public IActionResult CreateOrEditEmployeeBank(long? id, long eid, long typeid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            EmployeeBankCls employeeBankCls = new EmployeeBankCls();
            employeeBankCls.EmployeeID = eid;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Employee Bank Account", accessId, "Edit", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                var employeeBank = employeeBankAccountRepository.GetEmployeeBankAccountByEmployeeID(eid).ToList().Where(a => a.ID == id.Value).FirstOrDefault();
                employeeBankCls.BankNameID = employeeBank.BankNameID;
                employeeBankCls.BankTypeID = employeeBank.BankTypeID;
                employeeBankCls.BankBranchName = employeeBank.BankBranchName;
                employeeBankCls.IfscCode = employeeBank.IfscCode;
                employeeBankCls.AccountNumber = employeeBank.AccountNumber;
                employeeBankCls.AccountHolderName = employeeBank.AccountHolderName;
                employeeBankCls.ID = employeeBank.ID;
                employeeBankCls.banks = bankRepository.GetAllBank();
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("BANK").ID;
                employeeBankCls.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                    where a.DocumentTypeID == documentTypeId
                                                    select new documentTypesCls
                                                    {
                                                        TypeId = a.DocumentTypeID,
                                                        ID = a.ID,
                                                        Name = a.Name,
                                                        IsRequired = commonMethods.employeeRequiredDocument(eid, a.DocumentTypeID, a.ID)
                                                    }).ToList();
                ViewBag.status = "Update";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Employee Bank Account", accessId, "Create", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                employeeBankCls.BankTypeID = typeid;
                employeeBankCls.BankNameID = 0;
                employeeBankCls.BankBranchName = "";
                employeeBankCls.IfscCode = "";
                employeeBankCls.AccountNumber = "";
                employeeBankCls.AccountHolderName = "";
                employeeBankCls.ID = 0;
                employeeBankCls.banks = bankRepository.GetAllBank();
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("BANK").ID;
                employeeBankCls.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                    where a.DocumentTypeID == documentTypeId
                                                    select new documentTypesCls
                                                    {
                                                        TypeId = a.DocumentTypeID,
                                                        ID = a.ID,
                                                        Name = a.Name,
                                                        IsRequired = false
                                                    }).ToList();
                ViewBag.status = "Create";
            }
            return View(employeeBankCls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeBank([Bind("ID,BankTypeID,BankNameID,BankBranchName,IfscCode,AccountNumber,AccountHolderName,EmployeeID")]EmployeeBankAccount employeeBankAccount, [Bind("documentTypesCls")]List<documentTypesCls> documentTypesCls)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                employeeBankAccount.ModifiedId = userId;
                employeeBankAccount.Active = true;
                if (employeeBankAccount.ID == 0)
                {
                    employeeBankAccount.InsertedId = userId;
                    employeeBankAccount.Status = EntityStatus.ACTIVE;
                    long eid = employeeBankAccountRepository.CreateEmployeeBankAccount(employeeBankAccount);
                    foreach (var item in documentTypesCls)
                    {
                        if (item.IsRequired == true)
                        {
                            EmployeeRequiredDocument employeeRequired = new EmployeeRequiredDocument();
                            employeeRequired.Active = true;
                            employeeRequired.DocumentSubTypeID = item.ID;
                            employeeRequired.DocumentTypeID = item.TypeId;
                            employeeRequired.EmployeeID = employeeBankAccount.EmployeeID;
                            employeeRequired.InsertedId = userId;
                            employeeRequired.IsApproved = false;
                            employeeRequired.IsMandatory = true;
                            employeeRequired.IsReject = false;
                            employeeRequired.IsVerified = false;
                            employeeRequired.ModifiedId = userId;
                            employeeRequired.RelatedID = eid;
                            employeeRequired.Status = EntityStatus.ACTIVE;
                            employeeRequired.VerifiedAccessID = 0;
                            employeeRequiredDocumentRepository.CreateEmployeeRequiredDocument(employeeRequired);
                        }
                    }
                    return RedirectToAction(nameof(EmployeeBankDetails), new { id = employeeBankAccount.EmployeeID }).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    employeeBankAccountRepository.UpdateEmployeeBankAccount(employeeBankAccount);
                    foreach (var item in documentTypesCls)
                    {
                        EmployeeRequiredDocument employeeRequired = employeeRequiredDocumentRepository.GetAllEmployeeRequiredDocuments().Where(a => a.DocumentTypeID == item.TypeId && a.DocumentSubTypeID == item.ID && a.EmployeeID == employeeBankAccount.EmployeeID).FirstOrDefault();
                        if (item.IsRequired == true)
                        {
                            if (employeeRequired == null)
                            {
                                employeeRequired = new EmployeeRequiredDocument();
                                employeeRequired.Active = true;
                                employeeRequired.DocumentSubTypeID = item.ID;
                                employeeRequired.DocumentTypeID = item.TypeId;
                                employeeRequired.EmployeeID = employeeBankAccount.EmployeeID;
                                employeeRequired.InsertedId = userId;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employeeBankAccount.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.CreateEmployeeRequiredDocument(employeeRequired);
                            }
                            else
                            {
                                employeeRequired.Active = true;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employeeBankAccount.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(employeeRequired);
                            }
                        }
                        else
                        {
                            if (employeeRequired != null)
                            {
                                employeeRequired.Active = false;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employeeBankAccount.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(employeeRequired);
                            }

                        }

                    }
                    return RedirectToAction(nameof(EmployeeBankDetails), new { id = employeeBankAccount.EmployeeID }).WithSuccess("success", "Updated successfully");
                }

            }
            return View(employeeBankAccount).WithDanger("error", "Not Saved");
        }
        [ActionName("DeleteEmployeeBank")]
        public async Task<IActionResult> EmployeeBankDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Bank Account", accessId, "Delete", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var employeeBankAccount = employeeBankAccountRepository.DeleteEmployeeBankAccount(id.Value);
            return RedirectToAction(nameof(EmployeeBankDetails), new { id = eid }).WithSuccess("success", "Deleted successfully");
        }
        #endregion
        #region Employee Education Details
        public IActionResult EmployeeEducationDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Education", accessId, "List", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.EmployeeId = id;
            var qualificationtypes = educationQualificationTypeRepository.GetAllEducationQualificationType();
            var employeeEducations = commonMethods.GetAllEducationByUsingEmployeeId(id);
            var res = (from a in qualificationtypes
                       select new EmployeeTypewithAddresscls
                       {
                           typeId = a.ID,
                           typeName = a.Name,
                           employeeEducation = employeeEducations.Where(m => m.EducationQualificationTypeID == a.ID).FirstOrDefault()
                       }).ToList();

            return View(res);
        }
        public IActionResult CreateOrEditEmployeeEducation(long? id, long eid, long typeid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            EmployeeEducationCls employeeEducationCls = new EmployeeEducationCls();
            employeeEducationCls.EmployeeID = eid;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Employee Education", accessId, "Edit", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var employeeEducation = commonMethods.GetAllEducationByUsingEmployeeId(eid).ToList().Where(a => a.ID == id.Value).FirstOrDefault();
                employeeEducationCls.EducationQualificationID = employeeEducation.EducationQualificationID;
                employeeEducationCls.EducationQualificationTypeID = employeeEducation.EducationQualificationTypeID;
                employeeEducationCls.ID = employeeEducation.ID;
                employeeEducationCls.educationQualifications = educationQualificationRepository.GetAllEducationQualificationByTypeId(employeeEducation.EducationQualificationTypeID);
                employeeEducationCls.educationQualificationTypes = educationQualificationTypeRepository.GetAllEducationQualificationType();
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("EDUCATIONAL").ID;
                employeeEducationCls.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                         where a.DocumentTypeID == documentTypeId
                                                         select new documentTypesCls
                                                         {
                                                             TypeId = a.DocumentTypeID,
                                                             ID = a.ID,
                                                             Name = a.Name,
                                                             IsRequired = commonMethods.employeeRequiredDocument(eid, a.DocumentTypeID, a.ID)
                                                         }).ToList();
                employeeEducationCls.College = employeeEducation.College;
                employeeEducationCls.University = employeeEducation.University;
                employeeEducationCls.FromYear = employeeEducation.FromYear;
                employeeEducationCls.ToYear = employeeEducation.ToYear;
                ViewBag.status = "Update";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Employee Education", accessId, "Create", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                employeeEducationCls.EducationQualificationID = 0;
                employeeEducationCls.EducationQualificationTypeID = typeid;
                employeeEducationCls.educationQualificationTypes = educationQualificationTypeRepository.GetAllEducationQualificationType();
                employeeEducationCls.ID = 0;
                employeeEducationCls.educationQualifications = educationQualificationRepository.GetAllEducationQualificationByTypeId(typeid);
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("EDUCATIONAL").ID;
                employeeEducationCls.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                         where a.DocumentTypeID == documentTypeId
                                                         select new documentTypesCls
                                                         {
                                                             TypeId = a.DocumentTypeID,
                                                             ID = a.ID,
                                                             Name = a.Name,
                                                             IsRequired = false
                                                         }).ToList();
                employeeEducationCls.College = "";
                employeeEducationCls.University = "";
                employeeEducationCls.FromYear = null;
                employeeEducationCls.ToYear = null;
                ViewBag.status = "Create";
            }
            return View(employeeEducationCls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeEducation([Bind("ID,EducationQualificationID,EmployeeID,College,University,FromYear,ToYear")]EmployeeEducation employeeEducation, [Bind("documentTypesCls")]List<documentTypesCls> documentTypesCls)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                employeeEducation.ModifiedId = userId;
                employeeEducation.Active = true;
                if (employeeEducation.ID == 0)
                {
                    employeeEducation.InsertedId = userId;
                    employeeEducation.Status = EntityStatus.ACTIVE;
                    long eid = employeeEducationRepository.CreateEmployeeEducation(employeeEducation);
                    foreach (var item in documentTypesCls)
                    {
                        if (item.IsRequired == true)
                        {
                            EmployeeRequiredDocument employeeRequired = new EmployeeRequiredDocument();
                            employeeRequired.Active = true;
                            employeeRequired.DocumentSubTypeID = item.ID;
                            employeeRequired.DocumentTypeID = item.TypeId;
                            employeeRequired.EmployeeID = employeeEducation.EmployeeID;
                            employeeRequired.InsertedId = userId;
                            employeeRequired.IsApproved = false;
                            employeeRequired.IsMandatory = true;
                            employeeRequired.IsReject = false;
                            employeeRequired.IsVerified = false;
                            employeeRequired.ModifiedId = userId;
                            employeeRequired.RelatedID = eid;
                            employeeRequired.Status = EntityStatus.ACTIVE;
                            employeeRequired.VerifiedAccessID = 0;
                            employeeRequiredDocumentRepository.CreateEmployeeRequiredDocument(employeeRequired);
                        }
                    }
                    return RedirectToAction(nameof(EmployeeEducationDetails), new { id = employeeEducation.EmployeeID }).WithSuccess("success", "Saved successfully");
                }
                else
                {
                    employeeEducationRepository.UpdateEmployeeEducation(employeeEducation);
                    foreach (var item in documentTypesCls)
                    {
                        EmployeeRequiredDocument employeeRequired = employeeRequiredDocumentRepository.GetAllEmployeeRequiredDocuments().Where(a => a.DocumentTypeID == item.TypeId && a.DocumentSubTypeID == item.ID && a.EmployeeID == employeeEducation.EmployeeID).FirstOrDefault();
                        if (item.IsRequired == true)
                        {
                            if (employeeRequired == null)
                            {
                                EmployeeRequiredDocument employeeRequir = new EmployeeRequiredDocument();
                                employeeRequir.Active = true;
                                employeeRequir.DocumentSubTypeID = item.ID;
                                employeeRequir.DocumentTypeID = item.TypeId;
                                employeeRequir.EmployeeID = employeeEducation.EmployeeID;
                                employeeRequir.InsertedId = userId;
                                employeeRequir.IsApproved = false;
                                employeeRequir.IsMandatory = true;
                                employeeRequir.IsReject = false;
                                employeeRequir.IsVerified = false;
                                employeeRequir.ModifiedId = userId;
                                employeeRequir.RelatedID = employeeEducation.ID;
                                employeeRequir.Status = EntityStatus.ACTIVE;
                                employeeRequir.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.CreateEmployeeRequiredDocument(employeeRequir);
                            }
                            else
                            {
                                employeeRequired.Active = true;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employeeEducation.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(employeeRequired);
                            }
                        }
                        else
                        {
                            if (employeeRequired != null)
                            {
                                employeeRequired.Active = false;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = employeeEducation.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(employeeRequired);
                            }

                        }

                    }
                    return RedirectToAction(nameof(EmployeeEducationDetails), new { id = employeeEducation.EmployeeID }).WithSuccess("success", "Updated successfully");
                }
            }
            return View(employeeEducation).WithDanger("error", "Not Saved");
        }
        [ActionName("DeleteEmployeeEducation")]
        public async Task<IActionResult> EmployeeEducationDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Education", accessId, "Delete", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var employeeEducation = employeeEducationRepository.DeleteEmployeeEducation(id.Value);
            return RedirectToAction(nameof(EmployeeEducationDetails), new { id = eid }).WithSuccess("success", "Deleted successfully");
        }
        #endregion

        #region Employee Document Details
        public IActionResult EmployeeDocumentDetails(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            if (commonMethods.checkaccessavailable("Employee Document", accessId, "List", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.EmployeeId = id;
            if (commonMethods.checkaccessavailable("Employee Document", accessId, "Verify Document", "HR", roleId) == true)
            {
                ViewBag.verifiedaccess = true;
            }
            else
            {
                ViewBag.verifiedaccess = false;
            }
            if (commonMethods.checkaccessavailable("Employee Document", accessId, "Approve Document", "HR", roleId) == true)
            {
                ViewBag.approveaccess = true;
            }
            else
            {
                ViewBag.approveaccess = false;
            }
            if (commonMethods.checkaccessavailable("Employee Document", accessId, "Reject Document", "HR", roleId) == true)
            {
                ViewBag.rejectaccess = true;
            }
            else
            {
                ViewBag.rejectaccess = false;
            }
            return View(commonMethods.GetAllDocumentByUsingEmployeeId(id.Value));
        }

        public IActionResult CreateOrEditEmployeeDocumentDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Document", accessId, "Upload", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var doc = employeeRequiredDocumentRepository.GetEmployeeRequiredDocumentById(id);
            EmployeeDocumentUploadCls documentUploadCls = new EmployeeDocumentUploadCls();
            documentUploadCls.attachments = employeeDocumentAttachmentRepository.GetAllEmployeeDocumentAttachments().Where(a => a.EmployeeRequiredDocumentID == id).ToList();
            documentUploadCls.isapproved = doc.IsApproved;
            documentUploadCls.isverified = doc.IsVerified;
            documentUploadCls.ID = id;
            documentUploadCls.EmployeeID = doc.EmployeeID;

            return View(documentUploadCls);
        }
        public IActionResult RemoveAttachments(long id, long did)
        {
            employeeDocumentAttachmentRepository.DeleteEmployeeDocumentAttachment(id);
            return RedirectToAction(nameof(CreateOrEditEmployeeDocumentDetails), new { id = did }).WithSuccess("success", "Deleted successfully"); ;
        }
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateEmployeeDocument(long ID, IEnumerable<IFormFile> files)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (files != null)
            {
                if (files.Count() > 0)
                {
                    foreach (IFormFile file in files)
                    {
                        EmployeeDocumentAttachment employeeDocument = new EmployeeDocumentAttachment();
                        employeeDocument.Active = true;
                        employeeDocument.EmployeeRequiredDocumentID = ID;
                        employeeDocument.InsertedId = userId;
                        employeeDocument.ModifiedId = userId;
                        employeeDocument.Status = "Upload";
                        FileInfo fi = new FileInfo(file.FileName);
                        var newFilename = ID + "_" + String.Format("{0:d}",
                                          (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                        var webPath = hostingEnvironment.WebRootPath;
                        string path = Path.Combine("", webPath + @"\ODMImages\EmployeeDocument\" + newFilename);
                        var pathToSave = newFilename;
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }
                        employeeDocument.DocumentPath = pathToSave;
                        employeeDocumentAttachmentRepository.CreateEmployeeDocumentAttachment(employeeDocument);
                    }
                }
            }

            return RedirectToAction(nameof(CreateOrEditEmployeeDocumentDetails), new { id = ID }).WithSuccess("success", "Saved successfully");
        }
        public IActionResult verifyDocument(long id, bool verify)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            EmployeeRequiredDocument ob = employeeRequiredDocumentRepository.GetEmployeeRequiredDocumentById(id);
            if (verify == true)
            {
                ob.IsVerified = true;
                ob.VerifiedDate = DateTime.Now;
                ob.VerifiedAccessID = userId;
                ob.ModifiedId = userId;
            }
            else
            {
                ob.IsVerified = false;
                ob.VerifiedDate = null;
                ob.VerifiedAccessID = 0;
                ob.ModifiedId = userId;
            }
            employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(ob);
            return RedirectToAction(nameof(EmployeeDocumentDetails), new { id = ob.EmployeeID }).WithSuccess("success", "Verified successfully");
        }
        public IActionResult approveDocument(long id, bool verify)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            EmployeeRequiredDocument ob = employeeRequiredDocumentRepository.GetEmployeeRequiredDocumentById(id);
            if (verify == true)
            {
                ob.IsApproved = true;
                ob.ModifiedId = userId;
            }
            else
            {
                ob.IsApproved = false;
                ob.ModifiedId = userId;
            }
            employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(ob);
            return RedirectToAction(nameof(EmployeeDocumentDetails), new { id = ob.EmployeeID }).WithSuccess("success", "Approved successfully");
        }
        public IActionResult rejectDocument(long id, bool verify)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            EmployeeRequiredDocument ob = employeeRequiredDocumentRepository.GetEmployeeRequiredDocumentById(id);
            if (verify == true)
            {
                ob.IsReject = true;
                ob.IsVerified = false;
                ob.VerifiedDate = null;
                ob.VerifiedAccessID = 0;
                ob.IsApproved = false;
                ob.ModifiedId = userId;
            }
            else
            {
                ob.IsReject = false;
                ob.ModifiedId = userId;
            }
            employeeRequiredDocumentRepository.UpdateEmployeeRequiredDocument(ob);
            return RedirectToAction(nameof(EmployeeDocumentDetails), new { id = ob.EmployeeID }).WithSuccess("success", "Rejected successfully");
        }
        #endregion

        #region Employee Login Access
        public IActionResult EmployeeAccessDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Access", accessId, "Create", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.EmployeeId = id;
            return View(commonMethods.GetAccessTypeData(id));
        }
        public JsonResult CheckUserName(string userName)
        {
            var res = commonMethods.Checkuser(userName);
            return Json(res);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeAccessDetails([Bind("AccessID,RoleID,EmployeeID,Username,Password")]EmployeeAccesscls employeeLogInCls)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                Access access = new Access();
                access.EmployeeID = employeeLogInCls.EmployeeID;
                access.Active = true;
                access.ID = employeeLogInCls.AccessID;
                access.InsertedId = userId;
                access.Password = employeeLogInCls.Password;
                access.Username = employeeLogInCls.Username;
                access.RoleID = employeeLogInCls.RoleID;
                if (access.ID == 0)
                {
                    access.ModifiedId = userId;
                    accessRepository.CreateAccess(access);
                }
                else
                {
                    access.ModifiedId = userId;
                    accessRepository.UpdateAccess(access);
                }

                return RedirectToAction(nameof(ViewEmployeeDetails), new { id = employeeLogInCls.EmployeeID });
            }
            return View(nameof(EmployeeAccessDetails), new { id = employeeLogInCls.EmployeeID });
        }


        #endregion

        #region Employee Module and submodule access
        //public IActionResult EmployeeAccess(long id)
        //{
        //    ViewBag.EmployeeID = id;
        //    return View();
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> EmployeeAccessDetails(int action,long ID)
        //{
        //    if (action == 1)
        //    {
        //        var department = employeeDesignationRepository.GetAllEmployeeDesignations().Where(a => a.EmployeeID == ID).ToList();

        //    }
        //    return View();
        //}
        public IActionResult ModuleSubModuleAccess(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Module Access", accessId, "Create", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            long eid = accessRepository.GetAllAccess().Where(a => a.ID == id).FirstOrDefault().EmployeeID;
            ViewBag.EmployeeID = eid;
            return View(commonMethods.GetEmployeeModuleSubModuleAccess(id, eid));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeModuleSubModuleDetails(EmployeeModuleSubModuleAccess employeeModuleSubModuleAccess)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                foreach (var a in employeeModuleSubModuleAccess.modules)
                {
                    if (a.subModules != null)
                    {
                        foreach (var b in a.subModules)
                        {
                            if (b.actionSubModules != null)
                            {
                                foreach (var c in b.actionSubModules)
                                {
                                    if (c.IsRequired == true && c.ActionAccessId == 0)
                                    {
                                        ActionAccess actionAccess = new ActionAccess();
                                        actionAccess.AccessID = employeeModuleSubModuleAccess.ID;
                                        actionAccess.ActionID = c.ID;
                                        actionAccess.Active = true;
                                        actionAccess.InsertedId = userId;
                                        actionAccess.ModifiedId = userId;
                                        actionAccess.ModuleID = a.ID;
                                        actionAccess.Status = EntityStatus.ACTIVE;
                                        actionAccess.SubModuleID = b.ID;
                                        actionAccessRepository.CreateActionAccess(actionAccess);
                                    }
                                    else if (c.ActionAccessId != 0)
                                    {
                                        ActionAccess actionAccess = actionAccessRepository.GetActionAccessById(c.ActionAccessId);
                                        if (actionAccess != null)
                                        {
                                            if (c.IsRequired == false)
                                            {
                                                actionAccessRepository.DeleteActionAccess(actionAccess.ID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return RedirectToAction(nameof(ModuleSubModuleAccess), new { id = employeeModuleSubModuleAccess.ID }).WithSuccess("success", "Saved successfully");
            }
            return RedirectToAction(nameof(ModuleSubModuleAccess), new { id = employeeModuleSubModuleAccess.ID }).WithSuccess("success", "Saved successfully");
        }
        #endregion
        #region Employee Joining Details
        public IActionResult EmployeeJoiningDetails(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Joining", accessId, "List", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            ViewBag.EmployeeId = id;
            ViewBag.EmployeeName = employeeRepository.GetEmployeeById(id.Value).FirstName;
            var res = employeeRepository.GetAllEmployeeTimeline() == null ? null : employeeRepository.GetAllEmployeeTimeline().Where(a => a.EmployeeId == id).ToList();

            return View(res);
        }

        public IActionResult CreateOrEditEmployeeJoining(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            employeejoiningdatescls employeeCls = new employeejoiningdatescls();
            employeeCls.EmployeeId = eid;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Employee Joining", accessId, "Edit", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var employee = employeeRepository.GetEmployeeTimelineById(id.Value);
                employeeCls.JoiningOn = employee.JoiningOn.Value;
                employeeCls.ID = employee.ID;
                employeeCls.LeftOn = employee.LeftOn;
                employeeCls.IsPrimary = employee.IsPrimary;

                ViewBag.EmployeeId = id.Value;
                ViewBag.EmployeeName = employeeRepository.GetEmployeeById(id.Value).FirstName;
                ViewBag.status = "Update";

            }
            else
            {
                if (commonMethods.checkaccessavailable("Employee Joining", accessId, "Create", "HR", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                employeeCls.JoiningOn = null;
                employeeCls.ID = 0;
                employeeCls.LeftOn = null;
                employeeCls.IsPrimary = false;

                ViewBag.status = "Create";
            }
            return View(employeeCls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateEmployeeJoining([Bind("ID,JoiningOn,LeftOn,IsPrimary,EmployeeId")]EmployeeTimeline timeline, long EmployeeId)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                timeline.ModifiedId = userId;
                timeline.Active = true;
                if (timeline.ID == 0)
                {
                    timeline.InsertedId = userId;
                    timeline.Status = EntityStatus.ACTIVE;
                    if (timeline.IsPrimary == true)
                    {
                        var ss = employeeRepository.GetAllEmployeeTimeline().Where(a => a.EmployeeId == timeline.EmployeeId && a.IsPrimary == true).ToList();
                        foreach (var m in ss)
                        {
                            m.IsPrimary = false;
                            m.ModifiedId = userId;
                            employeeRepository.UpdateEmployeeTimeline(m);
                        }
                    }
                    long id = employeeRepository.CreateEmployeeTimeline(timeline);

                    if (timeline.IsPrimary == true && timeline.LeftOn != null)
                    {
                        var emp = employeeRepository.GetEmployeeById(EmployeeId);
                        emp.Isleft = true;
                        emp.LeftDate = timeline.LeftOn;
                        employeeRepository.UpdateEmployee(emp);
                    }
                    else if (timeline.IsPrimary == true && timeline.LeftOn == null)
                    {
                        var emp = employeeRepository.GetEmployeeById(EmployeeId);
                        emp.Isleft = false;
                        emp.LeftDate = null;
                        employeeRepository.UpdateEmployee(emp);
                    }

                }
                else
                {
                    if (timeline.IsPrimary == true)
                    {
                        var ss = employeeRepository.GetAllEmployeeTimeline().Where(a => a.EmployeeId == timeline.EmployeeId && a.IsPrimary == true && a.ID != timeline.ID).ToList();
                        foreach (var m in ss)
                        {
                            m.IsPrimary = false;
                            m.ModifiedId = userId;
                            employeeRepository.UpdateEmployeeTimeline(m);
                        }
                    }
                    employeeRepository.UpdateEmployeeTimeline(timeline);
                    if (timeline.IsPrimary == true && timeline.LeftOn != null)
                    {
                        var emp = employeeRepository.GetEmployeeById(EmployeeId);
                        emp.Isleft = true;
                        emp.LeftDate = timeline.LeftOn;
                        employeeRepository.UpdateEmployee(emp);
                    }
                    else if (timeline.IsPrimary == true && timeline.LeftOn == null)
                    {
                        var emp = employeeRepository.GetEmployeeById(EmployeeId);
                        emp.Isleft = false;
                        emp.LeftDate = null;
                        employeeRepository.UpdateEmployee(emp);
                    }

                }

                return RedirectToAction(nameof(EmployeeJoiningDetails), new { id = EmployeeId }).WithSuccess("success", "Saved successfully");
            }
            return View(timeline);
        }
        [ActionName("DeleteEmployeeJoining")]
        public async Task<IActionResult> EmployeeJoiningDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Joining", accessId, "Delete", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var employeeAddress = employeeRepository.DeleteEmployeeTimeline(id.Value);
            return RedirectToAction(nameof(EmployeeJoiningDetails), new { id = eid }).WithSuccess("success", "Deleted successfully");
        }

        #endregion



        #region Employee Bulk Upload
        //public IActionResult Employee

        #endregion





        public async Task<JsonResult> GetBankBranchByBankId(long id)
        {
            var res = bankBranchRepository.GetAllBankBranchByBankId(id).ToList();
            return Json(res);
        }
        public async Task<JsonResult> GetEducationalQualificationByTypeId(long id)
        {
            var res = educationQualificationRepository.GetAllEducationQualificationByTypeId(id).ToList();
            return Json(res);
        }
        public async Task<JsonResult> GetOrganisationByGroupId(long id)
        {
            var res = organizationRepository.GetAllOrganization().Where(a => a.GroupID == id).ToList();
            return Json(res);
        }
        //public async Task<JsonResult> GetDepartmentByOrganisationId(long gid,long oid)
        //{
        //    var res = commonMethods.GetDepartmentsByOrganisation(gid,oid);
        //    return Json(res);
        //}
        public async Task<JsonResult> GetDesignationByDepartmentandLevelId(long did)
        {
            var res = designationRepository.GetAllDesignation().Where(a => a.DepartmentID == did).ToList();
            return Json(res);
        }
        public async Task<JsonResult> GetStateByCountryId(long id)
        {
            var res = (from a in stateRepository.GetAllState().ToList()
                       where a.CountryID == id
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        public async Task<JsonResult> GetCityByStateId(long id)
        {
            var res = (from a in cityRepository.GetAllCity().ToList()
                       where a.StateID == id
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }



    }
}