﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.Web.BillDesk;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        protected readonly PaymentManager pManager;

        public PaymentController(PaymentManager pManager)
        {
            this.pManager = pManager;
        }

        [HttpPost("callback")]
        public async Task<ActionResult<String>> ServerCallback(string msg)
        {
            pManager.ProcessPaymentStatus(msg);
            return "success";
        }

    }
}