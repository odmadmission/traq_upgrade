﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.SupportAggregate;
using OdmErp.ApplicationCore.Entities.TodoAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using RestSharp.Extensions;

namespace OdmErp.Web.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AttachmentAPIController : ControllerBase
    {
        private readonly IHostingEnvironment hostingEnvironment;
        protected readonly ISupportRepository supportRepository;
        protected readonly IToDoRepository todoRepository;
        protected readonly IAdmissionStudentRepository admissionStudentRepository;
        protected readonly IAdmissionStudentParentRepository admissionStudentParentRepository;
        public AttachmentAPIController(IAdmissionStudentParentRepository admissionStudentParentRepo,IAdmissionStudentRepository admissionStudentRepo, ISupportRepository supportRepository, IHostingEnvironment hostingEnv, IToDoRepository todoRepository)
        {
            admissionStudentParentRepository = admissionStudentParentRepo;
            admissionStudentRepository = admissionStudentRepo;
            this.supportRepository = supportRepository;
            hostingEnvironment = hostingEnv;
            this.todoRepository = todoRepository;
        }
        public class supportAttachmentModel
        {
            public long userId { get; set; }
            public long supportrequestid { get; set; }
            public IFormFile file { get; set; }
        }
        public class AttachmentModels
        {
            public int StatusCode { get; set; }
            public string Status { get; set; }
            public string Message { get; set; }
            public long ID { get; set; }




        }
        public class TodoAttachmentModel
        {
            public long userId { get; set; }
            public long taskid { get; set; }
            public IFormFile file { get; set; }
            public string attachmentname { get; set; }

        }
        [HttpGet]
        public ActionResult<string> test()
        {
            return "hii";
        }
        //[HttpPost]
        //public ActionResult<AttachmentModel> uploadfile([FromForm]IFormFile file)
        //{
        //    AttachmentModel model = new AttachmentModel();
        //    if (file != null)
        //    {
        //        // Create a File Info 
        //        FileInfo fi = new FileInfo(file.FileName);
        //        var newFilename = String.Format("{0:d}",
        //                          (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
        //        var webPath = hostingEnvironment.WebRootPath;
        //        string path = Path.Combine("", webPath + @"\ODMImages\Tempfile\" + newFilename);
        //        var pathToSave = newFilename;
        //        using (var stream = new FileStream(path, FileMode.Create))
        //        {
        //            file.CopyToAsync(stream);
        //        }

        //    }
        //    model.StatusCode = 1015;
        //    model.Status = "FAILED";
        //    return model;
        //}

        [HttpPost]
        public ActionResult<AttachmentModels> addSupportRequestAttachmentComment()
        {
            AttachmentModels model = new AttachmentModels();

            try
            {
                var userId = Convert.ToInt64(Request.Form["userId"].ToString());
                var supportrequestid = Convert.ToInt64(Request.Form["supportrequestid"].ToString());
                var file = Request.Form.Files;
                SupportAttachment Attachment = new SupportAttachment();
                Attachment.Active = true;
                Attachment.Type = "COMMENT";
                if (file[0] != null)
                {
                    FileInfo fi = new FileInfo(file[0].FileName);
                    var newFilename = String.Format("{0:d}",
                                       (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    //var newFilename = file[0].FileName;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Tempfile\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        file[0].CopyTo(stream);
                    }
                    Attachment.Path = pathToSave;
                    Attachment.Name = newFilename;

                }

                Attachment.InsertedId = userId;
                Attachment.IsAvailable = true;
                Attachment.ModifiedId = userId;
                Attachment.Status = EntityStatus.ACTIVE;
                Attachment.SupportRequestID = supportrequestid;
                long response = supportRepository.CreateAttachment(Attachment);
                model.ID = Attachment.ID;
                if (response > 0)
                {
                    model.StatusCode = 1014;
                    model.Status = "SUCCESS";

                }
                else
                {
                    model.StatusCode = 1015;
                    model.Status = "FAILED";
                }
            }
            catch (Exception e)
            {
                model.StatusCode = 1017;
                model.Status = "EXCEPTION";
                model.Message = e.Message;
            }

            return model;

        }

        public class formdatacls
        {
            public long userId { get; set; }
            public long supportrequestid { get; set; }
        }


        [HttpPost]
        public ActionResult<AttachmentModels> addSupportRequestAttachment([FromForm] long userId, [FromForm] long supportrequestid)
        {
            AttachmentModels model = new AttachmentModels();

            try
            {
                // long userId =Convert.ToInt64(HttpContext.Request.Form["userId"]);
                // long supportrequestid = Convert.ToInt64(HttpContext.Request.Form["supportrequestid"]);
                var file = HttpContext.Request.Form.Files;
                foreach (var str in file)
                {
                    SupportAttachment Attachment = new SupportAttachment();
                    Attachment.Active = true;
                    Attachment.Type = "RAISED";
                    if (file != null)
                    {

                        model.Status = str.Length + "";

                        // Create a File Info 
                        FileInfo fi = new FileInfo(str.FileName);
                        var newFilename = supportrequestid + "_" + Guid.NewGuid().ToString() + fi.Extension;


                        //var newFilename = str.FileName;
                        var webPath = hostingEnvironment.WebRootPath;
                        string path = Path.Combine("", webPath + @"\ODMImages\Tempfile\" + newFilename);
                        var pathToSave = newFilename;
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            model.Status = model.Status + "-" + newFilename;
                            str.CopyTo(stream);
                            // stream.Close();
                            model.Status = model.Status + "-" + stream.Length;
                        }
                        Attachment.Path = pathToSave;
                        Attachment.Name = newFilename;

                    }

                    Attachment.InsertedId = userId;
                    Attachment.IsAvailable = true;
                    Attachment.ModifiedId = userId;
                    Attachment.Status = EntityStatus.ACTIVE;
                    Attachment.SupportRequestID = supportrequestid;
                    long response = supportRepository.CreateAttachment(Attachment);
                    model.ID = Attachment.ID;
                    if (response > 0)
                    {
                        model.StatusCode = 1014;
                        model.Status = model.Status + "-SUCCESS";

                    }
                    else
                    {
                        model.StatusCode = 1015;
                        model.Status = model.Status + "-FAILED";
                    }
                }

            }
            catch (Exception e)
            {
                model.StatusCode = 1017;
                model.Status = "EXCEPTION";
                model.Message = e.Message;
            }

            return model;

        }


        [HttpPost]
        public ActionResult<AttachmentModels> addSupportRequestAttachmentios([FromForm] long userId, [FromForm] long supportrequestid)
        {
            AttachmentModels model = new AttachmentModels();

            try
            {
                //long userId = userId;
                //long supportrequestid = supportrequestid;

                var file = HttpContext.Request.Form.Files;
                foreach (var str in file)
                {
                    SupportAttachment Attachment = new SupportAttachment();
                    Attachment.Active = true;
                    Attachment.Type = "RAISED";
                    if (file != null)
                    {
                        // Create a File Info 
                        FileInfo fi = new FileInfo(str.FileName);
                        var newFilename = String.Format("{0:d}",
                                          (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;


                        //var newFilename = str.FileName;
                        var webPath = hostingEnvironment.WebRootPath;
                        string path = Path.Combine("", webPath + @"\ODMImages\Tempfile\" + newFilename);
                        var pathToSave = newFilename;
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            str.CopyToAsync(stream);
                        }
                        Attachment.Path = pathToSave;
                        Attachment.Name = newFilename;

                    }

                    Attachment.InsertedId = userId;
                    Attachment.IsAvailable = true;
                    Attachment.ModifiedId = userId;
                    Attachment.Status = EntityStatus.ACTIVE;
                    Attachment.SupportRequestID = supportrequestid;
                    long response = supportRepository.CreateAttachment(Attachment);
                    model.ID = Attachment.ID;
                    if (response > 0)
                    {
                        model.StatusCode = 1014;
                        model.Status = "SUCCESS";

                    }
                    else
                    {
                        model.StatusCode = 1015;
                        model.Status = "FAILED";
                    }
                }

            }
            catch (Exception e)
            {
                model.StatusCode = 1017;
                model.Status = "EXCEPTION";
                model.Message = e.Message;
            }

            return model;

        }

        #region Create Attachment in todo         

        [HttpPost]
        public ActionResult<AttachmentModels> addTaskAttachment([FromForm] long userId, [FromForm] long taskid, [FromForm] string attachmentname)
        {
            AttachmentModels model = new AttachmentModels();
            try
            {
                var file = HttpContext.Request.Form.Files;
                foreach (var str in file)
                {
                    TodoAttachment todoAttachment = new TodoAttachment();
                    long id = 0;
                    todoAttachment.Active = true;

                    if (file != null)
                    {
                        // Create a File Info 
                        FileInfo fi = new FileInfo(str.FileName);
                        var newFilename = taskid + "_" + String.Format("{0:d}",
                                            (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                        // var newFilename = file.FileName;
                        var webPath = hostingEnvironment.WebRootPath;
                        string path = Path.Combine("", webPath + @"\ODMImages\ToDoAttachments\" + newFilename);
                        var pathToSave = newFilename;
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            str.CopyTo(stream);
                        }
                        todoAttachment.Attachmenturl = pathToSave;
                        todoAttachment.AttachmentName = newFilename;
                    }
                    todoAttachment.InsertedId = userId;
                    todoAttachment.IsAvailable = true;
                    todoAttachment.IsRead = false;
                    todoAttachment.ModifiedId = userId;
                    todoAttachment.Status = "Uploaded";
                    todoAttachment.TodoID = taskid;
                    var res = todoRepository.CreateTodoAttachment(todoAttachment);
                    id = todoAttachment.ID;
                    if (res != 0)
                    {
                        model.StatusCode = 1014;
                        model.Status = "SUCCESS";
                    }
                    else
                    {
                        model.StatusCode = 1015;
                        model.Status = "FAILED";
                    }

                }

            }
            catch (Exception e)
            {
                model.StatusCode = 1017;
                model.Status = "EXCEPTION";
                model.Message = e.Message;
            }

            return model;
        }

        [HttpPost]
        public ActionResult<AttachmentModels> addTaskAttachmentios([FromForm] long userId, [FromForm] long taskid, [FromForm] string attachmentname)
        {
            AttachmentModels model = new AttachmentModels();
            try
            {
                var file = HttpContext.Request.Form.Files;

                foreach (var str in file)
                {
                    TodoAttachment todoAttachment = new TodoAttachment();
                    long id = 0;
                    todoAttachment.Active = true;
                    todoAttachment.AttachmentName = attachmentname;
                    if (file != null)
                    {
                        // Create a File Info 
                        FileInfo fi = new FileInfo(str.FileName);
                        var newFilename = taskid + "_" + Guid.NewGuid().ToString() + fi.Extension;
                        // var newFilename = file.FileName;
                        var webPath = hostingEnvironment.WebRootPath;
                        string path = Path.Combine("", webPath + @"\ODMImages\ToDoAttachments\" + newFilename);
                        var pathToSave = newFilename;
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            str.CopyTo(stream);
                        }
                        todoAttachment.Attachmenturl = pathToSave;
                    }
                    todoAttachment.InsertedId = userId;
                    todoAttachment.IsAvailable = true;
                    todoAttachment.IsRead = false;
                    todoAttachment.ModifiedId = userId;
                    todoAttachment.Status = "Uploaded";
                    todoAttachment.TodoID = taskid;
                    var res = todoRepository.CreateTodoAttachment(todoAttachment);
                    id = todoAttachment.ID;
                    if (res != 0)
                    {
                        model.StatusCode = 1014;
                        model.Status = "SUCCESS";
                    }
                    else
                    {
                        model.StatusCode = 1015;
                        model.Status = "FAILED";
                    }

                }

            }
            catch (Exception e)
            {
                model.StatusCode = 1017;
                model.Status = "EXCEPTION";
                model.Message = e.Message;
            }

            return model;
        }

        #endregion


        #region Student Profile
        [Route("UploadAdmissionStudentProfile")]
        [HttpPost]
        public ActionResult<bool> AdmissionStudentProfileupload([FromForm] long admissionstudentid)
        {
            var str = HttpContext.Request.Form.Files;
            if (str != null && str.Count > 0)
            {
                AdmissionStudent studentobj = admissionStudentRepository.GetByIdAsync(admissionstudentid).Result;
                if (studentobj == null)
                {

                    // Create a File Info 
                    FileInfo fi = new FileInfo(str[0].FileName);
                    var newFilename = admissionstudentid + "_" + String.Format("{0:d}",
                                        (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    // var newFilename = file.FileName;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + studentobj.AdmissionId + "\\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        str[0].CopyTo(stream);
                    }
                    studentobj.Image = pathToSave;
                    studentobj.ModifiedDate = DateTime.Now;
                    studentobj.ModifiedId = admissionstudentid;
                    admissionStudentRepository.UpdateAsync(studentobj);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        [Route("UploadAdmissionStudentFather")]
        [HttpPost]
        public ActionResult<bool> AdmissionStudentFatherProfileupload([FromForm] long admissionStudentFatherID)
        {
            var str = HttpContext.Request.Form.Files;
            if (str != null && str.Count > 0)
            {
                AdmissionStudentParent studentobj = admissionStudentParentRepository.GetByIdAsync(admissionStudentFatherID).Result;
                var admissionstudent = admissionStudentRepository.GetByIdAsync(studentobj.AdmissionStudentId);
                if (studentobj == null)
                {

                    // Create a File Info 
                    FileInfo fi = new FileInfo(str[0].FileName);
                    var newFilename = "Father" + String.Format("{0:d}",
                                        (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    // var newFilename = file.FileName;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + admissionstudent.Result.AdmissionId + "\\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        str[0].CopyTo(stream);
                    }
                    studentobj.Image = pathToSave;
                    studentobj.ModifiedDate = DateTime.Now;
                    studentobj.ModifiedId = admissionstudent.Result.AdmissionId;
                    admissionStudentParentRepository.UpdateAsync(studentobj);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        [Route("UploadAdmissionStudentMother")]
        [HttpPost]
        public ActionResult<bool> AdmissionStudentMotherProfileupload([FromForm] long admissionStudentMotherID)
        {
            var str = HttpContext.Request.Form.Files;
            if (str != null && str.Count > 0)
            {
                AdmissionStudentParent studentobj = admissionStudentParentRepository.GetByIdAsync(admissionStudentMotherID).Result;
                var admissionstudent = admissionStudentRepository.GetByIdAsync(studentobj.AdmissionStudentId);
                if (studentobj == null)
                {

                    // Create a File Info 
                    FileInfo fi = new FileInfo(str[0].FileName);
                    var newFilename = "Mother" + String.Format("{0:d}",
                                        (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    // var newFilename = file.FileName;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + admissionstudent.Result.AdmissionId + "\\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        str[0].CopyTo(stream);
                    }
                    studentobj.Image = pathToSave;
                    studentobj.ModifiedDate = DateTime.Now;
                    studentobj.ModifiedId = admissionstudent.Result.AdmissionId;
                    admissionStudentParentRepository.UpdateAsync(studentobj);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
    }