﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.DTO
{
    public class ViewMethod
    {


        public static string CREATE = "Create";
        public static string UPDATE = "Update";


    }

    public class ViewConstants
    {


        public static string NOT_APPLICABLE = "NA";
        public static string ARROW = "->";


    }

    public class LoginSessionData
    {


        public static string ACCESS_ID = "accessId";
        public static string USER_ID = "userId";
        public static string ROLE_ID = "roleId";

        public static string MOBILE = "mobile";
        public static string UNIQUE_ID = "uniqueid";
        public static string LOGIN_ID = "loginid";
        public static string OTP_ID = "otp_code";

    }

    public class SupportAttachmentType
    {


        public static string RAISED = "RAISED";
        public static string COMMENT = "COMMENT";
    }
}
