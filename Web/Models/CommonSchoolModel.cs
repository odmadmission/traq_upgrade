﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Web.Models.SchoolBelq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OdmErp.Web.Models;
using OdmErp.ApplicationCore.Entities.SessionFeesType;

namespace OdmErp.Web.Models
{
    public class CommonSchoolModel
    {

        public IBoardRepository boardRepository;
        public IStandardRepository standardRepository;
        public IStreamRepository streamRepository;
        public IBoardStandardRepository boardStandardRepository;
        public ISubjectRepository subjectRepository;
        public IChapterRepository chapterRepository;
        public IAcademicSessionRepository academicSessionRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IOrganizationRepository organizationRepository;
        public IAcademicSubjectRepository academicSubjectRepository;
        // public IBoardStandardStreamRepository boardStandardStreamRepository;
        public IConceptRepository conceptRepository;
        public IAcademicStandardRepository academicStandardRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private CommonMethods commonMethods;
        public ISectionRepository sectionRepository;
        public IAcademicSectionRepository academicSectionRepository;
        // public IAcademicStandardStreamRepository academicStandardStreamRepository;
        public IAcademicConceptRepository academicConceptRepository;
        public IAcademicChapterRepository academicChapterRepository;
        public IAcademicTeacherSubjectRepository academicTeacherSubjectRepository;
        public ITeacherRepository teacherRepository;
        public IEmployeeRepository employeeRepository;

        public IAcademicStudentRepository academicStudentRepository;





        public IDepartmentRepository departmentRepository;
        public IDesignationRepository designationRepository;

        public IFeesTypeRepository feestypeRepo;
        public ISessionFeesType sessionFeestypeRepo;
        public IFeesTypeApplicableRepository applicableTypeRepo;
        public IFeesTypePrice feesTypePriceDataRepo;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IWingRepository wingRepository;

        private IPaymentCollectionType paymentCollectionTyperepo;
        public CommonSchoolModel(IWingRepository wingRepo, IBoardStandardWingRepository boardStandardWingRepo, IAcademicStandardWingRepository academicStandardWingRepo, IBoardRepository boardRepository, IStandardRepository standardRepository, IStreamRepository streamRepository,
             IBoardStandardRepository boardStandardRepository, ISubjectRepository subjectRepository, IAcademicTeacherSubjectRepository academicTeacherSubjectRepository,
             IAcademicSessionRepository academicSessionRepository, IOrganizationAcademicRepository organizationAcademicRepository,

             IOrganizationRepository organizationRepository, IAcademicSubjectRepository academicSubjectRepository,
             IChapterRepository chapterRepository,
             IConceptRepository conceptRepository, IAcademicStandardRepository academicStandardRepository,
             IStudentAggregateRepository studentAggregateRepository, CommonMethods commonMethods,
             ISectionRepository sectionRepository, IAcademicSectionRepository academicSectionRepository, IPaymentCollectionType paymentCollectionTyperepo,

             ITeacherRepository teacherRepository,
             IAcademicConceptRepository academicConceptRepository, IAcademicChapterRepository academicChapterRepository,
              IEmployeeRepository employeeRepository, IAcademicStudentRepository academicStudentRepository,

              IAcademicStudentRepository academicStudentRepo,
             IAcademicConceptRepository academicConceptRepo, IAcademicChapterRepository academicChapterRepo,


              IDesignationRepository designationRepository,
              IDepartmentRepository departmentRepository,

              IFeesTypeRepository feestypeRepository,
              ISessionFeesType sessionFeestypeRepository,
              IFeesTypeApplicableRepository applicableTypeRepository,
              IFeesTypePrice feesTypePriceDataRepository
            )

        {
            wingRepository = wingRepo;
            boardStandardWingRepository = boardStandardWingRepo;
            academicStandardWingRepository = academicStandardWingRepo;
            academicConceptRepository = academicConceptRepo;
            this.boardRepository = boardRepository;
            this.standardRepository = standardRepository;
            this.streamRepository = streamRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.subjectRepository = subjectRepository;
            this.chapterRepository = chapterRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.organizationRepository = organizationRepository;
            this.academicSubjectRepository = academicSubjectRepository;
            // this.boardStandardStreamRepository = boardStandardStreamRepository;
            this.conceptRepository = conceptRepository;
            this.academicStandardRepository = academicStandardRepository;
            this.studentAggregateRepository = studentAggregateRepository;
            this.commonMethods = commonMethods;
            this.sectionRepository = sectionRepository;
            this.academicSectionRepository = academicSectionRepository;

            // this.academicStandardStreamRepository = academicStandardStreamRepository;
            this.academicConceptRepository = academicConceptRepository;
            this.academicChapterRepository = academicChapterRepository;
            this.academicTeacherSubjectRepository = academicTeacherSubjectRepository;
            this.teacherRepository = teacherRepository;
            this.employeeRepository = employeeRepository;
            this.academicStudentRepository = academicStudentRepository;
            academicStudentRepository = academicStudentRepo;
            academicChapterRepository = academicChapterRepo;

            // this.academicStandardStreamRepository = academicStandardStreamRepository;

            this.departmentRepository = departmentRepository;
            this.designationRepository = designationRepository;
            this.academicStudentRepository = academicStudentRepository;
            this.academicTeacherSubjectRepository = academicTeacherSubjectRepository;
            this.teacherRepository = teacherRepository;
            this.employeeRepository = employeeRepository;

            this.feestypeRepo = feestypeRepository;
            this.sessionFeestypeRepo = sessionFeestypeRepository;
            this.applicableTypeRepo = applicableTypeRepository;
            this.feesTypePriceDataRepo = feesTypePriceDataRepository;
            this.paymentCollectionTyperepo = paymentCollectionTyperepo;
        }
        public List<AcademicStandardWingcls> GetacademicstandardWings(long AcademicStandardId)
        {
            var academicstandardwings = academicStandardWingRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStandardId == AcademicStandardId).ToList();
            var boardstandardwings = boardStandardWingRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var wings = wingRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var res = (from a in academicstandardwings
                       join b in boardstandardwings on a.BoardStandardWingID equals b.ID
                       join c in wings on b.WingID equals c.ID
                       select new AcademicStandardWingcls
                       {
                           AcademicStandardID = a.AcademicStandardId,
                           BoardStandardID = b.BoardStandardID,
                           BoardStandardWingId = a.BoardStandardWingID,
                           ID = a.ID,
                           ModifiedDate = a.ModifiedDate,
                           Name = c.Name
                       }).ToList();
            return res;
        }
        public List<CommonMasterModel> GetWingsListByAcademicStandardId(long AcademicStandardId, long boardstandardid)
        {
            var academicstandardwings = academicStandardWingRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStandardId == AcademicStandardId).Select(a => a.BoardStandardWingID).ToList();
            var boardstandardwings = boardStandardWingRepository.ListAllAsync().Result.Where(a => a.Active == true && a.BoardStandardID == boardstandardid).ToList();
            var boardstandardids = boardstandardwings.Select(a => a.ID).ToList().Except(academicstandardwings).ToList();
            var standardwings = boardstandardwings.Where(a => boardstandardids.Contains(a.ID)).ToList();
            var wings = wingRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var res = (from a in standardwings
                       join b in wings on a.WingID equals b.ID
                       select new CommonMasterModel { ID = a.ID, Name = b.Name }).ToList();
            return res;
        }
        public IEnumerable<BoardStandardViewModel> GetAllBoardStandard()
        {
            try
            {
                var boardStandardList = boardStandardRepository.ListAllAsyncIncludeAll().Result.ToList();
                var boardList = boardRepository.GetAllBoard();
                var standardList = standardRepository.GetAllStandard();
                //var streamList = streamRepository.GetAllStream();

                var res = (from a in boardStandardList
                           join b in boardList on a.BoardId equals b.ID
                           join c in standardList on a.StandardId equals c.ID
                           //join d in streamList on a.StreamId equals d.ID
                           select new BoardStandardViewModel
                           {
                               ID = a.ID,
                               BoardID = a.BoardId,
                               BoardName = b.Name,
                               //a.BoardId != 0 ? boardList.FirstOrDefault().Name : "",
                               StandardID = a.StandardId,
                               Status = a.Status,
                               StandardName = c.Name,
                               //a.StandardId != 0 ? standardList.FirstOrDefault().Name : "",

                               //StreamID = a.StreamId,
                               //StreamName = d.Name
                           }

                          ).ToList();


                return res;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public IEnumerable<AcademicStandardViewModel> GetAllAcademicStandard()
        {
            try
            {
                var AcademicStandard = academicStandardRepository.ListAllAsyncIncludeAll().Result;
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result;
                var Organisation = organizationRepository.GetAllOrganization();

                var res = (from a in AcademicStandard
                           join b in OrganisationAcademic on a.OrganizationAcademicId equals b.ID

                           select new AcademicStandardViewModel
                           {
                               ID = a.ID,
                               OraganisationAcademicId = a.OrganizationAcademicId,
                               BoardStandardId = a.BoardStandardId,
                               OraganisationName = organizationRepository.GetAllOrganization().Where(c => c.ID == a.OrganizationAcademicId).Select(c => c.Name).FirstOrDefault(),
                               AcademicSessionId = b.ID,
                               SessionName = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(c => c.ID == b.ID).Select(c => c.DisplayName).FirstOrDefault(),
                               StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.BoardStandardId).Select(e => e.StandardId).FirstOrDefault(),
                               StandardName = standardRepository.GetAllStandard().Where(e => e.ID == boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.BoardStandardId).Select(g => g.StandardId).FirstOrDefault()).Select(e => e.Name).FirstOrDefault(),
                               StandardStreamType = standardRepository.GetAllStandard().Where(e => e.ID == boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.BoardStandardId).Select(g => g.StandardId).FirstOrDefault()).Select(e => e.StreamType).FirstOrDefault(),
                               //SchoolWingId = a.SchoolWingId,
                               Status = a.Status,
                               ModifiedDate = a.ModifiedDate,
                               //WingName = commonMethods.GetWing().Where(d => d.ID == commonMethods.GetSchoolWing().Where(c => c.ID == a.SchoolWingId).Select(c => c.WingID).FirstOrDefault()).Select(d => d.Name).FirstOrDefault(),
                               CountAcademicSection = academicSectionRepository.ListAllAsyncIncludeAll().Result.Where(c => c.AcademicStandardId == a.ID).Count(),
                               //CountAcademicStandardStream = academicStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(c => c.AcademicStandardId == a.ID).Count(),

                           }

                          ).ToList();


                return res;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public IEnumerable<AcademicSectionViewModel> GetAllAcademicSection()
        {
            var AvademicSectionList = academicSectionRepository.ListAllAsyncIncludeAll().Result;
            var SectionList = sectionRepository.GetAllSection();
            if (AvademicSectionList != null)
            {
                var res = (from a in AvademicSectionList
                           join b in SectionList on a.SectionId equals b.ID
                           select new AcademicSectionViewModel
                           {
                               ID = a.ID,
                               Status = a.Status,
                               SectionId = a.SectionId,
                               AcademicStandardId = a.AcademicStandardId,
                               //AcademicStandardStreamId = a.AcademicStandardStreamId,
                               SessionName = b.Name,
                               ModifiedDate = a.ModifiedDate,
                           }

                           ).ToList();
                return res;
            }
            else
            {
                return null;
            }
        }

        //public IEnumerable<AcademicSectionViewModel> GetAllAcademicStandardSectionbyStream(long AcademicStandardStreamId)
        //{
        //    var AvademicSectionList = academicSectionRepository.ListAllAsyncIncludeAll().Result.Where(r => r.AcademicStandardStreamId == AcademicStandardStreamId);
        //    var AcademicStandardStreamList = academicStandardStreamRepository.ListAllAsyncIncludeAll().Result;

        //    var res = (from a in AvademicSectionList
        //               join b in AcademicStandardStreamList on a.AcademicStandardStreamId equals b.ID
        //               select new AcademicSectionViewModel
        //               {
        //                   ID = a.ID,
        //                   Status = a.Status,
        //                   SectionId = a.SectionId,
        //                   SessionName = sectionRepository.GetAllSection().Where(c => c.ID == a.SectionId).Select(c => c.Name).FirstOrDefault(),
        //                   AcademicStandardId = a.AcademicStandardId,
        //                   StreamName = streamRepository.GetAllStream().Where(c => c.ID == boardStandardStreamRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == b.BoardStandardStreamId).Select(d => d.StreamId).FirstOrDefault()).Select(c => c.Name).FirstOrDefault(),
        //                   ModifiedDate = a.ModifiedDate,
        //               }

        //               ).ToList();

        //    return res;
        //}




        //public IEnumerable<AcademicStandardStreamViewModel> GetAllAcademicstandardStream()
        //{
        //    var AvademicStandardStreamList = academicStandardStreamRepository.ListAllAsyncIncludeAll().Result;
        //    var BoardStradardStreamList = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result;

        //    var res = (from a in AvademicStandardStreamList
        //               join b in BoardStradardStreamList on a.BoardStandardStreamId equals b.ID
        //               select new AcademicStandardStreamViewModel
        //               {
        //                   ID = a.ID,
        //                   Status = a.Status,
        //                   BoardStandardStreamId = a.BoardStandardStreamId,
        //                   AcademicStandardId = a.AcademicStandardId,
        //                   BoardStandardId = b.BoardStandardId,
        //                   StreamId = b.StreamId,
        //                   StreamName = streamRepository.GetAllStream().Where(c => c.ID == b.StreamId).Select(c => c.Name).FirstOrDefault(),
        //                   ModifiedDate = a.ModifiedDate,
        //                   CountSection = academicSectionRepository.ListAllAsyncIncludeAll().Result.Where(c => c.AcademicStandardStreamId == a.ID && c.AcademicStandardStreamId != 0).Count(),

        //               }

        //               ).ToList();

        //    return res;
        //}




        public IEnumerable<SubjectViewModel> GetAllSubjects()
        {
            var subList = subjectRepository.ListAllAsyncIncludeAll().Result;
            var boardStandardList = boardStandardRepository.ListAllAsyncIncludeAll().Result;
            var res = (from a in subList
                       join b in boardStandardList on a.BoardStandardId equals b.ID
                       select new SubjectViewModel
                       {
                           ID = a.ID,
                           Name = a.Name,
                           Status = a.Status,
                           // ParentSubjectId = a.ParentSubjectId,
                           SubjectCode = a.SubjectCode,
                           BoardId = b.BoardId,
                           BoardStandardId = b.ID,
                           CountChapter = chapterRepository.GetChapterCountByBoardStandardId(a.ID)
                       }

                       ).ToList();

            return res;
        }


        public IEnumerable<SubjectViewModel> GetAllStreamSubjects()

        {
            // var subList = subjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.BoardStandardStreamId== BoardStandardStreamId);
            var subList = subjectRepository.ListAllAsyncIncludeAll().Result;
            //var boardStandardStreamList = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result;

            var res = (from a in subList
                           //join c in boardStandardStreamList on a.BoardStandardStreamId equals c.ID
                       select new SubjectViewModel
                       {
                           ID = a.ID,
                           Name = a.Name,
                           Status = a.Status,
                           IsOptional = a.IsOptional,
                           // ParentSubjectId = a.ParentSubjectId,
                           SubjectCode = a.SubjectCode,
                           BoardStandardId = a.BoardStandardId,
                           BoardId = boardStandardRepository.GetBoardIdByBoardStandardId(a.BoardStandardId),
                           //BoardStandardStreamId = c.ID,
                           CountChapter = chapterRepository.GetChapterCountByBoardStandardId(a.ID)

                       }

                       ).ToList();

            return res;
        }
        #region Chapter
        public IEnumerable<ChapterViewModel> GetAllChapters()
        {
            var chapterList = chapterRepository.ListAllAsyncIncludeAll().Result;
            var subList = subjectRepository.ListAllAsyncIncludeAll().Result;

            var res = (from a in chapterList
                       join b in subList on a.SubjectId equals b.ID
                       select new ChapterViewModel
                       {
                           ID = a.ID,
                           Name = a.Name,
                           ParentChapterId = a.ParentChapterId,
                           ParentChapterName = a.Name,
                           SubjectId = b.ID,
                           SubjectName = b.Name,
                           IsSubChapter = a.IsSubChapter,
                           ModifiedDate = a.ModifiedDate,
                           BoardStandardId = b.BoardStandardId,
                           //BoardStandardStreamId = b.BoardStandardStreamId,
                           Status = a.Status,
                           BoardId = boardStandardRepository.GetBoardIdByBoardStandardId(b.BoardStandardId),
                           CountConcept = conceptRepository.GetConceptCountBychapterid(a.ID),
                       }).ToList();

            return res;
        }


        public IEnumerable<ChapterViewModel> GetAllChaptersStreamwise()
        {
            var chapterList = chapterRepository.ListAllAsyncIncludeAll().Result;
            var subList = subjectRepository.ListAllAsyncIncludeAll().Result;
            var BoardStandardList = boardStandardRepository.ListAllAsyncIncludeAll().Result;
            var res = (from a in chapterList
                       join b in subList on a.SubjectId equals b.ID
                       join c in BoardStandardList on b.ID equals c.ID
                       select new ChapterViewModel
                       {
                           ID = a.ID,
                           Name = a.Name,
                           ParentChapterId = a.ParentChapterId,
                           ParentChapterName = a.Name,
                           SubjectId = b.ID,
                           SubjectName = b.Name,
                           IsSubChapter = a.IsSubChapter,
                           ModifiedDate = a.ModifiedDate,
                           BoardStandardId = b.BoardStandardId,
                           //BoardStandardStreamId = b.BoardStandardStreamId,
                           Status = a.Status,
                           BoardId = boardStandardRepository.GetBoardIdByBoardStandardId(b.BoardStandardId),
                           CountConcept = conceptRepository.GetConceptCountBychapterid(a.ID),
                       }).ToList();

            return res;
        }




        #endregion
        public IEnumerable<OrganizationAcademicViewModel> GetAllOrganisationAcademic()
        {
            try
            {

                var OrganisationList = organizationRepository.GetAllOrganization();
                // var AcademicSessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result;
                var OrganisationAcademicList = organizationAcademicRepository.ListAllAsyncIncludeAll().Result;

                var res = (from a in OrganisationAcademicList
                               //join b in AcademicSessionList on a.AcademicSessionId equals b.ID
                           join c in OrganisationList on a.OrganizationId equals c.ID
                           select new OrganizationAcademicViewModel
                           {
                               ID = a.ID,
                               AcademicSessionID = a.AcademicSessionId,
                               OrganisationID = a.OrganizationId,
                               //AcademicSessionName = b.DisplayName,
                               OrganisationName = c.Name,
                               ModifiedDate = a.ModifiedDate,
                               Status = a.Status,
                               //CountSchoolWing = commonMethods.GetSchoolWing().Where(d => d.OrganizationAcademicID == a.ID).Count(),

                           }

                           ).ToList();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public (string BoardName, string ClassName, string StreamName) GetBoardClassStream(long BoardStandardId, long BoardStandardStreamId)
        {


            BoardStandard bs = boardStandardRepository.GetByIdAsync(BoardStandardId).Result;


            string board = boardRepository.GetBoardById(bs.BoardId).Name;
            string standard = standardRepository.GetStandardById(bs.StandardId).Name;
            string stream = null;
            //if (BoardStandardStreamId > 0)
            //{
            //    BoardStandardStream bss = boardStandardStreamRepository.GetByIdAsync(BoardStandardStreamId).Result;
            //    stream = streamRepository.GetStreamById(bss.StreamId).Name;
            //}




            return (board, standard, stream);
        }

        public IEnumerable<AcademicSubjectViewModel> GetAllAcademicSubject()
        {
            try
            {
                var AcademicSubjectList = academicSubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
                var AcademicSessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
                var SubjectList = subjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
                var boardstandard = boardStandardRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var board = boardRepository.GetAllBoard().Where(a => a.Active == true).ToList();
                var standard = standardRepository.GetAllStandard().Where(a => a.Active == true).ToList();

                var res = (from a in AcademicSubjectList
                           join b in AcademicSessionList on a.AcademicSessionId equals b.ID
                           join c in SubjectList on a.SubjectId equals c.ID
                           join d in boardstandard on c.BoardStandardId equals d.ID
                           join e in board on d.BoardId equals e.ID
                           join f in standard on d.StandardId equals f.ID
                           select new AcademicSubjectViewModel
                           {
                               ID = a.ID,
                               SubjectCode = c.SubjectCode,
                               AcademicSessionID = a.AcademicSessionId,
                               SubjectID = a.SubjectId,
                               AcademicSessionName = b.DisplayName,
                               SubjectName = c.Name,
                               ModifiedDate = a.ModifiedDate,
                               Status = a.Status,
                               BoardClassStream = (e.Name, f.Name, "")
                               //BoardClassStream = GetBoardClassStream(c.BoardStandardId, c.BoardStandardStreamId)

                           }

                           ).ToList();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public IEnumerable<AcademicChapterViewModel> GetAllAcademicChapter()
        {
            try
            {
                var AcademicChapterList = academicChapterRepository.ListAllAsyncIncludeAll().Result;
                var AcademicSubjectList = academicSubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).Select(a => a.SubjectId).ToList();
                var chapterList = chapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true && AcademicSubjectList.Contains(a.SubjectId)).ToList();


                var res = (from a in AcademicChapterList
                           join b in chapterList on a.ChapterId equals b.ID

                           select new AcademicChapterViewModel
                           {
                               ID = a.ID,
                               ChapterId = a.ChapterId,
                               ChapterName = b.Name,
                               AcademicSubjectId = a.AcademicSubjectId,
                               SubjectName = subjectRepository.ListAllAsyncIncludeAll().Result.Where(c => c.ID == academicSubjectRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.AcademicSubjectId).Select(d => d.SubjectId).FirstOrDefault()).Select(c => c.Name).FirstOrDefault(),
                               ModifiedDate = a.ModifiedDate,
                               Status = a.Status,
                               CountAcademicConcept = academicConceptRepository.ListAllAsyncIncludeAll().Result.Where(c => c.AcademicChapterId == a.ID).Count(),
                           }

                           ).ToList();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }

        }


        public IEnumerable<AcademicConceptViewModel> GetAllAcademicConcept()
        {
            try
            {
                var AcademicConceptList = academicConceptRepository.ListAllAsyncIncludeAll().Result;
                var ConceptList = conceptRepository.ListAllAsyncIncludeAll().Result;


                var res = (from a in AcademicConceptList
                           join b in ConceptList on a.ConceptId equals b.ID

                           select new AcademicConceptViewModel
                           {
                               ID = a.ID,
                               ConceptId = a.ConceptId,
                               ConceptName = b.Name,
                               AcademicChapterId = a.AcademicChapterId,
                               ChapterName = chapterRepository.ListAllAsyncIncludeAll().Result.Where(c => c.ID == academicChapterRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.AcademicChapterId).Select(d => d.ChapterId).FirstOrDefault()).Select(c => c.Name).FirstOrDefault(),
                               ModifiedDate = a.ModifiedDate,
                               Status = a.Status,
                           }

                           ).ToList();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }

        }




        //public IEnumerable<BoardStandardStreamViewModel> GetAllBoardStandardStream()
        //{
        //    try
        //    {

        //        var boardStandardStreamList = boardStandardStreamRepository.ListAllAsyncIncludeAll().Result;
        //        var StreamList = streamRepository.GetAllStream();
        //        var boardStandardList = boardStandardRepository.ListAllAsyncIncludeAll().Result;
        //        var res = (from a in boardStandardStreamList
        //                   join b in boardStandardList on a.BoardStandardId equals b.ID
        //                   join c in StreamList on a.StreamId equals c.ID
        //                   select new BoardStandardStreamViewModel
        //                   {
        //                       ID = a.ID,
        //                       StreamId = a.StreamId,
        //                       BoardStandardId = a.BoardStandardId,
        //                       BoardId = b.BoardId,
        //                       StandardId = b.StandardId,
        //                       StreamName = c.Name,
        //                       ModifiedDate = a.ModifiedDate,
        //                       Status = a.Status,

        //                   }

        //                   ).ToList();

        //        return res;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }

        //}

        #region //Concept

        public IEnumerable<CoceptViewModel> GetAllConcept()
        {
            var ConceptList = conceptRepository.ListAllAsyncIncludeAll().Result;
            var chapterList = chapterRepository.ListAllAsyncIncludeAll().Result;
            var SubList = subjectRepository.ListAllAsyncIncludeAll().Result;


            var res = (from a in ConceptList
                       join b in chapterList on a.ChapterId equals b.ID
                       join c in SubList on b.SubjectId equals c.ID
                       select new CoceptViewModel
                       {
                           ID = a.ID,
                           Name = a.Name,
                           Chapter = b.Name,
                           ChapterId = b.ID,
                           ParentConceptId = a.ParentConceptId,
                           ParentConceptName = a.Name,
                           SubjectId = b.ID,
                           SubjectName = b.Name,
                           IsSubConcept = a.IsSubConcept,
                           ModifiedDate = a.ModifiedDate,
                           BoardStandardId = c.BoardStandardId,
                           //BoardStandardStreamId = c.BoardStandardStreamId,
                           Status = a.Status,
                           BoardId = boardStandardRepository.GetBoardIdByBoardStandardId(c.BoardStandardId),

                       }).ToList();

            return res;
        }

        public IEnumerable<CoceptViewModel> GetAllConceptsStreamwise()
        {
            var ConceptList = conceptRepository.ListAllAsyncIncludeAll().Result;
            var chapterList = chapterRepository.ListAllAsyncIncludeAll().Result;
            var subList = subjectRepository.ListAllAsyncIncludeAll().Result;
            var BoardStandardList = boardStandardRepository.ListAllAsyncIncludeAll().Result;
            var res = (from a in ConceptList
                       join b in chapterList on a.ChapterId equals b.ID
                       join c in subList on b.SubjectId equals c.ID
                       join d in BoardStandardList on c.ID equals d.ID
                       select new CoceptViewModel
                       {
                           ID = a.ID,
                           Name = a.Name,
                           ParentConceptId = a.ParentConceptId,
                           ParentConceptName = a.Name,
                           SubjectId = b.ID,
                           SubjectName = b.Name,
                           IsSubConcept = a.IsSubConcept,
                           ModifiedDate = a.ModifiedDate,
                           BoardStandardId = c.BoardStandardId,
                           //BoardStandardStreamId = c.BoardStandardStreamId,
                           Status = a.Status,
                           BoardId = boardStandardRepository.GetBoardIdByBoardStandardId(c.BoardStandardId),
                           CountConcept = conceptRepository.GetConceptCountBychapterid(a.ID),
                       }).ToList();

            return res;
        }

        #endregion


        public IEnumerable<AcademicTeacherSubjectViewModel> GetAllAcademicTearcherSubject()
        {
            try
            {

                var AcademicTeacherSubjects = academicTeacherSubjectRepository.ListAllAsyncIncludeAll().Result;
                var academicsubjectList = academicSubjectRepository.ListAllAsyncIncludeAll().Result;
                var employeeList = teacherRepository.ListAllAsyncIncludeAll();
                var res = (from a in AcademicTeacherSubjects
                           join b in academicsubjectList on a.AcademicSubjectId equals b.ID

                           select new AcademicTeacherSubjectViewModel
                           {
                               ID = a.ID,
                               InsertedDate = a.InsertedDate,
                               InsertedId = a.InsertedId,
                               SubjectId = b.SubjectId,
                               AcademicSubjectId = a.AcademicSubjectId,
                               SubjectName = subjectRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == b.SubjectId).Select(d => d.Name).FirstOrDefault(),
                               TeacherId = a.TeacherId,
                               TeacherName = employeeRepository.GetAllEmployee().Where(e => e.ID == teacherRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.TeacherId).Select(d => d.EmployeeId).FirstOrDefault()).Select(e => e.FirstName).FirstOrDefault(),
                               ModifiedDate = a.ModifiedDate,
                               AcademicSectiondId = a.AcademicSectionId,
                               SectionName = sectionRepository.GetAllSection().Where(e => e.ID == academicSectionRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.AcademicSectionId).Select(d => d.SectionId).FirstOrDefault()).Select(e => e.Name).FirstOrDefault(),
                               Status = a.Status,

                           }

                           ).ToList();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }

        }


        public IEnumerable<AcademicStudentViewModel> GetAllAcademicStudents()
        {
            try
            {
                var AcademicStudents = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                var students = studentAggregateRepository.GetAllStudent();
                var AcademicSession = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.IsAvailable == true).ToList();
                var AcademicStandard = academicStandardRepository.ListAllAsyncIncludeAll().Result;
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result;
                var Organisation = organizationRepository.GetAllOrganization();
                var boardstandard = boardStandardRepository.ListAllAsyncIncludeAll();
                var organisation = organizationRepository.GetAllOrganization();
                var standard = standardRepository.GetAllStandard();
                var board = boardRepository.GetAllBoard();
                // var stream = streamRepository.GetAllStream();
                // var boardstandardstream = boardStandardStreamRepository.ListAllAsyncIncludeActiveNoTrack();
                //  var AcademicStandardstream = academicStandardStreamRepository.ListAllAsyncIncludeActiveNoTrack();
                var wings = commonMethods.GetWing();
                var res = (from x in AcademicStudents
                           join y in students on x.StudentId equals y.ID
                           join a in AcademicStandard on x.AcademicStandardId equals a.ID
                           join b in OrganisationAcademic on a.OrganizationAcademicId equals b.ID
                           join m in AcademicSession on b.AcademicSessionId equals m.ID
                           select new AcademicStudentViewModel
                           {
                               ID = a.ID,
                               OraganisationAcademicId = a.OrganizationAcademicId,
                               BoardStandardId = a.BoardStandardId,
                               OraganisationName = organisation.Where(c => c.ID == (OrganisationAcademic.Where(d => d.ID == a.OrganizationAcademicId)).Select(d => d.OrganizationId).FirstOrDefault()).Select(c => c.Name).FirstOrDefault(),
                               AcademicSessionId = b.ID,
                               SessionName = m.DisplayName,
                               StandardId = boardstandard.Result.Where(d => d.ID == a.BoardStandardId).Select(e => e.StandardId).FirstOrDefault(),
                               StandardName = standard.Where(e => e.ID == boardstandard.Result.Where(d => d.ID == a.BoardStandardId).Select(g => g.StandardId).FirstOrDefault()).Select(e => e.Name).FirstOrDefault(),
                               StandardStreamType = standard.Where(e => e.ID == boardstandard.Result.Where(d => d.ID == a.BoardStandardId).Select(g => g.StandardId).FirstOrDefault()).Select(e => e.StreamType).FirstOrDefault(),
                               //  SchoolWingId = a.SchoolWingId,
                               Status = a.Status,
                               ModifiedDate = a.ModifiedDate,
                               // WingName = wings.Where(d => d.ID == commonMethods.GetSchoolWing().Where(c => c.ID == a.SchoolWingId).Select(c => c.WingID).FirstOrDefault()).Select(d => d.Name).FirstOrDefault(),
                               StudentId = x.StudentId,
                               studentname = y.FirstName + " " + y.LastName,
                               studentcode = y.StudentCode,
                               BoardName = board.Where(d => d.ID == boardstandard.Result.Where(c => c.ID == a.BoardStandardId).Select(c => c.BoardId).FirstOrDefault()).FirstOrDefault().Name,
                               // streamName = x.AcademicStandardStreamId == null ? "" : stream.Where(f => f.ID == boardstandardstream.Result.Where(d => d.ID == AcademicStandardstream.Result.Where(c => c.ID == x.AcademicStandardStreamId).Select(c => c.BoardStandardStreamId).FirstOrDefault()).Select(c => c.StreamId).FirstOrDefault()).Select(d => d.Name).FirstOrDefault()
                           }).ToList();
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }


        }


        public IEnumerable<SessionFeesTypeViewModel> GetAllSessionFeesType()
        {
            try
            {

                var FeesTypeList = feestypeRepo.GetAllFeesType().Where(x => x.Active == true).ToList();
                var AcademicSessionList = sessionFeestypeRepo.GetAllSessionFeesType().Where(x => x.Active == true).ToList();


                //var AcademicSessionList = academicSessionRepository.GetAllAcademicSession();
                var ApplicableList = applicableTypeRepo.GetAllFeesTypeApplicable(0, 0).Where(x => x.Active == true).ToList();

                //            var q =
                //from a in AcademicSessionList
                //join b in FeesTypeList on a.FeesTypeId equals b.ID
                //join c in ApplicableList on a.ApplicableId equals c.ID
                //select new SessionFeesTypeViewModel
                //{
                //    ID = a.ID,
                //    SessionId = a.SessionId,
                //    SessionName = academicSessionRepository.GetAllAcademicSession().Where(x => x.ID == a.SessionId).Select(x => x.DisplayName).FirstOrDefault(),
                //    FeesTypeId = b.ID,
                //    FeesTypeName = b.Name,
                //    ParentFeesTypeId = b.ParentFeeTypeID,
                //    ParentFeesType = b.ParentFeeTypeName,
                //    ApplicableId = c.ID,
                //    ApplicableTo = c.Name,
                //    ModifiedDate = a.ModifiedDate,
                //    Status = a.Status,
                //    Mandatory = a.IsMandatory,
                //    DeadlineDate = a.Deadlinedate
                //};

                //            return q.ToList();



                 var res = (from a in AcademicSessionList
                           join b in FeesTypeList on a.FeesTypeId equals b.ID
                           join c in ApplicableList on a.ApplicableId equals c.ID
                           select new SessionFeesTypeViewModel
                           {
                               ID = a.ID,
                               SessionId = a.SessionId,
                               SessionName = academicSessionRepository.GetAllAcademicSession().Where(x => x.ID == a.SessionId).Select(x => x.DisplayName).FirstOrDefault(),
                               FeesTypeId = b.ID,
                               FeesTypeName = b.Name,
                               ParentFeesTypeId = Convert.ToInt32(b.ParentFeeTypeID),
                               ParentFeesType = b.ParentFeeTypeName,
                               ApplicableId = c.ID,
                               ApplicableTo = c.Name,
                               ModifiedDate = a.ModifiedDate,
                               Status = b.Status,
                               Mandatory = a.IsMandatory,
                               DeadlineDate = a.Deadlinedate,
                               HaveInstallment = b.HaveInstallment,
                               HaveChild = b.HaveChild

                           }

                           ).ToList();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public IEnumerable<SessionFeesTypeViewModel> GetAllFeesType()
        {
            try
            {
                var FeesTypeList = feestypeRepo.GetAllFeesType().Where(x => x.Active == true ).ToList();  //&& x.HaveChild == true
                var AcademicSessionList = sessionFeestypeRepo.GetAllSessionFeesType().Where(x => x.Active == true).ToList();
                //var AcademicSessionList = academicSessionRepository.GetAllAcademicSession();
                var ApplicableList = applicableTypeRepo.GetAllFeesTypeApplicable(0, 0).Where(x => x.Active == true).ToList();

                //            var q =
                //from a in AcademicSessionList
                //join b in FeesTypeList on a.FeesTypeId equals b.ID
                //join c in ApplicableList on a.ApplicableId equals c.ID
                //select new SessionFeesTypeViewModel
                //{
                //    ID = a.ID,
                //    SessionId = a.SessionId,
                //    SessionName = academicSessionRepository.GetAllAcademicSession().Where(x => x.ID == a.SessionId).Select(x => x.DisplayName).FirstOrDefault(),
                //    FeesTypeId = b.ID,
                //    FeesTypeName = b.Name,
                //    ParentFeesTypeId = b.ParentFeeTypeID,
                //    ParentFeesType = b.ParentFeeTypeName,
                //    ApplicableId = c.ID,
                //    ApplicableTo = c.Name,
                //    ModifiedDate = a.ModifiedDate,
                //    Status = a.Status,
                //    Mandatory = a.IsMandatory,
                //    DeadlineDate = a.Deadlinedate
                //};

                //            return q.ToList();



                var res = (from a in AcademicSessionList
                           join b in FeesTypeList on a.FeesTypeId equals b.ID
                           join c in ApplicableList on a.ApplicableId equals c.ID
                           select new SessionFeesTypeViewModel
                           {
                               ID = a.ID,
                               SessionId = a.SessionId,
                               SessionName = academicSessionRepository.GetAllAcademicSession().Where(x => x.ID == a.SessionId).Select(x => x.DisplayName).FirstOrDefault(),
                               FeesTypeId = b.ID,
                               FeesTypeName = b.Name,
                               ParentFeesTypeId = Convert.ToInt64(b.ParentFeeTypeID),
                               ParentFeesType = b.ParentFeeTypeName,
                               ApplicableId = c.ID,
                               ApplicableTo = c.Name,
                               ModifiedDate = a.ModifiedDate,
                               Status = a.Status,
                               Mandatory = a.IsMandatory,
                               DeadlineDate = a.Deadlinedate,
                               HaveInstallment = b.HaveInstallment,
                               HaveChild = b.HaveChild


                           }

                           ).ToList();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public IEnumerable<FeesTypeViewModel> GetAllFeesTypePrices()
        {
            try
            {
                var FeesTypePriceData = feesTypePriceDataRepo.GetAllFeesTypePrice(0, 0).Where(x => x.Active == true).ToList();
                var feesTypePriceApplicabledata = feesTypePriceDataRepo.GetAllFeesTypePriceApplicableData().Where(x => x.Active == true).ToList();
                var FeesTypeList = feestypeRepo.GetAllFeesType().Where(x => x.Active == true).ToList();
                var AcademicSessionList = sessionFeestypeRepo.GetAllSessionFeesType().Where(x => x.Active == true).ToList();
                //var AcademicSessionList = academicSessionRepository.GetAllAcademicSession();
                var ApplicableList = applicableTypeRepo.GetAllFeesTypeApplicable(0, 0).Where(x => x.Active == true).ToList();

                var result = (from a in feesTypePriceApplicabledata
                              join b in FeesTypePriceData on a.FeesTypePriceDataID equals b.ID
                              join c in FeesTypeList on a.FeesTypePriceId equals c.ID
                             // join d in AcademicSessionList on b.AcademicSessionId equals d.ID
                              // join e in ApplicableList on a.ApplicableToId equals e.ID
                              select new FeesTypeViewModel
                              {
                                  ID = b.ID,
                                  SessionId = b.AcademicSessionId,
                                  SessionName = academicSessionRepository.GetAllAcademicSession().Where(x => x.ID == b.AcademicSessionId).Select(x => x.DisplayName).FirstOrDefault(),
                                  FeesTypeId = a.ID,
                                  FeesTypeName = c.Name,
                                  ParentFeesTypeId = Convert.ToInt32(c.ParentFeeTypeID),
                                  ParentFeesType = c.ParentFeeTypeName,
                                  ApplicableId = a.ApplicabledataId,
                                  ApplicableTo = applicableTypeRepo.GetAllFeesTypeApplicable(0, 0).Where(x => x.Active == true && x.ID == a.ApplicableToId).Select(r => r.Name).FirstOrDefault(),
                                  ModifiedDate = a.ModifiedDate,
                                  Status = a.Status,
                                  Price = a.Price.ToString(),
                                  ApplicableData = "Next Screen"
                              }

                          ).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public IEnumerable<PaymentCollectionTypeViewModel> GetAllPaymentCollectionTypes()
        {
            try
            {
                var PaymentcollectionType = paymentCollectionTyperepo.GetAllPaymentCollectionType().ToList(); //.Where(x => x.Active == true)
                var AcademicSessionList = sessionFeestypeRepo.GetAllSessionFeesType().Where(x => x.Active == true).ToList();

                var FeeType = feestypeRepo.GetAllFeesType().Where(a => a.Active == true).ToList();
                //var OrganisationList = organizationRepository.GetAllOrganization().Where(x => x.Active == true).ToList();
                // var standard = standardRepository.GetAllStandard();
                //var board = boardRepository.GetAllBoard();
                //var wings = commonMethods.GetWing();

                var OrganisationList = organizationRepository.GetAllOrganization().Where(x => x.Active == true).ToList();
                var standard = standardRepository.GetAllStandard();
                var board = boardRepository.GetAllBoard();
                var wings = commonMethods.GetWing();
                //  var schoolwings = studentAggregateRepository.GetAllSchoolWing();



                var res = (from a in PaymentcollectionType
                           join b in AcademicSessionList on a.AcademicSessionId equals b.ID
                           join c in FeeType on a.FeeTypeId equals c.ID
                           //join g in OrganisationList on a.OrganizationId equals g.ID
                           //join d in wings on a.WingId equals d.ID
                           //join e in board on a.Boardid equals e.ID
                           //join f in standard on a.ClassId equals f.ID



                           select new PaymentCollectionTypeViewModel
                           {
                               ID = a.ID,
                               AcademicSessionId = a.AcademicSessionId,
                               AcademicSession = academicSessionRepository.GetAllAcademicSession().Where(x => x.ID == a.AcademicSessionId).Select(x => x.DisplayName).FirstOrDefault(),

                               FeeType = c.Name,
                               FeeTypeId = a.FeeTypeId,
                               SumAmount = a.SumAmount.ToString("C", System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN")),
                               NoOfInstallment = a.NoOfInstallment,
                               Active = a.Active,
                               Status = a.Status,
                               //OrganizationId = a.OrganizationId,
                               //Organization = g.Name,
                               //WingId = a.WingId,
                               //Wing = d.Name, //commonMethods.GetWing().Where(w=>w.ID==d.WingID).Select(w=>w.Name).FirstOrDefault(),
                               //Boardid = a.Boardid,
                               //Board = e.Name,
                               //ClassId = a.ClassId,
                               //Class = f.Name
                           }

                           ).ToList();

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }



}
