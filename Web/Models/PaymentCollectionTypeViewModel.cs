﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using OdmErp.Web.Models;
using X.PagedList;
using X.PagedList.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace OdmErp.Web.Models
{
    public class PaymentCollectionTypeViewModel : BaseModel
    {
        public long AcademicSessionId { get; set; }
        public string AcademicSession { get; set; }
        public long OrganizationId { get; set; }
        public string Organization { get; set; }
        public long WingId { get; set; }
        public string Wing { get; set; } 
        public long Boardid { get; set; }
        public string Board { get; set; }
        public long ClassId { get; set; }
        public string Class { get; set; }
        public string SumAmount { get; set; }
        public long NoOfInstallment { get; set; }
        public bool Active { get; set; }
        public string FeeType { get; set; }
        public long FeeTypeId { get; set; }
    }
    public class PaymentGatewayBTBModel
    {
        public long ID { get; set; }
        public string ProviderName { get; set; }
        public string Host { get; set; }
        public string HttpMethod { get; set; }
        public string BtbParam { get; set; }
        public long PaymentGatewayId { get; set; }
    }
}
