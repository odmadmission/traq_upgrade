﻿using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Web.Areas.Tasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models
{
    public class PaymentServices
    {
        private IPaymentCollectionType paymentCollectionTypeRepository;
        private TodoMethod commonMethods;
        private TodoSmsnMails smssend;
        private CommonMethods common;
        public commonsqlquery commonsqlquery;
        private IAcademicStandardRepository academicStandardRepository;
        private IBoardStandardRepository BoardStandardRepository;
        private IStandardRepository StandardRepository;
        private IParentRepository parentRepository;
        private smssend smssendRepository;
        public PaymentServices(TodoMethod todoMethod, TodoSmsnMails sms, CommonMethods commonmeth, IPaymentCollectionType paymentCollectionTypeRepo, IAcademicStandardRepository academicStandardRepo, IBoardStandardRepository BoardStandardRepo, IStandardRepository StandardRepo, commonsqlquery commonsql, IParentRepository parentRepo, smssend smssendRepo)
        {
            commonMethods = todoMethod;
            smssend = sms;
            common = commonmeth;
            paymentCollectionTypeRepository = paymentCollectionTypeRepo;
            academicStandardRepository = academicStandardRepo;
            BoardStandardRepository = BoardStandardRepo;
            StandardRepository = StandardRepo;
            commonsqlquery = commonsql;
            parentRepository= parentRepo;
            smssendRepository = smssendRepo;
        }

        //SendPaymentRemindermail
        public void SendPaymentRemindermail()
        {
            try
            {
                DateTime givenDate = DateTime.Today.AddDays(10);
                //DateTime startOfWeek = new DateTime(givenDate.Year, givenDate.Month, 1);
                //DateTime endOfWeek = startOfWeek.AddMonths(1).AddDays(-1);
                var paymentcollectionTypeList = paymentCollectionTypeRepository.GetAllPaymentCollectionType().ToList();
                var todaysPayList = paymentCollectionTypeRepository.GetAllPaymentCollectionTypeData().Where(a => a.FromDate.Date == givenDate.Date).ToList();
                var academicstandard = academicStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result.ToList();
                var BoardStandardList = BoardStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result.ToList();
                var StandardList = StandardRepository.GetAllStandard();
                var allacademicsectionstudents = commonsqlquery.Get_view_All_Academic_Students().ToList();
                var allParent = parentRepository.GetAllParent();
                var alldata = (from d in paymentcollectionTypeList
                               join e in todaysPayList on d.ID equals e.PaymentCollectionId
                               select new
                               {
                                   d.ClassId,
                                   e.FromDate,
                                   e.ToDate,
                                   parentDetails=(from a in allacademicsectionstudents where a.AcademicStandardId==d.ClassId 
                                             select new
                                             {
                                                 a.studentname,
                                                 a.StudentId,
                                                 multipleparent=(from c in allParent where c.StudentID==a.StudentId select new {name=c.FirstName+" "+c.LastName,c.PrimaryMobile,c.EmailId}).ToList()
                                             }).ToList()
                               }).ToList();
                for(int i = 0; i < alldata.Count; i++)
                {
                    for(int j = 0; j < alldata[i].parentDetails.Count; j++)
                    {
                        for(int k = 0; k < alldata[i].parentDetails[j].multipleparent.Count; k++)
                        {
                            if (alldata[i].parentDetails[j].multipleparent[k].EmailId != null)
                            {
                                //var taskDashboardcls = "Test Message";
                                smssend.PaymentRemindermail("Payment Reminder", alldata[i].parentDetails[j].multipleparent[k].EmailId, alldata[i].parentDetails[j].studentname, alldata[i].parentDetails[j].multipleparent[k].name,alldata[i].FromDate,alldata[i].ToDate);
                            }
                            if (alldata[i].parentDetails[j].multipleparent[k].PrimaryMobile != null)
                            {
                                //var taskDashboardcls = "Test Message";
                                smssendRepository.PaymentReminderSms("Payment Reminder", alldata[i].parentDetails[j].multipleparent[k].PrimaryMobile, alldata[i].parentDetails[j].studentname, alldata[i].parentDetails[j].multipleparent[k].name, alldata[i].FromDate, alldata[i].ToDate);
                            }
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
