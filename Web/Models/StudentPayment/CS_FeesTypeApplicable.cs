﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.Web.Models.StudentPayment
{
    public class CS_FeesTypeApplicable
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ReturnMsg { get; set; }
    }
}
