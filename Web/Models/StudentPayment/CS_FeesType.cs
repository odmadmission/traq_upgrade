﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models.StudentPayment
{
    public class CS_FeesType
    {
        public long? ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsSubType { get; set; }
        public long? ParentFeeTypeID { get; set; }
        public string ParentFeeTypeName { get; set; }
        public string ReturnMsg { get; set; }
        public bool HaveChild { get; set; }
        public bool HaveInstallment { get; set; }
        public string LateFeePercentageType { get; set; }
        public string LateFeeAmountType { get; set; }
    }
}
