﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OdmErp.Web.Models.StudentPayment
{
    public class CS_FessTypePriceData
    {
        public long ID { get; set; }
        public long AcademicSessionId { get; set; }
        public long FeesTypePriceId { get; set; }
        public List<CS_SessionFeeTypePriceApplicableData> sessionFeesTypePriceApplicableData { get; set; } = new List<CS_SessionFeeTypePriceApplicableData>();
        public string ReturnMsg { get; set; }
    } 

    //public class CS_FessTypePriceData
    //{
    //    public long ID { get; set; }
    //    public long AcademicSessionId { get; set; }
    //    public long FeesTypePriceId { get; set; }
    //}
    public class CS_SessionFeeTypePriceApplicableData
    {
        public long ID { get; set; }
        public long ApplicabledataId { get; set; }
        public long FeesTypePriceId { get; set; }
        public decimal Price { get; set; }
        public long FeesTypePriceDataID { get; set; }
        public string SessionName { get; set; }
        public string FeesTypeName { get; set; }
        public string Applicabledata { get; set; }
        public string ParentFeesType { get; set; }
        public long ApplicableToId { get; set; }
        public string ReturnMsg { get; set; }
    }
}
