﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models.StudentPayment
{
    public class CS_SessionScholarship
    {
        public long id { get; set; }
        public string sessionName { get; set; }
        public string scholarshipName { get; set; }
        public long sessionId { get; set; }
        public string status { get; set; }
    }
}
