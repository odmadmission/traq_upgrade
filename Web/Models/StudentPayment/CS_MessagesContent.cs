﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models.StudentPayment
{
    public class CS_MessagesContent
    {
        public long ID { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Template { get; set; }
        public string ReturnMsg { get; set; }

    }
}
