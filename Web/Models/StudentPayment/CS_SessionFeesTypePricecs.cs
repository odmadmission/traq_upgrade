﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models.StudentPayment
{
    public class CS_SessionFeesType
    {
        public long ID { get; set; }
        public long SessionId { get; set; }
        public long FeesTypeId { get; set; }
        public long ApplicableId { get; set; }
        public DateTime Deadlinedate { get; set; }
        public bool IsMandatory { get; set; }
        public string ReturnMsg { get; set; }
    }
}
