﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models.StudentPayment
{
    public class CS_Scholarship
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string ReturnMsg { get; set; }
    }
}
