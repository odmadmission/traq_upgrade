﻿using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Entities.SupportAggregate;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models
{
    public class StudentModelClass
    {
        public long ID { get; set; }
        [Display(Name = "Student Code")]
        [Required(ErrorMessage = "Please Enter Student Code")]
        public string StudentCode { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please Enter Last Name")]

        public string LastName { get; set; }
        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Please Enter Gender")]

        public string Gender { get; set; }
        [Display(Name = "Date Of Birth")]
        [Required(ErrorMessage = "Please Enter Date Of Birth")]

        public DateTime DateOfBirth { get; set; }
        [Display(Name = "Blood Group")]
        [Required(ErrorMessage = "Please Enter Blood Group")]
        public long BloodGroupID { get; set; }
        public string BloodGroupName { get; set; }
        public IEnumerable<BloodGroup> bloodGroups { get; set; }
        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please Enter Category")]
        public long CategoryID { get; set; }
        public string CategoryName { get; set; }
        [Display(Name = "Nationality")]
        [Required(ErrorMessage = "Please Enter Nationality")]
        public long NatiobalityID { get; set; }
        public string NatiobalityName { get; set; }
        public IEnumerable<NationalityType> nationalityTypes { get; set; }
        [Display(Name = "Religion")]
        [Required(ErrorMessage = "Please Enter Religion")]
        public long ReligionID { get; set; }
        public string ReligionName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public List<documentTypesCls> documentTypesCls { get; set; }
        public IEnumerable<ReligionType> religionTypes { get; set; }
        public IEnumerable<StudentAdressCls> studentAdresses { get; set; }
        public IEnumerable<StudentParentsCls> studentParentsCls { get; set; }
        public StudentClassDetails studentClassDetails { get; set; }
        public IEnumerable<Category> categoryTypes { get; set; }
        public IEnumerable<EmployeeAccesscls> accesscls { get; set; }
        public List<GivenAccesscls> employeeModuleSubModuleAccess { get; set; }

        public List<StudentDocumentModelClass> StudentDocumentAccess { get; set; }
        public List<StudentDocumentModelClass> StudentPersonalDocumentAccess { get; set; }

        public List<StudentAcademicModel> StudentAcademicAccess { get; set; }
        public IEnumerable<StudentModelClass> StudentList { get; set; }


    }
    public class StudentAdressCls
    {
        public long StudentAddressId { get; set; }
        public long StudentId { get; set; }
        public long ID { get; set; }
        [Display(Name = "Address Line1")]
        [Required(ErrorMessage = "Please Enter Address Line1")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line2")]
        [Required(ErrorMessage = "Please Enter Address Line2")]
        public string AddressLine2 { get; set; }
        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Please Enter Postal Code")]
        public string PostalCode { get; set; }
        [Display(Name = "City")]
        [Required(ErrorMessage = "Please Select City")]
        public long CityID { get; set; }
        [Display(Name = "State")]
        [Required(ErrorMessage = "Please Select State")]
        public long StateID { get; set; }
        [Display(Name = "Country")]
        [Required(ErrorMessage = "Please Select Country")]
        public long CountryID { get; set; }
        [Display(Name = "Address Type")]
        [Required(ErrorMessage = "Please Select Address Type")]
        public long AddressTypeID { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string AddressTypeName { get; set; }
        public IEnumerable<Country> countries { get; set; }
        public IEnumerable<State> states { get; set; }
        public IEnumerable<City> cities { get; set; }
        public IEnumerable<AddressType> addressTypes { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
    }
    public class StudentParentsCls
    {
        public long ID { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please Enter Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Primary Mobile")]
        public string PrimaryMobile { get; set; }
        [Display(Name = "Alternative Mobile")]
        public string AlternativeMobile { get; set; }
        [Display(Name = "Landline Number")]
        public string LandlineNumber { get; set; }
        [Display(Name = "Email Id")]
        public string EmailId { get; set; }
        //[Display(Name = "Organization")]
        //public long OrganizationID { get; set; }
        public string ParentOrganization { get; set; }
        [Display(Name = "Profession Type")]
        public long ProfessionTypeID { get; set; }
        public string ProfessionTypeName { get; set; }
        public List<CommonMasterModel> ProfessionTypes { get; set; }
        public long StudentID { get; set; }
        [Display(Name = "Parent Relationship")]
        public long ParentRelationshipID { get; set; }
        public string ParentRelationshipName { get; set; }
        public string Imagepath { get; set; }
        public List<CommonMasterModel> ParentRelationships { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
    public class StudentClassDetails
    {
        public long ID { get; set; }
        public long StudentID { get; set; }
        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Standard")]
        public long StandardID { get; set; }
        public string StandardName { get; set; }
        public List<Standard> standards { get; set; }
        [Display(Name = "Section")]
        [Required(ErrorMessage = "Please Select Section")]
        public long SectionID { get; set; }
        public string SectionName { get; set; }
        public List<Section> sections { get; set; }
        [Display(Name = "Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long WingID { get; set; }
        public string WingName { get; set; }
        public List<Schoolwingcls> wings { get; set; }
        [Display(Name = "Organisation")]
        [Required(ErrorMessage = "Please Select Organisation")]
        public long OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public List<Organization> organizations { get; set; }
        [Display(Name = "Current Standard")]
        public bool IsCurrent { get; set; }
        [Display(Name = "Board")]
        [Required(ErrorMessage = "Please Select Board")]
        public long BoardID { get; set; }
        public string BoardName { get; set; }
        public List<Board> boards { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long SessionID { get; set; }
        public string SessionName { get; set; }
    }
    public class Schoolwingcls : BaseModel
    {
        public long ID { get; set; }
        [Display(Name = "Organisation")]
        [Required(ErrorMessage = "Please Select Organisation")]
        public long OrganizationAcademicID { get; set; }
        [Display(Name = "Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long WingID { get; set; }
        public long[] MultiWingID { get; set; }
        public string OrganisationName { get; set; }
        public string wingName { get; set; }
        public List<Organization> organizations { get; set; }
        public List<Wing> wings { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long AcademicSessionId { get; set; }
        public long OrganisationID { get; set; }
        public long OrganisationIDByAcademic { get; set; }
        public string OrganisationNameByAcademic { get; set; }



    }
   
    public class StudentTypewithAddresscls
    {
        public long typeId { get; set; }
        public string typeName { get; set; }
        public StudentAdressCls studentAdress { get; set; }

    }
    public class AcademicStudentcls:BaseModel
    {
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long AcademicStandardId { get; set; }
        public bool StandardStreamType { get; set; }
        public string StandardName { get; set; }

        [Display(Name = "Oraganisation Academic")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }

        [Display(Name = "Teacher Employee")]
        [Required(ErrorMessage = "Please Select Teacher")]
        public long ddlteacher { get; set; }
        [Display(Name = "Class Timing")]
        [Required(ErrorMessage = "Please Select Class Timing")]
        public long ddlclasstiming { get; set; }
        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Please Select Subject")]
        public long ddlsubjects { get; set; }

        public DateTime date { get; set; }        

        public string OraganisationAcademicName { get; set; }

        public string OraganisationName { get; set; }

        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        [Display(Name = "School Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }
        public long CountAcademicSection { get; set; }
        public long CountAcademicStandardStream { get; set; }
        public IEnumerable<AcademicSession> academicSessions { get; set; }
        public IEnumerable<Board> boards { get; set; }
        public IEnumerable<ClassTiming> ClassTiming { get; set; }
        public long AcademicStandardStreamId { get; set; }
        public long[] StudentId { get; set; }
        public long AcademicSectionId { get; set; }
        public string[] RollNo { get; set; }
        public long[] AcademicStudentId { get; set; }
    }
    public class AcademicStudentViewModel : BaseModel
    {
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        [Display(Name = "BoardStandardId")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long StandardId { get; set; }
        public bool StandardStreamType { get; set; }

        public string StandardName { get; set; }

        [Display(Name = "OraganisationAcademicId")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }


        public string OraganisationAcademicName { get; set; }

        public string OraganisationName { get; set; }

        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }

        [Display(Name = "MultiBoardStandardId")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long[] MultiBoardStandardId { get; set; }

        [Display(Name = "SchoolWingId")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }
        public long CountAcademicSection { get; set; }
        public long CountAcademicStandardStream { get; set; }
        public string studentname { get; set; }
        public string studentcode { get; set; }
        public long StudentId { get; set; }
        public string streamName { get; set; }
    }
    public class StudentDocumentModelClass : BaseModel
    { 
        public bool approveaccess { get; set; }
        public string OrganizationName { get; set; }
        public string BoardName { get; set; }
        public string ClassName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public long StudentID { get; set; }
        public long DocumentSubTypeID { get; set; }
        public string DocumentSubTypeName { get; set; }
        public bool IsMandatory { get; set; }
        public long DocumentTypeID { get; set; }
        public string DocumentTypeName { get; set; }
        public long RelatedID { get; set; }
        public string relatedDetails { get; set; }
        public long EmployeeID { get; set; }
        public bool IsVerified { get; set; }
        public long VerifiedAccessID { get; set; }
        public DateTime? VerifiedDate { get; set; }
        public string DocumentPath { get; set; }
        public bool IsApproved { get; set; }
        public bool IsReject { get; set; }
        public int totaldocument { get; set; }
        public bool verifyaccess { get; set; }       
        public IFormFile file { get; set; }
    }
    public class StudentDocumentUploadCls
    {
        public IEnumerable<IFormFile> files { get; set; }
        public List<StudentDocumentAttachment> attachments { get; set; }
        public bool isverified { get; set; }
        public bool isapproved { get; set; }
        public bool verifiedaccess { get; set; }
        public bool approveaccess { get; set; }
        public long ID { get; set; }
        public long StudentID { get; set; }
    }
    public class StudentAcademicModelClass
    {
        public long StudentID { get; set; }
        public long ID { get; set; }
        [Display(Name = "Organization Name")]
        [Required(ErrorMessage = "Please Enter Organization Name")]
        public string OrganizationName { get; set; }

        [Display(Name = "Board")]
        [Required(ErrorMessage = "Please Enter Board")]
        public int BoardID { get; set; }


        [Display(Name = "Class")]
        [Required(ErrorMessage = "Please Enter Salary Per Anum")]
        public int ClassID { get; set; }


        [Display(Name = "From Date")]
        [Required(ErrorMessage = "Please Enter From Date")]
        public DateTime FromDate { get; set; }
        [Display(Name = "To Date")]
        [Required(ErrorMessage = "Please Enter To Date")]
        public DateTime ToDate { get; set; }
        public List<documentTypesCls> documentTypesCls { get; set; }
    }

    public class StudentAcademicModel : BaseModel
    {
      
        public string OrganizationName { get; set; }
      
        public int BoardID { get; set; }     
        public string BoardName { get; set; }

        public int ClassID { get; set; }
        public string ClassName { get; set; }

        public DateTime FromDate { get; set; }
    
        public DateTime ToDate { get; set; }
      
        public long StudentID { get; set; }

    }

    public class SessionTimeTableModel
    {
        public long BoardId { get; set; }
     
        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long AcademicStandardId { get; set; }
        public bool StandardStreamType { get; set; }
    

        [Display(Name = "Oraganisation Academic")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }

        [Display(Name = "Section")]
        [Required(ErrorMessage = "Please Select Section")]
        public long AcademicSectionId { get; set; }       
      
        [Display(Name = "Section")]
        [Required(ErrorMessage = "Please Select Session")]
        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        [Display(Name = "School Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long SchoolWingId { get; set; }
        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Please Select Subject")]
        public long AcademicSubjectId { get; set; }

        public IEnumerable<AcademicSession> academicSessions { get; set; }
        public IEnumerable<Board> boards { get; set; }
        public long AcademicStandardStreamId { get; set; }    
         
        
        public string TimetableCode { get; set; }     
      

        public int SessionTimetableId { get; set; }
        [Display(Name = "StartDate")]
        [Required(ErrorMessage = "Please Enter StartDate")]
        public DateTime StartDate { get; set; }

        [Display(Name = "EndDate")]
        [Required(ErrorMessage = "Please Enter EndDate")]
        public DateTime EndDate { get; set; }
        [Display(Name = "PeriodType")]
        [Required(ErrorMessage = "Please Enter PeriodType")]
        public long PeriodType { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Start Time")]
        [Required(ErrorMessage = "Please Select Start Time")]
        public TimeSpan StartTime { get; set; }
        [Display(Name = "End Time")]
        [Required(ErrorMessage = "Please Select End Time")]
        public TimeSpan EndTime { get; set; }
        public string PeriodTypeName { get; set; }
        public bool isremove { get; set; }
        public long id { get; set; }
   
    }

    public class SessionTimetablePeriodSubjectModel
    {
        public long id { get; set; }
        public long SessionTimetablePeriodId { get; set; }     
        public TimeSpan StartTime { get; set; }      
        public TimeSpan EndTime { get; set; }
        public string StartTimeName { get; set; }
        public string EndTimeName { get; set; }
        public long AcademicSubjectId { get; set; }
        public string AcademicSubjectName { get; set; }

        public string Name { get; set; }    
        public string PeriodTypeName { get; set; }
        public bool isremove { get; set; }

    }
}
