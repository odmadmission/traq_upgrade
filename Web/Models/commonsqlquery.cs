﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Query;
using OdmErp.Infrastructure.Data;
using OdmErp.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models
{
    public class commonsqlquery
    {
        public ApplicationDbContext context;
        public commonsqlquery(ApplicationDbContext cont)
        {
            context = cont;
        }
        public List<View_All_Academic_Student> Get_view_All_Academic_Students()
        {
            var res = context.view_All_Academic_Students.FromSql(@" select a.ID as ID,c.OrganizationAcademicId as OraganisationAcademicId,
a.AcademicStandardId,a.AcademicStandardStreamId,
                        c.BoardStandardId as BoardStandardId,
                        f.Name as OraganisationName,
                        d.AcademicSessionId as AcademicSessionId,
                        e.DisplayName as SessionName,
                        g.StandardId as StandardId,
                        h.Name as StandardName,
                        h.StreamType as StandardStreamType,
                        c.SchoolWingId as SchoolWingId,
                        a.ModifiedDate as ModifiedDate,
                        j.Name as WingName,
                        a.StudentId as StudentId,
                        (b.FirstName+' '+b.LastName) as studentname,
                        b.StudentCode as studentcode,
                        k.Name as BoardName,
                        (case when (a.AcademicStandardStreamId is null or a.AcademicStandardStreamId=0) then '' else 
                        (select top(1) Name from Streams 
                        where Active=1 and ID=(select top(1) StreamId from BoardStandardStreams 
                        where Active=1 and ID=(select top(1) BoardStandardStreamId from AcademicStandardStreams where Active=1 and ID=a.AcademicStandardStreamId))) end) as streamName,
                        e.IsAvailable as IsAvailable
                         from AcademicStudents as a
                        inner join Students as b on a.StudentId=b.ID
                        inner join AcademicStandards as c on a.AcademicStandardId=c.ID
                        inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                        inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                        inner join Organizations as f on d.OrganizationId=f.ID 
                        inner join BoardStandards as g on c.BoardStandardId=g.ID
                        inner join Standards as h on g.StandardId=h.ID
                        inner join SchoolWing as i on c.SchoolWingId=i.ID
                        inner join Wing as j on i.WingID=j.ID
                        inner join Boards as k on g.BoardId=k.ID
                        where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1
                            and j.Active=1 and k.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Academic_Standard> Get_view_All_Academic_Standards()
        {
            var res = context.view_All_Academic_Standards.FromSql(@" select c.ID as ID,
                            c.OrganizationAcademicId as OraganisationAcademicId,
                            c.BoardStandardId as BoardStandardId,
                            f.Name as OraganisationName,
                            d.AcademicSessionId as AcademicSessionId,
                            e.DisplayName as SessionName,
                            g.StandardId as StandardId,
                            h.Name as StandardName,
                            h.StreamType as StandardStreamType,
                            c.ModifiedDate as ModifiedDate,
                            k.Name as BoardName,
                            e.IsAvailable as IsAvailable,
                            c.Status as Status,
							(select top(1) Nucleus from AcademicStandardSettings where AcademicStandardId=c.ID and Active=1) as Nucleus,
                            (select COUNT(*) from AcademicSections where AcademicStandardId=c.ID and Active=1) as CountAcademicSection,
                           (select COUNT(*) from academicStandardWings where AcademicStandardId=c.ID and Active=1) as CountAcademicWing,
						   (select COUNT(*) from AcademicSubjectGroups where AcademicStandardId=c.ID and Active=1) as CountAcademicSubjectGroup
						  
                             from AcademicStandards as c
                            inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                            inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                            inner join Organizations as f on d.OrganizationId=f.ID 
                            inner join BoardStandards as g on c.BoardStandardId=g.ID
                            inner join Standards as h on g.StandardId=h.ID
                            inner join Boards as k on g.BoardId=k.ID
                            where c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and k.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Academic_Student_Section> Get_view_All_Academic_Student_Sections()
        {
            var res = context.view_All_Academic_Student_Sections.FromSql(@"select l.ID as ID,c.OrganizationAcademicId as OraganisationAcademicId,
                        c.BoardStandardId as BoardStandardId, f.Name as OraganisationName,
                        d.AcademicSessionId as AcademicSessionId, e.DisplayName as SessionName,
                        g.StandardId as StandardId,h.Name as StandardName,h.StreamType as StandardStreamType,
                        c.SchoolWingId as SchoolWingId,a.ModifiedDate as ModifiedDate,j.Name as WingName,
                        a.StudentId as StudentId,(b.FirstName+' '+b.LastName) as studentname,
                        b.StudentCode as studentcode,k.Name as BoardName,
                        a.AcademicStandardStreamId,a.AcademicStandardId,
                        (case when (a.AcademicStandardStreamId is null or a.AcademicStandardStreamId=0) then '' else 
                        (select top(1) Name from Streams 
                        where Active=1 and ID=(select top(1) StreamId from BoardStandardStreams 
                        where Active=1 and ID=(select top(1) BoardStandardStreamId from AcademicStandardStreams where Active=1 and ID=a.AcademicStandardStreamId))) end) as streamName,
                        e.IsAvailable as IsAvailable,
						l.AcademicStudentId as AcademicStudentId,
						l.AcademicSectionId as AcademicSectionId,
						l.StudentCode as RollNo,
						n.Name as SectionName
                         from AcademicSectionStudents as l
						inner join AcademicStudents as a on l.AcademicStudentId=a.ID
						inner join AcademicSections as m on l.AcademicSectionId=m.ID
						inner join Sections as n on m.SectionId=n.ID
                        inner join Students as b on a.StudentId=b.ID
                        inner join AcademicStandards as c on a.AcademicStandardId=c.ID
                        inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                        inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                        inner join Organizations as f on d.OrganizationId=f.ID 
                        inner join BoardStandards as g on c.BoardStandardId=g.ID
                        inner join Standards as h on g.StandardId=h.ID
                        inner join SchoolWing as i on c.SchoolWingId=i.ID
                        inner join Wing as j on i.WingID=j.ID
                        inner join Boards as k on g.BoardId=k.ID
                        where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1
                            and j.Active=1 and k.Active=1 and l.Active=1 and m.Active=1 and n.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_TimeTable_For_Calender> Get_View_All_TimeTable_For_Calenders()
        {
            var res = context.view_All_TimeTable_For_Calenders.FromSql(@"                     select a.AcademicSectionId,a.AcademicStandardId, a.AcademicStandardStreamId, a.ID as typeId,a.NameOfMonth as typeName,(case when CAST(a.AssignDate As date)=GETDATE() then (select 'Current Period :'+f.Name+' (Teacher Name : )'+(e.FirstName+' '+e.LastName) from TimeTablePeriods as l 
                    inner join classTimings as m on l.ClassTimingId=m.ID
                     inner join PeriodEmployees as n on m.ID=n.TimeTablePeriodId 
                     inner join PeriodEmployees as d on l.ID=d.TimeTablePeriodId
                     inner join Employees as e on d.EmployeeId=e.ID
                     inner join Subjects as f on d.SubjectId=f.ID
                     where l.Active=1 and m.Active=1 and n.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and d.IsAvailable=1 and l.AcdemicTimeTableId=a.ID and CONVERT(TIME, GETDATE()) BETWEEN m.StartTime AND m.EndTime) else 'Alloted' end) as title,
                     a.AssignDate as start,'#82FA58' as backgroundColor,'#000000' as textColor 
                      from AcademicTimeTables as a
                     inner join TimeTablePeriods as b on a.ID=b.AcdemicTimeTableId
                     inner join classTimings as c on b.ClassTimingId=c.ID 
                     where a.Active=1 and b.Active=1 and c.Active=1 ").AsNoTracking().ToList();
            return res;
        }
        public List<View_Timetable> GetTimeTableList(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId)
        {
            var res = context.view_Timetables.FromSql(@"SELECT 
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.StartTime, 108), 103) as start,
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.EndTime, 108), 103) as [end],
            CONCAT(f.Name,' by ',e.FirstName,' ',e.LastName,'-',e.EmpCode) as title,
            cast(1 as bit) as allDay,cast(1 as bit) as editable,cast('#82FA58' as varchar(100))as backgroundColor,
            cast('#000000' as varchar(100))as textColor
              FROM [ODMERPDB].[dbo].[PeriodEmployees] as a
              inner join [dbo].[TimeTablePeriods] as b on a.TimeTablePeriodId=b.ID
              inner join [dbo].[AcademicTimeTables] as c on b.AcdemicTimeTableId=c.ID
              inner join [dbo].[classTimings] as d on b.[ClassTimingId]=d.ID
              inner join [dbo].[Employees] as e on a.EmployeeId=e.ID
               inner join [dbo].[Subjects] as f on a.SubjectId=f.ID
               where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and 
               c.AcademicSectionId=({@AcademicSectionId}) and c.AcademicStandardId=({@AcademicStandardId}) and 
                c.AcademicStandardStreamId=({@AcademicStandardStreamId})", new SqlParameter("@AcademicSectionId", AcademicSectionId),
               new SqlParameter("@AcademicStandardId", AcademicStandardId),
               new SqlParameter("@AcademicStandardStreamId", AcademicStandardStreamId)).AsNoTracking().ToList();
            return res;
        }
        public List<View_Timetable> GetTeacherTimeTableList(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId)
        {
            var res = context.view_Timetables.FromSql(@"            SELECT 
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.StartTime, 108), 103) as start,
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.EndTime, 108), 103) as [end],
            CONCAT(f.Name,' by ',e.FirstName,' ',e.LastName,'-',e.EmpCode) as title,
            cast(1 as bit) as allDay,cast(1 as bit) as editable,cast('#82FA58' as varchar(100))as backgroundColor,
            cast('#000000' as varchar(100))as textColor
              FROM [ODMERPDB].[dbo].[PeriodEmployees] as a
              inner join [dbo].[TimeTablePeriods] as b on a.TimeTablePeriodId=b.ID
              inner join [dbo].[AcademicTimeTables] as c on b.AcdemicTimeTableId=c.ID
              inner join [dbo].[classTimings] as d on b.[ClassTimingId]=d.ID
              inner join [dbo].[Employees] as e on a.EmployeeId=e.ID
               inner join [dbo].[Subjects] as f on a.SubjectId=f.ID
               where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 
               and f.Active=1 and c.AcademicSectionId=({0}) and c.AcademicStandardId=({1}) 
               and c.AcademicStandardStreamId=({2})", AcademicSectionId, AcademicStandardId, AcademicStandardStreamId).AsNoTracking().ToList();
            return res;
        }
        public List<View_Timetable> GetForTeacherTimeTableList(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, long userId)
        {
            var res = context.view_Timetables.FromSql(@"            SELECT 
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.StartTime, 108), 103) as start,
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.EndTime, 108), 103) as [end],
            CONCAT(f.Name,' by ',e.FirstName,' ',e.LastName,'-',e.EmpCode) as title,
            cast(1 as bit) as allDay,cast(1 as bit) as editable,cast('#82FA58' as varchar(100))as backgroundColor,
            cast('#000000' as varchar(100))as textColor
              FROM [ODMERPDB].[dbo].[PeriodEmployees] as a
              inner join [dbo].[TimeTablePeriods] as b on a.TimeTablePeriodId=b.ID
              inner join [dbo].[AcademicTimeTables] as c on b.AcdemicTimeTableId=c.ID
              inner join [dbo].[classTimings] as d on b.[ClassTimingId]=d.ID
              inner join [dbo].[Employees] as e on a.EmployeeId=e.ID
               inner join [dbo].[Subjects] as f on a.SubjectId=f.ID
               where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 
and f.Active=1 and c.AcademicSectionId=({0}) and c.AcademicStandardId=({1}) and c.AcademicStandardStreamId=({2}) 
and a.EmployeeId=({3})", AcademicSectionId, AcademicStandardId, AcademicStandardStreamId, userId).AsNoTracking().ToList();
            return res;
        }
        public List<View_All_TimeTable_Subjects> Get_View_All_TimeTable_Subjects()
        {
            var res = context.view_All_TimeTable_Subjects.FromSql(@"select a.AcademicSectionId,a.AcademicStandardId,a.AcademicStandardStreamId,d.SubjectId,d.EmployeeId as TeacherId,a.AssignDate,b.ClassTimingId,a.NameOfMonth,b.AcdemicTimeTableId,b.ID,b.NameOfDay,c.Name as classtime,c.StartTime,c.EndTime,(e.FirstName+' '+e.LastName) as TeacherName,f.Name as SubjectName,b.InsertedDate,b.ModifiedDate,
(case when d.IsForwared=1 then (select FirstName+' '+LastName+'-'+EmpCode from Employees where ID=d.EmployeeId) else 'NA' end) as ForwardedBy,d.ID as PeriodEmployeeID,
g.TakenEmployeeId,(h.FirstName+' '+h.LastName+'-'+h.EmpCode) as TakenEmployeeName,
g.Reason,i.AcademicChapterId,i.AcademicConceptId,i.AcademicSyllabusId,i.PeriodEmployeeStatusId,j.chaptername,j.chapterId,k.conceptId,k.conceptname,l.syllabusId,l.syllabusname

from TimeTablePeriods as b
inner join AcademicTimeTables as a on b.AcdemicTimeTableId=a.ID
inner join classTimings as c on b.ClassTimingId=c.ID 
inner join PeriodEmployees as d on b.ID=d.TimeTablePeriodId
inner join Employees as e on d.EmployeeId=e.ID
inner join Subjects as f on d.SubjectId=f.ID
left join [dbo].[PeriodEmployeeStatuses] as g on d.ID=g.PeriodEmployeeId and g.Active=1
left join Employees as h on g.TakenEmployeeId=h.ID and h.Active=1
left join [dbo].[PeriodSyllabus] as i on g.ID=i.PeriodEmployeeStatusId and i.Active=1
left join (select x.ID as academicchapterId,y.Name as chaptername,y.ID as chapterId from AcademicChapters as x inner join Chapters as y on x.ChapterId=y.ID where x.Active=1 and y.Active=1) as j on i.AcademicChapterId=j.academicchapterId
left join (select x.ID as academicconceptId,y.Name as conceptname,y.ID as conceptId from AcademicConcepts as x inner join Concepts as y on x.ConceptId=y.ID where x.Active=1 and y.Active=1) as k on i.AcademicConceptId=k.academicconceptId
left join (select x.ID as academicsyllabusId,y.Title as syllabusname,y.ID as syllabusId from AcademicSyllabus as x inner join Syllabus as y on x.SyllabusId=y.ID where x.Active=1 and y.Active=1) as l on i.AcademicSyllabusId=l.academicsyllabusId
where b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and d.IsAvailable=1 and a.Active=1 ").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Academic_Section> Get_View_All_Academic_Section()
        {
            var res = context.view_All_Academic_Sections.FromSql(@"SELECT a.ID,a.AcademicStandardId,a.AcademicStandardStreamId,a.SectionId,b.BoardStandardId,b.OrganizationAcademicId,
b.SchoolWingId,c.Name as Sectionname,d.BoardId,d.StandardId,e.AcademicSessionId,e.OrganizationId,f.WingID,
g.DisplayName as Sessionname,g.Start,g.[End],h.GroupID,h.Name as Organisationname,i.Name as Wingname,j.Name as Boardname,k.Name as Standardname,
(case when a.AcademicStandardStreamId =0 then '' else (select top(1) w.Name from [ODMERPDB].[dbo].[AcademicStandardStreams] as u
inner join [ODMERPDB].[dbo].BoardStandardStreams as v on u.BoardStandardStreamId=v.ID
inner join [ODMERPDB].[dbo].Streams as w on v.StreamId=w.ID
where u.Active=1 and v.Active=1 and w.Active=1 and u.ID=a.AcademicStandardStreamId
) end) as Streamname,(case when a.AcademicStandardStreamId =0 then '0' else (select top(1) u.BoardStandardStreamId from [ODMERPDB].[dbo].[AcademicStandardStreams] as u

where u.Active=1 and u.ID=a.AcademicStandardStreamId
) end) as BoardStandardStreamId

 
  FROM [ODMERPDB].[dbo].[AcademicSections] as a
  inner join [ODMERPDB].[dbo].[AcademicStandards] as b on a.AcademicStandardId=b.ID
  inner join [ODMERPDB].[dbo].Sections as c on a.SectionId=c.ID
  inner join [ODMERPDB].[dbo].BoardStandards as d on b.BoardStandardId=d.ID
  inner join [ODMERPDB].[dbo].OrganizationAcademics as e on b.OrganizationAcademicId=e.ID
  inner join [ODMERPDB].[dbo].SchoolWing as f on b.SchoolWingId=f.ID
  inner join [ODMERPDB].[dbo].AcademicSessions as g on e.AcademicSessionId=g.ID
  inner join [ODMERPDB].[dbo].Organizations as h on e.OrganizationId=h.ID
  inner join [ODMERPDB].[dbo].Wing as i on f.WingID=i.ID
  inner join [ODMERPDB].[dbo].Boards as j on d.BoardId=j.ID
  inner join [ODMERPDB].[dbo].Standards as k on d.StandardId=k.ID
where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1 and
 j.Active=1 and k.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Academic_Subject> Get_View_All_Academic_Subject()
        {
            var res = context.view_All_Academic_Subjects.FromSql(@"select a.AcademicSessionId,a.ID as AcademicSubjectsID,a.SubjectId,b.BoardStandardId,b.BoardStandardStreamId,b.Name as Subjectname,
b.SubjectCode  
from [dbo].[AcademicSubjects] as a
inner join [dbo].[Subjects] as b on a.SubjectId=b.ID
where a.Active=1 and b.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Teachers> Get_View_All_Teachers()
        {
            var res = context.view_All_Teachers.FromSql(@"select a.ID,a.EmployeeId,a.EndDate,a.GroupId,a.StartDate,a.OrganizationId,(b.FirstName+' '+b.LastName) as Teachername,b.EmpCode,b.Isleft,(case when (a.StartDate<=GETDATE() and a.EndDate>=GETDATE()) then 'A' else 'I' end) as Teacherstatus from [dbo].[Teachers] as a
inner join [dbo].[Employees] as b on a.EmployeeId=b.ID
where a.Active=1 and b.Active=1").AsNoTracking().ToList();
            return res;
        }

        public List<View_StudentPaymentData> Get_View_StudentPaymentData(long studentid)
        {
            try
            {
                var res = context.View_StudentPaymentData.FromSql(@"select distinct l.ID as ID,c.OrganizationAcademicId as OrganizationAcademicId,
                        c.BoardStandardId as BoardStandardId, f.Name as OrganisationName,f.ID as OrganizationId,
                        d.AcademicSessionId as AcademicSessionId, e.DisplayName as SessionName,
                        g.StandardId as StandardId,h.Name as Standardname,h.StreamType as StandardStreamType,
                        c.SchoolWingId as SchoolWingId,a.ModifiedDate as ModifiedDate,j.Name as Wingname,j.ID as WingID,
                        a.StudentId as StudentId,(b.FirstName+' '+b.LastName) as studentname,
                        b.StudentCode as studentcode,k.Name as Boardname,
                        a.AcademicStandardStreamId as AcademicStandardStreamId,a.AcademicStandardId as AcademicStandardId,
                        (case when (a.AcademicStandardStreamId is null or a.AcademicStandardStreamId=0) then '' else 
                        (select top(1) Name from Streams 
                        where Active=1 and ID=(select top(1) StreamId from BoardStandardStreams 
                        where Active=1 and ID=(select top(1) BoardStandardStreamId from AcademicStandardStreams where Active=1 and ID=a.AcademicStandardStreamId))) end) as Streamname,
                        e.IsAvailable as IsAvailable,
						l.AcademicStudentId as AcademicStudentId,
						l.AcademicSectionId as AcademicSectionId,
						b.StudentCode as RollNo,
						n.Name as SectionName,n.ID as SectionId,0.00 as SumAmount,g.BoardId as BoardId,b.Image
                        from AcademicSectionStudents as l
						inner join AcademicStudents as a on l.AcademicStudentId=a.ID
						inner join AcademicSections as m on l.AcademicSectionId=m.ID
						inner join Sections as n on m.SectionId=n.ID
                        inner join Students as b on a.StudentId=b.ID
                        inner join AcademicStandards as c on a.AcademicStandardId=c.ID
                        inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                        inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                        inner join Organizations as f on d.OrganizationId=f.ID 
                        inner join BoardStandards as g on c.BoardStandardId=g.ID
                        inner join Standards as h on g.StandardId=h.ID
                        inner join SchoolWing as i on c.SchoolWingId=i.ID
                        inner join Wing as j on i.WingID=j.ID
                        inner join Boards as k on g.BoardId=k.ID
                        where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1
                        and j.Active=1 and k.Active=1 and l.Active=1 and m.Active=1 and n.Active=1 and a.StudentId=({0})", studentid).AsNoTracking().ToList();
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public List<getMandatoryPrice> Get_getMandatoryPrice(long? ApplicabledataId, string Applicabledata)
        {
            try
            {
                var res = context.getMandatoryPrice.FromSql(@"select Price,ApplicabledataId,Applicabledata FROM [dbo].[SessionFeesTypeApplicableData] pricedata
                left outer join [dbo].[SessionFeesType] sf on pricedata.FeesTypePriceId=sf.FeesTypeId and pricedata.ApplicableToId=sf.ApplicableId
                where sf.IsMandatory=1
                and pricedata.Active=1 and sf.Active=1 and pricedata.ApplicabledataId =({0}) and pricedata.Applicabledata =({1})", ApplicabledataId, Applicabledata).AsNoTracking().ToList();
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<getfeePrice> Get_getFeePrice(long? id)
        {
            try
            {
                var res = context.getfeePrice.FromSql(@"SELECT ISNULL(SUM(sp.Price),0) AS price FROM [dbo].[SessionFeesTypeApplicableData] sp
                WHERE sp.FeesTypePriceId=({0})
                and sp.Active=1 group by sp.ApplicabledataId", id).AsNoTracking().ToList();

                //////old logic
                //var res = context.getfeePrice.FromSql(@"SELECT ISNULL(SUM(sp.Price),0) AS price FROM [dbo].[SessionFeesTypeApplicableData] sp
                //WHERE sp.FeesTypePriceId IN(SELECT f.ID FROM [dbo].[FeesTypes] f where  f.Active=1 and f.HaveInstallment=1 and f.ParentFeeTypeID=({0}))
                //and sp.Active=1 group by ApplicabledataId", id).AsNoTracking().ToList();


                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        public List<getStudentPaymentData> getStudentPaymentData(long? PaymentCollectionId,long? StudentId)
        {
            try
            {
                var res = context.getStudentPaymentData.FromSql(@"SELECT PaymentCollectionId,PaidUserId,PaidDate
                 ,AmountPaid,TransactionId,PaymentTypeId
                 ,ISNULL(PaymentStatus,'Due') AS PaymentStatus,StudentId FROM [dbo].[StudentPayment] SP where SP.Active=1
                 and  SP.PaymentCollectionId =({0}) and SP.StudentId=({1})", PaymentCollectionId, StudentId).AsNoTracking().ToList();
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
    public class view_time_table_cls
    {
        public List<View_All_TimeTable_Subjects> timetable { get; set; }
        public long AcademicSectionId { get; set; }
        public long AcademicStandardId { get; set; }
        public long? AcademicStandardStreamId { get; set; }
        public DateTime date { get; set; }
        public long? typeId { get; set; }
        public View_All_Academic_Section _Academic_Standard_With_Stream { get; set; }
        public List<ClassTimingcls> timingcls { get; set; }
        public List<View_All_Academic_Subject> subjects { get; set; }
        public List<View_All_Teachers> teachers { get; set; }
        public List<View_StudentPaymentData> View_StudentPaymentData { get; set; }
        public List<getMandatoryPrice> getMandatoryPrice { get; set; }
        public List<getStudentPaymentData> getStudentPaymentData { get; set; }
    }
}
