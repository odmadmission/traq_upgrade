﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models
{
    public class ErpBugModel
    {
        public long ID { get; set; }

        [Display(Name = "Module")]        
        public long ModuleID { get; set; }

        public string ModuleName { get; set; }

        public List<ModulesCls> mod { get; set; }

        [Display(Name = "SubModule")]       
        public long SubModuleID { get; set; }
        public string SubModuleName { get; set; }

        [Display(Name = "Action")]      
        public long ActionID { get; set; }
        public string ActionName { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Please Fill The Title")]
        public string  Title { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please Fill The Description")]
        public string Description { get; set; }

        [Display(Name = "Assign To")]      
        public long? AssignToID { get; set; }
                 
        public long? AssignFromID { get; set; }

        [Display(Name = "Completion Date")]
        [Required(ErrorMessage = "Please Enter Completion Date")]
        public Nullable<DateTime> ComplitionDate { get; set; }
        
        public Nullable<DateTime> VarifiedDate { get; set; }
        //public IEnumerable<SelectListItem> Modules { get; set; }
        //public IEnumerable<SelectListItem> SubModules { get; set; }
        //public IEnumerable<SelectListItem> Actions { get; set; }
    }
}
