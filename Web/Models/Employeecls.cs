﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.DocumentAggregate;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models
{
    public class Employeecls
    {
        public long ID { get; set; }

       // [Remote(action: "SaveOrUpdateEmployeeProfile", controller: "Employee", AdditionalFields = nameof(EmpCode))]
        

        [Display(Name = "Employee Code*")]
        [Required(ErrorMessage = "Please Enter Employee Code")]
        public string EmpCode { get; set; }
        [Display(Name = "First Name*")]
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Display(Name = "Last Name*")]
        [Required(ErrorMessage = "Please Enter Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Gender")]
       // [Required(ErrorMessage = "Please Enter Gender")]
        public string Gender { get; set; }
        [Display(Name = "Date Of Birth")]
       // [Required(ErrorMessage = "Please Enter Date Of Birth")]
        public DateTime? DateOfBirth { get; set; }
        [Display(Name = "Blood Group")]
       // [Required(ErrorMessage = "Please Enter Blood Group")]
        public long? BloodGroupID { get; set; }
        public string BloodGroupName { get; set; }
         public IEnumerable<BloodGroup> bloodGroups { get; set; }
       
        [Display(Name = "Mobile No*")]
        [Required(ErrorMessage = "Please Enter Mobile No")]
        public string PrimaryMobile { get; set; }
        [Display(Name = "Alt Mobile No")]
        public string AlternativeMobile { get; set; }
        [Display(Name = "Landline No")]
        public string LandlineNumber { get; set; }
        [Display(Name = "Email Id*")]
        [Required(ErrorMessage = "Please Enter Email Id")]
        public string EmailId { get; set; }
        [Display(Name = "Nationality")]
       // [Required(ErrorMessage = "Please Enter Nationality")]
        public long? NatiobalityID { get; set; }
        public string NatiobalityName { get; set; }
         public IEnumerable<NationalityType> nationalityTypes { get; set; }
       
        [Display(Name = "Religion")]
       // [Required(ErrorMessage = "Please Enter Religion")]
        public long? ReligionID { get; set; }
        public string profile { get; set; }
        public string ReligionName { get; set; }
        public DateTime ModifiedDate { get; set; }
        
        public DateTime? JoinDate { get; set; }
        
        public List<documentTypesCls> documentTypesCls { get; set; }
        public IEnumerable<ReligionType> religionTypes { get; set; }
        
        public IEnumerable<EmployeeTypewithAddresscls> employeeadresslist { get; set; }
        public IEnumerable<EmployeeDesignationCls> employeeDesignationlist { get; set; }
        public IEnumerable<EmployeeTypewithAddresscls> employeeEducationlist { get; set; }
        public IEnumerable<EmployeeExperience> employeeExperiencelist { get; set; }
        public IEnumerable<EmployeeTypewithAddresscls> employeeBanks { get; set; }
        public List<EmployeeRequiredDocumentCls> requiredDocumentCls { get; set; }
        public List<EmployeeTimeline> employeeTimelines { get; set; }
        public EmployeeAccesscls employeeAccesscls { get; set; }
        public List<GivenAccesscls> employeeModuleSubModuleAccess { get; set; }
        public bool IsLeft { get; set; }
        public Nullable<DateTime> LeftDate { get; set; }
    }
    public class Employeeddlview
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public long typeid { get; set; }
        public long subtypeid { get; set; }
    }
    public class DocumentsCls
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public List<EmployeeDocumentDetailsCls> doc { get; set; }
    }
    public class EmployeeDocumentDetailsCls
    {
        public long ID { get; set; }
        public string DocumentTypeName { get; set; }
        public List<EmployeeRequiredDocumentCls> requiredDocumentCls { get; set; }
    }
   
    public class EmployeeRequiredDocumentCls
    {
        public long ID { get; set; }
        public long DocumentSubTypeID { get; set; }
        public string DocumentSubTypeName { get; set; }
        public bool IsMandatory { get; set; }
        public long DocumentTypeID { get; set; }
        public string DocumentTypeName { get; set; }
        public long RelatedID { get; set; }
        public string relatedDetails { get; set; }
        public long EmployeeID { get; set; }
        public bool IsVerified { get; set; }
        public long VerifiedAccessID { get; set; }
        public DateTime? VerifiedDate { get; set; }
        public string DocumentPath { get; set; }
        public bool IsApproved { get; set; }
        public bool IsReject { get; set; }
        public int totaldocument { get; set; }
        public bool verifyaccess { get; set; }
        public bool approveaccess { get; set; }
        public IFormFile file { get; set; }
    }
    public class RequiredDocCls
    {
        public long ID { get; set; }
        public bool IsVerified { get; set; }
        public IFormFile file { get; set; }
        public bool IsApproved { get; set; }
        public long EmployeeID { get; set; }
    }

    public class documentTypesCls
    {
        public long TypeId { get; set; }
        public long ID { get; set; }
        public string Name { get; set; }
        public bool IsRequired { get; set; }
    }

    public class EmployeeAdressCls
    {
        public long EmployeeAddressId { get; set; }
        public long EmployeeId { get; set; }
        public long ID { get; set; }
      
        [Display(Name = "Address Line1")]
        [Required(ErrorMessage = "Please Enter Address Line1")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line2")]
        [Required(ErrorMessage = "Please Enter Address Line2")]
        public string AddressLine2 { get; set; }
        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Please Enter Postal Code")]
        public string PostalCode { get; set; }
        [Display(Name = "City")]
        [Required(ErrorMessage = "Please Select City")]
        public long CityID { get; set; }
        [Display(Name = "State")]
        [Required(ErrorMessage = "Please Select State")]
        public long StateID { get; set; }
        [Display(Name = "Country")]
        [Required(ErrorMessage = "Please Select Country")]
        public long CountryID { get; set; }
        [Display(Name = "Address Type")]
        [Required(ErrorMessage = "Please Select Address Type")]
        public long AddressTypeID { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string AddressTypeName { get; set; }
        public IEnumerable<Country> countries { get; set; }
        public IEnumerable<State> states { get; set; }
        public IEnumerable<City> cities { get; set; }
        public IEnumerable<AddressType> addressTypes { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
    }
    public class EmployeeDesignationCls
    {
        public long ID { get; set; }
        public long EmployeeID { get; set; }
        [Display(Name = "Designation")]
        [Required(ErrorMessage = "Please Select Designation")]
        public long DesignationId { get; set; }
        [Display(Name = "Designation Level")]
       // [Required(ErrorMessage = "Please Select Designation Level")]
        public long DesignationLevelID { get; set; }
        [Display(Name = "Department")]
        [Required(ErrorMessage = "Please Select Department")]
        public long DepartmentID { get; set; }
        [Display(Name = "Organization")]
        [Required(ErrorMessage = "Please Select Organization")]
        public long OrganizationID { get; set; }
        [Display(Name = "Group")]
        [Required(ErrorMessage = "Please Select Group")]
        public long GroupID { get; set; }
        public string DesignationName { get; set; }
        public string DesignationLevelName { get; set; }
        public string DepartmentName { get; set; }
        public string OrganizationName { get; set; }
        public string GroupName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<Group> groups { get; set; }
        public IEnumerable<Organization> organizations { get; set; }
        public IEnumerable<CommonMasterModel> departments { get; set; }
        public IEnumerable<DesignationLevel> designationLevels { get; set; }
        public IEnumerable<Designation> designations { get; set; }

        public string DepartmentType { get; set; }

    }
    public class EmployeeEducationCls
    {
        public long ID { get; set; }
        [Display(Name = "Education Qualification")]
        [Required(ErrorMessage = "Please Select Education Qualification")]
        public long EducationQualificationID { get; set; }

        public string EducationQualificationName { get; set; }
        public IEnumerable<EducationQualification> educationQualifications { get; set; }
        [Display(Name = "Education Qualification Type")]
        [Required(ErrorMessage = "Please Select Education Qualification Type")]
        public long EducationQualificationTypeID { get; set; }
        public string EducationQualificationTypeName { get; set; }
        public IEnumerable<EducationQualificationType> educationQualificationTypes { get; set; }
        public long EmployeeID { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public List<documentTypesCls> documentTypesCls { get; set; }
        [Display(Name = "College/School")]
        [Required(ErrorMessage = "Please Enter College/School Name")]
        public string College { get; set; }
        [Display(Name = "University")]
        public string University { get; set; }
        [Display(Name = "From Year")]
        [Required(ErrorMessage = "Please Select From Year")]
        public Nullable<int> FromYear { get; set; }
        [Display(Name = "To Year")]
        [Required(ErrorMessage = "Please Select To Year")]
        public Nullable<int> ToYear { get; set; }
    }
    public class EmployeeExperienceCls
    {
        public long EmployeeID { get; set; }
        public long ID { get; set; }
        [Display(Name = "Organization Name")]
        [Required(ErrorMessage = "Please Enter Organization Name")]
        public string OrganizationName { get; set; }
        [Display(Name = "Designation")]
        [Required(ErrorMessage = "Please Enter Designation")]
        public string Designation { get; set; }
        [Display(Name = "Salary Per Anum")]
        [Required(ErrorMessage = "Please Enter Salary Per Anum")]
        public int SalaryPerAnum { get; set; }
        [Display(Name = "From Date")]
        [Required(ErrorMessage = "Please Enter From Date")]
        public DateTime FromDate { get; set; }
        [Display(Name = "To Date")]
        [Required(ErrorMessage = "Please Enter To Date")]
        public DateTime ToDate { get; set; }
        public List<documentTypesCls> documentTypesCls { get; set; }
    }

    public class EmployeeBankCls
    {
        public long ID { get; set; }
        public long BankTypeID { get; set; }
        public IEnumerable<Bank> banks { get; set; }
        [Display(Name = "Bank")]
        [Required(ErrorMessage = "Please Select Bank")]
        public long BankNameID { get; set; }
        public string BankName { get; set; }
        [Display(Name = "Account Number")]
        [Required(ErrorMessage = "Please Enter Account Number")]
        public string AccountNumber { get; set; }
        [Display(Name = "Branch Name")]
        [Required(ErrorMessage = "Please Enter Branch Name")]
        public string BankBranchName { get; set; }
        [Display(Name = "IFSC Code")]
        [Required(ErrorMessage = "Please Enter IFSC Code")]
        public string IfscCode { get; set; }
        [Display(Name = "Account Holder Name")]
        [Required(ErrorMessage = "Please Enter Account Holder Name")]
        public string AccountHolderName { get; set; }
        public long EmployeeID { get; set; }       
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public List<documentTypesCls> documentTypesCls { get; set; }
    }
    public class EmployeeDocumentCls
    {
        public long ID { get; set; }
        public long EmployeeRequiredDocumentID { get; set; }
        public IEnumerable<EmployeeRequiredDocument> employeeRequiredDocuments { get; set; }
        public IEnumerable<DocumentSubType> documentSubTypes { get; set; }
        public IEnumerable<DocumentType> documentTypes { get; set; }
        public long DocumentTypeID { get; set; }
        public long DocumentSubTypeID { get; set; }

        public string DocumentPath { get; set; }
        public long EmployeeID { get; set; }
        public bool IsVerified { get; set; }
        public long VerifiedAccessID { get; set; }
        public DateTime VerifiedDate { get; set; }
        public string Name { get; set; }
        public long? RelatedId { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
    public class EmployeeAccesscls
    {
        public long AccessID { get; set; }
        [Display(Name = "User Name")]
        [Required(ErrorMessage = "Please Enter User Name")]
        public string Username { get; set; }
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please Enter Password")]
        public string Password { get; set; }
        [Display(Name = "Role")]
        [Required(ErrorMessage = "Please Select Role")]
        public long RoleID { get; set; }
        public string RoleName { get; set; }
        public long EmployeeID { get; set; }
        public IEnumerable<Role> roles { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
    public class EmployeeModuleSubModuleAccess
    {
        public long ID { get; set; }
        public long standardid { get; set; }
        public long standardstreamid { get; set; }
        public List<ModulesCls> modules { get; set; }

    }
    public class ModulesCls
    {
        public long ID { get; set; }
        public string ModuleName { get; set; }
        public List<SubModulesDtCls> subModules { get; set; }
    }
    public class SubModulesDtCls
    {
        public long ID { get; set; }
        public string SubModuleName { get; set; }
        public List<ActionSubModuleClass> actionSubModules { get; set; }
    }
    public class ActionSubModuleClass
    {
        public long ID { get; set; }
        public string ActionName { get; set; }
        public bool IsRequired { get; set; }
        public long ActionAccessId { get; set; }
    }
    public class GivenAccesscls
    {
        public long ID { get; set; }
        public string ModuleName { get; set; }
        public string SubModule { get; set; }
    }
    public class SidebarAccesscls
    {
        public long ID { get; set; }
        public string ModuleName { get; set; }
        public List<SidebarSubmodulecls> sidebarSubmodulecls { get; set; }
    }
    public class SidebarSubmodulecls
    {
        public long ID { get; set; }
        public string SubmoduleName { get; set; }
        public string Url { get; set; }
    }
    public class EmployeeTypewithAddresscls
    {
        public long typeId { get; set; }
        public string typeName { get; set; }
        public EmployeeAdressCls employeeAdress { get; set; }
        public EmployeeEducationCls employeeEducation { get; set; }
        public EmployeeBankCls bankCls { get; set; }
    }
    public class employeejoiningdatescls
    {
        public long ID { get; set; }
        public long EmployeeId { get; set; }
        [Display(Name = "Date Of Joining")]
        [Required(ErrorMessage = "Please Enter Date Of Joining")]
        public Nullable<DateTime> JoiningOn { get; set; }
        [Display(Name = "Date Of Leaving")]
        public Nullable<DateTime> LeftOn { get; set; }
        public bool IsPrimary { get; set; }
    }
}
