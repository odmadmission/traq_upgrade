﻿using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models
{
    public class StudentCommonMethod
    {
        private IStudentAggregateRepository studentAggregateRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IBoardRepository boardRepository;
        private IStandardRepository standardRepository;
        public StudentCommonMethod(IStudentAggregateRepository studentAggregateRepo, IDocumentTypeRepository documentTypeRepo,
            IDocumentSubTypeRepository documentSubTypeRepo, IBoardRepository boardRepo, IStandardRepository standardRepo)
        {
            studentAggregateRepository = studentAggregateRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
                boardRepository = boardRepo;
            standardRepository = standardRepo;
        }
        public bool studentRequiredDocument(long stuId, long typeId, long subtypeId,long relatedId)
        {
            StudentRequiredDocument studentRequired = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.DocumentTypeID == typeId && a.DocumentSubTypeID==subtypeId && a.RelatedID == relatedId && a.StudentID == stuId).FirstOrDefault();
            if (studentRequired == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<StudentDocumentModelClass> GetAllDocumentByUsingStudentId(long id)
        {
            try
            {
                var StudentRequiredDocumentList = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.StudentID == id).ToList();
                var documentTypeList = documentTypeRepository.GetAllDocumentTypes();
                var documentSubTypeList = documentSubTypeRepository.GetAllDocumentSubTypes();
                var documentsList = studentAggregateRepository.GetAllStudentDocumentAttachment();
                var StudentAcademicList = studentAggregateRepository.GetAllStudentAcademic().Where(a=>a.StudentID==id);
                var BoardList = boardRepository.GetAllBoard().ToList();
                var ClassList = standardRepository.GetAllStandard().ToList();
                var res = (from a in StudentRequiredDocumentList
                           join b in documentTypeList on a.DocumentTypeID equals b.ID
                           join c in documentSubTypeList on a.DocumentSubTypeID equals c.ID
                           join d in StudentAcademicList on a.RelatedID equals d.ID
                           join e in BoardList on d.BoardID equals e.ID
                           join f in ClassList on d.ClassID equals f.ID
                           select new StudentDocumentModelClass
                           {
                               ID = a.ID,
                               totaldocument = documentsList.Where(m => m.StudentRequiredDocumentID == a.ID).ToList().Count,

                               DocumentSubTypeID = a.DocumentSubTypeID,
                               DocumentSubTypeName = c.Name,
                               DocumentTypeID = a.DocumentTypeID,
                               DocumentTypeName = b.Name,
                               StudentID = a.StudentID,
                               IsApproved = a.IsApproved,
                               IsMandatory = a.IsMandatory,
                               IsReject = a.IsReject,
                               IsVerified = a.IsVerified,
                               RelatedID = a.RelatedID,
                               VerifiedAccessID = a.VerifiedAccessID,
                               VerifiedDate = a.VerifiedDate,
                               BoardName=e.Name,
                               ClassName=f.Name,
                               OrganizationName=d.OrganizationName
                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }



         public List<StudentAcademicModel> GetAllStudentAcademicByStudentId(long id)
        {
            try
            {
                var StudentAcademicList = studentAggregateRepository.GetAllStudentAcademic().Where(a => a.StudentID == id).ToList();
                var BoardList = boardRepository.GetAllBoard().ToList();
                var ClassList = standardRepository.GetAllStandard().ToList();

                var res = (from a in StudentAcademicList
                           join b in BoardList on a.BoardID equals b.ID
                           join c in ClassList on a.ClassID equals c.ID
                           select new StudentAcademicModel
                           {
                               ID = a.ID,
                               BoardID = a.BoardID,
                               BoardName = b.Name,
                               ClassID = a.ClassID,
                               ClassName = c.Name,
                               OrganizationName = a.OrganizationName,
                               ModifiedDate = a.ModifiedDate,
                               FromDate=a.FromDate,ToDate=a.ToDate
                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public List<StudentAcademicModel> GetAllStudentAcademicListByStudent()
        {
            try
            {
                var StudentAcademicList = studentAggregateRepository.GetAllStudentAcademic().ToList();
                var BoardList = boardRepository.GetAllBoard().ToList();
                var ClassList = standardRepository.GetAllStandard().ToList();

                var res = (from a in StudentAcademicList
                           join b in BoardList on a.BoardID equals b.ID
                           join c in ClassList on a.ClassID equals c.ID
                           select new StudentAcademicModel
                           {
                               StudentID=a.StudentID,
                               ID = a.ID,
                               BoardID = a.BoardID,
                               BoardName = b.Name,
                               ClassID = a.ClassID,
                               ClassName = c.Name,
                               OrganizationName = a.OrganizationName,
                               ModifiedDate = a.ModifiedDate,
                               FromDate = a.FromDate,
                               ToDate = a.ToDate
                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public List<StudentDocumentModelClass> GetAllDocumentByUsingStudent()
        {
            try
            {
                var StudentRequiredDocumentList = studentAggregateRepository.GetAllStudentRequiredDocument().ToList();
                var documentTypeList = documentTypeRepository.GetAllDocumentTypes();
                var documentSubTypeList = documentSubTypeRepository.GetAllDocumentSubTypes();
                var documentsList = studentAggregateRepository.GetAllStudentDocumentAttachment();
                var StudentAcademicList = studentAggregateRepository.GetAllStudentAcademic();
                var BoardList = boardRepository.GetAllBoard().ToList();
                var ClassList = standardRepository.GetAllStandard().ToList();
                var res = (from a in StudentRequiredDocumentList
                           join b in documentTypeList on a.DocumentTypeID equals b.ID
                           join c in documentSubTypeList on a.DocumentSubTypeID equals c.ID                         
                           select new StudentDocumentModelClass
                           {
                               ID = a.ID,
                               totaldocument = documentsList.Where(m => m.StudentRequiredDocumentID == a.ID).ToList().Count,
                               DocumentSubTypeID = a.DocumentSubTypeID,
                               DocumentSubTypeName = c.Name,
                               DocumentTypeID = a.DocumentTypeID,
                               DocumentTypeName = b.Name,
                               StudentID = a.StudentID,
                               IsApproved = a.IsApproved,
                               IsMandatory = a.IsMandatory,
                               IsReject = a.IsReject,
                               IsVerified = a.IsVerified,
                               RelatedID = a.RelatedID,
                               VerifiedAccessID = a.VerifiedAccessID,
                               VerifiedDate = a.VerifiedDate,
                               BoardName = a.RelatedID!=0? BoardList.Where(x=>x.ID == StudentAcademicList.Where(z => z.ID == a.RelatedID).Select(z=>z.BoardID).FirstOrDefault()).Select(s=>s.Name).FirstOrDefault() :"NA" ,
                               ClassName = a.RelatedID != 0 ? ClassList.Where(x => x.ID == StudentAcademicList.Where(z => z.ID == a.RelatedID).Select(z => z.ClassID).FirstOrDefault()).Select(s => s.Name).FirstOrDefault() : "NA",
                               OrganizationName = StudentAcademicList.Where(z=>z.ID==a.RelatedID).Select(z=>z.OrganizationName).FirstOrDefault(),
                               
                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
    }
}
