﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Web.Areas.Tasks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OdmErp.Web.Models.Background
{
    public class TimedHostedService : IHostedService
    {
        private readonly ILogger _log;
        private Timer _timer;
        private Timer _weeklytimer;
        private Timer _monthlytimer;

        //santanu
        private Timer _paymentBeforeTenDays;
        private Timer _paymentnow;

        //For Support Deadline
        private Timer _deadlinealerttimer;
        private Timer _deadlinetimer;

        private readonly IServiceScopeFactory _serviceScopeFactory;
        //  private IEmployeeRepository employeeRepository;

        public TimedHostedService(ILogger<TimedHostedService> log, IServiceScopeFactory serviceScopeFactory)
        {
            _log = log;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            
            DateTime now = DateTime.Now;

            //santanu
            DateTime BeforeTenDays = DateTime.Today.AddHours(06.0).AddMinutes(12.0)/*.AddDays(10.0)*/;
            DateTime recent = DateTime.Today;


            DateTime Daily = DateTime.Today.AddHours(18.0).AddMinutes(12.0);
            // DateTime weekly = DateTime.Today.AddHours(18.0).AddMinutes(12.0);
            DateTime monthly = DateTime.Today.AddHours(18.0).AddMinutes(12.0);
            // int daysUntilTuesday = ((int)DayOfWeek.Saturday - (int)now.DayOfWeek + 7) % 7;
            DateTime weekly = DateTime.Today.AddHours(18.0).AddMinutes(12.0);
            //Before 24 hr
            DateTime deadlinealertdt = Convert.ToDateTime(DateTime.Now.ToString("dd-MM-yyyy") + " 09:59:59");
            //Before 4 hr
            DateTime deadlindt = Convert.ToDateTime(DateTime.Now.ToString("dd-MM-yyyy") + " 13:59:59");


            if (now > Daily)
            {
                Daily = Daily.AddDays(1.0);
            }
            if (now > deadlinealertdt)
            {
                deadlinealertdt = deadlinealertdt.AddDays(1.0);
            }
            if (now > deadlindt)
            {
                deadlindt = deadlindt.AddDays(1.0);
            }
            if (now > weekly)
            {
                weekly = weekly.AddDays(7.0);
            }
            if (now > BeforeTenDays)
            {
                BeforeTenDays = BeforeTenDays.AddDays(10.0);
            }

            DateTime startOfWeek = new DateTime(now.Year, now.Month, 1);
            DateTime endOfWeek = startOfWeek.AddMonths(1).AddDays(-1);
            if (now == endOfWeek)
            {
                monthly = monthly.AddMonths(1).AddDays(-1);
            }



            var logs = new List<string>();
            logs.Add(Daily.ToString());
            logs.Add(deadlinealertdt.ToString());
            logs.Add(deadlindt.ToString());
            logs.Add(weekly.ToString());
            logs.Add(monthly.ToString());
            var timeToGo = Daily - now;
            logs.Add(timeToGo.ToString());

            var weektogo = weekly - now;
            logs.Add(weektogo.ToString());

            var monthtogo = monthly - now;
            logs.Add(monthtogo.ToString());

            var deadlinealertdtToGo = deadlinealertdt - now;
            logs.Add(deadlinealertdtToGo.ToString());

            var deadlindtToGo = deadlindt - now;
            logs.Add(deadlindtToGo.ToString());


            //santanu
            var PaymentBeforeTenDays = BeforeTenDays - now;
            logs.Add(PaymentBeforeTenDays.ToString());
            var paymentNow = DateTime.Now - now;
            logs.Add(paymentNow.ToString());
            //var 

            _timer = new Timer(DoWorkAsync, null, timeToGo, TimeSpan.FromDays(1));
            //Support Deadline
            _deadlinealerttimer = new Timer(deadlinealert, null, deadlinealertdtToGo, TimeSpan.FromDays(1));
            _deadlinetimer = new Timer(deadlinealertbeforefourhr, null, deadlindtToGo, TimeSpan.FromDays(1));

            _weeklytimer = new Timer(weeklytaskreportAsync, null, weektogo, TimeSpan.FromDays(7));

            //santanu
            _paymentBeforeTenDays = new Timer(TenDaysPaymentReminderAsync, null, PaymentBeforeTenDays, TimeSpan.FromDays(0));
            _paymentnow = new Timer(NowPaymentReminderAsync, null, paymentNow, TimeSpan.FromDays(0));
            //g_monthlytimer = new Timer(weeklytaskreportAsync, null, monthtogo, TimeSpan.FromDays(DateTime.DaysInMonth(now.Year, (now.Month + 1))));
            return Task.CompletedTask;
        }

        private async void deadlinealertbeforefourhr(object state)
        {
            try
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<Areas.Tasks.Models.TaskServices>();
                    var context1 = scope.ServiceProvider.GetRequiredService<Areas.Support.Models.SupportServices>();
                    context1.supportdeadlinesbefore4hr();
                    context.sendtaskdeadlinemailbefore4hr();
                    // now do your work
                }

            }
            catch
            {

            }
        }

        private async void deadlinealert(object state)
        {
            try
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<Areas.Tasks.Models.TaskServices>();
                    var context1 = scope.ServiceProvider.GetRequiredService<Areas.Support.Models.SupportServices>();
                    var context2 = scope.ServiceProvider.GetRequiredService<Areas.Admission.Models.AdmissionService>();
                    context1.supportdeadlinesbefore24hr();
                    context.sendtaskdeadlinemailbefore24hr();
                    context2.sendadmissionpaymentremainderafter3days();
                    context2.sendadmissionregistrationremainderafter3days();
                    // now do your work
                }

            }
            catch
            {

            }
        }

        private async void DoWorkAsync(object state)
        {
            //var mylog = Path.Combine(Path.GetTempPath(), "mylog.txt");
            //var logs = new List<string>();
            //logs.Add("Resync started at" + DateTime.UtcNow.ToString("s"));
            //_taskServices.sendmailtaskdeadlines();
            //File.AppendAllLines(mylog, logs);
            //my task here
            try
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<Areas.Support.Models.SupportServices>();
                    // context.sendmailtaskdeadlines();
                    // now do your work
                }

            }
            catch
            {

            }
        }
        private async void weeklytaskreportAsync(object state)
        {
            try
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<Areas.Tasks.Models.TaskServices>();
                    context.Sendweeklymailtaskreport();
                    // now do your work
                }

            }
            catch
            {

            }
        }

        //santanu
        private async void TenDaysPaymentReminderAsync(object state)
        {
            try
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<PaymentServices>();
                    context.SendPaymentRemindermail();
                    // now do your work
                }

            }
            catch
            {

            }
        }
        private async void NowPaymentReminderAsync(object state)
        {
            try
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<PaymentServices>();
                    context.SendPaymentRemindermail();
                    // now do your work
                }

            }
            catch
            {

            }
        }
        private async void monthlytaskreportAsync(object state)
        {
            try
            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<Areas.Tasks.Models.TaskServices>();
                    context.Sendmonthlymailtaskreport();
                    // now do your work
                }
            }
            catch
            {

            }
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _log.LogInformation("RecureHostedService is Stopping");
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }


    }
}
