﻿using DocumentFormat.OpenXml.Office2013.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.SqlServer.Management.Assessment.Configuration;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Entities.GrievanceAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static OdmErp.Web.Models.CommonMethods;

namespace OdmErp.Web.Models
{
    public class GrievanceModalClass
    {
        private IGrievanceRepository grievanceRepository;
        private IEmployeeRepository employeeRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IRoleRepository roleRepository;
        private IParentRepository parentRepository;
        private IGrievanceExtendedTimelinesRepository grievanceExtendedTimelinesRepository;
        private CommonMethods commonMethods;
        private readonly ApplicationDbContext _DbContext;

        public GrievanceModalClass(IGrievanceExtendedTimelinesRepository grievanceExtendedTimelinesRepo,IGrievanceRepository grievanceReposito, IEmployeeRepository employeeRepo, IStudentAggregateRepository studentAggregateRepo, IRoleRepository roleRepo, IParentRepository parentReposito, CommonMethods commonMethod, ApplicationDbContext DbContext)
        {
            grievanceExtendedTimelinesRepository = grievanceExtendedTimelinesRepo;
             grievanceRepository = grievanceReposito;
            employeeRepository = employeeRepo;
            studentAggregateRepository = studentAggregateRepo;
            roleRepository = roleRepo;
            parentRepository = parentReposito;
            commonMethods = commonMethod;
            _DbContext = DbContext;
        }

        public IEnumerable<GrievanceViewClass> GetAllGrievanceForView()
        {
            try
            {
                var grievance = grievanceRepository.GetAllGrievance().Where(a => a.GrievanceRelatedId != 1 && a.InsertedId != 1).ToList();
                var grievancetype = grievanceRepository.GetAllGrievanceType();
                var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievancecategory = grievanceRepository.GetAllGrievanceCategory();
                var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                var grievanceemployee = grievanceRepository.GetAllEmployeeGrievance();
                var roles = roleRepository.GetAllRole();
                var grievanceattachment = grievanceRepository.GetAllGrievanceAttachment();
                var grievancetimelines = grievanceRepository.GetAllGrievanceTimeline().Where(a => a.GrievanceStatusID == 4).ToList();

                IEnumerable<GrievanceViewClass> res = (from a in grievance
                                                       join b in grievancetype on a.GrievanceTypeID equals b.ID
                                                       join c in grievancestatus on a.GrievanceStatusID equals c.ID
                                                       join f in roles on a.GrievanceCategoryID equals f.ID
                                                       select new GrievanceViewClass
                                                       {
                                                           ID = a.ID,
                                                           GrievanceCategoryName = f.Name,
                                                           GrievanceTypeName = b.Name,
                                                           GrievanceStatusName = c.Name,
                                                           Code = a.Code,
                                                           Description = a.Description,
                                                           CompletionId = a.CompletionId,
                                                           CompletionName = a.CompletionId == 0 ? "" : getEmployeeById(a.CompletionId),
                                                           CompletionDate = a.CompletionDate,
                                                           VerificationDate = a.VerificationDate,
                                                           VerifiedId = a.VerifiedId,
                                                           VerifiedName = a.VerifiedId == 0 ? "" : getEmployeeById(a.VerifiedId),
                                                           RequestType = a.RequestType,
                                                           GrievancePriorityName = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.GrievancePriorityID).FirstOrDefault().Name,
                                                           GrievanceRelatedId = a.GrievanceRelatedId,
                                                           GrievanceRelatedName = a.GrievanceRelatedId == 0 ? "" : getRoleById(a.GrievanceCategoryID, a.GrievanceRelatedId),
                                                           attachments = grievanceattachment.Where(u => u.GrievanceID == a.ID && u.InsertedId == a.InsertedId).ToList(),
                                                           employeeattachments = grievanceattachment.Where(t => t.GrievanceID == a.ID && t.InsertedId == a.CompletionId).ToList(),
                                                           GrievanceCategoryID = f.ID,
                                                           GrievancePriorityID = a.GrievancePriorityID,
                                                           GrievanceStatusID = c.ID,
                                                           GrievanceTypeID = b.ID,
                                                           RelatedCode = getCodeById(a.GrievanceCategoryID, a.GrievanceRelatedId),
                                                           CreatedDate = a.InsertedDate,
                                                           RelatedMobile = getMobileById(a.GrievanceCategoryID, a.GrievanceRelatedId),
                                                           RoleId=a.RoleId,
                                                           grievanceempcls = (from m in grievanceemployee
                                                                              where m.GrievanceID == a.ID

                                                                              select new GrievanceEmployeeClass
                                                                              {
                                                                                  AssignedByID = m.InsertedId,
                                                                                  AssignedOn = m.InsertedDate,
                                                                                  AssignedByName = getEmployeeById(m.InsertedId),
                                                                                  AssignedToName = getEmployeeById(m.EmployeeID),
                                                                                  EmployeeID = m.EmployeeID,
                                                                                  GrievanceID = m.GrievanceID


                                                                              }).FirstOrDefault(),
                                                           grievancecommentscls = getgrievancecommentscls(a.ID,0,0,false)
                                                       }).ToList().OrderByDescending(a => a.ID);
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        //public class AuthorizedPersonClass
        //{
        //    public long Id { get; set; }
        //    public string Code { get; set; }
        //    public string Name { get; set; }

        //}
        public List<Employeeddlview> GrievanceAuthorizedPerson(long grivid)
        {
            var grievance = grievanceRepository.GetGrievanceById(grivid);
            var grivAccess = _DbContext.GrievanceAccess.Where(a => a.GrievanceCategoryId == grievance.GrievanceCategoryID && a.GrievanceTypeId == grievance.GrievanceTypeID).ToList();
            var empList = employeeRepository.GetAllEmployee().ToList();
            var res = (from a in grivAccess
                       join b in empList on a.EmployeeId equals b.ID
                       select new Employeeddlview
                       {
                           Id = a.ID,
                           Code = b.EmailId,
                           Name = b.FirstName + " " + b.LastName
                       }).ToList();

            return res;
        }
        public List<Employeeddlview> GrievanceAuthorizedDeptHeadPerson(long grivid)
        {
            var grievance = grievanceRepository.GetGrievanceById(grivid);
            var grivAccess = _DbContext.GrievanceAccess.Where(a => a.GrievanceCategoryId == grievance.GrievanceCategoryID && a.GrievanceTypeId == 0).ToList();
            var empList = employeeRepository.GetAllEmployee().ToList();
            var res = (from a in grivAccess
                       join b in empList on a.EmployeeId equals b.ID
                       select new Employeeddlview
                       {
                           Id = a.ID,
                           Code = b.EmailId,
                           Name = b.FirstName + " " + b.LastName
                       }).ToList();

            return res;
        }
        public string GetGrievanceAuthorizedPerson(long id)
        {
            var grievance = grievanceRepository.GetAllGrievance().Where(a => a.ID == id).FirstOrDefault();
            var grivanceAccess = _DbContext.GrievanceAccess.ToList();
            var types = grivanceAccess.Where(a => a.Active == true && a.GrievanceCategoryId == grievance.GrievanceCategoryID && a.GrievanceTypeId==0).ToList();
            var subtypes = grivanceAccess.Where(a => a.Active == true && a.GrievanceTypeId == grievance.GrievanceTypeID).ToList();
            //var grievanceType = grievanceRepository.GetAllGrievanceCategory().Where(a => a.ID == grivanceAccess.GrievanceCategoryId).FirstOrDefault();
            //var grievanceSubType = grievanceRepository.GetAllGrievanceType().Where(a => a.ID == grivanceAccess.GrievanceTypeId).FirstOrDefault();
            string empList = null;

            if (types.Count > 0)
            {
                foreach (var a in types)
                {
                    empList += getEmployeeById(a.EmployeeId) + ",";
                }
                empList = empList.Substring(0, (empList.Length - 1));
                bool first = false;
                if (grievance.GrievanceTypeID != 0)
                {
                    if (subtypes.Count > 0)
                    {
                        foreach (var a in subtypes)
                        {
                            if (first == false)
                            {
                                empList += "-" + getEmployeeById(a.EmployeeId) + ",";
                                first = true;
                            }
                            else
                            {
                                empList += getEmployeeById(a.EmployeeId) + ",";
                            }
                        }
                        empList = empList.Substring(0, (empList.Length - 1));

                    }
                }
            }

            return empList;
        }
        public GrievanceViewClass GetAllGrievanceDetails(long id, long userId, long roleId)
        {
            try
            {
                var grievance = grievanceRepository.GetAllGrievance();
                var grievanceType = grievanceRepository.GetAllGrievanceCategory();
                var grievanceSubType = grievanceRepository.GetAllGrievanceType();
                //var grievancecategory = grievanceRepository.GetAllGrievanceCategory();
                //var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                var grievanceemployee = grievanceRepository.GetAllEmployeeGrievance();
                //var roles = roleRepository.GetAllRole();
                var employeeList = employeeRepository.GetAllRequiredEmployee();
                var grievanceattachment = grievanceRepository.GetAllGrievanceAttachment();
                // var grievancetimelines = grievanceRepository.GetGrievanceTimelineById()
                //var grivForward = _DbContext.GrievanceForwards.Where(a => a.Active == true);
                var grivExtended = _DbContext.GrievanceExtendedTimelines.Where(a => a.Active == true);

                GrievanceViewClass res = (from a in grievance
                                              //join b in grievanceType on a.GrievanceCategoryID equals b.ID
                                          where a.ID == id
                                          select new GrievanceViewClass
                                          {
                                              ID = a.ID,
                                              GrievanceCategoryName = a.GrievanceCategoryID == 0 ? "" : grievanceType.Where(m => m.ID == a.GrievanceCategoryID).FirstOrDefault().Name,
                                              GrievanceTypeName = a.GrievanceCategoryID == 0 ? "" : grievanceType.Where(m => m.ID == a.GrievanceCategoryID).FirstOrDefault().Name,
                                              GrievanceStatusName = a.GrievanceTypeID == 0 ? "" : grievanceSubType.Where(m => m.ID == a.GrievanceTypeID).FirstOrDefault().Name,
                                              Code = a.Code,
                                              Description = a.Description,
                                              CompletionId = a.CompletionId,
                                              CompletionName = a.CompletionId == 0 ? "" : getEmployeeById(a.CompletionId),
                                              CompletionDate = a.CompletionDate,
                                              VerificationDate = a.VerificationDate,
                                              VerifiedId = a.VerifiedId,
                                              VerifiedName = a.VerifiedId == 0 ? "" : getEmployeeById(a.VerifiedId),
                                              RequestType = a.RequestType,
                                              //  GrievancePriorityName = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.GrievancePriorityID).FirstOrDefault().Name,
                                              GrievanceRelatedId = a.GrievanceRelatedId,
                                              GrievanceRelatedName = a.GrievanceRelatedId == 0 ? "" : getRoleById(a.RoleId, a.GrievanceRelatedId),
                                              Grivattachments = grievanceattachment.Where(u => u.GrievanceID == a.ID && u.InsertedId == a.InsertedId).LastOrDefault(),
                                              employeeattachments = grievanceattachment.Where(t => t.GrievanceID == a.ID).ToList(),
                                              GrievanceCategoryID = a.GrievanceCategoryID,
                                              GrievancePriorityID = a.GrievancePriorityID,
                                              GrievanceStatusID = a.GrievanceTypeID,
                                              RoleId = a.RoleId,
                                              GrievanceTypeID = a.GrievanceCategoryID,
                                              RelatedCode = getCodeById(a.RoleId, a.GrievanceRelatedId),
                                              CreatedDate = a.InsertedDate,
                                              StatusId = a.GrievanceStatusID,
                                              StatusName = commonMethods.GrievanceStatus(0, a.GrievanceStatusID,0).StatusName,
                                              IsGrievanceForward = IsGrievanceIsForward(a.ID),
                                              IsTimelineExtended = IsGrievanceTimelineIsExtended(a.ID),
                                              AuthorizedPerson = GetGrievanceAuthorizedPerson(id) != null ? GetGrievanceAuthorizedPerson(id) : "NA",
                                              RelatedMobile = getMobileById(a.RoleId, a.GrievanceRelatedId),
                                              grievanceempcls = (from m in grievanceemployee
                                                                 where m.GrievanceID == a.ID

                                                                 select new GrievanceEmployeeClass
                                                                 {
                                                                     AssignedByID = m.InsertedId,
                                                                     AssignedOn = m.InsertedDate,
                                                                     AssignedByName = getEmployeeById(m.InsertedId),
                                                                     AssignedToName = getEmployeeById(m.EmployeeID),
                                                                     EmployeeID = m.EmployeeID,
                                                                     GrievanceID = m.GrievanceID


                                                                 }).FirstOrDefault(),
                                              grievancecommentscls = getgrievancecommentsclses(a.ID, roleId) == null ? null : getgrievancecommentsclses(a.ID, roleId).OrderByDescending(m => m.ID).Take(5).OrderBy(m => m.ID).ToList(),
                                              grievanceinhousecommentscls = getgrievanceinhousecommentscls(a.ID,userId) == null ? null : getgrievanceinhousecommentscls(a.ID,userId).OrderByDescending(m => m.ID).Take(5).OrderBy(m => m.ID).ToList(),
                                              grievanceTimelines = (from s in grievanceRepository.GetAllGrievanceTimelineByGrivId(a.ID).ToList()
                                                                    select new GrievanceTimeLineModel
                                                                    {
                                                                        PostedBy = getRoleById(s.RoleId, s.InsertedId),
                                                                        PostedDateTime = s.InsertedDate,
                                                                        Status = commonMethods.GrievanceStatus(s.ID, s.GrievanceStatusID,s.RoleId)
                                                                    }).ToList(),
                                              ForwardDetail = GrievanceForwardDetails(a.ID, userId),
                                              ExtendedTimelineDetail = grievanceExtendedTimelinesRepository.ListAllAsync().Result.Where(s => s.GrievanceId == a.ID) != null ?
                                                                            GrievanceExtendedTimelineDetails(a.ID) : null,
                                              GrievaneSolvedDetails = a.GrievanceStatusID == 4 ? GrivCompletionDetails(a.ID,roleId) : a.GrievanceStatusID == 6 ? GrivCompletionDetails(a.ID,roleId) : null,
                                              isCommentUpdate = updateGrievanceCommentIsRead(a.ID, userId),
                                              IsRequestedForStudentDetails = a.IsRequestedForStudentDetails,
                                              RequestedName = a.RequestedId != 0 ? getEmployeeById(a.RequestedId) : "",
                                              IsApprovedForStudentDetails = a.IsApproveForStudentDetails,
                                              ApprovedName = a.ApprovedId != 0 ? getEmployeeById(a.ApprovedId) : "",
                                              LoggedInName = getEmployeeById(userId),
                                              IsRejectedForStudentDetails = a.IsRejectedForStudentDetails
                                          }).FirstOrDefault();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public GrievanceViewClass GetGrievanceDetails(long id, long userId,long roleId)
        {
            try
            {
                var grievance = grievanceRepository.GetAllGrievance();
                var grievanceType = grievanceRepository.GetAllGrievanceCategory();
                var grievanceSubType = grievanceRepository.GetAllGrievanceType();
                //var grievancecategory = grievanceRepository.GetAllGrievanceCategory();
                //var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                var grievanceemployee = grievanceRepository.GetAllEmployeeGrievance();
                //var roles = roleRepository.GetAllRole();
                var employeeList = employeeRepository.GetAllRequiredEmployee();
                var grievanceattachment = grievanceRepository.GetAllGrievanceAttachment();
                // var grievancetimelines = grievanceRepository.GetGrievanceTimelineById()
                //var grivForward = _DbContext.GrievanceForwards.Where(a => a.Active == true);
                var grivExtended = _DbContext.GrievanceExtendedTimelines.Where(a => a.Active == true);

                GrievanceViewClass res = (from a in grievance
                                          //join b in grievanceType on a.GrievanceCategoryID equals b.ID
                                          where a.ID == id
                                          select new GrievanceViewClass
                                          {
                                              ID = a.ID,
                                              GrievanceCategoryName = a.GrievanceCategoryID == 0 ? "" : grievanceType.Where(m => m.ID == a.GrievanceCategoryID).FirstOrDefault().Name,
                                              GrievanceTypeName = a.GrievanceCategoryID == 0 ? "" : grievanceType.Where(m => m.ID == a.GrievanceCategoryID).FirstOrDefault().Name,
                                              GrievanceStatusName = a.GrievanceTypeID == 0 ? "" : grievanceSubType.Where(m => m.ID == a.GrievanceTypeID).FirstOrDefault().Name,
                                              Code = a.Code,
                                              Description = a.Description,
                                              CompletionId = a.CompletionId,
                                              CompletionName = a.CompletionId == 0 ? "" : getEmployeeById(a.CompletionId),
                                              CompletionDate = a.CompletionDate,
                                              VerificationDate = a.VerificationDate,
                                              VerifiedId = a.VerifiedId,
                                              VerifiedName = a.VerifiedId == 0 ? "" : getEmployeeById(a.VerifiedId),
                                              RequestType = a.RequestType,
                                              //  GrievancePriorityName = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.GrievancePriorityID).FirstOrDefault().Name,
                                              GrievanceRelatedId = a.GrievanceRelatedId,
                                              GrievanceRelatedName = a.GrievanceRelatedId == 0 ? "" : getRoleById(a.RoleId, a.GrievanceRelatedId),
                                              Grivattachments = grievanceattachment.Where(u => u.GrievanceID == a.ID && u.InsertedId == a.InsertedId).LastOrDefault(),
                                              employeeattachments = grievanceattachment.Where(t => t.GrievanceID == a.ID).ToList(),
                                              GrievanceCategoryID = a.GrievanceCategoryID,
                                              GrievancePriorityID = a.GrievancePriorityID,
                                              GrievanceStatusID = a.GrievanceTypeID,
                                              RoleId = a.RoleId,
                                              GrievanceTypeID = a.GrievanceCategoryID,
                                              RelatedCode = getCodeById(a.RoleId, a.GrievanceRelatedId),
                                              CreatedDate = a.InsertedDate,
                                              StatusId = a.GrievanceStatusID,
                                              StatusName = commonMethods.GrievanceStatus(0, a.GrievanceStatusID,0).StatusName,
                                              IsGrievanceForward = IsGrievanceIsForward(a.ID),
                                              IsTimelineExtended = IsGrievanceTimelineIsExtended(a.ID),
                                              AuthorizedPerson = GetGrievanceAuthorizedPerson(id) != null ? GetGrievanceAuthorizedPerson(id) : "NA",
                                              RelatedMobile = getMobileById(a.RoleId, a.GrievanceRelatedId),
                                              grievanceempcls = (from m in grievanceemployee
                                                                 where m.GrievanceID == a.ID

                                                                 select new GrievanceEmployeeClass
                                                                 {
                                                                     AssignedByID = m.InsertedId,
                                                                     AssignedOn = m.InsertedDate,
                                                                     AssignedByName = getEmployeeById(m.InsertedId),
                                                                     AssignedToName = getEmployeeById(m.EmployeeID),
                                                                     EmployeeID = m.EmployeeID,
                                                                     GrievanceID = m.GrievanceID


                                                                 }).FirstOrDefault(),
                                              grievancecommentscls = getgrievancecommentscls(a.ID, roleId, userId, a.IsApproveForStudentDetails) == null ? null : getgrievancecommentscls(a.ID, roleId, userId, a.IsApproveForStudentDetails).OrderByDescending(m => m.ID).Take(5).OrderBy(m => m.ID).ToList(),
                                              grievanceinhousecommentscls = getgrievanceinhousecommentscls(a.ID,userId) == null ? null : getgrievanceinhousecommentscls(a.ID,userId).OrderByDescending(m => m.ID).Take(5).OrderBy(m => m.ID).ToList(),
                                              grievanceTimelines = (from s in grievanceRepository.GetAllGrievanceTimelineByGrivId(a.ID).ToList()
                                                                    select new GrievanceTimeLineModel
                                                                    {
                                                                        PostedBy = a.IsApproveForStudentDetails == true ? getRoleById(s.RoleId, s.InsertedId) : 
                                                                        roleId == 5 ? getRoleById(s.RoleId, s.InsertedId) : roleId == 6 ? getRoleById(s.RoleId, s.InsertedId) : getRoleByIds(s.RoleId, s.InsertedId),
                                                                        PostedDateTime = s.InsertedDate,
                                                                        Status = commonMethods.GrievanceStatus(s.ID, s.GrievanceStatusID,s.RoleId)
                                                                    }).ToList(),
                                              ForwardDetail = GrievanceForwardDetails(a.ID, userId),
                                              ExtendedTimelineDetail = grievanceExtendedTimelinesRepository.ListAllAsync().Result.Where(s => s.GrievanceId == a.ID) != null ?
                                                                            GrievanceExtendedTimelineDetails(a.ID) : null,
                                              GrievaneSolvedDetails = a.GrievanceStatusID == 4 ? GrivCompletionDetails(a.ID,roleId) : a.GrievanceStatusID == 6 ? GrivCompletionDetails(a.ID,roleId) : null,
                                              isCommentUpdate = updateGrievanceCommentIsRead(a.ID,userId),
                                              IsRequestedForStudentDetails = a.IsRequestedForStudentDetails,
                                              RequestedName = a.RequestedId != 0 ? getEmployeeById(a.RequestedId) : "",
                                              IsApprovedForStudentDetails = a.IsApproveForStudentDetails,
                                              ApprovedName = a.ApprovedId != 0 ? getEmployeeById(a.ApprovedId) : "",
                                              IsRejectedForStudentDetails = a.IsRejectedForStudentDetails,
                                              IsRejected = a.IsGrievanceRejected,
                                              Reason = a.RejectReason
                                          }).FirstOrDefault();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
               
        public List<GrievanceExtendedTimelineModal> GrievanceExtendedTimelineDetails(long grivid)
        {
            var exttimeline = grievanceExtendedTimelinesRepository.ListAllAsync().Result.Where(s => s.GrievanceId == grivid).ToList();
            var res = (from a in exttimeline
                       select new GrievanceExtendedTimelineModal
                       {
                           ID = a.ID,
                           ExtendedDate = a.ExtendedDate,
                           //ExtendedTime = a.ExtendedTime != null ? a.ExtendedTime?.ToString("hh:mm tt") : "",
                           ExtendedReason = a.ExtendedReason,
                           ExtendedBy = getEmployeeById(a.InsertedId)
                       }).ToList();

            return res;
        }
        public bool updateGrievanceCommentIsRead(long grivId,long userId)
        {
            var comments = grievanceRepository.GetAllGrievanceComment().Where(a => a.Active == true).ToList();
            var result = comments.Where(s => s.GrievanceID == grivId).ToList();
            foreach (var k in result)
            {
                if (k.InsertedId != userId)
                {
                    k.IsRead = true;
                    grievanceRepository.UpdateGrievanceComment(k);
                    //_DbContext.SaveChanges();
                }
            }

            return true;
        }


        public GrievanceViewClass GrivCompletionDetails(long grivid,long roleId)
        {
            var griv = grievanceRepository.GetAllGrievance().Where(a => a.ID == grivid)
                        .Select(a => new GrievanceViewClass { CompletionId = a.CompletionId, CompletionName = getEmployeeById(a.CompletionId), CompletionDate = a.CompletionDate, EstimateDate = a.CompletionDate - a.InsertedDate,
                            VerifiedName = GetVerifiedName(a.VerifiedRoleId,a.VerifiedId,roleId),Score = a.Score,VerifiedId = a.VerifiedId,VerifyRemark = a.VerifyRemark
                        }).FirstOrDefault();
            return griv;
        }
        public string GetVerifiedName(long roleid,long verifiedId,long loginRoleId)
        {
            string name = null;
            if (loginRoleId == 5 || loginRoleId == 6)  //if login user is parent or student
            {
                if (roleid == 6)
                {
                    var parent = parentRepository.GetParentById(verifiedId);
                    name = parent.FirstName + " " + parent.LastName;
                }
                if (roleid == 5)
                {
                    var student = studentAggregateRepository.GetStudentById(verifiedId);
                    name = student.FirstName + " " + student.LastName;
                }
            }
            else            //if login user is a employee
            {
                name = "XXXXXXXXX";
            }

            return name;
        }
        public List<GrievanceForwardDetails> GrievanceForwardDetails(long grivid, long userId)
        {
            try
            {
                var grivance = grievanceRepository.GetAllGrievance();
                var grivForward = _DbContext.GrievanceForwards.Where(a => a.Active == true && a.GrievanceId == grivid).ToList();
                if (grivForward.Count() > 0)
                {
                    var res = (from a in grivForward
                               join b in grivance on a.GrievanceId equals b.ID
                               select new GrievanceForwardDetails
                               {
                                   ID = a.ID,
                                   GrievanceId = grivid,
                                   EmpId = a.EmployeeId,
                                   LogedInUnserId = userId,
                                   ForwardToName = a.EmployeeId != 0 ? getEmployeeById(a.EmployeeId) : grievanceRepository.GetAllGrievanceCategory().Where(s => s.ID == a.DepartmentId).FirstOrDefault().Name + "(Dept)",
                                   ForwardByName = a.InsertedId != 0 ? getEmployeeById(a.InsertedId) : "",

                                   ForwardDate = a.InsertedDate,
                                   ForwardStatus = a.IsSubAssigned == false ? "FORWARDED" : "SUB-ASSIGNED",
                                   GrievanceStatus = commonMethods.GrievanceStatus(0, a.StatusId,0).StatusName,
                                   ComplitionName = commonMethods.GrievanceStatus(0, b.GrievanceStatusID,0).StatusName == "COMPLETED" ? getEmployeeById(b.CompletionId) : "NA",
                               }).ToList();

                    return res;
                }
            }
            catch (Exception e)
            {
                string msg = e.Message;
               
            }
            return null;
        }
        public bool IsGrievanceIsForward(long grivid)
        {
            var grivForward = _DbContext.GrievanceForwards.Where(a => a.Active == true && a.GrievanceId == grivid).ToList();
            if (grivForward.Count() > 0)
            {
                return true;
            }

            return false;
        }
        public bool IsGrievanceTimelineIsExtended(long grivid)
        {
            var grivExtended = _DbContext.GrievanceExtendedTimelines.Where(a => a.Active == true && a.GrievanceId == grivid).ToList();
            if (grivExtended.Count() > 0)
            {
                return true;
            }

            return false;
        }
        public List<GrievanceCommentModel> getgrievancecommentsclses(long id, long roleid)
        {
            try
            {
                //var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var comments = grievanceRepository.GetAllGrievanceComment().Where(a => a.Active == true).ToList();
                var commentattachments = grievanceRepository.GetAllGrievanceCommentAttachment().Where(a => a.Active == true).ToList();
                //var grievances = _DbContext.Grievances.Where(a => a.Active == true).ToList();
                var res = (from a in comments
                           where a.GrievanceID == id
                           select new GrievanceCommentModel
                           {
                               ID = a.ID,
                               InsertedId = a.InsertedId,
                               CommentBy = getRoleById(a.RoleId, a.InsertedId),
                               CommentedOn = a.InsertedDate,
                               Comment = a.Description,
                               Profile = getProfileById(a.RoleId, a.InsertedId),
                               EmpId = a.InsertedId,
                               CommentedAttachments = commentattachments.Where(m => m.GrievanceCommentID == a.ID).ToList()
                               //GrievanceStatusId = b.GrievanceStatusID
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<GrievanceCommentModel> getgrievancecommentscls(long id,long roleid, long userId, bool isapproveforstudentdetails)
        {
            try
            {
                //var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var comments = grievanceRepository.GetAllGrievanceComment().Where(a => a.Active == true).ToList();
                var commentattachments = grievanceRepository.GetAllGrievanceCommentAttachment().Where(a => a.Active == true).ToList();
                //var grievances = _DbContext.Grievances.Where(a => a.Active == true).ToList();
                var res = (from a in comments
                                   where a.GrievanceID == id
                                   select new GrievanceCommentModel
                                   {
                                       ID = a.ID,
                                       InsertedId = a.InsertedId,
                                       CommentBy = isapproveforstudentdetails == true ? getRoleById(a.RoleId, a.InsertedId) : 
                                       roleid == 5 ? getRoleById(a.RoleId, a.InsertedId) : roleid == 6 ? getRoleById(a.RoleId, a.InsertedId) : getRoleByIds(a.RoleId, a.InsertedId),
                                       CommentedOn = a.InsertedDate,
                                       Comment = a.Description,
                                       Profile = getProfileById(a.RoleId, a.InsertedId),
                                       EmpId = a.InsertedId,
                                       UserId = userId,
                                       CommentedAttachments = commentattachments.Where(m => m.GrievanceCommentID == a.ID).ToList()
                                       //GrievanceStatusId = b.GrievanceStatusID
                                   }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<GrievanceCommentModel> getgrievanceinhousecommentscls(long id,long userid)
        {
            try
            {
                //var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var comments = grievanceRepository.GrievanceInhouseComments(id).Where(a => a.Active == true).ToList();
                var commentattachments = grievanceRepository.GrievanceInhouseAttachment(id).Where(a => a.Active == true).ToList();
                //var grievances = _DbContext.Grievances.Where(a => a.Active == true).ToList();
                var res = (from a in comments
                           where a.GrievanceID == id
                           select new GrievanceCommentModel
                           {
                               ID = a.ID,
                               InsertedId = a.InsertedId,
                               CommentBy = getRoleById(a.RoleId, a.InsertedId),
                               CommentedOn = a.InsertedDate,
                               Comment = a.Description,
                               Profile = getProfileById(a.RoleId, a.InsertedId),
                               EmpId = a.InsertedId,
                               UserId = userid,
                               InhouseCommentedAttachments = commentattachments.Where(m => m.GrievanceInHouseCommentID == a.ID).ToList()
                               //GrievanceStatusId = b.GrievanceStatusID
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public string getEmployeeById(long id)
        {
            if (id == 1)
            {
                var a = employeeRepository.GetAllRequiredEmployee().Where(m => m.ID == id).FirstOrDefault();
                string empname = a.FirstName + " " + (a.MiddleName == null ? (a.LastName) : (a.MiddleName + " " + a.LastName));
                return empname;
            }
            else
            {
                var a = employeeRepository.GetAllEmployee().Where(m => m.ID == id).FirstOrDefault();
                string empname = a.FirstName + " " + (a.MiddleName == null ? (a.LastName) : (a.MiddleName + " " + a.LastName));
                return empname;
            }
        }

        //public string getStudentById(long id)
        //{
        //    var a = studentAggregateRepository.GetAllStudent().Where(m => m.ID == id).FirstOrDefault();
        //    string studname = a.FirstName + " " + a.MiddleName == null ? (a.LastName) : (a.MiddleName + " " + a.LastName);
        //    return studname;
        //}
        public string getRoleByIds(long roleid, long id)
        {
            if (roleid == 5)
            {
                var stud = studentAggregateRepository.GetAllStudent().Where(m => m.ID == id).FirstOrDefault();
                string studname = "XXXXXXXXX";
                return studname;

            }
            else if (roleid == 6)
            {
                var stud = studentAggregateRepository.GetAllParent().Where(m => m.ID == id).FirstOrDefault();
                string studname = "XXXXXXXXX";
                return studname;

            }
            else
            {
                var emp = employeeRepository.GetAllRequiredEmployee().ToList().Where(m => m.ID == id).FirstOrDefault();
                string empname = emp.FirstName + " " + (emp.MiddleName == null ? (emp.LastName) : (emp.MiddleName + " " + emp.LastName));
                return empname;
            }
            return "";
        }
        public string getRoleById(long roleid, long id)
        {
            if (roleid == 5)
            {
                var stud = studentAggregateRepository.GetAllStudent().Where(m => m.ID == id).FirstOrDefault();
                string studname = stud.FirstName + " " + (stud.MiddleName == null ? (stud.LastName) : (stud.MiddleName + " " + stud.LastName));
                return studname;

            }
            else if (roleid == 6)
            {
                var stud = studentAggregateRepository.GetAllParent().Where(m => m.ID == id).FirstOrDefault();
                string studname = stud.FirstName + " " + (stud.MiddleName == null ? (stud.LastName) : (stud.MiddleName + " " + stud.LastName));
                return studname;

            }
            else
            {
                var emp = employeeRepository.GetAllRequiredEmployee().ToList().Where(m => m.ID == id).FirstOrDefault();
                string empname = emp.FirstName + " " + (emp.MiddleName == null ? (emp.LastName) : (emp.MiddleName + " " + emp.LastName));
                return empname;
            }
            return "";
        }
        public string getProfileById(long roleid, long id)
        {
            if (roleid == 5)
            {
                var stud = studentAggregateRepository.GetAllStudent().Where(m => m.ID == id).FirstOrDefault();
                string studname = stud.Image != null ? "/ODMImages/StudentProfile/" + stud.Image : "/images/user_profile.png";
                return studname;

            }
            else if (roleid == 6)
            {
                var stud = studentAggregateRepository.GetAllParent().Where(m => m.ID == id).FirstOrDefault();
                string studname = stud.Image != null ? "/ODMImages/ParentProfile/" + stud.Image : "/images/user_profile.png";
                return studname;

            }
            else
            {
                var emp = employeeRepository.GetAllRequiredEmployee().ToList().Where(m => m.ID == id).FirstOrDefault();
                string empname = emp.Image != null ? "/ODMImages/EmployeeProfile/" + emp.Image : "/images/user_profile.png"; 
                return empname;
            }
            return "";
        }

        public string getCodeById(long roleid, long id)
        {
          
            if (roleid == 5) 
            {
                var stud = studentAggregateRepository.GetAllStudent().Where(m => m.ID == id).FirstOrDefault();
                string studcode = stud.StudentCode;
                return studcode;

            }
            else if (roleid == 6)
            {
                var part = studentAggregateRepository.GetAllParent().Where(m => m.ID == id).FirstOrDefault();
                var stud = studentAggregateRepository.GetAllStudent().Where(m => m.ID == part.StudentID).FirstOrDefault();
                string studcode = stud.StudentCode;
                return studcode;

            }
            else
            {
                var emp = employeeRepository.GetAllRequiredEmployee().Where(m => m.ID == id).FirstOrDefault();
                string empcode = emp.EmpCode;
                return empcode;
            }
            return "";

        }
        public string getMobileById(long roleid, long id)
        {
            var a = roleRepository.GetAllRole().Where(m => m.ID == roleid).FirstOrDefault();

            if (roleid == 6)
            {
                var par = studentAggregateRepository.GetAllParent().Where(m => m.ID == id).FirstOrDefault();
                string parno = par.PrimaryMobile;
                return parno;

            }
            else if (roleid == 5)
            {
                var par = studentAggregateRepository.GetAllStudent().Where(m => m.ID == id).FirstOrDefault();
                string parno = par.PrimaryMobile;
                return parno;

            }
            else
            {
                var emp = employeeRepository.GetAllEmployee().Where(m => m.ID == id).FirstOrDefault();
                string empno = emp.PrimaryMobile;
                return empno;
            }
            return "";

        }

    }
    public class grievancecommentscls
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public long GrievanceStatusID { get; set; }
        public GrievanceStatusModel GrievanceStatusName { get; set; }
        public long commentedBy { get; set; }
        public string commentedByName { get; set; }
        public string StatusName { get; set; }
        public DateTime insertedOn { get; set; }
        public List<GrievanceCommentAttachment> grievanceCommentAttachments { get; set; }

    }
    public class GrievanceForwardDetails
    {
        public long ID { get; set; }
        public string ForwardToName { get; set; }
        public string ForwardByName { get; set; }
        public Nullable<DateTime> ForwardDate { get; set; }
        public string ForwardStatus { get; set; }
        public string GrievanceStatus { get; set; }
        public string ComplitionName { get; set; }
        public long GrievanceId { get; set; }
        public long EmpId { get; set; }
        public long LogedInUnserId { get; set; }
    }
    public class FilterGrievanceClass
    {
        public List<MyGrievanceClass> myGrievanceClasses { get; set; }
        public long categoryId { get; set; }
        public long typeId { get; set; }
        public DateTime? F_Date { get; set; }
        public DateTime? L_Date { get; set; }
        public List<GrievanceType> grievanceTypes { get; set; }
        public List<grievancecategorycls> grievancecategories { get; set; }
    }
    public class grievancecategorycls
    {
        public long id { get; set; }
        public string name { get; set; }
    }
    public class MyGrievanceClass
    {
        public long ID { get; set; }
        [Display(Name = "Grievance ID")]
        public string Code { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please Enter Description")]
        public string Description { get; set; }
        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Select Type")]
        public long GrievanceTypeID { get; set; }
        public string GrievanceTypeName { get; set; }
        [Display(Name = "Sub Type")]
        public long GrievanceSubTypeID { get; set; }
        public long RoleId { get; set; }
        public string GrievanceSubTypeName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public IEnumerable<GrievanceType> types { get; set; }
        public string status { get; set; }
        public GrievanceStatusModel Grivstatus { get; set; }
        public Nullable<DateTime> ProgressDate { get; set; }
        public long GrievanceCategoryID { get; set; }
        public string GrievanceCategory { get; set; }
        public long GrievancePriorityID { get; set; }
        public string Priority { get; set; }
        public long GrievanceRelatedId { get; set; }
        public long GrievanceStatusID { get; set; }
        public List<GrievanceAttachment> grievanceAttachments { get; set; }
        public IEnumerable<GrievanceCategory> GrivType { get; set; }
        public IEnumerable<GrievanceType> GrivSubTypes { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Category { get; set; }
        public int UnreadCommentCount { get; set; }
        public bool IsRequestedForStudentDetails { get; set; }
        public bool IsRejected { get; set; }
    }
    public class grievanceattachments
    {
        public long id { get; set; }
        public string path { get; set; }
        public string Name { get; set; }
        public string file { get; set; }
        public long RoleId { get; set; }
    }

    
    public class GrievanceTypeClass
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Role")]
        [Required(ErrorMessage = "Please Select Role")]
        public long RoleID { get; set; }
        public string RoleName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public IEnumerable<Role> roles { get; set; }
        [Required(ErrorMessage = "Please Select Grievance Type")]
        public long CategoryId { get; set; }
        public string GrievanceName { get; set; }
        public IEnumerable<GrievanceCategory> Types { get; set; }
    }

    public class GrievanceViewClass
    {
        public long RoleId { get; set; }
        public long ID { get; set; }
        public long GrievanceCategoryID { get; set; }
        public string VerifyRemark { get; set; }
        public string GrievanceCategoryName { get; set; }
        public long GrievanceTypeID { get; set; }

        public string GrievanceTypeName { get; set; }

        public string Code { get; set; }
        public string AuthorizedPerson { get; set; }
        public string Description { get; set; }
        public long GrievanceStatusID { get; set; }

        public string GrievanceStatusName { get; set; }
        public DateTime? CompletionDate { get; set; }
        public long CompletionId { get; set; }
        public string CompletionName { get; set; }

        public DateTime? VerificationDate { get; set; }

        public long VerifiedId { get; set; }

        public string VerifiedName { get; set; }
        public string RequestType { get; set; }
        public long StatusId { get; set; }
        public string StatusName { get; set; }
        public long GrievancePriorityID { get; set; }
        public string GrievancePriorityName { get; set; }

        public long GrievanceRelatedId { get; set; }
        public string GrievanceRelatedName { get; set; }
        public string RelatedCode { get; set; }
        public string RelatedMobile { get; set; }

        public DateTime? CreatedDate { get; set; }

        public GrievanceEmployeeClass grievanceempcls { get; set; }

        public List<GrievanceAttachment> attachments { get; set; }
        public GrievanceAttachment Grivattachments { get; set; }
        public List<GrievanceAttachment> employeeattachments { get; set; }
        public IEnumerable<GrievanceTimeLineModel> grievanceTimelines { get; set; }
        public List<GrievanceCommentModel> grievancecommentscls { get; set; }
        public List<GrievanceCommentModel> grievanceinhousecommentscls { get; set; }

        public GrievanceCommentModel grievancecomments { get; set; }
        public bool IsGrievanceForward { get; set; }
        public bool IsTimelineExtended { get; set; }
        public List<GrievanceForwardDetails> ForwardDetail { get; set; }
        public List<GrievanceExtendedTimelineModal> ExtendedTimelineDetail { get; set; }

        public GrievanceViewClass GrievaneSolvedDetails { get; set; }
        public Nullable<TimeSpan> EstimateDate { get; set; }
        public decimal Score { get; set; }
        public bool isCommentUpdate { get; set; }
        public bool IsRequestedForStudentDetails { get; set; }
        public string RequestedName { get; set; }
        public bool IsApprovedForStudentDetails { get; set; }
        public bool IsRejectedForStudentDetails { get; set; }
        public string ApprovedName { get; set; }
        public string LoggedInName { get; set; }
        public bool IsRejected { get; set; }
        public string Reason { get; set; }
    }

    public class GrievanceEmployeeClass
    {
        public long EmployeeID { get; set; }

        public long GrievanceID { get; set; }

        public long AssignedByID { get; set; }
        public string AssignedByName { get; set; }

        public string AssignedToName { get; set; }

        public DateTime? AssignedOn { get; set; }


    }

    public class GrievanceTimeLineModel
    {
        public string PostedBy { get; set; }
        public Nullable<DateTime> PostedDateTime { get; set; }
        public GrievanceStatusModel Status { get; set; }

    }

    public class GrievanceCommentModel
    {
        public long ID { get; set; }
        public string Comment { get; set; }
        public long InsertedId { get; set; }
        public string CommentBy { get; set; }
        public string Profile { get; set; }
        public long EmpId { get; set; }
        public long UserId { get; set; }
        public long GrievanceStatusId { get; set; }
        public Nullable<DateTime> CommentedOn { get; set; }
        public List<GrievanceCommentAttachment> CommentedAttachments { get; set; }
        public List<GrievanceInhouseAttachment> InhouseCommentedAttachments { get; set; }
    }
    public class GrievanceAttachmentModel
    {
        public long ID { get; set; }
        public string AttachmentName { get; set; }
        public string AttachmentPath { get; set; }
        public long GrievanceId { get; set; }
        public long InsertedId { get; set; }
        public string PostedBy { get; set; }
        public long GrievanceStatusId { get; set; }
        public Nullable<DateTime> InsertedDate { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
    }

    public class GrievanceAttachmentNewModel
    {
        public List<GrievanceAttachmentModel> Attachments { get; set; }
        public long GrievanceId { get; set; }
    }
    public class GrievanceCommentPostModal
    {
        public long ID { get; set; }
        public string comment { get; set; }
        public long grievanceid { get; set; }
        public long grievancestatusid { get; set; }
    }
    public class GrievancePublicFormCls
    {
        public long ID { get; set; }

        public long EmployeeID { get; set; }

        public long GrievanceID { get; set; }

        public long StudentID { get; set; }

        public string EmpCode { get; set; }

        public string StudentCode { get; set; }
        public string EmployeeName { get; set; }

        public string StudentName { get; set; }

        public string ParentName { get; set; }
        public string Organisation { get; set; }

        public string Department { get; set; }
        public string Description { get; set; }

        public List<GrievanceAttachment> attachments { get; set; }
    }
    public class publicgrievancecls
    {
        [Display(Name = "Staff Name")]
        [Required(ErrorMessage = "Enter Staff Name")]
        public string staffname { get; set; }
        [Display(Name = "Staff Code")]
        [Required(ErrorMessage = "Enter Staff Code")]
        public string staffcode { get; set; }
        [Display(Name = "Student Name")]
        [Required(ErrorMessage = "Enter Student Name")]
        public string studentname { get; set; }
        [Display(Name = "Student Id")]
        [Required(ErrorMessage = "Enter Student Id")]
        public string studentcode { get; set; }
        [Display(Name = "Grievance ID")]
        public string Code { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please Enter Description")]
        public string Description { get; set; }
        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Select Type")]
        public long GrievanceTypeID { get; set; }
        public string GrievanceTypeName { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
        public IEnumerable<GrievanceType> types { get; set; }
        public string status { get; set; }
        public long GrievanceCategoryID { get; set; }
        public string GrievanceCategory { get; set; }
        public long GrievancePriorityID { get; set; }
        public string Priority { get; set; }
        public long GrievanceRelatedId { get; set; }
        public long GrievanceStatusID { get; set; }
    }
    public class staffGrievanceDetails
    {
        public long EmployeeId { get; set; }
        public string Empcode { get; set; }
        public string EmployeeName { get; set; }
        public IEnumerable<EmployeeDesignationCls> employeeDesignations { get; set; }
        [Display(Name = "Grievance and Solution Required")]
        [Required(ErrorMessage = "Please Enter Grievance and Solution Required")]
        public string Description { get; set; }
        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Select Type")]
        public long GrievanceTypeID { get; set; }
        public string GrievanceTypeName { get; set; }
        public IEnumerable<GrievanceType> types { get; set; }
    }
    public class studentGrievanceDetails
    {
        public long StudentId { get; set; }
        public string Studentcode { get; set; }
        public string StudentName { get; set; }
        public string school { get; set; }
        public string wing { get; set; }
        public string standard { get; set; }
        public string section { get; set; }
        [Display(Name = "Grievance and Solution Required")]
        [Required(ErrorMessage = "Please Enter Grievance and Solution Required")]
        public string Description { get; set; }
        [Display(Name = "Type")]
        [Required(ErrorMessage = "Please Select Type")]
        public long GrievanceTypeID { get; set; }
        public string GrievanceTypeName { get; set; }
        public IEnumerable<GrievanceType> types { get; set; }
    }

    public class GrievanceAccessModelClass
    {
        public long ID { get; set; }
        public Nullable<DateTime> InsertedDate { get; set; }
        [Required(ErrorMessage = "Please Select Type")]
        public long GrievanceCategoryId { get; set; }
        public string GrievanceCategoryName { get; set; }
        public long GrievanceTypeId { get; set; }
        public string GrievanceTypeName { get; set; }
        [Required(ErrorMessage = "Please Select Employee")]
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public bool Active { get; set; }
        public string Status { get; set; }
        public Nullable<DateTime> StartDate { get; set; }
        public Nullable<DateTime> EndDate { get; set; }
        public IEnumerable<GrievanceCategory> GrievType { get; set; }
        public IEnumerable<GrievanceType> GrivSubType { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
    }

    public class GrievanceExtendedTimelineModal
    {
        public long ID { get; set; }
        public Nullable<DateTime> ExtendedDate { get; set; }
        public string ExtendedTime { get; set; }
        public string ExtendedReason { get; set; }
        public string ExtendedBy { get; set; }
    }
}
