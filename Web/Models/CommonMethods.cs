﻿using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Drawing.ChartDrawing;
using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.DTO;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Entities.TodoAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models
{
    public class CommonMethods
    {
        private IEmployeeRepository employeeRepository;
        private IDepartmentRepository departmentRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private ICityRepository cityRepository;
        private IStateRepository stateRepository;
        private ICountryRepository countryRepository;
        private IDesignationLevelRepository designationLevelRepository;
        private IDesignationRepository designationRepository;
        private IGroupRepository groupRepository;
        private IOrganizationRepository organizationRepository;
        private IReligionTypeRepository religionTypeRepository;

        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IAddressRepository addressRepository;
        private IEmployeeAddressRepository employeeAddressRepository;
        private IEmployeeBankAccountRepository bankAccountRepository;
        private IBankRepository bankRepository;
        private IBankBranchRepository bankBranchRepository;
        private IEmployeeEducationRepository employeeEducationRepository;
        private IEducationQualificationRepository educationQualificationRepository;
        private IEducationQualificationTypeRepository educationQualificationTypeRepository;
        private IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IEmployeeExperienceRepository employeeExperienceRepository;
        private IAccessRepository accessRepository;
        private IAccessTypeRepository accessTypeRepository;
        private IRoleRepository roleRepository;
        private IModuleRepository moduleRepository;
        private ISubModuleRepository subModuleRepository;
        private IActionAccessRepository actionAccessRepository;
        private IActionRepository actionRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IGrievanceRepository grievanceRepository;
        private IStandardRepository standardRepository;
        private ISectionRepository sectionRepository;
        private IBoardRepository boardRepository;
        private IToDoRepository todoRepository;
        private IEmployeeDocumentAttachmentRepository employeeDocumentAttachmentRepository;
        private ISupportRepository supportRepository;
        private IErpBugRepository erpBugRepository;
        private ILogingRepository logingRepository;
        private IParentRepository parentRepository;
        private IOrganizationAcademicRepository organizationAcademicRepository;
        private IGrievanceForwardRepository grievanceForwardRepository;
        private OdmErp.Web.Areas.Student.Models.StudentSqlQuery sqlQuery;
        public commonsqlquery commonsql;
        /*  Session  */
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;
        private readonly ApplicationDbContext _dbContext;

        private IBoardStandardRepository boardStandardRepository;

        /*  Session  */
        public CommonMethods(IGrievanceForwardRepository grievanceForwardRepo,OdmErp.Web.Areas.Student.Models.StudentSqlQuery sql,IEmployeeDocumentAttachmentRepository employeeDocumentAttachmentRepo, IEmployeeRepository employeeRepo, IDepartmentRepository departmentRepo, IBloodGroupRepository bloodGroupRepo,
            INationalityTypeRepository nationalityTypeRepo, IAddressTypeRepository addressTypeRepo, ICityRepository cityRepo, ApplicationDbContext _dbContext,
            IStateRepository stateRepo, ICountryRepository countryRepo, IDesignationLevelRepository designationLevelRepo, IDesignationRepository designationRepo,
            IGroupRepository groupRepo, IOrganizationRepository organizationRepo, IReligionTypeRepository religionTypeRepo, IEmployeeDesignationRepository employeeDesignationRepo,
            IAddressRepository addressRepo, IEmployeeAddressRepository employeeAddressRepo, IEmployeeBankAccountRepository bankAccountRepo, IBankRepository bankRepo,
            IBankBranchRepository bankBranchRepo, IEmployeeEducationRepository employeeEducationRepo, IEducationQualificationRepository educationQualificationRepo,
            IEducationQualificationTypeRepository educationQualificationTypeRepo, IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepo,
            IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo, IEmployeeExperienceRepository employeeExperienceRepo,
            IAccessRepository accessRepo, IAccessTypeRepository accessTypeRepo, IRoleRepository roleRepo, IModuleRepository moduleRepo, ISubModuleRepository subModuleRepo,
            IActionAccessRepository actionAccessRepo, IActionRepository actionRepo, IStudentAggregateRepository studentAggregateRepo, IGrievanceRepository grievanceRepo,

            IStandardRepository standardRepo, ISectionRepository sectionRepo, IBoardRepository boardRepo, IToDoRepository todoRepo, ISupportRepository suppoRepository,
            IErpBugRepository erpBugRepo, ILogingRepository logingRepo, IOrganizationAcademicRepository organizationAcademicRepository,
            IHttpContextAccessor httpContextAccessor, commonsqlquery commonsqlquery, IBoardStandardRepository boardStandardRepo, IParentRepository parentRepo
           )

        {
            grievanceForwardRepository = grievanceForwardRepo;
            sqlQuery = sql;
            parentRepository = parentRepo;
            boardStandardRepository = boardStandardRepo;
            employeeDocumentAttachmentRepository = employeeDocumentAttachmentRepo;
            todoRepository = todoRepo;
            employeeRepository = employeeRepo;
            departmentRepository = departmentRepo;
            bloodGroupRepository = bloodGroupRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            addressTypeRepository = addressTypeRepo;
            cityRepository = cityRepo;
            stateRepository = stateRepo;
            countryRepository = countryRepo;
            designationLevelRepository = designationLevelRepo;
            designationRepository = designationRepo;
            groupRepository = groupRepo;
            organizationRepository = organizationRepo;
            religionTypeRepository = religionTypeRepo;
            employeeDesignationRepository = employeeDesignationRepo;
            addressRepository = addressRepo;
            employeeAddressRepository = employeeAddressRepo;
            bankAccountRepository = bankAccountRepo;
            bankRepository = bankRepo;
            bankBranchRepository = bankBranchRepo;
            employeeEducationRepository = employeeEducationRepo;
            educationQualificationRepository = educationQualificationRepo;
            educationQualificationTypeRepository = educationQualificationTypeRepo;
            employeeRequiredDocumentRepository = employeeRequiredDocumentRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            employeeExperienceRepository = employeeExperienceRepo;
            accessRepository = accessRepo;
            accessTypeRepository = accessTypeRepo;
            roleRepository = roleRepo;
            moduleRepository = moduleRepo;
            subModuleRepository = subModuleRepo;
            actionAccessRepository = actionAccessRepo;
            actionRepository = actionRepo;
            studentAggregateRepository = studentAggregateRepo;
            grievanceRepository = grievanceRepo;
            standardRepository = standardRepo;
            sectionRepository = sectionRepo;
            boardRepository = boardRepo;
            supportRepository = suppoRepository;
            erpBugRepository = erpBugRepo;
            logingRepository = logingRepo;
            this.organizationAcademicRepository = organizationAcademicRepository;
            _httpContextAccessor = httpContextAccessor;
           // _session = _httpContextAccessor.HttpContext.Session;
            commonsql = commonsqlquery;
            this._dbContext = _dbContext;
        }

       



        public string GetOrgGroupName(long employeeId)
        {
            return (from a in employeeDesignationRepository.GetAllEmployeeDesignationsByEmployeeId(employeeId).ToList()
                    join b in designationRepository.GetAllDesignation().ToList()
                       on a.DesignationID equals b.ID
                    join c in departmentRepository.GetAllDepartment().ToList()
                   on b.DepartmentID equals c.ID
                    select new
                    {
                        Name = c.GroupID == 0 ? organizationRepository.GetOrganizationById(c.OrganizationID).Name :
                        groupRepository.GetGroupById(c.GroupID).Name
                    }
                   ).Select(x => x.Name).FirstOrDefault();

        }
       

        public string GetEmployeeNameByEmployeeId(long employeeId, long loginEmployeeId)
        {

            return employeeId == loginEmployeeId ? "Me" : employeeRepository.GetEmployeeFullNameById(employeeId);

        }
     
        public List<CommonMasterModel> GetallchilddepartmentbasedonParent(long deptID)
        {
            var dept = departmentRepository.GetAllDepartment();
            List<CommonMasterModel> comm = new List<CommonMasterModel>();
            Department comdept = dept.Where(a => a.ID == deptID).FirstOrDefault();
            List<Department> childs = new List<Department>();
            comm.Add(new CommonMasterModel { ID = comdept.ID, Name = comdept.Name });
            for (int i = 0; i < comm.Count(); i++)
            {
                childs = dept.Where(a => a.ParentID == comm.ToList()[i].ID).ToList();
                if (childs.Count > 0)
                {
                    foreach (var df in childs)
                    {
                        comm.Add(new CommonMasterModel { ID = df.ID, Name = df.Name });
                    }
                }
            }
            return comm;
        }
      
        public IEnumerable<EmployeeAdressCls> GetAllAddressByUsingEmployeeId(long id)
        {
            try
            {

                var employeeAddresses = employeeAddressRepository.GetEmployeeAddressByEmployeeID(id);
                var address = addressRepository.GetAllAddress();
                var addressType = addressTypeRepository.GetAllAddressType();
                var city = cityRepository.GetAllCity();
                var state = stateRepository.GetAllState();
                var country = countryRepository.GetAllCountries();

                IEnumerable<EmployeeAdressCls> res = (from a in employeeAddresses
                                                      join b in address on a.AddressID equals b.ID
                                                      join c in addressType on b.AddressTypeID equals c.ID
                                                      join d in city on b.CityID equals d.ID
                                                      join e in state on b.StateID equals e.ID
                                                      join f in country on b.CountryID equals f.ID
                                                      select new EmployeeAdressCls
                                                      {
                                                          AddressLine1 = b.AddressLine1,
                                                          AddressLine2 = b.AddressLine2,
                                                          AddressTypeID = b.AddressTypeID,
                                                          AddressTypeName = c.Name,
                                                          CityID = b.CityID,
                                                          CityName = d.Name,
                                                          CountryID = b.CountryID,
                                                          CountryName = f.Name,
                                                          ID = a.ID,
                                                          PostalCode = b.PostalCode,
                                                          StateID = b.StateID,
                                                          StateName = e.Name,
                                                          ModifiedDate = a.ModifiedDate,

                                                          InsertedDate = a.InsertedDate

                                                      }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeDesignationCls> GetAllDesignationByUsingEmployeeId(long id)
        {
            try
            {
                var employeeDesignation = employeeDesignationRepository.GetAllEmployeeDesignationsByEmployeeId(id);
                var designation = designationRepository.GetAllDesignation();
                // var designationLevel = designationLevelRepository.GetAllDesignationLevel();
                var department = departmentRepository.GetAllDepartment();
                var groups = groupRepository.GetAllGroup();
                var organization = organizationRepository.GetAllOrganization();

                IEnumerable<EmployeeDesignationCls> res = (from a in employeeDesignation
                                                           join b in designation on a.DesignationID equals b.ID
                                                           //join c in designationLevel on b.DesignationLevelID equals c.ID
                                                           join d in department on b.DepartmentID equals d.ID
                                                           //  join e in organization on d.OrganizationID equals e.ID
                                                           // join f in groups on e.GroupID equals f.ID
                                                           select new EmployeeDesignationCls
                                                           {
                                                               DepartmentID = b.DepartmentID,
                                                               DepartmentName = d.Name,
                                                               DesignationId = a.DesignationID,
                                                               //  DesignationLevelID = b.DesignationLevelID,
                                                               //  DesignationLevelName = c.Name,
                                                               DesignationName = b.Name,
                                                               EmployeeID = id,
                                                               GroupID = d.GroupID,
                                                               GroupName = d.GroupID > 0 ?
                                                               groups.Where(x => x.ID == d.GroupID).FirstOrDefault().Name : d.OrganizationID > 0 ?
                                                               groups.Where(x => x.ID == organization.Where(y => y.ID == d.OrganizationID).
                                                               FirstOrDefault().GroupID).FirstOrDefault().Name : "",
                                                               ID = a.ID,
                                                               ModifiedDate = a.ModifiedDate,
                                                               OrganizationID = d.OrganizationID,
                                                               OrganizationName = d.OrganizationID > 0 ?
                                                               organization.Where(x => x.ID == d.OrganizationID).FirstOrDefault().Name : ""
                                                           }).ToList();
                return res;
            }
            catch(Exception e)
            {
                return null;
            }
        }
        public IEnumerable<EmployeeDesignationCls> GetAllDeptwithEmployee()
        {
            try
            {
                var employeeDesignation = employeeDesignationRepository.GetAllEmployeeDesignations();
                var designation = designationRepository.GetAllDesignation();
                // var designationLevel = designationLevelRepository.GetAllDesignationLevel();
                var department = departmentRepository.GetAllDepartment();
                var groups = groupRepository.GetAllGroup();
                var organization = organizationRepository.GetAllOrganization();

                IEnumerable<EmployeeDesignationCls> res = (from a in employeeDesignation
                                                           join b in designation on a.DesignationID equals b.ID
                                                           //join c in designationLevel on b.DesignationLevelID equals c.ID
                                                           join d in department on b.DepartmentID equals d.ID
                                                           //  join e in organization on d.OrganizationID equals e.ID
                                                           // join f in groups on e.GroupID equals f.ID
                                                           select new EmployeeDesignationCls
                                                           {
                                                               DepartmentID = b.DepartmentID,
                                                               DepartmentName = d.Name,
                                                               DesignationId = a.DesignationID,
                                                               DesignationName = b.Name,
                                                               EmployeeID = a.EmployeeID,
                                                               GroupID = d.GroupID,
                                                               ID = a.ID,
                                                               ModifiedDate = a.ModifiedDate,
                                                               OrganizationID = d.OrganizationID
                                                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeBankCls> GetAllBankByUsingEmployeeId(long id)
        {
            try
            {
                var employeeBank = bankAccountRepository.GetEmployeeBankAccountByEmployeeID(id);
                var bank = bankRepository.GetAllBank();
                var bankbranch = bankBranchRepository.GetAllBankBranch();

                IEnumerable<EmployeeBankCls> res = (from a in employeeBank
                                                    join b in bank on a.BankNameID equals b.ID
                                                    select new EmployeeBankCls
                                                    {
                                                        AccountHolderName = a.AccountHolderName,
                                                        AccountNumber = a.AccountNumber,
                                                        BankNameID = a.BankNameID,
                                                        BankBranchName = a.BankBranchName,
                                                        IfscCode = a.IfscCode,
                                                        BankName = b.Name,
                                                        EmployeeID = a.EmployeeID,
                                                        BankTypeID = a.BankTypeID,
                                                        ID = a.ID,
                                                        ModifiedDate = a.ModifiedDate,
                                                        InsertedDate = a.InsertedDate
                                                    }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeEducationCls> GetAllEducationByUsingEmployeeId(long id)
        {
            try
            {
                var employeeEducation = employeeEducationRepository.GetAllEmployeeEducationsByEmployeeId(id);
                var qualifications = educationQualificationRepository.GetAllEducationQualification();
                var qualificationTypes = educationQualificationTypeRepository.GetAllEducationQualificationType();

                IEnumerable<EmployeeEducationCls> res = (from a in employeeEducation
                                                         join b in qualifications on a.EducationQualificationID equals b.ID
                                                         join c in qualificationTypes on b.EducationQualificationTypeID equals c.ID
                                                         select new EmployeeEducationCls
                                                         {
                                                             EducationQualificationID = a.EducationQualificationID,
                                                             EducationQualificationName = b.Name,
                                                             EducationQualificationTypeID = b.EducationQualificationTypeID,
                                                             EducationQualificationTypeName = c.Name,
                                                             EmployeeID = a.EmployeeID,
                                                             ID = a.ID,
                                                             ModifiedDate = a.ModifiedDate,
                                                             InsertedDate = a.InsertedDate,
                                                             College = a.College,
                                                             University = a.University,
                                                             FromYear = a.FromYear,
                                                             ToYear = a.ToYear
                                                         }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public bool employeeRequiredDocument(long empId, long typeId, long subtypeId)
        {
            EmployeeRequiredDocument employeeRequired = employeeRequiredDocumentRepository.GetAllEmployeeRequiredDocuments().Where(a => a.DocumentTypeID == typeId && a.DocumentSubTypeID == subtypeId && a.EmployeeID == empId).FirstOrDefault();
            if (employeeRequired == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public List<EmployeeDocumentDetailsCls> GetDocumentDetails(long id)
        {
            try
            {
                List<EmployeeDocumentDetailsCls> documentDetailsCls = (from a in employeeRequiredDocumentRepository.GetAllEmployeeRequiredDocuments().Where(a => a.EmployeeID == id).Select(a => a.DocumentTypeID).Distinct().ToList()
                                                                       select new EmployeeDocumentDetailsCls
                                                                       {
                                                                           ID = a,
                                                                           DocumentTypeName = documentTypeRepository.GetDocumentTypeById(a).Name,
                                                                           // requiredDocumentCls = GetAllDocumentByUsingEmployeeId(id, a)
                                                                       }).ToList();
                return documentDetailsCls;
            }
            catch
            {
                return null;
            }
        }
        public List<EmployeeRequiredDocumentCls> GetAllDocumentByUsingEmployeeId(long id)
        {
            try
            {
                var employeeEducation = employeeRequiredDocumentRepository.GetAllEmployeeRequiredDocuments().Where(a => a.EmployeeID == id).ToList();
                var documentType = documentTypeRepository.GetAllDocumentTypes();
                var documentSubType = documentSubTypeRepository.GetAllDocumentSubTypes();
                var documents = employeeDocumentAttachmentRepository.GetAllEmployeeDocumentAttachments();

                List<EmployeeRequiredDocumentCls> res = (from a in employeeEducation
                                                         join b in documentType on a.DocumentTypeID equals b.ID
                                                         join c in documentSubType on a.DocumentSubTypeID equals c.ID
                                                         select new EmployeeRequiredDocumentCls
                                                         {
                                                             ID = a.ID,
                                                             totaldocument = documents.Where(m => m.EmployeeRequiredDocumentID == a.ID).ToList().Count,

                                                             DocumentSubTypeID = a.DocumentSubTypeID,
                                                             DocumentSubTypeName = c.Name,
                                                             DocumentTypeID = a.DocumentTypeID,
                                                             DocumentTypeName = b.Name,
                                                             EmployeeID = a.EmployeeID,
                                                             IsApproved = a.IsApproved,
                                                             IsMandatory = a.IsMandatory,
                                                             IsReject = a.IsReject,
                                                             IsVerified = a.IsVerified,
                                                             RelatedID = a.RelatedID,
                                                             VerifiedAccessID = a.VerifiedAccessID,
                                                             VerifiedDate = a.VerifiedDate,
                                                             relatedDetails = GetAllRelatedData(a.EmployeeID, b.Name, a.RelatedID)
                                                         }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public string GetAllRelatedData(long eid, string typeName, long RelatedId)
        {

            if (typeName == "EXPERIENCE")
            {
                EmployeeExperience employeeExperience = employeeExperienceRepository.GetExperienceById(RelatedId);
                return employeeExperience.OrganizationName;
            }
            else if (typeName == "BANK")
            {
                EmployeeBankAccount employeeBankAccount = bankAccountRepository.GetEmployeeBankAccountById(RelatedId);
                return bankRepository.GetBankById(employeeBankAccount.BankNameID).Name;
            }
            else if (typeName == "EDUCATIONAL")
            {
                EmployeeEducation employeeEducation = employeeEducationRepository.GetEmployeeEducationById(RelatedId);
                return educationQualificationRepository.GetEducationQualificationById(employeeEducation.EducationQualificationID).Name;
            }
            else
            {
                return "";
            }
        }

        public IEnumerable<Employeecls> GetAllEmployees()
        {
            try
            {
                var employeedetails = employeeRepository.GetAllEmployeewithleft();
                var nationalityTypes = nationalityTypeRepository.GetAllNationalities();
                var bloodgroup = bloodGroupRepository.GetAllBloodGroup();
                var religionType = religionTypeRepository.GetAllReligionTypes();
                var res = (from a in employeedetails
                               //join b in nationalityTypes on a.NatiobalityID equals b.ID
                               //join c in bloodgroup on a.BloodGroupID equals c.ID
                               //join d in religionType on a.ReligionID equals d.ID
                           select new Employeecls
                           {
                               AlternativeMobile = a.AlternativeMobile,
                               BloodGroupID = a.BloodGroupID,
                               BloodGroupName = (a.BloodGroupID != null && a.BloodGroupID != 0) ?
                                GetBloodGroupName(a.BloodGroupID.Value) : "",
                               DateOfBirth = a.DateOfBirth != null ? a.DateOfBirth : null,
                               EmailId = a.EmailId,
                               EmpCode = a.EmpCode,
                               FirstName = a.FirstName,
                               Gender = (a.Gender != null && a.Gender != "") ? a.Gender : "",
                               ID = a.ID,
                               LandlineNumber = a.LandlineNumber,
                               LastName = a.LastName,
                               profile = a.Image,
                               JoinDate = GetEmployeeActiveTimeLine(a.ID),
                               MiddleName = a.MiddleName,
                               NatiobalityID = a.NatiobalityID,
                               NatiobalityName = (a.NatiobalityID != null && a.NatiobalityID != 0) ? GetNationalityName(a.NatiobalityID.Value) : "",
                               PrimaryMobile = a.PrimaryMobile,
                               ReligionID = a.ReligionID,
                               ReligionName = (a.ReligionID != null && a.ReligionID != 0) ? GetReligionName(a.ReligionID.Value) : "",
                               ModifiedDate = a.ModifiedDate,
                               IsLeft = a.Isleft != null ? a.Isleft.Value : false,
                               LeftDate = a.LeftDate
                           }).ToList();
                return res;
            }
            catch (Exception ex)
            {
                return new List<Employeecls>();
            }

        }

        public string GetBloodGroupName(long id)
        {
            BloodGroup bg = bloodGroupRepository.GetBloodGroupById(id);
            return bg != null ? bg.Name : "";

        }

        public string GetNationalityName(long id)
        {
            NationalityType bg = nationalityTypeRepository.GetNationalityById(id);
            return bg != null ? bg.Name : "";

        }

        public string GetReligionName(long id)
        {
            ReligionType bg = religionTypeRepository.GetReligionTypeById(id);
            return bg != null ? bg.Name : "";

        }

        private DateTime? GetEmployeeActiveTimeLine(long employeeId)
        {
            EmployeeTimeline et = employeeRepository.GetActiveEmployeeTimelineByEmployeeId(employeeId);

            DateTime? jd = et != null ? et.JoiningOn : null;


            return jd;
        }
        public EmployeeAccesscls GetAccessTypeData(long id)
        {
            var accesstype = accessRepository.GetAllAccess().Where(a => a.EmployeeID == id).FirstOrDefault();
            var employee = employeeRepository.GetEmployeeById(id);
            EmployeeAccesscls employeeAccesscls = new EmployeeAccesscls();
            employeeAccesscls.EmployeeID = id;
            employeeAccesscls.roles = roleRepository.GetAllRole();
            employeeAccesscls.AccessID = 0;
            employeeAccesscls.RoleID = 0;
            if (accesstype != null)
            {
                employeeAccesscls.AccessID = accesstype.ID;
                employeeAccesscls.RoleID = accesstype.RoleID;
                employeeAccesscls.Password = accesstype.Password;
                employeeAccesscls.Username = accesstype.Username;

            }

            return employeeAccesscls;
        }

        public EmployeeModuleSubModuleAccess GetModuleSubModuleAccess(long id)
        {
            try
            {
                EmployeeModuleSubModuleAccess od = new EmployeeModuleSubModuleAccess();
                od.ID = id;
                od.modules = (from a in moduleRepository.GetAllModule()
                              select new ModulesCls
                              {
                                  ID = a.ID,
                                  ModuleName = a.Name,
                                  subModules = (from b in subModuleRepository.GetAllSubModule()
                                                where b.ModuleID == a.ID
                                                select new SubModulesDtCls
                                                {
                                                    ID = b.ID,
                                                    SubModuleName = b.Name,
                                                    actionSubModules = (from c in actionRepository.GetAllAction()
                                                                        where c.SubModuleID == b.ID
                                                                        select new ActionSubModuleClass
                                                                        {
                                                                            ID = c.ID,
                                                                            ActionName = c.Name,
                                                                            ActionAccessId = actionAccessRepository.GetAllActionDepartment().Where(d => d.DepartmentID == id && d.ActionID == c.ID).FirstOrDefault() == null ? 0 : actionAccessRepository.GetAllActionDepartment().Where(d => d.DepartmentID == id && d.ActionID == c.ID).FirstOrDefault().ID,
                                                                            IsRequired = actionAccessRepository.GetAllActionDepartment().Where(d => d.DepartmentID == id && d.ActionID == c.ID).FirstOrDefault() == null ? false : true

                                                                        }).ToList()
                                                }).ToList()
                              }).ToList();
                return od;
            }
            catch
            {
                return null;
            }
        }

        public EmployeeModuleSubModuleAccess GetModuleSubModuleActionAcademicAccess(long standardid, long standardstreamid)
        {
            try
            {
                EmployeeModuleSubModuleAccess od = new EmployeeModuleSubModuleAccess();
                od.standardid = standardid;
                od.standardstreamid = standardstreamid;
                od.modules = (from a in moduleRepository.GetAllModule()
                              select new ModulesCls
                              {
                                  ID = a.ID,
                                  ModuleName = a.Name,
                                  subModules = (from b in subModuleRepository.GetAllSubModule()
                                                where b.ModuleID == a.ID
                                                select new SubModulesDtCls
                                                {
                                                    ID = b.ID,
                                                    SubModuleName = b.Name,
                                                    actionSubModules = (from c in actionRepository.GetAllAction()
                                                                        where c.SubModuleID == b.ID
                                                                        select new ActionSubModuleClass
                                                                        {
                                                                            ID = c.ID,
                                                                            ActionName = c.Name,
                                                                            ActionAccessId = actionAccessRepository.GetAllActionAcademic().Where(d => d.AcademicStandardID == standardid && d.AcademicStandardStreamID == standardstreamid && d.ActionID == c.ID).FirstOrDefault() == null ? 0 : actionAccessRepository.GetAllActionAcademic().Where(d => d.AcademicStandardID == standardid && d.AcademicStandardStreamID == standardstreamid && d.ActionID == c.ID).FirstOrDefault().ID,
                                                                            IsRequired = actionAccessRepository.GetAllActionAcademic().Where(d => d.AcademicStandardID == standardid && d.AcademicStandardStreamID == standardstreamid && d.ActionID == c.ID).FirstOrDefault() == null ? false : true

                                                                        }).ToList()
                                                }).ToList()
                              }).ToList();
                return od;
            }
            catch
            {
                return null;
            }
        }


        public EmployeeModuleSubModuleAccess GetStudentModuleAccess(long roleid, long classid, long studentID)
        {
            try
            {
                var studentclass = commonsql.Get_view_All_Academic_Students().Where(a => a.StudentId == studentID && a.IsAvailable == true).FirstOrDefault();
                var actionrole = actionAccessRepository.GetAllActionAcademic().Where(a => a.AcademicStandardID == studentclass.AcademicStandardId && a.AcademicStandardStreamID == studentclass.AcademicStandardStreamId).ToList();
                var modules = moduleRepository.GetAllModule();
                var submodules = subModuleRepository.GetAllSubModule();

                EmployeeModuleSubModuleAccess od = new EmployeeModuleSubModuleAccess();
                od.modules = (from m in actionrole.Select(s => s.ModuleID).ToList().Distinct()
                              join a in modules on m equals a.ID
                              select new ModulesCls
                              {
                                  ID = a.ID,
                                  ModuleName = a.Name,
                                  subModules = (from n in actionrole.Select(s => s.SubModuleID).ToList().Distinct()
                                                join b in subModuleRepository.GetAllSubModule() on n equals b.ID
                                                where b.ModuleID == a.ID
                                                select new SubModulesDtCls
                                                {
                                                    ID = b.ID,
                                                    SubModuleName = b.Name,
                                                    actionSubModules = (from o in actionrole.Select(s => s.ActionID).ToList().Distinct()
                                                                        join c in actionRepository.GetAllAction() on o equals c.ID
                                                                        where c.SubModuleID == b.ID
                                                                        select new ActionSubModuleClass
                                                                        {
                                                                            ID = c.ID,
                                                                            ActionName = c.Name,
                                                                            ActionAccessId = actionAccessRepository.GetAllActionStudent().Where(d => d.RoleID == roleid && d.StudentID == studentID && d.ActionID == c.ID).FirstOrDefault() == null ? 0 : actionAccessRepository.GetAllActionStudent().Where(d => d.RoleID == roleid && d.StudentID == studentID && d.ActionID == c.ID).FirstOrDefault().ID,
                                                                            IsRequired = actionAccessRepository.GetAllActionStudent().Where(d => d.RoleID == roleid && d.StudentID == studentID && d.ActionID == c.ID).FirstOrDefault() == null ? false : true

                                                                        }).ToList()
                                                }).ToList()
                              }).ToList();
                return od;
            }
            catch
            {
                return null;
            }
        }
        public EmployeeModuleSubModuleAccess GetStudentParentModuleAccess(long roleid, long classid)
        {
            try
            {
                EmployeeModuleSubModuleAccess od = new EmployeeModuleSubModuleAccess();
                od.modules = (from a in moduleRepository.GetAllModule()
                              select new ModulesCls
                              {
                                  ID = a.ID,
                                  ModuleName = a.Name,
                                  subModules = (from b in subModuleRepository.GetAllSubModule()
                                                where b.ModuleID == a.ID
                                                select new SubModulesDtCls
                                                {
                                                    ID = b.ID,
                                                    SubModuleName = b.Name,
                                                    actionSubModules = (from c in actionRepository.GetAllAction()
                                                                        where c.SubModuleID == b.ID
                                                                        select new ActionSubModuleClass
                                                                        {
                                                                            ID = c.ID,
                                                                            ActionName = c.Name,
                                                                            ActionAccessId = actionAccessRepository.GetAllActionRole().Where(d => d.RoleID == roleid && d.StandardID == classid && d.ActionID == c.ID).FirstOrDefault() == null ? 0 : actionAccessRepository.GetAllActionRole().Where(d => d.RoleID == roleid && d.StandardID == classid && d.ActionID == c.ID).FirstOrDefault().ID,
                                                                            IsRequired = actionAccessRepository.GetAllActionRole().Where(d => d.RoleID == roleid && d.StandardID == classid && d.ActionID == c.ID).FirstOrDefault() == null ? false : true

                                                                        }).ToList()
                                                }).ToList()
                              }).ToList();
                return od;
            }
            catch
            {
                return null;
            }
        }


        public EmployeeModuleSubModuleAccess GetEmployeeModuleSubModuleAccess(long id, long eid)
        {
            try
            {
                var departments = (from a in employeeDesignationRepository.GetAllEmployeeDesignations().Where(a => a.EmployeeID == eid).ToList()
                                   join b in designationRepository.GetAllDesignation() on a.DesignationID equals b.ID
                                   select new
                                   {
                                       b.DepartmentID
                                   }).ToList();
                EmployeeModuleSubModuleAccess od = new EmployeeModuleSubModuleAccess();
                od.ID = id;
                od.modules = (from a in moduleRepository.GetAllModule()
                              select new ModulesCls
                              {
                                  ID = a.ID,
                                  ModuleName = a.Name,
                                  subModules = (from b in subModuleRepository.GetAllSubModule()
                                                where b.ModuleID == a.ID
                                                select new SubModulesDtCls
                                                {
                                                    ID = b.ID,
                                                    SubModuleName = b.Name,
                                                    actionSubModules = (from c in actionRepository.GetAllAction()
                                                                        where c.SubModuleID == b.ID
                                                                        select new ActionSubModuleClass
                                                                        {
                                                                            ID = c.ID,
                                                                            ActionName = c.Name,
                                                                            ActionAccessId = actionAccessRepository.GetAllActionAccess().Where(d => d.AccessID == id && d.ActionID == c.ID).FirstOrDefault() == null ? 0 : actionAccessRepository.GetAllActionAccess().Where(d => d.AccessID == id && d.ActionID == c.ID).FirstOrDefault().ID,
                                                                            IsRequired = actionAccessRepository.GetAllActionAccess().Where(d => d.AccessID == id && d.ActionID == c.ID).FirstOrDefault() == null ? false : true

                                                                        }).ToList()
                                                }).ToList()
                              }).ToList();

                foreach (var u in departments)
                {
                    var departmentaccess = actionAccessRepository.GetAllActionDepartment().Where(a => a.DepartmentID == u.DepartmentID).ToList();
                    foreach (var item in departmentaccess)
                    {
                        foreach (var a in od.modules)
                        {
                            foreach (var b in a.subModules)
                            {
                                var m = b.actionSubModules.Where(k => k.ID == item.ActionID).FirstOrDefault();
                                if (m != null)
                                {
                                    b.actionSubModules.Remove(m);
                                }
                                if (b.actionSubModules == null)
                                {
                                    a.subModules.Remove(b);
                                }
                            }
                        }
                    }
                }

                return od;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public IEnumerable<GivenAccesscls> givenAccesscls(long id)
        {
            try
            {
                var actionAccesses = actionAccessRepository.GetAllActionAccess().Where(a => a.AccessID == id).ToList();
                var modules = moduleRepository.GetAllModule();
                var subModules = subModuleRepository.GetAllSubModule();
                List<GivenAccesscls> mod = new List<GivenAccesscls>();
                var res = (from a in actionAccesses
                           join b in modules on a.ModuleID equals b.ID
                           select new
                           {
                               ID = b.ID,
                               ModuleName = b.Name,
                           }).ToList().Distinct();
                foreach (var a in res)
                {
                    GivenAccesscls given = new GivenAccesscls();
                    given.ID = a.ID;
                    given.ModuleName = a.ModuleName;
                    given.SubModule = string.Join(",", (from c in actionAccesses
                                                        join d in subModules on c.SubModuleID equals d.ID
                                                        where c.ModuleID == a.ID
                                                        select new
                                                        {
                                                            d.Name
                                                        }).Distinct().Select(m => m.Name).ToArray());
                    mod.Add(given);
                }
                return mod;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public IEnumerable<SidebarAccesscls> sidebarAccesscls(long id)
        {
            try
            {
                var actionAccesses = actionAccessRepository.GetAllActionAccess().Where(a => a.AccessID == id).ToList();
                var modules = moduleRepository.GetAllModule();
                var subModules = subModuleRepository.GetAllSubModule();
                List<SidebarAccesscls> mod = new List<SidebarAccesscls>();
                var res = (from a in actionAccesses
                           join b in modules on a.ModuleID equals b.ID
                           select new
                           {
                               ID = b.ID,
                               ModuleName = b.Name,
                           }).ToList().Distinct();
                foreach (var a in res)
                {
                    SidebarAccesscls given = new SidebarAccesscls();
                    given.ID = a.ID;
                    given.ModuleName = a.ModuleName;
                    given.sidebarSubmodulecls = (from c in actionAccesses
                                                 join d in subModules on c.SubModuleID equals d.ID
                                                 where c.ModuleID == a.ID
                                                 select new SidebarSubmodulecls
                                                 {
                                                     ID = d.ID,
                                                     SubmoduleName = d.Name,
                                                     Url = d.Url
                                                 }).Distinct().ToList();
                    mod.Add(given);
                }
                return mod;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public Access CheckLoginDetails(string userName, string Password)
        {
            try
            {
                var res = accessRepository.GetAccessByUserNameAndPassword(userName, Password);
                if (res != null)
                {
                    return res;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }
        public bool checkaccessavailable(string submoduleName, long accessId, string action, string moduleName, long roleId)
        {
            try
            {
                if (roleId == 1)
                {
                    return true;
                }
                else
                {
                    var moduleId = moduleRepository.GetModuleByName(moduleName).ID;
                    var submoduleId = subModuleRepository.GetAllSubModule().Where(a => a.Name == submoduleName && a.ModuleID == moduleId).FirstOrDefault().ID;
                    var actionId = actionRepository.GetAllAction().Where(a => a.SubModuleID == submoduleId && a.Name == action).FirstOrDefault().ID;

                    var employeeId = accessRepository.GetAccessById(accessId).EmployeeID;
                    if (roleId == 2 || roleId == 3 || roleId == 4)
                    {
                        var empdesignation = employeeDesignationRepository.GetAllEmployeeDesignationsByEmployeeId(employeeId).ToList();
                        var designation = designationRepository.GetAllDesignation();
                        var departmentaccess = actionAccessRepository.GetAllActionDepartment();

                        var deptres = (from a in empdesignation
                                       join b in designation on a.DesignationID equals b.ID
                                       join c in departmentaccess on b.DepartmentID equals c.DepartmentID
                                       where c.SubModuleID == submoduleId && c.ModuleID == moduleId && c.ActionID == actionId
                                       select c).ToList();


                        if (deptres.Count() == 0)
                        {
                            var access = actionAccessRepository.GetAllActionAccess().Where(a => a.AccessID == accessId && a.SubModuleID == submoduleId && a.ModuleID == moduleId && a.ActionID == actionId).FirstOrDefault();
                            if (access == null)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else if (roleId == 6)
                    {
                        string stdvalue = _httpContextAccessor.HttpContext.Session.GetString("ddlstudentid");
                        long stdId = Convert.ToInt64(stdvalue);
                        var studentclass = sqlQuery.Get_View_Academic_Student().Where(a => a.StudentId == stdId && a.IsAvailable == true).FirstOrDefault();
                        var access = actionAccessRepository.GetAllActionAcademic().Where(a => a.AcademicStandardID == studentclass.AcademicStandardId && a.SubModuleID == submoduleId && a.ModuleID == moduleId && a.ActionID == actionId).FirstOrDefault();

                        if (access == null)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else if (roleId == 5)
                    {
                        var access = actionAccessRepository.GetAllActionStudent().Where(a => a.RoleID == 5 && a.StudentID == employeeId && a.SubModuleID == submoduleId && a.ModuleID == moduleId && a.ActionID == actionId).FirstOrDefault();
                        if (access == null)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch(Exception e)
            {
                return false;
            }
        }
        public string GetDepartmentbyparentid(long id, long oid)
        {
            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            var a = departmentRepository.GetAllDepartment().Where(m => m.OrganizationID == oid && m.ID == id).FirstOrDefault();
            var departmentlist = departmentRepository.GetAllDepartment().Where(m => m.OrganizationID == oid).ToList();
            string deptname = a.Name;
            Department depts = a;
            while (depts.ParentID > 0)
            {
                depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                deptname = depts.Name + " --> " + deptname;
            }
            return deptname;
        }
        public IEnumerable<CommonMasterModel> GetDepartmentsByOrganisation(long id)
        {
            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            var departmentlist = departmentRepository.GetAllDepartment().Where(m => m.OrganizationID == id).ToList();
            foreach (var a in departmentlist)
            {
                string deptname = a.Name;
                Department depts = a;
                while (depts.ParentID > 0)
                {
                    depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                    deptname = depts.Name + "-->" + deptname;
                }
                CommonMasterModel om = new CommonMasterModel();
                om.ID = a.ID;
                om.Name = deptname;
                dept.Add(om);
            }
            return dept;
        }
        public IEnumerable<CommonMasterModel> GetDepartmentsByGroupID(long id)
        {
            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            var departmentlist = departmentRepository.GetAllDepartment().Where(m => m.GroupID == id).ToList();
            foreach (var a in departmentlist)
            {
                string deptname = a.Name;
                Department depts = a;
                while (depts.ParentID > 0)
                {
                    depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                    deptname = depts.Name + "-->" + deptname;
                }
                CommonMasterModel om = new CommonMasterModel();
                om.ID = a.ID;
                om.Name = deptname;
                dept.Add(om);
            }
            return dept;
        }
        public IEnumerable<CommonMasterModel> GetDepartments()
        {
            List<CommonMasterModel> dept = new List<CommonMasterModel>();
            var departmentlist = departmentRepository.GetAllDepartment().ToList();
            foreach (var a in departmentlist)
            {
                string deptname = a.Name;
                Department depts = a;
                while (depts.ParentID > 0)
                {
                    depts = departmentlist.Where(m => m.ID == depts.ParentID).FirstOrDefault();
                    deptname = depts.Name + "-->" + deptname;
                }
                CommonMasterModel om = new CommonMasterModel();
                om.ID = a.ID;
                om.Name = deptname;
                om.GroupID = a.GroupID;
                om.organisationID = a.OrganizationID;
                dept.Add(om);
            }
            return dept;
        }
        public IEnumerable<StudentAdressCls> GetAllAddressByUsingStudentId(long id)
        {
            try
            {
                var studentAddresses = studentAggregateRepository.GetAllStudentAddress().Where(a => a.StudentID == id).ToList();
                var address = addressRepository.GetAllAddress();
                var addressType = addressTypeRepository.GetAllAddressType();
                var city = cityRepository.GetAllCity();
                var state = stateRepository.GetAllState();
                var country = countryRepository.GetAllCountries();

                IEnumerable<StudentAdressCls> res = (from a in studentAddresses
                                                     join b in address on a.AddressID equals b.ID
                                                     join c in addressType on b.AddressTypeID equals c.ID
                                                     join d in city on b.CityID equals d.ID
                                                     join e in state on b.StateID equals e.ID
                                                     join f in country on b.CountryID equals f.ID
                                                     select new StudentAdressCls
                                                     {
                                                         AddressLine1 = b.AddressLine1,
                                                         AddressLine2 = b.AddressLine2,
                                                         AddressTypeID = b.AddressTypeID,
                                                         AddressTypeName = c.Name,
                                                         CityID = b.CityID,
                                                         CityName = d.Name,
                                                         CountryID = b.CountryID,
                                                         CountryName = f.Name,
                                                         ID = a.ID,
                                                         PostalCode = b.PostalCode,
                                                         StateID = b.StateID,
                                                         StateName = e.Name,
                                                         ModifiedDate = a.ModifiedDate,
                                                         InsertedDate = a.InsertedDate
                                                     }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<StudentModelClass> GetAllStudent()
        {
            try
            {
                var studentdetails = studentAggregateRepository.GetAllStudent();
                var nationalityTypes = nationalityTypeRepository.GetAllNationalities();
                var bloodgroup = bloodGroupRepository.GetAllBloodGroup();
                var religionType = religionTypeRepository.GetAllReligionTypes();
                var res = (from a in studentdetails
                           join b in nationalityTypes on a.NatiobalityID equals b.ID
                           join c in bloodgroup on a.BloodGroupID equals c.ID
                           join d in religionType on a.ReligionID equals d.ID
                           select new StudentModelClass
                           {
                               BloodGroupID = a.BloodGroupID,
                               BloodGroupName = c.Name,
                               DateOfBirth = a.DateOfBirth,
                               StudentCode = a.StudentCode,
                               FirstName = a.FirstName,
                               Gender = a.Gender,
                               ID = a.ID,
                               LastName = a.LastName,
                               MiddleName = a.MiddleName,
                               NatiobalityID = a.NatiobalityID,
                               NatiobalityName = b.Name,
                               ReligionID = a.ReligionID,
                               ReligionName = d.Name,
                               ModifiedDate = a.ModifiedDate
                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }

        }

        public IEnumerable<StudentParentsCls> GetAllParentsByUsingStudentId(long id)
        {
            try
            {
                var studentAddresses = studentAggregateRepository.GetAllParent().Where(a => a.StudentID == id).ToList();
                var parentRelationshipTypes = studentAggregateRepository.GetAllParentRelationshipType();
                var professionTypes = studentAggregateRepository.GetAllProfessionType();

                IEnumerable<StudentParentsCls> res = (from a in studentAddresses
                                                      join b in parentRelationshipTypes on a.ParentRelationshipID equals b.ID
                                                      join c in professionTypes on a.ProfessionTypeID equals c.ID
                                                      select new StudentParentsCls
                                                      {
                                                          ID = a.ID,
                                                          AlternativeMobile = a.AlternativeMobile,
                                                          EmailId = a.EmailId,
                                                          FirstName = a.FirstName,
                                                          LandlineNumber = a.LandlineNumber,
                                                          LastName = a.LastName,
                                                          MiddleName = a.MiddleName,
                                                          ParentRelationshipID = a.ParentRelationshipID,
                                                          ParentRelationshipName = b.Name,
                                                          PrimaryMobile = a.PrimaryMobile,
                                                          ProfessionTypeID = a.ProfessionTypeID,
                                                          ProfessionTypeName = c.Name,
                                                          StudentID = a.StudentID,
                                                          ModifiedDate = a.ModifiedDate,
                                                          //Imagepath = a.Image != null ? Path.Combine("/ODMImages/ParentProfile/", a.Image) : null
                                                      }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<StudentClassDetails> GetAllStudentClassByStudentId(long id)
        {
            try
            {
                var academics = commonsql.Get_view_All_Academic_Students().Where(a => a.StudentId == id && a.IsAvailable==true).ToList();
                var academicsection = commonsql.Get_view_All_Academic_Student_Sections().Where(a => a.StudentId == id && a.IsAvailable == true).ToList();
                IEnumerable<StudentClassDetails> res = (from a in academics
                                                        select new StudentClassDetails
                                                        {
                                                            ID = a.ID,
                                                            IsCurrent = true,
                                                            OrganizationID = a.OraganisationAcademicId,
                                                            OrganizationName = a.OraganisationName,
                                                            SectionID = academicsection!=null?(academicsection.FirstOrDefault()!=null? academicsection.FirstOrDefault().AcademicSectionId:0):0,
                                                            SectionName = academicsection != null ? (academicsection.FirstOrDefault() != null ? academicsection.FirstOrDefault().SectionName : "") : "",
                                                            StandardID = a.StandardId,
                                                            StandardName = a.StandardName,
                                                            StudentID = a.StudentId,
                                                            WingID = a.SchoolWingId,
                                                            WingName = a.WingName,
                                                            ModifiedDate = a.ModifiedDate,
                                                            BoardID = a.BoardStandardId,
                                                            BoardName = a.BoardName,
                                                            SessionID = a.AcademicSessionId,
                                                            SessionName = a.SessionName
                                                        }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<Organization> GetOrganizations()
        {
            try
            {
                var organisation = organizationRepository.GetAllOrganization().ToList();
                return organisation;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<Standard> GetStandard()
        {
            try
            {
                var Standard = standardRepository.GetAllStandard().ToList();
                //var academicstandard = boardStandardRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                //var res=(from a in academicstandard
                //         join b in Standard on a.StandardId equals b.ID
                //         select new )
                return Standard;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<Section> GetSection()
        {
            try
            {
                var Section = sectionRepository.GetAllSection().ToList();
                return Section;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<Wing> GetWing()
        {
            try
            {
                var Wing = studentAggregateRepository.GetAllWing().ToList();
                return Wing;
            }
            catch (Exception e1)
            {
                return null;
            }

        }
        //public List<Schoolwingcls> GetSchoolWing()
        //{
        //    try
        //    {
        //        var schoolwings = studentAggregateRepository.GetAllSchoolWing();
        //        var organisations = GetOrganizations();
        //        var Wing = GetWing();
        //        var res = (from a in schoolwings
        //                   join c in Wing on a.WingID equals c.ID
        //                   //join b in organisations on a.OrganisationID equals b.ID
        //                   select new Schoolwingcls
        //                   {
        //                       ID = a.ID,
        //                       OrganisationID = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.OrganizationAcademicID).Select(d => d.OrganizationId).FirstOrDefault(),
        //                       OrganizationAcademicID = a.OrganizationAcademicID,
        //                       OrganisationIDByAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.OrganizationAcademicID).Select(d => d.OrganizationId).FirstOrDefault(),
        //                       OrganisationNameByAcademic = organizationRepository.GetAllOrganization().Where(e => e.ID == organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(d => d.ID == a.OrganizationAcademicID).Select(d => d.OrganizationId).FirstOrDefault()).Select(e => e.Name).FirstOrDefault(),
        //                       WingID = a.WingID,

        //                       //OrganisationName = b.Name,
        //                       wingName = c.Name,
        //                       ModifiedDate = a.ModifiedDate,
        //                       Status = a.Status,
        //                   }).ToList();
        //        return res;
        //    }
        //    catch (Exception e1)
        //    {
        //        return null;
        //    }
        //}
        public List<Board> GetBoard()
        {
            try
            {
                var board = boardRepository.GetAllBoard().ToList();
                return board;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public bool Checkuser(string username)
        {
            var res = accessRepository.GetAllAccess().Where(a => a.Username == username).ToList().Count();
            if (res > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public IEnumerable<EmployeeAccesscls> GetStudentAccessDetails()
        {
            try
            {
                var access = accessRepository.GetAllAccess();
                var roles = roleRepository.GetAllRole();
                IEnumerable<EmployeeAccesscls> res = (from a in access
                                                      join b in roles on a.RoleID equals b.ID
                                                      select new EmployeeAccesscls
                                                      {
                                                          AccessID = a.ID,
                                                          EmployeeID = a.EmployeeID,
                                                          RoleID = a.RoleID,
                                                          RoleName = b.Name,
                                                          Password = a.Password,
                                                          Username = a.Username,
                                                          ModifiedDate = a.ModifiedDate
                                                      }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }

        }

        public List<MyGrievanceClass> GetallMyGrievance(long id, long roleId, long? typeid, long? subtypeid, long? statusid, string fromdate, string todate)
        {
            try
            {
                var grievanceType = grievanceRepository.GetAllGrievanceCategory();
                var grievanceSubType = grievanceRepository.GetAllGrievanceType();
                //var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                //var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievance = grievanceRepository.GetAllGrievance();
                var grievanceTimelines = grievanceRepository.GetAllGrievanceTimeline();
                //var roles = roleRepository.GetAllRole();
                List<MyGrievanceClass> res = (from a in grievance
                                              join b in grievanceType on a.GrievanceCategoryID equals b.ID
                                              select new MyGrievanceClass
                                              {
                                                  ID = a.ID,
                                                  InsertedDate = a.InsertedDate,                                                
                                                  ModifiedDate = a.ModifiedDate,
                                                  Code = a.Code,
                                                  RoleId = a.RoleId,
                                                  Description = a.Description,
                                                  GrievanceCategoryID = a.GrievanceCategoryID,
                                                  GrievanceCategory = b.Name,
                                                  GrievanceSubTypeID = a.GrievanceTypeID,
                                                  GrievanceSubTypeName = a.GrievanceTypeID==0?"": grievanceSubType.Where(m=>m.ID==a.GrievanceTypeID).FirstOrDefault().Name,
                                                  GrievancePriorityID = a.GrievancePriorityID,
                                                  //Priority = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.ID).FirstOrDefault().Name,
                                                  GrievanceRelatedId = a.GrievanceRelatedId,
                                                  GrievanceStatusID = a.GrievanceStatusID,
                                                  Grivstatus = GrievanceStatus(a.ID,a.GrievanceStatusID,0),
                                                  GrievanceTypeID = a.GrievanceTypeID,
                                                  GrievanceTypeName = b.Name, 
                                                  ProgressDate = a.ProgressDate,
                                                  UnreadCommentCount = GetUnreadCommentCount(id, a.ID)
                                              }).ToList();
                if (roleId == 6)
                {
                    long studentid = parentRepository.GetAllParent().Where(a => a.ID == id).FirstOrDefault().StudentID;
                    res = res.Where(a => (a.RoleId == 6 || a.RoleId == 5) && a.GrievanceRelatedId == studentid).ToList();
                }
                else
                {
                    if (roleId == 1)
                    {
                        res = res.ToList();
                    }
                    else
                    {
                        res = res.Where(a => a.RoleId == roleId && a.GrievanceRelatedId == id).ToList();
                    }
                }
                if(typeid != null)
                {
                    res = res.Where(a => a.GrievanceCategoryID == typeid).ToList();
                }
                if (subtypeid != null)
                {
                    res = res.Where(a => a.GrievanceTypeID == subtypeid).ToList();
                }
                if (statusid != null)
                {
                    res = res.Where(a => a.GrievanceStatusID == statusid).ToList();
                }
                if (fromdate != null)
                {
                    DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                    DateTime ToDate = System.DateTime.Now;
                    if (todate != null)
                    {
                        ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                    }
                    if (fromdate != null && todate != null)
                    {
                        res = res.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                    }
                    if (fromdate != null && todate == null)
                    {
                        res = res.Where(a => a.InsertedDate >= FromDate).ToList();
                    }
                }

                return res;
            }
            catch
            {
                return null;
            }
        }
        public List<MyGrievanceClass> GetallGrievances(long id, long roleId, long? typeid, long? subtypeid, long? statusid, string fromdate, string todate, long accessId)
        {
            try
            {
                //var access = grievanceRepository.GrievanceAccess().Where(a => a.Active == true && a.EmployeeId == id).ToList();


                var grievanceType = grievanceRepository.GetAllGrievanceCategory();
                //var grievanceType12 = grievanceType.Where(a => access.Select(s => s.GrievanceCategoryId).ToList().Contains(a.ID)).ToList();

                //  var accessrep = access.Where(a=>a.GrievanceTypeId==0 && a.GrievanceCategoryId!=0).ToList();

                var SubType = grievanceRepository.GetAllGrievanceType().Where(a => a.Active == true).ToList();

                //var accessreps = access.Where(a => a.GrievanceTypeId != 0 && a.GrievanceCategoryId != 0).ToList();
                //List<long> subtypeidlist = new List<long>();
                //if (accessreps.Count > 0)
                //{
                //   var Typess = SubType.Where(a => access.Select(s => s.GrievanceTypeId).ToList().Contains(a.ID)).ToList();
                //    subtypeidlist.AddRange(Typess.Select(a => a.ID).ToList());
                //}

                //if (accessrep.Count > 0)
                //{
                //    var Typess = SubType.Where(a => accessrep.Select(m => m.GrievanceCategoryId).ToList().Contains(a.CategoryId)).ToList();
                //    subtypeidlist.AddRange(Typess.Select(a => a.ID).ToList());
                //}

                //subtypeidlist = subtypeidlist.Distinct().ToList();

                //  var grievanceSubType = grievanceRepository.GetAllGrievanceType().Where(a => subtypeidlist.Contains(a.ID)).ToList();
                //var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                //var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievance = grievanceRepository.GetAllGrievance();
                var grievanceTimelines = grievanceRepository.GetAllGrievanceTimeline();
                var grievanceforward = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.EmployeeId == id && a.Active == true).Select(a => a.GrievanceId).ToList();
                //var roles = roleRepository.GetAllRole();
                List<MyGrievanceClass> res1 = (from a in grievance
                                               select new MyGrievanceClass
                                               {
                                                   ID = a.ID,
                                                   InsertedDate = a.InsertedDate,
                                                   ModifiedDate = a.ModifiedDate,
                                                   Code = a.Code,
                                                   RoleId = a.RoleId,
                                                   Description = a.Description,
                                                   GrievanceCategoryID = a.GrievanceCategoryID,
                                                   GrievanceCategory = a.GrievanceCategoryID == 0 ? "" : grievanceType.Where(m => m.ID == a.GrievanceCategoryID).FirstOrDefault().Name,
                                                   GrievanceSubTypeID = a.GrievanceTypeID,
                                                   GrievanceSubTypeName = a.GrievanceTypeID == 0 ? "" : SubType.Where(m => m.ID == a.GrievanceTypeID).FirstOrDefault().Name,
                                                   //GrievancePriorityID = a.GrievancePriorityID,
                                                   //Priority = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.ID).FirstOrDefault().Name,
                                                   GrievanceRelatedId = a.GrievanceRelatedId,
                                                   GrievanceStatusID = a.GrievanceStatusID,
                                                   Grivstatus = GrievanceStatus(a.ID, a.GrievanceStatusID,0),
                                                   GrievanceTypeID = a.GrievanceTypeID,
                                                   GrievanceTypeName = a.GrievanceCategoryID == 0 ? "" : grievanceType.Where(m => m.ID == a.GrievanceCategoryID).FirstOrDefault().Name,
                                                   ProgressDate = a.ProgressDate,
                                                      Category = GetCategoryByRoleId(a.RoleId),
                                                   UnreadCommentCount = GetUnreadCommentCount(id, a.ID)
                                               }).ToList();
                 
                //if(roleId != 0)
                //{
                //    res1 = res1.Where(a => a.RoleId == roleId).ToList();
                //}
                if (typeid != null)
                {
                    res1 = res1.Where(a => a.GrievanceCategoryID == typeid).ToList();
                }
                if (subtypeid != null)
                {
                    res1 = res1.Where(a => a.GrievanceTypeID == subtypeid).ToList();
                }
                if (statusid != null)
                {
                    res1 = res1.Where(a => a.GrievanceStatusID == statusid).ToList();
                }
                if (fromdate != null)
                {
                    DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                    DateTime ToDate = System.DateTime.Now;
                    if (todate != null)
                    {
                        ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                    }
                    if (fromdate != null && todate != null)
                    {
                        res1 = res1.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                    }
                    if (fromdate != null && todate == null)
                    {
                        res1 = res1.Where(a => a.InsertedDate >= FromDate).ToList();
                    }
                }

                return res1.ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<MyGrievanceClass> GetallManageGrievance(long id, long roleId, long? typeid, long? subtypeid, long? statusid, string fromdate, string todate,long accessId)
        {
            try
            {
                var access = grievanceRepository.GrievanceAccess().Where(a => a.Active == true && a.EmployeeId == id).ToList();


                var grievanceType = grievanceRepository.GetAllGrievanceCategory();
                var grievanceType12 = grievanceType.Where(a => access.Select(s => s.GrievanceCategoryId).ToList().Contains(a.ID)).ToList();

              //  var accessrep = access.Where(a=>a.GrievanceTypeId==0 && a.GrievanceCategoryId!=0).ToList();

                var SubType = grievanceRepository.GetAllGrievanceType().Where(a => a.Active == true).ToList();

                //var accessreps = access.Where(a => a.GrievanceTypeId != 0 && a.GrievanceCategoryId != 0).ToList();
                //List<long> subtypeidlist = new List<long>();
                //if (accessreps.Count > 0)
                //{
                //   var Typess = SubType.Where(a => access.Select(s => s.GrievanceTypeId).ToList().Contains(a.ID)).ToList();
                //    subtypeidlist.AddRange(Typess.Select(a => a.ID).ToList());
                //}

                //if (accessrep.Count > 0)
                //{
                //    var Typess = SubType.Where(a => accessrep.Select(m => m.GrievanceCategoryId).ToList().Contains(a.CategoryId)).ToList();
                //    subtypeidlist.AddRange(Typess.Select(a => a.ID).ToList());
                //}

                //subtypeidlist = subtypeidlist.Distinct().ToList();

              //  var grievanceSubType = grievanceRepository.GetAllGrievanceType().Where(a => subtypeidlist.Contains(a.ID)).ToList();
                //var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                //var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievance = grievanceRepository.GetAllGrievance();
                var grievanceTimelines = grievanceRepository.GetAllGrievanceTimeline();
                var grievanceforward = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.EmployeeId == id && a.Active == true).Select(a => a.GrievanceId).ToList();
                //var roles = roleRepository.GetAllRole();
                List<MyGrievanceClass> res1 = (from a in grievance
                                              select new MyGrievanceClass
                                              {
                                                  ID = a.ID,
                                                  InsertedDate = a.InsertedDate,
                                                  ModifiedDate = a.ModifiedDate,
                                                  Code = a.Code,
                                                  RoleId = a.RoleId,
                                                  Description = a.Description,
                                                  GrievanceCategoryID = a.GrievanceCategoryID,
                                                  GrievanceCategory = a.GrievanceCategoryID == 0 ? "" : grievanceType.Where(m => m.ID == a.GrievanceCategoryID).FirstOrDefault().Name,
                                                  GrievanceSubTypeID = a.GrievanceTypeID,
                                                  GrievanceSubTypeName = a.GrievanceTypeID == 0 ? "" : SubType.Where(m => m.ID == a.GrievanceTypeID).FirstOrDefault().Name,
                                                  //GrievancePriorityID = a.GrievancePriorityID,
                                                  //Priority = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.ID).FirstOrDefault().Name,
                                                  GrievanceRelatedId = a.GrievanceRelatedId,
                                                  GrievanceStatusID = a.GrievanceStatusID,
                                                  Grivstatus = GrievanceStatus(a.ID, a.GrievanceStatusID,0),
                                                  GrievanceTypeID = a.GrievanceTypeID,
                                                  GrievanceTypeName = a.GrievanceCategoryID == 0 ? "" : grievanceType.Where(m => m.ID == a.GrievanceCategoryID).FirstOrDefault().Name,
                                                  ProgressDate = a.ProgressDate,
                                                  Category = GetCategoryByRoleId(a.RoleId),
                                                  UnreadCommentCount = GetUnreadCommentCount(id, a.ID),
                                                  IsRequestedForStudentDetails = a.IsRequestedForStudentDetails,
                                                  IsRejected = a.IsGrievanceRejected
                                              }).ToList();
                if (checkaccessavailable("Manage Grievance", accessId, "Management", "Grievance", roleId) == false)
                {
                    var forwardgrievance = res1.Where(a => grievanceforward.Contains(a.ID)).ToList();
                    var exceptgrievance = res1.Except(forwardgrievance);

                    var typegrievance = exceptgrievance;
                    var typegrievance1 = exceptgrievance;
                    var accessrep = access.Where(a => a.GrievanceTypeId == 0 && a.GrievanceCategoryId != 0 && a.EmployeeId==id).ToList();
                    if (accessrep.Count > 0)
                    {
                        typegrievance = typegrievance.Where(a => accessrep.Select(m => m.GrievanceCategoryId).ToList().Contains(a.GrievanceCategoryID)).ToList();
                        
                    }
                    else
                    {
                        typegrievance = new List<MyGrievanceClass>();
                    }
                    var accessreps = access.Where(a => a.GrievanceTypeId != 0 && a.GrievanceCategoryId != 0 && a.EmployeeId == id).ToList();

                    if (accessreps.Count > 0)
                    {
                        typegrievance1 = typegrievance1.Where(a => access.Select(s => s.GrievanceTypeId).ToList().Contains(a.GrievanceTypeID)).ToList();
                    }
                    else
                    {
                        typegrievance1 = new List<MyGrievanceClass>();

                    }
                    var grev = typegrievance1.Union(typegrievance);

                  //  var res = grev.Union(forwardgrievance);
                    res1 = new List<MyGrievanceClass>();
                    res1 = grev.Union(forwardgrievance).ToList(); 
                }
                else
                {
                    var forwardgrievance = res1.Where(a => grievanceforward.Contains(a.ID)).ToList();
                    //var exceptgrievance = res1.Except(forwardgrievance);
                    if (forwardgrievance.Count() > 0)
                    {
                        res1 = res1.Where(a => access.Select(s => s.GrievanceCategoryId).Contains(a.GrievanceCategoryID)).ToList();
                        res1 = res1.Union(forwardgrievance).ToList();
                    }
                    else
                    {
                        res1 = res1.Where(a => access.Select(s => s.GrievanceCategoryId).Contains(a.GrievanceCategoryID)).ToList();
                    }
                }
                if (typeid != null)
                {
                    res1 = res1.Where(a => a.GrievanceCategoryID == typeid).ToList();
                }
                if (subtypeid != null)
                {
                    res1 = res1.Where(a => a.GrievanceTypeID == subtypeid).ToList();
                }
                if (statusid != null)
                {
                    res1 = res1.Where(a => a.GrievanceStatusID == statusid).ToList();
                }
                if (fromdate != null)
                {
                    DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                    DateTime ToDate = System.DateTime.Now;
                    if (todate != null)
                    {
                        ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                    }
                    if (fromdate != null && todate != null)
                    {
                        res1 = res1.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                    }
                    if (fromdate != null && todate == null)
                    {
                        res1 = res1.Where(a => a.InsertedDate >= FromDate).ToList();
                    }
                }

                return res1.ToList();
            }
            catch
            {
                return null;
            }
        }
        public int GetUnreadCommentCount(long userid,long grivid)
        {
            int commentCount = 0;
            var comment = _dbContext.GrievanceComments.Where(a => a.GrievanceID == grivid && a.Active == true).ToList();
            foreach(var k in comment)
            {
                if(k.InsertedId != userid && k.IsRead == false)
                {
                    commentCount = commentCount + 1;
                }
            }

            return commentCount;
        }

        public List<MyGrievanceClass> GetallGrievanceReport(long id, long roleId, long? typeid, long? subtypeid, long? statusid, string fromdate, string todate, long accessId)
        {
            try
            {
                var access = grievanceRepository.GrievanceAccess().Where(a => a.Active == true && a.EmployeeId == id).ToList();


                var grievanceType = grievanceRepository.GetAllGrievanceCategory();
                var grievanceType12 = grievanceType.Where(a => access.Select(s => s.GrievanceCategoryId).ToList().Contains(a.ID)).ToList();

                //  var accessrep = access.Where(a=>a.GrievanceTypeId==0 && a.GrievanceCategoryId!=0).ToList();

                var SubType = grievanceRepository.GetAllGrievanceType().Where(a => a.Active == true).ToList();

                //var accessreps = access.Where(a => a.GrievanceTypeId != 0 && a.GrievanceCategoryId != 0).ToList();
                //List<long> subtypeidlist = new List<long>();
                //if (accessreps.Count > 0)
                //{
                //   var Typess = SubType.Where(a => access.Select(s => s.GrievanceTypeId).ToList().Contains(a.ID)).ToList();
                //    subtypeidlist.AddRange(Typess.Select(a => a.ID).ToList());
                //}

                //if (accessrep.Count > 0)
                //{
                //    var Typess = SubType.Where(a => accessrep.Select(m => m.GrievanceCategoryId).ToList().Contains(a.CategoryId)).ToList();
                //    subtypeidlist.AddRange(Typess.Select(a => a.ID).ToList());
                //}

                //subtypeidlist = subtypeidlist.Distinct().ToList();

                //  var grievanceSubType = grievanceRepository.GetAllGrievanceType().Where(a => subtypeidlist.Contains(a.ID)).ToList();
                //var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                //var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievance = grievanceRepository.GetAllGrievance();
                var grievanceTimelines = grievanceRepository.GetAllGrievanceTimeline();
                var grievanceforward = grievanceForwardRepository.ListAllAsync().Result.Where(a => a.EmployeeId == id && a.Active == true).Select(a => a.GrievanceId).ToList();
                //var roles = roleRepository.GetAllRole();
                List<MyGrievanceClass> res1 = (from a in grievance
                                               select new MyGrievanceClass
                                               {
                                                   ID = a.ID,
                                                   InsertedDate = a.InsertedDate,
                                                   ModifiedDate = a.ModifiedDate,
                                                   Code = a.Code,
                                                   RoleId = a.RoleId,
                                                   Description = a.Description,
                                                   GrievanceCategoryID = a.GrievanceCategoryID,
                                                   GrievanceCategory = a.GrievanceCategoryID == 0 ? "" : grievanceType.Where(m => m.ID == a.GrievanceCategoryID).FirstOrDefault().Name,
                                                   GrievanceSubTypeID = a.GrievanceTypeID,
                                                   GrievanceSubTypeName = a.GrievanceTypeID == 0 ? "" : SubType.Where(m => m.ID == a.GrievanceTypeID).FirstOrDefault().Name,
                                                   //GrievancePriorityID = a.GrievancePriorityID,
                                                   //Priority = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.ID).FirstOrDefault().Name,
                                                   GrievanceRelatedId = a.GrievanceRelatedId,
                                                   GrievanceStatusID = a.GrievanceStatusID,
                                                   Grivstatus = GrievanceStatus(a.ID, a.GrievanceStatusID,0),
                                                   GrievanceTypeID = a.GrievanceTypeID,
                                                   GrievanceTypeName = a.GrievanceCategoryID == 0 ? "" : grievanceType.Where(m => m.ID == a.GrievanceCategoryID).FirstOrDefault().Name,
                                                   ProgressDate = a.ProgressDate,
                                                   Category = GetCategoryByRoleId(a.RoleId),
                                                   UnreadCommentCount = GetUnreadCommentCount(id, a.ID)
                                               }).ToList();
                if (checkaccessavailable("Manage Grievance", accessId, "Management", "Grievance", roleId) == false)
                {
                    var forwardgrievance = res1.Where(a => grievanceforward.Contains(a.ID)).ToList();
                    var exceptgrievance = res1.Except(forwardgrievance);

                    var typegrievance = exceptgrievance;
                    var typegrievance1 = exceptgrievance;
                    var accessrep = access.Where(a => a.GrievanceTypeId == 0 && a.GrievanceCategoryId != 0 && a.EmployeeId == id).ToList();
                    if (accessrep.Count > 0)
                    {
                        typegrievance = typegrievance.Where(a => accessrep.Select(m => m.GrievanceCategoryId).ToList().Contains(a.GrievanceCategoryID)).ToList();

                    }
                    else
                    {
                        typegrievance = new List<MyGrievanceClass>();
                    }
                    var accessreps = access.Where(a => a.GrievanceTypeId != 0 && a.GrievanceCategoryId != 0 && a.EmployeeId == id).ToList();

                    if (accessreps.Count > 0)
                    {
                        typegrievance1 = typegrievance1.Where(a => access.Select(s => s.GrievanceTypeId).ToList().Contains(a.GrievanceTypeID)).ToList();
                    }
                    else
                    {
                        typegrievance1 = new List<MyGrievanceClass>();

                    }
                    var grev = typegrievance1.Union(typegrievance);

                    //  var res = grev.Union(forwardgrievance);
                    res1 = new List<MyGrievanceClass>();
                    res1 = grev.Union(forwardgrievance).ToList();
                }
                if (typeid != null)
                {
                    res1 = res1.Where(a => a.GrievanceCategoryID == typeid).ToList();
                }
                if (subtypeid != null)
                {
                    res1 = res1.Where(a => a.GrievanceTypeID == subtypeid).ToList();
                }
                if (statusid != null)
                {
                    res1 = res1.Where(a => a.GrievanceStatusID == statusid).ToList();
                }
                if (fromdate != null)
                {
                    DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                    DateTime ToDate = System.DateTime.Now;
                    if (todate != null)
                    {
                        ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                    }
                    if (fromdate != null && todate != null)
                    {
                        res1 = res1.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                    }
                    if (fromdate != null && todate == null)
                    {
                        res1 = res1.Where(a => a.InsertedDate >= FromDate).ToList();
                    }
                }

                return res1.ToList();
            }
            catch
            {
                return null;
            }
        }









        public List<MyGrievanceClass> GetallGrievance(long id, long roleId, long? typeid, long? subtypeid, long? statusid, string fromdate, string todate)
        {
            try
            {
                var grievanceType = grievanceRepository.GetAllGrievanceCategory();
                var grievanceSubType = grievanceRepository.GetAllGrievanceType();
                //var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                //var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievance = grievanceRepository.GetAllGrievance();
                var grievanceTimelines = grievanceRepository.GetAllGrievanceTimeline();
                //var roles = roleRepository.GetAllRole();
                List<MyGrievanceClass> res = (from a in grievance
                                              join b in grievanceType on a.GrievanceCategoryID equals b.ID
                                              join c in grievanceSubType on a.GrievanceTypeID equals c.ID
                                              select new MyGrievanceClass
                                              {
                                                  ID = a.ID,
                                                  InsertedDate = a.InsertedDate,
                                                  ModifiedDate = a.ModifiedDate,
                                                  Code = a.Code,
                                                  RoleId = a.RoleId,
                                                  Description = a.Description,
                                                  GrievanceCategoryID = a.GrievanceCategoryID,
                                                  GrievanceCategory = b.Name,
                                                  GrievanceSubTypeID = a.GrievanceTypeID,
                                                  GrievanceSubTypeName = c.Name,
                                                  GrievancePriorityID = a.GrievancePriorityID,
                                                  //Priority = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.ID).FirstOrDefault().Name,
                                                  GrievanceRelatedId = a.GrievanceRelatedId,
                                                  GrievanceStatusID = a.GrievanceStatusID,
                                                  Grivstatus = GrievanceStatus(a.ID, a.GrievanceStatusID,0),
                                                  GrievanceTypeID = a.GrievanceTypeID,
                                                  GrievanceTypeName = b.Name,
                                                  ProgressDate = a.ProgressDate,
                                                  Category = GetCategoryByRoleId(a.RoleId)
                                              }).ToList();
              
                    if (roleId == 1)
                    {
                        res = res.ToList();
                    }
                    else
                    {
                        res = res.Where(a => a.RoleId == roleId && a.GrievanceRelatedId == id).ToList();
                    }
                
                if (typeid != null)
                {
                    res = res.Where(a => a.GrievanceCategoryID == typeid).ToList();
                }
                if (subtypeid != null)
                {
                    res = res.Where(a => a.GrievanceTypeID == subtypeid).ToList();
                }
                if (statusid != null)
                {
                    res = res.Where(a => a.GrievanceStatusID == statusid).ToList();
                }
                if (fromdate != null)
                {
                    DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                    DateTime ToDate = System.DateTime.Now;
                    if (todate != null)
                    {
                        ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                    }
                    if (fromdate != null && todate != null)
                    {
                        res = res.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                    }
                    if (fromdate != null && todate == null)
                    {
                        res = res.Where(a => a.InsertedDate >= FromDate).ToList();
                    }
                }

                return res;
            }
            catch
            {
                return null;
            }
        }
        public List<GrievanceStatusModel> GrievanceCategoryList()
        {
            List<GrievanceStatusModel> categoryModels = new List<GrievanceStatusModel> { 
                                new GrievanceStatusModel { ID = 5, StatusName = "Student" },
                                new GrievanceStatusModel { ID = 6, StatusName = "Parent" }, 
            }.ToList();

            return categoryModels;
        }
        public string GetCategoryByRoleId(long id)
        {
            string category = null; 
            if (id == 5)
            {
                return category = "Student";
            }
            if (id == 6)
            {
                return category = "Parent";
            }
            if(id == 1)
            {
                return category = "NA";
            }

            return category;
        }
        public class GrievanceStatusModel
        {
            public long ID { get; set; }
            public string StatusName { get; set; }
            public string EmpName { get; set; }
            public string SubAssignedToName { get; set; }
            public long RoleId { get; set; }
        }
        public List<GrievanceStatusModel> GrievanceStatusList()
        {
            List<GrievanceStatusModel> statusModels = new List<GrievanceStatusModel> {
                                new GrievanceStatusModel { ID = 1, StatusName = "PENDING" },
                                new GrievanceStatusModel { ID = 2, StatusName = "ON-Hold" },
                                new GrievanceStatusModel { ID = 3, StatusName = "IN-PROGRESS" },
                                new GrievanceStatusModel { ID = 4, StatusName = "COMPLETED" },
                                //new GrievanceStatusModel { ID = 5, StatusName = "REJECTED" },
                                new GrievanceStatusModel { ID = 6, StatusName = "VERIFIED" }
            }.ToList();

            return statusModels;
        }
        public string GetEmployeeNameByEmpId(long id)
        {
            var emp = employeeRepository.GetEmployeeById(id);
            var FullName = emp.FirstName + " " + emp.LastName;
            return FullName;
        }
        public GrievanceStatusModel GrievanceStatus(long? timelineid,long? id,long roleid)
        {
            
            GrievanceStatusModel statusModel = new GrievanceStatusModel();
            if (id.Value == 1)
            {
                statusModel.ID = 1;
                statusModel.StatusName = "PENDING";
                statusModel.EmpName = "NA";
                statusModel.RoleId = roleid;
            }
            if (id.Value == 2)
            {
                statusModel.ID = 2;
                statusModel.StatusName = "ON-Hold";
                statusModel.EmpName = "NA";
                statusModel.RoleId = roleid;
            }
            if (id.Value == 3)
            {
                statusModel.ID = 3;
                statusModel.StatusName = "IN-PROGRESS";
                statusModel.EmpName = "NA";
                statusModel.RoleId = roleid;
            }
            if (id.Value == 4)
            {
                statusModel.ID = 4;
                statusModel.StatusName = "COMPLETED";
                statusModel.EmpName = "NA";
                statusModel.RoleId = roleid;
            }
            if (id.Value == 5)
            {
                statusModel.ID = 5;
                statusModel.StatusName = "REJECTED";
                statusModel.EmpName = "NA";
                statusModel.RoleId = roleid;
            }
            if (id.Value == 6)
            {
                statusModel.ID = 6;
                statusModel.StatusName = "VERIFIED";
                statusModel.EmpName = "NA";
                statusModel.RoleId = roleid;
            }

            if (id.Value == 7)
            {
                var gv = _dbContext.GrievanceTimelines.Where(a => a.ID == timelineid).FirstOrDefault();
                if (gv != null)
                {
                    statusModel.ID = 7;
                    statusModel.StatusName = "Grievance is forwarded";
                    statusModel.EmpName = gv.ForwardToId != 0 ? GetEmployeeNameByEmpId(gv.ForwardToId.Value) : grievanceRepository.GetAllGrievanceCategory().Where(p => p.ID == gv.ForwardToDept).FirstOrDefault().Name + " (Dept)";
                    statusModel.RoleId = roleid;
                }
            }
            if (id.Value == 8)
            {
                var gv = _dbContext.GrievanceTimelines.Where(a => a.ID == timelineid).FirstOrDefault();
                if (gv != null)
                {
                    statusModel.ID = 8;
                    statusModel.StatusName = "Grievance Timeline is extended";
                    statusModel.EmpName = GetEmployeeNameByEmpId(gv.ModifiedId);
                    statusModel.RoleId = roleid;
                }
            }
            if (id.Value == 9)
            {
                var gv = _dbContext.GrievanceTimelines.Where(a => a.ID == timelineid).FirstOrDefault();
                if (gv != null)
                {
                    statusModel.ID = 9;
                    statusModel.StatusName = "Grievance is Sub-Assigned";
                    statusModel.EmpName = GetEmployeeNameByEmpId(gv.ModifiedId);
                    statusModel.SubAssignedToName = GetEmployeeNameByEmpId(gv.ForwardToId.Value);
                    statusModel.RoleId = roleid;
                }
            }
            
            return statusModel;
        }
        public List<MyGrievanceClass> GetallMyGrievance()
        {
            try
            {
                var grievancetype = grievanceRepository.GetAllGrievanceType();
                var grievancepriority = grievanceRepository.GetAllGrievancePriority();
                var grievancestatus = grievanceRepository.GetAllGrievanceStatus();
                var grievance = grievanceRepository.GetAllGrievance();
                var grievanceTimelines = grievanceRepository.GetAllGrievanceTimeline();
                var roles = roleRepository.GetAllRole();
                List<MyGrievanceClass> res = (from a in grievance
                                              join b in grievancetype on a.GrievanceTypeID equals b.ID
                                              join d in roles on a.GrievanceCategoryID equals d.ID
                                              join e in grievancestatus on a.GrievanceStatusID equals e.ID
                                              select new MyGrievanceClass
                                              {
                                                  Description = a.Description,
                                                  GrievanceCategoryID = a.GrievanceCategoryID,
                                                  GrievanceCategory = d.Name,
                                                  GrievancePriorityID = a.GrievancePriorityID,
                                                  Priority = a.GrievancePriorityID == 0 ? "" : grievancepriority.Where(m => m.ID == a.ID).FirstOrDefault().Name,
                                                  GrievanceRelatedId = a.GrievanceRelatedId,
                                                  GrievanceStatusID = a.GrievanceStatusID,
                                                  status = e.Name,
                                                  GrievanceTypeID = a.GrievanceTypeID,
                                                  GrievanceTypeName = b.Name,
                                                  ID = a.ID,
                                                  InsertedDate = a.InsertedDate,
                                                  Code = a.Code,
                                                  ModifiedDate = a.ModifiedDate
                                              }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public string GetGrievancecode(long roleid)
        {
            try
            {
                var grievance = grievanceRepository.GetactiveinactiveGrievances().Where(a => a.RoleId == roleid).ToList();
                long count = 0;
                string code = "";
                if (roleid == 4)
                {
                    code = "E-GV-";
                }
                else if (roleid == 5)
                {
                    code = "S-GV-";
                }
                else if (roleid == 6)
                {
                    code = "P-GV-";
                }
                else
                {
                    code = "O-GV-";
                }
                if (grievance.Count() > 0)
                {
                    count = (grievance.LastOrDefault().ID + 1);
                }
                else
                {
                    count = 1;
                }
                if (count.ToString().Length == 1)
                {
                    code += "0000" + count;
                }
                else if (count.ToString().Length == 2)
                {
                    code += "000" + count;
                }
                else if (count.ToString().Length == 3)
                {
                    code += "00" + count;
                }
                else if (count.ToString().Length == 4)
                {
                    code += "0" + count;
                }
                else
                {
                    code += count;
                }
                return code;
            }
            catch
            {
                return "";
            }
        }

        public List<Employeeddlview> GetManageGrievanceemployee()
        {
            try
            {
                var employees = employeeRepository.GetAllEmployee();
                var submoduleid = subModuleRepository.GetSubModuleByName("Manage Grievance").ID;
                var departmentaccess = actionAccessRepository.GetAllActionDepartment().Where(a => a.SubModuleID == submoduleid).ToList();
                var designations = designationRepository.GetAllDesignation();
                var employeedepartments = employeeDesignationRepository.GetAllEmployeeDesignations();
                List<Employeeddlview> res = (from a in employees
                                             join b in employeedepartments on a.ID equals b.EmployeeID
                                             join c in designations on b.DesignationID equals c.ID
                                             join d in departmentaccess on c.DepartmentID equals d.DepartmentID
                                             select new Employeeddlview
                                             {
                                                 Code = a.EmpCode,
                                                 Id = a.ID,
                                                 Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                                             }).ToList().Distinct().ToList();
                var actionaccess = actionAccessRepository.GetAllActionAccess();
                var access = accessRepository.GetAllAccess();
                List<Employeeddlview> resl = (from a in employees
                                              join b in access on a.ID equals b.EmployeeID
                                              join c in actionaccess on b.ID equals c.AccessID
                                              where c.SubModuleID == submoduleid
                                              select new Employeeddlview
                                              {
                                                  Code = a.EmpCode,
                                                  Id = a.ID,
                                                  Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                                              }).ToList().Distinct().ToList();
                List<Employeeddlview> result = res.Union(resl).ToList().Distinct().ToList();
                return result;
            }
            catch (Exception e1)
            {

                return null;
            }
        }
        public List<Employeeddlview> GetAssignverifyGrievanceemployee()
        {
            try
            {
                var employees = employeeRepository.GetAllEmployee();
                var grievancetype = grievanceRepository.GetAllGrievanceCategory().ToList();
                var grievancesubtype = grievanceRepository.GetAllGrievanceType().ToList();
                var grievanceaccess = grievanceRepository.GrievanceAccess().ToList();

                var result = (from a in grievanceaccess
                              join c in grievancetype on a.GrievanceCategoryId equals c.ID
                              join d in employees on a.EmployeeId equals d.ID
                              select new Employeeddlview
                              {
                                  typeid=a.GrievanceCategoryId,
                                  subtypeid=a.GrievanceTypeId,
                                  Code = d.EmailId,
                                  Id = d.ID,
                                  Name = d.FirstName + " " + (d.MiddleName == null ? d.LastName : (d.MiddleName + " " + d.LastName))
                              }).ToList();

                //var submoduleid = subModuleRepository.GetSubModuleByName("Assign & Verify Grievance").ID;
                //var departmentaccess = actionAccessRepository.GetAllActionDepartment().Where(a => a.SubModuleID == submoduleid).ToList();
                //var designations = designationRepository.GetAllDesignation();
                //var employeedepartments = employeeDesignationRepository.GetAllEmployeeDesignations();
                //List<Employeeddlview> res = (from a in employees
                //                             join b in employeedepartments on a.ID equals b.EmployeeID
                //                             join c in designations on b.DesignationID equals c.ID
                //                             join d in departmentaccess on c.DepartmentID equals d.DepartmentID
                //                             select new Employeeddlview
                //                             {
                //                                 Code = a.EmailId,
                //                                 Id = a.ID,
                //                                 Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                //                             }).ToList().Distinct().ToList();
                //var actionaccess = actionAccessRepository.GetAllActionAccess();
                //var access = accessRepository.GetAllAccess();
                //List<Employeeddlview> resl = (from a in employees
                //                              join b in access on a.ID equals b.EmployeeID
                //                              join c in actionaccess on b.ID equals c.AccessID
                //                              where c.SubModuleID == submoduleid
                //                              select new Employeeddlview
                //                              {
                //                                  Code = a.EmailId,
                //                                  Id = a.ID,
                //                                  Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                //                              }).ToList().Distinct().ToList();
                //List<Employeeddlview> result = res.Union(resl).ToList().Distinct().ToList();
                return result;
            }
            catch (Exception e1)
            {

                return null;
            }
        }

        public long AddSuperAdmin(string username, string password)
        {
            long accessId = 0;
            long roleId = roleRepository.CreateSuperAdminRole();

            if (roleId > 0)
            {
                long employeeId = employeeRepository.CreateSuperAdmin(roleId);

                if (employeeId > 0)
                {
                    accessId = accessRepository.CreateSuperAdminAccess(employeeId, username, password, roleId);
                }
            }


            return accessId;
        }
        public int GetRoleCount()
        {
            return roleRepository.GetAllRole().Count();
        }

        //private static List<TaskLayerclass> FillRecursiveTask(List<TaskLayerclass> flatObjects, long parentId)
        //{            
        //    List<TaskLayerclass> recursiveObjects = new List<TaskLayerclass>();
        //    foreach (var a in flatObjects.Where(x => x.ParentTaskID.Equals(parentId)))
        //    {
        //        recursiveObjects.Add(new TaskLayerclass
        //        {
        //            Id = a.Id,
        //            Title = a.Title,
        //            Description = a.Description,
        //            workName = a.workName,
        //            workSectionName = a.workSectionName,
        //            insertedOn = a.insertedOn,
        //            taskPrioritieName = a.taskPrioritieName,
        //            CompletionDate = a.CompletionDate,
        //            TaskStatus = a.TaskStatus,
        //            taskLayerclasses = FillRecursiveTask(flatObjects, a.Id)
        //        });
        //    }
        //    return recursiveObjects;
        //}
        //public IEnumerable<TaskLayerclass> GetallMyTasks(long empid)
        //{
        //    try
        //    {
        //        var tasks = taskAggregateRepository.GetAllTask();
        //        var accesstasks = taskAggregateRepository.GetAllAccessTasks();
        //        var employees = employeeRepository.GetAllEmployee();
        //        var works = taskAggregateRepository.GetAllWork();
        //        var worksection = taskAggregateRepository.GetAllWorkSection();
        //        var completion = taskAggregateRepository.GetAllTaskCompletionDetails();
        //        var statuses = taskAggregateRepository.GetAllTaskStatusDetails();
        //        var res = (from a in tasks
        //                   join b in accesstasks on a.ID equals b.TaskID
        //                   where a.InsertedId==empid && b.EmployeeID==empid && a.TaskType=="OWN"
        //                   select new TaskLayerclass
        //                   {
        //                       Id=a.ID,
        //                       Title=a.Title,
        //                       Description=a.Description,
        //                       workName=a.WorkID==null?"":taskAggregateRepository.GetWorkById(a.WorkID.Value).Title,
        //                       workSectionName=a.WorkSectionID==null?"":taskAggregateRepository.GetWorkSectionById(a.WorkSectionID.Value).Name,
        //                       insertedOn=a.InsertedDate,
        //                       taskPrioritieName=taskAggregateRepository.GetTaskPriorityById(completion.Where(m=>m.TaskId==a.ID).OrderByDescending(m=>m.ID).FirstOrDefault().TaskPriorityID).Name,
        //                       CompletionDate= completion.Where(m => m.TaskId == a.ID).OrderByDescending(m => m.ID).FirstOrDefault().CompletionDate,
        //                       TaskStatus= statuses.Where(m => m.TaskId == a.ID).OrderByDescending(m => m.ID).FirstOrDefault().TaskStatus

        //                   }).ToList();
        //        var e= FillRecursiveTask(res, 0); 
        //        return e;
        //    }
        //    catch 
        //    {
        //        return null;
        //    }
        //}

        public List<commonCls> GetDepartmentsByEmployeeID(long id)
        {
            try
            {
                var departments = GetDepartments();
                var employeedepartments = (from a in GetAllDesignationByUsingEmployeeId(id)
                                           select new { a.DepartmentID }).ToList().Distinct();
                var res = (from a in employeedepartments
                           join b in departments on a.DepartmentID equals b.ID
                           select new commonCls
                           {
                               id = b.ID,
                               name = b.Name,
                               groupid = b.GroupID
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<commonCls> GetSuperadminDepartments()
        {
            try
            {
                var departments = departmentRepository.GetAllDepartment();

                var res = (from a in departments

                           select new commonCls
                           {
                               id = a.ID,
                               name = a.Name,

                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<commonCls> GetEmployeesForTasks(long id)
        {
            try
            {
                var employees = employeeRepository.GetAllEmployee();

                var self = (from a in employees
                            where a.ID == id
                            select new commonCls
                            {
                                id = a.ID,
                                name = "me",
                                profile = a.Image
                            }).ToList();
                var exceptself = (from a in employees
                                  where a.ID != id
                                  select new commonCls
                                  {
                                      id = a.ID,
                                      name = a.FirstName + " " + a.LastName,
                                      profile = a.Image
                                  }).ToList();

                var res = self.Union(exceptself).ToList();

                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<commonCls> GetEmployeesForTaskswithleft(long id)
        {
            try
            {
                var employees = employeeRepository.GetAllEmployeewithleft();

                var self = (from a in employees
                            where a.ID == id
                            select new commonCls
                            {
                                id = a.ID,
                                name = "me",
                                profile = a.Image
                            }).ToList();
                var exceptself = (from a in employees
                                  where a.ID != id
                                  select new commonCls
                                  {
                                      id = a.ID,
                                      name = a.FirstName + " " + a.LastName,
                                      profile = a.Image
                                  }).ToList();

                var res = self.Union(exceptself).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

     

        #region GetAllErpBugs

        public async Task<IReadOnlyList<ErpBugModel>> GetAllErpBugs()
        {
            try
            {
                var erpBugList = erpBugRepository.ListAllAsyncIncludeAll().Result.ToList();
                var moduleList = moduleRepository.GetAllModule();
                var subModuleList = subModuleRepository.GetAllSubModule();
                var actionList = actionRepository.GetAllAction();

                var res = (from a in erpBugList
                           join b in moduleList on a.ModuleID equals b.ID
                           join c in subModuleList on a.SubModuleID equals c.ID
                           join d in actionList on a.ActionID equals d.ID
                           select new ErpBugModel
                           {
                               ID = a.ID,
                               ModuleID = a.ModuleID,
                               ModuleName = b.Name,
                               SubModuleName = c.Name,
                               ActionName = d.Name,
                               ActionID = a.ActionID,
                               Title = a.Title,
                               Description = a.Description

                           }

                          ).ToList();
                return res;
            }
            catch
            {
                return null;
            }

        }


        #endregion

        #region EditErpBugs

        public ErpBugModel EditErpBugs(long id)
        {
            try
            {
                var erpBugList = erpBugRepository.ListAllAsyncIncludeAll().Result.ToList();
                var moduleList = moduleRepository.GetAllModule();
                var subModuleList = subModuleRepository.GetAllSubModule();
                var actionList = actionRepository.GetAllAction();

                var res = (from a in erpBugList
                           join b in moduleList on a.ModuleID equals b.ID
                           join c in subModuleList on a.SubModuleID equals c.ID
                           join d in actionList on a.ActionID equals d.ID
                           select new ErpBugModel
                           {
                               ID = a.ID,
                               ModuleID = a.ModuleID,
                               ModuleName = b.Name,
                               SubModuleID = a.SubModuleID,
                               SubModuleName = c.Name,
                               ActionName = d.Name,
                               ActionID = a.ActionID,
                               Title = a.Title,
                               Description = a.Description

                           }

                          ).FirstOrDefault(w => w.ID == id);
                return res;
            }
            catch
            {
                return null;
            }

        }


        #endregion
        

        #region student details
        public IEnumerable<StudentAdressCls> GetAllAddressByUsingStudent()
        {
            try
            {
                var studentAddresses = studentAggregateRepository.GetAllStudentAddress().ToList();
                var address = addressRepository.GetAllAddress();
                var addressType = addressTypeRepository.GetAllAddressType();
                var city = cityRepository.GetAllCity();
                var state = stateRepository.GetAllState();
                var country = countryRepository.GetAllCountries();

                IEnumerable<StudentAdressCls> res = (from a in studentAddresses
                                                     join b in address on a.AddressID equals b.ID
                                                     join c in addressType on b.AddressTypeID equals c.ID
                                                     join d in city on b.CityID equals d.ID
                                                     join e in state on b.StateID equals e.ID
                                                     join f in country on b.CountryID equals f.ID
                                                     select new StudentAdressCls
                                                     {
                                                         AddressLine1 = b.AddressLine1,
                                                         AddressLine2 = b.AddressLine2,
                                                         AddressTypeID = b.AddressTypeID,
                                                         AddressTypeName = c.Name,
                                                         CityID = b.CityID,
                                                         CityName = d.Name,
                                                         CountryID = b.CountryID,
                                                         CountryName = f.Name,
                                                         ID = a.ID,
                                                         PostalCode = b.PostalCode,
                                                         StateID = b.StateID,
                                                         StateName = e.Name,
                                                         ModifiedDate = a.ModifiedDate,
                                                         InsertedDate = a.InsertedDate,
                                                         StudentId=a.StudentID,
                                                     }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<StudentClassDetails> GetAllStudentClassByStudent()
        {
            try
            {
                var academics = commonsql.Get_view_All_Academic_Students().Where(a =>  a.IsAvailable == true).ToList();
                var academicsection = commonsql.Get_view_All_Academic_Student_Sections().Where(a => a.StudentId == academics.FirstOrDefault().ID && a.IsAvailable == true).ToList();
              
                IEnumerable<StudentClassDetails> res = (from a in academics
                                                        select new StudentClassDetails
                                                        {
                                                            ID = a.ID,                                                            
                                                            IsCurrent = true,
                                                            OrganizationID = a.OraganisationAcademicId,
                                                            OrganizationName = a.OraganisationName,
                                                            SectionID = commonsql.Get_view_All_Academic_Student_Sections().Where(b => b.StudentId == a.StudentId && a.IsAvailable == true) != null ? (commonsql.Get_view_All_Academic_Student_Sections().Where(b => b.StudentId == a.StudentId && a.IsAvailable == true).FirstOrDefault() != null ? commonsql.Get_view_All_Academic_Student_Sections().Where(b => b.StudentId == a.StudentId && a.IsAvailable == true).FirstOrDefault().AcademicSectionId : 0) : 0,
                                                            SectionName = commonsql.Get_view_All_Academic_Student_Sections().Where(b => b.StudentId ==a.StudentId && a.IsAvailable == true) != null ? (commonsql.Get_view_All_Academic_Student_Sections().Where(c => c.StudentId ==a.StandardId && a.IsAvailable == true).FirstOrDefault() != null ? commonsql.Get_view_All_Academic_Student_Sections().Where(d => d.StudentId ==a.StandardId&& a.IsAvailable == true).FirstOrDefault().SectionName : "") : "",
                                                            StandardID = a.StandardId,
                                                            StandardName = a.StandardName,
                                                            StudentID = a.StudentId,
                                                            WingID = a.SchoolWingId,
                                                            WingName = a.WingName,
                                                            ModifiedDate = a.ModifiedDate,
                                                            BoardID = a.BoardStandardId,
                                                            BoardName = a.BoardName,
                                                            SessionID = a.AcademicSessionId,
                                                            SessionName = a.SessionName
                                                        }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        #endregion

        public class EmpViewModal
        {
            public long EmployeeId { get; set; }
            public string EmployeeName { get; set; }
        }
        public List<EmpViewModal> GetEmployeeFullName()
        {
            //List<EmpViewModal> viewModal1 = new List<EmpViewModal>();
            EmpViewModal viewModal = new EmpViewModal();
            var emp = employeeRepository.GetAllEmployee();
            //foreach (var a in emp)
            //{
            //    viewModal.EmployeeId = a.ID;
            //    viewModal.EmployeeName = a.FirstName + " " + a.LastName;               
            //}
            //viewModal1.Add(viewModal);
            var res = (from a in emp
                       select new EmpViewModal
                       {
                           EmployeeId = a.ID,
                           EmployeeName = a.EmpCode +"-"+ a.FirstName + " " + a.LastName
                       });                      

            return res.ToList();
        }
    }
}
