﻿using OdmErp.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using OdmErp.Web.Models;
using X.PagedList;
using X.PagedList.Mvc;

namespace OdmErp.Web.Models
{
    public class SessionFeesTypeViewModel : BaseModel
    {

        public long SessionId { get; set; }
        public long FeesTypeId { get; set; }
        public long ParentFeesTypeId { get; set; }
        public long ApplicableId { get; set; }
        public string FeesTypeName { get; set; }
        public string SessionName { get; set; }
        public string ParentFeesType { get; set; }
        public string ApplicableTo { get; set; }
        public bool Mandatory { get; set; }
        public DateTime DeadlineDate { get; set; }
        public bool HaveInstallment {get;set;}
        public bool HaveChild { get; set; }

       // public IPagedList<SessionFeesTypeViewModel> SessionFeesType { get; set; }
    }

    public class FeesTypeViewModel : BaseModel
    {

        public long SessionId { get; set; }
        public long FeesTypeId { get; set; }
        public long ParentFeesTypeId { get; set; }
        public long ApplicableId { get; set; }
        public string FeesTypeName { get; set; }
        public string SessionName { get; set; }
        public string ParentFeesType { get; set; }
        public string ApplicableTo { get; set; }
        public string Price { get; set; }
        public string ApplicableData { get; set; }

        // public IPagedList<SessionFeesTypeViewModel> SessionFeesType { get; set; }
    }
}
