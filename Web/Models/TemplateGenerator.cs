﻿using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.Data;
using OdmErp.Web.Areas.Admission.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Web.Models
{
    public class TemplateGenerator
    {
        private readonly ApplicationDbContext _DbContext;

        public IAdmissionExamDateAssignRepository admissionExamDateAssignRepository;
        public IAdmissionStandardExamRepository admissionStandardExamRepository;
        public IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository;
        public IEmployeeRepository employeeRepository;
        public IAdmissionDocumentRepository admissionDocumentRepository;
        public IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepository;
        public IAdmissionStudentDocumentRepository admissionStudentDocumentRepository;
        public IDocumentSubTypeRepository documentSubTypeRepository;
        public IDocumentTypeRepository documentTypeRepository;
        public IPaymentType paymentType;
        public IBoardStandardRepository boardStandardRepository;
        public IAdmissionExamDateAssignRepository admissionExamDateAssign;
        public IAdmissionStudentExamRepository admissionStudentExamRepository;
        public IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository;
        public IAdmissionRepository admissionRepository;
        public IAcademicSessionRepository academicSessionRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IOrganizationRepository organizationRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IBoardRepository boardRepository;
        public IStandardRepository standardRepository;
        public IWingRepository wingRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IAdmissionStudentRepository admissionStudentRepository;
        public IAdmissionStudentAcademicRepository admissionStudentAcademicRepository;
        public IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository;
        public IAdmissionCounsellorRepository admissionCounsellorRepository;
        public IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssign;
        public IAdmissionDocumentAssignRepository admissionDocumentAssignRepository;
        public ICounsellorStatusRepository counsellorStatusRepository;
        public IAdmissionInterViewerRepository admissionInterViewerRepository;
        public IAdmissionVerifierRepository admissionVerifierRepository;
        public CommonMethods commonMethods;
             public TemplateGenerator(IAdmissionStudentDocumentRepository admissionStudentDocumentRepo,
            IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo,
            IAdmissionDocumentRepository admissionDocumentRepo,
            CommonMethods commonMethods,
            IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepo,
            IEmployeeRepository employeeRepo, 
            IAdmissionExamDateAssignRepository admissionExamDateAssignRepo,
            IAdmissionStandardExamRepository admissionStandardExamRepo,IAdmissionDocumentAssignRepository adsRepo,
            IAcademicSessionRepository asrepo, IStandardRepository srepo, IBoardRepository brpo, IWingRepository wpo,
            IOrganizationAcademicRepository oarepo, IOrganizationRepository orepo, IAcademicStandardWingRepository aswrepo,
            IAcademicStandardRepository astdrepo, IAdmissionRepository admrepo, IBoardStandardRepository bostdrepo,
             IBoardStandardWingRepository boardStandardWingRepository,
              IAdmissionStudentRepository admStdrepo, IAdmissionStudentAcademicRepository admStdAcdrepo,
             IAdmissionStudentStandardRepository admStdStadrepo, IAdmissionFormPaymentRepository admFormParEpo,
             IPaymentType paty, IBoardStandardRepository boardStandardRepository, IAcademicStandardSettingsRepository academicStandardSettingsRepository,
         IAdmissionExamDateAssignRepository admissionExamDateAssign, IAdmissionStudentExamRepository admissionStudentExamRepository, IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository,
         IAdmissionPersonalInterviewAssignRepository apiasign, IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository,
         IAdmissionCounsellorRepository admissionCounsellorRepository, ICounsellorStatusRepository counsellorStatusRepo,
         IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository, IAdmissionInterViewerRepository admissionInterViewerRepository, IAdmissionVerifierRepository admissionVerifierRepository
            )
        {
            this.commonMethods = commonMethods;
            this.admissionInterViewerRepository = admissionInterViewerRepository;
            this.admissionStudentCounsellorRepository = admissionStudentCounsellorRepository;
            this.admissionCounsellorRepository = admissionCounsellorRepository;
            this.admissionDocumentAssignRepository = adsRepo;
            this.admissionPersonalInterviewAssign = apiasign;
            this.admissionPersonalInterviewAssignRepository = admissionPersonalInterviewAssignRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.admissionExamDateAssign = admissionExamDateAssign;
            this.admissionStudentExamRepository = admissionStudentExamRepository;
            this.admissionStandardExamResultDateRepository = admissionStandardExamResultDateRepository;
            paymentType = paty;
            admissionStudentStandardRepository = admStdStadrepo;
            admissionStudentAcademicRepository = admStdAcdrepo;
            admissionStudentRepository = admStdrepo;
            this.boardStandardWingRepository = boardStandardWingRepository;
            academicSessionRepository = asrepo;
            academicStandardRepository = astdrepo;
            academicStandardWingRepository = aswrepo;
            organizationRepository = orepo;
            organizationAcademicRepository = oarepo;
            boardRepository = brpo;
            standardRepository = srepo;
            wingRepository = wpo;
            admissionRepository = admrepo;
            this.boardStandardRepository = bostdrepo;
            this.admissionFormPaymentRepository = admFormParEpo;
            admissionStudentDocumentRepository = admissionStudentDocumentRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            admissionDocumentRepository = admissionDocumentRepo;
            admissionDocumentVerificationAssignRepository = admissionDocumentVerificationAssignRepo;
            employeeRepository = employeeRepo;
            admissionExamDateAssignRepository = admissionExamDateAssignRepo;
            admissionStandardExamRepository = admissionStandardExamRepo;
            this.counsellorStatusRepository = counsellorStatusRepo;
            this.admissionVerifierRepository = admissionVerifierRepository;
        }
        public static  string GetHTMLString(string sessionName, string FormNumber, string FirstName,string MiddleName,  string LastName, DateTime? DateOfBirth, string orgName, string board,string wingName, string standard, DateTime InsertedDate, string Image)
        {

            string imageUrl = "https://localhost:44315" + Image;
            string rollno = "111111";
            //sessionName,admissionSingle.FormNumber, admissionStudentSingle.FirstName, admissionStudentSingle.MiddleName, admissionStudentSingle.LastName, admissionStudentSingle.DateOfBirth, admissionstudentStandardSingle.orgName, admissionstudentStandardSingle.board, admissionstudentStandardSingle.standard, admissionstudentStandardSingle,admissionSingle.InsertedDate
            var sb = new StringBuilder();
            sb.Append(@"<html><head></head><body style='max-width:684px;' >
                    <div class='modal-body' style=' border: 3px solid red;margin:15px 15px 60px 15px;' id='printDiv'>
                        <div class='form-group row' style=' color: black; font-family: monospace; font-size: 18px;'>
                           <table style='font-family:monospace;font-size:17px;font-weight:600'><tr><td width='25%' style='overflow-x:hidden; overflow-y: hidden;'> <img style='width:150px;height:150px;margin-left:2%' src='" + imageUrl);
            sb.Append(@"'  /></td><td width='50%'>School Name- " + orgName);
            sb.Append("<br/> Phone Number - <span>1800-120-2316</span><br/> Admission for the Session <span id='sessionval'>" + sessionName);

            sb.Append(@"</span></td><td width='20%' style='overflow-x:hidden; overflow-y: hidden;'><img style='width:150px;  height:150px;' id='studentImageUrl' src=" + imageUrl);
            sb.Append(@"/></td></tr></table></div><div style='height: 30px; background: red; color: white;display: flex;justify-content: center;font-weight: 700;padding: 2px;margin-top: 20px;'>ADMIT CARD</div><div  style='margin-top: 30px;font-weight: 600;color: black;margin-bottom:30px;'><table><tr><td width='63%'><div>Application No : <span style=' font-weight: 400;' id='applicationId'>" + FormNumber);
            sb.Append("</span></div><br/><div >Board : <span style=' font-weight: 400;' id='boardVal'>" + board);
            sb.Append(@"</span></div><br/><div>Modified Date : <span id='modifiedData'>" + InsertedDate);
            sb.Append(@"</span></div></td><td width='50%'><div>Roll No : <span id='rollNo' style='font-weight: 400;'>" + rollno);
            sb.Append(@"</span></div><br/><div>Class : <span id='standardVal' style='font-weight: 400;'>" + standard);
            sb.Append(@"</span></div><br/><div>Boarding Type : <span style='font-weight: 400;' id='WingName'>" + wingName);
            sb.Append(@"</span></div></td></tr></table></div><div class='row' style=' height: 30px; background: red; color: white;display: flex;justify-content: center;font-weight: 500;padding: 2px;'>Instruction Details</div><div class='clearfix' style='margin-bottom:100px;'></div><div class='row' style=' height: 30px; background: red; color: white;display: flex;justify-content: center;font-weight: 500;padding: 2px;'>Venue of Entrance Test</div><div class='clearfix' style='margin-bottom:10px;'></div></div></body></html>");
            return sb.ToString();
        }
    }
}
