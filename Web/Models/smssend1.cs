﻿using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Data;

namespace OdmErp.Web.Models
{
    public class smssend1
    {
        public static string UserName = "parismita@thedigichamps.com";
        public static bool EnableSsl = true;
        public static string Host = "smtp.gmail.com";
        public static string Password = "abcd@1234";
        public static int Port = 587;
        public static string domain = "http://localhost:44315/";
        private readonly IHostingEnvironment hostingEnvironment;

        public smssend1(IHostingEnvironment hostingEnv)
        {
            hostingEnvironment = hostingEnv;
        }
        public bool Sendsmstoemployee(string mobile, string message)
        {
            string authKey = "232520Aycx7OOpF5b790d71";
            //Multiple mobiles numbers separated by comma
            string mobileNumber = mobile;
            //Sender ID,While using route4 sender id should be 6 characters long.
            string senderId = "ODMEGR";
            //Your message to send, Add URL encoding here.
            string msg = HttpUtility.UrlEncode(message);

            //Prepare you post parameters
            StringBuilder sbPostData = new StringBuilder();
            sbPostData.AppendFormat("authkey={0}", authKey);
            sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
            sbPostData.AppendFormat("&message={0}", msg);
            sbPostData.AppendFormat("&sender={0}", senderId);
            sbPostData.AppendFormat("&route={0}", "4");

            try
            {
                //Call Send SMS API
                string sendSMSUri = "http://api.msg91.com/api/sendhttp.php";
                //Create HTTPWebrequest
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                //Prepare and Add URL Encoded data
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(sbPostData.ToString());
                //Specify post method
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                //Get the response
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string responseString = reader.ReadToEnd();

                //Close the response
                reader.Close();
                response.Close();
                return true;
            }
            catch (SystemException ex)
            {
                return false;
            }
            //try
            //{
            //    string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=ODMEGR&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
            //    + "&country=91&message=" + message;

            //    HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);

            //    HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
            //    System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            //    string responseString = respStreamReader.ReadToEnd();
            //    respStreamReader.Close();
            //    myResp.Close();
            //    return true;

            //}
            //catch (Exception e1)
            //{
            //    return false;
            //}
        }


        public Attachment Make_ICS(MailMessage mailMessage, DataSet ds_ICS, string Type)
        {
            Attachment attach = null;
            DateTime ICS_Date = DateTime.Now;
            DateTime stdate = DateTime.Parse(ds_ICS.Tables[1].Rows[0]["InsertedDate"].ToString());
            string timeZone = "+0100";
            string timeZoneName = "India Standard Time";
            string ICS_Body = "";
            string organizerName = "ODMEGR";
            string Organizer_Email = UserName;
            string ICS_Subject = "";

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("BEGIN:VCALENDAR");
            sb.AppendLine("PRODID:-//Google Inc//Google Calendar 70.9054//EN");
            sb.AppendLine("VERSION:2.0");
            sb.AppendLine("METHOD:PUBLISH");
            sb.AppendLine("");
            sb.AppendLine("BEGIN:VTIMEZONE");
            sb.AppendLine("TZID:" + timeZoneName);
            sb.AppendLine("BEGIN:STANDARD");
            sb.AppendLine("DTSTART:20180910T100000");
            sb.AppendLine("TZOFFSETFROM:" + timeZone);
            sb.AppendLine("TZOFFSETTO:" + timeZone);
            sb.AppendLine("END:STANDARD");
            sb.AppendLine("END:VTIMEZONE");
            sb.AppendLine("BEGIN:VEVENT");

            sb.AppendLine("CLASS:PUBLIC");
            sb.AppendLine("CREATED:" + ICS_Date.ToString("yyyyMMddTHHmm00"));
            sb.AppendLine("DESCRIPTION:" + ICS_Body);
            sb.AppendLine("DTSTART;TZID=\"" + timeZoneName + "\":" + stdate.ToString("yyyyMMddTHHmm00"));
            sb.AppendLine("DTSTAMP:" + stdate.ToString("yyyyMMddTHHmm00"));
            sb.AppendLine("DTEND;TZID=\"" + timeZoneName + "\":" + stdate.ToString("yyyyMMddTHHmm00"));
            sb.AppendLine("LAST-MODIFIED:" + stdate.ToString("yyyyMMddTHHmm00"));
            sb.AppendLine("ORGANIZER;CN=\"" + organizerName + "\":mailto:" + Organizer_Email + "");
            sb.AppendLine("PRIORITY:5");
            sb.AppendLine("SEQUENCE:0");

            sb.AppendLine("SUMMARY;LANGUAGE=en-us:" + ICS_Subject);
            sb.AppendLine("TRANSP:OPAQUE");

            return attach;
        }


        private void SendHtmlFormattedEmail(string recepientEmail, string subject, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(UserName);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = Host;
                smtp.EnableSsl = EnableSsl;
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = UserName;
                NetworkCred.Password = Password;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = Port;
                smtp.Send(mailMessage);
            }
        }
        public string newgrievance(string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "grievancetemplate.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string assigngrievance(string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "assigngrievance.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string resolvegrievance(string code, string complain, string raisedby, string employeename, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "resolvegrievance.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{code}}", code).Replace("{{complain}}", complain).Replace("{{raisedby}}", raisedby).Replace("{{employeename}}", employeename);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string verifygrievance(string grievancedetails, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "verifiedgrievance.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{grievancedetails}}", grievancedetails);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string taskmail(string desc, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "tasktemplate.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Description}}", desc).Replace("{{subject}}",subject);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public string raisesupport(string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "raisesupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string deadlinedatesupport(string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "deadlinedatesupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public string assigneddeadlinedate(string duedate,string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "assigneddeadlinedate.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{duedate}}", duedate).Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public string resolvesupport(string code, string complain, string raisedby, string employeename, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "resolvesupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{code}}", code).Replace("{{complain}}", complain).Replace("{{raisedby}}", raisedby).Replace("{{employeename}}", employeename);
                SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public void Authenticate()
         {
            string[] scopes = new string[] {
     CalendarService.Scope.Calendar //, // Manage your calendars
 	//CalendarService.Scope.CalendarReadonly // View your Calendars
 };
            string cal_user = "parismita@thedigichamps.com"; //your CalendarID On which you want to put events
                                                      //you get your calender id "https://calendar.google.com/calendar"
                                                      //go to setting >>calenders tab >> select calendar >>Under calender Detailes at Calendar Address:
            var webPath = hostingEnvironment.WebRootPath;
            string filePath = Path.Combine("", webPath + @"\Key\key.json");
            var service = ServiceAccountExample.AuthenticateServiceAccount("google-calender-app@healthville.iam.gserviceaccount.com", filePath, scopes);
            //"xyz@projectName.iam.gserviceaccount.com" this is your service account email id replace with your service account emailID you got it .
            //when you create service account https://console.developers.google.com/projectselector/iam-admin/serviceaccounts
            Event myEvent = new Event
            {
                Summary = "Visa Counselling",
                Location = "Gurgaon sector 57",
                Start = new EventDateTime()
                {
                    DateTime = new DateTime(2019, 12, 21, 5, 0, 0),
                    TimeZone = "(GMT+05:30) India Standard Time"
                },
                End = new EventDateTime()
                {
                    DateTime = new DateTime(2019, 12, 21, 6, 0, 0),
                    TimeZone = "(GMT+05:30) India Standard Time"
                }
                //,
                // Recurrence = new String[] {
                //"RRULE:FREQ=WEEKLY;BYDAY=MO"
                //}
                //,
                // Attendees = new List<EventAttendee>()
                // {
                // new EventAttendee() { Email = "Srivastava998@gmail.com" }
                //}
            };
            insert(service, cal_user, myEvent);

        }



        public static Event insert(CalendarService service, string id, Event myEvent)
        {
            try
            {
                return service.Events.Insert(myEvent, id).Execute();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
