﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Models
{
    public class BaseModel
    {
        public long ID { get; set; }
       
        public DateTime InsertedDate { get; set; }
   
        public DateTime ModifiedDate { get; set; }

        public string ModifiedDateText { get; set; }

        public long InsertedId { get; set; }
     
        public long ModifiedId { get; set; }
     
        public bool Active { get; set; }
      
        public string Status { get; set; }
       
        public bool IsAvailable { get; set; }
    }
}
