﻿using OdmErp.ApplicationCore.Interfaces;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serialization.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.BillDesk
{
    public class RazorpayManager
    {


        protected readonly IAdmissionRepository admissionRepository;
        protected readonly IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        protected readonly IPaymentGatewayRepository paymentGatewayRepository;

        public RazorpayManager(IAdmissionRepository admissionRepository, 
            IAdmissionFormPaymentRepository admissionFormPaymentRepository, 
            IPaymentGatewayRepository paymentGatewayRepository)
        {
            this.admissionRepository = admissionRepository;
            this.admissionFormPaymentRepository = admissionFormPaymentRepository;
            this.paymentGatewayRepository = paymentGatewayRepository;
        }

        public List<FormPaymentSnapshot> GetPaymentList()
         {

            List<FormPaymentSnapshot> list = new List<FormPaymentSnapshot>();

            var data = (from a in paymentGatewayRepository.GetAllPaymentSnapshot().ToList()

                        select new FormPaymentSnapshot
                        {
                            TransactionId = admissionFormPaymentRepository.ListAllAsync().Result.
                            Where(b => b.PaymentGatewayProxyId == a.PaymentSnapshotId.ToString()).
                            Select(b=> b.TransactionId).ToList(),

                            TransactionDate=a.InsertedDate.ToString("dd-MMM-yyyy"),
                            OrderId=a.UniqueTxnId,
                           // response= GetRazorPayDetails(a.UniqueTxnId),
                            AdmissionId = a.RelatedId,
                           // Mobile= admissionRepository.GetAdmissionById(a.RelatedId).MobileNumber,
                           // Email = admissionRepository.GetAdmissionById(a.RelatedId).Email


                        }

                      ).ToList();


            Parallel.ForEach(data, (currentFile) =>
            {
                // The more computational work you do here, the greater
                currentFile.response = GetRazorPayDetails(currentFile.OrderId);
            });


            return data;
         }

        private OrdertResponse GetRazorPayDetails(string orderId)
        {

         //   var data = paymentGatewayRepository.GetAllPaymentGateway().Where(a => a.ProviderName =="RP").FirstOrDefault();

            var client = new RestClient
                 ("https://api.razorpay.com/v1/orders/"+ orderId + "/payments");
            client.Authenticator =
                new HttpBasicAuthenticator("rzp_live_OEKP7Fa708wk5Q", "TBKRIRcewC5EfobiyIkeEGRN");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/json");

            
            IRestResponse response = client.Execute(request);

            return
                new JsonDeserializer().
                Deserialize<OrdertResponse>(response);
        }


     


    }

    public class FormPaymentSnapshot
    {
        public long AdmissionId { get; set; }
        public List<string> TransactionId { get; set; }
        public string TransactionDate { get; set; }
        public string OrderId { get; set; }
        public string PaymentMode { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string PymentMethod { get; set; }

        public OrdertResponse response { get; set; }
    }

    public class OrdertResponse
    {
        public string entity { get; set; }
        public int count { get; set; }
        public List<FormPaymentResponse> items { get; set; }

    }

    public class FormPaymentResponse
    {

        public string id { get; set; }
        public string entity { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
        public string status { get; set; }
        public string order_id { get; set; }

        public string invoice_id { get; set; }
        public bool international { get; set; }
        public string method { get; set; }
        public string amount_refunded { get; set; }

        public string refund_status { get; set; }
        public bool captured { get; set; }

        public string card_id { get; set; }
        public string description { get; set; }
        public string bank { get; set; }

        public string wallet { get; set; }
        public string vpa { get; set; }


        public string email { get; set; }
        public string contact { get; set; }


        public int fee { get; set; }
        public int tax { get; set; }


        public string error_code { get; set; }
        public string error_description { get; set; }
        public long created_at { get; set; }




    }
}
