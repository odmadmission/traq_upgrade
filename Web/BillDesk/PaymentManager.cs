﻿using Microsoft.AspNetCore.Hosting;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo;
using OdmErp.Web.Areas.Admission.Models;
using OdmErp.Web.LeadSquared;
using OdmErp.Web.Models;
using OdmErp.Web.SendInBlue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static OdmErp.Web.LeadSquared.LeadManage;

namespace OdmErp.Web.BillDesk
{
    public class PaymentManager
    {
        public IAdmissionStudentContactRepository admissionStudentContactRepository;
        public IPaymentGatewayRepository pgRepo;
        public IAdmissionFeeRepository admissionFeeRepo;
        public IAdmissionRepository admissionRepo;
        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IPaymentType ipaymentType;
        public IHostingEnvironment hostingEnv;
        public IOrganizationRepository organizationRepo;
        public IBoardRepository boardRepo;
        public IStandardRepository standardRepo;
        public IWingRepository wingRepo;
        public AdmissionStudentCommonMethod
           admissionStudentCommonMethod;
        public smssend smssend;
        public SendInBlueEmailManager sendInBlueEmail;
        public IAcademicSessionRepository sessionRepo;
        public PaymentManager(IPaymentGatewayRepository
            pgRepo,
            IOrganizationRepository organizationRepo,
            IBoardRepository boardRepo,
            IWingRepository wingRepo,
            IStandardRepository standardRepo,
        IAcademicSessionRepository sessionRepo,
        IHostingEnvironment hostingEnv,
        IAdmissionFeeRepository admissionFeeRepo,
            IAdmissionRepository admissionRepo,
            IPaymentType ipaymentType,
            smssend smssend,
             SendInBlueEmailManager sendInBlueEmail,
            AdmissionStudentCommonMethod
           admissionStudentCommonMethod,
           IAdmissionStudentContactRepository admissionStudentContactRepository,
        IAdmissionFormPaymentRepository admissionFormPaymentRepository)
        {
            this.sendInBlueEmail = sendInBlueEmail;
            this.organizationRepo = organizationRepo;
            this.boardRepo = boardRepo;
            this.wingRepo = wingRepo;
            this.standardRepo = standardRepo;

            this.sessionRepo = sessionRepo;
            this.admissionStudentContactRepository =
               admissionStudentContactRepository;
            this.admissionStudentCommonMethod =
                admissionStudentCommonMethod;
            this.hostingEnv = hostingEnv;
            this.smssend = smssend;
            this.pgRepo = pgRepo;
            this.ipaymentType = ipaymentType;
            this.admissionFormPaymentRepository = 
                admissionFormPaymentRepository;
            this.admissionFeeRepo = admissionFeeRepo;
            this.admissionRepo = admissionRepo;
        }

        private (string url, string method, string msg,
            string fullUrl,string checkSum)
            CreateString(
             string admissionLeadReferenceId,
            string paymentSnashotId,
            string pgTransId,
             string amount,
            string mobile)
        {

            //string text =
            //     " MerchantID |" +
            //     "UniqueTxnID | " +
            //     "NA | " +
            //     "TxnAmount |" +
            //     "NA |" +
            //     "NA |" +
            //     "NA |" +
            //     "CurrencyType | " +
            //     "NA | " +
            //     "TypeField1 | " +
            //      "SecurityID |"+
            //      " NA |" +
            //      "NA |" +
            //      "TypeField2 |" +
            //      "txtadditional1 |" +
            //      "txtadditional2 |" +
            //      "txtadditional3 |" +
            //      "txtadditional4 |" +
            //      "txtadditional5 |" +
            //      "txtadditional6 |" +
            //      "txtadditional7 |" +
            //      "RU";

            var data = pgRepo.GetAllPaymentGateway().
                Where(a => a.Active == true).FirstOrDefault();

            var data2 = pgRepo.GetAllPaymentGatewayBTB().
              Where(a => a.Active == true && a.PaymentGatewayId
              == data.ID).FirstOrDefault();

            string text =
                 data.MerchantId.Trim() + "|" +
                 pgTransId.Trim() + "|" +
                 "NA|" +
                 amount.Trim() + ".00" + "|" +
                 "NA|" +
                 "NA|" +
                 "NA|" +
                 data.CurrencyType.Trim() + "|" +
                 "NA|" +
                  data.TypeField.Trim() + "|" +
                   data.MerchantId.Trim().ToLower() + "|" +
                  "NA|" +
                  "NA|" +
                 data.TypeFieldTwo.Trim() + "|" +
                  paymentSnashotId.Trim() + "|" +
                  mobile.Trim() + "|" +
                  "AdmissionRegistrationFee|" +
                  admissionLeadReferenceId.Trim() + "|" +
                  "NA|" +
                  "NA|" +
                  "NA|" +
                 data2.BtbParam.Trim();


            string checksumKey = new HashGenerator().
                GetHMACSHA256(text, data.CheckSum);
            string msg = text + "|" + checksumKey;

            string getUrl = data.Host + "?" + data.RequestParameter + "=" + msg;



            return (data.Host, data.HttpMethod,
                msg, getUrl
                , checksumKey);
        }


        public string
            BillDeskPaymentRequestAsync(string id)
        {

            OdmErp.ApplicationCore.Entities.Admission admi
                = admissionRepo.
                    GetAllAdmission().Where(a => a.LeadReferenceId == Guid.Parse
                    (id)).FirstOrDefault();
            DateTimeOffset utcNow = DateTimeOffset.UtcNow;
            string UniqTxnid = "ODMEGR-ADMRF-" + utcNow.Year + "" + utcNow.Month
                + utcNow.Day + "" + utcNow.Hour + "" +
               utcNow.Minute + "" + utcNow.Second + "" + utcNow.ToString("ffffff");

            var fee = admissionFeeRepo.ListAllAsync().
                Result.Where(a => a.AcademicSessionId
                == admi.AcademicSessionId
                && a.Name == "online").
                FirstOrDefault();
            var paymentSnapshotId = Guid.NewGuid();
            var reqString = CreateString(id,paymentSnapshotId.ToString(),UniqTxnid,
                fee.Ammount + "",
                admi.MobileNumber);
            SavePaymentSnapShot(paymentSnapshotId,
                admi.ID,
                PaymentRelatedName.RELATED_TO_ADMISSION,
                 reqString.msg, reqString.checkSum, UniqTxnid, 
                 fee.Ammount + "", fee.ID);

            return reqString.fullUrl;
        }


       private BillDeskResponseModel SplitResponseString(string msg)
        {

            BillDeskResponseModel model = null;
          
            string[] responseSplits = msg.Split("|");
            if (responseSplits != null &&
               responseSplits.Length > 0)
            {
                model = new BillDeskResponseModel();
                model.Msg = msg;              
                model.MerchantID = responseSplits[0];
                model.UniqueTxnID = responseSplits[1];
                model.TxnReferenceNo = responseSplits[2];
                model.BankReferenceNo = responseSplits[3];
                model.TxnAmount = responseSplits[4];
                model.BankID = responseSplits[5];
                model.BIN = responseSplits[6];
                model.TxnType = responseSplits[7];
                model.CurrencyName = responseSplits[8];
                model.ItemCode = responseSplits[9];
                model.SecurityType = responseSplits[10];
                model.SecurityID = responseSplits[11];
                model.SecurityPassword = responseSplits[12];
                model.TxnDate = responseSplits[13];
                model.AuthStatus = responseSplits[14];
                model.SettlementType = responseSplits[15];
                model.AdditionalInfo1 = responseSplits[16];
                model.AdditionalInfo2 = responseSplits[17];
                model.AdditionalInfo3 = responseSplits[18];
                model.AdditionalInfo4 = responseSplits[19];
                model.AdditionalInfo5 = responseSplits[20];
                model.AdditionalInfo6 = responseSplits[21];
                model.AdditionalInfo7 = responseSplits[22];
                model.ErrorStatus = responseSplits[23];
                model.ErrorDescription = responseSplits[24];
                model.CheckSum = responseSplits[25];


                

            }

            return model;

        }
        public PaymentResponseViewModel ProcessRazorPaymentStatus(
            string paymentid,string admid)
        {
            PaymentResponseViewModel prvModel =
                new PaymentResponseViewModel();

            if(paymentid == null)
            {
                prvModel.Status = PaymentViewStatus.FAILURE;
                prvModel.ErrorDescription = "Response Null";
                return prvModel;
            }
            var admission = admissionRepo.
                    GetAdmissionByLeadReferenceId(admid);
            var data =
                pgRepo.GetPaymentSnapshotByAdmissionId(admission.ID);

            if (data != null)
            {


                #region FormPayment
                
                if (admission == null)
                {
                    prvModel.Status = PaymentViewStatus.FAILURE;
                    prvModel.ErrorDescription = "Admission Null";
                    return prvModel;
                }



                var fee = admissionRepo.GetAdmissionFeeById(
            "online", admission.AcademicSessionId);
                if (admission.IsAdmissionFormAmountPaid)
                {
                    prvModel.UniqueTxnID = paymentid;
                    prvModel.Amount = fee.Ammount + "";
                    prvModel.LeadReferenceId = admission.LeadReferenceId.ToString();
                    prvModel.Status = PaymentViewStatus.SUCCESS;
                    prvModel.ErrorDescription = "Transaction sucessfull";
                    return prvModel;
                }


                if (fee == null)
                {
                    prvModel.Status = PaymentViewStatus.FAILURE;
                    prvModel.ErrorDescription = "Admission Fee Not found";
                    return prvModel;
                }
                var paymentType = ipaymentType.
                     GetPaymentTypeByBillDeskCode(data.TxnType);
                if (paymentType == null)
                {
                    prvModel.Status = PaymentViewStatus.FAILURE;
                    prvModel.ErrorDescription = "PaymentType Fee Not found";
                    return prvModel;
                }


              

                AdmissionFormPayment payment = new AdmissionFormPayment();
                payment.AdmissionId = admission.ID;

                payment.PaymentModeId = paymentType.ID;
                payment.TransactionId = paymentid;
                payment.AdmissionFeeId = fee.ID;
                payment.PaidDate = DateTime.Now;
                payment.ReceivedId = 0;
                payment.Active = true;
                payment.IsAvailable = true;
                payment.Status = EntityStatus.ACTIVE;
                payment.InsertedDate = DateTime.Now;
                payment.ModifiedDate = DateTime.Now;
                payment.InsertedId = 0;
                payment.ModifiedId = 0;
                payment.PaymentGatewayProxyId = data.PaymentSnapshotId.ToString();

                admissionRepo.CreateAdmissionFormPayment(payment);

                AdmissionTimeline timeline1 = new AdmissionTimeline();
                timeline1.AdmissionId = admission.ID;
                timeline1.Timeline = "Registration Fee Paid";
                timeline1.Active = true;
                timeline1.IsAvailable = true;
                timeline1.Status = "PAID";
                timeline1.InsertedDate = DateTime.Now;
                timeline1.ModifiedDate = DateTime.Now;
                timeline1.InsertedId = 0;
                timeline1.ModifiedId = 0;
                admissionRepo.CreateAdmissionTimeline(timeline1);

                DateTime utcNow = DateTime.Now;
                string formNumber = utcNow.Year + "" + utcNow.Month
                    + utcNow.Day + "" + utcNow.Hour + "" +
                   utcNow.Minute + "" + utcNow.Second + "" + utcNow.ToString("ff");

                try
                {
                    admission.FormNumber = formNumber;
                    admission.IsAdmissionFormAmountPaid = true;
                    admission.ModifiedDate = DateTime.Now;
                    admission.Status = Admission_Status.PAID;
                    admission.ModifiedId = 0;

                    var data2 = admissionRepo.UpdateAdmission(admission);
                }
                catch(Exception e)
                {
                    AdmissionTimeline timeline2 = new AdmissionTimeline();
                    timeline1.AdmissionId = admission.ID;
                    timeline1.Timeline = "FormNumber Exception:"+e.Message;
                    timeline1.Active = false;
                    timeline1.IsAvailable = false;
                    timeline1.Status = "EXCEPTION";
                    timeline1.InsertedDate = DateTime.Now;
                    timeline1.ModifiedDate = DateTime.Now;
                    timeline1.InsertedId = 0;
                    timeline1.ModifiedId = 0;
                    admissionRepo.CreateAdmissionTimeline(timeline2);
                }
              //  var admStd = admissionRepo.GetAdmissionStudentById(admission.ID);

                SendReceiptCard(admission.ID,
                    payment.PaidDate,
                    fee.Ammount+"",
                    paymentType.Name,
                    payment.TransactionId);

                try
                {
                    LeadCreateDto dto = new LeadCreateDto();

                    dto.Status = "PAID";
                    dto.PaidOn = payment.InsertedDate.ToString("dd-MMM-yyyy hh:mm tt");
                        



                    LeadManage leadManage = new LeadManage();
                    LeadResponse lead = leadManage.
                        updateLead(leadManage.UpdateLeadData(dto),
                        admission.MobileNumber);
                    //if (lead != null && lead.Message != null)
                    //{
                    //    adm.LeadId = Guid.Parse(lead.Message.Id);
                    //    admissionRepository.UpdateAdmission(adm);
                    //}

                }
                catch (Exception e)
                {

                }


                //var admissionStudentContact=EntityStatus.ACTIVE.
                //    GetByIdAsync(admission.ID).Result;
               // prvModel.ParnetName = admissionStudentContact.FullName;
                prvModel.Mobile = admission.MobileNumber;

                var admStudent = admissionRepo.
              GetAdmissionStudentById(admission.ID);

                prvModel.Name = admStudent.FirstName + " " + 
                    admStudent.LastName;
               

                prvModel.UniqueTxnID = payment.TransactionId;
                prvModel.Amount = data.TxnAmount;
                prvModel.LeadReferenceId = admission.LeadReferenceId.ToString();
                prvModel.Status = PaymentViewStatus.SUCCESS;
                prvModel.ErrorDescription = "Transaction sucessfull";
                return prvModel;

                #endregion
            }
            else
            {
                prvModel.Status = PaymentViewStatus.INVALID;
                prvModel.ErrorDescription = TechnicalError.PG_DATA_FAILED;
                return prvModel;
            }

          
        }
       

        public void SendReceiptCard(long admissionId,DateTime date,
            string amount,string mode,string transid)
        {
           

            var admission = admissionRepo.GetAdmissionById(admissionId);
            var admStudent = admissionRepo.
                GetAdmissionStudentById(admission.ID);

            var admStudentContact = admissionStudentContactRepository.ListAllAsync().Result.Where(a => a.AdmissionStudentId
          == admStudent.ID && a.ConatctType == "official").FirstOrDefault();

            string name = admStudent.FirstName + " ";
            string mname = admStudent.MiddleName != null ? admStudent.MiddleName + " " : "";
            name = name + mname + admStudent.LastName;



            //AdmitCardGenerator card = new AdmitCardGenerator();
            //var webPath = hostingEnv.WebRootPath;
            //var fileName = System.IO.Path.GetFileName("AdmissionFeeInvoice_" +
            //    admission.ID + ".pdf");
            //string path = System.IO.Path.
            //    Combine("", webPath + @"\PaymentReceipt\" + fileName);

            //AdmitCardGenerator card = new AdmitCardGenerator();
            //var webPath = hostingEnv.WebRootPath;
            //var fileName = System.IO.Path.GetFileName("AdmissionFeeInvoice_" +
            //    admission.FormNumber + ".pdf");
            //string path = System.IO.Path.
            //    Combine("", webPath + @"\PaymentReceipt\" + fileName);




            
            var admData = admissionStudentCommonMethod.GetStudentIdData(
             admStudent.ID);
            var sessionval = sessionRepo.
               GetByIdAsync(admission.AcademicSessionId).
               Result.DisplayName;
            var org = organizationRepo.GetOrganizationById(admData.
               organizationId);
            var standard = standardRepo.GetStandardById(admData.
             standardId);
            var wing = wingRepo.GetByIdAsync(admData.
             wingId).Result;

            var board = boardRepo.GetBoardById(admData.
                boardId);
            //card.SaveReceiptCard(GetAdmitCardModel(admission.
            //    LeadReferenceId.ToString(), path,
            //    org.Name,board.Name,standard.Name,wing.Name, sessionval));
            try
            {
                System.Uri address = new System.Uri("http://tinyurl.com/api-create.php?url=" +
              "http://tracq.odmps.org/Admission//Student/DownloadReceipt/"+admission.LeadReferenceId);
                System.Net.WebClient client = new System.Net.WebClient();
                string tinyUrl = client.DownloadString(address);

                Thread t1 = new Thread(delegate ()
                {
                    smssend.SendPaymentSms(
                      name, admission.MobileNumber,
                     amount, date.ToString("dd-MMM-yyyy hh:mm tt"), tinyUrl);


                });
                t1.Start();

                sendInBlueEmail.SendPaymentMail(
                    sessionval, org.Name, board.Name,standard.Name,
                    wing.Name, name, admission.MobileNumber,
                    transid,mode, date.ToString("dd-MMM-yyyy hh:mm tt"), amount,tinyUrl,
                    admStudentContact.EmailId, admStudentContact.EmailId, admStudentContact.FullName
                    );


                
                

            }
            catch (Exception e)
            {

            }
        }

      //  public AdmitCardModel GetAdmitCardModel(string admissionId, 
      //      string path,string orgName,string boardName,string standardName,
      //      string wingName,string sessionval)
      //  {
      //      var admission = admissionRepo.
      //          GetAdmissionByLeadReferenceId(admissionId);
           
      //      var admStudent = admissionRepo.
      //          GetAdmissionStudentById(admission.ID);
      //      var admConctact = admissionStudentContactRepository.ListAllAsync().Result.Where(a => a.AdmissionStudentId
      //== admStudent.ID && a.ConatctType == "official").FirstOrDefault();



      //      int startIndex = admission.MobileNumber.Length - 4;

      //      //  int endIndex = admission.MobileNumber.Length-1;

      //      //string fname = admStudent.FirstName.
      //      //    Substring(
      //      //        0, 3
      //      //        );
      //      //string mobile = admission.MobileNumber.Substring(
      //      //       startIndex, 4
      //      //         );

      //      AdmitCardModel model = new AdmitCardModel();
      //      model.pdfFilePath = path;
      //      //model.username = Encoding.ASCII.
      //      //    GetBytes(fname.ToUpper() + "" + mobile);

         

      //      AdmitCardHeaderModel hmodel = new AdmitCardHeaderModel();

      //      var webPath = hostingEnv.WebRootPath;
      //      // var fileName = System.IO.Path.GetFileName("AdmitCard_" + admStudentContact.Mobile + ".pdf");
      //      string logoPath = System.IO.Path.Combine("", webPath + @"/images/odm_logo.png");

      //      hmodel.logoPath = logoPath;
      //      // hmodel.studentPath = studentPath;
      //      hmodel.organization = "ODM PUBLIC SCHOOL";
      //      hmodel.address = "Sishu Vihar,Infocity Road,\nPatia,Bhubaneswar - 751024";

      //      hmodel.tollfreeNo = " Phone Number - 1800-120-2316";
      //      hmodel.session = "Admission for the Session " + sessionval;
      //      model.headerModel = hmodel;


      //      #region AdmissionDetails

      //      AdmitCardStudentModel smodel = new AdmitCardStudentModel();
      //      smodel.title = "Registration Details";
      //      smodel.data = new List<List<string>>();
      //      string name = UtilityModel.GetFullName(
      //          admStudent.FirstName, admStudent.MiddleName, admStudent.LastName);

      //      List<string> data1 = new List<string>();
      //      data1.Add("Application No:" + admission.FormNumber);

      //      smodel.data.Add(data1);

      //      List<string> data2 = new List<string>();
      //      data2.Add("Student Name:" + name);
      //      data2.Add("Mobile:" + admission.MobileNumber);
      //      smodel.data.Add(data2);

           

      //      List<string> data21 = new List<string>();
      //      data21.Add("Parent Name:" + admConctact.FullName);
      //      data21.Add("Email:" + admConctact.EmailId);
      //      smodel.data.Add(data21);
      //      List<string> data6 = new List<string>();
      //      data6.Add("School:" + orgName);
      //      data6.Add("Board:" + boardName);
      //      smodel.data.Add(data6);
      //      List<string> data9 = new List<string>();
      //      data9.Add("Class:" + standardName);
      //      data9.Add("Boarding Type:" + wingName);
      //      smodel.data.Add(data9);

      //      List<string> data31 = new List<string>();
      //      data31.Add("Date of Submission:" + admission.InsertedDate.ToString("dd-MMM-yyyy"));
      //      smodel.data.Add(data31);


      //      model.studentModel = smodel;

      //      #endregion

      //      #region FormDetails

      //      var form = admissionFormPaymentRepository.
      //          GetByAdmissionId(admission.ID).Result;

      //      var fee = admissionFeeRepo.
      //          GetByIdAsync(form.AdmissionFeeId).Result;


      //      var mode = ipaymentType.
      //          GetPaymentTypeById(form.PaymentModeId);

      //      AdmitCardStudentModel fmodel = new AdmitCardStudentModel();
      //      fmodel.title = "Payment Details";
      //      fmodel.data = new List<List<string>>();

      //      List<string> data11 = new List<string>();
      //      data11.Add("Transaction Id:" + form.TransactionId);
      //      data11.Add("Payment Mode:" + mode.Name);
      //      fmodel.data.Add(data11);

      //      List<string> data111 = new List<string>();
      //      data111.Add("Amount (INR):" + fee.Ammount);
      //      data111.Add("Paid Date:" + form.PaidDate.ToString("dd-MMM-yyyy"));
      //      fmodel.data.Add(data111);


      //      model.formModel = fmodel;

      //      #endregion

      //      return model;


      //  }
  
       

        public PaymentResponseViewModel ProcessPaymentStatus(string msg)
        {
            PaymentResponseViewModel prvModel = new PaymentResponseViewModel();

            BillDeskResponseModel model = SplitResponseString(msg);
            if(model!=null)
            {
                string responseWithOutChecksum = null;
                try
                {
                     responseWithOutChecksum =
                          msg.Substring(0, msg.LastIndexOf("|"));
                }
                catch(Exception e)
                {
                    prvModel.Status = PaymentViewStatus.SERVER_ERROR;
                    prvModel.ErrorDescription = TechnicalError.SPLIT_FAILED;
                }
                


                var masterData = pgRepo.GetAllPaymentGateway().
                  Where(a => a.Active == true).FirstOrDefault();

                if (masterData != null)
                {



                    string genCheckSumForResponse =
                        new HashGenerator().
                        GetHMACSHA256(responseWithOutChecksum,
                        masterData.CheckSum);

                    if (genCheckSumForResponse != null &&
                        responseWithOutChecksum != null &&
                        genCheckSumForResponse.Equals(model.CheckSum))
                    {
                        //this is LeadReferenceId
                        prvModel.LeadReferenceId = model.AdditionalInfo4;
                        UpdatePaymentStatus(model, prvModel);
                    }
                    else
                    {
                        prvModel.ErrorDescription = TechnicalError.RESPONSE_CHECKSUM_MISMATCH;
                        prvModel.Status = PaymentViewStatus.SERVER_ERROR;
                    }

                }
                else
                {
                    prvModel.ErrorDescription = TechnicalError.PG_DATA_FAILED;
                    prvModel.Status = PaymentViewStatus.SERVER_ERROR;
                }

            }
            else
            {
                prvModel.ErrorDescription = TechnicalError.SPLIT_FAILED;
                prvModel.Status = PaymentViewStatus.SERVER_ERROR;
            }

            return prvModel;

        }
        private int SavePaymentSnapShot(
           Guid paymentSnapshotId,
           long admissionId, string name
           , string reqMsg, string reqCheckSum,string uniqueTxnId, string amount,long feeId)
        {
            DateTime dt = DateTime.Now;
           


            PaymentSnapshot snapShot = new PaymentSnapshot();
            snapShot.UniqueTxnId = uniqueTxnId;
            snapShot.PaymentSnapshotId = paymentSnapshotId;
            snapShot.PaymentStatus = PaymentSnapshotStatus.STATUS_INITIATED;
            snapShot.RelatedId = admissionId;
            snapShot.RelatedName = name;
            snapShot.RequestMsg = reqMsg;
            snapShot.RequestChecksum = reqCheckSum;
            snapShot.TxnAmount = amount;
            snapShot.InsertedDate = DateTime.Now;
            snapShot.ModifiedDate = DateTime.Now;
            snapShot.Active = true;
            snapShot.Status = EntityStatus.ACTIVE;
            snapShot.InsertedId = 0;
            snapShot.IsAvailable = true;
            snapShot.ModifiedId = 0;

           var data= pgRepo.CreatePaymentSnapshot(snapShot);



            return 0;


        }
        private PaymentResponseViewModel
            UpdatePaymentStatus(BillDeskResponseModel model,
            PaymentResponseViewModel prVModel)
        {
            var paymentSnapshot =
                pgRepo.GetPaymentSnapshotBySnapshotId(
                    model.AdditionalInfo1);

            if(paymentSnapshot!=null)
            {
                prVModel.UniqueTxnID = model.UniqueTxnID;
                prVModel.Amount = model.TxnAmount;
                prVModel.TxnDate = model.TxnDate;
               

                paymentSnapshot.ResponseChecksum = model.CheckSum;
                paymentSnapshot.ResponseMsg = model.Msg;
                paymentSnapshot.TxnType = model.TxnType;
                paymentSnapshot.TxnReferenceNo = model.TxnReferenceNo;

                paymentSnapshot.AuthStatus = model.AuthStatus;
                // paymentSnapshot.TxnAmount = model.TxnAmount;

                try
                {
                    paymentSnapshot.TxnDate = DateTime.
                        ParseExact(model.TxnDate, "dd-MM-yyyy HH:mm:ss", null);
                }
                catch (Exception e)
                {
                    prVModel.ErrorDescription = TechnicalError.TRANSACTION_DATE_INVALID;
                    prVModel.Status = PaymentViewStatus.SERVER_ERROR;
                }

                #region FormPayment
                var admission = admissionRepo.GetAdmissionByLeadReferenceId(model.AdditionalInfo4);
                var fee = admissionFeeRepo.ListAllAsync().
               Result.Where(a => a.AcademicSessionId
               == admission.AcademicSessionId
               && a.Name == "online").FirstOrDefault();
              
                var paymentType = ipaymentType.
                     GetPaymentTypeByBillDeskCode(model.TxnType);
                


                

                AdmissionFormPayment payment = new AdmissionFormPayment();
                payment.AdmissionId = admission.ID;
                
                payment.PaymentModeId = paymentType.ID;
                payment.TransactionId = model.TxnReferenceNo;
                payment.AdmissionFeeId = fee.ID;
                payment.PaidDate = paymentSnapshot.TxnDate.GetValueOrDefault();
                payment.ReceivedId = 0;
                payment.Active = true;
                payment.IsAvailable = true;
                payment.Status = EntityStatus.ACTIVE;
                payment.InsertedDate = DateTime.Now;
                payment.ModifiedDate = DateTime.Now;
                payment.InsertedId = 0;
                payment.ModifiedId = 0;
                payment.PaymentGatewayProxyId = model.AdditionalInfo1.ToString();
                admissionRepo.CreateAdmissionFormPayment(payment);

                #endregion




                if(model.AuthStatus==BillDeskAuthStatus.Auth_300)
                {
                    admission.FormNumber = UtilityModel.GenerateFormNumber(DateTime.Now);
                    admission.ModifiedDate = DateTime.Now;
                    admissionRepo.UpdateAdmission(admission);

                    paymentSnapshot.PaymentStatus = PaymentSnapshotStatus.STATUS_SUCCESS;
                }
               
                else
                     if (model.AuthStatus == BillDeskAuthStatus.Auth_0002)
                {
                    prVModel.Status = PaymentViewStatus.PENDING;
                    paymentSnapshot.PaymentStatus = PaymentSnapshotStatus.STATUS_PENDING;
                }
               
                    else
                {
                    prVModel.ErrorDescription = model.ErrorDescription;
                    prVModel.Status = PaymentViewStatus.FAILURE;
                    paymentSnapshot.PaymentStatus = PaymentSnapshotStatus.STATUS_FAILURE;
                }
               pgRepo.UpdatePaymentSnapshot(paymentSnapshot);

                prVModel.Amount = fee.Ammount+"";


            }
            else
            {
                prVModel.ErrorDescription = TechnicalError.TRANSACTION_SNAPSHOT_NOT_FOUND;
                prVModel.Status = PaymentViewStatus.SERVER_ERROR;
            }
            return prVModel;

        }    

    }

   

    public static class PaymentSnapshotStatus
    {

        public static string STATUS_PENDING = "PENDING";
        public static string STATUS_SUCCESS = "SUCCESS";
        public static string STATUS_FAILURE = "FAILURE";
        public static string STATUS_INITIATED = "INITIATED";

       

    }

    public static class PaymentViewStatus
    {

        public static string PENDING = "Pending";
        public static string SUCCESS = "Suceess";
        public static string FAILURE = "Failure";
        public static string SERVER_ERROR = "Server Error";
        public static string INITIATED = "Processing";
        public static string INVALID = "Invalid";


    }

    public static class TechnicalError
    {

        public static string SPLIT_FAILED = 
            "Sorry, Technical error occured. Response parse failed.";
        public static string CHECKSUM_SEPARATION_FAILED =
            "Sorry, Technical error occured. Key separation failed.";

        public static string PG_DATA_FAILED =
           "Sorry, Technical error occured. Data not available.";

        public static string RESPONSE_CHECKSUM_MISMATCH =
        "Sorry, Technical error occured. Response invalid.";

        public static string TRANSACTION_SNAPSHOT_NOT_FOUND =
       "Sorry, Technical error occured. Transaction snapshot not found.";

        public static string TRANSACTION_DATE_INVALID =
      "Sorry, Technical error occured. Invalid transaction date .";

    }

    public static class PaymentRelatedName
    {

        public static string RELATED_TO_ADMISSION = "RELATED_TO_ADMISSION";
        public static string RELATED_TO_STUDENT_PAYMENT = "RELATED_TO_STUDENT_PAYMENT";

    }
    public static class BillDeskAuthStatus
    {

        public static string Auth_300 = "0300";
        public static string Auth_399 = "0399";
        public static string Auth_NA = "NA";
        public static string Auth_0002 = "0002";
        public static string Auth_0001 = "0001";

    }
    public class BillDeskResponseModel
    {

        public string MerchantID { get; set; }
        public string UniqueTxnID { get; set; }
        public string TxnReferenceNo { get; set; }
        public string BankReferenceNo { get; set; }
        public string TxnAmount { get; set; }

        public string BankID { get; set; }
        public string BIN { get; set; }

        public string TxnType { get; set; }
        public string CurrencyName { get; set; }
        public string ItemCode { get; set; }
        public string SecurityType { get; set; }
        public string SecurityID { get; set; }
        public string SecurityPassword { get; set; }
        public string TxnDate { get; set; }
        public string AuthStatus { get; set; }
        public string SettlementType { get; set; }
        public string AdditionalInfo1 { get; set; }
        public string AdditionalInfo2 { get; set; }

        public string AdditionalInfo3 { get; set; }
        public string AdditionalInfo4 { get; set; }
        public string AdditionalInfo5 { get; set; }
        public string AdditionalInfo6 { get; set; }
        public string AdditionalInfo7 { get; set; }
        public string ErrorStatus { get; set; }
        public string ErrorDescription { get; set; }
        public string CheckSum { get; set; }

        public string Msg { get; set; }

        
    }
}
