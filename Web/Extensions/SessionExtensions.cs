﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Extensions
{
    public class SessionExtensions
    {
        public readonly IHttpContextAccessor _httpContextAccessor;
        public ISession _session => _httpContextAccessor.HttpContext.Session;

        public SessionExtensions(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public void TestSet(string key,string value)
        {
            _session.SetString(key, value);
        }

        public string TestGet(string key)
        {
            return _session.GetString(key);
        }

    }
}
