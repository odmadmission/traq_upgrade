﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Areas.SchoolBellQ.Models;
using Web.Controllers;
using OdmErp.Web.Areas.Student.Models;

namespace OdmErp.Web.Areas.SchoolBellQ.Controllers
{
    [Area("SchoolBellQ")]
    public class TimeTableController : AppController
    {
        public IAcademicSessionRepository academicSessionRepository;
        public IBoardRepository boardRepository;
        public IStreamRepository streamRepository;
        public IAcademicStudentRepository academicStudentRepository;
        public IAcademicSectionRepository academicSectionRepository;       
        public IClassTimingRepository classTimingRepository;
        public ITeacherRepository teacherRepository;
        public IEmployeeRepository employeeRepository;
        public IAcademicTimeTableRepository academicTimeTableRepository;
        public ITimeTablePeriodRepository timeTablePeriodRepository;
        public IPeriodEmployeeRepository periodEmployeeRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IOrganizationRepository organizationRepository;
        public TimeTableMethod commonMethods;
        public TimeTableSQLQuery sqlQuery;
        public StudentSqlQuery StudentsqlQuery;
        public IAcademicStandardRepository academicStandardRepository;
        public IBoardStandardRepository BoardStandardRepository;
        public IStandardRepository StandardRepository;
        public IAcademicTeacherSubjectRepository academicTeacherSubjectRepository;
        public IAcademicSubjectRepository academicSubjectRepository;
        public ISubjectRepository SubjectRepository;
        public ISectionRepository sectionRepository;
        public IStudentAggregateRepository studentAggregateRepository;
        public IAcademicChapterRepository academicChapterRepository;
        public IChapterRepository chapterRepository;
        public OdmErp.Web.Models.CommonMethods common;
        public IAcademicConceptRepository academicConceptRepository;
        public IConceptRepository conceptRepository;

        public IAcademicSyllabusRepository academicSyllabusRepository;
        public ISyallbusRepository syallbusRepository;

        public IPeriodEmployeeStatusRepository periodEmployeeStatusRepository;
        public IPeriodSyllabusRepository periodSyllabusRepository;
        public TimeTableController(OdmErp.Web.Models.CommonMethods comm,IAcademicSyllabusRepository academicSyllabusRepo, ISyallbusRepository syallbusRepo, IAcademicConceptRepository academicConceptRepo, IConceptRepository conceptRepo, IChapterRepository chapterRepo,IAcademicChapterRepository academicChapterRepo,StudentSqlQuery Studentsql,TimeTableSQLQuery sql,IPeriodSyllabusRepository periodSyllabusRepo,IPeriodEmployeeStatusRepository periodEmployeeStatusRepo,IAcademicSessionRepository academicSessionRepository,
            IBoardRepository boardRepository, IStreamRepository streamRepository,ITeacherRepository teacherRepository,
             IAcademicSectionRepository academicSectionRepository, IClassTimingRepository classTimingRepository, 
            IEmployeeRepository employeeRepository, IAcademicStudentRepository academicStudentRepository,IAcademicTimeTableRepository academicTimeTableRepository,
            ITimeTablePeriodRepository timeTablePeriodRepository,IPeriodEmployeeRepository periodEmployeeRepository,
            IOrganizationAcademicRepository organizationAcademicRepository, IOrganizationRepository organizationRepository,TimeTableMethod commonMethods,
            IAcademicStandardRepository academicStandardRepository, IBoardStandardRepository BoardStandardRepository, IStandardRepository StandardRepository,
            IAcademicTeacherSubjectRepository academicTeacherSubjectRepository, IAcademicSubjectRepository academicSubjectRepository,ISubjectRepository SubjectRepository, 
            ISectionRepository sectionRepository, IStudentAggregateRepository studentAggregateRepository)
        {
            common = comm;
            academicSyllabusRepository = academicSyllabusRepo;
            syallbusRepository = syallbusRepo;
            academicConceptRepository = academicConceptRepo;
            conceptRepository = conceptRepo;
            chapterRepository = chapterRepo;
            academicChapterRepository = academicChapterRepo;
            StudentsqlQuery = Studentsql;
            sqlQuery = sql;
            periodEmployeeStatusRepository=periodEmployeeStatusRepo;
            periodSyllabusRepository = periodSyllabusRepo;
           this.academicSessionRepository = academicSessionRepository;
            this. boardRepository = boardRepository;
            this.streamRepository = streamRepository;  
            this.academicSectionRepository = academicSectionRepository;
            this. classTimingRepository = classTimingRepository;
            this.teacherRepository = teacherRepository;
            this.employeeRepository = employeeRepository;
            this.academicStudentRepository = academicStudentRepository;
            this.academicTimeTableRepository = academicTimeTableRepository;
            this.timeTablePeriodRepository = timeTablePeriodRepository;
            this.periodEmployeeRepository = periodEmployeeRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.organizationRepository = organizationRepository;
            this.commonMethods = commonMethods;
            this.academicStandardRepository = academicStandardRepository;
            this.BoardStandardRepository = BoardStandardRepository;
            this.StandardRepository = StandardRepository;
            this.academicTeacherSubjectRepository = academicTeacherSubjectRepository;
            this.academicSubjectRepository = academicSubjectRepository;
            this.SubjectRepository = SubjectRepository;
            this.sectionRepository = sectionRepository;
            this.studentAggregateRepository = studentAggregateRepository;
        }

        #region----------------------- Time Table----------------------
        public IActionResult StudentTimeTable(long? AcademicSectionId)
        {
            CheckLoginStatus();
            if (common.checkaccessavailable("TimeTable", accessId, "List", "TimeTable", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = " " } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true), "ID", "DisplayName"));
            ViewData["BoardName"] = new[] { new SelectListItem { Text = "Select Board", Value = " " } }.Concat(new SelectList(boardRepository.GetAllBoard(), "ID", "Name"));
            if(AcademicSectionId!=null)
            {
                var ob = sqlQuery.Get_View_All_Academic_Section().Where(a => a.ID == AcademicSectionId).FirstOrDefault();
                ViewBag.section = ob;
            }
            AcademicStudentcls academicStudentcls = new AcademicStudentcls();
            academicStudentcls.academicSessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.IsAvailable == true).ToList();
            academicStudentcls.boards = boardRepository.GetAllBoard();
            return View(academicStudentcls);
        }
        public IActionResult TeacherTimeTable(long? AcademicSectionId)
        {
            CheckLoginStatus();
            if (common.checkaccessavailable("Teacher TimeTable", accessId, "List", "TimeTable", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var res = sqlQuery.Get_View_All_Subject_By_TeacherId().Where(a => a.EmployeeId == userId).ToList();
            if (AcademicSectionId != null)
            {
                var ob = sqlQuery.Get_View_All_Academic_Section().Where(a => a.ID == AcademicSectionId).FirstOrDefault();
                ViewBag.section = ob;
            }
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = " " } }.Concat(new SelectList(res.Select(a => new{ ID = a.AcademicSessionId,DisplayName =a.SessionName}).Distinct().ToList(), "ID", "DisplayName"));
            ViewData["BoardName"] = new[] { new SelectListItem { Text = "Select Board", Value = " " } }.Concat(new SelectList(res.Select(a=>new { ID=a.BoardId,Name=a.BoardName}).Distinct().ToList(), "ID", "Name"));

          
            return View();
        }
        public async Task<JsonResult> GetOrganisationByAcademicSession(long AcademicSessionId)                 //Filter the selected Standard
        {
            try
            {
                CheckLoginStatus();
                var orgs = sqlQuery.Get_View_All_Subject_By_TeacherId().Where(a => a.EmployeeId == userId && a.AcademicSessionId== AcademicSessionId).ToList();

             
                var res = (from a in orgs
                           select new
                           {
                               id = a.OrganizationAcademicId,
                               name = a.OrganizationName
                           }).Distinct().ToList();



                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        public async Task<JsonResult> GetAcademicStandard(long BoardId, long OraganisationAcademicId, long AcademicSessionId)
        {
            CheckLoginStatus();
            var orgs = sqlQuery.Get_View_All_Subject_By_TeacherId().Where(a => a.EmployeeId == userId && a.BoardId==BoardId && a.OrganizationAcademicId== OraganisationAcademicId && a.AcademicSessionId == AcademicSessionId).ToList();
            var res = (from a in orgs
                       select new 
                       {
                           ID = a.AcademicStandardId,
                           Name = a.StandardName
                       }).Distinct().ToList();
            return Json(res);
        }
        public JsonResult GetAcademicsection(long AcademicStandardId)
        {
            CheckLoginStatus();
            var orgs = sqlQuery.Get_View_All_Subject_By_TeacherId().Where(a => a.EmployeeId == userId && a.AcademicStandardId == AcademicStandardId).ToList();
            var res = (from a in orgs
                       select new
                       {
                           ID = a.AcademicSectionId,
                           Name = a.SectionName
                       }).Distinct().ToList();
            return Json(res);
        }
        public IActionResult AcademicStudentTimeTable()
        {
            CheckLoginStatus();
            if (common.checkaccessavailable("Student TimeTable", accessId, "List", "TimeTable", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (roleId == 6)
            {
                string stdvalue = HttpContext.Session.GetString("ddlstudentid");
                userId = Convert.ToInt64(stdvalue);
            }
            var s = StudentsqlQuery.Get_view_All_Academic_Student_Sections().Where(a => a.StudentId == userId && a.IsAvailable == true).FirstOrDefault();
            view_time_table_cls table_Cls = new view_time_table_cls();
            table_Cls.AcademicSectionId = s.AcademicSectionId;
            // table_Cls.AcademicStandardId = s.AcademicStandardId;
            // table_Cls.AcademicStandardStreamId = s.AcademicStandardStreamId;
            return View(table_Cls);
        }
        public IActionResult CreateOrEditStudentTimeTable()
        {
            CheckLoginStatus();
            if (common.checkaccessavailable("TimeTable", accessId, "Create", "TimeTable", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            AcademicStudentcls academicStudentcls = new AcademicStudentcls();
            academicStudentcls.academicSessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.IsAvailable == true).ToList();
            academicStudentcls.boards = boardRepository.GetAllBoard();
            academicStudentcls.ClassTiming = classTimingRepository.ListAllAsyncIncludeAll().Result.Where(s => s.Active = true).ToList();
            var res = (from a in teacherRepository.ListAllAsyncIncludeAll().Result
                       join b in employeeRepository.GetAllEmployee() on a.EmployeeId equals b.ID
                       select new
                       {
                           id = a.ID,
                           name = b.FirstName
                       }
                       ).ToList();

            ViewBag.TeacherNameList = new[] { new SelectListItem { Text = "Select Teacher", Value = " " } }.Concat(new SelectList(res, "id", "name"));

            //if(id.Value!=0 && id!=null)
            // {
            //     var academictimetable = academicTimeTableRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID == id).FirstOrDefault();
            //     var timetableperiod = timeTablePeriodRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcdemicTimeTableId == id.Value).FirstOrDefault();
            //     var periodemployee = periodEmployeeRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.TimeTablePeriodId == timetableperiod.ID).FirstOrDefault();
            //     academicStudentcls.date = academictimetable.AssignDate;
            //     academicStudentcls.AcademicStandardId = academictimetable.AcademicStandardId;
            //     academicStudentcls.AcademicStandardStreamId = academictimetable.AcademicStandardStreamId.Value;
            //     academicStudentcls.ddlclasstiming = timetableperiod.ClassTimingId;
            //     academicStudentcls.ddlsubjects = periodemployee.SubjectId;
            //     academicStudentcls.ddlteacher = periodemployee.EmployeeId;
            //     academicStudentcls.ID = academictimetable.ID;
            // }
            // else
            // {
            //     academicStudentcls.ID = 0;
            // }   
            return View(academicStudentcls);
        }

        public IActionResult ViewTimeTable(long AcademicSectionId, long AcademicStandardId, DateTime date)
        {
            CheckLoginStatus();
            if (common.checkaccessavailable("TimeTable", accessId, "View", "TimeTable", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            view_time_table_cls table_Cls = new view_time_table_cls();
            table_Cls.AcademicSectionId = AcademicSectionId;
            table_Cls.AcademicStandardId = AcademicStandardId;
            table_Cls.date = date;
            table_Cls._Academic_Standard_With_Stream = sqlQuery.Get_View_All_Academic_Section().Where(a => a.ID == AcademicSectionId).FirstOrDefault();
            var timetableperiods = sqlQuery.Get_View_All_TimeTable_Subjects().Where(a => a.AcademicSectionId == AcademicSectionId && a.AcademicStandardId == AcademicStandardId && a.AssignDate.Date == date.Date).ToList();
            table_Cls.timetable = timetableperiods;
          //  table_Cls.teachers = sqlQuery.Get_View_All_Teachers().Where(a => a.OrganizationId == table_Cls._Academic_Standard_With_Stream.OrganizationId && a.GroupId == table_Cls._Academic_Standard_With_Stream.GroupID && a.Teacherstatus=="A" && a.Isleft==false).ToList();
            table_Cls.subjects = sqlQuery.Get_View_All_Academic_Subject().Where(a => a.BoardStandardId == table_Cls._Academic_Standard_With_Stream.BoardStandardId).ToList();
            var classtiming = classTimingRepository.ListAllAsyncIncludeAllNoTrack().Result.Where(a=>a.Active==true && !timetableperiods.Select(m=>m.ClassTimingId).ToList().Contains(a.ID)).ToList();
            table_Cls.timingcls = (from a in classtiming
                                   select new ClassTimingcls
                                   {
                                       EndTime = a.EndTime,
                                       ID = a.ID,
                                       IsAvailable = a.IsAvailable,
                                       ModifiedDate = a.ModifiedDate,
                                       Name = a.Name,
                                       StartTime = a.StartTime,
                                       Active = a.Active
                                   }).ToList();
            return View(table_Cls);
        }
        public async Task<JsonResult> GetTeachers(long academicsectionid, long academicsubjectid)
        {
            var res = sqlQuery.Get_View_All_Teachers().Where(a => a.AcademicSectionId==academicsectionid && a.AcademicSubjectId==academicsubjectid && a.Teacherstatus=="A" && a.TeacherSubjectstatus=="A" && a.Isleft==false).ToList();
            var rest = (from a in res
                        select new {
                        id=a.ID,
                        name=a.Teachername+"-"+a.EmpCode
                        }).ToList();
            return Json(rest);
        }
        public IActionResult ViewTeacherTimeTable(long AcademicSectionId, long AcademicStandardId, DateTime date)
        {
            CheckLoginStatus();
            if (common.checkaccessavailable("Teacher TimeTable", accessId, "View", "TimeTable", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            view_time_table_cls table_Cls = new view_time_table_cls();
           

            table_Cls.AcademicSectionId = AcademicSectionId;
            table_Cls.AcademicStandardId = AcademicStandardId;
            table_Cls.date = date;
            table_Cls._Academic_Standard_With_Stream = sqlQuery.Get_View_All_Academic_Section().Where(a => a.ID == AcademicSectionId).FirstOrDefault();
            var timetableperiods = sqlQuery.Get_View_All_TimeTable_Subjects().Where(a => a.AcademicSectionId == AcademicSectionId && a.AcademicStandardId == AcademicStandardId && a.AssignDate.Date == date.Date).ToList();
            table_Cls.timetable = timetableperiods.Where(a=>a.EmployeeId==userId).ToList();
            table_Cls.teachers = sqlQuery.Get_View_All_Teachers().Where(a => a.AcademicSectionId == AcademicSectionId && a.Teacherstatus == "A" && a.TeacherSubjectstatus == "A" && a.Isleft == false).ToList();

            //  table_Cls.teachers = sqlQuery.Get_View_All_Teachers().Where(a => a.OrganizationId == table_Cls._Academic_Standard_With_Stream.OrganizationId && a.GroupId == table_Cls._Academic_Standard_With_Stream.GroupID && a.Teacherstatus=="A" && a.Isleft==false).ToList();
            //   table_Cls.subjects = sqlQuery.Get_View_All_Academic_Subject().Where(a => a.BoardStandardId == table_Cls._Academic_Standard_With_Stream.BoardStandardId).ToList();
            //  var classtiming = classTimingRepository.ListAllAsyncIncludeAllNoTrack().Result.Where(a => a.Active == true && !timetableperiods.Select(m => m.ClassTimingId).ToList().Contains(a.ID)).ToList();





            return View(table_Cls);
        }

        public IActionResult ViewAcademicStudentTimeTable(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {
            CheckLoginStatus();
            if (common.checkaccessavailable("Student TimeTable", accessId, "View", "TimeTable", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (roleId == 6)
            {
                string stdvalue = HttpContext.Session.GetString("ddlstudentid");
                userId = Convert.ToInt64(stdvalue);
            }

            view_time_table_cls table_Cls = new view_time_table_cls();
            table_Cls.AcademicSectionId = AcademicSectionId;
            table_Cls.AcademicStandardId = AcademicStandardId;
            table_Cls.AcademicStandardStreamId = AcademicStandardStreamId;
            table_Cls.date = date;
            AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
            table_Cls._Academic_Standard_With_Stream = sqlQuery.Get_View_All_Academic_Section().Where(a => a.ID == AcademicSectionId).FirstOrDefault();
            var timetableperiods = sqlQuery.Get_View_All_TimeTable_Subjects().Where(a => a.AcademicSectionId == AcademicSectionId && a.AcademicStandardId == AcademicStandardId && a.AssignDate.Date == date.Date).ToList();
            table_Cls.timetable = timetableperiods;
            var academicstudent = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicStandardId == AcademicStandardId && a.StudentId == userId).ToList();
            return View(table_Cls);
        }



        public async Task<IActionResult> SaveOrUpdateTimeTable(long? timetableid, long AcademicSectionId, long AcademicStandardId, DateTime date, long ddlclasstiming, long ddlsubjects, long ddlteacher)
        {
            CheckLoginStatus();

            //AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
            var timetableperiods = academicTimeTableRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.AcademicSectionId == AcademicSectionId && a.AcademicStandardId == AcademicStandardId && a.AssignDate.Date == date.Date).ToList();
            if (timetableid == 0 || timetableid == null)
            {
                if (timetableperiods.Count > 0)
                {
                    TimeTablePeriod timeTablePeriod = timeTablePeriodRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ClassTimingId == ddlclasstiming && a.AcdemicTimeTableId == timetableperiods.ToList()[0].ID).FirstOrDefault();
                    if (timeTablePeriod == null)
                    {
                        timeTablePeriod = new TimeTablePeriod();
                        timeTablePeriod.AcdemicTimeTableId = timetableperiods.ToList()[0].ID;
                        timeTablePeriod.Active = true;
                        timeTablePeriod.ClassTimingId = ddlclasstiming;
                        timeTablePeriod.Code = "";
                        timeTablePeriod.InsertedDate = DateTime.Now;
                        timeTablePeriod.ModifiedDate = DateTime.Now;
                        timeTablePeriod.InsertedId = userId;
                        timeTablePeriod.ModifiedId = userId;
                        timeTablePeriod.IsAvailable = true;
                        timeTablePeriod.NameOfDay = date.ToString("dddd");
                        timeTablePeriod.Status = EntityStatus.ACTIVE;
                        var res = timeTablePeriodRepository.AddAsync(timeTablePeriod);
                        PeriodEmployee periodEmployee = new PeriodEmployee();
                        periodEmployee.Active = true;
                        periodEmployee.EmployeeId = ddlteacher;
                        periodEmployee.InsertedDate = DateTime.Now;
                        periodEmployee.InsertedId = userId;
                        periodEmployee.IsAvailable = true;
                        periodEmployee.IsForwared = false;
                        periodEmployee.ModifiedDate = DateTime.Now;
                        periodEmployee.ModifiedId = userId;
                        periodEmployee.ParentPeriodEmployeeId = 0;
                        periodEmployee.Status = EntityStatus.ACTIVE;
                        periodEmployee.SubjectId = ddlsubjects;
                        periodEmployee.TimeTablePeriodId = res.Result.ID;
                        await periodEmployeeRepository.AddAsync(periodEmployee);
                        return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") });

                    }
                    else
                    {
                        return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") });
                    }
                }
                else
                {
                    AcademicTimeTable academicTimeTable = new AcademicTimeTable();
                    academicTimeTable.AcademicSectionId = AcademicSectionId;
                    academicTimeTable.AcademicStandardId = AcademicStandardId;
                    //academicTimeTable.AcademicStandardStreamId = AcademicStandardStreamId == null ? 0 : AcademicStandardStreamId;
                    academicTimeTable.Active = true;
                    academicTimeTable.AssignDate = date;
                    var academictt = academicTimeTableRepository.ListAllAsyncIncludeAll().Result.ToList().LastOrDefault();
                    if (academictt == null)
                    {
                        academicTimeTable.Code = "ODM-TT-01";
                    }
                    else
                    {
                        academicTimeTable.Code = "ODM-TT-0" + (academictt.ID + 1);
                    }

                    academicTimeTable.NameOfMonth = date.ToString("MMMM");
                    academicTimeTable.InsertedDate = DateTime.Now;
                    academicTimeTable.ModifiedDate = DateTime.Now;
                    academicTimeTable.InsertedId = userId;
                    academicTimeTable.ModifiedId = userId;
                    academicTimeTable.IsAvailable = true;
                    academicTimeTable.Status = EntityStatus.ACTIVE;
                    var acd = academicTimeTableRepository.AddAsync(academicTimeTable).Result;
                    TimeTablePeriod timeTablePd = new TimeTablePeriod();
                    timeTablePd.AcdemicTimeTableId = acd.ID;
                    timeTablePd.Active = true;
                    timeTablePd.ClassTimingId = ddlclasstiming;
                    timeTablePd.Code = acd.Code;
                    timeTablePd.InsertedDate = DateTime.Now;
                    timeTablePd.ModifiedDate = DateTime.Now;
                    timeTablePd.InsertedId = userId;
                    timeTablePd.ModifiedId = userId;
                    timeTablePd.IsAvailable = true;
                    timeTablePd.NameOfDay = date.ToString("dddd");
                    timeTablePd.Status = EntityStatus.ACTIVE;
                    var res = timeTablePeriodRepository.AddAsync(timeTablePd).Result;
                    PeriodEmployee periodEmployee = new PeriodEmployee();
                    periodEmployee.Active = true;
                    periodEmployee.EmployeeId = ddlteacher;
                    periodEmployee.InsertedDate = DateTime.Now;
                    periodEmployee.InsertedId = userId;
                    periodEmployee.IsAvailable = true;
                    periodEmployee.IsForwared = false;
                    periodEmployee.ModifiedDate = DateTime.Now;
                    periodEmployee.ModifiedId = userId;
                    periodEmployee.ParentPeriodEmployeeId = 0;
                    periodEmployee.Status = EntityStatus.ACTIVE;
                    periodEmployee.SubjectId = ddlsubjects;
                    periodEmployee.TimeTablePeriodId = res.ID;
                    await periodEmployeeRepository.AddAsync(periodEmployee);
                    return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") });
                }
            }
            else
            {
                TimeTablePeriod timeTablePeriod = timeTablePeriodRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ClassTimingId == ddlclasstiming && a.AcdemicTimeTableId == timetableperiods.ToList()[0].ID && a.ID != timetableid).FirstOrDefault();
                if (timeTablePeriod == null)
                {
                    timeTablePeriod = timeTablePeriodRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID != timetableid).FirstOrDefault();
                    timeTablePeriod.ClassTimingId = ddlclasstiming;
                    timeTablePeriod.ModifiedDate = DateTime.Now;
                    timeTablePeriod.ModifiedId = userId;
                    var res = timeTablePeriodRepository.UpdateAsync(timeTablePeriod);
                    PeriodEmployee periodEmployee = periodEmployeeRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.TimeTablePeriodId == timetableid).FirstOrDefault();

                    periodEmployee.EmployeeId = ddlteacher;
                    periodEmployee.ModifiedDate = DateTime.Now;
                    periodEmployee.ModifiedId = userId;
                    periodEmployee.SubjectId = ddlsubjects;
                    await periodEmployeeRepository.UpdateAsync(periodEmployee);
                    return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") });

                }
                else
                {
                    return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") });
                }
            }
        }
        public async Task<IActionResult> ForwardSubject(long? timetableid, long ddlteacher, long AcademicSectionId, long AcademicStandardId, DateTime date)
        {
            CheckLoginStatus();
            

            PeriodEmployee periodEmployee = periodEmployeeRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID == timetableid).FirstOrDefault();
            if (periodEmployee == null)
            {
                periodEmployee.Active = false;
                periodEmployee.ModifiedDate = DateTime.Now;
                periodEmployee.ModifiedId = userId;
                await periodEmployeeRepository.UpdateAsync(periodEmployee);

                PeriodEmployee periodEmp = new PeriodEmployee();
                periodEmp.Active = true;
                periodEmp.EmployeeId = ddlteacher;
                periodEmp.InsertedDate = DateTime.Now;
                periodEmp.InsertedId = userId;
                periodEmp.IsAvailable = true;
                periodEmp.IsForwared = true;
                periodEmp.ModifiedDate = DateTime.Now;
                periodEmp.ModifiedId = userId;
                periodEmp.ParentPeriodEmployeeId = periodEmployee.EmployeeId;
                periodEmp.Status = EntityStatus.ACTIVE; ;
                periodEmp.SubjectId = periodEmployee.SubjectId;
                periodEmp.TimeTablePeriodId = periodEmployee.ID;
                await periodEmployeeRepository.AddAsync(periodEmp);
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") });

            }
            else
            {
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, date = date.ToString("yyyy-MM-dd") });
            }



        }


        public JsonResult Getchapters(long subjectId, long academicsessionId)
        {
           // var Academicsubject = academicSubjectRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == subjectId && a.AcademicSessionId==academicsessionId && a.Active == true).FirstOrDefault();
            var Academicchapter = academicChapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSubjectId == subjectId && a.Active == true).ToList();
            var chapters = chapterRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
            var res = (from a in Academicchapter
                       join b in chapters on a.ChapterId equals b.ID
                       select new
                       {
                           a.ID,
                           a.AcademicSubjectId,
                           b.Name
                       }).ToList();

            return Json(res);

        }
        public JsonResult Getconcept(long academicchapterId)
        {
            var Academicconcept = academicConceptRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicChapterId == academicchapterId && a.Active == true).ToList();
            var concepts = conceptRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
            var res = (from a in Academicconcept
                       join b in concepts on a.ConceptId equals b.ID
                       select new
                       {
                           a.ID,
                           a.AcademicChapterId,
                           b.Name
                       }).ToList();

            return Json(res);

        }

        public JsonResult GetSyllabus(long academicconceptId)
        {
            var Academicsyllabus = academicSyllabusRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicConceptId == academicconceptId && a.Active == true).ToList();
            var syllabus = syallbusRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
            var res = (from a in Academicsyllabus
                       join b in syllabus on a.SyllabusId equals b.ID
                       select new
                       {
                           a.ID,
                           a.AcademicConceptId,
                           b.Title
                       }).ToList();

            return Json(res);

        }

        public async Task<IActionResult> SyllabusUpdate(long periodemployeeid, long ddlchapter, long ddlconcept, long ddlsyllabus, string txtdesc, long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {
            CheckLoginStatus();
            PeriodEmployeeStatus periodEmployeeStatus = periodEmployeeStatusRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.PeriodEmployeeId == periodemployeeid && a.Active == true).FirstOrDefault();
            if (periodEmployeeStatus == null)
            {
                periodEmployeeStatus = new PeriodEmployeeStatus();
                periodEmployeeStatus.IsAvailable = true;
                periodEmployeeStatus.PeriodEmployeeId = periodemployeeid;
                periodEmployeeStatus.Reason = txtdesc;
                periodEmployeeStatus.TakenEmployeeId = userId;
                periodEmployeeStatus.Active = true;
                periodEmployeeStatus.InsertedDate = DateTime.Now;
                periodEmployeeStatus.InsertedId = userId;
                periodEmployeeStatus.ModifiedDate = DateTime.Now;
                periodEmployeeStatus.ModifiedId = userId;
                var res = periodEmployeeStatusRepository.AddAsync(periodEmployeeStatus);

                PeriodSyllabus periodSyllabus = new PeriodSyllabus();
                periodSyllabus.Active = true;
                periodSyllabus.AcademicChapterId = ddlchapter;
                periodSyllabus.InsertedDate = DateTime.Now;
                periodSyllabus.InsertedId = userId;
                periodSyllabus.IsAvailable = true;
                periodSyllabus.AcademicConceptId = ddlconcept;
                periodSyllabus.ModifiedDate = DateTime.Now;
                periodSyllabus.ModifiedId = userId;
                periodSyllabus.AcademicSyllabusId = ddlsyllabus;
                periodSyllabus.Status = EntityStatus.ACTIVE;
                periodSyllabus.PeriodEmployeeStatusId = res.Result.ID;
                await periodSyllabusRepository.AddAsync(periodSyllabus);
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") });

            }
            else
            {
                periodEmployeeStatus.Reason = txtdesc;
                periodEmployeeStatus.TakenEmployeeId = userId;
                periodEmployeeStatus.ModifiedDate = DateTime.Now;
                periodEmployeeStatus.ModifiedId = userId;
                await periodEmployeeStatusRepository.UpdateAsync(periodEmployeeStatus);

                PeriodSyllabus periodSyllabus = periodSyllabusRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.PeriodEmployeeStatusId == periodEmployeeStatus.ID).FirstOrDefault();
                periodSyllabus.AcademicChapterId = ddlchapter;
                periodSyllabus.AcademicConceptId = ddlconcept;
                periodSyllabus.ModifiedDate = DateTime.Now;
                periodSyllabus.ModifiedId = userId;
                periodSyllabus.AcademicSyllabusId = ddlsyllabus;
                await periodSyllabusRepository.UpdateAsync(periodSyllabus);
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") });
            }



        }
        public async Task<IActionResult> RatingUpdate(long periodemployeeid, long ddlchapter, long ddlconcept, long ddlsyllabus, string txtdesc, long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {
            CheckLoginStatus();
            PeriodEmployeeStatus periodEmployeeStatus = periodEmployeeStatusRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.PeriodEmployeeId == periodemployeeid && a.Active == true).FirstOrDefault();
            if (periodEmployeeStatus == null)
            {
                periodEmployeeStatus = new PeriodEmployeeStatus();
                periodEmployeeStatus.IsAvailable = true;
                periodEmployeeStatus.PeriodEmployeeId = periodemployeeid;
                periodEmployeeStatus.Reason = txtdesc;
                periodEmployeeStatus.TakenEmployeeId = userId;
                periodEmployeeStatus.Active = true;
                periodEmployeeStatus.InsertedDate = DateTime.Now;
                periodEmployeeStatus.InsertedId = userId;
                periodEmployeeStatus.ModifiedDate = DateTime.Now;
                periodEmployeeStatus.ModifiedId = userId;
                var res = periodEmployeeStatusRepository.AddAsync(periodEmployeeStatus);

                PeriodSyllabus periodSyllabus = new PeriodSyllabus();
                periodSyllabus.Active = true;
                periodSyllabus.AcademicChapterId = ddlchapter;
                periodSyllabus.InsertedDate = DateTime.Now;
                periodSyllabus.InsertedId = userId;
                periodSyllabus.IsAvailable = true;
                periodSyllabus.AcademicConceptId = ddlconcept;
                periodSyllabus.ModifiedDate = DateTime.Now;
                periodSyllabus.ModifiedId = userId;
                periodSyllabus.AcademicSyllabusId = ddlsyllabus;
                periodSyllabus.Status = EntityStatus.ACTIVE;
                periodSyllabus.PeriodEmployeeStatusId = res.Result.ID;
                await periodSyllabusRepository.AddAsync(periodSyllabus);
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") });

            }
            else
            {
                periodEmployeeStatus.Reason = txtdesc;
                periodEmployeeStatus.TakenEmployeeId = userId;
                periodEmployeeStatus.ModifiedDate = DateTime.Now;
                periodEmployeeStatus.ModifiedId = userId;
                await periodEmployeeStatusRepository.UpdateAsync(periodEmployeeStatus);

                PeriodSyllabus periodSyllabus = periodSyllabusRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.PeriodEmployeeStatusId == periodEmployeeStatus.ID).FirstOrDefault();
                periodSyllabus.AcademicChapterId = ddlchapter;
                periodSyllabus.AcademicConceptId = ddlconcept;
                periodSyllabus.ModifiedDate = DateTime.Now;
                periodSyllabus.ModifiedId = userId;
                periodSyllabus.AcademicSyllabusId = ddlsyllabus;
                await periodSyllabusRepository.UpdateAsync(periodSyllabus);
                return RedirectToAction(nameof(ViewTeacherTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") });
            }



        }


        public async Task<IActionResult> DeletestudentTimetable(long id, long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId, DateTime date)
        {
            if (common.checkaccessavailable("TimeTable", accessId, "Remove", "TimeTable", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            TimeTablePeriod timeTablePeriod = timeTablePeriodRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID != id).FirstOrDefault();
            timeTablePeriod.Active = false;
            timeTablePeriod.ModifiedDate = DateTime.Now;
            timeTablePeriod.ModifiedId = userId;
            var res = timeTablePeriodRepository.UpdateAsync(timeTablePeriod);
            PeriodEmployee periodEmployee = periodEmployeeRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.TimeTablePeriodId == id).FirstOrDefault();

            periodEmployee.Active = false;
            periodEmployee.ModifiedDate = DateTime.Now;
            periodEmployee.ModifiedId = userId;
            await periodEmployeeRepository.UpdateAsync(periodEmployee);
            return RedirectToAction(nameof(ViewTimeTable), new { AcademicSectionId = AcademicSectionId, AcademicStandardId = AcademicStandardId, AcademicStandardStreamId = AcademicStandardStreamId, date = date.ToString("yyyy-MM-dd") });

        }

        public JsonResult GetCalenderEngagementEventsForTimeTable(long AcademicSectionId, long AcademicStandardId)
        {
            var data = sqlQuery.GetTimeTableList(AcademicSectionId, AcademicStandardId);
            //ToString("yyyy-MM-dd'T'HH:mm:ss.fffffffZ"))
            var res = (from a in data
                       select new CalenderEventData
                       {
                           allDay = a.allDay,
                           backgroundColor = a.backgroundColor,
                           editable = a.editable,
                           end = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.end),
                           start = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.start),
                           textColor = a.textColor,
                           title = a.title
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetCalenderEngagementEventsForAcademicTimeTable(long AcademicSectionId, long AcademicStandardId)
        {
            var data = sqlQuery.GetTimeTableList(AcademicSectionId, AcademicStandardId);
            //ToString("yyyy-MM-dd'T'HH:mm:ss.fffffffZ"))
            var res = (from a in data
                       select new CalenderEventData
                       {
                           allDay = a.allDay,
                           backgroundColor = a.backgroundColor,
                           editable = a.editable,
                           end = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.end),
                           start = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.start),
                           textColor = a.textColor,
                           title = a.title
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetCalenderEngagementEventsForzteacherTimeTable(long AcademicSectionId, long AcademicStandardId)
        {
            CheckLoginStatus();
            var data = sqlQuery.GetForTeacherTimeTableList(AcademicSectionId, AcademicStandardId, userId);
            //ToString("yyyy-MM-dd'T'HH:mm:ss.fffffffZ"))
            var res = (from a in data
                       select new CalenderEventData
                       {
                           allDay = a.allDay,
                           backgroundColor = a.backgroundColor,
                           editable = a.editable,
                           end = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.end),
                           start = String.Format("{0:yyyy-MM-ddTHH:mm:ss.fffffffzzz}", a.start),
                           textColor = a.textColor,
                           title = a.title
                       }).ToList();
            return Json(res);
        }

        #endregion
    }

}
