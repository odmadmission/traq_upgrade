﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Areas.SchoolBellQ.Models;
using OdmErp.Web.Models;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Web.Controllers;



namespace OdmErp.Web.Areas.SchoolBellQ.Controllers
{

    [Area("SchoolBellQ")]
    public class NoticeController : AppController
    {
        public IAcademicSessionRepository academicSessionRepository;
        public IBoardRepository boardRepository;
        public IStreamRepository streamRepository;
        public IAcademicStudentRepository academicStudentRepository;
        public CommonSchoolModel commonSchoolModel;
        public IAcademicSectionRepository academicSectionRepository;
        public IClassTimingRepository classTimingRepository;
        public ITeacherRepository teacherRepository;
        public IEmployeeRepository employeeRepository;
        public IAcademicTimeTableRepository academicTimeTableRepository;
        public ITimeTablePeriodRepository timeTablePeriodRepository;
        public IPeriodEmployeeRepository periodEmployeeRepository;
        public commonsqlquery commonsqlquery;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IOrganizationRepository organizationRepository;
        public CommonMethods commonMethods;
        public IAcademicStandardRepository academicStandardRepository;
        public IBoardStandardRepository BoardStandardRepository;
        public IStandardRepository StandardRepository;
        public IAcademicTeacherSubjectRepository academicTeacherSubjectRepository;
        public IAcademicSubjectRepository academicSubjectRepository;
        public ISubjectRepository SubjectRepository;
        public ISectionRepository sectionRepository;
        private readonly IHostingEnvironment hostingEnvironment;
        public IUserGroupRepository userGroupRepository;
        public IUserGroupTimeLineRepository userGroupTimeLineRepository;
        public IUserGroupEmployeeRepository userGroupEmployeeRepository;
        public IUserGroupEmployeeTimeLineRepository userGroupEmployeeTimeLineRepository;
        public INoticeRepository noticeRepository;
        public INoticeDataRepository noticeDataRepository;
        public ISupportRepository supportRepository;
        public IEmployeeDesignationRepository employeeDesignationRepository;
        public IDesignationRepository designationRepository;
        public IDepartmentRepository departmentRepository;
        public IStudentAggregateRepository studentAggregateRepository;
        public smssend smssend;
        public IGroupRepository groupRepository;
        public NoticeController(IAcademicSessionRepository academicSessionRepository, 
            IBoardRepository boardRepository, IStreamRepository streamRepository, ITeacherRepository teacherRepository,
            CommonSchoolModel commonSchoolModel, IAcademicSectionRepository academicSectionRepository, IClassTimingRepository classTimingRepository,
            IEmployeeRepository employeeRepository, IAcademicStudentRepository academicStudentRepository, IAcademicTimeTableRepository academicTimeTableRepository,
            ITimeTablePeriodRepository timeTablePeriodRepository, IPeriodEmployeeRepository periodEmployeeRepository, commonsqlquery commonsqlquery,
            IOrganizationAcademicRepository organizationAcademicRepository, IOrganizationRepository organizationRepository, CommonMethods commonMethods,
            IAcademicStandardRepository academicStandardRepository, IBoardStandardRepository BoardStandardRepository, IStandardRepository StandardRepository,
            IAcademicTeacherSubjectRepository academicTeacherSubjectRepository, IAcademicSubjectRepository academicSubjectRepository, ISubjectRepository SubjectRepository,
            ISectionRepository sectionRepository, IHostingEnvironment hostingEnvironment, IUserGroupRepository userGroupRepository, IUserGroupTimeLineRepository userGroupTimeLineRepository,
            IUserGroupEmployeeRepository userGroupEmployeeRepository, IUserGroupEmployeeTimeLineRepository userGroupEmployeeTimeLineRepository, INoticeRepository noticeRepository,
            INoticeDataRepository noticeDataRepository,ISupportRepository supportRepository, IEmployeeDesignationRepository employeeDesignationRepository,
            IDesignationRepository designationRepository, IDepartmentRepository departmentRepository, IStudentAggregateRepository studentAggregateRepository,smssend smssend,IGroupRepository groupRepository)
        {
            this.academicSessionRepository = academicSessionRepository;
            this.boardRepository = boardRepository;
            this.streamRepository = streamRepository;
            this.commonSchoolModel = commonSchoolModel;
            this.academicSectionRepository = academicSectionRepository;
            this.classTimingRepository = classTimingRepository;
            this.teacherRepository = teacherRepository;
            this.employeeRepository = employeeRepository;
            this.academicStudentRepository = academicStudentRepository;
            this.academicTimeTableRepository = academicTimeTableRepository;
            this.timeTablePeriodRepository = timeTablePeriodRepository;
            this.periodEmployeeRepository = periodEmployeeRepository;
            this.commonsqlquery = commonsqlquery;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.organizationRepository = organizationRepository;
            this.commonMethods = commonMethods;
            this.academicStandardRepository = academicStandardRepository;
            this.BoardStandardRepository = BoardStandardRepository;
            this.StandardRepository = StandardRepository;
            this.academicTeacherSubjectRepository = academicTeacherSubjectRepository;
            this.academicSubjectRepository = academicSubjectRepository;
            this.SubjectRepository = SubjectRepository;
            this.sectionRepository = sectionRepository;
            this.hostingEnvironment = hostingEnvironment;
            this.userGroupRepository = userGroupRepository;
            this.userGroupTimeLineRepository = userGroupTimeLineRepository;
            this.userGroupEmployeeRepository = userGroupEmployeeRepository;
            this.userGroupEmployeeTimeLineRepository = userGroupEmployeeTimeLineRepository;
            this.noticeRepository = noticeRepository;
            this.noticeDataRepository = noticeDataRepository;
            this.supportRepository = supportRepository;
            this.employeeDesignationRepository = employeeDesignationRepository;
            this.designationRepository = designationRepository;
            this.departmentRepository = departmentRepository;
            this.studentAggregateRepository = studentAggregateRepository;
            this.smssend = smssend;
            this.groupRepository = groupRepository;
        }
        #region------------------------------Employee Group----------------------------
        [HttpGet]
        public ActionResult ViewUserGroup()
        {
            var usergroup = userGroupRepository.ListAllAsyncIncludeAll().Result;
            var usergroupemp = userGroupEmployeeRepository.ListAllAsyncIncludeAll().Result;
            var timeline = userGroupTimeLineRepository.ListAllAsyncIncludeAll().Result;
            var employee = employeeRepository.GetAllEmployee();
            ResponseUserGroupModel usergrp = new ResponseUserGroupModel();
            List<UserGroupModel> res = null;
            List<UserGroupModel> res1 = null;
            if (usergroup != null)
            {
                res = (from a in usergroup
                      //join b in timeline on a.ID equals b.UserGroupID
                       select new UserGroupModel
                       {
                           ID = a.ID,
                           stringStartDate = NullableDateTimeToStringDate(timeline.Where(x => x.UserGroupID == a.ID).FirstOrDefault().StartDate.Value),
                         // stringEndDate = NullableDateTimeToStringDate(timeline.Where(x => x.UserGroupID == a.ID).FirstOrDefault().EndDate.Value),
                           GroupName = a.GroupName,
                           Description = a.Description,
                           Imageicon = a.Icon,
                           EmployeeId = a.EmployeeId,
                           InsertedDate = a.InsertedDate,
                           ModifiedDateText = NullableDateTimeToStringDate(a.ModifiedDate),
                          // StartDate = b.StartDate,
                           //EndDate = b.EndDate,
                           Status = a.Status,
                           Active = a.Active,
                           EmployeeCount = a.ID != 0 ? usergroupemp.Where(x => x.UserGroupId == a.ID).Count() : 0,
                       }
                         ).ToList();
            }

            var usergroupempList = userGroupEmployeeRepository.ListAllAsyncIncludeAll().Result;
            var emptimeline = userGroupEmployeeTimeLineRepository.ListAllAsyncIncludeAll().Result;
            if (usergroupempList != null)
            {
                res1 = (from a in usergroupempList
                        select new UserGroupModel
                        {
                            ID = a.ID,
                            EmployeeId = a.EmployeeId,
                            InsertedDate = a.InsertedDate,
                            EmployeeCode = employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().EmpCode,
                            EmployeeName = employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().FirstName,
                            EmployeeLastName = employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().LastName,
                            ModifiedDateText = NullableDateTimeToStringDate(a.ModifiedDate),
                            StartDate = emptimeline.Where(x => x.UserGroupEmployeeID == a.ID).FirstOrDefault()==null?null: emptimeline.Where(x => x.UserGroupEmployeeID == a.ID).FirstOrDefault().StartDate,
                            Status = a.Status,
                            Active = a.Active,
                            EmployeeCount = usergroupemp.Where(x => x.UserGroupId == a.ID).Count(),
                            UserGroupID = a.UserGroupId,
                            EmployeeImage = employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().Image != null ? "/ODMImages/EmployeeProfile/" + employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().Image : null,
                        }
                    ).ToList();
            }


            usergrp.UserGrouplist = res;
            usergrp.UserGroupEmployeelist = res1;
            usergrp.employeelist = employeeRepository.GetAllEmployee().ToList();
            return View(usergrp);

        }

        [HttpPost]
        public async Task<IActionResult> SaveUserGroup(UserGroupModel usergrp)
        {
            CheckLoginStatus();

            string newFilename = " ";
            ArrayList arlist = new ArrayList(usergrp.MultiEmployeeId);

            UserGroup grp = new UserGroup();
            if (usergrp.Icon != null)
            {

                FileInfo fi = new FileInfo(usergrp.Icon.FileName);
                newFilename = String.Format("{0:d}",
                                 (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\SchoolBellQ\Notice\" + newFilename);
                var pathToSave = newFilename;
                grp.Icon = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await usergrp.Icon.CopyToAsync(stream);
                }
                grp.Icon = pathToSave;
            }
            grp.EmployeeId = userId;
            grp.GroupName = usergrp.GroupName;
            grp.Description = usergrp.Description;
            grp.Active = true;
            grp.InsertedId = userId;
            grp.IsAvailable = true;
            grp.ModifiedId = userId;
            grp.Status = EntityStatus.ACTIVE;
            grp.InsertedDate = DateTime.Now;
            grp.ModifiedDate = DateTime.Now;
            var usergroupid = userGroupRepository.AddAsync(grp);
            UserGroupTimeLine timeline = new UserGroupTimeLine();
            timeline.UserGroupID = usergroupid.Result.ID;
            timeline.StartDate = DateTime.Now;
            timeline.EndDate = null;
            timeline.Active = true;
            timeline.InsertedId = userId;
            timeline.IsAvailable = true;
            timeline.ModifiedId = userId;
            timeline.Status = EntityStatus.ACTIVE;
            timeline.InsertedDate = DateTime.Now;
            timeline.ModifiedDate = DateTime.Now;
            await userGroupTimeLineRepository.AddAsync(timeline);
            foreach (long str in arlist)
            {
                UserGroupEmployee usergrpemp = new UserGroupEmployee();
                usergrpemp.UserGroupId = usergroupid.Result.ID;
                usergrpemp.EmployeeId = str;
                usergrpemp.Active = true;
                usergrpemp.InsertedId = userId;
                usergrpemp.IsAvailable = true;
                usergrpemp.ModifiedId = userId;
                usergrpemp.Status = EntityStatus.ACTIVE;
                usergrpemp.InsertedDate = DateTime.Now;
                usergrpemp.ModifiedDate = DateTime.Now;
                var usergroupempid = userGroupEmployeeRepository.AddAsync(usergrpemp);
                UserGroupEmployeeTimeLine emptimeline = new UserGroupEmployeeTimeLine();
                emptimeline.UserGroupEmployeeID = usergroupempid.Result.ID;
                emptimeline.StartDate = DateTime.Now;
                emptimeline.EndDate = null;
                emptimeline.IsLeft = false;
                emptimeline.Active = true;
                emptimeline.InsertedId = userId;
                emptimeline.IsAvailable = true;
                emptimeline.ModifiedId = userId;
                emptimeline.Status = EntityStatus.ACTIVE;
                emptimeline.InsertedDate = DateTime.Now;
                emptimeline.ModifiedDate = DateTime.Now;
                await userGroupEmployeeTimeLineRepository.AddAsync(emptimeline);
            }
            return RedirectToAction(nameof(ViewUserGroup)).WithSuccess("Data", "Submitted Successfully");


        }


        public async Task<IActionResult> DeleteUserGroup(long id)
        {
            CheckLoginStatus();
            UserGroup grp = userGroupRepository.GetByIdAsyncIncludeAll(id).Result;
            grp.IsAvailable = false;
            grp.Active = false;
            grp.ModifiedId = userId;
            grp.Status = EntityStatus.INACTIVE;
            grp.ModifiedDate = DateTime.Now;
            await userGroupRepository.UpdateAsync(grp);
            UserGroupTimeLine timeline = userGroupTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(z => z.UserGroupID == id).FirstOrDefault();

            timeline.Active = false;
            timeline.IsAvailable = false;
            timeline.ModifiedId = userId;
            timeline.EndDate = DateTime.Now;
            timeline.Status = EntityStatus.INACTIVE;
            timeline.ModifiedDate = DateTime.Now;
            await userGroupTimeLineRepository.UpdateAsync(timeline);
            List<UserGroupEmployee> res = userGroupEmployeeRepository.ListAllAsyncIncludeAll().Result.Where(a => a.UserGroupId == id).ToList();

            foreach (UserGroupEmployee empgrp in res)
            {
                empgrp.IsAvailable = false;
                empgrp.Active = false;
                empgrp.Status = EntityStatus.INACTIVE;
                empgrp.ModifiedId = userId;
                empgrp.ModifiedDate = DateTime.Now;
                await userGroupEmployeeRepository.UpdateAsync(empgrp);
                long usergrpempid = empgrp.ID;
                UserGroupEmployeeTimeLine emptimeline = userGroupEmployeeTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(z => z.UserGroupEmployeeID == usergrpempid).FirstOrDefault();
                 if(emptimeline!=null)
                    {
                    emptimeline.Active = false;
                    emptimeline.IsAvailable = false;
                    emptimeline.Status = EntityStatus.INACTIVE;
                    emptimeline.ModifiedId = userId;
                    emptimeline.ModifiedDate = DateTime.Now;
                    await userGroupEmployeeTimeLineRepository.UpdateAsync(emptimeline);
                  }
               
            }
            return RedirectToAction(nameof(ViewUserGroup)).WithSuccess("Data", "Deactivated Successfully");

        }

        public JsonResult DeactivateAllUserGroup(long[] id)
        {
            int data = 0;
            try
            {           
            CheckLoginStatus();  
            foreach (long Allempgrpid in id)
            {
                UserGroup grp = userGroupRepository.GetByIdAsyncIncludeAll(Allempgrpid).Result;
                grp.IsAvailable = false;
                grp.Active = false;
                grp.ModifiedId = userId;
                grp.Status = EntityStatus.INACTIVE;
                grp.ModifiedDate = DateTime.Now;
                userGroupRepository.UpdateAsync(grp);
                UserGroupTimeLine timeline = userGroupTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(z => z.UserGroupID == Allempgrpid).FirstOrDefault();

                timeline.Active = false;
                timeline.IsAvailable = false;
                timeline.ModifiedId = userId;
                timeline.EndDate = DateTime.Now;
                timeline.Status = EntityStatus.INACTIVE;
                timeline.ModifiedDate = DateTime.Now;
                userGroupTimeLineRepository.UpdateAsync(timeline);
                List<UserGroupEmployee> res = userGroupEmployeeRepository.ListAllAsyncIncludeAll().Result.Where(a => a.UserGroupId == Allempgrpid).ToList();

                foreach (UserGroupEmployee empgrp in res)
                {
                    empgrp.IsAvailable = false;
                    empgrp.Active = false;
                    empgrp.Status = EntityStatus.INACTIVE;
                    empgrp.ModifiedId = userId;
                    empgrp.ModifiedDate = DateTime.Now;
                    userGroupEmployeeRepository.UpdateAsync(empgrp);
                    long usergrpempid = empgrp.ID;
                    UserGroupEmployeeTimeLine emptimeline = userGroupEmployeeTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(z => z.UserGroupEmployeeID == usergrpempid).FirstOrDefault();
                    if (emptimeline != null)
                    {
                        emptimeline.Active = false;
                        emptimeline.IsAvailable = false;
                        emptimeline.Status = EntityStatus.INACTIVE;
                        emptimeline.ModifiedId = userId;
                        emptimeline.ModifiedDate = DateTime.Now;
                        userGroupEmployeeTimeLineRepository.UpdateAsync(emptimeline);
                    }

                }
            }
                data = 1;
            }
            catch(Exception ex)
            {
                data = 0;
            }

            return Json(data);
           
           // return RedirectToAction(nameof(ViewUserGroup)).WithSuccess("Data", "Deactivated Successfully");

        }
        public JsonResult ActivateAllUserGroup(long[] id)
        {
            int data = 0;
            try
            {
                CheckLoginStatus();
                foreach (long Allempgrpid in id)
                {
                    UserGroup grp = userGroupRepository.GetByIdAsyncIncludeAll(Allempgrpid).Result;
                    grp.IsAvailable = true;
                    grp.Active = true;
                    grp.ModifiedId = userId;
                    grp.Status = EntityStatus.ACTIVE;
                    grp.ModifiedDate = DateTime.Now;
                    userGroupRepository.UpdateAsync(grp);
                    UserGroupTimeLine timeline = userGroupTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(z => z.UserGroupID == Allempgrpid).FirstOrDefault();

                    timeline.Active = true;
                    timeline.IsAvailable = true;
                    timeline.ModifiedId = userId;
                    timeline.EndDate = null;
                    timeline.Status = EntityStatus.ACTIVE;
                    timeline.ModifiedDate = DateTime.Now;
                    userGroupTimeLineRepository.UpdateAsync(timeline);
                    List<UserGroupEmployee> res = userGroupEmployeeRepository.ListAllAsyncIncludeAll().Result.Where(a => a.UserGroupId == Allempgrpid).ToList();

                    foreach (UserGroupEmployee empgrp in res)
                    {
                        empgrp.IsAvailable = true;
                        empgrp.Active = true;
                        empgrp.Status = EntityStatus.ACTIVE;
                        empgrp.ModifiedId = userId;
                        empgrp.ModifiedDate = DateTime.Now;
                        userGroupEmployeeRepository.UpdateAsync(empgrp);
                        long usergrpempid = empgrp.ID;
                        UserGroupEmployeeTimeLine emptimeline = userGroupEmployeeTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(z => z.UserGroupEmployeeID == usergrpempid).FirstOrDefault();
                        if (emptimeline != null)
                        {
                            emptimeline.Active = true;
                            emptimeline.IsAvailable = true;
                            emptimeline.Status = EntityStatus.ACTIVE;
                            emptimeline.ModifiedId = userId;
                            emptimeline.ModifiedDate = DateTime.Now;
                            userGroupEmployeeTimeLineRepository.UpdateAsync(emptimeline);
                        }

                    }
                }
                data = 1;
            }
            catch (Exception ex)
            {
                data = 0;
            }

            return Json(data);

            // return RedirectToAction(nameof(ViewUserGroup)).WithSuccess("Data", "Deactivated Successfully");

        }

        public async Task<IActionResult> ActiveUserGroup(long id)
        {
            CheckLoginStatus();
            UserGroup grp = userGroupRepository.GetByIdAsyncIncludeAll(id).Result;
            grp.IsAvailable = true;
            grp.Active = true;
            grp.ModifiedId = userId;
            grp.Status = EntityStatus.ACTIVE;
            grp.ModifiedDate = DateTime.Now;
            await userGroupRepository.UpdateAsync(grp);
            long usergroupid = grp.ID;
            UserGroupTimeLine timeline = userGroupTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(z => z.UserGroupID == id).FirstOrDefault();

            timeline.Active = Convert.ToBoolean(1);
            timeline.IsAvailable = true;
            timeline.EndDate = null;
            timeline.ModifiedId = userId;
            timeline.Status = EntityStatus.ACTIVE;
            timeline.ModifiedDate = DateTime.Now;
            await userGroupTimeLineRepository.UpdateAsync(timeline);
            List<UserGroupEmployee> res = userGroupEmployeeRepository.ListAllAsyncIncludeAll().Result.Where(a => a.UserGroupId == id).ToList();

            foreach (UserGroupEmployee empgrp in res)
            {
                empgrp.IsAvailable = true;
                empgrp.Active = true;
                empgrp.Status = EntityStatus.ACTIVE;
                empgrp.ModifiedId = userId;
                empgrp.ModifiedDate = DateTime.Now;
                await userGroupEmployeeRepository.UpdateAsync(empgrp);
                long usergrpempid = empgrp.ID;
                UserGroupEmployeeTimeLine emptimeline = userGroupEmployeeTimeLineRepository.ListAllAsyncIncludeAll().Result.Where(z => z.UserGroupEmployeeID == usergrpempid).FirstOrDefault();
                if(emptimeline!=null)
                {
                    emptimeline.Active = true;
                    emptimeline.IsAvailable = true;
                    emptimeline.Status = EntityStatus.ACTIVE;
                    emptimeline.ModifiedId = userId;
                    emptimeline.ModifiedDate = DateTime.Now;
                    await userGroupEmployeeTimeLineRepository.UpdateAsync(emptimeline);
                }
              
            }
            return RedirectToAction(nameof(ViewUserGroup)).WithSuccess("Data", "Activated Successfully");

        }
        //Download Excel Sample
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public IActionResult DownloadUserGroupample()
        {

            XLWorkbook oWB = new XLWorkbook();
            var EmpList = employeeRepository.GetAllEmployee().OrderBy(a => a.EmpCode).ToList();
            DataTable categorydt = new DataTable();
            categorydt.Columns.Add("EmpName");
            foreach (var a in EmpList)
            {
                DataRow dr = categorydt.NewRow();
                dr["EmpName"] =a.FirstName+" "+a.LastName+"-"+a.EmpCode;
                categorydt.Rows.Add(dr);
            }
            categorydt.TableName = "Employee";

            int lastCellNo5 = categorydt.Rows.Count + 1;
            oWB.AddWorksheet(categorydt);
            var worksheet1 = oWB.Worksheet(1);

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Group Name");
            validationTable.Columns.Add("Description");
            validationTable.Columns.Add("Employee");
            validationTable.TableName = "UserGroup";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(3).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo5), true);

            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"EmployeeGroupSample.xlsx");
        }
        //Uplooad Excel
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadEmployeeGroup(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {

                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            var GroupName = "";
                            if (firstWorksheet.Cells[i, 1].Value != null)
                            {
                                if (firstWorksheet.Cells[i, 1].Value.ToString() != "")
                                {
                                    GroupName = firstWorksheet.Cells[i, 1].Value.ToString();

                                }
                            }
                            if (GroupName!="") {
                                UserGroup usergroup = new UserGroup();
                                usergroup = userGroupRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.GroupName.ToLower() == GroupName.ToLower() && a.Active == true).FirstOrDefault();
                                if (usergroup == null)
                                {
                                    usergroup = new UserGroup();
                                    if (firstWorksheet.Cells[i, 2] != null && firstWorksheet.Cells[i, 2].Value != null)
                                    {
                                        usergroup.Description = firstWorksheet.Cells[i, 2].Value.ToString();
                                    }
                                    usergroup.EmployeeId = userId;
                                    usergroup.GroupName = GroupName;
                                    usergroup.Active = true;
                                    usergroup.IsAvailable = true;
                                    usergroup.InsertedId = userId;
                                    usergroup.IsAvailable = true;
                                    usergroup.ModifiedId = userId;
                                    usergroup.ModifiedDate = DateTime.Now;
                                    usergroup.InsertedDate = DateTime.Now;
                                    usergroup.Status = EntityStatus.ACTIVE;
                                    var usergroupid = userGroupRepository.AddAsync(usergroup);
                                    UserGroupTimeLine timeline = new UserGroupTimeLine();
                                    timeline.UserGroupID = usergroupid.Result.ID;
                                    timeline.StartDate = DateTime.Now;
                                    timeline.EndDate = null;
                                    timeline.Active = true;
                                    timeline.InsertedId = userId;
                                    timeline.IsAvailable = true;
                                    timeline.ModifiedId = userId;
                                    timeline.Status = EntityStatus.ACTIVE;
                                    timeline.InsertedDate = DateTime.Now;
                                    timeline.ModifiedDate = DateTime.Now;
                                    userGroupTimeLineRepository.AddAsync(timeline);

                                    UserGroupEmployee usergrpemp = new UserGroupEmployee();
                                    usergrpemp.UserGroupId = usergroupid.Result.ID;
                                    var emp = firstWorksheet.Cells[i, 3].Value.ToString();
                                    var empcode = emp.Split('-')[1].ToString();
                                    usergrpemp.EmployeeId = employeeRepository.GetAllEmployee().Where(a => a.EmpCode == empcode).FirstOrDefault().ID;
                                    usergrpemp.Active = true;
                                    usergrpemp.InsertedId = userId;
                                    usergrpemp.IsAvailable = true;
                                    usergrpemp.ModifiedId = userId;
                                    usergrpemp.Status = EntityStatus.ACTIVE;
                                    usergrpemp.InsertedDate = DateTime.Now;
                                    usergrpemp.ModifiedDate = DateTime.Now;
                                    var usergroupempid = userGroupEmployeeRepository.AddAsync(usergrpemp);
                                    UserGroupEmployeeTimeLine emptimeline = new UserGroupEmployeeTimeLine();
                                    emptimeline.UserGroupEmployeeID = usergroupempid.Result.ID;
                                    emptimeline.StartDate = DateTime.Now;
                                    emptimeline.EndDate = null;
                                    emptimeline.IsLeft = false;
                                    emptimeline.Active = true;
                                    emptimeline.InsertedId = userId;
                                    emptimeline.IsAvailable = true;
                                    emptimeline.ModifiedId = userId;
                                    emptimeline.Status = EntityStatus.ACTIVE;
                                    emptimeline.InsertedDate = DateTime.Now;
                                    emptimeline.ModifiedDate = DateTime.Now;
                                    userGroupEmployeeTimeLineRepository.AddAsync(emptimeline);
                                }
                                else
                                {
                                    if (firstWorksheet.Cells[i, 2] != null && firstWorksheet.Cells[i, 2].Value .ToString()!= " ")
                                    {
                                        usergroup.Description = firstWorksheet.Cells[i, 2].Value.ToString();
                                    }
                                    usergroup.EmployeeId = userId;
                                    usergroup.GroupName = GroupName;
                                    usergroup.Active = true;
                                    usergroup.IsAvailable = true;
                                    usergroup.ModifiedId = userId;
                                    usergroup.ModifiedDate = DateTime.Now;
                                    usergroup.InsertedDate = DateTime.Now;
                                    usergroup.Status = EntityStatus.ACTIVE;
                                    userGroupRepository.UpdateAsync(usergroup);

                                    var emp = firstWorksheet.Cells[i, 3].Value.ToString();
                                    var empcode = emp.Split('-')[1].ToString();
                                    var empid = employeeRepository.GetAllEmployee().Where(a => a.EmpCode == empcode).FirstOrDefault().ID;
                                    UserGroupEmployee usergrpemp = userGroupEmployeeRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.UserGroupId == usergroup.ID && a.EmployeeId == empid).FirstOrDefault();

                                    if (usergrpemp == null)
                                    {
                                        usergrpemp = new UserGroupEmployee();
                                        usergrpemp.UserGroupId = usergroup.ID;
                                        usergrpemp.EmployeeId = empid;
                                        usergrpemp.Active = true;
                                        usergrpemp.InsertedId = userId;
                                        usergrpemp.IsAvailable = true;
                                        usergrpemp.ModifiedId = userId;
                                        usergrpemp.Status = EntityStatus.ACTIVE;
                                        usergrpemp.InsertedDate = DateTime.Now;
                                        usergrpemp.ModifiedDate = DateTime.Now;
                                        var usergroupempid = userGroupEmployeeRepository.AddAsync(usergrpemp);

                                        UserGroupEmployeeTimeLine emptimeline = new UserGroupEmployeeTimeLine();
                                        emptimeline.UserGroupEmployeeID = usergroupempid.Result.ID;
                                        emptimeline.StartDate = DateTime.Now;
                                        emptimeline.EndDate = null;
                                        emptimeline.IsLeft = false;
                                        emptimeline.Active = true;
                                        emptimeline.InsertedId = userId;
                                        emptimeline.IsAvailable = true;
                                        emptimeline.ModifiedId = userId;
                                        emptimeline.Status = EntityStatus.ACTIVE;
                                        emptimeline.InsertedDate = DateTime.Now;
                                        emptimeline.ModifiedDate = DateTime.Now;
                                        userGroupEmployeeTimeLineRepository.AddAsync(emptimeline);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(ViewUserGroup)).WithSuccess("Employee Group", "Excel uploaded successfully"); ;
        }
        [HttpGet]
        public ActionResult ViewUserGroupEmployee(long usergroupid)
        {

            var usergroupemp = userGroupEmployeeRepository.ListAllAsyncIncludeAll().Result.Where(a => a.UserGroupId == usergroupid).ToList();
            var timeline = userGroupEmployeeTimeLineRepository.ListAllAsyncIncludeAll().Result;
            var employee = employeeRepository.GetAllEmployee();
            ResponseUserGroupModel usergrp = new ResponseUserGroupModel();
            var res = (from a in usergroupemp

                       select new UserGroupModel
                       {
                           ID = a.ID,
                           EmployeeId = a.EmployeeId,
                           InsertedDate = a.InsertedDate,
                           EmployeeCode = employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().EmpCode,
                           EmployeeName = employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().FirstName,
                           ModifiedDateText = NullableDateTimeToStringDate(a.ModifiedDate),
                           StartDate = timeline.Where(x => x.UserGroupEmployeeID == a.ID).FirstOrDefault().StartDate,
                           Status = a.Status,
                           Active = a.Active,
                           EmployeeCount = usergroupemp.Where(x => x.UserGroupId == a.ID).Count(),
                           UserGroupID = a.UserGroupId,
                       }
                      ).ToList();

            usergrp.UserGrouplist = res;
            usergrp.employeelist = employeeRepository.GetAllEmployee().ToList();
            return View(usergrp);

        }
        public async Task<IActionResult> DeleteUserGroupEmployee(long id, long usergroupid, string status)
        {
            CheckLoginStatus();
            UserGroupEmployee grp = userGroupEmployeeRepository.GetByIdAsyncIncludeAll(id).Result;

            if (status == "INACTIVE")
            {
                grp.IsAvailable = false;
                grp.Active = false;
                grp.Status = EntityStatus.INACTIVE;

            }
            grp.ModifiedId = userId;
            grp.ModifiedDate = DateTime.Now;
            await userGroupEmployeeRepository.UpdateAsync(grp);
            UserGroupEmployeeTimeLine timeline = userGroupEmployeeTimeLineRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(z => z.UserGroupEmployeeID == id).FirstOrDefault();
            if (status == "INACTIVE")
            {

                timeline.Active = false;
                timeline.IsAvailable = false;
                timeline.EndDate = DateTime.Now;
                timeline.Status = EntityStatus.INACTIVE;

            }
            timeline.ModifiedId = userId;
            timeline.ModifiedDate = DateTime.Now;
            await userGroupEmployeeTimeLineRepository.UpdateAsync(timeline);


            //return RedirectToAction(nameof(ViewUserGroupEmployee),new { usergroupid= usergroupid }).WithSuccess("Status", "Updated Successfully");
            return RedirectToAction(nameof(ViewUserGroup)).WithSuccess("Status", "Updated Successfully");
        }

        [HttpPost]
        public async Task<IActionResult> SaveUserGroupEmployee(long[] MultiEmployeeId, long usergroupid)
        {
            CheckLoginStatus();
            ArrayList arlist = new ArrayList(MultiEmployeeId);
            foreach (long str in arlist)
            {
                UserGroupEmployee usergrpemp = new UserGroupEmployee();
                usergrpemp.UserGroupId = usergroupid;
                usergrpemp.EmployeeId = str;
                usergrpemp.Active = true;
                usergrpemp.InsertedId = userId;
                usergrpemp.IsAvailable = true;
                usergrpemp.ModifiedId = userId;
                usergrpemp.Status = EntityStatus.ACTIVE;
                usergrpemp.InsertedDate = DateTime.Now;
                usergrpemp.ModifiedDate = DateTime.Now;
                var usergroupempid = userGroupEmployeeRepository.AddAsync(usergrpemp);
                UserGroupEmployeeTimeLine emptimeline = new UserGroupEmployeeTimeLine();
                emptimeline.UserGroupEmployeeID = usergroupempid.Result.ID;
                emptimeline.StartDate = DateTime.Now;
                emptimeline.EndDate = null;
                emptimeline.IsLeft = false;
                emptimeline.Active = true;
                emptimeline.InsertedId = userId;
                emptimeline.IsAvailable = true;
                emptimeline.ModifiedId = userId;
                emptimeline.Status = EntityStatus.ACTIVE;
                emptimeline.InsertedDate = DateTime.Now;
                emptimeline.ModifiedDate = DateTime.Now;
                await userGroupEmployeeTimeLineRepository.AddAsync(emptimeline);
            }
            return RedirectToAction(nameof(ViewUserGroup)).WithSuccess("Data", "Submitted Successfully");

        }

        #endregion-------------------------------------------------------
        #region---------------------------------Notice-------------------------------
        public ActionResult Notice()
        {
            CheckLoginStatus();
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
           
            if (commonMethods.checkaccessavailable("Notice", accessId, "List", "School", roleId) == true)
            {
                ViewBag.access = 1;
                ViewBag.roleId = roleId;
                var res = GetAllNotice(userId, roleId);
                return View(res);               
            }
            else
            {
                ViewBag.access = 0;
                var res = GetAllNotice(userId, roleId);
                return View(res);
            }         
        }  
        public IEnumerable<NoticeDataModel> GetAllNotice(long? userId,long? roleId)
        {
            var noticelist = noticeRepository.ListAllAsyncIncludeAll().Result.ToList();
            var noticedatalist = noticeDataRepository.ListAllAsyncIncludeAll().Result.ToList();
            var emplist = employeeRepository.GetAllEmployee().ToList();
            var stulist = studentAggregateRepository.GetAllStudent().ToList();
            IEnumerable<NoticeDataModel> res = (from b in noticedatalist
                                                select new NoticeDataModel
                                                {
                                                    ID = b.ID,
                                                    Status = b.Status,
                                                    NoticeId = b.NoticeId,
                                                    EmployeeId = b.EmployeeId,
                                                    StudentId = b.StudentId!=null&&b.StudentId!=0?b.StudentId:0,
                                                    EmpName = b.EmployeeId > 0 ? emplist.Where(x => x.ID == b.EmployeeId).FirstOrDefault().FirstName : "",
                                                    EmpMobile = b.EmployeeId > 0 ? emplist.Where(x => x.ID == b.EmployeeId).FirstOrDefault().PrimaryMobile : "",
                                                    Message = noticelist.Where(x => x.ID == b.NoticeId).FirstOrDefault().Message,
                                                    SendSubType = noticelist.Where(x => x.ID == b.NoticeId).FirstOrDefault().SendSubType,
                                                    SendType = noticelist.Where(x => x.ID == b.NoticeId).FirstOrDefault().SendType,
                                                    InsertedDate = b.InsertedDate,
                                                    //StudentName = b.StudentId > 0 ? stulist.Where(x => x.ID == b.StudentId).FirstOrDefault().FirstName : "",
                                                }).OrderByDescending(s => s.ID).ToList();
            if(roleId==4)
            {
                res = res.Where(x => x.EmployeeId == userId).OrderByDescending(s => s.ID).ToList();
            }
            if (roleId == 5)
            {
                res = res.Where(x => x.StudentId == userId).OrderByDescending(s => s.ID).ToList();
            }
            return res;
          

        }

        [HttpGet]
        public ActionResult CreateNotice()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Notice", accessId, "Create", "School", roleId) == false)
            {
               
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            NoticeModel modelclslist = new NoticeModel();
            modelclslist.academicSessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.IsAvailable == true).ToList();
            modelclslist.groupList = userGroupRepository.ListAllAsyncIncludeActiveNoTrack().Result.ToList();
            modelclslist.boards = boardRepository.GetAllBoard();
            modelclslist.departmentList = GetAllDepartment();
            modelclslist.organisationList = organizationRepository.GetAllOrganization().ToList();
            return View(modelclslist);
        }
        [HttpPost]
        public async Task<IActionResult> SaveNotice(NoticeModel ntc)
        {
            CheckLoginStatus();
            Notice notice = new Notice();            
            notice.SendType = ntc.SendType;
            notice.SendSubType = ntc.Employee_SendTo;            
            notice.Message = ntc.Message;          
            notice.Active = true;
            notice.InsertedId = userId;
            notice.IsAvailable = true;
            notice.ModifiedId = userId;
            notice.Status = EntityStatus.ACTIVE;
            notice.InsertedDate = DateTime.Now;
            notice.ModifiedDate = DateTime.Now;
            var noticelist = noticeRepository.AddAsync(notice);
            var noticeid = noticelist.Result.ID;
            var Message = noticelist.Result.Message;

            if (ntc.SendType=="All")
            {
                var studentlist = studentAggregateRepository.GetAllStudent().ToList();
                foreach (var str in studentlist)
                {

                    NoticeData noticedata = new NoticeData();
                    noticedata.StudentId = str.ID;
                    noticedata.EmployeeId = 0;
                   noticedata.Active = true;
                    noticedata.IsAvailable = true;
                    noticedata.Status = EntityStatus.ACTIVE;
                    noticedata.InsertedId = userId;
                    noticedata.ModifiedId = userId;
                    noticedata.Status = EntityStatus.ACTIVE;
                    noticedata.InsertedDate = DateTime.Now;
                    noticedata.ModifiedDate = DateTime.Now;
                    noticedata.NoticeId = noticeid;
                    var noticedatalist= noticeDataRepository.AddAsync(noticedata);
                   // string empphone = employeeRepository.GetAllEmployee().Where(z => z.ID == noticedatalist.Result.EmployeeId).FirstOrDefault().PrimaryMobile;
                    //smssend.Sendsmstoemployee(empphone, noticelist.Result.Message);
                    
                }
                long[] EmployeeGroupid = new Int64[0];
                long[] Departmentid = new Int64[0];
                List<UserGroupModel> usergrplist = GetAllEmployeeByGroup(EmployeeGroupid).ToList();
                List<EmployeeModel> deptlist = GetAllEmployeeByDepartment(Departmentid).ToList();

                foreach (var str in usergrplist)
                {

                    NoticeData noticedata = new NoticeData();
                    noticedata.StudentId = 0;
                    noticedata.EmployeeId = str.EmployeeId;                    
                    noticedata.Active = true;
                    noticedata.IsAvailable = true;
                    noticedata.Status = EntityStatus.ACTIVE;
                    noticedata.InsertedId = userId;
                    noticedata.ModifiedId = userId;
                    noticedata.Status = EntityStatus.ACTIVE;
                    noticedata.InsertedDate = DateTime.Now;
                    noticedata.ModifiedDate = DateTime.Now;
                    noticedata.NoticeId = noticeid;                   
                    var noticedatalist = noticeDataRepository.AddAsync(noticedata);
                    var employeeid = noticedatalist.Result.EmployeeId;
                    string empphone = employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile != null ? employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile : " ";
                    if (empphone != null && empphone != "" && Message != null && Message != "")
                    {
                        smssend.Sendsmstoemployee(empphone, noticelist.Result.Message);
                    }
                }
                foreach (var str in deptlist)
                {
                    NoticeData noticedata = new NoticeData();
                    noticedata.Active = true;
                    noticedata.IsAvailable = true;
                    noticedata.Status = EntityStatus.ACTIVE;
                    noticedata.InsertedId = userId;
                    noticedata.ModifiedId = userId;
                    noticedata.Status = EntityStatus.ACTIVE;
                    noticedata.InsertedDate = DateTime.Now;
                    noticedata.ModifiedDate = DateTime.Now;
                    noticedata.NoticeId = noticeid;
                    noticedata.StudentId = 0;
                    noticedata.EmployeeId = str.EmployeeId;
                    var noticedatalist = noticeDataRepository.AddAsync(noticedata);
                    var employeeid = noticedatalist.Result.EmployeeId;
                    string empphone = employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile != null ? employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile : " ";
                    if (empphone != null && empphone != "" && Message != null && Message != "")
                    {
                        smssend.Sendsmstoemployee(empphone, noticelist.Result.Message);
                    }

                }

            }
            else if(ntc.SendType == "Employee")
            {
                if (ntc.Employee_SendTo == "All")
                {
                    long[] EmployeeGroupid = new Int64[0];
                    long[] Departmentid = new Int64[0];
                    List<UserGroupModel> usergrplist = GetAllEmployeeByGroup(EmployeeGroupid).ToList();
                    List<EmployeeModel> deptlist = GetAllEmployeeByDepartment(Departmentid).ToList();

                    foreach (var str in usergrplist)
                    {

                        NoticeData noticedata = new NoticeData();
                        noticedata.StudentId = 0;
                        noticedata.EmployeeId = str.EmployeeId;
                        noticedata.Active = true;
                        noticedata.IsAvailable = true;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.InsertedId = userId;
                        noticedata.ModifiedId = userId;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.InsertedDate = DateTime.Now;
                        noticedata.ModifiedDate = DateTime.Now;
                        noticedata.NoticeId = noticeid;
                        var noticedatalist = noticeDataRepository.AddAsync(noticedata);
                        var employeeid = noticedatalist.Result.EmployeeId;
                        string empphone = employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile != null ? employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile : " ";
                        if (empphone != null && empphone != "" && Message != null && Message != "")
                        {
                            smssend.Sendsmstoemployee(empphone, noticelist.Result.Message);
                        }

                    }
                    foreach (var str in deptlist)
                    {
                        NoticeData noticedata = new NoticeData();
                        noticedata.Active = true;
                        noticedata.IsAvailable = true;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.InsertedId = userId;
                        noticedata.ModifiedId = userId;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.InsertedDate = DateTime.Now;
                        noticedata.ModifiedDate = DateTime.Now;
                        noticedata.NoticeId = noticeid;
                        noticedata.StudentId = 0;
                        noticedata.EmployeeId = str.EmployeeId;
                        var noticedatalist = noticeDataRepository.AddAsync(noticedata);
                        var employeeid = noticedatalist.Result.EmployeeId;
                        string empphone = employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile != null ? employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile : " ";
                        if (empphone != null && empphone != "" && Message != null && Message != "")
                        {
                            smssend.Sendsmstoemployee(empphone, noticelist.Result.Message);
                        }

                    }
                }
                else if (ntc.Employee_SendTo == "Group")
                {
                    List<UserGroupModel> usergrplist = GetAllEmployeeByGroup(ntc.EmployeeGroupid).ToList();
                    foreach (var str in usergrplist)
                    {
                        NoticeData noticedata = new NoticeData();
                        noticedata.Active = true;
                        noticedata.IsAvailable = true;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.InsertedId = userId;
                        noticedata.ModifiedId = userId;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.InsertedDate = DateTime.Now;
                        noticedata.ModifiedDate = DateTime.Now;
                        noticedata.NoticeId = noticeid;
                        noticedata.StudentId = 0;
                        noticedata.EmployeeId = str.EmployeeId;
                        var noticedatalist = noticeDataRepository.AddAsync(noticedata);
                        var employeeid = noticedatalist.Result.EmployeeId;
                        string empphone = employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile != null ? employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile : " ";
                        if (empphone != null && empphone != "" && Message != null && Message != "")
                        {
                            smssend.Sendsmstoemployee(empphone, noticelist.Result.Message);
                        }

                    }

                }
                else if (ntc.Employee_SendTo == "Department")
                {
                    List<EmployeeModel> deptlist = GetAllEmployeeByDepartment(ntc.Departmentid).ToList();
                   
                    foreach (var str in deptlist)
                    {
                        NoticeData noticedata = new NoticeData();
                        noticedata.Active = true;
                        noticedata.IsAvailable = true;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.InsertedId = userId;
                        noticedata.ModifiedId = userId;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.InsertedDate = DateTime.Now;
                        noticedata.ModifiedDate = DateTime.Now;
                        noticedata.NoticeId = noticeid;
                        noticedata.StudentId = 0;
                        noticedata.EmployeeId = str.EmployeeId;
                        var noticedatalist = noticeDataRepository.AddAsync(noticedata);
                        var employeeid = noticedatalist.Result.EmployeeId;
                        string empphone = employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile != null ? employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile : " ";
                        if (empphone != null && empphone != "" && Message != null && Message != "")
                        {
                            smssend.Sendsmstoemployee(empphone, noticelist.Result.Message);
                        }
                    }
                }
                else if (ntc.Employee_SendTo == "Organisation")
                {
                    List<EmployeeDesignationModel> employeelist = GetAllEmployeeByOrganisation(ntc.Organisationid).ToList();

                    foreach (var str in employeelist)
                    {
                        NoticeData noticedata = new NoticeData();
                        noticedata.Active = true;
                        noticedata.IsAvailable = true;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.InsertedId = userId;
                        noticedata.ModifiedId = userId;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.InsertedDate = DateTime.Now;
                        noticedata.ModifiedDate = DateTime.Now;
                        noticedata.NoticeId = noticeid;
                        noticedata.StudentId = 0;
                        noticedata.EmployeeId = str.EmployeeID;
                        var noticedatalist = noticeDataRepository.AddAsync(noticedata);
                        var employeeid = noticedatalist.Result.EmployeeId;
                        string empphone = employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile != null ? employeeRepository.GetAllEmployee().Where(z => z.ID == employeeid).FirstOrDefault().PrimaryMobile : " ";
                        if (empphone != null && empphone != "" && Message != null && Message != "")
                        {
                            smssend.Sendsmstoemployee(empphone, noticelist.Result.Message);
                        }
                    }
                }
            }
            else if (ntc.SendType == "Student")
            {
               // long id = ntc.AcademicStudentId[0];
                if (ntc.AcademicStudentId != null)
                { 
                    if (ntc.AcademicStudentId[0] == 0)
                    {
                        List<StudentModel> studlist = GetAllStudentsByAcademicStandard(ntc.AcademicSessionId,ntc.AcademicStandarStreamdId,ntc.OraganisationAcademicId,ntc.BoardId,ntc.AcademicStandardId, ntc.SchoolWingId).ToList();
                        foreach (var str in studlist)
                        {
                            NoticeData noticedata = new NoticeData();
                            noticedata.Active = true;
                            noticedata.IsAvailable = true;
                            noticedata.Status = EntityStatus.ACTIVE;
                            noticedata.InsertedId = userId;
                            noticedata.ModifiedId = userId;
                            noticedata.Status = EntityStatus.ACTIVE;
                            noticedata.InsertedDate = DateTime.Now;
                            noticedata.ModifiedDate = DateTime.Now;
                            noticedata.NoticeId = noticeid;
                            noticedata.StudentId = str.id;
                            noticedata.EmployeeId = 0;
                            var studentlist= noticeDataRepository.AddAsync(noticedata);
                            var studentid = studentlist.Result.StudentId;
                            //string studentphone = studentAggregateRepository.GetAllStudent().Where(z => z.ID == studentid).FirstOrDefault().StudentCode != null ? studentAggregateRepository.GetAllStudent().Where(z => z.ID == studentid).FirstOrDefault().StudentCode : " ";
                            //if (studentphone != null && studentphone != "" && Message != null && Message != "")
                            //{
                            //    smssend.Sendsmstoemployee(studentphone, noticelist.Result.Message);
                            //}
                        }
                    }
                    else
                    {
                        ArrayList arlist = new ArrayList(ntc.AcademicStudentId);
                        foreach (long str in arlist)
                        {
                            NoticeData noticedata = new NoticeData();
                            noticedata.Active = true;
                            noticedata.IsAvailable = true;
                            noticedata.Status = EntityStatus.ACTIVE;
                            noticedata.InsertedId = userId;
                            noticedata.ModifiedId = userId;
                            noticedata.Status = EntityStatus.ACTIVE;
                            noticedata.InsertedDate = DateTime.Now;
                            noticedata.ModifiedDate = DateTime.Now;
                            noticedata.NoticeId = noticeid;
                            noticedata.StudentId = str;
                            noticedata.EmployeeId = 0;
                            var studentlist = noticeDataRepository.AddAsync(noticedata);
                            var studentid = studentlist.Result.StudentId;
                        }
                    }
                }
            }
           


            return RedirectToAction(nameof(CreateNotice)).WithSuccess("Notice", "Send Successfully");

        }
        public async Task<IActionResult> DeleteNotice(long id)
        {            
            try
            {
                CheckLoginStatus();               
                Notice grp = noticeRepository.GetByIdAsyncIncludeAll(id).Result;
                grp.IsAvailable = false;
                grp.Active = false;
                grp.ModifiedId = userId;
                grp.Status = EntityStatus.INACTIVE;
                grp.ModifiedDate = DateTime.Now;
               await noticeRepository.UpdateAsync(grp);
                List<NoticeData> res = noticeDataRepository.ListAllAsyncIncludeAll().Result.Where(a => a.NoticeId == id).ToList();
                foreach (NoticeData noticedata in res)
                {
                    noticedata.IsAvailable = false;
                    noticedata.Active = false;
                    noticedata.ModifiedId = userId;
                    noticedata.Status = EntityStatus.INACTIVE;
                    noticedata.ModifiedDate = DateTime.Now;
                    await noticeDataRepository.UpdateAsync(noticedata);
                }
                return RedirectToAction(nameof(Notice)).WithSuccess("Notice", "Deleted Successfully");
            }
            catch (Exception ex)
            {
                return null;
            }

          
        }
        public JsonResult DeactivateAllNotice(long[] id)
        {
           
            int data = 0;
            try
            {
                CheckLoginStatus();
                foreach (long Allempgrpid in id)
                {
                    NoticeData noticedata = noticeDataRepository.GetByIdAsyncIncludeAll(Allempgrpid).Result;
                    noticedata.IsAvailable = false;
                    noticedata.Active = false;
                    noticedata.ModifiedId = userId;
                    noticedata.Status = EntityStatus.INACTIVE;
                    noticedata.ModifiedDate = DateTime.Now;
                    noticeDataRepository.UpdateAsync(noticedata);
                }
                 data = 1;
            }
            catch (Exception ex)
            {
                data = 0;
            }

            return Json(data);            
        }
        public JsonResult ActivateAllNotice(long[] id)
        {           
            int data = 0;
            try
            {
                CheckLoginStatus();
                foreach (long Allempgrpid in id)
                {
                    Notice grp = noticeRepository.GetByIdAsyncIncludeAll(Allempgrpid).Result;
                    grp.IsAvailable = true;
                    grp.Active = true;
                    grp.ModifiedId = userId;
                    grp.Status = EntityStatus.ACTIVE;
                    grp.ModifiedDate = DateTime.Now;
                    noticeRepository.UpdateAsync(grp);
                    List<NoticeData> res = noticeDataRepository.ListAllAsyncIncludeAll().Result.Where(a => a.NoticeId == Allempgrpid).ToList();
                    foreach (NoticeData noticedata in res)
                    {
                        noticedata.IsAvailable = true;
                        noticedata.Active = true;
                        noticedata.ModifiedId = userId;
                        noticedata.Status = EntityStatus.ACTIVE;
                        noticedata.ModifiedDate = DateTime.Now;
                        noticeDataRepository.UpdateAsync(noticedata);
                    }
                }
                data = 1;
            }
            catch (Exception ex)
            {
                data = 0;
            }

            return Json(data);
        }

      

        #endregion----------------------------------------------------------------------------

        #region---------------Common Method---------------------------------------------------
        public IEnumerable<EmployeeModel> GetAllEmployeeByDepartment(long[] departmentid)
        {
            try
            {
                var employeeDesignation = employeeDesignationRepository.GetAllEmployeeDesignations();
                var department = departmentRepository.GetAllDepartment();
                var organization = organizationRepository.GetAllOrganization();
                var designation = designationRepository.GetAllDesignation().ToList();

                IEnumerable<EmployeeModel> res = (from a in employeeDesignation
                            join b in designation on a.DesignationID equals b.ID
                            //join d in department on b.DepartmentID equals d.ID

                            select new EmployeeModel
                            {
                                DepartmentID = b.DepartmentID,
                                DesignationId = a.DesignationID,
                                EmployeeId = a.EmployeeID,
                                ID = a.ID,
                            }).ToList();
                if (departmentid.Count() > 0 && departmentid[0]!=0)
                {
                    res = res.Where(a => departmentid.Contains(a.DepartmentID)).ToList();
                }
                return res;
                
              
                
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IEnumerable<UserGroupModel> GetAllEmployeeByGroup(long[] usergroupid)
        {
            try
            {                
                var usergroupempList = userGroupEmployeeRepository.ListAllAsyncIncludeActiveNoTrack().Result.ToList();                
                var employee = employeeRepository.GetAllEmployee();                
                var emptimeline = userGroupEmployeeTimeLineRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                if (usergroupempList != null)
                {
                    IEnumerable<UserGroupModel> res = (from a in usergroupempList
                            select new UserGroupModel
                            {                                                   
                                ID = a.ID,
                                EmployeeId = a.EmployeeId,
                                InsertedDate = a.InsertedDate,
                                EmployeeCode = employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().EmpCode,
                                EmployeeName = employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().FirstName,
                                EmployeeLastName = employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().LastName,
                                EmployeePhone= employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().PrimaryMobile!= null?employee.Where(z => z.ID == a.EmployeeId).FirstOrDefault().PrimaryMobile:"NA",
                                ModifiedDateText = NullableDateTimeToStringDate(a.ModifiedDate),
                                Status = a.Status,
                                Active = a.Active,
                                UserGroupID = a.UserGroupId,
                            }
                        ).ToList();
                    if (usergroupid.Count() > 0 && usergroupid!=null && usergroupid[0]!=0)
                    {
                        res = res.Where(a => usergroupid.Contains(a.UserGroupID.Value)).ToList();
                    }
                    return res;
                }
                return null;

            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public IEnumerable<StudentModel> GetAllStudentsByAcademicStandard(long AcademicSessionId, long? AcademicStandardStreamId, long OrganistionAcademicId, long BoardId, long BoardStandardId, long SchoolWingId)
        {
            try
            {
                var sessions = academicSessionRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID < AcademicSessionId).LastOrDefault();
                var organisation = organizationAcademicRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.ID == OrganistionAcademicId).FirstOrDefault();
                var boardstandardlist = BoardStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                var academicstandardlist = academicStandardRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                var boardstandarddet = boardstandardlist.Where(a => a.ID == academicstandardlist.Where(b => b.ID == BoardStandardId).Select(b => b.BoardStandardId).FirstOrDefault()).FirstOrDefault();
                var standards = StandardRepository.GetAllStandard().Where(a => a.ID < boardstandarddet.StandardId).LastOrDefault();
                var boardstandard = standards == null ? null : boardstandardlist.Where(a => a.BoardId == BoardId && a.StandardId == standards.ID).FirstOrDefault();
                var students = studentAggregateRepository.GetAllStudent();
                var allacademicstudents = academicStudentRepository.ListAllAsyncIncludeActiveNoTrack().Result;
                var newres = students.Select(a => a.ID).Except(allacademicstudents.Select(a => a.StudentId));
                IEnumerable<StudentModel> newstudents = (from a in students
                                   where newres.Contains(a.ID)
                                   select new StudentModel
                                   {
                                       id = a.ID,
                                       name = a.FirstName + " " + a.LastName,
                                       code = a.StudentCode,
                                       status = "N"
                                   }).ToList();
                if (boardstandard != null && sessions != null)
                {
                    var organisationAcademy = organizationAcademicRepository.ListAllAsyncIncludeActiveNoTrack().Result.Where(a => a.OrganizationId == organisation.OrganizationId && a.AcademicSessionId == sessions.ID).FirstOrDefault();
                    var academicstandard = academicstandardlist.Where(a => a.BoardStandardId == boardstandard.ID && a.OrganizationAcademicId == organisationAcademy.ID).FirstOrDefault();
                    if (academicstandard != null)
                    {
                        var academicstudents = allacademicstudents.Where(a => a.AcademicStandardId == academicstandard.ID).ToList();
                        var commonres = (from a in students
                                         join b in academicstudents on a.ID equals b.StudentId
                                         select new StudentModel
                                         {
                                             id = a.ID,
                                             name = a.FirstName + " " + a.LastName,
                                             code = a.StudentCode,
                                             status = "O"
                                         }).ToList();
                        var res = newstudents.Union(commonres);
                        return res;
                    }
                    else
                    {
                        return newstudents;
                    }
                }
                else
                {
                    return newstudents;
                }
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeDesignationModel> GetAllEmployeeByOrganisation(long[] organisationid)
        {
            try
            {
                var employeeDesignation = employeeDesignationRepository.GetAllEmployeeDesignations();
                var designation = designationRepository.GetAllDesignation();
                // var designationLevel = designationLevelRepository.GetAllDesignationLevel();
                var department = departmentRepository.GetAllDepartment();
                var groups = groupRepository.GetAllGroup();
                var organization = organizationRepository.GetAllOrganization();

                IEnumerable<EmployeeDesignationModel> res = (from a in employeeDesignation
                                                           join b in designation on a.DesignationID equals b.ID
                                                           //join c in designationLevel on b.DesignationLevelID equals c.ID
                                                           join d in department on b.DepartmentID equals d.ID
                                                           //  join e in organization on d.OrganizationID equals e.ID
                                                           // join f in groups on e.GroupID equals f.ID
                                                           select new EmployeeDesignationModel
                                                           {
                                                               DepartmentID = b.DepartmentID,
                                                               DepartmentName = d.Name,
                                                               DesignationId = a.DesignationID,
                                                               DesignationName = b.Name,
                                                               EmployeeID = a.EmployeeID,
                                                               GroupID = d.GroupID,
                                                               ID = a.ID,
                                                               ModifiedDate = a.ModifiedDate,
                                                               OrganizationID = d.OrganizationID
                                                           }).ToList();
                if (organisationid.Count() > 0 && organisationid != null && organisationid[0] != 0)
                {
                    res = res.Where(a => organisationid.Contains(a.OrganizationID)).ToList();
                }
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<EmployeeDesignationModel> GetAllDepartment()
        {
            try
            {
               
                var department = departmentRepository.GetAllDepartment();
                var groups = groupRepository.GetAllGroup();
                var organization = organizationRepository.GetAllOrganization();

                IEnumerable<EmployeeDesignationModel> res = (  from d in department 
                                                            //join e in organization on d.OrganizationID equals e.ID
                                                          // join f in groups on e.GroupID equals f.ID
                                                           select new EmployeeDesignationModel
                                                           {
                                                               ID=d.ID,                                                               
                                                               DepartmentID = d.ID,
                                                               DepartmentName = d.Name,
                                                               GroupID = d.GroupID,
                                                               GroupName = d.GroupID > 0 ?
                                                               groups.Where(x => x.ID == d.GroupID).FirstOrDefault().Name :" ",                                         
                                                               OrganizationID = d.OrganizationID,
                                                               OrganizationName = d.OrganizationID > 0 ?
                                                               organization.Where(x => x.ID == d.OrganizationID).FirstOrDefault().Name : " "
                                                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        #endregion------------------------------------

    }

}
