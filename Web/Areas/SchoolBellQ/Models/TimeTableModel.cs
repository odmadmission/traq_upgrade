﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Query;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.SchoolBellQ.Models
{
    public class TimetableStudentModel : BaseModel
    {
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long AcademicStandardId { get; set; }
        public bool StandardStreamType { get; set; }
        public string StandardName { get; set; }

        [Display(Name = "Oraganisation Academic")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }

        [Display(Name = "Teacher Employee")]
        [Required(ErrorMessage = "Please Select Teacher")]
        public long ddlteacher { get; set; }
        [Display(Name = "Class Timing")]
        [Required(ErrorMessage = "Please Select Class Timing")]
        public long ddlclasstiming { get; set; }
        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Please Select Subject")]
        public long ddlsubjects { get; set; }

        public DateTime date { get; set; }

        public string OraganisationAcademicName { get; set; }

        public string OraganisationName { get; set; }

        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        [Display(Name = "School Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }
        public long CountAcademicSection { get; set; }
        public long CountAcademicStandardStream { get; set; }
        public IEnumerable<AcademicSession> academicSessions { get; set; }
        public IEnumerable<Board> boards { get; set; }
        public IEnumerable<ClassTiming> ClassTiming { get; set; }
        public long AcademicStandardStreamId { get; set; }
        public long[] StudentId { get; set; }
        public long AcademicSectionId { get; set; }
        public string[] RollNo { get; set; }
        public long[] AcademicStudentId { get; set; }

        public IEnumerable<AcademicTimeTable> academictimetable { get; set; }
    }
    public class AcademicTimeTableModel
    {      
        public long AcademicSectionId { get; set; }
        public string AcademicSection { get; set; }
        public string Code { get; set; }      
        public string NameOfMonth { get; set; }      
        public long AcademicStandardId { get; set; }
        public string AcademicStandard { get; set; }
        public long? AcademicStandardStreamId { get; set; }       
        public DateTime AssignDate { get; set; }
    }
    public class TimetableModel
    {
        public long id { get; set; }
        public long AcademicSectionId { get; set; }
        public long AcademicStandardId { get; set; }

        public long AcademicStandardStreamId { get; set; }
        public long TeacherId { get; set; }
        public long SubjectId { get; set; }
        public long ClassTimimgId { get; set; }       
        public DateTime Date { get; set; }
        public string Datestring { get; set; }
        public bool isremove { get; set; }

        public string Teachername { get; set; }
        public string Subjectname { get; set; }
        public string ClassTimimgname { get; set; }

    }
    public class view_time_table_cls
    {
        public List<View_All_TimeTable_Subjects> timetable { get; set; }
        public long AcademicSectionId { get; set; }
        public long AcademicStandardId { get; set; }
        public long? AcademicStandardStreamId { get; set; }
        public DateTime date { get; set; }
        public long? typeId { get; set; }
        public View_All_Academic_Section _Academic_Standard_With_Stream { get; set; }
        public List<ClassTimingcls> timingcls { get; set; }
        public List<View_All_Academic_Subject> subjects { get; set; }
        public List<View_All_Teachers> teachers { get; set; }
        public List<View_StudentPaymentData> View_StudentPaymentData { get; set; }
        public List<getMandatoryPrice> getMandatoryPrice { get; set; }
    }
    public class ClassTimingcls
    {
        public long ID { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Start Time")]
        [Required(ErrorMessage = "Please Select Start Time")]
        public TimeSpan StartTime { get; set; }
        [Display(Name = "End Time")]
        [Required(ErrorMessage = "Please Select End Time")]
        public TimeSpan EndTime { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Display(Name = "Available")]
        [Required(ErrorMessage = "Please Select Available Or Not")]
        public bool IsAvailable { get; set; }
        public bool Active { get; set; }
    }
    public class CalenderEventData
    {
        public string groupId { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }

        public bool allDay { get; set; }
        public bool editable { get; set; }
        public string backgroundColor { get; set; }
        public string textColor { get; set; }

        public ExtendedProperties extendedProps { get; set; }


    }
    public class ExtendedProperties
    {
        public ExtendedProperties(long typeId, string type, string name, string date)
        {
            TypeId = typeId;
            Type = type;
            Name = name;
            Date = date;
        }

        public long TypeId { get; set; }

        public string Type { get; set; }
        public string Name { get; set; }

        public string Date { get; set; }
    }
}
