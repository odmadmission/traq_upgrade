﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Query;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.SchoolBellQ.Models
{
    public class TimeTableSQLQuery
    {
        public ApplicationDbContext context;
        public TimeTableSQLQuery(ApplicationDbContext cont)
        {
            context = cont;
        }
        #region Academic Time Table
        public List<View_All_TimeTable_For_Calender> Get_View_All_TimeTable_For_Calenders()
        {
            var res = context.view_All_TimeTable_For_Calenders.FromSql(@"                     select a.AcademicSectionId,a.AcademicStandardId, a.AcademicStandardStreamId, a.ID as typeId,a.NameOfMonth as typeName,(case when CAST(a.AssignDate As date)=GETDATE() then (select 'Current Period :'+f.Name+' (Teacher Name : )'+(e.FirstName+' '+e.LastName) from TimeTablePeriods as l 
                    inner join classTimings as m on l.ClassTimingId=m.ID
                     inner join PeriodEmployees as n on m.ID=n.TimeTablePeriodId 
                     inner join PeriodEmployees as d on l.ID=d.TimeTablePeriodId
                     inner join Employees as e on d.EmployeeId=e.ID
                     inner join Subjects as f on d.SubjectId=f.ID
                     where l.Active=1 and m.Active=1 and n.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and d.IsAvailable=1 and l.AcdemicTimeTableId=a.ID and CONVERT(TIME, GETDATE()) BETWEEN m.StartTime AND m.EndTime) else 'Alloted' end) as title,
                     a.AssignDate as start,'#82FA58' as backgroundColor,'#000000' as textColor 
                      from AcademicTimeTables as a
                     inner join TimeTablePeriods as b on a.ID=b.AcdemicTimeTableId
                     inner join classTimings as c on b.ClassTimingId=c.ID 
                     where a.Active=1 and b.Active=1 and c.Active=1 ").AsNoTracking().ToList();
            return res;
        }
        public List<View_Timetable> GetTimeTableList(long AcademicSectionId, long AcademicStandardId)
        {
            var res = context.view_Timetables.FromSql(@"SELECT 
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.StartTime, 108), 103) as start,
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.EndTime, 108), 103) as [end],
            CONCAT(f.Name,' by ',e.FirstName,' ',e.LastName,'-',e.EmpCode) as title,
            cast(1 as bit) as allDay,cast(1 as bit) as editable,cast('#82FA58' as varchar(100))as backgroundColor,
            cast('#000000' as varchar(100))as textColor
              FROM [dbo].[PeriodEmployees] as a
              inner join [dbo].[TimeTablePeriods] as b on a.TimeTablePeriodId=b.ID
              inner join [dbo].[AcademicTimeTables] as c on b.AcdemicTimeTableId=c.ID
              inner join [dbo].[classTimings] as d on b.[ClassTimingId]=d.ID
			  inner join [dbo].[Teachers] as g on a.EmployeeId=g.ID
              inner join [dbo].[Employees] as e on g.EmployeeId=e.ID
			  inner join [dbo].[AcademicSubjects] as h on a.SubjectId=h.ID
               inner join [dbo].[Subjects] as f on h.SubjectId=f.ID
               where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and 
               c.AcademicSectionId=({0}) and c.AcademicStandardId=({1})", AcademicSectionId, AcademicStandardId).AsNoTracking().ToList();
            return res;
        }
        public List<View_Timetable> GetTeacherTimeTableList(long AcademicSectionId, long AcademicStandardId, long? AcademicStandardStreamId)
        {
            var res = context.view_Timetables.FromSql(@"            SELECT 
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.StartTime, 108), 103) as start,
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.EndTime, 108), 103) as [end],
            CONCAT(f.Name,' by ',e.FirstName,' ',e.LastName,'-',e.EmpCode) as title,
            cast(1 as bit) as allDay,cast(1 as bit) as editable,cast('#82FA58' as varchar(100))as backgroundColor,
            cast('#000000' as varchar(100))as textColor
              FROM [dbo].[PeriodEmployees] as a
              inner join [dbo].[TimeTablePeriods] as b on a.TimeTablePeriodId=b.ID
              inner join [dbo].[AcademicTimeTables] as c on b.AcdemicTimeTableId=c.ID
              inner join [dbo].[classTimings] as d on b.[ClassTimingId]=d.ID
              inner join [dbo].[Employees] as e on a.EmployeeId=e.ID
               inner join [dbo].[Subjects] as f on a.SubjectId=f.ID
               where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 
               and f.Active=1 and c.AcademicSectionId=({0}) and c.AcademicStandardId=({1}) 
               and c.AcademicStandardStreamId=({2})", AcademicSectionId, AcademicStandardId, AcademicStandardStreamId).AsNoTracking().ToList();
            return res;
        }
        public List<View_Timetable> GetForTeacherTimeTableList(long AcademicSectionId, long AcademicStandardId, long userId)
        {
            var res = context.view_Timetables.FromSql(@"SELECT Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.StartTime, 108), 103) as start,
            Convert(datetime, Convert(char(10), c.AssignDate, 103) + ' ' + Convert(char(8), d.EndTime, 108), 103) as [end],
            CONCAT(f.Name,' by ',e.FirstName,' ',e.LastName,'-',e.EmpCode) as title,
            cast(1 as bit) as allDay,cast(1 as bit) as editable,cast('#82FA58' as varchar(100))as backgroundColor,
            cast('#000000' as varchar(100))as textColor
              FROM [dbo].[PeriodEmployees] as a
              inner join [dbo].[TimeTablePeriods] as b on a.TimeTablePeriodId=b.ID
              inner join [dbo].[AcademicTimeTables] as c on b.AcdemicTimeTableId=c.ID
              inner join [dbo].[classTimings] as d on b.[ClassTimingId]=d.ID
			  inner join [dbo].[Teachers] as g on a.EmployeeId=g.ID
              inner join [dbo].[Employees] as e on g.EmployeeId=e.ID
			  inner join [dbo].[AcademicSubjects] as h on a.SubjectId=h.ID
               inner join [dbo].[Subjects] as f on h.SubjectId=f.ID
               where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 
                and f.Active=1 and c.AcademicSectionId=({0}) and c.AcademicStandardId=({1}) and g.EmployeeId=({2})", AcademicSectionId, AcademicStandardId, userId).AsNoTracking().ToList();
            return res;
        }
        public List<View_All_TimeTable_Subjects> Get_View_All_TimeTable_Subjects()
        {
            var res = context.view_All_TimeTable_Subjects.FromSql(@"select a.AcademicSectionId,a.AcademicStandardId,d.SubjectId,d.EmployeeId as TeacherId,a.AssignDate,b.ClassTimingId,a.NameOfMonth,b.AcdemicTimeTableId,b.ID,b.NameOfDay,c.Name as classtime,c.StartTime,c.EndTime,(e.FirstName+' '+e.LastName) as TeacherName,f.Name as SubjectName,b.InsertedDate,b.ModifiedDate,
                (case when d.IsForwared=1 then (select FirstName+' '+LastName+'-'+EmpCode from Employees where ID=d.EmployeeId) else 'NA' end) as ForwardedBy,d.ID as PeriodEmployeeID,
                g.TakenEmployeeId,(h.FirstName+' '+h.LastName+'-'+h.EmpCode) as TakenEmployeeName,
                g.Reason,i.AcademicChapterId,i.AcademicConceptId,i.AcademicSyllabusId,i.PeriodEmployeeStatusId,j.chaptername,j.chapterId,k.conceptId,k.conceptname,l.syllabusId,l.syllabusname,e.ID as EmployeeId

                from TimeTablePeriods as b
                inner join AcademicTimeTables as a on b.AcdemicTimeTableId=a.ID
                inner join classTimings as c on b.ClassTimingId=c.ID 
                inner join PeriodEmployees as d on b.ID=d.TimeTablePeriodId
                inner join Teachers as t on d.EmployeeId=t.ID
                inner join Employees as e on t.EmployeeId=e.ID
				inner join AcademicSubjects as s on d.SubjectId=s.ID
                inner join Subjects as f on s.SubjectId=f.ID
                left join [dbo].[PeriodEmployeeStatuses] as g on d.ID=g.PeriodEmployeeId and g.Active=1
                left join Employees as h on g.TakenEmployeeId=h.ID and h.Active=1
                left join [dbo].[PeriodSyllabus] as i on g.ID=i.PeriodEmployeeStatusId and i.Active=1
                left join (select x.ID as academicchapterId,y.Name as chaptername,y.ID as chapterId from AcademicChapters as x inner join Chapters as y on x.ChapterId=y.ID where x.Active=1 and y.Active=1) as j on i.AcademicChapterId=j.academicchapterId
                left join (select x.ID as academicconceptId,y.Name as conceptname,y.ID as conceptId from AcademicConcepts as x inner join Concepts as y on x.ConceptId=y.ID where x.Active=1 and y.Active=1) as k on i.AcademicConceptId=k.academicconceptId
                left join (select x.ID as academicsyllabusId,y.Title as syllabusname,y.ID as syllabusId from AcademicSyllabus as x inner join Syllabus as y on x.SyllabusId=y.ID where x.Active=1 and y.Active=1) as l on i.AcademicSyllabusId=l.academicsyllabusId
                where b.Active=1 and t.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and d.IsAvailable=1 and a.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Academic_Section> Get_View_All_Academic_Section()
        {
            var res = context.view_All_Academic_Sections.FromSql(@"SELECT a.ID,a.AcademicStandardId,a.SectionId,b.BoardStandardId,b.OrganizationAcademicId,
            l.ID as AcademicStandardWingsID,c.Name as Sectionname,d.BoardId,d.StandardId,e.AcademicSessionId,e.OrganizationId,f.WingID,
            g.DisplayName as Sessionname,g.Start,g.[End],h.GroupID,h.Name as Organisationname,i.Name as Wingname,j.Name as Boardname,k.Name as Standardname,
          l.BoardStandardWingID
 
              FROM [dbo].[AcademicSections] as a
              inner join [dbo].[AcademicStandards] as b on a.AcademicStandardId=b.ID
              inner join [dbo].Sections as c on a.SectionId=c.ID
              inner join [dbo].BoardStandards as d on b.BoardStandardId=d.ID
              inner join [dbo].OrganizationAcademics as e on b.OrganizationAcademicId=e.ID
              inner join [dbo].AcademicSessions as g on e.AcademicSessionId=g.ID
              inner join [dbo].Organizations as h on e.OrganizationId=h.ID
			  inner join [dbo].academicStandardWings as l on b.ID=l.AcademicStandardId
			  inner join [dbo].[BoardStandardWings] as f on l.BoardStandardWingID=f.ID
              inner join [dbo].Wing as i on f.WingID=i.ID
              inner join [dbo].Boards as j on d.BoardId=j.ID
              inner join [dbo].Standards as k on d.StandardId=k.ID
            where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1 and
             j.Active=1 and k.Active=1 and l.Active=1 and g.IsAvailable=1 and a.IsAvailable=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Academic_Subject> Get_View_All_Academic_Subject()
        {
            var res = context.view_All_Academic_Subjects.FromSql(@"select a.AcademicSessionId,a.ID as AcademicSubjectsID,a.SubjectId,b.BoardStandardId,b.Name as Subjectname,
                b.SubjectCode  
                from [dbo].[AcademicSubjects] as a
                inner join [dbo].[Subjects] as b on a.SubjectId=b.ID
                where a.Active=1 and b.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Teachers> Get_View_All_Teachers()
        {
            var res = context.view_All_Teachers.FromSql(@"select e.ID,e.EmployeeId,f.EndDate,e.GroupId,f.StartDate,e.OrganizationId,(g.FirstName+' '+g.LastName) as Teachername,g.EmpCode,g.Isleft,(case when f.EndDate is null then 'A' else (case when (CONVERT(date,f.EndDate)>=CONVERT(date,GETDATE())) then 'A' else 'I' end) end) as Teacherstatus,
a.AcademicSectionId,a.AcademicSubjectId,a.ID as AcademicTeacherSubjectsId, (case when b.EndDate is null then 'A' else (case when (CONVERT(date,b.EndDate)>=CONVERT(date,GETDATE())) then 'A' else 'I' end) end) as TeacherSubjectstatus,
c.ID as AcademicSubjectsId,c.SubjectId,d.Name as SubjectName, d.BoardStandardId

from [dbo].[AcademicTeacherSubjects] as a
inner join [dbo].[AcademicTeacherSubjectsTimeLines] as b on a.ID=b.AcademicTeacherSubjectId
inner join [dbo].[AcademicSubjects] as c on a.AcademicSubjectId=c.ID
inner join [dbo].[Subjects] as d on c.SubjectId=d.ID
inner join [dbo].[Teachers] as e on a.TeacherId=e.ID
inner join [dbo].[TeacherTimeLines] as f on e.ID=f.TeacherID
inner join [dbo].[Employees] as g on e.EmployeeId=g.ID
 where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Teachers_Subjects> Get_View_All_Subject_By_TeacherId()
        {
            var res =context.view_All_Teachers_Subjects.FromSql(@"select a.ID,a.AcademicSectionId,a.AcademicSubjectId,a.TeacherId,b.AcademicSessionId,b.SubjectId,c.BoardStandardId,c.Name as SubjectName,c.SubjectCode,k.DisplayName as SessionName,
d.EmployeeId,d.GroupId,d.OrganizationId,(e.FirstName+' '+e.LastName) as EmployeeName,e.EmpCode,f.BoardId,f.StandardId,h.OrganizationAcademicId,i.Name as BoardName,j.Name as StandardName,
l.Name as OrganizationName,h.ID as AcademicStandardId, n.Name as SectionName
from [dbo].[AcademicTeacherSubjects] as a
inner join [dbo].AcademicSections as m on a.AcademicSectionId=m.ID
inner join [dbo].Sections as n on m.SectionId=n.ID
inner join [dbo].[AcademicSubjects] as b on a.AcademicSubjectId=b.ID
inner join [dbo].[Subjects] as c on b.SubjectId=c.ID
inner join [dbo].[Teachers] as d on a.TeacherId=d.ID
inner join [dbo].[Employees] as e on d.EmployeeId=e.ID
inner join [dbo].[AcademicStandards] as h on m.AcademicStandardId=h.ID
inner join [dbo].[BoardStandards] as f on h.[BoardStandardId]=f.ID

inner join [dbo].[OrganizationAcademics] as g on h.OrganizationAcademicId=g.ID
inner join [dbo].AcademicSessions as k on g.AcademicSessionId=k.ID
inner join [dbo].[Organizations] as l on g.OrganizationId=l.ID
inner join [dbo].[Boards] as i on f.BoardId=i.ID
inner join [dbo].[Standards] as j on f.StandardId=j.ID
where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1 and 
j.Active=1 and k.Active=1 and k.IsAvailable=1 and l.Active=1 and m.Active=1 and n.Active=1").AsNoTracking().ToList();
            return res;
        }
        #endregion
    }
}
