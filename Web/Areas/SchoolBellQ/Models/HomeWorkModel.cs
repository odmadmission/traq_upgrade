﻿using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.Entities;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.SchoolBellQ.Models
{
    public class HomeWorkModel:BaseModel
    {
      
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter Description")]
        public string Description { get; set; }
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long AcademicStandardId { get; set; }
        public bool StandardStreamType { get; set; }
        public string StandardName { get; set; }

        [Display(Name = "Oraganisation Academic")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }        
       
        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Please Select Subject")]
        public long AcademicSubjectId { get; set; }
        public DateTime HomeWorkDate { get; set; }
        public string OraganisationAcademicName { get; set; }
        public string OraganisationName { get; set; }

        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        [Display(Name = "School Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }      
        public IEnumerable<AcademicSession> academicSessions { get; set; }
        public IEnumerable<Board> boards { get; set; }       
        public long AcademicStandardStreamId { get; set; }      
        public long AcademicSectionId { get; set; }
        public long AcademicChapterId { get; set; }

        public long ddlteacher { get; set; }
        public IFormFile AttachmentFile { get; set; }
        public string Attachment { get; set; }
        public string ChapterName { get; set; }
        public long AcademicHomeworkId { get; set; }
    }
    public class ResponseHomeworkModel
    {
        public IEnumerable<AcademicSession> academicSessions { get; set; }
        public IEnumerable<Board> boards { get; set; }
        public IEnumerable<HomeWorkModel> HomeWorklist { get; set; }
    }
}
