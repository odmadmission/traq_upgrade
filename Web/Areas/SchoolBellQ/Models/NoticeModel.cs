﻿using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Entities.SupportAggregate;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.SchoolBellQ.Models
{
    public class UserGroupModel:BaseModel
    {
        public long? UserGroupID { get; set; }
        public long EmployeeId { get; set; }
        public long[] MultiEmployeeId { get; set; }
        [Display(Name = "GroupName")]
        [Required(ErrorMessage = "Please Enter GroupName")]
        public string GroupName { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please Enter Description")]
        public string Description { get; set; }      
        public IFormFile Icon { get; set; }  
        public Nullable<DateTime> StartDate { get; set; }     
        public Nullable<DateTime>  EndDate { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeLastName { get; set; }
        public string EmployeePhone { get; set; }
        public string EmployeeCode { get; set; }
        public string stringStartDate { get; set; }
        public string stringEndDate { get; set; }
        public string Imageicon { get; set; }
        public List<Employee> employeelist { get; set; }
        public long EmployeeCount { get; set; }
        public string EmployeeImage { get; set; }

    }
    public class ResponseUserGroupModel : BaseModel
    {
        public List<Employee> employeelist { get; set; }
        public IEnumerable<UserGroupModel> UserGrouplist { get; set; }
        public IEnumerable<UserGroupModel> UserGroupEmployeelist { get; set; }
        
    }
    public class NoticeModel:BaseModel
    {       
        public string SendType { get; set; }       
        public string SendSubType { get; set; }       
        public string MessageType { get; set; }     
        public string Message { get; set; }    
        public long SendTo { get; set; }
        public long CountSend { get; set; }
        public IEnumerable<AcademicSession> academicSessions { get; set; }
        public IEnumerable<Board> boards { get; set; }
        public IEnumerable<UserGroup> groupList { get; set; }
        public IEnumerable<EmployeeDesignationModel> departmentList { get; set; }
        public IEnumerable<Organization> organisationList { get; set; }

        public long[] Departmentid { get; set; }
        public long [] EmployeeGroupid { get; set; }
        public string Employee_SendTo { get; set; }
        public long[] AcademicStudentId { get; set; }
        public long AcademicSessionId { get; set; }
        public long OraganisationAcademicId { get; set; }
        public long BoardId { get; set; }
        public long AcademicStandardId { get; set; }
        public long? AcademicStandarStreamdId { get; set; }
        public long AcademicSectiondId { get; set; }
        public long SchoolWingId { get; set; }
        public List<NoticeDataModel> noticedatalist { get; set; }
        public long[] Organisationid { get; set; }

    }
    public class NoticeDataModel:BaseModel
    {
        public string SendType { get; set; }
        public string SendSubType { get; set; }
        public long NoticeId { get; set; }
        public string StudentMobile { get; set; }
        public long StudentId { get; set; }
        public long EmployeeId { get; set; }
        public string EmpMobile { get; set; }
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string Message { get; set; }
        public string StudentName { get; set; }
    }
    public class DepartmentModel
    {
        public long DepartmentID { get; set; }
        public long GroupID { get; set; }
        public string Name { get; set; }

    }

    public class EmployeeModel
    {
        public long ID { get; set; }
        public long EmployeeId { get; set; }       
        public long DesignationId { get; set; }
        public long DepartmentID { get; set; }   
    }
    public class StudentModel
    {
        public long id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        

    }
    public class EmployeeDesignationModel
    {
        public long ID { get; set; }
        public long EmployeeID { get; set; }        
        public long DesignationId { get; set; }
      
        public long DesignationLevelID { get; set; }       
        public long DepartmentID { get; set; }       
        public long OrganizationID { get; set; }       
        public long GroupID { get; set; }
        public string DesignationName { get; set; }
        public string DesignationLevelName { get; set; }
        public string DepartmentName { get; set; }
        public string OrganizationName { get; set; }
        public string GroupName { get; set; }
        public DateTime ModifiedDate { get; set; }      
        public string DepartmentType { get; set; }

    }
}
