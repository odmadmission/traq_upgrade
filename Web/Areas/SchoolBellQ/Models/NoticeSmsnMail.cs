﻿using Microsoft.AspNetCore.Hosting;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.SchoolBellQ.Models
{
    public class NoticeSmsnMail
    {
        private smssend smssend { get; set; }
        private readonly IHostingEnvironment hostingEnvironment;
        public NoticeSmsnMail(smssend sms, IHostingEnvironment hostingEnv)
        {
            smssend = sms;
            hostingEnvironment = hostingEnv;
        }
        public string SendNoticeToMail( string noticemessage, string name, string email, string phone, string code,  string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "notice.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Notice}}", noticemessage).Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{email}}", email).Replace("{{phone}}", phone);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
    }
}
