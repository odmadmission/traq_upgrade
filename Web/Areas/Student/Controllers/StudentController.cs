﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Areas.Student.Models;
using OfficeOpenXml;
using Web.Controllers;

namespace OdmErp.Web.Areas.Student.Controllers
{
    [Area("Student")]
    public class StudentController : AppController
    {
        private readonly IFileProvider fileProvider;
        private readonly IHostingEnvironment hostingEnvironment;

        private IAddressRepository addressRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private ICityRepository cityRepository;
        private IStateRepository stateRepository;
        private ICountryRepository countryRepository;
        private IReligionTypeRepository religionTypeRepository;
        private IStudentAggregateRepository studentAggregateRepository;
        private IAccessRepository accessRepository;
        private IRoleRepository roleRepository;
        public Web.Models.CommonMethods commonMethods;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IOrganizationRepository organizationRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IBoardStandardRepository BoardStandardRepository;
        public IStandardRepository StandardRepository;
        public IAcademicSessionRepository academicSessionRepository;
        //public IAcademicStandardStreamRepository academicStandardStreamRepository;
        public IBoardRepository boardRepository;
        public IStreamRepository streamRepository;
        //public IBoardStandardStreamRepository boardStandardStreamRepository;
        public IAcademicStudentRepository academicStudentRepository;
        public IAcademicSectionRepository academicSectionRepository;
        public ISectionRepository sectionRepository;
        public IAcademicSectionStudentRepository academicSectionStudentRepository;

        public IAcademicCalenderRepository academicCalenderRepository;
        public IAcademicTimeTableRepository academicTimeTableRepository;
        public ITimeTablePeriodRepository timeTablePeriodRepository;
        public IPeriodEmployeeRepository periodEmployeeRepository;
        public IPeriodEmployeeStatusRepository periodEmployeeStatusRepository;
        public IPeriodSyllabusRepository periodSyllabusRepository;
        public IClassTimingRepository classTimingRepository;
        public IAcademicSubjectRepository academicSubjectRepository;
        public IAcademicChapterRepository academicChapterRepository;
        public IAcademicConceptRepository academicConceptRepository;
        public IChapterRepository chapterRepository;
        public IConceptRepository conceptRepository;
        public ISyallbusRepository syallbusRepository;
        public IAcademicSyllabusRepository academicSyllabusRepository;
        private IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IAcademicSubjectOptionalRepository academicSubjectOptionalRepository;
        private IAcademicSubjectOptionalDetailsRepository academicSubjectOptionalDetailsRepository;
        private Models.StudentModelClass studentModelClass;
        private StudentMethod studentMethod;
        private StudentServices studentServices;
        private StudentSmsnMails studentSmsnMails;
        private StudentSqlQuery sqlQuery;
        private IStudentLanguagesKnownRepository studentLanguagesKnownRepository;
        public ICategoryRepository categoryRepository;
        public ISubjectRepository SubjectRepository;
        public ITeacherRepository teacherRepository;
        public IEmployeeRepository employeeRepository;
        public ILanguageRepository languageRepository;
        public StudentController(IAcademicSubjectOptionalRepository academicSubjectOptionalRepo,
           IAcademicSubjectOptionalDetailsRepository academicSubjectOptionalDetailsRepo, StudentSqlQuery sqlQ, IFileProvider fileprovider, IHostingEnvironment env, IStudentAggregateRepository studentAggregateRepo, IBloodGroupRepository bloodGroupRepo, INationalityTypeRepository nationalityTypeRepo,
          IAddressTypeRepository addressTypeRepo, ICityRepository cityRepo, IStateRepository stateRepo, ICountryRepository countryRepo, IReligionTypeRepository religionTypeRepo, Web.Models.CommonMethods common, IAddressRepository addressRepo, IAccessRepository accessRepo,
          IRoleRepository roleRepo, IOrganizationAcademicRepository organizationAcademicRepo, IOrganizationRepository organizationRepo, IAcademicStandardRepository academicStandardRepo, IBoardStandardRepository BoardStandardRepo, IStandardRepository StandardRepo,
          IAcademicSessionRepository academicSessionRepo, IBoardRepository boardRepo, IStreamRepository streamRepo, IAcademicStudentRepository academicStudentRepo,
          IAcademicSectionRepository academicSectionRepo, ISectionRepository sectionRepo, IAcademicSectionStudentRepository academicSectionStudentRepo,
          IAcademicCalenderRepository academicCalenderRepo, IAcademicChapterRepository academicChapterRepo, ICategoryRepository categoryRepository,
      IAcademicTimeTableRepository academicTimeTableRepo, IAcademicConceptRepository academicConceptRepo,
      IPeriodEmployeeRepository periodEmployeeRepo, IAcademicSubjectRepository academicSubjectRepo, IAcademicSyllabusRepository academicSyllabusRepo,
          IPeriodEmployeeStatusRepository periodEmployeeStatusRepo, IChapterRepository chapterRepo, IConceptRepository conceptRepo, ISyallbusRepository syallbusRepo,
          IPeriodSyllabusRepository periodSyllabusRepo, IClassTimingRepository classTimingRepo, ITimeTablePeriodRepository timeTablePeriodReo, IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepo,
          IDocumentTypeRepository documentTypeRepo, Models.StudentModelClass studentModel, StudentSmsnMails studentSmsn, StudentServices studentServ, StudentMethod studentMet, IDocumentSubTypeRepository documentSubTypeRepo, ISubjectRepository subjectRepository, IEmployeeRepository employeeRepository, ITeacherRepository teacherRepository,
          ILanguageRepository languageRepo, IStudentLanguagesKnownRepository studentLanguagesKnownRepo)
        {
            academicSubjectOptionalRepository = academicSubjectOptionalRepo;
            academicSubjectOptionalDetailsRepository = academicSubjectOptionalDetailsRepo;
            sqlQuery = sqlQ;
            studentLanguagesKnownRepository = studentLanguagesKnownRepo;
            languageRepository = languageRepo;
            studentServices = studentServ;
            studentSmsnMails = studentSmsn;
            studentModelClass = studentModel;
            studentMethod = studentMet;
            academicSectionStudentRepository = academicSectionStudentRepo;
            academicSectionRepository = academicSectionRepo;
            sectionRepository = sectionRepo;
            fileProvider = fileprovider;
            hostingEnvironment = env;
            BoardStandardRepository = BoardStandardRepo;
            studentAggregateRepository = studentAggregateRepo;
            bloodGroupRepository = bloodGroupRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            addressTypeRepository = addressTypeRepo;
            cityRepository = cityRepo;
            stateRepository = stateRepo;
            countryRepository = countryRepo;
            religionTypeRepository = religionTypeRepo;
            commonMethods = common;
            addressRepository = addressRepo;
            accessRepository = accessRepo;
            roleRepository = roleRepo;
            organizationAcademicRepository = organizationAcademicRepo;
            organizationRepository = organizationRepo;
            academicStandardRepository = academicStandardRepo;
            StandardRepository = StandardRepo;
            academicSessionRepository = academicSessionRepo;
            boardRepository = boardRepo;
            //academicStandardStreamRepository = academicStandardStreamRepo;
            //boardStandardStreamRepository = boardStandardStreamRepo;
            streamRepository = streamRepo;
            academicStudentRepository = academicStudentRepo;
            academicCalenderRepository = academicCalenderRepo;
            academicTimeTableRepository = academicTimeTableRepo;
            periodEmployeeRepository = periodEmployeeRepo;
            periodEmployeeStatusRepository = periodEmployeeStatusRepo;
            periodSyllabusRepository = periodSyllabusRepo;
            classTimingRepository = classTimingRepo;
            timeTablePeriodRepository = timeTablePeriodReo;
            academicSubjectRepository = academicSubjectRepo;
            academicChapterRepository = academicChapterRepo;
            academicConceptRepository = academicConceptRepo;
            chapterRepository = chapterRepo;
            conceptRepository = conceptRepo;
            syallbusRepository = syallbusRepo;
            academicSyllabusRepository = academicSyllabusRepo;
            employeeRequiredDocumentRepository = employeeRequiredDocumentRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            this.categoryRepository = categoryRepository;
            SubjectRepository = subjectRepository;
            this.teacherRepository = teacherRepository;
            this.employeeRepository = employeeRepository;
        }
        #region ---------------------Student----------------------

        public class Gettogglecls
        {
            public bool? IsAadharNO { get; set; }
            public bool? IsBloodGroupID { get; set; }
            public bool? IsCategoryID { get; set; }
            public bool? IsDateOfBirth { get; set; }
            public bool? IsEmailID { get; set; }
            public bool? IsemergencyContact { get; set; }
            public bool? IsFirstName { get; set; }
            public bool? IsGender { get; set; }
            public bool? IsLanguagesKnowns { get; set; }
            public bool? IsLastName { get; set; }
            public bool? IsMiddleName { get; set; }
            public bool? IsMotherTongueID { get; set; }
            public bool? IsNatiobalityID { get; set; }
            public bool? IsPassportNO { get; set; }
            public bool? IsPrimaryMobile { get; set; }
            public bool? IsProfileImage { get; set; }
            public bool? IsReligionID { get; set; }
            public bool? IsStudentAcademicAccess { get; set; }
            public bool? IsstudentAdresses { get; set; }
            public bool? IsstudentClassDetails { get; set; }
            public bool? IsstudentParentsCls { get; set; }
            public bool? Istransport { get; set; }
            public bool? IsWhatsappMobile { get; set; }
            public bool? IsZoomMailID { get; set; }
            public bool? IsStudentCode { get; set; }
        }

        [HttpGet]
        public async Task<IActionResult> Index(Gettogglecls model)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            Models.StudentModelClass studentcls = new Models.StudentModelClass();

            studentcls.IsAadharNO = model.IsAadharNO == null ? true : model.IsAadharNO.Value;
            studentcls.IsBloodGroupID = model.IsBloodGroupID == null ? true : model.IsBloodGroupID.Value;
            studentcls.IsCategoryID = model.IsCategoryID == null ? true : model.IsCategoryID.Value;
            studentcls.IsDateOfBirth = model.IsDateOfBirth == null ? true : model.IsDateOfBirth.Value;
            studentcls.IsEmailID = model.IsEmailID == null ? true : model.IsEmailID.Value;
            studentcls.IsemergencyContact = model.IsemergencyContact == null ? true : model.IsemergencyContact.Value;
            studentcls.IsFirstName = model.IsFirstName == null ? true : model.IsFirstName.Value;
            studentcls.IsGender = model.IsGender == null ? true : model.IsGender.Value;
            studentcls.IsLanguagesKnowns = model.IsLanguagesKnowns == null ? true : model.IsLanguagesKnowns.Value;
            studentcls.IsLastName = model.IsLastName == null ? true : model.IsLastName.Value;
            studentcls.IsMiddleName = model.IsMiddleName == null ? true : model.IsMiddleName.Value;
            studentcls.IsMotherTongueID = model.IsMotherTongueID == null ? true : model.IsMotherTongueID.Value;
            studentcls.IsNatiobalityID = model.IsNatiobalityID == null ? true : model.IsNatiobalityID.Value;
            studentcls.IsPassportNO = model.IsPassportNO == null ? true : model.IsPassportNO.Value;
            studentcls.IsPrimaryMobile = model.IsPrimaryMobile == null ? true : model.IsPrimaryMobile.Value;
            studentcls.IsProfileImage = model.IsProfileImage == null ? true : model.IsProfileImage.Value;
            studentcls.IsReligionID = model.IsReligionID == null ? true : model.IsReligionID.Value;
            studentcls.IsStudentAcademicAccess = model.IsStudentAcademicAccess == null ? true : model.IsStudentAcademicAccess.Value;
            studentcls.IsstudentAdresses = model.IsstudentAdresses == null ? true : model.IsstudentAdresses.Value;
            studentcls.IsstudentClassDetails = model.IsstudentClassDetails == null ? true : model.IsstudentClassDetails.Value;
            studentcls.IsstudentParentsCls = model.IsstudentParentsCls == null ? true : model.IsstudentParentsCls.Value;
            studentcls.Istransport = model.Istransport == null ? true : model.Istransport.Value;
            studentcls.IsWhatsappMobile = model.IsWhatsappMobile == null ? true : model.IsWhatsappMobile.Value;
            studentcls.IsZoomMailID = model.IsZoomMailID == null ? true : model.IsZoomMailID.Value;
            studentcls.IsStudentCode = model.IsStudentCode == null ? true : model.IsStudentCode.Value;

            studentcls.addressTypes = addressTypeRepository.GetAllAddressType().Where(a => a.Active == true).ToList();
            studentcls.relationshipTypes = studentAggregateRepository.GetAllParentRelationshipType().Where(a => a.Active == true).ToList();

            //studentcls.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
            //studentcls.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
            //studentcls.religionTypes = religionTypeRepository.GetAllReligionTypes();
            //studentcls.categoryTypes = categoryRepository.ListAllAsyncIncludeAll().Result;
            studentcls.StudentList = studentMethod.GetStudentDetailsList();



            HttpContext.Session.SetString("studentlist", JsonConvert.SerializeObject(studentcls));



            ViewBag.status = "Create";
            return View(studentcls);


        }
        [HttpGet]
        public async Task<IActionResult> DownloadStudentList()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            Models.StudentModelClass studentlist = new Models.StudentModelClass();
            if (HttpContext.Session.GetString("studentlist") != null)
            {
                var attach = HttpContext.Session.GetString("studentlist");

                studentlist = JsonConvert.DeserializeObject<Models.StudentModelClass>(attach);
            }
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            #region Columns
            if (studentlist.IsStudentCode == true)
            {
                validationTable.Columns.Add("Student Code");
            }
            else
            {

            }
            if (studentlist.IsFirstName == true)
            {
                validationTable.Columns.Add("First Name");
            }
            else
            {

            }
            if (studentlist.IsMiddleName == true)
            {
                validationTable.Columns.Add("Middle Name");
            }
            else
            {

            }
            if (studentlist.IsLastName == true)
            {
                validationTable.Columns.Add("Last Name");
            }
            else
            {

            }
            if (studentlist.IsPrimaryMobile == true)
            {
                validationTable.Columns.Add("Primary Mobile");
            }
            else
            {

            }
            if (studentlist.IsEmailID == true)
            {
                validationTable.Columns.Add("Email ID");
            }
            else
            {

            }
            if (studentlist.IsWhatsappMobile == true)
            {
                validationTable.Columns.Add("Whatsapp Mobile");
            }
            else
            {

            }
            if (studentlist.IsZoomMailID == true)
            {
                validationTable.Columns.Add("Zoom MailID");
            }
            else
            {

            }
            if (studentlist.IsAadharNO == true)
            {
                validationTable.Columns.Add("Aadhar No");
            }
            else
            {

            }
            if (studentlist.IsBloodGroupID == true)
            {
                validationTable.Columns.Add("Blood Group");
            }
            else
            {

            }
            if (studentlist.IsCategoryID == true)
            {
                validationTable.Columns.Add("Category");
            }
            else
            {

            }
            if (studentlist.IsDateOfBirth == true)
            {
                validationTable.Columns.Add("DOB");
            }
            else
            {

            }
            if (studentlist.IsGender == true)
            {
                validationTable.Columns.Add("Gender");
            }
            else
            {

            }
            if (studentlist.IsMotherTongueID == true)
            {
                validationTable.Columns.Add("Mother Tongue");
            }
            else
            {

            }
            if (studentlist.IsLanguagesKnowns == true)
            {
                validationTable.Columns.Add("Languages Known");
            }
            else
            {

            }
            if (studentlist.IsNatiobalityID == true)
            {
                validationTable.Columns.Add("Nationality");
            }
            else
            {

            }
            if (studentlist.IsPassportNO == true)
            {
                validationTable.Columns.Add("Passport No");
            }
            else
            {

            }
            if (studentlist.IsReligionID == true)
            {
                validationTable.Columns.Add("Religion");
            }
            else
            {

            }
            if (studentlist.IsemergencyContact == true)
            {
                validationTable.Columns.Add("Contact Person Name");
                validationTable.Columns.Add("Contact Person Mobile");
                validationTable.Columns.Add("Contact Person Email ID");
            }
            else
            {

            }
            if (studentlist.IsstudentClassDetails == true)
            {
                validationTable.Columns.Add("Session");
                validationTable.Columns.Add("Organisation");
                validationTable.Columns.Add("Board");
                validationTable.Columns.Add("Standard");
                validationTable.Columns.Add("Section");
                validationTable.Columns.Add("Wing");
                validationTable.Columns.Add("Subject Group");
                validationTable.Columns.Add("Optional Subject");
            }
            else
            {

            }
            if (studentlist.IsstudentAdresses == true)
            {
                if (studentlist.addressTypes.Count > 0)
                {
                    foreach (var a in studentlist.addressTypes)
                    {
                        validationTable.Columns.Add(a.Name);
                    }
                }
            }
            else
            {

            }
            if (studentlist.IsstudentParentsCls == true)
            {
                if (studentlist.relationshipTypes.Count > 0)
                {
                    foreach (var a in studentlist.relationshipTypes)
                    {
                        validationTable.Columns.Add(a.Name);
                    }
                }
            }
            else
            {

            }
            if (studentlist.Istransport == true)
            {
                validationTable.Columns.Add("Required Transport");
                validationTable.Columns.Add("Vehicle Type");
                validationTable.Columns.Add("Vehicle No");
                validationTable.Columns.Add("City");
                validationTable.Columns.Add("Distance");
                validationTable.Columns.Add("Location");
                validationTable.Columns.Add("Departure Time");
            }
            else
            {

            }
            if (studentlist.IsStudentAcademicAccess == true)
            {
                validationTable.Columns.Add("Organization Name");
                validationTable.Columns.Add("Board Name");
                validationTable.Columns.Add("Class Name");
                validationTable.Columns.Add("From Date");
                validationTable.Columns.Add("To Date");
            }
            else
            {

            }
            #endregion
            #region rows
            if (studentlist.StudentList != null)
            {

                foreach (var item in studentlist.StudentList)
                {
                    DataRow dr = validationTable.NewRow();

                    if (studentlist.IsStudentCode == true)
                    {
                        dr["Student Code"] = item.StudentCode;
                    }
                    else
                    {

                    }
                    if (studentlist.IsFirstName == true)
                    {
                        dr["First Name"] = item.FirstName;
                    }
                    else
                    {

                    }
                    if (studentlist.IsMiddleName == true)
                    {
                        dr["Middle Name"] = item.MiddleName;
                    }
                    else
                    {

                    }
                    if (studentlist.IsLastName == true)
                    {
                        dr["Last Name"] = item.LastName;
                    }
                    else
                    {

                    }
                    if (studentlist.IsPrimaryMobile == true)
                    {
                        dr["Primary Mobile"] = item.PrimaryMobile;
                    }
                    else
                    {

                    }
                    if (studentlist.IsEmailID == true)
                    {
                        dr["Email ID"] = item.EmailID;
                    }
                    else
                    {

                    }
                    if (studentlist.IsWhatsappMobile == true)
                    {
                        dr["Whatsapp Mobile"] = item.WhatsappMobile;
                    }
                    else
                    {

                    }
                    if (studentlist.IsZoomMailID == true)
                    {
                        dr["Zoom MailID"] = item.ZoomMailID;
                    }
                    else
                    {

                    }
                    if (studentlist.IsAadharNO == true)
                    {
                        dr["Aadhar No"] = item.AadharNO;
                    }
                    else
                    {

                    }
                    if (studentlist.IsBloodGroupID == true)
                    {
                        dr["Blood Group"] = item.BloodGroupName;
                    }
                    else
                    {

                    }
                    if (studentlist.IsCategoryID == true)
                    {
                        dr["Category"] = item.CategoryName;
                    }
                    else
                    {

                    }
                    if (studentlist.IsDateOfBirth == true)
                    {
                        dr["DOB"] = item.DateOfBirth == null ? "" : item.DateOfBirth.ToString("dd-MM-yyyy");
                    }
                    else
                    {

                    }
                    if (studentlist.IsGender == true)
                    {
                        dr["Gender"] = item.Gender;
                    }
                    else
                    {

                    }
                    if (studentlist.IsMotherTongueID == true)
                    {
                        dr["Mother Tongue"] = item.MotherTongue;
                    }
                    else
                    {

                    }
                    if (studentlist.IsLanguagesKnowns == true)
                    {
                        dr["Languages Known"] = item.LanguagesKnowns;
                    }
                    else
                    {

                    }
                    if (studentlist.IsNatiobalityID == true)
                    {
                        dr["Nationality"] = item.NatiobalityName;
                    }
                    else
                    {

                    }
                    if (studentlist.IsPassportNO == true)
                    {
                        dr["Passport No"] = item.PassportNO;
                    }
                    else
                    {

                    }
                    if (studentlist.IsReligionID == true)
                    {
                        dr["Religion"] = item.ReligionName;
                    }
                    else
                    {

                    }
                    if (studentlist.IsemergencyContact == true)
                    {
                        if (item.emergencyContact != null)
                        {
                            dr["Contact Person Name"] = item.emergencyContact.ContactPersonName;
                            dr["Contact Person Mobile"] = item.emergencyContact.Mobile;
                            dr["Contact Person Email ID"] = item.emergencyContact.EmailId;
                        }
                        else
                        {
                            dr["Contact Person Name"] = "";
                            dr["Contact Person Mobile"] = "";
                            dr["Contact Person Email ID"] = "";
                        }

                    }
                    else
                    {

                    }
                    if (studentlist.IsstudentClassDetails == true)
                    {
                        if (item.studentClassDetails != null)
                        {
                            dr["Session"] = item.studentClassDetails.SectionName;
                            dr["Organisation"] = item.studentClassDetails.OraganisationName;
                            dr["Board"] = item.studentClassDetails.BoardName;
                            dr["Standard"] = item.studentClassDetails.StandardName;
                            dr["Section"] = item.studentClassDetails.SectionName;
                            dr["Wing"] = item.studentClassDetails.WingName;
                            dr["Subject Group"] = item.studentClassDetails.SubjectGroupName;
                            dr["Optional Subject"] = item.studentClassDetails.optionalSubject;
                        }
                        else
                        {
                            dr["Session"] = "";
                            dr["Organisation"] = "";
                            dr["Board"] = "";
                            dr["Standard"] = "";
                            dr["Section"] = "";
                            dr["Wing"] = "";
                            dr["Subject Group"] = "";
                            dr["Optional Subject"] = "";
                        }

                    }
                    else
                    {

                    }
                    if (studentlist.IsstudentAdresses == true)
                    {
                        if (studentlist.addressTypes.Count > 0)
                        {
                            foreach (var a in studentlist.addressTypes)
                            {
                                if (item.studentAdresses != null)
                                {
                                    var address = item.studentAdresses.Where(s => s.AddressTypeID == a.ID).FirstOrDefault();
                                    if (address != null)
                                    {
                                        dr[a.Name] = (address.AddressLine1 + "," + address.AddressLine2 + "," + address.CountryName + "," + address.StateName + "," + address.CityName + "," + address.PostalCode);
                                    }
                                    else
                                    {
                                        dr[a.Name] = "";
                                    }
                                }
                                else
                                {
                                    dr[a.Name] = "";
                                }
                            }
                        }
                    }
                    else
                    {

                    }
                    if (studentlist.IsstudentParentsCls == true)
                    {
                        if (studentlist.relationshipTypes.Count > 0)
                        {
                            foreach (var a in studentlist.relationshipTypes)
                            {
                                if (item.studentParentsCls != null)
                                {
                                    var parent = item.studentParentsCls.Where(s => s.ParentRelationshipID == a.ID).FirstOrDefault();
                                    if (parent != null)
                                    {
                                        dr[a.Name] = "Name :" + ((parent.FirstName + " " + parent.LastName)) + "\n Mobile :" + parent.PrimaryMobile + "\n Email ID :" + parent.EmailId + "\n Organization :" + parent.ParentOrganization + "\n Profession Type :" + parent.ProfessionTypeName;
                                    }
                                    else
                                    {
                                        dr[a.Name] = "";
                                    }
                                }
                                else
                                {
                                    dr[a.Name] = "";
                                }
                            }
                        }
                    }
                    else
                    {

                    }
                    if (studentlist.Istransport == true)
                    {
                        if (item.transport != null)
                        {
                            dr["Required Transport"] = (item.transport.IsTransport == true ? "Yes" : "No");
                            dr["Vehicle Type"] = item.transport.VehicleType;
                            dr["Vehicle No"] = item.transport.VehicleNumber;
                            dr["City"] = item.transport.CityName;
                            dr["Distance"] = item.transport.Distance;
                            dr["Location"] = item.transport.LocationName;
                            dr["Departure Time"] = item.transport.departureName;
                        }
                        else
                        {
                            dr["Required Transport"] = "";
                            dr["Vehicle Type"] = "";
                            dr["Vehicle No"] = "";
                            dr["City"] = "";
                            dr["Distance"] = "";
                            dr["Location"] = "";
                            dr["Departure Time"] = "";
                        }
                    }
                    else
                    {

                    }
                    if (studentlist.IsStudentAcademicAccess == true)
                    {
                        if (item.StudentAcademicAccess.Count > 0)
                        {
                            dr["Organization Name"] = item.StudentAcademicAccess.FirstOrDefault().OrganizationName;
                            dr["Board Name"] = item.StudentAcademicAccess.FirstOrDefault().BoardName;
                            dr["Class Name"] = item.StudentAcademicAccess.FirstOrDefault().ClassName;
                            dr["From Date"] = item.StudentAcademicAccess.FirstOrDefault().FromDate.ToString("dd-MM-yyyy");
                            dr["To Date"] = item.StudentAcademicAccess.FirstOrDefault().ToDate.ToString("dd-MM-yyyy");
                        }
                        else
                        {
                            dr["Organization Name"] = "";
                            dr["Board Name"] = "";
                            dr["Class Name"] = "";
                            dr["From Date"] = "";
                            dr["To Date"] = "";
                        }
                    }
                    else
                    {

                    }
                    validationTable.Rows.Add(dr);
                }
            }
            #endregion
            validationTable.TableName = "Students";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StudentList.xlsx");
        }
        public IActionResult CreateOrEditStudent(long? id)
        {
            CheckLoginStatus();

            StudentModelClass studentModelClass = new StudentModelClass();
            studentModelClass.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
            studentModelClass.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
            studentModelClass.religionTypes = religionTypeRepository.GetAllReligionTypes();
            studentModelClass.categoryTypes = categoryRepository.ListAllAsyncIncludeAll().Result;
            studentModelClass.languages = languageRepository.ListAllAsync().Result.ToList();
            ViewBag.status = "Create";

            if (id != null && id != 0)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                var studentdetails = studentAggregateRepository.GetStudentById(id.Value);

                studentModelClass.AadharNO = studentdetails.AadharNO;
                studentModelClass.BloodGroupID = studentdetails.BloodGroupID;
                studentModelClass.CategoryID = studentdetails.CategoryID;
                studentModelClass.DateOfBirth = studentdetails.DateOfBirth;
                studentModelClass.EmailID = studentdetails.EmailID;
                studentModelClass.FirstName = studentdetails.FirstName;
                studentModelClass.Gender = studentdetails.Gender;
                studentModelClass.LastName = studentdetails.LastName;
                studentModelClass.MiddleName = studentdetails.MiddleName;
                studentModelClass.MotherTongueID = studentdetails.MotherTongueID;
                studentModelClass.NatiobalityID = studentdetails.NatiobalityID;
                studentModelClass.PassportNO = studentdetails.PassportNO;
                studentModelClass.PrimaryMobile = studentdetails.PrimaryMobile;
                studentModelClass.ReligionID = studentdetails.ReligionID;
                studentModelClass.StudentCode = studentdetails.StudentCode;
                studentModelClass.WhatsappMobile = studentdetails.WhatsappMobile;
                studentModelClass.ZoomMailID = studentdetails.ZoomMailID;
                studentModelClass.ID = studentdetails.ID;
                var knownlanguages = studentLanguagesKnownRepository.ListAllAsync().Result.Where(a => a.StudentID == studentdetails.ID && a.Active == true).ToList();
                studentModelClass.languagesknown = knownlanguages.Select(a => a.LanguageID).ToList();


                var webPath = hostingEnvironment.WebRootPath;
                if (studentdetails.Image != null)
                {
                    string path = Path.Combine("/ODMImages/StudentProfile/", studentdetails.Image);
                    ViewBag.Profileimage = path;
                }
                else
                {
                    ViewBag.Profileimage = null;
                }
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("PERSONAL").ID;
                studentModelClass.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                      where a.DocumentTypeID == documentTypeId
                                                      select new Web.Models.documentTypesCls
                                                      {
                                                          TypeId = a.DocumentTypeID,
                                                          ID = a.ID,
                                                          Name = a.Name,
                                                          IsRequired = studentMethod.studentRequiredDocument(studentdetails.ID, a.DocumentTypeID, a.ID, studentdetails.ID),
                                                      }).ToList();
                ViewBag.status = "Update";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                studentModelClass.AadharNO = "";
                studentModelClass.BloodGroupID = 0;
                studentModelClass.CategoryID = 0;
                studentModelClass.EmailID = "";
                studentModelClass.FirstName = "";
                studentModelClass.Gender = "";
                studentModelClass.LastName = "";
                studentModelClass.MiddleName = "";
                studentModelClass.MotherTongueID = 0;
                studentModelClass.NatiobalityID = 0;
                studentModelClass.PassportNO = "";
                studentModelClass.PrimaryMobile = "";
                studentModelClass.ReligionID = 0;
                studentModelClass.StudentCode = "";
                studentModelClass.WhatsappMobile = "";
                studentModelClass.ZoomMailID = "";
                studentModelClass.ID = 0;
                studentModelClass.languagesknown = null;


                ViewBag.Profileimage = null;
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("PERSONAL").ID;
                studentModelClass.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                      where a.DocumentTypeID == documentTypeId
                                                      select new Web.Models.documentTypesCls
                                                      {
                                                          TypeId = a.DocumentTypeID,
                                                          ID = a.ID,
                                                          Name = a.Name,
                                                          IsRequired = false
                                                      }).ToList();
                ViewBag.status = "Create";
            }

            return View(studentModelClass);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentProfile(Models.StudentModelClass obstudent, IFormFile file, List<Web.Models.documentTypesCls> documentTypesCls)
        {

            CheckLoginStatus();
            try
            {

                if (obstudent.ID == 0)
                {
                    var std = studentAggregateRepository.GetAllStudent().Where(a => a.StudentCode == obstudent.StudentCode).FirstOrDefault();
                    if (std != null)
                    {
                        return Json(new { sts = 3 });
                    }
                    OdmErp.ApplicationCore.Entities.Student student = new OdmErp.ApplicationCore.Entities.Student();
                    student.ModifiedId = userId;
                    student.Active = true;
                    student.AadharNO = obstudent.AadharNO;
                    student.BloodGroupID = obstudent.BloodGroupID;
                    student.CategoryID = obstudent.CategoryID;
                    student.DateOfBirth = obstudent.DateOfBirth;
                    student.EmailID = obstudent.EmailID;
                    student.FirstName = obstudent.FirstName;
                    student.Gender = obstudent.Gender;
                    student.InsertedDate = DateTime.Now;
                    student.IsAvailable = true;
                    student.LastName = obstudent.LastName;
                    student.MiddleName = obstudent.MiddleName;
                    student.MotherTongueID = obstudent.MotherTongueID;
                    student.NatiobalityID = obstudent.NatiobalityID;
                    student.PassportNO = obstudent.PassportNO;
                    student.PrimaryMobile = obstudent.PrimaryMobile;
                    student.ReligionID = obstudent.ReligionID;
                    student.StudentCode = obstudent.StudentCode;
                    student.WhatsappMobile = obstudent.WhatsappMobile;
                    student.ZoomMailID = obstudent.ZoomMailID;
                    if (file != null)
                    {
                        // Create a File Info 
                        FileInfo fi = new FileInfo(file.FileName);
                        var newFilename = student.StudentCode + "_" + String.Format("{0:d}",
                                          (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                        var webPath = hostingEnvironment.WebRootPath;
                        string path = Path.Combine("", webPath + @"\ODMImages\StudentProfile\" + newFilename);
                        var pathToSave = newFilename;
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }
                        student.Image = pathToSave;
                    }
                    student.InsertedId = userId;
                    student.Status = "CREATED";
                    long sid = studentAggregateRepository.CreateStudent(student);
                    if (obstudent.languagesknown.Count > 0)
                    {
                        foreach (var s in obstudent.languagesknown)
                        {
                            StudentLanguagesKnown studentLanguages = new StudentLanguagesKnown();
                            studentLanguages.Active = true;
                            studentLanguages.InsertedId = userId;
                            studentLanguages.InsertedDate = DateTime.Now;
                            studentLanguages.ModifiedDate = DateTime.Now;
                            studentLanguages.IsAvailable = true;
                            studentLanguages.LanguageID = s;
                            studentLanguages.ModifiedId = userId;
                            studentLanguages.Status = "CREATED";
                            studentLanguages.StudentID = sid;
                            await studentLanguagesKnownRepository.AddAsync(studentLanguages);
                        }

                    }
                    Access access = new Access();
                    access.EmployeeID = sid;
                    access.Active = true;
                    access.InsertedId = userId;
                    access.Password = "";
                    access.Username = student.StudentCode;
                    access.RoleID = 5;
                    access.ModifiedId = userId;
                    accessRepository.CreateAccess(access);
                    if (documentTypesCls.ToList().Count > 0)
                    {
                        foreach (var item in documentTypesCls)
                        {
                            if (item.IsRequired == true)
                            {
                                StudentRequiredDocument studentRequired = new StudentRequiredDocument();
                                studentRequired.Active = true;
                                studentRequired.DocumentSubTypeID = item.ID;
                                studentRequired.DocumentTypeID = item.TypeId;
                                studentRequired.StudentID = sid;
                                studentRequired.InsertedId = userId;
                                studentRequired.IsApproved = false;
                                studentRequired.IsMandatory = true;
                                studentRequired.IsReject = false;
                                studentRequired.IsVerified = false;
                                studentRequired.ModifiedId = userId;
                                studentRequired.RelatedID = sid;
                                studentRequired.Status = "CREATED";
                                studentRequired.VerifiedAccessID = 0;
                                studentAggregateRepository.CreateStudentRequiredDocument(studentRequired);
                            }
                        }
                    }
                    return Json(new { id = sid, sts = 1 });
                }
                else
                {
                    var std = studentAggregateRepository.GetAllStudent().Where(a => a.StudentCode == obstudent.StudentCode && a.ID != obstudent.ID).FirstOrDefault();
                    if (std != null)
                    {
                        return Json(new { sts = 3 });
                    }
                    OdmErp.ApplicationCore.Entities.Student student = studentAggregateRepository.GetStudentById(obstudent.ID);
                    student.ModifiedId = userId;
                    student.Active = true;
                    student.AadharNO = obstudent.AadharNO;
                    student.BloodGroupID = obstudent.BloodGroupID;
                    student.CategoryID = obstudent.CategoryID;
                    student.DateOfBirth = obstudent.DateOfBirth;
                    student.EmailID = obstudent.EmailID;
                    student.FirstName = obstudent.FirstName;
                    student.Gender = obstudent.Gender;
                    student.InsertedDate = DateTime.Now;
                    student.IsAvailable = true;
                    student.LastName = obstudent.LastName;
                    student.MiddleName = obstudent.MiddleName;
                    student.MotherTongueID = obstudent.MotherTongueID;
                    student.NatiobalityID = obstudent.NatiobalityID;
                    student.PassportNO = obstudent.PassportNO;
                    student.PrimaryMobile = obstudent.PrimaryMobile;
                    student.ReligionID = obstudent.ReligionID;
                    student.StudentCode = obstudent.StudentCode;
                    student.WhatsappMobile = obstudent.WhatsappMobile;
                    student.ZoomMailID = obstudent.ZoomMailID;
                    if (file != null)
                    {
                        // Create a File Info 
                        FileInfo fi = new FileInfo(file.FileName);
                        var newFilename = student.StudentCode + "_" + String.Format("{0:d}",
                                          (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                        var webPath = hostingEnvironment.WebRootPath;
                        string path = Path.Combine("", webPath + @"\ODMImages\StudentProfile\" + newFilename);
                        var pathToSave = newFilename;
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }
                        student.Image = pathToSave;
                    }

                    student.Status = "CREATED";
                    studentAggregateRepository.UpdateStudent(student);
                    if (obstudent.languagesknown.Count > 0)
                    {
                        foreach (var s in obstudent.languagesknown)
                        {
                            StudentLanguagesKnown studentLanguages = studentLanguagesKnownRepository.ListAllAsyncIncludeAll().Result.Where(a => a.StudentID == obstudent.ID && a.LanguageID == s && a.Active == true).FirstOrDefault();
                            if (studentLanguages == null)
                            {
                                studentLanguages = new StudentLanguagesKnown();
                                studentLanguages.Active = true;
                                studentLanguages.InsertedId = userId;
                                studentLanguages.IsAvailable = true;
                                studentLanguages.InsertedDate = DateTime.Now;
                                studentLanguages.ModifiedDate = DateTime.Now;
                                studentLanguages.LanguageID = s;
                                studentLanguages.ModifiedId = userId;
                                studentLanguages.Status = "CREATED";
                                studentLanguages.StudentID = obstudent.ID;
                                await studentLanguagesKnownRepository.AddAsync(studentLanguages);
                            }
                            else
                            {
                                studentLanguages.IsAvailable = true;
                                studentLanguages.LanguageID = s;
                                studentLanguages.ModifiedDate = DateTime.Now;
                                studentLanguages.ModifiedId = userId;
                                studentLanguages.Status = "UPDATED";
                                studentLanguages.StudentID = obstudent.ID;
                                await studentLanguagesKnownRepository.UpdateAsync(studentLanguages);
                            }
                        }
                        var studentLanguages1 = studentLanguagesKnownRepository.ListAllAsyncIncludeAll().Result.Where(a => a.StudentID == obstudent.ID && !obstudent.languagesknown.Select(m => m).Contains(a.LanguageID) && a.Active == true).ToList();
                        if (studentLanguages1 != null)
                        {
                            foreach (var s in studentLanguages1)
                            {
                                StudentLanguagesKnown studentLanguages = studentLanguagesKnownRepository.ListAllAsyncIncludeAll().Result.Where(a => a.StudentID == obstudent.ID && a.LanguageID == s.ID && a.Active == true).FirstOrDefault();

                                studentLanguages.Active = false;
                                studentLanguages.ModifiedId = userId;
                                studentLanguages.Status = "DELETED";
                                await studentLanguagesKnownRepository.UpdateAsync(studentLanguages);

                            }
                        }
                    }
                    Access access = accessRepository.GetAllAccess().Where(a => a.EmployeeID == student.ID && a.RoleID == 5).FirstOrDefault();
                    access.Username = student.StudentCode;
                    access.ModifiedId = userId;
                    accessRepository.UpdateAccess(access);
                    if (documentTypesCls.ToList().Count > 0)
                    {
                        foreach (var item in documentTypesCls)
                        {
                            StudentRequiredDocument studentRequired = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.DocumentTypeID == item.TypeId && a.DocumentSubTypeID == item.ID && a.StudentID == student.ID).FirstOrDefault();
                            if (item.IsRequired == true)
                            {
                                if (studentRequired == null)
                                {
                                    studentRequired = new StudentRequiredDocument();
                                    studentRequired.Active = true;
                                    studentRequired.DocumentSubTypeID = item.ID;
                                    studentRequired.DocumentTypeID = item.TypeId;
                                    studentRequired.StudentID = student.ID;
                                    studentRequired.InsertedId = userId;
                                    studentRequired.IsApproved = false;
                                    studentRequired.IsMandatory = true;
                                    studentRequired.IsReject = false;
                                    studentRequired.IsVerified = false;
                                    studentRequired.ModifiedId = userId;
                                    studentRequired.RelatedID = student.ID;
                                    studentRequired.Status = "CREATED";
                                    studentRequired.VerifiedAccessID = 0;
                                    studentAggregateRepository.CreateStudentRequiredDocument(studentRequired);
                                }
                                else
                                {
                                    studentRequired.Active = true;
                                    studentRequired.IsApproved = false;
                                    studentRequired.IsMandatory = true;
                                    studentRequired.IsReject = false;
                                    studentRequired.IsVerified = false;
                                    studentRequired.ModifiedId = userId;
                                    studentRequired.RelatedID = student.ID;
                                    studentRequired.Status = "CREATED";
                                    studentRequired.VerifiedAccessID = 0;
                                    studentAggregateRepository.UpdateStudentRequiredDocument(studentRequired);
                                }
                            }
                            else
                            {
                                if (studentRequired != null)
                                {
                                    studentRequired.Active = false;
                                    studentRequired.IsApproved = false;
                                    studentRequired.IsMandatory = true;
                                    studentRequired.IsReject = false;
                                    studentRequired.IsVerified = false;
                                    studentRequired.ModifiedId = userId;
                                    studentRequired.RelatedID = student.ID;
                                    studentRequired.Status = "CREATED";
                                    studentRequired.VerifiedAccessID = 0;
                                    studentAggregateRepository.UpdateStudentRequiredDocument(studentRequired);
                                }

                            }

                        }
                    }
                    return Json(new { id = student.ID, sts = 2 });
                }


            }

            catch
            {
                return Json(new { sts = 0 });
            }

        }
        [ActionName("DeleteStudent")]
        public async Task<IActionResult> StudentDeleteConfirmed(long id)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var student = studentAggregateRepository.DeleteStudent(id);
            return RedirectToAction(nameof(Index));
        }
        public IActionResult ViewStudentDetails(long? id)
        {
            StudentModelClass studentcls = new StudentModelClass();
            studentcls.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
            studentcls.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
            studentcls.religionTypes = religionTypeRepository.GetAllReligionTypes();
            studentcls.categoryTypes = categoryRepository.ListAllAsyncIncludeAll().Result;
            ViewBag.status = "Create";
            if (id != null)
            {
                var studentdetails = studentAggregateRepository.GetStudentById(id.Value);
                studentcls.BloodGroupID = studentdetails.BloodGroupID;
                studentcls.BloodGroupName = bloodGroupRepository.GetBloodGroupById(studentdetails.BloodGroupID).Name;
                studentcls.DateOfBirth = studentdetails.DateOfBirth;
                studentcls.StudentCode = studentdetails.StudentCode;
                studentcls.FirstName = studentdetails.FirstName;
                studentcls.Gender = studentdetails.Gender;
                studentcls.ID = studentdetails.ID;
                studentcls.LastName = studentdetails.LastName;
                studentcls.MiddleName = studentdetails.MiddleName;
                studentcls.NatiobalityID = studentdetails.NatiobalityID;
                studentcls.NatiobalityName = nationalityTypeRepository.GetNationalityById(studentdetails.NatiobalityID).Name;
                studentcls.ReligionID = studentdetails.ReligionID;
                studentcls.ReligionName = religionTypeRepository.GetReligionTypeById(studentdetails.ReligionID).Name;
                studentcls.CategoryName = categoryRepository.GetCategoryNameById(studentdetails.CategoryID).Result.Name;
                var webPath = hostingEnvironment.WebRootPath;
                if (studentdetails.Image != null)
                {
                    string path = Path.Combine("/ODMImages/StudentProfile/", studentdetails.Image);
                    ViewBag.Profileimage = path;
                }
                else
                {
                    ViewBag.Profileimage = null;
                }
                studentcls.studentAdresses = studentMethod.GetAllAddressByUsingStudentId(studentdetails.ID);
                studentcls.studentParentsCls = studentMethod.GetAllParentsByUsingStudentId(studentdetails.ID);
                studentcls.studentClassDetails = sqlQuery.Get_View_Academic_Student().Where(a => a.StudentId == studentdetails.ID) == null ? null : sqlQuery.Get_View_Academic_Student().Where(a => a.StudentId == studentdetails.ID).Where(a => a.IsAvailable == true).FirstOrDefault();
                studentcls.accesscls = commonMethods.GetStudentAccessDetails().Where(a => a.EmployeeID == studentdetails.ID && (a.RoleID == 5 || a.RoleID == 6)).ToList();
                studentcls.StudentDocumentAccess = studentMethod.GetAllDocumentByUsingStudentId(studentdetails.ID) == null ? null : studentMethod.GetAllDocumentByUsingStudentId(studentdetails.ID).Where(a => a.DocumentTypeName == "EDUCATIONAL").ToList();
                studentcls.StudentPersonalDocumentAccess = studentMethod.GetStudentDocuments(studentdetails.ID) == null ? null : studentMethod.GetStudentDocuments(studentdetails.ID).Where(a => a.DocumentTypeName == "PERSONAL").ToList();
                studentcls.StudentAcademicAccess = studentMethod.GetAllStudentAcademicByStudentId(studentdetails.ID);
                studentcls.emergencyContact = studentMethod.GetStudentEmergencyContacts(studentdetails.ID).FirstOrDefault();
                studentcls.transport = studentMethod.GetStudentTransportation(studentdetails.ID) == null ? null : studentMethod.GetStudentTransportation(studentdetails.ID).FirstOrDefault();
                ViewBag.status = "Update";
            }
            return View(studentcls);
        }
        #endregion
        #region Excel Upload 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadStudent(IFormFile file, int ddlexceltype)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                if (file != null && ddlexceltype == 1)
                {

                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[6];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var request = studentAggregateRepository.GetAllStudent();
                        var bloodgroups = bloodGroupRepository.GetAllBloodGroup();
                        var nationalities = nationalityTypeRepository.GetAllNationalities();
                        var religions = religionTypeRepository.GetAllReligionTypes();
                        var languages = languageRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
                        var category = categoryRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            OdmErp.ApplicationCore.Entities.Student student = new OdmErp.ApplicationCore.Entities.Student();
                            //Random generator = new Random();
                            //String s = generator.Next(0, 999999).ToString("D6");
                            var StudentCode = firstWorksheet.Cells[i, 1].Value.ToString();

                            if (request.Where(a => a.StudentCode == StudentCode).FirstOrDefault() == null)
                            {

                                student.ModifiedId = userId;
                                if (firstWorksheet.Cells[i, 2].Value != null)
                                {
                                    student.FirstName = firstWorksheet.Cells[i, 2].Value.ToString();

                                }
                                if (firstWorksheet.Cells[i, 3].Value != null)
                                {
                                    student.MiddleName = firstWorksheet.Cells[i, 3].Value.ToString();

                                }
                                if (firstWorksheet.Cells[i, 4].Value != null)
                                {
                                    student.LastName = firstWorksheet.Cells[i, 4].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 5].Value != null)
                                {
                                    student.Gender = firstWorksheet.Cells[i, 5].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 6].Value != null)
                                {
                                    string dateTime = firstWorksheet.Cells[i, 6].Value.ToString();
                                    student.DateOfBirth = Convert.ToDateTime(dateTime);
                                }
                                if (firstWorksheet.Cells[i, 7].Value != null)
                                {
                                    student.BloodGroupID = bloodgroups.Where(a => a.Name == firstWorksheet.Cells[i, 7].Value.ToString()).FirstOrDefault().ID;
                                }
                                if (firstWorksheet.Cells[i, 8].Value != null)
                                {
                                    student.NatiobalityID = nationalities.Where(a => a.Name == firstWorksheet.Cells[i, 8].Value.ToString()).FirstOrDefault().ID;
                                }
                                if (firstWorksheet.Cells[i, 9].Value != null)
                                {
                                    student.ReligionID = religions.Where(a => a.Name == firstWorksheet.Cells[i, 9].Value.ToString()).FirstOrDefault().ID;
                                }
                                if (firstWorksheet.Cells[i, 10].Value != null)
                                {
                                    student.CategoryID = category.Where(a => a.Name == firstWorksheet.Cells[i, 10].Value.ToString()).FirstOrDefault().ID;
                                }
                                if (firstWorksheet.Cells[i, 11].Value != null)
                                {
                                    student.MotherTongueID = languages.Where(a => a.Name == firstWorksheet.Cells[i, 11].Value.ToString()).FirstOrDefault().ID;
                                }


                                if (firstWorksheet.Cells[i, 13].Value != null)
                                {
                                    student.PassportNO = firstWorksheet.Cells[i, 13].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 14].Value != null)
                                {
                                    student.AadharNO = firstWorksheet.Cells[i, 14].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 15].Value != null)
                                {
                                    student.PrimaryMobile = firstWorksheet.Cells[i, 15].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 16].Value != null)
                                {
                                    student.WhatsappMobile = firstWorksheet.Cells[i, 16].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 17].Value != null)
                                {
                                    student.EmailID = firstWorksheet.Cells[i, 17].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 18].Value != null)
                                {
                                    student.ZoomMailID = firstWorksheet.Cells[i, 18].Value.ToString();
                                }
                                student.Active = true;
                                student.StudentCode = StudentCode;
                                student.InsertedId = userId;
                                student.IsAvailable = true;
                                student.ModifiedId = userId;
                                student.Status = EntityStatus.ACTIVE;
                                long id = studentAggregateRepository.CreateStudent(student);
                                if (firstWorksheet.Cells[i, 12].Value != null)
                                {
                                    string knownlang = firstWorksheet.Cells[i, 12].Value.ToString();
                                    string[] langarr = knownlang.Split(',');
                                    for (int m = 0; m < langarr.Count(); m++)
                                    {
                                        if (languages.Where(a => a.Name == langarr[m]).FirstOrDefault() != null)
                                        {
                                            StudentLanguagesKnown studentLanguages = new StudentLanguagesKnown();
                                            studentLanguages.Active = true;
                                            studentLanguages.InsertedId = userId;
                                            studentLanguages.InsertedDate = DateTime.Now;
                                            studentLanguages.ModifiedDate = DateTime.Now;
                                            studentLanguages.IsAvailable = true;
                                            studentLanguages.LanguageID = languages.Where(a => a.Name == langarr[m]).FirstOrDefault().ID;
                                            studentLanguages.ModifiedId = userId;
                                            studentLanguages.Status = "CREATED";
                                            studentLanguages.StudentID = id;
                                            studentLanguagesKnownRepository.AddAsync(studentLanguages);
                                        }
                                    }
                                }
                                if (id > 0)
                                {
                                    Access accesss = new Access();
                                    accesss.RoleID = 5;
                                    accesss.Username = student.StudentCode;
                                    accesss.EmployeeID = id;
                                    accesss.Active = true;
                                    accesss.InsertedDate = DateTime.Now;
                                    accesss.InsertedId = userId;
                                    accesss.ModifiedDate = DateTime.Now;
                                    accesss.ModifiedId = userId;
                                    accessRepository.CreateAccess(accesss);
                                }
                            }
                        }
                    }
                }
                if (file != null && ddlexceltype == 2)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[3];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var request = studentAggregateRepository.GetAllParent();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            Parent parent = new Parent();
                            string PrimaryNumber = firstWorksheet.Cells[i, 4].Value.ToString();
                            string stdcd = firstWorksheet.Cells[i, 9].Value.ToString();
                            if (stdcd != "")
                            {
                                string StudentCode = stdcd.Split("-")[1].ToString();
                                var studentid = studentAggregateRepository.GetAllStudent().Where(a => a.StudentCode == StudentCode).Select(a => a.ID).FirstOrDefault();
                                string ParentRelationshipName = firstWorksheet.Cells[i, 10].Value.ToString();
                                var parentrelationshipid = studentAggregateRepository.GetAllParentRelationshipType().Where(a => a.Name == ParentRelationshipName).Select(a => a.ID).FirstOrDefault();
                                var par = request.Where(a => a.StudentID == studentid && a.ParentRelationshipID == parentrelationshipid).FirstOrDefault();
                                if (par == null)
                                {
                                    if (PrimaryNumber != null)
                                    {
                                        parent.ModifiedId = userId;
                                        parent.FirstName = firstWorksheet.Cells[i, 1].Value.ToString();
                                        if (firstWorksheet.Cells[i, 2] != null && firstWorksheet.Cells[i, 2].Value != null)
                                        {
                                            parent.MiddleName = firstWorksheet.Cells[i, 2].Value.ToString();
                                        }
                                        if (firstWorksheet.Cells[i, 3] != null && firstWorksheet.Cells[i, 3].Value != null)
                                        {
                                            parent.LastName = firstWorksheet.Cells[i, 3].Value.ToString();
                                        }
                                        if (firstWorksheet.Cells[i, 4] != null && firstWorksheet.Cells[i, 4].Value != null)
                                        {
                                            parent.PrimaryMobile = firstWorksheet.Cells[i, 4].Value.ToString();
                                        }
                                        if (firstWorksheet.Cells[i, 5] != null && firstWorksheet.Cells[i, 5].Value != null)
                                        {
                                            parent.AlternativeMobile = firstWorksheet.Cells[i, 5].Value.ToString();

                                        }
                                        if (firstWorksheet.Cells[i, 6] != null && firstWorksheet.Cells[i, 6].Value != null)
                                        {
                                            parent.LandlineNumber = firstWorksheet.Cells[i, 6].Value.ToString();

                                        }
                                        parent.EmailId = firstWorksheet.Cells[i, 7].Value.ToString();
                                        string ProfessionTypeName = firstWorksheet.Cells[i, 8].Value.ToString();
                                        parent.ProfessionTypeID = studentAggregateRepository.GetAllProfessionType().Where(a => a.Name == ProfessionTypeName).Select(a => a.ID).FirstOrDefault();
                                        parent.StudentID = studentAggregateRepository.GetAllStudent().Where(a => a.StudentCode == StudentCode).Select(a => a.ID).FirstOrDefault();
                                        parent.ParentRelationshipID = studentAggregateRepository.GetAllParentRelationshipType().Where(a => a.Name == ParentRelationshipName).Select(a => a.ID).FirstOrDefault();
                                        if (firstWorksheet.Cells[i, 11] != null && firstWorksheet.Cells[i, 11].Value != null)
                                        {
                                            parent.ParentOrganization = firstWorksheet.Cells[i, 11].Value.ToString();

                                        }
                                        parent.Active = true;
                                        parent.InsertedId = userId;
                                        parent.IsAvailable = true;
                                        parent.ModifiedId = userId;
                                        parent.Status = EntityStatus.ACTIVE;
                                        long id = studentAggregateRepository.CreateParent(parent);
                                        if (id > 0)
                                        {
                                            Access accesss = new Access();
                                            accesss.RoleID = 6;
                                            accesss.Username = parent.PrimaryMobile;
                                            accesss.EmployeeID = id;
                                            accesss.Active = true;
                                            accesss.InsertedDate = DateTime.Now;
                                            accesss.InsertedId = userId;
                                            accesss.ModifiedDate = DateTime.Now;
                                            accesss.ModifiedId = userId;
                                            accessRepository.CreateAccess(accesss);
                                        }
                                    }
                                }
                                else
                                {
                                    parent = par;
                                    parent.ModifiedId = userId;
                                    parent.FirstName = firstWorksheet.Cells[i, 1].Value.ToString();
                                    if (firstWorksheet.Cells[i, 2] != null && firstWorksheet.Cells[i, 2].Value != null)
                                    {
                                        parent.MiddleName = firstWorksheet.Cells[i, 2].Value.ToString();
                                    }
                                    if (firstWorksheet.Cells[i, 3] != null && firstWorksheet.Cells[i, 3].Value != null)
                                    {
                                        parent.LastName = firstWorksheet.Cells[i, 3].Value.ToString();
                                    }
                                    if (firstWorksheet.Cells[i, 4] != null && firstWorksheet.Cells[i, 4].Value != null)
                                    {
                                        parent.PrimaryMobile = firstWorksheet.Cells[i, 4].Value.ToString();
                                    }
                                    if (firstWorksheet.Cells[i, 5] != null && firstWorksheet.Cells[i, 5].Value != null)
                                    {
                                        parent.AlternativeMobile = firstWorksheet.Cells[i, 5].Value.ToString();

                                    }
                                    if (firstWorksheet.Cells[i, 6] != null && firstWorksheet.Cells[i, 6].Value != null)
                                    {
                                        parent.LandlineNumber = firstWorksheet.Cells[i, 6].Value.ToString();

                                    }
                                    parent.EmailId = firstWorksheet.Cells[i, 7].Value.ToString();
                                    string ProfessionTypeName = firstWorksheet.Cells[i, 8].Value.ToString();
                                    parent.ProfessionTypeID = studentAggregateRepository.GetAllProfessionType().Where(a => a.Name == ProfessionTypeName).Select(a => a.ID).FirstOrDefault();
                                    parent.StudentID = studentAggregateRepository.GetAllStudent().Where(a => a.StudentCode == StudentCode).Select(a => a.ID).FirstOrDefault();
                                    parent.ParentRelationshipID = studentAggregateRepository.GetAllParentRelationshipType().Where(a => a.Name == ParentRelationshipName).Select(a => a.ID).FirstOrDefault();
                                    if (firstWorksheet.Cells[i, 11] != null && firstWorksheet.Cells[i, 11].Value != null)
                                    {
                                        parent.ParentOrganization = firstWorksheet.Cells[i, 11].Value.ToString();

                                    }
                                    parent.Active = true;
                                    parent.IsAvailable = true;
                                    parent.ModifiedId = userId;
                                    parent.Status = EntityStatus.ACTIVE;
                                    studentAggregateRepository.UpdateParent(parent);
                                }
                            }
                        }
                    }
                }
                if (file != null && ddlexceltype == 3)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var request = studentAggregateRepository.GetAllParent();
                        var students = studentAggregateRepository.GetAllStudent();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            if (firstWorksheet.Cells[i, 1].Value != null)
                            {
                                string studentcode = firstWorksheet.Cells[i, 1].Value.ToString();
                                string code = studentcode.Split('-')[1].ToString();
                                var studentdt = students.Where(a => a.StudentCode == code).FirstOrDefault();
                                if (studentdt != null)
                                {
                                    StudentEmergencyContact ob = new StudentEmergencyContact();
                                    ob.ContactPersonName = firstWorksheet.Cells[i, 2].Value.ToString();
                                    ob.EmailId = firstWorksheet.Cells[i, 4].Value.ToString();
                                    ob.Mobile = firstWorksheet.Cells[i, 3].Value.ToString();
                                    ob.StudentID = studentdt.ID;
                                    var m = studentMethod.SaveoreditStudentEmergencyContact(ob, userId);
                                }
                            }
                        }
                    }
                }
                if (file != null && ddlexceltype == 4)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[2];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var students = studentAggregateRepository.GetAllStudent();
                        var address = studentAggregateRepository.GetAllStudentAddress();
                        var addressdet = addressRepository.GetAllAddress();

                        for (int i = 2; i <= totalRows; i++)
                        {
                            if (firstWorksheet.Cells[i, 1].Value != null)
                            {
                                string studentcode = firstWorksheet.Cells[i, 1].Value.ToString();
                                string code = studentcode.Split('-')[1].ToString();
                                var studentdt = students.Where(a => a.StudentCode == code).FirstOrDefault();
                                if (studentdt != null)
                                {
                                    var addresstypeid = Convert.ToInt64(firstWorksheet.Cells[i, 2].Value.ToString().Split('_')[1]);
                                    var stdaddress = address.Where(a => a.StudentID == studentdt.ID).ToList();
                                    var sm = addressdet.Where(a => a.AddressTypeID == addresstypeid && stdaddress.Select(m => m.AddressID).Contains(a.ID)).FirstOrDefault();
                                    if (sm == null)
                                    {
                                        Address add = new Address();
                                        add.Active = true;
                                        add.AddressLine1 = firstWorksheet.Cells[i, 3].Value.ToString();
                                        add.AddressLine2 = firstWorksheet.Cells[i, 4].Value.ToString();
                                        add.AddressTypeID = addresstypeid;
                                        add.CityID = Convert.ToInt64(firstWorksheet.Cells[i, 7].Value.ToString().Split('_')[1]);
                                        add.StateID = Convert.ToInt64(firstWorksheet.Cells[i, 6].Value.ToString().Split('_')[1]);
                                        add.CountryID = Convert.ToInt64(firstWorksheet.Cells[i, 5].Value.ToString().Split('_')[1]);
                                        add.PostalCode = firstWorksheet.Cells[i, 8].Value.ToString();
                                        add.Status = "ACTIVE";
                                        add.EmployeeID = userId;
                                        add.InsertedDate = DateTime.Now;
                                        add.InsertedId = userId;
                                        add.ModifiedDate = DateTime.Now;
                                        add.ModifiedId = userId;
                                        add.IsAvailable = true;
                                        long addressid = addressRepository.CreateAddress(add);
                                        StudentAddress studentAddress = new StudentAddress();
                                        studentAddress.AddressID = addressid;
                                        studentAddress.StudentID = studentdt.ID;
                                        studentAddress.Active = true;
                                        studentAddress.IsAvailable = true;
                                        studentAddress.InsertedId = userId;
                                        studentAddress.ModifiedId = userId;
                                        studentAddress.Status = "CREATED";
                                        studentAggregateRepository.CreateStudentAddress(studentAddress);
                                    }
                                    else
                                    {
                                        sm.Active = true;
                                        sm.AddressLine1 = firstWorksheet.Cells[i, 3].Value.ToString();
                                        sm.AddressLine2 = firstWorksheet.Cells[i, 4].Value.ToString();
                                        sm.AddressTypeID = addresstypeid;
                                        sm.CityID = Convert.ToInt64(firstWorksheet.Cells[i, 7].Value.ToString().Split('_')[1]);
                                        sm.StateID = Convert.ToInt64(firstWorksheet.Cells[i, 6].Value.ToString().Split('_')[1]);
                                        sm.CountryID = Convert.ToInt64(firstWorksheet.Cells[i, 5].Value.ToString().Split('_')[1]);
                                        sm.PostalCode = firstWorksheet.Cells[i, 8].Value.ToString();
                                        sm.Status = "ACTIVE";
                                        sm.EmployeeID = userId;
                                        sm.ModifiedDate = DateTime.Now;
                                        sm.ModifiedId = userId;
                                        sm.IsAvailable = true;
                                        addressRepository.UpdateAddress(sm);
                                    }
                                }
                            }
                        }
                    }
                }
                if (file != null && ddlexceltype == 5)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[4];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var students = studentAggregateRepository.GetAllStudent();


                        for (int i = 2; i <= totalRows; i++)
                        {
                            if (firstWorksheet.Cells[i, 1].Value != null)
                            {
                                string studentcode = firstWorksheet.Cells[i, 1].Value.ToString();
                                string code = studentcode.Split('-')[1].ToString();
                                var studentdt = students.Where(a => a.StudentCode == code).FirstOrDefault();
                                if (studentdt != null)
                                {
                                    studenttransportcls os = new studenttransportcls();
                                    os.IsTransport = firstWorksheet.Cells[i, 2].Value.ToString() == "Yes" ? true : false;
                                    if (os.IsTransport == true)
                                    {
                                        var countryid = Convert.ToInt64(firstWorksheet.Cells[i, 3].Value.ToString().Split('_')[1]);
                                        var stateid = Convert.ToInt64(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]);
                                        var cityid = Convert.ToInt64(firstWorksheet.Cells[i, 5].Value.ToString().Split('_')[1]);
                                        var locationid = Convert.ToInt64(firstWorksheet.Cells[i, 6].Value.ToString().Split('_')[1]);
                                        var departureid = Convert.ToInt64(firstWorksheet.Cells[i, 7].Value.ToString().Split('_')[1]);
                                        os.CityID = cityid;
                                        os.CountryID = countryid;
                                        os.DepartureID = departureid;
                                        os.Distance = Convert.ToDecimal(firstWorksheet.Cells[i, 8].Value.ToString());
                                        os.LocationID = locationid;
                                        os.StateID = stateid;
                                    }
                                    else
                                    {
                                        os.CityID = 0;
                                        os.CountryID = 0;
                                        os.DepartureID = 0;
                                        os.Distance = 0;
                                        os.LocationID = 0;
                                        os.StateID = 0;
                                    }
                                    os.StudentID = studentdt.ID;
                                    studentMethod.SaveoreditTransportation(os, userId);
                                }
                            }
                        }
                    }
                }
                if (file != null && ddlexceltype == 6)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[2];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var students = studentAggregateRepository.GetAllStudent();
                        var studentacademics = studentAggregateRepository.GetAllStudentAcademic();

                        for (int i = 2; i <= totalRows; i++)
                        {
                            if (firstWorksheet.Cells[i, 1].Value != null)
                            {
                                string studentcode = firstWorksheet.Cells[i, 1].Value.ToString();
                                string code = studentcode.Split('-')[1].ToString();
                                var studentdt = students.Where(a => a.StudentCode == code).FirstOrDefault();
                                if (studentdt != null)
                                {
                                    var organisation = firstWorksheet.Cells[i, 2].Value.ToString();
                                    var chk = studentacademics.Where(a => a.StudentID == studentdt.ID && a.OrganizationName.ToLower() == organisation).FirstOrDefault();
                                    var boardid = Convert.ToInt32(firstWorksheet.Cells[i, 3].Value.ToString().Split('_')[1]);
                                    var standardid = Convert.ToInt32(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]);
                                    var stateid = Convert.ToInt32(firstWorksheet.Cells[i, 10].Value.ToString().Split('_')[1]);
                                    var cityid = Convert.ToInt32(firstWorksheet.Cells[i, 11].Value.ToString().Split('_')[1]);
                                    var fromdt = Convert.ToDateTime(firstWorksheet.Cells[i, 5].Value.ToString());
                                    var todt = Convert.ToDateTime(firstWorksheet.Cells[i, 6].Value.ToString());

                                    if (chk == null)
                                    {
                                        StudentAcademic acd = new StudentAcademic();
                                        acd.Active = true;
                                        acd.IsAvailable = true;
                                        acd.OrganizationName = organisation;
                                        acd.ReasonForChange = firstWorksheet.Cells[i, 7].Value.ToString();
                                        acd.GradeOrPercentage = firstWorksheet.Cells[i, 8].Value.ToString();
                                        acd.StateId = stateid;
                                        acd.CityId = cityid;
                                        acd.BoardID = boardid;
                                        acd.ClassID = standardid;
                                        acd.FromDate = fromdt;
                                        acd.ToDate = todt;
                                        acd.InsertedDate = DateTime.Now;
                                        acd.InsertedId = userId;
                                        acd.ModifiedDate = DateTime.Now;
                                        acd.ModifiedId = userId;
                                        studentAggregateRepository.CreateStudentAcademic(acd);
                                    }
                                    else
                                    {
                                        StudentAcademic acd = chk;
                                        acd.Active = true;
                                        acd.IsAvailable = true;
                                        acd.OrganizationName = organisation;
                                        acd.ReasonForChange = firstWorksheet.Cells[i, 7].Value.ToString();
                                        acd.GradeOrPercentage = firstWorksheet.Cells[i, 8].Value.ToString();
                                        acd.StateId = stateid;
                                        acd.CityId = cityid;
                                        acd.BoardID = boardid;
                                        acd.ClassID = standardid;
                                        acd.FromDate = fromdt;
                                        acd.ToDate = todt;
                                        acd.ModifiedDate = DateTime.Now;
                                        acd.ModifiedId = userId;
                                        studentAggregateRepository.UpdateStudentAcademic(acd);
                                    }


                                }
                            }
                        }
                    }
                }
                if (file != null && ddlexceltype == 7)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[2];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var students = studentAggregateRepository.GetAllStudent();


                        for (int i = 2; i <= totalRows; i++)
                        {
                            if (firstWorksheet.Cells[i, 1].Value != null)
                            {
                                string studentcode = firstWorksheet.Cells[i, 1].Value.ToString();
                                string code = studentcode.Split('-')[1].ToString();
                                var studentdt = students.Where(a => a.StudentCode == code).FirstOrDefault();
                                if (studentdt != null)
                                {
                                    var session = Convert.ToInt32(firstWorksheet.Cells[i, 2].Value.ToString().Split('_')[1]);
                                    var organisation = Convert.ToInt32(firstWorksheet.Cells[i, 3].Value.ToString().Split('_')[1]);
                                    var standard = Convert.ToInt32(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]);
                                    var section = Convert.ToInt32(firstWorksheet.Cells[i, 5].Value.ToString().Split('_')[1]);
                                    var subjectgroup = Convert.ToInt32(firstWorksheet.Cells[i, 7].Value.ToString().Split('_')[1]);
                                    var wing = Convert.ToInt32(firstWorksheet.Cells[i, 6].Value.ToString().Split('_')[1]);
                                    var rollno = firstWorksheet.Cells[i, 8].Value.ToString();
                                    studentacademicsstandarddetcls acd = new studentacademicsstandarddetcls();
                                    acd.AcademicSectionId = section;
                                    acd.AcademicSessionId = session;
                                    acd.AcademicStandardId = standard;
                                    acd.AcademicSubjectGroupId = subjectgroup;
                                    acd.AcademicSubjectWingId = wing;
                                    acd.ID = 0;
                                    acd.IsRequiredNucleus = false;
                                    acd.OraganisationAcademicId = organisation;
                                    acd.Nucleus = false;
                                    acd.StudentId = studentdt.ID;
                                    acd.Studentcode = rollno;
                                    long res = studentMethod.SaveOrEditStudentAcademicStandard(acd, userId).Result;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(Index));
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        public IActionResult DownloadStudentSample(int type)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (type == 1)//Profile
            {
                XLWorkbook oWB = new XLWorkbook();

                var NationalityList = nationalityTypeRepository.GetAllNationalities().ToList();
                var ReligionList = religionTypeRepository.GetAllReligionTypes().ToList();
                var CategoryList = categoryRepository.ListAllAsyncIncludeAll().Result.ToList();
                var BloodGroupList = bloodGroupRepository.GetAllBloodGroup().ToList();
                var MotherTongueLst = languageRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();

                DataTable genderdt = new DataTable();
                genderdt.Columns.Add("Gender");
                DataRow dr2 = genderdt.NewRow();
                dr2["Gender"] = "Male";
                genderdt.Rows.Add(dr2);
                DataRow dr1 = genderdt.NewRow();
                dr1["Gender"] = "Female";
                genderdt.Rows.Add(dr1);
                genderdt.TableName = "Gender";

                DataTable blooddt = new DataTable();
                blooddt.Columns.Add("BloodName");
                foreach (var a in BloodGroupList)
                {
                    DataRow dr = blooddt.NewRow();
                    dr["BloodName"] = a.Name;
                    blooddt.Rows.Add(dr);
                }
                blooddt.TableName = "Blood Group";

                DataTable nationalitydt = new DataTable();
                nationalitydt.Columns.Add("NationalityName");
                foreach (var a in NationalityList)
                {
                    DataRow dr = nationalitydt.NewRow();
                    dr["NationalityName"] = a.Name;
                    nationalitydt.Rows.Add(dr);
                }
                nationalitydt.TableName = "Nationality";

                DataTable religiondt = new DataTable();
                religiondt.Columns.Add("ReligionName");
                foreach (var a in ReligionList)
                {
                    DataRow dr = religiondt.NewRow();
                    dr["ReligionName"] = a.Name;
                    religiondt.Rows.Add(dr);
                }
                religiondt.TableName = "Religion";


                DataTable categorydt = new DataTable();
                categorydt.Columns.Add("CategoryName");
                foreach (var a in CategoryList)
                {
                    DataRow dr = categorydt.NewRow();
                    dr["CategoryName"] = a.Name;
                    categorydt.Rows.Add(dr);
                }
                categorydt.TableName = "Category";


                DataTable languagedt = new DataTable();
                languagedt.Columns.Add("LanguageName");
                foreach (var a in MotherTongueLst)
                {
                    DataRow dr = languagedt.NewRow();
                    dr["LanguageName"] = a.Name;
                    languagedt.Rows.Add(dr);
                }
                languagedt.TableName = "Mother Tongue";


                int lastCellNo1 = genderdt.Rows.Count + 1;
                int lastCellNo2 = blooddt.Rows.Count + 1;
                int lastCellNo3 = nationalitydt.Rows.Count + 1;
                int lastCellNo4 = religiondt.Rows.Count + 1;
                int lastCellNo5 = categorydt.Rows.Count + 1;
                int lastCellNo6 = languagedt.Rows.Count + 1;

                oWB.AddWorksheet(genderdt);
                oWB.AddWorksheet(blooddt);
                oWB.AddWorksheet(nationalitydt);
                oWB.AddWorksheet(religiondt);
                oWB.AddWorksheet(categorydt);
                oWB.AddWorksheet(languagedt);

                var worksheet1 = oWB.Worksheet(1);
                var worksheet2 = oWB.Worksheet(2);
                var worksheet3 = oWB.Worksheet(3);
                var worksheet4 = oWB.Worksheet(4);
                var worksheet5 = oWB.Worksheet(5);
                var worksheet6 = oWB.Worksheet(6);

                DataTable validationTable = new DataTable();
                validationTable.Columns.Add("School Code");
                validationTable.Columns.Add("First Name");
                validationTable.Columns.Add("Middle Name");
                validationTable.Columns.Add("Last Name");
                validationTable.Columns.Add("Gender");
                validationTable.Columns.Add("Date Of Birth");
                validationTable.Columns.Add("Blood Group");
                validationTable.Columns.Add("Nationality");
                validationTable.Columns.Add("Religion");
                validationTable.Columns.Add("Category");
                validationTable.Columns.Add("Mother Tongue");
                validationTable.Columns.Add("Languages Known");
                validationTable.Columns.Add("Passport No");
                validationTable.Columns.Add("Adhara No");
                validationTable.Columns.Add("Primary Mobile");
                validationTable.Columns.Add("Whatsapp Mobile");
                validationTable.Columns.Add("Email ID");
                validationTable.Columns.Add("Zoom Mail ID");
                validationTable.TableName = "Students";
                var worksheet = oWB.AddWorksheet(validationTable);
                worksheet.Column(5).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
                worksheet.Column(5).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(5).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(5).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(5).SetDataValidation().ErrorMessage = "Please Select From The List";


                worksheet.Column(7).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
                worksheet.Column(7).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(7).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(7).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(7).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(8).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
                worksheet.Column(8).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(8).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(8).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(8).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(9).SetDataValidation().List(worksheet4.Range("A2:A" + lastCellNo4), true);
                worksheet.Column(9).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(9).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(9).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(9).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(10).SetDataValidation().List(worksheet5.Range("A2:A" + lastCellNo5), true);
                worksheet.Column(10).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(10).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(10).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(10).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(11).SetDataValidation().List(worksheet6.Range("A2:A" + lastCellNo6), true);
                worksheet.Column(11).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(11).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(11).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(11).SetDataValidation().ErrorMessage = "Please Select From The List";
                worksheet1.Hide();
                worksheet2.Hide();
                worksheet3.Hide();
                worksheet4.Hide();
                worksheet5.Hide();
                worksheet6.Hide();
                Byte[] workbookBytes;
                MemoryStream ms = GetStream(oWB);
                workbookBytes = ms.ToArray();
                return File(workbookBytes, "application/ms-excel", $"StudentSample.xlsx");

            }
            else if (type == 2)//Parent
            {
                XLWorkbook oWB = new XLWorkbook();
                var ProfessionList = studentAggregateRepository.GetAllProfessionType().ToList();
                var StudentList = studentAggregateRepository.GetAllStudent().ToList();
                var ParentRelationshipList = studentAggregateRepository.GetAllParentRelationshipType().ToList();


                DataTable professiondt = new DataTable();
                professiondt.Columns.Add("Profession Type");
                foreach (var a in ProfessionList)
                {
                    DataRow dr = professiondt.NewRow();
                    dr["Profession Type"] = a.Name;
                    professiondt.Rows.Add(dr);
                }
                professiondt.TableName = "Profession Type";

                DataTable parentRelationsdt = new DataTable();
                parentRelationsdt.Columns.Add("ParentRelationship");
                foreach (var a in ParentRelationshipList)
                {
                    DataRow dr = parentRelationsdt.NewRow();
                    dr["ParentRelationship"] = a.Name;
                    parentRelationsdt.Rows.Add(dr);
                }
                parentRelationsdt.TableName = "ParentRelationship";

                DataTable studentdt = new DataTable();
                studentdt.Columns.Add("Student");
                foreach (var a in StudentList)
                {
                    DataRow dr = studentdt.NewRow();
                    dr["Student"] = a.FirstName + " " + a.LastName + "-" + a.StudentCode;
                    studentdt.Rows.Add(dr);
                }
                studentdt.TableName = "Student";


                int lastCellNo1 = professiondt.Rows.Count + 1;
                int lastCellNo2 = studentdt.Rows.Count + 1;
                int lastCellNo3 = parentRelationsdt.Rows.Count + 1;


                oWB.AddWorksheet(professiondt);
                oWB.AddWorksheet(studentdt);
                oWB.AddWorksheet(parentRelationsdt);


                var worksheet1 = oWB.Worksheet(1);
                var worksheet2 = oWB.Worksheet(2);
                var worksheet3 = oWB.Worksheet(3);


                DataTable validationTable = new DataTable();
                validationTable.Columns.Add("First Name");
                validationTable.Columns.Add("Middle Name");
                validationTable.Columns.Add("Last Name");
                validationTable.Columns.Add("Primary Mobile");
                validationTable.Columns.Add("Alternative Mobile");
                validationTable.Columns.Add("Landline Number");
                validationTable.Columns.Add("EmailId");
                validationTable.Columns.Add("Profession Type");
                validationTable.Columns.Add("Student");
                validationTable.Columns.Add("ParentRelationship");
                validationTable.Columns.Add("ParentOrganization");

                var parentts = studentMethod.GetAllParentsforstudents();

                foreach (var ob in StudentList)
                {
                    var res = parentts.Where(a => a.StudentID == ob.ID).ToList();
                    if (res.Count() > 0)
                    {
                        foreach (var os in res)
                        {
                            DataRow dr = validationTable.NewRow();
                            dr["First Name"] = os.FirstName;
                            dr["Middle Name"] = os.MiddleName;
                            dr["Last Name"] = os.LastName;
                            dr["Primary Mobile"] = os.PrimaryMobile;
                            dr["Alternative Mobile"] = os.AlternativeMobile;
                            dr["Landline Number"] = os.LandlineNumber;
                            dr["EmailId"] = os.EmailId;
                            dr["Profession Type"] = os.ProfessionTypeName;
                            dr["Student"] = ob.FirstName + " " + ob.LastName + "-" + ob.StudentCode;
                            dr["ParentRelationship"] = os.ProfessionTypeName;
                            dr["ParentOrganization"] = os.ParentOrganization;
                            validationTable.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        DataRow dr = validationTable.NewRow();
                        dr["First Name"] = "";
                        dr["Middle Name"] = "";
                        dr["Last Name"] = "";
                        dr["Primary Mobile"] = "";
                        dr["Alternative Mobile"] = "";
                        dr["Landline Number"] = "";
                        dr["EmailId"] = "";
                        dr["Profession Type"] = "";
                        dr["Student"] = ob.FirstName + " " + ob.LastName + "-" + ob.StudentCode;
                        dr["ParentRelationship"] = "";
                        dr["ParentOrganization"] = "";
                        validationTable.Rows.Add(dr);
                    }




                }

                validationTable.TableName = "StudentParent_Details";
                var worksheet = oWB.AddWorksheet(validationTable);
                worksheet.Column(8).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
                worksheet.Column(8).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(8).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(8).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(8).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(9).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
                worksheet.Column(9).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(9).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(9).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(9).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(10).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
                worksheet.Column(10).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(10).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(10).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(10).SetDataValidation().ErrorMessage = "Please Select From The List";
                worksheet1.Hide();
                worksheet2.Hide();
                worksheet3.Hide();

                Byte[] workbookBytes;
                MemoryStream ms = GetStream(oWB);
                workbookBytes = ms.ToArray();
                return File(workbookBytes, "application/ms-excel", $"StudentParentSample.xlsx");
            }
            else if (type == 3)//Emergency
            {
                XLWorkbook oWB = new XLWorkbook();
                var StudentList = studentAggregateRepository.GetAllStudent().ToList();

                DataTable studentdt = new DataTable();
                studentdt.Columns.Add("Student");
                foreach (var a in StudentList)
                {
                    DataRow dr = studentdt.NewRow();
                    dr["Student"] = a.FirstName + " " + a.LastName + "-" + a.StudentCode;
                    studentdt.Rows.Add(dr);
                }
                studentdt.TableName = "Student";

                int lastCellNo1 = studentdt.Rows.Count + 1;

                oWB.AddWorksheet(studentdt);

                var worksheet1 = oWB.Worksheet(1);

                DataTable validationTable = new DataTable();
                validationTable.Columns.Add("Student");
                validationTable.Columns.Add("Contact Person Name");
                validationTable.Columns.Add("Mobile");
                validationTable.Columns.Add("Email Id");
                var transport = studentMethod.studentEmergencyContactRepository.ListAllAsync().Result.Where(a => a.Active == true);
                foreach (var det in StudentList)
                {
                    var sm = transport.Where(a => a.StudentID == det.ID).FirstOrDefault();
                    DataRow dr = validationTable.NewRow();
                    dr["Student"] = det.FirstName + " " + det.LastName + "-" + det.StudentCode;
                    dr["Contact Person Name"] = sm == null ? "" : sm.ContactPersonName;
                    dr["Mobile"] = sm == null ? "" : sm.Mobile;
                    dr["Email Id"] = sm == null ? "" : sm.EmailId;

                    validationTable.Rows.Add(dr);
                }

                validationTable.TableName = "Student_Emergency_Details";
                var worksheet = oWB.AddWorksheet(validationTable);
                worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
                worksheet.Column(1).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(1).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(1).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(1).SetDataValidation().ErrorMessage = "Please Select From The List";




                worksheet1.Hide();

                Byte[] workbookBytes;
                MemoryStream ms = GetStream(oWB);
                workbookBytes = ms.ToArray();
                return File(workbookBytes, "application/ms-excel", $"StudentEmergencyContactSample.xlsx");
            }
            else if (type == 4)//Address
            {
                XLWorkbook oWB = new XLWorkbook();
                var StudentList = studentAggregateRepository.GetAllStudent().ToList();
                var city = cityRepository.GetAllCity().Where(a => a.Active == true).ToList();
                var state = stateRepository.GetAllState().Where(a => a.Active == true).ToList();
                var country = countryRepository.GetAllCountries().Where(a => a.Active == true).ToList();
                var addresstype = addressTypeRepository.GetAllAddressType().Where(a => a.Active == true).ToList();

                var wsData = oWB.Worksheets.Add("Data");
                wsData.Cell(1, 1).Value = "Student";
                int i = 1;
                foreach (var a in StudentList)
                {
                    wsData.Cell(++i, 1).Value = a.FirstName + " " + a.LastName + "-" + a.StudentCode;
                }
                wsData.Range("A2:A" + i).AddToNamed("Student");
                wsData.Cell(1, 2).Value = "Address Type";
                i = 1;
                foreach (var a in addresstype)
                {
                    wsData.Cell(++i, 2).Value = "T_" + a.ID + "_" + a.Name;
                }
                wsData.Range("B2:B" + i).AddToNamed("Address Type");

                wsData.Cell(1, 3).Value = "Country";
                i = 1;
                foreach (var a in country)
                {
                    wsData.Cell(++i, 3).Value = "C_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                }
                wsData.Range("C2:C" + i).AddToNamed("Country");

                int j = 3;
                foreach (var a in country)
                {
                    wsData.Cell(1, ++j).Value = "C_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in state.Where(m => m.CountryID == a.ID).ToList())
                    {
                        wsData.Cell(++k, j).Value = "S_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("C_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }
                var wsData1 = oWB.Worksheets.Add("Data1");
                j = 0;
                //wsData1.Cell(1, ++j).Value = "State";
                //i = 1;
                //foreach (var a in state)
                //{
                //    wsData1.Cell(++i, j).Value = "S_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                //}
                //wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(i, j)).AddToNamed("State");
                foreach (var a in state)
                {
                    wsData1.Cell(1, ++j).Value = "S_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in city.Where(m => m.StateID == a.ID).ToList())
                    {
                        wsData1.Cell(++k, j).Value = "Y_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(k, j)).AddToNamed("S_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }


                DataTable validationTable = new DataTable();
                validationTable.Columns.Add("Student");
                validationTable.Columns.Add("Address Type");
                validationTable.Columns.Add("Address Line 1");
                validationTable.Columns.Add("Address Line 2");
                validationTable.Columns.Add("Country");
                validationTable.Columns.Add("State");
                validationTable.Columns.Add("City");
                validationTable.Columns.Add("Postal Code");
                var address = studentMethod.GetAllStudentAddress();
                foreach (var det in StudentList)
                {
                    var smlist = address.Where(a => a.StudentId == det.ID).ToList();
                    if (smlist.Count() > 0)
                    {
                        foreach (var sm in smlist)
                        {
                            DataRow dr = validationTable.NewRow();
                            dr["Student"] = det.FirstName + " " + det.LastName + "-" + det.StudentCode;
                            dr["Address Type"] = sm == null ? "" : "T_" + sm.AddressTypeID + "_" + sm.AddressTypeName;
                            dr["Address Line 1"] = sm == null ? "" : sm.AddressLine1;
                            dr["Address Line 2"] = sm == null ? "" : sm.AddressLine2;
                            dr["Country"] = sm == null ? "" : "C_" + sm.CountryID + "_" + sm.CountryName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                            dr["State"] = sm == null ? "" : "S_" + sm.StateID + "_" + sm.StateName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"); ;
                            dr["City"] = sm == null ? "" : "Y_" + sm.CityID + "_" + sm.CityName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                            dr["Postal Code"] = sm == null ? "" : sm.PostalCode;

                            validationTable.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        DataRow dr = validationTable.NewRow();
                        dr["Student"] = det.FirstName + " " + det.LastName + "-" + det.StudentCode;
                        dr["Address Type"] = "";
                        dr["Address Line 1"] = "";
                        dr["Address Line 2"] = "";
                        dr["Country"] = "";
                        dr["State"] = "";
                        dr["City"] = "";
                        dr["Postal Code"] = "";

                        validationTable.Rows.Add(dr);
                    }
                }

                validationTable.TableName = "Student_Address_Details";
                var worksheet = oWB.AddWorksheet(validationTable);
                worksheet.Column(1).SetDataValidation().List(wsData.Range("Student"), true);
                worksheet.Column(1).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(1).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(1).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(1).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(2).SetDataValidation().List(wsData.Range("Address Type"), true);
                worksheet.Column(2).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(2).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(2).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(2).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(5).SetDataValidation().List(wsData.Range("Country"), true);
                worksheet.Column(5).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(5).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(5).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(5).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(6).SetDataValidation().InCellDropdown = true;
                worksheet.Column(6).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(6).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(6).SetDataValidation().List("=INDIRECT(SUBSTITUTE(E1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(6).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(6).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(6).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(6).SetDataValidation().ErrorMessage = "Please Select From The List";


                worksheet.Column(7).SetDataValidation().InCellDropdown = true;
                worksheet.Column(7).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(7).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(7).SetDataValidation().List("=INDIRECT(SUBSTITUTE(F1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(7).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(7).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(7).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(7).SetDataValidation().ErrorMessage = "Please Select From The List";
                wsData.Hide();
                wsData1.Hide();

                Byte[] workbookBytes;
                MemoryStream ms = GetStream(oWB);
                workbookBytes = ms.ToArray();
                return File(workbookBytes, "application/ms-excel", $"StudentAddressSample.xlsx");
            }
            else if (type == 5)//Transport
            {
                XLWorkbook oWB = new XLWorkbook();
                var StudentList = studentAggregateRepository.GetAllStudent().ToList();
                var city = cityRepository.GetAllCity().Where(a => a.Active == true).ToList();
                var state = stateRepository.GetAllState().Where(a => a.Active == true).ToList();
                var country = countryRepository.GetAllCountries().Where(a => a.Active == true).ToList();
                var addresstype = addressTypeRepository.GetAllAddressType().Where(a => a.Active == true).ToList();
                var location = studentMethod.vehicleLocationRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var departure = studentMethod.departureTimeRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();

                var wsData = oWB.Worksheets.Add("Data");
                wsData.Cell(1, 1).Value = "Student";
                int i = 1;
                foreach (var a in StudentList)
                {
                    wsData.Cell(++i, 1).Value = a.FirstName + " " + a.LastName + "-" + a.StudentCode;
                }
                wsData.Range("A2:A" + i).AddToNamed("Student");
                wsData.Cell(1, 2).Value = "Is Required";
                i = 1;

                wsData.Cell(++i, 2).Value = "Yes";
                wsData.Cell(++i, 2).Value = "No";

                wsData.Range("B2:B" + i).AddToNamed("Is Required");
                wsData.Cell(1, 3).Value = "Country";
                i = 1;
                foreach (var a in country)
                {
                    wsData.Cell(++i, 3).Value = "C_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                }
                wsData.Range("C2:C" + i).AddToNamed("Country");

                int j = 3;
                foreach (var a in country)
                {
                    wsData.Cell(1, ++j).Value = "C_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in state.Where(m => m.CountryID == a.ID).ToList())
                    {
                        wsData.Cell(++k, j).Value = "S_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("C_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }
                var wsData1 = oWB.Worksheets.Add("Data1");
                j = 0;

                foreach (var a in state)
                {
                    wsData1.Cell(1, ++j).Value = "S_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in city.Where(m => m.StateID == a.ID).ToList())
                    {
                        wsData1.Cell(++k, j).Value = "Y_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(k, j)).AddToNamed("S_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }


                var wsData2 = oWB.Worksheets.Add("Data2");
                j = 0;
                foreach (var a in city)
                {
                    wsData2.Cell(1, ++j).Value = "Y_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in location.Where(m => m.CityID == a.ID).ToList())
                    {
                        wsData2.Cell(++k, j).Value = "L_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData2.Range(wsData2.Cell(2, j), wsData2.Cell(k, j)).AddToNamed("Y_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }

                var wsData3 = oWB.Worksheets.Add("Data3");
                j = 0;
                foreach (var a in location)
                {
                    wsData3.Cell(1, ++j).Value = "L_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in departure.Where(m => m.LocationID == a.ID).ToList())
                    {
                        wsData3.Cell(++k, j).Value = "D_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData3.Range(wsData3.Cell(2, j), wsData3.Cell(k, j)).AddToNamed("L_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }

                DataTable validationTable = new DataTable();
                validationTable.Columns.Add("Student");
                validationTable.Columns.Add("Is Transport Required");
                validationTable.Columns.Add("Country");
                validationTable.Columns.Add("State");
                validationTable.Columns.Add("City");
                validationTable.Columns.Add("Location");
                validationTable.Columns.Add("Departure Time");
                validationTable.Columns.Add("Distance");
                var trans = studentMethod.GetAllStudentTransportation();
                foreach (var det in StudentList)
                {
                    var sm = trans.Where(a => a.StudentID == det.ID).FirstOrDefault();
                    DataRow dr = validationTable.NewRow();
                    dr["Student"] = det.FirstName + " " + det.LastName + "-" + det.StudentCode;
                    dr["Is Transport Required"] = sm == null ? "" : sm.IsTransport == true ? "Yes" : "No";
                    dr["Country"] = sm == null ? "" : ("C_" + sm.CountryID + "_" + sm.CountryName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                    dr["State"] = sm == null ? "" : ("S_" + sm.StateID + "_" + sm.StateName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                    dr["City"] = sm == null ? "" : ("Y_" + sm.CityID + "_" + sm.CityName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                    dr["Location"] = sm == null ? "" : ("L_" + sm.LocationID + "_" + sm.LocationName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                    dr["Departure Time"] = sm == null ? "" : ("D_" + sm.DepartureID + "_" + sm.departureName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                    dr["Distance"] = sm == null ? "" : sm.Distance.ToString();

                    validationTable.Rows.Add(dr);
                }

                validationTable.TableName = "Student_Transport_Details";
                var worksheet = oWB.AddWorksheet(validationTable);
                worksheet.Column(1).SetDataValidation().List(wsData.Range("Student"), true);
                worksheet.Column(1).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(1).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(1).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(1).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(2).SetDataValidation().List(wsData.Range("Is Required"), true);
                worksheet.Column(2).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(2).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(2).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(2).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(3).SetDataValidation().List(wsData.Range("Country"), true);
                worksheet.Column(3).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(3).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(3).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(3).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(4).SetDataValidation().InCellDropdown = true;
                worksheet.Column(4).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(4).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(4).SetDataValidation().List("=INDIRECT(SUBSTITUTE(C1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(4).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(4).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(4).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(4).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(5).SetDataValidation().InCellDropdown = true;
                worksheet.Column(5).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(5).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(5).SetDataValidation().List("=INDIRECT(SUBSTITUTE(D1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(5).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(5).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(5).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(5).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(6).SetDataValidation().InCellDropdown = true;
                worksheet.Column(6).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(6).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(6).SetDataValidation().List("=INDIRECT(SUBSTITUTE(E1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(6).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(6).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(6).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(6).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(7).SetDataValidation().InCellDropdown = true;
                worksheet.Column(7).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(7).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(7).SetDataValidation().List("=INDIRECT(SUBSTITUTE(F1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(7).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(7).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(7).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(7).SetDataValidation().ErrorMessage = "Please Select From The List";
                wsData.Hide();
                wsData1.Hide();
                wsData2.Hide();
                wsData3.Hide();

                Byte[] workbookBytes;
                MemoryStream ms = GetStream(oWB);
                workbookBytes = ms.ToArray();
                return File(workbookBytes, "application/ms-excel", $"StudentTransportSample.xlsx");
            }
            else if (type == 6)//Previous Academic Details
            {
                XLWorkbook oWB = new XLWorkbook();
                var StudentList = studentAggregateRepository.GetAllStudent().ToList();
                var board = boardRepository.GetAllBoard().Where(a => a.Active == true).ToList();
                var standard = StandardRepository.GetAllStandard().Where(a => a.Active == true).ToList();
                var city = cityRepository.GetAllCity().Where(a => a.Active == true).ToList();
                var state = stateRepository.GetAllState().Where(a => a.Active == true).ToList();
                var country = countryRepository.GetAllCountries().Where(a => a.Active == true).ToList();


                var wsData = oWB.Worksheets.Add("Data");
                wsData.Cell(1, 1).Value = "Student";
                int i = 1;
                foreach (var a in StudentList)
                {
                    wsData.Cell(++i, 1).Value = a.FirstName + " " + a.LastName + "-" + a.StudentCode;
                }
                wsData.Range("A2:A" + i).AddToNamed("Student");
                wsData.Cell(1, 2).Value = "Board";
                i = 1;
                foreach (var a in board)
                {
                    wsData.Cell(++i, 2).Value = "B_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                }
                wsData.Range("B2:B" + i).AddToNamed("Board");

                int j = 2;
                foreach (var a in board)
                {
                    wsData.Cell(1, ++j).Value = "B_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in standard.Where(m => m.BoardID == a.ID).ToList())
                    {
                        wsData.Cell(++k, j).Value = "S_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("B_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }

                wsData.Cell(1, ++j).Value = "Country";
                i = 1;
                foreach (var a in country)
                {
                    wsData.Cell(++i, j).Value = "C_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                }
                wsData.Range(wsData.Cell(2, j), wsData.Cell(i, j)).AddToNamed("Country");

                foreach (var a in country)
                {
                    wsData.Cell(1, ++j).Value = "C_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in state.Where(m => m.CountryID == a.ID).ToList())
                    {
                        wsData.Cell(++k, j).Value = "S_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("C_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }
                var wsData1 = oWB.Worksheets.Add("Data1");
                j = 0;
                //wsData1.Cell(1, ++j).Value = "State";
                //i = 1;
                //foreach (var a in state)
                //{
                //    wsData1.Cell(++i, j).Value = "S_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                //}
                //wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(i, j)).AddToNamed("State");
                foreach (var a in state)
                {
                    wsData1.Cell(1, ++j).Value = "S_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in city.Where(m => m.StateID == a.ID).ToList())
                    {
                        wsData1.Cell(++k, j).Value = "Y_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(k, j)).AddToNamed("S_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }



                DataTable validationTable = new DataTable();
                validationTable.Columns.Add("Student");
                validationTable.Columns.Add("Organization Name");
                validationTable.Columns.Add("Board");
                validationTable.Columns.Add("Standard");
                validationTable.Columns.Add("From Date");
                validationTable.Columns.Add("To Date");
                validationTable.Columns.Add("Reason");
                validationTable.Columns.Add("GradeOrPercentage");
                validationTable.Columns.Add("Country");
                validationTable.Columns.Add("State");
                validationTable.Columns.Add("City");
                var academics = sqlQuery.GetStudentPreviousAcademic();

                foreach (var det in StudentList)
                {
                    var smlist = academics.Where(a => a.StudentID == det.ID).ToList();
                    if(smlist.Count()>0)
                    {
                        foreach(var sm in smlist)
                        {
                            DataRow dr = validationTable.NewRow();
                            dr["Student"] = det.FirstName + " " + det.LastName + "-" + det.StudentCode;
                            dr["Organization Name"] = sm == null ? "" : sm.OrganizationName;
                            dr["Board"] = sm == null ? "" : ("B_" + sm.BoardID + "_" + sm.BoardName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                            dr["Standard"] = sm == null ? "" : "S_" + sm.ClassID + "_" + sm.StandardName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                            dr["From Date"] = sm == null ? "" : sm.FromDate.ToString("dd-MM-yyyy");
                            dr["To Date"] = sm == null ? "" : sm.ToDate.ToString("dd-MM-yyyy");
                            dr["Reason"] = sm == null ? "" : sm.ReasonForChange;
                            dr["GradeOrPercentage"] = sm == null ? "" : sm.GradeOrPercentage;
                            dr["Country"] = sm == null ? "" : "C_" + sm.CountryID + "_" + sm.CountryName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                            dr["State"] = sm == null ? "" : "S_" + sm.StateId + "_" + sm.StateName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                            dr["City"] = sm == null ? "" : "Y_" + sm.CityId + "_" + sm.CityName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                            validationTable.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        DataRow dr = validationTable.NewRow();
                        dr["Student"] = det.FirstName + " " + det.LastName + "-" + det.StudentCode;
                        dr["Organization Name"] = "";
                        dr["Board"] = "";
                        dr["Standard"] = ""; 
                        dr["From Date"] = "";
                        dr["To Date"] = "";
                        dr["Reason"] = "";
                        dr["GradeOrPercentage"] ="";
                        dr["Country"] = "";
                        dr["State"] = "";
                        dr["City"] = "";
                        validationTable.Rows.Add(dr);
                    }
                   
                }



                validationTable.TableName = "Student_Previous_Academic";
                var worksheet = oWB.AddWorksheet(validationTable);
                worksheet.Column(1).SetDataValidation().List(wsData.Range("Student"), true);
                worksheet.Column(1).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(1).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(1).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(1).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(3).SetDataValidation().List(wsData.Range("Board"), true);
                worksheet.Column(3).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(3).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(3).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(3).SetDataValidation().ErrorMessage = "Please Select From The List";


                worksheet.Column(4).SetDataValidation().InCellDropdown = true;
                worksheet.Column(4).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(4).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(4).SetDataValidation().List("=INDIRECT(SUBSTITUTE(C1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(4).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(4).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(4).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(4).SetDataValidation().ErrorMessage = "Please Select From The List";


                worksheet.Column(5).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(5).SetDataValidation().AllowedValues = XLAllowedValues.Date;
                worksheet.Column(5).SetDataValidation().MinValue = "E1";
                worksheet.Column(5).SetDataValidation().MaxValue = "E1000";
                worksheet.Column(5).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(5).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(5).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(5).SetDataValidation().ErrorMessage = "Please Enter a Valid Date";

                worksheet.Column(9).SetDataValidation().List(wsData.Range("Country"), true);
                worksheet.Column(9).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(9).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(9).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(9).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(10).SetDataValidation().InCellDropdown = true;
                worksheet.Column(10).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(10).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(10).SetDataValidation().List("=INDIRECT(SUBSTITUTE(I1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(10).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(10).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(10).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(10).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(11).SetDataValidation().InCellDropdown = true;
                worksheet.Column(11).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(11).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(11).SetDataValidation().List("=INDIRECT(SUBSTITUTE(J1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(11).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(11).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(11).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(11).SetDataValidation().ErrorMessage = "Please Select From The List";

               // wsData.Hide();
              //  wsData1.Hide();

                Byte[] workbookBytes;
                MemoryStream ms = GetStream(oWB);
                workbookBytes = ms.ToArray();
                return File(workbookBytes, "application/ms-excel", $"StudentreviousAcademic.xlsx");
            }
            else if (type == 7)//Current Academic Details
            {
                XLWorkbook oWB = new XLWorkbook();
                var StudentList = studentAggregateRepository.GetAllStudent().ToList();
                var sessions = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true).ToList();
                var organisation = studentMethod.GetOrganisationAcademic();
                var board = boardRepository.GetAllBoard().Where(a => a.Active == true).ToList();
                var standard = studentMethod.GetAcademicstandard();
                var wings = studentMethod.GetAcademicWing();
                var sections = studentMethod.GetAllAcademicSection();
                var subjectgroup = studentMethod.GetAcademicSubjectGroup();

                int s = 1;
                var wsData = oWB.Worksheets.Add("Data");
                wsData.Cell(1, s).Value = "Student";
                int i = 1;
                foreach (var a in StudentList)
                {
                    wsData.Cell(++i, s).Value = a.FirstName + " " + a.LastName + "-" + a.StudentCode;
                }
                wsData.Range(wsData.Cell(2, s), wsData.Cell(i, s)).AddToNamed("Student");
                s = s + 1;
                wsData.Cell(1, s).Value = "Session";
                i = 1;
                foreach (var a in sessions)
                {
                    wsData.Cell(++i, s).Value = "S_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                }
                wsData.Range(wsData.Cell(2, s), wsData.Cell(i, s)).AddToNamed("Session");

                s = s + 1;
                int j = s;
                foreach (var a in sessions)
                {
                    wsData.Cell(1, ++j).Value = "S_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in organisation.Where(m => m.GroupID == a.ID).ToList())
                    {
                        wsData.Cell(++k, j).Value = "O_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And").Replace(",", "");
                    }
                    wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("S_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }
                s = j;
                s = s + 1;
                wsData.Cell(1, s).Value = "Organisation";
                i = 1;
                foreach (var a in organisation)
                {
                    wsData.Cell(++i, s).Value = "O_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And").Replace(",", "");
                }
                wsData.Range(wsData.Cell(2, s), wsData.Cell(i, s)).AddToNamed("Organisation");

                j = s;
                foreach (var a in organisation)
                {
                    wsData.Cell(1, ++j).Value = "O_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And").Replace(",", "");
                    int k = 1;
                    foreach (var b in standard.Where(m => m.organisationID == a.ID).ToList())
                    {
                        wsData.Cell(++k, j).Value = "B_" + b.ID + "_" + b.Status.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_" + b.GroupID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("O_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And").Replace(",", ""));
                }
                s = j;
                var wsData1 = oWB.Worksheets.Add("Data1");
                s = 1;
                wsData1.Cell(1, s).Value = "Standard";
                i = 1;
                foreach (var b in standard)
                {
                    wsData1.Cell(++i, s).Value = "B_" + b.ID + "_" + b.Status.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_" + b.GroupID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                }
                wsData1.Range(wsData1.Cell(2, s), wsData1.Cell(i, s)).AddToNamed("Standard");

                s = s + 1;
                j = s;
                foreach (var a in standard)
                {
                    wsData1.Cell(1, ++j).Value = "B_" + a.ID + "_" + a.Status.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_" + a.GroupID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in sections.Where(m => m.AcademicStandardId == a.ID).ToList())
                    {
                        wsData1.Cell(++k, j).Value = "S_" + b.ID + "_" + b.SessionName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(k, j)).AddToNamed("B_" + a.ID + "_" + a.Status.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_" + a.GroupID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }
                s = j;

                s = s + 1;
                j = s;
                foreach (var a in standard)
                {
                    wsData1.Cell(1, ++j).Value = "B_" + a.ID + "_" + a.Status.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_" + a.GroupID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_G";
                    int k = 1;
                    foreach (var b in subjectgroup.Where(m => m.GroupID == a.ID).ToList())
                    {
                        wsData1.Cell(++k, j).Value = "M_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(k, j)).AddToNamed("B_" + a.ID + "_" + a.Status.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_" + a.GroupID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_G");
                }
                s = j;

                s = s + 1;
                j = s;
                foreach (var a in standard)
                {
                    wsData1.Cell(1, ++j).Value = "B_" + a.ID + "_" + a.Status.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_" + a.GroupID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_W";
                    int k = 1;
                    foreach (var b in wings.Where(m => m.AcademicStandardID == a.ID).ToList())
                    {
                        wsData1.Cell(++k, j).Value = "W_" + b.ID + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(k, j)).AddToNamed("B_" + a.ID + "_" + a.Status.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_" + a.GroupID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_W");
                }

                DataTable validationTable = new DataTable();
                validationTable.Columns.Add("Student");
                validationTable.Columns.Add("Session");
                validationTable.Columns.Add("Organisation");
                validationTable.Columns.Add("Standard");
                validationTable.Columns.Add("Section");
                validationTable.Columns.Add("Wing");
                validationTable.Columns.Add("Subject Group");
                validationTable.Columns.Add("Roll No");
                var studentstandards = sqlQuery.Get_View_Academic_Student();
                if (StudentList.Count > 0)
                {
                    foreach (var a in StudentList)
                    {
                        DataRow dr = validationTable.NewRow();
                        dr["Student"] = a.FirstName + " " + a.LastName + "-" + a.StudentCode;
                        dr["Session"] = studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).Select(m => new { name = "S_" + m.AcademicSessionId + "_" + m.SessionName.Replace(" ", "_").Replace("-", "_").Replace("&", "And") }).FirstOrDefault().name;
                        dr["Organisation"] = studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).Select(m => new { name = "O_" + m.OraganisationAcademicId + "_" + m.OraganisationName.Replace(" ", "_").Replace("-", "_").Replace("&", "And").Replace(",", "") }).FirstOrDefault().name;
                        dr["Standard"] = studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).Select(m => new { name = "B_" + m.AcademicStandardId + "_" + m.BoardName.Replace(" ", "_").Replace("-", "_").Replace("&", "And") + "_" + m.BoardId + "_" + m.StandardName.Replace(" ", "_").Replace("-", "_").Replace("&", "And") }).FirstOrDefault().name;
                        dr["Section"] = studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).Select(m => new { name = "S_" + m.AcademicSectionId + "_" + m.SectionName.Replace(" ", "_").Replace("-", "_").Replace("&", "And") }).FirstOrDefault().name;
                        dr["Wing"] = studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).Select(m => new { name = "W_" + m.AcademicStandardWingId + "_" + m.WingName.Replace(" ", "_").Replace("-", "_").Replace("&", "And") }).FirstOrDefault().name;
                        dr["Subject Group"] = studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).Select(m => new { name = m.AcademicSubjectCombinationId == 0 ? "" : "M_" + m.AcademicSubjectCombinationId + "_" + m.SubjectGroupName.Replace(" ", "_").Replace("-", "_").Replace("&", "And") }).FirstOrDefault().name;
                        dr["Roll No"] = studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).FirstOrDefault() == null ? "" : studentstandards.Where(m => m.StudentId == a.ID && m.IsAvailable == true).Select(m => new { name = m.RollNo }).FirstOrDefault().name;
                        validationTable.Rows.Add(dr);
                    }
                }


                validationTable.TableName = "Student_Current_Academic";
                var worksheet = oWB.AddWorksheet(validationTable);
                worksheet.Column(1).SetDataValidation().List(wsData.Range("Student"), true);
                worksheet.Column(1).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(1).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(1).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(1).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(2).SetDataValidation().List(wsData.Range("Session"), true);
                worksheet.Column(2).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(2).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(2).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(2).SetDataValidation().ErrorMessage = "Please Select From The List";


                worksheet.Column(3).SetDataValidation().InCellDropdown = true;
                worksheet.Column(3).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(3).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(3).SetDataValidation().List("=INDIRECT(SUBSTITUTE(B1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(3).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(3).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(3).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(3).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(4).SetDataValidation().InCellDropdown = true;
                worksheet.Column(4).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(4).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(4).SetDataValidation().List("=INDIRECT(SUBSTITUTE(C1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(4).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(4).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(4).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(4).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(5).SetDataValidation().InCellDropdown = true;
                worksheet.Column(5).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(5).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(5).SetDataValidation().List("=INDIRECT(SUBSTITUTE(D1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
                worksheet.Column(5).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(5).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(5).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(5).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(6).SetDataValidation().InCellDropdown = true;
                worksheet.Column(6).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(6).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(6).SetDataValidation().List("=INDIRECT(D1&" + '"' + "_W" + '"' + ")", true);
                worksheet.Column(6).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(6).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(6).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(6).SetDataValidation().ErrorMessage = "Please Select From The List";

                worksheet.Column(7).SetDataValidation().InCellDropdown = true;
                worksheet.Column(7).SetDataValidation().Operator = XLOperator.Between;
                worksheet.Column(7).SetDataValidation().AllowedValues = XLAllowedValues.List;
                worksheet.Column(7).SetDataValidation().List("=INDIRECT(D1&" + '"' + "_G" + '"' + ")", true);
                worksheet.Column(7).SetDataValidation().ShowErrorMessage = true;
                worksheet.Column(7).SetDataValidation().ErrorTitle = "Warning";
                worksheet.Column(7).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
                worksheet.Column(7).SetDataValidation().ErrorMessage = "Please Select From The List";



                wsData.Hide();
                wsData1.Hide();

                Byte[] workbookBytes;
                MemoryStream ms = GetStream(oWB);
                workbookBytes = ms.ToArray();
                return File(workbookBytes, "application/ms-excel", $"StudentCurrentAcademicSample.xlsx");
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }



        #endregion-------------------------------student--------------------------------

        #region-------------Student Parent Excel Upload---------------------------------
        public IActionResult DownloadStudentParentSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            XLWorkbook oWB = new XLWorkbook();
            var ProfessionList = studentAggregateRepository.GetAllProfessionType().ToList();
            var StudentList = studentAggregateRepository.GetAllStudent().ToList();
            var ParentRelationshipList = studentAggregateRepository.GetAllParentRelationshipType().ToList();


            DataTable professiondt = new DataTable();
            professiondt.Columns.Add("Profession Type");
            foreach (var a in ProfessionList)
            {
                DataRow dr = professiondt.NewRow();
                dr["Profession Type"] = a.Name;
                professiondt.Rows.Add(dr);
            }
            professiondt.TableName = "Profession Type";

            DataTable parentRelationsdt = new DataTable();
            parentRelationsdt.Columns.Add("ParentRelationship");
            foreach (var a in ParentRelationshipList)
            {
                DataRow dr = parentRelationsdt.NewRow();
                dr["ParentRelationship"] = a.Name;
                parentRelationsdt.Rows.Add(dr);
            }
            parentRelationsdt.TableName = "ParentRelationship";

            DataTable studentdt = new DataTable();
            studentdt.Columns.Add("Student");
            foreach (var a in StudentList)
            {
                DataRow dr = studentdt.NewRow();
                dr["Student"] = a.FirstName + " " + a.LastName + "-" + a.StudentCode;
                studentdt.Rows.Add(dr);
            }
            studentdt.TableName = "Student";


            int lastCellNo1 = professiondt.Rows.Count + 1;
            int lastCellNo2 = studentdt.Rows.Count + 1;
            int lastCellNo3 = parentRelationsdt.Rows.Count + 1;


            oWB.AddWorksheet(professiondt);
            oWB.AddWorksheet(studentdt);
            oWB.AddWorksheet(parentRelationsdt);


            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            var worksheet3 = oWB.Worksheet(3);


            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("First Name");
            validationTable.Columns.Add("Middle Name");
            validationTable.Columns.Add("Last Name");
            validationTable.Columns.Add("Primary Mobile");
            validationTable.Columns.Add("Alternative Mobile");
            validationTable.Columns.Add("Landline Number");
            validationTable.Columns.Add("EmailId");
            validationTable.Columns.Add("Profession Type");
            validationTable.Columns.Add("Student");
            validationTable.Columns.Add("ParentRelationship");
            validationTable.Columns.Add("ParentOrganization");

            validationTable.TableName = "StudentParent_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(8).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(9).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(10).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);

            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StudentParentSample.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadStudentParent(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                if (file != null)
                {

                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[3];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        var request = studentAggregateRepository.GetAllParent();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            Parent parent = new Parent();
                            string PrimaryNumber = firstWorksheet.Cells[i, 4].Value.ToString();

                            if (PrimaryNumber != null)
                            {
                                parent.ModifiedId = userId;
                                parent.FirstName = firstWorksheet.Cells[i, 1].Value.ToString();
                                if (firstWorksheet.Cells[i, 2] != null && firstWorksheet.Cells[i, 2].Value != null)
                                {
                                    parent.MiddleName = firstWorksheet.Cells[i, 2].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 3] != null && firstWorksheet.Cells[i, 3].Value != null)
                                {
                                    parent.LastName = firstWorksheet.Cells[i, 3].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 4] != null && firstWorksheet.Cells[i, 4].Value != null)
                                {
                                    parent.PrimaryMobile = firstWorksheet.Cells[i, 4].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 5] != null && firstWorksheet.Cells[i, 5].Value != null)
                                {
                                    parent.AlternativeMobile = firstWorksheet.Cells[i, 5].Value.ToString();

                                }
                                if (firstWorksheet.Cells[i, 6] != null && firstWorksheet.Cells[i, 6].Value != null)
                                {
                                    parent.LandlineNumber = firstWorksheet.Cells[i, 6].Value.ToString();

                                }
                                parent.EmailId = firstWorksheet.Cells[i, 7].Value.ToString();
                                string ProfessionTypeName = firstWorksheet.Cells[i, 8].Value.ToString();
                                string stdcd = firstWorksheet.Cells[i, 9].Value.ToString();
                                string ParentRelationshipName = firstWorksheet.Cells[i, 10].Value.ToString();
                                parent.ProfessionTypeID = studentAggregateRepository.GetAllProfessionType().Where(a => a.Name == ProfessionTypeName).Select(a => a.ID).FirstOrDefault();
                                string StudentCode = stdcd.Split("-")[1].ToString();
                                parent.StudentID = studentAggregateRepository.GetAllStudent().Where(a => a.StudentCode == StudentCode).Select(a => a.ID).FirstOrDefault(); ;
                                parent.ParentRelationshipID = studentAggregateRepository.GetAllParentRelationshipType().Where(a => a.Name == ParentRelationshipName).Select(a => a.ID).FirstOrDefault();
                                if (firstWorksheet.Cells[i, 11] != null && firstWorksheet.Cells[i, 11].Value != null)
                                {
                                    parent.ParentOrganization = firstWorksheet.Cells[i, 11].Value.ToString();

                                }
                                parent.Active = true;
                                parent.InsertedId = userId;
                                parent.IsAvailable = true;
                                parent.ModifiedId = userId;
                                parent.Status = EntityStatus.ACTIVE;
                                long id = studentAggregateRepository.CreateParent(parent);
                                if (id > 0)
                                {
                                    Access accesss = new Access();
                                    accesss.RoleID = 6;
                                    accesss.Username = parent.PrimaryMobile;
                                    accesss.EmployeeID = id;
                                    accesss.Active = true;
                                    accesss.InsertedDate = DateTime.Now;
                                    accesss.InsertedId = userId;
                                    accesss.ModifiedDate = DateTime.Now;
                                    accesss.ModifiedId = userId;
                                    accessRepository.CreateAccess(accesss);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(Index));
        }

        public IActionResult DownloadStudentParent()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();

            moduledt.Columns.Add("First Name");
            moduledt.Columns.Add("Middle Name");
            moduledt.Columns.Add("Last Name");
            moduledt.Columns.Add("Primary Mobile");
            moduledt.Columns.Add("Alternative Mobile");
            moduledt.Columns.Add("Landline Number");
            moduledt.Columns.Add("EmailId");
            moduledt.Columns.Add("Profession Type");
            moduledt.Columns.Add("Student Code");
            moduledt.Columns.Add("Student Name");
            moduledt.Columns.Add("ParentRelationship");
            moduledt.Columns.Add("ParentOrganization");
            var studentparent = studentAggregateRepository.GetAllParent();
            var ProfessionList = studentAggregateRepository.GetAllProfessionType().ToList();
            var StudentList = studentAggregateRepository.GetAllStudent().ToList();
            var ParentRelationshipList = studentAggregateRepository.GetAllParentRelationshipType().ToList();


            if (studentparent != null)
            {
                foreach (var a in studentparent)
                {
                    DataRow dr = moduledt.NewRow();
                    dr["First Name"] = a.FirstName;
                    dr["Middle Name"] = a.MiddleName;
                    dr["Last Name"] = a.LastName;
                    dr["Primary Mobile"] = a.PrimaryMobile;
                    dr["Alternative Mobile"] = a.AlternativeMobile;
                    dr["Landline Number"] = a.LandlineNumber;
                    dr["EmailId"] = a.EmailId;
                    dr["Profession Type"] = (a.ProfessionTypeID == null || a.ProfessionTypeID == 0) ? "" : ProfessionList.Where(m => m.ID == a.ProfessionTypeID).FirstOrDefault().Name;
                    dr["Student Code"] = (a.StudentID == null || a.StudentID == 0) ? "" : StudentList.Where(m => m.ID == a.StudentID).FirstOrDefault().StudentCode;
                    dr["Student Name"] = (a.StudentID == null || a.StudentID == 0) ? "" : StudentList.Where(m => m.ID == a.StudentID).FirstOrDefault().FirstName;

                    dr["ParentRelationship"] = (a.ParentRelationshipID == null || a.ParentRelationshipID == 0) ? "" : ParentRelationshipList.Where(m => m.ID == a.ParentRelationshipID).FirstOrDefault().Name;
                    dr["ParentOrganization"] = a.ParentOrganization;
                    moduledt.Rows.Add(dr);
                }
            }
            moduledt.TableName = "StudentsParent";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StudentsParentList.xlsx");
        }
        #endregion

        public async Task<JsonResult> GetStateByCountryId(long id)
        {
            var res = (from a in stateRepository.GetAllState().ToList()
                       where a.CountryID == id
                       select new Web.Models.CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        public async Task<JsonResult> GetCityByStateId(long id)
        {
            var res = (from a in cityRepository.GetAllCity().ToList()
                       where a.StateID == id
                       select new Web.Models.CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        //public JsonResult GetWingByOrganisationId(long id)
        //{
        //    var res = commonMethods.GetSchoolWing().Where(a => a.OrganisationID == id).ToList();
        //    return Json(res);
        //}
        public JsonResult GetStandardByBoardId(long id)
        {
            var res = commonMethods.GetStandard().Where(a => a.BoardID == id).ToList();
            return Json(res);
        }


        #region --------Student Address-------------------
        public IActionResult StudentAddressDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            ViewBag.StudentId = id;
            ViewBag.StudentName = studentAggregateRepository.GetStudentById(id).FirstName;
            var addressTypes = addressTypeRepository.GetAllAddressType();
            var address = studentMethod.GetAllAddressByUsingStudentId(id);
            var res = (from a in addressTypes
                       select new StudentTypewithAddresscls
                       {
                           typeId = a.ID,
                           typeName = a.Name,
                           studentAdress = address.Where(m => m.AddressTypeID == a.ID).FirstOrDefault()
                       }).ToList();

            return View(res);



        }
        public IActionResult CreateOrEditStudentAddress(long? id, long eid, long typeid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            ViewBag.StudentName = studentAggregateRepository.GetStudentById(eid).FirstName;
            StudentAdressCls studentAdressCls = new StudentAdressCls();
            studentAdressCls.addressTypes = addressTypeRepository.GetAllAddressType();
            studentAdressCls.countries = countryRepository.GetAllCountries();
            studentAdressCls.StudentId = eid;
            studentAdressCls.AddressTypeID = typeid;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var studentAddress = studentAggregateRepository.GetStudentAddressById(id.Value);
                studentAdressCls.StudentAddressId = studentAddress.ID;

                var address = addressRepository.GetAddressById(studentAddress.AddressID);
                studentAdressCls.cities = cityRepository.GetAllCityByStateId(address.StateID);
                studentAdressCls.states = stateRepository.GetAllStateByCountryId(address.CountryID);
                studentAdressCls.AddressLine1 = address.AddressLine1;
                studentAdressCls.AddressLine2 = address.AddressLine2;
                studentAdressCls.CityID = address.CityID;
                studentAdressCls.StateID = address.StateID;
                studentAdressCls.CountryID = address.CountryID;
                studentAdressCls.PostalCode = address.PostalCode;
                studentAdressCls.ID = address.ID;
                ViewBag.status = "Update";

            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                studentAdressCls.AddressLine1 = "";
                studentAdressCls.AddressLine2 = "";
                studentAdressCls.CityID = 0;
                studentAdressCls.StateID = 0;
                studentAdressCls.CountryID = 0;
                studentAdressCls.ID = 0;
                studentAdressCls.PostalCode = "";
                studentAdressCls.StudentAddressId = 0;
                ViewBag.status = "Create";
            }
            return View(studentAdressCls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentAddress([Bind("ID,AddressLine1,AddressLine2,AddressTypeID,CityID,CountryID,PostalCode,StateID")] Address address, long StudentAddressId, long StudentId)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                address.ModifiedId = userId;
                address.Active = true;
                if (address.ID == 0 && StudentAddressId == 0)
                {
                    address.InsertedId = userId;
                    address.Status = "CREATED";
                    long id = addressRepository.CreateAddress(address);
                    StudentAddress studentAddress = new StudentAddress();
                    studentAddress.AddressID = id;
                    studentAddress.StudentID = StudentId;
                    studentAddress.Active = true;
                    studentAddress.InsertedId = userId;
                    studentAddress.ModifiedId = userId;
                    studentAddress.Status = "CREATED";
                    studentAggregateRepository.CreateStudentAddress(studentAddress);
                }
                else
                {
                    addressRepository.UpdateAddress(address);
                }

                return Json(1);
            }
            return Json(2);
        }
        [ActionName("DeletestudentAddress")]
        public async Task<IActionResult> StudentAddressDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var studentAddress = studentAggregateRepository.DeleteStudentAddress(id.Value);
            return RedirectToAction(nameof(StudentAddressDetails), new { id = eid });
        }
        #endregion

        #region-------------------- Student Parents--------------
        public IActionResult StudentParentsDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.StudentId = id;
            var res = commonMethods.GetAllParentsByUsingStudentId(id);
            return View(res);
        }
        public IActionResult CreateOrEditStudentParents(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            StudentParentsCls studentParentsCls = new StudentParentsCls();
            studentParentsCls.ProfessionTypes = (from a in studentAggregateRepository.GetAllProfessionType()
                                                 select new Web.Models.CommonMasterModel
                                                 {
                                                     ID = a.ID,
                                                     Name = a.Name
                                                 }).ToList();
            studentParentsCls.ParentRelationships = (from a in studentAggregateRepository.GetAllParentRelationshipType()
                                                     select new Web.Models.CommonMasterModel
                                                     {
                                                         ID = a.ID,
                                                         Name = a.Name
                                                     }).ToList();
            studentParentsCls.StudentID = eid;
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var studentparents = studentAggregateRepository.GetParentById(id.Value);
                studentParentsCls.ID = studentparents.ID;
                studentParentsCls.AlternativeMobile = studentparents.AlternativeMobile;

                studentParentsCls.AlternativeMobile = studentparents.AlternativeMobile;
                studentParentsCls.EmailId = studentparents.EmailId;
                studentParentsCls.FirstName = studentparents.FirstName;
                studentParentsCls.LandlineNumber = studentparents.LandlineNumber;
                studentParentsCls.LastName = studentparents.LastName;
                studentParentsCls.MiddleName = studentparents.MiddleName;
                studentParentsCls.ParentRelationshipID = studentparents.ParentRelationshipID;
                studentParentsCls.PrimaryMobile = studentparents.PrimaryMobile;
                studentParentsCls.ProfessionTypeID = studentparents.ProfessionTypeID;
                studentParentsCls.ParentOrganization = studentparents.ParentOrganization;
                studentParentsCls.Imagepath = studentparents.Image != null ? Path.Combine("/ODMImages/ParentProfile/", studentparents.Image) : null;
                ViewBag.status = "Update";

            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                studentParentsCls.ID = 0;
                studentParentsCls.AlternativeMobile = "";
                studentParentsCls.EmailId = "";
                studentParentsCls.FirstName = "";
                studentParentsCls.LandlineNumber = "";
                studentParentsCls.LastName = "";
                studentParentsCls.MiddleName = "";
                studentParentsCls.ParentRelationshipID = 0;
                studentParentsCls.PrimaryMobile = "";
                studentParentsCls.ProfessionTypeID = 0;
                studentParentsCls.ParentOrganization = "";
                studentParentsCls.Imagepath = null;
                ViewBag.status = "Create";
            }
            return View(studentParentsCls);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentParents([Bind("ID,FirstName,MiddleName,LastName,ParentRelationshipID,PrimaryMobile,ProfessionTypeID,LandlineNumber," +
            "AlternativeMobile,EmailId,StudentID,ParentOrganization")]Parent parent, IFormFile file)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                parent.ModifiedId = userId;
                parent.Active = true;
                if (file != null)
                {
                    // Create a File Info 
                    FileInfo fi = new FileInfo(file.FileName);
                    var newFilename = parent.StudentID + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\ParentProfile\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                    parent.Image = pathToSave;
                }
                if (parent.ID == 0 && parent.ID == 0)
                {
                    parent.InsertedId = userId;
                    parent.Status = "CREATED";
                    long id = studentAggregateRepository.CreateParent(parent);

                    if (parent.PrimaryMobile != null)
                    {
                        Access access = accessRepository.GetAllAccess().Where(a => a.Username == parent.PrimaryMobile).FirstOrDefault();
                        if (access == null)
                        {
                            access = new Access();
                            access.EmployeeID = id;
                            access.Active = true;
                            access.InsertedId = userId;
                            access.Password = "";
                            access.Username = parent.PrimaryMobile;
                            access.RoleID = 6;
                            access.ModifiedId = userId;
                            long ids = accessRepository.CreateAccess(access);

                            Access acces = accessRepository.GetAccessById(ids);
                            var emp = studentAggregateRepository.GetParentById(acces.EmployeeID);
                            Thread t1 = new Thread(delegate ()
                            {
                                studentSmsnMails.Parentcreated(emp.FirstName, emp.EmailId, emp.PrimaryMobile);
                            });
                            t1.Start();


                        }
                    }
                }
                else
                {
                    studentAggregateRepository.UpdateParent(parent);
                    if (parent.PrimaryMobile != null)
                    {
                        Access access = accessRepository.GetAllAccess().Where(a => a.EmployeeID == parent.ID).FirstOrDefault();
                        if (access == null)
                        {
                            access = accessRepository.GetAllAccess().Where(a => a.Username == parent.PrimaryMobile).FirstOrDefault();
                            if (access == null)
                            {
                                access = new Access();
                                access.EmployeeID = parent.ID;
                                access.Active = true;
                                access.InsertedId = userId;
                                access.Password = "";
                                access.Username = parent.PrimaryMobile;
                                access.RoleID = 6;
                                access.ModifiedId = userId;
                                accessRepository.CreateAccess(access);
                            }
                        }
                        else
                        {
                            access.Username = parent.PrimaryMobile;
                            access.ModifiedId = userId;
                            accessRepository.UpdateAccess(access);
                        }
                    }
                }
                return Json(1);
            }
            return Json(2);
        }
        [ActionName("DeletestudentParents")]
        public async Task<IActionResult> StudentParentsDeleteConfirmed(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var student = studentAggregateRepository.DeleteParent(id.Value);
            return RedirectToAction(nameof(StudentParentsDetails), new { id = eid });
        }
        #endregion
        #region ------------------Student Academic Details-------------------------
        public IActionResult StudentAcademicDetails(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Employee Experience", accessId, "List", "HR", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            ViewBag.StudentId = id;
            var res = studentMethod.GetAllStudentAcademicByStudentId(id.Value);
            return View(res);
        }
        public IActionResult CreateOrEditStudentAcademic(long? id, long sid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            StudentAcademicModelClass studentacademic = new StudentAcademicModelClass();
            studentacademic.StudentID = sid;
            ViewBag.BoardList = boardRepository.GetAllBoard();
            ViewBag.ClassList = StandardRepository.GetAllStandard();

            if (id != null && id != 0)
            {
                if (commonMethods.checkaccessavailable("Student Document", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var employeeExp = studentAggregateRepository.GetStudentAcademicByStudentId(sid).ToList().Where(a => a.ID == id.Value).FirstOrDefault();
                studentacademic.BoardID = employeeExp.BoardID;
                studentacademic.FromDate = employeeExp.FromDate;
                studentacademic.ToDate = employeeExp.ToDate;
                studentacademic.ClassID = employeeExp.ClassID;
                studentacademic.OrganizationName = employeeExp.OrganizationName;
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("EDUCATIONAL").ID;
                studentacademic.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                    where a.DocumentTypeID == documentTypeId
                                                    select new Web.Models.documentTypesCls
                                                    {
                                                        TypeId = a.DocumentTypeID,
                                                        ID = a.ID,
                                                        Name = a.Name,
                                                        IsRequired = studentMethod.studentRequiredDocument(sid, a.DocumentTypeID, a.ID, employeeExp.ID)
                                                    }).ToList();
                ViewBag.status = "Update";
            }
            else
            {
                if (commonMethods.checkaccessavailable("Student Document", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                studentacademic.BoardID = 0;
                studentacademic.FromDate = DateTime.Now.Date;
                studentacademic.ToDate = DateTime.Now.Date;
                studentacademic.ClassID = 0;
                studentacademic.OrganizationName = "";
                var documentTypeId = documentTypeRepository.GetDocumentTypeByName("EDUCATIONAL").ID;
                studentacademic.documentTypesCls = (from a in documentSubTypeRepository.GetAllDocumentSubTypes().ToList()
                                                    where a.DocumentTypeID == documentTypeId
                                                    select new Web.Models.documentTypesCls
                                                    {
                                                        TypeId = a.DocumentTypeID,
                                                        ID = a.ID,
                                                        Name = a.Name,
                                                        IsRequired = false
                                                    }).ToList();
                var docsubtypelist = documentSubTypeRepository.GetAllDocumentSubTypes().Select(a => a.ID).ToList();

                var studentdoclist = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.StudentID == sid).Select(a => a.DocumentSubTypeID).ToList();

                var resuit = docsubtypelist.Except(studentdoclist);



                ViewBag.status = "Create";
            }
            return View(studentacademic);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentAcademic([Bind("ID,BoardID,StudentID,FromDate,ToDate,ClassID,OrganizationName")] StudentAcademic studentAcademic, [Bind("documentTypesCls")] List<Web.Models.documentTypesCls> documentTypesCls)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {

                studentAcademic.ModifiedId = userId;
                studentAcademic.Active = true;
                if (studentAcademic.ID == 0)
                {
                    studentAcademic.InsertedId = userId;
                    studentAcademic.Status = EntityStatus.ACTIVE;
                    long sid = studentAggregateRepository.CreateStudentAcademic(studentAcademic);
                    foreach (var item in documentTypesCls)
                    {
                        if (item.IsRequired == true)
                        {
                            StudentRequiredDocument studentRequired = new StudentRequiredDocument();
                            studentRequired.Active = true;
                            studentRequired.DocumentSubTypeID = item.ID;
                            studentRequired.DocumentTypeID = item.TypeId;
                            studentRequired.StudentID = studentAcademic.StudentID;
                            studentRequired.InsertedId = userId;
                            studentRequired.IsApproved = false;
                            studentRequired.IsMandatory = true;
                            studentRequired.IsReject = false;
                            studentRequired.IsVerified = false;
                            studentRequired.ModifiedId = userId;
                            studentRequired.RelatedID = sid;
                            studentRequired.Status = EntityStatus.ACTIVE;
                            studentRequired.VerifiedAccessID = 0;
                            studentAggregateRepository.CreateStudentRequiredDocument(studentRequired);
                        }
                    }
                    return RedirectToAction(nameof(StudentAcademicDetails), new { id = studentAcademic.StudentID });
                }
                else
                {
                    studentAggregateRepository.UpdateStudentAcademic(studentAcademic);
                    foreach (var item in documentTypesCls)
                    {
                        StudentRequiredDocument employeeRequired = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.DocumentTypeID == item.TypeId && a.DocumentSubTypeID == item.ID && a.StudentID == studentAcademic.StudentID).FirstOrDefault();
                        if (item.IsRequired == true)
                        {
                            if (employeeRequired == null)
                            {
                                employeeRequired = new StudentRequiredDocument();
                                employeeRequired.Active = true;
                                employeeRequired.DocumentSubTypeID = item.ID;
                                employeeRequired.DocumentTypeID = item.TypeId;
                                employeeRequired.StudentID = studentAcademic.StudentID;
                                employeeRequired.InsertedId = userId;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = studentAcademic.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                studentAggregateRepository.CreateStudentRequiredDocument(employeeRequired);
                            }
                            else
                            {
                                employeeRequired.Active = true;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = studentAcademic.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                studentAggregateRepository.UpdateStudentRequiredDocument(employeeRequired);
                            }
                        }
                        else
                        {
                            if (employeeRequired != null)
                            {
                                employeeRequired.Active = false;
                                employeeRequired.IsApproved = false;
                                employeeRequired.IsMandatory = true;
                                employeeRequired.IsReject = false;
                                employeeRequired.IsVerified = false;
                                employeeRequired.ModifiedId = userId;
                                employeeRequired.RelatedID = studentAcademic.ID;
                                employeeRequired.Status = EntityStatus.ACTIVE;
                                employeeRequired.VerifiedAccessID = 0;
                                studentAggregateRepository.UpdateStudentRequiredDocument(employeeRequired);
                            }

                        }

                    }
                    return RedirectToAction(nameof(StudentAcademicDetails), new { id = studentAcademic.StudentID });
                }



            }
            return View(studentAcademic);
        }
        //   [ActionName("DeleteEmployeeExperience")]
        public async Task<IActionResult> StudentAcademicDeleteConfirmed(long? id, long sid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student Document", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var studentdet = studentAggregateRepository.GetStudentAcademicById(id.Value);
            var documentTypeId = documentTypeRepository.GetDocumentTypeByName("EDUCATIONAL").ID;

            var documents = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.DocumentTypeID == documentTypeId && a.StudentID == studentdet.StudentID && a.RelatedID == studentdet.ID).ToList();
            foreach (var a in documents)
            {
                studentAggregateRepository.DeleteStudentRequiredDocument(a.ID);
            }

            var employeeExperience = studentAggregateRepository.DeleteStudentAcademic(id.Value);





            return RedirectToAction(nameof(StudentAcademicDetails), new { id = sid });
        }
        #endregion
        #region-------------------- Student Emergency Contact--------------
        public IActionResult StudentEmergencyContactDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.StudentId = id;
            var res = studentMethod.GetStudentEmergencyContacts(id);
            ViewBag.isnew = res.Count() > 0 ? false : true;
            return View(res);
        }
        public IActionResult CreateOrEditStudentEmergencyContact(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var res = studentMethod.CreateorEditStudentEmergencyContacts(id, eid);

                ViewBag.status = "Update";
                return View(res);
            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                var res = studentMethod.CreateorEditStudentEmergencyContacts(id, eid);
                return View(res);
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentEmergencyContact(StudentEmergencyContact ob)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                var res = studentMethod.SaveoreditStudentEmergencyContact(ob, userId);

                return Json(1);
            }
            return Json(2);
        }
        [ActionName("DeleteStudentEmergencyContact")]
        public async Task<IActionResult> DeleteStudentEmergencyContact(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var student = studentMethod.RemoveStudentEmergencyContact(id.Value, userId);
            return RedirectToAction(nameof(StudentEmergencyContactDetails), new { id = eid });
        }
        #endregion
        #region-------------------- Student Transportation--------------
        public IActionResult StudentTransportationDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.StudentId = id;
            ViewBag.countries = countryRepository.GetAllCountries().Where(a => a.Active == true).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
            var res = studentMethod.GetStudentTransportation(id);
            ViewBag.isnew = res != null ? false : true;
            return View(res);
        }
        public JsonResult GetLocations(long cityid)
        {
            var res = studentMethod.vehicleLocationRepository.ListAllAsync().Result.Where(a => a.Active == true && a.CityID == cityid).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
            return Json(res);
        }
        public JsonResult GetDeparture(long locationid)
        {
            var res = studentMethod.departureTimeRepository.ListAllAsync().Result.Where(a => a.Active == true && a.LocationID == locationid).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
            return Json(res);
        }
        public IActionResult CreateOrEditStudentTransportation(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            ViewBag.countries = countryRepository.GetAllCountries().Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var res = studentMethod.CreateorEditStudentTransportation(id, eid);

                ViewBag.status = "Update";
                return View(res);
            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                var res = studentMethod.CreateorEditStudentTransportation(id, eid);
                return View(res);
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentTransportation(studenttransportcls ob)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                var res = studentMethod.SaveoreditTransportation(ob, userId);

                return RedirectToAction(nameof(StudentTransportationDetails), new { id = ob.StudentID });
            }
            return View(ob);
        }
        [ActionName("DeleteStudentTransportation")]
        public async Task<IActionResult> DeleteStudentTransportation(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var student = studentMethod.RemoveTransportation(id.Value, userId);
            return RedirectToAction(nameof(StudentTransportationDetails), new { id = eid });
        }
        #endregion

        #region-------------------- Student Current Academic Details--------------
        public IActionResult StudentAcademicStandardDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.StudentId = id;
            var res = sqlQuery.Get_View_Academic_Student().Where(a => a.StudentId == id).ToList();

            return View(res);
        }
        public IActionResult CreateOrEditStudentAcademicStandard(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = " " } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true), "ID", "DisplayName"));
            ViewData["BoardName"] = new[] { new SelectListItem { Text = "Select Board", Value = " " } }.Concat(new SelectList(boardRepository.GetAllBoard(), "ID", "Name"));

            if (id != null)
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                var res = studentMethod.CreateorEditstudentacademicstandard(id, eid);
                ViewBag.status = "Update";
                return View(res);
            }
            else
            {
                if (commonMethods.checkaccessavailable("Student", accessId, "Create", "Student", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                ViewBag.status = "Create";
                var res = studentMethod.CreateorEditstudentacademicstandard(id, eid);
                return View(res);
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateStudentAcademicStandard(studentacademicsstandarddetcls ob)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                var res = studentMethod.SaveOrEditStudentAcademicStandard(ob, userId).Result;
                var category = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStandardID == ob.AcademicStandardId && a.AcademicStandardWingID == ob.AcademicSubjectWingId && a.AcademicSubjectGroupID == ob.AcademicSubjectGroupId).Select(a => a.ID).ToList();
                var categorydet = academicSubjectOptionalDetailsRepository.ListAllAsync().Result.Where(a => a.Active == true && category.Contains(a.AcademicSubjectOptionalID.Value)).Select(a => a.SubjectOptionalID).ToList();
                if (categorydet.Count > 0)
                {
                    return Json(new { id = res, studentid = ob.StudentId, sts = 1 });
                }
                return Json(new { id = ob.StudentId, sts = 2 });
            }
            return Json(new { sts = 3 });
        }
        public async Task<JsonResult> GetNucleus(long id)
        {
            var IsRequiredNucleus = studentMethod.academicStandardSettingsRepository.ListAllAsync().Result == null ? false : studentMethod.academicStandardSettingsRepository.ListAllAsync().Result.Where(a => a.AcademicStandardId == id && a.Active == true).FirstOrDefault().Nucleus;
            return Json(IsRequiredNucleus == false ? 0 : 1);

        }
        [ActionName("DeleteStudentAcademicStandard")]
        public async Task<IActionResult> DeleteStudentAcademicStandard(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var student = studentMethod.RemoveStudentAcademicStandard(id.Value, userId);
            return RedirectToAction(nameof(StudentAcademicStandardDetails), new { id = eid });
        }

        public IActionResult AddSubjectOption(long id, long studentid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.StudentId = id;
            var academic = academicStudentRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            var category = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStandardID == academic.AcademicStandardId && a.AcademicStandardWingID == academic.AcademicStandardWingId && a.AcademicSubjectGroupID == academic.AcademicSubjectCombinationId).ToList();
            var categorydet = academicSubjectOptionalDetailsRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var subjectoptions = studentMethod.subjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var subjects = studentMethod.Getsubjects();
            var SubjectOptionalCategorys = sqlQuery.Get_All_Subject_Optional_Categories().ToList();
            var academicsubjectoptions = studentMethod.academicStudentSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            academicsubjectoptioncls ob = new academicsubjectoptioncls();
            ob.AcademicStudentId = id;
            ob.StudentId = studentid;
            ob.optioncategorycls = (from a in category
                                    join c in SubjectOptionalCategorys on a.SubjectOptionalCategoryID equals c.SubjectOptionalCategoryID
                                    select new optioncategorycls
                                    {
                                        ID = a.ID,
                                        Name = c.SubjectOptionalTypeName,
                                        optionid = academicsubjectoptions.Where(m => m.AcademicStudentID == id && m.AcademicSubjectOptionalID == a.ID && a.Active == true).Select(m => m.AcademicSubjectOptionalDetailID).FirstOrDefault(),
                                        options = (from k in categorydet
                                                   join l in subjectoptions on k.SubjectOptionalID.Value equals l.ID
                                                   join s in subjects on l.SubjectID equals s.ID
                                                   where k.AcademicSubjectOptionalID == a.ID
                                                   select new optionscls
                                                   {
                                                       ID = k.ID,
                                                       Name = s.Name
                                                   }).ToList()
                                    }).ToList();
            return View(ob);
        }
        [HttpPost]
        public async Task<IActionResult> SaveStudentOptionalDetails(academicsubjectoptioncls ob)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {
                if (ob.optioncategorycls.Count > 0)
                {
                    foreach (var item in ob.optioncategorycls)
                    {
                        AcademicStudentSubjectOptional sm = studentMethod.academicStudentSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStudentID == ob.AcademicStudentId && a.AcademicSubjectOptionalID == item.ID).FirstOrDefault();
                        if (sm == null)
                        {
                            sm = new AcademicStudentSubjectOptional();
                            sm.AcademicStudentID = ob.AcademicStudentId;
                            sm.AcademicSubjectOptionalDetailID = item.optionid;
                            sm.AcademicSubjectOptionalID = item.ID;
                            sm.Active = true;
                            sm.InsertedDate = DateTime.Now;
                            sm.InsertedId = userId;
                            sm.IsAvailable = true;
                            sm.ModifiedDate = DateTime.Now;
                            sm.ModifiedId = userId;
                            sm.Status = "ACTIVE";
                            await studentMethod.academicStudentSubjectOptionalRepository.AddAsync(sm);
                        }
                        else
                        {
                            sm.AcademicSubjectOptionalDetailID = item.optionid;
                            sm.AcademicSubjectOptionalID = item.ID;
                            sm.Active = true;
                            sm.IsAvailable = true;
                            sm.ModifiedDate = DateTime.Now;
                            sm.ModifiedId = userId;
                            sm.Status = "ACTIVE";
                            await studentMethod.academicStudentSubjectOptionalRepository.UpdateAsync(sm);
                        }
                    }
                    return RedirectToAction(nameof(AddSubjectOption), new { id = ob.AcademicStudentId, studentid = ob.StudentId });
                }
            }
            return View(ob);
        }
        #endregion
        #region------------------------ Student Document Details-------------------------------------
        public IActionResult StudentDocumentDetails(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (id == null)
            {
                id = userId;
            }
            if (commonMethods.checkaccessavailable("Student Document", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.StudentId = id;
            if (commonMethods.checkaccessavailable("Student Document", accessId, "Verify Document", "Student", roleId) == true)
            {
                ViewBag.verifiedaccess = true;
            }
            else
            {
                ViewBag.verifiedaccess = false;
            }
            if (commonMethods.checkaccessavailable("Student Document", accessId, "Approve Document", "Student", roleId) == true)
            {
                ViewBag.approveaccess = true;
            }
            else
            {
                ViewBag.approveaccess = false;
            }
            if (commonMethods.checkaccessavailable("Student Document", accessId, "Reject Document", "Student", roleId) == true)
            {
                ViewBag.rejectaccess = true;
            }
            else
            {
                ViewBag.rejectaccess = false;
            }
            var StudentRequiredDocumentList = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.StudentID == id).ToList();
            var documentTypeList = documentTypeRepository.GetAllDocumentTypes();
            var documentSubTypeList = documentSubTypeRepository.GetAllDocumentSubTypes();
            var documentsList = studentAggregateRepository.GetAllStudentDocumentAttachment();

            var res = (from a in StudentRequiredDocumentList
                       join b in documentTypeList on a.DocumentTypeID equals b.ID
                       join c in documentSubTypeList on a.DocumentSubTypeID equals c.ID
                       select new StudentDocumentModelClass
                       {
                           ID = a.ID,
                           totaldocument = documentsList.Where(m => m.StudentRequiredDocumentID == a.ID).ToList().Count,

                           DocumentSubTypeID = a.DocumentSubTypeID,
                           DocumentSubTypeName = c.Name,
                           DocumentTypeID = a.DocumentTypeID,
                           DocumentTypeName = b.Name,
                           StudentID = a.StudentID,
                           IsApproved = a.IsApproved,
                           IsMandatory = a.IsMandatory,
                           IsReject = a.IsReject,
                           IsVerified = a.IsVerified,
                           RelatedID = a.RelatedID,
                           VerifiedAccessID = a.VerifiedAccessID,
                           VerifiedDate = a.VerifiedDate
                       }).ToList();

            return View(res);
        }

        public IActionResult CreateOrEditStudentDocumentDetails(long id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student Document", accessId, "Upload", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var doc = studentAggregateRepository.GetStudentRequiredDocumentById(id);
            StudentDocumentUploadCls documentUploadCls = new StudentDocumentUploadCls();
            documentUploadCls.attachments = studentAggregateRepository.GetAllStudentDocumentAttachment().Where(a => a.StudentRequiredDocumentID == id).ToList();
            documentUploadCls.isapproved = doc.IsApproved;
            documentUploadCls.isverified = doc.IsVerified;
            documentUploadCls.ID = id;
            documentUploadCls.StudentID = doc.StudentID;

            return View(documentUploadCls);
        }
        public IActionResult RemoveAttachments(long id, long did)
        {
            studentAggregateRepository.DeleteStudentDocumentAttachment(id);
            return RedirectToAction(nameof(CreateOrEditStudentDocumentDetails), new { id = did });
        }
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentDocument(long ID, IEnumerable<IFormFile> files)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (files != null)
            {
                if (files.Count() > 0)
                {
                    foreach (IFormFile file in files)
                    {
                        StudentDocumentAttachment employeeDocument = new StudentDocumentAttachment();
                        employeeDocument.Active = true;
                        employeeDocument.StudentRequiredDocumentID = ID;
                        employeeDocument.InsertedId = userId;
                        employeeDocument.ModifiedId = userId;
                        employeeDocument.Status = "Upload";
                        FileInfo fi = new FileInfo(file.FileName);
                        var newFilename = ID + "_" + String.Format("{0:d}",
                                          (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                        var webPath = hostingEnvironment.WebRootPath;
                        string path = Path.Combine("", webPath + @"\ODMImages\StudentDocument\" + newFilename);
                        var pathToSave = newFilename;
                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }
                        employeeDocument.DocumentPath = pathToSave;
                        studentAggregateRepository.CreateStudentDocumentAttachment(employeeDocument);
                    }
                }
            }

            return RedirectToAction(nameof(CreateOrEditStudentDocumentDetails), new { id = ID });
        }
        public IActionResult verifyDocument(long id, bool verify)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            StudentRequiredDocument ob = studentAggregateRepository.GetStudentRequiredDocumentById(id);
            if (verify == true)
            {
                ob.IsVerified = true;
                ob.VerifiedDate = DateTime.Now;
                ob.VerifiedAccessID = userId;
                ob.ModifiedId = userId;
            }
            else
            {
                ob.IsVerified = false;
                ob.VerifiedDate = null;
                ob.VerifiedAccessID = 0;
                ob.ModifiedId = userId;
            }
            studentAggregateRepository.UpdateStudentRequiredDocument(ob);
            return RedirectToAction(nameof(StudentDocumentDetails), new { id = ob.StudentID });
        }
        public IActionResult approveDocument(long id, bool verify)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            StudentRequiredDocument ob = studentAggregateRepository.GetStudentRequiredDocumentById(id);
            if (verify == true)
            {
                ob.IsApproved = true;
                ob.ModifiedId = userId;
            }
            else
            {
                ob.IsApproved = false;
                ob.ModifiedId = userId;
            }
            studentAggregateRepository.UpdateStudentRequiredDocument(ob);
            return RedirectToAction(nameof(StudentDocumentDetails), new { id = ob.StudentID });
        }
        public IActionResult rejectDocument(long id, bool verify)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            StudentRequiredDocument ob = studentAggregateRepository.GetStudentRequiredDocumentById(id);
            if (verify == true)
            {
                ob.IsReject = true;
                ob.IsVerified = false;
                ob.VerifiedDate = null;
                ob.VerifiedAccessID = 0;
                ob.IsApproved = false;
                ob.ModifiedId = userId;
            }
            else
            {
                ob.IsReject = false;
                ob.ModifiedId = userId;
            }
            studentAggregateRepository.UpdateStudentRequiredDocument(ob);
            return RedirectToAction(nameof(StudentDocumentDetails), new { id = ob.StudentID });
        }
        #endregion

        #region-------------------- Academic Student--------------
        public IActionResult AcademicStudentDetails(long? AcademicSessionId, long? OraganisationAcademicId, long? BoardId, long? AcademicStandardId, long? AcademicSectionId, long? AcademicSubjectWingId, long? AcademicSubjectGroupId)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = "0" } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true), "ID", "DisplayName"));
            ViewData["BoardName"] = new[] { new SelectListItem { Text = "Select Board", Value = "0" } }.Concat(new SelectList(boardRepository.GetAllBoard(), "ID", "Name"));

            var res = sqlQuery.Get_View_Academic_Student().ToList();

            if (AcademicSessionId != null && AcademicSessionId != 0)
            {
                res = res.Where(a => a.AcademicSessionId == AcademicSessionId).ToList();
            }
            if (OraganisationAcademicId != null && OraganisationAcademicId != 0)
            {
                res = res.Where(a => a.OraganisationAcademicId == OraganisationAcademicId).ToList();
            }
            if (BoardId != null && BoardId != 0)
            {
                res = res.Where(a => a.BoardId == BoardId).ToList();
            }
            if (AcademicStandardId != null && AcademicStandardId != 0)
            {
                res = res.Where(a => a.AcademicStandardId == AcademicStandardId).ToList();
            }
            if (AcademicSectionId != null && AcademicSectionId != 0)
            {
                res = res.Where(a => a.AcademicSectionId == AcademicSectionId).ToList();
            }
            if (AcademicSubjectWingId != null && AcademicSubjectWingId != 0)
            {
                res = res.Where(a => a.AcademicStandardWingId == AcademicSubjectWingId).ToList();
            }
            if (AcademicSubjectGroupId != null && AcademicSubjectGroupId != 0)
            {
                res = res.Where(a => a.AcademicSubjectCombinationId == AcademicSubjectGroupId).ToList();
            }
            academicstudentmodelcls ob = new academicstudentmodelcls();
            ob.AcademicSectionId = AcademicSectionId;
            ob.AcademicSessionId = AcademicSessionId;
            ob.AcademicStandardId = AcademicStandardId;
            ob.AcademicSubjectGroupId = AcademicSubjectGroupId;
            ob.AcademicSubjectWingId = AcademicSubjectWingId;
            ob.BoardId = BoardId;
            ob.OraganisationAcademicId = OraganisationAcademicId;
            ob.students = res;
            return View(ob);
        }
        public IActionResult CreateOrEditAcademicStudent(long[] id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = " " } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true), "ID", "DisplayName"));
            ViewData["BoardName"] = new[] { new SelectListItem { Text = "Select Board", Value = " " } }.Concat(new SelectList(boardRepository.GetAllBoard(), "ID", "Name"));

            if (id != null)
            {
                //if (commonMethods.checkaccessavailable("Student", accessId, "Edit", "Student", roleId) == false)
                //{
                //    return RedirectToAction("AuthenticationFailed", "Accounts");
                //}
                ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = " " } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true), "ID", "DisplayName"));
                ViewData["BoardName"] = new[] { new SelectListItem { Text = "Select Board", Value = " " } }.Concat(new SelectList(boardRepository.GetAllBoard(), "ID", "Name"));

                var res = sqlQuery.Get_View_Academic_Student().ToList();
                ViewBag.status = "Promote";
                academicstudentmodelcls ob = new academicstudentmodelcls();
                ob.AcademicSectionId = 0;
                ob.AcademicSessionId = 0;
                ob.AcademicStandardId = 0;
                ob.AcademicSubjectGroupId = 0;
                ob.AcademicSubjectWingId = 0;
                ob.BoardId = 0;
                ob.OraganisationAcademicId = 0;
                ob.students = res;
                return View(ob);
            }
            return RedirectToAction(nameof(AcademicStudent));

        }
        [HttpPost]
        public JsonResult SaveOrUpdateAcademicStudent(studentacademicsstandarddetcls ob)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                var res = studentMethod.SaveOrEditStudentAcademicStandard(ob, userId);
                //var category = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStandardID == ob.AcademicStandardId && a.AcademicStandardWingID == ob.AcademicSubjectWingId && a.AcademicSubjectGroupID == ob.AcademicSubjectGroupId).Select(a => a.ID).ToList();
                //var categorydet = academicSubjectOptionalDetailsRepository.ListAllAsync().Result.Where(a => a.Active == true && category.Contains(a.AcademicSubjectOptionalID.Value)).Select(a => a.SubjectOptionalID).ToList();
                //if (categorydet.Count > 0)
                //{
                //    return RedirectToAction(nameof(AddSubjectOption), new { id = res, studentid = ob.StudentId });
                //}
                return Json(1);
            }
            return Json(2);
        }
        [ActionName("DeleteAcademicStudent")]
        public async Task<IActionResult> DeleteAcademicStudent(long? id, long eid)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "Delete", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var student = studentMethod.RemoveStudentAcademicStandard(id.Value, userId);
            return RedirectToAction(nameof(AcademicStudentDetails));
        }

        #endregion

        #region Student Academic Option
        public IActionResult AcademicStudentOptionDetails(long? AcademicSessionId, long? OraganisationAcademicId, long? BoardId, long? AcademicStandardId, long? AcademicSectionId, long? AcademicSubjectWingId, long? AcademicSubjectGroupId)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewData["AcademicSession"] = new[] { new SelectListItem { Text = "Select Session", Value = "0" } }.Concat(new SelectList(academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true), "ID", "DisplayName"));
            ViewData["BoardName"] = new[] { new SelectListItem { Text = "Select Board", Value = "0" } }.Concat(new SelectList(boardRepository.GetAllBoard(), "ID", "Name"));

            var res = sqlQuery.Get_View_Academic_Student().ToList();
            int i = 0;
            if (AcademicSessionId != null && AcademicSessionId != 0)
            {
                res = res.Where(a => a.AcademicSessionId == AcademicSessionId).ToList();
                i = 1;
            }
            if (OraganisationAcademicId != null && OraganisationAcademicId != 0)
            {
                res = res.Where(a => a.OraganisationAcademicId == OraganisationAcademicId).ToList();
                i = 1;
            }
            if (BoardId != null && BoardId != 0)
            {
                res = res.Where(a => a.BoardId == BoardId).ToList();
                i = 1;
            }
            if (AcademicStandardId != null && AcademicStandardId != 0)
            {
                res = res.Where(a => a.AcademicStandardId == AcademicStandardId).ToList();
                i = 1;
            }
            if (AcademicSectionId != null && AcademicSectionId != 0)
            {
                res = res.Where(a => a.AcademicSectionId == AcademicSectionId).ToList();
                i = 1;
            }
            if (AcademicSubjectWingId != null && AcademicSubjectWingId != 0)
            {
                res = res.Where(a => a.AcademicStandardWingId == AcademicSubjectWingId).ToList();
                i = 1;
            }
            if (AcademicSubjectGroupId != 0 && AcademicSubjectGroupId != 0)
            {
                res = res.Where(a => a.AcademicSubjectCombinationId == AcademicSubjectGroupId).ToList();
                i = 1;
            }
            academicstudentmodelcls ob = new academicstudentmodelcls();
            ob.AcademicSectionId = AcademicSectionId;
            ob.AcademicSessionId = AcademicSessionId;
            ob.AcademicStandardId = AcademicStandardId;
            ob.AcademicSubjectGroupId = AcademicSubjectGroupId;
            ob.AcademicSubjectWingId = AcademicSubjectWingId;
            ob.BoardId = BoardId;
            ob.OraganisationAcademicId = OraganisationAcademicId;
            ob.students = i == 0 ? null : res;
            return View(ob);
        }

        public IActionResult AddOrEditSubjectOption(long[] id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            if (id != null)
            {
                var academic = sqlQuery.Get_View_Academic_Student().Where(a => id.Contains(a.ID)).ToList();
                var category = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStandardID == academic.FirstOrDefault().AcademicStandardId && a.AcademicStandardWingID == academic.FirstOrDefault().AcademicStandardWingId && a.AcademicSubjectGroupID == academic.FirstOrDefault().AcademicSubjectCombinationId).ToList();
                var categorydet = academicSubjectOptionalDetailsRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var subjectoptions = studentMethod.subjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var subjects = studentMethod.Getsubjects();
                var SubjectOptionalCategorys = sqlQuery.Get_All_Subject_Optional_Categories().ToList();
                var academicsubjectoptions = studentMethod.academicStudentSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                ViewBag.academicstudents = academic;
                academicsubjectoptioncls ob = new academicsubjectoptioncls();

                ob.optioncategorycls = (from a in category
                                        join c in SubjectOptionalCategorys on a.SubjectOptionalCategoryID equals c.SubjectOptionalCategoryID
                                        select new optioncategorycls
                                        {
                                            ID = a.ID,
                                            Name = c.SubjectOptionalTypeName,
                                            optionid = 0,
                                            options = (from k in categorydet
                                                       join l in subjectoptions on k.SubjectOptionalID.Value equals l.ID
                                                       join s in subjects on l.SubjectID equals s.ID
                                                       where k.AcademicSubjectOptionalID == a.ID
                                                       select new optionscls
                                                       {
                                                           ID = k.ID,
                                                           Name = s.Name
                                                       }).ToList()
                                        }).ToList();
                return View(ob);
            }
            else
            {
                return RedirectToAction(nameof(AcademicStudentOptionDetails));
            }
        }
        [HttpPost]
        public async Task<JsonResult> SaveorupdateAcademicStudentOptionalDetails(long? academicstudentid, long? academicoptionalid, long? academicoptionaldetailid)
        {
            CheckLoginStatus();

            AcademicStudentSubjectOptional sm = studentMethod.academicStudentSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStudentID == academicstudentid && a.AcademicSubjectOptionalID == academicoptionalid).FirstOrDefault();
            if (sm == null)
            {
                sm = new AcademicStudentSubjectOptional();
                sm.AcademicStudentID = academicstudentid.Value;
                sm.AcademicSubjectOptionalDetailID = academicoptionaldetailid.Value;
                sm.AcademicSubjectOptionalID = academicoptionalid.Value;
                sm.Active = true;
                sm.InsertedDate = DateTime.Now;
                sm.InsertedId = userId;
                sm.IsAvailable = true;
                sm.ModifiedDate = DateTime.Now;
                sm.ModifiedId = userId;
                sm.Status = "ACTIVE";
                studentMethod.academicStudentSubjectOptionalRepository.AddAsync(sm);
            }
            else
            {
                sm.AcademicSubjectOptionalDetailID = academicoptionaldetailid.Value;
                sm.AcademicSubjectOptionalID = academicoptionalid.Value;
                sm.Active = true;
                sm.IsAvailable = true;
                sm.ModifiedDate = DateTime.Now;
                sm.ModifiedId = userId;
                sm.Status = "ACTIVE";
                studentMethod.academicStudentSubjectOptionalRepository.AddAsync(sm);
            }

            return Json(1);
        }
        #endregion

    }
}
