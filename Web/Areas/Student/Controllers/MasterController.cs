﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Scaffolding;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Web.Areas.Student.Models;
using OdmErp.Web.Models;
using Web.Controllers;

namespace Web.Areas.Student.Controllers
{
    [Area("Student")]
    public class MasterController : AppController
    {
        public ISubjectGroupRepository subjectGroupRepository { get; set; }
        public ISubjectGroupDetailsRepository subjectGroupDetailsRepository { get; set; }
        public ISubjectOptionalTypeRepository subjectOptionalTypeRepository { get; set; }
        public ISubjectOptionalCategoryRepository subjectOptionalCategoryRepository { get; set; }
        public ISubjectOptionalRepository subjectOptionalRepository { get; set; }
        public IVehicleTypeRepository vehicleTypeRepository { get; set; }
        public IVehicleRepository vehicleRepository { get; set; }
        public IVehicleLocationRepository vehicleLocationRepository { get; set; }
        public IDepartureTimeRepository departureTimeRepository { get; set; }
        public StudentMethod studentMethod { get; set; }

        public MasterController(StudentMethod studentMe, ISubjectGroupRepository subjectGroupRepo, ISubjectGroupDetailsRepository subjectGroupDetailsRepo,
            ISubjectOptionalTypeRepository subjectOptionalTypeRepo, ISubjectOptionalCategoryRepository subjectOptionalCategoryRepo, ISubjectOptionalRepository subjectOptionalRepo,
            IVehicleTypeRepository vehicleTypeRepo, IVehicleRepository vehicleRepo, IVehicleLocationRepository vehicleLocationRepo, IDepartureTimeRepository departureTimeRepo)
        {
            studentMethod = studentMe;
            subjectGroupRepository = subjectGroupRepo;
            subjectGroupDetailsRepository = subjectGroupDetailsRepo;
            subjectOptionalTypeRepository = subjectOptionalTypeRepo;
            subjectOptionalCategoryRepository = subjectOptionalCategoryRepo;
            subjectOptionalRepository = subjectOptionalRepo;
            vehicleTypeRepository = vehicleTypeRepo;
            vehicleRepository = vehicleRepo;
            vehicleLocationRepository = vehicleLocationRepo;
            departureTimeRepository = departureTimeRepo;
        }
        #region Subject Group
        public  async Task<IActionResult> SubjectGroupList()
        {
            var res = studentMethod.GetSubjectGroupList();
            return View(res);
        }
        public  async Task<IActionResult> CreateorEditSubjectGroup(long? id)
        {
            var ob = studentMethod.GetSubjectgroupcls(id);
            return View(ob);
        }
        [HttpPost]
        public  async Task<IActionResult> SaveOrEditSubjectGroup(subjectgroupcls ob)
        {
            CheckLoginStatus();
            var res = studentMethod.SaveOrUpdateSubjectGroup(ob, userId).Result;
            return Json(res);
        }
        public JsonResult GetStandards(long id)
        {
            return Json(studentMethod.Getstandards(id));
        }
        public JsonResult GetSubjects(long id)
        {
            return Json(studentMethod.Getsubjects(id));
        }
        public  async Task<IActionResult> DeleteSubjectGroup(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.RemoveSubjectGroup(id, userId);
            return RedirectToAction(nameof(SubjectGroupList)).WithSuccess("Success", "Deleted Successfully");
        }
        public  async Task<IActionResult> ActiveInActiveSubjectGroup(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.ActiveInActiveSubjectGroup(id, userId);
            if (res == 1)
            {
                return RedirectToAction(nameof(SubjectGroupList)).WithSuccess("Success", "In-Active Successfully");
            }
            else
            {
                return RedirectToAction(nameof(SubjectGroupList)).WithSuccess("Success", "Active Successfully");
            }
        }
        #endregion
        #region Subject Group Details
        public  async Task<IActionResult> SubjectGroupDetailsList(long id)
        {
            ViewBag.subjectgroupid = id;
            var res = studentMethod.GetSubjectGroupDetailsList(id);

            return View(res);
        }
        public JsonResult GetSubjectsBySchoolGroup(long id)
        {
            return Json(studentMethod.GetSubectsbyusingSchoolGroup(id));
        }
        [HttpPost]
        public  async Task<IActionResult> SaveOrEditSubjectGroupDetails(long[] subjectids, long schoolgroupid)
        {
            CheckLoginStatus();
            var res = studentMethod.SaveOrUpdateSubjectGroupDetails(subjectids, schoolgroupid, userId);
            return Json(res);
        }
        public  async Task<IActionResult> DeleteSubjectGroupDetails(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.RemoveSubjectGroupDetails(id, userId);
            return RedirectToAction(nameof(SubjectGroupDetailsList), new { id = res }).WithSuccess("Success", "Deleted Successfully");
        }
        public  async Task<IActionResult> ActiveInActiveSubjectGroupDetails(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.ActiveInActiveSubjectGroupDetails(id, userId);

            return RedirectToAction(nameof(SubjectGroupDetailsList), new { id = res }).WithSuccess("Success", "Successfully Updated");

        }

        #endregion
        #region Subject Optional Type
        public  async Task<IActionResult> SubjectOptionalTypeDetailsList()
        {
            var res = studentMethod.GetSubjectOptionalTypeList();
            return View(res);
        }      
        [HttpPost]
        public  async Task<IActionResult> SaveOrEditSubjectOptionalType(long? id,string name)
        {
            CheckLoginStatus();
            var res = studentMethod.SaveOrUpdateSubjectOptionalType(id, name, userId);
            return Json(res);
        }
        public  async Task<IActionResult> DeleteSubjectOptionalType(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.RemoveSubjectOptionalType(id, userId);
            return RedirectToAction(nameof(SubjectOptionalTypeDetailsList)).WithSuccess("Success", "Deleted Successfully");
        }
        public  async Task<IActionResult> ActiveInActiveSubjectOptionalType(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.ActiveInActiveSubjectOptionalType(id, userId);

            return RedirectToAction(nameof(SubjectOptionalTypeDetailsList)).WithSuccess("Success", "Successfully Updated");

        }

        #endregion
        #region Subject Optional Category
        public  async Task<IActionResult> SubjectOptionalCategoryList()
        {
            var res = studentMethod.GetSubjectOptionalCategoryList();
            return View(res);
        }
        public  async Task<IActionResult> CreateorEditSubjectOptionalCategory(long? id)
        {
            var ob = studentMethod.GetSubjectOptionalCategorycls(id);
            return View(ob);
        }
        public JsonResult GetWings(long id)
        {
            return Json(studentMethod.GetWings(id));
        } 
        public JsonResult GetSchoolGroup(long id)
        {
            return Json(studentMethod.GetSchoolGroup(id));
        }
        [HttpPost]
        public async Task<IActionResult> SaveOrEditSubjectOptionalCategory(subjectoptionalcategorycls ob)
        {
            CheckLoginStatus();
            var res = studentMethod.SaveOrUpdateSubjectOptionalCategory(ob, userId);
            return Json(res);
        }     
        public  async Task<IActionResult> DeleteSubjectOptionalCategory(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.RemoveSubjectOptionalCategory(id, userId);
            return RedirectToAction(nameof(SubjectOptionalCategoryList)).WithSuccess("Success", "Deleted Successfully");
        }
        public  async Task<IActionResult> ActiveInActiveSubjectOptionalCategory(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.ActiveInActiveSubjectOptionalCategory(id, userId);
            if (res == 1)
            {
                return RedirectToAction(nameof(SubjectOptionalCategoryList)).WithSuccess("Success", "In-Active Successfully");
            }
            else
            {
                return RedirectToAction(nameof(SubjectOptionalCategoryList)).WithSuccess("Success", "Active Successfully");
            }
        }
        #endregion
        #region Subject Optional
        public  async Task<IActionResult> SubjectOptionalList(long id)
        {
            ViewBag.subjectgroupid = id;
            var res = studentMethod.GetSubjectOptionalList(id);

            return View(res);
        }
        public JsonResult GetSubjectsBySchoolOptionalCategory(long id)
        {
            return Json(studentMethod.GetSubectsbyusingSchoolOptionList(id));
        }

        [HttpPost]
        public  async Task<IActionResult> SaveOrEditSubjectOptional(long[] subjectids, long schoolgroupid)
        {
            CheckLoginStatus();
            var res = studentMethod.SaveOrUpdateSubjectOptionalDetails(subjectids, schoolgroupid, userId);
            return Json(res);
        }
        public  async Task<IActionResult> DeleteSubjectOptional(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.RemoveSubjectOptionalDetails(id, userId);
            return RedirectToAction(nameof(SubjectOptionalList), new { id = res }).WithSuccess("Success", "Deleted Successfully");
        }
        public  async Task<IActionResult> ActiveInActiveSubjectOptional(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.ActiveInActiveSubjectOptionalDetails(id, userId);

            return RedirectToAction(nameof(SubjectOptionalList), new { id = res }).WithSuccess("Success", "Successfully Updated");

        }

        #endregion
        #region Vehicle Type
        public async Task<IActionResult> VehicleTypeList()
        {
            var res = studentMethod.GetVehicleTypeList();
            return View(res);
        }
        [HttpPost]
        public async Task<IActionResult> SaveOrEditVehicleTypeList(long? id, string name)
        {
            CheckLoginStatus();
            var res = studentMethod.SaveOrUpdatevehicleType(id, name, userId);
            return Json(res);
        }
        public async Task<IActionResult> DeleteVehicleTypeList(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.RemovevehicleType(id, userId);
            return RedirectToAction(nameof(VehicleTypeList)).WithSuccess("Success", "Deleted Successfully");
        }
        public async Task<IActionResult> ActiveInActiveVehicleTypeList(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.ActiveInActivevehicleType(id, userId);

            return RedirectToAction(nameof(VehicleTypeList)).WithSuccess("Success", "Successfully Updated");

        }

        #endregion
        #region Vehicle
        public async Task<IActionResult> VehicleList()
        {
            var res = studentMethod.GetVehicleList();
            return View(res);
        }
        public JsonResult GetOrganisationbygroupid(long id)
        {
            var org = studentMethod.GetOrganisationByGroupId(id);
            return Json(org);
        }
        [HttpPost]
        public async Task<IActionResult> SaveOrEditVehicleList(vehicledetcls ob)
        {
            CheckLoginStatus();
            var res = studentMethod.SaveOrUpdatevehicle(ob, userId);
            return Json(res);
        }
        public async Task<IActionResult> DeleteVehicleList(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.Removevehicle(id, userId);
            return RedirectToAction(nameof(VehicleList)).WithSuccess("Success", "Deleted Successfully");
        }
        public async Task<IActionResult> ActiveInActiveVehicleList(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.ActiveInActivevehicle(id, userId);

            return RedirectToAction(nameof(VehicleList)).WithSuccess("Success", "Successfully Updated");

        }

        #endregion
        #region Vehicle Location
        public async Task<IActionResult> VehicleLocationList(long id)
        {
            var res = studentMethod.GetVehicleLocationList(id);
            return View(res);
        }
       
        [HttpPost]
        public async Task<IActionResult> SaveOrEditVehicleLocationList(vehiclelocationcls ob)
        {
            CheckLoginStatus();
            var res = studentMethod.SaveOrUpdatevehicleLocation(ob, userId);
            return Json(res);
        }
        public async Task<IActionResult> DeleteVehicleLocationList(long id,long vehicleid)
        {
            CheckLoginStatus();
            var res = studentMethod.RemovevehicleLocation(id, userId);
            return RedirectToAction(nameof(VehicleLocationList),new { id= vehicleid }).WithSuccess("Success", "Deleted Successfully");
        }
        public async Task<IActionResult> ActiveInActiveVehicleLocationList(long id)
        {
            CheckLoginStatus();
            var res = studentMethod.ActiveInActivevehicleLocation(id, userId);

            return RedirectToAction(nameof(VehicleLocationList)).WithSuccess("Success", "Successfully Updated");

        }

        #endregion
        #region Departure Timing
        public async Task<IActionResult> DepartureTimesList(long id)
        {
            var res = studentMethod.GetDepartureTimesList(id);
            return View(res);
        }

        [HttpPost]
        public async Task<IActionResult> SaveOrEditDepartureTimes(DepartureTime ob)
        {
            CheckLoginStatus();
            var res = studentMethod.SaveOrUpdateDepartureTimes(ob, userId);
            return Json(res);
        }
        public async Task<IActionResult> DeleteDepartureTimes(long id,long location)
        {
            CheckLoginStatus();
            var res = studentMethod.RemoveDepartureTimes(id, userId);
            return RedirectToAction(nameof(DepartureTimesList),new {id=location }).WithSuccess("Success", "Deleted Successfully");
        }
        public async Task<IActionResult> ActiveInActiveDepartureTimes(long id, long location)
        {
            CheckLoginStatus();
            var res = studentMethod.ActiveInActiveDepartureTimes(id, userId);

            return RedirectToAction(nameof(DepartureTimesList), new { id = location }).WithSuccess("Success", "Successfully Updated");

        }

        #endregion
    }
}