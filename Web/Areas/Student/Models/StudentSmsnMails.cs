﻿using Microsoft.AspNetCore.Hosting;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Student.Models
{
    public class StudentSmsnMails
    {
        private smssend smssend { get; set; }
        private readonly IHostingEnvironment hostingEnvironment;
        public StudentSmsnMails(smssend sms, IHostingEnvironment hostingEnv)
        {
            smssend = sms;
            hostingEnvironment = hostingEnv;
        }
        public string Parentcreated(string name, string sender, string code)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "accountcreation.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", name).Replace("{{empcode}}", code);
                smssend.SendHtmlFormattedEmail(sender, "Parent Created", body);
            }

            return body;
        }

    }
}
