﻿using Microsoft.AspNetCore.Http;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Query;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Student.Models
{
    public class StudentModelClass
    {
        public long ID { get; set; }
        [Display(Name = "Student Code")]
        [Required(ErrorMessage = "Please Enter Student Code")]
        public string StudentCode { get; set; }
        public bool IsStudentCode { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        public bool IsFirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        public bool IsMiddleName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please Enter Last Name")]

        public string LastName { get; set; }
        public bool IsLastName { get; set; }
        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Please Enter Gender")]

        public string Gender { get; set; }
        public bool IsGender { get; set; }
        [Display(Name = "Date Of Birth")]
        [Required(ErrorMessage = "Please Enter Date Of Birth")]

        public DateTime DateOfBirth { get; set; }
        public bool IsDateOfBirth { get; set; }
        [Display(Name = "Blood Group")]
        [Required(ErrorMessage = "Please Enter Blood Group")]
        public long BloodGroupID { get; set; }
        public bool IsBloodGroupID { get; set; }
        public string BloodGroupName { get; set; }
        public string MotherTongue { get; set; }
        public IEnumerable<BloodGroup> bloodGroups { get; set; }
        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please Enter Category")]
        public long CategoryID { get; set; }
        public bool IsCategoryID { get; set; }
        public string CategoryName { get; set; }
        [Display(Name = "Nationality")]
        [Required(ErrorMessage = "Please Enter Nationality")]
        public long NatiobalityID { get; set; }
        public bool IsNatiobalityID { get; set; }
        public string NatiobalityName { get; set; }
        public IEnumerable<NationalityType> nationalityTypes { get; set; }
        [Display(Name = "Religion")]
        [Required(ErrorMessage = "Please Enter Religion")]
        public long ReligionID { get; set; }
        public bool IsReligionID { get; set; }
        [Display(Name = "Aadhar Number")]
        [Required(ErrorMessage = "Please Enter Aadhar Number")]
        public string AadharNO { get; set; }
        public bool IsAadharNO { get; set; }
        [Display(Name = "Email ID")]
        [Required(ErrorMessage = "Please Enter Email ID")]
        public string EmailID { get; set; }
        public bool IsEmailID { get; set; }
        [Display(Name = "Passport Number")]
        [Required(ErrorMessage = "Please Enter Passport Number")]
        public string PassportNO { get; set; }
        public bool IsPassportNO { get; set; }
        [Display(Name = "Primary Mobile")]
        [Required(ErrorMessage = "Please Enter Primary Mobile")]
        public string PrimaryMobile { get; set; }
        public bool IsPrimaryMobile { get; set; }
        [Display(Name = "Whatsapp Mobile")]
        [Required(ErrorMessage = "Please Enter Whatsapp Mobile")]
        public string WhatsappMobile { get; set; }
        public bool IsWhatsappMobile { get; set; }
        [Display(Name = "Zoom Mail ID")]
        [Required(ErrorMessage = "Please Enter Zoom Mail ID")]
        public string ZoomMailID { get; set; }
        public bool IsZoomMailID { get; set; }
        [Display(Name = "Mother Tongue ID")]
        [Required(ErrorMessage = "Please Select Mother Tongue ID")]
        public long MotherTongueID { get; set; }
        public bool IsMotherTongueID { get; set; }
        public string ReligionName { get; set; }
        public string ProfileImage { get; set; }
        public bool IsProfileImage { get; set; }
        public string LanguagesKnowns { get; set; }
        public bool IsLanguagesKnowns { get; set; }
        public DateTime ModifiedDate { get; set; }
        public List<Web.Models.documentTypesCls> documentTypesCls { get; set; }
        public IEnumerable<ReligionType> religionTypes { get; set; }
        public List<Language> languages { get; set; }
        public List<AddressType> addressTypes { get; set; }
        public IEnumerable<StudentAdressCls> studentAdresses { get; set; }
        public bool IsstudentAdresses { get; set; }
        public IEnumerable<StudentParentsCls> studentParentsCls { get; set; }
        public bool IsstudentParentsCls { get; set; }
        public View_Academic_Student studentClassDetails { get; set; }
        public bool IsstudentClassDetails { get; set; }
        public IEnumerable<Category> categoryTypes { get; set; }
        public List<ParentRelationshipType> relationshipTypes { get; set; }
        public IEnumerable<Web.Models.EmployeeAccesscls> accesscls { get; set; }
        public List<Web.Models.GivenAccesscls> employeeModuleSubModuleAccess { get; set; }

        public List<StudentDocumentModelClass> StudentDocumentAccess { get; set; }
        public List<StudentDocumentModelClass> StudentPersonalDocumentAccess { get; set; }

        public List<StudentAcademicModel> StudentAcademicAccess { get; set; }
        public bool IsStudentAcademicAccess { get; set; }
        public IEnumerable<StudentModelClass> StudentList { get; set; }
        [Display(Name = "Languages Known")]
        public List<long> languagesknown { get; set; }
        public IFormFile file { get; set; }
        public StudentEmergencyContact emergencyContact { get; set; }
        public bool IsemergencyContact { get; set; }
        public studenttransportcls transport { get; set; }
        public bool Istransport { get; set; }
        public bool firsttime { get; set; }
        //  public List<Web.Models.documentTypesCls> documentTypesCls { get; set; }

    }
    public class StudentAdressCls
    {
        public long StudentAddressId { get; set; }
        public long StudentId { get; set; }
        public long ID { get; set; }
        [Display(Name = "Address Line1")]
        [Required(ErrorMessage = "Please Enter Address Line1")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line2")]
        [Required(ErrorMessage = "Please Enter Address Line2")]
        public string AddressLine2 { get; set; }
        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Please Enter Postal Code")]
        public string PostalCode { get; set; }
        [Display(Name = "City")]
        [Required(ErrorMessage = "Please Select City")]
        public long CityID { get; set; }
        [Display(Name = "State")]
        [Required(ErrorMessage = "Please Select State")]
        public long StateID { get; set; }
        [Display(Name = "Country")]
        [Required(ErrorMessage = "Please Select Country")]
        public long CountryID { get; set; }
        [Display(Name = "Address Type")]
        [Required(ErrorMessage = "Please Select Address Type")]
        public long AddressTypeID { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string AddressTypeName { get; set; }
        public IEnumerable<Country> countries { get; set; }
        public IEnumerable<State> states { get; set; }
        public IEnumerable<City> cities { get; set; }
        public IEnumerable<AddressType> addressTypes { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime InsertedDate { get; set; }
    }
    public class StudentParentsCls
    {
        public long ID { get; set; }
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please Enter Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Primary Mobile")]
        public string PrimaryMobile { get; set; }
        [Display(Name = "Alternative Mobile")]
        public string AlternativeMobile { get; set; }
        [Display(Name = "Landline Number")]
        public string LandlineNumber { get; set; }
        [Display(Name = "Email Id")]
        public string EmailId { get; set; }
        //[Display(Name = "Organization")]
        //public long OrganizationID { get; set; }
        public string ParentOrganization { get; set; }
        [Display(Name = "Profession Type")]
        public long ProfessionTypeID { get; set; }
        public string ProfessionTypeName { get; set; }
        public List<Web.Models.CommonMasterModel> ProfessionTypes { get; set; }
        public long StudentID { get; set; }
        [Display(Name = "Parent Relationship")]
        public long ParentRelationshipID { get; set; }
        public string ParentRelationshipName { get; set; }
        public string Imagepath { get; set; }
        public List<Web.Models.CommonMasterModel> ParentRelationships { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
    public class StudentClassDetails
    {
        public long ID { get; set; }
        public long StudentID { get; set; }
        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Standard")]
        public long StandardID { get; set; }
        public string StandardName { get; set; }
        public List<Standard> standards { get; set; }
        [Display(Name = "Section")]
        [Required(ErrorMessage = "Please Select Section")]
        public long SectionID { get; set; }
        public string SectionName { get; set; }
        public List<Section> sections { get; set; }
        [Display(Name = "Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long WingID { get; set; }
        public string WingName { get; set; }
        public List<Schoolwingcls> wings { get; set; }
        [Display(Name = "Organisation")]
        [Required(ErrorMessage = "Please Select Organisation")]
        public long OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public List<Organization> organizations { get; set; }
        [Display(Name = "Current Standard")]
        public bool IsCurrent { get; set; }
        [Display(Name = "Board")]
        [Required(ErrorMessage = "Please Select Board")]
        public long BoardID { get; set; }
        public string BoardName { get; set; }
        public List<Board> boards { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long SessionID { get; set; }
        public string SessionName { get; set; }
    }
    public class Schoolwingcls : Web.Models.BaseModel
    {
        public long ID { get; set; }
        [Display(Name = "Organisation")]
        [Required(ErrorMessage = "Please Select Organisation")]
        public long OrganizationAcademicID { get; set; }
        [Display(Name = "Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long WingID { get; set; }
        public long[] MultiWingID { get; set; }
        public string OrganisationName { get; set; }
        public string wingName { get; set; }
        public List<Organization> organizations { get; set; }
        public List<Wing> wings { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long AcademicSessionId { get; set; }
        public long OrganisationID { get; set; }
        public long OrganisationIDByAcademic { get; set; }
        public string OrganisationNameByAcademic { get; set; }



    }

    public class StudentTypewithAddresscls
    {
        public long typeId { get; set; }
        public string typeName { get; set; }
        public StudentAdressCls studentAdress { get; set; }

    }
    public class AcademicStudentcls : Web.Models.BaseModel
    {
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long AcademicStandardId { get; set; }
        public bool StandardStreamType { get; set; }
        public string StandardName { get; set; }

        [Display(Name = "Oraganisation Academic")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }

        [Display(Name = "Teacher Employee")]
        [Required(ErrorMessage = "Please Select Teacher")]
        public long ddlteacher { get; set; }
        [Display(Name = "Class Timing")]
        [Required(ErrorMessage = "Please Select Class Timing")]
        public long ddlclasstiming { get; set; }
        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Please Select Subject")]
        public long ddlsubjects { get; set; }

        public DateTime date { get; set; }

        public string OraganisationAcademicName { get; set; }

        public string OraganisationName { get; set; }

        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        [Display(Name = "School Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long AcademicSubjectWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }
        public long CountAcademicSection { get; set; }
        public long CountAcademicStandardStream { get; set; }
        public IEnumerable<AcademicSession> academicSessions { get; set; }
        public IEnumerable<Board> boards { get; set; }
        public IEnumerable<ClassTiming> ClassTiming { get; set; }
        public long AcademicStandardStreamId { get; set; }
        public long[] StudentId { get; set; }
        public long AcademicSectionId { get; set; }
        public string[] RollNo { get; set; }
        public long[] AcademicStudentId { get; set; }
    }
    public class AcademicStudentViewModel : Web.Models.BaseModel
    {
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        [Display(Name = "BoardStandardId")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long StandardId { get; set; }
        public bool StandardStreamType { get; set; }

        public string StandardName { get; set; }

        [Display(Name = "OraganisationAcademicId")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }


        public string OraganisationAcademicName { get; set; }

        public string OraganisationName { get; set; }

        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }

        [Display(Name = "MultiBoardStandardId")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long[] MultiBoardStandardId { get; set; }

        [Display(Name = "SchoolWingId")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long SchoolWingId { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }
        public long CountAcademicSection { get; set; }
        public long CountAcademicStandardStream { get; set; }
        public string studentname { get; set; }
        public string studentcode { get; set; }
        public long StudentId { get; set; }
        public string streamName { get; set; }
    }
    public class StudentDocumentModelClass : Web.Models.BaseModel
    {
        public bool approveaccess { get; set; }
        public string OrganizationName { get; set; }
        public string BoardName { get; set; }
        public string ClassName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public long StudentID { get; set; }
        public long DocumentSubTypeID { get; set; }
        public string DocumentSubTypeName { get; set; }
        public bool IsMandatory { get; set; }
        public long DocumentTypeID { get; set; }
        public string DocumentTypeName { get; set; }
        public long RelatedID { get; set; }
        public string relatedDetails { get; set; }
        public long EmployeeID { get; set; }
        public bool IsVerified { get; set; }
        public long VerifiedAccessID { get; set; }
        public DateTime? VerifiedDate { get; set; }
        public string DocumentPath { get; set; }
        public bool IsApproved { get; set; }
        public bool IsReject { get; set; }
        public int totaldocument { get; set; }
        public bool verifyaccess { get; set; }
        public IFormFile file { get; set; }
    }
    public class StudentDocumentUploadCls
    {
        public IEnumerable<IFormFile> files { get; set; }
        public List<StudentDocumentAttachment> attachments { get; set; }
        public bool isverified { get; set; }
        public bool isapproved { get; set; }
        public bool verifiedaccess { get; set; }
        public bool approveaccess { get; set; }
        public long ID { get; set; }
        public long StudentID { get; set; }
    }
    public class StudentAcademicModelClass
    {
        public long StudentID { get; set; }
        public long ID { get; set; }
        [Display(Name = "Organization Name")]
        [Required(ErrorMessage = "Please Enter Organization Name")]
        public string OrganizationName { get; set; }

        [Display(Name = "Board")]
        [Required(ErrorMessage = "Please Enter Board")]
        public int BoardID { get; set; }


        [Display(Name = "Class")]
        [Required(ErrorMessage = "Please Enter Salary Per Anum")]
        public int ClassID { get; set; }


        [Display(Name = "From Date")]
        [Required(ErrorMessage = "Please Enter From Date")]
        public DateTime FromDate { get; set; }
        [Display(Name = "To Date")]
        [Required(ErrorMessage = "Please Enter To Date")]
        public DateTime ToDate { get; set; }
        public List<Web.Models.documentTypesCls> documentTypesCls { get; set; }
    }

    public class StudentAcademicModel : Web.Models.BaseModel
    {

        public string OrganizationName { get; set; }

        public int BoardID { get; set; }
        public string BoardName { get; set; }

        public int ClassID { get; set; }
        public string ClassName { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public long StudentID { get; set; }

    }

    public class SessionTimeTableModel
    {
        public long BoardId { get; set; }

        [Display(Name = "Standard")]
        [Required(ErrorMessage = "Please Select Class ")]
        public long BoardStandardId { get; set; }
        public long AcademicStandardId { get; set; }
        public bool StandardStreamType { get; set; }


        [Display(Name = "Oraganisation Academic")]
        [Required(ErrorMessage = "Please Select Oraganisation")]
        public long OraganisationAcademicId { get; set; }

        [Display(Name = "Section")]
        [Required(ErrorMessage = "Please Select Section")]
        public long AcademicSectionId { get; set; }

        [Display(Name = "Section")]
        [Required(ErrorMessage = "Please Select Session")]
        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        [Display(Name = "School Wing")]
        [Required(ErrorMessage = "Please Select Wing")]
        public long SchoolWingId { get; set; }
        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Please Select Subject")]
        public long AcademicSubjectId { get; set; }

        public IEnumerable<AcademicSession> academicSessions { get; set; }
        public IEnumerable<Board> boards { get; set; }
        public long AcademicStandardStreamId { get; set; }


        public string TimetableCode { get; set; }


        public int SessionTimetableId { get; set; }
        [Display(Name = "StartDate")]
        [Required(ErrorMessage = "Please Enter StartDate")]
        public DateTime StartDate { get; set; }

        [Display(Name = "EndDate")]
        [Required(ErrorMessage = "Please Enter EndDate")]
        public DateTime EndDate { get; set; }
        [Display(Name = "PeriodType")]
        [Required(ErrorMessage = "Please Enter PeriodType")]
        public long PeriodType { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name { get; set; }
        [Display(Name = "Start Time")]
        [Required(ErrorMessage = "Please Select Start Time")]
        public TimeSpan StartTime { get; set; }
        [Display(Name = "End Time")]
        [Required(ErrorMessage = "Please Select End Time")]
        public TimeSpan EndTime { get; set; }
        public string PeriodTypeName { get; set; }
        public bool isremove { get; set; }
        public long id { get; set; }

    }

    public class SessionTimetablePeriodSubjectModel
    {
        public long id { get; set; }
        public long SessionTimetablePeriodId { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public string StartTimeName { get; set; }
        public string EndTimeName { get; set; }
        public long AcademicSubjectId { get; set; }
        public string AcademicSubjectName { get; set; }

        public string Name { get; set; }
        public string PeriodTypeName { get; set; }
        public bool isremove { get; set; }

    }
    public class commonclass
    {
        public long ID { get; set; }
        public string Name { get; set; }

    }
    public class subjectgroupcls
    {
        public long SubjectGroupID { get; set; }
        public string SubjectGroupName { get; set; }
        public long BoardStandardId { get; set; }
        public long BoardStandardStreamId { get; set; }
        public List<commonclass> boards { get; set; }
        public List<commonclass> standards { get; set; }
        public List<commonclass> streams { get; set; }
        public long BoardID { get; set; }
        public string BoardName { get; set; }
        public string StandardName { get; set; }
        public long[] subjectids { get; set; }
        public List<commonclass> subjects { get; set; }
    }
    public class subjectclass
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public long BoardStandardID { get; set; }
        public long BoardStandardStreamId { get; set; }
    }
    public class subjectgroupdetailsclass
    {
        public long ID { get; set; }
        public long SubjectGroupID { get; set; }
        public long SubjectID { get; set; }
        public long BoardStandardId { get; set; }
        public long BoardId { get; set; }
        public long StandardId { get; set; }
        public string BoardName { get; set; }
        public string StandardName { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
        public bool IsAvailable { get; set; }
        public long[] subjectids { get; set; }
        public long? SubjectOptionalCategoryID { get; set; }
        public DateTime inserteddate { get; set; }
    }
    public class academicsubjectgroupclass
    {
        public long ID { get; set; }
        public string session { get; set; }
        public long sessionID { get; set; }
        public long subjectoptionalID { get; set; }
        public bool IsAvailable { get; set; }
        public int NoOfSubjects { get; set; }
        public DateTime insertedDate { get; set; }
        public View_All_Subject_Group subjectGroup { get; set; }
        public View_All_Subject_Optional_Category category { get; set; }
        public View_All_Academic_Standard standards { get; set; }
    }
    public class subjectoptionalcategorycls
    {
        public long SubjectOptionalCategoryID { get; set; }
        public long SubjectOptionalTypeID { get; set; }
        public string SubjectOptionalTypeName { get; set; }
        public long BoardStandardId { get; set; }
        public long BoardStandardStreamId { get; set; }
        public List<commonclass> types { get; set; }
        public List<commonclass> boards { get; set; }
        public List<commonclass> standards { get; set; }
        public List<commonclass> streams { get; set; }
        public List<commonclass> wings { get; set; }
        public List<commonclass> subjectgroups { get; set; }
        public long BoardID { get; set; }
        public long? StreamID { get; set; }
        public string StreamName { get; set; }
        public long? WingID { get; set; }
        public long? SubjectGroupID { get; set; }
        public string WingName { get; set; }
        public string BoardName { get; set; }
        public string StandardName { get; set; }
        public long[] subjectids { get; set; }
        public List<commonclass> subjects { get; set; }
    }
    public class studentacademicsstandarddetcls
    {
        public long ID { get; set; }
        public long AcademicSessionId { get; set; }
        public long OraganisationAcademicId { get; set; }
        public long BoardId { get; set; }
        public long AcademicStandardId { get; set; }
        public long AcademicSectionId { get; set; }
        public long AcademicSubjectGroupId { get; set; }
        public long AcademicSubjectWingId { get; set; }
        public long StudentId { get; set; }
        public string Studentcode { get; set; }
        public bool Nucleus { get; set; }
        public bool? IsRequiredNucleus { get; set; }
    }
    public class academicsubjectoptioncls
    {
        public long AcademicStudentId { get; set; }
        public long StudentId { get; set; }
        public List<optioncategorycls> optioncategorycls { get; set; }
    }
    public class optioncategorycls
    {
        public long ID { get; set; }
        public string Name { get; set; }
        [Required(ErrorMessage = "Select Option")]
        public long optionid { get; set; }
        public List<optionscls> options { get; set; }
    }
    public class optionscls
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
    public class vehicledetcls
    {
        public long ID { get; set; }
        public string Number { get; set; }
        public long? VehicleTypeID { get; set; }
        public string VehicleTypeName { get; set; }
        public long? OrganisationID { get; set; }
        public string OrganisationName { get; set; }
        public long? GroupID { get; set; }
        public long Countlocation { get; set; }
        public string GroupName { get; set; }
        public DateTime insertedDate { get; set; }
        public List<commonclass> groups { get; set; }
        public List<commonclass> organisations { get; set; }
        public List<commonclass> vehicletypes { get; set; }

    }
    public class vehiclelistdetcls
    {
        public List<vehicledetcls> vehicledetcls { get; set; }
        public vehicledetcls GetVehicledetcls { get; set; }
    }
    public class vehiclelocationcls
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public vehicledetcls vehicle { get; set; }
        public long CityID { get; set; }
        public long VehicleID { get; set; }
        public long StateID { get; set; }
        public long CountryID { get; set; }
        public long Counttimings { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
    public class vehiclelocationdetcls
    {
        public long VehicleID { get; set; }
        public List<commonclass> countries { get; set; }
        public List<vehiclelocationcls> vehiclelocation { get; set; }
    }

    public class DepartureTimeDetcls
    {
        public long VehicleLocationID { get; set; }
        public List<DepartureTime> timings { get; set; }
    }
    public class studenttransportcls
    {
        public long DepartureID { get; set; }
        public long LocationID { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string departureName { get; set; }
        public long StudentID { get; set; }
        public long ID { get; set; }
        public bool IsTransport { get; set; }
        public decimal Distance { get; set; }
        public string LocationName { get; set; }
        public string VehicleNumber { get; set; }
        public string VehicleType { get; set; }

        public long CityID { get; set; }
        public long StateID { get; set; }
        public long CountryID { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
    }
    public class academicstudentmodelcls
    {
        public List<View_Academic_Student> students { get; set; }
        public long? AcademicSessionId { get; set; }
        public long? OraganisationAcademicId { get; set; }
        public long? BoardId { get; set; }
        public long? AcademicStandardId { get; set; }
        public long? AcademicSectionId { get; set; }
        public long? AcademicSubjectWingId { get; set; }
        public long? AcademicSubjectGroupId { get; set; }

    }

}
