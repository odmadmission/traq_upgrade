﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Query;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Student.Models
{
    public class StudentSqlQuery
    {
        public ApplicationDbContext context;
        public StudentSqlQuery(ApplicationDbContext cont)
        {
            context = cont;
        }
        #region Academic Student
        public List<View_All_Academic_Student> Get_view_All_Academic_Students()
        {
            var res = context.view_All_Academic_Students.FromSql(@" select a.ID as ID,c.OrganizationAcademicId as OraganisationAcademicId,
a.AcademicStandardId,a.AcademicStandardStreamId,
                        c.BoardStandardId as BoardStandardId,
                        f.Name as OraganisationName,
                        d.AcademicSessionId as AcademicSessionId,
                        e.DisplayName as SessionName,
                        g.StandardId as StandardId,
                        h.Name as StandardName,
                        h.StreamType as StandardStreamType,
                        c.SchoolWingId as SchoolWingId,
                        a.ModifiedDate as ModifiedDate,
                        j.Name as WingName,
                        a.StudentId as StudentId,
                        (b.FirstName+' '+b.LastName) as studentname,
                        b.StudentCode as studentcode,
                        k.Name as BoardName,
                        (case when (a.AcademicStandardStreamId is null or a.AcademicStandardStreamId=0) then '' else 
                        (select top(1) Name from Streams 
                        where Active=1 and ID=(select top(1) StreamId from BoardStandardStreams 
                        where Active=1 and ID=(select top(1) BoardStandardStreamId from AcademicStandardStreams where Active=1 and ID=a.AcademicStandardStreamId))) end) as streamName,
                        e.IsAvailable as IsAvailable
                         from AcademicStudents as a
                        inner join Students as b on a.StudentId=b.ID
                        inner join AcademicStandards as c on a.AcademicStandardId=c.ID
                        inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                        inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                        inner join Organizations as f on d.OrganizationId=f.ID 
                        inner join BoardStandards as g on c.BoardStandardId=g.ID
                        inner join Standards as h on g.StandardId=h.ID
                        inner join SchoolWing as i on c.SchoolWingId=i.ID
                        inner join Wing as j on i.WingID=j.ID
                        inner join Boards as k on g.BoardId=k.ID
                        where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1
                            and j.Active=1 and k.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Academic_Student_Section> Get_view_All_Academic_Student_Sections()
        {
            var res = context.view_All_Academic_Student_Sections.FromSql(@"                        select l.ID as ID,c.OrganizationAcademicId as OraganisationAcademicId,
                        c.BoardStandardId as BoardStandardId, f.Name as OraganisationName,
                        d.AcademicSessionId as AcademicSessionId, e.DisplayName as SessionName,
                        g.StandardId as StandardId,h.Name as StandardName,h.StreamType as StandardStreamType,
                        c.SchoolWingId as SchoolWingId,a.ModifiedDate as ModifiedDate,j.Name as WingName,
                        a.StudentId as StudentId,(b.FirstName+' '+b.LastName) as studentname,
                        b.StudentCode as studentcode,k.Name as BoardName,
                        a.AcademicStandardStreamId,a.AcademicStandardId,
                        (case when (a.AcademicStandardStreamId is null or a.AcademicStandardStreamId=0) then '' else 
                        (select top(1) Name from Streams 
                        where Active=1 and ID=(select top(1) StreamId from BoardStandardStreams 
                        where Active=1 and ID=(select top(1) BoardStandardStreamId from AcademicStandardStreams where Active=1 and ID=a.AcademicStandardStreamId))) end) as streamName,
                        e.IsAvailable as IsAvailable,
						l.AcademicStudentId as AcademicStudentId,
						l.AcademicSectionId as AcademicSectionId,
						l.StudentCode as RollNo,
						n.Name as SectionName
                         from AcademicSectionStudents as l
						inner join AcademicStudents as a on l.AcademicStudentId=a.ID
						inner join AcademicSections as m on l.AcademicSectionId=m.ID
						inner join Sections as n on m.SectionId=n.ID
                        inner join Students as b on a.StudentId=b.ID
                        inner join AcademicStandards as c on a.AcademicStandardId=c.ID
                        inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                        inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                        inner join Organizations as f on d.OrganizationId=f.ID 
                        inner join BoardStandards as g on c.BoardStandardId=g.ID
                        inner join Standards as h on g.StandardId=h.ID
                        inner join SchoolWing as i on c.SchoolWingId=i.ID
                        inner join Wing as j on i.WingID=j.ID
                        inner join Boards as k on g.BoardId=k.ID
                        where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1
                            and j.Active=1 and k.Active=1 and l.Active=1 and m.Active=1 and n.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Academic_Standard> Get_view_All_Academic_Standards()
        {
            var res = context.view_All_Academic_Standards.FromSql(@" select c.ID as ID,
                            c.OrganizationAcademicId as OraganisationAcademicId,
                            c.BoardStandardId as BoardStandardId,
                            f.Name as OraganisationName,
                            d.AcademicSessionId as AcademicSessionId,
                            e.DisplayName as SessionName,
                            g.StandardId as StandardId,
                            h.Name as StandardName,
                            h.StreamType as StandardStreamType,
                            c.ModifiedDate as ModifiedDate,
                            k.Name as BoardName,
                            e.IsAvailable as IsAvailable,
                            c.Status as Status,
c.Nucleus,
                            (select COUNT(*) from AcademicSections where AcademicStandardId=c.ID and Active=1) as CountAcademicSection,
                           (select COUNT(*) from academicStandardWings where AcademicStandardId=c.ID and Active=1) as CountAcademicWing,
						   (select COUNT(*) from AcademicSubjectGroups where AcademicStandardId=c.ID and Active=1) as CountAcademicSubjectGroup
						  
                             from AcademicStandards as c
                            inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                            inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                            inner join Organizations as f on d.OrganizationId=f.ID 
                            inner join BoardStandards as g on c.BoardStandardId=g.ID
                            inner join Standards as h on g.StandardId=h.ID
                            inner join Boards as k on g.BoardId=k.ID
                            where c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and k.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_All_Subject_Group> Get_View_All_Subject_Groups()
        {
            var res = context.view_All_Subject_Groups.FromSql(@"SELECT a.ID as SubjectGroupID,a.Name as SubjectGroupName, a.InsertedDate,a.ModifiedDate,a.IsAvailable,a.BoardStandardId,
h.Name as StandardName,h.ID as StandardID, k.Name as BoardName,k.ID as BoardID,(select count(*) from [dbo].[SubjectGroupDetailss] where Active=1 and IsAvailable=1 and SubjectGroupID=a.ID) as NoOfSubject
 
  FROM [dbo].[SubjectGroups] as a
 
                            inner join BoardStandards as g on a.BoardStandardId=g.ID
                            inner join Standards as h on g.StandardId=h.ID
                            inner join Boards as k on g.BoardId=k.ID
                            where a.Active=1 and g.Active=1 and h.Active=1
                            and k.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_Common_List> Get_Standard(long? BoardId)
        {
            if (BoardId != null)
            {
                var res = context.view_Common_Lists.FromSql(@"SELECT a.ID as ID,b.Name as Name
  FROM [dbo].[BoardStandards] as a
  inner join [dbo].[Standards] as b on a.StandardId=b.ID
  where a.Active=1 and b.Active=1 and a.BoardId=({0})", BoardId.Value).AsNoTracking().ToList();
                return res;
            }
            else
            {
                var res = context.view_Common_Lists.FromSql(@"SELECT a.ID as ID,b.Name as Name
  FROM [dbo].[BoardStandards] as a
  inner join [dbo].[Standards] as b on a.StandardId=b.ID
  where a.Active=1 and b.Active=1").AsNoTracking().ToList();
                return res;
            }
        }

        public List<View_All_Subject_Optional_Category> Get_All_Subject_Optional_Categories()
        {
            var res = context.view_All_Subject_Optional_Categories.FromSql(@"SELECT a.ID as SubjectOptionalCategoryID,a.BoardStandardID,a.SubjectGroupID,a.SubjectOptionalTypeID,f.Name as SubjectOptionalTypeName,a.WingID,a.ModifiedDate,a.IsAvailable,
g.StandardId,g.BoardId,h.Name as StandardName, k.Name as BoardName,l.Name as WingName,m.Name as SubjectGroupName,
(select count(*) from [dbo].[SubjectOptionals] where Active=1 and SubjectOptionalCategoryID=a.ID) as NoOfSubject
 FROM [dbo].[SubjectOptionalCategories] as a
 inner join [dbo].[SubjectOptionalTypes] as f on a.SubjectOptionalTypeID=f.ID
                            inner join BoardStandards as g on a.BoardStandardId=g.ID
                            inner join Standards as h on g.StandardId=h.ID
                            inner join Boards as k on g.BoardId=k.ID
							left join [dbo].[BoardStandardWings] as n on a.WingID=n.ID
							inner join Wing as l on n.WingID=l.ID 
							left join SubjectGroups as m on a.SubjectGroupID=m.ID
                            where a.Active=1 and g.Active=1 and h.Active=1
                            and k.Active=1 and l.Active=1 and m.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_Academic_Student> Get_View_Academic_Student()
        {
            var res = context.View_Academic_Students.FromSql(@"select a.ID as ID,a.Nucleus,c.OrganizationAcademicId as OraganisationAcademicId,
a.AcademicStandardId,a.AcademicSubjectCombinationId,a.AcademicStandardWingId,
a.StudentId,b.PrimaryMobile,b.ZoomMailID,b.EmailID,b.WhatsappMobile,
                        c.BoardStandardId as BoardStandardId,d.AcademicSessionId,
                        f.Name as OraganisationName,
                        e.DisplayName as SessionName,
                        g.StandardId as StandardId,
						g.BoardId as BoardId,
                        h.Name as StandardName,
						l.WingID as WingID,
                        a.ModifiedDate as ModifiedDate,
                        j.Name as WingName,
                        (b.FirstName+' '+b.LastName) as studentname,
                        b.StudentCode as studentcode,
                        k.Name as BoardName,
o.Name as SectionName,
						m.StudentCode as RollNo,
						m.AcademicSectionId,
						o.ID as SectionID,
						(select top(1) Nucleus from [dbo].[AcademicStandardSettings] where Active=1 and [AcademicStandardId]=a.AcademicStandardId) as IsRequiredNucleus,
                       (case when a.AcademicSubjectCombinationId!=0 then (Select y.Name from AcademicSubjectGroups as x inner join SubjectGroups as y on x.SubjectGroupID=y.ID  where x.Active=1 and x.ID=a.AcademicSubjectCombinationId and y.Active=1) else '' end)as SubjectGroupName,
					   (case when (select count(*) from academicStudentSubjectOptionals where AcademicStudentID=a.ID and Active=1)>0 then ((select w.Name+',' from academicStudentSubjectOptionals as x inner join AcademicSubjectOptionalDetailss as y on x.AcademicSubjectOptionalDetailID=y.ID inner join SubjectOptionals as z on y.SubjectOptionalID=z.ID inner join Subjects as w on z.SubjectID=w.ID where w.Active=1 and z.Active=1 and x.AcademicStudentID=a.ID and x.Active=1 and y.Active=1 FOR XML PATH (''))) else 'No Optional Subject' end) as optionalSubject,
                        e.IsAvailable as IsAvailable
                         from AcademicStudents as a
						inner join AcademicSectionStudents as m on a.ID=m.AcademicStudentId
                        inner join AcademicSections as n on m.AcademicSectionId=n.ID
						inner join Sections as o on n.SectionId=o.ID
                        inner join Students as b on a.StudentId=b.ID
                        inner join AcademicStandards as c on a.AcademicStandardId=c.ID
                        inner join OrganizationAcademics as d on c.OrganizationAcademicId=d.ID
                        inner join AcademicSessions as e on d.AcademicSessionId=e.ID
                        inner join Organizations as f on d.OrganizationId=f.ID 
                        inner join BoardStandards as g on c.BoardStandardId=g.ID
                        inner join Standards as h on g.StandardId=h.ID
                        inner join academicStandardWings as i on a.AcademicStandardWingId=i.ID
						inner join BoardStandardWings as l on i.BoardStandardWingID=l.ID
                        inner join Wing as j on l.WingID=j.ID
                        inner join Boards as k on g.BoardId=k.ID
                        where a.Active=1 and b.Active=1 and c.Active=1 and a.IsAvailable=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1
                            and j.Active=1 and k.Active=1 and m.IsAvailable=1 and m.Active=1 and l.Active=1").AsNoTracking().ToList();
            return res;
        }
        public List<View_Previous_Academic> GetStudentPreviousAcademic()
        {
            var res = context.view_Previous_Academics.FromSql(@"SELECT a.[ID]
      ,a.[InsertedDate]    
      ,a.[OrganizationName]
      ,a.[BoardID]
      ,a.[ClassID]
      ,a.[FromDate]
      ,a.[ToDate]
      ,a.[StudentID]
      ,a.[ReasonForChange]
      ,a.[GradeOrPercentage]
      ,a.[CityId]
      ,a.[StateId]
	  ,b.Name as BoardName
	  ,c.Name as StandardName
	  ,d.Name as CityName
	  ,e.Name as StateName
	  ,f.Name as CountryName,
	  e.CountryID as CountryID,
	  g.StudentCode,
	  (g.FirstName+' '+g.LastName) as StudentName
  FROM [dbo].[StudentAcademics] as a
  inner join Students as g on a.StudentID=g.ID
  inner join Boards as b on a.BoardID=b.ID
  inner join Standards as c on a.StudentID=c.ID
  inner join Cities as d on a.CityId=d.ID
  inner join States as e on a.StateId=e.ID
  inner join Countries as f on e.CountryID=f.ID
  where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1").AsNoTracking().ToList();
            return res;
        }
        #endregion

    }
}
