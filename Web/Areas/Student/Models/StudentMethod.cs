﻿using DocumentFormat.OpenXml.Drawing.ChartDrawing;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Infrastructure;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.ApplicationCore.Query;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Web.Models;
using OdmErp.Web.Models.SchoolBelq;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Student.Models
{
    public class StudentMethod
    {
        private IStudentAggregateRepository studentAggregateRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private IReligionTypeRepository religionTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private ICityRepository cityRepository;
        private IStateRepository stateRepository;
        private ICountryRepository countryRepository;
        private IAddressRepository addressRepository;
        private StudentSqlQuery sqlQuery;
        private IDocumentTypeRepository documentTypeRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IBoardRepository boardRepository;
        private IStandardRepository standardRepository;
        public IAcademicSessionRepository academicSessionRepository;
        public IStreamRepository streamRepository;
        public ISubjectRepository subjectRepository;
        public IStudentLanguagesKnownRepository studentLanguagesKnownRepository;
        public IBoardStandardRepository boardStandardRepository;
        //public IBoardStandardStreamRepository boardStandardStreamRepository;
        public IAcademicSubjectGroupRepository academicSubjectGroupRepository;
        public IAcademicSubjectOptionalRepository academicSubjectOptionalRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IWingRepository wingRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IStudentEmergencyContactRepository studentEmergencyContactRepository;
        public IStudentTransportationRepository studentTransportationRepository;
        public IAcademicStandardSettingsRepository academicStandardSettingsRepository;
        public ILanguageRepository languageRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public OdmErp.Web.Models.CommonMethods commonMethods;
        // public  academicSubjectGroupRepository;


        public ISubjectGroupRepository subjectGroupRepository { get; set; }
        public ISubjectGroupDetailsRepository subjectGroupDetailsRepository { get; set; }
        public ISubjectOptionalTypeRepository subjectOptionalTypeRepository { get; set; }
        public ISubjectOptionalCategoryRepository subjectOptionalCategoryRepository { get; set; }
        public ISubjectOptionalRepository subjectOptionalRepository { get; set; }
        public IVehicleTypeRepository vehicleTypeRepository { get; set; }
        public IVehicleRepository vehicleRepository { get; set; }
        public IVehicleLocationRepository vehicleLocationRepository { get; set; }
        public IDepartureTimeRepository departureTimeRepository { get; set; }
        public IOrganizationRepository organizationRepository { get; set; }
        public IOrganizationAcademicRepository organizationAcademicRepository { get; set; }
        public IGroupRepository groupRepository { get; set; }
        public IAcademicStudentRepository academicStudentRepository { get; set; }
        public IAcademicSectionStudentRepository academicSectionStudentRepository { get; set; }
        public IAcademicStudentSubjectOptionalRepository academicStudentSubjectOptionalRepository { get; set; }
        public ICategoryRepository categoryRepository { get; set; }
        public IAcademicSectionRepository academicSectionRepository { get; set; }
        public ISectionRepository sectionRepository { get; set; }

        public StudentMethod(ISectionRepository sectionRepo,IAcademicSectionRepository academicSectionRepo,IAcademicStandardRepository academicStandardRepo, IOrganizationAcademicRepository organizationAcademicRepo, IStudentLanguagesKnownRepository studentLanguagesKnownRepo, ILanguageRepository languageRepo, OdmErp.Web.Models.CommonMethods common, ICategoryRepository categoryRepo, IAcademicStandardSettingsRepository academicStandardSettingsRepo, IAcademicSectionStudentRepository academicSectionStudentRepo, IAcademicStudentSubjectOptionalRepository academicStudentSubjectOptionalRepo, IAcademicStudentRepository academicStudentRepo, IStudentTransportationRepository studentTransportationRepo, IStudentEmergencyContactRepository studentEmergencyContactRepo, IGroupRepository groupRepo, IOrganizationRepository organizationRepo, IAcademicStandardWingRepository academicStandardWingRepo, IAcademicSubjectOptionalRepository academicSubjectOptionalRepo, IWingRepository wingRepo, IBoardStandardWingRepository boardStandardWingRepo, IAcademicSubjectGroupRepository academicSubjectGroupRepo, IBoardStandardRepository boardStandardRepo, ISubjectRepository subjectRepo, IStreamRepository streamRepo, IStudentAggregateRepository studentAggregateRepo, INationalityTypeRepository nationalityTypeRepo,
           IBloodGroupRepository bloodGroupRepo, IReligionTypeRepository religionTypeRepo, IAddressTypeRepository addressTypeRepo,
           ICityRepository cityRepo, IStateRepository stateRepo, ICountryRepository countryRepo, IAddressRepository addressRepo, StudentSqlQuery sql, IDocumentTypeRepository documentTypeRepo,
            IDocumentSubTypeRepository documentSubTypeRepo, IBoardRepository boardRepo, IStandardRepository standardRepo, ISubjectGroupRepository subjectGroupRepo, ISubjectGroupDetailsRepository subjectGroupDetailsRepo,
            ISubjectOptionalTypeRepository subjectOptionalTypeRepo, ISubjectOptionalCategoryRepository subjectOptionalCategoryRepo, ISubjectOptionalRepository subjectOptionalRepo,
            IVehicleTypeRepository vehicleTypeRepo, IVehicleRepository vehicleRepo, IVehicleLocationRepository vehicleLocationRepo, IDepartureTimeRepository departureTimeRepo, IAcademicSessionRepository academicSessionRepo)
        {
            sectionRepository = sectionRepo;
            academicSectionRepository = academicSectionRepo;
            academicStandardRepository = academicStandardRepo;
            organizationAcademicRepository = organizationAcademicRepo;
            studentLanguagesKnownRepository = studentLanguagesKnownRepo;
            languageRepository = languageRepo;
            commonMethods = common;
            categoryRepository = categoryRepo;
            academicStandardSettingsRepository = academicStandardSettingsRepo;
            academicStudentRepository = academicStudentRepo;
            academicSectionStudentRepository = academicSectionStudentRepo;
            academicStudentSubjectOptionalRepository = academicStudentSubjectOptionalRepo;
            studentTransportationRepository = studentTransportationRepo;
            studentEmergencyContactRepository = studentEmergencyContactRepo;
            groupRepository = groupRepo;
            organizationRepository = organizationRepo;
            academicStandardWingRepository = academicStandardWingRepo;
            academicSubjectOptionalRepository = academicSubjectOptionalRepo;
            wingRepository = wingRepo;
            boardStandardWingRepository = boardStandardWingRepo;
            academicSubjectGroupRepository = academicSubjectGroupRepo;
            boardStandardRepository = boardStandardRepo;
            //  boardStandardStreamRepository = boardStandardStreamRepo;
            subjectRepository = subjectRepo;
            streamRepository = streamRepo;
            sqlQuery = sql;
            academicSessionRepository = academicSessionRepo;
            studentAggregateRepository = studentAggregateRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            bloodGroupRepository = bloodGroupRepo;
            religionTypeRepository = religionTypeRepo;
            addressRepository = addressRepo;
            addressTypeRepository = addressTypeRepo;
            cityRepository = cityRepo;
            stateRepository = stateRepo;
            countryRepository = countryRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            boardRepository = boardRepo;
            standardRepository = standardRepo;

            subjectGroupRepository = subjectGroupRepo;
            subjectGroupDetailsRepository = subjectGroupDetailsRepo;
            subjectOptionalTypeRepository = subjectOptionalTypeRepo;
            subjectOptionalCategoryRepository = subjectOptionalCategoryRepo;
            subjectOptionalRepository = subjectOptionalRepo;
            vehicleTypeRepository = vehicleTypeRepo;
            vehicleRepository = vehicleRepo;
            vehicleLocationRepository = vehicleLocationRepo;
            departureTimeRepository = departureTimeRepo;
        }
        #region Student
        public class StudentListCls
        {
            public long ID { get; set; }
            public DateTime InsertedOn { get; set; }
            public string StudentCode { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string LastName { get; set; }
            public string Gender { get; set; }
            public DateTime DateOfBirth { get; set; }
            public long BloodGroupID { get; set; }
            public string BloodGroupName { get; set; }
            public long NatiobalityID { get; set; }
            public string NatiobalityName { get; set; }
            public long ReligionID { get; set; }
            public string ReligionName { get; set; }
            public long CategoryID { get; set; }
            public string CategoryName { get; set; }
            public long MotherTongueID { get; set; }
            public string MotherTongueName { get; set; }
            public string PassportNO { get; set; }
            public string AadharNO { get; set; }
            public string PrimaryMobile { get; set; }
            public string WhatsappMobile { get; set; }
            public string EmailID { get; set; }
            public string ZoomMailID { get; set; }
            public string Image { get; set; }

        }
        public List<StudentModelClass> GetStudentDetailsList()
        {
            var studentlist = studentAggregateRepository.GetAllStudent();
            List<StudentModelClass> ob = new List<StudentModelClass>();
            foreach (var studentdetails in studentlist)
            {
                StudentModelClass studentcls = new StudentModelClass();
                studentcls.bloodGroups = bloodGroupRepository.GetAllBloodGroup();
                studentcls.nationalityTypes = nationalityTypeRepository.GetAllNationalities();
                studentcls.religionTypes = religionTypeRepository.GetAllReligionTypes();
                studentcls.categoryTypes = categoryRepository.ListAllAsyncIncludeAll().Result;
                studentcls.BloodGroupID = studentdetails.BloodGroupID;
                studentcls.BloodGroupName = bloodGroupRepository.GetBloodGroupById(studentdetails.BloodGroupID).Name;
                studentcls.DateOfBirth = studentdetails.DateOfBirth;
                studentcls.StudentCode = studentdetails.StudentCode;
                studentcls.FirstName = studentdetails.FirstName;
                studentcls.Gender = studentdetails.Gender;
                studentcls.ID = studentdetails.ID;
                studentcls.LastName = studentdetails.LastName;
                studentcls.MiddleName = studentdetails.MiddleName;
                studentcls.AadharNO = studentdetails.AadharNO;
                studentcls.CategoryID = studentdetails.CategoryID;
                studentcls.EmailID = studentdetails.EmailID;
                var languages = studentLanguagesKnownRepository.ListAllAsync().Result.Where(a => a.Active == true && a.StudentID == studentdetails.ID).Join(languageRepository.ListAllAsync().Result, a => a.LanguageID, b => b.ID, (a, b) => new { name = b.Name.ToString() }).Select(a => a.name.ToString()).ToArray();

                studentcls.LanguagesKnowns = string.Join(",", languages);
                studentcls.MotherTongue = languageRepository.ListAllAsync().Result.Where(a => a.ID == studentdetails.MotherTongueID).FirstOrDefault() == null ? "" : languageRepository.ListAllAsync().Result.Where(a => a.ID == studentdetails.MotherTongueID).FirstOrDefault().Name;
                studentcls.MotherTongueID = studentdetails.MotherTongueID;
                studentcls.PrimaryMobile = studentdetails.PrimaryMobile;
                studentcls.WhatsappMobile = studentdetails.WhatsappMobile;
                studentcls.ZoomMailID = studentdetails.ZoomMailID;
                studentcls.NatiobalityID = studentdetails.NatiobalityID;
                studentcls.NatiobalityName = nationalityTypeRepository.GetNationalityById(studentdetails.NatiobalityID).Name;
                studentcls.ReligionID = studentdetails.ReligionID;
                studentcls.ReligionName = religionTypeRepository.GetReligionTypeById(studentdetails.ReligionID).Name;
                studentcls.CategoryName = categoryRepository.GetCategoryNameById(studentdetails.CategoryID).Result.Name;

                if (studentdetails.Image != null)
                {
                    string path = Path.Combine("/ODMImages/StudentProfile/", studentdetails.Image);
                    studentcls.ProfileImage = path;
                }
                else
                {
                    studentcls.ProfileImage = null;
                }
                studentcls.studentAdresses = GetAllAddressByUsingStudentId(studentdetails.ID);
                studentcls.studentParentsCls = GetAllParentsByUsingStudentId(studentdetails.ID);
                studentcls.studentClassDetails = sqlQuery.Get_View_Academic_Student().Where(a => a.StudentId == studentdetails.ID) == null ? null : sqlQuery.Get_View_Academic_Student().Where(a => a.StudentId == studentdetails.ID).Where(a => a.IsAvailable == true).FirstOrDefault();
                studentcls.accesscls = commonMethods.GetStudentAccessDetails().Where(a => a.EmployeeID == studentdetails.ID && (a.RoleID == 5 || a.RoleID == 6)).ToList();
                studentcls.StudentDocumentAccess = GetAllDocumentByUsingStudentId(studentdetails.ID) == null ? null : GetAllDocumentByUsingStudentId(studentdetails.ID).Where(a => a.DocumentTypeName == "EDUCATIONAL").ToList();
                studentcls.StudentPersonalDocumentAccess = GetAllDocumentByUsingStudentId(studentdetails.ID) == null ? null : GetAllDocumentByUsingStudentId(studentdetails.ID).Where(a => a.DocumentTypeName == "PERSONAL").ToList();
                studentcls.StudentAcademicAccess = GetAllStudentAcademicByStudentId(studentdetails.ID);
                studentcls.emergencyContact = GetStudentEmergencyContacts(studentdetails.ID).FirstOrDefault();
                studentcls.transport = GetStudentTransportation(studentdetails.ID) == null ? null : GetStudentTransportation(studentdetails.ID).FirstOrDefault();
                ob.Add(studentcls);
            }
            return ob;
        }
        public IEnumerable<StudentModelClass> GetAllStudent()
        {
            try
            {
                var studentdetails = studentAggregateRepository.GetAllStudent();
                var nationalityTypes = nationalityTypeRepository.GetAllNationalities();
                var bloodgroup = bloodGroupRepository.GetAllBloodGroup();
                var religionType = religionTypeRepository.GetAllReligionTypes();
                var res = (from a in studentdetails
                           join b in nationalityTypes on a.NatiobalityID equals b.ID
                           join c in bloodgroup on a.BloodGroupID equals c.ID
                           join d in religionType on a.ReligionID equals d.ID
                           select new StudentModelClass
                           {
                               BloodGroupID = a.BloodGroupID,
                               BloodGroupName = c.Name,
                               DateOfBirth = a.DateOfBirth,
                               StudentCode = a.StudentCode,
                               FirstName = a.FirstName,
                               Gender = a.Gender,
                               ID = a.ID,
                               LastName = a.LastName,
                               MiddleName = a.MiddleName,
                               NatiobalityID = a.NatiobalityID,
                               NatiobalityName = b.Name,
                               ReligionID = a.ReligionID,
                               ReligionName = d.Name,
                               ModifiedDate = a.ModifiedDate
                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }

        }
        public IEnumerable<StudentAdressCls> GetAllAddressByUsingStudent()
        {
            try
            {
                var studentAddresses = studentAggregateRepository.GetAllStudentAddress().ToList();
                var address = addressRepository.GetAllAddress();
                var addressType = addressTypeRepository.GetAllAddressType();
                var city = cityRepository.GetAllCity();
                var state = stateRepository.GetAllState();
                var country = countryRepository.GetAllCountries();

                IEnumerable<StudentAdressCls> res = (from a in studentAddresses
                                                     join b in address on a.AddressID equals b.ID
                                                     join c in addressType on b.AddressTypeID equals c.ID
                                                     join d in city on b.CityID equals d.ID
                                                     join e in state on b.StateID equals e.ID
                                                     join f in country on b.CountryID equals f.ID
                                                     select new StudentAdressCls
                                                     {
                                                         AddressLine1 = b.AddressLine1,
                                                         AddressLine2 = b.AddressLine2,
                                                         AddressTypeID = b.AddressTypeID,
                                                         AddressTypeName = c.Name,
                                                         CityID = b.CityID,
                                                         CityName = d.Name,
                                                         CountryID = b.CountryID,
                                                         CountryName = f.Name,
                                                         ID = a.ID,
                                                         PostalCode = b.PostalCode,
                                                         StateID = b.StateID,
                                                         StateName = e.Name,
                                                         ModifiedDate = a.ModifiedDate,
                                                         InsertedDate = a.InsertedDate,
                                                         StudentId = a.StudentID,
                                                     }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<StudentClassDetails> GetAllStudentClassByStudent()
        {
            try
            {
                var academics = sqlQuery.Get_View_Academic_Student().Where(a => a.IsAvailable == true).ToList();

                IEnumerable<StudentClassDetails> res = (from a in academics
                                                        select new StudentClassDetails
                                                        {
                                                            ID = a.ID,
                                                            IsCurrent = true,
                                                            OrganizationID = a.OraganisationAcademicId,
                                                            OrganizationName = a.OraganisationName,
                                                            SectionID = a.SectionID,
                                                            SectionName = a.SectionName,
                                                            StandardID = a.StandardId,
                                                            StandardName = a.StandardName,
                                                            StudentID = a.StudentId,
                                                            WingID = a.WingID,
                                                            WingName = a.WingName,
                                                            ModifiedDate = a.ModifiedDate,
                                                            BoardID = a.BoardStandardId,
                                                            BoardName = a.BoardName,
                                                            SessionID = a.AcademicSessionId,
                                                            SessionName = a.SessionName
                                                        }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<StudentDocumentModelClass> GetAllDocumentByUsingStudent()
        {
            try
            {
                var StudentRequiredDocumentList = studentAggregateRepository.GetAllStudentRequiredDocument().ToList();
                var documentTypeList = documentTypeRepository.GetAllDocumentTypes();
                var documentSubTypeList = documentSubTypeRepository.GetAllDocumentSubTypes();
                var documentsList = studentAggregateRepository.GetAllStudentDocumentAttachment();
                var StudentAcademicList = studentAggregateRepository.GetAllStudentAcademic().Where(a => a.StudentID == StudentRequiredDocumentList.FirstOrDefault().ID);
                var BoardList = boardRepository.GetAllBoard().ToList();
                var ClassList = standardRepository.GetAllStandard().ToList();
                var res = (from a in StudentRequiredDocumentList
                           join b in documentTypeList on a.DocumentTypeID equals b.ID
                           join c in documentSubTypeList on a.DocumentSubTypeID equals c.ID
                           select new StudentDocumentModelClass
                           {
                               ID = a.ID,
                               totaldocument = documentsList.Where(m => m.StudentRequiredDocumentID == a.ID).ToList().Count,
                               DocumentSubTypeID = a.DocumentSubTypeID,
                               DocumentSubTypeName = c.Name,
                               DocumentTypeID = a.DocumentTypeID,
                               DocumentTypeName = b.Name,
                               StudentID = a.StudentID,
                               IsApproved = a.IsApproved,
                               IsMandatory = a.IsMandatory,
                               IsReject = a.IsReject,
                               IsVerified = a.IsVerified,
                               RelatedID = a.RelatedID,
                               VerifiedAccessID = a.VerifiedAccessID,
                               VerifiedDate = a.VerifiedDate,
                               BoardName = a.RelatedID != 0 ? BoardList.Where(x => x.ID == StudentAcademicList.Where(z => z.ID == a.RelatedID).Select(z => z.BoardID).FirstOrDefault()).Select(s => s.Name).FirstOrDefault() : "NA",
                               ClassName = a.RelatedID != 0 ? ClassList.Where(x => x.ID == StudentAcademicList.Where(z => z.ID == a.RelatedID).Select(z => z.ClassID).FirstOrDefault()).Select(s => s.Name).FirstOrDefault() : "NA",
                               OrganizationName = StudentAcademicList.Where(z => z.ID == a.RelatedID).Select(z => z.OrganizationName).FirstOrDefault(),

                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public List<StudentAcademicModel> GetAllStudentAcademicListByStudent()
        {
            try
            {
                var StudentAcademicList = studentAggregateRepository.GetAllStudentAcademic().ToList();
                var BoardList = boardRepository.GetAllBoard().ToList();
                var ClassList = standardRepository.GetAllStandard().ToList();

                var res = (from a in StudentAcademicList
                           join b in BoardList on a.BoardID equals b.ID
                           join c in ClassList on a.ClassID equals c.ID
                           select new StudentAcademicModel
                           {
                               StudentID = a.StudentID,
                               ID = a.ID,
                               BoardID = a.BoardID,
                               BoardName = b.Name,
                               ClassID = a.ClassID,
                               ClassName = c.Name,
                               OrganizationName = a.OrganizationName,
                               ModifiedDate = a.ModifiedDate,
                               FromDate = a.FromDate,
                               ToDate = a.ToDate
                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public bool studentRequiredDocument(long stuId, long typeId, long subtypeId, long relatedId)
        {
            StudentRequiredDocument studentRequired = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.DocumentTypeID == typeId && a.DocumentSubTypeID == subtypeId && a.RelatedID == relatedId && a.StudentID == stuId).FirstOrDefault();
            if (studentRequired == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public IEnumerable<StudentAdressCls> GetAllStudentAddress()
        {
            try
            {
                var studentAddresses = studentAggregateRepository.GetAllStudentAddress().ToList();
                var address = addressRepository.GetAllAddress();
                var addressType = addressTypeRepository.GetAllAddressType();
                var city = cityRepository.GetAllCity();
                var state = stateRepository.GetAllState();
                var country = countryRepository.GetAllCountries();

                IEnumerable<StudentAdressCls> res = (from a in studentAddresses
                                                     join b in address on a.AddressID equals b.ID
                                                     join c in addressType on b.AddressTypeID equals c.ID
                                                     join d in city on b.CityID equals d.ID
                                                     join e in state on b.StateID equals e.ID
                                                     join f in country on b.CountryID equals f.ID
                                                     select new StudentAdressCls
                                                     {
                                                         AddressLine1 = b.AddressLine1,
                                                         AddressLine2 = b.AddressLine2,
                                                         AddressTypeID = b.AddressTypeID,
                                                         AddressTypeName = c.Name,
                                                         CityID = b.CityID,
                                                         CityName = d.Name,
                                                         CountryID = b.CountryID,
                                                         CountryName = f.Name,
                                                         ID = a.ID,
                                                         PostalCode = b.PostalCode,
                                                         StateID = b.StateID,
                                                         StateName = e.Name,
                                                         ModifiedDate = a.ModifiedDate,
                                                         InsertedDate = a.InsertedDate,
                                                         StudentAddressId=a.ID,
                                                         StudentId=a.StudentID
                                                     }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<StudentAdressCls> GetAllAddressByUsingStudentId(long id)
        {
            try
            {
                var studentAddresses = studentAggregateRepository.GetAllStudentAddress().Where(a => a.StudentID == id).ToList();
                var address = addressRepository.GetAllAddress();
                var addressType = addressTypeRepository.GetAllAddressType();
                var city = cityRepository.GetAllCity();
                var state = stateRepository.GetAllState();
                var country = countryRepository.GetAllCountries();

                IEnumerable<StudentAdressCls> res = (from a in studentAddresses
                                                     join b in address on a.AddressID equals b.ID
                                                     join c in addressType on b.AddressTypeID equals c.ID
                                                     join d in city on b.CityID equals d.ID
                                                     join e in state on b.StateID equals e.ID
                                                     join f in country on b.CountryID equals f.ID
                                                     select new StudentAdressCls
                                                     {
                                                         AddressLine1 = b.AddressLine1,
                                                         AddressLine2 = b.AddressLine2,
                                                         AddressTypeID = b.AddressTypeID,
                                                         AddressTypeName = c.Name,
                                                         CityID = b.CityID,
                                                         CityName = d.Name,
                                                         CountryID = b.CountryID,
                                                         CountryName = f.Name,
                                                         ID = a.ID,
                                                         PostalCode = b.PostalCode,
                                                         StateID = b.StateID,
                                                         StateName = e.Name,
                                                         ModifiedDate = a.ModifiedDate,
                                                         InsertedDate = a.InsertedDate
                                                     }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<StudentParentsCls> GetAllParentsByUsingStudentId(long id)
        {
            try
            {
                var studentAddresses = studentAggregateRepository.GetAllParent().Where(a => a.StudentID == id).ToList();
                var parentRelationshipTypes = studentAggregateRepository.GetAllParentRelationshipType();
                var professionTypes = studentAggregateRepository.GetAllProfessionType();

                IEnumerable<StudentParentsCls> res = (from a in studentAddresses
                                                      join b in parentRelationshipTypes on a.ParentRelationshipID equals b.ID
                                                      join c in professionTypes on a.ProfessionTypeID equals c.ID
                                                      select new StudentParentsCls
                                                      {
                                                          ID = a.ID,
                                                          AlternativeMobile = a.AlternativeMobile,
                                                          EmailId = a.EmailId,
                                                          FirstName = a.FirstName,
                                                          LandlineNumber = a.LandlineNumber,
                                                          LastName = a.LastName,
                                                          MiddleName = a.MiddleName,
                                                          ParentRelationshipID = a.ParentRelationshipID,
                                                          ParentRelationshipName = b.Name,
                                                          PrimaryMobile = a.PrimaryMobile,
                                                          ProfessionTypeID = a.ProfessionTypeID,
                                                          ProfessionTypeName = c.Name,
                                                          StudentID = a.StudentID,
                                                          ModifiedDate = a.ModifiedDate,
                                                          Imagepath = a.Image != null ? Path.Combine("/ODMImages/ParentProfile/", a.Image) : null
                                                      }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<StudentParentsCls> GetAllParentsforstudents()
        {
            try
            {
                var studentAddresses = studentAggregateRepository.GetAllParent().ToList();
                var parentRelationshipTypes = studentAggregateRepository.GetAllParentRelationshipType();
                var professionTypes = studentAggregateRepository.GetAllProfessionType();

                IEnumerable<StudentParentsCls> res = (from a in studentAddresses
                                                      join b in parentRelationshipTypes on a.ParentRelationshipID equals b.ID
                                                      join c in professionTypes on a.ProfessionTypeID equals c.ID
                                                      select new StudentParentsCls
                                                      {
                                                          ID = a.ID,
                                                          AlternativeMobile = a.AlternativeMobile,
                                                          EmailId = a.EmailId,
                                                          FirstName = a.FirstName,
                                                          LandlineNumber = a.LandlineNumber,
                                                          LastName = a.LastName,
                                                          MiddleName = a.MiddleName,
                                                          ParentRelationshipID = a.ParentRelationshipID,
                                                          ParentRelationshipName = b.Name,
                                                          PrimaryMobile = a.PrimaryMobile,
                                                          ProfessionTypeID = a.ProfessionTypeID,
                                                          ProfessionTypeName = c.Name,
                                                          StudentID = a.StudentID,
                                                          ModifiedDate = a.ModifiedDate,
                                                          Imagepath = a.Image != null ? Path.Combine("/ODMImages/ParentProfile/", a.Image) : null
                                                      }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<StudentClassDetails> GetAllStudentClassByStudentId(long id)
        {
            try
            {
                var academics = sqlQuery.Get_view_All_Academic_Students().Where(a => a.StudentId == id && a.IsAvailable == true).ToList();
                var academicsection = sqlQuery.Get_view_All_Academic_Student_Sections().Where(a => a.StudentId == id && a.IsAvailable == true).ToList();
                IEnumerable<StudentClassDetails> res = (from a in academics
                                                        select new StudentClassDetails
                                                        {
                                                            ID = a.ID,
                                                            IsCurrent = true,
                                                            OrganizationID = a.OraganisationAcademicId,
                                                            OrganizationName = a.OraganisationName,
                                                            SectionID = academicsection != null ? (academicsection.FirstOrDefault() != null ? academicsection.FirstOrDefault().AcademicSectionId : 0) : 0,
                                                            SectionName = academicsection != null ? (academicsection.FirstOrDefault() != null ? academicsection.FirstOrDefault().SectionName : "") : "",
                                                            StandardID = a.StandardId,
                                                            StandardName = a.StandardName,
                                                            StudentID = a.StudentId,
                                                            WingID = a.SchoolWingId,
                                                            WingName = a.WingName,
                                                            ModifiedDate = a.ModifiedDate,
                                                            BoardID = a.BoardStandardId,
                                                            BoardName = a.BoardName,
                                                            SessionID = a.AcademicSessionId,
                                                            SessionName = a.SessionName
                                                        }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<StudentDocumentModelClass> GetAllDocumentByUsingStudentId(long id)
        {
            try
            {
                var StudentRequiredDocumentList = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.StudentID == id).ToList();
                var documentTypeList = documentTypeRepository.GetAllDocumentTypes();
                var documentSubTypeList = documentSubTypeRepository.GetAllDocumentSubTypes();
                var documentsList = studentAggregateRepository.GetAllStudentDocumentAttachment();
                var StudentAcademicList = studentAggregateRepository.GetAllStudentAcademic().Where(a => a.StudentID == id);
                var BoardList = boardRepository.GetAllBoard().ToList();
                var ClassList = standardRepository.GetAllStandard().ToList();
                var res = (from a in StudentRequiredDocumentList
                           join b in documentTypeList on a.DocumentTypeID equals b.ID
                           join c in documentSubTypeList on a.DocumentSubTypeID equals c.ID
                           join d in StudentAcademicList on a.RelatedID equals d.ID
                           join e in BoardList on d.BoardID equals e.ID
                           join f in ClassList on d.ClassID equals f.ID
                           select new StudentDocumentModelClass
                           {
                               ID = a.ID,
                               totaldocument = documentsList.Where(m => m.StudentRequiredDocumentID == a.ID).ToList().Count,

                               DocumentSubTypeID = a.DocumentSubTypeID,
                               DocumentSubTypeName = c.Name,
                               DocumentTypeID = a.DocumentTypeID,
                               DocumentTypeName = b.Name,
                               StudentID = a.StudentID,
                               IsApproved = a.IsApproved,
                               IsMandatory = a.IsMandatory,
                               IsReject = a.IsReject,
                               IsVerified = a.IsVerified,
                               RelatedID = a.RelatedID,
                               VerifiedAccessID = a.VerifiedAccessID,
                               VerifiedDate = a.VerifiedDate,
                               BoardName = e.Name,
                               ClassName = f.Name,
                               OrganizationName = d.OrganizationName
                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public List<StudentDocumentModelClass> GetStudentDocuments(long id)
        {
            var StudentRequiredDocumentList = studentAggregateRepository.GetAllStudentRequiredDocument().Where(a => a.StudentID == id).ToList();
            var documentTypeList = documentTypeRepository.GetAllDocumentTypes();
            var documentSubTypeList = documentSubTypeRepository.GetAllDocumentSubTypes();
            var documentsList = studentAggregateRepository.GetAllStudentDocumentAttachment();

            var res = (from a in StudentRequiredDocumentList
                       join b in documentTypeList on a.DocumentTypeID equals b.ID
                       join c in documentSubTypeList on a.DocumentSubTypeID equals c.ID
                       select new StudentDocumentModelClass
                       {
                           ID = a.ID,
                           totaldocument = documentsList.Where(m => m.StudentRequiredDocumentID == a.ID).ToList().Count,

                           DocumentSubTypeID = a.DocumentSubTypeID,
                           DocumentSubTypeName = c.Name,
                           DocumentTypeID = a.DocumentTypeID,
                           DocumentTypeName = b.Name,
                           StudentID = a.StudentID,
                           IsApproved = a.IsApproved,
                           IsMandatory = a.IsMandatory,
                           IsReject = a.IsReject,
                           IsVerified = a.IsVerified,
                           RelatedID = a.RelatedID,
                           VerifiedAccessID = a.VerifiedAccessID,
                           VerifiedDate = a.VerifiedDate
                       }).ToList();
            return res;

        }
        public List<StudentAcademicModel> GetAllStudentAcademicByStudentId(long id)
        {
            try
            {
                var StudentAcademicList = studentAggregateRepository.GetAllStudentAcademic().Where(a => a.StudentID == id).ToList();
                var BoardList = boardRepository.GetAllBoard().ToList();
                var ClassList = standardRepository.GetAllStandard().ToList();

                var res = (from a in StudentAcademicList
                           join b in BoardList on a.BoardID equals b.ID
                           join c in ClassList on a.ClassID equals c.ID
                           select new StudentAcademicModel
                           {
                               ID = a.ID,
                               BoardID = a.BoardID,
                               BoardName = b.Name,
                               ClassID = a.ClassID,
                               ClassName = c.Name,
                               OrganizationName = a.OrganizationName,
                               ModifiedDate = a.ModifiedDate,
                               FromDate = a.FromDate,
                               ToDate = a.ToDate
                           }).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        #endregion
        #region Subject Group
        public List<View_All_Subject_Group> GetSubjectGroupList()
        {
            try
            {
                var subjectgroups = sqlQuery.Get_View_All_Subject_Groups().ToList();
                return subjectgroups;
            }
            catch (Exception e1)
            {
                throw;
            }
        }

        public subjectgroupcls GetSubjectgroupcls(long? id)
        {
            try
            {
                subjectgroupcls ob = new subjectgroupcls();
                ob.boards = boardRepository.GetAllBoard().Where(a => a.Active == true).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
                if (id != null)
                {
                    var subjectgrp = sqlQuery.Get_View_All_Subject_Groups().Where(a => a.SubjectGroupID == id).FirstOrDefault();
                    ob.BoardID = subjectgrp.BoardID;
                    ob.BoardStandardId = subjectgrp.BoardStandardId;
                    ob.SubjectGroupID = subjectgrp.SubjectGroupID;
                    ob.SubjectGroupName = subjectgrp.SubjectGroupName;
                    ob.standards = sqlQuery.Get_Standard(subjectgrp.BoardID).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
                    var subjectgroupdet = subjectGroupDetailsRepository.ListAllAsync().Result.Where(a => a.Active == true && a.SubjectGroupID == id).Select(a => a.SubjectID).ToArray();
                    ob.subjectids = subjectgroupdet;
                    ob.subjects = Getsubjects().Where(a => a.BoardStandardID == ob.BoardStandardId).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
                }
                else
                {
                    ob.BoardID = 0;
                    ob.BoardStandardId = 0;
                    ob.SubjectGroupID = 0;
                    ob.SubjectGroupName = "";
                    ob.subjectids = null;
                }
                return ob;
            }
            catch (Exception e1)
            {

                throw;
            }
        }
        public List<subjectclass> Getsubjects()
        {
            List<subjectclass> subj = new List<subjectclass>();
            var subjectlist = subjectRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            foreach (var a in subjectlist)
            {
                string subname = a.Name;
                Subject subss = a;
                while (subss.ParentSubjectId > 0)
                {
                    subss = subjectlist.Where(m => m.ID == subss.ParentSubjectId).FirstOrDefault();
                    subname = subss.Name + "-->" + subname;
                }
                subjectclass om = new subjectclass();
                om.ID = a.ID;
                om.Name = subname;
                om.BoardStandardID = a.BoardStandardId;
                //om.BoardStandardStreamId = a.BoardStandardStreamId;
                subj.Add(om);
            }
            return subj;
        }
        public List<commonclass> Getsubjects(long boardstandardid)
        {
            return Getsubjects().Where(a => a.BoardStandardID == boardstandardid).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
        }
        public List<commonclass> Getstandards(long boardid)
        {
            return sqlQuery.Get_Standard(boardid).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
        }

        public async Task<int> SaveOrUpdateSubjectGroup(subjectgroupcls ob, long userid)
        {
            if (ob.SubjectGroupID == 0)
            {
                var res = subjectGroupRepository.ListAllAsync().Result.Where(a => a.Name == ob.SubjectGroupName && a.BoardStandardId == ob.BoardStandardId && a.IsAvailable == true && a.Active == true).FirstOrDefault();
                if (res == null)
                {
                    SubjectGroup subjectGroup = new SubjectGroup();
                    subjectGroup.Active = true;
                    subjectGroup.BoardStandardId = ob.BoardStandardId;
                    subjectGroup.InsertedDate = DateTime.Now;
                    subjectGroup.InsertedId = userid;
                    subjectGroup.ModifiedDate = DateTime.Now;
                    subjectGroup.ModifiedId = userid;
                    subjectGroup.Name = ob.SubjectGroupName;
                    subjectGroup.Status = "CREATED";
                    subjectGroup.IsAvailable = true;
                    var id = subjectGroupRepository.AddAsync(subjectGroup).Result.ID;
                    if (ob.subjectids.Count() > 0)
                    {
                        foreach (var a in ob.subjectids)
                        {
                            SubjectGroupDetails details = new SubjectGroupDetails();
                            details.Active = true;
                            details.IsAvailable = true;
                            details.InsertedDate = DateTime.Now;
                            details.InsertedId = userid;
                            details.ModifiedDate = DateTime.Now;
                            details.ModifiedId = userid;
                            details.SubjectGroupID = id;
                            details.SubjectID = a;
                            details.Status = "CREATED";
                            await subjectGroupDetailsRepository.AddAsync(details);
                        }
                    }
                    return 1;
                }
                else
                {
                    return 2;//Already Exists
                }
            }
            else
            {
                var res = subjectGroupRepository.ListAllAsync().Result.Where(a => a.Name == ob.SubjectGroupName && a.ID != ob.SubjectGroupID && a.BoardStandardId == ob.BoardStandardId && a.IsAvailable == true && a.Active == true).FirstOrDefault();
                if (res == null)
                {
                    SubjectGroup subjectGroup = subjectGroupRepository.ListAllAsync().Result.Where(a => a.ID == ob.SubjectGroupID && a.IsAvailable == true && a.Active == true).FirstOrDefault();

                    subjectGroup.BoardStandardId = ob.BoardStandardId;
                    subjectGroup.ModifiedDate = DateTime.Now;
                    subjectGroup.ModifiedId = userid;
                    subjectGroup.Name = ob.SubjectGroupName;
                    subjectGroup.Status = "UPDATED";
                    subjectGroup.IsAvailable = true;
                    await subjectGroupRepository.UpdateAsync(subjectGroup);
                    var id = ob.SubjectGroupID;
                    var subjectgroup = subjectGroupDetailsRepository.ListAllAsync().Result.Where(a => a.SubjectGroupID == id && a.IsAvailable == true && a.Active == true).ToList();
                    if (ob.subjectids.Count() > 0)
                    {
                        foreach (var a in ob.subjectids)
                        {
                            var chk = subjectgroup.Where(m => m.SubjectID == a).FirstOrDefault();
                            if (chk == null)
                            {
                                SubjectGroupDetails details = new SubjectGroupDetails();
                                details.IsAvailable = true;
                                details.Active = true;
                                details.InsertedDate = DateTime.Now;
                                details.InsertedId = userid;
                                details.ModifiedDate = DateTime.Now;
                                details.ModifiedId = userid;
                                details.SubjectGroupID = id;
                                details.SubjectID = a;
                                details.Status = "CREATED";
                                await subjectGroupDetailsRepository.AddAsync(details);
                            }
                        }
                        var cst = subjectgroup.Where(a => !ob.subjectids.Contains(a.SubjectID)).ToList();
                        foreach (var a in cst)
                        {
                            SubjectGroupDetails details = a;
                            details.Active = false;
                            details.ModifiedDate = DateTime.Now;
                            details.ModifiedId = userid;
                            details.Status = "DELETED";
                            await subjectGroupDetailsRepository.UpdateAsync(details);
                        }
                    }
                    return 3; //Updated Successfully
                }
                else
                {
                    return 2;//Already Exists
                }
            }
        }
        public int RemoveSubjectGroup(long id, long userid)
        {
            SubjectGroup subjectGroup = subjectGroupRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            subjectGroupRepository.UpdateAsync(subjectGroup);
            return 1;
        }
        public int ActiveInActiveSubjectGroup(long id, long userid)
        {
            SubjectGroup subjectGroup = subjectGroupRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                subjectGroupRepository.UpdateAsync(subjectGroup);
                return 1;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                subjectGroupRepository.UpdateAsync(subjectGroup);
                return 2;
            }
        }
        #endregion
        #region Subject Group Details
        public List<commonclass> GetSubectsbyusingSchoolGroup(long id)
        {
            var subjectgroup = subjectGroupRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            var subjects = Getsubjects().Where(a => a.BoardStandardID == subjectgroup.BoardStandardId).ToList();
            var subjectgroupdetails = subjectGroupDetailsRepository.ListAllAsync().Result.Where(a => a.Active == true && a.SubjectGroupID == id).Select(a => a.SubjectID).ToList();
            var req = subjects.Select(a => a.ID).ToList().Except(subjectgroupdetails).ToList();
            var res = subjects.Where(a => req.Contains(a.ID)).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
            return res;
        }
        public List<subjectgroupdetailsclass> GetSubjectGroupDetailsList(long id)
        {
            try
            {
                var subjectgroupdetails = subjectGroupDetailsRepository.ListAllAsync().Result.Where(a => a.Active == true && a.SubjectGroupID == id).ToList();
                var subjects = Getsubjects();
                var allsubjects = subjectRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var boardstandards = boardStandardRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var boards = boardRepository.GetAllBoard();
                var standards = standardRepository.GetAllStandard();
                //var streams = streamRepository.GetAllStream();

                var res = (from a in subjectgroupdetails
                           join b in subjects on a.SubjectID equals b.ID
                           join f in allsubjects on a.SubjectID equals f.ID
                           join c in boardstandards on b.BoardStandardID equals c.ID
                           join d in boards on c.BoardId equals d.ID
                           join e in standards on c.StandardId equals e.ID
                           select new subjectgroupdetailsclass
                           {
                               BoardId = c.BoardId,
                               BoardName = d.Name,
                               BoardStandardId = b.BoardStandardID,
                               ID = a.ID,
                               StandardId = c.StandardId,
                               StandardName = e.Name,
                               SubjectCode = f.SubjectCode,
                               SubjectGroupID = a.SubjectGroupID,
                               SubjectID = a.SubjectID,
                               SubjectName = f.Name,
                               IsAvailable = a.IsAvailable,
                               inserteddate = a.ModifiedDate
                           }).OrderByDescending(a => a.ID).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public int SaveOrUpdateSubjectGroupDetails(long[] subjectids, long schoolgroupid, long userid)
        {
            if (subjectids.Count() > 0)
            {
                foreach (var m in subjectids)
                {
                    var res = subjectGroupDetailsRepository.ListAllAsync().Result.Where(a => a.SubjectID == m && a.SubjectGroupID == schoolgroupid && a.Active == true).FirstOrDefault();
                    if (res == null)
                    {
                        SubjectGroupDetails subjectGroup = new SubjectGroupDetails();
                        subjectGroup.Active = true;
                        subjectGroup.SubjectGroupID = schoolgroupid;
                        subjectGroup.SubjectID = m;
                        subjectGroup.InsertedDate = DateTime.Now;
                        subjectGroup.InsertedId = userid;
                        subjectGroup.ModifiedDate = DateTime.Now;
                        subjectGroup.ModifiedId = userid;
                        subjectGroup.Status = "CREATED";
                        subjectGroup.IsAvailable = true;
                        subjectGroupDetailsRepository.AddAsync(subjectGroup);
                        // return 1;
                    }
                    else
                    {
                        if (res.IsAvailable == true)
                        {
                            //   return 2;//Already Exists
                        }
                        else
                        {
                            SubjectGroupDetails subjectGroup = subjectGroupDetailsRepository.ListAllAsync().Result.Where(a => a.ID == res.ID && a.Active == true).FirstOrDefault();
                            subjectGroup.ModifiedDate = DateTime.Now;
                            subjectGroup.ModifiedId = userid;
                            subjectGroup.Status = "ACTIVE";
                            subjectGroup.IsAvailable = true;
                            subjectGroupDetailsRepository.UpdateAsync(subjectGroup);
                            //  return 1;
                        }
                    }
                }
                return 1;
            }
            else
            {
                return 0;//No File Selected
            }
        }
        public long RemoveSubjectGroupDetails(long id, long userid)
        {
            SubjectGroupDetails subjectGroup = subjectGroupDetailsRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            subjectGroupDetailsRepository.UpdateAsync(subjectGroup);
            return subjectGroup.SubjectGroupID;
        }
        public long ActiveInActiveSubjectGroupDetails(long id, long userid)
        {
            SubjectGroupDetails subjectGroup = subjectGroupDetailsRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                subjectGroupDetailsRepository.UpdateAsync(subjectGroup);
                return subjectGroup.SubjectGroupID;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                subjectGroupDetailsRepository.UpdateAsync(subjectGroup);
                return subjectGroup.SubjectGroupID;
            }
        }
        #endregion
        #region Academic Subject Group
        public List<academicsubjectgroupclass> GetAcademicSubjectGroupList()
        {
            try
            {
                var academicsession = sqlQuery.Get_view_All_Academic_Standards();
                var academicsubject = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var subjectgroups = sqlQuery.Get_View_All_Subject_Groups().ToList();
                var res = (from a in academicsubject
                           join b in academicsession on a.AcademicStandardID equals b.ID
                           join c in subjectgroups on a.SubjectGroupID equals c.SubjectGroupID
                           select new academicsubjectgroupclass
                           {
                               ID = a.ID,
                               IsAvailable = a.IsAvailable,
                               standards = b,
                               sessionID = b.ID,
                               subjectGroup = c,
                               insertedDate = a.InsertedDate
                           }).OrderByDescending(a => a.ID).ToList();
                return res;
            }
            catch (Exception e1)
            {
                throw;
            }
        }
        public List<commonclass> GetAcademicSession()
        {
            try
            {
                return academicSessionRepository.GetAllAcademicSession().Where(a => a.Active == true).Select(a => new commonclass { ID = a.ID, Name = a.DisplayName }).ToList();
            }
            catch (Exception e1)
            {

                throw;
            }
        }
        public List<long> Getsubjectgroupids(long id)
        {
            try
            {
                return academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.Active == true).Select(a => a.SubjectGroupID).ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public int SaveOrUpdateAcademicSubjectGroup(long AcademicStandardID, long[] subjectGroupID, long userid)
        {
            if (AcademicStandardID != 0)
            {
                if (subjectGroupID.Count() > 0)
                {
                    foreach (var sub in subjectGroupID)
                    {
                        var res = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.AcademicStandardID == AcademicStandardID && a.Active == true).FirstOrDefault();
                        if (res == null)
                        {
                            AcademicSubjectGroup subjectGroup = new AcademicSubjectGroup();
                            subjectGroup.Active = true;
                            subjectGroup.AcademicStandardID = AcademicStandardID;
                            subjectGroup.SubjectGroupID = sub;
                            subjectGroup.InsertedDate = DateTime.Now;
                            subjectGroup.InsertedId = userid;
                            subjectGroup.ModifiedDate = DateTime.Now;
                            subjectGroup.ModifiedId = userid;
                            subjectGroup.Status = "CREATED";
                            subjectGroup.IsAvailable = true;
                            academicSubjectGroupRepository.AddAsync(subjectGroup);

                        }
                        else
                        {
                            if (res.IsAvailable == true)
                            {
                                //   return 2;//Already Exists
                            }
                            else
                            {
                                AcademicSubjectGroup subjectGroup = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.ID == res.ID && a.Active == true).FirstOrDefault();
                                subjectGroup.ModifiedDate = DateTime.Now;
                                subjectGroup.ModifiedId = userid;
                                subjectGroup.Status = "ACTIVE";
                                subjectGroup.IsAvailable = true;
                                academicSubjectGroupRepository.UpdateAsync(subjectGroup);
                                //  return 1;
                            }
                        }
                    }
                    var smr = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.AcademicStandardID == AcademicStandardID && !subjectGroupID.Contains(a.SubjectGroupID) && a.Active == true).ToList();
                    foreach (var st in smr)
                    {
                        AcademicSubjectGroup subjectGroup = st;
                        subjectGroup.Active = false;
                        subjectGroup.ModifiedDate = DateTime.Now;
                        subjectGroup.ModifiedId = userid;
                        subjectGroup.Status = "DELETED";
                        academicSubjectGroupRepository.UpdateAsync(subjectGroup);
                    }
                    return 1;//saved
                }
                else
                {
                    return 2; //select subject group
                }
            }
            else
            {
                return 3;
            }
        }
        public int RemoveAcademicSubjectGroup(long id, long userid)
        {
            AcademicSubjectGroup subjectGroup = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            academicSubjectGroupRepository.UpdateAsync(subjectGroup);
            return 1;
        }
        public int ActiveInActiveAcademicSubjectGroup(long id, long userid)
        {
            AcademicSubjectGroup subjectGroup = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                academicSubjectGroupRepository.UpdateAsync(subjectGroup);
                return 1;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                academicSubjectGroupRepository.UpdateAsync(subjectGroup);
                return 2;
            }
        }
        #endregion
        #region Subject Optional Type
        public List<SubjectOptionalType> GetSubjectOptionalTypeList()
        {
            try
            {
                var subjectoptiontypes = subjectOptionalTypeRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                return subjectoptiontypes;
            }
            catch (Exception e1)
            {
                throw;
            }
        }

        public SubjectOptionalType GetSubjectoptionaltypecls(long? id)
        {
            try
            {
                SubjectOptionalType ob = new SubjectOptionalType();
                if (id != null)
                {
                    var subjectgrp = subjectOptionalTypeRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == id).FirstOrDefault();
                    ob.Name = subjectgrp.Name;
                    ob.ID = subjectgrp.ID;
                }
                else
                {
                    ob.Name = "";
                    ob.ID = 0;
                }
                return ob;
            }
            catch (Exception e1)
            {

                throw;
            }
        }

        public int SaveOrUpdateSubjectOptionalType(long? id, string Name, long userid)
        {
            if (id == 0)
            {
                var res = subjectOptionalTypeRepository.ListAllAsync().Result.Where(a => a.Name == Name && a.Active == true).FirstOrDefault();
                if (res == null)
                {
                    SubjectOptionalType subjectGroup = new SubjectOptionalType();
                    subjectGroup.Active = true;
                    subjectGroup.Name = Name;
                    subjectGroup.InsertedDate = DateTime.Now;
                    subjectGroup.InsertedId = userid;
                    subjectGroup.ModifiedDate = DateTime.Now;
                    subjectGroup.ModifiedId = userid;
                    subjectGroup.Status = "CREATED";
                    subjectGroup.IsAvailable = true;
                    subjectOptionalTypeRepository.AddAsync(subjectGroup);

                    return 1;
                }
                else
                {
                    return 2;//Already Exists
                }
            }
            else
            {
                var res = subjectOptionalTypeRepository.ListAllAsync().Result.Where(a => a.ID != id && a.Name == Name && a.Active == true).FirstOrDefault();
                if (res == null)
                {
                    SubjectOptionalType subjectGroup = subjectOptionalTypeRepository.ListAllAsync().Result.Where(a => a.ID == id.Value && a.Active == true).FirstOrDefault();

                    subjectGroup.Name = Name;
                    subjectGroup.ModifiedDate = DateTime.Now;
                    subjectGroup.ModifiedId = userid;
                    subjectGroup.Status = "UPDATED";
                    subjectGroup.IsAvailable = true;
                    subjectOptionalTypeRepository.UpdateAsync(subjectGroup);

                    return 3; //Updated Successfully
                }
                else
                {
                    return 2;//Already Exists
                }
            }
        }
        public int RemoveSubjectOptionalType(long id, long userid)
        {
            SubjectOptionalType subjectGroup = subjectOptionalTypeRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            subjectOptionalTypeRepository.UpdateAsync(subjectGroup);
            return 1;
        }
        public int ActiveInActiveSubjectOptionalType(long id, long userid)
        {
            SubjectOptionalType subjectGroup = subjectOptionalTypeRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                subjectOptionalTypeRepository.UpdateAsync(subjectGroup);
                return 1;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                subjectOptionalTypeRepository.UpdateAsync(subjectGroup);
                return 2;
            }
        }
        #endregion
        #region Subject Optional Category
        public List<View_All_Subject_Optional_Category> GetSubjectOptionalCategoryList()
        {
            try
            {
                var SubjectOptionalCategorys = sqlQuery.Get_All_Subject_Optional_Categories().ToList();
                return SubjectOptionalCategorys;
            }
            catch (Exception e1)
            {
                throw;
            }
        }

        public subjectoptionalcategorycls GetSubjectOptionalCategorycls(long? id)
        {
            try
            {
                subjectoptionalcategorycls ob = new subjectoptionalcategorycls();
                ob.boards = boardRepository.GetAllBoard().Where(a => a.Active == true).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
                ob.types = subjectOptionalTypeRepository.ListAllAsync().Result.Where(a => a.Active == true).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();

                if (id != null)
                {
                    var subjectgrp = sqlQuery.Get_All_Subject_Optional_Categories().Where(a => a.SubjectOptionalCategoryID == id).FirstOrDefault();
                    ob.BoardID = subjectgrp.BoardId;
                    ob.BoardStandardId = subjectgrp.BoardStandardID;
                    ob.SubjectGroupID = subjectgrp.SubjectGroupID;
                    var subjectoptionaldet = subjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true && a.SubjectOptionalCategoryID == id).Select(a => a.SubjectID.Value).ToArray();
                    ob.subjectids = subjectoptionaldet;
                    ob.SubjectOptionalCategoryID = subjectgrp.SubjectOptionalCategoryID;
                    ob.SubjectOptionalTypeID = subjectgrp.SubjectOptionalTypeID;
                    ob.WingID = subjectgrp.WingID;
                }
                else
                {
                    ob.BoardID = 0;
                    ob.BoardStandardId = 0;
                    ob.SubjectGroupID = 0;
                    ob.SubjectOptionalCategoryID = 0;
                    ob.SubjectOptionalTypeID = 0;
                    ob.WingID = 0;

                    // ob.SubjectGroupName = "";
                    ob.subjectids = null;
                }
                return ob;
            }
            catch (Exception e1)
            {

                throw;
            }
        }
        public List<commonclass> GetWings(long id)
        {
            return boardStandardWingRepository.ListAllAsync().Result.Join(wingRepository.ListAllAsync().Result.ToList(), a => a.WingID, b => b.ID, (a, b) => new { s = a, t = b }).Where(a => a.s.BoardStandardID == id && a.s.Active == true && a.t.Active == true).Select(a => new commonclass { ID = a.s.ID, Name = a.t.Name }).ToList();
        }
        public List<commonclass> GetSchoolGroup(long id)
        {
            return subjectGroupRepository.ListAllAsync().Result.Where(a => a.BoardStandardId == id && a.Active == true && a.IsAvailable == true).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
        }
        public async Task<int> SaveOrUpdateSubjectOptionalCategory(subjectoptionalcategorycls ob, long userid)
        {
            if (ob.SubjectOptionalCategoryID == 0)
            {

                SubjectOptionalCategory subjectGroup = new SubjectOptionalCategory();
                subjectGroup.Active = true;
                subjectGroup.BoardStandardID = ob.BoardStandardId;
                subjectGroup.SubjectGroupID = ob.SubjectGroupID;
                subjectGroup.SubjectOptionalTypeID = ob.SubjectOptionalTypeID;
                subjectGroup.WingID = ob.WingID;
                subjectGroup.InsertedDate = DateTime.Now;
                subjectGroup.InsertedId = userid;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "CREATED";
                subjectGroup.IsAvailable = true;

                var id = subjectOptionalCategoryRepository.AddAsync(subjectGroup).Result.ID;
                if (ob.subjectids.Count() > 0)
                {
                    foreach (var st in ob.subjectids)
                    {
                        SubjectOptional subject = new SubjectOptional();
                        subject.Active = true;
                        subject.InsertedDate = DateTime.Now;
                        subject.ModifiedDate = DateTime.Now;
                        subject.InsertedId = userid;
                        subject.IsAvailable = true;
                        subject.ModifiedId = userid;
                        subject.SubjectID = st;
                        subject.SubjectOptionalCategoryID = id;
                        subject.Status = "CREATED";
                        await subjectOptionalRepository.AddAsync(subject);
                    }
                }
                return 1;

            }
            else
            {
                SubjectOptionalCategory subjectGroup = subjectOptionalCategoryRepository.ListAllAsync().Result.Where(a => a.ID == ob.SubjectOptionalCategoryID && a.Active == true && a.IsAvailable == true).FirstOrDefault();

                subjectGroup.BoardStandardID = ob.BoardStandardId;
                subjectGroup.SubjectGroupID = ob.SubjectGroupID;
                subjectGroup.SubjectOptionalTypeID = ob.SubjectOptionalTypeID;
                subjectGroup.WingID = ob.WingID;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "UPDATED";
                subjectGroup.IsAvailable = true;
                await subjectOptionalCategoryRepository.UpdateAsync(subjectGroup);
                var id = ob.SubjectOptionalCategoryID;
                if (ob.subjectids.Count() > 0)
                {
                    foreach (var st in ob.subjectids)
                    {
                        SubjectOptional subject = subjectOptionalRepository.ListAllAsync().Result.Where(a => a.SubjectOptionalCategoryID == id && a.SubjectID == st && a.Active == true && a.IsAvailable == true).FirstOrDefault();
                        if (subject == null)
                        {
                            subject = new SubjectOptional();
                            subject.Active = true;
                            subject.InsertedDate = DateTime.Now;
                            subject.ModifiedDate = DateTime.Now;
                            subject.InsertedId = userid;
                            subject.ModifiedId = userid;
                            subject.SubjectID = st;
                            subject.IsAvailable = true;
                            subject.SubjectOptionalCategoryID = id;
                            subject.Status = "CREATED";
                            await subjectOptionalRepository.AddAsync(subject);
                        }
                    }
                }
                return 2;
            }
        }
        public int RemoveSubjectOptionalCategory(long id, long userid)
        {
            SubjectOptionalCategory subjectGroup = subjectOptionalCategoryRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            subjectOptionalCategoryRepository.UpdateAsync(subjectGroup);
            return 1;
        }
        public int ActiveInActiveSubjectOptionalCategory(long id, long userid)
        {
            SubjectOptionalCategory subjectGroup = subjectOptionalCategoryRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                subjectOptionalCategoryRepository.UpdateAsync(subjectGroup);
                return 1;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                subjectOptionalCategoryRepository.UpdateAsync(subjectGroup);
                return 2;
            }
        }
        #endregion
        #region Subject Optional
        public List<commonclass> GetSubectsbyusingSchoolOptionList(long id)
        {
            var subjectgroup = subjectOptionalCategoryRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            var subjects = Getsubjects().Where(a => a.BoardStandardID == subjectgroup.BoardStandardID).ToList();
            var subjectgroupdetails = subjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true && a.SubjectOptionalCategoryID == id).Select(a => a.SubjectID.Value).ToList();
            var req = subjects.Select(a => a.ID).ToList().Except(subjectgroupdetails).ToList();
            var res = subjects.Where(a => req.Contains(a.ID)).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
            return res;
        }
        public List<subjectgroupdetailsclass> GetSubjectOptionalList(long id)
        {
            try
            {
                var SubjectOptionalCategorys = sqlQuery.Get_All_Subject_Optional_Categories().Where(a => a.SubjectOptionalCategoryID == id).ToList();
                var subjects = Getsubjects();
                var subjectoptional = subjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var allsubjects = subjectRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var res = (from a in subjectoptional
                           join b in subjects on a.SubjectID equals b.ID
                           join c in allsubjects on a.SubjectID equals c.ID
                           join f in SubjectOptionalCategorys on a.SubjectOptionalCategoryID equals f.SubjectOptionalCategoryID
                           select new subjectgroupdetailsclass
                           {
                               BoardId = f.BoardId,
                               BoardName = f.BoardName,
                               BoardStandardId = f.BoardStandardID,
                               ID = a.ID,
                               StandardId = f.StandardId,
                               StandardName = f.StandardName,
                               SubjectCode = c.SubjectCode,
                               SubjectGroupID = f.SubjectGroupID,
                               SubjectID = a.SubjectID.Value,
                               SubjectName = c.Name,
                               IsAvailable = a.IsAvailable,
                               SubjectOptionalCategoryID = a.SubjectOptionalCategoryID
                           }).OrderByDescending(a => a.ID).ToList();
                return res;
            }
            catch
            {
                return null;
            }
        }
        public async Task<int> SaveOrUpdateSubjectOptionalDetails(long[] subjectids, long SubjectOptionalCategoryID, long userid)
        {
            if (subjectids.Count() > 0)
            {
                foreach (var m in subjectids)
                {
                    var res = subjectOptionalRepository.ListAllAsync().Result.Where(a => a.SubjectID == m && a.SubjectOptionalCategoryID == SubjectOptionalCategoryID && a.Active == true).FirstOrDefault();
                    if (res == null)
                    {
                        SubjectOptional subjectGroup = new SubjectOptional();
                        subjectGroup.Active = true;
                        subjectGroup.SubjectOptionalCategoryID = SubjectOptionalCategoryID;
                        subjectGroup.SubjectID = m;
                        subjectGroup.InsertedDate = DateTime.Now;
                        subjectGroup.InsertedId = userid;
                        subjectGroup.ModifiedDate = DateTime.Now;
                        subjectGroup.ModifiedId = userid;
                        subjectGroup.Status = "CREATED";
                        subjectGroup.IsAvailable = true;
                        subjectOptionalRepository.AddAsync(subjectGroup);
                        // return 1;
                    }
                    else
                    {
                        if (res.IsAvailable == true)
                        {
                            //   return 2;//Already Exists
                        }
                        else
                        {
                            SubjectOptional subjectGroup = subjectOptionalRepository.ListAllAsync().Result.Where(a => a.ID == res.ID && a.Active == true).FirstOrDefault();
                            subjectGroup.ModifiedDate = DateTime.Now;
                            subjectGroup.ModifiedId = userid;
                            subjectGroup.Status = "ACTIVE";
                            subjectGroup.IsAvailable = true;
                            subjectOptionalRepository.UpdateAsync(subjectGroup);
                            //  return 1;
                        }
                    }
                }
                return 1;
            }
            else
            {
                return 0;//No File Selected
            }
        }
        public int RemoveSubjectOptionalDetails(long id, long userid)
        {
            SubjectOptional subjectGroup = subjectOptionalRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            subjectOptionalRepository.UpdateAsync(subjectGroup);
            return 1;
        }
        public int ActiveInActiveSubjectOptionalDetails(long id, long userid)
        {
            SubjectOptional subjectGroup = subjectOptionalRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                subjectOptionalRepository.UpdateAsync(subjectGroup);
                return 1;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                subjectOptionalRepository.UpdateAsync(subjectGroup);
                return 2;
            }
        }
        #endregion
        #region Academic Subject Optional
        public List<academicsubjectgroupclass> GetAcademicSubjectOptionalList()
        {
            try
            {
                var academicsession = sqlQuery.Get_view_All_Academic_Standards();
                var academicsubject = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var subjectoptional = subjectOptionalRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var subjectgroups = sqlQuery.Get_All_Subject_Optional_Categories().ToList();
                var res = (from a in academicsubject
                           join b in academicsession on a.AcademicStandardID equals b.ID
                           // join c in subjectoptional on a.SubjectOptionalID equals c.ID
                           join d in subjectgroups on a.SubjectOptionalCategoryID equals d.SubjectOptionalCategoryID
                           select new academicsubjectgroupclass
                           {
                               ID = a.ID,
                               IsAvailable = a.IsAvailable,
                               standards = b,
                               sessionID = b.ID,
                               category = d,
                               subjectoptionalID = a.SubjectOptionalCategoryID.Value,
                               insertedDate = a.InsertedDate
                           }).OrderByDescending(a => a.ID).ToList();
                return res;
            }
            catch (Exception e1)
            {
                throw;
            }
        }

        public async Task<int> SaveOrUpdateAcademicSubjectOptional(long AcademicStandardID, long[] subjectOptionalID, long userid)
        {
            if (AcademicStandardID != 0)
            {
                if (subjectOptionalID.Count() > 0)
                {
                    foreach (var sub in subjectOptionalID)
                    {
                        var res = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.AcademicStandardID == AcademicStandardID && a.SubjectOptionalCategoryID == sub && a.Active == true).FirstOrDefault();
                        if (res == null)
                        {
                            AcademicSubjectOptional subjectGroup = new AcademicSubjectOptional();
                            subjectGroup.Active = true;
                            subjectGroup.AcademicStandardID = AcademicStandardID;
                            subjectGroup.SubjectOptionalCategoryID = sub;
                            subjectGroup.InsertedDate = DateTime.Now;
                            subjectGroup.InsertedId = userid;
                            subjectGroup.ModifiedDate = DateTime.Now;
                            subjectGroup.ModifiedId = userid;
                            subjectGroup.Status = "CREATED";
                            subjectGroup.IsAvailable = true;
                            await academicSubjectOptionalRepository.AddAsync(subjectGroup);

                        }
                        else
                        {
                            if (res.IsAvailable == true)
                            {
                                //   return 2;//Already Exists
                            }
                            else
                            {
                                AcademicSubjectOptional subjectGroup = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.ID == res.ID && a.Active == true).FirstOrDefault();
                                subjectGroup.ModifiedDate = DateTime.Now;
                                subjectGroup.ModifiedId = userid;
                                subjectGroup.Status = "ACTIVE";
                                subjectGroup.IsAvailable = true;
                                await academicSubjectOptionalRepository.UpdateAsync(subjectGroup);
                                //  return 1;
                            }
                        }
                    }
                    var smr = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.AcademicStandardID == AcademicStandardID && !subjectOptionalID.Contains(a.SubjectOptionalCategoryID.Value) && a.Active == true).ToList();
                    foreach (var st in smr)
                    {
                        AcademicSubjectOptional subjectGroup = st;
                        subjectGroup.Active = false;
                        subjectGroup.ModifiedDate = DateTime.Now;
                        subjectGroup.ModifiedId = userid;
                        subjectGroup.Status = "DELETED";
                        await academicSubjectOptionalRepository.UpdateAsync(subjectGroup);
                    }
                    return 1;//saved
                }
                else
                {
                    return 2; //select subject group
                }
            }
            else
            {
                return 3;
            }
        }
        public int RemoveAcademicSubjectOptional(long id, long userid)
        {
            AcademicSubjectOptional subjectGroup = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            academicSubjectOptionalRepository.UpdateAsync(subjectGroup);
            return 1;
        }
        public int ActiveInActiveAcademicSubjectOptional(long id, long userid)
        {
            AcademicSubjectOptional subjectGroup = academicSubjectOptionalRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                academicSubjectOptionalRepository.UpdateAsync(subjectGroup);
                return 1;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                academicSubjectOptionalRepository.UpdateAsync(subjectGroup);
                return 2;
            }
        }
        #endregion
        public List<AcademicStandardWingcls> GetacademicstandardWings(long AcademicStandardId)
        {
            var academicstandardwings = academicStandardWingRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStandardId == AcademicStandardId).ToList();
            var boardstandardwings = boardStandardWingRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var wings = wingRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var res = (from a in academicstandardwings
                       join b in boardstandardwings on a.BoardStandardWingID equals b.ID
                       join c in wings on b.WingID equals c.ID
                       select new AcademicStandardWingcls
                       {
                           AcademicStandardID = a.AcademicStandardId,
                           BoardStandardID = b.BoardStandardID,
                           BoardStandardWingId = a.BoardStandardWingID,
                           ID = a.ID,
                           ModifiedDate = a.ModifiedDate,
                           Name = c.Name
                       }).ToList();
            return res;
        }
        public List<View_All_Academic_Standard> GetAcademicStandard()
        {
            var res = sqlQuery.Get_view_All_Academic_Standards();
            return res;
        }
        public IEnumerable<AcademicSectionViewModel> GetAllAcademicSection()
        {
            var AvademicSectionList = academicSectionRepository.ListAllAsyncIncludeAll().Result;
            var SectionList = sectionRepository.GetAllSection();
            if (AvademicSectionList != null)
            {
                var res = (from a in AvademicSectionList
                           join b in SectionList on a.SectionId equals b.ID
                           select new AcademicSectionViewModel
                           {
                               ID = a.ID,
                               Status = a.Status,
                               SectionId = a.SectionId,
                               AcademicStandardId = a.AcademicStandardId,
                               //AcademicStandardStreamId = a.AcademicStandardStreamId,
                               SessionName = b.Name,
                               ModifiedDate = a.ModifiedDate,
                           }

                           ).ToList();
                return res;
            }
            else
            {
                return null;
            }
        }
        public List<CommonMasterModel> GetAcademicSubjectGroup()
        {
            var academicsubjectgroup = academicSubjectGroupRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var subjectgroups = GetSubjectGroupList();

            var res = (from a in academicsubjectgroup
                       join b in subjectgroups on a.SubjectGroupID equals b.SubjectGroupID
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = b.SubjectGroupName,
                           GroupID=a.AcademicStandardID
                       }).ToList();
            return res;
        }
        #region Vehicle Type
        public List<VehicleType> GetVehicleTypeList()
        {
            try
            {
                var vehicletypes = vehicleTypeRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                return vehicletypes;
            }
            catch (Exception e1)
            {
                throw;
            }
        }

        public VehicleType GetVehicleTypeById(long? id)
        {
            try
            {
                VehicleType ob = new VehicleType();
                if (id != null)
                {
                    var vehicle = vehicleTypeRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == id).FirstOrDefault();
                    ob.Name = vehicle.Name;
                    ob.ID = vehicle.ID;
                }
                else
                {
                    ob.Name = "";
                    ob.ID = 0;
                }
                return ob;
            }
            catch (Exception e1)
            {

                throw;
            }
        }

        public int SaveOrUpdatevehicleType(long? id, string Name, long userid)
        {
            if (id == 0)
            {
                var res = vehicleTypeRepository.ListAllAsync().Result.Where(a => a.Name == Name && a.Active == true).FirstOrDefault();
                if (res == null)
                {
                    VehicleType vehicle = new VehicleType();
                    vehicle.Active = true;
                    vehicle.Name = Name;
                    vehicle.InsertedDate = DateTime.Now;
                    vehicle.InsertedId = userid;
                    vehicle.ModifiedDate = DateTime.Now;
                    vehicle.ModifiedId = userid;
                    vehicle.Status = "CREATED";
                    vehicle.IsAvailable = true;
                    vehicleTypeRepository.AddAsync(vehicle);

                    return 1;
                }
                else
                {
                    return 2;//Already Exists
                }
            }
            else
            {
                var res = vehicleTypeRepository.ListAllAsync().Result.Where(a => a.ID != id && a.Name == Name && a.Active == true).FirstOrDefault();
                if (res == null)
                {
                    VehicleType vehicle = vehicleTypeRepository.ListAllAsync().Result.Where(a => a.ID == id.Value && a.Active == true).FirstOrDefault();

                    vehicle.Name = Name;
                    vehicle.ModifiedDate = DateTime.Now;
                    vehicle.ModifiedId = userid;
                    vehicle.Status = "UPDATED";
                    vehicle.IsAvailable = true;
                    vehicleTypeRepository.UpdateAsync(vehicle);

                    return 3; //Updated Successfully
                }
                else
                {
                    return 2;//Already Exists
                }
            }
        }
        public int RemovevehicleType(long id, long userid)
        {
            VehicleType subjectGroup = vehicleTypeRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            vehicleTypeRepository.UpdateAsync(subjectGroup);
            return 1;
        }
        public int ActiveInActivevehicleType(long id, long userid)
        {
            VehicleType subjectGroup = vehicleTypeRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                vehicleTypeRepository.UpdateAsync(subjectGroup);
                return 1;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                vehicleTypeRepository.UpdateAsync(subjectGroup);
                return 2;
            }
        }
        #endregion
        #region Vehicle
        public vehiclelistdetcls GetVehicleList()
        {
            try
            {
                vehiclelistdetcls obj = new vehiclelistdetcls();
                var vehicle = vehicleRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var vehiclelocation = vehicleLocationRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var vehicletypes = vehicleTypeRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var organisations = organizationRepository.GetAllOrganization().Where(a => a.Active == true).ToList();
                var groups = groupRepository.GetAllGroup().Where(a => a.Active == true).ToList();
                var res = (from a in vehicle
                           join b in vehicletypes on a.VehicleTypeID equals b.ID
                           join c in organisations on a.OrganisationID equals c.ID
                           join d in groups on a.GroupID equals d.ID
                           select new vehicledetcls
                           {
                               GroupID = a.GroupID,
                               GroupName = d.Name,
                               ID = a.ID,
                               insertedDate = a.InsertedDate,
                               Number = a.Number,
                               OrganisationID = a.OrganisationID,
                               OrganisationName = c.Name,
                               VehicleTypeID = a.VehicleTypeID,
                               VehicleTypeName = b.Name,
                               Countlocation = vehiclelocation.Where(m => m.VehicleID == a.ID).ToList().Count()
                           }).ToList();
                obj.vehicledetcls = res;
                obj.GetVehicledetcls = GetVehicleById(null);
                return obj;
            }
            catch (Exception e1)
            {
                throw;
            }
        }
        public List<commonclass> GetOrganisationByGroupId(long id)
        {
            var organisation = organizationRepository.GetAllOrganization().Where(a => a.Active == true && a.GroupID == id).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
            return organisation;
        }
        public vehicledetcls GetVehicleById(long? id)
        {
            try
            {
                vehicledetcls ob = new vehicledetcls();
                ob.groups = groupRepository.GetAllGroup().Where(a => a.Active == true).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
                ob.vehicletypes = vehicleTypeRepository.ListAllAsync().Result.Where(a => a.Active == true).Select(a => new commonclass { ID = a.ID, Name = a.Name }).ToList();
                if (id != null)
                {
                    var vehicle = vehicleRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == id).FirstOrDefault();
                    ob.GroupID = vehicle.GroupID;
                    ob.ID = vehicle.ID;
                    ob.Number = vehicle.Number;
                    ob.OrganisationID = vehicle.OrganisationID;
                    ob.GroupID = vehicle.GroupID;
                    ob.VehicleTypeID = vehicle.VehicleTypeID;

                }
                else
                {
                    ob.GroupID = 0;
                    ob.ID = 0;
                    ob.Number = "";
                    ob.OrganisationID = 0;
                    ob.GroupID = 0;
                    ob.VehicleTypeID = 0;
                }
                return ob;
            }
            catch (Exception e1)
            {

                throw;
            }
        }

        public async Task<int> SaveOrUpdatevehicle(vehicledetcls ob, long userid)
        {

            if (ob.ID == 0)
            {
                Vehicle vehicle = new Vehicle();
                vehicle.Active = true;
                vehicle.Number = ob.Number;
                vehicle.OrganisationID = ob.OrganisationID;
                vehicle.GroupID = ob.GroupID;
                vehicle.VehicleTypeID = ob.VehicleTypeID;
                vehicle.InsertedDate = DateTime.Now;
                vehicle.InsertedId = userid;
                vehicle.ModifiedDate = DateTime.Now;
                vehicle.ModifiedId = userid;
                vehicle.Status = "CREATED";
                vehicle.IsAvailable = true;
                await vehicleRepository.AddAsync(vehicle);

                return 1;

            }
            else
            {

                Vehicle vehicle = vehicleRepository.ListAllAsync().Result.Where(a => a.ID == ob.ID && a.Active == true).FirstOrDefault();

                vehicle.Number = ob.Number;
                vehicle.OrganisationID = ob.OrganisationID;
                vehicle.GroupID = ob.GroupID;
                vehicle.VehicleTypeID = ob.VehicleTypeID;
                vehicle.ModifiedDate = DateTime.Now;
                vehicle.ModifiedId = userid;
                vehicle.Status = "UPDATED";
                vehicle.IsAvailable = true;
                await vehicleRepository.UpdateAsync(vehicle);

                return 3; //Updated Successfully

            }
        }
        public int Removevehicle(long id, long userid)
        {
            Vehicle subjectGroup = vehicleRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            vehicleRepository.UpdateAsync(subjectGroup);
            return 1;
        }
        public int ActiveInActivevehicle(long id, long userid)
        {
            Vehicle subjectGroup = vehicleRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                vehicleRepository.UpdateAsync(subjectGroup);
                return 1;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                vehicleRepository.UpdateAsync(subjectGroup);
                return 2;
            }
        }
        #endregion
        #region Vehicle Location
        public vehiclelocationdetcls GetVehicleLocationList(long id)
        {
            try
            {
                vehiclelocationdetcls obj = new vehiclelocationdetcls();
                var vehiclelocation = vehicleLocationRepository.ListAllAsync().Result.Where(a => a.Active == true && a.VehicleID == id).ToList();
                var vehicletiming = departureTimeRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var city = cityRepository.GetAllCity().Where(a => a.Active == true).ToList();
                var states = stateRepository.GetAllState().Where(a => a.Active == true).ToList();
                var country = countryRepository.GetAllCountries().Where(a => a.Active == true).ToList();
                var vehicle = GetVehicleList().vehicledetcls;
                var res = (from a in vehiclelocation
                           join b in city on a.CityID equals b.ID
                           join c in states on b.StateID equals c.ID
                           join d in country on c.CountryID equals d.ID
                           join e in vehicle on a.VehicleID equals e.ID
                           select new vehiclelocationcls
                           {
                               CityID = a.CityID,
                               CityName = b.Name,
                               CountryID = c.CountryID,
                               CountryName = d.Name,
                               ID = a.ID,
                               ModifiedDate = a.ModifiedDate,
                               Name = a.Name,
                               StateID = b.StateID,
                               StateName = c.Name,
                               vehicle = e,
                               Counttimings = vehicletiming.Where(m => m.LocationID == a.ID).ToList().Count()
                           }).ToList();
                obj.vehiclelocation = res;
                obj.VehicleID = id;
                obj.countries = country.Select(a => new commonclass { Name = a.Name, ID = a.ID }).ToList();
                return obj;
            }
            catch (Exception e1)
            {
                throw;
            }
        }
        public vehiclelocationdetcls GetAllVehicleLocationList()
        {
            try
            {
                vehiclelocationdetcls obj = new vehiclelocationdetcls();
                var vehiclelocation = vehicleLocationRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var vehicletiming = departureTimeRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                var city = cityRepository.GetAllCity().Where(a => a.Active == true).ToList();
                var states = stateRepository.GetAllState().Where(a => a.Active == true).ToList();
                var country = countryRepository.GetAllCountries().Where(a => a.Active == true).ToList();
                var vehicle = GetVehicleList().vehicledetcls;
                var res = (from a in vehiclelocation
                           join b in city on a.CityID equals b.ID
                           join c in states on b.StateID equals c.ID
                           join d in country on c.CountryID equals d.ID
                           join e in vehicle on a.VehicleID equals e.ID
                           select new vehiclelocationcls
                           {
                               CityID = a.CityID,
                               CityName = b.Name,
                               CountryID = c.CountryID,
                               CountryName = d.Name,
                               ID = a.ID,
                               ModifiedDate = a.ModifiedDate,
                               Name = a.Name,
                               StateID = b.StateID,
                               StateName = c.Name,
                               vehicle = e,
                               Counttimings = vehicletiming.Where(m => m.LocationID == a.ID).ToList().Count()
                           }).ToList();
                obj.vehiclelocation = res;
                obj.VehicleID = 0;
                obj.countries = country.Select(a => new commonclass { Name = a.Name, ID = a.ID }).ToList();
                return obj;
            }
            catch (Exception e1)
            {
                throw;
            }
        }


        public async Task<int> SaveOrUpdatevehicleLocation(vehiclelocationcls ob, long userid)
        {

            if (ob.ID == 0)
            {
                VehicleLocation vehicle = new VehicleLocation();
                vehicle.Active = true;
                vehicle.Name = ob.Name;
                vehicle.CityID = ob.CityID;
                vehicle.VehicleID = ob.VehicleID;
                vehicle.InsertedDate = DateTime.Now;
                vehicle.InsertedId = userid;
                vehicle.ModifiedDate = DateTime.Now;
                vehicle.ModifiedId = userid;
                vehicle.Status = "CREATED";
                vehicle.IsAvailable = true;
                await vehicleLocationRepository.AddAsync(vehicle);

                return 1;

            }
            else
            {

                VehicleLocation vehicle = vehicleLocationRepository.ListAllAsync().Result.Where(a => a.ID == ob.ID && a.Active == true).FirstOrDefault();

                vehicle.Name = ob.Name;
                vehicle.CityID = ob.CityID;
                vehicle.VehicleID = ob.VehicleID;
                vehicle.ModifiedDate = DateTime.Now;
                vehicle.ModifiedId = userid;
                vehicle.Status = "UPDATED";
                vehicle.IsAvailable = true;
                await vehicleLocationRepository.UpdateAsync(vehicle);

                return 3; //Updated Successfully

            }
        }
        public int RemovevehicleLocation(long id, long userid)
        {
            VehicleLocation subjectGroup = vehicleLocationRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            vehicleLocationRepository.UpdateAsync(subjectGroup);
            return 1;
        }
        public int ActiveInActivevehicleLocation(long id, long userid)
        {
            VehicleLocation subjectGroup = vehicleLocationRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                vehicleLocationRepository.UpdateAsync(subjectGroup);
                return 1;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                vehicleLocationRepository.UpdateAsync(subjectGroup);
                return 2;
            }
        }
        #endregion
        #region Departure Timing
        public DepartureTimeDetcls GetDepartureTimesList(long id)
        {
            try
            {
                DepartureTimeDetcls obj = new DepartureTimeDetcls();
                var vehicletiming = departureTimeRepository.ListAllAsync().Result.Where(a => a.Active == true && a.LocationID == id).ToList();


                obj.timings = vehicletiming;
                obj.VehicleLocationID = id;
                return obj;
            }
            catch (Exception e1)
            {
                throw;
            }
        }
        public DepartureTimeDetcls GetAllDepartureTimesList()
        {
            try
            {
                DepartureTimeDetcls obj = new DepartureTimeDetcls();
                var vehicletiming = departureTimeRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();


                obj.timings = vehicletiming;
                obj.VehicleLocationID = 0;
                return obj;
            }
            catch (Exception e1)
            {
                throw;
            }
        }

        public async Task<int> SaveOrUpdateDepartureTimes(DepartureTime ob, long userid)
        {

            if (ob.ID == 0)
            {
                DepartureTime vehicle = new DepartureTime();
                vehicle.Active = true;
                vehicle.Name = ob.Name;
                vehicle.DropTime = ob.DropTime;
                vehicle.PickupTime = ob.PickupTime;
                vehicle.LocationID = ob.LocationID;
                vehicle.InsertedDate = DateTime.Now;
                vehicle.InsertedId = userid;
                vehicle.ModifiedDate = DateTime.Now;
                vehicle.ModifiedId = userid;
                vehicle.Status = "CREATED";
                vehicle.IsAvailable = true;
                await departureTimeRepository.AddAsync(vehicle);

                return 1;

            }
            else
            {

                DepartureTime vehicle = departureTimeRepository.ListAllAsync().Result.Where(a => a.ID == ob.ID && a.Active == true).FirstOrDefault();

                vehicle.Name = ob.Name;
                vehicle.DropTime = ob.DropTime;
                vehicle.PickupTime = ob.PickupTime;
                vehicle.LocationID = ob.LocationID;
                vehicle.ModifiedDate = DateTime.Now;
                vehicle.ModifiedId = userid;
                vehicle.Status = "UPDATED";
                vehicle.IsAvailable = true;
                await departureTimeRepository.UpdateAsync(vehicle);

                return 3; //Updated Successfully

            }
        }
        public int RemoveDepartureTimes(long id, long userid)
        {
            DepartureTime subjectGroup = departureTimeRepository.ListAllAsync().Result.Where(a => a.ID == id).FirstOrDefault();
            subjectGroup.Active = false;
            subjectGroup.ModifiedDate = DateTime.Now;
            subjectGroup.ModifiedId = userid;
            subjectGroup.Status = "DELETED";
            departureTimeRepository.UpdateAsync(subjectGroup);
            return 1;
        }
        public int ActiveInActiveDepartureTimes(long id, long userid)
        {
            DepartureTime subjectGroup = departureTimeRepository.ListAllAsync().Result.Where(a => a.ID == id && a.Active == true).FirstOrDefault();
            if (subjectGroup.IsAvailable == true)
            {
                subjectGroup.IsAvailable = false;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "INACTIVE";
                departureTimeRepository.UpdateAsync(subjectGroup);
                return 1;
            }
            else
            {
                subjectGroup.IsAvailable = true;
                subjectGroup.ModifiedDate = DateTime.Now;
                subjectGroup.ModifiedId = userid;
                subjectGroup.Status = "ACTIVE";
                departureTimeRepository.UpdateAsync(subjectGroup);
                return 2;
            }
        }
        #endregion
        #region Student Emergency Contact
        public List<StudentEmergencyContact> GetStudentEmergencyContacts(long studentid)
        {
            try
            {
                var emergency = studentEmergencyContactRepository.ListAllAsync().Result.Where(a => a.Active == true && a.StudentID == studentid).ToList();
                return emergency;
            }
            catch (Exception e1)
            {

                throw;
            }
        }
        public StudentEmergencyContact CreateorEditStudentEmergencyContacts(long? id, long studentid)
        {
            var emergency = studentEmergencyContactRepository.ListAllAsync().Result.Where(a => a.Active == true && a.StudentID == studentid).FirstOrDefault();
            if (emergency != null)
            {
                return emergency;
            }
            else
            {
                StudentEmergencyContact ob = new StudentEmergencyContact();
                ob.ContactPersonName = "";
                ob.EmailId = "";
                ob.ID = 0;
                ob.Mobile = "";
                ob.StudentID = studentid;
                return ob;
            }
        }
        public async Task<bool> SaveoreditStudentEmergencyContact(StudentEmergencyContact ob, long userid)
        {
            try
            {
                StudentEmergencyContact emergency = studentEmergencyContactRepository.ListAllAsync().Result.Where(a => a.Active == true && a.StudentID == ob.StudentID).FirstOrDefault();
                if (emergency != null)
                {
                    emergency.Active = true;
                    emergency.ContactPersonName = ob.ContactPersonName;
                    emergency.EmailId = ob.EmailId;
                    emergency.IsAvailable = true;
                    emergency.Mobile = ob.Mobile;
                    emergency.ModifiedDate = DateTime.Now;
                    emergency.ModifiedId = userid;
                    emergency.Status = "ACTIVE";
                    emergency.StudentID = ob.StudentID;
                   await studentEmergencyContactRepository.UpdateAsync(emergency);

                }
                else
                {
                    emergency = new StudentEmergencyContact();
                    emergency.Active = true;
                    emergency.ContactPersonName = ob.ContactPersonName;
                    emergency.EmailId = ob.EmailId;
                    emergency.IsAvailable = true;
                    emergency.Mobile = ob.Mobile;
                    emergency.ModifiedDate = DateTime.Now;
                    emergency.ModifiedId = userid;
                    emergency.InsertedDate = DateTime.Now;
                    emergency.InsertedId = userid;
                    emergency.Status = "ACTIVE";
                    emergency.StudentID = ob.StudentID;
                    await studentEmergencyContactRepository.AddAsync(emergency);
                }
                return true;
            }
            catch (Exception e1)
            {
                return false;
            }
        }
        public long RemoveStudentEmergencyContact(long id, long userid)
        {
            StudentEmergencyContact emergency = studentEmergencyContactRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == id).FirstOrDefault();
            emergency.Active = true;
            emergency.ModifiedDate = DateTime.Now;
            emergency.ModifiedId = userid;
            emergency.Status = "INACTIVE";
            studentEmergencyContactRepository.UpdateAsync(emergency);
            return emergency.StudentID;
        }
        #endregion

        #region Student Transportation
        public List<studenttransportcls> GetAllStudentTransportation()
        {
            try
            {
                var transport = studentTransportationRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                if (transport.Count > 0)
                {
                    //if (transport.FirstOrDefault().IsTransport == true)
                    //{
                        var departure = GetAllDepartureTimesList();
                        var vehiclelocation = vehicleLocationRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
                        var location = GetAllVehicleLocationList();
                        var res = (from a in transport
                                   //join b in departure on a.DepartureTimeID equals b.ID
                                   //join c in location.vehiclelocation on b.LocationID equals c.ID
                                   select new studenttransportcls
                                   {
                                       CityID = a.LocationID!=0? location.vehiclelocation.Where(c=>c.ID==a.LocationID).FirstOrDefault().CityID:0,
                                       CityName = a.LocationID != 0 ? location.vehiclelocation.Where(c => c.ID == a.LocationID).FirstOrDefault().CityName : "",
                                       CountryID = a.LocationID != 0 ? location.vehiclelocation.Where(c => c.ID == a.LocationID).FirstOrDefault().CountryID:0,
                                       CountryName = a.LocationID != 0 ? location.vehiclelocation.Where(c => c.ID == a.LocationID).FirstOrDefault().CountryName:"",
                                       DepartureID = a.DepartureTimeID,
                                       departureName = a.DepartureTimeID != 0 ? departure.timings.Where(b=>b.ID==a.DepartureTimeID).FirstOrDefault().Name:"",
                                       Distance = a.Distance,
                                       ID = a.ID,
                                       IsTransport = a.IsTransport,
                                       LocationID = a.LocationID,
                                       LocationName = a.LocationID != 0 ? location.vehiclelocation.Where(c => c.ID == a.LocationID).FirstOrDefault().Name:"",
                                       ModifiedDate = a.ModifiedDate,
                                       StateID = a.LocationID != 0 ? location.vehiclelocation.Where(c => c.ID == a.LocationID).FirstOrDefault().StateID:0,
                                       StateName = a.LocationID != 0 ? location.vehiclelocation.Where(c => c.ID == a.LocationID).FirstOrDefault().StateName:"",
                                       StudentID = a.StudentID,
                                       VehicleNumber = a.LocationID != 0 ? location.vehiclelocation.Where(c => c.ID == a.LocationID).FirstOrDefault().vehicle.Number:"",
                                       VehicleType = a.LocationID != 0 ? location.vehiclelocation.Where(c => c.ID == a.LocationID).FirstOrDefault().vehicle.VehicleTypeName:""
                                   }).ToList();
                        return res;
                    //}
                    //else
                    //{
                    //    return null;
                    //}
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        
        public List<studenttransportcls> GetStudentTransportation(long studentid)
        {
            try
            {
                var transport = studentTransportationRepository.ListAllAsync().Result.Where(a => a.Active == true && a.StudentID == studentid).ToList();
                if (transport.Count > 0)
                {
                    if (transport.FirstOrDefault().IsTransport == true)
                    {
                        var departure = GetDepartureTimesList(transport.FirstOrDefault().LocationID).timings;
                        var vehiclelocation = vehicleLocationRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == transport.FirstOrDefault().LocationID).FirstOrDefault();
                        var location = GetVehicleLocationList(vehiclelocation.VehicleID.Value);
                        var res = (from a in transport
                                   join b in departure on a.DepartureTimeID equals b.ID
                                   join c in location.vehiclelocation on b.LocationID equals c.ID
                                   select new studenttransportcls
                                   {
                                       CityID = c.CityID,
                                       CityName = c.CityName,
                                       CountryID = c.CountryID,
                                       CountryName = c.CountryName,
                                       DepartureID = a.DepartureTimeID,
                                       departureName = b.Name,
                                       Distance = a.Distance,
                                       ID = a.ID,
                                       IsTransport = a.IsTransport,
                                       LocationID = a.LocationID,
                                       LocationName = c.Name,
                                       ModifiedDate = a.ModifiedDate,
                                       StateID = c.StateID,
                                       StateName = c.StateName,
                                       StudentID = a.StudentID,
                                       VehicleNumber = c.vehicle.Number,
                                       VehicleType = c.vehicle.VehicleTypeName
                                   }).ToList();
                        return res;
                    }
                    else
                    {
                        List<studenttransportcls> res = new List<studenttransportcls>();
                        studenttransportcls ob = new studenttransportcls();
                        ob.StudentID = studentid;
                        ob.ID = transport.FirstOrDefault().ID;
                        ob.IsTransport = transport.FirstOrDefault().IsTransport;
                        ob.ModifiedDate = DateTime.Now;
                        res.Add(ob);
                        return res;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public studenttransportcls CreateorEditStudentTransportation(long? id, long studentid)
        {
            var transport = studentTransportationRepository.ListAllAsync().Result.Where(a => a.Active == true && a.StudentID == studentid).ToList();
            if (transport.Count > 0)
            {
                if (transport.FirstOrDefault().IsTransport == true)
                {
                    var departure = GetDepartureTimesList(transport.FirstOrDefault().LocationID).timings;
                    var vehiclelocation = vehicleLocationRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == transport.FirstOrDefault().LocationID).FirstOrDefault();
                    var location = GetVehicleLocationList(vehiclelocation.VehicleID.Value);
                    var res = (from a in transport
                               join b in departure on a.DepartureTimeID equals b.ID
                               join c in location.vehiclelocation on b.LocationID equals c.ID
                               select new studenttransportcls
                               {
                                   CityID = c.CityID,
                                   CityName = c.CityName,
                                   CountryID = c.CountryID,
                                   CountryName = c.CountryName,
                                   DepartureID = a.DepartureTimeID,
                                   departureName = b.Name,
                                   Distance = a.Distance,
                                   ID = a.ID,
                                   IsTransport = a.IsTransport,
                                   LocationID = a.LocationID,
                                   LocationName = c.Name,
                                   ModifiedDate = a.ModifiedDate,
                                   StateID = c.StateID,
                                   StateName = c.StateName,
                                   StudentID = a.StudentID,
                                   VehicleNumber = c.vehicle.Number,
                                   VehicleType = c.vehicle.VehicleTypeName
                               }).FirstOrDefault();
                    return res;
                }
                else
                {
                    studenttransportcls ob = new studenttransportcls();
                    ob.StudentID = studentid;
                    ob.ID = transport.FirstOrDefault().ID;
                    ob.IsTransport = transport.FirstOrDefault().IsTransport;
                    ob.ModifiedDate = DateTime.Now;
                    return ob;
                }
            }
            else
            {
                studenttransportcls ob = new studenttransportcls();
                ob.StudentID = studentid;
                ob.ID = 0;
                ob.IsTransport = false;
                return ob;
            }
        }
        public async Task<bool> SaveoreditTransportation(studenttransportcls ob, long userid)
        {
            try
            {
                StudentTransportation transportation = studentTransportationRepository.ListAllAsync().Result.Where(a => a.Active == true && a.StudentID == ob.StudentID).FirstOrDefault();
                if (transportation != null)
                {
                    transportation.Active = true;
                    transportation.DepartureTimeID = ob.DepartureID;
                    transportation.Distance = ob.Distance;
                    transportation.IsAvailable = true;
                    transportation.IsTransport = ob.IsTransport;
                    transportation.LocationID = ob.LocationID;
                    transportation.ModifiedDate = DateTime.Now;
                    transportation.ModifiedId = userid;
                    transportation.Status = "ACTIVE";
                    transportation.StudentID = ob.StudentID;
                    await studentTransportationRepository.UpdateAsync(transportation);

                }
                else
                {
                    transportation = new StudentTransportation();
                    transportation.Active = true;
                    transportation.DepartureTimeID = ob.DepartureID;
                    transportation.Distance = ob.Distance;
                    transportation.IsAvailable = true;
                    transportation.IsTransport = ob.IsTransport;
                    transportation.LocationID = ob.LocationID;
                    transportation.ModifiedDate = DateTime.Now;
                    transportation.ModifiedId = userid;
                    transportation.InsertedDate = DateTime.Now;
                    transportation.InsertedId = userid;
                    transportation.Status = "ACTIVE";
                    transportation.StudentID = ob.StudentID;
                    studentTransportationRepository.AddAsync(transportation);
                }
                return true;
            }
            catch (Exception e1)
            {
                return false;
            }
        }
        public long RemoveTransportation(long id, long userid)
        {
            StudentTransportation transportation = studentTransportationRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == id).FirstOrDefault();
            transportation.Active = true;
            transportation.ModifiedDate = DateTime.Now;
            transportation.ModifiedId = userid;
            transportation.Status = "INACTIVE";
            studentTransportationRepository.UpdateAsync(transportation);
            return transportation.StudentID;
        }
        #endregion

        #region Student Academic Standard
        public studentacademicsstandarddetcls CreateorEditstudentacademicstandard(long? id, long studentid)
        {
            studentacademicsstandarddetcls ob = new studentacademicsstandarddetcls();
            if (id != null)
            {
                var sm = sqlQuery.Get_View_Academic_Student().Where(a => a.StudentId == studentid && a.ID == id).FirstOrDefault();
                ob.AcademicSectionId = sm.AcademicSectionId;
                ob.AcademicSessionId = sm.AcademicSessionId;
                ob.AcademicStandardId = sm.AcademicStandardId;
                ob.AcademicSubjectGroupId = sm.AcademicSubjectCombinationId;
                ob.AcademicSubjectWingId = sm.AcademicStandardWingId;
                ob.BoardId = sm.BoardId;
                ob.ID = sm.ID;
                ob.OraganisationAcademicId = sm.OraganisationAcademicId;
                ob.Studentcode = sm.RollNo;
                ob.StudentId = sm.StudentId;
                ob.IsRequiredNucleus = academicStandardSettingsRepository.ListAllAsync().Result == null ? false : academicStandardSettingsRepository.ListAllAsync().Result.Where(a => a.AcademicStandardId == sm.AcademicStandardId && a.Active == true).FirstOrDefault().Nucleus;
                ob.Nucleus = sm.Nucleus == null ? false : sm.Nucleus.Value;
            }
            else
            {
                ob.AcademicSectionId = 0;
                ob.AcademicSessionId = 0;
                ob.AcademicStandardId = 0;
                ob.AcademicSubjectGroupId = 0;
                ob.AcademicSubjectWingId = 0;
                ob.BoardId = 0;
                ob.ID = 0;
                ob.OraganisationAcademicId = 0;
                ob.Studentcode = "";
                ob.StudentId = studentid;
                ob.IsRequiredNucleus = false;
                ob.Nucleus = false;
            }
            return ob;
        }
        public List<CommonMasterModel> GetOrganisationAcademic()
        {
            var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result;
            var OrganisationList = organizationRepository.GetAllOrganization();
            var academicstandard = academicStandardRepository.ListAllAsyncIncludeAll().Result;

            var res = (from a in OrganisationAcademic
                       join b in OrganisationList on a.OrganizationId equals b.ID
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = b.Name,
                           GroupID = a.AcademicSessionId
                       }
                      ).ToList();
            return res;
        }
        public List<CommonMasterModel> GetAcademicstandard()
        {
            var boardstandard = boardStandardRepository.ListAllAsync().Result.ToList();
            var academicstandard = sqlQuery.Get_view_All_Academic_Standards().ToList();
            var res = (from a in academicstandard
                       join b in boardstandard on a.BoardStandardId equals b.ID
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.StandardName,
                           GroupID = b.BoardId,
                           organisationID=a.OraganisationAcademicId,
                           Status=a.BoardName
                       }).ToList();
            return res;
        }
        public List<AcademicStandardWingcls> GetAcademicWing()
        {
            var academicstandardwings = academicStandardWingRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var boardstandardwings = boardStandardWingRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var wings = wingRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            var res = (from a in academicstandardwings
                       join b in boardstandardwings on a.BoardStandardWingID equals b.ID
                       join c in wings on b.WingID equals c.ID
                       select new AcademicStandardWingcls
                       {
                           AcademicStandardID = a.AcademicStandardId,
                           BoardStandardID = b.BoardStandardID,
                           BoardStandardWingId = a.BoardStandardWingID,
                           ID = a.ID,
                           ModifiedDate = a.ModifiedDate,
                           Name = c.Name
                       }).ToList();
            return res;
        }
        
        public async Task<long> SaveOrEditStudentAcademicStandard(studentacademicsstandarddetcls ob, long userid)
        {

            var chk = academicStudentRepository.ListAllAsync().Result.ToList().Where(a => a.Active == true && a.AcademicStandardId == ob.AcademicStandardId && a.StudentId == ob.StudentId).FirstOrDefault();
            if (chk == null)
            {
                var obm = academicStudentRepository.ListAllAsync().Result.ToList().Where(a => a.Active == true && a.StudentId == ob.StudentId && a.IsAvailable == true).ToList();
                if (obm.Count > 0)
                {
                    foreach (var a in obm)
                    {
                        AcademicStudent academic = a;
                        academic.ModifiedDate = DateTime.Now;
                        academic.ModifiedId = userid;
                        academic.IsAvailable = false;
                        await academicStudentRepository.UpdateAsync(academic);
                    }
                }
                AcademicStudent academicStudent = new AcademicStudent();
                academicStudent.AcademicStandardId = ob.AcademicStandardId;
                academicStudent.AcademicStandardWingId = ob.AcademicSubjectWingId;
                academicStudent.AcademicSubjectCombinationId = ob.AcademicSubjectGroupId;
                academicStudent.Active = true;
                academicStudent.Nucleus = ob.Nucleus;
                academicStudent.InsertedDate = DateTime.Now;
                academicStudent.InsertedId = userid;
                academicStudent.ModifiedDate = DateTime.Now;
                academicStudent.ModifiedId = userid;
                academicStudent.Status = "ACTIVE";
                academicStudent.StudentId = ob.StudentId;
                academicStudent.IsAvailable = true;
                long academicstudentid = academicStudentRepository.AddAsync(academicStudent).Result.ID;
                AcademicSectionStudent section = new AcademicSectionStudent();
                section.AcademicSectionId = ob.AcademicSectionId;
                section.AcademicStudentId = academicstudentid;
                section.Active = true;
                section.InsertedDate = DateTime.Now;
                section.InsertedId = userid;
                section.ModifiedDate = DateTime.Now;
                section.ModifiedId = userid;
                section.Status = "ACTIVE";
                section.IsAvailable = true;
                section.StudentCode = ob.Studentcode;
                await academicSectionStudentRepository.AddAsync(section);
                return academicstudentid;
            }
            else
            {
                long academicstudentid = chk.ID;
                AcademicStudent academicStudent = chk;
                academicStudent.AcademicStandardId = ob.AcademicStandardId;
                academicStudent.AcademicStandardWingId = ob.AcademicSubjectWingId;
                academicStudent.AcademicSubjectCombinationId = ob.AcademicSubjectGroupId;
                academicStudent.Active = true;
                academicStudent.Nucleus = ob.Nucleus;
                academicStudent.ModifiedDate = DateTime.Now;
                academicStudent.ModifiedId = userid;
                academicStudent.Status = "ACTIVE";
                academicStudent.StudentId = ob.StudentId;
                academicStudent.IsAvailable = true;
                await academicStudentRepository.UpdateAsync(academicStudent);

                AcademicSectionStudent section = academicSectionStudentRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStudentId == academicstudentid && a.IsAvailable == true && a.AcademicSectionId == ob.AcademicSectionId).ToList().FirstOrDefault();

                if (section == null)
                {
                    AcademicSectionStudent section2 = academicSectionStudentRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStudentId == academicstudentid && a.IsAvailable == true).ToList().FirstOrDefault();
                    if (section2 != null)
                    {
                        section2.ModifiedDate = DateTime.Now;
                        section2.ModifiedId = userid;
                        section2.Status = "INACTIVE";
                        section2.IsAvailable = false;
                        await academicSectionStudentRepository.UpdateAsync(section2);
                    }

                    section = new AcademicSectionStudent();
                    section.AcademicSectionId = ob.AcademicSectionId;
                    section.AcademicStudentId = academicstudentid;
                    section.Active = true;
                    section.InsertedDate = DateTime.Now;
                    section.InsertedId = userid;
                    section.ModifiedDate = DateTime.Now;
                    section.ModifiedId = userid;
                    section.Status = "ACTIVE";
                    section.IsAvailable = true;
                    section.StudentCode = ob.Studentcode;
                    await academicSectionStudentRepository.AddAsync(section);
                    return academicstudentid;
                }
                else
                {
                    return academicstudentid;
                }
            }

        }



        public long RemoveStudentAcademicStandard(long id, long userid)
        {
            AcademicStudent academic = academicStudentRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == id).FirstOrDefault();
            academic.Active = true;
            academic.ModifiedDate = DateTime.Now;
            academic.ModifiedId = userid;
            academic.Status = "INACTIVE";
            academicStudentRepository.UpdateAsync(academic);
            AcademicSectionStudent section = academicSectionStudentRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AcademicStudentId == id && a.IsAvailable == true).FirstOrDefault();
            section.Active = true;
            section.ModifiedDate = DateTime.Now;
            section.ModifiedId = userid;
            section.Status = "INACTIVE";
            academicSectionStudentRepository.UpdateAsync(section);
            return academic.StudentId;
        }
        #endregion
    }
}

