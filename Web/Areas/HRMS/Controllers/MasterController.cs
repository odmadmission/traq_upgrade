﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OdmErp.Web.Areas.HRMS.Controllers
{
    [Area("HRMS")]
    public class MasterController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
