using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Text;
using OdmErp.Infrastructure.DTO;
using OdmErp.ApplicationCore.Entities.SupportAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.ApplicationCore.Entities;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;   
using Microsoft.Extensions.FileProviders;

using ClosedXML.Excel;
using OfficeOpenXml;
using System.Data;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using Web.Controllers;
using System.Globalization;
using Stream = System.IO.Stream;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.Web.DTO;
using OdmErp.Web.ViewModels;
using OdmErp.Web.ChartModels;
using OdmErp.Web.Areas.Support.Models;
using OdmErp.Web.Models;
using OdmErp.Infrastructure.Data;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using Floor = OdmErp.ApplicationCore.Entities.SupportAggregate.Floor;
using Microsoft.SqlServer.Management.HadrModel;

namespace OdmErp.Web.Areas.Support.Controllers
{
    [Area("Support")]
    public class SupportController : AppController
    {
        private ISupportRepository supportRepository;
        private IOrganizationRepository organizationRepository;
        private IEmployeeRepository employeeRepository;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IDesignationRepository designationRepository;
        private IDepartmentRepository departmentRepository;
        private ICityRepository cityRepository;
        private IDepartmentLeadRepository departmentLeadRepository;
        public SupportSmsnMails smssend;
        private readonly IHostingEnvironment hostingEnvironment;
        private IHttpContextAccessor _httpContextAccessor;
        private readonly IFileProvider fileprovider;

        private ISubModuleRepository subModuleRepository;
        private IActionAccessRepository actionAccessRepository;
        private OdmErp.Web.Models.CommonMethods common;
        private ApplicationDbContext _dbContext;

        private SupportMethod commonMethods;
        public SupportController(ICityRepository cr, IDepartmentLeadRepository dlr, IDepartmentRepository deptrep,
            IDesignationRepository degnrep, IEmployeeDesignationRepository edr, IEmployeeRepository er, ApplicationDbContext dbContext,
            ISupportRepository mr, SupportMethod commonMeth, IOrganizationRepository or, IFileProvider filepro, OdmErp.Web.Models.CommonMethods commonm,
            IHostingEnvironment hostingEnv, IHttpContextAccessor httpContextAccessor, SupportSmsnMails send, IActionAccessRepository aar, ISubModuleRepository smrepo)
        {
            common = commonm;
            commonMethods = commonMeth;
            supportRepository = mr;
            organizationRepository = or;
            fileprovider = filepro;
            hostingEnvironment = hostingEnv;
            employeeRepository = er;
            employeeDesignationRepository = edr;
            designationRepository = degnrep;
            departmentRepository = deptrep;
            cityRepository = cr;
            departmentLeadRepository = dlr;
            smssend = send;
            _httpContextAccessor = httpContextAccessor;
            subModuleRepository = smrepo;
            actionAccessRepository = aar;
            _dbContext = dbContext;
        }
        public IActionResult Index()
        {
            return View();
        }


        #region SupportCategory
        // GET: Countries
        public IActionResult SupportCategory()
        {
            CheckLoginStatus();

            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support Category", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var cat = supportRepository.GetAllCategory();
            var support = supportRepository.GetAllSupportType();

            var res = (from a in cat
                       join b in support on a.SupportTypeID equals b.ID
                       select new SupportCategoryCls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           SupportTypeName = b.Name,
                           IsAvailable = a.IsAvailable,
                           ModifiedDate = a.ModifiedDate
                       }
                     ).ToList();

            return View(res);
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditSupportCategory(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            SupportCategoryCls od = new SupportCategoryCls();

            od.supportTypes = (from a in supportRepository.GetAllSupportType()
                                   //where a.IsAvailable == true
                               select new SupportType
                               {
                                   ID = a.ID,
                                   Name = a.Name
                               }
                             ).ToList();

            if (id == null)
            {
                if (common.checkaccessavailable("Support Category", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Support Category", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var category = supportRepository.GetByCategoryId(id.Value);
                od.ID = category.ID;
                od.Name = category.Name;
                od.IsAvailable = category.IsAvailable;
                od.SupportTypeID = category.SupportTypeID;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateSupportCategory([Bind("ID,Name,IsAvailable,SupportTypeID")] SupportCategory category)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                category.ModifiedId = userId;
                category.Active = true;
                if (category.ID == 0)
                {
                    category.InsertedId = userId;
                    category.Status = EntityStatus.ACTIVE;
                    supportRepository.CreateCategory(category);
                }
                else
                {
                    supportRepository.UpdateCategory(category);
                }

                return RedirectToAction(nameof(SupportCategory));
            }
            return View(category);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteSupportCategory")]
        public async Task<IActionResult> SupportCategoryDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support Category", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (SupportCategoryExists(id.Value) == true)
            {
                var category = supportRepository.DeleteCategory(id.Value);
            }
            return RedirectToAction(nameof(SupportCategory));
        }

        private bool SupportCategoryExists(long id)
        {
            if (supportRepository.GetByCategoryId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region SupportPriority
        // GET: Countries
        public IActionResult SupportPriority()
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support Priority", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(supportRepository.GetAllPriority());
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditSupportPriority(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            SupportMasterModel od = new SupportMasterModel();
            if (id == null)
            {
                if (common.checkaccessavailable("Support Priority", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Support Priority", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var priority = supportRepository.GetByPriorityId(id.Value);
                od.ID = priority.ID;
                od.Name = priority.Name;
                od.IsAvailable = priority.IsAvailable;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateSupportPriority([Bind("ID,Name,IsAvailable")] SupportPriority priority)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                priority.ModifiedId = userId;
                priority.Active = true;
                if (priority.ID == 0)
                {
                    priority.InsertedId = userId;
                    priority.Status = EntityStatus.ACTIVE;
                    supportRepository.CreatePriority(priority);
                }
                else
                {
                    supportRepository.UpdatePriority(priority);
                }

                return RedirectToAction(nameof(SupportPriority));
            }
            return View(priority);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteSupportPriority")]
        public async Task<IActionResult> SupportPriorityDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support Priority", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (SupportPriorityExists(id.Value) == true)
            {
                var priority = supportRepository.DeletePriority(id.Value);
            }
            return RedirectToAction(nameof(SupportPriority));
        }

        private bool SupportPriorityExists(long id)
        {
            if (supportRepository.GetByPriorityId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region SupportStatus
        // GET: Countries
        public IActionResult SupportStatus()
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support Status", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(supportRepository.GetAllStatus());
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditSupportStatus(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            SupportMasterModel od = new SupportMasterModel();
            if (id == null)
            {
                if (common.checkaccessavailable("Support Status", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Support Status", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var status = supportRepository.GetByStatusId(id.Value);
                od.ID = status.ID;
                od.Name = status.Name;
                od.IsAvailable = status.IsAvailable;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateSupportStatus([Bind("ID,Name,IsAvailable")] SupportStatus status)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                status.ModifiedId = userId;
                status.Active = true;
                if (status.ID == 0)
                {
                    status.InsertedId = userId;
                    status.Status = EntityStatus.ACTIVE;
                    supportRepository.CreateStatus(status);
                }
                else
                {
                    supportRepository.UpdateStatus(status);
                }

                return RedirectToAction(nameof(SupportStatus));
            }
            return View(status);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteSupportStatus")]
        public async Task<IActionResult> SupportStatusDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support Status", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (SupportStatusExists(id.Value) == true)
            {
                var status = supportRepository.DeleteStatus(id.Value);
            }
            return RedirectToAction(nameof(SupportStatus));
        }

        private bool SupportStatusExists(long id)
        {
            if (supportRepository.GetByStatusId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Location
        // GET: Countries
        public IActionResult Location()
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Location", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var loc = supportRepository.GetAllLocation();
            var org = organizationRepository.GetAllOrganization();
            var city = cityRepository.GetAllCity();

            var res = (from a in loc
                       join b in org on a.OrganizationID equals b.ID
                       join c in city on a.CityID equals c.ID

                       select new LocationCls
                       {
                           ID = a.ID,
                           OrganizationName = b.Name,
                           Address = a.Address,
                           Name = a.Name,
                           IsAvailable = a.IsAvailable,
                           ModifiedDate = a.ModifiedDate,
                           CityName = c.Name
                       }
                     ).ToList();

            return View(res);
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditLocation(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            LocationCls od = new LocationCls();

            od.organisations = (from a in organizationRepository.GetAllOrganization()
                                    // where a.IsAvailable == true
                                select new Web.Models.Organisationcls
                                {
                                    ID = a.ID,
                                    Name = a.Name
                                }

                              ).ToList();

            od.city = (from b in cityRepository.GetAllCity()
                       select new Web.Models.Citycls
                       {
                           ID = b.ID,
                           Name = b.Name
                       }
                     ).ToList();


            if (id == null)
            {
                if (common.checkaccessavailable("Location", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Location", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var loc = supportRepository.GetByLocationId(id.Value);
                od.ID = loc.ID;
                od.Name = loc.Name;
                od.Address = loc.Address;
                od.OrganizationID = loc.OrganizationID;
                od.IsAvailable = loc.IsAvailable;
                od.CityID = loc.CityID;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateLocation([Bind("ID,Name,OrganizationID,Address,IsAvailable,CityID")] Location location)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                location.ModifiedId = userId;
                location.Active = true;
                if (location.ID == 0)
                {
                    location.InsertedId = userId;
                    location.Status = EntityStatus.ACTIVE;
                    supportRepository.CreateLocation(location);
                }
                else
                {
                    supportRepository.UpdateLocation(location);
                }

                return RedirectToAction(nameof(Location));
            }
            return View(location);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteLocation")]
        public async Task<IActionResult> LocationDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Location", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (LocationExists(id.Value) == true)
            {
                var loc = supportRepository.DeleteLocation(id.Value);
            }
            return RedirectToAction(nameof(Location));
        }

        private bool LocationExists(long id)
        {
            if (supportRepository.GetByLocationId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Room
        // GET: Countries
        public IActionResult Room()
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Room", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var building = supportRepository.GetAllBuilding();
            var roomcat = supportRepository.GetAllRoomCategory();
            var room = supportRepository.GetAllRoom();
            var floor = supportRepository.GetAllFloor();

            var res = (from a in room
                       join b in roomcat on a.RoomCategoryID equals b.ID
                       join c in building on a.BuildingID equals c.ID
                       join d in floor on a.FloorID equals d.ID

                       select new RoomCls
                       {
                           ID = a.ID,
                           RoomCategoryName = b.Name,
                           BuildingName = c.Name,
                           Name = a.Name,
                           IsAvailable = a.IsAvailable,
                           ModifiedDate = a.ModifiedDate,
                           FloorName = d.Name
                       }
                     ).ToList();

            return View(res);
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditRoom(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            RoomCls od = new RoomCls();

            od.buildings = (from a in supportRepository.GetAllBuilding()
                            where a.IsAvailable == true
                            select new BuildingCls
                            {
                                ID = a.ID,
                                Name = a.Name
                            }

                              ).ToList();

            od.roomcategory = (from b in supportRepository.GetAllRoomCategory()
                               where b.IsAvailable == true
                               select new SupportMasterModel
                               {
                                   ID = b.ID,
                                   Name = b.Name
                               }
                             ).ToList();

            od.floors = (from c in supportRepository.GetAllFloor()
                         where c.IsAvailable == true
                         select new Floor
                         {
                             ID = c.ID,
                             Name = c.Name
                         }
                       ).ToList();

            if (id == null)
            {
                if (common.checkaccessavailable("Room", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Room", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var room = supportRepository.GetByRoomId(id.Value);
                od.ID = room.ID;
                od.Name = room.Name;
                od.RoomCategoryID = room.RoomCategoryID;
                od.BuildingID = room.BuildingID;
                od.IsAvailable = room.IsAvailable;
                od.FloorID = room.FloorID;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateRoom([Bind("ID,Name,RoomCategoryID,BuildingID,IsAvailable,FloorID")] Room room)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                room.ModifiedId = userId;
                room.Active = true;
                if (room.ID == 0)
                {
                    room.InsertedId = userId;
                    room.Status = EntityStatus.ACTIVE;
                    supportRepository.CreateRoom(room);
                }
                else
                {
                    supportRepository.UpdateRoom(room);
                }

                return RedirectToAction(nameof(Room));
            }
            return View(room);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteRoom")]
        public async Task<IActionResult> RoomDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Room", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (RoomExists(id.Value) == true)
            {
                var room = supportRepository.DeleteRoom(id.Value);
            }
            return RedirectToAction(nameof(Room));
        }

        private bool RoomExists(long id)
        {
            if (supportRepository.GetByRoomId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region RoomCategory
        // GET: Countries
        public IActionResult RoomCategory()
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Room Category", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(supportRepository.GetAllRoomCategory());
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditRoomCategory(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            SupportMasterModel od = new SupportMasterModel();


            if (id == null)
            {
                if (common.checkaccessavailable("Room Category", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Room Category", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var roomcategory = supportRepository.GetByRoomCategoryId(id.Value);
                od.ID = roomcategory.ID;
                od.Name = roomcategory.Name;
                od.IsAvailable = roomcategory.IsAvailable;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateRoomCategory([Bind("ID,Name,IsAvailable")] RoomCategory roomCategory)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                roomCategory.ModifiedId = userId;
                roomCategory.Active = true;
                if (roomCategory.ID == 0)
                {
                    roomCategory.InsertedId = userId;
                    roomCategory.Status = EntityStatus.ACTIVE;
                    supportRepository.CreateRoomCategory(roomCategory);
                }
                else
                {
                    supportRepository.UpdateRoomCategory(roomCategory);
                }

                return RedirectToAction(nameof(RoomCategory));
            }
            return View(roomCategory);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteRoomCategory")]
        public async Task<IActionResult> RoomCategoryDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Room Category", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (RoomCategoryExists(id.Value) == true)
            {
                var roomcategory = supportRepository.DeleteRoomCategory(id.Value);
            }
            return RedirectToAction(nameof(RoomCategory));
        }

        private bool RoomCategoryExists(long id)
        {
            if (supportRepository.GetByRoomCategoryId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Building
        // GET: Countries
        public IActionResult Building()
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Building", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var location = supportRepository.GetAllLocation();
            var building = supportRepository.GetAllBuilding();

            var res = (from a in building
                       join b in location on a.LocationID equals b.ID

                       select new BuildingCls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           BuildingTypeID = a.BuildingTypeID,
                           LocationName = b.Name,
                           IsAvailable = a.IsAvailable,
                           ModifiedDate = a.ModifiedDate
                       }
                     ).ToList();

            return View(res);
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditBuilding(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            BuildingCls od = new BuildingCls();

            od.locations = (from a in supportRepository.GetAllLocation()
                            where a.IsAvailable == true
                            select new LocationCls
                            {
                                ID = a.ID,
                                Name = a.Name
                            }
                          ).ToList();

            if (id == null)
            {
                if (common.checkaccessavailable("Building", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Building", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var building = supportRepository.GetByBuildingId(id.Value);
                od.ID = building.ID;
                od.Name = building.Name;
                od.LocationID = building.LocationID;
                od.BuildingTypeID = building.BuildingTypeID;
                od.IsAvailable = building.IsAvailable;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateBuilding([Bind("ID,Name,LocationID,BuildingTypeID,IsAvailable")] Building build)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                build.ModifiedId = userId;
                build.Active = true;
                if (build.ID == 0)
                {
                    build.InsertedId = userId;
                    build.Status = EntityStatus.ACTIVE;
                    supportRepository.CreateBuilding(build);
                }
                else
                {
                    supportRepository.UpdateBuilding(build);
                }

                return RedirectToAction(nameof(Building));
            }
            return View(build);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteBuilding")]
        public async Task<IActionResult> BuildingDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Building", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (BuildingExists(id.Value) == true)
            {
                var building = supportRepository.DeleteBuilding(id.Value);
            }
            return RedirectToAction(nameof(Building));
        }

        private bool BuildingExists(long id)
        {
            if (supportRepository.GetByBuildingId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region SupportSubCategory
        // GET: Countries
        public IActionResult SupportSubCategory()
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support Sub-Category", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var cat = supportRepository.GetAllCategory();
            var subcat = supportRepository.GetAllSubCategory();

            var res = (from a in subcat
                       join b in cat on a.CategoryID equals b.ID

                       select new SupportSubCategoryCls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           CategoryName = b.Name,
                           IsAvailable = a.IsAvailable,
                           ModifiedDate = a.ModifiedDate
                       }
                     ).ToList();

            return View(res);
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditSupportSubCategory(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            SupportSubCategoryCls od = new SupportSubCategoryCls();

            od.category = (from a in supportRepository.GetAllCategory()
                           where a.IsAvailable == true
                           select new SupportMasterModel
                           {
                               ID = a.ID,
                               Name = a.Name
                           }
                         ).ToList();

            if (id == null)
            {
                if (common.checkaccessavailable("Support Sub-Category", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Support Sub-Category", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var subcategory = supportRepository.GetBySubCategoryId(id.Value);
                od.ID = subcategory.ID;
                od.Name = subcategory.Name;
                od.OthersName = subcategory.OthersName;
                od.CategoryID = subcategory.CategoryID;
                od.IsAvailable = subcategory.IsAvailable;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateSupportSubCategory([Bind("ID,Name,CategoryID,IsAvailable,OthersName")] SupportSubCategory subcat)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                subcat.ModifiedId = userId;
                subcat.Active = true;
                if (subcat.ID == 0)
                {
                    subcat.InsertedId = userId;
                    subcat.Status = EntityStatus.ACTIVE;
                    supportRepository.CreateSubCategory(subcat);
                }
                else
                {
                    supportRepository.UpdateSubCategory(subcat);
                }

                return RedirectToAction(nameof(SupportSubCategory));
            }
            return View(subcat);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteSupportSubCategory")]
        public async Task<IActionResult> SupportSubCategoryDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support Sub-Category", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (SupportSubCategoryExists(id.Value) == true)
            {
                var subcategory = supportRepository.DeleteSubCategory(id.Value);
            }
            return RedirectToAction(nameof(SupportSubCategory));
        }

        private bool SupportSubCategoryExists(long id)
        {
            if (supportRepository.GetBySubCategoryId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Parismita

        #region Support Request
        public IActionResult SupportRequest(long[] stype, string fromdate, string todate)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            ViewBag.SupportType = supportRepository.GetAllSupportType().ToList();
            if (common.checkaccessavailable("Raise", accessId, "List", "Support", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            TempData["FromDate"] = fromdate;
            TempData["ToDate"] = todate;


            SupportViewModel model = new SupportViewModel();
            var requests = supportRepository.GetAllSupportRequestByUserId(userId).ToList();
            //roleId > 1 ? supportRepository.GetAllSupportRequest().Where(a => a.InsertedId == userId).ToList() 
            //:
            //  supportRepository.GetAllSupportRequest().ToList();

            if (requests != null)
            {

                var cat = supportRepository.GetAllCategory().ToList();
                var subcat = supportRepository.GetAllSubCategory().ToList();
                var loc = supportRepository.GetAllLocation().ToList();
                var prior = supportRepository.GetAllPriority().ToList();
                var support = supportRepository.GetAllSupportType().ToList();

                var statusList = supportRepository.GetAllStatus();

                var assignedTo = _dbContext.SupportRequestAssign.ToList();



                var build = supportRepository.GetAllBuilding();
                var room = supportRepository.GetAllRoom();

                var res = (from a in requests
                           join b in cat on a.CategoryID equals b.ID
                           join c in subcat on a.SubCategoryID equals c.ID
                           join g in prior on a.PriorityID equals g.ID
                           join h in support on a.SupportTypeID equals h.ID


                           select new SupportRequestCls
                           {
                               ID = a.ID,
                               CategoryID = a.CategoryID,
                               SubCategoryID = a.SubCategoryID,
                               BuildingID = a.BuildingID,
                               PriorityID = a.PriorityID,
                               RoomID = a.RoomID,
                               CategoryName = b.Name,
                               SubCategoryName = c.Name,
                               Title = a.Title,
                               TicketCode = a.TicketCode,
                               BuildingName = a.BuildingID != 0 ? build != null ? build.Where(m => m.ID == a.BuildingID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE : ViewConstants.NOT_APPLICABLE,
                               RoomName = a.RoomID != 0 ? room != null ?
                               room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name :
                               ViewConstants.NOT_APPLICABLE : ViewConstants.NOT_APPLICABLE,
                               OrgGroupName = common.GetOrgGroupName(a.InsertedId),
                               PriorityName = g.Name,
                               IsApprove = a.IsApprove,
                               StatusID = a.StatusID,
                               Status = a.StatusName,
                               ModifiedDate = a.ModifiedDate,
                               SupportTypeID = h.ID,
                               SupportTypeName = h.Name,
                               InsertedDate = a.InsertedDate,
                               InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                               Description = a.Description,
                               SentApproval = a.SendApproval,
                               InsertedID = a.InsertedId,
                               emp_Name = common.GetEmployeeNameByEmployeeId(a.InsertedId, userId),
                               RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                               DueDate = NullableDateTimeToStringDate(a.DueDate),
                               commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, userId),
                               Comment = a.Comment,
                               score = a.Score,
                               AssignedToEmp = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                 employeeRepository.GetEmployeeFullNameById(assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId) : "NA" : "NA",
                               AssignedDate = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                              assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().InsertedDate.ToString() : "NA" : "NA",

                               Management = (
                               a.SendApproval,
                                NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                 a.SentApprovalID != null && a.SentApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, userId) : ViewConstants.NOT_APPLICABLE,
                               a.ManagementApproval,
                               NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                               a.ManagementApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, userId) : ViewConstants.NOT_APPLICABLE,
                               a.ManagementReject,
                               NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                               a.ManagementRejectedID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, userId) : ViewConstants.NOT_APPLICABLE


                               )
                           }
                         ).OrderByDescending(a => a.ID).ToList();
                if (stype.Count() > 0)
                {
                    res = res.Where(a => stype.Contains(a.SupportTypeID)).ToList();
                }

                if (fromdate != null)
                {
                    DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                    DateTime ToDate = System.DateTime.Now;
                    if (todate != null)
                    {
                        ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                    }
                    if (fromdate != null && todate != null)
                    {
                        res = res.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                    }
                    if (fromdate != null && todate == null)
                    {
                        res = res.Where(a => a.InsertedDate >= FromDate).ToList();
                    }
                }

                model.requests = res;
                HttpContext.Session.SetString("supportrequest", JsonConvert.SerializeObject(res));

                model.stype = stype;
                return View(model);
            }
            else
            {
                return View(model);
            }




        }
        public IActionResult CreateOrEditSupportRequest(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            SupportRequestCls od = new SupportRequestCls();

            od.supportTypes = (from a in supportRepository.GetAllSupportType()
                                   //where a.IsAvailable == true
                               select new SupportType
                               {
                                   ID = a.ID,
                                   Name = a.Name
                               }
                             ).ToList();


            od.buildings = (from b in supportRepository.GetAllBuilding()
                            where b.IsAvailable == true
                            select new BuildingCls
                            {
                                ID = b.ID,
                                Name = b.Name
                            }
                          ).ToList();

            od.priority = (from c in supportRepository.GetAllPriority()
                           where c.IsAvailable == true
                           select new SupportMasterModel
                           {
                               ID = c.ID,
                               Name = c.Name
                           }
                         ).ToList();


            if (id == null)
            {
                if (common.checkaccessavailable("Raise", accessId, "Create", "Support", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                SupportRequest supportRequests = new SupportRequest();
                ViewBag.status = "Create";
                od.ID = 0;
                if (HttpContext.Session.GetString("attachments") != null)
                {
                    HttpContext.Session.Remove("attachments");
                }

            }
            else
            {
                if (common.checkaccessavailable("Raise", accessId, "Edit", "Support", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                var catid = supportRepository.GetBySupportRequestId(id.Value).CategoryID;
                var buildid = supportRepository.GetBySupportRequestId(id.Value).BuildingID;
                var supportid = supportRepository.GetBySupportRequestId(id.Value).SupportTypeID;


                od.subcategory = (from b in supportRepository.GetAllSubCategory()
                                  where b.IsAvailable == true && b.CategoryID == catid
                                  select new SupportSubCategoryCls
                                  {
                                      ID = b.ID,
                                      Name = b.Name
                                  }
                         ).ToList();

                od.category = (from a in supportRepository.GetAllCategory()
                               where a.IsAvailable == true && a.SupportTypeID == supportid
                               select new SupportCategoryCls
                               {
                                   ID = a.ID,
                                   Name = a.Name
                               }
                         ).ToList();

                var room = supportRepository.GetAllRoom();
                var floor = supportRepository.GetAllFloor();

                od.room = (from b in room
                           join c in floor on b.FloorID equals c.ID
                           where b.IsAvailable == true && b.BuildingID == buildid
                           select new RoomCls
                           {
                               ID = b.ID,
                               Name = b.Name + " " + "(" + c.Name + ")",
                               FloorName = c.Name

                           }
                        ).ToList();

                od.statuses = (from c in supportRepository.GetAllStatus()
                               where c.IsAvailable == true
                               select new SupportStatus
                               {
                                   ID = c.ID,
                                   Name = c.Name
                               }
                             ).ToList();

                ViewBag.status = "Update";
                var request = supportRepository.GetBySupportRequestId(id.Value);
                od.ID = request.ID;
                od.CategoryID = request.CategoryID;
                od.SubCategoryID = request.SubCategoryID;
                od.LocationID = 0;

                od.SentApproval = request.SendApproval;
                od.IsApprove = request.ManagementApproval;
                od.IsReject = request.ManagementReject;
                od.Title = request.Title;
                od.TicketCode = request.TicketCode;
                od.Comment = request.Comment;
                od.RoomID = request.RoomID;
                od.PriorityID = request.PriorityID;
                od.BuildingID = request.BuildingID;
                od.Description = request.Description;
                od.SupportTypeID = request.SupportTypeID;
                od.StatusID = request.StatusID;
                od.Status = request.StatusName;

                od.CompletionDate = request.RequestedCompletionDate != null ? (request.RequestedCompletionDate.Value.ToString("yyyy-MM-dd")) : "";
                od.IsAvailable = supportRepository.GetBySupportTypeId(request.SupportTypeID).IsAvailable;
                od.commentcount = commonMethods.GetSupportComments(id.Value, userId).OrderByDescending(a => a.id).ToList().Count();

                od.Management = (
                          request.SendApproval,
                          null, null,
                         request.ManagementApproval,
                          null, null,
                        request.ManagementReject,
                          null, null


                          );

                //if (request.InsertedId == userId)
                //{
                //    var comments = supportRepository.GetAllSupportComment().Where(a => a.SupportRequestID == request.ID && a.InsertedId != userId && a.IsRead == false).ToList();
                //    foreach (var a in comments)
                //    {
                //        SupportComment comment = a;
                //        comment.IsRead = true;
                //        comment.ModifiedId = userId;
                //        supportRepository.UpdateSupportComment(comment);
                //    }
                //}

                // List<SupportRequestattachments> attachments = new List<SupportRequestattachments>();


                od.AttachmentCount = supportRepository.GetAllAttachment().Where(a => a.SupportRequestID == id.Value && a.Type == "RAISED").Count();

                ViewBag.UserId = userId;

                //  HttpContext.Session.SetString("attachments", JsonConvert.SerializeObject(od.Attachments));
            }

            return View(od);
        }
        [ActionName("DeleteSupportRequest")]
        public async Task<IActionResult> SupportRequestDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Raise", accessId, "Delete", "Support", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (SupportRequestExists(id.Value) == true)
            {
                var request = supportRepository.DeleteSupportRequest(id.Value);
            }
            return RedirectToAction(nameof(SupportRequest));
        }

        private bool SupportRequestExists(long id)
        {
            if (supportRepository.GetBySupportRequestId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public IActionResult CreateOrEditSupportApproveReject(long id)
        {
            var res = ViewSupportRequestDet(id);
            return View(res);
        }
        [HttpPost]
        public async Task<JsonResult> SaveSupportRequestAttachment(string Name, IFormFile file)
        {
            List<SupportRequestattachments> attachments = new List<SupportRequestattachments>();
            if (HttpContext.Session.GetString("attachments") != null)
            {
                attachments = JsonConvert.DeserializeObject<List<SupportRequestattachments>>(HttpContext.Session.GetString("attachments"));
            }
            SupportRequestattachments attach = new SupportRequestattachments();

            if (file != null)
            {
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "File" + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\Tempfile\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                attach.path = path;
                attach.file = "/ODMImages/Tempfile/" + pathToSave;
            }
            var webPathnew = hostingEnvironment.WebRootPath;
            string pathnew = Path.Combine("", webPathnew + @"\ODMImages\Tempfile\");
            //foreach (var item in attachments)
            //{
            //    if (item.file == null)
            //        item.file = "/ODMImages/Tempfile/" + item.path;
            //    else
            //    if (item.file.IndexOf("Tempfile") == 0)
            //        item.file = "/ODMImages/Tempfile/" + item.path;

            //    if (item.path == null)
            //        item.path = pathnew + item.path;
            //    else
            //    if (item.path.IndexOf("Tempfile") == 0)
            //        item.path = pathnew + item.path;
            //}

            attach.Name = "File";
            attach.id = attachments.Count() + 1;
            attachments.Add(attach);
            HttpContext.Session.SetString("attachments", JsonConvert.SerializeObject(attachments));
            return Json(attachments.Where(a => a.isremove == false).ToList());
        }
        public async Task<JsonResult> RemoveSupportRequestAttachment(int id)
        {
            List<SupportRequestattachments> attachments = new List<SupportRequestattachments>();
            if (HttpContext.Session.GetString("attachments") != null)
            {
                attachments = JsonConvert.DeserializeObject<List<SupportRequestattachments>>(HttpContext.Session.GetString("attachments"));
            }

            SupportRequestattachments re = attachments.Where(a => a.id == id).FirstOrDefault();
            re.isremove = true;

            HttpContext.Session.SetString("attachments", JsonConvert.SerializeObject(attachments));
            return Json(attachments.Where(a => a.isremove == false).ToList());
        }

        [HttpPost]
        public async Task<JsonResult> SaveSupportRequest(SupportRequest supportRequests)
        {
            try
            {
                long userId = HttpContext.Session.GetInt32("userId").Value;
                long roleId = HttpContext.Session.GetInt32("roleId").Value;
                SupportRequest request = new SupportRequest();
                if (supportRequests.ID == 0)
                {
                    request.Title = supportRequests.Title;

                    request.Description = supportRequests.Description;
                    request.CategoryID = supportRequests.CategoryID;
                    request.SubCategoryID = supportRequests.SubCategoryID;
                    request.LocationID = 0;
                    request.BuildingID = supportRequests.BuildingID;
                    request.RoomID = supportRequests.RoomID;
                    request.PriorityID = supportRequests.PriorityID;
                    request.ModifiedId = userId;
                    request.InsertedId = userId;
                    request.RequestedCompletionDate = supportRequests.CompletionDate;
                    request.Active = true;
                    request.StatusID = 1;
                    request.SupportTypeID = supportRequests.SupportTypeID;
                    request.StatusName = "PENDING";
                    var type = supportRepository.GetBySupportTypeId(supportRequests.SupportTypeID);
                    var allrequest = supportRepository.GetAllSupportRequestWithinactive() == null ? null : supportRepository.GetAllSupportRequestWithinactive().Where(a => a.SupportTypeID == supportRequests.SupportTypeID).ToList();
                    if (allrequest == null)
                    {
                        request.TicketCode = type.Name.Substring(0, 2) + "-1";
                    }
                    else
                    {
                        request.TicketCode = type.Name.Substring(0, 2) + "-" + (allrequest.ToList().Count + 1);
                    }

                    request.Status = EntityStatus.ACTIVE;
                    long id = supportRepository.CreateSupportRequest(request);

                    SupportTimeLine supportTimeLine = new SupportTimeLine();
                    supportTimeLine.InsertedId = userId;
                    supportTimeLine.ModifiedId = userId;
                    supportTimeLine.Active = true;
                    supportTimeLine.Status = "PENDING";

                    supportTimeLine.SupportRequestID = id;
                    supportTimeLine.SupportStatusID = 1;
                    supportRepository.CreateSupportTimeLine(supportTimeLine);


                    List<SupportRequestattachments> attachments = new List<SupportRequestattachments>();
                    if (HttpContext.Session.GetString("attachments") != null)
                    {
                        attachments = JsonConvert.DeserializeObject<List<SupportRequestattachments>>(HttpContext.Session.GetString("attachments"));
                    }

                    if (attachments.Count() > 0)

                    {
                        foreach (var a in attachments)
                        {

                            string getinfo = Path.GetFileName(a.path);
                            string filepath = hostingEnvironment.WebRootPath + "\\ODMImages\\Tempfile\\" + id;
                            SupportAttachment Attachment = new SupportAttachment();
                            if (!Directory.Exists(filepath))
                            {
                                DirectoryInfo di = Directory.CreateDirectory(filepath);
                            }


                            if (!System.IO.File.Exists(filepath + "\\" + getinfo))   // New Attachment
                            {
                                Attachment.SupportRequestID = id;
                                Attachment.ModifiedId = userId;
                                Attachment.Name = a.Name;
                                Attachment.Path = getinfo;

                                if (a.path != null && a.isremove == false)
                                {
                                    Attachment.Active = true;
                                    Attachment.InsertedId = userId;
                                    Attachment.Status = EntityStatus.ACTIVE;
                                    Attachment.Type = "RAISED";
                                    supportRepository.CreateAttachment(Attachment);

                                    if (!System.IO.File.Exists(filepath + "\\" + getinfo)) // id file already exist then no need to copy again to folder
                                        System.IO.File.Copy(a.path, filepath + "\\" + getinfo);
                                }

                            }
                            else   // Edited or Deleted 
                            {
                                Attachment = supportRepository.GetAllAttachment().Where(m => m.ID == a.id || m.Type == "RAISED").FirstOrDefault();

                                if (a.path != null && a.isremove == true)
                                {
                                    Attachment.Active = false;
                                    Attachment.InsertedId = userId;
                                    Attachment.Status = EntityStatus.DELETED;
                                    Attachment.Type = "RAISED";
                                    supportRepository.UpdateAttachment(Attachment);

                                    if (System.IO.File.Exists(filepath + "\\" + getinfo))
                                    {
                                        System.IO.File.Delete(filepath + "\\" + getinfo);
                                    }
                                }
                            }


                        }
                        HttpContext.Session.Remove("attachments");
                    }


                    var Department_Id = supportRepository.GetBySupportTypeId(request.SupportTypeID).DepartmentID;



                    List<long> DesignationIdes = designationRepository.GetDesignationByDepartmentId(Department_Id).ToList();

                    //Employee Head Details
                    List<DepartmentLead> employeeLeadid = departmentLeadRepository.GetAllEmployeeIdByDepartment(Department_Id).ToList();

                    List<long> employeeLeadid_Emplyee_Ides = employeeLeadid.Select(e => e.EmployeeID).ToList();
                    // Department Employee Details
                    List<EmployeeDesignation> EmployeeIdes = employeeDesignationRepository.GetAllEmployeeDesignationsByDesignationId(DesignationIdes).ToList();
                    List<long> Department_Emplyee_Ides = EmployeeIdes.Select(e => e.EmployeeID).ToList();

                    foreach (var item in employeeLeadid_Emplyee_Ides) // remove department head employee id from employee list
                    {
                        Department_Emplyee_Ides.Remove(item);
                    }

                    //Fire Email to Email Head


                    foreach (var items in employeeLeadid_Emplyee_Ides)
                    {

                        long emp_id = items;
                        Employee employees = employeeRepository.GetEmployeeById(emp_id);
                        string name = employees.FirstName + " " + employees.LastName;
                        string code = employees.EmpCode;
                        string email = employees.EmailId;
                        string phone = employees.PrimaryMobile;
                        string category = "Employee";
                        string whom = "Employee";
                        string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                        var description = supportRepository.GetBySupportRequestId(request.ID);

                        smssend.raisesupport(description.Title, description.Description, name, email, phone, code, category, typename, "New Support Request For Head-" + description.Title + "-" + name, whom, employees.EmailId);
                       
                    }


                    //Fire Email to Department Employees




                    foreach (var item in Department_Emplyee_Ides)
                    {

                        long emp_id = item;
                        Employee employees = employeeRepository.GetEmployeeById(emp_id);
                        string name = employees.FirstName + " " + employees.LastName;
                        string code = employees.EmpCode;
                        string email = employees.EmailId;
                        string phone = employees.PrimaryMobile;
                        string category = "Employee";
                        string whom = "Employee";
                        string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                        var description = supportRepository.GetBySupportRequestId(request.ID);

                        smssend.raisesupport(description.Title, description.Description, name, email, phone, code, category, typename, "New Support Request-" + description.Title + "-" + name, whom, employees.EmailId);

                    }



                    // var Designation_Id = employeeDesignationRepository.GetAllEmployeeDesignationsByEmployeeId(request.InsertedId);



                    // var Designation_Ides = designationRepository.Get(request.InsertedId);


                    // var employee = departmentLeadRepository.GetAllDepartmentLead().Where(c => c.EmployeeID == userId).FirstOrDefault().EmployeeID;


                    //List<DepartmentLead> departmentIdLead = departmentLeadRepository.GetAllByEmployeeId(userId).ToList();

                    //var employees = employeeRepository.GetEmployeeById(request.InsertedId);

                    //string name = employees.FirstName + " " + employees.LastName;
                    //string code = employees.EmpCode;
                    //string email = employees.EmailId;
                    //string phone = employees.PrimaryMobile;
                    //string category = "Employee";
                    //string whom = "Employee";
                    //string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;


                    //if (departmentIdLead.Count() > 0)
                    //{

                    //    smssend.deadlinedatesupport(name, email, phone, code, category, typename, "New Approval Support Request", whom, employees.EmailId);

                    //}

                    //else
                    //{

                    //    smssend.raisesupport(name, email, phone, code, category, typename, "New Support Request", whom, employees.EmailId);

                    //}



                }

                else
                {
                    SupportRequest requests = supportRepository.GetBySupportRequestId(supportRequests.ID);
                    requests.Title = supportRequests.Title;
                    // requests.TicketCode = supportRequests.TicketCode;
                    requests.Description = supportRequests.Description;
                    requests.CategoryID = supportRequests.CategoryID;
                    requests.SubCategoryID = supportRequests.SubCategoryID;
                    requests.LocationID = 0;
                    requests.BuildingID = supportRequests.BuildingID;
                    requests.RoomID = supportRequests.RoomID;
                    requests.PriorityID = supportRequests.PriorityID;

                    requests.RequestedCompletionDate = supportRequests.CompletionDate;
                    requests.SupportTypeID = supportRequests.SupportTypeID;
                    // requests.DueDate = supportRequests.DueDate;
                    requests.ModifiedId = userId;
                    supportRepository.UpdateSupportRequest(requests);

                    //SupportComment comments = new SupportComment();

                    //comments.SupportRequestID = supportRequests.ID;
                    //comments.InsertedId = userId;
                    //comments.ModifiedId = userId;
                    //comments.Active = true;
                    //comments.Name = supportRequests.Comment;
                    //// long cmntid = supportRepository.CreateSupportComment(comments);
                    //supportRepository.CreateSupportComment(comments);

                    List<SupportRequestattachments> attachments = new List<SupportRequestattachments>();
                    if (HttpContext.Session.GetString("attachments") != null)
                    {
                        attachments = JsonConvert.DeserializeObject<List<SupportRequestattachments>>(HttpContext.Session.GetString("attachments"));
                    }
                    if (attachments.Count() > 0)
                    {
                        foreach (var a in attachments)
                        {

                            string getinfo = Path.GetFileName(a.path);
                            string filepath = hostingEnvironment.WebRootPath + "\\ODMImages\\Tempfile\\" + requests.ID;
                            SupportAttachment Attachment = new SupportAttachment();
                            if (!Directory.Exists(filepath))
                            {
                                DirectoryInfo di = Directory.CreateDirectory(filepath);
                            }


                            if (!System.IO.File.Exists(filepath + "\\" + getinfo))   // New Attachment
                            {
                                Attachment.SupportRequestID = requests.ID;
                                Attachment.ModifiedId = userId;
                                Attachment.Name = a.Name;
                                Attachment.Path = getinfo;

                                if (a.path != null && a.isremove == false)
                                {
                                    Attachment.Active = true;
                                    Attachment.InsertedId = userId;
                                    Attachment.Status = EntityStatus.ACTIVE;
                                    Attachment.Type = "RAISED";
                                    supportRepository.CreateAttachment(Attachment);

                                    if (!System.IO.File.Exists(filepath + "\\" + getinfo)) // id file already exist then no need to copy again to folder
                                        System.IO.File.Copy(a.path, filepath + "\\" + getinfo);
                                }

                            }
                            else   // Edited or Deleted 
                            {
                                Attachment = supportRepository.GetAllAttachment().Where(m => m.ID == a.id || m.Type == "RAISED").FirstOrDefault();

                                if (a.path != null && a.isremove == true)
                                {
                                    Attachment.Active = false;
                                    Attachment.InsertedId = userId;
                                    Attachment.Status = EntityStatus.DELETED;
                                    Attachment.Type = "RAISED";
                                    supportRepository.UpdateAttachment(Attachment);

                                    if (System.IO.File.Exists(filepath + "\\" + getinfo))
                                    {
                                        System.IO.File.Delete(filepath + "\\" + getinfo);
                                    }
                                }
                            }


                        }
                        HttpContext.Session.Remove("attachments");
                    }

                }

                return Json(1);
            }
            catch (Exception e1)
            {
                return Json(0);
            }
        }
        [HttpPost]
        public async Task<IActionResult> SupportRequestVerify(long? hdnid, decimal txtscore)
        {
            long id = hdnid.Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;


            var request = supportRepository.GetBySupportRequestId(id);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            var Department_Id = supportRepository.GetBySupportTypeId(request.SupportTypeID).DepartmentID;

            List<long> DesignationIdes = designationRepository.GetDesignationByDepartmentId(Department_Id).ToList();

            //Employee Head Details
            List<DepartmentLead> employeeLeadid = departmentLeadRepository.GetAllEmployeeIdByDepartment(Department_Id).ToList();

            List<long> employeeLeadid_Emplyee_Ides = employeeLeadid.Select(e => e.EmployeeID).ToList();
            // Department Employee Details
            List<EmployeeDesignation> EmployeeIdes = employeeDesignationRepository.GetAllEmployeeDesignationsByDesignationId(DesignationIdes).ToList();
            List<long> Department_Emplyee_Ides = EmployeeIdes.Select(e => e.EmployeeID).ToList();

            foreach (var item in employeeLeadid_Emplyee_Ides) // remove department head employee id from employee list
            {
                Department_Emplyee_Ides.Remove(item);
            }


            var raisedEmp = employeeRepository.GetEmployeeById(userId);
            string raisedName = raisedEmp.FirstName + "" + raisedEmp.LastName;

            request.ApprovedID = userId;
            request.ApproveDate = DateTime.Now;
            request.ModifiedDate = DateTime.Now;
            request.IsApprove = true;
            request.StatusID = 6;
            request.StatusName = "VERIFIED";
            request.Score = txtscore;
            request.ScoreGivenId = userId;
            request.ScoreGivenDate = DateTime.Now;
            supportRepository.UpdateSupportRequest(request);
            //Fire Email to Email Head

            foreach (var items in employeeLeadid_Emplyee_Ides)
            {

                long emp_id = items;
                Employee employees = employeeRepository.GetEmployeeById(emp_id);
                string name = employees.FirstName + " " + employees.LastName;
                string code = employees.EmpCode;
                string email = employees.EmailId;
                string phone = employees.PrimaryMobile;
                string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                var description = supportRepository.GetBySupportRequestId(request.ID);

                smssend.Verifysupport(request.ID.ToString(), description.Description, raisedName, raisedName, "Verify Support Request By Employee-" + description.Title + "-" + name, email);

            }


            //Fire Email to Department Employees

            foreach (var item in Department_Emplyee_Ides)
            {

                long emp_id = item;
                Employee employees = employeeRepository.GetEmployeeById(emp_id);
                string name = employees.FirstName + " " + employees.LastName;
                string code = employees.EmpCode;
                string email = employees.EmailId;
                string phone = employees.PrimaryMobile;
                string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                var description = supportRepository.GetBySupportRequestId(request.ID);

                smssend.Verifysupport(request.ID.ToString(), description.Description, raisedName, raisedName, "Verify Support Request By Employee-" + description.Title + "-" + name, email);

            }
            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.SupportRequestID = id;
            supportTimeLine.Status = "VERIFIED";
            supportTimeLine.SupportStatusID = 6;
            supportTimeLine.ModifiedDate = DateTime.Now;
            supportTimeLine.InsertedDate = DateTime.Now;
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportRepository.CreateSupportTimeLine(supportTimeLine);

            return RedirectToAction(nameof(SupportRequest));
        }
        [HttpPost]
        public IActionResult assignscore(long? hdnid1, decimal txtscore1)
        {
            long id = hdnid1.Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;


            var request = supportRepository.GetBySupportRequestId(id);
            long userId = HttpContext.Session.GetInt32("userId").Value;
            request.ModifiedDate = DateTime.Now;
            request.Score = txtscore1;
            request.ScoreGivenId = userId;
            request.ScoreGivenDate = DateTime.Now;
            supportRepository.UpdateSupportRequest(request);
            return RedirectToAction(nameof(SupportRequest));
        }
        #endregion
        #region Support Resolve
        public class EmpClassModal
        {
            public long ID { get; set; }
            public string EmpName { get; set; }
            public long DeptId { get; set; }
            public string DeptName { get; set; }
        }
        public List<EmpClassModal> GetallEmployeeByDeptId()
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            var deptId = common.GetDepartmentsByEmployeeID(userId).Select(a => a.id);
            var empList = common.GetAllDeptwithEmployee().Where(a => deptId.Contains(a.DepartmentID)).ToList();
            //var emp = employeeRepository.get.GetAllEmployee();
            var res = (from a in empList
                       select new EmpClassModal
                       {
                           ID = a.EmployeeID,
                           EmpName = a.EmployeeID == userId ? "me" : employeeRepository.GetEmployeeFullNameById(a.EmployeeID),
                           DeptId = a.DepartmentID,
                           DeptName = a.DepartmentName
                       }).ToList();

            return res;
        }
        [HttpPost]
        public IActionResult AssignRequestToEmployee(long id, long assignEmp)
        {
            var req = _dbContext.SupportRequestAssign.Where(a => a.SupportRequestId == id).FirstOrDefault();
            long userId = HttpContext.Session.GetInt32("userId").Value;

            if (req == null)
            {

                SupportRequestAssign requestAssign = new SupportRequestAssign();
                requestAssign.InsertedDate = DateTime.Now;
                requestAssign.InsertedId = userId;
                requestAssign.ModifiedDate = DateTime.Now;
                requestAssign.ModifiedId = userId;
                requestAssign.Active = true;
                requestAssign.Status = "ACTIVE";
                requestAssign.IsAvailable = true;
                requestAssign.SupportRequestId = id;
                requestAssign.EmployeeId = assignEmp;

                _dbContext.SupportRequestAssign.Add(requestAssign);
                _dbContext.SaveChanges();

                if (requestAssign.ID > 0)
                {
                    SupportTimeLine timeLine = new SupportTimeLine();
                    timeLine.InsertedDate = DateTime.Now;
                    timeLine.InsertedId = userId;
                    timeLine.ModifiedDate = DateTime.Now;
                    timeLine.ModifiedId = userId;
                    timeLine.Active = true;
                    timeLine.Status = "REQUEST IS ASSIGNED";
                    timeLine.SupportRequestID = id;
                    timeLine.ReassignedToId = assignEmp;

                    _dbContext.SupportTimeLines.Add(timeLine);
                    _dbContext.SaveChanges();
                }
            }
            else
            {
                req.ModifiedDate = DateTime.Now;
                req.ModifiedId = userId;
                req.Active = true;
                req.Status = "UPDATED";
                req.IsAvailable = true;
                req.SupportRequestId = id;
                req.EmployeeId = assignEmp;

                _dbContext.Update(req);
                _dbContext.SaveChanges();

                if (req.ID > 0)
                {
                    SupportTimeLine timeLine = new SupportTimeLine();
                    timeLine.InsertedDate = DateTime.Now;
                    timeLine.InsertedId = userId;
                    timeLine.ModifiedDate = DateTime.Now;
                    timeLine.ModifiedId = userId;
                    timeLine.Active = true;
                    timeLine.Status = "REQUEST IS REASSIGNED";
                    timeLine.SupportRequestID = id;
                    timeLine.ReassignedToId = assignEmp;

                    _dbContext.SupportTimeLines.Add(timeLine);
                    _dbContext.SaveChanges();
                }
            }

            return RedirectToAction(nameof(SupportResolve));
        }

        public IActionResult SupportResolve(string Type, long deptemp,
            long[] OrgId,
             long[] DeptId, long[] SupportTypeId,
             long[] StatusId, long[] PriorityId, string fromdate, string todate)

        {

            CheckLoginStatus();
            TempData["FromDate"] = fromdate;
            TempData["ToDate"] = todate;
            ViewBag.DeptEmployee = GetallEmployeeByDeptId();
            var supportAssign = _dbContext.SupportRequestAssign.Where(a => a.EmployeeId == userId && a.Active == true).FirstOrDefault();

            if (supportAssign == null)
            {
                if (common.checkaccessavailable("Resolve", accessId, "Status Update", "Support", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
            }   

                SupportModelList model = new SupportModelList();


                List<long> pIds = new List<long>();
                List<long> statusIds = new List<long>();
                List<long> sTypeIds = new List<long>();
                List<long> deptIds = new List<long>();
                List<long> orgIds = new List<long>();


                model.Type = Type;
                model.OrgId = OrgId;
                model.DeptId = DeptId;
                model.SupportTypeId = SupportTypeId;
                model.StatusId = StatusId;
                model.PriorityId = PriorityId;

                if (PriorityId != null && PriorityId.Count() > 0)
                {

                    pIds.AddRange(PriorityId);
                }

                if (OrgId != null && OrgId.Count() > 0)
                {

                    orgIds.AddRange(OrgId);
                }

                if (DeptId != null && DeptId.Count() > 0)
                {

                    deptIds.AddRange(DeptId);

                }

                if (SupportTypeId != null && SupportTypeId.Count() > 0)
                {


                    sTypeIds.AddRange(SupportTypeId);

                }

                if (StatusId != null && StatusId.Count() > 0)
                {
                    statusIds.AddRange(StatusId);
                }


                model.priorities = (from a in supportRepository.GetAllPriority().ToList()
                                    select new NameIdViewModel
                                    {
                                        ID = a.ID,
                                        Name = a.Name

                                    }

                                    ).ToList();
                model.statuses = (from a in supportRepository.GetAllStatus().ToList()
                                  select new NameIdViewModel
                                  {
                                      ID = a.ID,
                                      Name = a.Name

                                  }

                                    ).ToList();
                List<NameIdViewModel> list = new List<NameIdViewModel>();

                List<SupportRequest> request = new List<SupportRequest>();
                bool deptlead = false;
                if (common.checkaccessavailable("Resolve", accessId, "Support Type", "Support", roleId) == true)
                {

                    NameIdViewModel o = new NameIdViewModel();
                    o.Name = "GroupType";
                    o.Status = "Group";

                    NameIdViewModel tw = new NameIdViewModel();
                    tw.Name = "OrganizationType";
                    tw.Status = "Organization";

                    NameIdViewModel th = new NameIdViewModel();
                    th.Name = "All";
                    th.Status = "All";


                    list.Add(th);
                    list.Add(o);
                    list.Add(tw);

                    model.Types = list;



                    if (Type == null || Type == "All")
                    {
                        request.AddRange(supportRepository.GetAllSupportRequest().ToList());
                    }
                    else
                    if (Type == "GroupType")
                    {
                        if (deptIds.Count() > 0)
                        {
                            if (sTypeIds.Count() > 0)
                            {
                                request.AddRange(supportRepository.GetAllBySupportTypeId(sTypeIds).ToList());
                            }
                            else
                            {
                                var supportTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds);

                                if (supportTypeIds != null && supportTypeIds.Count() > 0)
                                {
                                    request.AddRange(supportRepository.
                                        GetAllBySupportTypeId(supportTypeIds.ToList()).ToList());
                                }

                            }
                        }
                        else
                            request.AddRange(supportRepository.GetAllBySupportTypeId(null).ToList());
                    }
                    else
                    if (Type == "OrganizationType")
                    {

                        if (orgIds.Count() > 0)
                        {
                            if (deptIds.Count() > 0)
                            {
                                if (sTypeIds.Count() > 0)
                                {
                                    request.AddRange(supportRepository.
                                        GetAllBySupportTypeId(sTypeIds).ToList());
                                }
                                else
                                {
                                    var supportTypeIds = supportRepository.
                                        GetSupportTypeIdsByDepartmentIdIn(deptIds);

                                    if (supportTypeIds != null && supportTypeIds.Count() > 0)
                                    {
                                        request.AddRange(supportRepository.
                                            GetAllBySupportTypeId(supportTypeIds.ToList())
                                            .ToList());
                                    }

                                }
                            }
                            else
                            {

                                var allOrgDeptIds = departmentRepository.
                                    GetDepartmentListByOrganizationIdIn(orgIds).Select(a => a.ID);
                                if (allOrgDeptIds != null && allOrgDeptIds.Count() > 0)
                                {
                                    var supportTypeIds = supportRepository.
                                        GetSupportTypeIdsByDepartmentIdIn(allOrgDeptIds.Distinct().
                                        ToList());
                                    if (supportTypeIds != null && supportTypeIds.Count() > 0)
                                    {
                                        request.AddRange(supportRepository.
                                            GetAllBySupportTypeId(supportTypeIds.ToList())
                                            .ToList());

                                    }

                                }



                            }

                        }
                        else
                        {

                            var allOrgDeptIds = departmentRepository.GetAllDepartment().Where(a => a.GroupID == null || a.GroupID == 0).Select(a => a.ID);
                            if (allOrgDeptIds != null && allOrgDeptIds.Count() > 0)
                            {
                                var supportTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(allOrgDeptIds.Distinct().ToList());
                                if (supportTypeIds != null && supportTypeIds.Count() > 0)
                                {
                                    request.AddRange(supportRepository.GetAllBySupportTypeId(supportTypeIds.ToList()).ToList());

                                }

                            }
                        }

                    }

                }
                else
                {
                    if (common.checkaccessavailable("Department Head", accessId, "AccessResolve", "Support", roleId) == true)
                    {
                    deptlead = true;
                    NameIdViewModel th = new NameIdViewModel();
                    th.Name = "NoType";
                    th.Status = "NoType";
                    list.Add(th);
                    model.Types = list;
                    List<long> supportTypeIds = null;

                    var allDeptIds = common.GetDepartmentsByEmployeeID(userId).Select(a => a.id);
                    if (allDeptIds != null && allDeptIds.Count() > 0)
                    {
                        supportTypeIds = supportRepository.
                                GetSupportTypeIdsByDepartmentIdIn(allDeptIds.Distinct().
                                ToList()).ToList();
                        if (supportTypeIds != null && supportTypeIds.Count() > 0)
                        {


                            model.SupportTypes = (from a in
                                                      supportRepository.
                                                      GetAllSupportType().Where(a => supportTypeIds.Contains(a.ID)).ToList()
                                                  select new NameIdViewModel
                                                  {
                                                      ID = a.ID,
                                                      Name = a.Name

                                                  }

                                    ).ToList();

                        }

                    }

                    if (sTypeIds.Count() > 0)
                    {
                        request.AddRange(supportRepository.
                            GetAllBySupportTypeId(sTypeIds).ToList());
                    }
                    else
                    {

                        if (supportTypeIds != null && supportTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.
                                    GetAllBySupportTypeId(supportTypeIds)
                                    .ToList());
                        }


                    }
                    }

                   else                                 //if request is assiged to another employee by dept head
                   {
                    if (supportAssign != null)
                    {
                        ViewBag.RequestAssign = supportAssign.ID;
                        request.AddRange(supportRepository.GetAllSupportRequest().ToList());
                    }
                   }
                }




                if (request.Count() > 0)
                {

                    List<SupportRequest> filterList = new List<SupportRequest>();

                    //if (statusIds.Count() > 0 && pIds.Count() > 0)
                    //{
                    //    filterList.AddRange(request.Where(a => statusIds.Contains(a.StatusID) &&
                    //    pIds.Contains(a.PriorityID)));
                    //}
                    //else
                    if (pIds.Count() > 0)
                    {
                        filterList.AddRange(request.Where(a =>
                       pIds.Contains(a.PriorityID)));
                    }
                    //else
                    //if (statusIds.Count() > 0)
                    //{
                    //    filterList.AddRange(request.Where(a =>
                    //   statusIds.Contains(a.StatusID)));
                    //}
                    else
                        filterList.AddRange(request);

                    if (filterList.Count() > 0)
                    {

                        var cat = supportRepository.GetAllCategory().ToList();
                        var subcat = supportRepository.GetAllSubCategory().ToList();
                        var loc = supportRepository.GetAllLocation().ToList();
                        var build = supportRepository.GetAllBuilding();
                        var room = supportRepository.GetAllRoom();
                        var priorities = supportRepository.GetAllPriority().ToList();
                        var assignedTo = _dbContext.SupportRequestAssign.ToList();

                        List<SupportRequestCls> res = (from a in request
                                                       join b in cat on a.CategoryID equals b.ID
                                                       join c in subcat on a.SubCategoryID equals c.ID

                                                       join m in supportRepository.GetAllSupportType() on a.SupportTypeID equals m.ID

                                                       select new SupportRequestCls
                                                       {
                                                           ID = a.ID,
                                                           Title = a.Title,
                                                           TicketCode = a.TicketCode,
                                                           CategoryName = b.Name,
                                                           SubCategoryName = c.Name,
                                                           RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                           BuildingName = a.BuildingID != 0 ? build.Where(y => y.ID == a.BuildingID).FirstOrDefault().Name :
                                                           ViewConstants.NOT_APPLICABLE,
                                                           RoomName = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE,
                                                           PriorityName = priorities != null ? priorities.Where(x => x.ID == a.PriorityID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE,
                                                           StatusID = a.StatusID,
                                                           Status = a.StatusName,
                                                           OrgGroupName = common.GetOrgGroupName(a.InsertedId),
                                                           t_Date = a.DueDate != null ? a.DueDate : (a.RequestedCompletionDate == null ? a.InsertedDate : a.RequestedCompletionDate),
                                                           PriorityID = a.PriorityID,
                                                           AssignedToEmp = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId == userId ? "me" :
                                                                            employeeRepository.GetEmployeeFullNameById(assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId) : null : null,
                                                           AssignedDate = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                          assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().InsertedDate.ToString() : "NA" : "NA",
                                                           AssignToId = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                          assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId : 0 : 0,
                                                           emp_Name = common.GetEmployeeNameByEmployeeId(a.InsertedId, userId),
                                                           //emp_Phone = i.PrimaryMobile != null ? i.PrimaryMobile : "",
                                                           Description = a.Description,
                                                           SupportTypeID = a.SupportTypeID,
                                                           SupportTypeName = m.Name,
                                                           Comment = a.Comment,
                                                           InsertedDate = a.InsertedDate,
                                                           ModifiedDate = a.ModifiedDate,
                                                           InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                           SentApproval = a.SendApproval,
                                                           IsRead = a.IsRead == null ? false : a.IsRead,
                                                           IsManagementRead = a.IsManagementRead == null ? false : a.IsManagementRead,
                                                           DueDate = NullableDateTimeToStringDate(a.DueDate),

                                                           Management = (
                                       a.SendApproval,
                                        NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                         a.SentApprovalID != null && a.SentApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, userId) : ViewConstants.NOT_APPLICABLE,
                                       a.ManagementApproval,
                                       NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                       a.ManagementApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, userId) : ViewConstants.NOT_APPLICABLE,
                                       a.ManagementReject,
                                       NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                       a.ManagementRejectedID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, userId) : ViewConstants.NOT_APPLICABLE


                                       ),

                                                           commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, userId)

                                                       }
                                 ).ToList();

                        if (fromdate != null)
                        {
                            DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                            DateTime ToDate = System.DateTime.Now;
                            if (todate != null)
                            {
                                ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                            }
                            if (fromdate != null && todate != null)
                            {
                                res = res.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                            }
                            if (fromdate != null && todate == null)
                            {
                                res = res.Where(a => a.InsertedDate >= FromDate).ToList();
                            }
                        }
                        if(deptemp > 0)
                        {
                        res = res.Where(a => a.AssignToId == deptemp).ToList();
                        }

                        if (pIds.Count > 0)
                        {
                            res = res.Where(a => pIds.Contains(a.PriorityID)).ToList();
                        }
                    if (deptlead == false)
                    {
                        if (supportAssign != null)
                        {
                            res = res.Where(a => a.AssignToId == userId).ToList();
                        }
                    }
                        model.SupportRequestcls = res;
                        HttpContext.Session.SetString("supportresolve", JsonConvert.SerializeObject(res));
                    }


                }
            
           

            return View(model);



        }
        public IActionResult statuschangetoinprogress(long id)
        {
            var request = supportRepository.GetBySupportRequestId(id);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            request.ModifiedId = userId;
            request.StatusID = 3;
            request.StatusName = "IN-PROGRESS";
            supportRepository.UpdateSupportRequest(request);

            var employees = employeeRepository.GetEmployeeById(request.InsertedId);

            string name = employees.FirstName + " " + employees.LastName;

            var empname = employeeRepository.GetEmployeeById(userId);
            string employeename = empname.FirstName + " " + empname.LastName;
            string empcode = employees.EmpCode;

            smssend.inprogresssupport(empcode, request.Description, name, employeename, "Support Update-" + request.Title + "-" + name, employees.EmailId);


            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.Status = "IN-PROGRESS";

            supportTimeLine.SupportRequestID = id;
            supportTimeLine.SupportStatusID = 3;
            supportRepository.CreateSupportTimeLine(supportTimeLine);
            return RedirectToAction(nameof(SupportResolve));
        }

        [HttpPost]
        public JsonResult statuschangetoonhold(long id, string reason)
        {
            var request = supportRepository.GetBySupportRequestId(id);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            request.ModifiedId = userId;
            request.StatusID = 4;
            request.Comment = reason;
            request.StatusName = "ON-HOLD";
            supportRepository.UpdateSupportRequest(request);

            var employees = employeeRepository.GetEmployeeById(request.InsertedId);

            string name = employees.FirstName + " " + employees.LastName;

            var empname = employeeRepository.GetEmployeeById(userId);
            string employeename = empname.FirstName + " " + empname.LastName;
            string empcode = employees.EmpCode;

            smssend.onholdsupport(reason, empcode, request.Description, name, employeename, "Support Update-" + request.Title + "-" + name, employees.EmailId);

            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.Status = "ON-HOLD";
            supportTimeLine.comment = reason;
            supportTimeLine.SupportRequestID = id;
            supportTimeLine.SupportStatusID = 4;
            supportRepository.CreateSupportTimeLine(supportTimeLine);
            return Json(1);
        }
        public JsonResult statuschangetoreject(long id, string reason)
        {
            var request = supportRepository.GetBySupportRequestId(id);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            request.ModifiedId = userId;
            request.RejectedID = userId;
            request.Comment = reason;
            request.RejectDate = DateTime.Now;
            request.IsReject = true;
            request.StatusID = 5;
            request.StatusName = "REJECTED";
            supportRepository.UpdateSupportRequest(request);

            var employees = employeeRepository.GetEmployeeById(request.InsertedId);

            string name = employees.FirstName + " " + employees.LastName;

            var empname = employeeRepository.GetEmployeeById(userId);
            string employeename = empname.FirstName + " " + empname.LastName;
            string empcode = employees.EmpCode;

            smssend.rejectsupport(reason, empcode, request.Description, name, employeename, "Support Update-" + request.Title + "-" + name, employees.EmailId);


            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.Status = "REJECTED";
            supportTimeLine.comment = reason;
            supportTimeLine.SupportRequestID = id;
            supportTimeLine.SupportStatusID = 5;
            supportRepository.CreateSupportTimeLine(supportTimeLine);
            return Json(1);
        }

        public JsonResult Managementstatuschangetoreject(long id, string reason)
        {
            var request = supportRepository.GetBySupportRequestId(id);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            request.ModifiedId = userId;
            request.ManagementRejectedID = userId;
            request.ManagementRejectDate = DateTime.Now;
            request.ManagementReject = true;
            request.ManagementApprovalID = 0;
            request.ManagementApprovedDate = null;
            request.ManagementApproval = false;
            request.Comment = reason;
            //request.StatusID = 6;

            //request.StatusName = "MANAGEMENT REJECTED";
            supportRepository.UpdateSupportRequest(request);
            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.Status = "MANAGEMENT REJECTED";
            supportTimeLine.comment = reason;
            supportTimeLine.SupportRequestID = id;

            supportRepository.CreateSupportTimeLine(supportTimeLine);

            var Department_Id = supportRepository.GetBySupportTypeId(request.SupportTypeID).DepartmentID;



            List<long> DesignationIdes = designationRepository.GetDesignationByDepartmentId(Department_Id).ToList();

            //Employee Head Details
            List<DepartmentLead> employeeLeadid = departmentLeadRepository.GetAllEmployeeIdByDepartment(Department_Id).ToList();

            List<long> employeeLeadid_Emplyee_Ides = employeeLeadid.Select(e => e.EmployeeID).ToList();
            // Department Employee Details
            List<EmployeeDesignation> EmployeeIdes = employeeDesignationRepository.GetAllEmployeeDesignationsByDesignationId(DesignationIdes).ToList();
            List<long> Department_Emplyee_Ides = EmployeeIdes.Select(e => e.EmployeeID).ToList();

            foreach (var item in employeeLeadid_Emplyee_Ides) // remove department head employee id from employee list
            {
                Department_Emplyee_Ides.Remove(item);
            }

            //Fire Email to Email Head

            var empname = employeeRepository.GetEmployeeById(userId);
            string employeename = empname.FirstName + " " + empname.LastName;
            foreach (var items in employeeLeadid_Emplyee_Ides)
            {

                long emp_id = items;
                Employee employees = employeeRepository.GetEmployeeById(emp_id);
                string name = employees.FirstName + " " + employees.LastName;
                string code = employees.EmpCode;
                string email = employees.EmailId;
                string phone = employees.PrimaryMobile;
                string category = "Employee";
                string whom = "Employee";
                string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                var description = supportRepository.GetBySupportRequestId(request.ID);

                smssend.rejectsupport(reason, code, request.Description, name, employeename, "Support Rejected-" + request.Title + "-" + name, employees.EmailId);

            }


            //Fire Email to Department Employees




            foreach (var item in Department_Emplyee_Ides)
            {

                long emp_id = item;
                Employee employees = employeeRepository.GetEmployeeById(emp_id);
                string name = employees.FirstName + " " + employees.LastName;
                string code = employees.EmpCode;
                string email = employees.EmailId;
                string phone = employees.PrimaryMobile;
                string category = "Employee";
                string whom = "Employee";
                string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                var description = supportRepository.GetBySupportRequestId(request.ID);

                smssend.rejectsupport(reason, code, request.Description, name, employeename, "Support Rejected-" + request.Title + "-" + name, employees.EmailId);

            }

            return Json(1);
        }

        public IActionResult CreateOrEditSupportResolve(long? id)
        {
            CheckLoginStatus();
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            ViewBag.Id = id;

            if (common.checkaccessavailable("Resolve", accessId, "Status Update", "Support", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            SupportRequestCls od = new SupportRequestCls();

            od.IsAvailableLocation = (supportRepository.GetAllSupportType().FirstOrDefault().IsAvailable);

            od.supportTypes = (from a in supportRepository.GetAllSupportType()
                               where a.IsAvailable == true
                               select new SupportType
                               {
                                   ID = a.ID,
                                   Name = a.Name
                               }

                              ).ToList();

            od.category = (from a in supportRepository.GetAllCategory()
                           where a.IsAvailable == true
                           select new SupportCategoryCls
                           {
                               ID = a.ID,
                               Name = a.Name
                           }

                              ).ToList();

            od.location = (from b in supportRepository.GetAllLocation()
                           where b.IsAvailable == true
                           select new LocationCls
                           {
                               ID = b.ID,
                               Name = b.Name
                           }
                         ).ToList();

            od.priority = (from c in supportRepository.GetAllPriority()
                           where c.IsAvailable == true
                           select new SupportMasterModel
                           {
                               ID = c.ID,
                               Name = c.Name
                           }
                         ).ToList();

            if (id == null)
            {

                ViewBag.status = "Create";
                od.ID = 0;
                HttpContext.Session.Remove("attachments2");

            }
            else
            {
                CheckLoginStatus();
                //long roleId = HttpContext.Session.GetInt32("roleId").Value;
                //long accessId = HttpContext.Session.GetInt32("accessId").Value;

                if (common.checkaccessavailable("Resolve", accessId, "Status Update", "Support", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }

                long userId = HttpContext.Session.GetInt32("userId").Value;

                od.subcategory = (from b in supportRepository.GetAllSubCategory()
                                  where b.IsAvailable == true
                                  select new SupportSubCategoryCls
                                  {
                                      ID = b.ID,
                                      Name = b.Name
                                  }
                         ).ToList();

                od.buildings = (from b in supportRepository.GetAllBuilding()
                                where b.IsAvailable == true
                                select new BuildingCls
                                {
                                    ID = b.ID,
                                    Name = b.Name
                                }
                         ).ToList();

                var room = supportRepository.GetAllRoom();
                var floor = supportRepository.GetAllFloor();

                od.room = (from b in room
                           join c in floor on b.FloorID equals c.ID
                           where b.IsAvailable == true
                           select new RoomCls
                           {
                               ID = b.ID,
                               Name = b.Name + " " + "(" + c.Name + ")",
                               FloorName = c.Name

                           }
                        ).ToList();

                od.statuses = (from c in supportRepository.GetAllStatus()
                               where c.IsAvailable == true && c.ID != 1
                               select new SupportStatus
                               {
                                   ID = c.ID,
                                   Name = c.Name
                               }
                             ).ToList();

                ViewBag.status = "Update";
                var request = supportRepository.GetBySupportRequestId(id.Value);
                od.ID = request.ID;
                od.CategoryID = request.CategoryID;
                od.SubCategoryID = request.SubCategoryID;
                od.Title = request.Title;
                od.TicketCode = request.TicketCode;
                od.LocationID = request.LocationID;
                od.IsApprove = request.IsApprove;
                od.RoomID = request.RoomID;
                od.PriorityID = request.PriorityID;
                od.BuildingID = request.BuildingID;
                od.Description = request.Description;
                od.SupportTypeID = request.SupportTypeID;
                od.Comment = request.Comment;
                od.comments = commonMethods.GetSupportComments(id.Value, userId).Take(4).ToList();
                // od.attachements = commonMethods.GetSupportAttachments(id.Value).OrderByDescending(a => a.id).Take(4).ToList();
                od.CompletionDate = request.RequestedCompletionDate != null ? (request.RequestedCompletionDate.Value.ToString("yyyy-MM-dd")) : "";
                od.DueDate = (request.DueDate) != null ? (request.DueDate.Value.ToString("yyyy-MM-dd")) : "";
                od.InsertedID = request.InsertedId;
                od.UserID = userId;
                od.Status = request.StatusName;

                //if (request.InsertedId == userId)
                //{
                var comments = supportRepository.GetAllSupportComment().Where(a => a.SupportRequestID == request.ID && a.InsertedId != userId && a.IsRead == false).ToList();
                foreach (var a in comments)
                {
                    SupportComment comment = a;
                    comment.IsRead = true;
                    comment.ModifiedId = userId;
                    supportRepository.UpdateSupportComment(comment);
                }
                //  }

                List<SupportRequestattachments> attachments = new List<SupportRequestattachments>();

                od.Attachments = supportRepository.GetAllAttachment().Where(a => a.SupportRequestID == id.Value || a.Type == "RAISED").ToList();

                HttpContext.Session.SetString("attachments", JsonConvert.SerializeObject(od.Attachments));

                od.Attachments2 = supportRepository.GetAllAttachment().Where(a => a.SupportRequestID == id.Value || a.Type == "COMMENT").ToList();

                HttpContext.Session.SetString("attachments2", JsonConvert.SerializeObject(od.Attachments2));
            }

            return View(od);
        }

        [HttpPost]
        public async Task<JsonResult> SaveSupportAttachmentResolve(string Name, IFormFile file)
        {
            List<SupportResolveattachments> attachments = new List<SupportResolveattachments>();
            if (HttpContext.Session.GetString("attachments2") != null)
            {
                attachments = JsonConvert.DeserializeObject<List<SupportResolveattachments>>(HttpContext.Session.GetString("attachments2"));
            }
            SupportResolveattachments attach = new SupportResolveattachments();

            if (file != null)
            {
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = "File" + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\Tempfile\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                attach.path = path;
                attach.file = "/ODMImages/Tempfile/" + pathToSave;
            }

            var webPathnew = hostingEnvironment.WebRootPath;
            string pathnew = Path.Combine("", webPathnew + @"\ODMImages\Tempfile\");
            foreach (var item in attachments)
            {
                if (item.file == null)
                    item.file = "/ODMImages/Tempfile/" + item.path;
                else
                if (item.file.IndexOf("Tempfile") == 0)
                    item.file = "/ODMImages/Tempfile/" + item.path;

                if (item.path == null)
                    item.path = pathnew + item.path;
                else
                if (item.path.IndexOf("Tempfile") == 0)
                    item.path = pathnew + item.path;
            }


            attach.Name = "File";
            attach.id = attachments.Count() + 1;
            attachments.Add(attach);
            HttpContext.Session.SetString("attachments2", JsonConvert.SerializeObject(attachments));
            return Json(attachments.Where(a => a.isremove == false).ToList());
        }
        public async Task<JsonResult> RemoveSupportAttachmentResolve(int id)
        {
            List<SupportResolveattachments> attachments = new List<SupportResolveattachments>();
            if (HttpContext.Session.GetString("attachments2") != null)
            {
                attachments = JsonConvert.DeserializeObject<List<SupportResolveattachments>>(HttpContext.Session.GetString("attachments2"));
            }

            SupportResolveattachments re = attachments.Where(a => a.id == id).FirstOrDefault();
            re.isremove = true;

            HttpContext.Session.SetString("attachments2", JsonConvert.SerializeObject(attachments));
            return Json(attachments.Where(a => a.isremove == false).ToList());
        }

        [HttpPost]
        public async Task<JsonResult> SaveSupportResolve(SupportRequest supportRequests)
        {
            try
            {
                long userId = HttpContext.Session.GetInt32("userId").Value;


                SupportRequest requests = supportRepository.GetBySupportRequestId(supportRequests.ID);

                //SupportComment comments = new SupportComment();

                ////SupportComment supportcomments= supportRepository.GetBySupportCommentId(supportRequests.ID);


                //comments.SupportRequestID = supportRequests.ID;
                //comments.InsertedId = userId;
                //comments.ModifiedId = userId;
                //comments.Active = true;
                //comments.Name = supportRequests.Comment;
                //// comments.Status = supportRequests.StatusID == 0 ? "APPROVED" : supportRepository.GetByStatusId(supportRequests.StatusID).Name;
                //long cmntid = supportRepository.CreateSupportComment(comments);

                List<SupportResolveattachments> attachments = new List<SupportResolveattachments>();
                if (HttpContext.Session.GetString("attachments2") != null)
                {
                    attachments = JsonConvert.DeserializeObject<List<SupportResolveattachments>>(HttpContext.Session.GetString("attachments2"));
                }


                if (attachments.Count() > 0)
                {
                    foreach (var a in attachments)
                    {

                        string getinfo = Path.GetFileName(a.path);
                        string filepath = hostingEnvironment.WebRootPath + "\\ODMImages\\Tempfile\\" + requests.ID;
                        SupportAttachment Attachment = new SupportAttachment();
                        if (!Directory.Exists(filepath))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(filepath);
                        }


                        if (!System.IO.File.Exists(filepath + "\\" + getinfo))   // New Attachment
                        {
                            Attachment.SupportRequestID = requests.ID;
                            Attachment.ModifiedId = userId;
                            Attachment.Name = a.Name;
                            Attachment.Path = getinfo;

                            if (a.path != null && a.isremove == false)
                            {
                                Attachment.Active = true;
                                Attachment.InsertedId = userId;
                                Attachment.Status = EntityStatus.ACTIVE;
                                Attachment.Type = "COMMENT";
                                supportRepository.CreateAttachment(Attachment);

                                if (!System.IO.File.Exists(filepath + "\\" + getinfo)) // id file already exist then no need to copy again to folder
                                    System.IO.File.Copy(a.path, filepath + "\\" + getinfo);
                            }

                        }
                        else   // Edited or Deleted 
                        {
                            Attachment = supportRepository.GetAllAttachment().Where(m => m.ID == a.id || m.Type == "COMMENT").FirstOrDefault();

                            if (a.path != null && a.isremove == true)
                            {
                                Attachment.Active = false;
                                Attachment.InsertedId = userId;
                                Attachment.Status = EntityStatus.DELETED;
                                Attachment.Type = "COMMENT";
                                supportRepository.UpdateAttachment(Attachment);

                                if (System.IO.File.Exists(filepath + "\\" + getinfo))
                                {
                                    System.IO.File.Delete(filepath + "\\" + getinfo);
                                }
                            }
                        }


                    }
                    HttpContext.Session.Remove("attachments2");
                }

                //if (requests.StatusID == 2)
                //{
                //    var employees = employeeRepository.GetEmployeeById(requests.InsertedId);

                //    var empid = departmentLeadRepository.GetAllByEmployeeId(employees.ID);

                //    string name = employees.FirstName + " " + employees.LastName;
                //    string code = employees.EmpCode;
                //    string email = employees.EmailId;
                //    string phone = employees.PrimaryMobile;
                //    string category = "Employee";
                //    string whom = "Employee";

                //    var empname = employeeRepository.GetEmployeeById(userId);
                //    string employeename = empname.FirstName + " " + empname.LastName;

                //    smssend.resolvesupport(requests.ID.ToString(), requests.Description, name, employeename, "Support Resolved", employees.EmailId);
                //}


                return Json(1);
            }
            catch (Exception e1)
            {
                return Json(0);
            }
        }

        public IActionResult ViewSupportResolveDetails(long id)
        {
            ViewBag.DeptEmployees = GetallEmployeeByDeptId();
            var res = ViewSupportRequestDet(id); 

            return View(res);
        }

        public IActionResult ApprovalFromManagement(long id)
        {
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            //if (commonMethods.checkaccessavailable("Management Approval", accessId, "List", "Support", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            var request = supportRepository.GetBySupportRequestId(id);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            request.SentApprovalID = userId;

            request.SendApproval = true;

            request.SentApprovalDate = DateTime.Now;

            supportRepository.UpdateSupportRequest(request);

            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.SupportRequestID = request.ID;
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.Status = "SENT FOR MANAGEMENT APPROVAL";
            supportRepository.CreateSupportTimeLine(supportTimeLine);
            var employees = commonMethods.GetSupportManagementApprovalemployee();
            foreach (var a in employees)
            {
                Employee employee = employeeRepository.GetEmployeeById(a.Id);
                string name = employee.FirstName + " " + employee.LastName;
                string code = employee.EmpCode;
                string email = employee.EmailId;
                string phone = employee.PrimaryMobile;
                string category = "Employee";
                string whom = "Employee";
                string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                var requestdet = supportRepository.GetBySupportRequestId(request.ID);
                string description = requestdet.Description;

                smssend.managementApprovalsupport(requestdet.Title, description, name, email, phone, code, category, typename, "Management Approval Needed For Ticket Number : " + requestdet.TicketCode + "-" + requestdet.Title + "-" + name, whom, employee.EmailId);

            }
            return RedirectToAction(nameof(SupportResolve));
        }
        public IActionResult EditFromManagement(long id)
        {
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            //if (commonMethods.checkaccessavailable("Management Approval", accessId, "List", "Support", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            var request = supportRepository.GetBySupportRequestId(id);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            request.SentApprovalID = 0;
            request.SentApprovalDate = null;

            request.SendApproval = false;
            supportRepository.UpdateSupportRequest(request);

            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.SupportRequestID = request.ID;
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.Status = "TOOK MANAGEMENT APPROVAL BACK";
            supportRepository.CreateSupportTimeLine(supportTimeLine);

            return RedirectToAction(nameof(SupportResolve));
        }
        public class Empclass
        {
            public long ID { get; set; }
            public string FullName { get; set; }
        }
        public List<Empclass> GetAllEmployeeName()
        {
            var emp = employeeRepository.GetAllEmployee().Select(a => new Empclass { ID = a.ID, FullName = a.FirstName + " " + a.LastName }).ToList();
            return emp;
        }
        public JsonResult GetEmployeeByDeptId(long id)
        { 
            var empList = common.GetAllDeptwithEmployee().Where(a => a.DepartmentID == id).ToList();
            //var emp = employeeRepository.get.GetAllEmployee();
            var res = (from a in empList
                       select new EmpClassModal
                       {
                           ID = a.EmployeeID,
                           EmpName = a.EmployeeID == userId ? "me" : employeeRepository.GetEmployeeFullNameById(a.EmployeeID),
                           DeptId = a.DepartmentID,
                           DeptName = a.DepartmentName
                       }).ToList();

            return Json(res);
        }
        public async Task<IActionResult> SupportManagementApproval(
            string Type,
            long[] OrgId,
             long[] DeptId, long[] SupportTypeId,
             long[] StatusId, long[] PriorityId, string fromdate, string todate,long? deptemp)
        {
            CheckLoginStatus();
            TempData["FromDate"] = fromdate;
            TempData["ToDate"] = todate;
            ViewBag.Employees = GetAllEmployeeName();

            SupportModelList model = new SupportModelList();

            if (common.checkaccessavailable("Management Approval", accessId, "List", "Support", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }


            List<long> pIds = new List<long>();
            List<long> statusIds = new List<long>();
            List<long> sTypeIds = new List<long>();
            List<long> deptIds = new List<long>();
            List<long> orgIds = new List<long>();


            model.Type = Type;
            model.OrgId = OrgId;
            model.DeptId = DeptId;
            model.SupportTypeId = SupportTypeId;
            model.StatusId = StatusId;
            model.PriorityId = PriorityId;

            if (PriorityId != null && PriorityId.Count() > 0)
            {
                // string[] spIds= Priority.Split(",");

                // foreach(string s in spIds)
                // {
                //  pIds.Add(Convert.ToInt64(s));
                // }



                pIds.AddRange(PriorityId);
            }

            if (OrgId != null && OrgId.Count() > 0)
            {
                //string[] spIds = OrgId.Split(",");

                //foreach (string s in spIds)
                //{
                //    orgIds.Add(Convert.ToInt64(s));
                //}
                orgIds.AddRange(OrgId);
            }

            if (DeptId != null && DeptId.Count() > 0)
            {
                //string[] spIds = DeptId.Split(",");

                //foreach (string s in spIds)
                //{
                //    deptIds.Add(Convert.ToInt64(s));
                //}
                deptIds.AddRange(DeptId);

            }

            if (SupportTypeId != null && SupportTypeId.Count() > 0)
            {
                //string[] spIds = SuppotyTypeId.Split(",");

                //foreach (string s in spIds)
                //{
                //    sTypeIds.Add(Convert.ToInt64(s));
                //}

                sTypeIds.AddRange(SupportTypeId);

            }

            if (StatusId != null && StatusId.Count() > 0)
            {
                //string[] spIds = Status.Split(",");

                //foreach (string s in spIds)
                //{
                //    statusIds.Add(Convert.ToInt64(s));
                //}

                statusIds.AddRange(StatusId);
            }


            model.priorities = (from a in supportRepository.GetAllPriority().ToList()
                                select new NameIdViewModel
                                {
                                    ID = a.ID,
                                    Name = a.Name

                                }

                                ).ToList();
            model.statuses = (from a in supportRepository.GetAllStatus().ToList()
                              select new NameIdViewModel
                              {
                                  ID = a.ID,
                                  Name = a.Name

                              }

                                ).ToList();




            List<NameIdViewModel> list = new List<NameIdViewModel>();

            List<SupportRequest> request = new List<SupportRequest>();

            //if (commonMethods.checkaccessavailable("Management Approval", accessId, "Support Type", "Support", roleId) == false)
            //{


            NameIdViewModel o = new NameIdViewModel();
            o.Name = "GroupType";
            o.Status = "Group";

            NameIdViewModel tw = new NameIdViewModel();
            tw.Name = "OrganizationType";
            tw.Status = "Organization";

            NameIdViewModel th = new NameIdViewModel();
            th.Name = "All";
            th.Status = "All";


            list.Add(th);
            list.Add(o);
            list.Add(tw);

            model.Types = list;

            if (Type == null || Type == "All")
            {
                request.AddRange(supportRepository.GetApprovalSupportRequestBySupportTypeId(null).ToList());
            }
            else
            if (Type == "GroupType")
            {
                if (deptIds.Count() > 0)
                {
                    if (sTypeIds.Count() > 0)
                    {
                        request.AddRange(supportRepository.GetApprovalSupportRequestBySupportTypeId(sTypeIds).ToList());
                    }
                    else
                    {
                        var supportTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds);

                        if (supportTypeIds != null && supportTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.
                                GetApprovalSupportRequestBySupportTypeId(supportTypeIds.ToList()).ToList());
                        }

                    }
                }
                else
                    request.AddRange(supportRepository.GetApprovalSupportRequestBySupportTypeId(null).ToList());
            }
            else
            if (Type == "OrganizationType")
            {

                if (orgIds.Count() > 0)
                {
                    if (deptIds.Count() > 0)
                    {
                        if (sTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.
                                GetApprovalSupportRequestBySupportTypeId(sTypeIds).ToList());
                        }
                        else
                        {
                            var supportTypeIds = supportRepository.
                                GetSupportTypeIdsByDepartmentIdIn(deptIds);

                            if (supportTypeIds != null && supportTypeIds.Count() > 0)
                            {
                                request.AddRange(supportRepository.
                                    GetApprovalSupportRequestBySupportTypeId(supportTypeIds.ToList())
                                    .ToList());
                            }

                        }
                    }
                    else
                    {

                        var allOrgDeptIds = departmentRepository.
                            GetDepartmentListByOrganizationIdIn(orgIds).Select(a => a.ID);
                        if (allOrgDeptIds != null && allOrgDeptIds.Count() > 0)
                        {
                            var supportTypeIds = supportRepository.
                                GetSupportTypeIdsByDepartmentIdIn(allOrgDeptIds.Distinct().
                                ToList());
                            if (supportTypeIds != null && supportTypeIds.Count() > 0)
                            {
                                request.AddRange(supportRepository.
                                    GetApprovalSupportRequestBySupportTypeId(supportTypeIds.ToList())
                                    .ToList());

                            }

                        }



                    }

                }
                else
                {

                    var allOrgDeptIds = departmentRepository.GetAllDepartment().Where(a => a.GroupID == null || a.GroupID == 0).Select(a => a.ID);
                    if (allOrgDeptIds != null && allOrgDeptIds.Count() > 0)
                    {
                        var supportTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(allOrgDeptIds.Distinct().ToList());
                        if (supportTypeIds != null && supportTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.GetApprovalSupportRequestBySupportTypeId(supportTypeIds.ToList()).ToList());

                        }

                    }
                }

            }

            //}
            //else
            //{

            //    NameIdViewModel th = new NameIdViewModel();
            //    th.Name = "NoType";
            //    th.Status = "NoType";
            //    list.Add(th);
            //    model.Types = list;



            //}


            if (request.Count() > 0)
            {


                List<SupportRequest> filterList = new List<SupportRequest>();

                if (statusIds.Count() > 0 && pIds.Count() > 0)
                {
                    filterList.AddRange(request.Where(a => statusIds.Contains(a.StatusID) &&
                    pIds.Contains(a.PriorityID)));
                }
                else
                if (pIds.Count() > 0)
                {
                    filterList.AddRange(request.Where(a =>
                   pIds.Contains(a.PriorityID)));
                }
                else
                if (statusIds.Count() > 0)
                {
                    filterList.AddRange(request.Where(a =>
                   statusIds.Contains(a.StatusID)));
                }
                else
                    filterList.AddRange(request);

                if (filterList.Count() > 0)
                {


                    var cat = supportRepository.GetAllCategory().ToList();
                    var subcat = supportRepository.GetAllSubCategory().ToList();
                    var loc = supportRepository.GetAllLocation().ToList();
                    var build = supportRepository.GetAllBuilding();
                    var room = supportRepository.GetAllRoom();
                    var prior = supportRepository.GetAllPriority().ToList();
                    var supptype = supportRepository.GetAllSupportType().ToList();
                    var assignTo = _dbContext.SupportRequestAssign.ToList();


                    List<SupportRequestCls> res = (from a in filterList
                                                   join b in cat on a.CategoryID equals b.ID
                                                   join c in subcat on a.SubCategoryID equals c.ID
                                                   join g in prior on a.PriorityID equals g.ID
                                                   //join i in emp on a.InsertedId equals i.ID
                                                   join m in supptype on a.SupportTypeID equals m.ID
                                                   //join n in det on m.DepartmentID equals n.ID
                                                   select new SupportRequestCls
                                                   {
                                                       ID = a.ID,
                                                       CategoryName = b.Name,
                                                       TicketCode = a.TicketCode,
                                                       Title = a.Title,
                                                       SubCategoryName = c.Name,
                                                       RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                       BuildingName = a.BuildingID != 0 ? build.Where(y => y.ID == a.BuildingID).FirstOrDefault().Name :
                                                       ViewConstants.NOT_APPLICABLE,
                                                       RoomName = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE,
                                                       PriorityName = g.Name,
                                                       StatusID = a.StatusID,
                                                       ModifiedDate = a.ModifiedDate,
                                                       Status = a.StatusName,
                                                       t_Date = a.InsertedDate,
                                                       PriorityID = a.PriorityID,
                                                       OrgGroupName = common.GetOrgGroupName(a.InsertedId),
                                                       emp_Name = common.GetEmployeeNameByEmployeeId(a.InsertedId, userId),
                                                       //emp_Phone = i.PrimaryMobile != null ? i.PrimaryMobile : "",
                                                       Description = a.Description,
                                                       SupportTypeID = a.SupportTypeID,
                                                       SupportTypeName = m.Name,
                                                       IsManagementRead = a.IsManagementRead == null ? false : a.IsManagementRead,
                                                       InsertedDate = a.InsertedDate,
                                                       InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                       DueDate = NullableDateTimeToStringDate(a.DueDate),
                                                       Comment = a.Comment,
                                                       AssignedToEmp = assignTo != null ? assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                        employeeRepository.GetEmployeeFullNameById(assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId) : null : null,
                                                       AssignedDate = assignTo != null ? assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                      assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().InsertedDate.ToString() : "NA" : "NA",
                                                       AssignToId = assignTo != null ? assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                                                          assignTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId : 0 : 0,

                                                       Management = (
                                   a.SendApproval,
                                    NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                     a.SentApprovalID != null && a.SentApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, userId) : "NA",
                                   a.ManagementApproval,
                                   NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                   a.ManagementApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, userId) : "NA",
                                   a.ManagementReject,
                                   NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                   a.ManagementRejectedID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, userId) : "NA"
                                   )
                                                   }
                             ).ToList();

                    if (fromdate != null)
                    {
                        DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                        DateTime ToDate = System.DateTime.Now;
                        if (todate != null)
                        {
                            ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                        }
                        if (fromdate != null && todate != null)
                        {
                            res = res.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                        }
                        if (fromdate != null && todate == null)
                        {
                            res = res.Where(a => a.InsertedDate >= FromDate).ToList();
                        }
                    }
                    if(deptemp != null)
                    {
                        res = res.Where(a => a.AssignToId == deptemp).ToList();
                    }
                    model.SupportRequestcls = res;
                    HttpContext.Session.SetString("supportmanagementresolve", JsonConvert.SerializeObject(res));

                }
            }

            return View(model);
        }

        public IActionResult ManagementApproval(long id)
        {
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            //if (commonMethods.checkaccessavailable("Management Approval", accessId, "List", "Support", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            var request = supportRepository.GetBySupportRequestId(id);
            long userId = HttpContext.Session.GetInt32("userId").Value;



            request.ManagementApprovedDate = DateTime.Now;
            request.ManagementApprovalID = userId;
            request.ManagementApproval = true;
            request.Comment = null;

            request.ManagementReject = false;
            request.ManagementRejectDate = null;
            request.ManagementRejectedID = 0;


            supportRepository.UpdateSupportRequest(request);

            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.SupportRequestID = request.ID;
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.SupportStatusID = 0;
            supportTimeLine.Status = "MANAGEMENT APPROVED";
            supportRepository.CreateSupportTimeLine(supportTimeLine);

            return RedirectToAction(nameof(SupportManagementApproval));
        }

        public JsonResult StatusUpdateGrid(long id, long statusid)
        {
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            var request = supportRepository.GetBySupportRequestId(id);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            request.StatusID = statusid;
            request.StatusName = supportRepository.GetByStatusId(statusid).Name;
            request.ModifiedId = userId;
            request.Comment = null;
            if (statusid == 2 && request.StatusName == "COMPLETED")
            {
                request.CompletionDate = DateTime.Now;
            }
            else


            {
                request.CompletionDate = null;
            }


            supportRepository.UpdateSupportRequest(request);

            var employees = employeeRepository.GetEmployeeById(request.InsertedId);

            string name = employees.FirstName + " " + employees.LastName;

            var empname = employeeRepository.GetEmployeeById(userId);
            string employeename = empname.FirstName + " " + empname.LastName;
            string empcode = employees.EmpCode;

            if (request.StatusID == 2)
            {
                smssend.resolvesupport(empcode, request.Description, name, employeename, "Support Resolved-" + request.Title + "-" + name, employees.EmailId);
            }

            if (request.StatusID == 3)
            {
                smssend.inprogresssupport(empcode, request.Description, name, employeename, "Support Update-" + request.Title + "-" + name, employees.EmailId);
            }
            if (request.StatusName == "COMPLETED")
            {
                string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;

                smssend.askforverifySupport(request.Title, request.Description, name, employees.EmailId, employees.PrimaryMobile, request.TicketCode, "Employee", typename, "Support Request Completed-" + request.Title + "-" + employeename, "Employee", employees.EmailId, employeename);
            }
            if (request.StatusName == "VERIFIED")
            {
                var Department_Id = supportRepository.GetBySupportTypeId(request.SupportTypeID).DepartmentID;

                List<long> DesignationIdes = designationRepository.GetDesignationByDepartmentId(Department_Id).ToList();

                //Employee Head Details
                List<DepartmentLead> employeeLeadid = departmentLeadRepository.GetAllEmployeeIdByDepartment(Department_Id).ToList();

                List<long> employeeLeadid_Emplyee_Ides = employeeLeadid.Select(e => e.EmployeeID).ToList();
                // Department Employee Details
                List<EmployeeDesignation> EmployeeIdes = employeeDesignationRepository.GetAllEmployeeDesignationsByDesignationId(DesignationIdes).ToList();
                List<long> Department_Emplyee_Ides = EmployeeIdes.Select(e => e.EmployeeID).ToList();
                var raisedName = employees.FirstName + " " + employees.LastName;
                foreach (var item in employeeLeadid_Emplyee_Ides) // remove department head employee id from employee list
                {
                    Department_Emplyee_Ides.Remove(item);
                }

                foreach (var items in employeeLeadid_Emplyee_Ides)
                {

                    long emp_id = items;

                    employees = employeeRepository.GetEmployeeById(emp_id);
                    name = employees.FirstName + " " + employees.LastName;
                    string code = employees.EmpCode;
                    string email = employees.EmailId;
                    string phone = employees.PrimaryMobile;
                    string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                    var description = supportRepository.GetBySupportRequestId(request.ID);

                    smssend.Verifysupport(request.ID.ToString(), description.Description, raisedName, raisedName, "Verify Support Request By Employee-" + description.Title + "-" + name, email);

                }


                //Fire Email to Department Employees

                foreach (var item in Department_Emplyee_Ides)
                {

                    long emp_id = item;
                    employees = employeeRepository.GetEmployeeById(emp_id);
                    name = employees.FirstName + " " + employees.LastName;
                    string code = employees.EmpCode;
                    string email = employees.EmailId;
                    string phone = employees.PrimaryMobile;
                    string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                    var description = supportRepository.GetBySupportRequestId(request.ID);

                    smssend.Verifysupport(request.ID.ToString(), description.Description, raisedName, raisedName, "Verify Support Request By Employee-" + description.Title + "-" + name, email);

                }
            }
            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.SupportRequestID = request.ID;
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.SupportStatusID = request.StatusID;
            supportTimeLine.Status = supportRepository.GetByStatusId(request.StatusID).Name;
            supportRepository.CreateSupportTimeLine(supportTimeLine);

            return Json(1);
        }

        public IActionResult CreateOrEditSupportDetails(long id)
        {
            var res = ViewSupportRequestDet(id);
            return View(res);
        }

        #endregion


        #endregion





        #region SupportRequest




        #endregion

        #region GetCommon

        public IActionResult AllAttachments(long requestId, string requestType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            var req = supportRepository.GetBySupportRequestId(requestId);
            var attachments = commonMethods.GetSupportAttachments(requestId, userId).OrderByDescending(a => a.id);
            allsupportattachmentcls allattachmentcls = new allsupportattachmentcls();
            allattachmentcls.attachments = attachments.ToList();
            allattachmentcls.SupportRequestID = requestId;
            // allattachmentcls.todoName = todoRepository.GetTodoById(id).Name;
            ///

            ViewBag.ID = requestId;
            ViewBag.Type = requestType;
            ViewBag.Status = req.Status;
            ViewBag.UserId = userId;
            return View(allattachmentcls);
        }

        public IActionResult RemoveAttachments(long id)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            SupportAttachment Attachment = supportRepository.GetByAttachmentId(id);
            if (Attachment.InsertedId == userId)
            {
                Attachment.IsAvailable = false;
                Attachment.Active = false;
                Attachment.Status = EntityStatus.DELETED;
                Attachment.ModifiedId = userId;
                Attachment.ModifiedDate = DateTime.Now;
                supportRepository.UpdateAttachment(Attachment);
            }
            return RedirectToAction(nameof(AllAttachments), new { id = Attachment.SupportRequestID });
        }

        public async Task<JsonResult> savesupportattachments(IFormFile file, string attachmentname, long reqid)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            SupportAttachment Attachment = new SupportAttachment();
            Attachment.Active = true;
            Attachment.Type = attachmentname;
            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"/ODMImages/Tempfile/" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                Attachment.Path = pathToSave;
                Attachment.Name = newFilename;
            }

            Attachment.InsertedId = userId;
            Attachment.IsAvailable = true;
            Attachment.ModifiedId = userId;
            Attachment.Status = EntityStatus.ACTIVE;
            Attachment.SupportRequestID = reqid;
            supportRepository.CreateAttachment(Attachment);
            return Json(0);
        }

        public async Task<JsonResult> GetCategoryId(long id)
        {
            SupportRequestCls od = new SupportRequestCls();

            var request = supportRepository.GetAllSupportRequest().ToList();
            var support = supportRepository.GetAllSupportType().ToList();

            od.IsAvailableLocation = support.Where(a => a.ID == id).FirstOrDefault().IsAvailable;

            var res = (from a in supportRepository.GetAllCategory().ToList()
                       where a.SupportTypeID == id && a.IsAvailable == true
                       select new SupportCategoryCls
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        public async Task<JsonResult> GetIsAvailable(long id)
        {
            SupportRequestCls od = new SupportRequestCls();

            var request = supportRepository.GetAllSupportRequest().ToList();
            var support = supportRepository.GetAllSupportType().ToList();

            od.IsAvailableLocation = support.Where(a => a.ID == id).FirstOrDefault().IsAvailable;


            return Json(od.IsAvailableLocation);
        }
        public async Task<JsonResult> GetSubCategoryId(long id)
        {
            var res = (from a in supportRepository.GetAllSubCategory().ToList()
                       where a.CategoryID == id && a.IsAvailable == true
                       select new SupportSubCategoryCls
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }


        public async Task<JsonResult> GetRoomId(long id)
        {

            var room = supportRepository.GetAllRoom().ToList();
            var floor = supportRepository.GetAllFloor().ToList();

            var res = (from a in room
                       join b in floor on a.FloorID equals b.ID
                       where a.BuildingID == id && a.IsAvailable == true
                       select new RoomCls
                       {
                           ID = a.ID,
                           Name = a.Name,
                           FloorName = b.Name
                       }).ToList();
            return Json(res);
        }

        public async Task<JsonResult> GetOrganizationId(long id)
        {

            var department = departmentRepository.GetAllDepartment().ToList();
            var organization = organizationRepository.GetAllOrganization();
            var supptype = supportRepository.GetAllSupportType().ToList();

            var res = (from a in supptype
                       join b in department on a.DepartmentID equals b.ID
                       where a.ID == id
                       select new
                       {
                           org = organization.Where(m => m.GroupID == b.GroupID ||
                           m.ID == b.OrganizationID).ToList()
                       }).FirstOrDefault();

            return Json(res);
        }


        public IActionResult AllComments(long requestId, string requestType)

        {
            userId = HttpContext.Session.GetInt32("userId").Value;

            var req = supportRepository.GetBySupportRequestId(requestId);
            if (req != null)
            {
                //if (req.InsertedId == userId)
                //{
                var comments = supportRepository.GetAllSupportComment()
                    .Where(a => a.SupportRequestID == requestId && a.InsertedId != userId && a.IsRead == false).
                    ToList();
                foreach (var a in comments)
                {
                    SupportComment comment = a;
                    comment.IsRead = true;
                    comment.ModifiedId = userId;
                    supportRepository.UpdateSupportComment(comment);
                }
                // }

                var attachments = commonMethods.GetSupportComments(requestId, userId);
                Supportallcommentscls allcommentscls = new Supportallcommentscls();
                allcommentscls.comments = attachments.ToList();

                allcommentscls.SupportRequestID = requestId;
                allcommentscls.SupportRequestDescription = req.Description;


                Employee emp = employeeRepository.GetEmployeeById(req.InsertedId);

                allcommentscls.SupportRequestRaisedName = emp.FirstName + " " + emp.LastName;
                allcommentscls.SupportRequestDate = req.InsertedDate.ToString("dd-MMM-yyyy");

                ViewBag.UserId = userId;

                ViewBag.Type = requestType;

                return View(allcommentscls);
            }
            else
            {
                return View(nameof(SupportReport));
            }
        }

        [HttpPost]
        public async Task<JsonResult> savesupportcomments(string comments, long requestidd)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            SupportComment comment = new SupportComment();
            comment.Active = true;
            comment.Name = comments;
            comment.IsRead = false;
            comment.InsertedId = userId;
            comment.IsAvailable = true;
            comment.ModifiedId = userId;
            comment.Status = "Create";
            comment.SupportRequestID = requestidd;
            supportRepository.CreateSupportComment(comment);
            SupportRequest req = supportRepository.GetBySupportRequestId(requestidd);

            return Json(0);
        }

        public IActionResult RemoveComment(long requestId, string requestType)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            SupportComment supportComment = supportRepository.GetBySupportCommentId(requestId);
            if (supportComment.InsertedId == userId)
            {
                supportComment.IsAvailable = false;
                supportComment.Active = false;
                supportComment.ModifiedId = userId;
                supportComment.ModifiedDate = DateTime.Now;
                supportRepository.UpdateSupportComment(supportComment);
            }
            return RedirectToAction(nameof(AllComments), new { requestId = supportComment.SupportRequestID, requestType = requestType });
        }
        public IActionResult RemoveCommentByrequest(long id)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            SupportComment supportComment = supportRepository.GetBySupportCommentId(id);
            if (supportComment.InsertedId == userId)
            {
                supportComment.IsAvailable = false;
                supportComment.Active = false;
                supportComment.ModifiedId = userId;
                supportComment.ModifiedDate = DateTime.Now;
                supportRepository.UpdateSupportComment(supportComment);
            }
            return RedirectToAction(nameof(CreateOrEditSupportRequest), new { id = supportComment.SupportRequestID });
        }

        public IActionResult RemoveCommentByresolve(long id)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            SupportComment supportComment = supportRepository.GetBySupportCommentId(id);
            if (supportComment.InsertedId == userId)
            {
                supportComment.IsAvailable = false;
                supportComment.Active = false;
                supportComment.ModifiedId = userId;
                supportComment.ModifiedDate = DateTime.Now;
                supportRepository.UpdateSupportComment(supportComment);
            }
            return RedirectToAction(nameof(CreateOrEditSupportResolve), new { id = supportComment.SupportRequestID });
        }

        #endregion

        #region SupportResolve
        // GET: Countries

        public async Task<IActionResult> StatusUpdate(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            var request = supportRepository.GetBySupportRequestId(id.Value);

            request.StatusID = 2;
            request.StatusName = "Verification Pending";
            supportRepository.UpdateSupportRequest(request);

            var employees = employeeRepository.GetEmployeeById(request.InsertedId);

            string name = employees.FirstName + " " + employees.LastName;

            var empname = employeeRepository.GetEmployeeById(userId);
            string employeename = empname.FirstName + " " + empname.LastName;
            string empcode = employees.EmpCode;

            if (request.StatusID == 2)
            {
                smssend.resolvesupport(empcode, request.Description, name, employeename, "Support Resolved-" + request.Title + "-" + name, employees.EmailId);
            }

            if (request.StatusID == 3)
            {
                smssend.inprogresssupport(empcode, request.Description, name, employeename, "Support Update-" + request.Title + "-" + name, employees.EmailId);
            }

            return RedirectToAction(nameof(SupportResolve));
        }


        public async Task<JsonResult> AssignDeadlineDate(long id, string dueDate)
        {
            try
            {
                long roleId = HttpContext.Session.GetInt32("roleId").Value;
                long accessId = HttpContext.Session.GetInt32("accessId").Value;

                SupportRequest requests = supportRepository.GetBySupportRequestId(id);
                long userId = HttpContext.Session.GetInt32("userId").Value;

                string mobile = "";

                string name = "";
                string code = "";
                string email = "";
                string phone = "";
                string category = "";
                string whom = "";
                string typename = "";
                string duedate = "";

                //SupportRequest supportRequests = new SupportRequest();

                requests.DueDate = Convert.ToDateTime(dueDate);
                requests.ModifiedId = userId;
                supportRepository.UpdateSupportRequest(requests);

                SupportTimeLine supportTimeLine = new SupportTimeLine();
                supportTimeLine.SupportRequestID = requests.ID;
                supportTimeLine.SupportStatusID = requests.StatusID;
                supportTimeLine.InsertedId = userId;
                supportTimeLine.ModifiedId = userId;
                supportTimeLine.Active = true;
                supportTimeLine.Status = "DEADLINE DATE ASSIGNED";
                supportRepository.CreateSupportTimeLine(supportTimeLine);


                var employees = employeeRepository.GetEmployeeById(requests.InsertedId);

                //var empid = departmentLeadRepository.GetAllByEmployeeId(employees.ID);

                name = employees.FirstName + " " + employees.LastName;
                code = employees.EmpCode;
                email = employees.EmailId;
                phone = employees.PrimaryMobile;
                category = "Employee";
                whom = "Employee";
                duedate = dueDate;

                DateTime dt = DateTime.ParseExact(duedate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                string newdueDate = dt.ToString("dd-MM-yyyy");

                typename = supportRepository.GetBySupportTypeId(requests.SupportTypeID).Name;

                var empname = employeeRepository.GetEmployeeById(userId);
                string employeename = empname.FirstName + " " + empname.LastName;

                smssend.assigneddeadlinedate(newdueDate, code, requests.Description, name, employeename, "Support Deadline Date Assigned-" + requests.Title + "-" + name, employees.EmailId);


                return Json(1);
            }
            catch (Exception e1)
            {

                return Json(e1);
            }
        }



        public async Task<ActionResult> RejectSupportRequestInResolve(long? id)
        {
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            var request = supportRepository.GetBySupportRequestId(id.Value);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            request.RejectedID = userId;
            request.RejectDate = DateTime.Now;
            request.IsReject = true;
            request.IsApprove = false;
            request.ApprovedID = 0;
            request.ApproveDate = null;
            request.StatusID = 5;
            request.StatusName = request.StatusID == 0 ? "APPROVED" : supportRepository.GetByStatusId(request.StatusID).Name;
            supportRepository.UpdateSupportRequest(request);

            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.SupportRequestID = request.ID;
            supportTimeLine.SupportStatusID = request.StatusID;
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.Status = request.StatusID == 0 ? "APPROVED" : supportRepository.GetByStatusId(request.StatusID).Name;
            supportRepository.CreateSupportTimeLine(supportTimeLine);

            return RedirectToAction(nameof(SupportResolve));

        }





        #endregion


        #region SupportBulkUpload

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public IActionResult downloadSupportSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            var wsData = oWB.Worksheets.Add("Data");
            var supporttype = supportRepository.GetAllSupportType();
            var category = supportRepository.GetAllCategory();
            wsData.Cell(1, 1).Value = "Support Type";
            int i = 1;
            foreach (var a in supporttype)
            {
                wsData.Cell(++i, 1).Value = a.Name;
            }
            wsData.Range("A2:A" + i).AddToNamed("Support Type");
            wsData.Cell(1, 2).Value = "Category";
            i = 1;
            foreach (var a in category)
            {
                wsData.Cell(++i, 2).Value = "_C" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
            }
            wsData.Range("B2:B" + i).AddToNamed("Category");

            var subcategory = supportRepository.GetAllSubCategory();
            int j = 2;
            foreach (var a in category)
            {
                wsData.Cell(1, ++j).Value = "_C" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                int k = 1;
                foreach (var b in subcategory.Where(m => m.CategoryID == a.ID).ToList())
                {
                    wsData.Cell(++k, j).Value = "S" + b.ID + "_" + b.Name;
                }
                wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("_C" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
            }

            var wsData1 = oWB.Worksheets.Add("Data1");
            j = 0;
            wsData1.Cell(1, ++j).Value = "Priority";
            var prio = supportRepository.GetAllPriority();
            i = 1;
            foreach (var a in prio)
            {
                wsData1.Cell(++i, j).Value = a.Name;
            }
            wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(i, j)).AddToNamed("Priority");

            var building = supportRepository.GetAllBuilding();
            wsData1.Cell(1, ++j).Value = "Building";
            i = 1;
            foreach (var a in building)
            {
                wsData1.Cell(++i, j).Value = "B" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_");
            }
            wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(i, j)).AddToNamed("Building");



            var room = supportRepository.GetAllRoom();
            foreach (var a in building)
            {
                wsData1.Cell(1, ++j).Value = "B" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_");
                int k = 1;
                foreach (var b in room.Where(m => m.BuildingID == a.ID).ToList())
                {
                    wsData1.Cell(++k, j).Value = "R" + b.ID + "_" + b.Name;
                }
                wsData1.Range(wsData1.Cell(2, j), wsData1.Cell(k, j)).AddToNamed("B" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_"));
            }

            // var worksheet7 = oWB.Worksheet(7);
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Support Type");
            validationTable.Columns.Add("Category");
            validationTable.Columns.Add("SubCategory");
            validationTable.Columns.Add("Building");
            validationTable.Columns.Add("Room");
            validationTable.Columns.Add("Priority");
            validationTable.Columns.Add("Name");
            validationTable.Columns.Add("Description");
            //validationTable.Columns.Add("Status");
            validationTable.TableName = "Support_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(wsData.Range("Support Type"), true);
            worksheet.Column(2).SetDataValidation().List(wsData.Range("Category"), true);
            worksheet.Column(3).SetDataValidation().InCellDropdown = true;
            worksheet.Column(3).SetDataValidation().Operator = XLOperator.Between;
            worksheet.Column(3).SetDataValidation().AllowedValues = XLAllowedValues.List;
            worksheet.Column(3).SetDataValidation().List("=INDIRECT(SUBSTITUTE(B1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
            worksheet.Column(4).SetDataValidation().List(wsData1.Range("Building"), true);
            worksheet.Column(5).SetDataValidation().InCellDropdown = true;
            worksheet.Column(5).SetDataValidation().Operator = XLOperator.Between;
            worksheet.Column(5).SetDataValidation().AllowedValues = XLAllowedValues.List;
            worksheet.Column(5).SetDataValidation().List("=INDIRECT(SUBSTITUTE(D1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
            worksheet.Column(6).SetDataValidation().List(wsData1.Range("Priority"), true);
            // worksheet.Column(8).SetDataValidation().List(worksheet7.Range("A2:A" + lastCellNo7), true);
            wsData.Hide();
            wsData1.Hide();
            // worksheet7.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SupportRequest.xlsx");
        }




        public IActionResult DownloadSupport()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            moduledt.Columns.Add("Support Type");
            moduledt.Columns.Add("Category");
            moduledt.Columns.Add("Sub Category");
            moduledt.Columns.Add("Building");
            moduledt.Columns.Add("Room");
            moduledt.Columns.Add("Priority");
            moduledt.Columns.Add("Title");
            moduledt.Columns.Add("Description");
            moduledt.Columns.Add("RaisedBy");
            moduledt.Columns.Add("RaisedOn");
            moduledt.Columns.Add("Request Completion Date");
            moduledt.Columns.Add("Ticket Code");
            moduledt.Columns.Add("Status");
            List<SupportRequestCls> support = new List<SupportRequestCls>();
            if (HttpContext.Session.GetString("supportrequest") != null)
            {
                support = JsonConvert.DeserializeObject<List<SupportRequestCls>>(HttpContext.Session.GetString("supportrequest"));
            }

            int i = 0;
            foreach (var a in support)
            {
                DataRow dr = moduledt.NewRow();
                dr["Support Type"] = a.SupportTypeName;
                dr["Category"] = a.CategoryName;
                dr["Sub Category"] = a.SubCategoryName;
                dr["Building"] = a.BuildingName;
                dr["Room"] = a.RoomName;
                dr["Priority"] = a.PriorityName;
                dr["Title"] = a.Title;
                dr["Description"] = a.Description;
                var employee = employeeRepository.GetEmployeeById(a.InsertedID);
                string name = employee.FirstName + "" + employee.LastName;
                dr["RaisedBy"] = name;
                dr["RaisedOn"] = a.InsertedDate.ToString("dd-MMM-yyyy");
                dr["Request Completion Date"] = a.RequestedCompletionDate;
                dr["Status"] = a.Status;
                dr["Ticket Code"] = a.TicketCode;
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "SupportRequest";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SupportRequest.xlsx");
        }
        public IActionResult DownloadSupportResolve()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            moduledt.Columns.Add("Support Type");
            moduledt.Columns.Add("Category");
            moduledt.Columns.Add("Sub Category");
            moduledt.Columns.Add("Building");
            moduledt.Columns.Add("Room");
            moduledt.Columns.Add("Priority");
            moduledt.Columns.Add("RaisedBy");
            moduledt.Columns.Add("RaisedOn");
            moduledt.Columns.Add("Request Completion Date");
            moduledt.Columns.Add("Ticket Code");
            moduledt.Columns.Add("Status");
            moduledt.Columns.Add("Title");
            moduledt.Columns.Add("Description");
            List<SupportRequestCls> support = new List<SupportRequestCls>();
            if (HttpContext.Session.GetString("supportresolve") != null)
            {
                support = JsonConvert.DeserializeObject<List<SupportRequestCls>>(HttpContext.Session.GetString("supportresolve"));
            }

            int i = 0;
            foreach (var a in support)
            {
                DataRow dr = moduledt.NewRow();
                dr["Support Type"] = a.SupportTypeName;
                dr["Category"] = a.CategoryName;
                dr["Sub Category"] = a.SubCategoryName;
                dr["Building"] = a.BuildingName;
                dr["Room"] = a.RoomName;
                dr["Priority"] = a.PriorityName;
                dr["RaisedBy"] = a.emp_Name;
                dr["RaisedOn"] = a.InsertedDate.ToString("dd-MMM-yyyy");
                dr["Request Completion Date"] = a.RequestedCompletionDate;
                dr["Status"] = a.Status;
                dr["Ticket Code"] = a.TicketCode;
                dr["Title"] = a.Title;
                dr["Description"] = a.Description;

                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "SupportResolve";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SupportResolve.xlsx");
        }
        public IEnumerable<SupportRequestCls> SupportResoveForBulk()
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            var priority = TempData["Priority"];
            var fromDate = TempData["FromDate"];
            var toDate = TempData["ToDate"];

            List<long> supportTypeIds = null;
            IEnumerable<SupportRequest> requests = null;

            if (userId != 2 || userId != 1)
            {
                var allDeptIds = common.GetDepartmentsByEmployeeID(userId).Select(a => a.id);
                if (allDeptIds != null && allDeptIds.Count() > 0)
                {
                    supportTypeIds = supportRepository.
                            GetSupportTypeIdsByDepartmentIdIn(allDeptIds.Distinct().
                            ToList()).ToList();
                    if (supportTypeIds != null && supportTypeIds.Count() > 0)
                    {


                        var SupportTypes = (from a in
                                                  supportRepository.
                                                  GetAllSupportType().Where(a => supportTypeIds.Contains(a.ID)).ToList()
                                            select new NameIdViewModel
                                            {
                                                ID = a.ID,
                                                Name = a.Name

                                            }

                                ).ToList();

                    }

                }

                if (supportTypeIds != null && supportTypeIds.Count() > 0)
                {
                    requests = supportRepository.
                            GetAllBySupportTypeId(supportTypeIds)
                            .ToList();
                }

                //if (priority != null)
                //{
                //    requests = requests.Where(a =>
                //                priority.Contains(a.PriorityID));
                //}
            }
            else
            {
                requests = supportRepository.GetAllSupportRequest().ToList();
            }
            if (requests != null)
            {
                // var request = supportRepository.GetAllSupportRequest().ToList();
                var cat = supportRepository.GetAllCategory().ToList();
                var subcat = supportRepository.GetAllSubCategory().ToList();
                var loc = supportRepository.GetAllLocation().ToList();
                var build = supportRepository.GetAllBuilding();
                var room = supportRepository.GetAllRoom();
                var priorities = supportRepository.GetAllPriority().ToList();


                List<SupportRequestCls> res = (from a in requests
                                               join b in cat on a.CategoryID equals b.ID
                                               join c in subcat on a.SubCategoryID equals c.ID

                                               join m in supportRepository.GetAllSupportType() on a.SupportTypeID equals m.ID

                                               select new SupportRequestCls
                                               {
                                                   ID = a.ID,
                                                   Title = a.Title,
                                                   TicketCode = a.TicketCode,
                                                   CategoryName = b.Name,
                                                   SubCategoryName = c.Name,
                                                   RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                   BuildingName = a.BuildingID != 0 ? build.Where(y => y.ID == a.BuildingID).FirstOrDefault().Name :
                                                   ViewConstants.NOT_APPLICABLE,
                                                   RoomName = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE,
                                                   PriorityName = priorities != null ? priorities.Where(x => x.ID == a.PriorityID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE,
                                                   StatusID = a.StatusID,
                                                   Status = a.StatusName,
                                                   OrgGroupName = common.GetOrgGroupName(a.InsertedId),
                                                   t_Date = a.DueDate != null ? a.DueDate : (a.RequestedCompletionDate == null ? a.InsertedDate : a.RequestedCompletionDate),
                                                   PriorityID = a.PriorityID,

                                                   emp_Name = common.GetEmployeeNameByEmployeeId(a.InsertedId, userId),
                                                   //emp_Phone = i.PrimaryMobile != null ? i.PrimaryMobile : "",
                                                   Description = a.Description,
                                                   SupportTypeID = a.SupportTypeID,
                                                   SupportTypeName = m.Name,
                                                   Comment = a.Comment,
                                                   InsertedDate = a.InsertedDate,
                                                   ModifiedDate = a.ModifiedDate,
                                                   InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                   SentApproval = a.SendApproval,
                                                   IsRead = a.IsRead == null ? false : a.IsRead,
                                                   IsManagementRead = a.IsManagementRead == null ? false : a.IsManagementRead,
                                                   DueDate = NullableDateTimeToStringDate(a.DueDate),

                                                   Management = (
                               a.SendApproval,
                                NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                 a.SentApprovalID != null && a.SentApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, userId) : ViewConstants.NOT_APPLICABLE,
                               a.ManagementApproval,
                               NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                               a.ManagementApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, userId) : ViewConstants.NOT_APPLICABLE,
                               a.ManagementReject,
                               NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                               a.ManagementRejectedID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, userId) : ViewConstants.NOT_APPLICABLE


                               ),

                                                   commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, userId)

                                               }
                         ).ToList();

                if (fromDate != null)
                {
                    DateTime FromDate = Convert.ToDateTime(fromDate + " 00:00:00 AM");
                    DateTime ToDate = System.DateTime.Now;
                    if (toDate != null)
                    {
                        ToDate = Convert.ToDateTime(toDate + " 23:59:59 PM");
                    }
                    if (fromDate != null && toDate != null)
                    {
                        res = res.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                    }
                    if (fromDate != null && toDate == null)
                    {
                        res = res.Where(a => a.InsertedDate >= FromDate).ToList();
                    }
                }

                return res;
            }
            else
                return null;
        }
        public IActionResult DownloadSupportManagementApproval()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();

            moduledt.Columns.Add("Support Type");
            moduledt.Columns.Add("Category");
            moduledt.Columns.Add("Sub Category");
            moduledt.Columns.Add("Building");
            moduledt.Columns.Add("Room");
            moduledt.Columns.Add("Priority");
            moduledt.Columns.Add("RaisedBy");
            moduledt.Columns.Add("RaisedOn");
            moduledt.Columns.Add("Request Completion Date");
            moduledt.Columns.Add("Ticket Code");
            moduledt.Columns.Add("Status");
            moduledt.Columns.Add("IsPending");
            moduledt.Columns.Add("IsApprove");
            moduledt.Columns.Add("IsRejected");
            moduledt.Columns.Add("Title");
            moduledt.Columns.Add("Description");

            List<SupportRequestCls> support = new List<SupportRequestCls>();
            if (HttpContext.Session.GetString("supportmanagementresolve") != null)
            {
                support = JsonConvert.DeserializeObject<List<SupportRequestCls>>(HttpContext.Session.GetString("supportmanagementresolve"));
            }
            int i = 0;
            foreach (var a in support)
            {
                DataRow dr = moduledt.NewRow();
                //dr["Sl No"] = ++i;
                dr["Support Type"] = a.SupportTypeName;
                dr["Category"] = a.CategoryName;
                dr["Sub Category"] = a.SubCategoryName;
                dr["Building"] = a.BuildingName;
                dr["Room"] = a.RoomName;
                dr["Priority"] = a.PriorityName;
                dr["RaisedBy"] = a.emp_Name;
                dr["RaisedOn"] = a.InsertedDate.ToString("dd-MMM-yyyy");
                dr["Request Completion Date"] = a.RequestedCompletionDate;
                dr["Ticket Code"] = a.TicketCode;
                dr["Status"] = a.Status;
                dr["IsPending"] = a.IsApprove != true ? a.IsReject != true ? true : false : false;
                dr["IsApprove"] = a.IsApprove;
                dr["IsRejected"] = a.IsReject;
                dr["Title"] = a.Title;
                dr["Description"] = a.Description;

                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "ManagementApproval";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"ManagementApproval.xlsx");
        }
        public IEnumerable<SupportRequestCls> SupportManagementApprovalBulk()
        {
            var priority = TempData["Priority"];
            var fromdate = TempData["FromDate"];
            var todate = TempData["ToDate"];

            var request = supportRepository.GetApprovalSupportRequestBySupportTypeId(null).ToList();

            if (request != null)
            {
                var cat = supportRepository.GetAllCategory().ToList();
                var subcat = supportRepository.GetAllSubCategory().ToList();
                var loc = supportRepository.GetAllLocation().ToList();
                var build = supportRepository.GetAllBuilding();
                var room = supportRepository.GetAllRoom();
                var prior = supportRepository.GetAllPriority().ToList();
                var supptype = supportRepository.GetAllSupportType().ToList();



                List<SupportRequestCls> res = (from a in request
                                               join b in cat on a.CategoryID equals b.ID
                                               join c in subcat on a.SubCategoryID equals c.ID
                                               join g in prior on a.PriorityID equals g.ID
                                               //join i in emp on a.InsertedId equals i.ID
                                               join m in supptype on a.SupportTypeID equals m.ID
                                               //join n in det on m.DepartmentID equals n.ID
                                               select new SupportRequestCls
                                               {
                                                   ID = a.ID,
                                                   CategoryName = b.Name,
                                                   TicketCode = a.TicketCode,
                                                   Title = a.Title,
                                                   SubCategoryName = c.Name,
                                                   RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                   BuildingName = a.BuildingID != 0 ? build.Where(y => y.ID == a.BuildingID).FirstOrDefault().Name :
                                                   ViewConstants.NOT_APPLICABLE,
                                                   RoomName = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE,
                                                   PriorityName = g.Name,
                                                   StatusID = a.StatusID,
                                                   IsApprove = a.ManagementApproval,
                                                   IsReject = a.ManagementReject,
                                                   ModifiedDate = a.ModifiedDate,
                                                   Status = a.StatusName,
                                                   t_Date = a.InsertedDate,
                                                   PriorityID = a.PriorityID,
                                                   OrgGroupName = common.GetOrgGroupName(a.InsertedId),
                                                   emp_Name = common.GetEmployeeNameByEmployeeId(a.InsertedId, userId),
                                                   //emp_Phone = i.PrimaryMobile != null ? i.PrimaryMobile : "",
                                                   Description = a.Description,
                                                   SupportTypeID = a.SupportTypeID,
                                                   SupportTypeName = m.Name,
                                                   IsManagementRead = a.IsManagementRead == null ? false : a.IsManagementRead,
                                                   InsertedDate = a.InsertedDate,
                                                   InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                   DueDate = NullableDateTimeToStringDate(a.DueDate),
                                                   Comment = a.Comment,
                                                   Management = (
                               a.SendApproval,
                                NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                 a.SentApprovalID != null && a.SentApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, userId) : "NA",
                               a.ManagementApproval,
                               NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                               a.ManagementApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, userId) : "NA",
                               a.ManagementReject,
                               NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                               a.ManagementRejectedID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, userId) : "NA"
                               )
                                               }
                         ).ToList();

                if (fromdate != null)
                {
                    DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                    DateTime ToDate = System.DateTime.Now;
                    if (todate != null)
                    {
                        ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                    }
                    if (fromdate != null && todate != null)
                    {
                        res = res.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                    }
                    if (fromdate != null && todate == null)
                    {
                        res = res.Where(a => a.InsertedDate >= FromDate).ToList();
                    }
                }
                return res;
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadSupport(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[2];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var request = supportRepository.GetAllSupportRequest();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        SupportRequest supportRequests = new SupportRequest();
                        string supportname = firstWorksheet.Cells[i, 1].Value.ToString();
                        string categoryname = firstWorksheet.Cells[i, 2].Value.ToString();
                        string subcategoryname = firstWorksheet.Cells[i, 3].Value.ToString();
                        string buildingname = firstWorksheet.Cells[i, 4].Value.ToString();
                        string roomname = firstWorksheet.Cells[i, 5].Value.ToString();
                        string priorityname = firstWorksheet.Cells[i, 6].Value.ToString();
                        string title = firstWorksheet.Cells[i, 7].Value.ToString();
                        string description = firstWorksheet.Cells[i, 8].Value.ToString();
                        var supportid = supportRepository.GetAllSupportType().Where(m => m.Name == supportname).FirstOrDefault().ID;
                        var categoryid = categoryname == "" ? 0 : Convert.ToInt64(categoryname.Split('_')[1].Substring(1, categoryname.Split('_')[1].Length - 1));
                        var subcategoryid = subcategoryname == "" ? 0 : Convert.ToInt64(subcategoryname.Split('_')[0].Substring(1, subcategoryname.Split('_')[0].Length - 1));
                        var buildingid = buildingname == "" ? 0 : Convert.ToInt64(buildingname.Split('_')[0].Substring(1, buildingname.Split('_')[0].Length - 1));
                        var roomid = roomname == "" ? 0 : Convert.ToInt64(roomname.Split('_')[0].Substring(1, roomname.Split('_')[0].Length - 1));
                        var priorityid = supportRepository.GetAllPriority().Where(m => m.Name == priorityname).FirstOrDefault().ID;
                        //  var statusid = supportRepository.GetAllStatus().Where(m => m.Name == statusname).FirstOrDefault().ID;

                        //if (request.Where(a => a.Description == description && a.SupportTypeID == supportid && a.CategoryID == categoryid && a.SubCategoryID == subcategoryid && a.BuildingID == buildingid && a.RoomID == roomid && a.PriorityID == priorityid).FirstOrDefault() == null)
                        //{
                        supportRequests.Description = description;
                        supportRequests.Title = title;
                        supportRequests.ModifiedId = userId;
                        supportRequests.CategoryID = categoryid;
                        supportRequests.SupportTypeID = supportid;
                        supportRequests.SubCategoryID = subcategoryid;
                        supportRequests.BuildingID = buildingid;
                        supportRequests.RoomID = roomid;
                        supportRequests.PriorityID = priorityid;
                        supportRequests.StatusID = 1;
                        supportRequests.StatusName = "PENDING";
                        supportRequests.Active = true;
                        supportRequests.InsertedId = userId;
                        supportRequests.Status = EntityStatus.ACTIVE;
                        var type = supportRepository.GetBySupportTypeId(supportRequests.SupportTypeID);
                        var allrequest = supportRepository.GetAllSupportRequestWithinactive() == null ? null : supportRepository.GetAllSupportRequestWithinactive().Where(a => a.SupportTypeID == supportRequests.SupportTypeID).ToList();
                        if (allrequest == null)
                        {
                            supportRequests.TicketCode = type.Name.Substring(0, 2) + "-1";
                        }
                        else
                        {
                            supportRequests.TicketCode = type.Name.Substring(0, 2) + "-" + (allrequest.ToList().Count + 1);
                        }
                        long id = supportRepository.CreateSupportRequest(supportRequests);

                        SupportTimeLine supportTimeLine = new SupportTimeLine();
                        supportTimeLine.InsertedId = userId;
                        supportTimeLine.ModifiedId = userId;
                        supportTimeLine.Active = true;
                        supportTimeLine.Status = "PENDING";

                        supportTimeLine.SupportRequestID = id;
                        supportTimeLine.SupportStatusID = 1;
                        supportRepository.CreateSupportTimeLine(supportTimeLine);



                        var Department_Id = supportRepository.GetBySupportTypeId(supportRequests.SupportTypeID).DepartmentID;



                        List<long> DesignationIdes = designationRepository.GetDesignationByDepartmentId(Department_Id).ToList();

                        //Employee Head Details
                        List<DepartmentLead> employeeLeadid = departmentLeadRepository.GetAllEmployeeIdByDepartment(Department_Id).ToList();

                        List<long> employeeLeadid_Emplyee_Ides = employeeLeadid.Select(e => e.EmployeeID).ToList();
                        // Department Employee Details
                        List<EmployeeDesignation> EmployeeIdes = employeeDesignationRepository.GetAllEmployeeDesignationsByDesignationId(DesignationIdes).ToList();
                        List<long> Department_Emplyee_Ides = EmployeeIdes.Select(e => e.EmployeeID).ToList();

                        foreach (var item in employeeLeadid_Emplyee_Ides) // remove department head employee id from employee list
                        {
                            Department_Emplyee_Ides.Remove(item);
                        }

                        //Fire Email to Email Head


                        foreach (var items in employeeLeadid_Emplyee_Ides)
                        {

                            long emp_id = items;
                            Employee employees = employeeRepository.GetEmployeeById(emp_id);
                            string name = employees.FirstName + " " + employees.LastName;
                            string code = employees.EmpCode;
                            string email = employees.EmailId;
                            string phone = employees.PrimaryMobile;
                            string category = "Employee";
                            string whom = "Employee";
                            string typename = supportRepository.GetBySupportTypeId(supportRequests.SupportTypeID).Name;
                            var spt = supportRepository.GetBySupportRequestId(supportRequests.ID);

                            smssend.raisesupport(spt.Title, spt.Description, name, email, phone, code, category, typename, "New Support Request For Head-" + spt.Title + "-" + name, whom, employees.EmailId);

                        }


                        //Fire Email to Department Employees




                        foreach (var item in Department_Emplyee_Ides)
                        {

                            long emp_id = item;
                            Employee employees = employeeRepository.GetEmployeeById(emp_id);
                            string name = employees.FirstName + " " + employees.LastName;
                            string code = employees.EmpCode;
                            string email = employees.EmailId;
                            string phone = employees.PrimaryMobile;
                            string category = "Employee";
                            string whom = "Employee";
                            string typename = supportRepository.GetBySupportTypeId(supportRequests.SupportTypeID).Name;
                            var descr = supportRepository.GetBySupportRequestId(supportRequests.ID);

                            smssend.raisesupport(descr.Title, descr.Description, name, email, phone, code, category, typename, "New Support Request-" + descr.Title + "-" + name, whom, employees.EmailId);

                        }

                        //}
                    }
                }
            }
            return RedirectToAction(nameof(SupportRequest));
        }

        public IActionResult downloadSupportCategorySample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();

            DataTable supportdt = new DataTable();
            supportdt.Columns.Add("Name");
            var support = supportRepository.GetAllSupportType();
            foreach (var a in support)
            {
                DataRow dr = supportdt.NewRow();
                dr["Name"] = a.Name;
                supportdt.Rows.Add(dr);
            }
            supportdt.TableName = "SupportType";


            int lastCellNo1 = supportdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(supportdt);
            var worksheet1 = oWB.Worksheet(1);
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("SupportType");
            validationTable.Columns.Add("Category Name");
            validationTable.TableName = "Support_Category_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SupportCategory.xlsx");
        }
        public IActionResult DownloadSupportCategory()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            //if (commonMethods.checkaccessavailable("Raise", accessId, "Download", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            //moduledt.Columns.Add("Sl No");
            moduledt.Columns.Add("Support Type");
            moduledt.Columns.Add("Category Name");
            moduledt.Columns.Add("Availability");
            // moduledt.Columns.Add("Modified On");

            var supp = supportRepository.GetAllSupportType();
            var category = supportRepository.GetAllCategory();

            var resss = (from a in category
                         join b in supp on a.SupportTypeID equals b.ID
                         select a).ToList();
            int i = 0;
            foreach (var a in resss)
            {
                DataRow dr = moduledt.NewRow();
                //  dr["Sl No"] = ++i;
                dr["Support Type"] = supp.Where(m => m.ID == a.SupportTypeID).FirstOrDefault().Name;
                dr["Category Name"] = a.Name;
                dr["Availability"] = a.IsAvailable;
                //  dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "SupportCategory";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SupportCategory.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadSupportCategory(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var request = supportRepository.GetAllCategory();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        SupportCategory supportRequests = new SupportCategory();
                        string supportname = firstWorksheet.Cells[i, 1].Value.ToString();
                        string supporttypename = firstWorksheet.Cells[i, 2].Value.ToString();

                        var supportid = supportRepository.GetAllSupportType().Where(m => m.Name == supportname).FirstOrDefault().ID;

                        if (request.Where(a => a.Name == supporttypename && a.SupportTypeID == supportid).FirstOrDefault() == null)
                        {
                            supportRequests.ModifiedId = userId;
                            supportRequests.SupportTypeID = supportid;
                            supportRequests.Name = firstWorksheet.Cells[i, 2].Value.ToString();
                            supportRequests.Active = true;
                            supportRequests.InsertedId = userId;
                            supportRequests.Status = EntityStatus.ACTIVE;
                            supportRepository.CreateCategory(supportRequests);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(SupportCategory));
        }

        public IActionResult downloadSupportSubCategorySample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();

            DataTable supportdt = new DataTable();
            supportdt.Columns.Add("Name");
            var support = supportRepository.GetAllCategory();
            foreach (var a in support)
            {
                DataRow dr = supportdt.NewRow();
                dr["Name"] = a.Name;
                supportdt.Rows.Add(dr);
            }
            supportdt.TableName = "SupportCategory";


            int lastCellNo1 = supportdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(supportdt);
            var worksheet1 = oWB.Worksheet(1);
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("SupportCategory");
            validationTable.Columns.Add("Name");
            validationTable.TableName = "Support_Sub_Category_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SupportSub-Category.xlsx");
        }
        public IActionResult DownloadSupportSubCategory()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            //if (commonMethods.checkaccessavailable("Raise", accessId, "Download", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            //moduledt.Columns.Add("Sl No");
            moduledt.Columns.Add("Support Category");
            moduledt.Columns.Add("Name");
            moduledt.Columns.Add("Availability");
            // moduledt.Columns.Add("Modified On");

            var supp = supportRepository.GetAllSubCategory();
            var category = supportRepository.GetAllCategory();

            var resss = (from a in supp
                         join b in category on a.CategoryID equals b.ID
                         select a).ToList();
            int i = 0;
            foreach (var a in resss)
            {
                DataRow dr = moduledt.NewRow();
                // dr["Sl No"] = ++i;
                dr["Support Category"] = category.Where(m => m.ID == a.CategoryID).FirstOrDefault().Name;
                dr["Name"] = a.Name;
                dr["Availability"] = a.IsAvailable;
                // dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "SupportSub-Category";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SupportSub-Category.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadSupportSubCategory(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var request = supportRepository.GetAllSubCategory();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        SupportSubCategory supportRequests = new SupportSubCategory();
                        string supportname = firstWorksheet.Cells[i, 1].Value.ToString();
                        string supporttypename = firstWorksheet.Cells[i, 2].Value.ToString();

                        var supportid = supportRepository.GetAllCategory().Where(m => m.Name == supportname).FirstOrDefault().ID;

                        if (request.Where(a => a.Name == supporttypename && a.CategoryID == supportid).FirstOrDefault() == null)
                        {
                            supportRequests.ModifiedId = userId;
                            supportRequests.CategoryID = supportid;
                            supportRequests.Name = firstWorksheet.Cells[i, 2].Value.ToString();
                            supportRequests.Active = true;
                            supportRequests.InsertedId = userId;
                            supportRequests.Status = EntityStatus.ACTIVE;
                            supportRepository.CreateSubCategory(supportRequests);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(SupportSubCategory));
        }


        public IActionResult downloadFloorSample()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            userId = HttpContext.Session.GetInt32("userId").Value;

            var comlumHeadrs = new string[]
         {
                "Name"
         };

            byte[] result;

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Floor"); //Worksheet name
                using (var cells = worksheet.Cells[1, 1]) //(1,1) (1,5)
                {
                    cells.Style.Font.Bold = true;
                }

                //First add the headers
                for (var i = 0; i < comlumHeadrs.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = comlumHeadrs[i];
                }
                result = package.GetAsByteArray();
            }

            return File(result, "application/ms-excel", $"Floor.xlsx");
        }
        public IActionResult DownloadFloor()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            //if (commonMethods.checkaccessavailable("Raise", accessId, "Download", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            //  moduledt.Columns.Add("Sl No");
            moduledt.Columns.Add("Name");
            moduledt.Columns.Add("Availability");
            // moduledt.Columns.Add("Modified On");

            var floor = supportRepository.GetAllFloor();
            var category = supportRepository.GetAllCategory();

            var resss = (from a in floor
                         select a).ToList();
            int i = 0;
            foreach (var a in resss)
            {
                DataRow dr = moduledt.NewRow();
                // dr["Sl No"] = ++i;
                dr["Name"] = a.Name;
                dr["Availability"] = a.IsAvailable;
                // dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "Floor";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Floor.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadFloor(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            if (file != null)
            {
                // string path1 = file.FileName;
                byte[] filestr;
                Stream stream = file.OpenReadStream();

                using (XLWorkbook workBook = new XLWorkbook(stream))
                {
                    //Read the first Sheet from Excel file.
                    IXLWorksheet workSheet = workBook.Worksheet(1);
                    var countries = supportRepository.GetAllFloor();
                    foreach (IXLRow row in workSheet.Rows().Skip(1))
                    {
                        Floor country = new Floor();
                        if (row.Cell(1).Value != null && row.Cell(1).Value.ToString() != "")
                        {
                            string countryname = row.Cell(1).Value.ToString();
                            if (countries.Where(a => a.Name == countryname).FirstOrDefault() == null)
                            {
                                country.Name = countryname;
                                country.ModifiedId = userId;
                                country.Active = true;
                                country.InsertedId = userId;
                                country.IsAvailable = true;
                                country.Status = EntityStatus.ACTIVE;
                                supportRepository.CreateFloor(country);
                            }
                        }
                    }
                    workBook.Dispose();
                }
                _httpContextAccessor.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

            }
            return RedirectToAction(nameof(Floor));
        }


        public IActionResult downloadBuildingSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();

            DataTable supportdt = new DataTable();
            supportdt.Columns.Add("Name");
            var support = supportRepository.GetAllLocation();
            foreach (var a in support)
            {
                DataRow dr = supportdt.NewRow();
                dr["Name"] = a.Name;
                supportdt.Rows.Add(dr);
            }
            supportdt.TableName = "Location";


            int lastCellNo1 = supportdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(supportdt);
            var worksheet1 = oWB.Worksheet(1);
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Location");
            validationTable.Columns.Add("Name");
            validationTable.TableName = "Building_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Building.xlsx");
        }
        public IActionResult DownloadBuilding()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            //if (commonMethods.checkaccessavailable("Raise", accessId, "Download", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            // moduledt.Columns.Add("Sl No");
            moduledt.Columns.Add("Location");
            moduledt.Columns.Add("Name");
            moduledt.Columns.Add("Type");
            // moduledt.Columns.Add("Modified On");

            var supp = supportRepository.GetAllLocation();
            var category = supportRepository.GetAllBuilding();
            var types = new string[2];
            types[0] = "CAMPUS";
            types[1] = "HOSTEL";

            var resss = (from a in category
                         join b in supp on a.LocationID equals b.ID
                         select a).ToList();
            int i = 0;
            foreach (var a in resss)
            {
                DataRow dr = moduledt.NewRow();
                // dr["Sl No"] = ++i;
                dr["Location"] = supp.Where(m => m.ID == a.LocationID).FirstOrDefault().Name;
                dr["Name"] = a.Name;
                dr["Type"] = types[0];
                // dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "Building";

            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Building.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadBuilding(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var request = supportRepository.GetAllBuilding();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Building supportRequests = new Building();
                        string supportname = firstWorksheet.Cells[i, 1].Value.ToString();
                        string supporttypename = firstWorksheet.Cells[i, 2].Value.ToString();

                        var supportid = supportRepository.GetAllLocation().Where(m => m.Name == supportname).FirstOrDefault().ID;

                        if (request.Where(a => a.Name == supporttypename && a.LocationID == supportid).FirstOrDefault() == null)
                        {
                            supportRequests.ModifiedId = userId;
                            supportRequests.LocationID = supportid;
                            supportRequests.Name = firstWorksheet.Cells[i, 2].Value.ToString();

                            if (firstWorksheet.Cells[i, 3].Value.ToString() == "CAMPUS")
                            {
                                supportRequests.BuildingTypeID = 1;
                            }
                            else
                            if (firstWorksheet.Cells[i, 3].Value.ToString() == "HOSTEL")
                            {
                                supportRequests.BuildingTypeID = 2;
                            }
                            supportRequests.IsAvailable = true;
                            supportRequests.Active = true;
                            supportRequests.InsertedId = userId;
                            supportRequests.Status = EntityStatus.ACTIVE;
                            long id = supportRepository.CreateBuilding(supportRequests);

                        }
                    }
                }
            }
            return RedirectToAction(nameof(Building));
        }

        public IActionResult downloadRoomSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();


            DataTable buildingdt = new DataTable();
            buildingdt.Columns.Add("Name");
            var building = supportRepository.GetAllBuilding();
            foreach (var d in building)
            {
                DataRow dr = buildingdt.NewRow();
                dr["Name"] = d.Name;
                buildingdt.Rows.Add(dr);
            }
            buildingdt.TableName = "Building";

            DataTable roomdt = new DataTable();
            roomdt.Columns.Add("Name");
            var room = supportRepository.GetAllRoomCategory();
            foreach (var e in room)
            {
                DataRow dr = roomdt.NewRow();
                dr["Name"] = e.Name;
                roomdt.Rows.Add(dr);
            }
            roomdt.TableName = "RoomCategory";

            DataTable subcategorydt = new DataTable();
            subcategorydt.Columns.Add("Name");
            var subcategory = supportRepository.GetAllFloor();
            foreach (var c in subcategory)
            {
                DataRow dr = subcategorydt.NewRow();
                dr["Name"] = c.Name;
                subcategorydt.Rows.Add(dr);
            }
            subcategorydt.TableName = "Floor";



            int lastCellNo1 = buildingdt.Rows.Count + 1;
            int lastCellNo2 = roomdt.Rows.Count + 1;
            int lastCellNo3 = subcategorydt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK

            oWB.AddWorksheet(buildingdt);
            oWB.AddWorksheet(roomdt);
            oWB.AddWorksheet(subcategorydt);

            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            var worksheet3 = oWB.Worksheet(3);

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Building");
            validationTable.Columns.Add("RoomCategory");
            validationTable.Columns.Add("Floor");
            validationTable.Columns.Add("Name");
            validationTable.TableName = "Room_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(2).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(3).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);

            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Room.xlsx");
        }
        public IActionResult DownloadRoom()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            //if (commonMethods.checkaccessavailable("Raise", accessId, "Download", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            // moduledt.Columns.Add("Sl No");
            moduledt.Columns.Add("Building");
            moduledt.Columns.Add("Room Category");
            moduledt.Columns.Add("Floor");
            moduledt.Columns.Add("Name");
            //  moduledt.Columns.Add("Modified On");

            var roomcat = supportRepository.GetAllRoomCategory();
            var floor = supportRepository.GetAllFloor();
            var build = supportRepository.GetAllBuilding();
            var room = supportRepository.GetAllRoom();
            var resss = (from a in room
                         join b in roomcat on a.RoomCategoryID equals b.ID
                         join c in floor on a.FloorID equals c.ID
                         join d in build on a.BuildingID equals d.ID
                         select a).ToList();
            int i = 0;
            foreach (var a in resss)
            {
                DataRow dr = moduledt.NewRow();
                //  dr["Sl No"] = ++i;
                dr["Building"] = build.Where(m => m.ID == a.BuildingID).FirstOrDefault().Name;
                dr["Room Category"] = roomcat.Where(m => m.ID == a.RoomCategoryID).FirstOrDefault().Name;
                dr["Floor"] = floor.Where(m => m.ID == a.FloorID).FirstOrDefault().Name;
                dr["Name"] = a.Name;
                //  dr["Modified On"] = a.ModifiedDate.ToString("dd-MMM-yyyy");
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "Room";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Room.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadRoom(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Raise", accessId, "Upload", "Support", staffTypeID) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}

            if (file != null)
            {

                using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                    int totalRows = firstWorksheet.Dimension.Rows;
                    var request = supportRepository.GetAllRoom();
                    for (int i = 2; i <= totalRows; i++)
                    {
                        Room supportRequests = new Room();
                        string buildingname = firstWorksheet.Cells[i, 1].Value.ToString();
                        string roomcategoryname = firstWorksheet.Cells[i, 2].Value.ToString();
                        string floorname = firstWorksheet.Cells[i, 3].Value.ToString();
                        string roomname = firstWorksheet.Cells[i, 4].Value.ToString();


                        var buildingid = supportRepository.GetAllBuilding().Where(m => m.Name == buildingname).FirstOrDefault().ID;
                        // var roomid = supportRepository.GetAllRoom().Where(m => m.Name == roomname).FirstOrDefault().ID;
                        var roomcategoryid = supportRepository.GetAllRoomCategory().Where(m => m.Name == roomcategoryname).FirstOrDefault().ID;
                        var floorid = supportRepository.GetAllFloor().Where(m => m.Name == floorname).FirstOrDefault().ID;

                        if (request.Where(a => a.Name == roomname && a.BuildingID == buildingid && a.RoomCategoryID == roomcategoryid && a.FloorID == floorid).FirstOrDefault() == null)
                        {
                            supportRequests.Name = firstWorksheet.Cells[i, 4].Value.ToString();
                            supportRequests.ModifiedId = userId;
                            supportRequests.BuildingID = buildingid;
                            supportRequests.RoomCategoryID = roomcategoryid;
                            supportRequests.FloorID = floorid;
                            supportRequests.Active = true;
                            supportRequests.InsertedId = userId;
                            supportRequests.IsAvailable = true;
                            supportRequests.Status = EntityStatus.ACTIVE;
                            supportRepository.CreateRoom(supportRequests);
                        }
                    }
                }
            }
            return RedirectToAction(nameof(Room));
        }



        #endregion

        #region Floor
        // GET: Countries
        public IActionResult Floor()
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Floor", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(supportRepository.GetAllFloor());
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditFloor(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            SupportMasterModel od = new SupportMasterModel();
            if (id == null)
            {
                if (common.checkaccessavailable("Floor", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Floor", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var floor = supportRepository.GetByFloorId(id.Value);
                od.ID = floor.ID;
                od.Name = floor.Name;
                od.IsAvailable = floor.IsAvailable;
            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateFloor([Bind("ID,Name,IsAvailable")] Floor floor)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                floor.ModifiedId = userId;
                floor.Active = true;
                if (floor.ID == 0)
                {
                    floor.InsertedId = userId;
                    floor.Status = EntityStatus.ACTIVE;
                    supportRepository.CreateFloor(floor);
                }
                else
                {
                    supportRepository.UpdateFloor(floor);
                }

                return RedirectToAction(nameof(Floor));
            }
            return View(floor);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteFloor")]
        public async Task<IActionResult> FloorDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            //long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Floor", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (FloorExists(id.Value) == true)
            {
                var floor = supportRepository.DeleteFloor(id.Value);
            }
            return RedirectToAction(nameof(Floor));
        }

        private bool FloorExists(long id)
        {
            if (supportRepository.GetByFloorId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region SupportType
        // GET: Countries
        public IActionResult SupportType()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support Type", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(supportRepository.GetAllSupportType());
        }

        // GET: Countries/Create
        public IActionResult CreateOrEditSupportType(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            SupportTypeCls od = new SupportTypeCls();

            od.departments = (from a in departmentRepository.GetAllDepartment()
                              select new Web.Models.Departmentcls
                              {
                                  ID = a.ID,
                                  Name = a.Name
                              }
                            ).ToList();

            if (id == null)
            {
                if (common.checkaccessavailable("Support Type", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Support Type", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var category = supportRepository.GetBySupportTypeId(id.Value);
                od.ID = category.ID;
                od.Name = category.Name;
                od.IsAvailable = category.IsAvailable;
                od.DepartmentID = category.DepartmentID;

            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateSupportType([Bind("ID,Name,IsAvailable,DepartmentID")] SupportType supportTypes)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                supportTypes.ModifiedId = userId;
                supportTypes.Active = true;
                if (supportTypes.ID == 0)
                {
                    supportTypes.InsertedId = userId;
                    supportTypes.Status = EntityStatus.ACTIVE;
                    supportRepository.CreateSupportType(supportTypes);
                }
                else
                {
                    supportRepository.UpdateSupportType(supportTypes);
                }

                return RedirectToAction(nameof(SupportType));
            }
            return View(supportTypes);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteSupportType")]
        public async Task<IActionResult> SupportTypeDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support Type", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (SupportTypeExists(id.Value) == true)
            {
                var category = supportRepository.DeleteSupportType(id.Value);
            }
            return RedirectToAction(nameof(SupportType));
        }

        private bool SupportTypeExists(long id)
        {
            if (supportRepository.GetBySupportTypeId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region DepartmentSupport
        // GET: Countries
        public IActionResult DepartmentSupport()
        {

            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Department Support", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var support = supportRepository.GetAllSupportType();
            var dept = departmentRepository.GetAllDepartment();
            var deptsupport = supportRepository.GetAllDepartmentSupport();

            var res = (from a in deptsupport
                       join b in dept on a.DepartmentID equals b.ID
                       join c in support on a.SupportTypeID equals c.ID
                       select new DepartmentSupportCls
                       {
                           ID = a.ID,
                           // Name = a.Name,
                           DeptName = b.Name,
                           SupportTypeName = c.Name,
                           ModifiedDate = a.ModifiedDate
                       }
                     ).ToList();

            return View(res);
        }

        public JsonResult GetDepartmentSupportAll(long? deptId, long? supptypeId)
        {
            try
            {
                var res = supportRepository.GetAllDepartmentSupport().Where(a => a.DepartmentID == deptId && a.SupportTypeID == supptypeId).FirstOrDefault();

                if (res == null)
                {
                    return Json(false);
                }
                else
                {
                    return Json(true);
                }

            }
            catch (Exception ex)
            {

                return Json(false);
            }

        }

        // GET: Countries/Create
        public IActionResult CreateOrEditDepartmentSupport(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            DepartmentSupportCls od = new DepartmentSupportCls();

            od.departments = (from a in departmentRepository.GetAllDepartment()
                              select new Web.Models.Departmentcls
                              {
                                  ID = a.ID,
                                  Name = a.Name
                              }
                            ).ToList();

            od.supportTypes = (from b in supportRepository.GetAllSupportType()
                               select new SupportType
                               {
                                   ID = b.ID,
                                   Name = b.Name
                               }
                             ).ToList();

            if (id == null)
            {
                if (common.checkaccessavailable("Department Support", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Create";
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (common.checkaccessavailable("Department Support", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = "Update";
                var category = supportRepository.GetByDepartmentSupportId(id.Value);
                od.ID = category.ID;
                od.Name = category.Name;
                od.IsAvailable = category.IsAvailable;
                od.DepartmentID = category.DepartmentID;
                od.SupportTypeID = category.SupportTypeID;

            }
            return View(od);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateDepartmentSupport([Bind("ID,Name,DepartmentID,SupportTypeID")] DepartmentSupport deptSupport)
        {


            if (ModelState.IsValid)
            {
                var res = supportRepository.GetAllDepartmentSupport().Where(a => a.DepartmentID == deptSupport.DepartmentID && a.SupportTypeID == deptSupport.SupportTypeID).FirstOrDefault();


                if (res == null)
                {

                    deptSupport.ModifiedId = userId;
                    deptSupport.Active = true;
                    if (deptSupport.ID == 0)
                    {
                        deptSupport.InsertedId = userId;
                        deptSupport.Status = EntityStatus.ACTIVE;
                        supportRepository.CreateDepartmentSupport(deptSupport);
                    }

                    return RedirectToAction(nameof(DepartmentSupport));
                }
                else
                {
                    return RedirectToAction(nameof(CreateOrEditDepartmentSupport));
                }


            }
            return View(deptSupport);
        }

        // POST: Countries/Delete/5
        [ActionName("DeleteDepartmentSupport")]
        public async Task<IActionResult> DepartmentSupportDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Department Support", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (DepartmentSupportExists(id.Value) == true)
            {
                var category = supportRepository.DeleteDepartmentSupport(id.Value);
            }
            return RedirectToAction(nameof(DepartmentSupport));
        }

        private bool DepartmentSupportExists(long id)
        {
            if (supportRepository.GetByDepartmentSupportId(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region SupportApproveReject
        public cls_supportrequest_details ViewSupportRequestDet(long supportrequestid)
        {
            CheckLoginStatus();
            long userId = HttpContext.Session.GetInt32("userId").Value;
            List<SupportRequest> list = new List<SupportRequest>();
            bool depthead = false;
            if (common.checkaccessavailable("Department Head", accessId, "AccessResolve", "Support", roleId) == true)
            {
                depthead = true;
            }

                var supportrequest = supportRepository.GetBySupportRequestIdNoTracking(supportrequestid);
            if (supportrequest != null)
            {
                var cat = supportRepository.GetByCategoryId(supportrequest.CategoryID);
                var support = supportRepository.GetBySupportTypeId(supportrequest.SupportTypeID);
                var subcat = supportRepository.GetBySubCategoryId(supportrequest.SubCategoryID);
                //var room = supportRepository.GetByRoomId(supportrequest.RoomID);
                //var build = supportRepository.GetAllBuilding();
                var prior = supportRepository.GetByPriorityId(supportrequest.PriorityID);
                //var locations = supportRepository.GetAllLocation();
                var attach = supportRepository.GetAttachmentsBySupportIdAndType(supportrequestid, SupportAttachmentType.RAISED);

                var timeline = supportRepository.GetTimelineBySupportId(supportrequestid);
                var status = supportRepository.GetByStatusId(supportrequest.StatusID);
                list.Add(supportrequest);

                var emp = employeeRepository.GetEmployeeById(supportrequest.InsertedId);
                var assignedTo = _dbContext.SupportRequestAssign.ToList();
                //    req.IsRead = true;
                //supportRepository.UpdateSupportRequest(req);

                var res = (from a in list

                           select new cls_supportrequest_details
                           {
                               ID = a.ID,
                               Title = a.Title,
                               TicketCode = a.TicketCode,
                               BuildingID = a.BuildingID,
                               BuildingName = (a.BuildingID != null && a.BuildingID != 0) ?
                               supportRepository.GetByBuildingId(a.BuildingID).Name : "NA",
                               CategoryID = a.CategoryID,
                               CategoryName = cat != null ? cat.Name : "NA",
                               Description = a.Description,
                               LocationID = a.LocationID,
                               //  LocationName = (a.LocationID != 0 && a.LocationID != null) ? locations.Where(m => m.ID == a.LocationID).FirstOrDefault().Name : "NA",
                               PriorityID = a.PriorityID,
                               PriorityName = prior != null ? prior.Name : "NA",
                               RequestedCompletionDate = a.RequestedCompletionDate != null ?
                               NullableDateTimeToStringDate(a.RequestedCompletionDate) : "NA",
                               RoomID = a.RoomID,
                               RoomName = (a.RoomID != null && a.RoomID != 0) ?
                               supportRepository.GetByRoomId(a.RoomID).Name : "NA",
                               StatusName = status != null ? status.Name : "NA",
                               SubCategoryID = a.SubCategoryID,
                               SubCategoryName = subcat != null ? subcat.Name : "NA",
                               SupportTypeID = a.SupportTypeID,
                               SupportTypeName = support != null ? support.Name : "NA",
                               ModifiedDateTime = a.StatusID == 7 ? NullableDateTimeToStringDateTime(a.ModifiedDate).DateTime : "NA",
                               RequestEmployeeID = a.InsertedId,
                               RequestOn = NullableDateTimeToStringDateTime(a.InsertedDate).DateTime,
                               CompletionOn = NullableDateTimeToStringDateTime(a.CompletionDate).DateTime,
                               RequestEmployeeName = emp.FirstName + " " + emp.LastName,
                               RequestEmployeeCode = emp.EmpCode,
                               RequestAttachments = attach,
                               Comment = a.Comment,
                               DeptHead = depthead,
                               //  RequestAttachments = attach.Where(u => u.SupportRequestID == a.ID && u.Type == "RAISED").ToList(),
                               // ResolveAttachments = attach.Where(u => u.SupportRequestID == a.ID && u.Type == "COMMENT").ToList(),
                               supporttimelinecls = getRecentComment(a.ID),
                               supporttimelinecomments = getcommenttimelines(a.ID),
                               DeadlineDate = NullableDateTimeToStringDate(a.DueDate),
                               AssignToEmpName = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId == userId ? "me" : 
                                                 employeeRepository.GetEmployeeFullNameById(assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId) : "NA" : "NA",
                               AssignedDate = assignedTo != null ? assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ?
                                              assignedTo.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().ModifiedDate.ToString() : "NA" : "NA",
                               Management = (
                               a.SendApproval,
                                NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                 a.SentApprovalID != null && a.SentApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, userId) : "NA",
                               a.ManagementApproval,
                               NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                               a.ManagementApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, userId) : "NA",
                               a.ManagementReject,
                               NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                               a.ManagementRejectedID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, userId) : "NA")

                           }).FirstOrDefault();




                return res;
            }
            else
            {
                return null;
            }

        }

        public AssignEmployeeModel GetAssignedEmployeeName(long supportid)
        {
            var assignedTo = _dbContext.SupportRequestAssign.Where(a => a.SupportRequestId == supportid).ToList();
            var result = (from a in assignedTo
                          select new AssignEmployeeModel
                          {
                              ID = a.ID,
                              Name = employeeRepository.GetEmployeeFullNameById(a.EmployeeId),
                              AssignedDate = a.InsertedDate
                          }).FirstOrDefault();

            return result;
        }
        public List<supporttimelinecls> gettimelines(long id)
        {
            var emp = employeeRepository.GetAllEmployee();
            var supporttimeline = supportRepository.GetAllSupportComment();
            var supportattachment = supportRepository.GetAllAttachment();
            var res = (from a in supporttimeline
                       join b in emp on a.InsertedId equals b.ID
                       where a.SupportRequestID == id
                       select new supporttimelinecls
                       {
                           Description = a.Name == null ? "--" : a.Name,
                           commentedBy = a.InsertedId.ToString(),
                           commentedByName = b.FirstName + " " + b.LastName,
                           insertedOn = a.InsertedDate.ToString("dd-MMM-yyyy  hh:mm tt"),
                           ResolveAttachments = supportattachment.Where(m => m.SupportRequestID == a.ID || m.Type == "COMMENT").ToList(),

                       }).ToList();
            return res;
        }

        public List<supporttimelinecls> getRecentComment(long supportRequestId)
        {
            List<supporttimelinecls> list = new List<supporttimelinecls>();
            List<SupportComment> comments = new List<SupportComment>();
            SupportComment supporttimeline = supportRepository.GetLastCommentBySupportId(supportRequestId);
            if (supporttimeline != null)
            {
                comments.Add(supporttimeline);
                var emp = employeeRepository.GetEmployeeById(supporttimeline.InsertedId);
                var res = (from a in comments

                           select new supporttimelinecls
                           {
                               Description = a.Name == null ? "NA" : a.Name,

                               commentedByName = emp.FirstName + " " + emp.LastName,
                               insertedOn = NullableDateTimeToStringDateTime(a.InsertedDate).DateTime,


                           }).ToList();
                if (res != null)
                    list.AddRange(res);

            }




            return list;
        }

        public List<supporttimelinecls> getRecenttimelines(long id)
        {
            var emp = employeeRepository.GetAllEmployee();
            var supporttimeline = supportRepository.GetAllSupportComment();
            var supportattachment = supportRepository.GetAllAttachment();
            var res = (from a in supporttimeline
                       join b in emp on a.InsertedId equals b.ID
                       where a.SupportRequestID == id
                       select new supporttimelinecls
                       {
                           Description = a.Name == null ? "--" : a.Name,
                           commentedBy = a.InsertedId.ToString(),
                           commentedByName = b.FirstName + " " + b.LastName,
                           insertedOn = a.InsertedDate.ToString("dd-MMM-yyyy  hh:mm tt"),
                           ResolveAttachments = supportattachment.Where(m => m.SupportRequestID == a.ID || m.Type == "COMMENT").ToList(),

                       }).LastOrDefault();

            List<supporttimelinecls> list = new List<supporttimelinecls>();

            if (res != null)
                list.Add(res);

            return list;
        }

        public List<supporttimelinecls> getcommenttimelines(long id)
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            var supporttimeline = supportRepository.GetTimelineBySupportId(id);
            var assignedTo = _dbContext.SupportRequestAssign.Where(a => a.SupportRequestId == id).FirstOrDefault();

            var res = (from a in supporttimeline

                       select new supporttimelinecls
                       {
                           Description = a.comment == null ? "NA" : a.comment,

                           commentedByName = common.GetEmployeeNameByEmployeeId(a.InsertedId, userId),
                           insertedOn = NullableDateTimeToStringDateTime(a.InsertedDate).DateTime,
                           StatusName = a.Status,
                           AssignedToName = assignedTo != null ? employeeRepository.GetEmployeeFullNameById(assignedTo.EmployeeId) : "NA"
                       }).ToList();
            return res;
        }

        public SupportDetailsViewCls SupportRequestDetails(long supportrequestid)
        {
            var emp = employeeRepository.GetAllEmployee();
            var empdes = employeeDesignationRepository.GetAllEmployeeDesignations();
            var desg = designationRepository.GetAllDesignation();
            var dept = departmentRepository.GetAllDepartment();
            var supportrequestsid = supportRepository.GetBySupportRequestId(supportrequestid);
            var supportrequest = supportRepository.GetAllSupportRequest();
            var cat = supportRepository.GetAllCategory();
            var support = supportRepository.GetAllSupportType();
            var subcat = supportRepository.GetAllSubCategory();
            var room = supportRepository.GetAllRoom();
            var build = supportRepository.GetAllBuilding();
            var prior = supportRepository.GetAllPriority();
            var status = supportRepository.GetAllStatus();
            var attach = supportRepository.GetAllAttachment();
            var deptsupport = supportRepository.GetAllDepartmentSupport();

            EmployeeDetails employeecls = new EmployeeDetails();
            var employeedetails = employeeRepository.GetEmployeeById(supportrequestsid.InsertedId);

            employeecls.EmployeeName = employeedetails.FirstName + " " + employeedetails.LastName;
            employeecls.ID = employeedetails.ID;
            employeecls.Mobile = employeedetails.PrimaryMobile;
            if (employeedetails.Image != null)
            {
                string path = Path.Combine("/ODMImages/EmployeeProfile/", employeedetails.Image);
                employeecls.ProfileImage = path;
            }


            SupportDetailsViewCls res = new SupportDetailsViewCls();

            res.ID = supportrequestid;
            res.IsApprove = supportrequestsid.IsApprove == true ? true : false;
            res.IsReject = supportrequestsid.IsReject == true ? true : false;

            res.depart = common.GetAllDesignationByUsingEmployeeId(supportrequestsid.InsertedId);
            res.employees = employeecls;
            res.requests = (from a in supportrequest
                            join b in cat on a.CategoryID equals b.ID
                            join c in subcat on a.SubCategoryID equals c.ID
                            //join d in build on a.BuildingID equals d.ID
                            //join e in room on a.RoomID equals e.ID
                            join g in status on a.StatusID equals g.ID
                            join h in prior on a.PriorityID equals h.ID
                            join f in support on a.SupportTypeID equals f.ID
                            where a.ID == supportrequestid
                            select new SupportRequestDetails
                            {
                                ID = a.ID,
                                CategoryName = b.Name,
                                SubCategoryName = c.Name,
                                BuildingName = a.BuildingID != 0 ? build.Where(m => m.ID == a.BuildingID).FirstOrDefault().Name : "",
                                RoomName = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name : "",
                                SupportTypeName = f.Name,
                                Description = a.Description,
                                StatusID = a.StatusID,
                                StatusName = a.StatusID == 2 && a.IsApprove != true ? "Verification Pending" : a.StatusName,
                                PriorityName = h.Name,
                                attachments = attach.Where(u => u.SupportRequestID == a.ID && u.Type == "RAISED").ToList(),
                                attachments2 = attach.Where(p => p.SupportRequestID == a.ID && p.Type == "COMMENT").ToList()
                            }).FirstOrDefault();


            return res;

        }



        public IActionResult SupportApproveReject()
        {
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            //if (commonMethods.checkaccessavailable("Approve/Reject", accessId, "Approve", "Support", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}


            var deptLeadList = departmentLeadRepository.GetAllByEmployeeId(userId);

            if (deptLeadList == null || deptLeadList.Count() == 0 && roleId != 1)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            else
            {

                HashSet<DateTime> activeDates = new HashSet<DateTime>();
                List<DateStringValue> finalDates = new List<DateStringValue>();

                var status = supportRepository.GetAllStatus().ToList();
                var priority = supportRepository.GetAllPriority().ToList();

                finalDates = (from a in activeDates.ToList()
                              select new DateStringValue
                              {
                                  ActiveDate = a.Date,
                                  ActiveDateValue = a.ToString("dd-MMM-yyyy")
                              }

                                 )
                    .ToList();

                SupportModelList model = new SupportModelList();
                //  model.statuses = status;
                model.all_date = finalDates;
                //  model.priorities = priority;

                return View(model);
            }
        }

        public async Task<ActionResult> ApproveSupportRequest(long? id)
        {

            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            if (common.checkaccessavailable("Approve/Reject", accessId, "ApproveReject", "Support", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var request = supportRepository.GetBySupportRequestId(id.Value);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            request.ApprovedID = userId;
            request.ApproveDate = DateTime.Now;
            request.IsApprove = true;
            request.IsReject = false;
            request.RejectedID = 0;
            request.RejectDate = null;
            request.StatusID = 4;
            request.StatusName = request.StatusID == 0 ? "APPROVED" : supportRepository.GetByStatusId(request.StatusID).Name;
            supportRepository.UpdateSupportRequest(request);

            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.SupportRequestID = request.ID;
            supportTimeLine.SupportStatusID = request.StatusID;
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.Status = request.StatusID == 0 ? "APPROVED" : supportRepository.GetByStatusId(request.StatusID).Name;
            supportRepository.CreateSupportTimeLine(supportTimeLine);

            var employees = employeeRepository.GetEmployeeById(request.InsertedId);

            // var empid = departmentLeadRepository.GetAllByEmployeeId(employees.ID);

            string name = employees.FirstName + " " + employees.LastName;
            string code = employees.EmpCode;
            string email = employees.EmailId;
            string phone = employees.PrimaryMobile;
            string category = "Employee";
            string whom = "Employee";
            string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;

            List<DepartmentLead> departmentIdLead = departmentLeadRepository.GetAllByEmployeeId(userId).ToList();

            if (departmentIdLead.Count() <= 0)

                smssend.deadlinedatesupport(request.Title, name, email, phone, code, category, typename, "New Approval Support Request-" + request.Title + "-" + name, whom, employees.EmailId);


            return RedirectToAction(nameof(SupportApproveReject));
        }

        public async Task<ActionResult> RejectSupportRequest(long? id)
        {
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            if (common.checkaccessavailable("Approve/Reject", accessId, "ApproveReject", "Support", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var request = supportRepository.GetBySupportRequestId(id.Value);
            long userId = HttpContext.Session.GetInt32("userId").Value;

            request.RejectedID = userId;
            request.RejectDate = DateTime.Now;
            request.IsReject = true;
            request.IsApprove = false;
            request.ApprovedID = 0;
            request.ApproveDate = null;
            request.StatusID = 5;
            request.StatusName = request.StatusID == 0 ? "APPROVED" : supportRepository.GetByStatusId(request.StatusID).Name;
            supportRepository.UpdateSupportRequest(request);

            SupportTimeLine supportTimeLine = new SupportTimeLine();
            supportTimeLine.SupportRequestID = request.ID;
            supportTimeLine.SupportStatusID = request.StatusID;
            supportTimeLine.InsertedId = userId;
            supportTimeLine.ModifiedId = userId;
            supportTimeLine.Active = true;
            supportTimeLine.Status = request.StatusID == 0 ? "APPROVED" : supportRepository.GetByStatusId(request.StatusID).Name;
            supportRepository.CreateSupportTimeLine(supportTimeLine);

            return RedirectToAction(nameof(SupportApproveReject));

        }

        public JsonResult SupportListApproveReject(string dateid, long statusid, long priorityid)
        {

            long userId = HttpContext.Session.GetInt32("userId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            if (roleId != 1)
            {
                List<Int64> departmentIdLead = departmentLeadRepository.GetAllByEmployeeId(userId).Select(a => a.DepartmentID).Distinct().ToList();

                List<Int64> supportIdLead = supportRepository.GetAllDepartmentSupport().Where(a => a.Active == true &&
                departmentIdLead.Contains(a.DepartmentID)).Select(a => a.SupportTypeID).Distinct().ToList();


                var requests = supportRepository.GetBySupportRequestByNotSupportTypeId(supportIdLead);

                var status = supportRepository.GetAllStatus().ToList();

                var cat = supportRepository.GetAllCategory().ToList();
                var subcat = supportRepository.GetAllSubCategory().ToList();
                var loc = supportRepository.GetAllLocation().ToList();
                var build = supportRepository.GetAllBuilding();
                var room = supportRepository.GetAllRoom();
                var prior = supportRepository.GetAllPriority().ToList();
                var des = designationRepository.GetAllDesignation().Where(a => a.Active == true && departmentIdLead.Contains(a.DepartmentID)).ToList();

                List<Int64> desIds = des.Select(b => b.ID).ToList();
                List<Int64> empIdIdLead = employeeDesignationRepository.GetAllEmployeeDesignationsByDesignationId(desIds).Select(a => a.EmployeeID).Distinct().ToList();


                var dept = departmentRepository.GetAllDepartment().ToList();//.Where(a=>a.Active==true&& departmentIdLead.Contains(a.ID));

                var emp = employeeRepository.GetAllByEmployeeId(empIdIdLead).ToList();
                var support = supportRepository.GetAllSupportType().ToList();
                var deptsupportlist = supportRepository.GetAllDepartmentSupport().ToList();//.Where(a => a.Active == true && departmentIdLead.Contains(a.DepartmentID)).


                List<SupportRequestCls> res = (from a in requests
                                               join b in cat on a.CategoryID equals b.ID
                                               join c in subcat on a.SubCategoryID equals c.ID
                                               // join d in loc on a.LocationID equals d.ID
                                               //join e in build on a.BuildingID equals e.ID
                                               //join f in room on a.RoomID equals f.ID
                                               join g in prior on a.PriorityID equals g.ID
                                               // join h in status on a.StatusID equals h.ID
                                               join i in emp on a.InsertedId equals i.ID
                                               join m in support on a.SupportTypeID equals m.ID

                                               select new SupportRequestCls
                                               {
                                                   ID = a.ID,
                                                   CategoryName = b.Name,
                                                   SubCategoryName = c.Name,
                                                   BuildingName = a.BuildingID != 0 ? build.Where(y => y.ID == a.BuildingID).FirstOrDefault().Name : "",
                                                   RoomName = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name : "",
                                                   PriorityName = g.Name,
                                                   Status = a.StatusName,
                                                   StatusID = a.StatusID,
                                                   t_Date = a.InsertedDate,
                                                   PriorityID = a.PriorityID,
                                                   emp_Name = i.FirstName + " " + i.LastName,
                                                   emp_Phone = i.PrimaryMobile != null ? i.PrimaryMobile : "",
                                                   // emp_Dept = l.Name,
                                                   SupportTypeID = a.SupportTypeID,
                                                   SupportTypeName = m.Name,
                                                   //IsApprove = a.IsApprove,
                                                   // IsReject = a.IsReject,
                                                   RejectDate = a.RejectDate != null ? a.RejectDate.Value.ToString("dd-MM-yyyy") : "",
                                                   ApproveDate = a.ApproveDate != null ? a.ApproveDate.Value.ToString("dd-MM-yyyy") : "",
                                                   ApproveReject = a.ApproveDate != null ? "Approved" : (a.RejectDate != null ? "Rejected" : ""),
                                                   DueDate = a.DueDate != null ? a.DueDate.Value.ToString("dd-MM-yyyy") : ""
                                               }).ToList();

                if (statusid != 0 && statusid != null)
                {
                    res = res.Where(a => a.StatusID == statusid).ToList();
                }
                if (dateid != "" && dateid != null)
                {
                    DateTime dt = Convert.ToDateTime(dateid);
                    res = res.Where(a => a.t_Date.Value.Date == dt.Date).ToList();
                }
                if (priorityid != 0 && priorityid != null)
                {
                    res = res.Where(a => a.PriorityID == priorityid).ToList();
                }

                return Json(new { data = res });
            }
            else
            {
                var requests = supportRepository.GetAllSupportRequest().ToList();
                var status = supportRepository.GetAllStatus().ToList();
                var emp = employeeRepository.GetAllEmployee().ToList();
                var support = supportRepository.GetAllSupportType().ToList();
                var cat = supportRepository.GetAllCategory().ToList();
                var subcat = supportRepository.GetAllSubCategory().ToList();
                var loc = supportRepository.GetAllLocation().ToList();
                var build = supportRepository.GetAllBuilding();
                var room = supportRepository.GetAllRoom();
                var prior = supportRepository.GetAllPriority().ToList();

                List<SupportRequestCls> res = (from a in requests
                                               join b in cat on a.CategoryID equals b.ID
                                               join c in subcat on a.SubCategoryID equals c.ID
                                               // join d in loc on a.LocationID equals d.ID
                                               // join e in build on a.BuildingID equals e.ID
                                               // join f in room on a.RoomID equals f.ID
                                               join g in prior on a.PriorityID equals g.ID
                                               // join h in status on a.StatusID equals h.ID
                                               join i in emp on a.InsertedId equals i.ID
                                               join m in support on a.SupportTypeID equals m.ID

                                               select new SupportRequestCls
                                               {
                                                   ID = a.ID,
                                                   CategoryName = b.Name,
                                                   SubCategoryName = c.Name,
                                                   BuildingName = a.BuildingID != 0 ? build.Where(y => y.ID == a.BuildingID).FirstOrDefault().Name : "",
                                                   RoomName = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name : "",
                                                   PriorityName = g.Name,
                                                   Status = a.StatusName,
                                                   StatusID = a.StatusID,
                                                   t_Date = a.InsertedDate,
                                                   PriorityID = a.PriorityID,
                                                   emp_Name = i.FirstName + " " + i.LastName,
                                                   emp_Phone = i.PrimaryMobile != null ? i.PrimaryMobile : "",
                                                   // emp_Dept = l.Name,
                                                   SupportTypeID = a.SupportTypeID,
                                                   SupportTypeName = m.Name,
                                                   //IsApprove = a.IsApprove,
                                                   // IsReject = a.IsReject,
                                                   RejectDate = a.RejectDate != null ? a.RejectDate.Value.ToString("dd-MM-yyyy") : "",
                                                   ApproveDate = a.ApproveDate != null ? a.ApproveDate.Value.ToString("dd-MM-yyyy") : "",
                                                   ApproveReject = a.ApproveDate != null ? "Approved" : (a.RejectDate != null ? "Rejected" : ""),
                                                   DueDate = a.DueDate != null ? a.DueDate.Value.ToString("dd-MM-yyyy") : ""
                                               }).ToList();

                if (statusid != 0 && statusid != null)
                {
                    res = res.Where(a => a.StatusID == statusid).ToList();
                }
                if (dateid != "" && dateid != null)
                {
                    DateTime dt = Convert.ToDateTime(dateid);
                    res = res.Where(a => a.t_Date.Value.Date == dt.Date).ToList();
                }
                if (priorityid != 0 && priorityid != null)
                {
                    res = res.Where(a => a.PriorityID == priorityid).ToList();
                }


                return Json(new { data = res });


            }
        }


        #endregion

        #region EMAIL

        //public IActionResult GetEmails()
        //{
        //    SupportRequest request = new SupportRequest();

        //    var Department_Id = supportRepository.GetBySupportTypeId(request.SupportTypeID).DepartmentID;

        //    var submoduleid = subModuleRepository.GetSubModuleByName("Management Approval").ID;

        //    List<long> actiondept = actionAccessRepository.GetAllDepartmentIdBySubmoduleId(submoduleid).ToList();

        //    List<long> DesignationIdes = designationRepository.GetDesignationByDepartmentId(Department_Id).ToList();

        //    //Employee Head Details
        //    List<DepartmentLead> employeeLeadid = departmentLeadRepository.GetAllEmployeeIdByDepartment(Department_Id).ToList();

        //    List<long> employeeLeadid_Emplyee_Ides = employeeLeadid.Select(e => e.EmployeeID).ToList();
        //    // Department Employee Details
        //    List<EmployeeDesignation> EmployeeIdes = employeeDesignationRepository.GetAllEmployeeDesignationsByDesignationId(DesignationIdes).ToList();
        //    List<long> Department_Emplyee_Ides = EmployeeIdes.Select(e => e.EmployeeID).ToList();


        //    return View();
        //}

        #endregion

        #region SupportReport

        public IActionResult SupportTabularReport(long? datetype, string f_date, string t_date, string m_date, long? supporttype_ddl)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;

            long userId = HttpContext.Session.GetInt32("userId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            if (common.checkaccessavailable("Report", accessId, "List", "Support", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.fdt = "";
            ViewBag.tdt = "";
            if (common.checkaccessavailable("Report", accessId, "Support Type", "Support", roleId) == true)
            {
                ViewBag.supportType = supportRepository.GetAllSupportType().ToList();
                ViewBag.supporttypeid = supporttype_ddl;
            }
            else
            {
                ViewBag.supportType = null;
                ViewBag.supporttypeid = 0;
            }

            ViewBag.datetypeid = datetype;
            if (m_date != null)
            {

                ViewBag.mdt = m_date;
            }
            else
            {
                ViewBag.mdt = null;
            }

            var employee = employeeRepository.GetAllEmployee().ToList();
            var employeedesignation = employeeDesignationRepository.GetAllEmployeeDesignations().ToList();
            var designation = designationRepository.GetAllDesignation().ToList();
            var department = departmentRepository.GetAllDepartment().ToList();
            var organization = organizationRepository.GetAllOrganization();
            var req = supportRepository.GetAllSupportRequest().ToList();
            var supptype = supportRepository.GetAllSupportType().ToList();

            //ViewBag.organizations = (from a in organization
            //                         join b in department on a.GroupID equals b.GroupID
            //                         join c in designation on b.ID equals c.DepartmentID
            //                         join d in employeedesignation on c.ID equals d.DesignationID
            //                         join e in employee on d.EmployeeID equals e.ID
            //                         join f in supptype on b.ID equals f.DepartmentID
            //                         join g in req on f.ID equals g.SupportTypeID
            //                         where g.SupportTypeID == supporttype_ddl
            //                         select a).Distinct().ToList();


            //ViewBag.organizations = (from a in organization
            //                         join b in department on a.GroupID equals b.GroupID
            //                         join c in designation on b.ID equals c.DepartmentID
            //                         join d in employeedesignation on c.ID equals d.DesignationID
            //                         join e in employee on d.EmployeeID equals e.ID

            //                         select a).Distinct().ToList();

            var allempdept = common.GetDepartmentsByEmployeeID(userId);

            bool group = false;
            foreach (var x in allempdept)
            {
                if (x.groupid > 0)
                {
                    group = true;

                }
            }

            ViewBag.groups = group;

            if (f_date != null && t_date != null)
            {
                if (Convert.ToDateTime(f_date) <= Convert.ToDateTime(t_date))
                {
                    string fdtt = f_date;
                    string tdtt = t_date;
                    DateTime f_dt = Convert.ToDateTime(f_date);
                    DateTime t_dt = Convert.ToDateTime(t_date);
                    ViewBag.fdt = f_dt.ToString("yyyy-MM-dd");
                    ViewBag.tdt = t_dt.ToString("yyyy-MM-dd");
                    ViewBag.messg = "";
                    var res = commonMethods.GetAllReportDetails(datetype, f_dt, t_dt, m_date, supporttype_ddl, accessId, roleId, userId);
                    // ViewBag.raised = res.Sum(a => a.Raise) + "(" + (res.Sum(a => a.Raise) != 0 ? (int)Math.Round((double)(100 * (res.Sum(a => a.Raise))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.complete = res.Sum(a => a.Resolve) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Resolve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Resolve))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.pending = res.Sum(a => a.Pending) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Pending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Pending))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.verified = res.Sum(a => a.Verify) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Verify) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Verify))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.inprogress = res.Sum(a => a.Inprogress) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Inprogress) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Inprogress))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.onhold = res.Sum(a => a.Onhold) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Onhold) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Onhold))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.rejected = res.Sum(a => a.Reject) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Reject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Reject))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.approved = res.Sum(a => a.Approve) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Approve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Approve))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.approvalpending = res.Sum(a => a.ApprovalPending) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.ApprovalPending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.ApprovalPending))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.mgtreject = res.Sum(a => a.MgtReject) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.MgtReject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.MgtReject))) / res.Sum(a => a.Raise)) : 0) + "%)";


                    ViewBag.raised = res.Sum(a => a.Raise);
                    ViewBag.complete = res.Sum(a => a.Resolve);
                    ViewBag.pending = res.Sum(a => a.Pending);
                    ViewBag.verified = res.Sum(a => a.Verify);
                    ViewBag.inprogress = res.Sum(a => a.Inprogress);
                    ViewBag.onhold = res.Sum(a => a.Onhold);
                    ViewBag.rejected = res.Sum(a => a.Reject);
                    ViewBag.approved = res.Sum(a => a.Approve);
                    ViewBag.approvalpending = res.Sum(a => a.ApprovalPending);
                    ViewBag.mgtreject = res.Sum(a => a.MgtReject);
                    ViewBag.SendApproval = res.Sum(a => a.SendApproval);

                    ViewBag.raisedP = (res.Sum(a => a.Raise) != 0 ? (int)Math.Round((double)(100 * (res.Sum(a => a.Raise))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.completeP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Resolve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Resolve))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.pendingP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Pending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Pending))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.verifiedP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Verify) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Verify))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.inprogressP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Inprogress) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Inprogress))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.onholdP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Onhold) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Onhold))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.rejectedP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Reject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Reject))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.approvedP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Approve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Approve))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.approvalpendingP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.ApprovalPending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.ApprovalPending))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.mgtrejectP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.MgtReject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.MgtReject))) / res.Sum(a => a.Raise)) : 0);

                    return View(res);
                }
                else
                {

                    ViewBag.messg = "From date should be less or equal to todate.";
                    //ViewBag.vfdt = DateTime.Now.ToString("yyyy-MM-dd");
                    //ViewBag.vtdt = DateTime.Now.ToString("yyyy-MM-dd");
                    var res = commonMethods.GetAllReportDetails(null, null, null, null, supporttype_ddl, accessId, roleId, userId);
                    //ViewBag.raised = res.Sum(a => a.Raise) + "(" + (res.Sum(a => a.Raise) != 0 ? (int)Math.Round((double)(100 * (res.Sum(a => a.Raise))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.complete = res.Sum(a => a.Resolve) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Resolve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Resolve))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.pending = res.Sum(a => a.Pending) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Pending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Pending))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.verified = res.Sum(a => a.Verify) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Verify) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Verify))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.inprogress = res.Sum(a => a.Inprogress) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Inprogress) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Inprogress))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.onhold = res.Sum(a => a.Onhold) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Onhold) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Onhold))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.rejected = res.Sum(a => a.Reject) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Reject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Reject))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.approved = res.Sum(a => a.Approve) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Approve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Approve))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.approvalpending = res.Sum(a => a.ApprovalPending) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.ApprovalPending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.ApprovalPending))) / res.Sum(a => a.Raise)) : 0) + "%)";
                    //ViewBag.mgtreject = res.Sum(a => a.MgtReject) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.MgtReject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.MgtReject))) / res.Sum(a => a.Raise)) : 0) + "%)";

                    ViewBag.raised = res.Sum(a => a.Raise);
                    ViewBag.complete = res.Sum(a => a.Resolve);
                    ViewBag.pending = res.Sum(a => a.Pending);
                    ViewBag.verified = res.Sum(a => a.Verify);
                    ViewBag.inprogress = res.Sum(a => a.Inprogress);
                    ViewBag.onhold = res.Sum(a => a.Onhold);
                    ViewBag.rejected = res.Sum(a => a.Reject);
                    ViewBag.approved = res.Sum(a => a.Approve);
                    ViewBag.approvalpending = res.Sum(a => a.ApprovalPending);
                    ViewBag.mgtreject = res.Sum(a => a.MgtReject);
                    ViewBag.SendApproval = res.Sum(a => a.SendApproval);

                    ViewBag.raisedP = (res.Sum(a => a.Raise) != 0 ? (int)Math.Round((double)(100 * (res.Sum(a => a.Raise))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.completeP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Resolve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Resolve))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.pendingP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Pending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Pending))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.verifiedP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Verify) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Verify))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.inprogressP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Inprogress) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Inprogress))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.onholdP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Onhold) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Onhold))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.rejectedP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Reject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Reject))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.approvedP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Approve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Approve))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.approvalpendingP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.ApprovalPending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.ApprovalPending))) / res.Sum(a => a.Raise)) : 0);
                    ViewBag.mgtrejectP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.MgtReject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.MgtReject))) / res.Sum(a => a.Raise)) : 0);

                    return View(res);
                }
            }
            else
            {
                //ViewBag.vfdt = DateTime.Now.ToString("yyyy-MM-dd");
                //ViewBag.vtdt = DateTime.Now.ToString("yyyy-MM-dd");
                var res = commonMethods.GetAllReportDetails(datetype, null, null, m_date, supporttype_ddl, accessId, roleId, userId);
                //ViewBag.raised = res.Sum(a => a.Raise) + "(" + (res.Sum(a => a.Raise) != 0 ? (int)Math.Round((double)(100 * (res.Sum(a => a.Raise))) / res.Sum(a => a.Raise)) : 0) + "%)";
                //ViewBag.complete = res.Sum(a => a.Resolve) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Resolve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Resolve))) / res.Sum(a => a.Raise)) : 0) + "%)";
                //ViewBag.pending = res.Sum(a => a.Pending) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Pending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Pending))) / res.Sum(a => a.Raise)) : 0) + "%)";
                //ViewBag.verified = res.Sum(a => a.Verify) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Verify) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Verify))) / res.Sum(a => a.Raise)) : 0) + "%)";
                //ViewBag.inprogress = res.Sum(a => a.Inprogress) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Inprogress) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Inprogress))) / res.Sum(a => a.Raise)) : 0) + "%)";
                //ViewBag.onhold = res.Sum(a => a.Onhold) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Onhold) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Onhold))) / res.Sum(a => a.Raise)) : 0) + "%)";
                //ViewBag.rejected = res.Sum(a => a.Reject) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Reject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Reject))) / res.Sum(a => a.Raise)) : 0) + "%)";
                //ViewBag.approved = res.Sum(a => a.Approve) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Approve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Approve))) / res.Sum(a => a.Raise)) : 0) + "%)";
                //ViewBag.approvalpending = res.Sum(a => a.ApprovalPending) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.ApprovalPending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.ApprovalPending))) / res.Sum(a => a.Raise)) : 0) + "%)";
                //ViewBag.mgtreject = res.Sum(a => a.MgtReject) + "(" + ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.MgtReject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.MgtReject))) / res.Sum(a => a.Raise)) : 0) + "%)";

                ViewBag.raised = res.Sum(a => a.Raise);
                ViewBag.complete = res.Sum(a => a.Resolve);
                ViewBag.pending = res.Sum(a => a.Pending);
                ViewBag.verified = res.Sum(a => a.Verify);
                ViewBag.inprogress = res.Sum(a => a.Inprogress);
                ViewBag.onhold = res.Sum(a => a.Onhold);
                ViewBag.rejected = res.Sum(a => a.Reject);
                ViewBag.approved = res.Sum(a => a.Approve);
                ViewBag.approvalpending = res.Sum(a => a.ApprovalPending);
                ViewBag.mgtreject = res.Sum(a => a.MgtReject);
                ViewBag.SendApproval = res.Sum(a => a.SendApproval);

                ViewBag.raisedP = (res.Sum(a => a.Raise) != 0 ? (int)Math.Round((double)(100 * (res.Sum(a => a.Raise))) / res.Sum(a => a.Raise)) : 0);
                ViewBag.completeP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Resolve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Resolve))) / res.Sum(a => a.Raise)) : 0);
                ViewBag.pendingP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Pending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Pending))) / res.Sum(a => a.Raise)) : 0);
                ViewBag.verifiedP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Verify) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Verify))) / res.Sum(a => a.Raise)) : 0);
                ViewBag.inprogressP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Inprogress) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Inprogress))) / res.Sum(a => a.Raise)) : 0);
                ViewBag.onholdP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Onhold) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Onhold))) / res.Sum(a => a.Raise)) : 0);
                ViewBag.rejectedP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Reject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Reject))) / res.Sum(a => a.Raise)) : 0);
                ViewBag.approvedP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.Approve) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.Approve))) / res.Sum(a => a.Raise)) : 0);
                ViewBag.approvalpendingP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.ApprovalPending) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.ApprovalPending))) / res.Sum(a => a.Raise)) : 0);
                ViewBag.mgtrejectP = ((res.Sum(a => a.Raise) != 0 && res.Sum(a => a.MgtReject) != 0) ? (int)Math.Round((double)(100 * (res.Sum(a => a.MgtReject))) / res.Sum(a => a.Raise)) : 0);

                return View(res);
            }
        }

        public IActionResult SupportReport1(long? supporttypeid, long? categoryid, long? subcategoryid)
        {
            SupportReports rep = new SupportReports();
            rep.categories = supportRepository.GetAllCategory().Where(a => a.SupportTypeID == supporttypeid).ToList();
            rep.subcategories = supportRepository.GetAllSubCategory().Where(a => a.CategoryID == categoryid).ToList();
            rep.supportTypes = supportRepository.GetAllSupportType().ToList();

            var res = supportRepository.GetAllSupportRequest();

            if (supporttypeid != 0 && supporttypeid != null)
            {
                res = res.Where(a => a.SupportTypeID == supporttypeid).ToList();
            }
            if (categoryid != 0 && categoryid != null)
            {
                res = res.Where(a => a.CategoryID == categoryid).ToList();
            }
            if (subcategoryid != 0 && subcategoryid != null)
            {
                res = res.Where(a => a.SubCategoryID == subcategoryid).ToList();
            }



            ViewBag.Raised = res.Where(a => a.Active == true).Count();
            ViewBag.Resolved = res.Where(a => a.Active == true && a.StatusName == "Completed" || a.StatusID == 2).Count();

            ViewBag.Pending = res.Where(a => a.Active == true && a.StatusName != "Completed" || a.StatusID != 2).Count();

            return View(rep);
        }

        public IActionResult SupportReport(long? supporttype_ddl, string f_Date, string t_Date)
        {
            CheckLoginStatus();
            //long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;

            if (common.checkaccessavailable("Departmentwise Report", accessId, "List", "Support", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.vfdt = "";
            ViewBag.vtdt = "";
            ViewBag.supportType = supportRepository.GetAllSupportType().ToList();
            SupportReports rep = new SupportReports();


            var allempdept = common.GetDepartmentsByEmployeeID(userId);
            List<Web.Models.CommonMasterModel> mast = new List<Web.Models.CommonMasterModel>();
            foreach (var a in allempdept)
            {
                var departments = common.GetallchilddepartmentbasedonParent(a.id);
                mast.AddRange(departments);
            }
            var det = mast.Distinct();

            var request = supportRepository.GetAllSupportRequest();
            var support = supportRepository.GetAllSupportType();

            var res = (from a in request
                       join m in support on a.SupportTypeID equals m.ID
                       // join n in det on m.DepartmentID equals n.ID
                       select a).ToList();


            if (f_Date != null && t_Date != null)
            {
                if (Convert.ToDateTime(f_Date) <= Convert.ToDateTime(t_Date))
                {
                    string fdtt = f_Date;
                    string tdtt = t_Date;
                    DateTime fdt = Convert.ToDateTime(fdtt);
                    DateTime tdt = Convert.ToDateTime(tdtt);
                    ViewBag.vfdt = fdt.ToString("yyyy-MM-dd");
                    ViewBag.vtdt = tdt.ToString("yyyy-MM-dd");
                    //ViewBag.supportType = supportRepository.GetBySupportTypeId(supporttype_ddl.Value).Name;
                    ViewBag.messg = "";
                    res = res.Where(a => a.InsertedDate >= fdt && a.InsertedDate <= tdt).ToList();
                    ViewBag.raised = res.ToList().Count();
                    ViewBag.complete = res.Where(a => a.StatusName == "COMPLETED").ToList().Count();
                    ViewBag.pending = res.Where(a => a.StatusName == "PENDING").ToList().Count();
                    ViewBag.verified = res.Where(a => a.StatusName == "VERIFIED").ToList().Count();
                    ViewBag.inprogress = res.Where(a => a.StatusName == "IN-PROGRESS").ToList().Count();
                    ViewBag.onhold = res.Where(a => a.StatusName == "ON-HOLD").ToList().Count();
                    ViewBag.rejected = res.Where(a => a.StatusName == "REJECTED").ToList().Count();
                    ViewBag.approved = res.Where(a => a.StatusName == "APPROVED").ToList().Count();
                    return View(res);
                    //return View(rep);
                }

                else
                {

                    ViewBag.messg = "From date should be less or equal to todate.";
                    //ViewBag.vfdt = DateTime.Now.ToString("yyyy-MM-dd");
                    //ViewBag.vtdt = DateTime.Now.ToString("yyyy-MM-dd");

                    ViewBag.raised = res.ToList().Count();
                    ViewBag.complete = res.Where(a => a.StatusName == "COMPLETED").ToList().Count();
                    ViewBag.pending = res.Where(a => a.StatusName == "PENDING").ToList().Count();
                    ViewBag.verified = res.Where(a => a.StatusName == "VERIFIED").ToList().Count();
                    ViewBag.inprogress = res.Where(a => a.StatusName == "IN-PROGRESS").ToList().Count();
                    ViewBag.onhold = res.Where(a => a.StatusName == "ON-HOLD").ToList().Count();
                    ViewBag.rejected = res.Where(a => a.StatusName == "REJECTED").ToList().Count();
                    ViewBag.approved = res.Where(a => a.StatusName == "APPROVED").ToList().Count();
                    return View(res);
                }
            }


            else if (f_Date == null && t_Date == null && supporttype_ddl != null)
            {
                //string fdtt = f_Date;
                //string tdtt = t_Date;
                //DateTime fdt = Convert.ToDateTime(fdtt);
                //DateTime tdt = Convert.ToDateTime(tdtt);
                //ViewBag.vfdt = fdt.ToString("yyyy-MM-dd");
                //ViewBag.vtdt = tdt.ToString("yyyy-MM-dd");
                //ViewBag.supportType = supportRepository.GetBySupportTypeId(supporttype_ddl.Value).Name;
                ViewBag.messg = "";
                res = res.Where(a => a.SupportTypeID == supporttype_ddl).ToList();
                ViewBag.raised = res.ToList().Count();
                ViewBag.complete = res.Where(a => a.StatusName == "COMPLETED").ToList().Count();
                ViewBag.pending = res.Where(a => a.StatusName == "PENDING").ToList().Count();
                ViewBag.verified = res.Where(a => a.StatusName == "VERIFIED").ToList().Count();
                ViewBag.inprogress = res.Where(a => a.StatusName == "IN-PROGRESS").ToList().Count();
                ViewBag.onhold = res.Where(a => a.StatusName == "ON-HOLD").ToList().Count();
                ViewBag.rejected = res.Where(a => a.StatusName == "REJECTED").ToList().Count();
                ViewBag.approved = res.Where(a => a.StatusName == "APPROVED").ToList().Count();
                return View(res);
                //return View(rep);
            }


            else
            {
                ViewBag.raised = res.ToList().Count();
                ViewBag.complete = res.Where(a => a.StatusName == "COMPLETED").ToList().Count();
                ViewBag.pending = res.Where(a => a.StatusName == "PENDING").ToList().Count();
                ViewBag.verified = res.Where(a => a.StatusName == "VERIFIED").ToList().Count();
                ViewBag.inprogress = res.Where(a => a.StatusName == "IN-PROGRESS").ToList().Count();
                ViewBag.onhold = res.Where(a => a.StatusName == "ON-HOLD").ToList().Count();
                ViewBag.rejected = res.Where(a => a.StatusName == "REJECTED").ToList().Count();
                ViewBag.approved = res.Where(a => a.StatusName == "APPROVED").ToList().Count();
                return View(res);
                // return View(rep);
            }

        }

        #endregion

        #region TimeLine
        public IActionResult SupportTimeLine()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (common.checkaccessavailable("Support TimeLine", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var request = supportRepository.GetAllSupportRequest().ToList();
            var timeline = supportRepository.GetAllSupportTimeLine().ToList();
            var emp = employeeRepository.GetAllEmployee().ToList();

            var res = (from a in timeline
                       join b in request on a.SupportRequestID equals b.ID
                       join c in emp on b.InsertedId equals c.ID
                       select new SupportTimeLineCls
                       {
                           ID = a.ID,
                           EmployeeName = c.FirstName + " " + c.LastName,
                           InsertedDate = a.InsertedDate,
                           SupportStatus = a.Status
                       }
                     ).ToList();

            return View(res);
        }
        #endregion


        #region Reports
        public IActionResult SupportDetailsReport(string Type,
          long[] OrgId,
           long[] DeptId, long[] SupportTypeId,
           long Status)

        {

            CheckLoginStatus();



            SupportModelList model = new SupportModelList();


            List<long> pIds = new List<long>();
            List<long> statusIds = new List<long>();
            List<long> sTypeIds = new List<long>();
            List<long> deptIds = new List<long>();
            List<long> orgIds = new List<long>();


            model.Type = Type;
            model.OrgId = OrgId;
            model.DeptId = DeptId;
            model.SupportTypeId = SupportTypeId;
            model.StatusID = Status;

            if (OrgId != null && OrgId.Count() > 0)
            {

                orgIds.AddRange(OrgId);
            }

            if (DeptId != null && DeptId.Count() > 0)
            {

                deptIds.AddRange(DeptId);

            }

            if (SupportTypeId != null && SupportTypeId.Count() > 0)
            {


                sTypeIds.AddRange(SupportTypeId);

            }

            List<NameIdViewModel> list = new List<NameIdViewModel>();

            List<SupportRequest> request = new List<SupportRequest>();





            if (Type == null || Type == "All")
            {
                request.AddRange(supportRepository.GetAllSupportRequest().ToList());
            }
            else
            if (Type == "GroupType")
            {
                if (deptIds.Count() > 0)
                {
                    if (sTypeIds.Count() > 0)
                    {
                        request.AddRange(supportRepository.GetAllBySupportTypeId(sTypeIds).ToList());
                    }
                    else
                    {
                        var supportTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds);

                        if (supportTypeIds != null && supportTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.
                                GetAllBySupportTypeId(supportTypeIds.ToList()).ToList());
                        }

                    }
                }
                else
                    request.AddRange(supportRepository.GetAllBySupportTypeId(null).ToList());
            }
            else
            if (Type == "OrganizationType")
            {

                if (orgIds.Count() > 0)
                {
                    if (deptIds.Count() > 0)
                    {
                        if (sTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.
                                GetAllBySupportTypeId(sTypeIds).ToList());
                        }
                        else
                        {
                            var supportTypeIds = supportRepository.
                                GetSupportTypeIdsByDepartmentIdIn(deptIds);

                            if (supportTypeIds != null && supportTypeIds.Count() > 0)
                            {
                                request.AddRange(supportRepository.
                                    GetAllBySupportTypeId(supportTypeIds.ToList())
                                    .ToList());
                            }

                        }
                    }
                    else
                    {

                        var allOrgDeptIds = departmentRepository.
                            GetDepartmentListByOrganizationIdIn(orgIds).Select(a => a.ID);
                        if (allOrgDeptIds != null && allOrgDeptIds.Count() > 0)
                        {
                            var supportTypeIds = supportRepository.
                                GetSupportTypeIdsByDepartmentIdIn(allOrgDeptIds.Distinct().
                                ToList());
                            if (supportTypeIds != null && supportTypeIds.Count() > 0)
                            {
                                request.AddRange(supportRepository.
                                    GetAllBySupportTypeId(supportTypeIds.ToList())
                                    .ToList());

                            }

                        }



                    }

                }
                else
                {

                    var allOrgDeptIds = departmentRepository.GetAllDepartment().Where(a => a.GroupID == null || a.GroupID == 0).Select(a => a.ID);
                    if (allOrgDeptIds != null && allOrgDeptIds.Count() > 0)
                    {
                        var supportTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(allOrgDeptIds.Distinct().ToList());
                        if (supportTypeIds != null && supportTypeIds.Count() > 0)
                        {
                            request.AddRange(supportRepository.GetAllBySupportTypeId(supportTypeIds.ToList()).ToList());

                        }

                    }
                }

            }
            if (request.Count() > 0)
            {

                List<SupportRequest> filterList = new List<SupportRequest>();

                //if (statusIds.Count() > 0 && pIds.Count() > 0)
                //{
                //    filterList.AddRange(request.Where(a => statusIds.Contains(a.StatusID) &&
                //    pIds.Contains(a.PriorityID)));
                //}
                //else
                //if (pIds.Count() > 0)
                //{
                //    filterList.AddRange(request.Where(a =>
                //   pIds.Contains(a.PriorityID)));
                //}
                //else
                //if (statusIds.Count() > 0)
                //{
                //    filterList.AddRange(request.Where(a =>
                //   statusIds.Contains(a.StatusID)));
                //}
                //else
                filterList.AddRange(request);

                if (filterList.Count() > 0)
                {

                    var cat = supportRepository.GetAllCategory().ToList();
                    var subcat = supportRepository.GetAllSubCategory().ToList();
                    var loc = supportRepository.GetAllLocation().ToList();
                    var build = supportRepository.GetAllBuilding();
                    var room = supportRepository.GetAllRoom();
                    var priorities = supportRepository.GetAllPriority().ToList();


                    List<SupportRequestCls> res = (from a in request
                                                   join b in cat on a.CategoryID equals b.ID
                                                   join c in subcat on a.SubCategoryID equals c.ID

                                                   join m in supportRepository.GetAllSupportType() on a.SupportTypeID equals m.ID

                                                   select new SupportRequestCls
                                                   {
                                                       ID = a.ID,
                                                       Title = a.Title,
                                                       TicketCode = a.TicketCode,
                                                       CategoryName = b.Name,
                                                       SubCategoryName = c.Name,
                                                       RequestedCompletionDate = NullableDateTimeToStringDate(a.RequestedCompletionDate),
                                                       BuildingName = a.BuildingID != 0 ? build.Where(y => y.ID == a.BuildingID).FirstOrDefault().Name :
                                                       ViewConstants.NOT_APPLICABLE,
                                                       RoomName = a.RoomID != 0 ? room.Where(z => z.ID == a.RoomID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE,
                                                       PriorityName = priorities != null ? priorities.Where(x => x.ID == a.PriorityID).FirstOrDefault().Name : ViewConstants.NOT_APPLICABLE,
                                                       StatusID = a.StatusID,
                                                       Status = a.StatusName,
                                                       OrgGroupName = common.GetOrgGroupName(a.InsertedId),
                                                       t_Date = a.DueDate != null ? a.DueDate : (a.RequestedCompletionDate == null ? a.InsertedDate : a.RequestedCompletionDate),
                                                       PriorityID = a.PriorityID,

                                                       emp_Name = common.GetEmployeeNameByEmployeeId(a.InsertedId, userId),
                                                       //emp_Phone = i.PrimaryMobile != null ? i.PrimaryMobile : "",
                                                       Description = a.Description,
                                                       SupportTypeID = a.SupportTypeID,
                                                       SupportTypeName = m.Name,
                                                       Comment = a.Comment,
                                                       InsertedDate = a.InsertedDate,
                                                       ModifiedDate = a.ModifiedDate,
                                                       InsertedDateTime = NullableDateTimeToStringDateTime(a.InsertedDate),
                                                       SentApproval = a.SendApproval,
                                                       IsRead = a.IsRead == null ? false : a.IsRead,
                                                       IsManagementRead = a.IsManagementRead == null ? false : a.IsManagementRead,
                                                       DueDate = NullableDateTimeToStringDate(a.DueDate),

                                                       Management = (
                                   a.SendApproval,
                                    NullableDateTimeToStringDateTime(a.SentApprovalDate).DateTime,
                                     a.SentApprovalID != null && a.SentApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.SentApprovalID.Value, userId) : ViewConstants.NOT_APPLICABLE,
                                   a.ManagementApproval,
                                   NullableDateTimeToStringDateTime(a.ManagementApprovedDate).DateTime,
                                   a.ManagementApprovalID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementApprovalID.Value, userId) : ViewConstants.NOT_APPLICABLE,
                                   a.ManagementReject,
                                   NullableDateTimeToStringDateTime(a.ManagementRejectDate).DateTime,
                                   a.ManagementRejectedID > 0 ? common.GetEmployeeNameByEmployeeId(a.ManagementRejectedID, userId) : ViewConstants.NOT_APPLICABLE


                                   ),

                                                       commentcount = supportRepository.GetUnreadSupportCommentCountBySupportIdAndInsertedId(a.ID, userId)

                                                   }
                             ).ToList();



                    if (Status == 0)  //Pending
                    {
                        res = res.Where(m => m.Status == "PENDING" && (m.Management.Sent == false && m.Management.Rejected == false)).ToList();
                    }
                    if (Status == 1) //In-Progress
                    {
                        res = res.Where(m => m.Status == "IN-PROGRESS" && (m.Management.Sent == false && m.Management.Rejected == false)).ToList();
                    }
                    if (Status == 2) //Completed
                    {
                        res = res.Where(m => m.Status == "COMPLETED").ToList();
                    }
                    if (Status == 3)// On-Hold
                    {
                        res = res.Where(m => m.Status == "ON-HOLD").ToList();
                    }
                    if (Status == 4) //Rejected
                    {
                        res = res.Where(m => (m.Management.Sent == false && m.Management.Approved == false && m.Management.Rejected == false) && m.Status == "REJECTED").ToList();
                    }
                    if (Status == 5) //Verified
                    {
                        res = res.Where(m => m.Status == "VERIFIED").ToList();
                    }
                    if (Status == 6) //Management Pending
                    {
                        res = res.Where(m => m.Management.Sent == true && m.Management.Approved == false && m.Management.Rejected == false).ToList();
                    }
                    if (Status == 7) //Management Rejected
                    {
                        res = res.Where(m => m.Management.Sent == true && m.Management.Approved == false && m.Management.Rejected == true).ToList();
                    }

                    model.SupportRequestcls = res;
                    HttpContext.Session.SetString("supportreport", JsonConvert.SerializeObject(res));
                }


            }

            return View(model);



        }
        public IActionResult DownloadSupportDetailsReport()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            moduledt.Columns.Add("Support Type");
            moduledt.Columns.Add("Category");
            moduledt.Columns.Add("Sub Category");
            moduledt.Columns.Add("Building");
            moduledt.Columns.Add("Room");
            moduledt.Columns.Add("Priority");
            moduledt.Columns.Add("RaisedBy");
            moduledt.Columns.Add("RaisedOn");
            moduledt.Columns.Add("Request Completion Date");
            moduledt.Columns.Add("Ticket Code");
            moduledt.Columns.Add("Status");
            moduledt.Columns.Add("Title");
            moduledt.Columns.Add("Description");
            List<SupportRequestCls> support = new List<SupportRequestCls>();
            if (HttpContext.Session.GetString("supportreport") != null)
            {
                support = JsonConvert.DeserializeObject<List<SupportRequestCls>>(HttpContext.Session.GetString("supportreport"));
            }

            int i = 0;
            foreach (var a in support)
            {
                DataRow dr = moduledt.NewRow();
                dr["Support Type"] = a.SupportTypeName;
                dr["Category"] = a.CategoryName;
                dr["Sub Category"] = a.SubCategoryName;
                dr["Building"] = a.BuildingName;
                dr["Room"] = a.RoomName;
                dr["Priority"] = a.PriorityName;
                dr["RaisedBy"] = a.emp_Name;
                dr["RaisedOn"] = a.InsertedDate.ToString("dd-MMM-yyyy");
                dr["Request Completion Date"] = a.RequestedCompletionDate;
                dr["Status"] = a.Status;
                dr["Ticket Code"] = a.TicketCode;
                dr["Title"] = a.Title;
                dr["Description"] = a.Description;

                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "SupportReport";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"SupportReport.xlsx");
        }
        public IActionResult SupportDynamicReport(string fromdate, string todate)
        {
            CheckLoginStatus();
            ViewBag.Employees = GetAllEmployeeName();
            ViewBag.DeptEmployees = GetallEmployeeByDeptId();
            SupportReportViewModel model = new SupportReportViewModel();

            if (common.checkaccessavailable("Report", accessId, "List", "Support", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }



            if (common.checkaccessavailable("Report", accessId, "Support Type", "Support", roleId) == true)
            {

                List<NameIdCountViewModel> list = new List<NameIdCountViewModel>();

                NameIdCountViewModel o = new NameIdCountViewModel();
                o.Name = "GroupType";
                o.Status = "Group";

                NameIdCountViewModel tw = new NameIdCountViewModel();
                tw.Name = "OrganizationType";
                tw.Status = "Organization";

                NameIdCountViewModel th = new NameIdCountViewModel();
                th.Name = "All";
                th.Status = "All";


                list.Add(th);
                list.Add(o);
                list.Add(tw);

                model.Types = list;
                model.FilterShow = true;

                //model.Organizations = organizationRepository.GetAllOrganization().ToList();
                //model.Departments = departmentRepository.GetAllDepartment().ToList();
                //model.SupportTypes = supportRepository.GetAllSupportType().ToList();

            }
            else
            {
                List<NameIdCountViewModel> list = new List<NameIdCountViewModel>();



                NameIdCountViewModel th = new NameIdCountViewModel();
                th.Name = "NoType";
                th.Status = "NoType";


                list.Add(th);


                model.Types = list;

                var allempdept = common.GetDepartmentsByEmployeeID(userId);
                if (allempdept != null && allempdept.Count() > 0)
                {
                    var supportIds = supportRepository.
                        GetSupportTypeIdsByDepartmentIdIn(allempdept.Distinct().Select(a => a.id).ToList());

                    if (supportIds != null && supportIds.Count() > 0)
                    {

                        model.Departments = departmentRepository.GetDepartmentByIdIn(allempdept.Distinct().Select(a => a.id).ToList()).ToList();

                        model.SupportTypes = supportRepository.GetAllSupportType().Where(a => supportIds.Contains(a.ID)).ToList();

                        if (model.SupportTypes != null && model.SupportTypes.Count() > 1)
                        {
                            model.FilterShow = true;
                        }
                    }
                }

            }



            return View(model);
        }

        [HttpPost]
        public JsonResult PrepareTimelineData(SupportReportChartViewModel model)
        {
            CheckLoginStatus();
            ApexChartOptions chartOptions = new ApexChartOptions();

            List<SupportTimeLine> timelineList = supportRepository.GetTimelineBySupportId(model.RequestID).ToList();
            ApexChartTimeLine[] chartTimelineArray = new ApexChartTimeLine[timelineList.Count()];
            for (int i = 0; i < timelineList.Count(); i++)
            {
                ApexChartTimeLine chartTimeline = new ApexChartTimeLine();

                SupportTimeLine st = timelineList[i];
                int start = i;
                if (st.Status == "PENDING")
                {
                    chartTimeline.fillColor = "#F9CE1D";
                    chartTimeline.x = "PENDING";
                    chartTimeline.y = new Int64[2];

                    DateTime dt1970 = new DateTime(1970, 1, 1);


                    chartTimeline.y[0] = (long)((st.InsertedDate - dt1970).TotalMilliseconds);
                    // TimeSpan span= st.InsertedDate-new DateTime(1970,1,1);
                    if ((start + 1) == timelineList.Count())
                    {

                        chartTimeline.y[1] = (long)((DateTime.Now - dt1970).TotalMilliseconds);
                    }
                    else
                    {
                        SupportTimeLine nextst = timelineList[(start + 1)];
                        chartTimeline.y[1] = (long)((nextst.InsertedDate - dt1970).TotalMilliseconds);

                    }
                }
                else if (st.Status == "IN-PROGRESS")
                {
                    chartTimeline.fillColor = "#008FFB";
                    chartTimeline.x = "IN-PROGRESS";
                    chartTimeline.y = new Int64[2];

                    DateTime dt1970 = new DateTime(1970, 1, 1);


                    chartTimeline.y[0] = (long)((st.InsertedDate - dt1970).TotalMilliseconds);
                    // TimeSpan span= st.InsertedDate-new DateTime(1970,1,1);
                    if ((start + 1) == timelineList.Count())
                    {

                        chartTimeline.y[1] = (long)((DateTime.Now - dt1970).TotalMilliseconds);
                    }
                    else
                    {
                        SupportTimeLine nextst = timelineList[(start + 1)];
                        chartTimeline.y[1] = (long)((nextst.InsertedDate - dt1970).TotalMilliseconds);

                    }
                }
                else if (st.Status == "COMPLETED")
                {
                    chartTimeline.fillColor = "#4CAF50";
                    chartTimeline.x = "COMPLETED";
                    chartTimeline.y = new Int64[2];

                    DateTime dt1970 = new DateTime(1970, 1, 1);


                    chartTimeline.y[0] = (long)((st.InsertedDate - dt1970).TotalMilliseconds);
                    // TimeSpan span= st.InsertedDate-new DateTime(1970,1,1);
                    if ((start + 1) == timelineList.Count())
                    {

                        chartTimeline.y[1] = (long)((DateTime.Now - dt1970).TotalMilliseconds);
                    }
                    else
                    {
                        SupportTimeLine nextst = timelineList[(start + 1)];
                        chartTimeline.y[1] = (long)((nextst.InsertedDate - dt1970).TotalMilliseconds);

                    }
                }
                else if (st.Status == "ON-HOLD")
                {
                    chartTimeline.fillColor = "#546E7A";
                    chartTimeline.x = "ON-HOLD";
                    chartTimeline.y = new Int64[2];

                    DateTime dt1970 = new DateTime(1970, 1, 1);


                    chartTimeline.y[0] = (long)((st.InsertedDate - dt1970).TotalMilliseconds);
                    // TimeSpan span= st.InsertedDate-new DateTime(1970,1,1);
                    if ((start + 1) == timelineList.Count())
                    {

                        chartTimeline.y[1] = (long)((DateTime.Now - dt1970).TotalMilliseconds);
                    }
                    else
                    {
                        SupportTimeLine nextst = timelineList[(start + 1)];
                        chartTimeline.y[1] = (long)((nextst.InsertedDate - dt1970).TotalMilliseconds);

                    }
                }
                else if (st.Status == "REJECTED")
                {
                    chartTimeline.fillColor = "#D7263D";
                    chartTimeline.x = "REJECTED";
                    chartTimeline.y = new Int64[2];

                    DateTime dt1970 = new DateTime(1970, 1, 1);


                    chartTimeline.y[0] = (long)((st.InsertedDate - dt1970).TotalMilliseconds);
                    // TimeSpan span= st.InsertedDate-new DateTime(1970,1,1);
                    if ((start + 1) == timelineList.Count())
                    {

                        chartTimeline.y[1] = (long)((DateTime.Now - dt1970).TotalMilliseconds);
                    }
                    else
                    {
                        SupportTimeLine nextst = timelineList[(start + 1)];
                        chartTimeline.y[1] = (long)((nextst.InsertedDate - dt1970).TotalMilliseconds);

                    }
                }
                else if (st.Status == "VERIFIED")
                {
                    chartTimeline.fillColor = "#F86624";
                    chartTimeline.x = "VERIFIED";
                    chartTimeline.y = new Int64[2];

                    DateTime dt1970 = new DateTime(1970, 1, 1);


                    chartTimeline.y[0] = (long)((st.InsertedDate - dt1970).TotalMilliseconds);
                    // TimeSpan span= st.InsertedDate-new DateTime(1970,1,1);
                    if ((start + 1) == timelineList.Count())
                    {

                        chartTimeline.y[1] = (long)((DateTime.Now - dt1970).TotalMilliseconds);
                    }
                    else
                    {
                        SupportTimeLine nextst = timelineList[(start + 1)];
                        chartTimeline.y[1] = (long)((nextst.InsertedDate - dt1970).TotalMilliseconds);

                    }
                }
                else if (st.Status == "APPROVAL PENDING")
                {
                    chartTimeline.fillColor = "#F9CE1D";
                    chartTimeline.x = "APPROVAL PENDING";
                    chartTimeline.y = new Int64[2];

                    DateTime dt1970 = new DateTime(1970, 1, 1);


                    chartTimeline.y[0] = (long)((st.InsertedDate - dt1970).TotalMilliseconds);
                    // TimeSpan span= st.InsertedDate-new DateTime(1970,1,1);
                    if ((start + 1) == timelineList.Count())
                    {

                        chartTimeline.y[1] = (long)((DateTime.Now - dt1970).TotalMilliseconds);
                    }
                    else
                    {
                        SupportTimeLine nextst = timelineList[(start + 1)];
                        chartTimeline.y[1] = (long)((nextst.InsertedDate - dt1970).TotalMilliseconds);

                    }
                }
                else if (st.Status == "MANAGEMENT REJECTED")
                {
                    chartTimeline.fillColor = "#D7263D";
                    chartTimeline.x = "MANAGEMENT REJECTED";
                    chartTimeline.y = new Int64[2];

                    DateTime dt1970 = new DateTime(1970, 1, 1);


                    chartTimeline.y[0] = (long)((st.InsertedDate - dt1970).TotalMilliseconds);
                    // TimeSpan span= st.InsertedDate-new DateTime(1970,1,1);
                    if ((start + 1) == timelineList.Count())
                    {

                        chartTimeline.y[1] = (long)((DateTime.Now - dt1970).TotalMilliseconds);
                    }
                    else
                    {
                        SupportTimeLine nextst = timelineList[(start + 1)];
                        chartTimeline.y[1] = (long)((nextst.InsertedDate - dt1970).TotalMilliseconds);

                    }
                }

                chartTimelineArray[i] = chartTimeline;

            }

            chartOptions.timeline = chartTimelineArray;

            return Json(chartOptions);
        }

        [HttpPost]
        public JsonResult PrepareReportData(SupportReportChartViewModel model)
        {
            CheckLoginStatus();
            ApexChartOptions chartOptions = new ApexChartOptions();

            List<SupportType> types = new List<SupportType>();
            List<SupportRequest> requests = new List<SupportRequest>();

            if (model.Type == null || model.Type == "NoType")
            {
                if (model.SupportTypeIds == null || model.SupportTypeIds.Count() == 0)
                {
                    var allempdept = common.GetDepartmentsByEmployeeID(userId);
                    var supportIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(allempdept.Distinct().Select(a => a.id).ToList());

                    types.AddRange(supportRepository.GetAllSupportType().Where(a => supportIds.Contains(a.ID)).ToList());
                    requests.AddRange(supportRepository.GetAllBySupportTypeId(types.Select(a => a.ID).ToList()).ToList());
                }
                else
                {
                    requests = supportRepository.
                    GetAllBySupportTypeId(model.SupportTypeIds).ToList();
                }

            }
            else
            if (model.Type == "All")
            {

                types.AddRange(supportRepository.GetAllSupportType().ToList());
                requests.AddRange(supportRepository.GetAllBySupportTypeId(types.Select(a => a.ID).ToList()).ToList());
            }
            else
                  if (model.Type == "GroupType")
            {
                if (model.DeptIds != null && model.DeptIds.Count() > 0)
                {
                    if (model.SupportTypeIds != null && model.SupportTypeIds.Count() > 0)
                    {
                        requests = supportRepository.
                        GetAllBySupportTypeId(model.SupportTypeIds).ToList();
                    }
                    else
                    {

                        var sTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(model.DeptIds).ToList();
                        if (sTypeIds != null && sTypeIds.Count() > 0)
                        {
                            requests = supportRepository.
                      GetAllBySupportTypeId(sTypeIds)
                      .ToList();
                        }



                    }

                }
                else
                {

                    var deptIds = departmentRepository.GetDepartmentListByGroupIdIn(model.GrpIds);


                    if (deptIds != null && deptIds.Count() > 0)
                    {
                        var sTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds.Select(a => a.ID).ToList()).ToList();

                        if (sTypeIds != null && sTypeIds.Count() > 0)
                        {
                            requests = supportRepository.
                  GetAllBySupportTypeId(sTypeIds)
                  .ToList();

                        }
                    }


                }
            }
            else
                  if (model.Type == "OrganizationType")
            {

                if (model.OrgIds != null && model.OrgIds.Count() > 0)
                {


                    if (model.DeptIds != null && model.DeptIds.Count() > 0)
                    {
                        if (model.SupportTypeIds != null && model.SupportTypeIds.Count() > 0)
                        {
                            requests = supportRepository.
                            GetAllBySupportTypeId(model.SupportTypeIds).ToList();
                        }
                        else
                        {
                            requests = supportRepository.
                            GetAllBySupportTypeId(supportRepository.GetAllSupportTypeById(
                             supportRepository.GetSupportTypeIdsByDepartmentIdIn(model.DeptIds).ToList()).Select(a => a.ID).ToList())
                            .ToList();


                        }

                    }
                    else
                    {

                        var deptIds = departmentRepository.GetDepartmentListByOrganizationIdIn(model.OrgIds);

                        requests = supportRepository.
                            GetAllBySupportTypeId(supportRepository.GetAllSupportTypeById(
                             supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds.Select(a => a.ID).Distinct().ToList())
                             .ToList()).Select(a => a.ID).ToList())
                            .ToList();

                    }

                }
                else
                {

                    var orgs = organizationRepository.GetAllOrganizationByGroupId(model.GrpIds[0]);


                    if (orgs != null && orgs.Count() > 0)
                    {


                        var deptIds = departmentRepository.GetDepartmentListByOrganizationIdIn(orgs.Select(a => a.ID).ToList());


                        if (deptIds != null && deptIds.Count() > 0)
                        {
                            var sTypeIds = supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds.Select(a => a.ID).ToList()).ToList();

                            if (sTypeIds != null && sTypeIds.Count() > 0)
                            {
                                requests = supportRepository.
                      GetAllBySupportTypeId(sTypeIds)
                      .ToList();

                            }
                        }
                    }
                    //requests = supportRepository.
                    //    GetAllBySupportTypeId(supportRepository.GetAllSupportTypeById(
                    //     supportRepository.GetSupportTypeIdsByDepartmentIdIn(deptIds.Select(a => a.ID).Distinct().ToList())
                    //     .ToList()).Select(a => a.ID).ToList())
                    //    .ToList();
                }

            }



            if (requests != null)
            {
                var requestAssign = _dbContext.SupportRequestAssign.Where(a => a.Active == true).ToList();
                var requestas = (from a in requests
                                 select new SupportRequestNew
                                 {
                                     StatusID = a.StatusID,
                                     ManagementReject = a.ManagementReject,
                                     SendApproval = a.SendApproval,
                                     SupportRequestId = a.ID,
                                     EmployeeId = requestAssign != null ? requestAssign.Where(s => s.SupportRequestId == a.ID).FirstOrDefault() != null ? requestAssign.Where(s => s.SupportRequestId == a.ID).FirstOrDefault().EmployeeId : 0 : 0
                                 }).ToList();
               
                if(model.DeptEmp != null)
                {
                    requestas = requestas.Where(a => a.EmployeeId == model.DeptEmp).ToList();
                }
                if (model.DEmp != null)
                {
                    requestas = requestas.Where(a => a.EmployeeId == model.DEmp).ToList();
                }

                SupportReportCountViewModel vm = new SupportReportCountViewModel();
                vm.PendingCount = requestas.Where(a => a.StatusID == 1 && a.ManagementReject == false)
                    .ToList().
                    Count();

                int ppc = requestas.Where(a => a.StatusID == 1 && a.SendApproval == true &&
                  a.ManagementApproval == false && a.ManagementReject == false)
                    .ToList().
                    Count();

                vm.ProgressCount = requestas.Where(a => a.StatusID == 3 && a.ManagementReject == false
                ).ToList().Count();

                int pprc = requestas.Where(a => a.StatusID == 3 && a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false)
                .ToList().
                Count();

                vm.CompletedCount = requestas.Where(a => a.StatusID == 2 && a.ManagementReject == false
                ).ToList().Count();

                int cc = requestas.Where(a => a.StatusID == 2 && a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false)
               .ToList().
               Count();

                vm.OnHoldCount = requestas.Where(a => a.StatusID == 4 && a.ManagementReject == false
               ).ToList().Count();

                int ohc = requestas.Where(a => a.StatusID == 4 && a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false)
            .ToList().
            Count();

                vm.RejectedCount = requestas.Where(a => a.StatusID == 5 && a.ManagementReject == false
               ).ToList().Count();

                int rjc = requestas.Where(a => a.StatusID == 5 && a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false)
    .ToList().
    Count();

                vm.VerifiedCount = requestas.Where(a => a.StatusID == 6 && a.ManagementReject == false
               ).ToList().Count();

                int vrc = requestas.Where(a => a.StatusID == 6 && a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false)
.ToList().
Count();

                vm.VerifiedCount = vm.VerifiedCount > vrc ? vm.VerifiedCount - vrc : vrc - vm.VerifiedCount;
                vm.RejectedCount = vm.RejectedCount > rjc ? vm.RejectedCount - rjc : rjc - vm.RejectedCount;
                vm.OnHoldCount = vm.OnHoldCount > ohc ? vm.OnHoldCount - ohc : ohc - vm.OnHoldCount;
                vm.CompletedCount = vm.CompletedCount > cc ? vm.CompletedCount - cc : cc - vm.CompletedCount;
                vm.ProgressCount = vm.ProgressCount > pprc ? vm.ProgressCount - pprc : pprc - vm.ProgressCount;
                vm.PendingCount = vm.PendingCount > ppc ? vm.PendingCount - ppc : ppc - vm.PendingCount;


                vm.MangementApprovedCount = requestas.Where(a => a.ManagementApproval == true).ToList().Count();
                vm.MangementRejectedCount = requestas.Where(a => a.ManagementReject == true).ToList().Count();
                vm.MangementPendingCount = requestas.Where(a => a.SendApproval == true &&
                 a.ManagementApproval == false && a.ManagementReject == false).ToList().Count();

                vm.TotalSupportCount = vm.PendingCount + vm.ProgressCount + vm.CompletedCount + vm.OnHoldCount +
                    vm.RejectedCount + vm.VerifiedCount + vm.MangementApprovedCount + vm.MangementRejectedCount
                    + vm.TotalMangementCount;

                chartOptions.series = new Int64[8];

                chartOptions.series[0] = vm.PendingCount;
                chartOptions.series[1] = vm.ProgressCount;
                chartOptions.series[2] = vm.CompletedCount;
                chartOptions.series[3] = vm.OnHoldCount;
                chartOptions.series[4] = vm.RejectedCount;
                chartOptions.series[5] = vm.VerifiedCount;
                chartOptions.series[6] = vm.MangementPendingCount;
                chartOptions.series[7] = vm.MangementRejectedCount;

                chartOptions.labels = new string[8];


                chartOptions.labels[0] = "PENDING";
                chartOptions.labels[1] = "PROGRESS";
                chartOptions.labels[2] = "COMPLETED";
                chartOptions.labels[3] = "ON-HOLD";
                chartOptions.labels[4] = "REJECTED";
                chartOptions.labels[5] = "VERIFIED";
                chartOptions.labels[6] = "APPROVAL PENDING";
                chartOptions.labels[7] = "MANAGEMENT REJECTED";
                chartOptions.max = requests.Count();


                return Json(chartOptions);
            }
            else
            {
                return null;
            }
        }


        private ApexChartOptions GetMonthlyStatusWiseCount(ApexChartOptions chartOptions, List<SupportRequest> requests)
        {

            return chartOptions;
        }

        public JsonResult GetAllOrganization()
        {



            return Json(organizationRepository.GetAllOrganization().ToList());
        }

        public JsonResult GetAllDepartmentById(SupportReportModel model)
        {
            if (model.Type == "GroupType")
            {
                var depts = (from a in departmentRepository.GetDepartmentListByGroupId(model.GrpIds[0]).ToList()
                             join b in supportRepository.GetAllDepartmentSupport()
                             on a.ID equals b.DepartmentID
                             select new
                             {
                                 ID = a.ID,
                                 Name = a.ParentID != null && a.ParentID > 0 ?
                               departmentRepository.GetDepartmentById(a.ParentID.Value).Name + ViewConstants.ARROW + a.Name : a.Name,
                             }).ToList();
                return Json(depts);
            }
            else
            if (model.Type == "OrganizationType")
            {

                var depts = (from a in departmentRepository.GetDepartmentListByOrganizationIdIn(model.OrgIds).ToList()
                             join b in supportRepository.GetAllDepartmentSupport()
                             on a.ID equals b.DepartmentID
                             select new
                             {
                                 ID = a.ID,
                                 Name = a.ParentID != null && a.ParentID > 0 ?
                                 departmentRepository.GetDepartmentById(a.ParentID.Value).Name + ViewConstants.ARROW : a.Name,
                             }).ToList();
                return Json(depts);

            }
            else
                return null;
        }
        [HttpPost]
        public JsonResult GetAllSupportTypeByDepartmentId(SupportReportModel model)
        {


            var depts = supportRepository.GetAllSupportTypeById(
                supportRepository.GetSupportTypeIdsByDepartmentIdIn(model.DeptIds).ToList()).ToList();
            return Json(depts);

        }
        #endregion


    }
}