﻿using OdmErp.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Support.Models
{
    public class SupportMethod
    {
        private IEmployeeRepository employeeRepository;
        private ISupportRepository supportRepository;
        private OdmErp.Web.Models.CommonMethods common;
        private IDepartmentRepository departmentRepository;
        private ISubModuleRepository subModuleRepository;
        private IActionAccessRepository actionAccessRepository;
        private IDesignationRepository designationRepository;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IAccessRepository accessRepository;
        public SupportMethod(IEmployeeRepository employeeRepo, ISupportRepository suppoRepository, OdmErp.Web.Models.CommonMethods commonm, IDepartmentRepository departmentRepo,
            ISubModuleRepository subModuleRepo, IActionAccessRepository actionAccessRepo, IDesignationRepository designationRepo, IEmployeeDesignationRepository employeeDesignationRepo,
            IAccessRepository accessRepo)
        {
            employeeRepository = employeeRepo;
            supportRepository = suppoRepository;
            common = commonm;
            departmentRepository = departmentRepo;
            subModuleRepository = subModuleRepo;
            actionAccessRepository = actionAccessRepo;
            designationRepository = designationRepo;
            employeeDesignationRepository = employeeDesignationRepo;
            accessRepository = accessRepo;
        }
        public List<SupportRequestCls> GetallSupportFormail()
        {
            try
            {
                var employees = employeeRepository.GetAllEmployeewithleft();
                var req = supportRepository.GetAllSupportRequest();
                var priority = supportRepository.GetAllPriority();
                var departments = common.GetDepartments();
                var res = (from a in req
                           join e in employees on a.InsertedId equals e.ID
                           join c in priority on a.PriorityID equals c.ID
                           select new SupportRequestCls
                           {
                               ID = a.ID,
                               Status = a.Status,
                               //InsertedDate = a.InsertedDate.Value.ToString("dd-MMM-yyyy"),
                               Description = a.Description,
                               DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                               //assignedto = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).Select(m => m.FirstName + " " + m.LastName).FirstOrDefault() : "",
                               //assignedby = e.FirstName + " " + e.LastName,
                               profile = a.InsertedId != 0 ? employees.Where(m => m.ID == a.InsertedId).FirstOrDefault().EmailId : "",
                               PriorityName = c.Name,
                               department = a.DepartmentID != 0 ? departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name : "",
                               assigntime = a.InsertedDate.ToString("hh:mm tt"),
                               InsertedID = a.InsertedId,
                               // AssignDate = a.AssignedDate,
                               //AssignToID = a.AssignedId,
                               // DueDt = a.DueDate,
                               ModifiedDate = a.ModifiedDate,
                               //  parentID = a.ParentId,
                               DepartmentID = a.DepartmentID,
                               PriorityID = a.PriorityID
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
      
        public List<supportattachmentclass> GetSupportAttachments(long id, long empid)
        {
            try
            {
                var attachments = supportRepository.GetAllAttachment();
                //var employees = GetEmployeesForTasks(empid);
                var employees = employeeRepository.GetAllEmployee().ToList();
                var res = (from a in attachments
                           join b in employees on a.InsertedId equals b.ID
                           where a.SupportRequestID == id
                           select new supportattachmentclass
                           {
                               AttachmentName = a.Name,
                               Attachmenturl = a.Path,
                               employeeid = a.InsertedId,
                               employeeName = empid == a.InsertedId ? "Me" : b.FirstName + " " + b.LastName,
                               SupportRequestID = a.SupportRequestID,
                               id = a.ID,
                               insertedOn = a.InsertedDate,
                               modifiedOn = a.ModifiedDate
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public List<supportcomments> GetSupportComments(long id, long empid)
        {
            try
            {
                var comments = supportRepository.GetAllSupportComment();
                //var employees = GetEmployeesForTasks(empid);
                var employees = employeeRepository.GetAllEmployee().ToList();
                var res = (from a in comments
                           join b in employees on a.InsertedId equals b.ID
                           where a.SupportRequestID == id
                           select new supportcomments
                           {
                               id = a.ID,
                               Name = a.Name,
                               EmployeeName = empid == a.InsertedId ? "Me" : b.FirstName + " " + b.LastName,
                               EmployeeID = a.InsertedId,
                               SupportRequestID = a.SupportRequestID,
                               postedON = a.InsertedDate.ToString("dd-MMM-yyyy hh:mm tt"),
                               profile = b.Image,
                               insertedOn = a.InsertedDate,
                               modifiedOn = a.ModifiedDate
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<SupportTabularReportCls> GetAllReportDetails(long? datetype, DateTime? f_date, DateTime? t_date, string m_date, long? supporttype_ddl, long accessId, long roleId, long userId)

        {
            try
            {
                var request = supportRepository.GetAllSupportRequest();

                var supporttype = supportRepository.GetAllSupportType();
                var allempdept = common.GetDepartmentsByEmployeeID(userId);
                List<Web.Models.CommonMasterModel> mast = new List<Web.Models.CommonMasterModel>();
                foreach (var a in allempdept)
                {
                    var departments = common.GetallchilddepartmentbasedonParent(a.id);
                    mast.AddRange(departments);
                }
                var dept = mast.Distinct();
                if (common.checkaccessavailable("Report", accessId, "Support Type", "Support", roleId) == true)
                {
                    var alldept = departmentRepository.GetAllDepartment();
                    dept = (from a in alldept
                            select new Web.Models.CommonMasterModel
                            {
                                ID = a.ID,
                                Name = a.Name
                            }).ToList();
                }

                if (supporttype_ddl != null && supporttype_ddl != 0)
                {
                    request = request.Where(a => a.SupportTypeID == supporttype_ddl.Value).ToList();
                }
                if (f_date != null && t_date != null)
                {
                    request = request.Where(a => a.InsertedDate.Date >= f_date.Value.Date && a.InsertedDate.Date <= t_date.Value.Date).ToList();
                }
                if (m_date != null && m_date != "")
                {
                    DateTime m_dt = Convert.ToDateTime(m_date);
                    request = request.Where(a => a.InsertedDate.Month == m_dt.Month && a.InsertedDate.Year == m_dt.Year).ToList();
                }
                var res = (from a in request
                           join b in supporttype on a.SupportTypeID equals b.ID
                           join c in dept on b.DepartmentID equals c.ID
                           select new supportReportDashboard
                           {
                               ID = a.ID,
                               DepartmentID = c.ID,
                               DepartmentName = c.Name,
                               StatusID = a.StatusID,
                               StatusName = a.StatusName,
                               SupportTypeID = a.SupportTypeID,
                               SendApproval = a.SendApproval
                           }).ToList();

                var result = (from a in res
                              group a by a.DepartmentID into g
                              select new SupportTabularReportCls
                              {
                                  DepartmentID = g.FirstOrDefault().DepartmentID,
                                  DepartmentName = g.FirstOrDefault().DepartmentName,
                                  Raise = g.Count(),
                                  Resolve = g.Where(x => x.StatusName == "COMPLETED").ToList().Count(),
                                  Pending = g.Where(x => x.StatusName == "PENDING").ToList().Count(),
                                  Reject = g.Where(x => x.StatusName == "REJECTED").ToList().Count(),
                                  Verify = g.Where(x => x.StatusName == "VERIFIED").ToList().Count(),
                                  Onhold = g.Where(x => x.StatusName == "ON-HOLD").ToList().Count(),
                                  Inprogress = g.Where(x => x.StatusName == "IN-PROGRESS").ToList().Count(),
                                  Approve = g.Where(x => x.StatusName == "APPROVED").ToList().Count(),
                                  SendApproval = g.Where(x => x.SendApproval == true).ToList().Count(),
                                  ApprovalPending = g.Where(x => x.StatusName == "APPROVAL PENDING").ToList().Count(),
                                  MgtReject = g.Where(x => x.StatusName == "MANAGEMENT REJECTED").ToList().Count(),
                                  RaiseP = (int)Math.Round((double)(100 * (g.Count())) / g.Count()),
                                  SendApprovalP = (int)Math.Round((double)(100 * (g.Where(x => x.SendApproval == true).ToList().Count())) / (g.Count())),
                                  ResolveP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "COMPLETED").ToList().Count())) / (g.Count())),
                                  RejectP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "REJECTED").ToList().Count())) / (g.Count())),
                                  VerifyP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "VERIFIED").ToList().Count())) / (g.Count())),
                                  OnholdP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "ON-HOLD").ToList().Count())) / (g.Count())),
                                  PendingP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "PENDING").ToList().Count())) / (g.Count())),
                                  InprogressP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "IN-PROGRESS").ToList().Count())) / (g.Count())),
                                  ApproveP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "APPROVED").ToList().Count())) / (g.Count())),
                                  ApprovalPendingP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "APPROVAL PENDING").ToList().Count())) / (g.Count())),
                                  MgtRejectP = (int)Math.Round((double)(100 * (g.Where(x => x.StatusName == "MANAGEMENT REJECTED").ToList().Count())) / (g.Count()))
                              }).ToList();



                return result;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        #region Support
        public List<Web.Models.Employeeddlview> GetSupportManagementApprovalemployee()
        {
            try
            {
                var employees = employeeRepository.GetAllEmployee();
                var submoduleid = subModuleRepository.GetSubModuleByName("Management Approval").ID;
                var departmentaccess = actionAccessRepository.GetAllActionDepartment().Where(a => a.SubModuleID == submoduleid).ToList();
                var designations = designationRepository.GetAllDesignation();
                var employeedepartments = employeeDesignationRepository.GetAllEmployeeDesignations();
                List<Web.Models.Employeeddlview> res = (from a in employees
                                             join b in employeedepartments on a.ID equals b.EmployeeID
                                             join c in designations on b.DesignationID equals c.ID
                                             join d in departmentaccess on c.DepartmentID equals d.DepartmentID
                                             select new Web.Models.Employeeddlview
                                             {
                                                 Code = a.EmailId,
                                                 Id = a.ID,
                                                 Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                                             }).ToList().Distinct().ToList();
                var actionaccess = actionAccessRepository.GetAllActionAccess();
                var access = accessRepository.GetAllAccess();
                List<Web.Models.Employeeddlview> resl = (from a in employees
                                              join b in access on a.ID equals b.EmployeeID
                                              join c in actionaccess on b.ID equals c.AccessID
                                              where c.SubModuleID == submoduleid
                                              select new Web.Models.Employeeddlview
                                              {
                                                  Code = a.EmailId,
                                                  Id = a.ID,
                                                  Name = a.FirstName + " " + (a.MiddleName == null ? a.LastName : (a.MiddleName + " " + a.LastName))
                                              }).ToList().Distinct().ToList();
                List<Web.Models.Employeeddlview> result = res.Union(resl).ToList().Distinct().ToList();
                return result;
            }
            catch (Exception e1)
            {

                return null;
            }
        }
        #endregion
    }
}
