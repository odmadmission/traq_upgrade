﻿using Microsoft.AspNetCore.Hosting;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Support.Models
{
   
    public class SupportSmsnMails
    {
        private smssend smssend { get; set; }
        private readonly IHostingEnvironment hostingEnvironment;
        public SupportSmsnMails(smssend sms, IHostingEnvironment hostingEnv)
        {
            smssend = sms;
            hostingEnvironment = hostingEnv;
        }
        public string raisesupport(string supportname, string description, string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "raisesupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{supportname}}", supportname).Replace("{{description}}", description).Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string deadlinesupport(string supportname, string description, string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "supportdeadline.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{supportname}}", supportname).Replace("{{description}}", description).Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string deadlinemissedsupport(string supportname, string description, string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "supportdeadlinemissed.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{supportname}}", supportname).Replace("{{description}}", description).Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string managementApprovalsupport(string supportname, string description, string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "ManagementApprovaltemplate.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{supportname}}", supportname).Replace("{{description}}", description).Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string deadlinedatesupport(string supportname, string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "deadlinedatesupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{supportname}}", supportname).Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public string assigneddeadlinedate(string duedate, string code, string complain, string raisedby, string employeename, string subject, string sender)
        {
            //string body = string.Empty;
            //string filepath = hostingEnvironment.WebRootPath
            //                + Path.DirectorySeparatorChar.ToString()
            //                + "template"
            //                + Path.DirectorySeparatorChar.ToString()
            //                + "assigneddeadlinedate.html";
            //using (StreamReader reader = new StreamReader(filepath))
            //{
            //    body = reader.ReadToEnd().Replace("{{duedate}}", duedate).Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type);
            //    SendHtmlFormattedEmail(sender, subject, body);
            //}

            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "assigneddeadlinedate.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{duedate}}", duedate).Replace("{{code}}", code).Replace("{{complain}}", complain).Replace("{{raisedby}}", raisedby).Replace("{{employeename}}", employeename);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public string resolvesupport(string code, string complain, string raisedby, string employeename, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "resolvesupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{code}}", code).Replace("{{complain}}", complain).Replace("{{raisedby}}", raisedby).Replace("{{employeename}}", employeename);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public string inprogresssupport(string code, string complain, string raisedby, string employeename, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "inprogresssupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{code}}", code).Replace("{{complain}}", complain).Replace("{{raisedby}}", raisedby).Replace("{{employeename}}", employeename);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public string Verifysupport(string code, string complain, string raisedby, string employeename, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "verifySupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{code}}", code).Replace("{{complain}}", complain).Replace("{{raisedby}}", raisedby).Replace("{{employeename}}", employeename);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string askforverifySupport(string supportname, string description, string name, string email, string phone, string code, string category, string type, string subject, string whom, string sender, string employeename)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "askforverifySupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{supportname}}", supportname).Replace("{{description}}", description).Replace("{{name}}", name).Replace("{{code}}", code).Replace("{{whom}}", whom).Replace("{{category}}", category).Replace("{{email}}", email).Replace("{{phone}}", phone).Replace("{{type}}", type).Replace("{{employeename}}", employeename);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public string rejectsupport(string reason, string code, string complain, string raisedby, string employeename, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "rejectsupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{reason}}", reason).Replace("{{code}}", code).Replace("{{complain}}", complain).Replace("{{raisedby}}", raisedby).Replace("{{employeename}}", employeename);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        public string onholdsupport(string reason, string code, string complain, string raisedby, string employeename, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "onholdsupport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{reason}}", reason).Replace("{{code}}", code).Replace("{{complain}}", complain).Replace("{{raisedby}}", raisedby).Replace("{{employeename}}", employeename);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }


    }
}
