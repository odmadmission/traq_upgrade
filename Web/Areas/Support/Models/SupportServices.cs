﻿using Microsoft.Extensions.Hosting;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Support.Models
{
   public class SupportServices
    {
        private CommonMethods common;
        private SupportSmsnMails smssend;
        private ISupportRepository supportRepository;
        private IOrganizationRepository organizationRepository;
        private IEmployeeRepository employeeRepository;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IDesignationRepository designationRepository;
        private IDepartmentRepository departmentRepository;
        private ICityRepository cityRepository;
        private IDepartmentLeadRepository departmentLeadRepository;
        public SupportServices(CommonMethods common, ICityRepository cr, IDepartmentLeadRepository dlr, IDepartmentRepository deptrep,
            IDesignationRepository degnrep, IEmployeeDesignationRepository edr, IEmployeeRepository er,
            ISupportRepository mr, IOrganizationRepository or, SupportSmsnMails sms)
        {
            smssend = sms;
            supportRepository = mr;
            organizationRepository = or;
            employeeRepository = er;
            employeeDesignationRepository = edr;
            designationRepository = degnrep;
            departmentRepository = deptrep;
            cityRepository = cr;
            departmentLeadRepository = dlr;
        }
        #region Support
        public void supportdeadlinesbefore24hr()
        {
            try
            {
                // alert before 24 hr
                var req = supportRepository.GetAllSupportRequest();
                var ded = req.Where(a => a.DueDate != null).ToList();
                var res = ded.Where(a => (a.StatusName == "PENDING" || a.StatusName == "IN-PROGRESS") && a.DueDate.Value.Date.Subtract(DateTime.Now.Date).Days == 1).ToList();
                foreach (var request in res)
                {
                    var Department_Id = supportRepository.GetBySupportTypeId(request.SupportTypeID).DepartmentID;



                    List<long> DesignationIdes = designationRepository.GetDesignationByDepartmentId(Department_Id).ToList();

                    //Employee Head Details
                    List<DepartmentLead> employeeLeadid = departmentLeadRepository.GetAllEmployeeIdByDepartment(Department_Id).ToList();

                    List<long> employeeLeadid_Emplyee_Ides = employeeLeadid.Select(e => e.EmployeeID).ToList();
                    // Department Employee Details
                    List<EmployeeDesignation> EmployeeIdes = employeeDesignationRepository.GetAllEmployeeDesignationsByDesignationId(DesignationIdes).ToList();
                    List<long> Department_Emplyee_Ides = EmployeeIdes.Select(e => e.EmployeeID).ToList();

                    foreach (var item in employeeLeadid_Emplyee_Ides) // remove department head employee id from employee list
                    {
                        Department_Emplyee_Ides.Remove(item);
                    }

                    //Fire Email to Email Head


                    foreach (var items in employeeLeadid_Emplyee_Ides)
                    {

                        long emp_id = items;
                        Employee employees = employeeRepository.GetEmployeeById(emp_id);
                        string name = employees.FirstName + " " + employees.LastName;
                        string code = employees.EmpCode;
                        string email = employees.EmailId;
                        string phone = employees.PrimaryMobile;
                        string category = "Employee";
                        string whom = "Employee";
                        string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                        var description = supportRepository.GetBySupportRequestId(request.ID);

                        smssend.deadlinesupport(description.Title, description.Description, name, email, phone, code, category, typename, "New Support Request For Head-" + description.Title + "-" + name, whom, employees.EmailId);

                    }


                    //Fire Email to Department Employees




                    foreach (var item in Department_Emplyee_Ides)
                    {

                        long emp_id = item;
                        Employee employees = employeeRepository.GetEmployeeById(emp_id);
                        string name = employees.FirstName + " " + employees.LastName;
                        string code = employees.EmpCode;
                        string email = employees.EmailId;
                        string phone = employees.PrimaryMobile;
                        string category = "Employee";
                        string whom = "Employee";
                        string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                        var description = supportRepository.GetBySupportRequestId(request.ID);

                        smssend.deadlinesupport(description.Title, description.Description, name, email, phone, code, category, typename, "New Support Request-" + description.Title + "-" + name, whom, employees.EmailId);

                    }
                }

                //Deadline missed
                var ded1 = req.Where(a => a.DueDate != null).ToList();
                var res1 = ded.Where(a => (a.StatusName == "PENDING" || a.StatusName == "IN-PROGRESS") && a.DueDate.Value.Date.Subtract(DateTime.Now.Date).Days < 0).ToList();
                foreach (var request in res1)
                {
                    var Department_Id = supportRepository.GetBySupportTypeId(request.SupportTypeID).DepartmentID;



                    List<long> DesignationIdes = designationRepository.GetDesignationByDepartmentId(Department_Id).ToList();

                    //Employee Head Details
                    List<DepartmentLead> employeeLeadid = departmentLeadRepository.GetAllEmployeeIdByDepartment(Department_Id).ToList();

                    List<long> employeeLeadid_Emplyee_Ides = employeeLeadid.Select(e => e.EmployeeID).ToList();
                    // Department Employee Details
                    List<EmployeeDesignation> EmployeeIdes = employeeDesignationRepository.GetAllEmployeeDesignationsByDesignationId(DesignationIdes).ToList();
                    List<long> Department_Emplyee_Ides = EmployeeIdes.Select(e => e.EmployeeID).ToList();

                    foreach (var item in employeeLeadid_Emplyee_Ides) // remove department head employee id from employee list
                    {
                        Department_Emplyee_Ides.Remove(item);
                    }

                    //Fire Email to Email Head


                    foreach (var items in employeeLeadid_Emplyee_Ides)
                    {

                        long emp_id = items;
                        Employee employees = employeeRepository.GetEmployeeById(emp_id);
                        string name = employees.FirstName + " " + employees.LastName;
                        string code = employees.EmpCode;
                        string email = employees.EmailId;
                        string phone = employees.PrimaryMobile;
                        string category = "Employee";
                        string whom = "Employee";
                        string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                        var description = supportRepository.GetBySupportRequestId(request.ID);

                        smssend.deadlinemissedsupport(description.Title, description.Description, name, email, phone, code, category, typename, "New Support Request For Head-" + description.Title + "-" + name, whom, employees.EmailId);

                    }


                    //Fire Email to Department Employees




                    foreach (var item in Department_Emplyee_Ides)
                    {

                        long emp_id = item;
                        Employee employees = employeeRepository.GetEmployeeById(emp_id);
                        string name = employees.FirstName + " " + employees.LastName;
                        string code = employees.EmpCode;
                        string email = employees.EmailId;
                        string phone = employees.PrimaryMobile;
                        string category = "Employee";
                        string whom = "Employee";
                        string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                        var description = supportRepository.GetBySupportRequestId(request.ID);

                        smssend.deadlinemissedsupport(description.Title, description.Description, name, email, phone, code, category, typename, "New Support Request-" + description.Title + "-" + name, whom, employees.EmailId);

                    }
                }
                //Deadline missed

                res1 = ded.Where(a => a.StatusName == "COMPLETED").ToList();
                foreach (var request in res1)
                {
                    var employees = employeeRepository.GetEmployeeById(request.InsertedId);

                    string name = employees.FirstName + " " + employees.LastName;

                    var empname = employeeRepository.GetEmployeeById(request.ModifiedId);
                    string employeename = empname.FirstName + " " + empname.LastName;
                    string empcode = employees.EmpCode;
                    string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;

                    smssend.askforverifySupport(request.Title, request.Description, name, employees.EmailId, employees.PrimaryMobile, request.TicketCode, "Employee", typename, "Support Verify Request-" + request.Title + "-" + employeename, "Employee", employees.EmailId, employeename);

                }
            }
            catch
            {

            }
        }
        public void supportdeadlinesbefore4hr()
        {
            try
            {
                var req = supportRepository.GetAllSupportRequest().Where(a => a.DueDate != null).ToList();
                var res = req.Where(a => (a.StatusName == "PENDING" || a.StatusName == "IN-PROGRESS") && a.DueDate.Value.Date == DateTime.Now.Date).ToList();
                foreach (var request in res)
                {
                    var Department_Id = supportRepository.GetBySupportTypeId(request.SupportTypeID).DepartmentID;



                    List<long> DesignationIdes = designationRepository.GetDesignationByDepartmentId(Department_Id).ToList();

                    //Employee Head Details
                    List<DepartmentLead> employeeLeadid = departmentLeadRepository.GetAllEmployeeIdByDepartment(Department_Id).ToList();

                    List<long> employeeLeadid_Emplyee_Ides = employeeLeadid.Select(e => e.EmployeeID).ToList();
                    // Department Employee Details
                    List<EmployeeDesignation> EmployeeIdes = employeeDesignationRepository.GetAllEmployeeDesignationsByDesignationId(DesignationIdes).ToList();
                    List<long> Department_Emplyee_Ides = EmployeeIdes.Select(e => e.EmployeeID).ToList();

                    foreach (var item in employeeLeadid_Emplyee_Ides) // remove department head employee id from employee list
                    {
                        Department_Emplyee_Ides.Remove(item);
                    }

                    //Fire Email to Email Head


                    foreach (var items in employeeLeadid_Emplyee_Ides)
                    {

                        long emp_id = items;
                        Employee employees = employeeRepository.GetEmployeeById(emp_id);
                        string name = employees.FirstName + " " + employees.LastName;
                        string code = employees.EmpCode;
                        string email = employees.EmailId;
                        string phone = employees.PrimaryMobile;
                        string category = "Employee";
                        string whom = "Employee";
                        string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                        var description = supportRepository.GetBySupportRequestId(request.ID);

                        smssend.deadlinesupport(description.Title, description.Description, name, email, phone, code, category, typename, "New Support Request For Head-" + description.Title + "-" + name, whom, employees.EmailId);

                    }


                    //Fire Email to Department Employees




                    foreach (var item in Department_Emplyee_Ides)
                    {

                        long emp_id = item;
                        Employee employees = employeeRepository.GetEmployeeById(emp_id);
                        string name = employees.FirstName + " " + employees.LastName;
                        string code = employees.EmpCode;
                        string email = employees.EmailId;
                        string phone = employees.PrimaryMobile;
                        string category = "Employee";
                        string whom = "Employee";
                        string typename = supportRepository.GetBySupportTypeId(request.SupportTypeID).Name;
                        var description = supportRepository.GetBySupportRequestId(request.ID);

                        smssend.deadlinesupport(description.Title, description.Description, name, email, phone, code, category, typename, "New Support Request-" + description.Title + "-" + name, whom, employees.EmailId);

                    }
                }
            }
            catch
            {

            }
        }
        #endregion
    }
}

