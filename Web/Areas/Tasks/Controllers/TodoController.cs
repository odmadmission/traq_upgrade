﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Entities.GrievanceAggregate;
using OdmErp.ApplicationCore.Entities.TodoAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Web.DTO;
using OdmErp.Web.ViewModels;
using OfficeOpenXml;
using Web.Controllers;
using OdmErp.Web.Areas.Tasks.Models;
using OdmErp.Web.Models;
using Newtonsoft.Json;

namespace OdmErp.Web.Areas.Tasks.Controllers
{
    [Area("Tasks")]
    public class TodoController : AppController
    {
        private IToDoRepository todoRepository;
        private TodoModelClass commonTaskMethods;
        private IEmployeeRepository employeeRepository;
        private IDepartmentRepository departmentRepository;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly IEmployeeDesignationRepository employeeDesignationRepository;
        private readonly IDesignationRepository designationRepository;
        private IGroupRepository groupRepository;
        private IOrganizationRepository organizationRepository;
        private TodoMethod commonMethods;
        private IHttpContextAccessor _httpContextAccessor;
        public TodoSmsnMails sms;
        public OdmErp.Web.Models.CommonMethods Common;
        private readonly IFileProvider fileProvider;
        public TodoController(IToDoRepository toDoRepositor, IFileProvider filepro,
            TodoModelClass commonTask, IEmployeeRepository employeeRepositor,
            IDepartmentRepository deptRepositor, IHostingEnvironment env, IOrganizationRepository organizationRepo,
            IEmployeeDesignationRepository employeeDesignationRepositor, IGroupRepository groupRepo, OdmErp.Web.Models.CommonMethods Commonmeth,
            IDesignationRepository designationRepositor, TodoMethod common, TodoSmsnMails smss, IHttpContextAccessor httpContextAccessor)
        {
            Common = Commonmeth;
            hostingEnvironment = env;
            todoRepository = toDoRepositor;
            commonTaskMethods = commonTask;
            employeeRepository = employeeRepositor;
            departmentRepository = deptRepositor;
            employeeDesignationRepository = employeeDesignationRepositor;
            designationRepository = designationRepositor;
            commonMethods = common;
            fileProvider = filepro;
            sms = smss;
            _httpContextAccessor = httpContextAccessor;
            groupRepository = groupRepo;
            organizationRepository = organizationRepo;
        }


        public IActionResult Index(string fromdate, string todate)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("Dashboard", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            var tasks = commonMethods.GetallTasks(userId);
            ViewBag.Msg = "";
            if (fromdate != null && todate != null)
            {
                DateTime F_dt = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                DateTime T_dt = Convert.ToDateTime(todate + " 23:59:59 PM");
                if (F_dt > T_dt)
                {
                    ViewBag.Msg = "From Date must be less than To Date";
                }
                else
                {
                    tasks = tasks.Where(a => a.AssignDate.Value >= F_dt && a.AssignDate.Value <= T_dt).ToList();
                }
                ViewBag.fromdate = F_dt.ToString("yyyy-MM-dd");
                ViewBag.todate = T_dt.ToString("yyyy-MM-dd");
            }
            else
            {
                var month = DateTime.Now.Month;
                var year = DateTime.Now.Year;
                var totaldays = DateTime.DaysInMonth(year, month);
                DateTime F_dt = Convert.ToDateTime(year + "-" + month + "-01 00:00:00 AM");
                DateTime T_dt = Convert.ToDateTime(year + "-" + month + "-" + totaldays + " 23:59:59 PM");
                ViewBag.fromdate = F_dt.ToString("yyyy-MM-dd");
                ViewBag.todate = T_dt.ToString("yyyy-MM-dd");
                tasks = tasks.Where(a => a.AssignDate.Value >= F_dt && a.AssignDate.Value <= T_dt).ToList();
            }
            TaskDashboardcls taskDashboardcls = new TaskDashboardcls();
            taskDashboardcls.TotalMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0)).ToList().Count();
            taskDashboardcls.PendingMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "PENDING").ToList().Count();
            taskDashboardcls.onholdMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "ON-HOLD").ToList().Count();
            taskDashboardcls.inprogressMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "IN-PROGRESS").ToList().Count();
            taskDashboardcls.CompletedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "COMPLETED").ToList().Count();
            taskDashboardcls.VerifiedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "VERIFY").ToList().Count();


            taskDashboardcls.TotalTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId).ToList().Count();
            taskDashboardcls.PendingTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "PENDING").ToList().Count();
            taskDashboardcls.onholdTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "ON-HOLD").ToList().Count();
            taskDashboardcls.inprogressTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "IN-PROGRESS").ToList().Count();
            taskDashboardcls.CompletedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "COMPLETED").ToList().Count();
            taskDashboardcls.VerifiedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "VERIFY").ToList().Count();


            taskDashboardcls.TotalTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId).ToList().Count();
            taskDashboardcls.PendingTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "PENDING").ToList().Count();
            taskDashboardcls.onholdTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "ON-HOLD").ToList().Count();
            taskDashboardcls.inprogressTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "IN-PROGRESS").ToList().Count();
            taskDashboardcls.CompletedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "COMPLETED").ToList().Count();
            taskDashboardcls.VerifiedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "VERIFY").ToList().Count();



            return View(taskDashboardcls);
        }

        #region Todo


        public IActionResult Todo()
        {
            CheckLoginStatus();
            //if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            //var res = commonTaskMethods.GetAllTaskDetails();

            return View();
        }
        public IActionResult MyTodo()
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            userId = HttpContext.Session.GetInt32("userId").Value;
            var res = commonMethods.GetAllMyParentTask(userId);
            var tasks = (from a in res
                         group a.AssignedDate by a into g
                         select new
                         {
                             AssignDate = g.Key.AssignedDate
                         }).ToList();
            List<DatewiseTasks> dttasks = new List<DatewiseTasks>();
            foreach (var a in tasks)
            {
                DatewiseTasks datewiseTasks = new DatewiseTasks();
                datewiseTasks.AssignDate = a.AssignDate;
                datewiseTasks.Completedlist = res.Where(m => m.AssignedDate == a.AssignDate && m.status == "COMPLETED").ToList();
                datewiseTasks.inprogresslist = res.Where(m => m.AssignedDate == a.AssignDate && m.status == "IN-PROGRESS").ToList();
                datewiseTasks.onholdlist = res.Where(m => m.AssignedDate == a.AssignDate && m.status == "ON-HOLD").ToList();
                datewiseTasks.Pendinglist = res.Where(m => m.AssignedDate == a.AssignDate && m.status == "PENDING").ToList();
                datewiseTasks.Verifiedlist = res.Where(m => m.AssignedDate == a.AssignDate && m.status == "VERIFIED").ToList();
                dttasks.Add(datewiseTasks);
            }
            return View(dttasks);
        }
        public IActionResult TaskAssignToMe(string[] TodoStatusName, long[] TodoPriority, string fromdate, string todate)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("Assigned By Others", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            TempData["FromDate"] = fromdate;
            TempData["ToDate"] = todate;

            taskviewmodelcls taskviewmodelcls = new taskviewmodelcls();
            taskviewmodelcls.Priorities = todoRepository.GetAllTodoPriority();
            taskviewmodelcls.employeecls = Common.GetEmployeesForTasks(userId);
            taskviewmodelcls.TodoStatusName = TodoStatusName.ToList().Count > 0 ? TodoStatusName : null;
            taskviewmodelcls.TodoPriority = TodoPriority.ToList().Count > 0 ? TodoPriority : null;
            taskviewmodelcls.assignbydepartmentcls = Common.GetDepartmentsByEmployeeID(userId);
            taskviewmodelcls.tasks = commonMethods.GetAssignToMe(userId, TodoStatusName, TodoPriority, fromdate, todate);


            return View(taskviewmodelcls);
        }
        public IActionResult TaskAssignToOther(long[] employeeid, long[] TodoPriority, string fromdate, string todate)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("Assigned To Others", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            TempData["FromDate"] = fromdate;
            TempData["ToDate"] = todate;
            //ViewBag.Priorities = todoRepository.GetAllTodoPriority();
            //ViewBag.employeecls = commonMethods.GetEmployeesForTasks(userId);
            //ViewBag.status = TodoStatusName;
            //ViewBag.priority = TodoPriority;
            //ViewBag.assignbydepartmentcls = commonMethods.GetDepartmentsByEmployeeID(userId);
            //var res = commonMethods.GetAssignToOthers(userId, TodoStatusName, TodoPriority);

            taskviewmodelcls taskviewmodelcls = new taskviewmodelcls();
            taskviewmodelcls.Priorities = todoRepository.GetAllTodoPriority();
            taskviewmodelcls.employeecls = Common.GetEmployeesForTasks(userId);
            taskviewmodelcls.employeeid = employeeid.ToList().Count > 0 ? employeeid : null;
            taskviewmodelcls.TodoPriority = TodoPriority.ToList().Count > 0 ? TodoPriority : null;
            taskviewmodelcls.assignbydepartmentcls = Common.GetDepartmentsByEmployeeID(userId);
            taskviewmodelcls.assigntoemployeecls = commonMethods.getassigntoemployeelist(userId);

            taskviewmodelcls.tasks = commonMethods.GetAssignToOthers(userId, employeeid, TodoPriority, fromdate, todate);








            return View(taskviewmodelcls);
        }
        public IActionResult CreateOrEditTodo(long? id, string act)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("New Task", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            TodoModel od = new TodoModel();
            userId = HttpContext.Session.GetInt32("userId").Value;
            ViewBag.act = act;
            od.employeecls = Common.GetEmployeesForTasks(userId);
            od.assignbydepartmentcls = Common.GetDepartmentsByEmployeeID(userId);
            od.Priorities = todoRepository.GetAllTodoPriority();
            if (id != null)
            {
                var tododetails = todoRepository.GetTodoById(id.Value);
                od.ID = tododetails.ID;
                od.Name = tododetails.Name;
                od.AssignedId = tododetails.AssignedId;
                od.TodoPriorityID = tododetails.TodoPriorityID;
                od.TodoStatusID = tododetails.TodoStatusID;
                od.AssignedEmployeeID = tododetails.EmployeeID;
                od.ParentID = tododetails.ParentId;
                od.PriorityName = od.Priorities.Where(m => m.ID == tododetails.TodoPriorityID).FirstOrDefault().Name;
                od.TodoStatusName = tododetails.Status;
                od.Description = tododetails.Description;
                od.DueDate = tododetails.DueDate;
                od.CompletionDate = tododetails.CompletionDate;
                od.AssignedOn = tododetails.AssignedDate;
                od.VerifiedDate = tododetails.VerifiedDate;
                od.departmentcls = Common.GetDepartmentsByEmployeeID(tododetails.AssignedId);
                od.AssignByDepartmentID = tododetails.AssignByDepartmentID;
                od.DepartmentID = tododetails.DepartmentID;
                od.todoattachments = commonMethods.GetAttachments(id.Value, userId).OrderByDescending(a => a.id).Take(4).ToList();
                od.todocomments = commonMethods.GetComments(id.Value, userId).Take(4).ToList();
                if (tododetails.ParentId == 0)
                {
                    od.subtasklist_Cls = commonMethods.GetSubTaskDetails(id.Value, userId);
                }
                if (tododetails.EmployeeID == userId || tododetails.AssignedId == userId)
                {
                    var comments = todoRepository.GetAllTodoComment().Where(a => a.TodoID == tododetails.ID && a.InsertedId != userId && a.IsRead == false).ToList();
                    foreach (var a in comments)
                    {
                        TodoComment comment = a;
                        comment.IsRead = true;
                        comment.ModifiedId = userId;
                        todoRepository.UpdateTodoComment(comment);
                    }
                }
                ViewBag.status = "Update Task";
                ViewBag.statusnew = "Update and Add New Task";
            }
            else
            {
                od.ID = 0;
                od.AssignedId = userId;
                od.AssignedEmployeeID = userId;
                od.departmentcls = Common.GetDepartmentsByEmployeeID(userId);
                od.TodoStatusName = "PENDING";
                ViewBag.status = "Create Task";
                ViewBag.statusnew = "Create and Add New Task";
            }
            return View(od);
        }
        public IActionResult CreateOrEditMyTodo(long? id)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("New Task", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            TodoModel od = new TodoModel();
            userId = HttpContext.Session.GetInt32("userId").Value;

            od.employeecls = Common.GetEmployeesForTasks(userId);
            od.assignbydepartmentcls = Common.GetDepartmentsByEmployeeID(userId);
            od.Priorities = todoRepository.GetAllTodoPriority();
            if (id != null)
            {
                var tododetails = todoRepository.GetTodoById(id.Value);
                od.ID = tododetails.ID;
                od.Name = tododetails.Name;
                od.AssignedId = tododetails.AssignedId;
                od.TodoPriorityID = tododetails.TodoPriorityID;
                od.TodoStatusID = tododetails.TodoStatusID;
                od.AssignedEmployeeID = tododetails.EmployeeID;
                od.ParentID = tododetails.ParentId;
                od.PriorityName = od.Priorities.Where(m => m.ID == tododetails.TodoPriorityID).FirstOrDefault().Name;
                od.TodoStatusName = tododetails.Status;
                od.Description = tododetails.Description;
                od.DueDate = tododetails.DueDate;
                od.CompletionDate = tododetails.CompletionDate;
                od.AssignedOn = tododetails.AssignedDate;
                od.VerifiedDate = tododetails.VerifiedDate;
                od.departmentcls = Common.GetDepartmentsByEmployeeID(tododetails.AssignedId);
                od.AssignByDepartmentID = tododetails.AssignByDepartmentID;
                od.DepartmentID = tododetails.DepartmentID;
                od.todoattachments = commonMethods.GetAttachments(id.Value, userId).OrderByDescending(a => a.id).Take(4).ToList();
                od.todocomments = commonMethods.GetComments(id.Value, userId).Take(4).ToList();
                if (tododetails.ParentId == 0)
                {
                    od.subtasklist_Cls = commonMethods.GetSubTaskDetails(id.Value, userId);
                }
                ViewBag.status = "Update Task";
                ViewBag.statusnew = "Update and Add New Task";
            }
            else
            {
                od.ID = 0;
                od.AssignedId = userId;
                od.AssignedEmployeeID = userId;
                od.departmentcls = Common.GetDepartmentsByEmployeeID(userId);
                od.TodoStatusName = "PENDING";
                ViewBag.status = "Create Task";
                ViewBag.statusnew = "Create and Add New Task";
            }
            return View(od);
        }
        public async Task<IActionResult> ViewTaskDetails(long id, string act)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            viewtaskclass viewtaskclass = new viewtaskclass();
            viewtaskclass.todoattachments = commonMethods.GetAttachments(id, userId).OrderByDescending(a => a.id).ToList();
            viewtaskclass.todocomments = commonMethods.GetComments(id, userId).ToList();
            viewtaskclass.subtasklist_Cls = commonMethods.GetSubTaskDetails(id, userId);
            viewtaskclass.task = commonMethods.GetallTasks(userId).Where(a => a.todoid == id).FirstOrDefault();
            viewtaskclass.todoTimelines = commonMethods.GetTaskTimelines(id);
            ViewBag.act = act;
            return View(viewtaskclass);
        }
        public async Task<IActionResult> MyTasks(string[] TodoStatusName, long[] TodoPriority, string fromdate, string todate)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("My Tasks", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            TempData["FromDate"] = fromdate;
            TempData["ToDate"] = todate;

            taskviewmodelcls taskviewmodelcls = new taskviewmodelcls();
            taskviewmodelcls.Priorities = todoRepository.GetAllTodoPriority();
            taskviewmodelcls.employeecls = Common.GetEmployeesForTasks(userId);
            taskviewmodelcls.TodoStatusName = TodoStatusName.ToList().Count > 0 ? TodoStatusName : null;
            taskviewmodelcls.TodoPriority = TodoPriority.ToList().Count > 0 ? TodoPriority : null;
            taskviewmodelcls.assignbydepartmentcls = Common.GetDepartmentsByEmployeeID(userId);
            taskviewmodelcls.tasks = await Task.Run(() => commonMethods.GetMyTasks(userId, TodoStatusName, TodoPriority, fromdate, todate));

            return View(taskviewmodelcls);
        }
        public IActionResult AllComments(long id, string act)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            var todo = todoRepository.GetTodoById(id);
            if (todo.EmployeeID == userId || todo.AssignedId == userId)
            {
                var comments = todoRepository.GetAllTodoComment().Where(a => a.TodoID == id && a.InsertedId != userId && a.IsRead == false).ToList();
                foreach (var a in comments)
                {
                    TodoComment comment = a;
                    comment.IsRead = true;
                    comment.ModifiedId = userId;
                    todoRepository.UpdateTodoComment(comment);
                }
            }
            var attachments = commonMethods.GetComments(id, userId);
            ViewBag.act = act;
            allcommentscls allcommentscls = new allcommentscls();
            allcommentscls.todo = attachments.ToList();
            allcommentscls.todoID = id;
            allcommentscls.todoName = todoRepository.GetTodoById(id).Name;

            return View(allcommentscls);
        }
        public IActionResult RemoveComment(long id)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            TodoComment todoComment = todoRepository.GetTodoCommentById(id);
            if (todoComment.InsertedId == userId)
            {
                todoComment.IsAvailable = false;
                todoComment.Active = false;
                todoComment.ModifiedId = userId;
                todoComment.ModifiedDate = DateTime.Now;
                todoRepository.UpdateTodoComment(todoComment);
            }
            //string url= R;
            return RedirectToAction(nameof(AllComments)).WithSuccess("success", "Deleted successfully");
            // return Redirect(url).WithSuccess("success", "Deleted successfully");
        }
        public IActionResult RemoveCommentBytask(long id)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            TodoComment todoComment = todoRepository.GetTodoCommentById(id);
            if (todoComment.InsertedId == userId)
            {
                todoComment.IsAvailable = false;
                todoComment.Active = false;
                todoComment.ModifiedId = userId;
                todoComment.ModifiedDate = DateTime.Now;
                todoRepository.UpdateTodoComment(todoComment);
            }
            return RedirectToAction(nameof(CreateOrEditTodo), new { id = todoComment.TodoID }).WithSuccess("success", "Deleted successfully"); ;
        }
        public IActionResult AllAttachments(long id, string act)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            var attachments = commonMethods.GetAttachments(id, userId).OrderByDescending(a => a.id);
            allattachmentcls allattachmentcls = new allattachmentcls();
            allattachmentcls.todo = attachments.ToList();
            allattachmentcls.todoID = id;
            allattachmentcls.todoName = todoRepository.GetTodoById(id).Name;
            ViewBag.act = act;
            return View(allattachmentcls);
        }
        public IActionResult RemoveAttachments(long id)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            TodoAttachment todoAttachment = todoRepository.GetTodoAttachmentById(id);
            if (todoAttachment.InsertedId == userId)
            {
                todoAttachment.IsAvailable = false;
                todoAttachment.Active = false;
                todoAttachment.ModifiedId = userId;
                todoAttachment.ModifiedDate = DateTime.Now;
                todoRepository.UpdateTodoAttachment(todoAttachment);
            }
            return RedirectToAction(nameof(AllAttachments)).WithSuccess("success", "Deleted successfully");
        }
        public IActionResult RemoveAttachmentsByTask(long id, string act)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            TodoAttachment todoAttachment = todoRepository.GetTodoAttachmentById(id);
            if (todoAttachment.InsertedId == userId)
            {
                todoAttachment.IsAvailable = false;
                todoAttachment.Active = false;
                todoAttachment.ModifiedId = userId;
                todoAttachment.ModifiedDate = DateTime.Now;
                todoRepository.UpdateTodoAttachment(todoAttachment);
            }
            return RedirectToAction(act, new { id = todoAttachment.TodoID }).WithSuccess("success", "Deleted successfully");
        }
        public async Task<JsonResult> GetDepartmentsByEmployeeID(long id)
        {
            return Json(Common.GetDepartmentsByEmployeeID(id));
        }


        public async Task<IActionResult> SaveOrUpdateMyTodo(TodoModel todoModel, string submitbuttonvalue)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {
                userId = HttpContext.Session.GetInt32("userId").Value;
                Todo todo = new Todo();
                long id = 0;
                var emp = employeeRepository.GetEmployeeById(userId);
                var priorities = todoRepository.GetTodoPriorityById(todoModel.TodoPriorityID).Name;
                if (todoModel.ID == 0)
                {
                    todo.Active = true;
                    todo.AssignedDate = DateTime.Now;
                    todo.AssignedId = userId;
                    todo.DueDate = todoModel.DueDate;
                    todo.EmployeeID = userId;
                    todo.InsertedId = userId;
                    todo.ModifiedId = userId;
                    todo.Description = todoModel.Description;
                    todo.Name = todoModel.Name;
                    todo.ParentId = 0;
                    todo.Status = "PENDING";
                    todo.TodoPriorityID = todoModel.TodoPriorityID;
                    todoRepository.CreateTodo(todo);
                    id = todo.ID;
                    TodoTimeline todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId;
                    // todoTimeline.RelatedDate = DateTime.Now;
                    // todoTimeline.RelatedId = userId;
                    todoTimeline.Status = "PENDING";
                    todoTimeline.TodoID = id;
                    todoTimeline.TodoStatus = "Task Created";
                    todoRepository.CreateTodoTimeline(todoTimeline);

                    if (todoModel.DueDate != null)
                    {
                        todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        todoTimeline.RelatedDate = todoModel.DueDate;
                        //  todoTimeline.RelatedId = userId;
                        // todoTimeline.Status = todoModel.TodoStatusName;
                        todoTimeline.TodoID = id;
                        todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                        todoRepository.CreateTodoTimeline(todoTimeline);
                    }
                }
                else
                {
                    todo = todoRepository.GetTodoById(todoModel.ID);

                    if (todoModel.DueDate != null)
                    {
                        if (todo.DueDate != null)
                        {
                            if (todoModel.DueDate.Value.ToString("dd-MMM-yyyy") != todo.DueDate.Value.ToString("dd-MMM-yyyy"))
                            {
                                TodoTimeline todoTimeline = new TodoTimeline();
                                todoTimeline.Active = true;
                                todoTimeline.InsertedId = userId;
                                todoTimeline.IsAvailable = true;
                                todoTimeline.ModifiedId = userId;
                                todoTimeline.RelatedDate = todoModel.DueDate;
                                //  todoTimeline.RelatedId = userId;
                                //   todoTimeline.Status = todoModel.TodoStatusName;
                                todoTimeline.TodoID = todoModel.ID;
                                todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                                todoRepository.CreateTodoTimeline(todoTimeline);
                            }
                        }
                        else
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = userId;
                            todoTimeline.RelatedDate = todoModel.DueDate;
                            // todoTimeline.RelatedId = userId;
                            // todoTimeline.Status = todoModel.TodoStatusName;
                            todoTimeline.TodoID = todoModel.ID;
                            todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                            todoRepository.CreateTodoTimeline(todoTimeline);
                        }
                    }
                    if (todoModel.TodoStatusName != todo.Status)
                    {
                        TodoTimeline todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        //  todoTimeline.RelatedDate = todoModel.DueDate;
                        //  todoTimeline.RelatedId = userId;
                        todoTimeline.Status = todoModel.TodoStatusName;
                        todoTimeline.TodoID = todoModel.ID;
                        todoTimeline.TodoStatus = "Task " + todoModel.TodoStatusName.ToLower();
                        todoRepository.CreateTodoTimeline(todoTimeline);
                        //if (todoModel.TodoStatusName == "COMPLETED")
                        //{
                        //    var assignemp = employeeRepository.GetEmployeeById(todo.AssignedId);
                        //    sms.taskmail("The task assigned by you to " + assignemp.FirstName + " " + assignemp.LastName + ", has been marked completed.<br/>Task Name : " + todoModel.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + todoModel.Description + "<br/><br/>Kindly verify the same and mark it complete from your end.", "Task Completed", emp.EmailId);
                        //}
                    }
                    todo.Active = true;
                    todo.AssignedDate = DateTime.Now;
                    todo.AssignedId = userId;
                    todo.DueDate = todoModel.DueDate;
                    todo.EmployeeID = todo.EmployeeID;
                    todo.ModifiedId = userId;
                    todo.Description = todoModel.Description;
                    todo.Name = todoModel.Name;
                    todo.TodoPriorityID = todoModel.TodoPriorityID;
                    if (todoModel.TodoStatusName == "COMPLETED")
                    {
                        todo.CompletionDate = DateTime.Now;
                    }
                    else
                    {
                        todo.CompletionDate = null;
                    }
                    todo.Status = todoModel.TodoStatusName;
                    todoRepository.UpdateTodo(todo);
                    var subtasks = todoRepository.GetAllTodo().Where(m => m.ParentId == todoModel.ID).ToList();
                    if (subtasks.Count() > 0)
                    {
                        foreach (var a in subtasks)
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = userId;
                            //  todoTimeline.RelatedId = userId;
                            todoTimeline.Status = todoModel.TodoStatusName;
                            todoTimeline.TodoID = a.ID;
                            todoTimeline.TodoStatus = "Task " + todoModel.TodoStatusName;
                            todoRepository.CreateTodoTimeline(todoTimeline);

                            Todo todoS = a;
                            todoS.Status = todoModel.TodoStatusName;
                            todoS.ModifiedId = userId;
                            if (todoModel.TodoStatusName == "COMPLETED")
                            {
                                todoS.CompletionDate = DateTime.Now;
                            }
                            todoRepository.UpdateTodo(todoS);
                        }
                    }
                    id = todoModel.ID;
                }
                if (submitbuttonvalue == "S")
                {
                    return RedirectToAction(nameof(CreateOrEditMyTodo), new { id = id }).WithSuccess("success", "Saved successfully");

                }
                else
                {
                    return RedirectToAction(nameof(CreateOrEditMyTodo)).WithSuccess("success", "Saved successfully");
                }
            }
            return View(nameof(CreateOrEditMyTodo), todoModel).WithDanger("error", "Not Saved");
        }

        public IActionResult DownloadSampleMyTasks()
        {
            CheckLoginStatus();// long accessId = HttpContext.Session.GetInt32("accessId").Value;
                               // long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("My Tasks", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            XLWorkbook oWB = new XLWorkbook();
            DataTable prioritydt = new DataTable();
            prioritydt.Columns.Add("Name");
            var priority = todoRepository.GetAllTodoPriority();
            foreach (var a in priority)
            {
                DataRow dr = prioritydt.NewRow();
                dr["Name"] = a.Name;
                prioritydt.Rows.Add(dr);
            }
            prioritydt.TableName = "Priority";
            int lastCellNo1 = prioritydt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(prioritydt);
            var worksheet1 = oWB.Worksheet(1);

            DataTable statusdt = new DataTable();
            statusdt.Columns.Add("Name");

            DataRow dr1 = statusdt.NewRow();
            dr1["Name"] = "PENDING";
            statusdt.Rows.Add(dr1);
            dr1 = statusdt.NewRow();
            dr1["Name"] = "ON-HOLD";
            statusdt.Rows.Add(dr1);
            dr1 = statusdt.NewRow();
            dr1["Name"] = "IN-PROGRESS";
            statusdt.Rows.Add(dr1);
            dr1 = statusdt.NewRow();
            dr1["Name"] = "COMPLETED";
            statusdt.Rows.Add(dr1);
            statusdt.TableName = "Status";
            int lastCellNo2 = statusdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(statusdt);
            var worksheet2 = oWB.Worksheet(2);


            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Name");
            validationTable.Columns.Add("Description");
            validationTable.Columns.Add("Due Date(yyyy-MM-dd)");
            validationTable.Columns.Add("Priority");
            validationTable.Columns.Add("Status");
            validationTable.TableName = "My_Task_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            //worksheet.Column(3).Cell(1).SetDataType(XLDataType.Text);
            //worksheet.Range("C3:C2").SetDataType(XLDataType.DateTime);
            worksheet.Column(3).SetDataValidation().Date.EqualOrGreaterThan(DateTime.Now.Date);
            worksheet.Column(4).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(5).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            worksheet2.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"My_Task_Details.xlsx");
        }
        public IActionResult DownloadSampleAssignToOther()
        {
            CheckLoginStatus();
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (Common.checkaccessavailable("Assigned To Others", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var wb = new XLWorkbook();
            // var wsPresentation = wb.Worksheets.Add("Presentation");
            var wsData = wb.Worksheets.Add("Data");

            // Fill up some data

            //  od.employeecls = commonMethods.GetEmployeesForTasks(userId);
            var assignbydepartmentcls = Common.GetDepartmentsByEmployeeID(userId);
            var Priorities = todoRepository.GetAllTodoPriority();
            var departments = Common.GetDepartments();
            var employeecls = Common.GetAllDeptwithEmployee();
            var employees = employeeRepository.GetAllEmployee();
            //var groups = groupRepository.GetAllGroup();
            //var organisations = organizationRepository.GetAllOrganization();

            wsData.Cell(1, 1).Value = "My Department";
            int i = 1;
            foreach (var a in assignbydepartmentcls)
            {
                wsData.Cell(++i, 1).Value = a.name;
            }
            wsData.Range("A2:A" + i).AddToNamed("My Department");
            wsData.Cell(1, 2).Value = "Priority";
            i = 1;
            foreach (var a in Priorities)
            {
                wsData.Cell(++i, 2).Value = a.Name;
            }
            wsData.Range("B2:B" + i).AddToNamed("Priority");
            wsData.Cell(1, 3).Value = "Status";
            i = 1;
            //foreach (var a in departments)
            //{
            wsData.Cell(++i, 3).Value = "PENDING";
            wsData.Cell(++i, 3).Value = "ON-HOLD";
            wsData.Cell(++i, 3).Value = "IN-PROGRESS";
            wsData.Cell(++i, 3).Value = "COMPLETED";
            //}
            wsData.Range("C2:C" + i).AddToNamed("Status");


            wsData.Cell(1, 4).Value = "Assign Department";
            i = 1;
            foreach (var a in departments)
            {
                wsData.Cell(++i, 4).Value = "D" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-->", "__").Replace("-", "_");
            }
            wsData.Range("D2:D" + i).AddToNamed("Assign Department");
            int j = 4;
            foreach (var a in departments)
            {
                wsData.Cell(1, ++j).Value = "D" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-->", "__").Replace("-", "_");
                int k = 1;
                foreach (var b in employeecls.Where(m => m.DepartmentID == a.ID).ToList())
                {
                    wsData.Cell(++k, j).Value = employees.Where(h => h.ID == b.EmployeeID).Select(m => (m.FirstName + " " + m.LastName + "(Emp. code-" + m.EmpCode + ")")).FirstOrDefault();
                }
                wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("D" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-->", "__").Replace("-", "_"));
            }


            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("My Department");
            validationTable.Columns.Add("Name");
            validationTable.Columns.Add("Description");
            validationTable.Columns.Add("Due Date(yyyy-MM-dd)");
            validationTable.Columns.Add("Priority");
            validationTable.Columns.Add("Assign Department");
            validationTable.Columns.Add("Employee");
            validationTable.Columns.Add("Status");
            validationTable.TableName = "My_Task_Details";
            var worksheet = wb.AddWorksheet(validationTable);
            //worksheet.Column(3).Cell(1).SetDataType(XLDataType.Text);
            //worksheet.Range("C3:C2").SetDataType(XLDataType.DateTime);

            worksheet.Column(1).SetDataValidation().List(wsData.Range("My Department"), true);
            worksheet.Column(4).SetDataValidation().Date.EqualOrGreaterThan(DateTime.Now.Date);
            worksheet.Column(5).SetDataValidation().List(wsData.Range("Priority"), true);
            worksheet.Column(6).SetDataValidation().List(wsData.Range("Assign Department"), true);
            worksheet.Column(7).SetDataValidation().InCellDropdown = true;
            worksheet.Column(7).SetDataValidation().Operator = XLOperator.Between;
            worksheet.Column(7).SetDataValidation().AllowedValues = XLAllowedValues.List;
            worksheet.Column(7).SetDataValidation().List("=INDIRECT(SUBSTITUTE(F1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))");
            worksheet.Column(8).SetDataValidation().List(wsData.Range("Status"), true);
            wsData.Hide();
            //worksheet2.Hide();



            Byte[] workbookBytes;
            MemoryStream ms = GetStream(wb);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"Assign_Task_Details.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadAssignToOther(IFormFile file)
        {
            CheckLoginStatus();
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (Common.checkaccessavailable("Assigned To Others", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (file != null)
            {
                // string path1 = file.FileName;
                byte[] filestr;
                System.IO.Stream stream = file.OpenReadStream();
                using (XLWorkbook workBook = new XLWorkbook(stream))
                {
                    //Read the first Sheet from Excel file.
                    IXLWorksheet workSheet = workBook.Worksheet(2);
                    var Priorities = todoRepository.GetAllTodoPriority();
                    var departments = Common.GetDepartments();
                    var assignbydepartmentcls = Common.GetDepartmentsByEmployeeID(userId);
                    var employees = employeeRepository.GetAllEmployee();
                    foreach (IXLRow row in workSheet.Rows().Skip(1))
                    {
                        if (row.Cell(1).Value != null)
                        {
                            string duedate = row.Cell(4).Value.ToString();
                            Todo todo = new Todo();
                            long id = 0;
                            var mydept = assignbydepartmentcls.Where(a => a.name == row.Cell(1).Value.ToString()).FirstOrDefault();
                            var assigndept = departments.Where(a => a.ID == Convert.ToInt64(row.Cell(6).Value.ToString().Split('_')[0].Substring(1, (row.Cell(6).Value.ToString().Split('_')[0].Length - 1)))).FirstOrDefault();
                            var priority = Priorities.Where(a => a.Name == row.Cell(5).Value.ToString()).FirstOrDefault();
                            var empdet = row.Cell(7).Value.ToString() == "" ? null : employees.Where(m => (m.FirstName + " " + m.LastName + "(Emp. code-" + m.EmpCode + ")") == row.Cell(7).Value.ToString()).FirstOrDefault();
                            todo.Active = true;
                            todo.AssignedDate = DateTime.Now;
                            todo.AssignedId = empdet == null ? 0 : empdet.ID;
                            todo.DueDate = (duedate == null && duedate == "") ? null : Convert.ToDateTime(duedate) as Nullable<DateTime>;
                            todo.AssignByDepartmentID = mydept.id;
                            todo.EmployeeID = userId;
                            todo.InsertedId = userId;
                            todo.ModifiedId = userId;
                            todo.DepartmentID = assigndept.ID;
                            todo.Description = row.Cell(3).Value.ToString();
                            todo.Name = row.Cell(2).Value.ToString();
                            todo.ParentId = 0;
                            todo.Status = row.Cell(8).Value.ToString();
                            todo.TodoPriorityID = priority.ID;
                            todoRepository.CreateTodo(todo);
                            id = todo.ID;
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = userId;

                            todoTimeline.Status = row.Cell(8).Value.ToString();
                            todoTimeline.TodoID = id;
                            todoTimeline.TodoStatus = "Task " + row.Cell(8).Value.ToString();
                            todoRepository.CreateTodoTimeline(todoTimeline);
                            var emp = employeeRepository.GetEmployeeById(userId);
                            if (empdet != null)
                            {
                                todoTimeline = new TodoTimeline();
                                todoTimeline.Active = true;
                                todoTimeline.InsertedId = userId;
                                todoTimeline.IsAvailable = true;
                                todoTimeline.ModifiedId = userId;
                                //  todoTimeline.RelatedDate = DateTime.Now;
                                todoTimeline.RelatedId = empdet == null ? 0 : empdet.ID;
                                //  todoTimeline.Status = todoModel.TodoStatusName;
                                todoTimeline.TodoID = id;
                                todoTimeline.TodoStatus = "Task Assigned";
                                todoRepository.CreateTodoTimeline(todoTimeline);
                                sms.taskmail("You have been assigned a new task by " + emp.FirstName + " " + emp.LastName + ".<br/>Task Name : " + row.Cell(2).Value.ToString() + "<br/>Priority : " + row.Cell(5).Value.ToString() + "<br/>Status : Pending<br/>Description : " + row.Cell(3).Value.ToString() + "<br/><br/>We are sure you will complete the given task before the deadline. In case of any issue with the task, contact the assignor.", "Task Assigned-" + row.Cell(2).Value.ToString() + "-" + (emp.FirstName + " " + emp.LastName), empdet.EmailId);
                            }
                            if (duedate != null)
                            {
                                todoTimeline = new TodoTimeline();
                                todoTimeline.Active = true;
                                todoTimeline.InsertedId = userId;
                                todoTimeline.IsAvailable = true;
                                todoTimeline.ModifiedId = userId;
                                todoTimeline.RelatedDate = (duedate == null && duedate == "") ? null : Convert.ToDateTime(duedate) as Nullable<DateTime>;
                                //  todoTimeline.RelatedId = userId;
                                // todoTimeline.Status = todoModel.TodoStatusName;
                                todoTimeline.TodoID = id;
                                todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                                todoRepository.CreateTodoTimeline(todoTimeline);
                            }
                        }
                    }
                    _httpContextAccessor.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

                }
                return RedirectToAction(nameof(TaskAssignToOther)).WithSuccess("success", "Excel uploaded successfully");
            }
            else
            {
                return RedirectToAction(nameof(TaskAssignToOther)).WithWarning("Warning", "Please Upload Excel File");
            }
        }
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadMyTasks(IFormFile file)
        {
            CheckLoginStatus();
            userId = HttpContext.Session.GetInt32("userId").Value;
            if (Common.checkaccessavailable("My Tasks", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            if (file != null)
            {
                // string path1 = file.FileName;
                byte[] filestr;
                System.IO.Stream stream = file.OpenReadStream();

                using (XLWorkbook workBook = new XLWorkbook(stream))
                {
                    //Read the first Sheet from Excel file.
                    IXLWorksheet workSheet = workBook.Worksheet(3);
                    var priorities = todoRepository.GetAllTodoPriority();
                    foreach (IXLRow row in workSheet.Rows().Skip(1))
                    {
                        string duedate = row.Cell(3).Value.ToString();

                        Todo todo = new Todo();
                        todo.Active = true;
                        todo.AssignedDate = DateTime.Now;
                        todo.AssignedId = userId;
                        todo.DueDate = (duedate == null && duedate == "") ? null : Convert.ToDateTime(duedate) as Nullable<DateTime>;
                        todo.EmployeeID = userId;
                        todo.InsertedId = userId;
                        todo.ModifiedId = userId;
                        todo.Description = row.Cell(2).Value.ToString();
                        todo.Name = row.Cell(1).Value.ToString();
                        todo.ParentId = 0;
                        todo.Status = row.Cell(5).Value.ToString();
                        todo.TodoPriorityID = priorities.Where(a => a.Name == row.Cell(4).Value.ToString()).FirstOrDefault().ID;
                        todoRepository.CreateTodo(todo);
                        long id = todo.ID;
                        TodoTimeline todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        // todoTimeline.RelatedDate = DateTime.Now;
                        // todoTimeline.RelatedId = userId;
                        todoTimeline.Status = row.Cell(5).Value.ToString();
                        todoTimeline.TodoID = id;
                        todoTimeline.TodoStatus = "Task Created";
                        todoRepository.CreateTodoTimeline(todoTimeline);

                        if (duedate != null)
                        {
                            todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = userId;
                            todoTimeline.RelatedDate = (duedate == null && duedate == "") ? null : Convert.ToDateTime(duedate) as Nullable<DateTime>;
                            //  todoTimeline.RelatedId = userId;
                            // todoTimeline.Status = todoModel.TodoStatusName;
                            todoTimeline.TodoID = id;
                            todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                            todoRepository.CreateTodoTimeline(todoTimeline);
                        }

                    }
                    workBook.Dispose();
                }
                _httpContextAccessor.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

            }

            return RedirectToAction(nameof(MyTasks)).WithSuccess("success", "Excel uploaded successfully");
        }

        public async Task<IActionResult> SaveOrUpdateTodo(TodoModel todoModel, string act, string submitbuttonvalue)
        {
            CheckLoginStatus();
            if (ModelState.IsValid)
            {
                userId = HttpContext.Session.GetInt32("userId").Value;

                Todo todo = new Todo();
                long id = 0;
                var emp = employeeRepository.GetEmployeeById(userId);
                var assignemp = employeeRepository.GetEmployeeById(todoModel.AssignedId.Value);
                var priorities = todoRepository.GetTodoPriorityById(todoModel.TodoPriorityID).Name;
                if (todoModel.ID == 0)
                {
                    todo.Active = true;
                    todo.AssignedDate = DateTime.Now;
                    todo.AssignedId = todoModel.AssignedId == null ? 0 : todoModel.AssignedId.Value;
                    todo.DueDate = todoModel.DueDate;
                    todo.AssignByDepartmentID = todoModel.AssignByDepartmentID;
                    todo.EmployeeID = userId;
                    todo.InsertedId = userId;
                    todo.ModifiedId = userId;
                    todo.DepartmentID = todoModel.DepartmentID;
                    todo.Description = todoModel.Description;
                    todo.Name = todoModel.Name;
                    todo.ParentId = 0;
                    todo.Status = "PENDING";
                    todo.TodoPriorityID = todoModel.TodoPriorityID;
                    todoRepository.CreateTodo(todo);
                    id = todo.ID;
                    TodoTimeline todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId;
                    // todoTimeline.RelatedDate = DateTime.Now;
                    // todoTimeline.RelatedId = userId;
                    //if (todoModel.TodoStatusName != null)
                    //{
                    //    todoTimeline.Status = todoModel.TodoStatusName;
                    //}
                    //else
                    //{
                    todoTimeline.Status = "PENDING";
                    //}
                    todoTimeline.TodoID = id;
                    todoTimeline.TodoStatus = "Task Created";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                    if (todoModel.AssignedId != 0 && todoModel.AssignedId != null)
                    {

                        todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        //  todoTimeline.RelatedDate = DateTime.Now;
                        todoTimeline.RelatedId = todoModel.AssignedId == null ? 0 : todoModel.AssignedId.Value;
                        //  todoTimeline.Status = todoModel.TodoStatusName;
                        todoTimeline.TodoID = id;
                        todoTimeline.TodoStatus = "Task Assigned";
                        todoRepository.CreateTodoTimeline(todoTimeline);
                        sms.taskmail("You have been assigned a new task by " + emp.FirstName + " " + emp.LastName + ".<br/>Task Name : " + todoModel.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + todoModel.Description + "<br/><br/>We are sure you will complete the given task before the deadline. In case of any issue with the task, contact the assignor.", "Task Assigned-" + todoModel.Name + "-" + (emp.FirstName + " " + emp.LastName), assignemp.EmailId);
                    }
                    if (todoModel.DueDate != null)
                    {
                        todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        todoTimeline.RelatedDate = todoModel.DueDate;
                        //  todoTimeline.RelatedId = userId;
                        // todoTimeline.Status = todoModel.TodoStatusName;
                        todoTimeline.TodoID = id;
                        todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                        todoRepository.CreateTodoTimeline(todoTimeline);
                    }
                }
                else
                {
                    todo = todoRepository.GetTodoById(todoModel.ID);

                    if (todoModel.AssignedId != 0 && todoModel.AssignedId != null)
                    {
                        if (todoModel.AssignedId != todo.AssignedId)
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = userId;
                            //  todoTimeline.RelatedDate = DateTime.Now;
                            todoTimeline.RelatedId = todoModel.AssignedId == null ? 0 : todoModel.AssignedId.Value;
                            //  todoTimeline.Status = todoModel.TodoStatusName;
                            todoTimeline.TodoID = todoModel.ID;
                            todoTimeline.TodoStatus = "Task Assigned";
                            todoRepository.CreateTodoTimeline(todoTimeline);
                            sms.taskmail("You have been assigned a new task by " + emp.FirstName + " " + emp.LastName + ".<br/>Task Name : " + todoModel.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + todoModel.Description + "<br/><br/>We are sure you will complete the given task before the deadline. In case of any issue with the task, contact the assignor.", "Task Assigned-" + todoModel.Name + "-" + (emp.FirstName + " " + emp.LastName), assignemp.EmailId);
                        }
                    }
                    if (todoModel.DueDate != null)
                    {
                        if (todo.DueDate != null)
                        {
                            if (todoModel.DueDate.Value.ToString("dd-MMM-yyyy") != todo.DueDate.Value.ToString("dd-MMM-yyyy"))
                            {
                                TodoTimeline todoTimeline = new TodoTimeline();
                                todoTimeline.Active = true;
                                todoTimeline.InsertedId = userId;
                                todoTimeline.IsAvailable = true;
                                todoTimeline.ModifiedId = userId;
                                todoTimeline.RelatedDate = todoModel.DueDate;
                                //  todoTimeline.RelatedId = userId;
                                //   todoTimeline.Status = todoModel.TodoStatusName;
                                todoTimeline.TodoID = todoModel.ID;
                                todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                                todoRepository.CreateTodoTimeline(todoTimeline);
                            }
                        }
                        else
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = userId;
                            todoTimeline.RelatedDate = todoModel.DueDate;
                            // todoTimeline.RelatedId = userId;
                            // todoTimeline.Status = todoModel.TodoStatusName;
                            todoTimeline.TodoID = todoModel.ID;
                            todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                            todoRepository.CreateTodoTimeline(todoTimeline);
                        }
                    }
                    if (todoModel.TodoStatusName != todo.Status)
                    {
                        TodoTimeline todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        //  todoTimeline.RelatedDate = todoModel.DueDate;
                        //  todoTimeline.RelatedId = userId;
                        todoTimeline.Status = todoModel.TodoStatusName;
                        todoTimeline.TodoID = todoModel.ID;
                        todoTimeline.TodoStatus = "Task " + todoModel.TodoStatusName.ToLower();
                        todoRepository.CreateTodoTimeline(todoTimeline);
                        if (todoModel.TodoStatusName == "COMPLETED")
                        {
                            var assignemp1 = employeeRepository.GetEmployeeById(todo.AssignedId);
                            sms.taskmail("The task assigned by you to " + assignemp1.FirstName + " " + assignemp1.LastName + ", has been marked completed.<br/>Task Name : " + todoModel.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + todoModel.Description + "<br/><br/>Kindly verify the same and mark it complete from your end.", "Task Completed-" + todoModel.Name + "-" + (assignemp1.FirstName + " " + assignemp1.LastName), emp.EmailId);
                        }
                    }
                    todo.Active = true;
                    todo.AssignedDate = DateTime.Now;
                    todo.AssignedId = todoModel.AssignedId == null ? 0 : todoModel.AssignedId.Value;
                    todo.DueDate = todoModel.DueDate;
                    todo.EmployeeID = todo.EmployeeID;
                    todo.ModifiedId = userId;
                    todo.DepartmentID = todoModel.DepartmentID;
                    todo.Description = todoModel.Description;
                    todo.Name = todoModel.Name;
                    todo.AssignByDepartmentID = todoModel.AssignByDepartmentID;
                    todo.TodoPriorityID = todoModel.TodoPriorityID;
                    if (todoModel.TodoStatusName == "COMPLETED")
                    {
                        todo.CompletionDate = DateTime.Now;
                    }
                    else
                    {
                        todo.CompletionDate = null;
                    }
                    todo.Status = todoModel.TodoStatusName;
                    todoRepository.UpdateTodo(todo);
                    var subtasks = todoRepository.GetAllTodo().Where(m => m.ParentId == todoModel.ID).ToList();
                    if (subtasks.Count() > 0)
                    {
                        foreach (var a in subtasks)
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = userId;
                            //  todoTimeline.RelatedId = userId;
                            todoTimeline.Status = todoModel.TodoStatusName;
                            todoTimeline.TodoID = a.ID;
                            todoTimeline.TodoStatus = "Task " + todoModel.TodoStatusName;
                            todoRepository.CreateTodoTimeline(todoTimeline);
                            if (todoModel.TodoStatusName == "COMPLETED")
                            {
                                var empss = employeeRepository.GetEmployeeById(a.EmployeeID);
                                var prioritiess = todoRepository.GetTodoPriorityById(a.TodoPriorityID).Name;
                                var assignemp1 = employeeRepository.GetEmployeeById(a.AssignedId);
                                sms.taskmail("The task assigned by you to " + assignemp.FirstName + " " + assignemp.LastName + ", has been marked completed.<br/>Task Name : " + a.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + a.Description + "<br/><br/>Kindly verify the same and mark it complete from your end.", "Task Completed-" + a.Name + "-" + (assignemp.FirstName + " " + assignemp.LastName), emp.EmailId);
                            }
                            Todo todoS = a;
                            todoS.Status = todoModel.TodoStatusName;
                            todoS.ModifiedId = userId;
                            if (todoModel.TodoStatusName == "COMPLETED")
                            {
                                todoS.CompletionDate = DateTime.Now;
                            }
                            todoRepository.UpdateTodo(todoS);
                        }
                    }
                    id = todoModel.ID;
                }
                if (submitbuttonvalue == "S")
                {
                    return RedirectToAction(nameof(CreateOrEditTodo), new { id = id, act = act }).WithSuccess("success", "Saved successfully");

                }
                else
                {
                    return RedirectToAction(nameof(CreateOrEditTodo)).WithSuccess("success", "Saved successfully");
                }
            }
            return View(nameof(CreateOrEditTodo), todoModel).WithDanger("error", "Not Saved");
        }
        public async Task<IActionResult> SaveOrUpdateSubTodo(string name, long priority, long employeeid, long deptid, long assigndeptid, string desc, long parentid, DateTime? duedate, long subtaskid)
        {
            CheckLoginStatus();
            userId = HttpContext.Session.GetInt32("userId").Value;
            Todo todo = new Todo();
            long id = 0;
            var emp = employeeRepository.GetEmployeeById(userId);
            var priorities = todoRepository.GetTodoPriorityById(priority).Name;
            if (subtaskid == 0)
            {
                todo.AssignByDepartmentID = assigndeptid;
                todo.Active = true;
                todo.AssignedDate = DateTime.Now;
                todo.AssignedId = employeeid;
                todo.DueDate = duedate;
                todo.EmployeeID = userId;
                todo.InsertedId = userId;
                todo.ModifiedId = userId;
                todo.DepartmentID = deptid;
                todo.Description = desc;
                todo.Name = name;
                todo.ParentId = parentid;
                todo.Status = "PENDING";
                todo.TodoPriorityID = priority;
                todoRepository.CreateTodo(todo);
                id = todo.ID;
                TodoTimeline todoTimeline = new TodoTimeline();
                todoTimeline.Active = true;
                todoTimeline.InsertedId = userId;
                todoTimeline.IsAvailable = true;
                todoTimeline.ModifiedId = userId;
                // todoTimeline.RelatedDate = DateTime.Now;
                // todoTimeline.RelatedId = userId;
                todoTimeline.Status = "PENDING";
                todoTimeline.TodoID = id;
                todoTimeline.TodoStatus = "Task Created";
                todoRepository.CreateTodoTimeline(todoTimeline);
                if (employeeid != 0 && employeeid != null)
                {
                    todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId;
                    //todoTimeline.RelatedDate = DateTime.Now;
                    todoTimeline.RelatedId = employeeid;
                    todoTimeline.Status = "PENDING";
                    todoTimeline.TodoID = id;
                    todoTimeline.TodoStatus = "Task Assigned";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                    sms.taskmail("You have been assigned a new task by " + emp.FirstName + " " + emp.LastName + ".<br/>Task Name : " + name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + desc + "<br/><br/>We are sure you will complete the given task before the deadline. In case of any issue with the task, contact the assignor.", "Task Assigned-" + name + "-" + (emp.FirstName + " " + emp.LastName), emp.EmailId);
                }
                if (duedate != null)
                {
                    todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId;
                    todoTimeline.RelatedDate = duedate;
                    //  todoTimeline.RelatedId = userId;
                    //   todoTimeline.Status = "PENDING";
                    todoTimeline.TodoID = id;
                    todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                }
            }
            else
            {
                todo = todoRepository.GetTodoById(subtaskid);
                if (employeeid != 0 && employeeid != null)
                {
                    if (employeeid != todo.AssignedId)
                    {
                        TodoTimeline todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        //  todoTimeline.RelatedDate = DateTime.Now;
                        todoTimeline.RelatedId = employeeid;
                        // todoTimeline.Status = todo.Status;
                        todoTimeline.TodoID = subtaskid;
                        todoTimeline.TodoStatus = "Task Assigned";
                        todoRepository.CreateTodoTimeline(todoTimeline);
                        sms.taskmail("You have been assigned a new task by " + emp.FirstName + " " + emp.LastName + ".<br/>Task Name : " + name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + desc + "<br/><br/>We are sure you will complete the given task before the deadline. In case of any issue with the task, contact the assignor.", "Task Assigned-" + name + "-" + (emp.FirstName + " " + emp.LastName), emp.EmailId);
                    }
                }
                if (duedate != null)
                {
                    if (todo.DueDate != null)
                    {
                        if (duedate.Value.ToString("dd-MMM-yyyy") != todo.DueDate.Value.ToString("dd-MMM-yyyy"))
                        {
                            TodoTimeline todoTimeline = new TodoTimeline();
                            todoTimeline.Active = true;
                            todoTimeline.InsertedId = userId;
                            todoTimeline.IsAvailable = true;
                            todoTimeline.ModifiedId = userId;
                            todoTimeline.RelatedDate = duedate;
                            // todoTimeline.RelatedId = userId;
                            // todoTimeline.Status = todo.Status;
                            todoTimeline.TodoID = subtaskid;
                            todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                            todoRepository.CreateTodoTimeline(todoTimeline);
                        }
                    }
                    else
                    {
                        TodoTimeline todoTimeline = new TodoTimeline();
                        todoTimeline.Active = true;
                        todoTimeline.InsertedId = userId;
                        todoTimeline.IsAvailable = true;
                        todoTimeline.ModifiedId = userId;
                        todoTimeline.RelatedDate = duedate;
                        //  todoTimeline.RelatedId = userId;
                        //   todoTimeline.Status = todo.Status;
                        todoTimeline.TodoID = subtaskid;
                        todoTimeline.TodoStatus = "Task Deadline Date Assigned";
                        todoRepository.CreateTodoTimeline(todoTimeline);
                    }
                }
                todo.AssignByDepartmentID = assigndeptid;
                todo.Active = true;
                todo.AssignedDate = DateTime.Now;
                todo.AssignedId = employeeid;
                todo.DueDate = duedate;
                todo.EmployeeID = userId;
                todo.InsertedId = userId;
                todo.ModifiedId = userId;
                todo.DepartmentID = deptid;
                todo.Description = desc;
                todo.Name = name;
                todo.ParentId = parentid;
                todo.TodoPriorityID = priority;
                todoRepository.UpdateTodo(todo);
                id = subtaskid;

            }
            return Json(0);
        }
        [HttpPost]
        public async Task<IActionResult> statuschangetoverify(long hdntodoid, string hdnact, decimal txtscore)
        {
            CheckLoginStatus();
            long id = hdntodoid;
            string act = hdnact;
            var subtasks = todoRepository.GetAllTodo().Where(m => m.ParentId == id).ToList();
            if (subtasks.Count() > 0)
            {
                foreach (var a in subtasks)
                {
                    var emp = employeeRepository.GetEmployeeById(a.EmployeeID);
                    var priorities = todoRepository.GetTodoPriorityById(a.TodoPriorityID).Name;
                    TodoTimeline todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId;
                    // todoTimeline.RelatedId = userId;
                    todoTimeline.Status = "VERIFY";
                    todoTimeline.TodoID = a.ID;
                    todoTimeline.TodoStatus = "Task " + "VERIFY";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                    if (act != "MyTasks")
                    {
                        var assignemp = employeeRepository.GetEmployeeById(a.AssignedId);
                        sms.taskmail("The task assigned by you to " + assignemp.FirstName + " " + assignemp.LastName + ", has been marked completed.<br/>Task Name : " + a.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + a.Description + "<br/><br/>Kindly verify the same and mark it complete from your end.", "Task Completed-" + a.Name + "-" + (assignemp.FirstName + " " + assignemp.LastName), emp.EmailId);
                    }
                    Todo todo = a;
                    todo.Status = "VERIFY";
                    todo.VerifiedDate = DateTime.Now;
                    todoRepository.UpdateTodo(todo);
                }
            }
            //else
            //{
            var ab = todoRepository.GetTodoById(id);
            var emp1 = employeeRepository.GetEmployeeById(ab.EmployeeID);
            var priorities1 = todoRepository.GetTodoPriorityById(ab.TodoPriorityID).Name;
            TodoTimeline todoTimeline1 = new TodoTimeline();
            todoTimeline1.Active = true;
            todoTimeline1.InsertedId = userId;
            todoTimeline1.IsAvailable = true;
            todoTimeline1.ModifiedId = userId;
            // todoTimeline1.RelatedId = userId;
            todoTimeline1.Status = "VERIFY";
            todoTimeline1.TodoID = ab.ID;
            todoTimeline1.TodoStatus = "Task " + "VERIFY";
            todoRepository.CreateTodoTimeline(todoTimeline1);
            if (act != "MyTasks")
            {
                var assign = employeeRepository.GetEmployeeById(ab.AssignedId);
                sms.taskmail("The task assigned by you to " + assign.FirstName + " " + assign.LastName + ", has been marked completed.<br/>Task Name : " + ab.Name + "<br/>Priority : " + priorities1 + "<br/>Status : Pending<br/>Description : " + ab.Description + "<br/><br/>Kindly verify the same and mark it complete from your end.", "Task Completed-" + ab.Name + "-" + (assign.FirstName + " " + assign.LastName), emp1.EmailId);
            }
            ab.Status = "VERIFY";
            ab.VerifiedDate = DateTime.Now;
            ab.ModifiedDate = DateTime.Now;
            ab.ModifiedId = userId;
            ab.Score = txtscore;
            ab.ScoreGivenId = userId;
            ab.ScoreGivenDate = DateTime.Now;
            todoRepository.UpdateTodo(ab);
            //}
            return RedirectToAction(act).WithSuccess("success", "Verified successfully");
            //return Redirect(Request.Path).WithSuccess("success", "Verified successfully"); ;
        }
        public async Task<IActionResult> statuschangetocomplete(long id, string act)
        {
            var subtasks = todoRepository.GetAllTodo().Where(m => m.ParentId == id).ToList();
            if (subtasks.Count() > 0)
            {
                foreach (var a in subtasks)
                {
                    var emp = employeeRepository.GetEmployeeById(a.EmployeeID);
                    var priorities = todoRepository.GetTodoPriorityById(a.TodoPriorityID).Name;
                    TodoTimeline todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId;
                    //  todoTimeline.RelatedId = userId;
                    todoTimeline.Status = "COMPLETED";
                    todoTimeline.TodoID = a.ID;
                    todoTimeline.TodoStatus = "Task " + "COMPLETED";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                    if (act != "MyTasks")
                    {
                        var assignemp = employeeRepository.GetEmployeeById(a.AssignedId);
                        sms.taskmail("The task assigned by you to " + assignemp.FirstName + " " + assignemp.LastName + ", has been marked completed.<br/>Task Name : " + a.Name + "<br/>Priority : " + priorities + "<br/>Status : Pending<br/>Description : " + a.Description + "<br/><br/>Kindly verify the same and mark it complete from your end.", "Task Completed-" + a.Name + "-" + (assignemp.FirstName + " " + assignemp.LastName), emp.EmailId);
                    }
                    Todo todo = a;
                    todo.Status = "COMPLETED";
                    todo.CompletionDate = DateTime.Now;
                    todoRepository.UpdateTodo(todo);
                }
            }
            //else
            //{
            var ab = todoRepository.GetTodoById(id);
            var emp1 = employeeRepository.GetEmployeeById(ab.EmployeeID);
            var priorities1 = todoRepository.GetTodoPriorityById(ab.TodoPriorityID).Name;
            TodoTimeline todoTimeline1 = new TodoTimeline();
            todoTimeline1.Active = true;
            todoTimeline1.InsertedId = userId;
            todoTimeline1.IsAvailable = true;
            todoTimeline1.ModifiedId = userId;
            //   todoTimeline1.RelatedId = userId;
            todoTimeline1.Status = "COMPLETED";
            todoTimeline1.TodoID = ab.ID;
            todoTimeline1.TodoStatus = "Task " + "COMPLETED";
            todoRepository.CreateTodoTimeline(todoTimeline1);
            if (act != "MyTasks")
            {
                var assign = employeeRepository.GetEmployeeById(ab.AssignedId);
                sms.taskmail("The task assigned by you to " + assign.FirstName + " " + assign.LastName + ", has been marked completed.<br/>Task Name : " + ab.Name + "<br/>Priority : " + priorities1 + "<br/>Status : Pending<br/>Description : " + ab.Description + "<br/><br/>Kindly verify the same and mark it complete from your end.", "Task Completed-" + ab.Name + "-" + (assign.FirstName + " " + assign.LastName), emp1.EmailId);
            }
            ab.Status = "COMPLETED";
            ab.CompletionDate = DateTime.Now;
            todoRepository.UpdateTodo(ab);
            //}
            return RedirectToAction(act).WithSuccess("success", "Completed successfully");
        }
        public async Task<IActionResult> statuschangetoinprogress(long id, string act)
        {
            var subtasks = todoRepository.GetAllTodo().Where(m => m.ParentId == id).ToList();
            if (subtasks.Count() > 0)
            {
                foreach (var a in subtasks)
                {
                    var emp = employeeRepository.GetEmployeeById(a.EmployeeID);
                    var priorities = todoRepository.GetTodoPriorityById(a.TodoPriorityID).Name;
                    TodoTimeline todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId;
                    //  todoTimeline.RelatedId = userId;
                    todoTimeline.Status = "IN-PROGRESS";
                    todoTimeline.TodoID = a.ID;
                    todoTimeline.TodoStatus = "Task " + "IN-PROGRESS";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                    Todo todo = a;
                    todo.Status = "IN-PROGRESS";
                    todo.CompletionDate = null;
                    todoRepository.UpdateTodo(todo);
                    if (act != "MyTasks")
                    {
                        var assign = employeeRepository.GetEmployeeById(a.AssignedId);
                        sms.taskmail("The task assigned by you to " + assign.FirstName + " " + assign.LastName + ", has been marked IN-PROGRESS.<br/>Task Name : " + a.Name + "<br/>Priority : " + priorities + "<br/>Status : IN-PROGRESS<br/>Description : " + a.Description + "", "Task IN-PROGRESS-" + a.Name + "-" + (assign.FirstName + " " + assign.LastName), emp.EmailId);
                    }
                }
            }
            //else
            //{
            var ab = todoRepository.GetTodoById(id);

            TodoTimeline todoTimeline1 = new TodoTimeline();
            todoTimeline1.Active = true;
            todoTimeline1.InsertedId = userId;
            todoTimeline1.IsAvailable = true;
            todoTimeline1.ModifiedId = userId;
            //   todoTimeline1.RelatedId = userId;
            todoTimeline1.Status = "IN-PROGRESS";
            todoTimeline1.TodoID = ab.ID;
            todoTimeline1.TodoStatus = "Task " + "IN-PROGRESS";
            todoRepository.CreateTodoTimeline(todoTimeline1);

            ab.Status = "IN-PROGRESS";
            ab.CompletionDate = null;
            todoRepository.UpdateTodo(ab);
            var emp1 = employeeRepository.GetEmployeeById(ab.EmployeeID);
            var priorities1 = todoRepository.GetTodoPriorityById(ab.TodoPriorityID).Name;
            if (act != "MyTasks")
            {
                var assign = employeeRepository.GetEmployeeById(ab.AssignedId);
                sms.taskmail("The task assigned by you to " + assign.FirstName + " " + assign.LastName + ", has been marked IN-PROGRESS.<br/>Task Name : " + ab.Name + "<br/>Priority : " + priorities1 + "<br/>Status : IN-PROGRESS<br/>Description : " + ab.Description + "", "Task IN-PROGRESS-" + ab.Name + "-" + (assign.FirstName + " " + assign.LastName), emp1.EmailId);
            }
            //}
            return RedirectToAction(act).WithSuccess("success", "In-Progress successfully"); ;
        }
        public async Task<IActionResult> statuschangetoonhold(long id, string act)
        {
            var subtasks = todoRepository.GetAllTodo().Where(m => m.ParentId == id).ToList();
            if (subtasks.Count() > 0)
            {
                foreach (var a in subtasks)
                {

                    TodoTimeline todoTimeline = new TodoTimeline();
                    todoTimeline.Active = true;
                    todoTimeline.InsertedId = userId;
                    todoTimeline.IsAvailable = true;
                    todoTimeline.ModifiedId = userId;
                    //  todoTimeline.RelatedId = userId;
                    todoTimeline.Status = "ON-HOLD";
                    todoTimeline.TodoID = a.ID;
                    todoTimeline.TodoStatus = "Task " + "ON-HOLD";
                    todoRepository.CreateTodoTimeline(todoTimeline);
                    Todo todo = a;
                    todo.Status = "ON-HOLD";
                    todo.CompletionDate = null;
                    todoRepository.UpdateTodo(todo);
                    var emp = employeeRepository.GetEmployeeById(a.EmployeeID);
                    var priorities = todoRepository.GetTodoPriorityById(a.TodoPriorityID).Name;
                    if (act != "MyTasks")
                    {
                        var assign = employeeRepository.GetEmployeeById(a.AssignedId);
                        sms.taskmail("The task assigned by you to " + assign.FirstName + " " + assign.LastName + ", has been marked ON-HOLD.<br/>Task Name : " + a.Name + "<br/>Priority : " + priorities + "<br/>Status : ON-HOLD<br/>Description : " + a.Description + "", "Task IN-PROGRESS-" + a.Name + "-" + (assign.FirstName + " " + assign.LastName), emp.EmailId);
                    }
                }
            }
            //else
            //{
            var ab = todoRepository.GetTodoById(id);

            TodoTimeline todoTimeline1 = new TodoTimeline();
            todoTimeline1.Active = true;
            todoTimeline1.InsertedId = userId;
            todoTimeline1.IsAvailable = true;
            todoTimeline1.ModifiedId = userId;
            //   todoTimeline1.RelatedId = userId;
            todoTimeline1.Status = "ON-HOLD";
            todoTimeline1.TodoID = ab.ID;
            todoTimeline1.TodoStatus = "Task " + "ON-HOLD";
            todoRepository.CreateTodoTimeline(todoTimeline1);

            ab.Status = "ON-HOLD";
            ab.CompletionDate = null;
            todoRepository.UpdateTodo(ab);
            var emp1 = employeeRepository.GetEmployeeById(ab.EmployeeID);
            var priorities1 = todoRepository.GetTodoPriorityById(ab.TodoPriorityID).Name;
            if (act != "MyTasks")
            {
                var assign = employeeRepository.GetEmployeeById(ab.AssignedId);
                sms.taskmail("The task assigned by you to " + assign.FirstName + " " + assign.LastName + ", has been marked ON-HOLD.<br/>Task Name : " + ab.Name + "<br/>Priority : " + priorities1 + "<br/>Status : ON-HOLD<br/>Description : " + ab.Description + "", "Task IN-PROGRESS-" + ab.Name + "-" + (assign.FirstName + " " + assign.LastName), emp1.EmailId);
            }
            return RedirectToAction(act).WithSuccess("success", "On-Hold successfully");
        }
        [HttpPost]
        public async Task<IActionResult> assignscore(long hdntodoid1, string hdnact1, decimal txtscore1)
        {
            CheckLoginStatus();
            long id = hdntodoid1;
            string act = hdnact1;
            var ab = todoRepository.GetTodoById(id);

            ab.ModifiedDate = DateTime.Now;
            ab.ModifiedId = userId;
            ab.Score = txtscore1;
            ab.ScoreGivenId = userId;
            ab.ScoreGivenDate = DateTime.Now;
            todoRepository.UpdateTodo(ab);
            //}
            return RedirectToAction(act).WithSuccess("success", "Assign Score successfully");
        }

        public async Task<JsonResult> savetodoattachments(IFormFile file, string attachmentname, long todoid)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            TodoAttachment todoAttachment = new TodoAttachment();
            todoAttachment.Active = true;
            todoAttachment.AttachmentName = attachmentname;
            if (file != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(file.FileName);
                var newFilename = attachmentname + "_" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                var webPath = hostingEnvironment.WebRootPath;
                string path = Path.Combine("", webPath + @"\ODMImages\ToDoAttachments\" + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                todoAttachment.Attachmenturl = pathToSave;
            }
            todoAttachment.InsertedId = userId;
            todoAttachment.IsAvailable = true;
            todoAttachment.IsRead = false;
            todoAttachment.ModifiedId = userId;
            todoAttachment.Status = "Uploaded";
            todoAttachment.TodoID = todoid;
            todoRepository.CreateTodoAttachment(todoAttachment);
            return Json(0);
        }

        public async Task<JsonResult> savetodocomments(string comments, long todoidd)
        {
            userId = HttpContext.Session.GetInt32("userId").Value;
            TodoComment todocomments = new TodoComment();
            todocomments.Active = true;
            todocomments.Description = comments;
            todocomments.InsertedId = userId;
            todocomments.IsAvailable = true;
            todocomments.IsRead = false;
            todocomments.ModifiedId = userId;
            todocomments.Status = "Create";
            todocomments.TodoID = todoidd;
            todoRepository.CreateTodoComment(todocomments);
            Todo todo = todoRepository.GetTodoById(todoidd);
            if (todo.EmployeeID == userId && todo.AssignedId != userId)
            {
                if (todo.AssignedId != 0)
                {
                    var emp = employeeRepository.GetEmployeeById(todo.EmployeeID);
                    var assign = employeeRepository.GetEmployeeById(todo.AssignedId);
                    sms.taskmail("The following comment has been given by " + emp.FirstName + " " + emp.LastName + ",<br/><br/><strong>" + comments + "</strong><br/><br/>For the task : " + todo.Name + "<br/>Status : " + todo.Status + "<br/>Description : " + todo.Description + "<br/>Kindly act accordingly.", "Task Comment-" + todo.Name + "-" + (emp.FirstName + " " + emp.LastName), assign.EmailId);
                }

            }
            else if (todo.AssignedId == userId && todo.EmployeeID != userId)
            {
                var emp = employeeRepository.GetEmployeeById(todo.AssignedId);
                var assign = employeeRepository.GetEmployeeById(todo.EmployeeID);
                sms.taskmail("The following comment has been given by " + emp.FirstName + " " + emp.LastName + ",<br/><br/><strong>" + comments + "</strong><br/><br/>For the task : " + todo.Name + "<br/>Status : " + todo.Status + "<br/>Description : " + todo.Description + "<br/>Kindly act accordingly.", "Task Comment-" + todo.Name + "-" + (emp.FirstName + " " + emp.LastName), assign.EmailId);
            }
            else if (todo.AssignedId != userId && todo.EmployeeID != userId)
            {
                var emp = employeeRepository.GetEmployeeById(userId);
                if (todo.AssignedId != 0)
                {
                    var assign = employeeRepository.GetEmployeeById(todo.AssignedId);
                    sms.taskmail("The following comment has been given by " + emp.FirstName + " " + emp.LastName + ",<br/><br/><strong>" + comments + "</strong><br/><br/>For the task : " + todo.Name + "<br/>Status : " + todo.Status + "<br/>Description : " + todo.Description + "<br/>Kindly act accordingly.", "Task Comment-" + todo.Name + "-" + (emp.FirstName + " " + emp.LastName), assign.EmailId);
                }
                if (todo.EmployeeID != 0)
                {
                    var assign = employeeRepository.GetEmployeeById(todo.EmployeeID);
                    sms.taskmail("The following comment has been given by " + emp.FirstName + " " + emp.LastName + ",<br/><br/><strong>" + comments + "</strong><br/><br/>For the task : " + todo.Name + "<br/>Status : " + todo.Status + "<br/>Description : " + todo.Description + "<br/>Kindly act accordingly.", "Task Comment-" + todo.Name + "-" + (emp.FirstName + " " + emp.LastName), assign.EmailId);
                }
            }
            return Json(0);
        }

        public async Task<ActionResult> ViewTaskList(string[] TodoStatusName, long[] TodoPriority, long[] employeeid, long[] departmentid, long? datetype, string f_date, string t_date, string m_date)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("View Tasks", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            taskviewmodelcls taskview = new taskviewmodelcls();
            taskview.TodoStatusName = TodoStatusName;

            taskview.TodoPriority = TodoPriority;
            taskview.Priorities = todoRepository.GetAllTodoPriority();

            taskview.datetype = datetype;
            if (m_date != null)
            {
                taskview.m_date = m_date;
            }
            else
            {
                taskview.m_date = null;
            }

            if (Common.checkaccessavailable("View Tasks", accessId, "Management", "Task", roleId) == true)
            {
                taskview.departments = Common.GetDepartments();
                taskview.departmentid = departmentid;
                taskview.employeecls = commonMethods.GetEmployeesForTasks(userId, departmentid);
                taskview.employeeid = employeeid;
            }
            else if (Common.checkaccessavailable("View Tasks", accessId, "Departmentwise", "Task", roleId) == true)
            {
                var allempdept = Common.GetDepartmentsByEmployeeID(userId);
                List<CommonMasterModel> mast = new List<CommonMasterModel>();
                foreach (var a in allempdept)
                {
                    var departments = commonMethods.GetallchilddepartmentbasedonParent(a.id);
                    mast.AddRange(departments);
                }
                taskview.departments = mast.Distinct();
                taskview.departmentid = departmentid;
                taskview.employeecls = commonMethods.GetEmployeesForTasks(userId, departmentid);
                taskview.employeeid = employeeid;
            }
            else
            {
                taskview.departments = null;
                taskview.departmentid = null;
                taskview.employeecls = null;
                taskview.employeeid = null;
            }
            if (f_date != null && t_date != null)
            {
                DateTime f_dt = Convert.ToDateTime(f_date);
                DateTime t_dt = Convert.ToDateTime(t_date);
                taskview.f_date = f_dt.ToString("yyyy-MM-dd");
                taskview.t_date = t_dt.ToString("yyyy-MM-dd");
                if (f_dt > t_dt)
                {
                    ViewBag.msg = "From date can't be greater than To date";
                    taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, null, null, null, null, null, null, null, null);
                    return View(taskview);
                }
                else
                {
                    taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, TodoStatusName, TodoPriority, employeeid, departmentid, datetype, f_dt, t_dt, m_date);
                    return View(taskview);
                }
            }
            else if ((f_date != null && t_date == null) || (f_date == null && t_date != null))
            {
                taskview.f_date = null;
                taskview.t_date = null;
                ViewBag.msg = "Both From date and To date Required";
                taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, null, null, null, null, null, null, null, null);
                return View(taskview);
            }
            else
            {
                taskview.f_date = null;
                taskview.t_date = null;
            }
            taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, TodoStatusName, TodoPriority, employeeid, departmentid, datetype, null, null, m_date);
            return View(taskview);

        }
        public async Task<ActionResult> ViewTaskReportList(string[] TodoStatusName, long[] TodoPriority, long[] employeeid, long[] departmentid, long? datetype, string f_date, string t_date, string m_date)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("View Tasks", accessId, "View", "Task", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            userId = HttpContext.Session.GetInt32("userId").Value;
            taskviewmodelcls taskview = new taskviewmodelcls();
            taskview.TodoStatusName = TodoStatusName;

            taskview.TodoPriority = TodoPriority;
            taskview.Priorities = todoRepository.GetAllTodoPriority();

            taskview.datetype = datetype;
            if (m_date != null)
            {
                taskview.m_date = m_date;
            }
            else
            {
                taskview.m_date = null;
            }

            if (Common.checkaccessavailable("View Tasks", accessId, "Management", "Task", roleId) == true)
            {
                taskview.departments = Common.GetDepartments();
                taskview.departmentid = departmentid;
                taskview.employeecls = commonMethods.GetEmployeesForTasks(userId, departmentid);
                taskview.employeeid = employeeid;
            }
            else if (Common.checkaccessavailable("View Tasks", accessId, "Departmentwise", "Task", roleId) == true)
            {
                var allempdept = Common.GetDepartmentsByEmployeeID(userId);
                List<CommonMasterModel> mast = new List<CommonMasterModel>();
                foreach (var a in allempdept)
                {
                    var departments = Common.GetallchilddepartmentbasedonParent(a.id);
                    mast.AddRange(departments);
                }
                taskview.departments = mast.Distinct();
                taskview.departmentid = departmentid;
                taskview.employeecls = commonMethods.GetEmployeesForTasks(userId, departmentid);
                taskview.employeeid = employeeid;
            }
            else
            {
                taskview.departments = null;
                taskview.departmentid = null;
                taskview.employeecls = null;
                taskview.employeeid = null;
            }
            if (f_date != null && t_date != null)
            {
                DateTime f_dt = Convert.ToDateTime(f_date);
                DateTime t_dt = Convert.ToDateTime(t_date);
                taskview.f_date = f_dt.ToString("yyyy-MM-dd");
                taskview.t_date = t_dt.ToString("yyyy-MM-dd");
                if (f_dt > t_dt)
                {
                    ViewBag.msg = "From date can't be greater than To date";
                    taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, null, null, null, null, null, null, null, null);
                    // return View(taskview);
                }
                else
                {
                    taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, TodoStatusName, TodoPriority, employeeid, departmentid, datetype, f_dt, t_dt, m_date);
                    //  return View(taskview);
                }
            }
            else if ((f_date != null && t_date == null) || (f_date == null && t_date != null))
            {
                taskview.f_date = null;
                taskview.t_date = null;
                ViewBag.msg = "Both From date and To date Required";
                taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, null, null, null, null, null, null, null, null);
                //return View(taskview);
            }
            else
            {
                taskview.f_date = null;
                taskview.t_date = null;
            }
            taskview.alltask = commonMethods.GetDepartmentLeadAllTask(accessId, roleId, userId, TodoStatusName, TodoPriority, employeeid, departmentid, datetype, null, null, m_date);
            var tasks = taskview.alltask.tasks;
            List<subtasklist_cls> tasklist = new List<subtasklist_cls>();
            if (TodoStatusName[0] == "Total")
            {
                tasklist = tasks.PendingList.Union(tasks.OnholdList).Union(tasks.InprogressList).Union(tasks.CompletedList).Union(tasks.VerifiedList).OrderByDescending(a => a.todoid).ToList();
            }
            else if (TodoStatusName[0] == "Pending")
            {
                tasklist = tasks.PendingList.OrderByDescending(a => a.todoid).ToList();
            }
            else if (TodoStatusName[0] == "On-Hold")
            {
                tasklist = tasks.OnholdList.OrderByDescending(a => a.todoid).ToList();
            }
            else if (TodoStatusName[0] == "In-Progress")
            {
                tasklist = tasks.InprogressList.OrderByDescending(a => a.todoid).ToList();
            }
            else if (TodoStatusName[0] == "Completed")
            {
                tasklist = tasks.CompletedList.OrderByDescending(a => a.todoid).ToList();
            }
            else if (TodoStatusName[0] == "Verified")
            {
                tasklist = tasks.VerifiedList.OrderByDescending(a => a.todoid).ToList();
            }
            taskview.alltask.subtasklist_Cls = tasklist;
            HttpContext.Session.SetString("TaskDashboard", JsonConvert.SerializeObject(tasklist));
            return View(taskview);


        }
        public IActionResult DownloadTaskDetailsReport()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            XLWorkbook oWB = new XLWorkbook();
            DataTable moduledt = new DataTable();
            moduledt.Columns.Add("Task Name");
            moduledt.Columns.Add("Assign Date");
            moduledt.Columns.Add("Assign Time");
            moduledt.Columns.Add("Description");
            moduledt.Columns.Add("Deadline Date");
            moduledt.Columns.Add("Priority");
            moduledt.Columns.Add("Department");
            moduledt.Columns.Add("AssignedByDepartment");
            moduledt.Columns.Add("RaisedOn");
            moduledt.Columns.Add("Task Completion Date");
            moduledt.Columns.Add("Verify Date");
            moduledt.Columns.Add("Status");
            moduledt.Columns.Add("Score");

            List<subtasklist_cls> support = new List<subtasklist_cls>();
            if (HttpContext.Session.GetString("TaskDashboard") != null)
            {
                support = JsonConvert.DeserializeObject<List<subtasklist_cls>>(HttpContext.Session.GetString("TaskDashboard"));
            }

            int i = 0;
            foreach (var a in support)
            {
                DataRow dr = moduledt.NewRow();
                dr["Task Name"] = a.todoname;
                dr["Assign Date"] = a.AssignedDate;
                dr["Assign Time"] = a.assigntime;
                dr["Description"] = a.tododescription;
                dr["Deadline Date"] = a.DueDate;
                dr["Priority"] = a.priority;
                dr["Department"] = a.department;
                dr["AssignedByDepartment"] = a.AssignByDepartmentName;
                dr["RaisedOn"] = a.InsertedDate;
                dr["Task Completion Date"] = a.completionDt;
                dr["Verify Date"] = a.VerifyDt;
                dr["Status"] = a.status;
                dr["Score"] = a.score;
                moduledt.Rows.Add(dr);
            }
            moduledt.TableName = "TaskReport";
            oWB.AddWorksheet(moduledt);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();

            return File(workbookBytes, "application/ms-excel", $"TaskDetailsReport.xlsx");
        }
        #endregion

        #region TodoStatus

        //ToDo List


        #endregion

        #region TodoStatus

        public IActionResult TodoStatus()
        {
            CheckLoginStatus();
            //if (commonMethods.checkaccessavailable("Country", accessId, "List", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            return View(todoRepository.GetAllTodoStatus());
        }

        public IActionResult CreateOrEditTodoStatus(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                //if (commonMethods.checkaccessavailable("Country", accessId, "Create", "Master", roleId) == false)
                //{
                //    return RedirectToAction("AuthenticationFailed", "Accounts");
                //}
                ViewBag.status = ViewMethod.CREATE;
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                //if (commonMethods.checkaccessavailable("Country", accessId, "Edit", "Master", roleId) == false)
                //{
                //    return RedirectToAction("AuthenticationFailed", "Accounts");
                //}
                ViewBag.status = ViewMethod.UPDATE;
                var todoStatus = todoRepository.GetTodoStatusById(id.Value);
                od.ID = todoStatus.ID;
                od.Name = todoStatus.Name;
            }
            return View(od);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveOrUpdateTodoStatus([Bind("ID,Name")] TodoStatus toDo)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                toDo.ModifiedId = userId;
                toDo.Active = true;
                if (toDo.ID == 0)
                {
                    toDo.InsertedId = userId;
                    toDo.Status = EntityStatus.ACTIVE;
                    todoRepository.CreateTodoStatus(toDo);
                }
                else
                {
                    todoRepository.UpdateTodoStatus(toDo);
                }

                return RedirectToAction(nameof(TodoStatus)).WithSuccess("success", "Saved successfully");
            }
            return View(todoRepository).WithDanger("error", "Not Saved");
        }
        //Delete ToDoStatus

        // POST: ToDoStatus/Delete/5
        [ActionName("DeleteTodoStatus")]
        public async Task<IActionResult> TodoStatusDeleteConfirmed(long? id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            //if (commonMethods.checkaccessavailable("Country", accessId, "Delete", "Master", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            if (TodoStatusExists(id.Value) == true)
            {
                var country = todoRepository.DeleteTodoStatus(id.Value);
            }
            return RedirectToAction(nameof(TodoStatus)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool TodoStatusExists(long id)
        {
            if (todoRepository.GetTodoStatusById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region TodoPriority
        public IActionResult TodoPriority()
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("Todo Priority", accessId, "List", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            return View(todoRepository.GetAllTodoPriority());
        }
        public IActionResult CreateOrEditTodoPriority(long? id)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            CommonMasterModel od = new CommonMasterModel();
            if (id == null)
            {
                if (Common.checkaccessavailable("Todo Priority", accessId, "Create", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = ViewMethod.CREATE;
                od.ID = 0;
                od.Name = "";
            }
            else
            {
                if (Common.checkaccessavailable("Todo Priority", accessId, "Edit", "Master", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.status = ViewMethod.UPDATE;
                var todoPriority = todoRepository.GetTodoPriorityById(id.Value);
                od.ID = todoPriority.ID;
                od.Name = todoPriority.Name;
            }
            return View(od);
        }
        public IActionResult DeleteTodo(long? id, string act)
        {
            var todo = todoRepository.GetAllTodo();
            var gettodobyid = todo.Where(a => a.ID == id).FirstOrDefault();
            if (gettodobyid.ParentId == 0)
            {
                var childs = todo.Where(a => a.ParentId == id).ToList();
                if (childs != null)
                {
                    foreach (var m in childs)
                    {
                        todoRepository.DeleteTodo(m.ID);
                    }
                }
            }
            todoRepository.DeleteTodo(gettodobyid.ID);
            return RedirectToAction(act, "Todo");
        }
        public async Task<IActionResult> SaveOrUpdateTodoPriority([Bind("ID,Name")] TodoPriority toDoPriority)
        {

            userId = HttpContext.Session.GetInt32("userId").Value;
            if (ModelState.IsValid)
            {
                toDoPriority.ModifiedId = userId;
                toDoPriority.Active = true;
                if (toDoPriority.ID == 0)
                {
                    toDoPriority.InsertedId = userId;
                    toDoPriority.Status = EntityStatus.ACTIVE;
                    todoRepository.CreateTodoPriority(toDoPriority);
                }
                else
                {
                    todoRepository.UpdateTodoPriority(toDoPriority);
                }

                return RedirectToAction(nameof(TodoPriority)).WithSuccess("success", "Saved successfully"); ;
            }
            return View(todoRepository).WithDanger("error", "Not Saved"); ;
        }


        //Delete ToDoPriority

        // POST: ToDoPriority/Delete/5
        [ActionName("DeleteTodoPriority")]
        public async Task<IActionResult> TodoPriorityDeleteConfirmed(long? id)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (Common.checkaccessavailable("Todo Priority", accessId, "Delete", "Master", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            if (TodoPriorityExists(id.Value) == true)
            {
                var country = todoRepository.DeleteTodoPriority(id.Value);
            }
            return RedirectToAction(nameof(TodoPriority)).WithSuccess("success", "Deleted successfully"); ;
        }

        private bool TodoPriorityExists(long id)
        {
            if (todoRepository.GetTodoPriorityById(id) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion


        public async Task<JsonResult> GetEmployeeWorkingDepartmentsByEmployeeId(long employeeId)
        {
            var employeeDeptList = (from a in employeeDesignationRepository.GetAllEmployeeDesignations().
                                    Where(a => a.EmployeeID == employeeId && a.Active == true)
                                    join b in designationRepository.GetAllDesignation().Where(b => b.Active == true)
                                    on a.DesignationID equals b.ID
                                    join c in departmentRepository.GetAllDepartment().Where(b => b.Active == true)
                                   on b.DepartmentID equals c.ID
                                    select new EmployeeDepartmentNameViewModel
                                    {

                                        DepartmentId = c.ID,
                                        DesignationId = b.ID,
                                        DepartmentName = c.Name,

                                        DesignationName = b.Name
                                    }).ToList();

            return Json(employeeDeptList);
        }
    }


}