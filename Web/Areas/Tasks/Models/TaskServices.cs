﻿using Microsoft.Extensions.Hosting;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Tasks.Models
{
   public class TaskServices
    {
        private TodoMethod commonMethods;
        private TodoSmsnMails smssend;
        private CommonMethods common;
        public TaskServices(TodoMethod todoMethod, TodoSmsnMails sms, CommonMethods commonmeth)
        {
            commonMethods = todoMethod;
            smssend = sms;
            common = commonmeth;
        }
        #region Task
        public void sendtaskdeadlinemailbefore24hr()
        {
            try
            {
                //deadline warning
                var stas = commonMethods.GetallTasksFormail();
                var alltasks = stas.Where(a => a.DueDt != null).ToList();
                var tasks = alltasks.Where(a => a.DueDt.Value.Date.Subtract(DateTime.Now.Date).Days == 1 && a.status != "COMPLETED" && a.status != "VERIFY" && a.status != "ON-HOLD").ToList();
                foreach (var a in tasks)
                {
                    smssend.taskmail("The deadline for the below mentioned task will complete Tomorrow. <br/>Task Name : " + a.todoname + "<br/>Status : " + a.status + "<br/>Priority : " + a.priority + "<br/>Description : " + a.tododescription + "<br/> Kindly see to it and work on it as soon as possible.", "Deadline Warning-" + a.todoname, a.profile);
                }

                //Every day deadline missed mail
                var dtasks = alltasks.Where(a => a.DueDt.Value.Date.Subtract(DateTime.Now.Date).Days < 0 && a.status != "COMPLETED" && a.status != "VERIFY" && a.status != "ON-HOLD").ToList();
                foreach (var a in tasks)
                {
                    smssend.taskmail("The deadline for the below mentioned task has been missed- <br/>Task Name : " + a.todoname + "<br/>Status : " + a.status + "<br/>Priority : " + a.priority + "<br/>Description : " + a.tododescription + "<br/>We are sure, there must be some genuine reason behind it. Kindly see to it and work on it as soon as possible.", "Deadline Missed-" + a.todoname, a.profile);
                }

                //Verify remainder
                var comptasks = stas.Where(a => a.completionDate != null).ToList();
                var vtasks = comptasks.Where(a => a.completionDate.Value.Date.Subtract(DateTime.Now.Date).Days < 0 && a.status == "COMPLETED").ToList();
                foreach (var a in tasks)
                {
                    smssend.taskmail("The task assigned by you to " + a.assignedto + ", has been marked completed.<br/>Task Name : " + a.todoname + "<br/>Priority : " + a.priority + "<br/>Status : COMPLETED<br/>Description : " + a.tododescription + "<br/><br/>Kindly verify the same and mark it complete from your end.", "Task Completed-" + a.todoname + "-" + (a.assignedby), a.empemail);
                }
            }
            catch { }
        }
        public void sendtaskdeadlinemailbefore4hr()
        {
            try
            {
                var alltasks = commonMethods.GetallTasksFormail().Where(a => a.DueDt != null).ToList();
                var tasks = alltasks.Where(a => a.DueDt.Value.Date.Subtract(DateTime.Now.Date).Days == 0 && a.status != "COMPLETED" && a.status != "VERIFY" && a.status != "ON-HOLD").ToList();
                foreach (var a in tasks)
                {
                    smssend.taskmail("The deadline for the below mentioned task will complete Today. <br/>Task Name : " + a.todoname + "<br/>Status : " + a.status + "<br/>Priority : " + a.priority + "<br/>Description : " + a.tododescription + "<br/> Kindly see to it and work on it as soon as possible.", "Deadline Warning-" + a.todoname, a.profile);
                }
            }
            catch { }
        }
        public void sendmailtaskdeadlines()
        {
            try
            {
                var tasks = commonMethods.GetallTasksFormail();
                foreach (var a in tasks)
                {
                    if (a.DueDt != null)
                    {
                        if (a.DueDt < DateTime.Now)
                        {
                            smssend.taskmail("The deadline for the below mentioned task has been missed- <br/>Task Name : " + a.todoname + "<br/>Status : " + a.status + "<br/>Priority : " + a.priority + "<br/>Description : " + a.tododescription + "<br/>We are sure, there must be some genuine reason behind it. Kindly see to it and work on it as soon as possible.", "Deadline Missed-" + a.todoname, a.profile);
                        }
                    }
                }
                List<subtasklist_cls> res = (from a in tasks
                                             where a.AssignToID != a.AssignByID && a.status == "COMPLETED"
                                             select a).ToList();
                foreach (var a in res)
                {
                    smssend.taskmail("The verification for the below mentioned task has been pending- <br/>Task Name : " + a.todoname + "<br/>Status : " + a.status + "<br/>Priority : " + a.priority + "<br/>Description : " + a.tododescription + "<br/>We are sure, there must be some genuine reason behind it. Kindly see to it and work on it as soon as possible.", "Verify Task-" + a.todoname, a.profile);
                }
            }
            catch
            {

            }
        }
        public void Sendweeklymailtaskreport()
        {
            try
            {
                DateTime givenDate = DateTime.Today;
                DateTime startOfWeek = givenDate.AddDays(-(int)givenDate.DayOfWeek);
                DateTime endOfWeek = startOfWeek.AddDays(7);
                var tasks = commonMethods.GetallTasksFormail().Where(a => a.AssignDate >= startOfWeek && a.AssignDate <= endOfWeek).ToList();
                var employees = common.GetAllEmployees();
                foreach (var m in employees)
                {
                    if (m.EmailId != null)
                    {
                        var userId = m.ID;
                        TaskDashboardcls taskDashboardcls = new TaskDashboardcls();
                        taskDashboardcls.TotalMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0)).ToList().Count();
                        taskDashboardcls.PendingMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "VERIFY").ToList().Count();


                        taskDashboardcls.TotalTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId).ToList().Count();
                        taskDashboardcls.PendingTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "VERIFY").ToList().Count();


                        taskDashboardcls.TotalTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId).ToList().Count();
                        taskDashboardcls.PendingTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "VERIFY").ToList().Count();
                        smssend.weeklytaskmail("Weekly Task Report", m.EmailId, taskDashboardcls);
                    }
                }
            }
            catch
            {

            }
        }
        public void Sendmonthlymailtaskreport()
        {
            try
            {
                DateTime givenDate = DateTime.Today;
                DateTime startOfWeek = new DateTime(givenDate.Year, givenDate.Month, 1);
                DateTime endOfWeek = startOfWeek.AddMonths(1).AddDays(-1);
                var tasks = commonMethods.GetallTasksFormail().Where(a => a.AssignDate >= startOfWeek && a.AssignDate <= endOfWeek).ToList();
                var employees = common.GetAllEmployees();
                foreach (var m in employees)
                {
                    if (m.EmailId != null)
                    {
                        var userId = m.ID;
                        TaskDashboardcls taskDashboardcls = new TaskDashboardcls();
                        taskDashboardcls.TotalMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0)).ToList().Count();
                        taskDashboardcls.PendingMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedMyTasks = tasks.Where(a => a.AssignByID == userId && (a.AssignToID == userId || a.AssignToID == 0) && a.status == "VERIFY").ToList().Count();


                        taskDashboardcls.TotalTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId).ToList().Count();
                        taskDashboardcls.PendingTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedTaskother = tasks.Where(a => a.AssignToID != userId && a.AssignToID != 0 && a.AssignByID == userId && a.status == "VERIFY").ToList().Count();


                        taskDashboardcls.TotalTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId).ToList().Count();
                        taskDashboardcls.PendingTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "PENDING").ToList().Count();
                        taskDashboardcls.onholdTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "ON-HOLD").ToList().Count();
                        taskDashboardcls.inprogressTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "IN-PROGRESS").ToList().Count();
                        taskDashboardcls.CompletedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "COMPLETED").ToList().Count();
                        taskDashboardcls.VerifiedTasktome = tasks.Where(a => a.AssignToID == userId && a.AssignToID != 0 && a.AssignByID != userId && a.status == "VERIFY").ToList().Count();
                        smssend.weeklytaskmail("Weekly Task Report", m.EmailId, taskDashboardcls);
                    }
                }
            }
            catch
            {

            }
        }
        #endregion
    }
}

