﻿using Microsoft.AspNetCore.Hosting;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Tasks.Models
{
   
    public class TodoSmsnMails
    {
        private smssend smssend { get; set; }
        private readonly IHostingEnvironment hostingEnvironment;
        public TodoSmsnMails(smssend sms, IHostingEnvironment hostingEnv)
        {
            smssend = sms;
            hostingEnvironment = hostingEnv;
        }
        public string taskmail(string desc, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "tasktemplate.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Description}}", desc).Replace("{{subject}}", subject);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string TaskVerifymail(string desc, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "tasktemplate.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Description}}", desc).Replace("{{subject}}", subject);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string InprogressTaskmail(string id, string desc, string empname, string userid, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "inprogessTask.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{id}}", id).Replace("{{desc}}", desc).Replace("{{empname}}", empname).Replace("{{userid}}", userid);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string OnHoldTaskmail(string id, string desc, string empname, string userid, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "onholdTask.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{id}}", id).Replace("{{desc}}", desc).Replace("{{empname}}", empname).Replace("{{userid}}", userid);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string weeklytaskmail(string subject, string sender, TaskDashboardcls task)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "weeklytaskreport.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{subject}}", subject)
                    .Replace("{{totalmycount}}", task.TotalMyTasks.ToString())
                    .Replace("{{totalmypending}}", task.PendingMyTasks.ToString())
                    .Replace("{{totalmyonhold}}", task.onholdMyTasks.ToString())
                    .Replace("{{totalmyinprogress}}", task.inprogressMyTasks.ToString())
                    .Replace("{{percentmypending}}", (task.TotalMyTasks == 0 ? 0 : ((task.PendingMyTasks * 100) / task.TotalMyTasks)) + "%")
                    .Replace("{{percentmyonhold}}", (task.TotalMyTasks == 0 ? 0 : ((task.onholdMyTasks * 100) / task.TotalMyTasks)) + "%")
                    .Replace("{{percentmyinprogress}}", (task.TotalMyTasks == 0 ? 0 : ((task.inprogressMyTasks * 100) / task.TotalMyTasks)) + "%")
                    .Replace("{{percentmycompleted}}", (task.TotalMyTasks == 0 ? 0 : ((task.CompletedMyTasks * 100) / task.TotalMyTasks)) + "%")
                    .Replace("{{percentmyverified}}", (task.TotalMyTasks == 0 ? 0 : ((task.VerifiedMyTasks * 100) / task.TotalMyTasks)) + "%")
                    .Replace("{{totalmycompleted}}", task.CompletedMyTasks.ToString())
                    .Replace("{{totalmyverified}}", task.VerifiedMyTasks.ToString())

                     .Replace("{{totalothercount}}", task.TotalTaskother.ToString())
                    .Replace("{{totalotherpending}}", task.PendingTaskother.ToString())
                    .Replace("{{totalotheronhold}}", task.onholdTaskother.ToString())
                    .Replace("{{totalotherinprogress}}", task.inprogressTaskother.ToString())
                    .Replace("{{percentotherpending}}", (task.TotalTaskother == 0 ? 0 : ((task.PendingTaskother * 100) / task.TotalTaskother)) + "%")
                    .Replace("{{percentotheronhold}}", (task.TotalTaskother == 0 ? 0 : ((task.onholdTaskother * 100) / task.TotalTaskother)) + "%")
                    .Replace("{{percentotherinprogress}}", (task.TotalTaskother == 0 ? 0 : ((task.inprogressTaskother * 100) / task.TotalTaskother)) + "%")
                    .Replace("{{percentothercompleted}}", (task.TotalTaskother == 0 ? 0 : ((task.CompletedTaskother * 100) / task.TotalTaskother)) + "%")
                    .Replace("{{percentotherverified}}", (task.TotalTaskother == 0 ? 0 : ((task.VerifiedTaskother * 100) / task.TotalTaskother)) + "%")
                    .Replace("{{totalothercompleted}}", task.CompletedTaskother.ToString())
                    .Replace("{{totalotherverified}}", task.VerifiedTaskother.ToString())

                    .Replace("{{totalmecount}}", task.TotalTasktome.ToString())
                    .Replace("{{totalmepending}}", task.PendingTasktome.ToString())
                    .Replace("{{totalmeonhold}}", task.onholdTasktome.ToString())
                    .Replace("{{totalmeinprogress}}", task.inprogressTasktome.ToString())
                    .Replace("{{percentmepending}}", (task.TotalTasktome == 0 ? 0 : ((task.PendingTasktome * 100) / task.TotalTasktome)) + "%")
                    .Replace("{{percentmeonhold}}", (task.TotalTasktome == 0 ? 0 : ((task.onholdTasktome * 100) / task.TotalTasktome)) + "%")
                    .Replace("{{percentmeinprogress}}", (task.TotalTasktome == 0 ? 0 : ((task.inprogressTasktome * 100) / task.TotalTasktome)) + "%")
                    .Replace("{{percentmecompleted}}", (task.TotalTasktome == 0 ? 0 : ((task.CompletedTasktome * 100) / task.TotalTasktome)) + "%")
                    .Replace("{{percentmeverified}}", (task.TotalTasktome == 0 ? 0 : ((task.VerifiedTasktome * 100) / task.TotalTasktome)) + "%")
                    .Replace("{{totalmecompleted}}", task.CompletedTasktome.ToString())
                    .Replace("{{totalmeverified}}", task.VerifiedTasktome.ToString());
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string PaymentRemindermail(string subject,string email,string studenName,string parentName,DateTime fromDate, DateTime toDate)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template"
                            + Path.DirectorySeparatorChar.ToString()
                            + "paymentReminderTemplate.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{name}}", parentName).Replace("{{studentName}}", studenName).Replace("{{startDate}}", fromDate.ToString("dd/MM/yyyy")).Replace("{{endDate}}", toDate.ToString("dd/MM/yyyy"));
                smssend.SendHtmlFormattedEmail(email, subject, body);
            }

            return body;
        }
            //"Payment Reminder", alldata[i].parentDetails[j].multipleparent[k].EmailId, alldata[i].parentDetails[j].studentname, alldata[i].parentDetails[j].multipleparent[k].name,alldata[i].FromDate,alldata[i].ToDate

    }
}
