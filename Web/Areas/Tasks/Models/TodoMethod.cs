﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.TodoAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Web.Models;

namespace OdmErp.Web.Areas.Tasks.Models
{
    public class TodoMethod
    {
        private CommonMethods common { get; set; }
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IDesignationRepository designationRepository;
        private IEmployeeRepository employeeRepository;
        private IToDoRepository todoRepository;
        private IDepartmentRepository departmentRepository;
        public TodoMethod(CommonMethods methods, IEmployeeDesignationRepository employeeDesignationRepo,
            IDesignationRepository designationRepo, IEmployeeRepository employeeRepo,
            IToDoRepository todoRepo, IDepartmentRepository departmentRepo)
        {
            common = methods;
            employeeDesignationRepository = employeeDesignationRepo;
            designationRepository = designationRepo;
            employeeRepository = employeeRepo;
            todoRepository = todoRepo;
            departmentRepository = departmentRepo;
        }
        

        #region Task Management
        public List<commonCls> GetEmployeesForTasks(long id, long[] deptid)
        {
            var allempdept = common.GetDepartmentsByEmployeeID(id);
            List<CommonMasterModel> mast = new List<CommonMasterModel>();
            foreach (var a in allempdept)
            {
                var departments = GetallchilddepartmentbasedonParent(a.id);
                mast.AddRange(departments);
            }
            var det = mast.Distinct();
            if (deptid.ToList().Count > 0)
            {
                det = common.GetDepartments();
            }
            var empdesg = employeeDesignationRepository.GetAllEmployeeDesignations();
            var desg = designationRepository.GetAllDesignation();
            var emp = employeeRepository.GetAllEmployee();

            var res = (from a in emp
                       join b in empdesg on a.ID equals b.EmployeeID
                       join c in desg on b.DesignationID equals c.ID
                       join d in det on c.DepartmentID equals d.ID
                       group a by a.ID into g
                       select new commonCls
                       {
                           id = g.FirstOrDefault().ID,
                           name = g.FirstOrDefault().FirstName + " " + g.FirstOrDefault().LastName,
                           profile = g.FirstOrDefault().Image
                       }).ToList().Distinct().ToList();

            return res;
        }
        public List<subtasklist_cls> GetAllMyParentTask(long employeeid)
        {
            try
            {
                var employees = common.GetEmployeesForTasks(employeeid);
                var todo = todoRepository.GetAllTodo() == null ? null : todoRepository.GetAllTodo().Where(a => a.AssignedId == employeeid && a.EmployeeID == employeeid).ToList();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = common.GetDepartments();
                if (todo != null)
                {
                    var res = (from a in todo
                               join b in employees on a.AssignedId equals b.id
                               join e in employees on a.EmployeeID equals e.id
                               join c in priority on a.TodoPriorityID equals c.ID
                               join d in departments on a.DepartmentID equals d.ID
                               select new subtasklist_cls
                               {
                                   todoid = a.ID,
                                   todoname = a.Name,
                                   status = a.Status,
                                   AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                                   tododescription = a.Description,
                                   DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                                   assignedto = b.name,
                                   assignedby = e.name,
                                   profile = b.profile,
                                   priority = c.Name,
                                   department = d.Name,
                                   assigntime = a.AssignedDate.Value.ToString("hh:mm tt")
                               }).ToList();
                    return res;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e1)
            {

                throw;
            }
        }
        public List<commonCls> getassigntoemployeelist(long empid)
        {
            var tasks = GetallTasks(empid);
            var employees = employeeRepository.GetAllEmployee();
            if (tasks != null)
            {
                List<commonCls> res = (from a in tasks
                                       where a.AssignToID != empid && a.AssignToID != 0 && a.AssignByID == empid
                                       group a by a.AssignToID into g
                                       select new commonCls
                                       {
                                           id = g.Key,
                                           name = employees.Where(m => m.ID == g.Key).Select(b => (b.FirstName + " " + b.LastName)).FirstOrDefault()
                                       }).Distinct().ToList();
                return res;
            }
            else
            {
                return null;
            }
        }
        public Mytaskscls GetAssignToOthers(long employeeid, long[] employees, long[] TodoPriority, string fromdate, string todate)
        {
            try
            {
                Mytaskscls mytaskscls = new Mytaskscls();
                var tasks = GetallTasks(employeeid).OrderByDescending(a => a.todoid).ToList();

                if (tasks != null)
                {
                    List<subtasklist_cls> res = (from a in tasks
                                                 where a.AssignToID != employeeid && a.AssignToID != 0 && a.AssignByID == employeeid
                                                 select a).ToList();
                    if (fromdate != null)
                    {
                        DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                        DateTime ToDate = System.DateTime.Now;
                        if (todate != null)
                        {
                            ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                        }
                        if (fromdate != null && todate != null)
                        {
                            res = res.Where(a => a.AssignDate >= FromDate && a.AssignDate <= ToDate).ToList();
                        }
                        if (fromdate != null && todate == null)
                        {
                            res = res.Where(a => a.AssignDate >= FromDate).ToList();
                        }
                    }
                    if (employees.ToList().Count > 0)
                    {
                        res = res.Where(a => employees.Contains(a.AssignToID)).ToList();
                    }
                    if (TodoPriority.ToList().Count > 0)
                    {
                        res = res.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                    }
                    mytaskscls.PendingList = res.Where(a => a.status == "PENDING").ToList();
                    mytaskscls.OnholdList = res.Where(a => a.status == "ON-HOLD").ToList();
                    mytaskscls.InprogressList = res.Where(a => a.status == "IN-PROGRESS").ToList();
                    mytaskscls.CompletedList = res.Where(a => a.status == "COMPLETED").ToList();
                    mytaskscls.VerifiedList = res.Where(a => a.status == "VERIFY").ToList();


                    //if (TodoPriority.ToList().Count > 0)
                    //{
                    //    mytaskscls.PendingList = mytaskscls.PendingList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                    //    mytaskscls.OnholdList = mytaskscls.OnholdList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                    //    mytaskscls.InprogressList = mytaskscls.InprogressList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                    //    mytaskscls.CompletedList = mytaskscls.CompletedList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                    //    mytaskscls.VerifiedList = mytaskscls.VerifiedList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                    //    //  tasks = tasks.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                    //}


                    List<subtasklist_cls> PendingListparents = mytaskscls.PendingList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> OnholdListparents = mytaskscls.OnholdList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> InprogressListparents = mytaskscls.InprogressList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> CompletedListparents = mytaskscls.CompletedList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> VerifiedListparents = mytaskscls.VerifiedList.Where(a => a.parentID == 0).ToList();
                    if (PendingListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in PendingListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.PendingList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.PendingList.Remove(t);
                                }
                            }
                        }
                    }
                    if (OnholdListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in OnholdListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.OnholdList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.OnholdList.Remove(t);
                                }
                            }
                        }
                    }
                    if (InprogressListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in InprogressListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.InprogressList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.InprogressList.Remove(t);
                                }
                            }
                        }
                    }
                    if (CompletedListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in CompletedListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.CompletedList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.CompletedList.Remove(t);
                                }
                            }
                        }
                    }
                    if (VerifiedListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in VerifiedListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.VerifiedList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.VerifiedList.Remove(t);
                                }
                            }
                        }
                    }
                    //if (mytaskscls.PendingList.Count > 0)
                    //{
                    //foreach (subtasklist_cls a in res.Where(m => m.subtasks == null).ToList())
                    //{

                    //}
                    //}
                    return mytaskscls;
                }
                else
                {
                    return null;
                }







                //var tasks = GetallTasks(employeeid);
                //if (TodoStatusName.ToList().Count > 0)
                //{
                //    tasks = tasks.Where(a => TodoStatusName.Contains(a.status)).ToList();
                //}
                //if (TodoPriority.ToList().Count > 0)
                //{
                //    tasks = tasks.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                //}
                //if (tasks != null)
                //{
                //    List<subtasklist_cls> res = (from a in tasks
                //                                 where a.AssignToID != employeeid && a.AssignToID != 0 && a.AssignByID == employeeid
                //                                 select a).ToList();
                //    List<subtasklist_cls> parents = res.Where(a => a.parentID == 0).ToList();
                //    if (parents.Count() > 0)
                //    {
                //        foreach (subtasklist_cls pa in parents.ToList())
                //        {
                //            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                //            var ch = res.Where(a => a.parentID == pa.todoid).ToList();
                //            if (ch.Count > 0)
                //            {
                //                foreach (var t in ch)
                //                {
                //                    res.Remove(t);
                //                }
                //            }
                //        }
                //    }
                //    if (res.Count > 0)
                //    {
                //        //foreach (subtasklist_cls a in res.Where(m => m.subtasks == null).ToList())
                //        //{
                //        //    if (TodoStatusName != null)
                //        //    {
                //        //        if (a.status != TodoStatusName)
                //        //        {
                //        //            res.Remove(a);
                //        //        }
                //        //    }
                //        //    if (TodoPriority != null)
                //        //    {
                //        //        if (a.priorityID != TodoPriority)
                //        //        {
                //        //            res.Remove(a);
                //        //        }
                //        //    }
                //        //}
                //    }
                //    return res;
                //}
                //else
                //{
                //    return null;
                //}
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public Mytaskscls GetAssignToMe(long employeeid, string[] TodoStatusName, long[] TodoPriority, string fromdate, string todate)
        {
            try
            {
                Mytaskscls mytaskscls = new Mytaskscls();
                var tasks = GetallTasks(employeeid).OrderByDescending(a => a.todoid).ToList();

                if (tasks != null)
                {
                    List<subtasklist_cls> res = (from a in tasks
                                                 where a.AssignToID == employeeid && a.AssignToID != 0 && a.AssignByID != employeeid
                                                 select a).ToList();

                    if (fromdate != null)
                    {
                        DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                        DateTime ToDate = System.DateTime.Now;
                        if (todate != null)
                        {
                            ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                        }
                        if (fromdate != null && todate != null)
                        {
                            res = res.Where(a => a.AssignDate >= FromDate && a.AssignDate <= ToDate).ToList();
                        }
                        if (fromdate != null && todate == null)
                        {
                            res = res.Where(a => a.AssignDate >= FromDate).ToList();
                        }
                    }
                    mytaskscls.PendingList = res.Where(a => a.status == "PENDING").ToList();
                    mytaskscls.OnholdList = res.Where(a => a.status == "ON-HOLD").ToList();
                    mytaskscls.InprogressList = res.Where(a => a.status == "IN-PROGRESS").ToList();
                    mytaskscls.CompletedList = res.Where(a => a.status == "COMPLETED").ToList();
                    mytaskscls.VerifiedList = res.Where(a => a.status == "VERIFY").ToList();
                    //if (TodoStatusName.ToList().Count > 0)
                    //{
                    //    tasks = tasks.Where(a => TodoStatusName.Contains(a.status)).ToList();
                    //}
                    if (TodoPriority.ToList().Count > 0)
                    {
                        mytaskscls.PendingList = mytaskscls.PendingList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                        mytaskscls.OnholdList = mytaskscls.OnholdList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                        mytaskscls.InprogressList = mytaskscls.InprogressList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                        mytaskscls.CompletedList = mytaskscls.CompletedList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                        mytaskscls.VerifiedList = mytaskscls.VerifiedList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                        //  tasks = tasks.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                    }


                    List<subtasklist_cls> PendingListparents = mytaskscls.PendingList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> OnholdListparents = mytaskscls.OnholdList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> InprogressListparents = mytaskscls.InprogressList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> CompletedListparents = mytaskscls.CompletedList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> VerifyListparents = mytaskscls.VerifiedList.Where(a => a.parentID == 0).ToList();
                    if (PendingListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in PendingListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.PendingList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.PendingList.Remove(t);
                                }
                            }
                        }
                    }
                    if (OnholdListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in OnholdListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.OnholdList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.OnholdList.Remove(t);
                                }
                            }
                        }
                    }
                    if (InprogressListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in InprogressListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.InprogressList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.InprogressList.Remove(t);
                                }
                            }
                        }
                    }
                    if (CompletedListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in CompletedListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.CompletedList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.CompletedList.Remove(t);
                                }
                            }
                        }
                    }
                    if (VerifyListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in VerifyListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.VerifiedList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.VerifiedList.Remove(t);
                                }
                            }
                        }
                    }
                    //if (mytaskscls.PendingList.Count > 0)
                    //{
                    //foreach (subtasklist_cls a in res.Where(m => m.subtasks == null).ToList())
                    //{

                    //}
                    //}
                    return mytaskscls;
                }
                else
                {
                    return null;
                }







                //var tasks = GetallTasks(employeeid);
                ////if (TodoStatusName.ToList().Count > 0)
                ////{
                ////    tasks = tasks.Where(a => TodoStatusName.Contains(a.status)).ToList();
                ////}
                ////if (TodoPriority.ToList().Count > 0)
                ////{
                ////    tasks = tasks.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                ////}
                //if (tasks != null)
                //{
                //    List<subtasklist_cls> res = (from a in tasks
                //                                 where a.AssignToID == employeeid && a.AssignToID != 0 && a.AssignByID != employeeid
                //                                 select a).ToList();
                //    List<subtasklist_cls> parents = res.Where(a => a.parentID == 0).ToList();
                //    if (parents.Count() > 0)
                //    {
                //        foreach (subtasklist_cls pa in parents.ToList())
                //        {
                //            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                //            var ch = res.Where(a => a.parentID == pa.todoid).ToList();
                //            if (ch.Count > 0)
                //            {
                //                foreach (var t in ch)
                //                {
                //                    res.Remove(t);
                //                }
                //            }
                //        }
                //    }

                //    if (res.Count > 0)
                //    {
                //        //foreach (subtasklist_cls a in res.Where(m => m.subtasks == null).ToList())
                //        //{

                //        //}
                //    }
                //    return res;
                //}
                //else
                //{
                //    return null;
                //}
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<subtasklist_cls> GetallTasks(long employeeid)
        {
            try
            {
                var employees = common.GetEmployeesForTaskswithleft(employeeid);
                var todo = todoRepository.GetAllTodo();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = common.GetDepartments();
                var comments = todoRepository.GetAllTodoComment();
                var res = (from a in todo
                               //join e in employees on a.EmployeeID equals e.id
                               //join c in priority on a.TodoPriorityID equals c.ID
                           select new subtasklist_cls
                           {
                               todoid = a.ID,
                               todoname = a.Name,
                               status = a.Status,
                               AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                               tododescription = a.Description,
                               DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                               AssignByDepartmentID = a.AssignByDepartmentID,
                               AssignByDepartmentName = (a.AssignByDepartmentID == 0) ? "" : departments.Where(m => m.ID == a.AssignByDepartmentID).FirstOrDefault().Name,
                               assignedto = a.AssignedId != 0 ? employees.Where(m => m.id == a.AssignedId).FirstOrDefault().name : "",
                               assignedby = a.EmployeeID == 0 ? "" : employees.Where(m => m.id == a.EmployeeID).FirstOrDefault().name,
                               profile = a.AssignedId != 0 ? employees.Where(m => m.id == a.AssignedId).FirstOrDefault().profile : "",
                               priority = a.TodoPriorityID == 0 ? "" : priority.Where(m => m.ID == a.TodoPriorityID).FirstOrDefault().Name,
                               department = (a.DepartmentID == 0) ? "" : departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name,
                               assigntime = a.AssignedDate.Value.ToString("hh:mm tt"),
                               AssignByID = a.EmployeeID,
                               AssignDate = a.AssignedDate,
                               AssignToID = a.AssignedId,
                               DueDt = a.DueDate,
                               ModifiedDate = a.ModifiedDate,
                               parentID = a.ParentId,
                               DepartmentID = a.DepartmentID,
                               priorityID = a.TodoPriorityID,
                               completionDt = a.CompletionDate != null ? a.CompletionDate.Value.ToString("dd-MMM-yyyy") : "",
                               VerifyDt = a.VerifiedDate != null ? a.VerifiedDate.Value.ToString("dd-MMM-yyyy") : "",
                               commentcount = comments != null ? comments.Where(m => m.TodoID == a.ID && m.InsertedId != employeeid && m.IsRead == false).ToList().Count() : 0,
                               score = a.Score,
                               InsertedDate = a.InsertedDate == null ? "" : a.InsertedDate.ToString("dd-MMM-yyyy")


                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<Cls_TodoTimelines> GetTaskTimelines(long id)
        {
            var timelines = todoRepository.GetAllTodoTimeline().Where(a => a.TodoID == id).ToList();
            var employees = employeeRepository.GetAllEmployee();
            var res = (from a in timelines
                       join b in employees on a.InsertedId equals b.ID
                       select new Cls_TodoTimelines
                       {
                           InsertedDate = a.InsertedDate,
                           RelatedDate = a.RelatedDate,
                           RelatedId = a.RelatedId,
                           Status = a.Status,
                           TodoStatus = a.TodoStatus,
                           RelatedName = a.RelatedId != 0 ? employees.Where(c => c.ID == a.RelatedId).Select(c => new { Name = c.FirstName + " " + c.LastName }).FirstOrDefault().Name : "",
                           insertedBy = b.FirstName + " " + b.LastName
                       }).ToList();
            return res;
        }
        public List<CommonMasterModel> GetallchilddepartmentbasedonParent(long deptID)
        {
            var dept = departmentRepository.GetAllDepartment();
            List<CommonMasterModel> comm = new List<CommonMasterModel>();
            Department comdept = dept.Where(a => a.ID == deptID).FirstOrDefault();
            List<Department> childs = new List<Department>();
            comm.Add(new CommonMasterModel { ID = comdept.ID, Name = comdept.Name });
            for (int i = 0; i < comm.Count(); i++)
            {
                childs = dept.Where(a => a.ParentID == comm.ToList()[i].ID).ToList();
                if (childs.Count > 0)
                {
                    foreach (var df in childs)
                    {
                        comm.Add(new CommonMasterModel { ID = df.ID, Name = df.Name });
                    }
                }
            }
            return comm;
        }
        public viewalltaskclass GetDepartmentLeadAllTask(long accessId, long roleId, long employeeid, string[] TodoStatusName, long[] TodoPriority, long[] emploid, long[] departmentid, long? datetype, DateTime? f_date, DateTime? t_date, string m_date)
        {

            IEnumerable<CommonMasterModel> det = new List<CommonMasterModel>();
            var alltasks = GetallTasks(employeeid).Where(a => a.DepartmentID != 0 && a.AssignByDepartmentID != 0).ToList();
            if (common.checkaccessavailable("View Tasks", accessId, "Management", "Task", roleId) == true)
            {
                det = common.GetDepartments();
            }
            else if (common.checkaccessavailable("View Tasks", accessId, "Departmentwise", "Task", roleId) == true)
            {
                var allempdept = common.GetDepartmentsByEmployeeID(employeeid);
                List<CommonMasterModel> mast = new List<CommonMasterModel>();
                foreach (var a in allempdept)
                {
                    var departments = GetallchilddepartmentbasedonParent(a.id);
                    mast.AddRange(departments);
                }
                det = mast.Distinct();
            }
            else
            {
                // emploid = employeeid;
                det = common.GetDepartments();
                alltasks = alltasks.Where(a => a.AssignByID == employeeid || a.AssignToID == employeeid).ToList();
            }




            //if (TodoStatusName.ToList().Count > 0)
            //{
            //    alltasks = alltasks.Where(a => TodoStatusName.Contains(a.status)).ToList();
            //}
            if (TodoPriority.ToList().Count > 0)
            {
                alltasks = alltasks.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
            }
            if (emploid.ToList().Count > 0)
            {
                alltasks = alltasks.Where(a => emploid.Contains(a.AssignToID) || emploid.Contains(a.AssignByID)).ToList();
            }

            if (departmentid.ToList().Count > 0)
            {
                alltasks = alltasks.Where(a => departmentid.Contains(a.AssignByDepartmentID) || departmentid.Contains(a.DepartmentID)).ToList();
            }

            if (f_date != null && t_date != null)
            {
                alltasks = alltasks.Where(a => a.AssignDate.Value.Date >= f_date.Value.Date && a.AssignDate.Value.Date <= t_date.Value.Date).ToList();
            }
            if (m_date != null && m_date != "")
            {
                DateTime m_dt = Convert.ToDateTime(m_date);
                alltasks = alltasks.Where(a => a.AssignDate.Value.Month == m_dt.Month && a.AssignDate.Value.Year == m_dt.Year).ToList();
            }
            var tasks1 = (from a in alltasks
                          join b in det on a.DepartmentID equals b.ID
                          select a).ToList();
            var tasks2 = (from a in alltasks
                          join b in det on a.AssignByDepartmentID equals b.ID
                          select a).ToList();
            var tasks = tasks1.Union(tasks2).Distinct();




            Mytaskscls mytaskscls = new Mytaskscls();
            viewalltaskclass viewalltaskclass = new viewalltaskclass();

            if (tasks != null)
            {
                List<subtasklist_cls> res = (from a in tasks
                                             select a).ToList();
                mytaskscls.PendingList = res.Where(a => a.status == "PENDING").ToList();
                mytaskscls.OnholdList = res.Where(a => a.status == "ON-HOLD").ToList();
                mytaskscls.InprogressList = res.Where(a => a.status == "IN-PROGRESS").ToList();
                mytaskscls.CompletedList = res.Where(a => a.status == "COMPLETED").ToList();
                mytaskscls.VerifiedList = res.Where(a => a.status == "VERIFY").ToList();

                List<subtasklist_cls> PendingListparents = mytaskscls.PendingList.Where(a => a.parentID == 0).ToList();
                List<subtasklist_cls> OnholdListparents = mytaskscls.OnholdList.Where(a => a.parentID == 0).ToList();
                List<subtasklist_cls> InprogressListparents = mytaskscls.InprogressList.Where(a => a.parentID == 0).ToList();
                List<subtasklist_cls> CompletedListparents = mytaskscls.CompletedList.Where(a => a.parentID == 0).ToList();
                List<subtasklist_cls> VerifiedListparents = mytaskscls.VerifiedList.Where(a => a.parentID == 0).ToList();

                if (PendingListparents.Count() > 0)
                {
                    foreach (subtasklist_cls pa in PendingListparents.ToList())
                    {
                        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                        var ch = mytaskscls.PendingList.Where(a => a.parentID == pa.todoid).ToList();
                        if (ch.Count > 0)
                        {
                            foreach (var t in ch)
                            {
                                mytaskscls.PendingList.Remove(t);
                            }
                        }
                    }
                }
                if (OnholdListparents.Count() > 0)
                {
                    foreach (subtasklist_cls pa in OnholdListparents.ToList())
                    {
                        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                        var ch = mytaskscls.OnholdList.Where(a => a.parentID == pa.todoid).ToList();
                        if (ch.Count > 0)
                        {
                            foreach (var t in ch)
                            {
                                mytaskscls.OnholdList.Remove(t);
                            }
                        }
                    }
                }
                if (InprogressListparents.Count() > 0)
                {
                    foreach (subtasklist_cls pa in InprogressListparents.ToList())
                    {
                        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                        var ch = mytaskscls.InprogressList.Where(a => a.parentID == pa.todoid).ToList();
                        if (ch.Count > 0)
                        {
                            foreach (var t in ch)
                            {
                                mytaskscls.InprogressList.Remove(t);
                            }
                        }
                    }
                }
                if (CompletedListparents.Count() > 0)
                {
                    foreach (subtasklist_cls pa in CompletedListparents.ToList())
                    {
                        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                        var ch = mytaskscls.CompletedList.Where(a => a.parentID == pa.todoid).ToList();
                        if (ch.Count > 0)
                        {
                            foreach (var t in ch)
                            {
                                mytaskscls.CompletedList.Remove(t);
                            }
                        }
                    }
                }
                if (VerifiedListparents.Count() > 0)
                {
                    foreach (subtasklist_cls pa in VerifiedListparents.ToList())
                    {
                        pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                        var ch = mytaskscls.VerifiedList.Where(a => a.parentID == pa.todoid).ToList();
                        if (ch.Count > 0)
                        {
                            foreach (var t in ch)
                            {
                                mytaskscls.VerifiedList.Remove(t);
                            }
                        }
                    }
                }
                //if (res.Count > 0)
                //{
                //    foreach (subtasklist_cls a in res.Where(m => m.subtasks == null).ToList())
                //    {

                //    }
                //}
                viewalltaskclass.tasks = mytaskscls;
                viewalltaskclass.totaltask = tasks.Count();
                viewalltaskclass.Completedtask = mytaskscls.CompletedList.Count();
                viewalltaskclass.inprogresstask = mytaskscls.InprogressList.Count();
                viewalltaskclass.onholdtask = mytaskscls.OnholdList.Count();
                viewalltaskclass.Pendingtask = mytaskscls.PendingList.Count();
                viewalltaskclass.Verifiedtask = mytaskscls.VerifiedList.Count();
                return viewalltaskclass;
            }
            else
            {
                viewalltaskclass = new viewalltaskclass();
                viewalltaskclass.tasks = null;
                viewalltaskclass.totaltask = 0;
                viewalltaskclass.Completedtask = 0;
                viewalltaskclass.inprogresstask = 0;
                viewalltaskclass.onholdtask = 0;
                viewalltaskclass.Pendingtask = 0;
                viewalltaskclass.Verifiedtask = 0;
                return viewalltaskclass;
            }
        }

        public List<subtasklist_cls> GetallTasksFormail()
        {
            try
            {
                var employees = employeeRepository.GetAllEmployeewithleft();
                var todo = todoRepository.GetAllTodo();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = common.GetDepartments();
                var res = (from a in todo
                           join e in employees on a.EmployeeID equals e.ID
                           join c in priority on a.TodoPriorityID equals c.ID
                           select new subtasklist_cls
                           {
                               todoid = a.ID,
                               todoname = a.Name,
                               status = a.Status,
                               AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                               tododescription = a.Description,
                               DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                               assignedto = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).Select(m => m.FirstName + " " + m.LastName).FirstOrDefault() : "",
                               assignedby = e.FirstName + " " + e.LastName,
                               profile = a.AssignedId != 0 ? employees.Where(m => m.ID == a.AssignedId).FirstOrDefault().EmailId : "",
                               priority = c.Name,
                               department = a.DepartmentID != 0 ? departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name : "",
                               assigntime = a.AssignedDate.Value.ToString("hh:mm tt"),
                               AssignByID = a.EmployeeID,
                               AssignDate = a.AssignedDate,
                               AssignToID = a.AssignedId,
                               empemail = e.EmailId,
                               DueDt = a.DueDate,
                               ModifiedDate = a.ModifiedDate,
                               parentID = a.ParentId,
                               DepartmentID = a.DepartmentID,
                               priorityID = a.TodoPriorityID,
                               completionDate = a.CompletionDate
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        public Mytaskscls GetMyTasks(long employeeid, string[] TodoStatusName, long[] TodoPriority, string fromdate, string todate)
        {
            try
            {
                Mytaskscls mytaskscls = new Mytaskscls();


                //if (tasks != null)
                //{
                //    List<subtasklist_cls> parent = tasks.Where(a => a.AssignByID == employeeid && a.DepartmentID==0 && (a.AssignToID == employeeid || a.AssignToID == 0) && a.parentID == 0).ToList();
                //    foreach (subtasklist_cls a in parent.ToList())
                //    {
                //        a.subtasks = tasks.Where(m => m.parentID == a.todoid).ToList();                        
                //    }
                //    return parent;
                //}
                //return null;
                var tasks = GetallTasks(employeeid);

                if (tasks != null)
                {



                    List<subtasklist_cls> res = (from a in tasks
                                                 where a.AssignByID == employeeid && (a.AssignToID == employeeid || a.AssignToID == 0) && a.parentID == 0 && a.DepartmentID == 0
                                                 select a).ToList();
                    if (fromdate != null)
                    {
                        DateTime FromDate = Convert.ToDateTime(fromdate + " 00:00:00 AM");
                        DateTime ToDate = System.DateTime.Now;
                        if (todate != null)
                        {
                            ToDate = Convert.ToDateTime(todate + " 23:59:59 PM");
                        }
                        if (fromdate != null && todate != null)
                        {
                            res = res.Where(a => a.AssignDate >= FromDate && a.AssignDate <= ToDate).ToList();
                        }
                        if (fromdate != null && todate == null)
                        {
                            res = res.Where(a => a.AssignDate >= FromDate).ToList();
                        }
                    }

                    mytaskscls.PendingList = res.Where(a => a.status == "PENDING").ToList();
                    mytaskscls.OnholdList = res.Where(a => a.status == "ON-HOLD").ToList();
                    mytaskscls.InprogressList = res.Where(a => a.status == "IN-PROGRESS").ToList();
                    mytaskscls.CompletedList = res.Where(a => a.status == "COMPLETED").ToList();
                    //if (TodoStatusName.ToList().Count > 0)
                    //{
                    //    tasks = tasks.Where(a => TodoStatusName.Contains(a.status)).ToList();
                    //}
                    if (TodoPriority.ToList().Count > 0)
                    {
                        mytaskscls.PendingList = mytaskscls.PendingList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                        mytaskscls.OnholdList = mytaskscls.OnholdList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                        mytaskscls.InprogressList = mytaskscls.InprogressList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                        mytaskscls.CompletedList = mytaskscls.CompletedList.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                        //  tasks = tasks.Where(a => TodoPriority.Contains(a.priorityID)).ToList();
                    }


                    List<subtasklist_cls> PendingListparents = mytaskscls.PendingList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> OnholdListparents = mytaskscls.OnholdList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> InprogressListparents = mytaskscls.InprogressList.Where(a => a.parentID == 0).ToList();
                    List<subtasklist_cls> CompletedListparents = mytaskscls.CompletedList.Where(a => a.parentID == 0).ToList();
                    if (PendingListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in PendingListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.PendingList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.PendingList.Remove(t);
                                }
                            }
                        }
                    }
                    if (OnholdListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in OnholdListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.OnholdList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.OnholdList.Remove(t);
                                }
                            }
                        }
                    }
                    if (InprogressListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in InprogressListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.InprogressList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.InprogressList.Remove(t);
                                }
                            }
                        }
                    }
                    if (CompletedListparents.Count() > 0)
                    {
                        foreach (subtasklist_cls pa in CompletedListparents.ToList())
                        {
                            pa.subtasks = tasks.Where(m => m.parentID == pa.todoid).ToList();

                            var ch = mytaskscls.CompletedList.Where(a => a.parentID == pa.todoid).ToList();
                            if (ch.Count > 0)
                            {
                                foreach (var t in ch)
                                {
                                    mytaskscls.CompletedList.Remove(t);
                                }
                            }
                        }
                    }
                    //if (mytaskscls.PendingList.Count > 0)
                    //{
                    //foreach (subtasklist_cls a in res.Where(m => m.subtasks == null).ToList())
                    //{

                    //}
                    //}
                    return mytaskscls;
                }
                else
                {
                    return null;
                }





            }
            catch (Exception e1)
            {
                return null;
            }
        }
        public List<attachmentclass> GetAttachments(long id, long empid)
        {
            try
            {
                var attachments = todoRepository.GetAllTodoAttachment();
                var employees = common.GetEmployeesForTasks(empid);
                var res = (from a in attachments
                           join b in employees on a.InsertedId equals b.id
                           where a.TodoID == id
                           select new attachmentclass
                           {
                               AttachmentName = a.AttachmentName,
                               Attachmenturl = a.Attachmenturl,
                               employeeid = a.InsertedId,
                               employeeName = b.name,
                               TodoID = a.TodoID,
                               id = a.ID,
                               insertedOn = a.InsertedDate,
                               modifiedOn = a.ModifiedDate
                           }).ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }



        public List<todocomments> GetComments(long id, long empid)
        {
            try
            {
                var comments = todoRepository.GetAllTodoComment();
                var employees = common.GetEmployeesForTaskswithleft(empid);
                var res = (from a in comments
                           join b in employees on a.InsertedId equals b.id
                           where a.TodoID == id
                           select new todocomments
                           {
                               id = a.ID,
                               Description = a.Description,
                               EmployeeName = b.name,
                               EmployeeID = a.InsertedId,
                               TodoID = a.TodoID,
                               postedON = a.InsertedDate.ToString("dd-MMM-yyyy hh:mm aa"),
                               profile = b.profile,
                               insertedOn = a.InsertedDate,
                               modifiedOn = a.ModifiedDate
                           }).Distinct().ToList();
                return res;
            }
            catch (Exception e1)
            {
                return null;
            }
        }



        public List<subtasklist_cls> GetSubTaskDetails(long todoparentid, long employeeid)
        {
            try
            {
                var employees = common.GetEmployeesForTasks(employeeid);
                var todo = todoRepository.GetAllTodo() == null ? null : todoRepository.GetAllTodo().Where(a => a.ParentId == todoparentid).ToList();
                var priority = todoRepository.GetAllTodoPriority();
                var departments = common.GetDepartments();
                if (todo != null)
                {
                    var res = (from a in todo
                               join b in employees on a.AssignedId equals b.id
                               join e in employees on a.EmployeeID equals e.id
                               join c in priority on a.TodoPriorityID equals c.ID
                               // join d in departments on a.DepartmentID equals d.ID
                               select new subtasklist_cls
                               {
                                   todoid = a.ID,
                                   todoname = a.Name,
                                   status = a.Status,
                                   AssignedDate = a.AssignedDate.Value.ToString("dd-MMM-yyyy"),
                                   tododescription = a.Description,
                                   DueDate = a.DueDate == null ? "" : a.DueDate.Value.ToString("dd-MMM-yyyy"),
                                   assignedto = b.name,
                                   assignedby = e.name,
                                   profile = b.profile,
                                   priority = c.Name,
                                   department = a.DepartmentID == 0 ? "" : departments.Where(m => m.ID == a.DepartmentID).FirstOrDefault().Name
                               }).ToList();
                    return res;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e1)
            {

                throw;
            }
        }
        public IEnumerable<TodoPriority> Gettaskpriority()
        {
            try
            {
                var priority = todoRepository.GetAllTodoPriority();
                return priority;
            }
            catch (Exception e1)
            {
                return null;
            }
        }
        #endregion
    }
}
