﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Constants
{
    public static class HttpAddSmg
    {
        #region---------complited--------------
       
        public static string EXAM_DATE_SMG = "Hello {{StudentName}}, Your ODM Entrance Exam date has been published, Your Examination date is : {{ExamDate}}, Please click on this link to download your Admit Card : <ODM TRACQ MOBILE APP LINK";
        public static string BasicRegistration_NotPaid = "Hello {{StudentName}} , Congratulation, You have successfully registered to ODM Educational Group!.";
        // Download TracQ Mobile Application from Playstore for further Admission Details & Updates. Click on this {{1}} to download ODM TracQ Mobile App
        public static string BasicRegistration_Paid = "Hello {{StudentName}}, Congratulations, You have successfully applied for admission in ODM Educational Group! Please download ODM TracQ Mobile Application from Playstore for further Admission Details and Updates!!!";
        #endregion
        #region
        public static string EXAM_DATE_RESULT = "Hello {{StudentName}}, Thank you for appearing ODM Entrance Exam, We will update you once your result will be published!!! ";
        //<Click on this link to get updated with ODM Admission Proces>  (examresult link)
        public static string PIDate_Time = "Hello {{StudentName}}, You have been Shortlisted for ODM Personal Interview Test. Personal Interview Date={{PI Date}} and Time={{PI Time}}";
        //, For futher information click on this link <ODM TRACQ MOBILE APP LINK>

        public static string PI_QUALIFIED = "Hello {{StudentName}}, You have successfully qualified ODM Personal Interview Test. Now you can procced with Documment Verification Round.";
        public static string PI_NOTQUALIFIED = "Hello {{StudentName}}, We are sorry to inform you that you have not qualified ODM Personal Interview Test . And we really appreciaite your effort &  We would love to see you next year in ODM. Best Wishes!!! Regards ODM Educational Group!!!";
        public static string PI_NOTAPPEARED = "Hello {{StudentName}}, You have missed the PI, Please contact your respective counsellor {{CounsellorName}} .";

        public static string DOCDATE_TIME = "Hello {{StudentName}}, You have been Shortlisted for ODM Document Verification ,Document Verification Date={{Verification Date}} and Time={{Verification Time}}";
        public static string DOC_STATUS = "Hello {{StudentName}}, You have successfully qualified ODM Document Verification";
      
        public static string DOCVERIFICATION_COMPLETED = "Hello {{StudentName}}, Congratulation, Your Document Verification Round has been completed, ODM Educational Group welcomes you to ODM Family. Please pay Admission Payment in Account Department to CONFIRM your ADMISSION!!!";
        public static string DOCVERIFICATION_PARTIALCOMPLETED = "Hello {{StudentName}}, We have verified <Doc X, Doc, Y, Doc Z>, & these are the pending document pending for verification {{DocumentName}}, Please provide these document for confirming your admission!!";

        

        #endregion
    }
}
