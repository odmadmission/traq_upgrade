﻿using Microsoft.EntityFrameworkCore;
using OdmErp.ApplicationCore.Query;
using OdmErp.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class AdmissionSqlQuery
    {
        public ApplicationDbContext context;
        public AdmissionSqlQuery(ApplicationDbContext cont)
        {
            context = cont;
        }
        public List<View_Student_Admission_Counseller> GetView_Student_Admission_Counsellers()
        {
            return context.view_Student_Admission_Counsellers.FromSql(@"SELECT  a.[ID] as AdmissionId
      ,a.[InsertedDate]
      ,a.[ModifiedDate]
      ,a.[InsertedId]
      ,a.[ModifiedId]
      ,a.[IsAvailable]
      ,a.[FormNumber]
      ,a.[AcademicSessionId]
      ,a.[AdmissionSlotId]
      ,a.[MobileNumber]
      ,a.[IsAdmissionFormAmountPaid]
      ,a.[IsAdmission]
      ,a.[IsPersonalInterviewAppeared]
      ,a.[IsPersonalInterviewQualified]
      ,a.[IsDocumentsSubmitted],b.ID as AcademicStudentID

	  ,(b.FirstName+' '+b.LastName) as StudentName,
	  b.Image as ProfileImage,
	  m.Name as OrganisationName
	  ,c.AcademicStandardWindId,
	  c.Nucleus,
	  d.AcademicStandardId,
	  d.BoardStandardWingID,
	  e.BoardStandardId,
	  e.OrganizationAcademicId,
	  f.BoardId,
	  f.StandardId,
	  g.Name as BoardName,
	  h.Name as StandardName,
	  i.WingID,
	  j.Name as WingName,
	  k.OrganizationId,
	  l.DisplayName as SessionName,
	  l.Start as startDate,
	  l.[End] as endDate
  FROM [admission].[Admission] as a
 inner join [admission].[AdmissionStudent] as b on a.ID=b.AdmissionId
 inner join [admission].[AdmissionStudentStandard] as c on b.ID=c.AdmissionStudentID
 inner join [dbo].[academicStandardWings] as d on c.[AcademicStandardWindId]=d.ID
 inner join [dbo].[AcademicStandards] as e on d.AcademicStandardId=e.ID
 inner join [dbo].[BoardStandards] as f on e.BoardStandardId=f.ID
 inner join [dbo].[Boards] as g on f.BoardId=g.ID
 inner join [dbo].[Standards] as h on f.StandardId=h.ID
 inner join [dbo].[BoardStandardWings] as i on d.BoardStandardWingID=i.ID
 inner join [dbo].[Wing] as j on i.WingID=j.ID
 inner join [dbo].[OrganizationAcademics] as k on e.OrganizationAcademicId=k.ID
 inner join [dbo].[Organizations] as m on k.OrganizationId=m.ID
 inner join [dbo].[AcademicSessions] as l on a.AcademicSessionId=l.ID
 where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1 and
 j.Active=1 and k.Active=1 and l.Active=1 and l.IsAvailable=1 and a.IsAvailable=1").AsNoTracking().ToList();
        }
        public List<View_All_Admission> GetView_Student_Admission_List()
        {
            return context.View_All_Admissions.FromSql(@"SELECT  a.[ID] as ID
      ,a.[InsertedDate]
      ,a.[ModifiedDate]
      ,a.[InsertedId]
      ,a.[ModifiedId]
      ,a.[IsAvailable]
      ,a.[FormNumber] as FormNumber
      ,a.[AcademicSessionId]
      ,a.[AdmissionSlotId] as AdmissionSlotId
      ,a.[MobileNumber] as Mobile
      ,a.[IsAdmissionFormAmountPaid]
      ,a.[IsAdmission]
      ,a.[IsPersonalInterviewAppeared]
      ,a.[IsPersonalInterviewQualified]
      ,a.[IsDocumentsSubmitted]
	  ,a.[Status] as [Status]
	  ,a.AcademicSourceId as AdmissionSourceId
	  ,b.ID as AcademicStudentID

	  ,b.ID as AdmissionStudentId
	  ,a.LeadReferenceId as LeadReferenceId
	  ,(b.FirstName+(case when b.MiddleName is null then '' else (' '+b.MiddleName) end)+' '+b.LastName) as Fullname,
	  b.Image as ProfileImage,
	  m.Name as OrganisationName
	  ,c.AcademicStandardWindId,
	  c.Nucleus,
	  d.AcademicStandardId as AcademicStandardId,
	  d.BoardStandardWingID as AcademicStandardWingId,
	  e.BoardStandardId,
	  e.OrganizationAcademicId as OrganisationAcademicId,
	  f.BoardId,
	  f.StandardId,
	  g.Name as BoardName,
	  h.Name as StandardName,
	  i.WingID as WingId,
	  j.Name as WingName,
	  k.OrganizationId,
	  l.DisplayName as AcademicSessionName,
	  l.Start as startDate,
	  l.[End] as endDate,
	  o.FullName as EmergencyName,
	  o.EmailId as EmergencyEmail,
	  o.Mobile as EmergencyMobile
	  ,n.EmployeeId as StudentCounselorId
	  ,(p.FirstName+' '+p.LastName+'(EmpCode-'+p.EmpCode+')') as StudentCounselorName
	  ,n.AssignedDate as StudentCounselorAssignDate
	  ,n.CounsellorStatusId
	  ,n.EndDate as StudentCounselorEndDate
	  ,n.Remarks as StudentCounselorRemarks
	  ,n.[Status] as StudentCounselorStatus
	  ,q.Name as CounsellorStatusName
	  ,r.Name as AcademicSourceName
	  ,s.ID as FormPaymentId
	  ,s.AdmissionFeeId
	  ,s.TransactionId
	  ,s.PaidDate
	  ,s.ReceivedId
	  ,t.Ammount as PaymentAmount
	  ,t.Name as AdmissionFeeName
	  ,(u.FirstName+' '+u.LastName+'(EmpCode-'+u.EmpCode+')') as ReceivedPaymentBy
	  ,v.ExamDate as ExamDate
,w.Name as PaymentModeName
,(select top(1) kitstatus from [admission].[AdmissionStudentKits] where Active=1 and AdmissionId=a.ID order by ID desc) as KitStatus
,(select top(1) kitdate from [admission].[AdmissionStudentKits] where Active=1 and AdmissionId=a.ID order by ID desc) as KitStatusDate
  FROM [admission].[Admission] as a
 inner join [admission].[AdmissionStudent] as b on a.ID=b.AdmissionId
 inner join [admission].[AdmissionStudentStandard] as c on b.ID=c.AdmissionStudentID
 inner join [admission].AdmissionStudentContact as o on b.ID=o.AdmissionStudentId
 inner join [dbo].[academicStandardWings] as d on c.[AcademicStandardWindId]=d.ID
 inner join [dbo].[AcademicStandards] as e on d.AcademicStandardId=e.ID
 inner join [dbo].[BoardStandards] as f on e.BoardStandardId=f.ID
 inner join [dbo].[Boards] as g on f.BoardId=g.ID
 inner join [dbo].[Standards] as h on f.StandardId=h.ID
 inner join [dbo].[BoardStandardWings] as i on d.BoardStandardWingID=i.ID
 inner join [dbo].[Wing] as j on i.WingID=j.ID
 inner join [dbo].[OrganizationAcademics] as k on e.OrganizationAcademicId=k.ID
 inner join [dbo].[Organizations] as m on k.OrganizationId=m.ID
 inner join [dbo].[AcademicSessions] as l on a.AcademicSessionId=l.ID
 left join (select * from [admission].[AdmissionStudentCounselor] where Active=1) as n on b.ID=n.AdmissionStudentId
 left join (select * from [admission].[AdmissionCounsellor] where Active=1) as x on n.EmployeeId=x.ID
 left join [dbo].[Employees] as p on x.EmployeeId=p.ID
 left join [admission].[CounsellorStatus] as q on n.CounsellorStatusId=q.ID
 left join [admission].[AdmissionSource] as r on a.AcademicSourceId=r.ID
 left join (select * from [admission].[AdmissionFormPayment] where Active=1) as s on a.ID=s.AdmissionId
 left join (select * from [admission].[AdmissionFee] where Active=1) as t on s.AdmissionFeeId=t.ID
 left join (select * from [dbo].[Employees] where Active=1) as u on s.ReceivedId=u.ID
 left join (select * from [admission].[AdmissionStudentExam] where Active=1) as v on b.ID=v.AdmissionStudentId
left join (select * from [dbo].[PaymentType] where Active=1) as w on s.PaymentModeId=w.ID
 where a.Active=1 and b.Active=1 and c.Active=1 and d.Active=1 and e.Active=1 and f.Active=1 and g.Active=1 and h.Active=1 and i.Active=1 and
 j.Active=1 and k.Active=1 and l.Active=1 and l.IsAvailable=1 and a.IsAvailable=1 and o.ConatctType='official'").AsNoTracking().ToList();
        }
        public List<View_Admission_Progresssheet> GetAdmisionProgresssheet()
        {
            return context.progresssheets.FromSql(@"SELECT a.ID,
(case when(select count(*) from [admission].[AdmissionStudent] where Active=1 and AdmissionId=a.ID)=1 then 1 else 0 end) as personal,
(case when(select count(*) from [admission].[AdmissionStudentAddress] as m where m.Active=1 and m.AdmissionStudentId=b.ID)=2 then 1 else 0 end) as addresses,
(case when(select count(*) from [admission].[AdmissionStudentStandard] as m where m.Active=1 and m.AdmissionStudentId=b.ID)=1 then 1 else 0 end) as standarddet,
(case when(select count(*) from [admission].[AdmissionStudentAcademic] as m where m.Active=1 and m.AdmissionStudentId=b.ID)=1 then 1 else 0 end) as academic,
(case when(select count(*) from [admission].[AdmissionStudentContact] as m where m.Active=1 and m.AdmissionStudentId=b.ID)=2 then 1 else 0 end) as contact,
(case when(select count(*) from [admission].[AdmissionStudentDeclaration] as m where m.Active=1 and m.AdmissionId=a.ID)=1 then 1 else 0 end) as declaration,
(case when(select count(*) from [admission].[AdmissionStudentParent] as m where m.Active=1 and m.AdmissionStudentId=b.ID)=2 then 1 else 0 end) as parent,
(case when(select count(*) from [admission].[AdmissionStudentProficiency] as m where m.Active=1 and m.AdmissionStudentId=b.ID)=1 then 1 else 0 end) as proficiency,
(case when(select count(*) from [admission].[AdmissionStudentReference] as m where m.Active=1 and m.AdmissionStudentId=b.ID)>0 then 1 else 0 end) as reference,
(case when(select count(*) from [admission].[AdmissionStudentTransport] as m where m.Active=1 and m.AdmissionStudentId=b.ID)=1 then 1 else 0 end) as transport

  FROM [admission].[Admission] as a
 inner join [admission].[AdmissionStudent] as b on a.ID=b.AdmissionId

 where a.Active=1 and b.Active=1").AsNoTracking().ToList();
        }
    }
}
