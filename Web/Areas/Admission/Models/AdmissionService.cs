﻿using OdmErp.Web.Models;
using OdmErp.Web.SendInBlue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class AdmissionService
    {
        public AdmissionSqlQuery sqlQuery;
        public smssend smsSendData;
        public SendInBlueEmailManager sendinBLUE;
        public AdmissionService(AdmissionSqlQuery sql, smssend smsSend, SendInBlueEmailManager sendin)
        {
            sqlQuery = sql;
            smsSendData = smsSend;
            sendinBLUE = sendin;
        }
        public void sendadmissionpaymentremainderafter3days()
        {
            try
            {
                var alladmissions = sqlQuery.GetView_Student_Admission_List().Where(a => a.FormPaymentId == null).ToList();
                var admissions = alladmissions.Where(a => a.InsertedDate.Date.Subtract(DateTime.Now.Date).Days%3 == 0).ToList();
                foreach (var a in admissions)
                {
                    if(a.Mobile!=null)
                    {
                        smsSendData.SendcOMMONSms(a.Fullname, a.Mobile, "Hello " + a.Fullname + ", Thank you for registering with ODM Educational Group, Click on this link to https://tracq.odmps.org/Admission/Student/Login to pay application form fee. Your Registered Mobile number is " + a.Mobile + ".\n With Regards, ODM PUBLIC SCHOOL");
                    }
                    if (a.EmergencyEmail != null)
                    {
                        sendinBLUE.SendCommonMail(a.Fullname,"Hello " + a.Fullname + ", </br>Thank you for registering with ODM Educational Group, Click on this link to https://tracq.odmps.org/Admission/Student/Login to pay application form fee. Your Registered Mobile number is " + a.Mobile + ".",a.EmergencyEmail);
                    }
                }
            }
            catch { }
        }
        public void sendadmissionregistrationremainderafter3days()
        {
            try
            {
                var allcount = sqlQuery.GetAdmisionProgresssheet().Where(a => (a.personal + a.academic + a.addresses + a.contact + a.declaration + a.parent + a.proficiency + a.reference + a.standarddet + a.transport) < 10).Select(a => a.ID).ToList();
                var alladmissions = sqlQuery.GetView_Student_Admission_List().Where(a => allcount.Contains(a.ID) && a.FormPaymentId != null).ToList();
                var admissions = alladmissions.Where(a => a.PaidDate.Value.Date.Subtract(DateTime.Now.Date).Days % 3 == 0).ToList();

                foreach (var a in admissions)
                {
                    if (a.Mobile != null)
                    {
                        smsSendData.SendcOMMONSms(a.Fullname, a.Mobile, "Hello " + a.Fullname + ", Click on this link https://tracq.odmps.org/Admission/Student/Login and complete the application form so that Admission Team can process your application. Your registered mobile number is " + a.Mobile + ".\n With Regards, ODM PUBLIC SCHOOL");
                    }
                    if (a.EmergencyEmail != null)
                    {
                        sendinBLUE.SendCommonMail(a.Fullname, "Hello " + a.Fullname + ", </br>Click on this link https://tracq.odmps.org/Admission/Student/Login and complete the application form so that Admission Team can process your application. Your registered mobile number is " + a.Mobile + ".", a.EmergencyEmail);
                    }
                }
            }
            catch { }
        }
    }
}
