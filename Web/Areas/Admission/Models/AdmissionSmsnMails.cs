﻿using Microsoft.AspNetCore.Hosting;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace OdmErp.Web.Areas.Admission.Models
{
    public class AdmissionSmsnMails
    {
        private smssend smssend { get; set; }
        private readonly IHostingEnvironment hostingEnvironment;
        public AdmissionSmsnMails(smssend sms, IHostingEnvironment hostingEnv)
        {
            smssend = sms;
            hostingEnvironment = hostingEnv;
        }

        #region----------commom-----------
        public string GetDateTimeFormat(DateTime d, TimeSpan span)
        {
            DateTime time = d.Add(span);
            return time.ToString("hh:mm tt");
        }
        #endregion

        #region----------Exam Date And Status-------------------
        public string ExamDateAssign(string name, string mobile, string examdate, string formnumber, string starttime, string endtime, string reachtime, string subject, string sender)
        {
            string body = string.Empty;           
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "ExamAssign.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Name}}", name).Replace("{{Mobile}}", mobile).Replace("{{FormNumber}}", formnumber)
                    .Replace("{{ExamDate}}", examdate).Replace("{{Start Time}}", starttime)
                    .Replace("{{End Time}}", endtime).Replace("{{Reach Time}}", reachtime);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string ExamStatus(string name, string mobile, string examdate, string formnumber, string starttime, string endtime, string reachtime, string AppearedDate, string score,string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "ExamAssign.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Name}}", name).Replace("{{Mobile}}", mobile).Replace("{{FormNumber}}", formnumber)
                    .Replace("{{ExamDate}}", examdate).Replace("{{Start Time}}", starttime)
                    .Replace("{{End Time}}", endtime).Replace("{{Reach Time}}", reachtime).Replace("{{Appear Date}}", AppearedDate).Replace("{{Score}}", score);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        #endregion------------------------------------------------

        #region----------PI Date And Status-------------------       
        public string PIDateAssign(string session, string school, string baord, string classname, string wing,string formnumber, string name, string mobile, string email, string pidate, string pitime, string interviewer, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "PIAssign.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Session}}", session).Replace("{{Organisation}}", school).Replace("{{Board}}", baord)
                    .Replace("{{Class}}", classname).Replace("{{Wing}}", wing).Replace("{{FormNumber}}", formnumber)
                    .Replace("{{Name}}", name).Replace("{{Mobile}}", mobile)
                    .Replace("{{Email}}", email).Replace("{{PIDate}}", pidate).Replace("{{PITime}}", pitime).Replace("{{Interviewer}}", interviewer);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string PIStatus(string session, string school, string baord, string classname, string wing, string formnumber, string name, string mobile, string email, string pidate, string pitime, string interviewer,string score,string result,string remark,string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "PIAssignResult.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Session}}", session).Replace("{{Organisation}}", school).Replace("{{Board}}", baord)
                    .Replace("{{Class}}", classname).Replace("{{Wing}}", wing).Replace("{{FormNumber}}", formnumber)
                    .Replace("{{Name}}", name).Replace("{{Mobile}}", mobile)
                    .Replace("{{Email}}", email).Replace("{{PIDate}}", pidate).Replace("{{PITime}}", pitime).Replace("{{Interviewer}}", interviewer)
                    .Replace("{{Result}}", result).Replace("{{Score}}", score).Replace("{{Remark}}", remark);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        #endregion------------------------------------------------

        #region----------Document Date And Status-------------------
        public string DocumentDateAssign(string session, string school, string board, string classname, string wing, string formnumber, string name, string mobile, string email, string verificationdate, string verificationtime, string interviewer, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "PIAssign.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Session}}", session).Replace("{{Organisation}}", school).Replace("{{Board}}", board)
                    .Replace("{{Class}}", classname).Replace("{{Wing}}", wing).Replace("{{FormNumber}}", formnumber)
                    .Replace("{{Name}}", name).Replace("{{Mobile}}", mobile)
                    .Replace("{{Email}}", email).Replace("{{VerificationDate}}", verificationdate).Replace("{{VerificationTime}}", verificationtime).Replace("{{Verifier Name}}", interviewer);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string DocumentVerificationStatus(string session, string school, string board, string classname, string wing, string formnumber, string name, string mobile, string email, string verificationdate, string verificationtime, string interviewer, string result, string remark, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "DocumentAssignResult.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Session}}", session).Replace("{{Organisation}}", school).Replace("{{Board}}", board)
                    .Replace("{{Class}}", classname).Replace("{{Wing}}", wing).Replace("{{FormNumber}}", formnumber)
                    .Replace("{{Name}}", name).Replace("{{Mobile}}", mobile)
                    .Replace("{{Email}}", email).Replace("{{VerificationDate}}", verificationdate).Replace("{{VerificationTime}}", verificationtime).Replace("{{Interviewer}}", interviewer)
                    .Replace("{{Result}}", result).Replace("{{Remark}}", remark);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        #endregion------------------------------------------------

        #region-------- Basic Regd NotPaid (Comp)------------------
        public string BasicRegd_Paid(string session,
            string school,string board,
            string classname,string wing,
            string formnumber,string name, 
            string mobile,string email,
            string paymentmode,string transno,
            long ammount,string receivedname,string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "BasicRegistration_Paid.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Session}}", session).Replace("{{Organisation}}", school).Replace("{{Board}}", board)
                    .Replace("{{Class}}", classname).Replace("{{Wing}}", wing).Replace("{{FormNumber}}", formnumber)
                    .Replace("{{Name}}", name).Replace("{{Mobile}}", mobile)
                    .Replace("{{Email}}", email).Replace("{{PaymentMode}}", paymentmode).Replace("{{TransactionId}}", transno).Replace("{{Ammount}}", ammount.ToString()).Replace("{{Received Person}}", receivedname);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }
        public string BasicRegd_NotPaid(string session, string school,
            string board, string classname, string wing, 
            
            string name, 
            string mobile, string email, string subject, string sender)
        {
            string body = string.Empty;
            string filepath = hostingEnvironment.WebRootPath
                            + Path.DirectorySeparatorChar.ToString()
                            + "template/Admission/"
                            + Path.DirectorySeparatorChar.ToString()
                            + "BasicRegistration_NotPaid.html";
            using (StreamReader reader = new StreamReader(filepath))
            {
                body = reader.ReadToEnd().Replace("{{Session}}", session).Replace("{{Organisation}}", school).Replace("{{Board}}", board)
                    .Replace("{{Class}}", classname).Replace("{{Wing}}", wing)
                    .Replace("{{Name}}", name).Replace("{{Mobile}}", mobile)
                    .Replace("{{Email}}", email);
                smssend.SendHtmlFormattedEmail(sender, subject, body);
            }

            return body;
        }

        #endregion

    }

}
