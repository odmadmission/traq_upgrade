﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Areas.Admission.Models;
using OdmErp.Web.Areas.Student.Models;
using OdmErp.Web.DTO;
using OdmErp.Web.LeadSquared;
using OdmErp.Web.Models;
using OdmErp.Web.SendInBlue;
using OfficeOpenXml;
using Web.Controllers;
using static OdmErp.Web.LeadSquared.LeadManage;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class AdmissionStudentCommonMethod:AppController
    {

        public IAcademicSessionRepository academicSessionRepository;
        public IAdmissionRepository admissionRepository;
        public IOrganizationRepository organizationRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IStandardRepository StandardRepository;
        public IBoardRepository boardRepository;
        public IWingRepository wingRepository;
        public IBoardStandardRepository boardStandardRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IAdmissionStudentRepository admissionStudentRepository;
        public IAdmissionStudentContactRepository admissionStudentContactRepository;
        public IAdmissionStudentAddressRepository admissionStudentAddressRepository;
        public IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        public IPaymentType paymentType;
        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IEmployeeRepository employeeRepository;
        public CommonMethods commonMethods;

             public smssend smssend;
        public SendInBlueEmailManager senndInBlue;

        public IAdmissionStudentCounselorRepository admissionStudentCounselorRepository;
        public IBoardStandardRepository BoardStandardRepository;
        public commonsqlquery commonsqlquery;
        public IAdmissionStudentExamRepository admissionStudentExamRepository;
        private StudentSqlQuery sqlQuery;
        private IAdmissionCounsellorRepository admissionCounsellorRepository;
        public IAdmissionStudentKitRepository admissionStudentKitRepository;
        public AdmissionStudentCommonMethod(IAdmissionStudentKitRepository admissionStudentKitRepo,IAcademicSessionRepository academicSessionRepository, IAdmissionRepository admissionRepository,
            IOrganizationRepository organizationRepository, 
            IAcademicStandardRepository academicStandardRepository,
            IOrganizationAcademicRepository organizationAcademicRepository,
            IStandardRepository StandardRepository,
            IWingRepository wingRepository, 
            IBoardStandardRepository boardStandardRepository, 
            IBoardRepository boardRepository,
        IAcademicStandardWingRepository academicStandardWingRepository, 
        IBoardStandardWingRepository boardStandardWingRepository,
        IAdmissionStudentRepository admissionStudentRepository, 
        IAdmissionStudentContactRepository admissionStudentContactRepository,
        IAdmissionStudentAddressRepository admissionStudentAddressRepository, 
        IAdmissionStudentStandardRepository admissionStudentStandardRepository,
        IPaymentType paymentType,


              smssend smssend,
         SendInBlueEmailManager senndInBlue,
        IAdmissionFormPaymentRepository admissionFormPaymentRepository, IEmployeeRepository employeeRepository,
      
        CommonMethods commonMethods, IAdmissionStudentCounselorRepository admissionStudentCounselorRepository,
        IBoardStandardRepository BoardStandardRepository,
         IAdmissionCounsellorRepository admissionCounsellorRepository,
        commonsqlquery commonsqlquery, 
        IAdmissionStudentExamRepository admissionStudentExamRepository, StudentSqlQuery sqlQuery)
        {
            admissionStudentKitRepository = admissionStudentKitRepo;
            this.smssend = smssend;
            this.senndInBlue = senndInBlue;

            this.admissionCounsellorRepository = admissionCounsellorRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.admissionRepository = admissionRepository;
            this.organizationRepository = organizationRepository;
            this.academicStandardRepository = academicStandardRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.StandardRepository = StandardRepository;
            this.wingRepository = wingRepository;
            this.boardRepository = boardRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.academicStandardWingRepository = academicStandardWingRepository;
            this.boardStandardWingRepository = boardStandardWingRepository;
            this.admissionStudentRepository = admissionStudentRepository;
            this.admissionStudentContactRepository = admissionStudentContactRepository;
            this.admissionStudentAddressRepository = admissionStudentAddressRepository;
            this.admissionStudentStandardRepository = admissionStudentStandardRepository;
            this.paymentType = paymentType;
            this.admissionFormPaymentRepository = admissionFormPaymentRepository;
            this.employeeRepository = employeeRepository;         
            this.commonMethods = commonMethods;
            this.admissionStudentCounselorRepository = admissionStudentCounselorRepository;
            this.BoardStandardRepository = BoardStandardRepository;
            this.commonsqlquery = commonsqlquery;
            this.admissionStudentExamRepository = admissionStudentExamRepository;
            this.sqlQuery = sqlQuery;
        }



        public OdmErp.ApplicationCore.Entities.Admission UpdateAdmissionDataAMobile(string FirstName,
        string LastName,
        string ContactName,
        string ContactMobile,
        string EmailId,
        string ClassId,
        string AcademicStandardWindId, long sourceId,long admissionId)
        {
            CheckLoginStatus();
            // admissionRepository.GetAllAdmission().Where(a => a.MobileNumber == ContactMobile).FirstOrDefault();

            var adm = admissionRepository.GetAdmissionById(admissionId);
            long addmisionid = admissionId;

            AdmissionStudent student = admissionRepository.GetAdmissionStudentById(addmisionid);
            student.AdmissionId = addmisionid;
            student.FirstName = FirstName;
            student.LastName = LastName;
            student.DateOfBirth = null;
            student.Active = true;
            student.IsAvailable = true;
            student.Status = EntityStatus.ACTIVE;
            student.InsertedDate = DateTime.Now;
            student.ModifiedDate = DateTime.Now;
            student.InsertedId = userId;
            student.ModifiedId = userId;
             admissionRepository.UpdateAdmissionStudent(student);
            long addmisionstudentid = student.ID;


            string board = null;
            string org = null;
            string stand = null;
            string wing = null;

            AdmissionStudentStandard studentstandard = admissionRepository.GetAdmissionStudentStandardByAdmissionStudentId(addmisionstudentid);
            if (AcademicStandardWindId != null)
            {
                var res = academicStandardWingRepository.
                   GetByIdAsync(Convert.ToInt64(AcademicStandardWindId))
                   .Result;
                studentstandard.AcademicStandardWindId = Convert.ToInt64(AcademicStandardWindId);

                if (res != null)
                {
                    var data = boardStandardWingRepository.
                        GetByIdAsync(res.BoardStandardWingID).Result;
                    var data1 = boardStandardRepository.
                        GetByIdAsync(data.BoardStandardID).Result;
                    var data2 = academicStandardRepository.
                        GetByIdAsync(res.AcademicStandardId).Result;
                    var data3 = organizationAcademicRepository.
                        GetByIdAsync(data2.OrganizationAcademicId).Result;
                    board = boardRepository.
                        GetBoardById(data1.BoardId).Name;
                    wing = wingRepository.
                       GetByIdAsync(data.WingID).Result.Name;
                    stand = StandardRepository.
                      GetStandardById(data1.StandardId).Name;
                    org = organizationRepository.
                      GetOrganizationById(data3.OrganizationId).Name;

                    studentstandard.BoardId = data1.BoardId;
                    studentstandard.StandardId = data1.StandardId;
                    studentstandard.WingId = data.WingID;
                    studentstandard.OrganizationId = data3.OrganizationId;
                }
            }

            studentstandard.AdmissionStudentId = addmisionstudentid;
            studentstandard.Active = true;
            studentstandard.IsAvailable = true;
            studentstandard.Status = EntityStatus.ACTIVE;
            studentstandard.InsertedDate = DateTime.Now;
            studentstandard.ModifiedDate = DateTime.Now;
            studentstandard.InsertedId = userId;
            studentstandard.ModifiedId = userId;
            admissionRepository.UpdateAdmissionStudentStandard(studentstandard);
            AdmissionStudentContact stucontact = admissionRepository.GetAdmissionStudentContactByAdmissionStudentId(addmisionstudentid);
            stucontact.AdmissionStudentId = addmisionstudentid;
            stucontact.FullName = ContactName;
            stucontact.EmailId = EmailId;
            stucontact.Mobile = ContactMobile;
            stucontact.Active = true;
            stucontact.IsAvailable = true;
            stucontact.Status = EntityStatus.ACTIVE;
            stucontact.InsertedDate = DateTime.Now;
            stucontact.ModifiedDate = DateTime.Now;
            stucontact.InsertedId = userId;
            stucontact.ModifiedId = userId;
            stucontact.ConatctType = "official";
            admissionRepository.UpdateAdmissionStudentContact(stucontact);

          
            #region SMS and EMAIL


            string sessionname =
                 academicSessionRepository.
                 GetByIdAsync(adm.AcademicSessionId).Result.DisplayName;


            if (FirstName != null && LastName != null)
            {
                smssend.SendRegistrationSms(FirstName + " " + LastName, ContactMobile);
            }
            if (org != null)
            {
                senndInBlue.SendRegistrationMail(

                        sessionname, org, board, stand, wing, FirstName + " " + LastName, ContactName, ContactMobile, EmailId,
                        EmailId);
            }

            #endregion
            try
            {

                LeadCreateDto dto = new LeadCreateDto();
                dto.AdmissionId = adm.LeadReferenceId.ToString();
                dto.FirstName = student.FirstName;
                dto.LastName = student.LastName;
                dto.Name = stucontact.FullName;
                dto.Phone = stucontact.Mobile;
                dto.SchoolName = studentstandard.OrganizationId > 0 ? organizationRepository.
                    GetOrganizationById(studentstandard.OrganizationId).Name : "NA";
                dto.ClassName = studentstandard.StandardId > 0 ? StandardRepository.
                    GetStandardById(studentstandard.StandardId).Name : "NA";
                dto.WingName = studentstandard.WingId > 0 ? wingRepository.
                    GetByIdAsync(studentstandard.WingId).Result.Name : "NA";
                dto.EmailAddress = stucontact.EmailId;
                dto.Source = admissionRepository.
                    GetAdmissionSourceById(adm.AcademicSourceId).Name;
                dto.Status = "NOT PAID";

                dto.CreatedOn = NullableDateTimeToStringDateTime(adm.InsertedDate).DateTime;



                LeadManage leadManage = new LeadManage();
                LeadResponse lead = leadManage.updateLead(leadManage.CreateLeadData(dto));
              
            }
            catch (Exception e)
            {

            }

            return adm;
        }



        public async Task<OdmErp.ApplicationCore.Entities.Admission> SaveAdmissionDataAsync(string FirstName,
           string LastName,
           string ContactName,
           string ContactMobile,
           string EmailId,
           string ClassId,
           string AcademicStandardWindId, long sourceId)
        {
            CheckLoginStatus();
            // admissionRepository.GetAllAdmission().Where(a => a.MobileNumber == ContactMobile).FirstOrDefault();
            var SessionId = academicSessionRepository.
                GetAllAcademicSession().Where(a => a.IsAdmission == true)
                .FirstOrDefault().ID;
            OdmErp.ApplicationCore.Entities.Admission adm =
                new OdmErp.ApplicationCore.Entities.Admission();
            adm.AcademicSessionId = SessionId;
            adm.AdmissionSlotId = 0;
            adm.AcademicSourceId = sourceId;
            adm.MobileNumber = ContactMobile;
            adm.Active = true;
            adm.IsAvailable = true;
            adm.Status = Admission_Status.REGISTERED;
            adm.InsertedDate = DateTime.Now;
            adm.ModifiedDate = DateTime.Now;
            adm.InsertedId = userId;
            adm.ModifiedId = userId;
            //adm.FormNumber = "";
            //adm.LeadId = Guid.NewGuid();
            adm.LeadReferenceId = Guid.NewGuid();
            admissionRepository.CreateAdmission(adm);
            long addmisionid = adm.ID;
            AdmissionStudentKit kit = new AdmissionStudentKit();
            kit.kitdate = DateTime.Now;
            kit.kitstatus = "PENDING";
            kit.AdmissionId = addmisionid;
            kit.IsAvailable = true;
            kit.Status = Admission_Status.REGISTERED;
            kit.InsertedDate = DateTime.Now;
            kit.ModifiedDate = DateTime.Now;
            kit.InsertedId = 0;
            kit.ModifiedId = 0;
            await admissionStudentKitRepository.AddAsync(kit);
            AdmissionStudent student = new AdmissionStudent();
            student.AdmissionId = addmisionid;
            student.FirstName = FirstName;
            student.LastName = LastName;
            student.DateOfBirth = null;
            student.Active = true;
            student.IsAvailable = true;
            student.Status = EntityStatus.ACTIVE;
            student.InsertedDate = DateTime.Now;
            student.ModifiedDate = DateTime.Now;
            student.InsertedId = userId;
            student.ModifiedId = userId;
            await admissionStudentRepository.AddAsync(student);
            long addmisionstudentid = student.ID;


            string board = null;
            string org = null;
            string stand = null;
            string wing = null;

            AdmissionStudentStandard studentstandard = new AdmissionStudentStandard();
            if (AcademicStandardWindId != null)
            {
                var res = academicStandardWingRepository.
                   GetByIdAsync(Convert.ToInt64(AcademicStandardWindId))
                   .Result;
                studentstandard.AcademicStandardWindId = Convert.ToInt64(AcademicStandardWindId);
              
                if (res != null)
                {
                    var data = boardStandardWingRepository.
                        GetByIdAsync(res.BoardStandardWingID).Result;
                    var data1 = boardStandardRepository.
                        GetByIdAsync(data.BoardStandardID).Result;
                    var data2 = academicStandardRepository.
                        GetByIdAsync(res.AcademicStandardId).Result;
                    var data3 = organizationAcademicRepository.
                        GetByIdAsync(data2.OrganizationAcademicId).Result;
                    board = boardRepository.
                        GetBoardById(data1.BoardId).Name;
                    wing = wingRepository.
                       GetByIdAsync(data.WingID).Result.Name;
                    stand = StandardRepository.
                      GetStandardById(data1.StandardId).Name;
                    org = organizationRepository.
                      GetOrganizationById(data3.OrganizationId).Name;

                    studentstandard.BoardId = data1.BoardId;
                    studentstandard.StandardId = data1.StandardId;
                    studentstandard.WingId = data.WingID;
                    studentstandard.OrganizationId = data3.OrganizationId;
                }
            }

            studentstandard.AdmissionStudentId = addmisionstudentid;
            studentstandard.Active = true;
            studentstandard.IsAvailable = true;
            studentstandard.Status = EntityStatus.ACTIVE;
            studentstandard.InsertedDate = DateTime.Now;
            studentstandard.ModifiedDate = DateTime.Now;
            studentstandard.InsertedId = userId;
            studentstandard.ModifiedId = userId;
            await admissionStudentStandardRepository.AddAsync(studentstandard);
            AdmissionStudentContact stucontact = new AdmissionStudentContact();
            stucontact.AdmissionStudentId = addmisionstudentid;
            stucontact.FullName = ContactName;
            stucontact.EmailId = EmailId;
            stucontact.Mobile = ContactMobile;
            stucontact.Active = true;
            stucontact.IsAvailable = true;
            stucontact.Status = EntityStatus.ACTIVE;
            stucontact.InsertedDate = DateTime.Now;
            stucontact.ModifiedDate = DateTime.Now;
            stucontact.InsertedId = userId;
            stucontact.ModifiedId = userId;
            stucontact.ConatctType = "official";
            await admissionStudentContactRepository.AddAsync(stucontact);

            AdmissionTimeline timeline = new AdmissionTimeline();
            timeline.AdmissionId = addmisionid;
            timeline.Timeline = "REGISTRATION";
            timeline.Active = true;
            timeline.IsAvailable = true;
            timeline.Status = EntityStatus.ACTIVE;
            timeline.InsertedDate = DateTime.Now;
            timeline.ModifiedDate = DateTime.Now;
            timeline.InsertedId = userId;
            timeline.ModifiedId = userId;
            admissionRepository.CreateAdmissionTimeline(timeline);
            #region SMS and EMAIL


            string sessionname =
                 academicSessionRepository.
                 GetByIdAsync(adm.AcademicSessionId).Result.DisplayName;


            if (FirstName != null && LastName != null)
            {
                smssend.SendRegistrationSms(FirstName + " " + LastName, ContactMobile);
            }
            if (org != null)
            {
                senndInBlue.SendRegistrationMail(

                        sessionname, org, board, stand, wing, FirstName + " " + LastName, ContactName, ContactMobile, EmailId,
                        EmailId);
            }

            #endregion
            try
            {

                LeadCreateDto dto = new LeadCreateDto();
                dto.AdmissionId = adm.LeadReferenceId.ToString();
                dto.FirstName = student.FirstName;
                dto.LastName = student.LastName;
                dto.Name = stucontact.FullName;
                dto.Phone = stucontact.Mobile;
                dto.SchoolName = studentstandard.OrganizationId>0? organizationRepository.
                    GetOrganizationById(studentstandard.OrganizationId).Name:"NA";
                dto.ClassName = studentstandard.StandardId>0?StandardRepository.
                    GetStandardById(studentstandard.StandardId).Name:"NA";
                dto.WingName = studentstandard.WingId>0?wingRepository.
                    GetByIdAsync(studentstandard.WingId).Result.Name:"NA";
                dto.EmailAddress = stucontact.EmailId;
                dto.Source = admissionRepository.
                    GetAdmissionSourceById(adm.AcademicSourceId).Name;
                dto.Status = "NOT PAID";

                dto.CreatedOn = NullableDateTimeToStringDateTime(adm.InsertedDate).DateTime;



                LeadManage leadManage = new LeadManage();
                LeadResponse lead = leadManage.createLead(leadManage.CreateLeadData(dto));
                if (lead != null && lead.Message != null)
                {
                    adm.LeadId = Guid.Parse(lead.Message.Id);
                    admissionRepository.UpdateAdmission(adm);
                }
            }
            catch (Exception e)
            {

            }

            return adm;
        }

        public (string Date, string Time, string DateTime) NullableDateTimeToStringDateTime(DateTime? time)
        {
            return time == null ? (ViewConstants.NOT_APPLICABLE,
                ViewConstants.NOT_APPLICABLE, ViewConstants.NOT_APPLICABLE) : (time.Value.ToString("dd-MMM-yyyy"),
                time.Value.ToString("hh:mm tt"), time.Value.ToString("dd-MMM-yyyy hh:mm tt"));

        }

        public (long sessionId, long organizationId, long boardId,
           long standardId, long wingId, long academicStandardId, long boardStandardId,
           long boardStandardWingId, long organizationAcademicId)
           GetStudentIdData(long admissionStudentId)
        {
            long sessionId = 0;
            long organizationId = 0;
            long boardId = 0;
            long standardId = 0;
            long wingId = 0;
            long academicStandardId = 0;
            long boardStandardId = 0;

            long boardStandardWingId = 0;
            long organizationAcademicId = 0;

            if (admissionStudentId != 0)
            {


                var admission = admissionStudentRepository.
                    ListAllAsyncIncludeAll()
                    .Result.Where(a => a.ID == admissionStudentId).FirstOrDefault();
                if (admission != null)
                {

                    var admissionStudentStandard = admissionStudentStandardRepository.
                        ListAllAsyncIncludeAll()
                       .Result.
                       Where(a => a.AdmissionStudentId == admissionStudentId).FirstOrDefault();



                    if (admissionStudentStandard != null)
                    {
                        var academicStandardWing = academicStandardWingRepository.
                        GetByIdAsyncIncludeAll(admissionStudentStandard.AcademicStandardWindId
                        )
                        .Result;

                        if (academicStandardWing != null)
                        {
                            academicStandardId = academicStandardWing.AcademicStandardId;
                            boardStandardWingId = academicStandardWing.BoardStandardWingID;
                            wingId = academicStandardWing.WingID != null ? academicStandardWing.WingID.Value : 0;


                            var boardStandardWing = boardStandardWingRepository.
                            GetByIdAsyncIncludeAll(academicStandardWing.BoardStandardWingID).Result;

                            if (academicStandardWing != null)
                            {
                                boardStandardId = boardStandardWing.BoardStandardID;

                                var boardStandard = boardStandardRepository.
                                GetByIdAsyncIncludeAll(boardStandardWing.BoardStandardID).Result;

                                if (boardStandard != null)
                                {
                                    boardId = boardStandard.BoardId;
                                    standardId = boardStandard.StandardId;

                                    var academicStandard = academicStandardRepository.
                                    GetByIdAsyncIncludeAll(academicStandardWing.AcademicStandardId).Result;

                                    if (academicStandard != null)
                                    {
                                        //  academicStandardId = academicStandard.ID;

                                        var organizationAcademic = organizationAcademicRepository.
                                            GetByIdAsync(academicStandard.OrganizationAcademicId).Result;

                                        if (organizationAcademic != null)
                                        {
                                            organizationAcademicId = organizationAcademic.ID;
                                            sessionId = organizationAcademic.AcademicSessionId;
                                            organizationId = organizationAcademic.OrganizationId;
                                        }

                                    }
                                }
                            }




                        }
                    }

                }

                return (sessionId, organizationId, boardId,
             standardId, wingId, academicStandardId, boardStandardId,
             boardStandardWingId, organizationAcademicId);

            }
            else
            {
                return (0, 0, 0, 0, 0, 0, 0, 0, 0);
            }


        }


    }
}
