﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class StudentProfileViewModel
    {

        public string TabName { get; set; }

    }
    public class PersonalDetails
    {
        public IFormFile image { get; set; }
        public long AdmissionStudentId { get; set; }
        public long AdmissionId { get; set; }
        public string formNumber { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public Nullable<DateTime> dob { get; set; }
        public string gender { get; set; }
        public long Religion { get; set; }
        public long Nationality { get; set; }
        public long Category { get; set; }
        public long Language { get; set; }
        public long BloodGroup { get; set; }
        public string Aadhaar { get; set; }
        public string Passport { get; set; }
        public long[] lKnown { get; set; }

    }
    public class AddressDetails
    {
     public string presentAddressType { get; set; }
        public string permanentAddressType { get; set; }
        public long AdmissionStudentId { get; set; }
        public long AdmissionId { get; set; }

        public string presentAddress1 { get; set; }
        public string prestntAddress2 { get; set; }
        public string presentcountry { get; set; }
        public string presentstate { get; set; }
        public string presentcity { get; set; }
        public string presentpin { get; set; }


        public string permanentAddress1 { get; set; }
        public string permanentAddress2 { get; set; }
        public string permanentcountry { get; set; }
        public string permanentstate { get; set; }
        public string permanentcity { get; set; }
        public string permanentpin { get; set; }
    }
    public class StudentClassModel
    {      
        public long AdmissionStudentId { get; set; }
     
        public long AdmissionId { get; set; }
      
        public long ddlSloughtClass { get; set; }      
    
   
        public long AcademicStandardWindId { get; set; }

        public bool necleusAvail { get; set; }

    }

    public class StudentAcademicClassModel
    {
        public long AdmissionId { get; set; }
        public long AdmissionStudentId { get; set; }       
        public string OrganizationName { get; set; }     
        public string LastCityId { get; set; }     
        public string LastStateId { get; set; }      
        public string LastCountryId { get; set; }
        public string AggregatePercentageOrGrade { get; set; }      
        public string AggregatePercentageOrGradeDocument { get; set; }    
        public string ReasonForChange { get; set; }
    }
    public class StudentProficiencyModel
    {
        public long AdmissionId { get; set; }
        public long AdmissionStudentId { get; set; }
        public string Sports { get; set; }
        public string PerformingArts { get; set; }       
        public string PositionHeld { get; set; }
    }
    public class StudentTransportModel
    {
        public long AdmissionId { get; set; }
        public long AdmissionStudentId { get; set; }     
        public bool busAvail { get; set; }     
        public string LocationId { get; set; }      
        public decimal Distance { get; set; }
    }
    public class StudentContactViewmodel
    {
        public long AdmissionId { get; set; }
        public long AdmissionStudentId { get; set; }
        public string EmrName { get; set; }
        public string EmrPhNo { get; set; }
        public string OfficialContactType { get; set; }
        public string EmrOfcName { get; set; }
        public string EmrOfcPhNo { get; set; }
        public string EmrOfcEmail { get; set; }
        public string EmrContactType { get; set; }
      
    }
    public class StudentDeclarationClassModel
    {
        public long AdmissionId { get; set; }
        public long AdmissionStudentId { get; set; }
        public IFormFile parentSign { get; set; }
        public bool? IsDuaghter { get; set; }
        public bool? IsSon { get; set; }
        public long? ClassId { get; set; }
        public long? WingId { get; set; }    
        public bool? IsFather { get; set; }    
        public bool? IsMother { get; set; }      
        public bool? IsGuardian { get; set; }     
        public string StudentName { get; set; }      
        public string SignatureFileName { get; set; }      
        public string ParentName { get; set; }       
       
    }
    public class StudentReferenceModel
        {
        public string Firstperson { get; set; }
         public long AdmissionId { get; set; }
        public long AdmissionStudentId { get; set; }
        public string RefName1 { get; set; }
        public string RefMobile1 { get; set; }
        public string RefEmailId1 { get; set; }
        public string RefProfessionTypeId1 { get; set; }
        public string RefAddress1 { get; set; }      
        public string RefPincode1 { get; set; }
        public string RefCountrytId1 { get; set; }


        public string Secondperson { get; set; }
        public string RefName2 { get; set; }
        public string RefMobile2 { get; set; }
        public string RefEmailId2 { get; set; }
        public string RefProfessionTypeId2 { get; set; }
        public string RefAddress2 { get; set; }
        public string RefPincode2 { get; set; }
        public string RefCountrytId2 { get; set; }
    }

    public class StudentFatherAndMother
    {
        public long AdmissionStudentId { get; set; }
        public long AdmissionId { get; set; }
         public IFormFile motherimage { get; set; }
        public string MotFullName { get; set; }
        public string MotPrimaryMobile { get; set; }
        public string MotAlternativeMobile { get; set; }
        public string MotEmailId { get; set; }
        public string MotProfessionTypeId { get; set; }
        public string MotParentRelationship { get; set; }
        public string MotImage { get; set; }
        public string MotCompany { get; set; }

        // father deatils
        public IFormFile fatherimage { get; set; }
        public string FatFullName { get; set; }
        public string FatPrimaryMobile { get; set; }
        public string FatAlternativeMobile { get; set; }
        public string FatEmailId { get; set; }
        public string FatProfessionTypeId { get; set; }
        public string FatParentRelationship { get; set; }
        public string FatImage { get; set; }
        public string FatCompany { get; set; }

    }
}
