﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class AdmissionPaymentSummaryViewModel
    {
        public string regAmount { get; set; }

        public string txtStudentName { get; set; }

        public string txtContactName { get; set; }

        public string txtContactMobile { get; set; }
        public string txtEmailId { get; set; }
        public string txtSchool { get; set; }
        public string txtBoard { get; set; }

        public string txtClass { get; set; }

        public string Id { get; set; }

    }
}
