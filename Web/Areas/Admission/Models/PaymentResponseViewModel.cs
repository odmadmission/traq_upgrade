﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class PaymentResponseViewModel
    {
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string ParnetName { get; set; }

        public long AdmissionId { get; set; }
        public string UniqueTxnID { get; set; }

        public string Amount { get; set; }
        public string TxnDate { get; set; }

        public string Status { get; set; }
        public string ErrorDescription { get; set; }

        public string LeadReferenceId { get; set; }


    }
}
