﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Query;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class AdmitcardMastercls
    {
        public string Organization { get; set; }
        public string SessionName { get; set; }
        public string LogoName { get; set; }
        public string Address { get; set; }
        public string Tollfree { get; set; }
        public string Title { get; set; }
        public long OrganisationAcademicId { get; set; }
        public long sessionid { get; set; }
        public long ID { get; set; }
        public string Instruction { get; set; }
        public string OrganisationName { get; set; }
        public string VenueAddress { get; set; }
        public string Remarks { get; set; }
    }
    public class admitcardcls
    {
        public View_All_Admission all_Admission { get; set; }
        public AdmitcardMastercls admitcard { get; set; }
        public (bool? ExamRequired, List<studentdashboardexamcls> ob, bool? show) GetExamDet { get; set; }
    }
    public class AdmissionRegdDetailsCls
    {
        public OdmErp.ApplicationCore.Entities.Admission admission { get; set; }
        public AdmissionStudent admissionStudent { get; set; }
        public AdmissionFormPayment admissionFormPayment { get; set; }
        public AdmissionStudentContact admissionStudentOfficial { get; set; }
        public AdmissionStudentContact admissionStudentEmergency { get; set; }
        public AdmissionStudentAcademic admissionStudentAcademic { get; set; }
        public List<AdmissionStudentAddress> admissionStudentAddress { get; set; }
        public AdmissionStudentDeclaration admissionStudentDeclaration { get; set; }
        public List<commonCls> admissionStudentLanguage { get; set; }
        public AdmissionStudentParent admissionStudentFather { get; set; }
        public AdmissionStudentParent admissionStudentMother { get; set; }
        public AdmissionStudentProficiency admissionStudentProficiency { get; set; }
        public List<AdmissionStudentReference> admissionStudentReference { get; set; }
        public admissionStudentStandardcls admissionStudentStandard { get; set; }
        public AdmissionStudentTransport admissionStudentTransport { get; set; }
        public View_All_Admission all_Admission { get; set; }
        public bool? Is_Nucleus { get; set; }
        public List<commonCls> nationality { get; set; }
        public List<commonCls> category { get; set; }
        public List<commonCls> bloodGroups { get; set; }
        public List<commonCls> professiontypes { get; set; }
        public List<commonCls> languages { get; set; }
        public List<commonCls> locations { get; set; }
        public List<commonCls> countries { get; set; }
        public List<commonCls> religions { get; set; }
    }
    public class admissionStudentStandardcls
    {
        public long ID { get; set; }
        public long AcademicStandardWindId { get; set; }
        public long AdmissionStudentId { get; set; }
        public long StandardId { get; set; }
        public bool Nucleus { get; set; }
        public long OrganizationId { get; set; }
        public long WingId { get; set; }
        public long BoardStandardID { get; set; }
    }
    public class admissionStudentDetailscls
    {
        public long ID { get; set; }
        public long AdmissionId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public Nullable<DateTime> DateOfBirth { get; set; }
        public long BloodGroupID { get; set; }
        public long NatiobalityID { get; set; }
        public long ReligionID { get; set; }
        public long CategoryID { get; set; }
        public long MotherTongueID { get; set; }
        public string PassportNO { get; set; }
        public string AadharNO { get; set; }
        public string Image { get; set; }
        public List<long> languages { get; set; }
    }
    public class ContactViewmodel
    {
        public string txtEmrName { get; set; }
        public string txtEmrPhNo { get; set; }
        public string txtEmrOfcName { get; set; }
        public string txtEmrOfcPhNo { get; set; }
        public string txtEmrOfcEmail { get; set; }
        public int emegencyId { get; set; }
        public long admissionStudentID { get; set; }
        public int ofcContactId { get; set; }
    }
    public class PostModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }

    public class AdmissionSlotModel : BaseModel
    {
        public string SlotName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StartDatestring { get; set; }
        public string EndDatestring { get; set; }
        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
    }
    public class AdmissionFeeModel : BaseModel
    {
        public long AcademicSessionId { get; set; }
        public decimal Ammount { get; set; }
        public string AcademicSessionName { get; set; }
        public string Name { get; set; }
    }
    public class AdmissionCounsellorModal
    {
        public long Id { get; set; }
        public long EmpId { get; set; }
        public string EmpName { get; set; }
        public Nullable<DateTime> StartDate { get; set; }
        public Nullable<DateTime> EndDate { get; set; }
    }
    public class ResponseAdmission
    {
        public List<AdmissionSlotModel> AdmissionSlotList { get; set; }
        public List<AcademicSession> AcademicSessionList { get; set; }

    }

    public class AdmissionStandardExamDate
    {
        public long ID { get; set; }
        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        public long AcademicStandardId { get; set; }
        public string AcademicStandarName { get; set; }
        public Nullable<DateTime> ExamDate { get; set; }
        public Nullable<DateTime> ResultDate { get; set; }

    }
    public class AdmitCardVenueModelClass
    {
        public long ID { get; set; }
        public long OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public long AdmitCardVenueAddressId { get; set; }
        public string Address { get; set; }
    }
    public class AdmitcardInstructionsModel
    {
        public long ID { get; set; }
        public string LogoName { get; set; }
        public string Address { get; set; }
        public string Tollfree { get; set; }
        public string Title { get; set; }
        public string Instruction { get; set; }
        public long AdmitCardDetailsId { get; set; }
    }
    public class studentdashboardexamcls
    {
        public long Id { get; set; }
        public string ExamDate { get; set; }
        public string Score { get; set; }
        public string AppearedDate { get; set; }
        public string ResultDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ReachTime { get; set; }
        public string Qualified { get; set; }
        public string IsAppeared { get; set; }
        public long? RollNo { get; set; }
    }
    public class AdmissionEcounsellingListcls
    {

        public View_All_Admission admission { get; set; }
        public string status { get; set; }
        public string statusdate { get; set; }
        public bool smssend { get; set; }
        public DateTime? counsellingdate { get; set; }
        public string counsellingslot { get; set; }
        public long counsellingid { get; set; }
        public long counsellingdateid { get; set; }
        public long counsellingslotid { get; set; }

    }
    public class Admissionecounsellingcls
    {
        public long? AcademicSessionId { get; set; }
        public long? RegistrationSlot { get; set; }
        public long? OraganisationAcademicId { get; set; }
        public long? BoardId { get; set; }
        public long? AcademicStandardId { get; set; }
        public long? WingId { get; set; }
        public long? CounsellingDateID { get; set; }
        public long? CounsellingSlotID { get; set; }
        public long? RegistrationSource { get; set; }
        public string FormPaymentStatus { get; set; }
        public string formNumber { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string cstatusid { get; set; }
        public List<AcademicSession> AcademicSessionList { get; set; }

        public List<Board> BoardList { get; set; }
        public List<AdmissionEcounsellingListcls> admissionEcounsellingListcls { get; set; }

        public List<AdmissionSource> SourceList { get; set; }
        public List<commonCls> FormPaymentStatusList { get; set; }
        public List<commonCls> CounsellerDates { get; set; }
        public List<commonCls> CounsellerSlots { get; set; }
        public RegistrationModuleActionAccess regdaccess { get; set; }
    }
    public class Ecounsellingcls
    {
        public string counsellingstatus { get; set; }
        public bool counsellingissmssend { get; set; }
        public string counsellingstatuson { get; set; }
        public DateTime? counsellingdate { get; set; }
        public string counsellingslot { get; set; }
        public long counsellingID { get; set; }
        public long AdmissionId { get; set; }
        public long counsellingdateid { get; set; }
        public long counsellingslotid { get; set; }
    }
}