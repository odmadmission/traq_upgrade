﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OdmErp.Web.Models;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class EAndCModel
    {
    }
    public class AdmissionCampusTourDateModel : BaseModel
    {
        public string AcademicSessionName { get;set;}
        public DateTime TourDate { get; set; }
        public string TourDatestring { get; set; }
        public long SlotCount { get; set; }
    }
    public class AdmissionCampusTourSlotModel : BaseModel    
    {
      
        public string TourDatestring { get; set; }
        public string AcademicSessionName { get; set; }
        public long AdmissionCampusTourDateID { get; set; }
        public DateTime TourDate { get; set; }
        public string Name { get; set; }  
       public string No_Of_Registration { get; set; }   
      public TimeSpan StartTime { get; set; } 
      public Nullable<TimeSpan> EndTime { get; set; }
        public string StartTimestring { get; set; }
        public string EndTimestring { get; set; }
        public long AdmissionEcounsellingDateID { get; set; }
    }
   

}
