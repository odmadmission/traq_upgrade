﻿using OdmErp.ApplicationCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Models
{

    public class RazorPayModel
    {
        public RazorPayModel()
        {
        }

        public string key { get; set; }
        public string orderid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public long amount { get; set; }
        public string callback { get; set; }
        public string email { get; set; }
        public string contacts { get; set; }
    }

    public class RazorPayOrder
    {
        public string currency { get; set; }
        public string receipt { get; set; }
        public long amount { get; set; }
    }
    public class RazorPayResponse
    {
        public string razorpay_payment_id { get; set; }
        public string razorpay_order_id { get; set; }
        public string razorpay_signature { get; set; }
    }

    public class RazorPayOrderRepsonse
    {
        public RazorPayOrderRepsonse()
        {
        }

        public string id { get; set; }
        public string entity { get; set; }
        public int amount { get; set; }
        public int amount_paid { get; set; }
        public int amount_due { get; set; }
        public string currency { get; set; }

        public string offer_id { get; set; }
        public string status { get; set; }
        public int attempts { get; set; }
        public List<string> notes { get; set; }
        public long created_at { get; set; }
        public string receipt { get; set; }

    }
    public class AdmissionStudentViewModel
    {
        public AdmissionViewModel admissionViewModel { get; set; }
        public List<AdmDocumentViewModel> GetCommonFileDetailsList { get; set; }
        public List<AdmDocumentViewModel> GetExtraFileDetailsList { get; set; }

        public List<StudentExamDeatilsModel> StudentExamDeatilsModel { get; set; }
        public ModuleActionAccess access { get; set; }
        public AdmitcardMastercls admitcard { get; set; }

        public RazorPayModel razor { get; set; }
        public bool error { get; set; }
    }

    public class ModuleActionAccess
    {

        public bool ViewProfile { get; set; }
        public bool Pay { get; set; }
        public bool AssignCounsellor { get; set; }
        public bool CounsellorStatus { get; set; }

        public bool PIDate { get; set; }
        public bool PIStatus { get; set; }
        public bool VerifyDate { get; set; }
        public bool VerifyStatus { get; set; }

        public bool AddDocument { get; set; }
        public bool SubmitDocument { get; set; }

        public bool ExamDate { get; set; }
        public bool ExamStatus { get; set; }
    }
    public class RegistrationModuleActionAccess
    {
        public bool List { get; set; }
        public bool Pay { get; set; }
        public bool ViewProfile { get; set; }      
        public bool EditProfile { get; set; }      
        public bool UploadExcel { get; set; } 
        public bool NewRegistration { get; set; }
        public bool AssignCounsellor { get; set; }
        public bool Result { get; set; }
    }
    public class StudentTimeLineModal
    {
        public long AdmissionId { get; set; }
        public string Status { get; set; }
        public string Timeline { get; set; }
        public string EmployeeName { get; set; }
        public string InsetedDate { get; set; }
    }
    public class StudentFileDocumentModal
    {
        public long ID { get; set; }
        public string Status { get; set; }
        public string SubtypeName { get; set; }
       
    }

    public class PiModelClass
    {
        public Nullable<DateTime> PiDateInDate { get; set; }
     
        public long ID { get; set; }
        public string PiDate { get; set; }
        public Nullable<TimeSpan> PiTimespan { get; set; }
        public string PiTime { get; set; }
        public string AppearedDate { get; set; }
        public string AppearedStatus { get; set; }
        public string Qualified { get; set; }
        public string InterviewerName { get; set; }

        public string InterviewTakenBy { get; set; }
        public string score { get; set; }
        public string remarks { get; set; }

        public int active { get; set; }
    }
    

    public class AdmissionViewModel
    {
        public string LeadSquaredReferenceId { get; set; }
        public (string url, string method, string msg) BillDesk { get; set; }
        public long AdmissionId { get; set; }
        public string FormNumber { get; set; }
        public string EmergencyContactPerson { get; set; }
        public string ContactName { get; set; }

        public string Mobile { get; set; }
        public string EmailId { get; set; }
        public long AcademicSessionId { get; set; }

        public string AcademicSessionName { get; set; }
        public string LeadReferenceId { get; set; }

        public long SlotId { get; set; }
        public long SourceId { get; set; }

        public DateTime InserteDate { get; set; }

        public string Amount { get; set; }
        public string SlotName { get; set; }

        public string Status { get; set; }

        public IList<PiModelClass> GetPIList { get; set; }
        public IList<PiModelClass> GetDocumentList { get; set; }

        public string SourceName { get; set; }
        public (string boardName, string className,
            string schoolName, string wingName,
            long boardId,
            long standardId,
             long schoolId,
            long wingId
           ) ClassDetails
        { get; set; }

        public (int status, string TransactionId,
            string ModeOfPayment,string PaidOn,string RecievedEmployeeName,
            long Amount
           ) FormPayment
        { get; set; }

        public long AdmissionStudentId { get; set; }
        public string FullName { get; set; }
        public string Image { get; set; }

        public (bool? ExamRequired, List<studentdashboardexamcls> ob, bool? show) GetExamDet{ get; set; }
        public (bool? ExamRequired, string ExamDate, string AppearedDate, string ResultDate,
            string StartTime, string EndTime, string ReachTime, string Qualified, int show) GetExamDetails{ get; set; }
        public (string PiDate, string PiTime,
            string AppearedDate, string Qualified, string InterviewerName,
            string InterviewTakenBy, string score, string remarks) PiDetails
        { get; set; }

        public (string PiDate, string PiTime,
           string AppearedDate, string Qualified, string InterviewerName,
           string remarks
           ) DocumentDetails
        { get; set; }
        public (string IsQualified, string Score, 
            string ResultDate, string MarksheetFileName) ResultDetails { get; set; }

        public List<AdmDocumentViewModel> files { get; set; }

        public (string name, string assignedon,string remarks,string Status) Counsellor { get; set; }
        public List<StudentTimeLineModal> StudentTimeLine { get; set; }
        public View_All_Admission all_Admission { get; set; }
    }
    

}
