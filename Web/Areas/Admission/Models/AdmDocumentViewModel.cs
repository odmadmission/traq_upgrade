﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class AdmDocumentViewModel
    {
        public long AdmissionDocumentId { get; set; }
        public long DocumentTypeId { get; set; }
        public long DocumentSubTypeId { get; set; }
        public long AdmissionDocumentAssignId { get; set; }
        public string Name { get; set; }
        public (string Submitted,string SubmittedOn,string EmpName,string FileName) Submit { get; set; }
    
        
    }
    public class StudentExamDeatilsModel
    {
        public long AdmissionStandardExamId { get; set; }
        public string Examdatestring { get; set; }
        public Nullable<DateTime> ExamDate { get; set; }
        public Nullable<TimeSpan> ExamStartTime { get; set; }
        public Nullable<TimeSpan> ExamEndTime { get; set; }
        public Nullable<TimeSpan> ReachTime { get; set; }


    }
}
