﻿using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Query;
using OdmErp.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class BasicStudentModel
    {
      
        [Required(ErrorMessage = "Please Select Session")]
        public long AcademicSessionId { get; set; }
      
        [Required(ErrorMessage = "Please Select Source")]
        public long AcademicSourceId { get; set; }
    
        [Required(ErrorMessage = "Please Select Slot")]
        public long AdmissionSlotId { get; set; }
        [Display(Name = "FirstName")]
        [Required(ErrorMessage = "Please Enter First Name")]
        public string FirstName { get; set; }
        [Display(Name = "LastName")]
        [Required(ErrorMessage = "Please Enter Last Name")]
        public string LastName { get; set; }
        public long AdmissionStudentId { get; set; }
        public long StandardId { get; set; }
        public long AcademicStandardWindId { get; set; }
        [Display(Name = "FullName")]
        [Required(ErrorMessage = "Please Enter Full Name")]
        public string FullName { get; set; }

        [Display(Name = "Mobile")]
        [Required(ErrorMessage = "Please Enter Mobile Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
       
        public string Mobile { get; set; }
        [Display(Name = "EmailId")]
        [Required(ErrorMessage = "Please Enter EmailId")]
        public string EmailId { get; set; }

        [Display(Name = "FormNumber")]
        [Required(ErrorMessage = "Please Enter Form Number")]
        public string FormNumber { get; set; }

    }
    public class BasicStudentDetailsModel:BaseModel
    {
        public string LeadReferenceId { get; set; }
        public string Fullname { get; set; }
        public string FormNumber { get; set; }
        public long StudentCounselorId { get; set; }
        public string StudentCounselorName { get; set; }
        public long AdmissionStudentId { get; set; }
        public long AcademicSessionId { get; set; }
        public long OrganisationAcademicId { get; set; }
        public long AcademicStandardId { get; set; }
        public long AcademicSourceId { get; set; }
        public long AdmissionSlotId { get; set; }
        public string AcademicSourceName { get; set; }
        public string AdmissionSlotName { get; set; }
        public string AcademicSessionName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmergencyName { get; set; }
        public string EmergencyMobile { get; set; }
        public string EmergencyEmail { get; set; }
        public string Mobile { get; set; }
        public string EmailId { get; set; }
        public string StandardName { get; set; }
        public string WingName { get; set; }
        public string OrganisationName { get; set; }
        public string ExamDate { get; set; }
        public long WingId { get; set; }
        public long AcademicStandardWingId { get; set; }
        //public DateTime InsertedDate { get; set; }
        public long AdmissionSourceId { get; set; }
        public long StandardId { get; set; }
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        public long FormPaymentId { get; set; }
        public string QuestionLink { get; set; }
    }
    public class ResponseStudentModal
    {

        public long? AcademicSessionId { get; set; }
        public long? RegistrationSlot { get; set; }
        public long? OraganisationAcademicId { get; set; }
        public long? BoardId { get; set; }
        public long? AcademicStandardId { get; set; }
        public long? WingId { get; set; }
        public long? RegistrationSource { get; set; }
        public string FormPaymentStatus { get; set; }
        public string formNumber { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }

        public  List<View_All_Admission> BasicStudentDetailsModel { get; set; }
        public List<AcademicSession> AcademicSessionList { get; set; }
        public List<Employee> EmployeeList { get; set; }
        public List<Board> BoardList { get; set; }

        public List<AdmissionSource> SourceList { get; set; }
        public List<commonCls> FormPaymentStatusList { get; set; }
        public RegistrationModuleActionAccess regdaccess { get; set; }
        public List<View_Admission_Progresssheet> progresssheets { get; set; }

    }
    public class AdmissionStandardSettingModal:BaseModel
    {
        public bool? ExamDates { get; set; }
        public string Code { get; set; }    
        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        public bool? Nucleus { get; set; }      
        public long BoardStandardId { get; set; }
        public string StandardName { get; set; }      
        public bool? ExamRequired { get; set; }      
        public bool? PIRequired { get; set; }      
        public bool? DocumentRequird { get; set; }
        public long? OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public long? BoardId { get; set; }
        public string BoardName { get; set; }
    }
    public class AdmissionStandardSeatQuotaModal : BaseModel
    {
        public long OrganisationAcademicId { get; set; }
        public string Code { get; set; }
        public long AcademicSessionId { get; set; }
        public long BoardId { get; set; }
        public string AcademicSessionName { get; set; }
        public string BoardName { get; set; }
        public bool? Nucleus { get; set; }
        public long BoardStandardId { get; set; }
        public string StandardName { get; set; }
        public long NoOfSeat { get; set; }
        public string OrganisationName { get; set; }
    }
    public class ResponseAdmissionStandardSeatQuotaModal
    {
        public List<AdmissionStandardSeatQuotaModal> StandardSeatQuotaList { get; set; }
        public List<AcademicSession> AcademicSessionList { get; set; }      
        public List<Board> BoardList { get; set; }
    }
    public class AdmissionStanadardExamModal : BaseModel
    {       
        public long AcademicSessionId { get; set; }      
        public string AcademicSessionName { get; set; }      
        public long BoardStandardId { get; set; }
        public string StandardName { get; set; }
        public string BoardName { get; set; }
        public string Examdatestring { get; set; }
        public Nullable<DateTime> ExamDate { get; set; }
        public Nullable<TimeSpan> ExamStartTime { get; set; }
        public Nullable<TimeSpan> ExamEndTime { get; set; }
        public Nullable<TimeSpan> ReachTime { get; set; }
        public string ExamName { get; set; }
        public string ExamStartTimeString { get; set; }
        public string ExamEndTimeString { get; set; }
        public string ReachTimeString { get; set; }
        public long? NoOfStudent { get; set; }
        public long OrganisationId { get; set; }
        public string OrganisationName { get; set; }
        public string QuestionLink { get; set; }
        public long WingId { get; set; }
        public string WingName { get; set; }
        public Nullable<DateTime> ResultDate { get; set; }
        public Nullable<DateTime> PublishStartDate { get; set; }
        public Nullable<DateTime> PublishEndDate { get; set; }
        public bool? IsPublished { get; set; }
        public string ResultDatestring { get; set; }
    }
    public class ResponseAdmissionStanadardExamModal
    {
        public List<AdmissionStanadardExamModal> AdmissionStanadardExamList { get; set; }
        public List<AcademicSession> AcademicSessionList { get; set; }
        public List<CommonStudentModal> CommonStudentModal { get; set; }
        public List<Organization> OrganisationList { get; set; }
        public List<Wing> WingList { get; set; }
    }

    public class ResponseAdmissionStanadardPIModal
    {
        public List<AdmissionStanadardPIModal> AdmissionStanadardPIList { get; set; }
        public List<AcademicSession> AcademicSessionList { get; set; }
        public List<CommonStudentModal> AdmissionStudentList { get; set; }
        public List<Employee> EmployeeList { get; set; }
        public List<CommonStudentModal> CommonStudentModal { get; set; }
        public List<Board> Boardlist { get; set; }
        public List<Nullable<DateTime>> PersonalInterviewlist { get; set; }
        public List<Nullable<DateTime>> DocumentDatelist { get; set; }

    }
    public class CommonStudentModal
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string FormNumber { get; set; }
    }
    public class CommonStudentStandardModal
    {
        public long Id { get; set; }
        public long OrganisationId { get; set; }
        public string Name { get; set; }
        public string FormNumber { get; set; }
    }
    public class sessiondetailmodal
    {
        public string boardClass { get; set; }
        public string boardname { get; set; }
        public long boardStandardId { get; set; }
        public long boardId { get; set; }
        public long standardId { get; set; }
    }
    public class MapClassToAddmissionModal:BaseModel
    {
        public string ClassName { get; set; }
        public string BoardName { get; set; }
        public string WingName { get; set; }
        public string SessionName { get; set; }        
        public long AcademicSessionId { get; set; }      
        public long BoardId { get; set; }      
        public long ClassId { get; set; }      
        public long WingId { get; set; }

    }
    public class AdmissionStandardExamResultModal : BaseModel
    {
        public string ClassName { get; set; }      
        public string SessionName { get; set; }
        public long AcademicSessionId { get; set; }      
        public long BoardStandardId { get; set; }
        public string ResultDateString { get; set; }
        public DateTime ResultDate { get; set; }
        public string OrganisationName { get; set; }
        public long OrganisationAcademicId { get; set; }
        public long ClassId { get; set; }
        public long OrganisationId { get; set; }
        public long BoardId { get; set; }
        public string BoardName { get; set; }
    }
    public class AdmissionStanadardPIModal : BaseModel
    {
        
        public long AdmissionStudentId { get; set; }
        public string AdmissionStudentName { get; set; }
        public Nullable<DateTime> PIDate { get; set; }
        public string PIDateString { get; set; }
        public Nullable<TimeSpan> PITime { get; set; }
        public string PITimeString { get; set; }
        public long EmployeeId { get; set; }
        public String EmployeeName { get; set; }
        public Nullable<bool> IsAppeared { get; set; }      
        public Nullable<bool> IsQualified { get; set; }
       

        public string IsAppearedstring { get; set; }
        public string IsQualifiedstring { get; set; }
        public Nullable<DateTime> AppearedDateTime { get; set; }
        public long AssignedCounselorId { get; set; }
        public string AssignedCounselorName { get; set; }
        public string AppearedDateTimeString { get; set; }      
        public string StudentName { get; set; }
        public string StudentCode { get; set; }

        public long? Score { get; set; }
        public string ScoreString { get; set; }
        public long? InterviewTakenId { get; set; }     
        public string Remarks { get; set; }

    }
    public class ResponseAdmissionStanadardDocumentModal
    {
        public List<AdmissionStanadardDocumentModal> AdmissionStanadardDocumentList { get; set; }
        public List<AcademicSession> AcademicSessionList { get; set; }
        public List<CommonStudentModal> AdmissionStudentList { get; set; }
        public List<Employee> EmployeeList { get; set; }
        public List<CommonStudentModal> CommonStudentModal { get; set; }
        public List<Board> Boardlist { get; set; }
        public List<Nullable<DateTime>> DocumentDatelist { get; set; }

    }
    public class AdmissionStanadardDocumentModal : BaseModel
    {
        public string IsAppearedstring { get; set; }
        public string IsQualifiedstring { get; set; }
        public long AdmissionStudentId { get; set; }
        public string AdmissionStudentName { get; set; }
        public Nullable<DateTime> DocumentDate { get; set; }
        public string DocumentDateString { get; set; }
        public Nullable<TimeSpan> DocumentTime { get; set; }
        public string DocumentTimeString { get; set; }
        public long EmployeeId { get; set; }
        public String EmployeeName { get; set; }
        public Nullable<bool> IsAppeared { get; set; }
        public Nullable<bool> IsQualified { get; set; }       
        public Nullable<DateTime> AppearedDateTime { get; set; }
        public long AssignedCounselorId { get; set; }
        public string AssignedCounselorName { get; set; }
        public string AppearedDateTimeString { get; set; } 
        public long? InterviewTakenId { get; set; }
        public string Remarks { get; set; }

    }
    public class studentStandardModal : BaseModel
    {
        public long AdmissionStudentId { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string FormNumber { get; set; }
        public long AcademicSessionId { get; set; }
        public string AcademicSessionName { get; set; }
        public long BoardId { get; set; }
        public string BoardName { get; set; }
        public long ClassId { get; set; }
        public string ClassName { get; set; }
        public long WingId { get; set; }
        public string WingName { get; set; }
        public long OrganisationId { get; set; }
        public string OrganisationName { get; set; }
        public string StudentName { get; set; }

        public IList<PiModelClass> PiList { get; set; }

    }
}
