﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.SqlServer.Management.Smo;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Areas.Admission.Models;
using OdmErp.Web.Models;
using Web.Controllers;

namespace OdmErp.Web.Areas.Admission.Models
{
    public class StudentAdmissionMethod : AppController
    {

        public IAcademicSessionRepository academicSessionRepository;
        public IAdmissionRepository admissionRepository;
        public IOrganizationRepository organizationRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IStandardRepository StandardRepository;
        public IBoardRepository boardRepository;
        public IWingRepository wingRepository;
        public IBoardStandardRepository boardStandardRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IAdmissionStudentRepository admissionStudentRepository;
        public IAdmissionStudentContactRepository admissionStudentContactRepository;
        public IAdmissionStudentAddressRepository admissionStudentAddressRepository;
        public IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        public IPaymentType paymentType;
        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IEmployeeRepository employeeRepository;
        public IAdmissionStudentCounselorRepository admissionStudentCounselorRepository;
        public IAdmissionStudentExamRepository admissionStudentExamRepository;
        public IAdmissionStandardExamRepository admissionStandardExamRepository;
        public IAdmissionExamDateAssignRepository admissionExamDateAssign;
        public IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository;
        public IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssign;
        public IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepository;
        public IAdmissionDocumentRepository admissionDocumentRepository;
        public IAdmissionDocumentAssignRepository admissionDocumentAssignRepository;
        public IDocumentSubTypeRepository documentSubTypeRepository;
        public IDocumentTypeRepository documentTypeRepository;
        public IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository;
        public IAdmissionCounsellorRepository admissionCounsellorRepository;
        public IStandardRepository standardRepository;
        public CommonMethods commonMethods;
        public IAdmissionStudentDocumentRepository admissionStudentDocumentRepository;
        public IAdmissionFeeRepository admissionFeeRepository;
        public AdmissionStudentCommonMethod amissionStudentCommonMethod;
        public ICounsellorStatusRepository counsellorStatusRepository;
        public AdmissionSqlQuery admissionSqlQuery;
        public IAdmitcardDetailsRepository admitcardDetailsRepository;
        public IAdmitcardInstructionsRepository admitcardInstructionsRepository;
        public IAdmitcardVenueRepository admitcardVenueRepository;
        public StudentAdmissionMethod(IAdmitcardDetailsRepository admitcardDetailsRepo,
            IAdmitcardInstructionsRepository admitcardInstructionsRepo,
            IAdmitcardVenueRepository admitcardVenueRepo,
            AdmissionSqlQuery admissionSql, IAcademicSessionRepository academicSessionRepository, IAdmissionRepository admissionRepository,
            IOrganizationRepository organizationRepository, IAcademicStandardRepository academicStandardRepository,
            IOrganizationAcademicRepository organizationAcademicRepository, IStandardRepository StandardRepository,
            IWingRepository wingRepository, IBoardStandardRepository boardStandardRepository, IBoardRepository boardRepository,
        IAcademicStandardWingRepository academicStandardWingRepository, IBoardStandardWingRepository boardStandardWingRepository,
        IAdmissionStudentRepository admissionStudentRepository, IAdmissionStudentContactRepository admissionStudentContactRepository,
        IAdmissionStudentAddressRepository admissionStudentAddressRepository, IAdmissionStudentStandardRepository admissionStudentStandardRepository,
        IPaymentType paymentType, IAdmissionFormPaymentRepository admissionFormPaymentRepository, IEmployeeRepository employeeRepository,
        IAdmissionStudentCounselorRepository admissionStudentCounselorRepository,
       CommonMethods commonMethods,
        IAdmissionStudentExamRepository admissionStudentExamRepository, IAdmissionStandardExamRepository admissionStandardExamRepository,
        IAdmissionExamDateAssignRepository admissionExamDateAssign, IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository,
        IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssign, IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepository,
        IAdmissionDocumentRepository admissionDocumentRepository, IAdmissionDocumentAssignRepository admissionDocumentAssignRepository,
        IDocumentSubTypeRepository documentSubTypeRepository, IDocumentTypeRepository documentTypeRepository,
        IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository, IAdmissionCounsellorRepository admissionCounsellorRepository,
        IStandardRepository standardRepository, IAdmissionStudentDocumentRepository admissionStudentDocumentRepository,
        IAdmissionFeeRepository admissionFeeRepository, AdmissionStudentCommonMethod amissionStudentCommonMethod, ICounsellorStatusRepository counsellorStatusRepository)
        {
            admitcardDetailsRepository = admitcardDetailsRepo;
            admitcardInstructionsRepository = admitcardInstructionsRepo;
            admitcardVenueRepository = admitcardVenueRepo;
            admissionSqlQuery = admissionSql;
            this.counsellorStatusRepository = counsellorStatusRepository;
            this.amissionStudentCommonMethod = amissionStudentCommonMethod;
            this.commonMethods = commonMethods;
            this.admissionFeeRepository = admissionFeeRepository;
            this.admissionStudentDocumentRepository = admissionStudentDocumentRepository;
            this.standardRepository = standardRepository;
            this.admissionCounsellorRepository = admissionCounsellorRepository;
            this.admissionStudentCounsellorRepository = admissionStudentCounsellorRepository;
            this.documentTypeRepository = documentTypeRepository;
            this.documentSubTypeRepository = documentSubTypeRepository;
            this.admissionDocumentAssignRepository = admissionDocumentAssignRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.admissionRepository = admissionRepository;
            this.organizationRepository = organizationRepository;
            this.academicStandardRepository = academicStandardRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.StandardRepository = StandardRepository;
            this.wingRepository = wingRepository;
            this.boardRepository = boardRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.academicStandardWingRepository = academicStandardWingRepository;
            this.boardStandardWingRepository = boardStandardWingRepository;
            this.admissionStudentRepository = admissionStudentRepository;
            this.admissionStudentContactRepository = admissionStudentContactRepository;
            this.admissionStudentAddressRepository = admissionStudentAddressRepository;
            this.admissionStudentStandardRepository = admissionStudentStandardRepository;
            this.paymentType = paymentType;
            this.admissionFormPaymentRepository = admissionFormPaymentRepository;
            this.employeeRepository = employeeRepository;
            this.admissionStudentCounselorRepository = admissionStudentCounselorRepository;
            this.admissionStudentExamRepository = admissionStudentExamRepository;
            this.admissionStandardExamRepository = admissionStandardExamRepository;
            this.admissionExamDateAssign = admissionExamDateAssign;
            this.admissionStandardExamResultDateRepository = admissionStandardExamResultDateRepository;
            this.admissionPersonalInterviewAssign = admissionPersonalInterviewAssign;
            this.admissionDocumentVerificationAssignRepository = admissionDocumentVerificationAssignRepository;
            this.admissionDocumentRepository = admissionDocumentRepository;
        }
        public List<BasicStudentDetailsModel> GetstudentDetailsList()
        {
            try
            {
                var admission = admissionRepository.GetAllAdmission().ToList();
                // var admissionslot = admissionRepository.GetAllAdmissionSlot().ToList();
                var admissionsource = admissionRepository.GetAllAdmissionSource()
                    .ToList();
                var admissionstudent = admissionStudentRepository.
                    ListAllAsyncIncludeAll().Result.ToList();
                var admissionstudentstandard = admissionStudentStandardRepository.
                    ListAllAsyncIncludeAll().Result.ToList();
                var academicstandwing = academicStandardWingRepository.
                    ListAllAsyncIncludeAll().
                    Result.Where(a => a.Active == true).ToList();
                var acdemicstandard = academicStandardRepository.ListAllAsyncIncludeAll().
                    Result.ToList();
                var boardstandard = boardStandardRepository.ListAllAsyncIncludeAll().Result.ToList();
                var standard = StandardRepository.GetAllStandard().ToList();
                var admissionstudentcontact = admissionStudentContactRepository.ListAllAsyncIncludeAll().Result.ToList();
                var acdemicsession = academicSessionRepository.ListAllAsyncIncludeAll().Result.ToList();
                var organizationAcademics = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.ToList();
                var organization = organizationRepository.GetAllOrganization().ToList();
                var Counselorlist = admissionStudentCounselorRepository.ListAllAsyncIncludeAll().Result;


                var res = (from a in admission
                           join b in admissionstudent on a.ID equals b.AdmissionId
                           join c in admissionstudentstandard on b.ID equals c.AdmissionStudentId
                           join d in academicstandwing on c.AcademicStandardWindId equals d.ID
                           join e in acdemicstandard on d.AcademicStandardId equals e.ID
                           join m in organizationAcademics on e.OrganizationAcademicId equals m.ID
                           join n in organization on m.OrganizationId equals n.ID
                           join f in boardstandard on e.BoardStandardId equals f.ID
                           join g in standard on f.StandardId equals g.ID
                           join k in admissionstudentcontact on b.ID equals k.AdmissionStudentId
                           join l in acdemicsession on a.AcademicSessionId equals l.ID


                           select new BasicStudentDetailsModel
                           {
                               //InsertedDate=a.InsertedDate,
                               ID = a.ID,
                               FormNumber = a.FormNumber != null ? a.FormNumber : "",
                               OrganisationAcademicId = m.ID,
                               AcademicStandardId = e.ID,
                               AdmissionStudentId = b.ID,
                               LeadReferenceId = a.LeadReferenceId.ToString(),
                               InsertedDate = a.InsertedDate,
                               ModifiedDate = a.ModifiedDate,
                               Fullname = b.FirstName + " " + b.MiddleName != null ? b.MiddleName + " " : "" +
                               b.LastName,
                               FirstName = b.FirstName,
                               LastName = b.LastName,
                               Mobile = a.MobileNumber,
                               WingId = d.WingID.Value,
                               AcademicStandardWingId = d.ID,
                               BoardName = f.BoardId != 0 ? boardRepository.GetBoardById(f.BoardId).Name : "NA",
                               StandardName = g.Name != null ? g.Name : "NA",
                               WingName = d.WingID.Value != 0 ? wingRepository.GetByIdAsync(d.WingID.Value).Result.Name : "NA",
                               EmergencyName = k.FullName,
                               EmergencyEmail = k.EmailId,
                               AcademicSessionName = l.DisplayName,
                               AcademicSessionId = a.AcademicSessionId,
                               EmergencyMobile = k.Mobile,
                               OrganisationName = n.Name != null ? n.Name : "NA",
                               Status = a.Status,
                               AdmissionSlotId = a.AdmissionSlotId,
                               StudentCounselorId = GetCounselorId(b.ID),
                               StudentCounselorName = GetCounselorName(b.ID),
                               //AdmissionSlotName = a.AdmissionSlotId > 0 ? admissionslot.Where(y => y.ID == a.AdmissionSlotId).FirstOrDefault().SlotName : " ",
                               AcademicSourceName = a.AcademicSourceId > 0 ? admissionsource.Where(y => y.ID == a.AcademicSourceId).FirstOrDefault().Name : "NA",
                               // ExamDate= GetStudentExamDate(b.ID),
                               FormPaymentId = GetAdmissionFormPaymentId(a.ID)
                           }
                           ).Distinct().ToList();
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public long GetAdmissionFormPaymentId(long id)
        {
            var res = admissionFormPaymentRepository.ListAllAsync().
                                  Result.Where(x => x.AdmissionId == id).FirstOrDefault();

            return res != null ? res.ID : 0;
        }
        public string GetCounselorName(long admissionStudentId)
        {
            if (admissionStudentId != 0)
            {
                var emplist = admissionStudentCounselorRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AdmissionStudentId == admissionStudentId).FirstOrDefault();
                if (emplist != null)
                {
                    long empid = admissionStudentCounselorRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AdmissionStudentId == admissionStudentId).FirstOrDefault().EmployeeId;
                    if (empid != 0)
                    {
                        var employee = employeeRepository.GetAllEmployee().Where(a => a.ID == empid).FirstOrDefault();
                        return employee.FirstName + "" + employee.LastName;
                    }
                    else
                    {
                        return " ";
                    }
                }
                else
                {
                    return " ";
                }
            }
            else
            {
                return " ";
            }

        }
        public long GetCounselorId(long admissionStudentId)
        {
            if (admissionStudentId != 0)
            {
                var empid = admissionStudentCounselorRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AdmissionStudentId == admissionStudentId).FirstOrDefault();
                if (empid != null)
                {
                    var employee = employeeRepository.GetAllEmployee().Where(a => a.ID == empid.EmployeeId).FirstOrDefault();
                    if (employee != null)
                    {
                        return employee.ID;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }

        }
        public string GetStudentExamDate(long admissionStudentId)
        {
            if (admissionStudentId != 0)
            {
                var empdatelist = admissionStudentExamRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AdmissionStudentId == admissionStudentId).FirstOrDefault();
                if (empdatelist != null)
                {
                    var empdt = admissionStudentExamRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AdmissionStudentId == admissionStudentId).FirstOrDefault().ExamDate;
                    if (empdt != null)
                    {
                        return empdt.Value.ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        return " ";
                    }
                }
                else
                {
                    return " ";
                }
            }
            else
            {
                return " ";
            }

        }


        #region -------------Student Details By StudentId-------------
        public AdmissionViewModel GetStudentDetailsByStudentId(long id)
        {
            var admission = (from a in admissionRepository.GetAllAdmission()                               
                             join c in admissionRepository.GetAllAdmissionSource()
                             on a.AcademicSourceId equals c.ID
                             join d in academicSessionRepository.GetAllAcademicSession()
                            on a.AcademicSessionId equals d.ID
                             join e in admissionStudentRepository.ListAllAsync().Result.ToList()
                              on a.ID equals e.AdmissionId

                             join g in admissionStudentStandardRepository.ListAllAsync().Result.ToList()
                            on e.ID equals g.AdmissionStudentId

                             join k in admissionStudentContactRepository.ListAllAsyncIncludeAll().Result.Where(z=>z.ConatctType== "emergency").ToList()
                             on e.ID equals k.AdmissionStudentId
                             where a.ID == id
                             select new AdmissionViewModel()
                             {
                                 AdmissionId = a.ID,
                                 FormNumber = a.FormNumber != "" && a.FormNumber != null ? a.FormNumber : "NA",
                                 Mobile = a.MobileNumber,
                                 EmergencyContactPerson=k.FullName!=null?k.FullName:"NA",
                                 EmailId = k.EmailId,
                                 Status = a.Status,
                                 SourceId = c.ID,
                                 SourceName = c.Name,
                                 InserteDate = a.InsertedDate,
                                 AdmissionStudentId = e.ID,
                                 FullName = e.FirstName + " " + e.LastName,
                                 Image = e.Image != null ? e.Image : "",
                                 GetExamDetails = GetExamDetails(a.ID, g.AcademicStandardWindId,
                                 e.ID, a.IsExamQualified == null ? "NA" :
                                 a.IsExamQualified == false ? "NotQualified" : "Qualified"),
                                 AcademicSessionId = d.ID,
                                 AcademicSessionName = d.DisplayName,
                                 FormPayment = GetFormPaymentDetails(a.ID),
                                 ClassDetails = GetAdmissionClassDetails(e.ID),
                                 GetPIList = GetPiDetailsList(e.ID),
                                 GetDocumentList = GetDocumentDetailsList(e.ID),
                                 files = GetFileDetails(e.ID),
                                 Counsellor = GetCounsellorDetails(e.ID),
                                 StudentTimeLine = GetStudentTimeLine(e.ID)
                             }
                           ).FirstOrDefault();
            return admission;
        }

        #endregion

        #region----------------method----------------------------

        public (bool? ExamRequired, List<studentdashboardexamcls> ob,bool? show) GetExamDateDetails(long admissionid, long academicwingid, long admissionstudentid, string qualified)
        {
            var acstdid = academicStandardWingRepository.GetByIdAsync(academicwingid).Result;
            if (acstdid == null)
            {
                return (false, null,false);
            }
            var AcdBoardStandard = academicStandardRepository.GetByIdAsync(acstdid.AcademicStandardId).Result;
            if (acstdid == null)
            {
                return (false, null, false);
            }
            var OrganizationAcademic = organizationAcademicRepository.GetByIdAsync(AcdBoardStandard.OrganizationAcademicId).Result;

            var admissionExam = admissionRepository.GetAllAdmissionStandardSetting().Where(a => a.BoardStandardId ==
                AcdBoardStandard.BoardStandardId &&
                a.OrganizationId == OrganizationAcademic.OrganizationId
                && a.AcademicSessionId == OrganizationAcademic.AcademicSessionId
                && a.Active == true).FirstOrDefault();
            if (admissionExam == null)
            {
                return (false, null, false);
            }
            var standardExam = admissionStandardExamRepository.ListAllAsync().Result;
            var studentExam = admissionStudentExamRepository.ListAllAsync().Result;
            var exam = admissionExamDateAssign.ListAllAsync().Result.Where(a => a.AdmissionStudentId ==
                   admissionstudentid && a.Active == true).FirstOrDefault();
            List<studentdashboardexamcls> odd = new List<studentdashboardexamcls>();
            if (exam != null)
            {
                var classExam = studentExam.Where(a => a.AdmissionStudentId == admissionstudentid && a.AdmissionStandardExamId==exam.AdmissionStandardExamId && a.Active == true).ToList();
                if (classExam.Count>0)
                {
                    foreach(var m in classExam)
                    {
                        var standardex = standardExam.Where(a => a.ID == exam.AdmissionStandardExamId).FirstOrDefault();
                        studentdashboardexamcls sm = new studentdashboardexamcls();
                        sm.AppearedDate = m.AppearedDate.Value.ToString("dd-MM-yyyy");
                        sm.EndTime = DateTime.ParseExact(standardex.ExamEndTime.Value.ToString(), "HH:mm", null).ToString("hh:mm tt");
                        sm.ExamDate = standardex.ExamDate.Value.ToString("dd-MM-yyyy");
                        sm.Qualified = m.IsQualified == null ? "" : (m.IsQualified.Value == true ?"Yes":"No");
                        sm.IsAppeared = m.IsAppeared == null ? "" : (m.IsAppeared.Value == true ?"Yes":"No");
                        sm.ReachTime = DateTime.ParseExact(standardex.ReachTime.Value.ToString(), "HH:mm", null).ToString("hh:mm tt");
                        sm.ResultDate= standardex.ResultDate.Value.ToString("dd-MM-yyyy");
                        sm.StartTime= DateTime.ParseExact(standardex.ExamStartTime.Value.ToString(), "HH:mm", null).ToString("hh:mm tt");
                        sm.Score= m.Score;
                        sm.RollNo= exam.RollNo;
                        odd.Add(sm);
                    }
                    
                }
                else
                {
                    var standardex = standardExam.Where(a => a.ID == exam.AdmissionStandardExamId).FirstOrDefault();
                    studentdashboardexamcls sm = new studentdashboardexamcls();
                    sm.AppearedDate = "";
                    sm.EndTime = DateTime.ParseExact(standardex.ExamEndTime.Value.ToString().Substring(0,5), "HH:mm", null).ToString("hh:mm tt");
                    sm.ExamDate = standardex.ExamDate.Value.ToString("dd-MM-yyyy");
                    sm.Qualified = "";
                    sm.IsAppeared = "";
                    sm.ReachTime = DateTime.ParseExact(standardex.ReachTime.Value.ToString().Substring(0, 5), "HH:mm", null).ToString("hh:mm tt");
                    sm.ResultDate = standardex.ResultDate.Value.ToString("dd-MM-yyyy");
                    sm.StartTime = DateTime.ParseExact(standardex.ExamStartTime.Value.ToString().Substring(0, 5), "HH:mm", null).ToString("hh:mm tt");
                    sm.RollNo = exam.RollNo;
                    odd.Add(sm);
                }
                return (true, odd, false);
            }
            else
            {
                var standardex = standardExam.Where(a => a.BoardStandardId==admissionExam.BoardStandardId && a.AcademicSessionId==admissionExam.AcademicSessionId && a.WingId==academicwingid).FirstOrDefault();
                return (admissionExam.ExamRequired, null, standardex==null?false:(standardex.PublishStartDate==null?false:((standardex.PublishStartDate.Value.Date<=DateTime.Now && standardex.PublishEndDate.Value.Date>=DateTime.Now.Date)?true:false)));
            }
        }







        public (bool? ExamRequired, string ExamDate, string AppearedDate, string ResultDate,
            string StartTime, string EndTime, string ReachTime, string Qualified, int show)
            GetExamDetails(long admissionid, long acdemicStdWingId, long admissionStudentId, string qualified)
        {

            var acstdid = academicStandardWingRepository.
                GetByIdAsync(acdemicStdWingId).
                Result;
            if (acstdid == null)
            {
                return (false,
                           null, null, null,
                          null,
                           null,
                           null, qualified, 0);
            }
            var AcdBoardStandard = academicStandardRepository.
                GetByIdAsync(acstdid.AcademicStandardId).
                Result;

            if (AcdBoardStandard == null)
            {
                return (false,
                           null, null, null,
                          null,
                           null,
                           null, qualified, 0);
            }

            var OrganizationAcademic = organizationAcademicRepository.
                GetByIdAsync(AcdBoardStandard.OrganizationAcademicId).
                Result;

            var admissionExam = admissionRepository
                .GetAllAdmissionStandardSetting().Where(a => a.BoardStandardId ==
                AcdBoardStandard.BoardStandardId &&
                a.OrganizationId == OrganizationAcademic.OrganizationId
                && a.AcademicSessionId == OrganizationAcademic.AcademicSessionId
                && a.Active == true).FirstOrDefault();

            if (admissionExam != null)
            {
                int show = admissionExam.ExamDatesShow == true ? 1 : 0;
                var exam = admissionExamDateAssign.ListAllAsync().
                    Result.Where(a => a.AdmissionStudentId ==
                    admissionStudentId && a.Active == true)
                    .FirstOrDefault();

                if (exam == null)
                {
                    var classExam =
                         admissionStandardExamRepository.ListAllAsync().
                    Result.Where(a => a.BoardStandardId == AcdBoardStandard.BoardStandardId
                    && a.OrganizationId == OrganizationAcademic.OrganizationId
                     && a.AcademicSessionId == OrganizationAcademic.AcademicSessionId && a.Active == true
                    ).FirstOrDefault();



                    if (classExam != null)
                    {


                        //      var resultExam =
                        //     admissionStandardExamResultDateRepository.ListAllAsync().
                        //Result.Where(a => a.BoardStandardId == AcdBoardStandard.BoardStandardId
                        // && a.OrganisationAcademicId == OrganizationAcademic.ID
                        //  && a.AcademicSessionId == OrganizationAcademic.AcademicSessionId && a.Active == true
                        //).FirstOrDefault();

                        return (admissionExam.ExamRequired,
                     classExam != null ?
                     classExam.ExamDate.Value.ToString("dd-MMM-yyyy") : "NA",
                     "NA",
                       classExam != null ?
                        classExam.ResultDate.Value.ToString("dd-MMM-yyyy") : "NA",
                     classExam != null ?
                            GetDateTimeFormat(classExam.ExamDate.Value,
                            classExam.ExamStartTime.Value) : "NA",
                           classExam != null ? GetDateTimeFormat(classExam.ExamDate.Value,
                            classExam.ExamEndTime.Value) : "NA",
                            classExam != null ? GetDateTimeFormat(classExam.ExamDate.Value,
                            classExam.ReachTime.Value) : "NA", qualified, show);
                    }
                    else
                        return (admissionExam.ExamRequired,
                           "NA", "NA", "NA",
                          "NA",
                           "NA",
                           "NA", qualified, show);
                }
                else
                {

                    var classExam =
                        admissionStandardExamRepository.ListAllAsync().
                   Result.Where(a => a.ID == exam.AdmissionStandardExamId && a.Active == true
                   ).FirstOrDefault();


                    var resultExam =
                        admissionStandardExamResultDateRepository.ListAllAsync().
                   Result.Where(a => a.BoardStandardId == AcdBoardStandard.BoardStandardId
                    && a.OrganisationAcademicId == OrganizationAcademic.ID
                     && a.AcademicSessionId == OrganizationAcademic.AcademicSessionId && a.Active == true
                   ).FirstOrDefault();

                    return (admissionExam.ExamRequired,
                            classExam != null && classExam.ExamDate != null ?
                            classExam.ExamDate.Value.ToString("dd-MMM-yyyy") : "NA",
                            "NA",
                               classExam != null && classExam.ResultDate != null ?
                        classExam.ResultDate.Value.ToString("dd-MMM-yyyy") : "NA",
                            classExam != null ?
                            GetDateTimeFormat(classExam.ExamDate.Value,
                            classExam.ExamStartTime.Value) : "NA",
                           classExam != null ? GetDateTimeFormat(classExam.ExamDate.Value,
                            classExam.ExamEndTime.Value) : "NA",
                            classExam != null ? GetDateTimeFormat(classExam.ExamDate.Value,
                            classExam.ReachTime.Value) : "NA", qualified, show);
                }

            }
            else
            {
                return (false,
                            "NA", "NA", "NA",
                           "NA",
                            "NA",
                            "NA", qualified, 0);
            }


        }



        public (string PiDate, string PiTime,
            string AppearedDate, string Qualified, string InterviewerName,
            string InterviewTakenBy, string score, string remarks)
            GetPiDetails(long Id)
        {

            var pi = admissionPersonalInterviewAssign.GetByIdAsync(Id).Result;          

            if (pi != null)
            {
                return (pi.PIDate != null ?
                               pi.PIDate.Value.ToString("dd-MMM-yyyy") : "NA",
                                pi.PITime != null ? GetDateTimeFormat(pi.PIDate.Value,
                              pi.PITime.Value) : "NA",
                              pi.AppearedDateTime != null ?
                               pi.AppearedDateTime.Value.ToString("dd-MMM-yyyy") : "NA",
                               pi.IsQualified == null ? "NA" : pi.IsQualified == true ? "Qualified" :
                               "Not Qualified",
                               GetPIEmployeeName(pi.InterviewTakenId),
                               pi.IsQualified == null ? "NA" :
                               GetPIEmployeeName(pi.ModifiedId != null ?
                               pi.ModifiedId : 0),
                               pi.Score != null ? pi.Score.Value + "" : "NA",
                              pi.Remarks
                               );
            }
            else
            {
                return ("NA",
                              "NA",
                             "NA",
                               "NA",
                              "NA",
                               "NA",
                                "NA",
                     "NA"
                              );
            }

        }


        public string GetPIEmployeeName(long? intervierId)
        {
            // CheckLoginStatus();

            //long userId = HttpContext.Session.GetInt32("userId").Value;
            // long empId = admissionInterViewerRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == intervierId).Select(a => a.EmployeeId).FirstOrDefault();

            if (intervierId == null || intervierId == 0)
            {
                return "NA";
            }
            //else
            //if (intervierId == userId)
            //    return "Me";
            else
            {
                var emp = employeeRepository.GetEmployeeById(intervierId.Value);
                if (emp != null)
                {
                    //emp.EmpCode + "-" +
                    return emp.FirstName + " " + emp.LastName;
                }
                else
                {
                    return "NA";
                }
            }
        }

        public (string IsQualified, string Score, string ResultDate, string MarksheetFileName)
            StandardStudentExamResult(long admissionStudentId)
        {
            var examResult = admissionStudentExamRepository.ListAllAsync().Result.Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true).FirstOrDefault();
            if (examResult != null)
            {
                return (examResult.IsQualified == null ? "0" : examResult.IsQualified == true ? "True" : "False", examResult.Score == null ? "0" : examResult.Score, examResult.ResultDate != null ? examResult.ResultDate.Value.ToString("dd-MMM-yyyy") : "NA", examResult.MarksheetFileName == null ? "No marksheet Uploaded" : examResult.MarksheetFileName);
            }
            else
            {
                return ("NA",
                             "NA",
                            "NA",
                              "NA"
                             );

            }
        }
        public (string PiDate, string PiTime,
            string AppearedDate, string Qualified, string InterviewerName,
            string remarks
            )
            GetDocumentDetails(long id)
        {

            var pi = admissionDocumentVerificationAssignRepository
            .ListAllAsyncIncludeAll().Result.
            Where(a => a.ID == id ).FirstOrDefault();


            if (pi != null)
            {
                return (pi.VerificationDate != null ?
                               pi.VerificationDate.Value.ToString("dd-MMM-yyyy") : "NA",
                                pi.VerificationTime != null ?
                                GetDateTimeFormat(pi.VerificationDate.Value,
                              pi.VerificationTime.Value) : "NA",
                              pi.AppearedDateTime != null ?
                               pi.AppearedDateTime.Value.ToString("dd-MMM-yyyy") : "NA",
                               pi.IsQualified == null ? "NA" : pi.IsQualified == true ? "Qualified" :
                               "Not Qualified",
                              pi.IsQualified == null ? "NA" : GetDocumentEmployeeName(pi.ModifiedId),
                            pi.Remarks
                               );
            }
            else
            {
                return ("NA",
                              "NA",
                             "NA",
                               "NA",
                              "NA",
                               "NA"
                              );
            }

        }

        public string GetDocumentEmployeeName(long intervierId)
        {
            CheckLoginStatus();
            // long empId = admissionVerifierRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == intervierId).Select(a => a.EmployeeId).FirstOrDefault();

            if (intervierId == 0)
            {
                return "NA";
            }
            else
            if (intervierId == userId)
                return "Me";
            else
            {
                var emp = employeeRepository.GetEmployeeById(intervierId);
                if (emp != null)
                {
                    // emp.EmpCode + "-" + 
                    return emp.FirstName + " " + emp.LastName;
                }
                else
                {
                    return "NA";
                }
            }
        }

        public List<AdmDocumentViewModel> GetFileDetails(long admissionStudentId)
        {
            List<AdmDocumentViewModel> data = new List<AdmDocumentViewModel>();
            var admStd = admissionStudentStandardRepository.ListAllAsync().Result.
                Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true).FirstOrDefault();

            if (admStd != null)
            {
                var AcademicStandardId = academicStandardWingRepository.
                    GetByIdAsync(admStd.AcademicStandardWindId).Result
                    .AcademicStandardId;

                var Docs = admissionDocumentRepository.ListAllAsync().Result
                  .Where(a => a.AcademicStandardId == AcademicStandardId && a.Active == true && a.Status == "ACTIVE");

                data.AddRange((from a in Docs
                               join b in documentTypeRepository.GetAllDocumentTypes()
                               on a.DocumentTypeId equals b.ID
                               join c in documentSubTypeRepository.GetAllDocumentSubTypes()
                             on a.DocumentSubTypeId equals c.ID
                               select new AdmDocumentViewModel
                               {

                                   Name = c.Name,
                                   AdmissionDocumentId = a.ID,
                                   Submit = GetClassDocumentSubmittedInfo(a.ID, admissionStudentId)
                               }
                           ).ToList());

                var pi = admissionDocumentAssignRepository
                           .ListAllAsync().Result.
                           Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true).
                           ToList();

                data.AddRange((from a in pi
                               join b in documentTypeRepository.GetAllDocumentTypes()
                               on a.DocumentTypeId equals b.ID
                               join c in documentSubTypeRepository.GetAllDocumentSubTypes()
                             on a.DocumentSubTypeId equals c.ID
                               select new AdmDocumentViewModel
                               {
                                   Name = c.Name,
                                   AdmissionDocumentAssignId = a.ID,
                                   Submit = GetStudentDocumentSubmittedInfo(a.ID, admissionStudentId)
                               }
                           ).ToList());
            }
            return data;
        }


        public (string submitted, string submittedon,
            string empname, string filename)
            GetClassDocumentSubmittedInfo(long adid, long asid)
        {
            var res = admissionStudentDocumentRepository.
                             ListAllAsync().Result.Where(a => a.AdmissionDocumentId == adid
                             && a.AdmissionStudentId == asid).FirstOrDefault();

            if (res == null)
            {
                return ("PENDING", "NA", "NA", "NA");

            }
            else
            {
                return (res.Status,
               res.SubmittedDate == null ? "NA" :
               res.SubmittedDate.Value.ToString("dd-MMM-yyyy"),
               GetEmployeeName(res.ModifiedId), res.FileName == null ? "NA" : res.FileName
               );
            }
        }
        public (string submitted, string submittedon, string empname, string filename) GetStudentDocumentSubmittedInfo(long adid, long asid)
        {
            var res = admissionStudentDocumentRepository.
                             ListAllAsync().Result.Where(a => a.AdmissionDocumentAssignId == adid
                             && a.AdmissionStudentId == asid).FirstOrDefault();

            if (res == null)
            {
                return ("PENDING", "NA", "NA", "NA");

            }
            else
            {
                return (res.IsSubmitted == null ? "PENDING" :
                   res.IsSubmitted.Value == false ? "PENDING" : "SUBMITTED",
               res.SubmittedDate == null ? "NA" :
               res.SubmittedDate.Value.ToString("dd-MMM-yyyy"), GetEmployeeName(res.ModifiedId), res.FileName == null ? "NA" : res.FileName);
            }
        }


        public (string name, string assignedon, string remarks, string Status) GetCounsellorDetails(long admissionStudentId)
        {
            var res = admissionStudentCounsellorRepository.ListAllAsync().
                Result.Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true).FirstOrDefault();
            var counselloerstatus = counsellorStatusRepository.ListAllAsyncIncludeAll().Result.ToList();
            if (res != null)
            {
                var counsellor = admissionCounsellorRepository.ListAllAsync().
                Result.Where(a => a.ID == res.EmployeeId).FirstOrDefault();

                if (counsellor == null)
                {
                    return ("NA", res.AssignedDate.ToString("dd-MMM-yyyy"), res.Remarks, "NA");
                }
                else
                {
                    string counsellorstatus = res.CounsellorStatusId != null ? counselloerstatus.Where(z => z.ID == res.CounsellorStatusId).FirstOrDefault().Name : "NA";
                    return (GetEmployeeName(counsellor.EmployeeId),
                        res.AssignedDate.ToString("dd-MMM-yyyy"), res.Remarks, counsellorstatus);
                }

            }
            else
            {
                return ("NA", "NA", "NA", "NA");
            }


        }

        public string GetEmployeeName(long empId)
        {
            CheckLoginStatus();

            if (empId == 0)
            {
                return "NA";
            }
            else
            if (empId == userId)
                return "Me";
            else
            {
                var emp = employeeRepository.GetEmployeeById(empId);
                if (emp != null)
                {
                    return emp.EmpCode + "-" + emp.FirstName + " " + emp.LastName;
                }
                else
                {
                    return "NA";
                }
            }
        }

        public string GetDateTimeFormat(DateTime d, TimeSpan span)
        {

            DateTime time = d.Add(span);
            return time.ToString("hh:mm tt");
        }

        public (int status, string TransactionId,
            string ModeOfPayment, string PaidOn, string RecievedEmployeeName,
            long Amount
           ) GetFormPaymentDetails(long admissionid)
        {
            var admissionFormPayment = admissionFormPaymentRepository.ListAllAsync().Result
                .Where(a => a.AdmissionId == admissionid).FirstOrDefault();
            if (admissionFormPayment == null)
            {
                return (0, null, null, null, null, 0);
            }
            else
            {

                var emp = employeeRepository. GetEmployeeById(admissionFormPayment.ReceivedId);
                long amm = admissionFeeRepository.GetByIdAsync(admissionFormPayment.AdmissionFeeId).Result!=null? admissionFeeRepository.GetByIdAsync(admissionFormPayment.AdmissionFeeId).Result.Ammount:0;
                string paidOn = admissionFormPayment.InsertedDate.ToString("dd-MMM-yyyy");
                string modeOfpayment = paymentType.GetPaymentTypeById(admissionFormPayment.PaymentModeId)!=null? paymentType.GetPaymentTypeById(admissionFormPayment.PaymentModeId).Name:"NA";
                string transactionId = admissionFormPayment.TransactionId!=null? admissionFormPayment.TransactionId:"NA";

                return (1, transactionId, modeOfpayment, paidOn, emp == null ? "NA" : emp.EmpCode + "-" + emp.FirstName + " " + emp.LastName, amm);
            }

        }

        public (string boardName, string className,
            string schoolName, string wingName,
            long boardId,
            long standardId,
             long schoolId,
            long wingId
           ) GetAdmissionClassDetails(long studentadmissionId)
        {
            var data = amissionStudentCommonMethod.GetStudentIdData(studentadmissionId);

            var standard = standardRepository.GetStandardById(data.standardId);
            var org = organizationRepository.GetOrganizationById(data.organizationId);
            var wing = wingRepository.GetByIdAsync(data.wingId).Result;
            var bstd = boardRepository.GetBoardById(data.boardId);

            return (bstd != null ? bstd.Name : "NA", standard != null ? standard.Name : "NA",
                org != null ? org.Name : "NA", wing != null ? wing.Name : "NA",
               bstd != null ? bstd.ID : 0, standard != null ? standard.ID : 0,
               org != null ? org.ID : 0, wing != null ? wing.ID : 0);
        }
        #endregion--------------------

        #region------------------student Timeline---------------
        public List<StudentTimeLineModal> GetStudentTimeLine(long AdmissionStudentId)
        {
            try
            {
                long admissionid = admissionStudentRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AdmissionStudentId).Select(a => a.AdmissionId).FirstOrDefault();
                if (admissionid != 0)
                {
                    var timeline = admissionRepository.GetAllAdmissionTimeline().Where(a => a.AdmissionId == admissionid).ToList();
                    var employee = employeeRepository.GetAllEmployee().ToList();
                    var list = (from a in timeline

                                select new StudentTimeLineModal
                                {
                                    AdmissionId = a.AdmissionId,
                                    Status = a.Status,
                                    Timeline = a.Timeline,
                                    InsetedDate = NullableDateTimeToStringDate(a.InsertedDate),
                                    EmployeeName = a.InsertedId == 0  ?"Parent": (a.InsertedId == 1?"Super Admin": employee.Where(z => z.ID == a.InsertedId).Select(z => new { name=z.FirstName+""+z.LastName}).FirstOrDefault().name)
                                }).ToList();
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        #endregion

        #region------------------student FileDocument---------------
        public List<StudentFileDocumentModal> GetStudentFileDocument(long AdmissionStudentId)
        {
            try
            {
                var studentdoc = admissionStudentDocumentRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == AdmissionStudentId);
                var admissiondoc = admissionDocumentRepository.ListAllAsyncIncludeAll().Result;
                var doctype = documentTypeRepository.GetAllDocumentTypes();
                var subtype = documentSubTypeRepository.GetAllDocumentSubTypes();
                var list = (from a in studentdoc
                            join b in admissiondoc on a.AdmissionDocumentId equals b.ID
                            join c in subtype on b.DocumentSubTypeId equals c.ID
                            select new StudentFileDocumentModal
                            {
                                ID = a.ID,
                                SubtypeName = c.Name
                            }).ToList();
                return list;


            }
            catch (Exception ex)
            {
                return null;
            }

        }
        #endregion


        #region------------------GetPiDetailsList And GetDocumentDetailsList

        public IList<PiModelClass> GetPiDetailsList(long admissionStudentId)
        {
            var pilist = admissionPersonalInterviewAssign.ListAllAsync().Result.Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true).ToList();
            var list = (from a in pilist
                        select new PiModelClass
                        {
                            ID = a.ID,
                            PiTimespan = a.PITime,
                            PiDateInDate = a.PIDate,
                            PiDate = a.PIDate.Value != null ? a.PIDate.Value.ToString("dd-MMM-yyyy") : "NA",
                            PiTime = a.PITime != null ? GetDateTimeFormat(a.PIDate.Value, a.PITime.Value) : "NA",
                            AppearedDate = a.AppearedDateTime != null ? a.AppearedDateTime.Value.ToString("dd-MMM-yyyy") : "NA",
                            AppearedStatus=a.AppearedDateTime==null?a.PIDate.Value.Date<DateTime.Now.Date?"Not Appeared":"Pending":"Appeared",
                            Qualified = a.IsQualified == null ? "NA" : a.IsQualified == true ? "Qualified" : "Not Qualified",
                            InterviewerName = GetPIEmployeeName(a.InterviewTakenId),
                            InterviewTakenBy = GetPIEmployeeName(a.ModifiedId != 0 ? a.ModifiedId : 0),
                            score = a.Score != null ? a.Score.Value + "" : "NA",
                            remarks = a.Remarks != null && a.Remarks != "" ? a.Remarks : "NA",
                             active= a.PIDate.Value.Date >= DateTime.Now.Date?1:0
            

               
            
        });



            return list.ToList();
        }

        public IList<PiModelClass> GetDocumentDetailsList(long admissionStudentId)
        {

            var pilist = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.
            Where(a => a.AdmissionStudentId == admissionStudentId).ToList();
            var list = (from a in pilist
                        select new PiModelClass
                        {
                            PiTimespan = a.VerificationTime,
                            PiDateInDate = a.VerificationDate,
                            ID = a.ID,
                            PiDate = a.VerificationDate != null ? a.VerificationDate.Value.ToString("dd-MMM-yyyy") : "NA",
                            PiTime = a.VerificationTime != null ? GetDateTimeFormat(a.VerificationDate.Value, a.VerificationTime.Value) : "NA",
                            AppearedDate = a.AppearedDateTime != null ? a.AppearedDateTime.Value.ToString("dd-MMM-yyyy") : "NA",
                            AppearedStatus = a.AppearedDateTime == null ? a.VerificationDate.Value.Date < DateTime.Now.Date ? "Not Appeared" : "Pending" : "Appeared",
                            Qualified = a.IsQualified == null ? "NA" : a.IsQualified == true ? "Qualified" : "Not Qualified",
                            InterviewerName = a.IsQualified == null ? "NA" : GetDocumentEmployeeName(a.ModifiedId),

                            remarks = a.Remarks!=null && a.Remarks!=""?a.Remarks:"NA",
                           active = a.VerificationDate.Value.Date >= DateTime.Now.Date ? 1 : 0


                        });
            return list.ToList();

        }
        #endregion

        #region-----------------GetAdmissionByLeadReferenceId------------------------
        public AdmissionStudentViewModel GetAdmissionByLeadReferenceId(string id,
         bool loginStatus, bool admissionLoginStatus)
        {



            AdmissionStudentViewModel model = new AdmissionStudentViewModel();

            var guid = id;
            if (guid == null || guid.Equals(Guid.Empty))
            {
                model.error = true;
                return model;
            }

            var admission = admissionRepository.GetAdmissionByLeadReferenceId(id);
            if (admission == null)
            {
                model.error = true;
                return model;
            }


            var admissionSource = admissionRepository.
                GetAdmissionSourceById(admission.AcademicSourceId);
            if (admissionSource == null)
            {
                model.error = true;
                return model;
            }

            var admissionSession = academicSessionRepository.
                GetByIdAsync(admission.AcademicSessionId)
                .Result;
            if (admissionSession == null)
            {
                model.error = true;
                return model;
            }

            var admStudent = admissionStudentRepository.
                GetByAdmissionId(admission.ID).Result;
            if (admStudent == null)
            {
                model.error = true;
                return model;
            }

            var studentStandard =
                admissionStudentStandardRepository.GetByStudentId(admStudent.ID).Result;
            if (studentStandard == null)
            {
                model.error = true;
                return model;
            }
            var studentContact = admissionStudentContactRepository.GetStudentById(admStudent.ID).Result;
            var admissiondata = admissionSqlQuery.GetView_Student_Admission_List().Where(a=>a.ID== admission.ID).FirstOrDefault();


            if (studentContact == null)
            {
                model.error = true;
                return model;
            }

            AdmissionViewModel data = new AdmissionViewModel()
            {

                AdmissionId = admission.ID,
                FormNumber = admission.FormNumber != null &&
                                 admission.FormNumber != "" ? admission.FormNumber : "",
                Mobile = admission.MobileNumber,
                EmailId = studentContact.EmailId,
                ContactName = studentContact.FullName,
                Status = admission.Status,
                SourceId = admissionSource.ID,
                SourceName = admissionSource.Name,
                InserteDate = admission.InsertedDate,
                AdmissionStudentId = admStudent.ID,
                FullName = UtilityModel.GetFullName(admStudent.FirstName,
                                 admStudent.MiddleName, admStudent.LastName),
                Image = admStudent.Image,
                GetExamDet = GetExamDateDetails(admStudent.AdmissionId,
                                 studentStandard.AcademicStandardWindId,
                                admStudent.ID, admission.IsExamQualified == null ? "NA" :
                                 admission.IsExamQualified == false ? "NotQualified" : "Qualified"),
                AcademicSessionId = admissionSession.ID,
                LeadReferenceId = admission.LeadReferenceId.ToString(),
                AcademicSessionName = admissionSession.DisplayName,
                FormPayment = GetFormPaymentDetails(admission.ID),
                ClassDetails = GetStandardwingDetails(admission.ID),
                GetPIList = GetPiDetailsList(admStudent.ID),
                GetDocumentList = GetDocumentDetailsList(admStudent.ID),
                ResultDetails = StandardStudentExamResult(admStudent.ID),
                files = GetFileDetails(admStudent.ID),
                Counsellor = GetCounsellorDetails(admStudent.ID),
                StudentTimeLine = GetStudentTimeLine(admStudent.ID),
                all_Admission= admissiondata
            };


            if (data == null)
            {
                model.error = true;
                return model;
            }
            var det = admitcardDetailsRepository.GetAllAdmitcardDetails().Where(a => a.Active == true).ToList();
            var instructionsModel = admitcardInstructionsRepository.GetAllAdmicardInstructions();
            var admitcardVenue = admitcardVenueRepository.GetAllAdmitcardVenue();
            var sessions = academicSessionRepository.ListAllAsync().
                Result.Where(a => a.Active == true && a.IsAdmission == true).ToList();
            var acdorganisation = organizationAcademicRepository.ListAllAsync().Result;
            var organisation = organizationRepository.GetAllOrganization();
            var res = (from a in det
                       join b in instructionsModel on a.ID equals b.AdmitcardDetailsId
                       join c in admitcardVenue on a.ID equals c.AdmitcardDetailsId
                       join d in acdorganisation on a.OrganisationAcademicId equals d.ID
                       join e in organisation on d.OrganizationId equals e.ID
                       join f in sessions on d.AcademicSessionId equals f.ID
                       where a.OrganisationAcademicId == admissiondata.OrganisationAcademicId
                       select new AdmitcardMastercls
                       {
                           Address = a.Address,
                           ID = a.ID,
                           Instruction = b.Instruction,
                           LogoName = a.LogoName,
                           OrganisationAcademicId = a.OrganisationAcademicId,
                           OrganisationName = c.OrganisationName,
                           Organization = e.Name,
                           sessionid = f.ID,
                           SessionName = f.DisplayName,
                           Title = a.Title,
                           Tollfree = a.Tollfree,
                           VenueAddress = c.Address,
                           Remarks = a.Remarks
                       }).FirstOrDefault();
            model.admitcard = res;
            model.admissionViewModel = data;
            model.GetCommonFileDetailsList = GetCommonFileDetails(admStudent.ID);
            model.GetExtraFileDetailsList = GetExtraFileDetails(admStudent.ID);
            model.StudentExamDeatilsModel = GetStudentExamDetails(admissiondata.StandardId.Value);



            return model;
        }
        public (string boardName, string className,
           string schoolName, string wingName,
           long boardId,
           long standardId,
            long schoolId,
           long wingId
          ) GetStandardwingDetails(long studentadmissionId)
        {
            var data = admissionSqlQuery.GetView_Student_Admission_List().Where(a => a.ID == studentadmissionId).FirstOrDefault();


            return (data.BoardId != null ? data.BoardName : "NA", data.StandardId != null ? data.StandardName : "NA",
                data.OrganisationAcademicId != null ? data.OrganisationName : "NA", data.WingId != null ? data.WingName : "NA",
               data.BoardId != null ? data.BoardId.Value : 0, data.StandardId != null ? data.StandardId.Value : 0,
               data.OrganisationAcademicId != null ? data.OrganisationAcademicId.Value : 0, data.WingId != null ? data.WingId.Value : 0);
        }
        public List<AdmDocumentViewModel> GetCommonFileDetails(long admissionStudentId)
        {
            List<AdmDocumentViewModel> data = new List<AdmDocumentViewModel>();
            var admStd = admissionStudentStandardRepository.ListAllAsync().Result.
                Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true).FirstOrDefault();

            if (admStd != null)
            {
                var AcademicStandardId = academicStandardWingRepository.
                    GetByIdAsync(admStd.AcademicStandardWindId).Result
                    .AcademicStandardId;

                var Docs = admissionDocumentRepository.ListAllAsync().Result
                  .Where(a => a.AcademicStandardId == AcademicStandardId && a.Active == true && a.Status == "ACTIVE");

                var DocsAssigned = admissionDocumentAssignRepository.ListAllAsync().Result
                 .Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true && a.Status == "ACTIVE");

                data.AddRange((from a in Docs
                               join b in documentTypeRepository.GetAllDocumentTypes()
                               on a.DocumentTypeId equals b.ID
                               join c in documentSubTypeRepository.GetAllDocumentSubTypes()
                               on a.DocumentSubTypeId equals c.ID
                               select new AdmDocumentViewModel
                               {

                                   Name = c.Name,
                                   DocumentSubTypeId = c.ID,
                                   Submit = GetClassDocumentSubmittedInfo(a.ID, admissionStudentId)
                               }
                           ).ToList());

                data.AddRange((from a in DocsAssigned
                               join b in documentTypeRepository.GetAllDocumentTypes()
                               on a.DocumentTypeId equals b.ID
                               join c in documentSubTypeRepository.GetAllDocumentSubTypes()
                               on a.DocumentSubTypeId equals c.ID
                               select new AdmDocumentViewModel
                               {

                                   Name = c.Name,
                                   DocumentSubTypeId = c.ID,
                                   Submit = GetClassDocumentSubmittedInfo(a.ID, admissionStudentId)
                               }
                          ).ToList());

            }
            return data;
        }
        public List<AdmDocumentViewModel> GetExtraFileDetails(long admissionStudentId)
        {
            var documenttype = documentTypeRepository.GetAllDocumentTypes().ToList().Where(a => a.Name == "ADMISSION").Select(a => a.ID);
            var documentsubtype = documentSubTypeRepository.GetAllDocumentSubTypes()
                .Where(a => documenttype.Contains(a.DocumentTypeID)).ToList();

            var AdmissionStudentData = GetStudentIdData(admissionStudentId);
            HashSet<long> uniqueSubTypeIds = new HashSet<long>();
            var documentSubTypeIds = admissionDocumentRepository.ListAllAsync()
                 .Result.Where(a => a.AcademicStandardId == AdmissionStudentData.academicStandardId
                 && a.AdmissionSessionId == AdmissionStudentData.sessionId && a.Active == true)
                   .Select(a => a.DocumentSubTypeId).Distinct().ToList();

            var documentSubTypeIdsAssigned = admissionDocumentAssignRepository.ListAllAsync()
              .Result.Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true
              )
                .Select(a => a.DocumentSubTypeId).Distinct().ToList();

            foreach (var a in documentSubTypeIds)
            {
                uniqueSubTypeIds.Add(a);
            }

            foreach (var a in documentSubTypeIdsAssigned)
            {
                uniqueSubTypeIds.Add(a);
            }


            var documentsNotInAdmissionDocument =
                documentsubtype.Where(a => !uniqueSubTypeIds.ToList().Contains(a.ID)).ToList();
            List<AdmDocumentViewModel> data = new List<AdmDocumentViewModel>();
            if (documentsNotInAdmissionDocument != null)
            {
                data.AddRange((from a in documentsNotInAdmissionDocument
                               join b in documentTypeRepository.GetAllDocumentTypes()
                               on a.DocumentTypeID equals b.ID
                               select new AdmDocumentViewModel
                               {
                                   Name = a.Name,
                                   DocumentSubTypeId = a.ID,
                                   DocumentTypeId = b.ID
                                   //   Submit = GetStudentDocumentSubmittedInfo(a.ID, admissionStudentId)
                               }
                           ).ToList());
            }
            return data;
        }
        public (long sessionId, long organizationId, long boardId,
        long standardId, long wingId, long academicStandardId, long boardStandardId,
        long boardStandardWingId, long organizationAcademicId)
        GetStudentIdData(long admissionStudentId)
        {
            long sessionId = 0;
            long organizationId = 0;
            long boardId = 0;
            long standardId = 0;
            long wingId = 0;
            long academicStandardId = 0;
            long boardStandardId = 0;

            long boardStandardWingId = 0;
            long organizationAcademicId = 0;

            if (admissionStudentId != 0)
            {


                var admission = admissionStudentRepository.
                    ListAllAsyncIncludeAll()
                    .Result.Where(a => a.ID == admissionStudentId).FirstOrDefault();
                if (admission != null)
                {

                    var admissionStudentStandard = admissionStudentStandardRepository.
                        ListAllAsyncIncludeAll()
                       .Result.
                       Where(a => a.AdmissionStudentId == admissionStudentId).FirstOrDefault();



                    if (admissionStudentStandard != null)
                    {
                        var academicStandardWing = academicStandardWingRepository.
                        GetByIdAsyncIncludeAll(admissionStudentStandard.AcademicStandardWindId
                        )
                        .Result;

                        if (academicStandardWing != null)
                        {
                            academicStandardId = academicStandardWing.AcademicStandardId;
                            boardStandardWingId = academicStandardWing.BoardStandardWingID;
                            wingId = academicStandardWing.WingID != null ? academicStandardWing.WingID.Value : 0;


                            var boardStandardWing = boardStandardWingRepository.
                            GetByIdAsyncIncludeAll(academicStandardWing.BoardStandardWingID).Result;

                            if (academicStandardWing != null)
                            {
                                boardStandardId = boardStandardWing.BoardStandardID;

                                var boardStandard = boardStandardRepository.
                                GetByIdAsyncIncludeAll(boardStandardWing.BoardStandardID).Result;

                                if (boardStandard != null)
                                {
                                    boardId = boardStandard.BoardId;
                                    standardId = boardStandard.StandardId;

                                    var academicStandard = academicStandardRepository.
                                    GetByIdAsyncIncludeAll(academicStandardWing.AcademicStandardId).Result;

                                    if (academicStandard != null)
                                    {
                                        //  academicStandardId = academicStandard.ID;

                                        var organizationAcademic = organizationAcademicRepository.
                                            GetByIdAsync(academicStandard.OrganizationAcademicId).Result;

                                        if (organizationAcademic != null)
                                        {
                                            organizationAcademicId = organizationAcademic.ID;
                                            sessionId = organizationAcademic.AcademicSessionId;
                                            organizationId = organizationAcademic.OrganizationId;
                                        }

                                    }
                                }
                            }




                        }
                    }

                }

                return (sessionId, organizationId, boardId,
             standardId, wingId, academicStandardId, boardStandardId,
             boardStandardWingId, organizationAcademicId);

            }
            else
            {
                return (0, 0, 0, 0, 0, 0, 0, 0, 0);
            }


        }

        public List<StudentExamDeatilsModel> GetStudentExamDetails(long standardid)
        {
            List<StudentExamDeatilsModel> list = null;
            if (standardid != 0)
            {
                var StandardExam = admissionStandardExamRepository.ListAllAsyncIncludeAll().Result.Where(a=>a.PublishStartDate!=null).ToList();
                var boardStandard = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.StandardId == standardid).ToList();
                var stanadrd = standardRepository.GetAllStandard().ToList();
                list = (from a in StandardExam
                        join b in boardStandard on a.BoardStandardId equals b.ID
                        join c in stanadrd on b.StandardId equals c.ID
                        where (a.PublishStartDate.Value.Date<=DateTime.Now.Date && a.PublishEndDate.Value.Date>=DateTime.Now.Date)?true:false
                        select new StudentExamDeatilsModel
                        {
                            AdmissionStandardExamId = a.ID,
                            ExamStartTime = a.ExamStartTime,
                            ExamDate = a.ExamDate,
                            Examdatestring = NullableDateTimeToStringDate(a.ExamDate),
                            ReachTime = a.ReachTime,
                            ExamEndTime = a.ExamEndTime
                        }).ToList();
            }
            return list;
        }
        #endregion
    }

}
