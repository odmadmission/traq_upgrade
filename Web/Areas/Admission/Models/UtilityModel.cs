﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OdmErp.Web.Areas.Admission.Models
{
    public static class UtilityModel
    {
        public static string GetFullName(string fname,string mname,string lname)
        {
            string fullname = "";
            fullname = fullname + fname+" ";
            if(mname==null|| mname.Trim().Length==0)
            {
              
            }
            else
            {
                fullname = fullname + mname+" ";
            }
            fullname = fullname + lname + " ";
            return fullname;
        }

        public static string Encode(string text)
        {


            return text;
        
        }

        public static string Decode(string text)
        {


            return text;

        }
        public static string GenerateFormNumber(DateTime d)
        {
            DateTimeOffset utcNow = d;
            return utcNow.Year + "" + utcNow.Month
            + utcNow.Day + "" + utcNow.Hour + "" +
           utcNow.Minute + "" + utcNow.Second + "" + utcNow.ToString("ff");


        }
        public static string GenerateTransactionNumber(long id)
        {
            DateTimeOffset utcNow = DateTimeOffset.UtcNow;
            return "ODMEGR-ADMRF-"+utcNow.Year + "" + utcNow.Month
            + utcNow.Day + "" + utcNow.Hour + "" +
           utcNow.Minute + "" + utcNow.Second + "" + utcNow.ToString("ffffff")+ id;


        }
        public static string GetTinyUrl(string link)
        {
            string tinyUrl = null;
            try
            {
                System.Uri address = new System.Uri("http://tinyurl.com/api-create.php?url=" +
              "http://tracq.odmps.org/" + link);
                System.Net.WebClient client = new System.Net.WebClient();
                tinyUrl = client.DownloadString(address);



            }
            catch (Exception e)
            {

            }

            return tinyUrl;
        }
    }

   
}
