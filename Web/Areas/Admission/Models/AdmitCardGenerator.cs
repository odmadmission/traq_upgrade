﻿using iText.Html2pdf;
using iText.IO.Font;
using iText.IO.Image;
using iText.IO.Source;
using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.StyledXmlParser.Jsoup.Nodes;
using System;
using System.Collections.Generic;
using System.IO;
namespace OdmErp.Web.Areas.Admission.Models
{
    public class AdmitCardGenerator
    {

        public byte[] GenerateAdmitCard(AdmitCardModel model)
        {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            WriterProperties props = new WriterProperties();
            props.SetStandardEncryption(model.username, model.username,
                EncryptionConstants.ALLOW_PRINTING,
                     EncryptionConstants.ENCRYPTION_AES_128 |
                     EncryptionConstants.DO_NOT_ENCRYPT_METADATA);
            PdfWriter writer = new PdfWriter(byteArrayOutputStream, props);

           

            // Creating a PdfDocument object      
            PdfDocument pdf = new PdfDocument(writer);

            // Creating a Document object       
            iText.Layout.Document doc = new iText.Layout.Document(pdf, PageSize.A4);

            // Creating a table       

           

            #region Root Table and Cell    

            Table rootTable = new Table(1);
            rootTable.SetFixedLayout();
            rootTable.SetHeight(PageSize.A4.GetHeight());
            rootTable.UseAllAvailableWidth();
            rootTable.SetMargins(5, 5, 5, 5);
            // rootTable.SetFontFamily(new string[] { FontConstants.HELVETICA_BOLD });

            Cell topCell = CreateEmptyCellWithBoarder();

            #endregion

            Table headerTable = new Table(4);
            if (model.headerModel != null)
            {


                #region Header Table with three Cells   

               
                headerTable.SetFixedLayout();
                float height = 140;// rootTable.GetHeight().GetValue() / 10;
                headerTable.SetHeight(height);
                headerTable.SetWidth(rootTable.GetWidth());
                headerTable.SetMargins(1, 1, 1, 1);




                Cell middleTextCell = new Cell(1, 2);
                middleTextCell.SetHorizontalAlignment(HorizontalAlignment.CENTER);
                middleTextCell.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                middleTextCell.Add(GetHeaderTableCell2(headerTable,
                    model.headerModel.organization,
                    model.headerModel.address,
                    model.headerModel.tollfreeNo,
                    model.headerModel.session));
                middleTextCell.SetBorder(Border.NO_BORDER);


                headerTable.AddCell(CreateLogoImageCell(model.headerModel.logoPath));
                headerTable.AddCell(middleTextCell);
                headerTable.AddCell(CreateStudentImageCell(model.headerModel.studentPath));


                topCell.Add(headerTable);
            }

            #endregion
            if (model.studentModel != null)
            {
                #region AdmitCard Title Table with One Cells   

                Table admitCardTitleTable = new Table(1);
            admitCardTitleTable.SetFixedLayout();

            admitCardTitleTable.SetHeight(20);
            admitCardTitleTable.SetWidth(rootTable.GetWidth());
            admitCardTitleTable.SetMargins(1, 1, 1, 1);

            admitCardTitleTable.AddCell(CreateAdmitCardTextCell(model.studentModel.title));
            topCell.Add(admitCardTitleTable);
                #endregion
                if (model.studentModel.data != null)
                {
                    #region AdmitCard Data Rows Tables with One/Two Cells   

                    for (int i = 0; i < model.studentModel.data.Count; i++)
                    {
                        if (model.studentModel.data[i].Count == 1)
                        {
                            topCell.Add(CreateAdmitCardDetailsTable
                          (rootTable, model.studentModel.data[i][0], null));
                        }
                        else
                       if (model.studentModel.data[i].Count == 2)
                        {

                            topCell.Add(CreateAdmitCardDetailsTable
                         (rootTable, model.studentModel.data[i][0], model.studentModel.data[i][1]));

                        }
                    }
                }

                    #endregion
                }
                if (model.instrModel != null)
                {
                    #region Instructions Title Table with One Cells   

                    Table insTitleTable = new Table(1);
                    insTitleTable.SetFixedLayout();

                    insTitleTable.SetHeight(20);
                    insTitleTable.SetWidth((rootTable.GetWidth()));
                    insTitleTable.SetMargins(1, 1, 1, 1);

                    insTitleTable.AddCell(CreateAdmitCardTextCell(model.instrModel.title));
                    topCell.Add(insTitleTable);
                    #endregion
                    #region Instructions Data Rows Tables with One/Two Cells   

                    if (model.instrModel.instructions != null)
                    {
                        for (int k = 0; k < model.instrModel.instructions.Count; k++)
                        {
                        topCell.Add(CreateTextCellWithStyles
                (model.instrModel.instructions[k],
                TextAlignment.LEFT, ColorConstants.RED, ColorConstants.WHITE, 10));

                        //topCell.Add(CreateInstructionItemTextCell
                        // (model.instrModel.instructions[k]));

                    }
                    }
                topCell.Add(CreateEmptyCellWithOutBoarder(headerTable));
                topCell.Add(CreateEmptyCellWithOutBoarder(headerTable));
                if (model.instrModel.notes != null)
                {
                    for (int k = 0; k < model.instrModel.notes.Count; k++)
                    {
                        topCell.Add(CreateTextCellWithStyles
                    (model.instrModel.notes[k],
                    TextAlignment.CENTER, ColorConstants.BLACK, ColorConstants.WHITE, 8));

                    }
                }
                topCell.Add(CreateEmptyCellWithOutBoarder(headerTable));
                #endregion

            }

            if (model.venueModel != null)
            {
                #region  

                Table VenueTitleTable = new Table(1);
                VenueTitleTable.SetFixedLayout();

                VenueTitleTable.SetHeight(20);
                VenueTitleTable.SetWidth(rootTable.GetWidth());
                VenueTitleTable.SetMargins(1, 1, 1, 1);

                VenueTitleTable.AddCell(CreateAdmitCardTextCell(model.venueModel.title));
                topCell.Add(VenueTitleTable);
                #endregion

                #region Venue Data Rows Tables with One/Two Cells   

                if (model.venueModel.venues != null)
                {
                    for (int k = 0; k < model.venueModel.venues.Count; k++)
                    {
                        topCell.Add(CreateTextCellWithStyles
                    (model.venueModel.venues[k],
                    TextAlignment.CENTER, ColorConstants.BLACK, ColorConstants.WHITE, 10));

                    }
                }
                topCell.Add(CreateEmptyCellWithOutBoarder(headerTable));
                //topCell.Add(CreateEmptyCellWithOutBoarder(headerTable));
               // topCell.Add(CreateEmptyCellWithOutBoarder());
                if (model.venueModel.notes != null)
                {
                    for (int k = 0; k < model.venueModel.notes.Count; k++)
                    {
                        topCell.Add(CreateTextCellWithStyles
                    (model.venueModel.notes[k],
                    TextAlignment.CENTER, ColorConstants.BLACK, ColorConstants.WHITE, 8));

                    }
                }

                #endregion

            }



            rootTable.AddCell(topCell);



            // Adding Table to document        
            doc.Add(rootTable);

            // Closing the document       
            doc.Close();

           return byteArrayOutputStream.ToArray();
        }
        public byte[] GenerateReceiptCard(AdmitCardModel model)
        {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            WriterProperties props = new WriterProperties();
            props.SetStandardEncryption(model.username, model.username,
                EncryptionConstants.ALLOW_PRINTING,
                     EncryptionConstants.ENCRYPTION_AES_128 |
                     EncryptionConstants.DO_NOT_ENCRYPT_METADATA);
            PdfWriter writer = new PdfWriter(byteArrayOutputStream, props);



            // Creating a PdfDocument object      
            PdfDocument pdf = new PdfDocument(writer);

            // Creating a Document object       
            iText.Layout.Document doc = new iText.Layout.Document(pdf, PageSize.A4);

            // Creating a table       



            #region Root Table and Cell    

            Table rootTable = new Table(1);
            rootTable.SetFixedLayout();
            rootTable.SetHeight(PageSize.A4.GetHeight());
            rootTable.UseAllAvailableWidth();
            rootTable.SetMargins(5, 5, 5, 5);
            // rootTable.SetFontFamily(new string[] { FontConstants.HELVETICA_BOLD });

            Cell topCell = CreateEmptyCellWithBoarder();

            #endregion

            Table headerTable = new Table(4);
            if (model.headerModel != null)
            {


                #region Header Table with three Cells   


                headerTable.SetFixedLayout();
                float height = 140;// rootTable.GetHeight().GetValue() / 10;
                headerTable.SetHeight(height);
                headerTable.SetWidth(rootTable.GetWidth());
                headerTable.SetMargins(1, 1, 1, 1);




                Cell middleTextCell = new Cell(1, 2);
                middleTextCell.SetHorizontalAlignment(HorizontalAlignment.CENTER);
                middleTextCell.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                middleTextCell.Add(GetHeaderTableCell2(headerTable,
                    model.headerModel.organization,
                    model.headerModel.address,
                    model.headerModel.tollfreeNo,
                    model.headerModel.session));
                middleTextCell.SetBorder(Border.NO_BORDER);


                headerTable.AddCell(CreateLogoImageCell(model.headerModel.logoPath));
                headerTable.AddCell(middleTextCell);
               // headerTable.AddCell(CreateStudentImageCell(model.headerModel.studentPath));


                topCell.Add(headerTable);
            }

            #endregion
            if (model.studentModel != null)
            {
                #region AdmitCard Title Table with One Cells   

                Table admitCardTitleTable = new Table(1);
                admitCardTitleTable.SetFixedLayout();

                admitCardTitleTable.SetHeight(20);
                admitCardTitleTable.SetWidth(rootTable.GetWidth());
                admitCardTitleTable.SetMargins(1, 1, 1, 1);

                admitCardTitleTable.AddCell(CreateAdmitCardTextCell(model.studentModel.title));
                topCell.Add(admitCardTitleTable);
                #endregion
                if (model.studentModel.data != null)
                {
                    #region AdmitCard Data Rows Tables with One/Two Cells   

                    for (int i = 0; i < model.studentModel.data.Count; i++)
                    {
                        if (model.studentModel.data[i].Count == 1)
                        {
                            topCell.Add(CreateAdmitCardDetailsTable
                          (rootTable, model.studentModel.data[i][0], null));
                        }
                        else
                       if (model.studentModel.data[i].Count == 2)
                        {

                            topCell.Add(CreateAdmitCardDetailsTable
                         (rootTable, model.studentModel.data[i][0], model.studentModel.data[i][1]));

                        }
                    }
                }

                #endregion
            }
            if (model.formModel != null)
            {
                #region AdmitCard Title Table with One Cells   

                Table admitCardTitleTable = new Table(1);
                admitCardTitleTable.SetFixedLayout();

                admitCardTitleTable.SetHeight(20);
                admitCardTitleTable.SetWidth(rootTable.GetWidth());
                admitCardTitleTable.SetMargins(1, 1, 1, 1);

                admitCardTitleTable.AddCell(CreateAdmitCardTextCell(model.formModel.title));
                topCell.Add(admitCardTitleTable);
                #endregion
                if (model.formModel.data != null)
                {
                    #region AdmitCard Data Rows Tables with One/Two Cells   

                    for (int i = 0; i < model.formModel.data.Count; i++)
                    {
                        if (model.formModel.data[i].Count == 1)
                        {
                            topCell.Add(CreateAdmitCardDetailsTable
                          (rootTable, model.formModel.data[i][0], null));
                        }
                        else
                       if (model.formModel.data[i].Count == 2)
                        {

                            topCell.Add(CreateAdmitCardDetailsTable
                         (rootTable, model.formModel.data[i][0],
                         model.formModel.data[i][1]));

                        }
                    }
                }

                #endregion
            }


            rootTable.AddCell(topCell);



            // Adding Table to document        
            doc.Add(rootTable);

            // Closing the document       
            doc.Close();

            return byteArrayOutputStream.ToArray();
        }
        public void SaveAdmitCard(AdmitCardModel model)
        {
            // ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            WriterProperties props = new WriterProperties();
            props.SetStandardEncryption(model.username, model.username,
                EncryptionConstants.ALLOW_PRINTING,
                     EncryptionConstants.ENCRYPTION_AES_128 |
                     EncryptionConstants.DO_NOT_ENCRYPT_METADATA);
         //   PdfWriter writer = new PdfWriter(byteArrayOutputStream, props);
            PdfWriter writer = new PdfWriter(model.pdfFilePath, props);

            // Creating a PdfDocument object      
            PdfDocument pdf = new PdfDocument(writer);

            // Creating a Document object       
            iText.Layout.Document doc = new iText.Layout.Document(pdf, PageSize.A4);

            // Creating a table       



            #region Root Table and Cell    

            Table rootTable = new Table(1);
            rootTable.SetFixedLayout();
            rootTable.SetHeight(PageSize.A4.GetHeight());
            rootTable.UseAllAvailableWidth();
            rootTable.SetMargins(5, 5, 5, 5);
            // rootTable.SetFontFamily(new string[] { FontConstants.HELVETICA_BOLD });

            Cell topCell = CreateEmptyCellWithBoarder();

            #endregion

            Table headerTable = new Table(4);
            if (model.headerModel != null)
            {


                #region Header Table with three Cells   


                headerTable.SetFixedLayout();
                float height = 140;// rootTable.GetHeight().GetValue() / 10;
                headerTable.SetHeight(height);
                headerTable.SetWidth(rootTable.GetWidth());
                headerTable.SetMargins(1, 1, 1, 1);




                Cell middleTextCell = new Cell(1, 2);
                middleTextCell.SetHorizontalAlignment(HorizontalAlignment.CENTER);
                middleTextCell.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                middleTextCell.Add(GetHeaderTableCell2(headerTable,
                    model.headerModel.organization,
                    model.headerModel.address,
                    model.headerModel.tollfreeNo,
                    model.headerModel.session));
                middleTextCell.SetBorder(Border.NO_BORDER);


                headerTable.AddCell(CreateLogoImageCell(model.headerModel.logoPath));
                headerTable.AddCell(middleTextCell);
               // headerTable.AddCell(/*CreateStudentImageCell(model.headerModel.studentPath)*/);


                topCell.Add(headerTable);
            }

            #endregion
            if (model.studentModel != null)
            {
                #region AdmitCard Title Table with One Cells   

                Table admitCardTitleTable = new Table(1);
                admitCardTitleTable.SetFixedLayout();

                admitCardTitleTable.SetHeight(20);
                admitCardTitleTable.SetWidth(rootTable.GetWidth());
                admitCardTitleTable.SetMargins(1, 1, 1, 1);

                admitCardTitleTable.AddCell(CreateAdmitCardTextCell(model.studentModel.title));
                topCell.Add(admitCardTitleTable);
                #endregion
                if (model.studentModel.data != null)
                {
                    #region AdmitCard Data Rows Tables with One/Two Cells   

                    for (int i = 0; i < model.studentModel.data.Count; i++)
                    {
                        if (model.studentModel.data[i].Count == 1)
                        {
                            topCell.Add(CreateAdmitCardDetailsTable
                          (rootTable, model.studentModel.data[i][0], null));
                        }
                        else
                       if (model.studentModel.data[i].Count == 2)
                        {

                            topCell.Add(CreateAdmitCardDetailsTable
                         (rootTable, model.studentModel.data[i][0], model.studentModel.data[i][1]));

                        }
                    }
                }

                #endregion
            }
            if (model.instrModel != null)
            {
                #region Instructions Title Table with One Cells   

                Table insTitleTable = new Table(1);
                insTitleTable.SetFixedLayout();

                insTitleTable.SetHeight(20);
                insTitleTable.SetWidth((rootTable.GetWidth()));
                insTitleTable.SetMargins(1, 1, 1, 1);

                insTitleTable.AddCell(CreateAdmitCardTextCell(model.instrModel.title));
                topCell.Add(insTitleTable);
                #endregion
                #region Instructions Data Rows Tables with One/Two Cells   

                if (model.instrModel.instructions != null)
                {
                    for (int k = 0; k < model.instrModel.instructions.Count; k++)
                    {
                        topCell.Add(CreateTextCellWithStyles
                (model.instrModel.instructions[k],
                TextAlignment.LEFT, ColorConstants.RED, ColorConstants.WHITE, 10));

                        //topCell.Add(CreateInstructionItemTextCell
                        // (model.instrModel.instructions[k]));

                    }
                }
                topCell.Add(CreateEmptyCellWithOutBoarder(headerTable));
                topCell.Add(CreateEmptyCellWithOutBoarder(headerTable));
                if (model.instrModel.notes != null)
                {
                    for (int k = 0; k < model.instrModel.notes.Count; k++)
                    {
                        topCell.Add(CreateTextCellWithStyles
                    (model.instrModel.notes[k],
                    TextAlignment.CENTER, ColorConstants.BLACK, ColorConstants.WHITE, 8));

                    }
                }
                topCell.Add(CreateEmptyCellWithOutBoarder(headerTable));
                #endregion

            }

            if (model.venueModel != null)
            {
                #region  

                Table VenueTitleTable = new Table(1);
                VenueTitleTable.SetFixedLayout();

                VenueTitleTable.SetHeight(20);
                VenueTitleTable.SetWidth(rootTable.GetWidth());
                VenueTitleTable.SetMargins(1, 1, 1, 1);

                VenueTitleTable.AddCell(CreateAdmitCardTextCell(model.venueModel.title));
                topCell.Add(VenueTitleTable);
                #endregion

                #region Venue Data Rows Tables with One/Two Cells   

                if (model.venueModel.venues != null)
                {
                    for (int k = 0; k < model.venueModel.venues.Count; k++)
                    {
                        topCell.Add(CreateTextCellWithStyles
                    (model.venueModel.venues[k],
                    TextAlignment.CENTER, ColorConstants.BLACK, ColorConstants.WHITE, 10));

                    }
                }
                topCell.Add(CreateEmptyCellWithOutBoarder(headerTable));
                //topCell.Add(CreateEmptyCellWithOutBoarder(headerTable));
                // topCell.Add(CreateEmptyCellWithOutBoarder());
                if (model.venueModel.notes != null)
                {
                    for (int k = 0; k < model.venueModel.notes.Count; k++)
                    {
                        topCell.Add(CreateTextCellWithStyles
                    (model.venueModel.notes[k],
                    TextAlignment.CENTER, ColorConstants.BLACK, ColorConstants.WHITE, 8));

                    }
                }

                #endregion

            }



            rootTable.AddCell(topCell);



            // Adding Table to document        
            doc.Add(rootTable);

            // Closing the document       
            doc.Close();

            //return byteArrayOutputStream.ToArray();
        }
        public void SaveReceiptCard(AdmitCardModel model)
        {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            WriterProperties props = new WriterProperties();
            props.SetStandardEncryption(model.username, model.username,
                EncryptionConstants.ALLOW_PRINTING,
                     EncryptionConstants.ENCRYPTION_AES_128 |
                     EncryptionConstants.DO_NOT_ENCRYPT_METADATA);
            PdfWriter writer = new PdfWriter(byteArrayOutputStream, props);



            // Creating a PdfDocument object      
            PdfDocument pdf = new PdfDocument(writer);

            // Creating a Document object       
            iText.Layout.Document doc = new iText.Layout.Document(pdf, PageSize.A4);

            // Creating a table       



            #region Root Table and Cell    

            Table rootTable = new Table(1);
            rootTable.SetFixedLayout();
            rootTable.SetHeight(PageSize.A4.GetHeight());
            rootTable.UseAllAvailableWidth();
            rootTable.SetMargins(5, 5, 5, 5);
            // rootTable.SetFontFamily(new string[] { FontConstants.HELVETICA_BOLD });

            Cell topCell = CreateEmptyCellWithBoarder();

            #endregion

            Table headerTable = new Table(4);
            if (model.headerModel != null)
            {


                #region Header Table with three Cells   


                headerTable.SetFixedLayout();
                float height = 140;// rootTable.GetHeight().GetValue() / 10;
                headerTable.SetHeight(height);
                headerTable.SetWidth(rootTable.GetWidth());
                headerTable.SetMargins(1, 1, 1, 1);




                Cell middleTextCell = new Cell(1, 2);
                middleTextCell.SetHorizontalAlignment(HorizontalAlignment.CENTER);
                middleTextCell.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                middleTextCell.Add(GetHeaderTableCell2(headerTable,
                    model.headerModel.organization,
                    model.headerModel.address,
                    model.headerModel.tollfreeNo,
                    model.headerModel.session));
                middleTextCell.SetBorder(Border.NO_BORDER);


                headerTable.AddCell(CreateLogoImageCell(model.headerModel.logoPath));
                headerTable.AddCell(middleTextCell);
                // headerTable.AddCell(CreateStudentImageCell(model.headerModel.studentPath));


                topCell.Add(headerTable);
            }

            #endregion
            if (model.studentModel != null)
            {
                #region AdmitCard Title Table with One Cells   

                Table admitCardTitleTable = new Table(1);
                admitCardTitleTable.SetFixedLayout();

                admitCardTitleTable.SetHeight(20);
                admitCardTitleTable.SetWidth(rootTable.GetWidth());
                admitCardTitleTable.SetMargins(1, 1, 1, 1);

                admitCardTitleTable.AddCell(CreateAdmitCardTextCell(model.studentModel.title));
                topCell.Add(admitCardTitleTable);
                #endregion
                if (model.studentModel.data != null)
                {
                    #region AdmitCard Data Rows Tables with One/Two Cells   

                    for (int i = 0; i < model.studentModel.data.Count; i++)
                    {
                        if (model.studentModel.data[i].Count == 1)
                        {
                            topCell.Add(CreateAdmitCardDetailsTable
                          (rootTable, model.studentModel.data[i][0], null));
                        }
                        else
                       if (model.studentModel.data[i].Count == 2)
                        {

                            topCell.Add(CreateAdmitCardDetailsTable
                         (rootTable, model.studentModel.data[i][0], model.studentModel.data[i][1]));

                        }
                    }
                }

                #endregion
            }
            if (model.formModel != null)
            {
                #region AdmitCard Title Table with One Cells   

                Table admitCardTitleTable = new Table(1);
                admitCardTitleTable.SetFixedLayout();

                admitCardTitleTable.SetHeight(20);
                admitCardTitleTable.SetWidth(rootTable.GetWidth());
                admitCardTitleTable.SetMargins(1, 1, 1, 1);

                admitCardTitleTable.AddCell(CreateAdmitCardTextCell(model.formModel.title));
                topCell.Add(admitCardTitleTable);
                #endregion
                if (model.formModel.data != null)
                {
                    #region AdmitCard Data Rows Tables with One/Two Cells   

                    for (int i = 0; i < model.formModel.data.Count; i++)
                    {
                        if (model.formModel.data[i].Count == 1)
                        {
                            topCell.Add(CreateAdmitCardDetailsTable
                          (rootTable, model.formModel.data[i][0], null));
                        }
                        else
                       if (model.formModel.data[i].Count == 2)
                        {

                            topCell.Add(CreateAdmitCardDetailsTable
                         (rootTable, model.formModel.data[i][0],
                         model.formModel.data[i][1]));

                        }
                    }
                }

                #endregion
            }


            rootTable.AddCell(topCell);



            // Adding Table to document        
            doc.Add(rootTable);

            // Closing the document       
            doc.Close();

           // return byteArrayOutputStream.ToArray();
        }
        private Table GetHeaderTableCell2(Table parentTable, string orgName,
            String address, String phoneNumber, string session)
        {
            Table headerTableCell2Table = new Table(2);
            headerTableCell2Table.SetFixedLayout();
            headerTableCell2Table.SetHeight(parentTable.GetHeight());
            headerTableCell2Table.SetWidth(parentTable.GetColumnWidth(1));
            headerTableCell2Table.SetMargins(1, 1, 1, 1);
            headerTableCell2Table.AddCell(CreateHeaderTextCell( orgName,
             address,  phoneNumber,  session));

            headerTableCell2Table.SetHorizontalAlignment(HorizontalAlignment.CENTER);
            headerTableCell2Table.SetVerticalAlignment(VerticalAlignment.MIDDLE);
            return headerTableCell2Table;
        }

        private Cell CreateStudentImageCell(string path)
        {
            Image img = new Image(ImageDataFactory.Create(path));
            img.SetWidth(120);
            img.SetHeight(120);

            //   img.Se(HorizontalAlignment.CENTER);

            Cell cell = new Cell(1, 1).Add(img);
            cell.SetHorizontalAlignment(HorizontalAlignment.CENTER);
            cell.SetVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.SetBorder(Border.NO_BORDER);

            return cell;
        }
        private Cell CreateLogoImageCell(string path)
        {
            Image img = new Image(ImageDataFactory.Create(path));
            img.SetWidth(120);
            img.SetHeight(120);
          
            Cell cell = new Cell(1, 1).Add(img);
            cell.SetHorizontalAlignment(HorizontalAlignment.CENTER);
            cell.SetVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.SetBorder(Border.NO_BORDER);

            return cell;
        }

        [Obsolete]
        private Cell CreateHeaderTextCell(string orgName,
            String address, String phoneNumber, string session)
        {
            PdfFont font = PdfFontFactory.CreateFont(FontConstants.HELVETICA);
            PdfFont bold = PdfFontFactory.CreateFont(FontConstants.HELVETICA_BOLD);

            Text orgNameText = new Text(orgName).SetFontSize(15).
                SetFontColor(ColorConstants.BLACK).SetFont(bold);
            Text addressText = new Text(address).SetFontSize(12).
                SetFontColor(ColorConstants.BLACK).SetFont(bold);
            Text phoneNumberText = new Text(phoneNumber).SetFontSize(8).
                SetFontColor(ColorConstants.BLACK).SetFont(font);
            Text sessionText = new Text(session).SetFontSize(8).
                SetFontColor(ColorConstants.BLACK).SetFont(font);
            Paragraph p = new Paragraph();
            p.Add(orgNameText);
            p.Add("\n");
            p.Add(addressText);
            p.Add("\n");
            p.Add(phoneNumberText);
            p.Add("\n");
            p.Add(sessionText);


            Cell cell = new Cell(1, 1);
            cell.SetHorizontalAlignment(HorizontalAlignment.CENTER);
            cell.SetVerticalAlignment(VerticalAlignment.MIDDLE);
            p.SetTextAlignment(TextAlignment.CENTER);

            cell.Add(p);
            cell.SetBorder(Border.NO_BORDER);



            return cell;
        }

        private Cell CreateAdmitCardTextCell(string text)
        {
            PdfFont bold = PdfFontFactory.CreateFont(FontConstants.HELVETICA_BOLD);
            Cell cell = new Cell(1, 1);
            Paragraph p = new Paragraph(text);
            p.SetTextAlignment(TextAlignment.CENTER).SetFontColor(ColorConstants.WHITE)
                .SetBackgroundColor(ColorConstants.RED).SetFont(bold);
            cell.Add(p).SetVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.SetBorder(Border.NO_BORDER);
            return cell;
        }
        private Cell CreateInstructionItemTextCell(string text)
        {
            PdfFont bold = PdfFontFactory.CreateFont(FontConstants.HELVETICA);
            Cell cell = new Cell(1, 1);
            Paragraph p = new Paragraph(text);
            p.SetTextAlignment(TextAlignment.LEFT).SetFontColor(ColorConstants.RED).SetFont(bold)
                .SetFontSize(10);
          //  p.SetMargins(0, 8, 0, 4);
            cell.Add(p).SetVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.SetBorder(Border.NO_BORDER);
            return cell;
        }

        private Cell CreateTextCellWithStyles(string text, TextAlignment ta,
            Color fontColor, Color bgColor, int fontSize)
        {
            //PdfFont bold = PdfFontFactory.CreateFont(FontConstants.HELVETICA_BOLD);
            Cell cell = new Cell(1, 1);
            Paragraph p = new Paragraph(text);
            p.SetTextAlignment(ta).SetFontColor(fontColor).SetBackgroundColor(bgColor)
                .SetFontSize(fontSize);//.SetFont(bold);

            cell.Add(p).SetVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.SetBorder(Border.NO_BORDER);
            return cell;
        }

        [Obsolete]
        private Table CreateAdmitCardDetailsTable(Table parentTable,
            String cell1Data, String cell2Data)
        {
            PdfFont font = PdfFontFactory.CreateFont(FontConstants.HELVETICA);
            PdfFont bold = PdfFontFactory.CreateFont(FontConstants.HELVETICA_BOLD);
            Table detailsTable = new Table(2);
            detailsTable.SetFixedLayout();
            detailsTable.SetHeight(22);
            detailsTable.SetWidth(parentTable.GetWidth());
            detailsTable.SetMargins(1, 1, 1, 1);
            detailsTable.SetPaddingLeft(5);
            detailsTable.SetPaddingRight(5);
            detailsTable.SetPaddingTop(0);
            detailsTable.SetPaddingBottom(0);
            if (cell2Data == null)
            {
                Cell cell = new Cell(1, 2);
                Text leftText = new Text(cell1Data.Split(":")[0]).SetFontSize(10).
               SetFontColor(ColorConstants.BLACK).SetFont(bold);
                Text rightText = new Text(cell1Data.Split(":")[1]).SetFontSize(10).
             SetFontColor(ColorConstants.DARK_GRAY).SetFont(font);

                Paragraph p = new Paragraph();
                p.SetTextAlignment(TextAlignment.LEFT);
               
                p.Add(leftText).Add(" : ").
                    Add(rightText).
                    SetVerticalAlignment(VerticalAlignment.TOP);
                cell.Add(p);
                cell.SetBorder(Border.NO_BORDER);
               
                detailsTable.AddCell(cell);
            }
            else
            {

                Cell cell = new Cell(1, 1);
                Text leftText = new Text(cell1Data.Split(":")[0]).SetFontSize(10).
               SetFontColor(ColorConstants.BLACK).SetFont(bold);
                Text rightText = new Text(cell1Data.Split(":")[1]).SetFontSize(10).
             SetFontColor(ColorConstants.DARK_GRAY).SetFont(font);

                Paragraph p = new Paragraph();
                p.SetTextAlignment(TextAlignment.LEFT);
              
                p.Add(leftText).Add(" : ").
                    Add(rightText).
                    SetVerticalAlignment(VerticalAlignment.TOP);
                cell.Add(p);
              
                cell.SetBorder(Border.NO_BORDER);
                detailsTable.AddCell(cell);
                Cell cell2 = new Cell(1, 1);
                Text leftText2 = new Text(cell2Data.Split(":")[0]).SetFontSize(10).
               SetFontColor(ColorConstants.BLACK).SetFont(bold);
                Text rightText2 = new Text(cell2Data.Split(":")[1]).SetFontSize(10).
             SetFontColor(ColorConstants.DARK_GRAY).SetFont(font);

                Paragraph p2 = new Paragraph();
                p2.SetTextAlignment(TextAlignment.LEFT);
              
                p2.Add(leftText2).Add(" : ").
                    Add(rightText2).
                    SetVerticalAlignment(VerticalAlignment.TOP);
                cell2.Add(p2);
                
                cell2.SetBorder(Border.NO_BORDER);

                detailsTable.AddCell(cell2);
            }
            return detailsTable;
        }

        private Cell CreateEmptyCellWithBoarder()
        {
            Cell cell = new Cell(1, 1);
            cell.SetBorder(new SolidBorder(2));

            return cell;
        }
        private Table CreateEmptyCellWithOutBoarder(Table parent)
        {
           Table detailsTable = new Table(1);
            detailsTable.SetFixedLayout();
            detailsTable.SetHeight(20);
            detailsTable.SetWidth(parent.GetWidth());
            detailsTable.SetMargins(1, 1, 1, 1);

            return detailsTable;
        }

    }
    public class AdmitCardModel
    {
        public AdmitCardHeaderModel headerModel{get;set;}
        public AdmitCardInstrModel instrModel { get; set; }
        public AdmitCardVenueModel venueModel { get; set; }
        public AdmitCardStudentModel studentModel { get; set; }
        public AdmitCardStudentModel formModel { get; set; }
        public string pdfFilePath { get; set; }
        public string admissionId { get; set; }
        public byte[] username { get; set; }
    }

    public class AdmitCardHeaderModel
    {
        public string logoPath { get; set; }
        public string studentPath { get; set; }
        public string organization { get; set; }
        public string address { get; set; }
        public string tollfreeNo { get; set; }
        public string session { get; set; }

    }

    public class AdmitCardInstrModel
    {
        public string title { get; set; }
        public List<string> instructions  { get; set; }
        public List<string> notes { get; set; }
      

    }
    public class AdmitCardVenueModel
    {
        public string title { get; set; }
        public List<string> venues { get; set; }
        public List<string> notes { get; set; }

    }

    public class AdmitCardStudentModel
    {
        public string title { get; set; }
        public List<List<string>> data { get; set; }
       

    }
}
