﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Drawing;
using iText.Html2pdf;
using iText.Kernel.Pdf;
using iText.Layout;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.ApplicationCore.Query;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo;
using OdmErp.Web.Areas.Admission.Models;
using OdmErp.Web.Areas.Support.Models;
using OdmErp.Web.BillDesk;
using OdmErp.Web.LeadSquared;
using OdmErp.Web.Models;
using Web.Controllers;

using static OdmErp.Web.LeadSquared.LeadManage;

using OdmErp.Web.Areas.Admission.Constants;
using RestSharp;
using RestSharp.Serialization.Json;
using OdmErp.Web.SendInBlue;

namespace OdmErp.Web.Areas.Admission.Controllers
{
    [Area("Admission")]
    public class CounsellerController : AppController
    {
        const string ImgAnswerUrl = "answerVal";
        public AdmissionSqlQuery sqlQuery;       
        public IAdmissionExamDateAssignRepository admissionExamDateAssignRepository;
        public IAdmissionStandardExamRepository admissionStandardExamRepository;
        public IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository;
        public IEmployeeRepository employeeRepository;
        public IAdmissionDocumentRepository admissionDocumentRepository;
        public IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepository;
        public IAdmissionStudentDocumentRepository admissionStudentDocumentRepository;
        public IDocumentSubTypeRepository documentSubTypeRepository;
        public IDocumentTypeRepository documentTypeRepository;
        public IPaymentType paymentType;
        public AdmissionStudentCommonMethod admissionStudentCommonMethod;
        public IBoardStandardRepository boardStandardRepository;
        public IAdmissionExamDateAssignRepository admissionExamDateAssign;
        public IAdmissionStudentExamRepository admissionStudentExamRepository;
        public IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository;
        public IAdmissionRepository admissionRepository;
        public IAcademicSessionRepository academicSessionRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IOrganizationRepository organizationRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IBoardRepository boardRepository;
        public IAdmissionStudentContactRepository admissionStudentContactRepository;
        public IStandardRepository standardRepository;
        public IWingRepository wingRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IAdmissionStudentRepository admissionStudentRepository;
        public IAdmissionStudentAcademicRepository admissionStudentAcademicRepository;
        public IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository;
        public IAdmissionCounsellorRepository admissionCounsellorRepository;
        public IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssign;
        public IAdmissionDocumentAssignRepository admissionDocumentAssignRepository;
        public ICounsellorStatusRepository counsellorStatusRepository;
        public IAdmissionInterViewerRepository admissionInterViewerRepository;
        public IAdmissionVerifierRepository admissionVerifierRepository;
        public IAdmitcardDetailsRepository admitcardDetailsRepository;
        public IAdmitcardInstructionsRepository admitcardInstructionsRepository;
        public IAdmitcardVenueAddressRepository admitcardVenueAddressRepository;
        public IAdmitcardVenueRepository admitcardVenueRepository;
        public CommonMethods commonMethods;
        public AdmissionSmsnMails Emailsend;
        public IAdmissionFeeRepository admissionFeeRepository;
        public IHostingEnvironment hostingEnv;
        public smssend smssend;
        public StudentAdmissionMethod studentAdmissionMethod;
        public PaymentManager pManger;
        public AdmissionStudentCommonMethod amissionStudentCommonMethod;
        public SendInBlueEmailManager senndInBlue;


        public CounsellerController(IAdmissionStudentDocumentRepository admissionStudentDocumentRepo,
            IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo,
            IAdmissionDocumentRepository admissionDocumentRepo,
            CommonMethods commonMethods,smssend smssend, AdmissionSmsnMails Emailsend,IHostingEnvironment hostingEnv,
           IAdmissionStudentContactRepository admissionStudentContactRepository,
            IAdmissionFeeRepository admissionFeeRepository,
            PaymentManager pManger,
           
        IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepo,
            IEmployeeRepository employeeRepo,AdmissionStudentCommonMethod admissionStudentCommonMethod,
            IAdmissionExamDateAssignRepository admissionExamDateAssignRepo,
            IAdmissionStandardExamRepository admissionStandardExamRepo, AdmissionSqlQuery sqlQ, IAdmissionDocumentAssignRepository adsRepo,
            IAcademicSessionRepository asrepo, IStandardRepository srepo, IBoardRepository brpo, IWingRepository wpo,
            IOrganizationAcademicRepository oarepo,IOrganizationRepository orepo, IAcademicStandardWingRepository aswrepo,
            IAcademicStandardRepository astdrepo, IAdmissionRepository admrepo, IBoardStandardRepository bostdrepo,
             IBoardStandardWingRepository boardStandardWingRepository,
              IAdmissionStudentRepository admStdrepo, IAdmissionStudentAcademicRepository admStdAcdrepo,
             IAdmissionStudentStandardRepository admStdStadrepo, IAdmissionFormPaymentRepository admFormParEpo,
             IPaymentType paty, IBoardStandardRepository boardStandardRepository, IAcademicStandardSettingsRepository academicStandardSettingsRepository,
         IAdmissionExamDateAssignRepository admissionExamDateAssign,IAdmissionStudentExamRepository admissionStudentExamRepository, IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository,
         IAdmissionPersonalInterviewAssignRepository apiasign, IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository,
         IAdmissionCounsellorRepository admissionCounsellorRepository, ICounsellorStatusRepository counsellorStatusRepo,
         IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository, IAdmissionInterViewerRepository admissionInterViewerRepository, 
         IAdmissionVerifierRepository admissionVerifierRepository, IAdmitcardDetailsRepository admitcardDetailsRepo,
         IAdmitcardInstructionsRepository admitcardInstructionsRepo, IAdmitcardVenueAddressRepository admitcardVenueAddressRepo, 
         IAdmitcardVenueRepository admitcardVenueRepo, StudentAdmissionMethod studentAdmissionMethod, AdmissionStudentCommonMethod amissionStudentCommonMethod, SendInBlueEmailManager senndInBlue
            )
        {
            this.amissionStudentCommonMethod = amissionStudentCommonMethod;
            this.senndInBlue = senndInBlue;
            this.pManger = pManger;
            this.admissionStudentContactRepository = admissionStudentContactRepository;
            this.smssend = smssend;
            this.hostingEnv = hostingEnv;
            this.admissionFeeRepository = admissionFeeRepository;
            this.commonMethods = commonMethods;
            this.admissionInterViewerRepository = admissionInterViewerRepository;
            this.admissionStudentCommonMethod = admissionStudentCommonMethod;
            this.admissionStudentCounsellorRepository = admissionStudentCounsellorRepository;
            this.admissionCounsellorRepository = admissionCounsellorRepository;
            this.admissionDocumentAssignRepository = adsRepo;
            this.admissionPersonalInterviewAssign = apiasign;
            this.admissionPersonalInterviewAssignRepository = admissionPersonalInterviewAssignRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.admissionExamDateAssign = admissionExamDateAssign;
            this.admissionStudentExamRepository = admissionStudentExamRepository;
            this.admissionStandardExamResultDateRepository = admissionStandardExamResultDateRepository;
            paymentType = paty;
            admissionStudentStandardRepository = admStdStadrepo;
            admissionStudentAcademicRepository = admStdAcdrepo;
            admissionStudentRepository = admStdrepo;
            this.boardStandardWingRepository = boardStandardWingRepository;
            academicSessionRepository = asrepo;
            academicStandardRepository = astdrepo;
            academicStandardWingRepository = aswrepo;
            organizationRepository = orepo;
            organizationAcademicRepository = oarepo;
            boardRepository = brpo;
            standardRepository = srepo;
            wingRepository = wpo;
            admissionRepository = admrepo;
            this.boardStandardRepository = bostdrepo;
            this.admissionFormPaymentRepository = admFormParEpo;
            admissionStudentDocumentRepository = admissionStudentDocumentRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            admissionDocumentRepository = admissionDocumentRepo;
            admissionDocumentVerificationAssignRepository = admissionDocumentVerificationAssignRepo;
            employeeRepository = employeeRepo;
            admissionExamDateAssignRepository = admissionExamDateAssignRepo;
            admissionStandardExamRepository = admissionStandardExamRepo;
            admitcardDetailsRepository = admitcardDetailsRepo;
            admitcardInstructionsRepository = admitcardInstructionsRepo;
            admitcardVenueAddressRepository = admitcardVenueAddressRepo;
            admitcardVenueRepository = admitcardVenueRepo;
            sqlQuery = sqlQ;
            this.studentAdmissionMethod = studentAdmissionMethod;
            this.counsellorStatusRepository = counsellorStatusRepo;
            this.admissionVerifierRepository = admissionVerifierRepository;
            this.Emailsend = Emailsend;
        }
        
        #region-------------Commom Method----------------------------------
        public List<CommonStudentModal> GetstudentDetailsWithFormNumber()
        {
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result.ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();

            var studentlist = (from a in admissionstudentlist
                               join b in admissionlist on a.AdmissionId equals b.ID
                               select new CommonStudentModal
                               {
                                   Id = a.ID,
                                   Name = b.FormNumber + "-" + a.FirstName + " " + a.LastName
                               }
                                ).ToList();
            return studentlist;
        }
        public string GetDateTimeFormat(DateTime d, TimeSpan span)
        {

            DateTime time = d.Add(span);
            return time.ToString("hh:mm tt");
        }
        //public List<AdmDocumentViewModel> GetCommonFileDetails(long admissionStudentId)
        //{
        //    List<AdmDocumentViewModel> data = new List<AdmDocumentViewModel>();
        //    var admStd = admissionStudentStandardRepository.ListAllAsync().Result.
        //        Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true).FirstOrDefault();

        //    if (admStd != null)
        //    {
        //        var AcademicStandardId = academicStandardWingRepository.
        //            GetByIdAsync(admStd.AcademicStandardWindId ).Result
        //            .AcademicStandardId;

        //        var Docs = admissionDocumentRepository.ListAllAsync().Result
        //          .Where(a => a.AcademicStandardId == AcademicStandardId && a.Active == true && a.Status == "ACTIVE");

        //        var DocsAssigned = admissionDocumentAssignRepository.ListAllAsync().Result
        //         .Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true && a.Status == "ACTIVE");

        //        data.AddRange((from a in Docs
        //                       join b in documentTypeRepository.GetAllDocumentTypes()
        //                       on a.DocumentTypeId equals b.ID
        //                       join c in documentSubTypeRepository.GetAllDocumentSubTypes()
        //                       on a.DocumentSubTypeId equals c.ID
        //                       select new AdmDocumentViewModel
        //                       {

        //                           Name = c.Name,
        //                           DocumentSubTypeId = c.ID,
        //                           Submit = GetClassDocumentSubmittedInfo(a.ID, admissionStudentId)
        //                       }
        //                   ).ToList());

        //        data.AddRange((from a in DocsAssigned
        //                       join b in documentTypeRepository.GetAllDocumentTypes()
        //                       on a.DocumentTypeId equals b.ID
        //                       join c in documentSubTypeRepository.GetAllDocumentSubTypes()
        //                       on a.DocumentSubTypeId equals c.ID
        //                       select new AdmDocumentViewModel
        //                       {

        //                           Name = c.Name,
        //                           DocumentSubTypeId = c.ID,
        //                           Submit = GetClassDocumentSubmittedInfo(a.ID, admissionStudentId)
        //                       }
        //                  ).ToList());

        //    }
        //    return data;
        //}
        //public List<AdmDocumentViewModel> GetExtraFileDetails(long admissionStudentId)
        //{
        //    var documenttype = documentTypeRepository.GetAllDocumentTypes().ToList().Where(a => a.Name == "ADMISSION").Select(a => a.ID);
        //    var documentsubtype = documentSubTypeRepository.GetAllDocumentSubTypes()
        //        .Where(a => documenttype.Contains(a.DocumentTypeID)).ToList();

        //    var AdmissionStudentData = admissionCommonMethod.GetStudentIdData(admissionStudentId);
        //    HashSet<long> uniqueSubTypeIds = new HashSet<long>();
        //   var documentSubTypeIds = admissionDocumentRepository.ListAllAsync()
        //        .Result.Where(a => a.AcademicStandardId == AdmissionStudentData.academicStandardId
        //        && a.AdmissionSessionId == AdmissionStudentData.sessionId&&a.Active==true)
        //          .Select(a => a.DocumentSubTypeId).Distinct().ToList();

        //    var documentSubTypeIdsAssigned = admissionDocumentAssignRepository.ListAllAsync()
        //      .Result.Where(a => a.AdmissionStudentId == admissionStudentId && a.Active == true
        //      )
        //        .Select(a => a.DocumentSubTypeId).Distinct().ToList();

        //      foreach(var a in documentSubTypeIds)
        //    {
        //        uniqueSubTypeIds.Add(a);
        //    }

        //    foreach (var a in documentSubTypeIdsAssigned)
        //    {
        //        uniqueSubTypeIds.Add(a);
        //    }


        //    var documentsNotInAdmissionDocument =
        //        documentsubtype.Where(a => !uniqueSubTypeIds.ToList().Contains(a.ID)).ToList();
        //    List<AdmDocumentViewModel> data = new List<AdmDocumentViewModel>();
        //    if (documentsNotInAdmissionDocument != null)
        //    {
        //        data.AddRange((from a in documentsNotInAdmissionDocument
        //                       join b in documentTypeRepository.GetAllDocumentTypes()
        //                       on a.DocumentTypeID equals b.ID
        //                       select new AdmDocumentViewModel
        //                       {
        //                           Name = a.Name,
        //                           DocumentSubTypeId = a.ID,
        //                           DocumentTypeId = b.ID
        //                           //   Submit = GetStudentDocumentSubmittedInfo(a.ID, admissionStudentId)
        //                       }
        //                   ).ToList());
        //    }
        //    return data;
        //}

        //public AdmissionViewModel GetStudentDetailsById(long id)
        //{
        //    var admission = (from a in admissionRepository.GetAllAdmission()
        //                         //join b in admissionRepository.GetAllAdmissionSlot()
        //                         //on a.AdmissionSlotId equals b.ID
        //                     join c in admissionRepository.GetAllAdmissionSource()
        //                     on a.AcademicSourceId equals c.ID
        //                     join d in academicSessionRepository.GetAllAcademicSession()
        //                    on a.AcademicSessionId equals d.ID
        //                     join e in admissionStudentRepository.ListAllAsync().Result.ToList()
        //                      on a.ID equals e.AdmissionId

        //                     join g in admissionStudentStandardRepository.ListAllAsync().Result.ToList()
        //                    on e.ID equals g.AdmissionStudentId

        //                    join k in admissionStudentContactRepository.ListAllAsyncIncludeAll().Result.ToList()
        //                    on e.ID equals k.AdmissionStudentId
        //                     where a.ID == id
        //                     select new AdmissionViewModel()
        //                     {
        //                         AdmissionId = a.ID,
        //                         FormNumber = a.FormNumber,
        //                         Mobile = a.MobileNumber,
        //                         EmailId=k.EmailId,
        //                         //SlotId = b.ID,
        //                         //SlotName = b.SlotName,
        //                         Status = a.Status,
        //                         SourceId = c.ID,
        //                         SourceName = c.Name,
        //                         InserteDate = a.InsertedDate,
        //                         AdmissionStudentId = e.ID,
        //                         FullName = e.FirstName +  " " + e.LastName,
        //                         Image = e.Image,
        //                         GetExamDetails = appModel.GetExamDetails(a.ID, g.AcademicStandardWindId,
        //                         e.ID, a.IsExamQualified == null ? "NA" :
        //                         a.IsExamQualified == false ? "NotQualified" : "Qualified"),
        //                         AcademicSessionId = d.ID,
        //                         AcademicSessionName = d.DisplayName,
        //                         FormPayment = GetFormPaymentDetails(a.ID),
        //                         ClassDetails = GetAdmissionClassDetails(g.AcademicStandardWindId),
        //                         PiDetails = GetPiDetails(e.ID),
        //                         DocumentDetails = GetDocumentDetails(e.ID),
        //                         files = GetFileDetails(e.ID),
        //                         Counsellor = GetCounsellorDetails(e.ID),
        //                         StudentTimeLine = GetStudentTimeLine(e.ID)
        //                     }
        //                   ).FirstOrDefault();
        //    return admission;
        //}


        //public List<StudentExamDeatilsModel> GetStudentExamDetails(long admissionStudentId)
        //{
        //    List<StudentExamDeatilsModel> list = null;
        //   long studentstandard = admissionStudentStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == admissionStudentId).FirstOrDefault().StandardId;
        //   if(studentstandard!=0)
        //    {
        //        var StandardExam = admissionStandardExamRepository.ListAllAsyncIncludeAll().Result.ToList();
        //        var boardStandard = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a=>a.StandardId== studentstandard).ToList();
        //        var stanadrd = standardRepository.GetAllStandard().ToList();
        //         list = (from a in StandardExam
        //                    join b in boardStandard on a.BoardStandardId equals b.ID
        //                    join c in stanadrd on b.StandardId equals c.ID
        //                    select new StudentExamDeatilsModel
        //                    {
        //                        AdmissionStandardExamId=a.ID,
        //                        ExamStartTime=a.ExamStartTime,
        //                        ExamDate=a.ExamDate,
        //                        Examdatestring=NullableDateTimeToStringDate(a.ExamDate),
        //                        ReachTime=a.ReachTime,
        //                        ExamEndTime=a.ExamEndTime
        //                    }).ToList();
        //    }
        //    return list;
        //}

        #endregion-----------------------------------------------

        #region------------------------Json Method-----------------

        [HttpGet]
        public JsonResult GetExamDetailsById(long AdmissionStandardExamId)                
        {
            try
            {
                var admissionstandardexam = admissionStandardExamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AdmissionStandardExamId).ToList();

                var res = (from a in admissionstandardexam
                           select new 
                           {
                               admissionStandardExamId = a.ID,
                               examDate = a.ExamDate,
                               examStartTime = GetDateTimeFormat(a.ExamDate.Value, a.ExamStartTime.Value) ,
                               examEndTime = GetDateTimeFormat(a.ExamDate.Value, a.ExamEndTime.Value),
                               reachTime = GetDateTimeFormat(a.ExamDate.Value, a.ReachTime.Value)
                           }
                        ).FirstOrDefault();
                return Json(res);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region------------------student Timeline---------------
        public List<StudentTimeLineModal> GetStudentTimeLine(long AdmissionStudentId)
        {
            try
            {
                long admissionid = admissionStudentRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == AdmissionStudentId).Select(a => a.AdmissionId).FirstOrDefault();               
                if(admissionid!=0)
                {
                    var timeline = admissionRepository.GetAllAdmissionTimeline().Where(a => a.AdmissionId == admissionid).ToList();
                    var employee = employeeRepository.GetAllEmployee().ToList();
                    var list = (from a in timeline
                                join b in employee on a.InsertedId equals b.ID
                                select new StudentTimeLineModal
                                {
                                    AdmissionId = a.AdmissionId,
                                    Status = a.Status,
                                    Timeline = a.Timeline,
                                    InsetedDate = NullableDateTimeToStringDate(a.InsertedDate),
                                    EmployeeName = b.FirstName + " " + b.LastName
                                }).ToList();
                    return list;
                }
                else
                { 
                    return null;
                }
            }
            catch(Exception ex)
            {
                return null;
            }
            
        }
        #endregion
        public IActionResult Index()
        {
            var res = sqlQuery.GetView_Student_Admission_Counsellers();
            return View(res);
        }
        #region-----------------------modal Class------------------------
        public class RequiredDocumentscls
        {
            public long DocumentSubTypeId { get; set; }
            public long DocumentTypeId { get; set; }
            public long AdmissionDocumentID { get; set; }
            public string DocumentTypeName { get; set; }
            public string DocumentSubTypeName { get; set; }
            public string FilePath { get; set; }
            public bool IsSubmitted { get; set; }
        }
        public class studentcounsellorcls
        {
            public long AdmissionID { get; set; }
            public View_Student_Admission_Counseller view_Student_ { get; set; }
            public AdmissionExamDateAssign dateAssign { get; set; }
            public AdmissionStandardExam standardExam { get; set; }
            public examcounsellercls examcounseller { get; set; }
            public documentcounsellercls verificationAssign { get; set; }
            public List<RequiredDocumentscls> documentscls { get; set; }
        }
        public class examcounsellercls
        {
            public AdmissionPersonalInterviewAssign interviewAssign { get; set; }
            public string EmployeeName { get; set; }
            public string AssignedCounselorName { get; set; }
        }
        public class documentcounsellercls
        {
            public AdmissionDocumentVerificationAssign verificationAssign { get; set; }
            public string EmployeeName { get; set; }
            public string AssignedCounselorName { get; set; }
        }
        #endregion--------------------------------------------------------

        #region----------------ViewAdmissionDetails-------------------------
        public IActionResult ViewAdmissionDetails(string id)
        {
            CheckLoginStatus();
            #region--------------------basic List-------------------------------

            ViewBag.PaymentType = paymentType.GetAllPaymentType().ToList();
            OdmErp.ApplicationCore.Entities.Admission admi = admissionRepository.
                 GetAdmissionByLeadReferenceId(id);
            ViewBag.AdmissionFees = admissionFeeRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == admi.AcademicSessionId).ToList();
            var emplist= employeeRepository.GetAllEmployee().ToList();
            var pilist = admissionInterViewerRepository.ListAllAsyncIncludeAll().Result.ToList();
            var verifierlist = admissionVerifierRepository.ListAllAsyncIncludeAll().Result.ToList();
            var PIEmpList = (from a in pilist
                             join b in emplist on a.EmployeeId equals b.ID
                             select new CommonStudentModal { 
                             Id=a.ID,
                             Name=b.EmpCode+"-"+b.FirstName+" "+b.LastName});


            ViewBag.PIEmployeeList = PIEmpList.ToList();
            var DocVerifierEmpList = (from a in verifierlist
                                      join b in emplist on a.EmployeeId equals b.ID
                                         select new CommonStudentModal
                                         {
                                             Id = a.ID,
                                             Name = b.EmpCode + "-" + b.FirstName + " " + b.LastName
                                         });

            ViewBag.DocVerifierEmployeeList = DocVerifierEmpList.ToList();
            ViewBag.EmployeeList = employeeRepository.GetAllEmployee().ToList();
            ViewBag.studentlist = GetstudentDetailsWithFormNumber().ToList();
            ViewBag.CouncellorEmployeeList = employeeRepository.GetAllEmployee().
                Where(b => admissionCounsellorRepository.ListAllAsync()
                .Result.Where(a => a.Active == true).
                Select(a => a.EmployeeId).Distinct().Contains(b.ID))
                .ToList();

            ViewBag.Councellorstatus = counsellorStatusRepository.
                ListAllAsyncIncludeAll().Result.ToList();


            ViewBag.Councellorstatus = counsellorStatusRepository.ListAllAsyncIncludeAll().Result.ToList();
            #endregion    
            var model = studentAdmissionMethod.GetAdmissionByLeadReferenceId(id,true, false);
            if (model.error)
                return RedirectToAction("Error", "Student");
            else
            {
                model.access = GetAdmissionAccessDetails(); return View(model);
            }
        }
        #endregion

        #region---------------------Admit Card (Santunu)-----------------
        //public IActionResult sendAdmitCard(long admissionId)
        //{
        //    //bool status = false;
        //    //smssend.sendadmitcardemail("name", "admission.FormNumber",
        //    //           "admStudentContact.Mobile", "tinyUrl",
        //    //           "Admit Card", "rajusatheesh@odmegroup.org");

          

        //        var admission = admissionRepository.GetAdmissionById(admissionId);
        //        var admStudent = admissionStudentRepository.
        //           ListAllAsync().Result.Where(a => a.AdmissionId
        //       == admissionId).FirstOrDefault();

        //        var admStudentContact = admissionStudentContactRepository.
        //          ListAllAsync().Result.Where(a => a.AdmissionStudentId
        //      == admStudent.ID && a.ConatctType == "Official").FirstOrDefault();

        //        string name = admStudent.FirstName + " ";
        //        string mname = admStudent.MiddleName != null ? admStudent.MiddleName + " " : "";
        //        name = name + mname + admStudent.LastName;


        //        AdmitCardGenerator card = new AdmitCardGenerator();
        //        var webPath = hostingEnv.WebRootPath;
        //        var fileName = System.IO.Path.GetFileName("AdmitCard_" +
        //            admission.FormNumber + ".pdf");
        //        string path = System.IO.Path.Combine("", webPath + @"\admitcards\" + fileName);



        //        card.SaveAdmitCard(GetAdmitCardModel(admissionId, path));

        //        try
        //        {
        //            System.Uri address = new System.Uri("http://tinyurl.com/api-create.php?url=" +
        //          "http://www.tracq.odmps.org/admitcards/" + fileName);
        //            System.Net.WebClient client = new System.Net.WebClient();
        //            string tinyUrl = client.DownloadString(address);

        //            //Thread t1 = new Thread(delegate ()
        //            //{
        //            // smssend.SendAdmitCard(tinyUrl, admStudentContact.Mobile);

        //            //});
        //            //t1.Start();
        //            //Thread t2 = new Thread(delegate ()
        //            //{
        //            smssend.sendadmitcardemail(name, admission.FormNumber,
        //                 admission.MobileNumber, tinyUrl,
        //                 "Admit Card", admStudentContact.EmailId);

        //            //});
        //            //t2.Start();

        //        }
        //        catch (Exception e)
        //        {

        //        }
            

        //    return RedirectToAction(nameof(ViewAdmissionDetails), new { id = admissionId });
              
            
        //}

        //public IActionResult downloadAdmitCard(long admissionId)
        //{
        //    AdmitCardGenerator card = new AdmitCardGenerator();
        //    var admission = admissionRepository.GetAdmissionById(admissionId);
        //    var admStudent = admissionStudentRepository.ListAllAsync().Result.Where(a => a.AdmissionId== admissionId).FirstOrDefault();
        //    var admStudentContact = admissionStudentContactRepository.ListAllAsync().Result.Where(a => a.AdmissionStudentId== admStudent.ID && a.ConatctType == "Official").FirstOrDefault();


        //    return File(card.GenerateAdmitCard(GetAdmitCardModel(admissionId,null)),
        //        "application/pdf", $"AdmitCard_"+ admission.FormNumber+ ".pdf");
        //}
        //public  AdmitCardModel GetAdmitCardModel(long admissionId,string path)
        //{
        //    var admission = admissionRepository.GetAdmissionById(admissionId);
        //    var sessionval = academicSessionRepository.GetByIdAsync(admission.AcademicSessionId).Result.DisplayName;
        //    var admStudent = admissionStudentRepository.
        //        ListAllAsync().Result.Where(a=>a.AdmissionId
        //    == admissionId).FirstOrDefault();
        //    var admData =studentAdmissionMethod.GetAdmissionClassDetails(admissionId);
        //    var admitcsardDetails = admitcardDetailsRepository.GetAllAdmitcardDetails().Where(a => a.IsAvailable == true).FirstOrDefault();
        //    var admitcardcardInstruction = admitcardInstructionsRepository.GetAllAdmicardInstructions().Where(a => a.IsAvailable == true && a.AdmitcardDetailsId == admitcsardDetails.ID).ToList();
        //    var admitcardvenue = admitcardVenueRepository.GetAllAdmitcardVenue().Where(a => a.IsAvailable == true).FirstOrDefault();
        //    var organisationName = organizationRepository.GetAllOrganization().Where(a => a.ID == admitcardvenue.OrganizationId).FirstOrDefault().Name;
        //    var orgVenueDetails = admitcardVenueAddressRepository.GetAllAdmitcardVenueAddress().Where(a => a.ID == admitcardvenue.AdmitcardVenueAddressId && a.IsAvailable == true).FirstOrDefault().Address;


        //    int startIndex = admission.MobileNumber.Length - 4;

        //  //  int endIndex = admission.MobileNumber.Length-1;

        //    string fname = admStudent.FirstName.
        //        Substring(
        //            0, 3
        //            );
        //    string mobile = admission.MobileNumber.Substring(
        //           startIndex, 4
        //             );

        //    AdmitCardModel model = new AdmitCardModel();
        //    model.pdfFilePath = path;
        //    model.username = Encoding.ASCII.
        //        GetBytes(fname.ToUpper() + ""+ mobile);


        //    AdmitCardHeaderModel hmodel = new AdmitCardHeaderModel();

        //    var webPath = hostingEnv.WebRootPath;
        //  //  var fileName = System.IO.Path.GetFileName("AdmitCard_" + admStudentContact.Mobile + ".pdf");
        //    string logoPath = System.IO.Path.Combine("", webPath + @"" + admitcsardDetails.LogoName);
        //    string studentPath = System.IO.Path.Combine("", webPath + @"" + admStudent.Image);
        //    hmodel.logoPath = logoPath;
        //    hmodel.studentPath = studentPath;
        //    hmodel.organization = admitcsardDetails.Title;
        //    hmodel.address = admitcsardDetails.Address;

        //    hmodel.tollfreeNo = " Phone Number -" + admitcsardDetails.Tollfree;
        //    hmodel.session = "Admission for the Session "+ sessionval;
        //    model.headerModel = hmodel;

        //    AdmitCardStudentModel smodel = new AdmitCardStudentModel();
        //    smodel.title = admitcsardDetails.Title;
        //    smodel.data = new List<List<string>>();
        //    string name = UtilityModel.GetFullName(
        //        admStudent.FirstName, admStudent.MiddleName, admStudent.LastName);


        //    List<string> data2 = new List<string>();
        //    data2.Add("Name:"+ name);
        //    data2.Add("Mobile:" + admission.MobileNumber);
        //    smodel.data.Add(data2);

        //    List<string> data3 = new List<string>();
        //    data3.Add("Date of Submission:" + admission.InsertedDate.ToString("dd-MMM-yyyy"));
        //    smodel.data.Add(data3);

        //    //List<string> data4 = new List<string>();
        //    //data4.Add("Date of Submission:"+ admission.InsertedDate.ToString("dd-MMM-yyyy"));

        //    //smodel.data.Add(data4);

        //    List<string> data5 = new List<string>();
        //    data5.Add("Application No:"+admission.FormNumber);
        //    //pending
        //    data5.Add("Roll No:123");
        //    smodel.data.Add(data5);

        //    List<string> data6 = new List<string>();
        //    data6.Add("Board:"+admData.boardName);
        //    data6.Add("Class:" + admData.className);
        //    smodel.data.Add(data6);

        //    List<string> data7= new List<string>();
        //    data7.Add("Boarding Type:" + admData.wingName);
        //    //data6.Add("Class:" + admData.className);
        //    smodel.data.Add(data7);

        //    model.studentModel = smodel;


        //    AdmitCardInstrModel imodel = new AdmitCardInstrModel();
        //    imodel.title = "OSAT 2020- Your Entrance Details";
        //    imodel.instructions = new List<string>();
        //    for(int i =0; i< admitcardcardInstruction.Count; i++)
        //    {
        //        imodel.instructions.Add(admitcardcardInstruction[i].Instruction);
        //    }
           
        //    //imodel.instructions.Add("ODM Public School is the flagship school of ODM Educational Group, Odisha's emerging educational group. ");
        //    //imodel.instructions.Add("ODM Public School is the flagship school of ODM Educational Group, Odisha's emerging educational group. ");

        //    imodel.notes = new List<string>();
        //    imodel.notes.Add("ODM Public School is the flagship school of ODM Educational Group, Odisha's emerging educational group. ");

        //    model.instrModel = imodel;

        //    AdmitCardVenueModel vmodel = new AdmitCardVenueModel();
        //    vmodel.title = "VENUE";
        //    vmodel.venues = new List<string>();
        //    vmodel.venues.Add(orgVenueDetails);
          

        //    vmodel.notes = new List<string>();
        //    vmodel.notes.Add("(This is system generated Admit Card. Does not require signature)");

        //    model.venueModel = vmodel;

        //    return model;


        //}
        #endregion

        #region--------------Access-----------------------------
        public ModuleActionAccess GetAdmissionAccessDetails()
        {
            ModuleActionAccess acc = new ModuleActionAccess();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            acc.ViewProfile = commonMethods.checkaccessavailable("Registrations", accessId,
                "ViewProfile", "Admission", roleId);

            acc.Pay = commonMethods.checkaccessavailable("Registrations", accessId,
               "Pay", "Admission", roleId);

            acc.AssignCounsellor = commonMethods.checkaccessavailable("Counsellor", accessId,
               "AssignCounsellor", "Admission", roleId);

            acc.CounsellorStatus = commonMethods.checkaccessavailable("Counsellor", accessId,
               "CounsellorStatus", "Admission", roleId);

            acc.ExamDate = commonMethods.checkaccessavailable("Exam", accessId,
               "ExamDate", "Admission", roleId);

            acc.ExamStatus = commonMethods.checkaccessavailable("Exam", accessId,
               "ExamStatus", "Admission", roleId);

            acc.PIDate = commonMethods.checkaccessavailable("PI", accessId,
               "PIDate", "Admission", roleId);

            acc.PIStatus = commonMethods.checkaccessavailable("PI", accessId,
               "PIStatus", "Admission", roleId);

            acc.VerifyDate = commonMethods.checkaccessavailable("DocumentVerification", accessId,
               "VerificationDate", "Admission", roleId);

            acc.VerifyStatus = commonMethods.checkaccessavailable("DocumentVerification", accessId,
               "VerificationStatus", "Admission", roleId);

            acc.AddDocument = commonMethods.checkaccessavailable("DocumentVerification", accessId,
               "AddDocument", "Admission", roleId);

            acc.SubmitDocument = commonMethods.checkaccessavailable("DocumentVerification", accessId,
               "SubmitDocument", "Admission", roleId);


            return acc;

        }
        #endregion-------------------------------------------------

        #region
        public IActionResult SendPaymentLinkToParent(long AdmissionStudentId)
        {
        var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
        if (details.FullName != null )
        {
            smssend.SendRegistrationSms(details.FullName, details.Mobile);
        }
        if (details.EmailId != null)
        {
            senndInBlue.SendRegistrationMail(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName, details.FullName, details.EmergencyContactPerson, details.Mobile, details.EmailId,details.EmailId);
        }

            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = details.AdmissionId }).WithSuccess("Send Payment Link To Parent", "Send Successfully");
        }
        #endregion


        #region---------------------ViewStudentDetails----------------------
        public IActionResult ViewStudentDetails(long id)
        {

            
            studentcounsellorcls ob = new studentcounsellorcls();
            var res = sqlQuery.GetView_Student_Admission_Counsellers().Where(a => a.AdmissionId == id).FirstOrDefault();
            ob.view_Student_ = res;
            var examassign = admissionExamDateAssignRepository.ListAllAsync().Result.Where(a => a.AdmissionStudentId == res.AcademicStudentID && a.Active == true && a.IsAvailable == true).FirstOrDefault();


            var examdates = examassign == null ? null : admissionStandardExamRepository.ListAllAsync().Result.Where(a => a.ID == examassign.AdmissionStandardExamId).FirstOrDefault();
            ob.dateAssign = examassign;
            ob.standardExam = examdates;
            ob.AdmissionID = id;
            var piinterviews = admissionPersonalInterviewAssignRepository.ListAllAsync().Result.Where(a => a.AdmissionStudentId == res.AcademicStudentID).FirstOrDefault();
            var employees = employeeRepository.GetAllEmployee();
            examcounsellercls examcoun = new examcounsellercls();
            examcoun.interviewAssign = piinterviews;
            examcoun.EmployeeName = employees.Where(a => a.ID == piinterviews.EmployeeId).Select(a => new { name = (a.FirstName + " " + a.LastName) }).FirstOrDefault().name;
            examcoun.AssignedCounselorName = employees.Where(a => a.ID == piinterviews.AssignedCounselorId).Select(a => new { name = (a.FirstName + " " + a.LastName) }).FirstOrDefault().name;
            ob.examcounseller = examcoun;

            var documentverification = admissionDocumentVerificationAssignRepository.ListAllAsync().Result.Where(a => a.AdmissionStudentId == res.AcademicStudentID && a.Active == true && a.IsAvailable == true).FirstOrDefault();
            documentcounsellercls mm = new documentcounsellercls();
            mm.AssignedCounselorName = employees.Where(a => a.ID == documentverification.AssignedCounselorId).Select(a => new { name = (a.FirstName + " " + a.LastName) }).FirstOrDefault().name;
            mm.EmployeeName = employees.Where(a => a.ID == documentverification.EmployeeId).Select(a => new { name = (a.FirstName + " " + a.LastName) }).FirstOrDefault().name;
            mm.verificationAssign = documentverification;
            ob.verificationAssign = mm;
            var studentdocument = admissionStudentDocumentRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AdmissionStudentId == res.AcademicStudentID).ToList();


            var documents = admissionDocumentRepository.ListAllAsync().Result.Where(a => a.IsAvailable = true && a.Active == true && a.AdmissionSessionId == res.AcademicSessionId && a.AcademicStandardId == res.AcademicStandardId).ToList();
            var documenttypes = documentTypeRepository.GetAllDocumentTypes();
            var documentsubtypes = documentSubTypeRepository.GetAllDocumentSubTypes();
            var documentresult = (from a in documents
                                  join b in documenttypes on a.DocumentTypeId equals b.ID
                                  join c in documentsubtypes on a.DocumentSubTypeId equals c.ID
                                  select new RequiredDocumentscls
                                  {
                                      DocumentSubTypeId = a.DocumentSubTypeId,
                                      DocumentTypeId = a.DocumentTypeId,
                                      AdmissionDocumentID = a.ID,
                                      DocumentTypeName = b.Name,
                                      DocumentSubTypeName = c.Name,
                                      FilePath = studentdocument != null ? (studentdocument.Where(m => m.AdmissionDocumentId == a.ID).FirstOrDefault() != null ? ("/ODMImages/AdmissionDocuments/" + studentdocument.Where(m => m.AdmissionDocumentId == a.ID).FirstOrDefault().FileName) : "") : "",
                                      IsSubmitted = studentdocument != null ? (studentdocument.Where(m => m.AdmissionDocumentId == a.ID).FirstOrDefault() != null ? true : false) : false

                                  }).ToList();
            ob.documentscls = documentresult;

            return View(ob);
        }
        public IActionResult SubmitExamDates(long txtdate, long hdnassignexamid, long academicStudentID, long admissionID)
        {
            CheckLoginStatus();
            AdmissionExamDateAssign admission = new AdmissionExamDateAssign();
            admission.Active = true;
            admission.AdmissionStandardExamId = txtdate;
            admission.AdmissionStudentId = academicStudentID;
            admission.InsertedDate = DateTime.Now;
            admission.InsertedId = userId;
            admission.ModifiedDate = DateTime.Now;
            admission.ModifiedId = userId;
            admission.Status = "ACTIVE";
            admission.IsAvailable = true;
            admissionExamDateAssignRepository.AddAsync(admission);
            return Json(new { sts = 1, id = admissionID });
        }
        #endregion-----------------------------------------------


        #region-----------------------StudentPayment----------------------------


        [HttpPost]
        public  IActionResult SaveOrUpdateStudentPaymentMode()
        {
            CheckLoginStatus();
            long AdmissionId = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            OdmErp.ApplicationCore.Entities.Admission adm = admissionRepository.GetAdmissionById(AdmissionId);
            long PaymentModeid = Convert.ToInt64(HttpContext.Request.Form["PaymentModeid"]);
            var TransNumber = HttpContext.Request.Form["TransNumber"];
            var PaidAmount = Convert.ToInt64(HttpContext.Request.Form["PaidAmount"]);
            DateTime PaidDate = Convert.ToDateTime(HttpContext.Request.Form["PaidDate"]);
            long ReceivedId = Convert.ToInt64(HttpContext.Request.Form["ReceivedId"]);
            if (AdmissionId != 0)
            {
                AdmissionFormPayment payment = new AdmissionFormPayment();
                payment.AdmissionId = AdmissionId;
                payment.PaymentModeId = PaymentModeid;
                payment.TransactionId = TransNumber;
                payment.AdmissionFeeId = PaidAmount;
                payment.PaidAmount = Convert.ToDecimal(admissionFeeRepository.GetByIdAsync(PaidAmount).Result.Ammount);
                payment.PaidDate = PaidDate;
                payment.ReceivedId = ReceivedId;
                payment.Active = true;
                payment.IsAvailable = true;
                payment.Status = EntityStatus.ACTIVE;
                payment.InsertedDate = DateTime.Now;
                payment.ModifiedDate = DateTime.Now;
                payment.InsertedId = userId;
                payment.ModifiedId = userId;
                var data = admissionFormPaymentRepository.AddAsync(payment).Result;    
                if (data != null && data.ID > 0)
                {
                    DateTimeOffset utcNow = DateTimeOffset.UtcNow;
                    string formNumber = utcNow.Year + "" + utcNow.Month
                        + utcNow.Day + "" + utcNow.Hour + "" +
                       utcNow.Minute + "" + utcNow.Second + "" + utcNow.ToString("ff");

                    OdmErp.ApplicationCore.Entities.Admission admi = admissionRepository.
                        GetAdmissionById(AdmissionId);

                    admi.FormNumber = formNumber;
                    admi.IsAdmissionFormAmountPaid = true;
                    admi.ModifiedDate = DateTime.Now;
                    admi.Status = Admission_Status.PAID;
                    admi.ModifiedId = userId;
                    admissionRepository.UpdateAdmission(admi);

                    OdmErp.ApplicationCore.Entities.Admission admission = admissionRepository.GetAdmissionById(AdmissionId);
                    admission.IsAdmissionFormAmountPaid = true;
                    admissionRepository.UpdateAdmission(admission);

                    AdmissionTimeline timeline = new AdmissionTimeline();
                    timeline.AdmissionId = data.AdmissionId;
                    timeline.Timeline = "PAID";
                    timeline.Active = true;
                    timeline.IsAvailable = true;
                    timeline.Status = EntityStatus.ACTIVE;
                    timeline.InsertedDate = DateTime.Now;
                    timeline.ModifiedDate = DateTime.Now;
                    timeline.InsertedId = userId;
                    timeline.ModifiedId = userId;
                    admissionRepository.CreateAdmissionTimeline(timeline);

                    var feelist = admissionFeeRepository.GetByIdAsyncIncludeAll(data.AdmissionFeeId).Result;
                    var paymentmode = paymentType.GetPaymentTypeById(data.PaymentModeId);
                    pManger.SendReceiptCard(AdmissionId, payment.PaidDate, feelist.Ammount + "",
                 paymentmode.Name,
                 data.TransactionId);
                    try
                    {
                        LeadCreateDto dto = new LeadCreateDto();

                        dto.Status = "PAID";
                        dto.PaidOn = NullableDateTimeToStringDateTime(payment.InsertedDate)
                            .DateTime;



                        LeadManage leadManage = new LeadManage();
                        LeadResponse lead = leadManage.
                            updateLead(leadManage.UpdateLeadData(dto),
                            admi.MobileNumber);
                        return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithSuccess("Student Payment Details", "Saved Successfully");

                    }
                    catch (Exception e)
                    {

                    }                   
                  
                }
                else
                {
                  
                    return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithSuccess("Registration fee",
                "Failed");
                   
                }
            }
            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId });
        }
        #region----------------------Counsellor And Status-------------------------
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateCounsellor()
        {
            CheckLoginStatus();
            ViewBag.AdmissionId = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            long CounsellorId = Convert.ToInt64(HttpContext.Request.Form["CouncellorEmployeeId"]);

            OdmErp.ApplicationCore.Entities.Admission adm = admissionRepository.GetAdmissionById(ViewBag.AdmissionId);
        
            var list = admissionStudentCounsellorRepository.ListAllAsyncIncludeAll().Result.ToList().Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();
            if (list == null)
            {
                AdmissionStudentCounselor payment = new AdmissionStudentCounselor();
                payment.AdmissionStudentId = AdmissionStudentId;
                payment.EmployeeId = CounsellorId;
                payment.AssignedDate = DateTime.Now;
                payment.Remarks = "";
                payment.Active = true;
                payment.IsAvailable = true;
                payment.Status = EntityStatus.ACTIVE;
                payment.InsertedDate = DateTime.Now;
                payment.ModifiedDate = DateTime.Now;
                payment.InsertedId = userId;
                payment.ModifiedId = userId;
                await admissionStudentCounsellorRepository.AddAsync(payment);
                return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithSuccess("Student Counsellor", "Saved Successfully");
            }
            else
            {
                AdmissionStudentCounselor payment2 = admissionStudentCounsellorRepository.ListAllAsyncIncludeAll().Result.ToList().Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();
                payment2.EmployeeId = CounsellorId;
                payment2.ModifiedDate = DateTime.Now;
                payment2.ModifiedId = userId;
                await admissionStudentCounsellorRepository.UpdateAsync(payment2);
                return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithSuccess("Student Counsellor", "Updated Successfully");
            }

           
        }

        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateCounsellorStatus()
        {
            CheckLoginStatus();
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            ViewBag.AdmissionId = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            long CounsellorStatusId = Convert.ToInt64(HttpContext.Request.Form["CounsellorStatusId"]);
            string Remark = HttpContext.Request.Form["Remarks"];
            OdmErp.ApplicationCore.Entities.Admission adm = admissionRepository.GetAdmissionById(ViewBag.AdmissionId);

            AdmissionStudentCounselor payment2 = admissionStudentCounsellorRepository.ListAllAsyncIncludeAll().Result.ToList().Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();
           if(payment2!=null)
            {
                payment2.CounsellorStatusId = CounsellorStatusId;
                payment2.Remarks = Remark;
                payment2.ModifiedDate = DateTime.Now;
                payment2.ModifiedId = userId;
                await admissionStudentCounsellorRepository.UpdateAsync(payment2);               
                return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithSuccess("Student Counsellor Status", "Updated Successfully");


            }

            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithDanger("Please Enter first Counsellor Details", "Then Update Status");


        }

        #endregion-----------------------

        #region------------------- Pi And Pi Status----------------------

        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentPI(long ID, Nullable<DateTime> PIDate, TimeSpan PITime, /*long EmployeeId,*/ long AdmissionStudentId)
        {
            CheckLoginStatus();
            long data = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            ViewBag.AdmissionId = admissionRepository.GetAdmissionById(data).LeadReferenceId;
           
            try
            {
                if (ID != 0)
                {
                    AdmissionPersonalInterviewAssign list = admissionPersonalInterviewAssign.ListAllAsyncIncludeAll().Result.Where(a => a.ID != ID && a.AdmissionStudentId == AdmissionStudentId && a.PIDate.Value.Date == PIDate.Value.Date).FirstOrDefault();
                  
                    if(list == null)
                    {
                        AdmissionPersonalInterviewAssign type1 = admissionPersonalInterviewAssign.GetByIdAsync(ID).Result;
                            
                        if (type1 != null)
                        {
                            type1.EmployeeId = 0;// EmployeeId;
                            type1.ModifiedId = userId;
                            type1.ModifiedDate = DateTime.Now;
                            if (PIDate != null)
                            {
                                type1.PIDate = PIDate;
                            }
                            if (PITime != null)
                            {
                                type1.PITime = PITime;
                            }
                           await admissionPersonalInterviewAssign.UpdateAsync(type1);

                            string pidate = type1.PIDate.Value.ToString("dd-MMM-yyyy");
                            string pitime = GetDateTimeFormat(type1.PIDate.Value, type1.PITime.Value);

                            AdmissionTimeline timeline = new AdmissionTimeline();

                            timeline.Timeline = "PI Date and Time : " +
                                GetDateTimeFormat(PIDate.Value, PITime);
                            timeline.Active = true;
                            timeline.IsAvailable = true;
                            timeline.AdmissionId = data;
                            timeline.Status = "PI Date and Time";
                            timeline.InsertedDate = DateTime.Now;
                            timeline.ModifiedDate = DateTime.Now;
                            timeline.InsertedId = userId;
                            timeline.ModifiedId = userId;
                            admissionRepository.CreateAdmissionTimeline(timeline);
                            #region--------- smg And Email (Complited)------------------

                            var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
                            string Message = HttpAddSmg.PIDate_Time.Replace("{{StudentName}}", details.FullName).Replace("{{PI Date}}", pidate).Replace("{{PI Time}}", pitime);
                            if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                            {
                                smssend.SendPISms(details.Mobile, Message);
                            }
                            if (details.EmailId != null && details.EmailId != "")
                            {
                                Emailsend.PIDateAssign(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName, details.FormNumber, details.FullName, details.Mobile, details.EmailId, pidate, pitime, details.PiDetails.InterviewerName, Message, details.EmailId);
                            }
                            #endregion
                            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithSuccess("Student PI Date", "Updated Sucessfully");
                        }
                        else
                        {
                            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithWarning("Student PI Date", "Already Present");
                        }
                    }                  

                }
                else
                {
                    AdmissionPersonalInterviewAssign typecheck = admissionPersonalInterviewAssign.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == AdmissionStudentId && a.PIDate.Value.Date == PIDate.Value.Date).FirstOrDefault();
                    if (typecheck == null)
                    {
                        AdmissionPersonalInterviewAssign type = new AdmissionPersonalInterviewAssign();
                        type.PIDate = PIDate;
                        type.PITime = PITime;
                        type.AdmissionStudentId = AdmissionStudentId;
                        type.EmployeeId = 0;// EmployeeId;
                        type.AssignedCounselorId = 0;
                        type.InsertedId = userId;
                        type.ModifiedId = userId;
                        type.InsertedDate = DateTime.Now;
                        type.ModifiedDate = DateTime.Now;
                        type.Status = EntityStatus.ACTIVE;
                        type.IsAvailable = true;
                        type.Active = true;
                        await admissionPersonalInterviewAssign.AddAsync(type);
                        string pidate = type.PIDate.Value.ToString("dd-MMM-yyyy");
                        string pitime = GetDateTimeFormat(type.PIDate.Value, type.PITime.Value);
                        AdmissionTimeline timeline = new AdmissionTimeline();

                        timeline.Timeline = "PI Date and Time : " +  GetDateTimeFormat(PIDate.Value, PITime);
                        timeline.Active = true;
                        timeline.AdmissionId = data;
                        timeline.IsAvailable = true;
                        timeline.Status = "PI Date and Time";
                        timeline.InsertedDate = DateTime.Now;
                        timeline.ModifiedDate = DateTime.Now;
                        timeline.InsertedId = userId;
                        timeline.ModifiedId = userId;
                        admissionRepository.CreateAdmissionTimeline(timeline);
                        #region--------- smg And Email (Complited)------------------

                        var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
                        string Message = HttpAddSmg.PIDate_Time.Replace("{{StudentName}}", details.FullName).Replace("{{PI Date}}", pidate).Replace("{{PI Time}}", pitime);
                        if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                        {
                            smssend.SendPISms(details.Mobile, Message);
                        }
                        if (details.EmailId != null && details.EmailId != "")
                        {
                            Emailsend.PIDateAssign(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName, details.FormNumber, details.FullName, details.Mobile, details.EmailId, pidate, pitime, details.PiDetails.InterviewerName, Message, details.EmailId);
                        }
                        #endregion

                        return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithSuccess("Student PI Date", "Saved Sucessfully");
                       
                    }
                    else
                    {
                        return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithWarning("Student PI Date", "Already Present");
                    }

                }               

               
            }
            catch (Exception e)
            {

            }
          

            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId });
        
       
        }

        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentPIStatus(bool Qulified, string Remarks, long Score,long StudentPIID)
        {
            CheckLoginStatus();
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            long data = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            ViewBag.AdmissionId = admissionRepository.GetAdmissionById(data).LeadReferenceId;
          
            AdmissionPersonalInterviewAssign type = admissionPersonalInterviewAssign.ListAllAsyncIncludeAll().Result.Where(b => b.ID == StudentPIID).FirstOrDefault();
                if(type!=null)
                {

                type.IsAppeared = true;
                type.AppearedDateTime = DateTime.Now;
                type.IsQualified = Qulified ;
                type.Score = Score;
                type.Remarks = Remarks;
                type.ModifiedId = userId;
                type.ModifiedDate = DateTime.Now;
                await admissionPersonalInterviewAssign.UpdateAsync(type);
                string pidate = type.PIDate.Value.ToString("dd-MMM-yyyy");
                string pitime = GetDateTimeFormat(type.PIDate.Value, type.PITime.Value);
                string Qualified = type.IsQualified == true ? "Qualified" : "Not Qulified";

                OdmErp.ApplicationCore.Entities.Admission admission = admissionRepository.GetAdmissionById(data);
                admission.IsPersonalInterviewQualified = Qulified;
                admissionRepository.UpdateAdmission(admission);

                AdmissionTimeline timeline = new AdmissionTimeline();
                string status = Qulified == true ? "Qualified" : "Not Qualified";
                timeline.Timeline = "PI Status : "+ status;
                timeline.Active = true;
                timeline.AdmissionId = data;
                timeline.IsAvailable = true;
                timeline.Status = "PI Status";
                timeline.InsertedDate = DateTime.Now;
                timeline.ModifiedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedId = userId;
                admissionRepository.CreateAdmissionTimeline(timeline);             

                #region--------- smg And Email (Complited)------------------
                bool pistatus = Qulified;
                if (pistatus==true)
                {
                    var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
                    string Message = HttpAddSmg.PI_QUALIFIED.Replace("{{StudentName}}", details.FullName);
                    if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                    {
                        smssend.SendPIStatusSendSms(details.Mobile, Message);
                    }
                    if (details.EmailId != null && details.EmailId != "")
                    {
                        Emailsend.PIStatus(details.AcademicSessionName, details.ClassDetails.schoolName,
                        details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName, details.FormNumber, details.FullName, details.Mobile, details.EmailId, pidate, pitime, 
                        details.PiDetails.InterviewerName, Qualified, type.Score.ToString(),type.Remarks, Message, details.EmailId);
                    }
                }
                #endregion
                return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithSuccess("Student PI Status", "Updated Successfully");
            }
          
            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithDanger(" PI Status Details  ", "Failuer");
        }

        #endregion

        #region-----------StudentDocument And Status---------------
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentDocument(long ID,Nullable<DateTime> VerificationDate, TimeSpan VerificationTime/*, long EmployeeId*/)
        {
            CheckLoginStatus();
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            long data = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            ViewBag.AdmissionId = admissionRepository.GetAdmissionById(data).LeadReferenceId;
            OdmErp.ApplicationCore.Entities.Admission adm = admissionRepository.GetAdmissionById(data);


            if (ID != 0)
            {
                AdmissionDocumentVerificationAssign type1 = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.Where(b =>b.ID!=ID && b.AdmissionStudentId == AdmissionStudentId && b.VerificationDate.Value.Date == VerificationDate.Value.Date).FirstOrDefault();
                if (type1 == null)
                {
                    AdmissionDocumentVerificationAssign type2 = admissionDocumentVerificationAssignRepository.GetByIdAsyncIncludeAll(ID).Result;
                    //type2.EmployeeId = 0;// EmployeeId;
                    if (VerificationDate != null)
                    {
                        type2.VerificationDate = VerificationDate;
                    }
                    if (VerificationTime != null)
                    {
                        type2.VerificationTime = VerificationTime;
                    }
                    type2.ModifiedId = userId;
                    type2.ModifiedDate = DateTime.Now;
                    await admissionDocumentVerificationAssignRepository.UpdateAsync(type2);
                    string verificationdate = type2.VerificationDate.Value.ToString("dd-MMM-yyyy");
                    string verificationtime = GetDateTimeFormat(type2.VerificationDate.Value, type2.VerificationTime.Value);
                   
                    AdmissionTimeline timeline = new AdmissionTimeline();
                    timeline.Timeline = "Document Verification Date and Time : " +
                    GetDateTimeFormat(VerificationDate.Value, VerificationTime);
                    timeline.Active = true;
                    timeline.IsAvailable = true;
                    timeline.AdmissionId = data;
                    timeline.Status = "Document Verification Date and Time";
                    timeline.InsertedDate = DateTime.Now;
                    timeline.ModifiedDate = DateTime.Now;
                    timeline.InsertedId = userId;
                    timeline.ModifiedId = userId;
                    admissionRepository.CreateAdmissionTimeline(timeline);


                    #region--------- smg And Email (Complited)------------------

                    var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
                    string Message = HttpAddSmg.DOCDATE_TIME.Replace("{{StudentName}}", details.FullName).Replace("{{Verification Date}}", verificationdate).Replace("{{Verification Time}}", verificationtime);
                    if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                    {
                        smssend.SendDocumentSendSms(details.Mobile, Message);
                    }
                    if (details.EmailId != null && details.EmailId != "")
                    {
                        Emailsend.DocumentDateAssign(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName,
                            details.FormNumber, details.FullName, details.Mobile, details.EmailId, verificationdate, verificationtime, details.DocumentDetails.InterviewerName, Message, details.EmailId);
                    }
                    #endregion
                    return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithSuccess("Student Document Date And Time", "Update- Successfully");


                }
                else
                {
                    return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithWarning("Student Document Date And Time", "Already Present");

                }
            }

            else
            {

                var list = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AdmissionStudentId == AdmissionStudentId && b.VerificationDate.Value.Date == VerificationDate.Value.Date).FirstOrDefault();
                if (list == null)
                {
                    AdmissionDocumentVerificationAssign type =
                        new AdmissionDocumentVerificationAssign();
                    type.VerificationDate = VerificationDate;
                    type.VerificationTime = VerificationTime;
                    type.AdmissionStudentId = AdmissionStudentId;
                    type.EmployeeId = 0;// EmployeeId;
                    type.AssignedCounselorId = 0;
                    type.InsertedId = userId;
                    type.ModifiedId = userId;
                    type.InsertedDate = DateTime.Now;
                    type.ModifiedDate = DateTime.Now;
                    type.Status = EntityStatus.ACTIVE;
                    type.IsAvailable = true;
                    type.Active = true;
                    await admissionDocumentVerificationAssignRepository.AddAsync(type);
                    string verificationdate = type.VerificationDate.Value.ToString("dd-MMM-yyyy");
                    string verificationtime = GetDateTimeFormat(type.VerificationDate.Value, type.VerificationTime.Value);

                    AdmissionTimeline timeline = new AdmissionTimeline();

                    timeline.Timeline = "Document Verification Date and Time : " +
                        GetDateTimeFormat(VerificationDate.Value, VerificationTime);
                    timeline.Active = true;
                    timeline.AdmissionId = data;
                    timeline.IsAvailable = true;
                    timeline.Status = "Document Verification Date and Time";
                    timeline.InsertedDate = DateTime.Now;                   
                    timeline.ModifiedDate = DateTime.Now;
                    timeline.InsertedId = userId;
                    timeline.ModifiedId = userId;
                    admissionRepository.CreateAdmissionTimeline(timeline);

                    #region--------- smg And Email (Complited)------------------

                    var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
                    string Message = HttpAddSmg.DOCDATE_TIME.Replace("{{StudentName}}", details.FullName).Replace("{{Verification Date}}", verificationdate).Replace("{{Verification Time}}", verificationtime);
                    if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                    {
                        smssend.SendDocumentSendSms(details.Mobile, Message);
                    }
                    if (details.EmailId != null && details.EmailId != "")
                    {
                        Emailsend.DocumentDateAssign(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName,
                            details.FormNumber, details.FullName, details.Mobile, details.EmailId, verificationdate, verificationtime, details.DocumentDetails.InterviewerName, Message, details.EmailId);
                    }
                    #endregion


                    return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithSuccess("Student Document Date And Time", "Saved Successfully");

                }
                else
                {
                    return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithWarning("Student Document Date And Time", "Already Present");

                }

            }

           
            
             
               // return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithSuccess("Student Document Date And Time", "Updated Successfully");

            



        }

        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentDocumentStatus(long ID,bool Qualified, string Remarks)
        {
            CheckLoginStatus();
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            long data = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            ViewBag.AdmissionId = admissionRepository.GetAdmissionById(data).LeadReferenceId;
            OdmErp.ApplicationCore.Entities.Admission adm = admissionRepository.GetAdmissionById(data);


            AdmissionDocumentVerificationAssign type = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == ID).FirstOrDefault();
            if (type != null)
            {
                type.IsAppeared = true;
                type.AppearedDateTime = DateTime.Now;
                type.IsQualified = Qualified ;
                type.Remarks = Remarks;
                type.ModifiedId = userId;
                type.ModifiedDate = DateTime.Now;
                await admissionDocumentVerificationAssignRepository.UpdateAsync(type);

                string verificationdate = type.VerificationDate.Value.ToString("dd-MMM-yyyy");
                string verificationtime = GetDateTimeFormat(type.VerificationDate.Value, type.VerificationTime.Value);
                string QualifiedStatus = type.IsQualified == true ? "Qualified" : "Not Qulified";

             
                AdmissionTimeline timeline = new AdmissionTimeline();
                string status = Qualified == true ? "Qualified" : "Not Qualified";
                timeline.Timeline = "Document Verification Status : " + status;
                timeline.Active = true;
                timeline.IsAvailable = true;
                timeline.AdmissionId = data;
                timeline.Status = "Document Verification Status";
                timeline.InsertedDate = DateTime.Now;
                timeline.ModifiedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedId = userId;
                admissionRepository.CreateAdmissionTimeline(timeline);            

                #region--------- smg And Email (Complited)------------------

                var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
                string Message = HttpAddSmg.DOC_STATUS.Replace("{{StudentName}}", details.FullName);
                if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                {
                    smssend.SendDocumentStatusSendSms(details.Mobile, Message);
                }
                if (details.EmailId != null && details.EmailId != "")
                {
                    Emailsend.DocumentVerificationStatus(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName,
                        details.FormNumber, details.FullName, details.Mobile, details.EmailId, verificationdate, verificationtime, details.DocumentDetails.InterviewerName,
                        QualifiedStatus, type.Remarks, Message, details.EmailId);
                }
                #endregion
                return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithSuccess("Student Document Status", "Updated Successfully");

            }
            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithDanger("Please Enter Student Document Details  ", "Then Update Document Status");

        }
        #endregion
        #endregion

        #region---------------Exam Details Updated-----------------------------------------
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentExam(Nullable<DateTime> ExamDate, TimeSpan ExamStartTime,
            TimeSpan ExamEndTime, TimeSpan ReachTime,string DisplayName, Nullable<DateTime> ResultDate)
        {
            CheckLoginStatus();
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            long data = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            ViewBag.AdmissionId = admissionRepository.GetAdmissionById(data).LeadReferenceId ;
            OdmErp.ApplicationCore.Entities.Admission adm = admissionRepository.GetAdmissionById(data);

            string Remark = HttpContext.Request.Form["Remark"];

            AdmissionStandardExam type = new AdmissionStandardExam();
            type.AcademicSessionId = 0;
            type.OrganizationId = 0;
            type.NoOfStudent = 0;
            type.WingId = 0;
            type.BoardStandardId = 0;
            type.ExamTimeDisplayName = DisplayName;
            type.ExamDate = ExamDate;
            type.ResultDate = ResultDate;
            type.ExamStartTime = ExamStartTime;
            type.ExamEndTime = ExamEndTime;
            type.ReachTime = ReachTime;
            type.ModifiedId = userId;
            type.InsertedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.InsertedDate = DateTime.Now;
            type.Active = true;
            type.Status = EntityStatus.ACTIVE;
            type.IsAvailable = true;
            await admissionStandardExamRepository.AddAsync(type);
            long AdmissionStandardExamid = type.ID;
            var list = admissionExamDateAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();
            var exam = admissionExamDateAssignRepository.ListAllAsyncIncludeAll().Result;
            if (list==null)
            {
                AdmissionExamDateAssign assign = new AdmissionExamDateAssign();
                assign.AdmissionStandardExamId = AdmissionStandardExamid;
                assign.AdmissionStudentId = AdmissionStudentId;
                assign.ModifiedId = userId;
                assign.ModifiedDate = DateTime.Now;
                assign.InsertedDate = DateTime.Now;
                assign.Active = true;
                assign.Status = EntityStatus.ACTIVE;
                assign.IsAvailable = true;
                assign.RollNo = exam == null ? 2021001 : (2021001 + exam.LastOrDefault().ID);
                await admissionExamDateAssignRepository.AddAsync(assign);
            }
            else
            {
                AdmissionExamDateAssign assign1= admissionExamDateAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();
                assign1.AdmissionStandardExamId = AdmissionStandardExamid;
                assign1.AdmissionStudentId = AdmissionStudentId;               
                assign1.ModifiedDate = DateTime.Now;               
                assign1.Active = true;
                assign1.Status = EntityStatus.ACTIVE;
                assign1.IsAvailable = true;
                await admissionExamDateAssignRepository.UpdateAsync(assign1);
            }
            #region--------- smg And Email (Complited)------------------
            var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
            string Message = HttpAddSmg.EXAM_DATE_SMG.Replace("{{StudentName}}", details.FullName).Replace("{{ExamDate}}", details.GetExamDetails.ExamDate);
            if (details.Mobile != null && details.Mobile != "" && Message != null && Message != "")
            {
                smssend.Sendsmstoemployee(details.Mobile, Message);
            }
            if (details.EmailId != null && details.EmailId != "")
            {
             Emailsend.ExamDateAssign(details.FullName, details.Mobile, details.GetExamDetails.ExamDate, details.FormNumber, details.GetExamDetails.StartTime, details.GetExamDetails.EndTime, details.GetExamDetails.ReachTime, "Exam Assign" , details.EmailId);
            }
            #endregion

            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = adm.LeadReferenceId }).WithSuccess("Student Exam Date And Time", "Updated Successfully");

            
          
        }


        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentExamStatus(IFormFile file,bool Qualified, string Score)
        {
            CheckLoginStatus();
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            long data = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            ViewBag.AdmissionId = admissionRepository.GetAdmissionById(data).LeadReferenceId;
            var ggg = HttpContext.Request.Form["file"];
            var answersheet = HttpContext.Session.GetString(ImgAnswerUrl);
              var admissionstandardexamlist = admissionExamDateAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();           
            long admissionstandardexamId = admissionstandardexamlist.AdmissionStandardExamId;           
            
            var list = admissionStudentExamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStandardExamId == admissionstandardexamId).FirstOrDefault();
            if (list==null)
            {
                AdmissionStudentExam type1 = new AdmissionStudentExam();
                type1.IsQualified = Qualified;
                type1.Score = Score;
                type1.AdmissionStandardExamId= admissionstandardexamId;
                type1.ModifiedId = userId;
                type1.InsertedId = userId;
                type1.AdmissionStudentId = AdmissionStudentId;
                type1.ModifiedDate = DateTime.Now;
                type1.InsertedDate = DateTime.Now;
                type1.Active = true;
                type1.Status = EntityStatus.ACTIVE;
                type1.IsAvailable = true;
                type1.MarksheetFileName = answersheet;
                await admissionStudentExamRepository.AddAsync(type1);
                string scoremark = type1.Score;

                OdmErp.ApplicationCore.Entities.Admission admission = admissionRepository.GetAdmissionById(data);
                admission.IsExamQualified= Qualified;
                admissionRepository.UpdateAdmission(admission);

                #region--------- smg And Email (Complited)------------------

                var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
                string Message = HttpAddSmg.EXAM_DATE_RESULT.Replace("{{StudentName}}", details.FullName);
                if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                {
                    smssend.Sendsmstoemployee(details.Mobile, Message);
                }
                if (details.EmailId != null && details.EmailId != "")
                {
                    Emailsend.BasicRegd_Paid(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName,
                    details.FormNumber, details.FullName, details.Mobile, details.EmailId, details.FormPayment.ModeOfPayment, details.FormPayment.TransactionId, details.FormPayment.Amount, details.FormPayment.RecievedEmployeeName, Message, details.EmailId);
                }
                #endregion
                return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithSuccess("Student Exam Status", "Updated Successfully");

            }
            else
            {
                AdmissionStudentExam type = admissionStudentExamRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStandardExamId == admissionstandardexamId).FirstOrDefault();
                if (type != null)
                {
                    type.IsQualified = Qualified;
                    type.Score = Score;
                    type.ModifiedId = userId;
                    type.ModifiedDate = DateTime.Now;
                    await admissionStudentExamRepository.UpdateAsync(type);
                    OdmErp.ApplicationCore.Entities.Admission admission = admissionRepository.GetAdmissionById(data);
                    admission.IsExamQualified = Qualified;
                    admissionRepository.UpdateAdmission(admission);

                    string scoremark = type.Score;

                    #region--------------- smg And Email-------------------------
                    var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
                    string Message = "You have scored =" + scoremark;
                    if (details.Mobile != null && details.Mobile != "" && Message != null && Message != "")
                    {
                        smssend.Sendsmstoemployee(details.Mobile, Message);
                    }
                    if (details.EmailId != null && details.EmailId != "")
                    {
                        Emailsend.ExamStatus(details.FullName, details.Mobile, details.GetExamDetails.ExamDate, details.FormNumber, details.GetExamDetails.StartTime, details.GetExamDetails.EndTime, details.GetExamDetails.ReachTime, details.GetExamDetails.AppearedDate, scoremark, "Exam Assign", details.EmailId);
                    }
                    #endregion
                    return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithSuccess("Student Exam Status", "Updated Successfully");

                }               
            }
            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId });

        }
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentFileDocument(long[] AdmissionDocumentId)
        {
            CheckLoginStatus();
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            long data = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            ViewBag.AdmissionId = admissionRepository.
                GetAdmissionById(data).LeadReferenceId;

            ArrayList arlistdocid = new ArrayList(AdmissionDocumentId);

            var admStd = admissionStudentStandardRepository.ListAllAsync().Result.
                 Where(a => a.AdmissionStudentId == AdmissionStudentId && 
                 a.Active == true).FirstOrDefault();

            
                var AcademicStandardId = academicStandardWingRepository.
                    GetByIdAsync(admStd.AcademicStandardWindId).Result
                    .AcademicStandardId;

                if (AdmissionStudentId!=0)
            {
                if (arlistdocid.Count > 0)
                {
                    foreach (long docid in arlistdocid)
                    {
                       // var file = Request.Form.Files;
                     
                        var file= HttpContext.Request.Form.Files["sumitedDocument_" + docid];
                        string status = HttpContext.Request.Form["DocStatus_" + docid];
                        DateTime DatesumitedDocument = Convert.ToDateTime(HttpContext.Request.Form["DatesumitedDocument_" + docid]);

                        long Docs = admissionDocumentRepository.ListAllAsync().Result
                 .Where(a => a.AcademicStandardId == AcademicStandardId && 
                 a.Active == true && a.Status == "ACTIVE"&&a.DocumentSubTypeId==docid).Select(a=>a.ID).FirstOrDefault();
                        long DocsAssigned = 0;
                        if (Docs==0)
                        {
                             DocsAssigned = admissionDocumentAssignRepository.ListAllAsync().Result
                        .Where(a => a.AdmissionStudentId == AdmissionStudentId && 
                        a.Active == true && a.Status == "ACTIVE" && a.DocumentSubTypeId == docid).Select(a => a.ID).FirstOrDefault();
                        }
                       


                        var list2 = Docs==0? DocsAssigned==0?null:
                            admissionStudentDocumentRepository.
                            ListAllAsync().Result.
                            Where(b => b.AdmissionStudentId == AdmissionStudentId 
                            && b.AdmissionDocumentAssignId == DocsAssigned).
                            FirstOrDefault():
                            admissionStudentDocumentRepository.
                            ListAllAsync().Result.
                            Where(b => b.AdmissionStudentId == AdmissionStudentId
                            && b.AdmissionDocumentId == Docs).
                            FirstOrDefault();
                        if (list2 == null)
                        {
                           
                            AdmissionStudentDocument type = new AdmissionStudentDocument();
                            type.AdmissionDocumentAssignId = DocsAssigned;
                            type.AdmissionDocumentId = Docs;
                          
                            type.AdmissionStudentId = AdmissionStudentId;
                            type.SubmittedDate = DateTime.Now;
                            type.IsSubmitted = true;
                            type.InsertedId = userId;
                            type.ModifiedId = userId;
                            type.InsertedDate = DateTime.Now;
                            type.ModifiedDate = DateTime.Now;
                            type.Status = status;
                            if (file!=null)
                            {

                                FileInfo fi = new FileInfo(file.FileName);
                                var newFilename = "Doc"+"_"+ AdmissionStudentId + "_" + String.Format("{0:d}",
                                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                                var webPath = hostingEnv.WebRootPath;
                                string path = System.IO.Path.Combine("", webPath + @"\ODMImages\StudentCertificate\" + newFilename);
                                var pathToSave = newFilename;
                                using (var stream = new FileStream(path, FileMode.Create))
                                {
                                    await file.CopyToAsync(stream);
                                }
                                //type.path = path;
                                type.FileName =  pathToSave;
                            }
                            type.IsAvailable = true;
                            type.Active = true;
                            await admissionStudentDocumentRepository.AddAsync(type);

                        }
                        else
                        {
                            //AdmissionStudentDocument docfile = admissionStudentDocumentRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AdmissionStudentId == AdmissionStudentId && b.AdmissionDocumentId == docid).FirstOrDefault();
                            //if (docfile != null)
                            //{

                            list2.AdmissionDocumentAssignId = DocsAssigned;
                            list2.AdmissionDocumentId = Docs;
                            list2.SubmittedDate = DateTime.Now;
                            list2.Status = status;

                            if (file!=null)
                                
                            {
                                FileInfo fi = new FileInfo(file.FileName);
                                var newFilename = "Doc" + "_" + AdmissionStudentId + "_" + String.Format("{0:d}",
                                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                                var webPath = hostingEnv.WebRootPath;
                                string path = System.IO.Path.Combine("", webPath + @"\ODMImages\StudentCertificate\" + newFilename);
                                var pathToSave = newFilename;
                                using (var stream = new FileStream(path, FileMode.Create))
                                {
                                    await file.CopyToAsync(stream);
                                }
                                //type.path = path;
                                list2.FileName = pathToSave;
                            }
                            list2.ModifiedId = userId;
                            list2.ModifiedDate = DateTime.Now;
                            await admissionStudentDocumentRepository.UpdateAsync(list2);
                            //}
                        }
                    }

                    var list = studentAdmissionMethod.GetStudentFileDocument(AdmissionStudentId);

                    #region--------- smg And Email (Complited)------------------

                    var details = studentAdmissionMethod.GetStudentDetailsByStudentId(AdmissionStudentId);
                    string Message = HttpAddSmg.DOCVERIFICATION_COMPLETED.Replace("{{StudentName}}", details.FullName);
                    if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                    {
                        smssend.Sendsmstoemployee(details.Mobile, Message);
                    }
                 
                    #endregion

                    return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithSuccess("Student File Document ", "Updated Successfully");

                }               
            }
                           
                    

            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId });
        }
        public async Task<IActionResult> 
            SaveOrUpdateStudentExtraFileDocument(long[] AdmissionDocumentAssignId)

        {
            CheckLoginStatus();
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            long data = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            ViewBag.AdmissionId = admissionRepository.GetAdmissionById(data).LeadReferenceId;


            ArrayList arlistassigndocid = new ArrayList(AdmissionDocumentAssignId);

            if (AdmissionStudentId != 0)
            {              

                if (arlistassigndocid.Count > 0)
                {
                    foreach (long assid in arlistassigndocid)
                    {
                        //DateTime DatesumitedDocument1 = Convert.ToDateTime(HttpContext.Request.Form["DateAssignDocument_" + assid]);
                        //string status = HttpContext.Request.Form["DocStatus_" + assid];
                        var list2 = admissionDocumentAssignRepository.ListAllAsync().Result.Where(b => b.AdmissionStudentId == AdmissionStudentId && b.DocumentSubTypeId == assid).FirstOrDefault();
                        if (list2 == null)
                        {
                            AdmissionDocumentAssign type = new AdmissionDocumentAssign();
                          
                            type.AdmissionStudentId = AdmissionStudentId;
                        type.DocumentSubTypeId = assid;
                        type.DocumentTypeId = documentSubTypeRepository.
                            GetDocumentSubTypeById(assid).DocumentTypeID;
                        type.InsertedId = userId;
                            type.ModifiedId = userId;
                            type.InsertedDate = DateTime.Now;
                            type.ModifiedDate = DateTime.Now;
                            type.IsAvailable = true;
                            type.IsAvailable = true;
                            type.Status = EntityStatus.ACTIVE;
                            type.Active = true;
                            await admissionDocumentAssignRepository.AddAsync(type);
                        }
                        else
                        {

                            // AdmissionStudentDocument assignfile = admissionStudentDocumentRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AdmissionStudentId == AdmissionStudentId && b.AdmissionDocumentAssignId == assid).FirstOrDefault();

                            list2.Active = true;
                            list2.Status = EntityStatus.ACTIVE;
                            
                            list2.ModifiedId = userId;
                            list2.ModifiedDate = DateTime.Now;
                            await admissionDocumentAssignRepository.UpdateAsync(list2);

                        }
                        return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithSuccess("Student Document ", " Updated Successfully");

                    }

                }
            }



            return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId });
        }


        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentWiseExamById(long AdmissionStandardExamId)
        {
            CheckLoginStatus();
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            long data = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            ViewBag.AdmissionId = admissionRepository.GetAdmissionById(data).LeadReferenceId;
            var exam = admissionExamDateAssignRepository.ListAllAsyncIncludeAll().Result;
            if (AdmissionStandardExamId != 0)
            {
                var list = admissionExamDateAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();
                if (list == null)
                {
                    AdmissionExamDateAssign assign = new AdmissionExamDateAssign();
                    assign.AdmissionStandardExamId = AdmissionStandardExamId;
                    assign.AdmissionStudentId = AdmissionStudentId;
                    assign.ModifiedId = userId;
                    assign.ModifiedDate = DateTime.Now;
                    assign.InsertedDate = DateTime.Now;
                    assign.Active = true;
                    assign.Status = EntityStatus.ACTIVE;
                    assign.IsAvailable = true;
                    assign.RollNo = exam.Count==0 ? 2021001 : (2021001 + exam.LastOrDefault().ID);
                    await admissionExamDateAssignRepository.AddAsync(assign);
                    return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithSuccess("Student Exam", "Saved Successfully");

                }
                else
                {
                    AdmissionExamDateAssign assign1 = admissionExamDateAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();
                    assign1.AdmissionStandardExamId = AdmissionStandardExamId;
                    assign1.AdmissionStudentId = AdmissionStudentId;
                    assign1.ModifiedDate = DateTime.Now;
                    assign1.Active = true;
                    assign1.Status = EntityStatus.ACTIVE;
                    assign1.IsAvailable = true;
                    await admissionExamDateAssignRepository.UpdateAsync(assign1);
                    return RedirectToAction(nameof(ViewAdmissionDetails), new { id = ViewBag.AdmissionId }).WithSuccess("Student Exam", "Updated Successfully");

                }

            }
            return RedirectToAction(nameof(ViewAdmissionDetails),
                new { id = ViewBag.AdmissionId });



        }



        #endregion


        //#region-------------------- Admitcard-------------------------------------------
        //public async Task<IActionResult>
        //    StudentAdmitCardDetails(string AdmissionId, 
        //    string AdmissionStudentId)
        //{
        //    var admissionSingle = admissionRepository.GetAdmissionById(Convert.ToInt64(AdmissionId));
        //    var admissionStudentSingle = await admissionStudentRepository.GetByIdAsyncIncludeAll(Convert.ToInt64(AdmissionId));
        //    var admissionstudentStandardSingle = (from c in await admissionStudentStandardRepository.ListAllAsyncIncludeAll()
        //                                          where c.AdmissionStudentId == Convert.ToInt64(AdmissionStudentId)
        //                                          join d in await academicStandardWingRepository.ListAllAsyncIncludeAll() on c.AcademicStandardWindId equals d.ID
        //                                          join e in await boardStandardWingRepository.ListAllAsyncIncludeAll() on d.BoardStandardWingID equals e.ID
        //                                          join f in await boardStandardRepository.ListAllAsyncIncludeAll() on e.BoardStandardID equals f.ID
        //                                          join g in standardRepository.GetAllStandard() on f.StandardId equals g.ID
        //                                          select new
        //                                          {
        //                                              c.ID,
        //                                              c.AcademicStandardWindId,
        //                                              c.AdmissionStudentId,
        //                                              c.StandardId,
        //                                              c.Nucleus,
        //                                              OrgId= c.OrganizationId,
        //                                              orgName = organizationRepository.GetOrganizationById(c.OrganizationId).Name,
        //                                              wingName = wingRepository.GetByIdAsync(c.WingId).Result.Name,
        //                                              standard = standardRepository.GetStandardById(c.StandardId).Name,
        //                                              board = boardRepository.GetBoardById(f.BoardId).Name,
        //                                              e.BoardStandardID

        //                                          }).FirstOrDefault();
            
        //  //  var admitcardVenueAddress = admitcardVenueAddressRepository.GetAllAdmitcardVenueAddress().Where(a=>a.IsAvailable==true).ToList();
            
        //    var admitcsardDetails = admitcardDetailsRepository.GetAllAdmitcardDetails().
        //        Where(a=>a.IsAvailable==true).FirstOrDefault();
        //    var admitcardcardInstruction = admitcardInstructionsRepository.
        //        GetAllAdmicardInstructions().
        //        Where(a => a.IsAvailable == true && a.AdmitcardDetailsId== admitcsardDetails.ID).ToList();
        //    var organisationName = organizationRepository.GetAllOrganization().
        //        Where(a => a.ID == admissionstudentStandardSingle.OrgId).FirstOrDefault().Name;
        //    var admitcardvenue = admitcardVenueRepository.GetAllAdmitcardVenue().
        //        Where(a => a.IsAvailable == true).FirstOrDefault();

        //    var orgVenueDetails = admitcardVenueAddressRepository.GetAllAdmitcardVenueAddress().
        //        Where(a => a.ID == admitcardvenue.AdmitcardVenueAddressId && a.IsAvailable == true)
        //        .FirstOrDefault().Address;

        //    // var 
        //    //var orgVenueDetails=(from a in admitcardvenue 
        //    //                    select new
        //    //                    {

        //    //                    })
        //    var sessionName = academicSessionRepository.GetByIdAsync(admissionSingle.AcademicSessionId).Result.DisplayName;
        //    //var cadeStWing = academicStandardWingRepository.GetByIdAsync(acwingId).Result;
        //    //var acStd = academicStandardRepository.GetByIdAsync(cadeStWing.AcademicStandardId).Result;
        //    //var orgAcd = organizationAcademicRepository.GetByIdAsync(acStd.OrganizationAcademicId).Result;
        //    //var org = organizationRepository.GetOrganizationById(orgAcd.OrganizationId);
        //    //var stdBoard = boardStandardRepository.GetByIdAsync(acStd.BoardStandardId).Result;
        //    //var wing = wingRepository.GetByIdAsync(cadeStWing.WingID.Value).Result;
        //    //var bstd = boardRepository.GetBoardById(stdBoard.BoardId);
        //    //var standard = standardRepository.GetStandardById(stdBoard.StandardId);
        //    return Json(new
        //    {
        //        admissionSingleData = admissionSingle,
        //        admissionStudentSingleData = admissionStudentSingle,
        //        admissionStudentStandardData = admissionstudentStandardSingle,
        //        sessionname = sessionName!=null? sessionName:"",
        //        admitcardInstructionList = admitcardcardInstruction,
        //        admitcardDetails= admitcsardDetails,
        //        organisationname= organisationName!=null? organisationName:"",
        //        orgVenueDetail= orgVenueDetails
        //    }) ;



        //}

        //public IActionResult DownloadExamAppearedStudents()
        //{
        //    CheckLoginStatus();
        //    //if (commonMethods.checkaccessavailable("Exam",accessId, "PublishResults", "Admission", roleId) == false)
        //    //{
        //    //    return RedirectToAction("AuthenticationFailed", "Accounts");
        //    //}
        //    if (commonMethods.checkaccessavailable("Registrations", accessId, "Result", "Admission", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }
        //    var academicSession = academicSessionRepository.GetAllAcademicSession()
        //        .Where(a => a.Active == true && a.IsAdmission == true).
        //        FirstOrDefault();
        //    if(academicSession!=null)
        //    {
        //        var admissionStds =
        //            (from a in admissionRepository.GetAllAdmission().Where(a => a.IsAdmissionFormAmountPaid == true)
        //             join b in admissionStudentRepository.ListAllAsync().Result
        //             on a.ID equals b.AdmissionId
        //             select new
        //             {
        //                Session = academicSession.DisplayName,
        //                FormNumber =a.FormNumber,
        //                StudentName=UtilityModel.GetFullName(b.FirstName,
        //                b.MiddleName,b.LastName),
                         

        //             }


        //             ).ToList();


                
        //    }

        //    return null;
        //}

        //#endregion














        #region upad answeer sheet
        [HttpPost]
        public async Task<IActionResult> UploadAnswerSheet(IFormFile files)
        {
            //var id = HttpContext.Session.GetInt32(SessionAdmissionId);
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            string wwwPath = hostingEnv.WebRootPath;
            string contentPath = hostingEnv.ContentRootPath;

            string admissionlink1 = "/ODMImages/Admission/" + AdmissionStudentId;
            string path = System.IO.Path.Combine(hostingEnv.WebRootPath, admissionlink1);
            string admissionlink = @"\ODMImages\Admission\" + AdmissionStudentId + @"\";
            var savedUrl = "";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var pathToSave = "";
            if (files != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(files.FileName);
                var newFilename = "Answer" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                //var webPath = "";
                //if (string.IsNullOrWhiteSpace(hostingEnvironment.WebRootPath))
                //{
                //    webPath= hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                //}
                var webPath = hostingEnv.WebRootPath;
                string path1 = System.IO.Path.Combine("", webPath + admissionlink + newFilename);
                pathToSave = newFilename;
                using (var stream = new FileStream(path1, FileMode.Create))
                {
                   await files.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                savedUrl = admissionlink1 + "/" + pathToSave;
                HttpContext.Session.SetString(ImgAnswerUrl, pathToSave);
            }
            return Json(pathToSave);


        }
        #endregion
    }
}
