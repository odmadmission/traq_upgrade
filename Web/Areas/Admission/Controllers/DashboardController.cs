﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Web.Areas.Admission.Controllers
{
    [Area("Admission")]
    public class DashboardController : Controller
    {
       
        public IActionResult Index()
        {
            return View();
        }
       
    }
}