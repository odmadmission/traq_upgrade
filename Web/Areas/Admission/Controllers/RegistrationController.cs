﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.FileProviders;
using Microsoft.SqlServer.Management.Smo.Agent;
using Microsoft.SqlServer.Management.XEvent;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.Web.Areas.Admission.Models;
using OdmErp.Web.Areas.Support.Models;
using OdmErp.Web.Models;
using OdmErp.Web.SendInBlue;
using OfficeOpenXml;
using Web.Controllers;
using SelectPdf;
using static OdmErp.Web.Areas.Admission.Controllers.BasicRegistrationController;
using Microsoft.Extensions.Configuration;
using System.Threading;

namespace OdmErp.Web.Areas.Admission.Controllers
{
    [Area("Admission")]
    public class RegistrationController : AppController
    {
        private readonly IFileProvider fileProvider;
        private readonly IHostingEnvironment hostingEnvironment;
        const string SessionAdmissionId = "id";
        const string SessionStudentId = "sid";
        const string ImgAnswerUrl = "answerVal";

        private IEmployeeRepository employeeRepository;
        private IDepartmentRepository departmentRepository;
        private IBloodGroupRepository bloodGroupRepository;
        private INationalityTypeRepository nationalityTypeRepository;
        private IAddressTypeRepository addressTypeRepository;
        private ICityRepository cityRepository;
        private IStateRepository stateRepository;
        private ISupportRepository supportRepository;
        private ICountryRepository countryRepository;
        private IDesignationLevelRepository designationLevelRepository;
        private IDesignationRepository designationRepository;
        private IGroupRepository groupRepository;
        private IOrganizationRepository organizationRepository;
        private IReligionTypeRepository religionTypeRepository;
        private IBankTypeRepository bankTypeRepository;
        private IEmployeeDocumentAttachmentRepository employeeDocumentAttachmentRepository;
        private IEmployeeDesignationRepository employeeDesignationRepository;
        private IAddressRepository addressRepository;
        private IEmployeeAddressRepository employeeAddressRepository;
        private IEmployeeExperienceRepository employeeExperienceRepository;
        private IEmployeeBankAccountRepository employeeBankAccountRepository;
        private IBankRepository bankRepository;
        private IBankBranchRepository bankBranchRepository;
        private IEmployeeEducationRepository employeeEducationRepository;
        private IEducationQualificationRepository educationQualificationRepository;
        private IEducationQualificationTypeRepository educationQualificationTypeRepository;
        private IDocumentTypeRepository documentTypeRepository;
        private IAdmissionDocumentRepository admissionDocumentRepository;
        private IDocumentSubTypeRepository documentSubTypeRepository;
        private IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepository;
        private IAccessTypeRepository accessTypeRepository;
        private IStreamRepository streamRepository;
        private IAccessRepository accessRepository;
        private IActionAccessRepository actionAccessRepository;
        private ICategoryRepository categoryRepository;
        private ILanguageRepository languageRepository;
        private IOrganizationAcademicRepository organizationAcademicRepository;
        private IAcademicStandardRepository academicStandardRepository;
        private IBoardStandardRepository boardStandardRepository;
        private IStandardRepository StandardRepository;
        private IBoardRepository boardRepository;
        private IAcademicStandardWingRepository academicStandardWingRepository;
        private IBoardStandardWingRepository boardStandardWingRepository;
        private IWingRepository wingRepository;
        private IAdmissionRepository admissionRepository;
        public smssend smssend;
        public IAdmissionStudentRepository admissionStudentRepository;
        public IAdmissionStudentContactRepository admissionStudentContactRepository;
        public IAdmissionStudentAddressRepository admissionStudentAddressRepository;
        public IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        public IPaymentType paymentType;
        public IProfessionTypeRepository professionTypeRepository;
        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IAdmissionStudentCounselorRepository admissionStudentCounselorRepository;
        public IAdmissionStudentAcademicRepository admissionStudentAcademicRepository;
        public IAdmissionStudentLanguageRepository admissionStudentLanguageRepository;
        public IAdmissionStudentParentRepository admissionStudentParentRepository;
        public IAdmissionStudentProficiencyRepository admissionStudentProficiencyRepository;
        public IAdmissionStudentReferenceRepository admissionStudentReferenceRepository;
        public IAdmissionStudentTransportRepository admissionStudentTransportRepository;
        public IAcademicSessionRepository academicSessionRepository;
        public commonsqlquery commonsqlquery;
        public IStudentAggregateRepository studentAggregateRepository;
        public IStudentLanguagesKnownRepository studentLanguagesKnownRepository;
        public IAcademicStudentRepository academicStudentRepository;
        public IStudentEmergencyContactRepository studentEmergencyContactRepository;
        public IStudentTransportationRepository studentTransportationRepository;
        public AdmissionSqlQuery admissionSqlQuery;
        // private IAdmissionStandardSetting admissionStandardSetting;
        long userId = 0;
        private CommonMethods commonMethods;

        public SendInBlueEmailManager sendinBLUE;
        public string Questionlink { get; private set; }
        public IConfiguration Configuration { get; }
        public RegistrationController(IConfiguration configuration, AdmissionSqlQuery admissionSql, smssend sms, SendInBlueEmailManager sendin, IStudentTransportationRepository studentTransportationRepo, IStudentEmergencyContactRepository studentEmergencyContactRepo, IAcademicStudentRepository academicStudentRepo, IStudentLanguagesKnownRepository studentLanguagesKnownRepo, IStudentAggregateRepository studentAggregateRepo, IFileProvider fileprovider, IHostingEnvironment env, commonsqlquery commonsql, CommonMethods commonMethod, IAcademicSessionRepository academicSessionRepo,
            IEmployeeRepository employeeRepo, IDepartmentRepository departmentRepo, IBloodGroupRepository bloodGroupRepo,
            INationalityTypeRepository nationalityTypeRepo, IAddressTypeRepository addressTypeRepo, ICityRepository cityRepo,
            IStateRepository stateRepo, ICountryRepository countryRepo, IDesignationLevelRepository designationLevelRepo, IDesignationRepository designationRepo,
            IGroupRepository groupRepo, IOrganizationRepository organizationRepo, IReligionTypeRepository religionTypeRepo, IEmployeeDesignationRepository employeeDesignationRepo,
            IAddressRepository addressRepo, IEmployeeAddressRepository employeeAddressRepo, IEmployeeExperienceRepository employeeExperienceRepo,
            IEmployeeBankAccountRepository employeeBankAccountRepo, IBankRepository bankRepo, IBankBranchRepository bankBranchRepo, IEmployeeEducationRepository employeeEducationRepo,
            IEducationQualificationRepository educationQualificationRepo, IEducationQualificationTypeRepository educationQualificationTypeRepo, IDocumentTypeRepository documentTypeRepo,
            IDocumentSubTypeRepository documentSubTypeRepo, IAdmissionDocumentRepository admissionDocumentRepo, IEmployeeRequiredDocumentRepository employeeRequiredDocumentRepo, IAccessTypeRepository accessTypeRepo, IAccessRepository accessRepo, IStreamRepository streamRepo,
            IActionAccessRepository actionAccessRepo, IBankTypeRepository bankTypeRepo, IEmployeeDocumentAttachmentRepository employeeDocumentAttachmentRepo, ICategoryRepository categoryRepo, ILanguageRepository languageRepo, IOrganizationAcademicRepository organizationAcademicRepo, IAcademicStandardRepository academicStandardRepo, IBoardStandardRepository boardStandardRepo, IStandardRepository StandardRepo, IBoardRepository boardRepo, IAcademicStandardWingRepository academicStandardWingRepo, IBoardStandardWingRepository boardStandardWingRepo, IWingRepository wingRepo, IAdmissionRepository admissionRepo, IAdmissionStudentRepository admissionStudentRepository, IAdmissionStudentContactRepository admissionStudentContactRepository,
        IAdmissionStudentAddressRepository admissionStudentAddressRepository, IAdmissionStudentStandardRepository admissionStudentStandardRepository,
        IPaymentType paymentType, IAdmissionFormPaymentRepository admissionFormPaymentRepository, IAdmissionStudentCounselorRepository admissionStudentCounselorRepository, IAdmissionStudentAcademicRepository admissionStudentAcademicRepo, IAdmissionStudentLanguageRepository admissionStudentLanguageRepo, IAdmissionStudentParentRepository admissionStudentParentRepo,
        IAdmissionStudentProficiencyRepository admissionStudentProficiencyRepo, IAdmissionStudentReferenceRepository admissionStudentReferenceRepo, IAdmissionStudentTransportRepository admissionStudentTransportRepo, IProfessionTypeRepository professionTypeRepo, ISupportRepository supportRepo)
        {
            Configuration = configuration;
            admissionSqlQuery = admissionSql;
            smssend = sms;
            sendinBLUE = sendin;
            studentTransportationRepository = studentTransportationRepo;
            studentEmergencyContactRepository = studentEmergencyContactRepo;
            academicStudentRepository = academicStudentRepo;
            studentLanguagesKnownRepository = studentLanguagesKnownRepo;
            studentAggregateRepository = studentAggregateRepo;
            fileProvider = fileprovider;
            hostingEnvironment = env;
            commonMethods = commonMethod;
            commonsqlquery = commonsql;
            bankTypeRepository = bankTypeRepo;
            academicSessionRepository = academicSessionRepo;
            employeeRepository = employeeRepo;
            departmentRepository = departmentRepo;
            bloodGroupRepository = bloodGroupRepo;
            nationalityTypeRepository = nationalityTypeRepo;
            addressTypeRepository = addressTypeRepo;
            cityRepository = cityRepo;
            stateRepository = stateRepo;
            countryRepository = countryRepo;
            designationLevelRepository = designationLevelRepo;
            designationRepository = designationRepo;
            groupRepository = groupRepo;
            organizationRepository = organizationRepo;
            religionTypeRepository = religionTypeRepo;
            employeeDesignationRepository = employeeDesignationRepo;
            addressRepository = addressRepo;
            employeeAddressRepository = employeeAddressRepo;
            streamRepository = streamRepo;
            employeeExperienceRepository = employeeExperienceRepo;
            employeeBankAccountRepository = employeeBankAccountRepo;
            bankRepository = bankRepo;
            bankBranchRepository = bankBranchRepo;
            employeeEducationRepository = employeeEducationRepo;
            educationQualificationRepository = educationQualificationRepo;
            educationQualificationTypeRepository = educationQualificationTypeRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            admissionDocumentRepository = admissionDocumentRepo;
            employeeRequiredDocumentRepository = employeeRequiredDocumentRepo;
            accessTypeRepository = accessTypeRepo;
            accessRepository = accessRepo;
            actionAccessRepository = actionAccessRepo;
            employeeDocumentAttachmentRepository = employeeDocumentAttachmentRepo;
            organizationAcademicRepository = organizationAcademicRepo;
            categoryRepository = categoryRepo;
            languageRepository = languageRepo;
            academicStandardRepository = academicStandardRepo;
            boardStandardRepository = boardStandardRepo;
            StandardRepository = StandardRepo;
            boardRepository = boardRepo;
            academicStandardWingRepository = academicStandardWingRepo;
            boardStandardWingRepository = boardStandardWingRepo;
            wingRepository = wingRepo;
            admissionRepository = admissionRepo;
            this.admissionStudentRepository = admissionStudentRepository;
            this.admissionStudentContactRepository = admissionStudentContactRepository;
            this.admissionStudentAddressRepository = admissionStudentAddressRepository;
            this.admissionStudentStandardRepository = admissionStudentStandardRepository;
            this.paymentType = paymentType;
            this.admissionFormPaymentRepository = admissionFormPaymentRepository;
            this.admissionStudentCounselorRepository = admissionStudentCounselorRepository;
            admissionStudentAcademicRepository = admissionStudentAcademicRepo;
            admissionStudentLanguageRepository = admissionStudentLanguageRepo;
            admissionStudentParentRepository = admissionStudentParentRepo;
            admissionStudentProficiencyRepository = admissionStudentProficiencyRepo;
            admissionStudentReferenceRepository = admissionStudentReferenceRepo;
            admissionStudentTransportRepository = admissionStudentTransportRepo;
            professionTypeRepository = professionTypeRepo;
            supportRepository = supportRepo;
        }

        public IActionResult Profile(string id, string name)
        {
            AdmissionLoginStatus();
            if (id == null || id.Equals(Guid.Empty))
                return RedirectToAction("Error", "Student");

            StudentProfileViewModel model = new StudentProfileViewModel();
            model.TabName = name;
            ViewBag.admissionReferenceId = id;/*leade reference id*/

            var adm = admissionRepository.GetAdmissionByLeadReferenceId(id);
            if (adm == null)
            {
                return RedirectToAction("Error", "Student");
            }
            var student = admissionStudentRepository.GetByAdmissionId(adm.ID).Result;
            ViewBag.admissionId = adm.ID;
            if (student == null)
                return RedirectToAction("Error", "Student");
            ViewBag.admissionstudentid = student.ID;

            return View(model);
        }
        public IActionResult FileInformation()
        {
            return View();
        }
        public IActionResult ExamQuestion()
        {
            return View();
        }
        public List<AcademicSession> GetSessionData()
        {
            var res = (from a in academicSessionRepository.GetAllAcademicSession()
                       where a.IsAdmission == true
                       select new AcademicSession
                       {
                           ID = a.ID,
                           DisplayName = a.DisplayName
                       }).ToList();
            return res;
        }
        public List<AcademicSession> GetAllSessionData()
        {
            var res = (from a in academicSessionRepository.GetAllAcademicSession()
                       select new AcademicSession
                       {
                           ID = a.ID,
                           DisplayName = a.DisplayName
                       }).ToList();
            return res;
        }
        //public List<AcademicSession> GetStream()
        //{
        //    var res = (from a in streamRepository.()
        //               where a.IsAdmission == true
        //               select new AcademicSession
        //               {
        //                   ID = a.ID,
        //                   DisplayName = a.DisplayName
        //               }).ToList();
        //    return res;
        //}
        //GetBoardData GetStream
        public List<Board> GetBoardData()
        {
            var res = (from a in boardRepository.GetAllBoard()
                       select new Board
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return res;
        }

        public JsonResult GetAcademicStandard(long BoardId, long OraganisationAcademicId, long AcademicSessionId)
        {
            // var boardstandard = boardStandardRepository.ListAllAsync().Result.Where(a => a.BoardId == BoardId && a.Active == true).ToList();
            //var academicstandard = commonsqlquery.Get_view_All_Academic_Standards().Where(a => a.AcademicSessionId == AcademicSessionId && a.OraganisationAcademicId == OraganisationAcademicId).ToList();
            //var res = (from a in academicstandard
            //           join b in boardstandard on a.BoardStandardId equals b.ID
            //           select new CommonMasterModel
            //           {
            //               ID = a.ID,
            //               Name = a.StandardName
            //           }).ToList();

            var boardstandard = boardStandardRepository.ListAllAsync().Result.Where(a => a.BoardId == BoardId && a.Active == true).Select(a => a.ID).ToList();
            var boardStandards = academicStandardRepository.ListAllAsync().Result.Where(a => a.OrganizationAcademicId == OraganisationAcademicId
                && a.Active == true && boardstandard.Contains(a.BoardStandardId)).Select(a => a.BoardStandardId).ToList();

            var standards = boardStandardRepository
                .ListAllAsync().Result.Where(a => a.Active == true && boardStandards.Contains(a.ID)).Select(a => a.StandardId).Distinct().ToList();

            //var academicstandard = commonsqlquery.Get_view_All_Academic_Standards().Where(a => a.AcademicSessionId == AcademicSessionId && a.OraganisationAcademicId == OraganisationAcademicId).ToList();
            var res = (from a in standards
                       join b in StandardRepository.GetAllStandard() on a equals b.ID
                       select new CommonMasterModel
                       {
                           ID = a, //StandardId
                           Name = b.Name
                       }).ToList();
            return Json(res);



        }
        //GetDocList  UpdateSaveFile
        public async Task<JsonResult> UpdateSaveFile(long AcademicSessionId, long BoardId, long OraganisationAcademicId, long AcademicStandardId, string[] FileTypeIdArray)
        {
            CheckLoginStatus();
            var boardStandard = (from d in await boardStandardRepository.ListAllAsyncIncludeAll() where d.Active == true && d.BoardId == BoardId && d.StandardId == AcademicStandardId select d.ID).FirstOrDefault();

            var academicStandardid = (from e in await academicStandardRepository.ListAllAsyncIncludeAll() where e.Active == true && e.BoardStandardId == boardStandard && e.OrganizationAcademicId == OraganisationAcademicId select e.ID).FirstOrDefault();
            //var academicStandardId = from f in  academicStandard Where(a => a.BoardStandardId == boardStandard && a.OrganizationAcademicId == OraganisationAcademicId).FirstOrDefault().ID;
            //  var list
            var doctypeId = documentTypeRepository.GetDocumentTypeByName("ADMISSION").ID;
            var SubDocTypeList = documentSubTypeRepository.GetAllDocumentSubTypeByDocumentTypeId(doctypeId).ToList();
            var AllSelectedData = (from a in await admissionDocumentRepository.ListAllAsyncIncludeAll() where a.AcademicStandardId == academicStandardid && a.Status == "ACTIVE" select a.DocumentSubTypeId).ToList();
            List<string> DBfiletypelist = new List<string>();
            List<string> Inputfiletypelist = new List<string>();
            for (int j = 0; j < AllSelectedData.Count(); j++)
            {
                DBfiletypelist.Add(AllSelectedData[j].ToString());

            }
            for (int l = 0; l < FileTypeIdArray.Count(); l++)
            {
                Inputfiletypelist.Add(FileTypeIdArray[l]);
            }

            var removed = DBfiletypelist.Except(Inputfiletypelist).ToList();
            var added = Inputfiletypelist.Except(DBfiletypelist).ToList();
            var unchanged = DBfiletypelist.Intersect(Inputfiletypelist).ToList();
            //sessionScholarshipDiscountApplicableRepository.GetAllSessionScholarshipDiscountApplicable().Where(d => d.SessionScholarshipDiscountPriceId == getSessionscholarshipPrice.ID).ToList();

            //var admissionStudentParentList = (from a in await admissionStudentParentRepository.ListAllAsyncIncludeAll()
            // where a.AdmissionStudentId == studentId
            //       select a).ToList();
            for (int e = 0; e < removed.Count(); e++)
            {
                var removedval = Convert.ToInt64(removed[e]);
                var doclistClasswise = (from x in await admissionDocumentRepository.ListAllAsyncIncludeAll() where x.AcademicStandardId == academicStandardid && x.Status == "ACTIVE" && x.DocumentSubTypeId == removedval select x).FirstOrDefault();
                doclistClasswise.Status = EntityStatus.INACTIVE;
                doclistClasswise.ModifiedId = userId;
                doclistClasswise.ModifiedDate = DateTime.Now;
                doclistClasswise.ModifiedId = userId;
                await admissionDocumentRepository.UpdateAsync(doclistClasswise);
            }
            for (int d = 0; d < added.Count(); d++)
            {
                AdmissionDocument obj = new AdmissionDocument();
                obj.AcademicStandardId = academicStandardid;
                obj.AdmissionSessionId = AcademicSessionId;
                obj.DocumentSubTypeId = Convert.ToInt64(added[d]);
                obj.DocumentTypeId = doctypeId;
                obj.InsertedId = userId;
                obj.ModifiedId = userId;
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.Status = EntityStatus.ACTIVE;
                obj.IsAvailable = true;
                obj.Active = true;
                await admissionDocumentRepository.AddAsync(obj);
            }
            return Json("Data Added Sucessfully.");

        }
        public JsonResult GetDocList(long AcademicSessionId, long BoardId, long OraganisationAcademicId, long AcademicStandardId)
        {
            var boardStandard = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true && a.BoardId == BoardId && a.StandardId == AcademicStandardId).FirstOrDefault().ID;
            var academicStandard = academicStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true);
            var academicStandardId = academicStandard.Where(a => a.BoardStandardId == boardStandard && a.OrganizationAcademicId == OraganisationAcademicId).FirstOrDefault().ID;
            //  var list
            var doctypeId = documentTypeRepository.GetDocumentTypeByName("ADMISSION").ID;
            var SubDocTypeList = documentSubTypeRepository.GetAllDocumentSubTypeByDocumentTypeId(doctypeId).ToList();
            var AllSelectedData = (from a in admissionDocumentRepository.ListAllAsyncIncludeAll().Result where a.AcademicStandardId == academicStandardId && a.Status == "ACTIVE" select a.DocumentSubTypeId).ToList();

            return Json(new { subDocTypeList = SubDocTypeList, allSelectedData = AllSelectedData });

            //return Json(1);           

        }
        public JsonResult GetOrganisationByAcademicSession(long AcademicSessionId)                 //Filter the selected Standard
        {
            try
            {
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.Active == true);
                var OrganisationList = organizationRepository.GetAllOrganization();
                //var academicstandard = academicStandardRepository.ListAllAsyncIncludeAll().Result;

                var res = (from a in OrganisationAcademic
                           join b in OrganisationList on a.OrganizationId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }
                          ).ToList();



                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        public List<BloodGroup> GetBloodGroupData()
        {

            var res = (from a in bloodGroupRepository.GetAllBloodGroup()
                       select new BloodGroup
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return res;
        }
        //professionTypeRepository
        public IActionResult GetprofessionType()
        {

            var res = (from a in professionTypeRepository.GetAllProfessionTypes()
                       select new ProfessionType
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        public IActionResult GetNationalityTypeData()
        {

            var res = (from a in nationalityTypeRepository.GetAllNationalities()
                       select new NationalityType
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }

        public IActionResult GetMotherToungueData()
        {
            var res = (from a in languageRepository.ListAllAsyncIncludeAll().Result
                       select new Language
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return Json(res);
        }
        public IActionResult GetLocationData()
        {
            var res = (from a in supportRepository.GetAllLocation()
                       select new
                       {
                           a.ID,
                           a.Name
                       }).ToList();
            return Json(res);
        }

        public List<Category> GetCategoryData()
        {

            var res = (from a in categoryRepository.ListAllAsyncIncludeAll().Result
                       select new Category
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return res;
        }
        public List<ReligionType> GetReligionType()
        {

            var res = (from a in religionTypeRepository.GetAllReligionTypes()
                       select new ReligionType
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return res;
        }
        public List<BloodGroup> GetBloodGroup()
        {

            var res = (from a in bloodGroupRepository.GetAllBloodGroup()
                       select new BloodGroup
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return res;
        }
        public List<Country> GetCountry()
        {

            var res = (from a in countryRepository.GetAllCountries()
                       select new Country
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return res;
        }
        public List<State> GetStateByCountryId(string id)
        {
            var cid = Convert.ToInt64(id);

            var res = (from a in stateRepository.GetAllStateByCountryId(cid)
                       select new State
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return res;
        }
        public List<State> GetCityByStateId(string id)
        {
            var cid = Convert.ToInt64(id);

            var res = (from a in cityRepository.GetAllCityByStateId(cid)
                       select new State
                       {
                           ID = a.ID,
                           Name = a.Name
                       }).ToList();
            return res;
        }
        public JsonResult GetAllClass()
        {
            var acadmicsessinid = academicSessionRepository.GetAllAcademicSession().Where(a => a.IsAdmission == true).FirstOrDefault().ID;
            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                   Result.Where(a => a.AcademicSessionId == acadmicsessinid && a.Active == true).ToList().Select(a => a.ID);

            var boardStandards = academicStandardRepository.ListAllAsync().Result.
                    Where(a => orgAcademics.Contains(a.OrganizationAcademicId) && a.Active == true).ToList().Select(a => a.BoardStandardId);

            var standardsBoards = boardStandardRepository.ListAllAsync().
                    Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
            var standards = (from a in standardsBoards
                             select new
                             {
                                 boardClass = StandardRepository.GetStandardById(a.StandardId).Name,
                                 boardId = a.BoardId,
                                 boardName = boardRepository.GetBoardById(a.BoardId).Name,
                                 standardId = a.StandardId,
                                 boardStandardId = a.ID,
                                 boardStandardName = boardRepository.GetBoardById(a.BoardId).Name + "--" + StandardRepository.GetStandardById(a.StandardId).Name
                             }
                             ).ToList();
            return Json(standards);
        }
        public JsonResult GetAllClassByBoardStandard(long id)
        {
            var acadmicsessinid = academicSessionRepository.GetAllAcademicSession().Where(a => a.IsAdmission == true).FirstOrDefault().ID;
            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                   Result.Where(a => a.AcademicSessionId == acadmicsessinid && a.Active == true).ToList().Select(a => a.ID);

            var boardStandards = academicStandardRepository.ListAllAsync().Result.
                    Where(a => orgAcademics.Contains(a.OrganizationAcademicId) && a.Active == true).ToList().Select(a => a.BoardStandardId);

            var standardsBoards = boardStandardRepository.ListAllAsync().
                    Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
            var standards = (from a in standardsBoards
                             where a.ID == id
                             select new
                             {
                                 boardClass = StandardRepository.GetStandardById(a.StandardId).Name,
                                 boardId = a.BoardId,
                                 boardName = boardRepository.GetBoardById(a.BoardId).Name,
                                 standardId = a.StandardId,
                                 boardStandardId = a.ID,
                                 boardStandardName = boardRepository.GetBoardById(a.BoardId).Name + "--" + StandardRepository.GetStandardById(a.StandardId).Name
                             }
                             ).ToList();
            return Json(standards);
        }
        public JsonResult GetWingByBoardStandard(long BoardStandardId)
        {

            //return Json(new { schooolwingname = schoolWingNames, nucleus = nucleusBool });
            var standardsBoards = boardStandardRepository.ListAllAsync().
              Result.Where(a => a.ID == BoardStandardId && a.Active == true).ToList();
            var nucleusBool = admissionRepository.GetAllAdmissionStandardSetting().Where(a => a.BoardStandardId == BoardStandardId).Select(a => a.Nucleus);
            var standardsBoardswing = boardStandardWingRepository.ListAllAsync().
                 Result.Where(a => a.BoardStandardID == BoardStandardId && a.Active == true).ToList();
            List<WingOrgModel> list = new List<WingOrgModel>();
            var academicstandard = academicStandardRepository.ListAllAsync().Result;
            var academicstandardwing = academicStandardWingRepository.ListAllAsync().Result;
            var organisation = organizationRepository.GetAllOrganization();
            var session = academicSessionRepository.ListAllAsync().Result.Where(a => a.IsAdmission == true && a.IsAvailable == true).FirstOrDefault();
            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                 Result.Where(a => a.AcademicSessionId == session.ID && a.Active == true).ToList();
            var wings = wingRepository.ListAllAsync().Result;
            foreach (var org in orgAcademics)
            {
                var boardStandards = academicstandard.
                    Where(a => a.OrganizationAcademicId == org.ID && a.Active == true && a.BoardStandardId == BoardStandardId).FirstOrDefault();
                if (boardStandards != null)
                {
                    foreach (var st in standardsBoardswing)
                    {
                        var academicwing = academicstandardwing.Where(a => a.AcademicStandardId == boardStandards.ID && a.Active == true && a.BoardStandardWingID == st.ID).FirstOrDefault();
                        if (academicwing != null)
                        {
                            var wingname = wings.Where(a => a.ID == st.WingID).FirstOrDefault().Name;
                            var organisationname = organisation.Where(a => a.ID == org.OrganizationId).FirstOrDefault().Name;
                            WingOrgModel od = new WingOrgModel();
                            od.AcademicStandardWindId = academicwing.ID;
                            od.Name = organisationname + "-" + wingname;
                            od.SchoolId = org.ID;
                            od.WingId = st.WingID;
                            list.Add(od);
                        }
                    }
                }
            }
            return Json(new { schooolwingname = list, nucleus = nucleusBool });

            //    }
            //        catch (Exception ex)
            //        {
            //            return Json(0);
            //}

        }
        public async Task<IActionResult> GetAdmissionData(string id)
        {



            try
            {
                var admissionSingle = admissionRepository.GetAdmissionByLeadReferenceId(id);
                var admissionStudentSingle = admissionRepository.GetAdmissionStudentById(admissionSingle.ID);
                var studentId = admissionStudentSingle.ID;
                var admissionStudentContactList = (from a in await admissionStudentContactRepository.ListAllAsyncIncludeActiveNoTrack()
                                                   where a.AdmissionStudentId == studentId
                                                   select a).ToList();
                var admissionstudentaddressList = (from b in await admissionStudentAddressRepository.ListAllAsyncIncludeActiveNoTrack()
                                                   where b.AdmissionStudentId == studentId
                                                   select b).ToList();
                var admissionstudentStandardSingle = (from c in admissionStudentStandardRepository.ListAllAsyncIncludeAll().Result
                                                      join d in academicStandardWingRepository.ListAllAsyncIncludeAll().Result on c.AcademicStandardWindId equals d.ID
                                                      join e in boardStandardWingRepository.ListAllAsyncIncludeAll().Result on d.BoardStandardWingID equals e.ID
                                                      join f in boardStandardRepository.ListAllAsyncIncludeAll().Result on e.BoardStandardID equals f.ID
                                                      join g in StandardRepository.GetAllStandard() on f.StandardId equals g.ID
                                                      where c.AdmissionStudentId == studentId && c.Active == true
                                                      select new
                                                      {
                                                          c.ID,
                                                          c.AcademicStandardWindId,
                                                          c.AdmissionStudentId,
                                                          c.StandardId,
                                                          c.Nucleus,
                                                          c.OrganizationId,
                                                          c.WingId,
                                                          e.BoardStandardID
                                                      }).FirstOrDefault();
                var admissionFormPaymentSingle = (from d in await admissionFormPaymentRepository.ListAllAsyncIncludeAll()
                                                  where d.AdmissionId == admissionSingle.ID
                                                  select d).FirstOrDefault();
                var admissionStudentdeclarationSingle = (from e in admissionRepository.GetAllAdmissionStudentDeclaration()
                                                         where e.AdmissionId == admissionSingle.ID
                                                         select e).FirstOrDefault();
                var admissionStudentAcademicSingle = (from a in admissionStudentAcademicRepository.AllAdmissionStudentAcademic()
                                                      where a.AdmissionStudentID == studentId
                                                      select a).FirstOrDefault();
                var admissionStudentLanguageList = (from a in await admissionStudentLanguageRepository.ListAllAsyncIncludeAll()
                                                    join b in await languageRepository.ListAllAsyncIncludeAll() on a.LanguageId equals b.ID
                                                    where a.AdmissionStudentId == studentId
                                                    select new
                                                    {
                                                        b.ID,
                                                        b.Name
                                                    }).ToList();
                var admissionStudentParentList = (from a in await admissionStudentParentRepository.ListAllAsyncIncludeAll()
                                                  where a.AdmissionStudentId == studentId
                                                  select a).ToList();
                var admissionStudentProficiencySingle = (from a in await admissionStudentProficiencyRepository.ListAllAsyncIncludeAll()
                                                         where a.AdmissionStudentID == studentId
                                                         select a).FirstOrDefault();

                List<AdmissionStudentReference> admissionStudentReferenceList = new List<AdmissionStudentReference>();
                var jjj = await admissionStudentReferenceRepository.ListAllAsyncIncludeActiveNoTrack();
                if (await admissionStudentReferenceRepository.ListAllAsyncIncludeAll() != null)
                {
                    admissionStudentReferenceList = (from a in await admissionStudentReferenceRepository.ListAllAsyncIncludeAll()
                                                     where a.AdmissionStudentId == studentId
                                                     select a).ToList();
                }
                else
                {
                    admissionStudentReferenceList = null;
                }
                //var admissionStudentReferenceList = (from a in admissionStudentReferenceRepository.ListAllAsyncIncludeAll().Result
                //                                     where a.AdmissionStudentId == admissionStudentSingle.ID
                //                                     select a).ToList();

                var admissionStudentTransportSingle = (from a in await admissionStudentTransportRepository.ListAllAsyncIncludeAll()
                                                       where a.AdmissionStudentId == studentId
                                                       select a).FirstOrDefault();
                return Json(new
                {
                    admissionSingleData = admissionSingle,
                    admissionStudentSingleData = admissionStudentSingle,
                    admissionStudentContactListData = admissionStudentContactList,
                    admissionstudentaddressListData = admissionstudentaddressList,
                    admissionstudentStandardSingleData = admissionstudentStandardSingle,
                    admissionFormPaymentSingleData = admissionFormPaymentSingle,
                    admissionStudentdeclarationSingleData = admissionStudentdeclarationSingle,
                    admissionStudentAcademicSingleData = admissionStudentAcademicSingle,
                    admissionStudentLanguageListData = admissionStudentLanguageList,
                    admissionStudentParentListData = admissionStudentParentList,
                    admissionStudentProficiencySingleData = admissionStudentProficiencySingle,
                    admissionStudentReferenceListData = admissionStudentReferenceList,
                    admissionStudentTransportSingledata = admissionStudentTransportSingle,
                    total = ((admissionStudentSingle == null ? 0 : 1) + (admissionStudentContactList.Count == 0 ? 0 : admissionStudentContactList.Count == 0 ? 0 : 1) +
                    (admissionstudentaddressList.Count == 0 ? 0 : admissionstudentaddressList.Count == 0 ? 0 : 1) + (admissionstudentStandardSingle == null ? 0 : 1) + (admissionStudentdeclarationSingle == null ? 0 : 1) + (admissionStudentAcademicSingle == null ? 0 : 1) + (admissionStudentParentList.Count == 0 ? 0 : admissionStudentParentList.Count == 0 ? 0 : 1) + (admissionStudentProficiencySingle == null ? 0 : 1) + (admissionStudentReferenceList.Count == 0 ? 0 : admissionStudentReferenceList.Count == 0 ? 0 : 1) + (admissionStudentTransportSingle == null ? 0 : 1))

                });


            }
            catch (Exception e)
            {
                return null;
            }



        }
        //UploadAnswerSheet

        [HttpPost]
        public IActionResult UploadQuestion(IFormFile files)
        {
            try
            {
                string wwwPath = hostingEnvironment.WebRootPath;
                string contentPath = hostingEnvironment.ContentRootPath;

                string Questionlink1 = "/ODMImages/AdmissionQuestion/";
                string path = Path.Combine(hostingEnvironment.WebRootPath, Questionlink1);
                string Questionlink = @"\ODMImages\AdmissionQuestion\";
                var savedUrl = "";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                if (files != null)
                {
                    // Create a File Info 
                    FileInfo fi = new FileInfo(files.FileName);
                    var newFilename = "Question" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    //var webPath = "";
                    //if (string.IsNullOrWhiteSpace(hostingEnvironment.WebRootPath))
                    //{
                    //    webPath= hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                    //}
                    var webPath = hostingEnvironment.WebRootPath;
                    string path1 = Path.Combine("", webPath + Questionlink + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path1, FileMode.Create))
                    {
                        files.CopyTo(stream);
                    }
                    FileInfo fil = new FileInfo(path);
                    savedUrl = newFilename;
                }
                return Json(savedUrl);
            }
            catch (Exception ex)
            {
                return Json(0);
            }


        }
        public async Task<IActionResult> UploadDataWithStudentImage(string url)
        {


            //string wwwPath = hostingEnvironment.WebRootPath;
            //string contentPath = hostingEnvironment.ContentRootPath;
            //string admissionlink = "Admission/myid_1";
            //string path = Path.Combine(hostingEnvironment.WebRootPath, admissionlink);
            //if (!Directory.Exists(path))
            //{
            //    Directory.CreateDirectory(path);
            //}
            try
            {
                var id = HttpContext.Session.GetInt32(SessionAdmissionId);

                string wwwPath = hostingEnvironment.WebRootPath;
                string contentPath = hostingEnvironment.ContentRootPath;

                string admissionlink1 = "/ODMImages/Admission/" + id;
                string path = Path.Combine(hostingEnvironment.WebRootPath, admissionlink1);
                string admissionlink = @"\ODMImages\Admission\" + id + @"\";
                var savedUrl = "";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                // FileInfo fi = new FileInfo(files.FileName);
                var newFilename = "Student" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + ".jpeg";
                url = url.Split(',')[1];
                byte[] bytess = Convert.FromBase64String(url);
                var webPath = hostingEnvironment.WebRootPath;
                string path1 = Path.Combine("", webPath + admissionlink + newFilename);

                using (var imageFile = new FileStream(path1, FileMode.Create))
                {
                    imageFile.Write(bytess, 0, bytess.Length);
                    imageFile.Flush();
                }
                //byte[] bytes = Convert.FromBase64String(url);
                //Image image;
                //using (MemoryStream ms = new MemoryStream(bytes))
                //{
                //    image = Image.FromStream(ms);
                //}

                //byte[] imgByteArray = Convert.FromBase64String(url);
                //File.WriteAllBytes(admissionlink +"/"+ newFilename, imgByteArray);
                savedUrl = admissionlink1 + "/" + newFilename;
                return Json(savedUrl);
            }
            catch (Exception ex)
            {
                return Json("");
            }

        }


        [HttpPost]
        public async Task<IActionResult> UploadData(IFormFile files)
        {
            var id = HttpContext.Session.GetInt32(SessionAdmissionId);

            string wwwPath = hostingEnvironment.WebRootPath;
            string contentPath = hostingEnvironment.ContentRootPath;

            string admissionlink1 = "/ODMImages/Admission/" + id;
            string path = Path.Combine(hostingEnvironment.WebRootPath, admissionlink1);
            string admissionlink = @"\ODMImages\Admission\" + id + @"\";
            var savedUrl = "";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (files != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(files.FileName);
                var newFilename = "Student" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                //var webPath = "";
                //if (string.IsNullOrWhiteSpace(hostingEnvironment.WebRootPath))
                //{
                //    webPath= hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                //}
                var webPath = hostingEnvironment.WebRootPath;
                string path1 = Path.Combine("", webPath + admissionlink + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path1, FileMode.Create))
                {
                    await files.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                savedUrl = admissionlink1 + "/" + pathToSave;
            }
            return Json(savedUrl);
        }
        [HttpPost]
        public IActionResult UploadSignature(IFormFile files)
        {
            var id = HttpContext.Session.GetInt32(SessionAdmissionId);

            string wwwPath = hostingEnvironment.WebRootPath;
            string contentPath = hostingEnvironment.ContentRootPath;

            string admissionlink1 = "/ODMImages//Admission/" + id;
            string path = Path.Combine(hostingEnvironment.WebRootPath, admissionlink1);
            string admissionlink = @"\ODMImages\Admission\" + id + @"\";
            var savedUrl = "";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (files != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(files.FileName);
                var newFilename = "ParentSign" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                //var webPath = "";
                //if (string.IsNullOrWhiteSpace(hostingEnvironment.WebRootPath))
                //{
                //    webPath= hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                //}
                var webPath = hostingEnvironment.WebRootPath;
                string path1 = Path.Combine("", webPath + admissionlink + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path1, FileMode.Create))
                {
                    files.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                savedUrl = admissionlink1 + "/" + pathToSave;
            }
            return Json(savedUrl);
        }
        //UploadDataFather
        [HttpPost]
        public async Task<IActionResult> UploadDataFather(IFormFile files)
        {
            var id = HttpContext.Session.GetInt32(SessionAdmissionId);

            string wwwPath = hostingEnvironment.WebRootPath;
            string contentPath = hostingEnvironment.ContentRootPath;

            string admissionlink1 = "/ODMImages//Admission/" + id;
            string path = Path.Combine(hostingEnvironment.WebRootPath, admissionlink1);
            string admissionlink = @"\ODMImages\Admission\" + id + @"\";
            var savedUrl = "";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (files != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(files.FileName);
                var newFilename = "Father" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                //var webPath = "";
                //if (string.IsNullOrWhiteSpace(hostingEnvironment.WebRootPath))
                //{
                //    webPath= hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                //}
                var webPath = hostingEnvironment.WebRootPath;
                string path1 = Path.Combine("", webPath + admissionlink + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path1, FileMode.Create))
                {
                    await files.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                savedUrl = admissionlink1 + "/" + pathToSave;
            }
            return Json(savedUrl);
        }

        [HttpPost]
        public async Task<IActionResult> UploadDataMother(IFormFile files)
        {
            var id = HttpContext.Session.GetInt32(SessionAdmissionId);

            string wwwPath = hostingEnvironment.WebRootPath;
            string contentPath = hostingEnvironment.ContentRootPath;

            string admissionlink1 = "/ODMImages//Admission/" + id;
            string path = Path.Combine(hostingEnvironment.WebRootPath, admissionlink1);
            string admissionlink = @"\ODMImages\Admission\" + id + @"\";
            var savedUrl = "";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (files != null)
            {
                // Create a File Info 
                FileInfo fi = new FileInfo(files.FileName);
                var newFilename = "Mother" + String.Format("{0:d}",
                                  (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                //var webPath = "";
                //if (string.IsNullOrWhiteSpace(hostingEnvironment.WebRootPath))
                //{
                //    webPath= hostingEnvironment.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                //}
                var webPath = hostingEnvironment.WebRootPath;
                string path1 = Path.Combine("", webPath + admissionlink + newFilename);
                var pathToSave = newFilename;
                using (var stream = new FileStream(path1, FileMode.Create))
                {
                    await files.CopyToAsync(stream);
                }
                FileInfo fil = new FileInfo(path);
                savedUrl = admissionlink1 + "/" + pathToSave;
            }
            return Json(savedUrl);
        }

        [HttpPost]
        public IActionResult UploadStudentData(IFormFile formData)
        {
            string wwwPath = hostingEnvironment.WebRootPath;
            string contentPath = hostingEnvironment.ContentRootPath;
            string admissionlink = "Admission/myid_1";
            string path = Path.Combine(hostingEnvironment.WebRootPath, admissionlink);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return Json(1);
        }
        public IActionResult UpdateStudentAdmission(string formNo)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var id = HttpContext.Session.GetInt32(SessionAdmissionId);
            var admission = admissionRepository.GetAdmissionById(Convert.ToInt64(id));
            admission.FormNumber = formNo;
            admission.ModifiedDate = DateTime.Now;
            admission.ModifiedId = accessId;
            admissionRepository.UpdateAdmission(admission);
            return Json(1);
        }
        public IActionResult UploadStudentDetails(AdmissionStudent StudentData)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            AdmissionStudent studentobj = admissionStudentRepository.GetByIdAsync(StudentData.ID).Result;
            if (StudentData.Image != null)
            {
                studentobj.Image = StudentData.Image;
            }
            studentobj.FirstName = StudentData.FirstName;
            studentobj.MiddleName = StudentData.MiddleName;
            studentobj.LastName = StudentData.LastName;
            studentobj.Gender = StudentData.Gender;
            studentobj.BloodGroupID = StudentData.BloodGroupID;
            studentobj.NatiobalityID = StudentData.NatiobalityID;
            studentobj.ReligionID = StudentData.ReligionID;
            studentobj.CategoryID = StudentData.CategoryID;
            studentobj.MotherTongueID = StudentData.MotherTongueID;
            studentobj.PassportNO = StudentData.PassportNO;
            studentobj.AadharNO = StudentData.AadharNO;
            studentobj.DateOfBirth = StudentData.DateOfBirth;
            studentobj.IsAvailable = true;
            studentobj.ModifiedId = accessId;
            studentobj.ModifiedDate = DateTime.Now;
            admissionStudentRepository.UpdateAsync(studentobj);
            return Json(1);
        }
        public IActionResult UpdateDeclaration(AdmissionStudentDeclaration objAdDeclaration)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var sid = HttpContext.Session.GetString(SessionStudentId);
            var studentid = sid.Split("_")[1];
            AdmissionStudentDeclaration studentaddressobj = new AdmissionStudentDeclaration();
            if (objAdDeclaration.ID != 0)
            {
                AdmissionStudentDeclaration studentDeclarobj1 = admissionRepository.GetAdmissionStudentDeclarationById(objAdDeclaration.ID);
                studentDeclarobj1.IsSon = objAdDeclaration.IsSon;
                studentDeclarobj1.IsDuaghter = objAdDeclaration.IsDuaghter;
                studentDeclarobj1.IsFather = objAdDeclaration.IsFather;
                studentDeclarobj1.IsMother = objAdDeclaration.IsMother;
                studentDeclarobj1.IsGuardian = objAdDeclaration.IsGuardian;
                studentDeclarobj1.ClassId = objAdDeclaration.ClassId;
                studentDeclarobj1.SignatureFileName = objAdDeclaration.SignatureFileName;
                studentDeclarobj1.StudentName = objAdDeclaration.StudentName;
                studentDeclarobj1.ParentName = objAdDeclaration.ParentName;
                studentDeclarobj1.ModifiedDate = DateTime.Now;
                studentDeclarobj1.ModifiedId = accessId;

                admissionRepository.UpdateAdmissionStudentDeclaration(studentDeclarobj1);
            }
            else
            {
                //  AdmissionStudentAddress studentaddressobj = new AdmissionStudentAddress();
                studentaddressobj.IsSon = objAdDeclaration.IsSon;
                studentaddressobj.IsDuaghter = objAdDeclaration.IsDuaghter;
                studentaddressobj.IsFather = objAdDeclaration.IsFather;
                studentaddressobj.IsMother = objAdDeclaration.IsMother;
                studentaddressobj.IsGuardian = objAdDeclaration.IsGuardian;
                studentaddressobj.ClassId = objAdDeclaration.ClassId;
                studentaddressobj.SignatureFileName = objAdDeclaration.SignatureFileName;
                studentaddressobj.StudentName = objAdDeclaration.StudentName;
                studentaddressobj.ParentName = objAdDeclaration.ParentName;
                studentaddressobj.InsertedId = accessId;
                studentaddressobj.ModifiedId = accessId;
                studentaddressobj.IsAvailable = true;
                studentaddressobj.Status = "ACTIVE";
                studentaddressobj.Active = true;
                studentaddressobj.AdmissionId = Convert.ToInt64(HttpContext.Session.GetInt32(SessionAdmissionId));
                var changeid1 = admissionRepository.CreateAdmissionStudentDeclaration(studentaddressobj);

            }
            if (studentaddressobj.ID > 0)
            {
                return Json(studentaddressobj.ID);
            }
            else
            {
                return Json(0);
            }
        }
        public IActionResult UploadExamQuestion(long AcademicSession, long OraganisationId, long Board, long AcademicStandard, string Imageval)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            //  var boardStandard = (from a in boardStandardRepository.ListAllAsyncIncludeAll().Result where a.BoardId == Convert.ToInt64(Board) && a.StandardId == Convert.ToInt64(AcademicStandard) select a.ID).FirstOrDefault();

            AdmissionStandardExamQuestion od = admissionRepository.GetAllAdmissionStandardExamQuestion().Where(a => a.AcademicSessionId == AcademicSession && a.OrganizationId == OraganisationId && a.BoardStandardId == AcademicStandard && a.Active == true).FirstOrDefault();
            if (od != null)
            {
                od.OrganizationId = Convert.ToInt64(OraganisationId);
                od.QuestionUrl = Imageval;
                od.BoardStandardId = AcademicStandard;
                od.AcademicSessionId = Convert.ToInt64(AcademicSession);
                od.IsAvailable = true;
                od.Active = true;
                od.Status = "ACTIVE";
                od.ModifiedDate = DateTime.Now;
                od.ModifiedId = userId;
                admissionRepository.UpdateAdmissionStandardExamQuestion(od);
                return Json(od.ID);
            }
            else
            {
                AdmissionStandardExamQuestion obj = new AdmissionStandardExamQuestion();
                obj.OrganizationId = Convert.ToInt64(OraganisationId);
                obj.QuestionUrl = Imageval;
                obj.BoardStandardId = AcademicStandard;
                obj.AcademicSessionId = Convert.ToInt64(AcademicSession);
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = "ACTIVE";
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = userId;
                var returnval = admissionRepository.CreateAdmissionStandardExamQuestion(obj);
                return Json(returnval);
            }
            
        }
        //public async Task<IActionResult> UpdateStudentAddress(AdmissionStudentAddress StudentPreAddressData)
        //{
        //    long accessId = HttpContext.Session.GetInt32("accessId").Value;
        //    long roleId = HttpContext.Session.GetInt32("roleId").Value;
        //    if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
        //    {
        //        return RedirectToAction("AuthenticationFailed", "Accounts");
        //    }
        //    var sid = HttpContext.Session.GetString(SessionStudentId);
        //    var studentid = sid.Split("_")[1];
        //    AdmissionStudentAddress studentaddressobj = new AdmissionStudentAddress();
        //    //var sidval = "sid_" + studentid;
        //    if (StudentPreAddressData.ID != 0)
        //    {
        //        AdmissionStudentAddress studentaddressobj1 = admissionStudentAddressRepository.GetByIdAsync(StudentPreAddressData.ID).Result;
        //        studentaddressobj1.AddressLine1 = StudentPreAddressData.AddressLine1;
        //        studentaddressobj1.AddressLine2 = StudentPreAddressData.AddressLine2;
        //        studentaddressobj1.AddressType = StudentPreAddressData.AddressType;
        //        studentaddressobj1.CountryId = StudentPreAddressData.CountryId;
        //        studentaddressobj1.StateId = StudentPreAddressData.StateId;
        //        studentaddressobj1.CityId = StudentPreAddressData.CityId;
        //        studentaddressobj1.PostalCode = StudentPreAddressData.PostalCode;
        //        studentaddressobj1.ModifiedDate = DateTime.Now;
        //        studentaddressobj1.ModifiedId = accessId;
        //        await admissionStudentAddressRepository.UpdateAsync(studentaddressobj);
        //    }
        //    else
        //    {
        //        //  AdmissionStudentAddress studentaddressobj = new AdmissionStudentAddress();
        //        studentaddressobj.AddressLine1 = StudentPreAddressData.AddressLine1;
        //        studentaddressobj.AddressLine2 = StudentPreAddressData.AddressLine2;
        //        studentaddressobj.AddressType = StudentPreAddressData.AddressType;
        //        studentaddressobj.CountryId = StudentPreAddressData.CountryId;
        //        studentaddressobj.StateId = StudentPreAddressData.StateId;
        //        studentaddressobj.CityId = StudentPreAddressData.CityId;
        //        studentaddressobj.PostalCode = StudentPreAddressData.PostalCode;
        //        studentaddressobj.InsertedDate = DateTime.Now;
        //        studentaddressobj.ModifiedDate = DateTime.Now;
        //        studentaddressobj.InsertedId = accessId;
        //        studentaddressobj.ModifiedId = accessId;
        //        studentaddressobj.IsAvailable = true;
        //        studentaddressobj.Status = "ACTIVE";
        //        studentaddressobj.Active = true;
        //        studentaddressobj.AdmissionStudentId = Convert.ToInt64(studentid);
        //        var changeid1 = await admissionStudentAddressRepository.AddAsync(studentaddressobj);

        //    }
        //    if (studentaddressobj.ID > 0)
        //    {
        //        return Json(studentaddressobj.ID);
        //    }
        //    else
        //    {
        //        return Json(0);
        //    }

        //}

        public async Task<ActionResult> UpdateStudentTransfort(AdmissionStudentTransport StudentTransportData)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var sid = HttpContext.Session.GetString(SessionStudentId);
            var studentid = sid.Split("_")[1];
            AdmissionStudentTransport obj = new AdmissionStudentTransport();
            if (StudentTransportData.ID != 0)
            {
                var studentTransport = await admissionStudentTransportRepository.GetByIdAsync(StudentTransportData.ID);
                if (StudentTransportData.IsTransport == true)
                {
                    studentTransport.IsTransport = true;
                    studentTransport.LocationId = StudentTransportData.LocationId;
                    studentTransport.Distance = StudentTransportData.Distance;


                }
                else
                {
                    studentTransport.IsTransport = false;
                    studentTransport.LocationId = "";
                    studentTransport.Distance = 0;
                }
                studentTransport.AdmissionStudentId = Convert.ToInt64(studentid);
                await admissionStudentTransportRepository.UpdateAsync(studentTransport);
            }
            else
            {

                if (StudentTransportData.IsTransport == true)
                {
                    obj.IsTransport = true;
                    obj.LocationId = StudentTransportData.LocationId;
                    obj.Distance = StudentTransportData.Distance;
                }
                else
                {
                    obj.IsTransport = false;
                    obj.LocationId = "";
                    obj.Distance = 0;
                }
                obj.AdmissionStudentId = Convert.ToInt64(studentid);
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.InsertedId = accessId;
                obj.ModifiedId = accessId;
                obj.IsAvailable = true;
                obj.Status = "ACTIVE";
                obj.Active = true;
                await admissionStudentTransportRepository.AddAsync(obj);

            }
            if (obj.ID > 0)
            {
                return Json(obj.ID);
            }
            else
            {
                return Json(0);
            }


        }
        public async Task<ActionResult> UpdateStudentProficiency(AdmissionStudentProficiency studentProficiencyData)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var sid = HttpContext.Session.GetString(SessionStudentId);
            var studentid = sid.Split("_")[1];
            AdmissionStudentProficiency obj = new AdmissionStudentProficiency();
            if (studentProficiencyData.ID != 0)
            {
                var academicProficiency = await admissionStudentProficiencyRepository.GetByIdAsync(studentProficiencyData.ID);
                academicProficiency.PerformingArts = studentProficiencyData.PerformingArts;
                academicProficiency.Sports = studentProficiencyData.Sports;
                academicProficiency.PositionHeld = studentProficiencyData.PositionHeld;
                academicProficiency.ModifiedDate = DateTime.Now;
                academicProficiency.ModifiedId = accessId;
                await admissionStudentProficiencyRepository.UpdateAsync(academicProficiency);
            }
            else
            {

                obj.Sports = studentProficiencyData.Sports;
                obj.PerformingArts = studentProficiencyData.PerformingArts;
                obj.PositionHeld = studentProficiencyData.PositionHeld;
                obj.AdmissionStudentID = Convert.ToInt64(studentid);
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.InsertedId = accessId;
                obj.ModifiedId = accessId;
                obj.IsAvailable = true;
                obj.Status = "ACTIVE";
                obj.Active = true;
                await admissionStudentProficiencyRepository.AddAsync(obj);
            }
            if (obj.ID > 0)
            {
                return Json(obj.ID);
            }
            else
            {
                return Json(0);
            }


        }
        public async Task<ActionResult> UpdateStudentAcademic(AdmissionStudentAcademic StudentAcademicData)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            AdmissionStudentAcademic newAdmissionStudentAcademicObj = new AdmissionStudentAcademic();
            var sid = HttpContext.Session.GetString(SessionStudentId);
            var studentid = sid.Split("_")[1];
            if (StudentAcademicData.ID != 0)
            {
                var academicSingle = await admissionStudentAcademicRepository.GetByIdAsync(StudentAcademicData.ID);
                //academicSingle.AdmissionStudentID = Convert.ToInt64(studentid);
                academicSingle.AggregatePercentageOrGrade = StudentAcademicData.AggregatePercentageOrGrade;
                academicSingle.CountryId = StudentAcademicData.CountryId;
                academicSingle.StateId = StudentAcademicData.StateId;
                academicSingle.CityId = StudentAcademicData.CityId;
                academicSingle.ReasonForChange = StudentAcademicData.ReasonForChange;
                academicSingle.OrganizationName = StudentAcademicData.OrganizationName;
                academicSingle.ModifiedId = accessId;
                academicSingle.ModifiedDate = DateTime.Now;
                await admissionStudentAcademicRepository.UpdateAsync(academicSingle);
            }
            else
            {
                // AdmissionStudentAcademic newAdmissionStudentAcademicObj = new AdmissionStudentAcademic();
                newAdmissionStudentAcademicObj.AdmissionStudentID = Convert.ToInt64(studentid);
                newAdmissionStudentAcademicObj.AggregatePercentageOrGrade = StudentAcademicData.AggregatePercentageOrGrade;
                newAdmissionStudentAcademicObj.CountryId = StudentAcademicData.CountryId;
                newAdmissionStudentAcademicObj.StateId = StudentAcademicData.StateId;
                newAdmissionStudentAcademicObj.CityId = StudentAcademicData.CityId;
                newAdmissionStudentAcademicObj.OrganizationName = StudentAcademicData.OrganizationName;
                newAdmissionStudentAcademicObj.ReasonForChange = StudentAcademicData.ReasonForChange;
                newAdmissionStudentAcademicObj.IsAvailable = true;
                newAdmissionStudentAcademicObj.Active = true;
                newAdmissionStudentAcademicObj.Status = "ACTIVE";
                newAdmissionStudentAcademicObj.InsertedDate = DateTime.Now;
                newAdmissionStudentAcademicObj.InsertedId = accessId;
                newAdmissionStudentAcademicObj.ModifiedDate = DateTime.Now;
                newAdmissionStudentAcademicObj.ModifiedId = accessId;
                await admissionStudentAcademicRepository.AddAsync(newAdmissionStudentAcademicObj);
            }
            if (newAdmissionStudentAcademicObj.ID > 0)
            {
                return Json(newAdmissionStudentAcademicObj.ID);
            }
            else
            {
                return Json(0);
            }
            // return Json(1);
        }
        //religionTypeRepository UpdateStudentStandard //UpdateStudenContact
        public async Task<IActionResult> UpdateStudenContact(ContactViewmodel contactViewmodel)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var sid = HttpContext.Session.GetString(SessionStudentId);
            var studentid = sid.Split("_")[1];
            if (contactViewmodel.emegencyId != 0)
            {
                var id = contactViewmodel.emegencyId;
                var contactdetails = await admissionStudentContactRepository.GetByIdAsync(id);
                //contactdetails.AdmissionStudentId = Convert.ToInt64(studentid);
                contactdetails.FullName = contactViewmodel.txtEmrName;
                contactdetails.Mobile = contactViewmodel.txtEmrPhNo;
                contactdetails.ModifiedDate = DateTime.Now;
                contactdetails.ModifiedId = accessId;
                await admissionStudentContactRepository.UpdateAsync(contactdetails);
            }
            else
            {
                AdmissionStudentContact obj = new AdmissionStudentContact();
                obj.AdmissionStudentId = Convert.ToInt64(studentid);
                obj.FullName = contactViewmodel.txtEmrName;
                obj.Mobile = contactViewmodel.txtEmrPhNo;
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = "ACTIVE";
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = accessId;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = accessId;
                obj.ConatctType = "emergency";
                await admissionStudentContactRepository.AddAsync(obj);
            }
            AdmissionStudentContact obj1 = new AdmissionStudentContact();
            if (contactViewmodel.ofcContactId != 0)
            {
                var id = contactViewmodel.ofcContactId;
                var contactdetails1 = await admissionStudentContactRepository.GetByIdAsync(id);
                //contactdetails.AdmissionStudentId = Convert.ToInt64(studentid);
                contactdetails1.FullName = contactViewmodel.txtEmrOfcName;
                contactdetails1.Mobile = contactViewmodel.txtEmrOfcPhNo;
                contactdetails1.EmailId = contactViewmodel.txtEmrOfcEmail;
                contactdetails1.ModifiedDate = DateTime.Now;
                contactdetails1.ModifiedId = accessId;
                await admissionStudentContactRepository.UpdateAsync(contactdetails1);
            }
            else
            {

                obj1.AdmissionStudentId = Convert.ToInt64(studentid);
                obj1.FullName = contactViewmodel.txtEmrOfcName;
                obj1.Mobile = contactViewmodel.txtEmrOfcPhNo;
                obj1.EmailId = contactViewmodel.txtEmrOfcEmail;
                obj1.IsAvailable = true;
                obj1.Active = true;
                obj1.Status = "ACTIVE";
                obj1.InsertedDate = DateTime.Now;
                obj1.InsertedId = accessId;
                obj1.ModifiedDate = DateTime.Now;
                obj1.ModifiedId = accessId;
                obj1.ConatctType = "official";
                await admissionStudentContactRepository.AddAsync(obj1);
            }
            if (obj1.ID > 0)
            {
                return Json(obj1.ID);
            }
            else
            {
                return Json(0);
            }

        }
        public async Task<IActionResult> UpdateStudentReference(AdmissionStudentReference studentReference)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var sid = HttpContext.Session.GetString(SessionStudentId);
            var studentid = sid.Split("_")[1];
            AdmissionStudentReference obj = new AdmissionStudentReference();
            if (studentReference.ID != 0)
            {
                AdmissionStudentReference studentStandardobj = admissionStudentReferenceRepository.GetByIdAsync(studentReference.ID).Result;
                studentStandardobj.FullName = studentReference.FullName;
                studentStandardobj.ProfessionTypeId = studentReference.ProfessionTypeId;
                studentStandardobj.Mobile = studentReference.Mobile;
                studentStandardobj.Address = studentReference.Address;
                studentStandardobj.Pincode = studentReference.Pincode;
                studentStandardobj.CountrytId = studentReference.CountrytId;
                studentStandardobj.ModifiedDate = DateTime.Now;
                studentStandardobj.ModifiedId = accessId;
                await admissionStudentReferenceRepository.UpdateAsync(studentStandardobj);
            }
            else
            {

                obj.FullName = studentReference.FullName;
                obj.ProfessionTypeId = studentReference.ProfessionTypeId;
                obj.Mobile = studentReference.Mobile;
                obj.Address = studentReference.Address;
                obj.Pincode = studentReference.Pincode;
                obj.CountrytId = studentReference.CountrytId;
                obj.ModifiedDate = DateTime.Now;
                obj.AdmissionStudentId = Convert.ToInt64(studentid);
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = "ACTIVE";
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = accessId;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = accessId;
                await admissionStudentReferenceRepository.AddAsync(obj);
            }
            if (obj.ID > 0)
            {
                return Json(obj.ID);
            }
            else
            {
                return Json(0);
            }


        }
        public async Task<IActionResult> UpdateStudentStandard(AdmissionStudentStandard StudentStandardData)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var sid = HttpContext.Session.GetString(SessionStudentId);
            var studentid = sid.Split("_")[1];
            //var sidval = "sid_" + studentid;
            if (StudentStandardData.ID != 0)
            {
                AdmissionStudentStandard studentStandardobj = admissionStudentStandardRepository.GetByIdAsync(StudentStandardData.ID).Result;
                studentStandardobj.WingId = StudentStandardData.WingId;
                studentStandardobj.OrganizationId = StudentStandardData.OrganizationId;
                studentStandardobj.Nucleus = StudentStandardData.Nucleus;
                studentStandardobj.ModifiedDate = DateTime.Now;
                studentStandardobj.ModifiedId = accessId;
                await admissionStudentStandardRepository.UpdateAsync(studentStandardobj);
            }
            else
            {
                //AdmissionStudentAddress studentaddressobj = new AdmissionStudentAddress();
                //studentaddressobj.AddressLine1 = StudentPreAddressData.AddressLine1;
                //studentaddressobj.AddressLine2 = StudentPreAddressData.AddressLine2;
                //studentaddressobj.AddressType = StudentPreAddressData.AddressType;
                //studentaddressobj.CountryId = StudentPreAddressData.CountryId;
                //studentaddressobj.StateId = StudentPreAddressData.StateId;
                //studentaddressobj.CityId = StudentPreAddressData.CityId;
                //studentaddressobj.PostalCode = StudentPreAddressData.PostalCode;
                //studentaddressobj.InsertedDate = DateTime.Now;
                //studentaddressobj.ModifiedDate = DateTime.Now;
                //studentaddressobj.InsertedId = 1;
                //studentaddressobj.ModifiedId = 1;
                //studentaddressobj.IsAvailable = true;
                //studentaddressobj.Status = "ACTIVE";
                //studentaddressobj.Active = true;
                //studentaddressobj.AdmissionStudentId = Convert.ToInt64(studentid);
                //await admissionStudentAddressRepository.AddAsync(studentaddressobj);
            }
            return Json(1);
        }

        public async Task<IActionResult> UploadSaveFatherMotherData(AdmissionStudentParent admissionStudentParent)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var sid = HttpContext.Session.GetString(SessionStudentId);
            var studentid = sid.Split("_")[1];
            AdmissionStudentParent obj = new AdmissionStudentParent();
            if (admissionStudentParent.ID != 0)
            {
                AdmissionStudentParent studentParentobj = admissionStudentParentRepository.GetByIdAsync(admissionStudentParent.ID).Result;
                studentParentobj.FullName = admissionStudentParent.FullName;
                studentParentobj.PrimaryMobile = admissionStudentParent.PrimaryMobile;
                studentParentobj.AlternativeMobile = admissionStudentParent.AlternativeMobile;
                studentParentobj.EmailId = admissionStudentParent.EmailId;
                studentParentobj.ProfessionTypeId = admissionStudentParent.ProfessionTypeId;
                studentParentobj.ParentRelationship = admissionStudentParent.ParentRelationship;
                studentParentobj.Company = admissionStudentParent.Company;
                studentParentobj.ModifiedDate = DateTime.Now;
                studentParentobj.ModifiedId = accessId;
                if (admissionStudentParent.Image != null)
                {
                    studentParentobj.Image = admissionStudentParent.Image;
                }


                await admissionStudentParentRepository.UpdateAsync(studentParentobj);
            }
            else
            {

                obj.FullName = admissionStudentParent.FullName;
                obj.PrimaryMobile = admissionStudentParent.PrimaryMobile;
                obj.AlternativeMobile = admissionStudentParent.AlternativeMobile;
                obj.EmailId = admissionStudentParent.EmailId;
                obj.ProfessionTypeId = admissionStudentParent.ProfessionTypeId;
                obj.ParentRelationship = admissionStudentParent.ParentRelationship;
                obj.Company = admissionStudentParent.Company;
                obj.Image = admissionStudentParent.Image;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.AdmissionStudentId = Convert.ToInt64(studentid);
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = "ACTIVE";
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = accessId;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = accessId;
                await admissionStudentParentRepository.AddAsync(obj);
            }
            if (obj.ID > 0)
            {
                return Json(obj.ID);
            }
            else
            {
                return Json(0);
            }
            //return Json(1);

        }
        #region-----
        public IActionResult DownloadAdmissionFileUploadSample(string id)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();
            var doctype = documentTypeRepository.GetAllDocumentTypes().Where(a => a.Name == "ADMISSION").FirstOrDefault();
            var docsubtype = documentSubTypeRepository.GetAllDocumentSubTypeByDocumentTypeId(doctype.ID).ToList();
            var BoardList = boardRepository.GetAllBoard().ToList();
            var ClassList = StandardRepository.GetAllStandard().ToList();
            var OrganisationList = organizationRepository.GetAllOrganization().Where(a => a.Active == true).ToList();
            var SessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == Convert.ToInt64(id)).ToList();
            var orgAcademics = from a in organizationAcademicRepository.ListAllAsync().
                     Result.Where(a => a.AcademicSessionId == Convert.ToInt64(id) && a.Active == true)
                               join b in OrganisationList on a.OrganizationId equals b.ID
                               select new
                               {
                                   a.ID,
                                   Name = b.Name
                               };
            DataTable docType = new DataTable();
            docType.Columns.Add("DocumentType");
            DataRow drdocType = docType.NewRow();
            drdocType["DocumentType"] = "D_" + doctype.ID + "_" + doctype.Name;
            docType.Rows.Add(drdocType);
            docType.TableName = "DocumentType";

            DataTable docSubType = new DataTable();
            docSubType.Columns.Add("DocumentSubType");
            foreach (var a in docsubtype)
            {
                DataRow dr = docSubType.NewRow();
                dr["DocumentSubType"] = "DS_" + a.ID + "_" + a.Name;
                docSubType.Rows.Add(dr);
            }
            docSubType.TableName = "DocumentSubType";

            System.Data.DataTable blooddt = new System.Data.DataTable();
            blooddt.Columns.Add("SessionName");
            foreach (var a in SessionList)
            {
                DataRow dr = blooddt.NewRow();
                dr["SessionName"] = "S_" + a.ID + "_" + a.DisplayName;
                blooddt.Rows.Add(dr);
            }
            blooddt.TableName = "Session";

            System.Data.DataTable categorydt = new System.Data.DataTable();
            categorydt.Columns.Add("OrganisationName");
            foreach (var a in orgAcademics)
            {
                DataRow dr = categorydt.NewRow();
                dr["OrganisationName"] = "O_" + a.ID + "_" + a.Name;
                categorydt.Rows.Add(dr);
            }
            categorydt.TableName = "Organisation";

            System.Data.DataTable nationalitydt = new System.Data.DataTable();
            nationalitydt.Columns.Add("BoardName");
            foreach (var a in BoardList)
            {
                DataRow dr = nationalitydt.NewRow();
                dr["BoardName"] = "B_" + a.ID + "_" + a.Name;
                nationalitydt.Rows.Add(dr);
            }
            nationalitydt.TableName = "Board";

            System.Data.DataTable religiondt = new System.Data.DataTable();
            religiondt.Columns.Add("ClassName");
            foreach (var a in ClassList)
            {
                DataRow dr = religiondt.NewRow();
                dr["ClassName"] = "C_" + a.ID + "_" + a.Name;
                religiondt.Rows.Add(dr);
            }
            religiondt.TableName = "Class";





            int lastCellNo1 = docType.Rows.Count + 1;
            int lastCellNo2 = docSubType.Rows.Count + 1;
            int lastCellNo3 = blooddt.Rows.Count + 1;
            int lastCellNo4 = categorydt.Rows.Count + 1;
            int lastCellNo5 = nationalitydt.Rows.Count + 1;
            int lastCellNo6 = religiondt.Rows.Count + 1;

            oWB.AddWorksheet(docType);
            oWB.AddWorksheet(docSubType);
            oWB.AddWorksheet(blooddt);
            oWB.AddWorksheet(categorydt);
            oWB.AddWorksheet(nationalitydt);
            oWB.AddWorksheet(religiondt);



            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            var worksheet3 = oWB.Worksheet(3);
            var worksheet4 = oWB.Worksheet(4);
            var worksheet5 = oWB.Worksheet(5);
            var worksheet6 = oWB.Worksheet(6);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("Document Type (Compulsory)");
            validationTable.Columns.Add("Document SubType (Compulsory)");
            validationTable.Columns.Add("Session (Compulsory)");
            validationTable.Columns.Add("Organisation (Compulsory)");
            validationTable.Columns.Add("Board (Compulsory)");
            validationTable.Columns.Add("Class (Compulsory)");
            validationTable.TableName = "DocumentListUploadSample";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(2).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(3).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
            worksheet.Column(4).SetDataValidation().List(worksheet4.Range("A2:A" + lastCellNo4), true);
            worksheet.Column(5).SetDataValidation().List(worksheet5.Range("A2:A" + lastCellNo5), true);
            worksheet.Column(6).SetDataValidation().List(worksheet6.Range("A2:A" + lastCellNo6), true);
            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();
            worksheet4.Hide();
            worksheet5.Hide();
            worksheet6.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"DocumentListUploadSample.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadAdmissionFile(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[6];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            var doctypeId = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            var docsubtypeId = Convert.ToInt64(firstWorksheet.Cells[i, 2].Value.ToString().Split('_')[1]);
                            var sessionId = Convert.ToInt64(firstWorksheet.Cells[i, 3].Value.ToString().Split('_')[1]);
                            long organisationAcaId = Convert.ToInt64(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]);
                            long boardId = Convert.ToInt64(firstWorksheet.Cells[i, 5].Value.ToString().Split('_')[1]);
                            long classid = Convert.ToInt64(firstWorksheet.Cells[i, 6].Value.ToString().Split('_')[1]);

                            var organisationlist = GetSchoolListByAcademicSession(sessionId);
                            var boardstandardlist = GetBoardStandardListByBoardAndStandard(boardId, classid);
                            long brdstandid = boardstandardlist != null ? boardstandardlist.FirstOrDefault().ID : 0;
                            var academicStandar = (from a in await academicStandardRepository.ListAllAsync() where a.Status == "ACTIVE" && a.BoardStandardId == brdstandid && a.OrganizationAcademicId == organisationAcaId select a).FirstOrDefault();
                            long academicstandardid = academicStandar != null ? academicStandar.ID : 0;


                            if (brdstandid != 0 && organisationAcaId != 0)
                            {
                                var academicStd = (from b in await academicStandardRepository.ListAllAsync()
                                                   where b.OrganizationAcademicId == organisationAcaId
&& b.Active == true && b.BoardStandardId == brdstandid
                                                   select b).FirstOrDefault();
                                var academicStandardId = academicStd != null ? academicStd.ID : 0;
                                if (academicStandardId != 0)
                                {
                                    var documentPresent = (from c in await admissionDocumentRepository.ListAllAsyncIncludeAll() where c.DocumentTypeId == doctypeId && c.DocumentSubTypeId == docsubtypeId && c.AcademicStandardId == academicStandardId && c.AdmissionSessionId == sessionId && c.Status == "ACTIVE" select c).ToList().Count;
                                    if (documentPresent == 0)
                                    {
                                        AdmissionDocument obj = new AdmissionDocument();
                                        obj.DocumentTypeId = doctypeId;
                                        obj.DocumentSubTypeId = docsubtypeId;
                                        obj.AdmissionSessionId = sessionId;
                                        obj.AcademicStandardId = academicStandardId;
                                        obj.IsAvailable = true;
                                        obj.Status = "ACTIVE";
                                        obj.Active = true;
                                        obj.InsertedDate = DateTime.Now;
                                        obj.InsertedId = userId;
                                        obj.ModifiedDate = DateTime.Now;
                                        obj.ModifiedId = userId;
                                        await admissionDocumentRepository.AddAsync(obj);
                                    }

                                }
                            }

                        }
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(FileInformation)).WithSuccess("File Information", "Excel uploaded successfully"); ;
        }
        public List<Organization> GetSchoolListByAcademicSession(long AcademicSessionId)
        {

            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                    Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.Active == true).Select(a => a.OrganizationId).ToList();

            var orgs = organizationRepository.GetAllOrganization().Where(a => orgAcademics.Contains(a.ID));


            return orgs.ToList();
        }
        public List<CommonMasterModel> GetBoardStandardListByBoardAndStandard(long boardId, long classid)
        {
            var boardstandard = boardStandardRepository.ListAllAsync().Result.Where(a => a.BoardId == boardId && a.StandardId == classid && a.Active == true).ToList();
            var standard = StandardRepository.GetAllStandard().ToList();
            var res = (from a in boardstandard
                       join c in standard on a.StandardId equals c.ID
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = c.Name
                       }).ToList();
            if (res != null && res.Count() > 0)
            {
                return res;
            }
            else
            {
                return null;
            }


        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        #endregion
        #region  Data Sync with Student Dashboard
        public async Task<IActionResult> DataSyncwithstudentdashboard(string id, string studentcode, string rollno)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "Sync", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var aid = Convert.ToInt64(id);
            var sid = HttpContext.Session.GetString(SessionStudentId);
            var studentid = sid.Split("_")[1];
            //var sidval = "sid_" + studentid;
            var studentId = Convert.ToInt64(studentid);



            var admissionSingle = admissionRepository.GetAdmissionById(aid);
            var obstudent = await admissionStudentRepository.GetByIdAsyncIncludeAll(admissionSingle.ID);
            var admissionStudentContactList = (from a in await admissionStudentContactRepository.ListAllAsyncIncludeActiveNoTrack()
                                               where a.AdmissionStudentId == studentId
                                               select a).ToList();
            #region Student Personal Details
            OdmErp.ApplicationCore.Entities.Student student = new OdmErp.ApplicationCore.Entities.Student();
            student.ModifiedId = userId;
            student.Active = true;
            student.AadharNO = obstudent.AadharNO;
            student.BloodGroupID = obstudent.BloodGroupID;
            student.CategoryID = obstudent.CategoryID;
            student.DateOfBirth = obstudent.DateOfBirth.Value;
            student.EmailID = admissionStudentContactList.Where(a => a.ConatctType == "official").FirstOrDefault().EmailId;
            student.FirstName = obstudent.FirstName;
            student.Gender = obstudent.Gender;
            student.InsertedDate = DateTime.Now;
            student.IsAvailable = true;
            student.LastName = obstudent.LastName;
            student.MiddleName = obstudent.MiddleName;
            student.MotherTongueID = obstudent.MotherTongueID;
            student.NatiobalityID = obstudent.NatiobalityID;
            student.PassportNO = obstudent.PassportNO;
            student.PrimaryMobile = admissionSingle.MobileNumber;
            student.ReligionID = obstudent.ReligionID;
            student.StudentCode = studentcode;
            student.WhatsappMobile = "";
            student.ZoomMailID = "";
            var webPath = hostingEnvironment.WebRootPath;
            string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + admissionSingle.ID + @"\" + obstudent.Image);
            string path1 = Path.Combine("", webPath + @"\ODMImages\StudentProfile\" + obstudent.Image);
            System.IO.File.Copy(path, path1, true);
            student.Image = obstudent.Image;

            student.InsertedId = userId;
            student.InsertedDate = DateTime.Now;
            student.ModifiedDate = DateTime.Now;

            student.Status = "CREATED";
            long stid = studentAggregateRepository.CreateStudent(student);

            var languages = admissionStudentLanguageRepository.ListAllAsync().Result.Where(a => a.AdmissionStudentId == obstudent.ID && a.Active == true).ToList();


            if (languages.Count > 0)
            {
                foreach (var s in languages)
                {
                    StudentLanguagesKnown studentLanguages = new StudentLanguagesKnown();
                    studentLanguages.Active = true;
                    studentLanguages.InsertedId = userId;
                    studentLanguages.InsertedDate = DateTime.Now;
                    studentLanguages.ModifiedDate = DateTime.Now;
                    studentLanguages.IsAvailable = true;
                    studentLanguages.LanguageID = s.LanguageId;
                    studentLanguages.ModifiedId = userId;
                    studentLanguages.Status = "CREATED";
                    studentLanguages.StudentID = stid;
                    await studentLanguagesKnownRepository.AddAsync(studentLanguages);
                }

            }
            Access access = new Access();
            access.EmployeeID = stid;
            access.Active = true;
            access.InsertedId = userId;
            access.Password = "";
            access.Username = student.StudentCode;
            access.RoleID = 5;
            access.ModifiedId = userId;
            accessRepository.CreateAccess(access);
            #endregion
            #region Student Address
            var admissionstudentaddressList = (from b in await admissionStudentAddressRepository.ListAllAsyncIncludeActiveNoTrack()
                                               where b.AdmissionStudentId == studentId
                                               select b).ToList();
            foreach (var ad in admissionstudentaddressList)
            {
                Address address = new Address();
                address.AddressLine1 = ad.AddressLine1;
                address.AddressLine2 = ad.AddressLine2;
                address.AddressTypeID = addressTypeRepository.GetAllAddressType().Where(a => a.Name.ToLower() == ad.AddressType.ToLower()).FirstOrDefault().ID;
                //address.CityID = ad.CityId;
                //address.CountryID = ad.CountryId;
                //address.StateID = ad.StateId;
                address.PostalCode = ad.PostalCode;
                address.ModifiedId = userId;
                address.Active = true;
                address.InsertedId = userId;
                address.Status = "CREATED";
                long adid = addressRepository.CreateAddress(address);
                StudentAddress studentAddress = new StudentAddress();
                studentAddress.AddressID = adid;
                studentAddress.StudentID = stid;
                studentAddress.Active = true;
                studentAddress.InsertedId = userId;
                studentAddress.ModifiedId = userId;
                studentAddress.Status = "CREATED";
                studentAggregateRepository.CreateStudentAddress(studentAddress);
            }

            #endregion
            #region Student Academic Standard
            var admissionstudentStandardSingle = (from c in await admissionStudentStandardRepository.ListAllAsyncIncludeAll()
                                                  where c.AdmissionStudentId == studentId
                                                  join d in await academicStandardWingRepository.ListAllAsyncIncludeAll() on c.AcademicStandardWindId equals d.ID
                                                  join e in await boardStandardWingRepository.ListAllAsyncIncludeAll() on d.BoardStandardWingID equals e.ID
                                                  join f in await boardStandardRepository.ListAllAsyncIncludeAll() on e.BoardStandardID equals f.ID
                                                  join g in StandardRepository.GetAllStandard() on f.StandardId equals g.ID
                                                  select new
                                                  {
                                                      c.ID,
                                                      c.AcademicStandardWindId,
                                                      c.AdmissionStudentId,
                                                      c.StandardId,
                                                      c.Nucleus,
                                                      c.OrganizationId,
                                                      c.WingId,
                                                      e.BoardStandardID
                                                  }).FirstOrDefault();
            var academicswing = academicStandardWingRepository.ListAllAsync().Result.Where(a => a.ID == admissionstudentStandardSingle.AcademicStandardWindId).FirstOrDefault();
            AcademicStudent academicStudent = new AcademicStudent();
            academicStudent.AcademicStandardId = academicswing.AcademicStandardId;
            academicStudent.AcademicStandardWingId = academicswing.ID;
            academicStudent.AcademicSubjectCombinationId = 0;
            academicStudent.Active = true;
            academicStudent.Nucleus = admissionstudentStandardSingle.Nucleus;
            academicStudent.InsertedDate = DateTime.Now;
            academicStudent.InsertedId = userId;
            academicStudent.ModifiedDate = DateTime.Now;
            academicStudent.ModifiedId = userId;
            academicStudent.Status = "ACTIVE";
            academicStudent.StudentId = stid;
            academicStudent.IsAvailable = true;
            long academicstudentid = academicStudentRepository.AddAsync(academicStudent).Result.ID;


            #endregion
            #region previous academic
            var admissionStudentAcademicSingle = (from a in admissionStudentAcademicRepository.AllAdmissionStudentAcademic()
                                                  where a.AdmissionStudentID == studentId
                                                  select a).FirstOrDefault();


            StudentAcademic acd = new StudentAcademic();
            acd.Active = true;
            acd.IsAvailable = true;
            acd.OrganizationName = admissionStudentAcademicSingle.OrganizationName;
            acd.ReasonForChange = admissionStudentAcademicSingle.ReasonForChange;
            acd.GradeOrPercentage = admissionStudentAcademicSingle.AggregatePercentageOrGrade;
            acd.StateId = Convert.ToInt32(admissionStudentAcademicSingle.StateId);
            acd.CityId = Convert.ToInt32(admissionStudentAcademicSingle.CityId);
            acd.BoardID = 0;
            acd.ClassID = 0;
            acd.FromDate = DateTime.Now;
            acd.ToDate = DateTime.Now;
            acd.InsertedDate = DateTime.Now;
            acd.InsertedId = userId;
            acd.ModifiedDate = DateTime.Now;
            acd.ModifiedId = userId;
            studentAggregateRepository.CreateStudentAcademic(acd);

            #endregion

            #region Parent List
            var admissionStudentParentList = (from a in await admissionStudentParentRepository.ListAllAsyncIncludeAll()
                                              where a.AdmissionStudentId == studentId
                                              select a).ToList();
            foreach (var par in admissionStudentParentList)
            {
                Parent parent = new Parent();
                parent.ModifiedId = userId;
                parent.FirstName = par.FullName.Split(' ').Length > 0 ? par.FullName.Split(' ')[0] : par.FullName;

                parent.MiddleName = par.FullName.Split(' ').Length > 2 ? par.FullName.Split(' ')[1] : par.FullName;

                parent.LastName = par.FullName.Split(' ').Length > 1 ? par.FullName.Split(' ')[1] : (par.FullName.Split(' ').Length > 2 ? par.FullName.Split(' ')[2] : "");
                parent.PrimaryMobile = par.PrimaryMobile;

                parent.AlternativeMobile = par.AlternativeMobile;





                parent.EmailId = par.EmailId;
                // parent.ProfessionTypeID = par.ProfessionTypeId;
                parent.StudentID = stid;
                parent.ParentRelationshipID = studentAggregateRepository.GetAllParentRelationshipType().Where(a => a.Name.ToLower() == par.ParentRelationship.ToLower()).Select(a => a.ID).FirstOrDefault();

                parent.ParentOrganization = par.Company;

                parent.Active = true;
                parent.InsertedId = userId;
                parent.IsAvailable = true;
                parent.ModifiedId = userId;
                parent.Status = "ACTIVE";
                long pid = studentAggregateRepository.CreateParent(parent);
                if (pid > 0)
                {
                    Access accesss = new Access();
                    accesss.RoleID = 6;
                    accesss.Username = parent.PrimaryMobile;
                    accesss.EmployeeID = pid;
                    accesss.Active = true;
                    accesss.InsertedDate = DateTime.Now;
                    accesss.InsertedId = userId;
                    accesss.ModifiedDate = DateTime.Now;
                    accesss.ModifiedId = userId;
                    accessRepository.CreateAccess(accesss);
                }
            }
            #endregion
            #region Emergency Details
            var emerg = admissionStudentContactList.Where(a => a.ConatctType == "emergency").FirstOrDefault();
            StudentEmergencyContact emergency = new StudentEmergencyContact();
            emergency.Active = true;
            emergency.ContactPersonName = emerg.FullName;
            emergency.EmailId = emerg.EmailId;
            emergency.IsAvailable = true;
            emergency.Mobile = emerg.Mobile;
            emergency.ModifiedDate = DateTime.Now;
            emergency.ModifiedId = userId;
            emergency.InsertedDate = DateTime.Now;
            emergency.InsertedId = userId;
            emergency.Status = "ACTIVE";
            emergency.StudentID = stid;
            await studentEmergencyContactRepository.AddAsync(emergency);

            #endregion
            #region Transport
            var admissionStudentTransportSingle = (from a in await admissionStudentTransportRepository.ListAllAsyncIncludeAll()
                                                   where a.AdmissionStudentId == studentId
                                                   select a).FirstOrDefault();

            StudentTransportation transportation = new StudentTransportation();
            transportation.Active = true;
            transportation.DepartureTimeID = 0;
            transportation.Distance = admissionStudentTransportSingle.Distance;
            transportation.IsAvailable = true;
            transportation.IsTransport = admissionStudentTransportSingle.IsTransport;
            //transportation.LocationID = admissionStudentTransportSingle.LocationId;
            transportation.ModifiedDate = DateTime.Now;
            transportation.ModifiedId = userId;
            transportation.InsertedDate = DateTime.Now;
            transportation.InsertedId = userId;
            transportation.Status = "ACTIVE";
            transportation.StudentID = stid;
            await studentTransportationRepository.AddAsync(transportation);
            #endregion


            return Json(1);


        }

        #endregion



        #region-----------------student detail Update---------------------

        #region-----------------student personal---------------

        [HttpPost]
        public async Task<IActionResult> UpdateStudentDetailsByAdmissionId(
            PersonalDetails StudentData)
        {

            if (StudentData == null || StudentData.AdmissionStudentId == 0)
                return RedirectToAction("Error", "Student");
            AdmissionStudent studentobj = admissionStudentRepository.
                GetByIdAsync(StudentData.AdmissionStudentId).Result;


            if (studentobj != null)
            {
                studentobj.FirstName = StudentData.firstName;
                studentobj.MiddleName = StudentData.middleName;
                studentobj.LastName = StudentData.lastName;
                studentobj.Gender = StudentData.gender;
                studentobj.BloodGroupID = StudentData.BloodGroup;
                studentobj.NatiobalityID = StudentData.Nationality;
                studentobj.ReligionID = StudentData.Religion;
                studentobj.CategoryID = StudentData.Category;
                studentobj.MotherTongueID = StudentData.Language;
                studentobj.PassportNO = StudentData.Passport;
                studentobj.AadharNO = StudentData.Aadhaar;
                studentobj.DateOfBirth = StudentData.dob;
                studentobj.IsAvailable = true;
                studentobj.ModifiedId = accessId;
                studentobj.ModifiedDate = DateTime.Now;
                if (StudentData.image != null)
                {
                    FileInfo fi = new FileInfo(StudentData.image.FileName);
                    var newFilename = "Student" + "_" + StudentData.AdmissionStudentId + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await StudentData.image.CopyToAsync(stream);
                    }

                    studentobj.Image = "/ODMImages/Admission/" + pathToSave;
                }

                await admissionStudentRepository.UpdateAsync(studentobj);
            }
            if (StudentData.lKnown != null)
            {
                foreach (long language in StudentData.lKnown)
                {
                    AdmissionStudentLanguage languageedit = admissionStudentLanguageRepository.ListAllAsyncIncludeAll().
                        Result.Where(a => a.AdmissionStudentId == StudentData.AdmissionStudentId && a.LanguageId == language).FirstOrDefault();
                    if (languageedit == null)
                    {
                        AdmissionStudentLanguage studentlanguage = new AdmissionStudentLanguage();
                        studentlanguage.LanguageId = language;
                        studentlanguage.AdmissionStudentId = StudentData.AdmissionStudentId;
                        studentlanguage.ModifiedDate = DateTime.Now;
                        studentlanguage.ModifiedId = accessId;
                        studentlanguage.InsertedDate = DateTime.Now;
                        studentlanguage.InsertedId = accessId;
                        studentlanguage.Active = true;
                        studentlanguage.IsAvailable = true;
                        studentlanguage.Status = EntityStatus.ACTIVE;
                        await admissionStudentLanguageRepository.AddAsync(studentlanguage);
                    }
                }
                var languageeditlist = admissionStudentLanguageRepository.ListAllAsyncIncludeAll().
                      Result.Where(a => a.AdmissionStudentId == StudentData.AdmissionStudentId && !StudentData.lKnown.Contains(a.LanguageId)).ToList();
                foreach (var languages in languageeditlist)
                {
                    AdmissionStudentLanguage studentlanguage = languages;
                    studentlanguage.ModifiedDate = DateTime.Now;
                    studentlanguage.ModifiedId = accessId;
                    studentlanguage.Active = false;
                    studentlanguage.IsAvailable = true;
                    studentlanguage.Status = EntityStatus.INACTIVE;
                    await admissionStudentLanguageRepository.UpdateAsync(studentlanguage);
                }
            }
            OdmErp.ApplicationCore.Entities.Admission adm =
                admissionRepository.GetAdmissionById(
                    studentobj.AdmissionId);
            if (adm == null)
            {
                return Json(0);
            }

            adm.FormNumber = StudentData.formNumber;
            admissionRepository.UpdateAdmission(adm);



            // return  RedirectToAction("Profile",new { id= adm.LeadReferenceId.ToString()});
            return Json(1);
        }
        #endregion------------------------------------------------------------

        #region-----------------Student Address-------------------------
        [HttpPost]
        public async Task<IActionResult> UpdateStudentAddress(AddressDetails StudentPreAddressData)
        {

            if (StudentPreAddressData == null || StudentPreAddressData.AdmissionStudentId == 0)
                return Json(0);
            // return RedirectToAction("Error", "Student");


            AdmissionStudentAddress studentaddressobj1 = admissionStudentAddressRepository.
                ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == StudentPreAddressData.AdmissionStudentId && a.AddressType == "present").FirstOrDefault();

            if (studentaddressobj1 != null)
            {
                if (StudentPreAddressData.presentAddressType == "present")
                {
                    studentaddressobj1.AddressLine1 = StudentPreAddressData.presentAddress1;
                    studentaddressobj1.AddressLine2 = StudentPreAddressData.prestntAddress2;
                    studentaddressobj1.AddressType = StudentPreAddressData.presentAddressType;
                    studentaddressobj1.CountryId = StudentPreAddressData.presentcountry;
                    studentaddressobj1.StateId = StudentPreAddressData.presentstate;
                    studentaddressobj1.CityId = StudentPreAddressData.presentcity;
                    studentaddressobj1.PostalCode = StudentPreAddressData.presentpin;
                    studentaddressobj1.ModifiedDate = DateTime.Now;
                    studentaddressobj1.ModifiedId = accessId;
                    await admissionStudentAddressRepository.UpdateAsync(studentaddressobj1);
                }
            }
            else
            {
                AdmissionStudentAddress studentaddressobj2 = new AdmissionStudentAddress();
                if (StudentPreAddressData.presentAddressType == "present")
                {
                    studentaddressobj2.AddressLine1 = StudentPreAddressData.presentAddress1;
                    studentaddressobj2.AddressLine2 = StudentPreAddressData.prestntAddress2;
                    studentaddressobj2.AddressType = StudentPreAddressData.presentAddressType;
                    studentaddressobj2.CountryId = StudentPreAddressData.presentcountry;
                    studentaddressobj2.StateId = StudentPreAddressData.presentstate;
                    studentaddressobj2.CityId = StudentPreAddressData.presentcity;
                    studentaddressobj2.PostalCode = StudentPreAddressData.presentpin;
                    studentaddressobj2.ModifiedDate = DateTime.Now;
                    studentaddressobj2.AdmissionStudentId = StudentPreAddressData.AdmissionStudentId;
                    studentaddressobj2.ModifiedId = accessId;
                    studentaddressobj2.InsertedDate = DateTime.Now;
                    studentaddressobj2.InsertedId = accessId;
                    studentaddressobj2.Active = true;
                    studentaddressobj2.IsAvailable = true;
                    studentaddressobj2.Status = EntityStatus.ACTIVE;
                    await admissionStudentAddressRepository.AddAsync(studentaddressobj2);
                }
            }
            AdmissionStudentAddress studentaddressobj3 = admissionStudentAddressRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == StudentPreAddressData.AdmissionStudentId && a.AddressType == "permanent").FirstOrDefault();

            if (studentaddressobj3 != null)
            {

                if (StudentPreAddressData.permanentAddressType == "permanent")
                {
                    studentaddressobj3.AddressLine1 = StudentPreAddressData.permanentAddress1;
                    studentaddressobj3.AddressLine2 = StudentPreAddressData.permanentAddress2;
                    studentaddressobj3.AddressType = StudentPreAddressData.permanentAddressType;
                    studentaddressobj3.CountryId = StudentPreAddressData.permanentcountry;
                    studentaddressobj3.StateId = StudentPreAddressData.permanentstate;
                    studentaddressobj3.CityId = StudentPreAddressData.permanentcity;
                    studentaddressobj3.PostalCode = StudentPreAddressData.permanentpin;
                    studentaddressobj3.ModifiedDate = DateTime.Now;
                    studentaddressobj3.ModifiedId = accessId;
                    await admissionStudentAddressRepository.UpdateAsync(studentaddressobj3);
                }

            }
            else
            {
                AdmissionStudentAddress studentaddressobj4 = new AdmissionStudentAddress();
                studentaddressobj4.AddressLine1 = StudentPreAddressData.permanentAddress1;
                studentaddressobj4.AdmissionStudentId = StudentPreAddressData.AdmissionStudentId;
                studentaddressobj4.AddressLine2 = StudentPreAddressData.permanentAddress2;
                studentaddressobj4.AddressType = StudentPreAddressData.permanentAddressType;
                studentaddressobj4.CountryId = StudentPreAddressData.permanentcountry;
                studentaddressobj4.StateId = StudentPreAddressData.permanentstate;
                studentaddressobj4.CityId = StudentPreAddressData.permanentcity;
                studentaddressobj4.PostalCode = StudentPreAddressData.permanentpin;
                studentaddressobj4.ModifiedDate = DateTime.Now;
                studentaddressobj4.ModifiedId = accessId;
                studentaddressobj4.InsertedDate = DateTime.Now;
                studentaddressobj4.InsertedId = accessId;
                studentaddressobj4.Active = true;
                studentaddressobj4.IsAvailable = true;
                studentaddressobj4.Status = EntityStatus.ACTIVE;
                await admissionStudentAddressRepository.AddAsync(studentaddressobj4);
            }
            //  AdmissionStudent admstudent =
            //admissionStudentRepository.GetByIdAsync(
            //    StudentPreAddressData.AdmissionStudentId).Result;

            //  OdmErp.ApplicationCore.Entities.Admission adm =
            //    admissionRepository.GetAdmissionById(
            //        admstudent.AdmissionId);
            //  return RedirectToAction("Profile", new { id = adm.LeadReferenceId});
            return Json(1);
        }


        #endregion---------------------------------------------

        #region-----------------Student Class/Standard-------------------------

        [HttpPost]
        public async Task<IActionResult> UpdateStudentClass(StudentClassModel StandardData)
        {
            //  CheckLoginStatus();
            if (StandardData == null || StandardData.AdmissionStudentId == 0)
                return Json(0);
            // return RedirectToAction("Error", "Student");
            if (StandardData.AdmissionStudentId != 0)
            {
                AdmissionStudentStandard studentstandard = admissionStudentStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == StandardData.AdmissionStudentId).FirstOrDefault();
                var res = academicStandardWingRepository.
               GetByIdAsync(Convert.ToInt64(StandardData.AcademicStandardWindId))
               .Result;

                studentstandard.AcademicStandardWindId = Convert.ToInt64(StandardData.AcademicStandardWindId);

                if (res != null)
                {
                    var data = boardStandardWingRepository.GetByIdAsync(res.BoardStandardWingID).Result;
                    var data1 = boardStandardRepository.GetByIdAsync(data.BoardStandardID).Result;
                    var data2 = academicStandardRepository.GetByIdAsync(res.AcademicStandardId).Result;
                    var data3 = organizationAcademicRepository.GetByIdAsync(data2.OrganizationAcademicId).Result;

                    studentstandard.BoardId = data1.BoardId;
                    studentstandard.StandardId = data1.StandardId;
                    studentstandard.WingId = data.WingID;
                    studentstandard.OrganizationId = data3.OrganizationId;
                }
                studentstandard.Nucleus = StandardData.necleusAvail;
                studentstandard.ModifiedDate = DateTime.Now;
                studentstandard.ModifiedId = accessId;
                await admissionStudentStandardRepository.UpdateAsync(studentstandard);
            }

            // AdmissionStudent admstudent =
            //admissionStudentRepository.GetByAdmissionId(
            //    StandardData.AdmissionStudentId).Result;

            // OdmErp.ApplicationCore.Entities.Admission adm =
            //   admissionRepository.GetAdmissionById(
            //       admstudent.AdmissionId);
            return Json(1);
            //  return RedirectToAction("Profile", new { id = adm.LeadReferenceId, name = "personal" });
        }

        #endregion-------------------------------------------------------------

        #region-----------------Last Student   Academic-------------------------
        [HttpPost]
        public async Task<ActionResult> SaveOrUpdateStudentAcademic(StudentAcademicClassModel StudentAcademicData)
        {
            CheckLoginStatus();
            if (StudentAcademicData == null || StudentAcademicData.AdmissionStudentId == 0)
                return Json(0);
            // return RedirectToAction("Error", "Student");
            AdmissionStudentAcademic academicSingle = admissionStudentAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentID == StudentAcademicData.AdmissionStudentId).FirstOrDefault();

            if (academicSingle != null)
            {
                academicSingle.AggregatePercentageOrGrade = StudentAcademicData.AggregatePercentageOrGrade;
                academicSingle.CountryId = StudentAcademicData.LastCountryId;
                academicSingle.StateId = StudentAcademicData.LastStateId;
                academicSingle.CityId = StudentAcademicData.LastCityId;
                academicSingle.ReasonForChange = StudentAcademicData.ReasonForChange;
                academicSingle.OrganizationName = StudentAcademicData.OrganizationName;
                academicSingle.ModifiedId = accessId;
                academicSingle.ModifiedDate = DateTime.Now;
                await admissionStudentAcademicRepository.UpdateAsync(academicSingle);
            }
            else
            {
                AdmissionStudentAcademic newAdmissionStudentAcademicObj = new AdmissionStudentAcademic();
                newAdmissionStudentAcademicObj.AdmissionStudentID = StudentAcademicData.AdmissionStudentId;
                newAdmissionStudentAcademicObj.AggregatePercentageOrGrade = StudentAcademicData.AggregatePercentageOrGrade;
                newAdmissionStudentAcademicObj.CountryId = StudentAcademicData.LastCountryId;
                newAdmissionStudentAcademicObj.StateId = StudentAcademicData.LastStateId;
                newAdmissionStudentAcademicObj.CityId = StudentAcademicData.LastCityId;
                newAdmissionStudentAcademicObj.OrganizationName = StudentAcademicData.OrganizationName;
                newAdmissionStudentAcademicObj.ReasonForChange = StudentAcademicData.ReasonForChange;
                newAdmissionStudentAcademicObj.IsAvailable = true;
                newAdmissionStudentAcademicObj.Active = true;
                newAdmissionStudentAcademicObj.Status = "ACTIVE";
                newAdmissionStudentAcademicObj.InsertedDate = DateTime.Now;
                newAdmissionStudentAcademicObj.InsertedId = accessId;
                newAdmissionStudentAcademicObj.ModifiedDate = DateTime.Now;
                newAdmissionStudentAcademicObj.ModifiedId = accessId;
                await admissionStudentAcademicRepository.AddAsync(newAdmissionStudentAcademicObj);
            }
            //OdmErp.ApplicationCore.Entities.Admission adm =
            //admissionRepository.GetAdmissionById(
            //    StudentAcademicData.AdmissionId);
            //   return RedirectToAction("Profile", new { id = adm.LeadReferenceId });
            return Json(1);
        }
        #endregion--------------------------------------------------------

        #region-----------------StudentProficiency-------------------------
        [HttpPost]
        public async Task<ActionResult> SaveOrUpdateStudentProficiency(StudentProficiencyModel ProficiencyData)
        {
            if (ProficiencyData == null || ProficiencyData.AdmissionStudentId == 0)
                return Json(0);
            // return RedirectToAction("Error", "Student");

            AdmissionStudentProficiency academicProficiency = admissionStudentProficiencyRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentID == ProficiencyData.AdmissionStudentId).FirstOrDefault();
            if (academicProficiency != null)
            {
                academicProficiency.PerformingArts = ProficiencyData.PerformingArts;
                academicProficiency.Sports = ProficiencyData.Sports;
                academicProficiency.PositionHeld = ProficiencyData.PositionHeld;
                academicProficiency.ModifiedDate = DateTime.Now;
                await admissionStudentProficiencyRepository.UpdateAsync(academicProficiency);
            }
            else
            {
                AdmissionStudentProficiency obj = new AdmissionStudentProficiency();
                obj.Sports = ProficiencyData.Sports;
                obj.PerformingArts = ProficiencyData.PerformingArts;
                obj.PositionHeld = ProficiencyData.PositionHeld;
                obj.AdmissionStudentID = ProficiencyData.AdmissionStudentId;
                obj.InsertedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.InsertedId = 0;
                obj.ModifiedId = 0;
                obj.IsAvailable = true;
                obj.Status = EntityStatus.ACTIVE;
                obj.Active = true;
                await admissionStudentProficiencyRepository.AddAsync(obj);
            }
            //OdmErp.ApplicationCore.Entities.Admission adm =
            //admissionRepository.GetAdmissionById(
            //    ProficiencyData.AdmissionId);
            // return RedirectToAction("Profile", new { id = adm.LeadReferenceId });
            return Json(1);

        }
        #endregion--------------------------------------------------------

        #region-----------------Student Transfort-------------------------
        [HttpPost]
        public async Task<ActionResult> SaveOrUpdateStudentTransfort(StudentTransportModel TransportData)
        {
            if (TransportData == null || TransportData.AdmissionStudentId == 0)
                return Json(0);
            //  return RedirectToAction("Error", "Student");
            AdmissionStudentTransport obj = admissionStudentTransportRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == TransportData.AdmissionStudentId).FirstOrDefault();
            if (obj != null)
            {
                if (TransportData.busAvail == true)
                {
                    obj.IsTransport = true;
                    obj.LocationId = TransportData.LocationId;
                    obj.Distance = TransportData.Distance;
                }
                else
                {
                    obj.IsTransport = false;
                    obj.LocationId = "";
                    obj.Distance = 0;
                }
                obj.AdmissionStudentId = TransportData.AdmissionStudentId;
                await admissionStudentTransportRepository.UpdateAsync(obj);
            }
            else
            {
                AdmissionStudentTransport obj1 = new AdmissionStudentTransport();
                if (TransportData.busAvail == true)
                {
                    obj1.IsTransport = true;
                    obj1.LocationId = TransportData.LocationId;
                    obj1.Distance = TransportData.Distance;
                }
                else
                {
                    obj1.IsTransport = false;
                    obj1.LocationId = "";
                    obj1.Distance = 0;
                }
                obj1.AdmissionStudentId = TransportData.AdmissionStudentId;
                obj1.InsertedDate = DateTime.Now;
                obj1.ModifiedDate = DateTime.Now;
                obj1.InsertedId = 0;
                obj1.ModifiedId = 0;
                obj1.IsAvailable = true;
                obj1.Status = EntityStatus.ACTIVE;
                obj1.Active = true;
                await admissionStudentTransportRepository.AddAsync(obj1);
            }

            // OdmErp.ApplicationCore.Entities.Admission adm =
            //admissionRepository.GetAdmissionById(
            //    TransportData.AdmissionId);
            // return RedirectToAction("Profile", new { id = adm.LeadReferenceId });
            return Json(1);
        }

        #endregion--------------------------------------------------------

        #region-----------------Student Contact-------------------------
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudenContact(StudentContactViewmodel contact)
        {
            if (contact == null || contact.AdmissionStudentId == 0)
                return Json(0);
            // return RedirectToAction("Error", "Student");

            AdmissionStudentContact officialcontact = admissionStudentContactRepository.ListAllAsyncIncludeAll().Result.
                Where(a => a.AdmissionStudentId == contact.AdmissionStudentId && a.ConatctType == "official").FirstOrDefault();


            if (officialcontact != null)
            {
                officialcontact.FullName = contact.EmrOfcName;
                officialcontact.Mobile = contact.EmrOfcPhNo;
                officialcontact.EmailId = contact.EmrOfcEmail;
                officialcontact.ModifiedDate = DateTime.Now;
                officialcontact.ModifiedId = 0;
                await admissionStudentContactRepository.UpdateAsync(officialcontact);
            }
            else
            {
                AdmissionStudentContact obj1 = new AdmissionStudentContact();
                obj1.AdmissionStudentId = contact.AdmissionStudentId;
                obj1.FullName = contact.EmrOfcName;
                obj1.Mobile = contact.EmrOfcPhNo;
                obj1.EmailId = contact.EmrOfcEmail;
                obj1.IsAvailable = true;
                obj1.Active = true;
                obj1.Status = "ACTIVE";
                obj1.InsertedDate = DateTime.Now;
                obj1.InsertedId = 0;
                obj1.ModifiedDate = DateTime.Now;
                obj1.ModifiedId = 0;
                obj1.ConatctType = "official";
                await admissionStudentContactRepository.AddAsync(obj1);
            }

            AdmissionStudentContact emergencycontact = admissionStudentContactRepository.ListAllAsyncIncludeAll().Result.
               Where(a => a.AdmissionStudentId == contact.AdmissionStudentId && a.ConatctType == "emergency").FirstOrDefault();

            if (emergencycontact != null)
            {
                emergencycontact.FullName = contact.EmrName;
                emergencycontact.Mobile = contact.EmrPhNo;
                //emergencycontact.EmailId = contact.;
                emergencycontact.ModifiedDate = DateTime.Now;
                emergencycontact.ModifiedId = 0;
                await admissionStudentContactRepository.UpdateAsync(emergencycontact);
            }
            else
            {
                AdmissionStudentContact obj1 = new AdmissionStudentContact();
                obj1.AdmissionStudentId = contact.AdmissionStudentId;
                obj1.FullName = contact.EmrName;
                obj1.Mobile = contact.EmrPhNo;
                obj1.IsAvailable = true;
                obj1.Active = true;
                obj1.Status = "ACTIVE";
                obj1.InsertedDate = DateTime.Now;
                obj1.ModifiedDate = DateTime.Now;
                obj1.InsertedId = 0;
                obj1.ModifiedId = 0;
                obj1.ConatctType = "emergency";
                await admissionStudentContactRepository.AddAsync(obj1);
            }
            //  OdmErp.ApplicationCore.Entities.Admission adm =
            //admissionRepository.GetAdmissionById(
            //    contact.AdmissionId);
            //  return RedirectToAction("Profile", new { id = adm.LeadReferenceId });
            return Json(1);
        }


        #endregion--------------------------------------------------------

        #region-----------------Student Declaration-------------------------
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateDeclaration(StudentDeclarationClassModel objAdDeclaration)
        {
            if (objAdDeclaration == null || objAdDeclaration.AdmissionStudentId == 0)
                return Json(0);
            //return RedirectToAction("Error", "Student");


            long AcademicStandardWindId = admissionStudentStandardRepository.ListAllAsyncIncludeAll().Result.
                  Where(a => a.AdmissionStudentId == objAdDeclaration.AdmissionStudentId).FirstOrDefault().AcademicStandardWindId;

            var res = academicStandardWingRepository.GetByIdAsync(Convert.ToInt64(AcademicStandardWindId)).Result;



            AdmissionStudentDeclaration studentDeclarobj1 = admissionRepository.GetAllAdmissionStudentDeclaration().Where(a => a.AdmissionId == objAdDeclaration.AdmissionId).FirstOrDefault();
            if (studentDeclarobj1 != null)
            {
                studentDeclarobj1.IsSon = objAdDeclaration.IsSon;
                studentDeclarobj1.IsDuaghter = objAdDeclaration.IsDuaghter;
                studentDeclarobj1.IsFather = objAdDeclaration.IsFather;
                studentDeclarobj1.IsMother = objAdDeclaration.IsMother;
                studentDeclarobj1.IsGuardian = objAdDeclaration.IsGuardian;
                if (AcademicStandardWindId != 0)
                {
                    var data = boardStandardWingRepository.GetByIdAsync(res.BoardStandardWingID).Result;
                    var data1 = boardStandardRepository.GetByIdAsync(data.BoardStandardID).Result;
                    studentDeclarobj1.ClassId = data1.StandardId;
                    studentDeclarobj1.WingId = data.WingID;
                }

                if (objAdDeclaration.parentSign != null)
                {
                    FileInfo fi = new FileInfo(objAdDeclaration.parentSign.FileName);
                    var newFilename = "ParentSign" + "_" + objAdDeclaration.AdmissionStudentId + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        objAdDeclaration.parentSign.CopyTo(stream);
                    }

                    studentDeclarobj1.SignatureFileName = "/ODMImages/Admission/" + pathToSave;
                }



                studentDeclarobj1.StudentName = objAdDeclaration.StudentName;
                studentDeclarobj1.ParentName = objAdDeclaration.ParentName;
                studentDeclarobj1.ModifiedDate = DateTime.Now;
                studentDeclarobj1.ModifiedId = 0;
                admissionRepository.UpdateAdmissionStudentDeclaration(studentDeclarobj1);
            }
            else
            {
                AdmissionStudentDeclaration studentaddressobj = new AdmissionStudentDeclaration();
                studentaddressobj.IsSon = objAdDeclaration.IsSon;
                studentaddressobj.IsDuaghter = objAdDeclaration.IsDuaghter;
                studentaddressobj.IsFather = objAdDeclaration.IsFather;
                studentaddressobj.IsMother = objAdDeclaration.IsMother;
                studentaddressobj.IsGuardian = objAdDeclaration.IsGuardian;

                if (AcademicStandardWindId != 0)
                {
                    var data = boardStandardWingRepository.GetByIdAsync(res.BoardStandardWingID).Result;
                    var data1 = boardStandardRepository.GetByIdAsync(data.BoardStandardID).Result;
                    studentaddressobj.ClassId = data1.StandardId;
                    studentaddressobj.WingId = data.WingID;
                }
                if (objAdDeclaration.parentSign != null)
                {
                    FileInfo fi = new FileInfo(objAdDeclaration.parentSign.FileName);
                    var newFilename = "ParentSign" + "_" + objAdDeclaration.AdmissionStudentId + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        objAdDeclaration.parentSign.CopyTo(stream);
                    }

                    studentaddressobj.SignatureFileName = "/ODMImages/Admission/" + pathToSave;
                }
                studentaddressobj.StudentName = objAdDeclaration.StudentName;
                studentaddressobj.ParentName = objAdDeclaration.ParentName;
                studentaddressobj.InsertedId = 0;
                studentaddressobj.ModifiedId = 0;
                studentaddressobj.IsAvailable = true;
                studentaddressobj.Status = EntityStatus.ACTIVE;
                studentaddressobj.Active = true;
                studentaddressobj.AdmissionId = objAdDeclaration.AdmissionId;
                var changeid1 = admissionRepository.CreateAdmissionStudentDeclaration(studentaddressobj);

            }
            //    OdmErp.ApplicationCore.Entities.Admission adm =
            //admissionRepository.GetAdmissionById(
            //    objAdDeclaration.AdmissionId);
            return Json(1);
            //return RedirectToAction("Profile", new { id = adm.LeadReferenceId });
        }
        #endregion--------------------------------------------------------

        #region-----------------Student Reference-------------------------
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentReference(StudentReferenceModel studentReference)
        {
            if (studentReference == null || studentReference.AdmissionStudentId == 0)
                return Json(0);
            //  return RedirectToAction("Error", "Student");
            AdmissionStudentReference studentStandardobj = admissionStudentReferenceRepository.ListAllAsyncIncludeAll().Result.
                Where(a => a.AdmissionStudentId == studentReference.AdmissionStudentId && a.ConatctType == "1st").FirstOrDefault();
            if (studentStandardobj != null)
            {
                studentStandardobj.FullName = studentReference.RefName1;
                studentStandardobj.ProfessionTypeId = studentReference.RefProfessionTypeId1;
                studentStandardobj.Mobile = studentReference.RefMobile1;
                studentStandardobj.Address = studentReference.RefAddress1;
                studentStandardobj.Pincode = studentReference.RefPincode1;
                studentStandardobj.CountrytId = studentReference.RefCountrytId1;
                studentStandardobj.ModifiedDate = DateTime.Now;
                studentStandardobj.ModifiedId = 0;
                await admissionStudentReferenceRepository.UpdateAsync(studentStandardobj);
            }
            else
            {

                AdmissionStudentReference obj = new AdmissionStudentReference();
                obj.FullName = studentReference.RefName1;
                obj.ProfessionTypeId = studentReference.RefProfessionTypeId1;
                obj.Mobile = studentReference.RefMobile1;
                obj.Address = studentReference.RefAddress1;
                obj.Pincode = studentReference.RefPincode1;
                obj.CountrytId = studentReference.RefCountrytId1;
                obj.ModifiedDate = DateTime.Now;
                obj.AdmissionStudentId = studentReference.AdmissionStudentId;
                obj.ConatctType = "1st";
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = EntityStatus.ACTIVE;
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = 0;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = 0;
                await admissionStudentReferenceRepository.AddAsync(obj);
            }

            AdmissionStudentReference studentStandard = admissionStudentReferenceRepository.ListAllAsyncIncludeAll().Result.
              Where(a => a.AdmissionStudentId == studentReference.AdmissionStudentId && a.ConatctType == "2nd").FirstOrDefault();
            if (studentStandard != null)
            {
                studentStandard.FullName = studentReference.RefName2;
                studentStandard.ProfessionTypeId = studentReference.RefProfessionTypeId2;
                studentStandard.Mobile = studentReference.RefMobile2;
                studentStandard.Address = studentReference.RefAddress2;
                studentStandard.Pincode = studentReference.RefPincode2;
                studentStandard.CountrytId = studentReference.RefCountrytId2;
                studentStandard.ModifiedDate = DateTime.Now;
                studentStandard.ModifiedId = accessId;
                await admissionStudentReferenceRepository.UpdateAsync(studentStandard);
            }
            else
            {

                AdmissionStudentReference obj1 = new AdmissionStudentReference();
                obj1.FullName = studentReference.RefName2;
                obj1.ProfessionTypeId = studentReference.RefProfessionTypeId2;
                obj1.Mobile = studentReference.RefMobile2;
                obj1.Address = studentReference.RefAddress2;
                obj1.Pincode = studentReference.RefPincode2;
                obj1.CountrytId = studentReference.RefCountrytId2;
                obj1.ModifiedDate = DateTime.Now;
                obj1.AdmissionStudentId = studentReference.AdmissionStudentId;
                obj1.ConatctType = "2nd";
                obj1.IsAvailable = true;
                obj1.Active = true;
                obj1.Status = EntityStatus.ACTIVE;
                obj1.InsertedDate = DateTime.Now;
                obj1.InsertedId = accessId;
                obj1.ModifiedDate = DateTime.Now;
                obj1.ModifiedId = accessId;
                await admissionStudentReferenceRepository.AddAsync(obj1);
            }

            //     OdmErp.ApplicationCore.Entities.Admission adm =
            //admissionRepository.GetAdmissionById(
            //    studentReference.AdmissionId);
            //     //  return RedirectToAction("Profile", new { id = adm.LeadReferenceId });
            return Json(1);
        }
        #endregion--------------------------------------------------------

        #region-----------------Student Father Mother Data-------------------------
        [HttpPost]
        public async Task<IActionResult> SaveOrUploadStudentFatherMotherData(StudentFatherAndMother StudentParent)
        {
            if (StudentParent == null || StudentParent.AdmissionStudentId == 0)
                return Json(0);
            // return RedirectToAction("Error", "Student");

            AdmissionStudentParent studentParentobj = admissionStudentParentRepository.ListAllAsyncIncludeAll().Result.
                Where(a => a.AdmissionStudentId == StudentParent.AdmissionStudentId && a.ParentRelationship == "father").FirstOrDefault();
            if (studentParentobj != null)
            {

                studentParentobj.FullName = StudentParent.FatFullName;
                studentParentobj.PrimaryMobile = StudentParent.FatPrimaryMobile;
                studentParentobj.AlternativeMobile = StudentParent.FatAlternativeMobile;
                studentParentobj.EmailId = StudentParent.FatEmailId;
                studentParentobj.ProfessionTypeId = StudentParent.FatProfessionTypeId;
                studentParentobj.Company = StudentParent.FatCompany;
                studentParentobj.ModifiedDate = DateTime.Now;
                studentParentobj.ModifiedId = 0;
                if (StudentParent.fatherimage != null)
                {
                    FileInfo fi = new FileInfo(StudentParent.fatherimage.FileName);
                    var newFilename = "Father" + "_" + StudentParent.AdmissionStudentId + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await StudentParent.fatherimage.CopyToAsync(stream);
                    }

                    studentParentobj.Image = "/ODMImages/Admission/" + pathToSave;
                }


                await admissionStudentParentRepository.UpdateAsync(studentParentobj);
            }
            else
            {
                AdmissionStudentParent obj = new AdmissionStudentParent();
                obj.FullName = StudentParent.FatFullName;
                obj.PrimaryMobile = StudentParent.FatPrimaryMobile;
                obj.AlternativeMobile = StudentParent.FatAlternativeMobile;
                obj.EmailId = StudentParent.FatEmailId;
                obj.ProfessionTypeId = StudentParent.FatProfessionTypeId;
                obj.ParentRelationship = "father";
                obj.Company = StudentParent.FatCompany;
                if (StudentParent.fatherimage != null)
                {
                    FileInfo fi = new FileInfo(StudentParent.fatherimage.FileName);
                    var newFilename = "Father" + "_" + StudentParent.AdmissionStudentId + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await StudentParent.fatherimage.CopyToAsync(stream);
                    }

                    obj.Image = "/ODMImages/Admission/" + pathToSave;
                }

                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.AdmissionStudentId = StudentParent.AdmissionStudentId;
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = EntityStatus.ACTIVE;
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = 0;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = 0;
                await admissionStudentParentRepository.AddAsync(obj);
            }



            AdmissionStudentParent studentmotherobj = admissionStudentParentRepository.ListAllAsyncIncludeAll().Result.
               Where(a => a.AdmissionStudentId == StudentParent.AdmissionStudentId && a.ParentRelationship == "mother").FirstOrDefault();
            if (studentmotherobj != null)
            {

                studentmotherobj.FullName = StudentParent.MotFullName;
                studentmotherobj.PrimaryMobile = StudentParent.MotPrimaryMobile;
                studentmotherobj.AlternativeMobile = StudentParent.MotAlternativeMobile;
                studentmotherobj.EmailId = StudentParent.MotEmailId;
                studentmotherobj.ProfessionTypeId = StudentParent.MotProfessionTypeId;
                studentmotherobj.Company = StudentParent.MotCompany;
                studentmotherobj.ModifiedDate = DateTime.Now;
                studentmotherobj.ModifiedId = 0;
                if (StudentParent.motherimage != null)
                {
                    FileInfo fi = new FileInfo(StudentParent.motherimage.FileName);
                    var newFilename = "Mother" + "_" + StudentParent.AdmissionStudentId + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await StudentParent.motherimage.CopyToAsync(stream);
                    }

                    studentmotherobj.Image = "/ODMImages/Admission/" + pathToSave;
                }


                await admissionStudentParentRepository.UpdateAsync(studentParentobj);
            }
            else
            {
                AdmissionStudentParent obj = new AdmissionStudentParent();
                obj.FullName = StudentParent.MotFullName;
                obj.PrimaryMobile = StudentParent.MotPrimaryMobile;
                obj.AlternativeMobile = StudentParent.MotAlternativeMobile;
                obj.EmailId = StudentParent.MotEmailId;
                obj.ProfessionTypeId = StudentParent.MotProfessionTypeId;
                obj.ParentRelationship = "mother";
                obj.Company = StudentParent.MotCompany;
                if (StudentParent.motherimage != null)
                {
                    FileInfo fi = new FileInfo(StudentParent.motherimage.FileName);
                    var newFilename = "Mother" + "_" + StudentParent.AdmissionStudentId + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admission\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await StudentParent.motherimage.CopyToAsync(stream);
                    }

                    obj.Image = "/ODMImages/Admission/" + pathToSave;
                }
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedDate = DateTime.Now;
                obj.AdmissionStudentId = StudentParent.AdmissionStudentId;
                obj.IsAvailable = true;
                obj.Active = true;
                obj.Status = EntityStatus.ACTIVE;
                obj.InsertedDate = DateTime.Now;
                obj.InsertedId = 0;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedId = 0;
                await admissionStudentParentRepository.AddAsync(obj);
            }

            //     OdmErp.ApplicationCore.Entities.Admission adm =
            //admissionRepository.GetAdmissionById(
            //    StudentParent.AdmissionId);
            // return RedirectToAction("Profile", new { id = adm.LeadReferenceId });
            return Json(1);
        }

        #endregion--------------------------------------------------------

        #region final submission
        public IActionResult finaladmissionsubmission(long id, string rid)
        {
            ApplicationCore.Entities.Admission admission = admissionRepository.GetAdmissionById(id);
            admission.Status = "SUBMITTED";
            admission.ModifiedDate = DateTime.Now;
            admissionRepository.UpdateAdmission(admission);
            AdmissionStudent std = admissionStudentRepository.GetByAdmissionId(id).Result;
            AdmissionStudentContact stdcnt = admissionStudentContactRepository.ListAllAsync().Result.Where(a => a.AdmissionStudentId == std.ID && a.ConatctType == "official").FirstOrDefault();

            smssend.SendFinalSubmissionRegistrationSms(std.FirstName + " " + std.LastName, admission.MobileNumber);
            sendinBLUE.SendFinalRegistrationMail(std.FirstName + " " + std.LastName, stdcnt.EmailId);
            return Json(1);
        }
        #endregion

        #endregion
        #region Admission Form Report
        public async Task<IActionResult> ViewStudentAdmissionReport(string id)
        {

            try
            {
                var admissionSingle = admissionRepository.GetAdmissionByLeadReferenceId(id);
                var admissionStudentSingle = admissionRepository.GetAdmissionStudentById(admissionSingle.ID);
                var studentId = admissionStudentSingle.ID;
                var admissionStudentContactList = (from a in await admissionStudentContactRepository.ListAllAsyncIncludeActiveNoTrack()
                                                   where a.AdmissionStudentId == studentId
                                                   select a).ToList();
                var admissionstudentaddressList = (from b in await admissionStudentAddressRepository.ListAllAsyncIncludeActiveNoTrack()
                                                   where b.AdmissionStudentId == studentId
                                                   select b).ToList();
                var admissionstudentStandardSingle = (from c in admissionStudentStandardRepository.ListAllAsyncIncludeAll().Result
                                                      join d in academicStandardWingRepository.ListAllAsyncIncludeAll().Result on c.AcademicStandardWindId equals d.ID
                                                      join e in boardStandardWingRepository.ListAllAsyncIncludeAll().Result on d.BoardStandardWingID equals e.ID
                                                      join f in boardStandardRepository.ListAllAsyncIncludeAll().Result on e.BoardStandardID equals f.ID
                                                      join g in StandardRepository.GetAllStandard() on f.StandardId equals g.ID
                                                      where c.AdmissionStudentId == studentId && c.Active == true
                                                      select new admissionStudentStandardcls
                                                      {
                                                          ID = c.ID,
                                                          AcademicStandardWindId = c.AcademicStandardWindId,
                                                          AdmissionStudentId = c.AdmissionStudentId,
                                                          StandardId = c.StandardId,
                                                          Nucleus = c.Nucleus,
                                                          OrganizationId = c.OrganizationId,
                                                          WingId = c.WingId,
                                                          BoardStandardID = e.BoardStandardID
                                                      }).FirstOrDefault();
                var admissionFormPaymentSingle = (from d in await admissionFormPaymentRepository.ListAllAsyncIncludeAll()
                                                  where d.AdmissionId == admissionSingle.ID
                                                  select d).FirstOrDefault();
                var admissionStudentdeclarationSingle = (from e in admissionRepository.GetAllAdmissionStudentDeclaration()
                                                         where e.AdmissionId == admissionSingle.ID
                                                         select e).FirstOrDefault();
                var admissionStudentAcademicSingle = (from a in admissionStudentAcademicRepository.AllAdmissionStudentAcademic()
                                                      where a.AdmissionStudentID == studentId
                                                      select a).FirstOrDefault();
                var admissionStudentLanguageList = (from a in await admissionStudentLanguageRepository.ListAllAsyncIncludeAll()
                                                    join b in await languageRepository.ListAllAsyncIncludeAll() on a.LanguageId equals b.ID
                                                    where a.AdmissionStudentId == studentId
                                                    select new commonCls
                                                    {
                                                        id = b.ID,
                                                        name = b.Name
                                                    }).ToList();
                var admissionStudentParentList = (from a in await admissionStudentParentRepository.ListAllAsyncIncludeAll()
                                                  where a.AdmissionStudentId == studentId
                                                  select a).ToList();
                var admissionStudentProficiencySingle = (from a in await admissionStudentProficiencyRepository.ListAllAsyncIncludeAll()
                                                         where a.AdmissionStudentID == studentId
                                                         select a).FirstOrDefault();

                List<AdmissionStudentReference> admissionStudentReferenceList = new List<AdmissionStudentReference>();
                var jjj = await admissionStudentReferenceRepository.ListAllAsyncIncludeActiveNoTrack();
                if (await admissionStudentReferenceRepository.ListAllAsyncIncludeAll() != null)
                {
                    admissionStudentReferenceList = (from a in await admissionStudentReferenceRepository.ListAllAsyncIncludeAll()
                                                     where a.AdmissionStudentId == studentId
                                                     select a).ToList();
                }
                else
                {
                    admissionStudentReferenceList = null;
                }
                //var admissionStudentReferenceList = (from a in admissionStudentReferenceRepository.ListAllAsyncIncludeAll().Result
                //                                     where a.AdmissionStudentId == admissionStudentSingle.ID
                //                                     select a).ToList();

                var admissionStudentTransportSingle = (from a in await admissionStudentTransportRepository.ListAllAsyncIncludeAll()
                                                       where a.AdmissionStudentId == studentId
                                                       select a).FirstOrDefault();
                var nationality = (from a in nationalityTypeRepository.GetAllNationalities()
                                   select new commonCls
                                   {
                                       id = a.ID,
                                       name = a.Name
                                   }).ToList();
                var categories = (from a in categoryRepository.ListAllAsyncIncludeAll().Result
                                  select new commonCls
                                  {
                                      id = a.ID,
                                      name = a.Name
                                  }).ToList();
                var bloodGroups = (from a in bloodGroupRepository.GetAllBloodGroup()
                                   select new commonCls
                                   {
                                       id = a.ID,
                                       name = a.Name
                                   }).ToList();
                var professiontypes = (from a in professionTypeRepository.GetAllProfessionTypes()
                                       select new commonCls
                                       {
                                           id = a.ID,
                                           name = a.Name
                                       }).ToList();
                var languages = (from a in languageRepository.ListAllAsyncIncludeAll().Result
                                 select new commonCls
                                 {
                                     id = a.ID,
                                     name = a.Name
                                 }).ToList();
                var locations = (from a in supportRepository.GetAllLocation()
                                 select new commonCls
                                 {
                                     id = a.ID,
                                     name = a.Name
                                 }).ToList();
                var countries = (from a in countryRepository.GetAllCountries()
                                 select new commonCls
                                 {
                                     id = a.ID,
                                     name = a.Name
                                 }).ToList();
                var religions = (from a in religionTypeRepository.GetAllReligionTypes()
                                 select new commonCls
                                 {
                                     id = a.ID,
                                     name = a.Name
                                 }).ToList();
                var alldet = admissionSqlQuery.GetView_Student_Admission_List().Where(a => a.ID == admissionSingle.ID).FirstOrDefault();
                var nucleusBool = admissionRepository.GetAllAdmissionStandardSetting().Where(a => a.BoardStandardId == alldet.BoardStandardId).Select(a => a.Nucleus).FirstOrDefault();

                AdmissionRegdDetailsCls ob = new AdmissionRegdDetailsCls()
                {
                    Is_Nucleus = nucleusBool,
                    admission = admissionSingle,
                    admissionStudent = admissionStudentSingle,
                    admissionStudentEmergency = admissionStudentContactList.Count() > 0 ? admissionStudentContactList.Where(a => a.ConatctType == "emergency").FirstOrDefault() : null,
                    admissionStudentOfficial = admissionStudentContactList.Count() > 0 ? admissionStudentContactList.Where(a => a.ConatctType == "official").FirstOrDefault() : null,
                    admissionStudentAddress = admissionstudentaddressList,
                    admissionStudentStandard = admissionstudentStandardSingle,
                    admissionFormPayment = admissionFormPaymentSingle,
                    admissionStudentDeclaration = admissionStudentdeclarationSingle,
                    admissionStudentAcademic = admissionStudentAcademicSingle,
                    admissionStudentLanguage = admissionStudentLanguageList,
                    admissionStudentFather = admissionStudentParentList.Count() > 0 ? admissionStudentParentList.Where(a => a.ParentRelationship == "father").FirstOrDefault() : null,
                    admissionStudentMother = admissionStudentParentList.Count() > 0 ? admissionStudentParentList.Where(a => a.ParentRelationship == "mother").FirstOrDefault() : null,
                    admissionStudentProficiency = admissionStudentProficiencySingle,
                    admissionStudentReference = admissionStudentReferenceList,
                    admissionStudentTransport = admissionStudentTransportSingle,
                    all_Admission = admissionSqlQuery.GetView_Student_Admission_List().Where(a => a.ID == admissionSingle.ID).FirstOrDefault(),
                    nationality = nationality,
                    category = categories,
                    bloodGroups = bloodGroups,
                    professiontypes = professiontypes,
                    languages = languages,
                    locations = locations,
                    countries = countries,
                    religions = religions
                };

                return View(ob);
            }
            catch (Exception e)
            {
                return View(null);
            }

        }
        public FileStreamResult PrintStudentAdmissionReportID(string id)
        {
            HtmlToPdf converter = new HtmlToPdf();
            var webpath = Configuration.GetValue<string>("domain");
            // create a new pdf document converting an url 
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
            converter.Options.MarginLeft = 20;
            converter.Options.MarginRight = 20;
            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 20;
            converter.Options.JavaScriptEnabled = true;
            converter.Options.CssMediaType = HtmlToPdfCssMediaType.Screen;
            // converter.ConvertUrl(webpath + "Report/Index/" + id);
            converter.Options.DisplayFooter = true;
            converter.Footer.Height = 50;

            var path = hostingEnvironment.WebRootPath;
            //PdfHtmlSection footerHtml = new PdfHtmlSection(Path.Combine(path + "\\htmlpages\\dischargefooter.html"));
            //footerHtml.AutoFitHeight = HtmlToPdfPageFitMode.AutoFit;
            //converter.Footer.Add(footerHtml);

            PdfDocument doc = converter.ConvertUrl(webpath + "/Admission/Registration/ViewStudentAdmissionReport/" + id);

            // save pdf document 
            byte[] pdf = doc.Save();
            // close pdf document 
            doc.Close();
            Stream stream = new MemoryStream(pdf);

            return File(stream, "application/pdf", $"" + id + ".pdf");

        }

        #endregion
        public async Task<IActionResult> SendMailSmsForExamDate(long id, string msg)
        {
            var classsettings = admissionRepository.GetAllAdmissionStandardSetting().Where(a => a.ID == id).FirstOrDefault();
            var stds = admissionSqlQuery.GetView_Student_Admission_List().Where(a => a.BoardStandardId == classsettings.BoardStandardId && a.AcademicSessionId == classsettings.AcademicSessionId && a.Status == "SUBMITTED").ToList();

            List<mailsmscls> list = new List<mailsmscls>();
            foreach (var a in stds)
            {
                mailsmscls ob = new mailsmscls();
                ob.email = a.EmergencyEmail;
                ob.mobile = a.Mobile;
                ob.msg = msg;
                ob.name = a.Fullname;
                list.Add(ob);
            }
            Sendsmsmails(list);
            return RedirectToAction("AdmissionStandardSetting", "Master", new { Areas = "Admission" });
        }
        public void Sendsmsmails(List<mailsmscls> mm)
        {
            foreach (var ob in mm)
            {
                Thread bgThread = new Thread(new ParameterizedThreadStart(sendbackgroundmail));
                bgThread.IsBackground = true;
                bgThread.Start(ob);
            }
        }
        public void sendbackgroundmail(Object obj1)
        {
            mailsmscls ob = (mailsmscls)obj1;
            if (ob.mobile != "")
            {
                smssend.SendcOMMONSms(ob.name, ob.mobile, ob.msg);
            }
            if (ob.email != "")
            {
                sendinBLUE.SendCommonMail(ob.name, ob.msg, ob.email);
            }
        }
    }

    public class mailsmscls
    {
        public string name { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string msg { get; set; }
    }

    public class ContactViewmodel
    {
        public string txtEmrName { get; set; }
        public string txtEmrPhNo { get; set; }
        public string txtEmrOfcName { get; set; }
        public string txtEmrOfcPhNo { get; set; }
        public string txtEmrOfcEmail { get; set; }
        public int emegencyId { get; set; }
        public int ofcContactId { get; set; }
    }




}
