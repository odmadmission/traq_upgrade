﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Areas.Admission.Models;
using OdmErp.Web.Areas.Student.Models;
using OdmErp.Web.BillDesk;
using OdmErp.Web.LeadSquared;
using OdmErp.Web.Models;
using OfficeOpenXml;
using RestSharp;
using Web.Controllers;

using OdmErp.Web.Areas.Admission.Constants;

using static OdmErp.Web.LeadSquared.LeadManage;
using System.Text;
using OdmErp.Web.DTO;
using System.Net;
using OdmErp.Web.SendInBlue;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using Newtonsoft.Json;
using OdmErp.ApplicationCore.Query;

namespace OdmErp.Web.Areas.Admission.Controllers
{
    [Area("Admission")]

    public class BasicRegistrationController : AppController
    {
        public IOtpMessageRepository otpMessageRepo;
        public IAcademicSessionRepository academicSessionRepository;
        public IAdmissionRepository admissionRepository;
        public IOrganizationRepository organizationRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IStandardRepository StandardRepository;
        public IBoardRepository boardRepository;
        public IWingRepository wingRepository;
        public AdmissionStudentCommonMethod admissionStudentCommonMethod;
        public IBoardStandardRepository boardStandardRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IAdmissionStudentRepository admissionStudentRepository;
        public IAdmissionStudentContactRepository admissionStudentContactRepository;
        public IAdmissionStudentAddressRepository admissionStudentAddressRepository;
        public IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        public IPaymentType paymentType;
        public IAdmissionFeeRepository admissionFeeRepository;
        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IEmployeeRepository employeeRepository;
        public StudentAdmissionMethod studentAdmissionMethod;
        public CommonMethods commonMethods;
        public IAdmissionStudentCounselorRepository admissionStudentCounselorRepository;
        public IBoardStandardRepository BoardStandardRepository;
        public commonsqlquery commonsqlquery;
        public IAdmissionStudentExamRepository admissionStudentExamRepository;
        private StudentSqlQuery sqlQuery;
        private AdmissionSqlQuery admissionSqlQuery;
        private AdmissionSqlQuery admissionsqlQuery;
        private IAdmissionCounsellorRepository admissionCounsellorRepository;
        private IAdmissionInterViewerRepository admissionInterViewerRepository;
        public IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository;
        private IAdmissionStudentInterviewRepository admissionStudentInterviewRepository;
        private IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository;
        private IAdmissionDocumentAssignRepository admissionDocumentAssignRepository;
        private IAdmissionStudentDocumentRepository admissionStudentDocumentRepository;
        private IAdmissionVerifierRepository admissionVerifierRepository;
        private IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepository;
        public IAdmissionExamDateAssignRepository admissionExamDateAssign;
        public smssend smssend;
        public AdmissionSmsnMails Emailsend;
        private PaymentManager pManger;
        private ILogingRepository loginRepo;
        public SendInBlueEmailManager sendinBLUE;
        public IAdmissionStudentKitRepository admissionStudentKitRepository;
        protected readonly PaymentManager pManager;
        public IAdmissionCampusTourDateRepository admissionCampusTourDateRepository;
        public IAdmissionCampusTourSlotRepository admissionCampusTourSlotRepository;
        public IAdmissionEcounsellingDateRepository admissionEcounsellingDateRepository;
        public IAdmissionEcounsellingSlotRepository admissionEcounsellingSlotRepository;
        public IAdmissionEcounsellingRepository admissionEcounsellingRepository;
        public IAdmissionCampusTourRepository admissionCampusTourRepository;
        public IAdmissionCampusTourTimelineRepository admissionCampusTourTimelineRepository;
        public IAdmissionEcounsellingTimelineRepository admissionEcounsellingTimelineRepository;
        public BasicRegistrationController(IAdmissionCampusTourDateRepository admissionCampusTourDateRepo,
            IAdmissionCampusTourSlotRepository admissionCampusTourSlotRepo,
            IAdmissionEcounsellingDateRepository admissionEcounsellingDateRepo,
            IAdmissionEcounsellingSlotRepository admissionEcounsellingSlotRepo,
            IAdmissionEcounsellingRepository admissionEcounsellingRepo,
            IAdmissionCampusTourRepository admissionCampusTourRepo,
            IAdmissionCampusTourTimelineRepository admissionCampusTourTimelineRepo,
            IAdmissionEcounsellingTimelineRepository admissionEcounsellingTimelineRepo, IAdmissionStudentKitRepository admissionStudentKitRepo, AdmissionSqlQuery admissionSql,
            IAcademicSessionRepository academicSessionRepository, smssend smssend,
            IAdmissionRepository admissionRepository, AdmissionSmsnMails Emailsend,
            AdmissionStudentCommonMethod commonMethod, PaymentManager pManger, AdmissionSqlQuery admissionsql,
            IOrganizationRepository organizationRepository, IAcademicStandardRepository academicStandardRepository,
            IOrganizationAcademicRepository organizationAcademicRepository, IStandardRepository StandardRepository,
            IWingRepository wingRepository, IBoardStandardRepository boardStandardRepository, IBoardRepository boardRepository,
        IAcademicStandardWingRepository academicStandardWingRepository, IBoardStandardWingRepository boardStandardWingRepository,
        IAdmissionStudentRepository admissionStudentRepository, IAdmissionStudentContactRepository admissionStudentContactRepository,
        IAdmissionStudentAddressRepository admissionStudentAddressRepository, IAdmissionStudentStandardRepository admissionStudentStandardRepository,
        IPaymentType paymentType, IAdmissionFormPaymentRepository admissionFormPaymentRepository, IEmployeeRepository employeeRepository,
        StudentAdmissionMethod studentAdmissionMethod, CommonMethods commonMethods, IAdmissionStudentCounselorRepository admissionStudentCounselorRepository,
        IBoardStandardRepository BoardStandardRepository, IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository,
         IAdmissionCounsellorRepository admissionCounsellorRepository, IAdmissionFeeRepository admissionFeeRepository, IOtpMessageRepository otpMessageRepo, ILogingRepository loginRepo,
            SendInBlueEmailManager sendinBLUE,
        commonsqlquery commonsqlquery, IAdmissionStudentExamRepository admissionStudentExamRepository, StudentSqlQuery sqlQuery,
        IAdmissionInterViewerRepository admissionInterViewerRepository, IAdmissionStudentInterviewRepository admissionStudentInterviewRepository,
        IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository,
        IAdmissionDocumentAssignRepository admissionDocumentAssignRepository, IAdmissionStudentDocumentRepository admissionStudentDocumentRepository,
        IAdmissionVerifierRepository admissionVerifierRepository, IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepository,
        IAdmissionExamDateAssignRepository admissionExamDateAssign, PaymentManager pManager)
        {
            admissionCampusTourDateRepository = admissionCampusTourDateRepo;
            admissionCampusTourSlotRepository = admissionCampusTourSlotRepo;
            admissionEcounsellingDateRepository = admissionEcounsellingDateRepo;
            admissionEcounsellingSlotRepository = admissionEcounsellingSlotRepo;
            admissionEcounsellingRepository = admissionEcounsellingRepo;
            admissionCampusTourRepository = admissionCampusTourRepo;
            admissionCampusTourTimelineRepository = admissionCampusTourTimelineRepo;
            admissionEcounsellingTimelineRepository = admissionEcounsellingTimelineRepo;
            admissionStudentKitRepository = admissionStudentKitRepo;
            admissionSqlQuery = admissionSql;
            this.pManager = pManager;
            this.sendinBLUE = sendinBLUE;
            this.otpMessageRepo = otpMessageRepo;
            this.Emailsend = Emailsend;
            this.smssend = smssend;
            this.loginRepo = loginRepo;
            admissionsqlQuery = admissionsql;
            this.admissionStudentCommonMethod = commonMethod;
            this.admissionFeeRepository = admissionFeeRepository;
            this.admissionDocumentAssignRepository = admissionDocumentAssignRepository;
            this.admissionStudentDocumentRepository = admissionStudentDocumentRepository;
            this.admissionCounsellorRepository = admissionCounsellorRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.admissionRepository = admissionRepository;
            this.organizationRepository = organizationRepository;
            this.academicStandardRepository = academicStandardRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.StandardRepository = StandardRepository;
            this.wingRepository = wingRepository;
            this.pManger = pManger;
            this.boardRepository = boardRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.academicStandardWingRepository = academicStandardWingRepository;
            this.boardStandardWingRepository = boardStandardWingRepository;
            this.admissionStudentRepository = admissionStudentRepository;
            this.admissionStudentContactRepository = admissionStudentContactRepository;
            this.admissionStudentAddressRepository = admissionStudentAddressRepository;
            this.admissionStudentStandardRepository = admissionStudentStandardRepository;
            this.paymentType = paymentType;
            this.admissionFormPaymentRepository = admissionFormPaymentRepository;
            this.employeeRepository = employeeRepository;
            this.studentAdmissionMethod = studentAdmissionMethod;
            this.commonMethods = commonMethods;
            this.admissionStudentCounselorRepository = admissionStudentCounselorRepository;
            this.BoardStandardRepository = BoardStandardRepository;
            this.commonsqlquery = commonsqlquery;
            this.admissionStudentExamRepository = admissionStudentExamRepository;
            this.sqlQuery = sqlQuery;
            this.admissionStudentCounsellorRepository = admissionStudentCounsellorRepository;
            this.admissionInterViewerRepository = admissionInterViewerRepository;
            this.admissionStudentInterviewRepository = admissionStudentInterviewRepository;
            this.admissionPersonalInterviewAssignRepository = admissionPersonalInterviewAssignRepository;
            this.admissionVerifierRepository = admissionVerifierRepository;
            this.admissionDocumentVerificationAssignRepository = admissionDocumentVerificationAssignRepository;

            this.admissionExamDateAssign = admissionExamDateAssign;

        }
        #region-----------Json Method------------------------
        public JsonResult GetOrganisationByAcademicSession(long AcademicSessionId)                 //Filter the selected Standard
        {
            try
            {
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll()
                    .Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.Active == true);
                var OrganisationList = organizationRepository.GetAllOrganization();
                var academicstandard = academicStandardRepository.ListAllAsyncIncludeAll().Result
                    .Where(a => a.Active == true); ;

                var res = (from a in OrganisationAcademic
                           join b in OrganisationList on a.OrganizationId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name
                           }
                          ).ToList();



                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        public JsonResult GetAcademicStandard(long BoardId, long OraganisationAcademicId, long AcademicSessionId)
        {
            var boardstandard = BoardStandardRepository.ListAllAsync()
                .Result.Where(a => a.BoardId == BoardId && a.Active == true).Select(a => a.ID).ToList();
            var boardStandards = academicStandardRepository
                .ListAllAsync().Result.Where(a => a.OrganizationAcademicId == OraganisationAcademicId
                && a.Active == true && boardstandard.Contains(a.BoardStandardId)).Select(a => a.BoardStandardId).ToList();

            var standards = BoardStandardRepository
                .ListAllAsync().Result.Where(a => a.Active == true && boardStandards.Contains(a.ID)).Select(a => a.StandardId).Distinct().ToList();

            //var academicstandard = commonsqlquery.Get_view_All_Academic_Standards().Where(a => a.AcademicSessionId == AcademicSessionId && a.OraganisationAcademicId == OraganisationAcademicId).ToList();
            var res = (from a in standards
                       join b in StandardRepository.GetAllStandard() on a equals b.ID
                       select new CommonMasterModel
                       {
                           ID = a, //StandardId
                           Name = b.Name
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetAcademicWings(long BoardId,
            long StandardId, long OraganisationAcademicId, long AcademicSessionId)
        {


            var boardstandard = BoardStandardRepository.ListAllAsync()
                .Result.Where(a => a.BoardId == BoardId && a.StandardId == StandardId && a.Active == true).Select(a => a.ID).ToList();
            var academicStandards = academicStandardRepository
                .ListAllAsync().Result.Where(a => a.OrganizationAcademicId == OraganisationAcademicId
                && a.Active == true && boardstandard.Contains(a.BoardStandardId)).Select(a => a.ID).ToList();

            var wingIds = academicStandardWingRepository
                .ListAllAsync().Result.Where(a => a.Active == true && academicStandards.
                Contains(a.AcademicStandardId)).Select(a => a.WingID).Distinct().ToList();

            // var academicstandard = commonsqlquery.Get_view_All_Academic_Standards().Where(a => a.AcademicSessionId == AcademicSessionId && a.OraganisationAcademicId == OraganisationAcademicId).ToList();
            var res = (from a in wingIds
                       join b in wingRepository.ListAllAsync().Result.ToList() on a equals b.ID
                       select new CommonMasterModel
                       {
                           ID = a.Value, //WingID
                           Name = b.Name
                       }).ToList();
            return Json(res);
        }

        [HttpGet]
        public JsonResult GetBoardsByAcademicOrganizationId(long AcademicOrganizationId)
        {
            var boardStandardIds = academicStandardRepository.ListAllAsync().Result.
                Where(a => a.OrganizationAcademicId == AcademicOrganizationId && a.Active == true).Select(a => a.BoardStandardId).ToList();

            var boardIds = boardStandardRepository.ListAllAsync().
                Result.Where(a => boardStandardIds.Contains(a.ID)).Select(a => a.BoardId).Distinct().ToList();

            var res = (from a in boardIds

                       join c in boardRepository.GetAllBoard().ToList()
                          on a equals c.ID
                       select new CommonMasterModel
                       {

                           ID = a, //boardId
                           Name = c.Name //BoardName
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetBoardStandard(long BoardId, long orgid)
        {
            var academicstandard = academicStandardRepository.ListAllAsync().Result.Where(a => a.OrganizationAcademicId == orgid && a.Active == true).Select(a => a.BoardStandardId).ToList();
            var boardstandard = BoardStandardRepository.ListAllAsync().Result.Where(a => a.BoardId == BoardId && academicstandard.Contains(a.ID) && a.Active == true).ToList();
            var standard = StandardRepository.GetAllStandard().ToList();

            var res = (from a in boardstandard
                       join b in standard on a.StandardId equals b.ID
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = b.Name
                       }).ToList();
            return Json(res);
        }

        public JsonResult GetSlots(long AcademicSessionId)
        {
            var slots = admissionRepository.
                GetAllAdmissionSlot().Where(a => a.AcademicSessionId == AcademicSessionId
                && a.Active == true).ToList();
            //var standard = StandardRepository.GetAllStandard().ToList();

            var res = (from a in slots

                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = a.SlotName
                       }).ToList();
            return Json(res);
        }


        public JsonResult GetClassListBySession()
        {
            var academicsessionid = academicSessionRepository.GetAllAcademicSession().Where(a => a.IsAdmission == true).FirstOrDefault().ID;

            return GetClassByAcademicSession1(academicsessionid);
        }
        public JsonResult GetClassByAcademicSession1(long sessionid)
        {

            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                    Result.Where(a => a.AcademicSessionId == sessionid && a.Active == true).ToList().Select(a => a.ID);


            var boardStandards = academicStandardRepository.ListAllAsync().Result.
                    Where(a => orgAcademics.Contains(a.OrganizationAcademicId) && a.Active == true).ToList().Select(a => a.BoardStandardId);

            var standardsBoards = boardStandardRepository.ListAllAsync().
                    Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
            var standards = StandardRepository.GetAllStandard();
            var boards = boardRepository.GetAllBoard();

            var res = (from a in standardsBoards
                       select new
                       {
                           boardClass = standards.Where(m => m.ID == a.StandardId).FirstOrDefault().Name,
                           boardId = a.BoardId,
                           boardname = boards.Where(m => m.ID == a.BoardId).FirstOrDefault().Name,
                           standardId = a.StandardId,
                           boardStandardId = a.ID
                       }
                             ).Distinct().ToList();
            return Json(res);
        }


        public JsonResult GetClassByAcademicSession(long OrgAcdId)
        {

            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                    Result.Where(a => a.ID == OrgAcdId && a.Active == true).ToList().Select(a => a.ID);


            var boardStandards = academicStandardRepository.ListAllAsync().Result.
                    Where(a => orgAcademics.Contains(a.OrganizationAcademicId) && a.Active == true).ToList().Select(a => a.BoardStandardId);

            var standardsBoards = boardStandardRepository.ListAllAsync().
                    Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
            var standards = StandardRepository.GetAllStandard();
            var boards = boardRepository.GetAllBoard();

            var res = (from a in standardsBoards
                       select new
                       {
                           boardClass = standards.Where(m => m.ID == a.StandardId).FirstOrDefault().Name,
                           boardId = a.BoardId,
                           boardname = boards.Where(m => m.ID == a.BoardId).FirstOrDefault().Name,
                           standardId = a.StandardId,
                           boardStandardId = a.ID
                       }
                             ).Distinct().ToList();
            return Json(res);
        }

        public JsonResult GetClassByAcademicSessionclasssettings(long OrgAcdId)
        {

            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                    Result.Where(a => a.ID == OrgAcdId && a.Active == true).ToList().Select(a => a.ID);

            var admissionstandardsetinglist = admissionRepository.GetAllAdmissionStandardSetting().ToList().Count==0 ? null : admissionRepository.GetAllAdmissionStandardSetting().Where(a => a.OrganizationId == OrgAcdId && a.Active == true).Select(a => a.BoardStandardId);

            var boardStandards = academicStandardRepository.ListAllAsync().Result.
                    Where(a => orgAcademics.Contains(a.OrganizationAcademicId) && a.Active == true).ToList().Select(a => a.BoardStandardId);
            if (admissionstandardsetinglist!=null)
            {
                boardStandards = boardStandards.Except(admissionstandardsetinglist);
            }
            var standardsBoards = boardStandardRepository.ListAllAsync().
                    Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
            var standards = StandardRepository.GetAllStandard();
            var boards = boardRepository.GetAllBoard();

            var res = (from a in standardsBoards
                       select new
                       {
                           boardClass = standards.Where(m => m.ID == a.StandardId).FirstOrDefault().Name,
                           boardId = a.BoardId,
                           boardname = boards.Where(m => m.ID == a.BoardId).FirstOrDefault().Name,
                           standardId = a.StandardId,
                           boardStandardId = a.ID
                       }
                             ).Distinct().ToList();
            return Json(res);
        }
        public JsonResult GetSchoolWingByBoardStandard(long BoardStandardId)
        {
            var academicsessionid = academicSessionRepository.GetAllAcademicSession().Where(a => a.IsAdmission == true).FirstOrDefault().ID;

            return GetWingByBoardStandard(BoardStandardId, academicsessionid);
        }

        public JsonResult GetWingByBoardStandard(long BoardStandardId, long SessionId)
        {
            //long BoardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().BoardId;
            //long StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().StandardId;

            //#region GetOrganizationNames


            //var boardStandards = boardStandardRepository.ListAllAsync().Result.
            //    Where(a => a.Active == true && a.BoardId == BoardId & a.StandardId == StandardId).
            //    ToList().Select(a => a.ID);


            //var organizationAcademics = academicStandardRepository.ListAllAsync().Result.
            //    Where(a => a.Active == true && boardStandards.Contains(a.BoardStandardId)).
            //    ToList().Select(a => a.OrganizationAcademicId);

            //var OrganizationIds = organizationAcademicRepository.
            //       ListAllAsync().Result.Where(a => organizationAcademics.
            //       Contains(a.ID) && a.Active == true && a.AcademicSessionId == SessionId).ToList().Select(a => a.OrganizationId).Distinct();

            //var organisation = organizationRepository.GetAllOrganization().
            //    Where(a => OrganizationIds.Contains(a.ID)).ToList();
            //#endregion

            //List<WingOrgModel> schoolWingNames = new List<WingOrgModel>();

            //foreach (var org in organisation)
            //{


            //var boardStandardWingId = academicStandardWingRepository.ListAllAsync().Result
            //    .Where(a => a.AcademicStandardId == academicStandardId && a.Active == true).
            //    Select(a => a.BoardStandardWingID).ToList();
            //foreach(var b in boardStandardWingId)
            //{
            //    var AcademicStandardWingId = academicStandardWingRepository.ListAllAsync().Result
            //     .Where(a => a.BoardStandardWingID == b && a.Active == true).
            //     Select(a => a.ID).FirstOrDefault();


            //    var orgAcademicId = organizationAcademicRepository.
            //        ListAllAsync().Result.Where(a => a.OrganizationId == org.ID && a.AcademicSessionId == SessionId)
            //        .Select(a => a.ID).FirstOrDefault();
            //    var academicStandardId = academicStandardRepository.ListAllAsync()
            //        .Result.Where(a => a.BoardStandardId == BoardStandardId && a.Active == true &&
            //        a.OrganizationAcademicId == orgAcademicId).Select(a => a.ID).FirstOrDefault();

            ////    var boardStandardWingId = academicStandardWingRepository.ListAllAsync().Result
            ////        .Where(a => a.AcademicStandardId == academicStandardId && a.Active == true).
            ////        Select(a => a.BoardStandardWingID).ToList();
            ////    foreach(var b in boardStandardWingId)
            ////    {
            ////        var AcademicStandardWingId = academicStandardWingRepository.ListAllAsync().Result
            ////         .Where(a => a.AcademicStandardId == academicStandardId && a.Active == true).
            ////         Select(a => a.ID).FirstOrDefault();

            //        var WingId = boardStandardWingRepository.ListAllAsync().Result
            //           .Where(a => a.ID == b && a.Active == true).
            //           Select(a => a.WingID).FirstOrDefault();

            //        var WingName = wingRepository.ListAllAsync().Result
            //            .Where(a => a.ID == WingId && a.Active == true).
            //            Select(a => a.Name).FirstOrDefault();

            //        if (WingName != null)
            //        {
            //            WingOrgModel m = new WingOrgModel();
            //            m.SchoolId = org.ID;
            //            m.WingId = WingId;
            //            m.Name = org.Name + "-" + WingName;
            //            m.AcademicStandardWindId = AcademicStandardWingId;
            //            schoolWingNames.Add(m);

            //        }

            //    }

            //}
            //return Json(schoolWingNames);
            var standardsBoards = boardStandardRepository.ListAllAsync().
                  Result.Where(a => a.ID == BoardStandardId && a.Active == true).ToList();
            var standardsBoardswing = boardStandardWingRepository.ListAllAsync().
                 Result.Where(a => a.BoardStandardID == BoardStandardId && a.Active == true).ToList();
            List<WingOrgModel> list = new List<WingOrgModel>();
            var academicstandard = academicStandardRepository.ListAllAsync().Result;
            var academicstandardwing = academicStandardWingRepository.ListAllAsync().Result;
            var organisation = organizationRepository.GetAllOrganization();
            var session = academicSessionRepository.ListAllAsync().Result.Where(a => a.IsAdmission == true && a.IsAvailable == true).FirstOrDefault();
            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                 Result.Where(a => a.AcademicSessionId == session.ID && a.Active == true).ToList();
            var wings = wingRepository.ListAllAsync().Result;
            foreach (var org in orgAcademics)
            {
                var boardStandards = academicstandard.
                    Where(a => a.OrganizationAcademicId == org.ID && a.Active == true && a.BoardStandardId == BoardStandardId).FirstOrDefault();
                if (boardStandards != null)
                {
                    foreach (var st in standardsBoardswing)
                    {
                        var academicwing = academicstandardwing.Where(a => a.AcademicStandardId == boardStandards.ID && a.Active == true && a.BoardStandardWingID == st.ID).FirstOrDefault();
                        if (academicwing != null)
                        {
                            var wingname = wings.Where(a => a.ID == st.WingID).FirstOrDefault().Name;
                            var organisationname = organisation.Where(a => a.ID == org.OrganizationId).FirstOrDefault().Name;
                            WingOrgModel od = new WingOrgModel();
                            od.AcademicStandardWindId = academicwing.ID;
                            od.Name = organisationname + "-" + wingname;
                            od.SchoolId = org.ID;
                            od.WingId = st.WingID;
                            od.WingName = wingname;
                            list.Add(od);
                        }
                    }
                }
            }

            return Json(list);
        }
        public class WingOrgModel
        {
            public long SchoolId { get; set; }
            public long WingId { get; set; }
            public string Name { get; set; }
            public string WingName { get; set; }
            public long AcademicStandardWindId { get; set; }
        }

        public JsonResult MobileValidationBySession(string Mobile)
        {
            var academicsessionid = academicSessionRepository.GetAllAcademicSession().Where(a => a.IsAdmission == true).FirstOrDefault().ID;

            return MobileValidationBySessionWise(academicsessionid, Mobile);
        }

        public JsonResult MobileValidationBySessionWise(long AcademicSessionId, string Mobile)
        {
            int data = 0;
            var session = admissionRepository.GetAllAdmission().Where(a => a.AcademicSessionId == AcademicSessionId).FirstOrDefault();
            if (session != null)
            {
                var sessionmobile = admissionRepository.GetAllAdmission().Where(a => a.MobileNumber == Mobile).FirstOrDefault();
                if (sessionmobile != null)
                {
                    data = 1;
                }
                else
                {
                    data = 0;
                }
            }
            else
            {
                data = 0;
            }
            return Json(data);
        }
        #endregion-----------------------------------------------------------

        #region--------------------------Common Method--------------------------------
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public List<sessiondetailmodal> GetClassListByAcademicSession(long? AcademicSessionId)
        {

            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                    Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.Active == true).ToList().Select(a => a.ID);

            var boardStandards = academicStandardRepository.ListAllAsync().Result.
                    Where(a => orgAcademics.Contains(a.OrganizationAcademicId) && a.Active == true).ToList().Select(a => a.BoardStandardId);

            var standardsBoards = boardStandardRepository.ListAllAsync().
                    Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
            var standards = (from a in standardsBoards
                             select new sessiondetailmodal
                             {
                                 boardClass = StandardRepository.GetStandardById(a.StandardId).Name,
                                 boardId = a.BoardId,
                                 boardname = boardRepository.GetBoardById(a.BoardId).Name,
                                 standardId = a.StandardId,
                                 boardStandardId = a.ID
                             }
                             ).Distinct().ToList();
            return standards;
        }
        public List<WingOrgModel> GetWingListByBoardStandardId(long? BoardStandardId)
        {
            long BoardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().BoardId;
            long StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().StandardId;

            #region GetOrganizationNames


            var boardStandards = boardStandardRepository.ListAllAsync().Result.
                Where(a => a.Active == true && a.BoardId == BoardId & a.StandardId == StandardId).
                ToList().Select(a => a.ID);


            var organizationAcademics = academicStandardRepository.ListAllAsync().Result.
                Where(a => a.Active == true && boardStandards.Contains(a.BoardStandardId)).
                ToList().Select(a => a.OrganizationAcademicId);

            var OrganizationIds = organizationAcademicRepository.
                   ListAllAsync().Result.Where(a => organizationAcademics.
                   Contains(a.ID) && a.Active == true).ToList().Select(a => a.OrganizationId).Distinct();

            var organisation = organizationRepository.GetAllOrganization().
                Where(a => OrganizationIds.Contains(a.ID));

            #endregion

            List<WingOrgModel> schoolWingNames = new List<WingOrgModel>();

            foreach (var org in organisation)
            {
                var orgAcademicId = organizationAcademicRepository.
                    ListAllAsync().Result.Where(a => a.OrganizationId == org.ID).
                    Select(a => a.ID).FirstOrDefault();
                var academicStandardId = academicStandardRepository.ListAllAsync()
                    .Result.Where(a => a.BoardStandardId == BoardStandardId && a.Active == true &&
                    a.OrganizationAcademicId == orgAcademicId).Select(a => a.ID).FirstOrDefault();

                var boardStandardWingId = academicStandardWingRepository.ListAllAsync().Result
                    .Where(a => a.AcademicStandardId == academicStandardId && a.Active == true).
                    Select(a => a.BoardStandardWingID).FirstOrDefault();

                var AcademicStandardWingId = academicStandardWingRepository.ListAllAsync().Result
                   .Where(a => a.AcademicStandardId == academicStandardId && a.Active == true).
                   Select(a => a.ID).FirstOrDefault();

                var WingId = boardStandardWingRepository.ListAllAsync().Result
                   .Where(a => a.ID == boardStandardWingId && a.Active == true).
                   Select(a => a.WingID).FirstOrDefault();

                var WingName = wingRepository.ListAllAsync().Result
                    .Where(a => a.ID == WingId && a.Active == true).
                    Select(a => a.Name).FirstOrDefault();

                if (WingName != null)
                {
                    WingOrgModel m = new WingOrgModel();
                    m.SchoolId = org.ID;
                    m.WingId = WingId;
                    m.Name = org.Name + "-" + WingName;
                    m.AcademicStandardWindId = AcademicStandardWingId;
                    schoolWingNames.Add(m);

                }
            }
            return schoolWingNames;
        }
        #endregion-------------------------

        #region............registration Module access--------------------
        public RegistrationModuleActionAccess GetRegdAdmissionAccessDetails()
        {
            RegistrationModuleActionAccess acc = new RegistrationModuleActionAccess();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            acc.ViewProfile = commonMethods.checkaccessavailable("Registrations", accessId,
                "ViewProfile", "Admission", roleId);
            acc.EditProfile = commonMethods.checkaccessavailable("Registrations", accessId,
                "EditProfile", "Admission", roleId);
            acc.List = commonMethods.checkaccessavailable("Registrations", accessId,
               "List", "Admission", roleId);
            acc.Pay = commonMethods.checkaccessavailable("Registrations", accessId,
             "Pay", "Admission", roleId);
            acc.UploadExcel = commonMethods.checkaccessavailable("Registrations", accessId,
             "UploadExcel", "Admission", roleId);
            acc.NewRegistration = commonMethods.checkaccessavailable("Registrations", accessId,
             "NewRegistration", "Admission", roleId);
            acc.AssignCounsellor = commonMethods.checkaccessavailable("Registrations", accessId,
             "AssignCounsellor", "Admission", roleId);
            acc.Result = commonMethods.checkaccessavailable("Registrations", accessId,
           "Result", "Admission", roleId);
            return acc;

        }
        #endregion

        #region-----------------------Save And List Student Registration--------------
        public IActionResult StudentRegistration()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId,
                "NewRegistration", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.Session = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();
            ViewBag.AdmissionSlot = admissionRepository.GetAllAdmissionSlot().ToList();
            ViewBag.AdmissionSource = admissionRepository.GetAllAdmissionSource().ToList();

            return View();
        }


        public IActionResult ValidMobileNoLogin(string MobileNo)
        {
            OdmErp.ApplicationCore.Entities.Admission objAdmission =
                admissionRepository.GetAllAdmission().
                Where(a => a.MobileNumber == MobileNo).FirstOrDefault();
            if (objAdmission != null)
            {
                HttpContext.Session.SetString(LoginSessionData.UNIQUE_ID,
                    Convert.ToString(objAdmission.ID));

                //  return Json(addmisionid);
                return Json("/Admission/Student/MobileValidation/" + objAdmission.LeadReferenceId);
                //return RedirectToAction("MobileValidation",
                //    "Student",
                //         new
                //         {
                //             area = "Admission",
                //             id = objAdmission.LeadReferenceId

                //         });


            }
            else
            {
                // return Json(0);

                return Json("/Admission/Student/Login");

                //return RedirectToAction("Login", "Student",
                //         new { area = "Admission" });
            }

        }

        [HttpPost]
        public async Task<IActionResult> AdmissionSave(string firstName,
            string lastName,
            string contactName,
            string contactMobile,
            string email,
            string Class,
            string School, long sourceId, bool chkecounselling,
            long? counsellingdate,
            long? counsellingslot,
            bool chkcampustour, long? campustourdate, long? campustourslot)
        {
            CheckLoginStatus();
            var addmisionid = await admissionStudentCommonMethod.SaveAdmissionDataAsync(
               firstName,
                lastName,
             contactName,
             contactMobile,
             email,
             Class,
             School,
             sourceId
               );
            if (chkecounselling == true)
            {
                AdmissionEcounselling counsellor = new AdmissionEcounselling();
                counsellor.Active = true;
                counsellor.AdmissionEcounsellingDateId = counsellingdate.Value;
                counsellor.AdmissionEcounsellingSlotId = counsellingslot.Value;
                counsellor.AdmissionId = addmisionid.ID;
                counsellor.VisitedDate = DateTime.Now;
                counsellor.InsertedDate = DateTime.Now;
                counsellor.ModifiedDate = DateTime.Now;
                counsellor.InsertedId = userId;
                counsellor.ModifiedId = userId;
                counsellor.IsAvailable = true;
                counsellor.IssmsSend = false;
                counsellor.Isvisited = false;
                counsellor.Status = "BOOKED";
                var ecounselling = await admissionEcounsellingRepository.AddAsync(counsellor);
                AdmissionEcounsellingTimeline timeline = new AdmissionEcounsellingTimeline();
                timeline.InsertedDate = DateTime.Now;
                timeline.ModifiedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedId = userId;
                timeline.IsAvailable = true;
                timeline.Active = true;
                timeline.AdmissionEcounsellingId = ecounselling.ID;
                timeline.Name = "BOOKED";
                timeline.Status = "ACTIVE";
                await admissionEcounsellingTimelineRepository.AddAsync(timeline);
            }
            if (chkcampustour == true)
            {
                AdmissionCampusTour counsellor = new AdmissionCampusTour();
                counsellor.Active = true;
                counsellor.AdmissionCampusTourDateId = campustourdate.Value;
                counsellor.AdmissionCampusTourSlotId = campustourslot.Value;
                counsellor.AdmissionId = addmisionid.ID;
                counsellor.VisitedDate = DateTime.Now;
                counsellor.InsertedDate = DateTime.Now;
                counsellor.ModifiedDate = DateTime.Now;
                counsellor.InsertedId = userId;
                counsellor.ModifiedId = userId;
                counsellor.IsAvailable = true;
                counsellor.IssmsSend = false;
                counsellor.Isvisited = false;
                counsellor.Status = "BOOKED";
                var camustourid = await admissionCampusTourRepository.AddAsync(counsellor);
                AdmissionCampusTourTimeline timeline = new AdmissionCampusTourTimeline();
                timeline.InsertedDate = DateTime.Now;
                timeline.ModifiedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedId = userId;
                timeline.IsAvailable = true;
                timeline.Active = true;
                timeline.AdmissionCampusTourId = camustourid.ID;
                timeline.Name = "BOOKED";
                timeline.Status = "ACTIVE";
                await admissionCampusTourTimelineRepository.AddAsync(timeline);
            }
            if (addmisionid != null)
            {



                string encoded = UtilityModel.Encode(addmisionid.LeadReferenceId.ToString());
                HttpContext.Session.SetString(LoginSessionData.UNIQUE_ID,
                   Convert.ToString(addmisionid.ID));

                //  return Json(addmisionid);
                return Json(new { sts = 1, encode = encoded });
                //return RedirectToAction("MobileValidation", "Student",
                //         new { area = "Admission", id = encoded });


            }
            else
            {
                // return Json(0);

                return Json(new { sts = 2 });

                //return RedirectToAction("Register", "Student",
                //         new { area = "Admission" });
            }
            //  return json(nameof(ParentRegistration), new { Admid = addmisionid }).WithSuccess("Registration", "Saved Successfully");
        }



        [HttpPost]
        public async Task<IActionResult> SaveStudentRegistration(
            BasicStudentModel stumodel, bool chkecounselling,
            long? counsellingdate,
            long? counsellingslot,
            bool chkcampustour, long? campustourdate, long? campustourslot)
        {
            CheckLoginStatus();


            long OtpID = 0;
            var addmisionid = await admissionStudentCommonMethod.SaveAdmissionDataAsync
                (
                stumodel.FirstName,
                stumodel.LastName,
                stumodel.FullName,
                stumodel.Mobile,
                 stumodel.EmailId,
                stumodel.StandardId + "",
                stumodel.AcademicStandardWindId + "",
                stumodel.AcademicSourceId
                );
            if (chkecounselling == true)
            {
                AdmissionEcounselling counsellor = new AdmissionEcounselling();
                counsellor.Active = true;
                counsellor.AdmissionEcounsellingDateId = counsellingdate.Value;
                counsellor.AdmissionEcounsellingSlotId = counsellingslot.Value;
                counsellor.AdmissionId = addmisionid.ID;
                counsellor.VisitedDate = DateTime.Now;
                counsellor.InsertedDate = DateTime.Now;
                counsellor.ModifiedDate = DateTime.Now;
                counsellor.InsertedId = userId;
                counsellor.ModifiedId = userId;
                counsellor.IsAvailable = true;
                counsellor.IssmsSend = false;
                counsellor.Isvisited = false;
                counsellor.Status = "BOOKED";
                var ecounselling = await admissionEcounsellingRepository.AddAsync(counsellor);
                AdmissionEcounsellingTimeline timeline = new AdmissionEcounsellingTimeline();
                timeline.InsertedDate = DateTime.Now;
                timeline.ModifiedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedId = userId;
                timeline.IsAvailable = true;
                timeline.Active = true;
                timeline.AdmissionEcounsellingId = ecounselling.ID;
                timeline.Name = "BOOKED";
                timeline.Status = "ACTIVE";
                await admissionEcounsellingTimelineRepository.AddAsync(timeline);
            }
            if (chkcampustour == true)
            {
                AdmissionCampusTour counsellor = new AdmissionCampusTour();
                counsellor.Active = true;
                counsellor.AdmissionCampusTourDateId = campustourdate.Value;
                counsellor.AdmissionCampusTourSlotId = campustourslot.Value;
                counsellor.AdmissionId = addmisionid.ID;
                counsellor.VisitedDate = DateTime.Now;
                counsellor.InsertedDate = DateTime.Now;
                counsellor.ModifiedDate = DateTime.Now;
                counsellor.InsertedId = userId;
                counsellor.ModifiedId = userId;
                counsellor.IsAvailable = true;
                counsellor.IssmsSend = false;
                counsellor.Isvisited = false;
                counsellor.Status = "BOOKED";
                var camustourid = await admissionCampusTourRepository.AddAsync(counsellor);
                AdmissionCampusTourTimeline timeline = new AdmissionCampusTourTimeline();
                timeline.InsertedDate = DateTime.Now;
                timeline.ModifiedDate = DateTime.Now;
                timeline.InsertedId = userId;
                timeline.ModifiedId = userId;
                timeline.IsAvailable = true;
                timeline.Active = true;
                timeline.AdmissionCampusTourId = camustourid.ID;
                timeline.Name = "BOOKED";
                timeline.Status = "ACTIVE";
                await admissionCampusTourTimelineRepository.AddAsync(timeline);
            }
            if (addmisionid != null)
            {
                long councellorid = 0;
                if (roleId == 1)
                {
                    councellorid = 0;
                }
                else
                {
                    //Start AdmissionStudentCounselor insert date
                    councellorid = admissionCounsellorRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == userId).FirstOrDefault().ID;
                    AdmissionStudentCounselor counsellor = new AdmissionStudentCounselor();
                    counsellor.Status = EntityStatus.ACTIVE;
                    counsellor.Active = true;
                    counsellor.IsAvailable = true;
                    counsellor.InsertedDate = DateTime.Now;
                    counsellor.InsertedId = userId;
                    counsellor.ModifiedDate = DateTime.Now;
                    counsellor.ModifiedId = userId;
                    counsellor.AdmissionStudentId = admissionStudentRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionId == addmisionid.ID).FirstOrDefault().ID;
                    counsellor.EmployeeId = councellorid;
                    counsellor.AssignedDate = DateTime.Now;
                    await admissionStudentCounsellorRepository.AddAsync(counsellor);
                }

                //End AdmissionStudentCounselor insert date

                var admStudent = admissionStudentRepository.ListAllAsync()
                    .Result.Where(a => a.AdmissionId ==
                addmisionid.ID).FirstOrDefault();

                var admission = admissionRepository.GetAllAdmission().
              Where(a => a.LeadReferenceId == addmisionid.LeadReferenceId).FirstOrDefault();
                if (admission != null)
                {
                    string regdotp = GenerateRandomNumber(6);
                    var otpvalue = smssend.SendotpForParent(regdotp, admission.MobileNumber);
                    if (otpvalue == true)
                    {
                        OtpMessage objotp = new OtpMessage();
                        objotp.AppType = "WEB";
                        objotp.AccessID = 0;
                        objotp.Mobile = admission.MobileNumber;
                        objotp.SentOtpCode = regdotp;
                        objotp.Status = "ACTIVE";
                        objotp.ValidTill = DateTime.Now.AddMinutes(10);
                        objotp.OtpType = "ADMISSION_REGISTRATION";
                        objotp.IsAvailable = true;
                        objotp.Status = EntityStatus.ACTIVE;
                        objotp.Active = true;
                        objotp.InsertedDate = DateTime.Now;
                        objotp.InsertedId = userId;
                        objotp.ModifiedDate = DateTime.Now;
                        objotp.ModifiedId = userId;
                        var res = otpMessageRepo.AddAsync(objotp).Result;
                        OtpID = res.ID;
                    }
                }

                //#region--------- smg And Email (Complited)------------------
                //var details = studentAdmissionMethod.GetStudentDetailsByStudentId(admStudent.ID);

                //string Message = HttpAddSmg.BasicRegistration_NotPaid.Replace("{{StudentName}}", details.FullName);
                //if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                //{
                //    smssend.Sendsmstoemployee(details.Mobile, Message);
                //}
                //if (details.EmailId != null && details.EmailId != "")
                //{
                //    Emailsend.BasicRegd_NotPaid(details.AcademicSessionName,
                //        details.ClassDetails.schoolName, 
                //        details.ClassDetails.boardName, 
                //        details.ClassDetails.className,
                //        details.ClassDetails.wingName,
                //        details.FormNumber,
                //        details.FullName,
                //        details.Mobile, 
                //        details.EmailId,
                //        Message,
                //        details.EmailId);
                //}
                //#endregion


                //return RedirectToAction(nameof(StudentPaymentMode), new { Admid = addmisionid.ID }). WithSuccess("Basic Registration", "Saved Successfully");
                return RedirectToAction(nameof(RegistrationOtp), new { leadReferenceId = addmisionid.LeadReferenceId, otpid = OtpID });
            }
            else
            {
                return RedirectToAction(nameof(StudentRegistration)).
                  WithSuccess("Basic Registration ", "Failed");
            }
        }
        public IActionResult RegistrationOtp(long OtpID, Guid? leadReferenceId)
        {
            ViewBag.LeadReferenceId = leadReferenceId;
            var admission = admissionRepository.GetAllAdmission().Where(a => a.LeadReferenceId == leadReferenceId).FirstOrDefault();
            ViewBag.MobileNumber = admission.MobileNumber;
            ViewBag.AdmissionId = admission.ID;
            var otp = otpMessageRepo.GetByIdAsyncIncludeAll(OtpID).Result;
            ViewBag.OtpNumber = otp.SentOtpCode;
            ViewBag.OtpId = OtpID;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ValidateRegistrationOfflineOtp(long otpId, string MobileNo, string OtpNumber, string AdmissionId)
        {
            var res = await ValidateOtpCode(otpId, OtpNumber);
            string encoded = UtilityModel.Encode(res.Item1.ToString());

            if (res.Item2 == "success")
            {
                var adm = admissionRepository.
                    GetAdmissionByLeadReferenceId(res.Item1.ToString());

                if (adm != null)
                {
                    Login login = new Login();
                    login.InsertedDate = DateTime.Now;
                    login.InsertedId = 1;
                    login.ModifiedDate = DateTime.Now;
                    login.ModifiedId = 1;
                    login.Active = true;
                    login.Status = EntityStatus.ACTIVE;
                    login.IsAvailable = true;
                    login.RelatedId = adm.ID;
                    login.RelatedName = RELATED_TYPE.ADMISSION;
                    login.Type = LOGIN_TYPE.ADMISSION_LOGIN;
                    login.Data = Guid.NewGuid().ToString();

                    var loginRes = loginRepo.AddAsync(login).Result;
                    if (HttpContext != null && HttpContext.Session != null)
                    {
                        HttpContext.Session.
                            SetString(LoginSessionData.UNIQUE_ID, login.Data);
                    }

                    var fprmFee = admissionFormPaymentRepository.
                        GetByAdmissionId(adm.ID).Result;

                    if (fprmFee == null)
                    {
                        return RedirectToAction("StudentPaymentMode", "BasicRegistration",
                                new { area = "Admission", Admid = adm.ID });
                    }
                    //else
                    //{

                    //    return RedirectToAction("Profile", "Registration",
                    //            new { area = "Admission", id = encoded });

                    //}
                }
                else
                {
                    return RedirectToAction(nameof(StudentRegistration));
                }
            }
            else if (res.Item2 == "incorrect")
            {
                return RedirectToAction(nameof(RegistrationOtp), new { leadReferenceId = res.Item1.ToString(), OtpID = otpId }).WithDanger("Otp", "is Incorrect");
            }

            else if (res.Item2 == "expired")
            {
                return RedirectToAction(nameof(RegistrationOtp), new { leadReferenceId = res.Item1.ToString(), OtpID = otpId }).WithDanger("Otp", "is Expired");

            }
            return RedirectToAction(nameof(RegistrationOtp), new { leadReferenceId = res.Item1.ToString(), OtpID = otpId });


            //return RedirectToAction(nameof(RegistrationOtp)).WithDanger("Otp", "Wroung");
        }
        #endregion----------------------------------------------------------------------

        #region----------------Student Payment ---------------------
        public IActionResult StudentPaymentMode(long Admid)
        {
            CheckLoginStatus();
            //if (commonMethods.checkaccessavailable("Registrations", accessId, "Create", "Admission", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            OdmErp.ApplicationCore.Entities.Admission admi = admissionRepository.
                    GetAdmissionById(Admid);
            ViewBag.AdmissionId = Admid;
            ViewBag.PaymentType = paymentType.GetAllPaymentType().ToList();
            ViewBag.AdmissionFees = admissionFeeRepository.ListAllAsync().Result.Where(a => a.AcademicSessionId
            == admi.AcademicSessionId).ToList();


            var counsellorlist = admissionCounsellorRepository.ListAllAsyncIncludeAll().Result.Select(a => a.EmployeeId).Distinct();
            var emplist = employeeRepository.GetAllEmployee().Where(b => counsellorlist.Contains(b.ID)).ToList();
            if (userId != 1 && userId != 0)
            {
                emplist = emplist.Where(a => a.ID == userId).ToList();
            }

            ViewBag.EmployeeList = emplist;
            return View();
        }
        [HttpPost]
        public IActionResult SaveStudentPaymentMode()
        {
            CheckLoginStatus();
            long AdmissionId = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            long PaymentModeid = Convert.ToInt64(HttpContext.Request.Form["PaymentModeid"]);
            var TransNumber = HttpContext.Request.Form["TransNumber"];
            var FromNumber = HttpContext.Request.Form["FromNumber"];

            var PaidAmount = Convert.ToInt64(HttpContext.Request.Form["PaidAmount"]);
            DateTime PaidDate = Convert.ToDateTime(HttpContext.Request.Form["PaidDate"]);
            long ReceivedId = Convert.ToInt64(HttpContext.Request.Form["ReceivedId"]);
            if (AdmissionId != 0)
            {

                AdmissionFormPayment payment = new AdmissionFormPayment();
                payment.AdmissionId = AdmissionId;
                payment.PaymentModeId = PaymentModeid;
                payment.TransactionId = TransNumber;
                payment.AdmissionFeeId = PaidAmount;
                payment.PaidDate = PaidDate;
                payment.ReceivedId = ReceivedId;
                payment.Active = true;
                payment.IsAvailable = true;
                payment.Status = EntityStatus.ACTIVE;
                payment.InsertedDate = DateTime.Now;
                payment.ModifiedDate = DateTime.Now;
                payment.InsertedId = userId;
                payment.ModifiedId = userId;
                var data = admissionFormPaymentRepository.AddAsync(payment).Result;


                if (data != null && data.ID > 0)
                {
                    DateTimeOffset utcNow = DateTimeOffset.UtcNow;
                    //string formNumber = utcNow.Year + "" + utcNow.Month
                    //    + utcNow.Day + "" + utcNow.Hour + "" +
                    //   utcNow.Minute + "" + utcNow.Second + "" + utcNow.ToString("ff");



                    OdmErp.ApplicationCore.Entities.Admission admi = admissionRepository.
                        GetAdmissionById(AdmissionId);

                    admi.FormNumber = FromNumber;
                    admi.IsAdmissionFormAmountPaid = true;
                    admi.ModifiedDate = DateTime.Now;
                    admi.Status = Admission_Status.PAID;
                    admi.ModifiedId = userId;
                    admissionRepository.UpdateAdmission(admi);

                    AdmissionTimeline timeline = new AdmissionTimeline();
                    timeline.AdmissionId = data.AdmissionId;
                    timeline.Timeline = "PAID";
                    timeline.Active = true;
                    timeline.IsAvailable = true;
                    timeline.Status = EntityStatus.ACTIVE;
                    timeline.InsertedDate = DateTime.Now;
                    timeline.ModifiedDate = DateTime.Now;
                    timeline.InsertedId = userId;
                    timeline.ModifiedId = userId;
                    admissionRepository.CreateAdmissionTimeline(timeline);

                    var feelist = admissionFeeRepository.GetByIdAsyncIncludeAll(data.AdmissionFeeId).Result;
                    var paymentmode = paymentType.GetPaymentTypeById(data.PaymentModeId);


                    pManager.SendReceiptCard(AdmissionId, payment.PaidDate, feelist.Ammount + "",
                 paymentmode.Name,
                 data.TransactionId);
                    try
                    {
                        LeadCreateDto dto = new LeadCreateDto();

                        dto.Status = "PAID";
                        dto.PaidOn = NullableDateTimeToStringDateTime(payment.InsertedDate)
                            .DateTime;



                        LeadManage leadManage = new LeadManage();
                        LeadResponse lead = leadManage.
                            updateLead(leadManage.UpdateLeadData(dto),
                            admi.MobileNumber);
                        //if (lead != null && lead.Message != null)
                        //{
                        //    adm.LeadId = Guid.Parse(lead.Message.Id);
                        //    admissionRepository.UpdateAdmission(adm);
                        //}

                    }
                    catch (Exception e)
                    {

                    }


                    return RedirectToAction(nameof(ViewStudentAdmissionDetails)).
                        WithSuccess("Registration fee",
                     "Saved successfully");

                }
                else
                {
                    return RedirectToAction(nameof(StudentPaymentMode),
                new { Admid = AdmissionId }).WithSuccess("Registration fee",
                "Failed");
                }
            }
            return RedirectToAction(nameof(StudentPaymentMode),
               new { Admid = AdmissionId });
        }




        #endregion

        #region-----------------------Validate Otp-----------------------------
        private static string GenerateRandomNumber(int length)
        {
            try
            {
                const string valid = "1234567890";
                StringBuilder res = new StringBuilder();
                Random rnd = new Random();
                while (0 < length--)
                {
                    res.Append(valid[rnd.Next(valid.Length)]);
                }
                return res.ToString();
            }
            catch (Exception e1)
            {
                return null;
            }
        }

        [HttpPost]
        public async Task<ActionResult> ValidOtpFromStudentLogin(long otpId, string OTP)
        {

            var res = await ValidateOtpCode(otpId, OTP);
            string encoded = UtilityModel.Encode(res.Item1.ToString());
            if (res.Item2 == "success")
            {



                var adm = admissionRepository.
                    GetAdmissionByLeadReferenceId(res.Item1.ToString());


                if (adm != null)
                {
                    var fprmFee = admissionFormPaymentRepository.
                   GetByAdmissionId(adm.ID).Result;
                    Login login = new Login();
                    login.InsertedDate = DateTime.Now;
                    login.InsertedId = 1;
                    login.ModifiedDate = DateTime.Now;
                    login.ModifiedId = 1;
                    login.Active = true;
                    login.Status = EntityStatus.ACTIVE;
                    login.IsAvailable = true;
                    login.RelatedId = adm.ID;
                    login.RelatedName = RELATED_TYPE.ADMISSION;
                    login.Type = LOGIN_TYPE.ADMISSION_LOGIN;
                    login.Data = Guid.NewGuid().ToString();

                    var loginRes = loginRepo.AddAsync(login).Result;
                    if (HttpContext != null && HttpContext.Session != null)
                    {
                        HttpContext.Session.
                            SetString(LoginSessionData.UNIQUE_ID, login.Data);
                    }



                    if (fprmFee == null)
                    {
                        return Json("/Admission/Student/Payment/" + encoded);
                        //return RedirectToAction("Payment", "Student",
                        //        new { area = "Admission", id = encoded });
                    }
                    else
                    {

                        //return RedirectToAction("Profile", "Registration",
                        //        new { area = "Admission", id = encoded });


                        if (adm.IsAdmissionFormAmountPaid)
                        {
                            if (adm.FormNumber == null)
                            {
                                adm.FormNumber = UtilityModel.GenerateFormNumber(fprmFee.PaidDate);
                                admissionRepository.UpdateAdmission(adm);
                            }
                        }
                        if (adm.Status == "SUBMITTED")
                        {
                            return Json("/Admission/Student/Dashboard/" + encoded);
                        }
                        else
                        {
                            return Json("/Admission/Registration/Profile/" + encoded);
                        }


                        //return RedirectToAction("Profile", "Registration",
                        //        new { area = "Admission", id = encoded });

                    }
                }
                else
                {
                    return Json("/Admission/Student/Register/" + encoded);
                    //return RedirectToAction("Register", "Student",
                    //           new { area = "Admission", id = encoded });
                }
            }

            else
                return Json("/Admission/Student/MobileValidation/" + encoded);
            //return RedirectToAction("MobileValidation", "Student",
            //         new { area = "Admission", id = encoded });


            //return Json(1);
        }

        private async Task<(Guid?, string)> ValidateOtpCode(long otpId, string OTP)
        {
            OtpMessage otpDetails = otpMessageRepo.GetByIdAsync(otpId).Result;
            var admissionid = admissionRepository.GetAllAdmission().
                       Where(a => a.MobileNumber == otpDetails.Mobile).FirstOrDefault();
            if (otpDetails != null && otpDetails.IsAvailable == true &&
                otpDetails.ValidTill > DateTime.Now)
            {

                if (OTP == otpDetails.SentOtpCode)
                {
                    otpDetails.ModifiedDate = DateTime.Now;
                    otpDetails.ModifiedId = 1;
                    otpDetails.IsAvailable = false;

                    otpDetails.RecievedOtpCode = OTP;
                    await otpMessageRepo.UpdateAsync(otpDetails);



                    HttpContext.Session.SetString(LoginSessionData.UNIQUE_ID,
                        Convert.ToString(admissionid.ID));
                    // admis
                    // return Json("The OTP is not matched");

                    return (admissionid.LeadReferenceId, "success");
                    //return RedirectToAction("Payment", "Student",
                    //    new { id = admissionid.ID });
                }
                else
                {

                    return (admissionid.LeadReferenceId, "incorrect");
                }
            }
            else
            {
                return (admissionid.LeadReferenceId, "expired");
            }
        }

        #endregion--------------------------------------------------------------

        #region-------------------------Otp And Send Message----------------
        [HttpGet]
        public async Task<ActionResult> SaveOtpAndSendMessage(string admId)
        {

            //var data=HttpContext.Session.GetInt32(LoginSessionData.UNIQUE_ID);
            //if (data != admId)
            //    return Json(0);

            var admission = admissionRepository.GetAllAdmission().
                Where(a => a.LeadReferenceId == Guid.Parse(admId)).FirstOrDefault();
            if (admission != null)
            {
                string otp = GenerateRandomNumber(6);
                var otpvalue = smssend.SendotpForParent(otp, admission.MobileNumber);
                if (otpvalue == true)
                {
                    OtpMessage objotp = new OtpMessage();
                    objotp.AppType = "WEB";
                    objotp.AccessID = 0;
                    objotp.Mobile = admission.MobileNumber;
                    objotp.SentOtpCode = otp;
                    objotp.Status = "ACTIVE";
                    objotp.ValidTill = DateTime.Now.AddMinutes(10);
                    objotp.OtpType = "ADMISSION_REGISTRATION";
                    objotp.IsAvailable = true;
                    objotp.Status = "ACTIVE";
                    objotp.Active = true;
                    objotp.InsertedDate = DateTime.Now;
                    objotp.InsertedId = 0;
                    objotp.ModifiedDate = DateTime.Now;
                    objotp.ModifiedId = 0;
                    var res = otpMessageRepo.AddAsync(objotp).Result;

                    if (res != null && res.ID > 0)
                    {
                        HttpContext.Session.SetString(LoginSessionData.OTP_ID,
                            Convert.ToString(res.ID));

                        return Json(res.ID);
                    }
                    else
                    {
                        return Json(0);
                    }
                }
                else
                {
                    return Json(0);
                }
            }
            else
            {
                return Json(0);
            }

        }
        #endregion

        #region-------------------------Student Admission Details---------------
        public IActionResult ViewStudentAdmissionDetails(long? AcademicSessionId,
            long? RegistrationSlot,
            long? OraganisationAcademicId,
            long? BoardId, long? ClassId, long? WingId, long? RegistrationSource,
            string FormPaymentStatus,
            string fromDate, string toDate, string formNumber)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations",
                accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ResponseStudentModal res = new ResponseStudentModal();
            var details = admissionSqlQuery.GetView_Student_Admission_List();
            var progresssheet = admissionsqlQuery.GetAdmisionProgresssheet();
            HashSet<long> uniqueStudentIds = new HashSet<long>();
            res.progresssheets = progresssheet;
            #region-------------Councellor Filterration Access -----------------

            List<long> concellor_AdmissionStudentId = null;
            var councellorid = admissionCounsellorRepository.
                ListAllAsyncIncludeAll().Result.
                Where(a => a.EmployeeId == userId).FirstOrDefault();
            if (councellorid != null)
            {
                concellor_AdmissionStudentId = admissionStudentCounsellorRepository.
                    ListAllAsyncIncludeAll().Result.
                    Where(a => a.EmployeeId == councellorid.ID).
                    Select(a => a.AdmissionStudentId).ToList();

                if (concellor_AdmissionStudentId != null)
                {
                    foreach (var a in concellor_AdmissionStudentId)
                    {
                        uniqueStudentIds.Add(a);
                    }
                }
            }
            #endregion



            if (uniqueStudentIds != null && uniqueStudentIds.Count > 0)
            {
                details = details.Where(a => uniqueStudentIds.
                Contains(a.AdmissionStudentId) || a.StudentCounselorId == null).ToList();
            }

            if (AcademicSessionId != null && AcademicSessionId > 0)
            {
                res.AcademicSessionId = AcademicSessionId;
                details = details.Where(a => a.AcademicSessionId == AcademicSessionId).ToList();
            }
            if (OraganisationAcademicId != null && OraganisationAcademicId > 0)
            {
                res.OraganisationAcademicId = OraganisationAcademicId;
                details = details.Where(a => a.OrganisationAcademicId == OraganisationAcademicId).ToList();
            }

            if (BoardId != null && BoardId > 0)
            {
                res.BoardId = BoardId;
                details = details.Where(a => a.BoardId == BoardId).ToList();
            }
            if (ClassId != null && ClassId > 0)
            {
                res.AcademicStandardId = ClassId;
                details = details.Where(a => a.StandardId == ClassId).ToList();
            }

            if (WingId != null && WingId > 0)
            {
                res.WingId = WingId;
                details = details.Where(a => a.WingId == WingId).ToList();
            }

            if (RegistrationSlot != null && RegistrationSlot > 0)
            {
                res.RegistrationSlot = RegistrationSlot;
                details = details.Where(a => a.AdmissionSlotId == RegistrationSlot).ToList();
            }

            if (RegistrationSource != null && RegistrationSource > 0)
            {
                res.RegistrationSource = RegistrationSource;
                details = details.Where(a => a.AdmissionSourceId == RegistrationSource).ToList();
            }
            res.FormPaymentStatus = FormPaymentStatus;
            if (FormPaymentStatus == null)
            {
                //nothing
            }
            else
                if (FormPaymentStatus == "Paid")
            {
                details = details.Where(a => a.FormPaymentId > 0).ToList();
            }
            else
                if (FormPaymentStatus == "Not Paid")
            {
                details = details.Where(a => a.FormPaymentId == null).ToList();
            }

            if (formNumber != null && formNumber.Trim().Length > 0)
            {
                details = details.Where(a => formNumber.Contains(a.FormNumber)).ToList();
            }

            if (fromDate != null)
            {
                DateTime FromDate = Convert.ToDateTime(fromDate + " 00:00:00 AM");
                DateTime ToDate = System.DateTime.Now;
                if (toDate != null)
                {
                    ToDate = Convert.ToDateTime(toDate + " 23:59:59 PM");
                }
                if (fromDate != null && toDate != null)
                {
                    details = details.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                }
                if (fromDate != null && toDate == null)
                {
                    details = details.Where(a => a.InsertedDate >= FromDate).ToList();
                }
            }

            TempData["FromDate"] = fromDate;
            TempData["ToDate"] = toDate;
            res.BasicStudentDetailsModel = details;

            commonCls paid = new commonCls();
            paid.name = "Paid";

            commonCls paidNo = new commonCls();
            paidNo.name = "Not Paid";

            res.FormPaymentStatusList = new List<commonCls>();
            res.FormPaymentStatusList.Add(paidNo);
            res.FormPaymentStatusList.Add(paid);
            // counselor employee list

            var counselloremplist = admissionCounsellorRepository.ListAllAsync()
                .Result.Where(a => a.Active == true && (a.EndDate != null ? (a.EndDate <= DateTime.Now) : true)).ToList();
            var emplist = employeeRepository.GetAllEmployee();

            res.EmployeeList = (from a in counselloremplist
                                join b in emplist on a.EmployeeId equals b.ID
                                select new Employee
                                {
                                    ID = a.ID,
                                    FirstName = b.FirstName,
                                    LastName = b.LastName,
                                    EmpCode = b.EmpCode
                                }).ToList();

            res.SourceList = admissionRepository.GetAllAdmissionSource().ToList();
            res.AcademicSessionList = academicSessionRepository.ListAllAsync().
                Result.Where(a => a.Active == true && a.IsAdmission == true).ToList();
            res.regdaccess = GetRegdAdmissionAccessDetails();
            HttpContext.Session.SetString("admissionlist", JsonConvert.SerializeObject(details));

            return View(res);
        }


        public async Task<IActionResult> DownloadStudentRegistration()
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations",
                accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            List<View_All_Admission> studentlist = new List<View_All_Admission>();
            if (HttpContext.Session.GetString("admissionlist") != null)
            {
                var attach = HttpContext.Session.GetString("admissionlist");
                studentlist = JsonConvert.DeserializeObject<List<View_All_Admission>>(attach);
            }
            XLWorkbook oWB = new XLWorkbook();
            var progresssheet = admissionsqlQuery.GetAdmisionProgresssheet();
            var addresslist = admissionStudentAddressRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();
            DataTable validationTable = new DataTable();
            #region Columns

            validationTable.Columns.Add("Session");
            validationTable.Columns.Add("Application No");
            validationTable.Columns.Add("Source");
            validationTable.Columns.Add("Full Name");
            validationTable.Columns.Add("Emergency Contact Person");
            validationTable.Columns.Add("Organisation");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Wing");
            validationTable.Columns.Add("Mobile");
            validationTable.Columns.Add("Email");
            validationTable.Columns.Add("Present Address");
            validationTable.Columns.Add("Permanent Address");
            validationTable.Columns.Add("Payment Status");
            validationTable.Columns.Add("Paid Date");
            validationTable.Columns.Add("Paid Amount");
            validationTable.Columns.Add("Payment Mode");
            validationTable.Columns.Add("Transaction ID");
            validationTable.Columns.Add("Received Payment");
            validationTable.Columns.Add("Status");
            validationTable.Columns.Add("Counsellor");
            validationTable.Columns.Add("Progress");
            validationTable.Columns.Add("Kit Send Status");

            #endregion
            #region rows
            if (studentlist != null)
            {

                foreach (var item in studentlist)
                {
                    var adrs = addresslist.Where(a => a.AdmissionStudentId == item.AdmissionStudentId).ToList();
                    DataRow dr = validationTable.NewRow();
                    dr["Session"] = item.AcademicSessionName;
                    dr["Application No"] = item.FormNumber;
                    dr["Source"] = item.AcademicSourceName;
                    dr["Full Name"] = item.Fullname;
                    dr["Emergency Contact Person"] = item.EmergencyName;
                    dr["Organisation"] = item.OrganisationName;
                    dr["Board"] = item.BoardName;
                    dr["Class"] = item.StandardName;
                    dr["Wing"] = item.WingName;
                    dr["Mobile"] = item.Mobile;
                    dr["Email"] = item.EmergencyEmail;
                    dr["Present Address"] = addresslist.Count > 0 ? addresslist.Where(a => a.AddressType == "present" && a.AdmissionStudentId==item.AdmissionStudentId).Select(a => new { address = a.AddressLine1 + (a.AddressLine2 != "" ? (", " + a.AddressLine2) : "") + a.CountryId + ", " + a.StateId + ", " + a.CityId + ", " + a.PostalCode }).FirstOrDefault().address : "";
                    dr["Permanent Address"] = addresslist.Count > 0 ? addresslist.Where(a => a.AddressType == "permanent" && a.AdmissionStudentId == item.AdmissionStudentId).Select(a => new { address = a.AddressLine1 + (a.AddressLine2 != "" ? (", " + a.AddressLine2) : "") + a.CountryId + ", " + a.StateId + ", " + a.CityId + ", " + a.PostalCode }).FirstOrDefault().address : "";
                    dr["Payment Status"] = (item.FormPaymentId != null && item.FormPaymentId != 0) ? "Paid" : "Not Paid";
                    dr["Paid Date"] = item.PaidDate;
                    dr["Paid Amount"] = item.PaymentAmount;
                    dr["Payment Mode"] = item.PaymentModeName;
                    dr["Transaction ID"] = item.TransactionId;
                    dr["Received Payment"] = item.ReceivedPaymentBy;
                    dr["Status"] = item.Status;
                    dr["Counsellor"] = item.StudentCounselorName;
                    var count = progresssheet.Where(m => m.ID == item.ID).Select(m => new { total = (m.parent + m.academic + m.addresses + m.contact + m.declaration + m.personal + m.proficiency + m.reference + m.standarddet + m.transport) * 10 }).FirstOrDefault().total;
                    dr["Progress"] = count + "%";
                    dr["Kit Send Status"] = item.KitStatus+(item.KitStatusDate== null?"":(" on " +item.KitStatusDate.Value.ToString("dd-MM-yyyy")));


                    validationTable.Rows.Add(dr);
                }
            }
            #endregion
            validationTable.TableName = "Admission Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"AdmissionStudentList.xlsx");







        }
        #endregion------------------------------------------------------------------------

        #region-------------------- Student PI Document----------------------------
        public IActionResult DownloadPIData(long sessionid, long org, long board, long standardid, long wingid)
        {
            XLWorkbook oWB = new XLWorkbook();
            DataTable genderdt = new DataTable();
            genderdt.Columns.Add("Answer");
            DataRow dr2 = genderdt.NewRow();
            dr2["Answer"] = "Yes";
            genderdt.Rows.Add(dr2);
            DataRow dr1 = genderdt.NewRow();
            dr1["Answer"] = "No";
            genderdt.Rows.Add(dr1);
            genderdt.TableName = "Answer";

            int lastCellNo1 = genderdt.Rows.Count + 1;

            oWB.AddWorksheet(genderdt);


            var worksheet1 = oWB.Worksheet(1);

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Session");
            validationTable.Columns.Add("Organisation");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Standard");
            validationTable.Columns.Add("Wing");
            validationTable.Columns.Add("Form Number");
            validationTable.Columns.Add("Student Name");
            validationTable.Columns.Add("Appeared");
            validationTable.Columns.Add("Appeared Date(dd-MM-yyyy)");
            validationTable.Columns.Add("Qualified");
            validationTable.Columns.Add("Score");


            var admissionformfees = admissionFormPaymentRepository.ListAllAsync().Result.Select(a => a.AdmissionId).ToList();
            var admissiondetails = admissionsqlQuery.GetView_Student_Admission_List().Where(a => admissionformfees.Contains(a.ID) && a.AcademicSessionId == sessionid && a.OrganisationAcademicId == org && a.BoardId == board && a.BoardStandardId == standardid && a.AcademicStandardWingId == wingid).ToList();


            var admissionstudentinterview = admissionStudentInterviewRepository.ListAllAsync().Result;

            foreach (var ob in admissiondetails)
            {
                DataRow dr = validationTable.NewRow();
                dr["Session"] = ob.AcademicSessionName;
                dr["Organisation"] = ob.OrganisationName;
                dr["Board"] = ob.BoardName;
                dr["Standard"] = ob.StandardName;
                dr["Wing"] = ob.WingName;
                dr["Form Number"] = ob.FormNumber;
                dr["Student Name"] = ob.Fullname;
                dr["Appeared"] = admissionstudentinterview != null ? (admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).FirstOrDefault() == null ? "" : admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).Select(a => new { sts = a.IsAppeared == true ? "Yes" : "No" }).FirstOrDefault().sts) : "";
                dr["Qualified"] = admissionstudentinterview != null ? (admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).FirstOrDefault() == null ? "" : admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).Select(a => new { sts = a.IsQualified == true ? "Yes" : "No" }).FirstOrDefault().sts) : "";
                dr["Score"] = admissionstudentinterview != null ? (admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).FirstOrDefault() == null ? "" : admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).Select(a => new { sts = a.Score }).FirstOrDefault().sts) : "";
                dr["Appeared Date(dd-MM-yyyy)"] = admissionstudentinterview != null ? (admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).FirstOrDefault() == null ? "" : admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).Select(a => new { sts = a.AppearedDate.Value.ToString("dd-MM-yyyy") }).FirstOrDefault().sts) : "";
                validationTable.Rows.Add(dr);
            }

            validationTable.TableName = "Students PI Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(8).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(8).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(8).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(8).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(8).SetDataValidation().ErrorMessage = "Please Select From The List";


            worksheet.Column(10).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(10).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(10).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(10).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(10).SetDataValidation().ErrorMessage = "Please Select From The List";


            worksheet1.Hide();

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StudentPIDetails.xlsx");

        }
        public IActionResult UploadPIData(IFormFile fileexcel)
        {
            CheckLoginStatus();
            using (ExcelPackage excelPackage = new ExcelPackage(fileexcel.OpenReadStream()))
            {
                ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                int totalRows = firstWorksheet.Dimension.Rows;
                var admissiondetails = admissionsqlQuery.GetView_Student_Admission_Counsellers().ToList();

                for (int i = 2; i <= totalRows; i++)
                {
                    if (firstWorksheet.Cells[i, 6].Value != null)
                    {
                        string studentcode = firstWorksheet.Cells[i, 6].Value.ToString();
                        var studentdt = admissiondetails.Where(a => a.FormNumber == studentcode).FirstOrDefault();
                        if (studentdt != null)
                        {
                            AdmissionStudentInterview ob = new AdmissionStudentInterview();
                            ob.Active = true;
                            ob.AdmissionStudentId = studentdt.AcademicStandardId;
                            ob.AppearedDate = Convert.ToDateTime(firstWorksheet.Cells[i, 9].Value.ToString());
                            ob.InsertedDate = DateTime.Now;
                            ob.ModifiedDate = DateTime.Now;
                            ob.InsertedId = userId;
                            ob.ModifiedId = userId;
                            ob.Score = firstWorksheet.Cells[i, 11].Value.ToString();
                            ob.IsAppeared = firstWorksheet.Cells[i, 8].Value.ToString() == "Yes" ? true : false;
                            ob.IsAvailable = true;
                            ob.IsQualified = firstWorksheet.Cells[i, 10].Value.ToString() == "Yes" ? true : false;
                            ob.Status = "ACTIVE";
                            var m = admissionStudentInterviewRepository.AddAsync(ob).Result;
                        }
                    }
                }
                return RedirectToAction(nameof(ViewStudentAdmissionDetails));
            }
        }

        #endregion

        #region--------------- Student Document------------------------
        public IActionResult DownloadDocumentData(long sessionid, long org, long board, long standardid, long wingid)
        {
            XLWorkbook oWB = new XLWorkbook();
            DataTable genderdt = new DataTable();
            genderdt.Columns.Add("Answer");
            DataRow dr2 = genderdt.NewRow();
            dr2["Answer"] = "Yes";
            genderdt.Rows.Add(dr2);
            DataRow dr1 = genderdt.NewRow();
            dr1["Answer"] = "No";
            genderdt.Rows.Add(dr1);
            genderdt.TableName = "Answer";

            int lastCellNo1 = genderdt.Rows.Count + 1;

            oWB.AddWorksheet(genderdt);


            var worksheet1 = oWB.Worksheet(1);

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Session");
            validationTable.Columns.Add("Organisation");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Standard");
            validationTable.Columns.Add("Wing");
            validationTable.Columns.Add("Form Number");
            validationTable.Columns.Add("Student Name");
            validationTable.Columns.Add("Appeared");
            validationTable.Columns.Add("Appeared Date(dd-MM-yyyy)");
            validationTable.Columns.Add("Qualified");


            var admissionformfees = admissionFormPaymentRepository.ListAllAsync().Result.Select(a => a.AdmissionId).ToList();
            var admissiondetails = admissionsqlQuery.GetView_Student_Admission_List().Where(a => admissionformfees.Contains(a.ID) && a.AcademicSessionId == sessionid && a.OrganisationAcademicId == org && a.BoardId == board && a.BoardStandardId == standardid && a.AcademicStandardWingId == wingid).ToList();


            var admissionstudentinterview = admissionDocumentVerificationAssignRepository.ListAllAsync().Result;

            foreach (var ob in admissiondetails)
            {
                DataRow dr = validationTable.NewRow();
                dr["Session"] = ob.AcademicSessionName;
                dr["Organisation"] = ob.OrganisationName;
                dr["Board"] = ob.BoardName;
                dr["Standard"] = ob.StandardName;
                dr["Wing"] = ob.WingName;
                dr["Form Number"] = ob.FormNumber;
                dr["Student Name"] = ob.Fullname;
                dr["Appeared"] = admissionstudentinterview != null ? (admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).FirstOrDefault() == null ? "" : admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).Select(a => new { sts = a.IsAppeared == true ? "Yes" : "No" }).FirstOrDefault().sts) : "";
                dr["Qualified"] = admissionstudentinterview != null ? (admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).FirstOrDefault() == null ? "" : admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).Select(a => new { sts = a.IsQualified == true ? "Yes" : "No" }).FirstOrDefault().sts) : "";
                dr["Appeared Date(dd-MM-yyyy)"] = admissionstudentinterview != null ? (admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).FirstOrDefault() == null ? "" : admissionstudentinterview.Where(a => a.AdmissionStudentId == ob.AcademicStudentID).Select(a => new { sts = a.AppearedDateTime.Value.ToString("dd-MM-yyyy") }).FirstOrDefault().sts) : "";
                validationTable.Rows.Add(dr);
            }

            validationTable.TableName = "Students Document Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(8).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(8).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(8).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(8).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(8).SetDataValidation().ErrorMessage = "Please Select From The List";


            worksheet.Column(10).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(10).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(10).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(10).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(10).SetDataValidation().ErrorMessage = "Please Select From The List";


            worksheet1.Hide();

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StudentDocumentDetails.xlsx");

        }
        public IActionResult UploadDocumentData(IFormFile docfileexcel)
        {
            CheckLoginStatus();
            using (ExcelPackage excelPackage = new ExcelPackage(docfileexcel.OpenReadStream()))
            {
                ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                int totalRows = firstWorksheet.Dimension.Rows;
                var admissiondetails = admissionsqlQuery.GetView_Student_Admission_Counsellers().ToList();

                for (int i = 2; i <= totalRows; i++)
                {
                    if (firstWorksheet.Cells[i, 6].Value != null)
                    {
                        string studentcode = firstWorksheet.Cells[i, 6].Value.ToString();
                        var studentdt = admissiondetails.Where(a => a.FormNumber == studentcode).FirstOrDefault();
                        if (studentdt != null)
                        {
                            AdmissionDocumentVerificationAssign ob = new AdmissionDocumentVerificationAssign();
                            ob.Active = true;
                            ob.AdmissionStudentId = studentdt.AcademicStandardId;
                            ob.AppearedDateTime = Convert.ToDateTime(firstWorksheet.Cells[i, 9].Value.ToString());
                            ob.InsertedDate = DateTime.Now;
                            ob.ModifiedDate = DateTime.Now;
                            ob.InsertedId = userId;
                            ob.ModifiedId = userId;
                            ob.IsAppeared = firstWorksheet.Cells[i, 8].Value.ToString() == "Yes" ? true : false;
                            ob.IsAvailable = true;
                            ob.IsQualified = firstWorksheet.Cells[i, 10].Value.ToString() == "Yes" ? true : false;
                            ob.Status = "ACTIVE";
                            var m = admissionDocumentVerificationAssignRepository.AddAsync(ob).Result;
                        }
                    }
                }
                return RedirectToAction(nameof(ViewStudentAdmissionDetails));
            }
        }

        #endregion

        #region-------------------------------Assign Counselor-------------------
        [HttpPost]
        public async Task<IActionResult> SaveAssignCounselorToStudent(long EmployeeId, string Remarks, string ArrayStudentId)
        {
            CheckLoginStatus();
            string[] allstudent = ArrayStudentId.Split(',');
            ArrayList arlist = new ArrayList(allstudent);
            foreach (var studentid in arlist)
            {
                AdmissionStudentCounselor counselor = new AdmissionStudentCounselor();
                counselor.Active = true;
                counselor.IsAvailable = true;
                counselor.Status = EntityStatus.ACTIVE;
                counselor.InsertedDate = DateTime.Now;
                counselor.ModifiedDate = DateTime.Now;
                counselor.InsertedId = userId;
                counselor.ModifiedId = userId;
                counselor.EmployeeId = EmployeeId;
                counselor.AdmissionStudentId = Convert.ToInt64(studentid);
                counselor.AssignedDate = DateTime.Now;
                counselor.Remarks = Remarks;
                await admissionStudentCounselorRepository.AddAsync(counselor);
            }
            return RedirectToAction(nameof(ViewStudentAdmissionDetails)).WithSuccess("Assign Counselor", "Saved Successfully");
        }
        #endregion----------------------------------------------------------------------

        #region----------------------------Change AssignCounselor--------------
        [HttpPost]
        public async Task<IActionResult> ChangeAssignCounselorToStudent(long ChangeEmployeeId, string Remarks, string ChangeCounStudentId)
        {
            CheckLoginStatus();
            string[] allstudent = ChangeCounStudentId.Split(',');
            ArrayList arlist = new ArrayList(allstudent);
            foreach (var studentid in arlist)
            {
                AdmissionStudentCounselor counselor = admissionStudentCounselorRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == Convert.ToInt64(studentid)).FirstOrDefault();
                counselor.ModifiedDate = DateTime.Now;
                counselor.ModifiedId = userId;
                counselor.EmployeeId = ChangeEmployeeId;
                counselor.AdmissionStudentId = Convert.ToInt64(studentid);
                counselor.AssignedDate = DateTime.Now;
                counselor.Remarks = Remarks;
                await admissionStudentCounselorRepository.UpdateAsync(counselor);
            }
            return RedirectToAction(nameof(ViewStudentAdmissionDetails)).WithSuccess("Counselor", "Update Successfully");
        }
        #endregion---------------------------------------------------------

        #region-----------------Delete Counselor----------------------------
        public JsonResult DeleteCounselorByStudentId(long[] id)
        {
            int data = 0;
            try
            {
                CheckLoginStatus();
                foreach (long studentid in id)
                {
                    AdmissionStudentCounselor counselor = admissionStudentCounselorRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == studentid).FirstOrDefault();
                    if (counselor != null)
                    {
                        counselor.Active = false;
                        counselor.IsAvailable = false;
                        counselor.Status = EntityStatus.INACTIVE;
                        counselor.ModifiedDate = DateTime.Now;
                        counselor.ModifiedId = userId;
                        counselor.AssignedDate = DateTime.Now;
                        admissionStudentCounselorRepository.UpdateAsync(counselor);
                    }
                }
                data = 1;
            }
            catch (Exception ex)
            {
                data = 0;
            }

            return Json(data);

            // return RedirectToAction(nameof(ViewStudentAdmissionDetails)).WithSuccess("Counselor", "Update Successfully");
        }
        #endregion---------------------------------------------------------------

        #region-----------------Change Or RemoveCounselor-----------------
        [HttpPost]
        public async Task<IActionResult> ChangeOrRemoveCounselorToStudent(long RemoveEmployeeId, string StatusId, long AdmissionStudentId)
        {
            CheckLoginStatus();

            AdmissionStudentCounselor counselor = admissionStudentCounselorRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();
            if (counselor != null)
            {
                counselor.ModifiedDate = DateTime.Now;
                counselor.ModifiedId = userId;
                counselor.EmployeeId = RemoveEmployeeId;
                counselor.AssignedDate = DateTime.Now;
                if (StatusId == "ACTIVE")
                {
                    counselor.Status = EntityStatus.ACTIVE;
                    counselor.IsAvailable = true;
                    counselor.Active = true;
                }
                else
                {
                    counselor.Status = EntityStatus.INACTIVE;
                    counselor.IsAvailable = false;
                    counselor.Active = false;
                }

                await admissionStudentCounselorRepository.UpdateAsync(counselor);
            }
            else
            {
                AdmissionStudentCounselor counselor1 = new AdmissionStudentCounselor();
                counselor1.Active = true;
                counselor1.IsAvailable = true;
                counselor1.Status = EntityStatus.ACTIVE;
                counselor1.InsertedDate = DateTime.Now;
                counselor1.ModifiedDate = DateTime.Now;
                counselor1.InsertedId = userId;
                counselor1.ModifiedId = userId;
                counselor1.EmployeeId = RemoveEmployeeId;
                counselor1.AdmissionStudentId = AdmissionStudentId;
                counselor1.AssignedDate = DateTime.Now;
                counselor1.Remarks = "";
                await admissionStudentCounselorRepository.AddAsync(counselor1);
            }
            return RedirectToAction(nameof(ViewStudentAdmissionDetails)).WithSuccess("Counselor", "Update Successfully");

        }
        #endregion---------------------------------------------------------------

        #region-------------Assign ExamDate ToStudent----------------------------
        [HttpPost]
        public async Task<IActionResult> SaveAssignExamDateToStudent(Nullable<DateTime> ExamDate, string ArrayStudentId)
        {
            CheckLoginStatus();
            string[] allstudent = ArrayStudentId.Split(',');
            ArrayList arlist = new ArrayList(allstudent);
            foreach (var studentid in arlist)
            {
                AdmissionStudentExam counselor = new AdmissionStudentExam();
                counselor.Active = true;
                counselor.IsAvailable = true;
                counselor.Status = EntityStatus.ACTIVE;
                counselor.InsertedDate = DateTime.Now;
                counselor.ModifiedDate = DateTime.Now;
                counselor.InsertedId = userId;
                counselor.ModifiedId = userId;
                counselor.ExamDate = ExamDate;
                counselor.AdmissionStudentId = Convert.ToInt64(studentid);
                await admissionStudentExamRepository.AddAsync(counselor);
            }
            return RedirectToAction(nameof(ViewStudentAdmissionDetails)).WithSuccess("Assign Exam Date", "Saved Successfully");
        }
        #endregion

        #region-----------------Download And Upload Excel---------------------
        public IActionResult DownloadStudentRegistrationSample()
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            XLWorkbook oWB = new XLWorkbook();
            var ProfessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true).ToList();
            var SlotList = admissionRepository.GetAllAdmissionSlot().ToList();
            var SourceList = admissionRepository.GetAllAdmissionSource().ToList();
            var PaymentList = paymentType.GetAllPaymentType().ToList();
            var EmployeeList = employeeRepository.GetAllEmployee().ToList();
            int i = 1;
            var wsData = oWB.Worksheets.Add("Data");

            wsData.Cell(1, 2).Value = "Academic Session";
            i = 1;
            foreach (var a in ProfessionList)
            {
                wsData.Cell(++i, 2).Value = "C_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
            }
            wsData.Range("B2:B" + i).AddToNamed("Academic Session");



            //class and wing
            int j = 2;
            foreach (var a in ProfessionList)
            {
                wsData.Cell(1, ++j).Value = "C_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                int k = 1;
                foreach (var b in GetClassListByAcademicSession(a.ID))
                {
                    wsData.Cell(++k, j).Value = "S_" + b.boardStandardId + "_" + b.boardname + "_" + b.boardClass.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                }
                wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("C_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
            }

            wsData.Cell(1, 5).Value = "Slot";
            int z = 1;
            foreach (var a in SlotList)
            {
                wsData.Cell(++z, 5).Value = "S_" + a.ID + "_" + a.SlotName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
            }
            wsData.Range("E2:E" + z).AddToNamed("Slot");

            wsData.Cell(1, 6).Value = "Source";
            int x = 1;
            foreach (var a in SourceList)
            {
                wsData.Cell(++x, 6).Value = "S_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
            }
            wsData.Range("F2:F" + x).AddToNamed("Source");


            wsData.Cell(1, 12).Value = "Payment Type";
            int h = 1;
            foreach (var a in PaymentList)
            {
                wsData.Cell(++h, 12).Value = "P_" + a.ID + "_" + a.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
            }
            wsData.Range("L2:L" + h).AddToNamed("Payment Type");


            wsData.Cell(1, 16).Value = "ReceivedBy";
            int v = 1;
            foreach (var a in EmployeeList)
            {
                wsData.Cell(++v, 16).Value = "E_" + a.ID + "_" + a.FirstName + "_" + a.LastName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
            }

            wsData.Range("P2:P" + v).AddToNamed("ReceivedBy");

            j = 16;
            foreach (var m in ProfessionList)
            {
                foreach (var a in GetClassListByAcademicSession(m.ID))
                {
                    wsData.Cell(1, ++j).Value = "S_" + a.boardStandardId + "_" + a.boardname + "_" + a.boardClass.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    int k = 1;
                    foreach (var b in GetWingListByBoardStandardId(a.boardStandardId))
                    {
                        wsData.Cell(++k, j).Value = "Y_" + b.AcademicStandardWindId + "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
                    }
                    wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("S_" + a.boardStandardId + "_" + a.boardname + "_" + a.boardClass.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
                }
            }
            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("Form Number");
            validationTable.Columns.Add("Academic Session");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Wing");
            //validationTable.Columns.Add("Slot");
            validationTable.Columns.Add("Source");

            validationTable.Columns.Add("FirstName");
            validationTable.Columns.Add("LastName");
            validationTable.Columns.Add("EmergencyContactPersonName");
            validationTable.Columns.Add("EmergencyContact");
            validationTable.Columns.Add("Email");
            validationTable.Columns.Add("Payment Type");
            validationTable.Columns.Add("TransactionNo");
            validationTable.Columns.Add("ReceivedAmmount");
            validationTable.Columns.Add("ReceivedDate");
            validationTable.Columns.Add("ReceivedBy");
            validationTable.TableName = "BasicRegistration_Details";

            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(2).SetDataValidation().List(wsData.Range("Academic Session"), true);
            worksheet.Column(2).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(2).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(2).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(2).SetDataValidation().ErrorMessage = "Please Select Session ";

            worksheet.Column(3).SetDataValidation().InCellDropdown = true;
            worksheet.Column(3).SetDataValidation().Operator = XLOperator.Between;
            worksheet.Column(3).SetDataValidation().AllowedValues = XLAllowedValues.List;
            worksheet.Column(3).SetDataValidation().List("=INDIRECT(SUBSTITUTE(B1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
            worksheet.Column(3).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(3).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(3).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(3).SetDataValidation().ErrorMessage = "Please Select Class";


            worksheet.Column(4).SetDataValidation().InCellDropdown = true;
            worksheet.Column(4).SetDataValidation().Operator = XLOperator.Between;
            worksheet.Column(4).SetDataValidation().AllowedValues = XLAllowedValues.List;
            worksheet.Column(4).SetDataValidation().List("=INDIRECT(SUBSTITUTE(C1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
            worksheet.Column(4).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(4).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(4).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(4).SetDataValidation().ErrorMessage = "Please Select Wing";


            //worksheet.Column(5).SetDataValidation().List(wsData.Range("Slot"), true);
            //worksheet.Column(5).SetDataValidation().ShowErrorMessage = true;
            //worksheet.Column(5).SetDataValidation().ErrorTitle = "Warning";
            //worksheet.Column(5).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            //worksheet.Column(5).SetDataValidation().ErrorMessage = "Please Select Slot  List";

            worksheet.Column(5).SetDataValidation().List(wsData.Range("Source"), true);
            worksheet.Column(5).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(5).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(5).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(5).SetDataValidation().ErrorMessage = "Please Select Source";

            worksheet.Column(15).SetDataValidation().List(wsData.Range("ReceivedBy"), true);
            worksheet.Column(15).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(15).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(15).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(15).SetDataValidation().ErrorMessage = "Please Select Source";

            worksheet.Column(11).SetDataValidation().List(wsData.Range("Payment Type"), true);
            worksheet.Column(11).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(11).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(11).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(11).SetDataValidation().ErrorMessage = "Please Select Source";

            worksheet.Column(6).SetDataValidation().IgnoreBlanks = true;
            worksheet.Column(6).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(6).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(6).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(6).SetDataValidation().ErrorMessage = "Please Enter  FirstName";

            worksheet.Column(7).SetDataValidation().IgnoreBlanks = true;
            worksheet.Column(7).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(7).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(7).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(7).SetDataValidation().ErrorMessage = "Please Enter  LastName";


            worksheet.Column(1).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(1).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(1).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(1).SetDataValidation().ErrorMessage = "Please Enter  Form Number";

            worksheet.Column(8).SetDataValidation().IgnoreBlanks = true;
            worksheet.Column(8).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(8).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(8).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(8).SetDataValidation().ErrorMessage = "Please Enter Emergency Person Name";

            worksheet.Column(9).SetDataValidation().TextLength.EqualTo(10);
            worksheet.Column(9).SetDataValidation().IgnoreBlanks = true;
            worksheet.Column(9).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(9).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(9).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(9).SetDataValidation().ErrorMessage = "Please Enter Emergency Contact Number";


            worksheet.Column(10).SetDataValidation().IgnoreBlanks = true;
            worksheet.Column(10).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(10).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(10).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(10).SetDataValidation().ErrorMessage = "Please Enter Emergency Person Email Id";

            worksheet.Column(12).SetDataValidation().IgnoreBlanks = true;
            worksheet.Column(12).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(12).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(12).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(12).SetDataValidation().ErrorMessage = "Please Enter Transaction Number";

            worksheet.Column(13).SetDataValidation().IgnoreBlanks = true;
            worksheet.Column(13).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(13).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(13).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(13).SetDataValidation().ErrorMessage = "Please Enter Received Ammount";


            worksheet.Column(14).SetDataValidation().Operator = XLOperator.Between;
            worksheet.Column(14).SetDataValidation().AllowedValues = XLAllowedValues.Date;
            worksheet.Column(14).SetDataValidation().MinValue = "E1";
            worksheet.Column(14).SetDataValidation().MaxValue = "E1000";
            worksheet.Column(14).SetDataValidation().ShowErrorMessage = true;
            worksheet.Column(14).SetDataValidation().ErrorTitle = "Warning";
            worksheet.Column(14).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            worksheet.Column(14).SetDataValidation().ErrorMessage = "Please Enter a Valid Received Date";



            //worksheet.Column(8).SetDataValidation().List(wsData.Range("LastName"), true);
            //worksheet.Column(8).SetDataValidation().ShowErrorMessage = true;
            //worksheet.Column(8).SetDataValidation().ErrorTitle = "Warning";
            //worksheet.Column(8).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
            //worksheet.Column(8).SetDataValidation().ErrorMessage = "Please Enter LastName";
            //wsData.Hide();
            //wsData1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"RegistrationSample.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadViewStudentAdmissionDetails(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[2];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            string formnumber = firstWorksheet.Cells[i, 1].Value.ToString();
                            //Nullable<DateTime> startdate = Convert.ToDateTime(firstWorksheet.Cells[i, 3].Value);
                            var slotlist = admissionRepository.GetAllAdmission().Where(b => b.FormNumber == formnumber.ToUpper()).FirstOrDefault();
                            if (slotlist == null)
                            {
                                OdmErp.ApplicationCore.Entities.Admission adm = new OdmErp.ApplicationCore.Entities.Admission();
                                adm.AcademicSessionId = Convert.ToInt64(firstWorksheet.Cells[i, 2].Value.ToString().Split('_')[1]);
                                adm.AdmissionSlotId = Convert.ToInt64(firstWorksheet.Cells[i, 5].Value.ToString().Split('_')[1]);
                                adm.AcademicSourceId = Convert.ToInt64(firstWorksheet.Cells[i, 6].Value.ToString().Split('_')[1]);
                                adm.MobileNumber = firstWorksheet.Cells[i, 10].Value.ToString();
                                adm.Active = true;
                                adm.IsAvailable = true;
                                adm.Status = EntityStatus.ACTIVE;
                                adm.InsertedDate = DateTime.Now;
                                adm.ModifiedDate = DateTime.Now;
                                adm.InsertedId = userId;
                                adm.ModifiedId = userId;
                                adm.FormNumber = firstWorksheet.Cells[i, 1].Value.ToString();
                                admissionRepository.CreateAdmission(adm);
                                long addmisionid = adm.ID;

                                AdmissionStudent student = new AdmissionStudent();
                                student.AdmissionId = addmisionid;
                                student.FirstName = firstWorksheet.Cells[i, 7].Value.ToString();
                                student.LastName = firstWorksheet.Cells[i, 8].Value.ToString();
                                student.DateOfBirth = null;
                                student.Active = true;
                                student.IsAvailable = true;
                                student.Status = EntityStatus.ACTIVE;
                                student.InsertedDate = DateTime.Now;
                                student.ModifiedDate = DateTime.Now;
                                student.InsertedId = userId;
                                student.ModifiedId = userId;
                                long addmisionstudentid = admissionStudentRepository.AddAsync(student).Result.ID;

                                AdmissionStudentContact stucontact = new AdmissionStudentContact();
                                stucontact.AdmissionStudentId = addmisionstudentid;
                                stucontact.FullName = firstWorksheet.Cells[i, 9].Value.ToString();
                                stucontact.Mobile = firstWorksheet.Cells[i, 10].Value.ToString();
                                stucontact.EmailId = firstWorksheet.Cells[i, 11].Value.ToString();
                                stucontact.Active = true;
                                stucontact.IsAvailable = true;
                                stucontact.Status = EntityStatus.ACTIVE;
                                stucontact.InsertedDate = DateTime.Now;
                                stucontact.ModifiedDate = DateTime.Now;
                                stucontact.InsertedId = userId;
                                stucontact.ModifiedId = userId;
                                await admissionStudentContactRepository.AddAsync(stucontact);

                                AdmissionStudentStandard studentstandard = new AdmissionStudentStandard();
                                //studentstandard.AcademicStandardWindId = Convert.ToInt64(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]).ToString()!= null? Convert.ToInt64(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]):0;
                                studentstandard.AcademicStandardWindId = 0;
                                studentstandard.AdmissionStudentId = addmisionstudentid;
                                studentstandard.Active = true;
                                studentstandard.IsAvailable = true;
                                studentstandard.Status = EntityStatus.ACTIVE;
                                studentstandard.InsertedDate = DateTime.Now;
                                studentstandard.ModifiedDate = DateTime.Now;
                                studentstandard.InsertedId = userId;
                                studentstandard.ModifiedId = userId;
                                await admissionStudentStandardRepository.AddAsync(studentstandard);

                                AdmissionTimeline timeline = new AdmissionTimeline();
                                timeline.AdmissionId = addmisionid;
                                timeline.Timeline = "Registration Completed";
                                timeline.Active = true;
                                timeline.IsAvailable = true;
                                timeline.Status = "REGISTRATION";
                                timeline.InsertedDate = DateTime.Now;
                                timeline.ModifiedDate = DateTime.Now;
                                timeline.InsertedId = userId;
                                timeline.ModifiedId = userId;
                                admissionRepository.CreateAdmissionTimeline(timeline);

                                AdmissionFormPayment payment = new AdmissionFormPayment();
                                payment.AdmissionId = addmisionid;
                                payment.PaymentModeId = Convert.ToInt64(firstWorksheet.Cells[i, 12].Value.ToString().Split('_')[1]);
                                payment.TransactionId = firstWorksheet.Cells[i, 13].Value.ToString();
                                payment.PaidAmount = Convert.ToDecimal(firstWorksheet.Cells[i, 14].Value.ToString());
                                payment.PaidDate = Convert.ToDateTime(firstWorksheet.Cells[i, 15].Value);
                                payment.ReceivedId = Convert.ToInt64(firstWorksheet.Cells[i, 16].Value.ToString().Split('_')[1]);
                                payment.Active = true;
                                payment.IsAvailable = true;
                                payment.Status = EntityStatus.ACTIVE;
                                payment.InsertedDate = DateTime.Now;
                                payment.ModifiedDate = DateTime.Now;
                                payment.InsertedId = userId;
                                payment.ModifiedId = userId;
                                await admissionFormPaymentRepository.AddAsync(payment);
                                AdmissionTimeline timeline1 = new AdmissionTimeline();
                                timeline1.AdmissionId = addmisionid;
                                timeline1.Timeline = "Registration Fee Paid";
                                timeline1.Active = true;
                                timeline1.IsAvailable = true;
                                timeline1.Status = "PAID";
                                timeline1.InsertedDate = DateTime.Now;
                                timeline1.ModifiedDate = DateTime.Now;
                                timeline1.InsertedId = userId;
                                timeline1.ModifiedId = userId;
                                admissionRepository.CreateAdmissionTimeline(timeline1);
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(ViewStudentAdmissionDetails)).WithSuccess("success", "Excel uploaded successfully"); ;
        }
        #endregion

        #region-------------------------Student Wise CouncellorList---------------
        public IActionResult ViewStudentWiseCouncellorDetails(long? AcademicSessionId, long? RegistrationSlot,
            long? OraganisationAcademicId,
            long? BoardId, long? ClassId, long? WingId, long? RegistrationSource,
            string FormPaymentStatus,
            string fromDate, string toDate, string formNumber)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ResponseStudentModal res = new ResponseStudentModal();
            var details = admissionSqlQuery.GetView_Student_Admission_List();
            #region-------------Councellor Filterration Access -----------------
            CheckLoginStatus();
            List<long> concellor_AdmissionStudentId = null;
            var councellorid = admissionCounsellorRepository.
                ListAllAsyncIncludeAll().Result.
                Where(a => a.ID == userId).FirstOrDefault();
            if (councellorid != null)
            {
                concellor_AdmissionStudentId = admissionStudentCounsellorRepository.
                    ListAllAsyncIncludeAll().Result.
                    Where(a => a.EmployeeId == councellorid.ID).
                    Select(a => a.AdmissionStudentId).ToList();
            }

            if (concellor_AdmissionStudentId != null)
            {
                concellor_AdmissionStudentId = admissionStudentCounsellorRepository.
                    ListAllAsyncIncludeAll().Result.
                    Where(a => a.EmployeeId == councellorid.ID).
                    Select(a => a.AdmissionStudentId).ToList();
            }


            if (concellor_AdmissionStudentId != null && concellor_AdmissionStudentId.Count > 0)
            {
                details = details.Where(a => concellor_AdmissionStudentId.
                Contains(a.AdmissionStudentId)).ToList();
            }
            else
            {
                details = null;
            }
            #endregion---------------------------------------------------

            if (AcademicSessionId != null && AcademicSessionId > 0)
            {
                res.AcademicSessionId = AcademicSessionId;
                details = details.Where(a => a.AcademicSessionId == AcademicSessionId).ToList();
            }
            if (OraganisationAcademicId != null && OraganisationAcademicId > 0)
            {
                res.OraganisationAcademicId = OraganisationAcademicId;
                details = details.Where(a => a.OrganisationAcademicId == OraganisationAcademicId).ToList();
            }

            if (BoardId != null && BoardId > 0)
            {
                res.BoardId = BoardId;
                details = details.Where(a => a.BoardId == BoardId).ToList();
            }
            if (ClassId != null && ClassId > 0)
            {
                res.AcademicStandardId = ClassId;
                details = details.Where(a => a.StandardId == ClassId).ToList();
            }

            if (WingId != null && WingId > 0)
            {
                res.WingId = WingId;
                details = details.Where(a => a.WingId == WingId).ToList();
            }

            //if (RegistrationSlot != null && RegistrationSlot > 0)
            //{
            //    res.RegistrationSlot = RegistrationSlot;
            //    details = details.Where(a => a.AdmissionSlotId == RegistrationSlot).ToList();
            //}

            if (RegistrationSource != null && RegistrationSource > 0)
            {
                res.RegistrationSource = RegistrationSource;
                details = details.Where(a => a.AdmissionSourceId == RegistrationSource).ToList();
            }
            res.FormPaymentStatus = FormPaymentStatus;
            if (FormPaymentStatus == null)
            {
                //nothing
            }
            else
                if (FormPaymentStatus == "Paid")
            {
                details = details.Where(a => a.FormPaymentId == 1).ToList();
            }
            else
                if (FormPaymentStatus == "Not Paid")
            {
                details = details.Where(a => a.FormPaymentId == 0).ToList();
            }

            if (formNumber != null && formNumber.Trim().Length > 0)
            {
                details = details.Where(a => formNumber.Contains(a.FormNumber)).ToList();
            }

            if (fromDate != null)
            {
                DateTime FromDate = Convert.ToDateTime(fromDate + " 00:00:00 AM");
                DateTime ToDate = System.DateTime.Now;
                if (toDate != null)
                {
                    ToDate = Convert.ToDateTime(toDate + " 23:59:59 PM");
                }
                if (fromDate != null && toDate != null)
                {
                    details = details.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                }
                if (fromDate != null && toDate == null)
                {
                    details = details.Where(a => a.InsertedDate >= FromDate).ToList();
                }
            }

            TempData["FromDate"] = fromDate;
            TempData["ToDate"] = toDate;
            res.BasicStudentDetailsModel = details;

            commonCls paid = new commonCls();
            paid.name = "Paid";

            commonCls paidNo = new commonCls();
            paidNo.name = "Not Paid";

            res.FormPaymentStatusList = new List<commonCls>();
            res.FormPaymentStatusList.Add(paidNo);
            res.FormPaymentStatusList.Add(paid);
            // counselor employee list
            res.EmployeeList = employeeRepository.GetAllEmployee().
                Where(b => admissionCounsellorRepository.ListAllAsync()
                .Result.Where(a => a.Active == true).Select(a => a.EmployeeId).Distinct().Contains(b.ID))
                .ToList();
            res.SourceList = admissionRepository.GetAllAdmissionSource().ToList();
            res.AcademicSessionList = academicSessionRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();

            return View(res);
        }
        #endregion------------------------------------------------------------------------

        #region-------------------------Student Wise InterViewer List----------------------
        public IActionResult ViewStudentWiseInterViewerDetails(long? AcademicSessionId, long? RegistrationSlot,
            long? OraganisationAcademicId,
            long? BoardId, long? ClassId, long? WingId, long? RegistrationSource,
            string FormPaymentStatus,
            string fromDate, string toDate, string formNumber)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ResponseStudentModal res = new ResponseStudentModal();
            var details = admissionSqlQuery.GetView_Student_Admission_List();
            #region-------------Councellor Filterration Access -----------------
            CheckLoginStatus();


            List<long> interviewer_AdmissionStudentId = null;
            var interviewerid = admissionInterViewerRepository.
                ListAllAsyncIncludeAll().Result.
                Where(a => a.EmployeeId == userId).FirstOrDefault();
            if (interviewerid != null)
            {
                interviewer_AdmissionStudentId = admissionPersonalInterviewAssignRepository.
                    ListAllAsyncIncludeAll().Result.
                    Where(a => a.EmployeeId == interviewerid.ID).
                    Select(a => a.AdmissionStudentId).ToList();
            }

            if (interviewer_AdmissionStudentId != null)
            {
                interviewer_AdmissionStudentId = admissionPersonalInterviewAssignRepository.
                    ListAllAsyncIncludeAll().Result.
                    Where(a => a.EmployeeId == interviewerid.ID).
                    Select(a => a.AdmissionStudentId).ToList();
            }


            if (interviewer_AdmissionStudentId != null && interviewer_AdmissionStudentId.Count > 0)
            {
                details = details.Where(a => interviewer_AdmissionStudentId.
                Contains(a.AdmissionStudentId)).ToList();
            }
            else
            {
                details = null;
            }
            #endregion---------------------------------------------------

            if (AcademicSessionId != null && AcademicSessionId > 0)
            {
                res.AcademicSessionId = AcademicSessionId;
                details = details.Where(a => a.AcademicSessionId == AcademicSessionId).ToList();
            }
            if (OraganisationAcademicId != null && OraganisationAcademicId > 0)
            {
                res.OraganisationAcademicId = OraganisationAcademicId;
                details = details.Where(a => a.OrganisationAcademicId == OraganisationAcademicId).ToList();
            }

            if (BoardId != null && BoardId > 0)
            {
                res.BoardId = BoardId;
                details = details.Where(a => a.BoardId == BoardId).ToList();
            }
            if (ClassId != null && ClassId > 0)
            {
                res.AcademicStandardId = ClassId;
                details = details.Where(a => a.StandardId == ClassId).ToList();
            }

            if (WingId != null && WingId > 0)
            {
                res.WingId = WingId;
                details = details.Where(a => a.WingId == WingId).ToList();
            }

            //if (RegistrationSlot != null && RegistrationSlot > 0)
            //{
            //    res.RegistrationSlot = RegistrationSlot;
            //    details = details.Where(a => a.AdmissionSlotId == RegistrationSlot).ToList();
            //}

            if (RegistrationSource != null && RegistrationSource > 0)
            {
                res.RegistrationSource = RegistrationSource;
                details = details.Where(a => a.AdmissionSourceId == RegistrationSource).ToList();
            }
            res.FormPaymentStatus = FormPaymentStatus;
            if (FormPaymentStatus == null)
            {
                //nothing
            }
            else
                if (FormPaymentStatus == "Paid")
            {
                details = details.Where(a => a.FormPaymentId == 1).ToList();
            }
            else
                if (FormPaymentStatus == "Not Paid")
            {
                details = details.Where(a => a.FormPaymentId == 0).ToList();
            }

            if (formNumber != null && formNumber.Trim().Length > 0)
            {
                details = details.Where(a => formNumber.Contains(a.FormNumber)).ToList();
            }

            if (fromDate != null)
            {
                DateTime FromDate = Convert.ToDateTime(fromDate + " 00:00:00 AM");
                DateTime ToDate = System.DateTime.Now;
                if (toDate != null)
                {
                    ToDate = Convert.ToDateTime(toDate + " 23:59:59 PM");
                }
                if (fromDate != null && toDate != null)
                {
                    details = details.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                }
                if (fromDate != null && toDate == null)
                {
                    details = details.Where(a => a.InsertedDate >= FromDate).ToList();
                }
            }

            TempData["FromDate"] = fromDate;
            TempData["ToDate"] = toDate;
            res.BasicStudentDetailsModel = details;

            commonCls paid = new commonCls();
            paid.name = "Paid";

            commonCls paidNo = new commonCls();
            paidNo.name = "Not Paid";

            res.FormPaymentStatusList = new List<commonCls>();
            res.FormPaymentStatusList.Add(paidNo);
            res.FormPaymentStatusList.Add(paid);
            // counselor employee list
            res.EmployeeList = employeeRepository.GetAllEmployee().
                Where(b => admissionCounsellorRepository.ListAllAsync()
                .Result.Where(a => a.Active == true).Select(a => a.EmployeeId).Distinct().Contains(b.ID))
                .ToList();
            res.SourceList = admissionRepository.GetAllAdmissionSource().ToList();
            res.AcademicSessionList = academicSessionRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();

            return View(res);
        }
        #endregion------------------------------------------------------------------------

        #region-------------------------Student Wise DocumentVerifier List----------------------
        public IActionResult ViewStudentWiseDocumentVerifier(long? AcademicSessionId, long? RegistrationSlot,
            long? OraganisationAcademicId,
            long? BoardId, long? ClassId, long? WingId, long? RegistrationSource,
            string FormPaymentStatus,
            string fromDate, string toDate, string formNumber)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Registrations", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ResponseStudentModal res = new ResponseStudentModal();
            var details = admissionSqlQuery.GetView_Student_Admission_List();
            #region-------------Councellor Filterration Access -----------------
            CheckLoginStatus();
            List<long> docassign_AdmissionStudentId = null;
            var documentassignid = admissionVerifierRepository.
                ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == userId).FirstOrDefault();
            if (documentassignid != null)
            {
                docassign_AdmissionStudentId = admissionDocumentVerificationAssignRepository.
                    ListAllAsyncIncludeAll().Result.
                    Where(a => a.EmployeeId == documentassignid.ID).
                    Select(a => a.AdmissionStudentId).ToList();
            }

            if (docassign_AdmissionStudentId != null)
            {
                docassign_AdmissionStudentId = admissionDocumentVerificationAssignRepository.
                    ListAllAsyncIncludeAll().Result.
                    Where(a => a.EmployeeId == documentassignid.ID).
                    Select(a => a.AdmissionStudentId).ToList();
            }


            if (docassign_AdmissionStudentId != null && docassign_AdmissionStudentId.Count > 0)
            {
                details = details.Where(a => docassign_AdmissionStudentId.
                Contains(a.AdmissionStudentId)).ToList();
            }
            else
            {
                details = null;
            }
            #endregion---------------------------------------------------

            if (AcademicSessionId != null && AcademicSessionId > 0)
            {
                res.AcademicSessionId = AcademicSessionId;
                details = details.Where(a => a.AcademicSessionId == AcademicSessionId).ToList();
            }
            if (OraganisationAcademicId != null && OraganisationAcademicId > 0)
            {
                res.OraganisationAcademicId = OraganisationAcademicId;
                details = details.Where(a => a.OrganisationAcademicId == OraganisationAcademicId).ToList();
            }

            if (BoardId != null && BoardId > 0)
            {
                res.BoardId = BoardId;
                details = details.Where(a => a.BoardId == BoardId).ToList();
            }
            if (ClassId != null && ClassId > 0)
            {
                res.AcademicStandardId = ClassId;
                details = details.Where(a => a.StandardId == ClassId).ToList();
            }

            if (WingId != null && WingId > 0)
            {
                res.WingId = WingId;
                details = details.Where(a => a.WingId == WingId).ToList();
            }

            //if (RegistrationSlot != null && RegistrationSlot > 0)
            //{
            //    res.RegistrationSlot = RegistrationSlot;
            //    details = details.Where(a => a.AdmissionSlotId == RegistrationSlot).ToList();
            //}

            if (RegistrationSource != null && RegistrationSource > 0)
            {
                res.RegistrationSource = RegistrationSource;
                details = details.Where(a => a.AdmissionSourceId == RegistrationSource).ToList();
            }
            res.FormPaymentStatus = FormPaymentStatus;
            if (FormPaymentStatus == null)
            {
                //nothing
            }
            else
                if (FormPaymentStatus == "Paid")
            {
                details = details.Where(a => a.FormPaymentId == 1).ToList();
            }
            else
                if (FormPaymentStatus == "Not Paid")
            {
                details = details.Where(a => a.FormPaymentId == 0).ToList();
            }

            if (formNumber != null && formNumber.Trim().Length > 0)
            {
                details = details.Where(a => formNumber.Contains(a.FormNumber)).ToList();
            }

            if (fromDate != null)
            {
                DateTime FromDate = Convert.ToDateTime(fromDate + " 00:00:00 AM");
                DateTime ToDate = System.DateTime.Now;
                if (toDate != null)
                {
                    ToDate = Convert.ToDateTime(toDate + " 23:59:59 PM");
                }
                if (fromDate != null && toDate != null)
                {
                    details = details.Where(a => a.InsertedDate >= FromDate && a.InsertedDate <= ToDate).ToList();
                }
                if (fromDate != null && toDate == null)
                {
                    details = details.Where(a => a.InsertedDate >= FromDate).ToList();
                }
            }

            TempData["FromDate"] = fromDate;
            TempData["ToDate"] = toDate;
            res.BasicStudentDetailsModel = details;

            commonCls paid = new commonCls();
            paid.name = "Paid";

            commonCls paidNo = new commonCls();
            paidNo.name = "Not Paid";

            res.FormPaymentStatusList = new List<commonCls>();
            res.FormPaymentStatusList.Add(paidNo);
            res.FormPaymentStatusList.Add(paid);
            // counselor employee list
            res.EmployeeList = employeeRepository.GetAllEmployee().
                Where(b => admissionCounsellorRepository.ListAllAsync()
                .Result.Where(a => a.Active == true).
                Select(a => a.EmployeeId).Distinct().Contains(b.ID))
                .ToList();
            res.SourceList = admissionRepository.GetAllAdmissionSource().ToList();
            res.AcademicSessionList = academicSessionRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();

            return View(res);
        }
        #endregion------------------------------------------------------------------------


        #region Edit Admission Form
        public async Task<IActionResult> EditAdmissionDetails(string id, string name)
        {
            AdmissionLoginStatus();
            if (id == null || id.Equals(Guid.Empty))
                return RedirectToAction("Error", "Student");

            StudentProfileViewModel model = new StudentProfileViewModel();
            model.TabName = name;
            ViewBag.admissionReferenceId = id;/*leade reference id*/

            var adm = admissionRepository.GetAdmissionByLeadReferenceId(id);
            if (adm == null)
            {
                return RedirectToAction("Error", "Student");
            }
            var student = admissionStudentRepository.GetByAdmissionId(adm.ID).Result;
            ViewBag.admissionId = adm.ID;
            if (student == null)
                return RedirectToAction("Error", "Student");
            ViewBag.admissionstudentid = student.ID;

            return View(model);
        }

        #endregion


        #region kit status update
        public async Task<IActionResult> admissionkitstatuschange(string act, long id)
        {
            CheckLoginStatus();
            AdmissionStudentKit kit = new AdmissionStudentKit();
            kit.kitdate = DateTime.Now;
            kit.kitstatus = act;
            kit.AdmissionId = id;
            kit.IsAvailable = true;
            kit.Status = Admission_Status.REGISTERED;
            kit.InsertedDate = DateTime.Now;
            kit.ModifiedDate = DateTime.Now;
            kit.InsertedId = userId;
            kit.ModifiedId = userId;
            kit.Active = true;
            var ss = admissionStudentKitRepository.AddAsync(kit).Result;

            if (act == "DISPATCHED")
            {
                var admission = admissionsqlQuery.GetView_Student_Admission_List().Where(a => a.ID == id).FirstOrDefault();
                if (admission.Mobile != null)
                {
                    smssend.SendcOMMONSms(admission.Fullname, admission.Mobile, "Hello" + admission.Fullname + ",\n Your admission kit has been dispatched on this " + DateTime.Now.ToString("dd-MM-yyyy") + ".\n With Regards,\n ODM PUBLIC SCHOOL");
                }
                if (admission.EmergencyEmail != null)
                {
                    sendinBLUE.SendCommonMail(admission.Fullname, " Your admission kit has been dispatched on this " + DateTime.Now.ToString("dd-MM-yyyy"), admission.EmergencyEmail);
                }
            }

            return RedirectToAction(nameof(ViewStudentAdmissionDetails));
        }

        #endregion


        #region E-Counseller
        public async Task<IActionResult> ViewAdmissionStudentECounseller(long? AcademicSessionId,
            long? RegistrationSlot,
            long? OraganisationAcademicId,
            long? BoardId, long? ClassId, long? WingId, long? RegistrationSource,
            string FormPaymentStatus,string cstatusid,
            string fromDate, string toDate, long? ecounsellingdate,long? ecounsellingslot )
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("E-Counselling",
                accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var ecounseller = admissionEcounsellingRepository.ListAllAsync().Result;
            var ecounsellerdate = admissionEcounsellingDateRepository.ListAllAsync().Result;
            var ecounsellerslot = admissionEcounsellingSlotRepository.ListAllAsync().Result;
            List<Ecounsellingcls> counselling = null; 
            if (ecounseller.Count>0)
            {
                 counselling = (from a in ecounseller
                                   join b in ecounsellerdate on a.AdmissionEcounsellingDateId equals b.ID
                                   join c in ecounsellerslot on a.AdmissionEcounsellingSlotId equals c.ID
                                   where a.Active == true && b.Active == true && c.Active == true
                                   select new Ecounsellingcls
                                   {
                                       counsellingstatus = a.Status,
                                       counsellingissmssend = a.IssmsSend==null?false:(a.IssmsSend.Value),
                                       counsellingstatuson = a.VisitedDate.Value.ToString("dd MMM yyyy"),
                                       counsellingdate = (Nullable<DateTime>)b.EcounsellingDate,
                                       counsellingslot = c.Name,
                                       counsellingID = a.ID,
                                       AdmissionId = a.AdmissionId,
                                       counsellingdateid = a.AdmissionEcounsellingDateId,
                                       counsellingslotid = a.AdmissionEcounsellingSlotId
                                   }).ToList();
            }
           
            var admissiondetails = admissionsqlQuery.GetView_Student_Admission_List();
            var res = (from a in admissiondetails
                       select new AdmissionEcounsellingListcls
                       {
                           admission = a,
                           status = counselling == null ? "NOT BOOKED" : (counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault() == null ? "NOT BOOKED" : counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault().counsellingstatus),
                           statusdate = counselling == null ? "NOT BOOKED" : (counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault() == null ? "NOT BOOKED" : counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault().counsellingstatuson),
                           smssend = counselling == null ? false : (counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault() == null ? false : counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault().counsellingissmssend),
                           counsellingdate = counselling == null ? null : (counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault() == null ? null : counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault().counsellingdate),
                           counsellingslot = counselling == null ? "" : (counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault() == null ? "" : counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault().counsellingslot),
                           counsellingid = counselling == null ? 0 : (counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault() == null ? 0 : counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault().counsellingID),
                           counsellingdateid = counselling == null ? 0 : (counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault() == null ? 0 : counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault().counsellingdateid),
                           counsellingslotid = counselling == null ? 0 : (counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault() == null ? 0 : counselling.Where(m => m.AdmissionId == a.ID).FirstOrDefault().counsellingslotid)
                       }).ToList();
            Admissionecounsellingcls ob = new Admissionecounsellingcls();
            
            if (AcademicSessionId != null && AcademicSessionId > 0)
            {
                ob.AcademicSessionId = AcademicSessionId;
                res = res.Where(a => a.admission.AcademicSessionId == AcademicSessionId).ToList();
            }
            if (OraganisationAcademicId != null && OraganisationAcademicId > 0)
            {
                ob.OraganisationAcademicId = OraganisationAcademicId;
                res = res.Where(a => a.admission.OrganisationAcademicId == OraganisationAcademicId).ToList();
            }

            if (BoardId != null && BoardId > 0)
            {
                ob.BoardId = BoardId;
                res = res.Where(a => a.admission.BoardId == BoardId).ToList();
            }
            if (ClassId != null && ClassId > 0)
            {
                ob.AcademicStandardId = ClassId;
                res = res.Where(a => a.admission.StandardId == ClassId).ToList();
            }

            if (WingId != null && WingId > 0)
            {
                ob.WingId = WingId;
                res = res.Where(a => a.admission.WingId == WingId).ToList();
            }

            if (RegistrationSlot != null && RegistrationSlot > 0)
            {
                ob.RegistrationSlot = RegistrationSlot;
                res = res.Where(a => a.admission.AdmissionSlotId == RegistrationSlot).ToList();
            }

            if (RegistrationSource != null && RegistrationSource > 0)
            {
                ob.RegistrationSource = RegistrationSource;
                res = res.Where(a => a.admission.AdmissionSourceId == RegistrationSource).ToList();
            }
            if (ecounsellingdate != null && ecounsellingdate > 0)
            {
                ob.CounsellingDateID = ecounsellingdate;
                res = res.Where(a => a.counsellingdateid == ecounsellingdate).ToList();
            } 
            if (ecounsellingslot != null && ecounsellingslot > 0)
            {
                ob.CounsellingSlotID = ecounsellingslot;
                res = res.Where(a => a.counsellingslotid == ecounsellingslot).ToList();
            }
            if (cstatusid != null && cstatusid != "")
            {
                ob.cstatusid = cstatusid;
                if (cstatusid == "NOT BOOKED")
                {
                    res = res.Where(a => a.counsellingid == 0).ToList();
                }
                else
                {
                    res = res.Where(a => a.status == cstatusid).ToList();
                }
            }
            ob.FormPaymentStatus = FormPaymentStatus;
            if (FormPaymentStatus == null)
            {
                //nothing
            }
            else
                if (FormPaymentStatus == "Paid")
            {
                res = res.Where(a => a.admission.FormPaymentId > 0).ToList();
            }
            else
                if (FormPaymentStatus == "Not Paid")
            {
                res = res.Where(a => a.admission.FormPaymentId == null).ToList();
            }

           

            if (fromDate != null)
            {
                DateTime FromDate = Convert.ToDateTime(fromDate + " 00:00:00 AM");
                DateTime ToDate = System.DateTime.Now;
                if (toDate != null)
                {
                    ToDate = Convert.ToDateTime(toDate + " 23:59:59 PM");
                }
                if (fromDate != null && toDate != null)
                {
                    res = res.Where(a => a.admission.InsertedDate >= FromDate && a.admission.InsertedDate <= ToDate).ToList();
                }
                if (fromDate != null && toDate == null)
                {
                    res = res.Where(a => a.admission.InsertedDate >= FromDate).ToList();
                }
            }

            TempData["FromDate"] = fromDate;
            TempData["ToDate"] = toDate;
            ob.admissionEcounsellingListcls = res;

            commonCls paid = new commonCls();
            paid.name = "Paid";

            commonCls paidNo = new commonCls();
            paidNo.name = "Not Paid";

            ob.FormPaymentStatusList = new List<commonCls>();
            ob.FormPaymentStatusList.Add(paidNo);
            ob.FormPaymentStatusList.Add(paid);
           

            ob.SourceList = admissionRepository.GetAllAdmissionSource().ToList();
            ob.AcademicSessionList = academicSessionRepository.ListAllAsync().
                Result.Where(a => a.Active == true && a.IsAdmission == true).ToList();
            ob.regdaccess = GetRegdAdmissionAccessDetails();
            ob.CounsellerDates = (from a in admissionEcounsellingDateRepository.ListAllAsync().Result
                                  where a.Active==true
                                  select new commonCls {
                                      id = a.ID,
                                      name = a.EcounsellingDate.ToString("dd MMM yyyy")
                                  }).ToList();
            HttpContext.Session.SetString("admissionecounsellinglist", JsonConvert.SerializeObject(res));


            return View(ob);
        }
       [HttpPost]
        public async Task<IActionResult> StatusUpdateEcounselling(long counsellingid,string StatusId,string txtremark,DateTime txtdate, long? counselldateid,long? counsellslotid)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("E-Counselling",
                accessId, "Status Update", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            AdmissionEcounselling ecounselling = admissionEcounsellingRepository.GetByIdAsync(counsellingid).Result;
            ecounselling.Status = StatusId;
            ecounselling.VisitedDate = txtdate;
            ecounselling.ModifiedDate = DateTime.Now;
            ecounselling.ModifiedId = userId;
            ecounselling.Remarks = txtremark;
            if(StatusId== "RE-SCHEDULED")
            {
                ecounselling.AdmissionEcounsellingDateId = counselldateid.Value;
                ecounselling.AdmissionEcounsellingSlotId = counsellslotid.Value;
            }
            await admissionEcounsellingRepository.UpdateAsync(ecounselling);
            AdmissionEcounsellingTimeline timeline = new AdmissionEcounsellingTimeline();
            timeline.InsertedDate = DateTime.Now;
            timeline.ModifiedDate = DateTime.Now;
            timeline.InsertedId = userId;
            timeline.ModifiedId = userId;
            timeline.IsAvailable = true;
            timeline.Active = true;
            timeline.AdmissionEcounsellingId = counsellingid;
            timeline.Name = StatusId;
            timeline.Status = "ACTIVE";
            await admissionEcounsellingTimelineRepository.AddAsync(timeline);
            return RedirectToAction(nameof(ViewAdmissionStudentECounseller));
        }
       [HttpPost]
        public async Task<IActionResult> BookEcounselling(long admissionid, long? counselldateid1,long? counsellslotid1)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("E-Counselling",
                accessId, "Booking", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            AdmissionEcounselling counsellor = new AdmissionEcounselling();
            counsellor.Active = true;
            counsellor.AdmissionEcounsellingDateId = counselldateid1.Value;
            counsellor.AdmissionEcounsellingSlotId = counsellslotid1.Value;
            counsellor.AdmissionId = admissionid;
            counsellor.VisitedDate = DateTime.Now;
            counsellor.InsertedDate = DateTime.Now;
            counsellor.ModifiedDate = DateTime.Now;
            counsellor.InsertedId = userId;
            counsellor.ModifiedId = userId;
            counsellor.IsAvailable = true;
            counsellor.IssmsSend = false;
            counsellor.Isvisited = false;
            counsellor.Status = "BOOKED";
            var ecounselling = await admissionEcounsellingRepository.AddAsync(counsellor);
            AdmissionEcounsellingTimeline timeline = new AdmissionEcounsellingTimeline();
            timeline.InsertedDate = DateTime.Now;
            timeline.ModifiedDate = DateTime.Now;
            timeline.InsertedId = userId;
            timeline.ModifiedId = userId;
            timeline.IsAvailable = true;
            timeline.Active = true;
            timeline.AdmissionEcounsellingId = ecounselling.ID;
            timeline.Name = "BOOKED";
            timeline.Status = "ACTIVE";
            await admissionEcounsellingTimelineRepository.AddAsync(timeline);
            return RedirectToAction(nameof(ViewAdmissionStudentECounseller));
        }
        [HttpPost]
        public async Task<IActionResult> SendsmsmailEcounselling(long counsellingid1,string txtmail)
        {
            CheckLoginStatus();
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("E-Counselling",
                accessId, "Send SMS", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            AdmissionEcounselling ecounselling = admissionEcounsellingRepository.GetByIdAsync(counsellingid1).Result;
            
            ecounselling.ModifiedDate = DateTime.Now;
            ecounselling.ModifiedId = userId;
            ecounselling.smscontent = txtmail;
            ecounselling.IssmsSend = true;
            
            await admissionEcounsellingRepository.UpdateAsync(ecounselling);
            var det = admissionSqlQuery.GetView_Student_Admission_List().Where(a => a.ID == ecounselling.AdmissionId).FirstOrDefault();
            if (det.Mobile != null)
            {
                smssend.SendcOMMONSms(det.Fullname, det.Mobile, txtmail);
            } 
            if (det.EmergencyEmail != null)
            {
                sendinBLUE.SendCommonMail(det.Fullname, txtmail,det.EmergencyEmail);
            }
            return RedirectToAction(nameof(ViewAdmissionStudentECounseller));
        }
        #region E-Counselling & Campus Tour
        public JsonResult GetEcounsellingDateListBysessionid(long sessionid)
        {
            var session = academicSessionRepository.ListAllAsync().Result.Where(a => a.ID == sessionid && a.Active == true).Select(a => a.ID).ToList();
            var counsellingdate = admissionEcounsellingDateRepository.ListAllAsync().Result.Where(a => a.Active == true && session.Contains(a.AcademicSessionId)).ToList();
            var res = (from a in counsellingdate
                       select new
                       {
                           id = a.ID,
                           name = a.EcounsellingDate.ToString("dd MMM yyyy")
                       }).ToList();
            return Json(res);
        }
      
        public JsonResult GetCampusTourDateListBysessionid(long sessionid)
        {
            var session = academicSessionRepository.ListAllAsync().Result.Where(a => a.ID == sessionid && a.Active == true).Select(a => a.ID).ToList();
            var campustourdate = admissionCampusTourDateRepository.ListAllAsync().Result.Where(a => a.Active == true && session.Contains(a.AcademicSessionId)).ToList();
            var res = (from a in campustourdate
                       select new
                       {
                           id = a.ID,
                           name = a.CampusTourDate.ToString("dd MMM yyyy")
                       }).ToList();
            return Json(res);
        }
       
        #endregion
        #endregion
    }
}
