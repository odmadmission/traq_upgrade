﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OdmErp.Web.Areas.Admission.Models;
using OdmErp.Web.BillDesk;
using Web.Controllers;

namespace Web.Areas.Admission.Controllers
{
    [Area("Admission")]
    public class PaymentStatusController : AppController
    {
        protected readonly PaymentManager pManager;

        public PaymentStatusController(PaymentManager pManager)
        {
            this.pManager = pManager;
        }

        public IActionResult PaymentResponse(string msg)
        {
            var data = pManager.ProcessPaymentStatus(msg);
            return View(data);
        }

        public IActionResult RazorPayPaymentResponse(string razorpay_payment_id, string admid)
        {

            var data = pManager.ProcessRazorPaymentStatus(razorpay_payment_id,admid);
            return View("PaymentResponse",data);
        }

        public IActionResult PostToBD(string id)
        {
            return Json(pManager.BillDeskPaymentRequestAsync(id));
        }
    }
}