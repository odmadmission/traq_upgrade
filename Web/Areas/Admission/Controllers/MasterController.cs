﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.RepositoryImpl;
using OdmErp.ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;
using OdmErp.Web.Extensions;
using Microsoft.AspNetCore.Http;
using OdmErp.Web.Models;
using OdmErp.Infrastructure.DTO;
using System.Linq;
using OdmErp.ApplicationCore.Entities.DocumentAggregate;
using Action = OdmErp.ApplicationCore.Entities.Action;
using Microsoft.AspNetCore.Authorization;
using OdmErp.ApplicationCore.Entities.MasterAggregate;
using OdmErp.ApplicationCore.Entities.StudentAggregate;
using OdmErp.ApplicationCore.Entities.ModuleAggregate;
using OdmErp.ApplicationCore.Entities.EmployeeAggregate;
using OfficeOpenXml;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Text;
using OdmErp.Infrastructure.DTO;
using Web.Controllers;
using ClosedXML.Excel;
using System.Data;
using OdmErp.Web.ViewModels;
using Stream = System.IO.Stream;
using OdmErp.Web.DTO;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections;
using Microsoft.AspNetCore.Routing;
using OdmErp.ApplicationCore.Entities.SchoolAggregate;
using OdmErp.Web.Areas.Admission.Models;
using System;
using DocumentFormat.OpenXml.Drawing.Charts;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using System.Collections.Generic;
using System.Data.SqlClient;
using OdmErp.Infrastructure.RepositoryImpl.AdmissionRepo;
using Microsoft.AspNetCore.Hosting.Internal;
using static System.Net.WebRequestMethods;
using OdmErp.Web.Areas.Admission.Constants;

namespace OdmErp.Web.Areas.Admission.Controllers
{
    [Area("Admission")]
    public class MasterController : AppController
    {
        public IAdmissionInterViewerRepository admissionInterViewerRepository;
        public IAdmissionVerifierRepository admissionVerifierRepository;
        public IAcademicSessionRepository academicSessionRepository;
        public IAdmissionRepository admissionRepository;
        public IBoardStandardRepository boardStandardRepository;
        public IBoardRepository boardRepository;
        public IStandardRepository standardRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IOrganizationRepository organizationRepository;
        public IAdmissionStandardSeatQuotaRepository admissionStandardSeatQuotaRepository;
        public IAdmissionStandardExamRepository admissionStandardExamRepository;
        public CommonMethods commonMethods;
        public IAcademicStandardRepository academicStandardRepository;
        public IStandardRepository StandardRepository;
        public IWingRepository wingRepository;
        public IMapClassToAdmissionRepository mapClassToAdmissionRepository;
        public IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository;
        public commonsqlquery commonsqlquery;
        public IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository;
        public IEmployeeRepository employeeRepository;
        public IAdmissionStudentRepository admissionStudentRepository;
        public IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepository;
        public ICounsellorStatusRepository counsellorStatusRepository;
        public IAdmissionCounsellorRepository admissionCounsellorRepository;
        public IAdmissionFeeRepository admissionFeeRepository;
        public IAdmitcardDetailsRepository admitcardDetailsRepository;
        public IMasterEmailRepository masterEmailRepository;
        public IMasterSMSRepository masterSMSRepository;
        public IAdmitcardVenueAddressRepository admitcardVenueAddressRepository;
        public IAdmitcardVenueRepository admitcardVenueRepository;
        public IAdmitcardInstructionsRepository admitcardInstructionsRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        private readonly IHostingEnvironment hostingEnvironment;

        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        public AdmissionSmsnMails Emailsend;
        public StudentAdmissionMethod studentAdmissionMethod;
        public smssend smssend;


        public MasterController(
            IBoardStandardWingRepository boardStandardWingRepo, IAcademicStandardWingRepository academicStandardWingRepo, IAcademicSessionRepository academicSessionRepository, IAdmissionRepository admissionRepository,

            IBoardStandardRepository boardStandardRepository, IBoardRepository boardRepository, IStandardRepository standardRepository,
            IAdmissionStandardSeatQuotaRepository admissionStandardSeatQuotaRepository, IAdmissionInterViewerRepository admissionInterViewerRepository,
            IOrganizationAcademicRepository organizationAcademicRepository, IOrganizationRepository organizationRepository,
            IAdmissionStandardExamRepository admissionStandardExamRepository, CommonMethods commonMethods, IAdmissionVerifierRepository admissionVerifierRepository,
            IAcademicStandardRepository academicStandardRepository, IStandardRepository StandardRepository, IAdmitcardVenueAddressRepository admitcardVenueAddressRepository,
            IWingRepository wingRepository, IMapClassToAdmissionRepository mapClassToAdmissionRepository, IAdmitcardVenueRepository admitcardVenueRepository,
            IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository, commonsqlquery commonsqlquery,
            IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository, IAdmissionCounsellorRepository admissionCounsellorRepository,
            IEmployeeRepository employeeRepository, IAdmissionStudentRepository admissionStudentRepository, IHostingEnvironment hostingEnvironment,
            IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepository, IAdmitcardInstructionsRepository admitcardInstructionsRepository,
            smssend sms,
            ICounsellorStatusRepository counsellorStatusRepo, IAdmissionFeeRepository admissionFeeRepository,
            IAdmitcardDetailsRepository admitcardDetailsRepo, IMasterEmailRepository masterEmailRepository, IMasterSMSRepository masterSMSRepository,
            IAdmissionFormPaymentRepository admissionFormPaymentRepository,
            IAdmissionStudentStandardRepository admissionStudentStandardRepository, AdmissionSmsnMails Emailsend, StudentAdmissionMethod studentAdmissionMethod)



        {
            boardStandardWingRepository = boardStandardWingRepo;
            academicStandardWingRepository = academicStandardWingRepo;

            smssend = sms;
            this.studentAdmissionMethod = studentAdmissionMethod;
            this.Emailsend = Emailsend;
            this.admissionVerifierRepository = admissionVerifierRepository;
            this.admissionInterViewerRepository = admissionInterViewerRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.admissionRepository = admissionRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.boardRepository = boardRepository;
            this.standardRepository = standardRepository;
            this.admissionStandardSeatQuotaRepository = admissionStandardSeatQuotaRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.organizationRepository = organizationRepository;
            this.admissionStandardExamRepository = admissionStandardExamRepository;
            this.commonMethods = commonMethods;
            this.academicStandardRepository = academicStandardRepository;
            this.StandardRepository = StandardRepository;
            this.wingRepository = wingRepository;
            this.mapClassToAdmissionRepository = mapClassToAdmissionRepository;
            this.admissionStandardExamResultDateRepository = admissionStandardExamResultDateRepository;
            this.commonsqlquery = commonsqlquery;
            this.admissionPersonalInterviewAssignRepository = admissionPersonalInterviewAssignRepository;
            this.employeeRepository = employeeRepository;
            this.admissionDocumentVerificationAssignRepository = admissionDocumentVerificationAssignRepository;
            this.admissionStudentRepository = admissionStudentRepository;
            this.counsellorStatusRepository = counsellorStatusRepo;
            this.admissionCounsellorRepository = admissionCounsellorRepository;
            this.admissionFeeRepository = admissionFeeRepository;
            this.admitcardDetailsRepository = admitcardDetailsRepo;
            this.masterEmailRepository = masterEmailRepository;
            this.masterSMSRepository = masterSMSRepository;
            this.admitcardVenueRepository = admitcardVenueRepository;
            this.admitcardVenueAddressRepository = admitcardVenueAddressRepository;
            this.admitcardInstructionsRepository = admitcardInstructionsRepository;
            this.hostingEnvironment = hostingEnvironment;
            this.admissionFormPaymentRepository = admissionFormPaymentRepository;
            this.admissionStudentStandardRepository = admissionStudentStandardRepository;
        }
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        #region---------------json Method-------------------
        public string GetDateTimeFormat(DateTime d, TimeSpan span)
        {

            DateTime time = d.Add(span);
            return time.ToString("hh:mm tt");
        }
        public JsonResult GetOrganisationByAcademicSessionId(long AcademicSessionId)                 //Filter the selected Standard
        {
            try
            {
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == AcademicSessionId);
                var OrganisationList = organizationRepository.GetAllOrganization();
                var res = (from a in OrganisationAcademic
                           join b in OrganisationList on a.OrganizationId equals b.ID
                           select new
                           {
                               id = a.ID,
                               name = b.Name,
                               organisationid = a.OrganizationId
                           }
                        ).ToList();

                return Json(res);
            }
            catch
            {
                return Json(null);
            }
        }
        public JsonResult GetBoardStandardById(long BoardId, long OraganisationAcademicId, long AcademicSessionId)
        {
            var boardstandard = boardStandardRepository.ListAllAsync().Result.Where(a => a.BoardId == BoardId && a.Active == true).ToList();
            var academicstandard = academicStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.OrganizationAcademicId == OraganisationAcademicId).ToList();
            var standard = standardRepository.GetAllStandard().ToList();

            var res = (from a in academicstandard
                       join b in boardstandard on a.BoardStandardId equals b.ID
                       join c in standard on b.StandardId equals c.ID
                       select new CommonMasterModel
                       {
                           ID = a.BoardStandardId,
                           Name = c.Name,
                           standardID = b.StandardId
                       }).ToList();
            return Json(res);
        }
        #endregion

        #region-----------------------Common Method---------------------------------
        public List<Organization> GetSchoolListByAcademicSession(long AcademicSessionId)
        {

            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                    Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.Active == true).Select(a => a.OrganizationId).ToList();

            var orgs = organizationRepository.GetAllOrganization();
            var res = (from a in orgAcademics
                       join b in orgs on a equals b.ID
                       select new Organization { ID = a, Name = b.Name }).ToList();

            return res.ToList();
        }
        public List<CommonStudentModal> GetOrganisationAcademicByAcademicSessionId(long AcademicSessionId)                 //Filter the selected Standard
        {
            try
            {
                var OrganisationAcademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == AcademicSessionId);
                var OrganisationList = organizationRepository.GetAllOrganization();
                var res = (from a in OrganisationAcademic
                           join b in OrganisationList on a.OrganizationId equals b.ID
                           select new CommonStudentModal
                           {
                               Id = a.ID,
                               Name = b.Name
                           }
                        ).ToList();

                return res;
            }
            catch
            {
                return null;
            }
        }
        public List<CommonMasterModel> GetBoardStandardListByBoardAndStandard(long boardId, long classid)
        {
            var boardstandard = boardStandardRepository.ListAllAsync().Result.Where(a => a.BoardId == boardId && a.StandardId == classid && a.Active == true).ToList();
            var standard = standardRepository.GetAllStandard().ToList();
            var res = (from a in boardstandard
                       join c in standard on a.StandardId equals c.ID
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = c.Name
                       }).ToList();
            if (res != null && res.Count() > 0)
            {
                return res;
            }
            else
            {
                return null;
            }


        }
        public List<CommonMasterModel> GetBoardStandardListByOrganisationId(long orgacdId)
        {
            var organisationAcademic = organizationAcademicRepository.ListAllAsync().Result.Where(a => a.ID == orgacdId).FirstOrDefault();
            var admissionsetting = admissionRepository.GetAllAdmissionStandardSetting().Where(a => a.Active == true && a.AcademicSessionId == organisationAcademic.AcademicSessionId && a.OrganizationId == organisationAcademic.OrganizationId).ToList();
            var boardstandard = boardStandardRepository.ListAllAsync().Result.Where(a => admissionsetting.Select(m => m.BoardStandardId).ToList().Contains(a.ID) && a.Active == true).ToList();
            var standard = standardRepository.GetAllStandard().ToList();
            var res = (from a in boardstandard
                       join c in standard on a.StandardId equals c.ID
                       select new CommonMasterModel
                       {
                           ID = a.ID,
                           Name = c.Name
                       }).ToList();
            if (res != null && res.Count() > 0)
            {
                return res;
            }
            else
            {
                return null;
            }


        }
        public List<CommonMasterModel> GetWingListByBoardStandardId(long orgacdId, long BoardStandardId)
        {
            var academicstandardwing = academicStandardWingRepository.ListAllAsync().Result;
            var standardsBoardswing = boardStandardWingRepository.ListAllAsync().
               Result.Where(a => a.BoardStandardID == BoardStandardId && a.Active == true).ToList();
            var academicstandard = academicStandardRepository.ListAllAsync().Result.
                   Where(a => a.OrganizationAcademicId == orgacdId && a.Active == true && a.BoardStandardId == BoardStandardId).FirstOrDefault();
            List<CommonMasterModel> list = new List<CommonMasterModel>();
            if (academicstandard != null)
            {
                foreach (var st in standardsBoardswing)
                {
                    var academicwing = academicstandardwing.Where(a => a.AcademicStandardId == academicstandard.ID && a.Active == true && a.BoardStandardWingID == st.ID).FirstOrDefault();
                    if (academicwing != null)
                    {
                        var wingname = wingRepository.ListAllAsync().Result.Where(a => a.ID == st.WingID).FirstOrDefault().Name;
                        CommonMasterModel od = new CommonMasterModel();
                        od.ID = academicwing.ID;
                        od.Name = wingname;
                        list.Add(od);
                    }
                }
            }
            return list;
        }

        public List<CommonStudentModal> GetstudentDetailsWithFormNumber()
        {
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result;
            var paymentlist = admissionFormPaymentRepository.ListAllAsyncIncludeAll().Result.Select(a => a.AdmissionId).Distinct().ToList();
            var list = admissionstudentlist.Where(a => paymentlist.Contains(a.AdmissionId)).ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();

            var studentlist = (from a in list
                               join b in admissionlist on a.AdmissionId equals b.ID
                               select new CommonStudentModal
                               {
                                   Id = a.ID,
                                   Name = a.FirstName + " " + a.LastName,
                                   FormNumber = b.FormNumber != "" && b.FormNumber != null ?  b.FormNumber : ""

                               }
                                ).ToList();
            return studentlist;
        }
        public IActionResult GetstudentListStandardWise(long? OraganisationAcademicId,long? BoardId,long? BoardStandardId,long? WingId,bool? excel)
        {
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result;
            var paymentlist = admissionFormPaymentRepository.ListAllAsyncIncludeAll().Result.Select(a => a.AdmissionId).Distinct().ToList();
            var list = admissionstudentlist.Where(a => paymentlist.Contains(a.AdmissionId)).ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();
            var detailslist = admissionStudentStandardRepository.ListAllAsyncIncludeAll().Result;
            List<studentStandardModal> studentlist = null;
            if (excel.Value==true)
            {
                 studentlist = GetstudentDetailsWithFilter(true, false);
            }
            else
            {
                 studentlist = GetstudentDetailsWithFilter(true, true);
            }
           
            if  (OraganisationAcademicId != null &&  OraganisationAcademicId != 0 )
            {
                long OraganisationId = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == OraganisationAcademicId).FirstOrDefault().OrganizationId;
                studentlist = studentlist.Where(a => a.OrganisationId == OraganisationId).ToList();
            }
            if (BoardId != null && BoardId != 0)
            {
                studentlist = studentlist.Where(a => a.BoardId == BoardId).ToList();
            }
            if (BoardStandardId != null && BoardStandardId!= 0)
            {
                long StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().StandardId;
                studentlist = studentlist.Where(a => a.ClassId == StandardId).ToList();
            }
            if (WingId != null && WingId != 0)
            {
                studentlist = studentlist.Where(a => a.WingId == WingId).ToList();
            }
            return Json (studentlist);
        }

            public List<studentStandardModal> GetstudentDetailsWithFilter(bool piDateActiveCheck, bool piDateDoFilter)
        {
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result;
            var paymentlist = admissionFormPaymentRepository.ListAllAsyncIncludeAll().Result.Select(a => a.AdmissionId).Distinct().ToList();
            var list = admissionstudentlist.Where(a => paymentlist.Contains(a.AdmissionId)).ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();         
            var detailslist = admissionStudentStandardRepository.ListAllAsyncIncludeAll().Result;
            var organisation = organizationRepository.GetAllOrganization().ToList();
            var winglist = wingRepository.ListAllAsyncIncludeAll().Result.ToList();
            var board = boardRepository.GetAllBoard().ToList();
            var classlist = StandardRepository.GetAllStandard().ToList();
            var sessionlist = academicSessionRepository.GetAllAcademicSession().Where(a=>a.IsAdmission==true).ToList();
            var studentlist = (from a in list
                               join b in admissionlist on a.AdmissionId equals b.ID
                               join c in detailslist on a.ID equals c.AdmissionStudentId
                               select new studentStandardModal
                               {
                                   AdmissionStudentId=c.AdmissionStudentId,
                                   Id = a.ID,
                                   BoardId = c.BoardId,
                                   BoardName = board.Where(z => z.ID == c.BoardId).FirstOrDefault()!=null? board.Where(z => z.ID == c.BoardId).FirstOrDefault().Name:"NA",
                                   WingId = c.WingId,
                                   WingName = winglist.Where(z => z.ID == c.ID).FirstOrDefault()!=null ? winglist.Where(z => z.ID == c.ID).FirstOrDefault().Name:"NA",
                                   OrganisationName = organisation.Where(z => z.ID == c.OrganizationId).FirstOrDefault()!=null ? organisation.Where(z => z.ID == c.OrganizationId).FirstOrDefault().Name:"NA",
                                   OrganisationId = c.OrganizationId,
                                   ClassName = classlist.Where(z => z.ID == c.StandardId).FirstOrDefault()!=null ? classlist.Where(z => z.ID == c.StandardId).FirstOrDefault().Name :"NA",
                                   ClassId = c.StandardId,
                                   Name = a.FirstName + " " + a.LastName,
                                   FormNumber = b.FormNumber != "" && b.FormNumber != null ? b.FormNumber : " ",
                                   StudentName = admissionstudentlist.Where(z => z.ID == c.AdmissionStudentId).Select(x => new { name = x.FirstName +" "+x.LastName }).FirstOrDefault().name,
                                   PiList = piDateActiveCheck == true ? studentAdmissionMethod.GetPiDetailsList(c.AdmissionStudentId) : null,
                               }
                                ).ToList().AsParallel();

            List<studentStandardModal> dataList = new List<studentStandardModal>();

            if (piDateDoFilter)
            {
                Parallel.ForEach(studentlist, item =>
                {

                    int exist = 0;
                    if (item.PiList != null && item.PiList.Count > 0)
                    {
                        foreach (var inneritem in item.PiList)
                        {
                            if (inneritem.active == 1)
                            {
                                exist = 1;
                            }
                        }

                    }

                    if (exist == 0)
                    {
                        dataList.Add(item);
                    }


                });
            }
            else
                dataList.AddRange(studentlist);

            //foreach(var item in studentlist)
            //{
            //    int exist = 0;
            //    if (item.PiList != null && item.PiList.Count > 0)
            //    {
            //        foreach (var inneritem in item.PiList)
            //        {
            //            if (inneritem.active == 1)
            //            {
            //                exist = 1;
            //            }
            //        }

            //    }

            //    if (exist == 0)
            //    {
            //        dataList.Add(item);
            //    }
            //}


            return dataList;
        }



        #endregion
        #region-----------Document Wize Student List (Json Method)-------------------------
        public IActionResult GetstudentDocumentListStandardWise(long? OraganisationAcademicId, long? BoardId, long? BoardStandardId, long? WingId, bool? excel)
        {
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result;
            var paymentlist = admissionFormPaymentRepository.ListAllAsyncIncludeAll().Result.Select(a => a.AdmissionId).Distinct().ToList();
            var list = admissionstudentlist.Where(a => paymentlist.Contains(a.AdmissionId)).ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();
            var detailslist = admissionStudentStandardRepository.ListAllAsyncIncludeAll().Result;
            List<studentStandardModal> studentlist = null;
            if (excel.Value == true)
            {
                studentlist = GetstudentDocumentDetailsWithFilter(true, false);
            }
            else
            {
                studentlist = GetstudentDocumentDetailsWithFilter(true, true);
            }

            if (OraganisationAcademicId != null && OraganisationAcademicId != 0)
            {
                long OraganisationId = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == OraganisationAcademicId).FirstOrDefault().OrganizationId;
                studentlist = studentlist.Where(a => a.OrganisationId == OraganisationId).ToList();
            }
            if (BoardId != null && BoardId != 0)
            {
                studentlist = studentlist.Where(a => a.BoardId == BoardId).ToList();
            }
            if (BoardStandardId != null && BoardStandardId != 0)
            {
                long StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().StandardId;
                studentlist = studentlist.Where(a => a.ClassId == StandardId).ToList();
            }
            if (WingId != null && WingId != 0)
            {
                studentlist = studentlist.Where(a => a.WingId == WingId).ToList();
            }
            return Json(studentlist);
        }
        #endregion

        #region---------------------- AdmitCardMaster-------------------------------
        public IActionResult AdmitcardMaster()
        {

            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_AdmitCard", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            //var sessionlist = academicSessionRepository.ListAllAsyncIncludeAll().Result;
            //var feelist = admissionFeeRepository.ListAllAsyncIncludeAll().Result.ToList();
            //var list = (from a in feelist
            //            join b in sessionlist on a.AcademicSessionId equals b.ID
            //            select new AdmissionFeeModel
            //            {

            //                ID = a.ID,
            //                InsertedDate = a.InsertedDate,
            //                AcademicSessionId = a.AcademicSessionId,
            //                AcademicSessionName = b.DisplayName,
            //                Ammount = a.Ammount,
            //                Status = a.Status
            //            }
            //           ).ToList();
            //ViewBag.AcademicSessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();
            //return View(list);
            return View();
        }
        public JsonResult GetAdmitcardHeader()
        {
            var admitcardData = admitcardDetailsRepository.GetAllAdmitcardDetails().Where(a => a.Active == true).ToList();


            //var boardStandards = academicStandardRepository.ListAllAsync().Result.
            //        Where(a => orgAcademics.Contains(a.OrganizationAcademicId) && a.Active == true).ToList().Select(a => a.BoardStandardId);

            //var standardsBoards = boardStandardRepository.ListAllAsync().
            //        Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
            //var standards = (from a in standardsBoards
            //                 select new
            //                 {
            //                     boardClass = StandardRepository.GetStandardById(a.StandardId).Name,
            //                     boardId = a.BoardId,
            //                     boardName = boardRepository.GetBoardById(a.BoardId).Name,
            //                     standardId = a.StandardId,
            //                     boardStandardId = a.ID,
            //                     boardStandardName = boardRepository.GetBoardById(a.BoardId).Name + "--" + StandardRepository.GetStandardById(a.StandardId).Name
            //                 }
            //                 ).ToList();
            return Json(admitcardData);
        }

        #endregion

        #region------------------------------AdmissionSlot------------------
        public IActionResult AdmissionSlot()
        {

            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_AdmissionSlot", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ResponseAdmission res = new ResponseAdmission();
            var sessionlist = academicSessionRepository.ListAllAsyncIncludeAll().Result;
            var admissionslotlist = admissionRepository.GetAllAdmissionSlot().ToList();
            res.AdmissionSlotList = (from a in admissionslotlist
                                     join b in sessionlist on a.AcademicSessionId equals b.ID
                                     select new AdmissionSlotModel
                                     {
                                         SlotName = a.SlotName,
                                         ID = a.ID,
                                         InsertedDate = a.InsertedDate,
                                         AcademicSessionId = a.AcademicSessionId,
                                         AcademicSessionName = b.DisplayName,
                                         StartDatestring = NullableDateTimeToStringDate(a.StartDate),
                                         EndDatestring = NullableDateTimeToStringDate(a.EndDate),
                                         StartDate = a.StartDate.Value,
                                         EndDate = a.EndDate.Value,
                                         Status = a.Status
                                     }
                      ).ToList();
            res.AcademicSessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();
            return View(res);
        }

        [HttpGet]
        public JsonResult CheckRegistrationSlotName(string name)
        {
            AdmissionSlot type = admissionRepository.GetAdmissionSlotByName(name);
            if (type == null)
            {
                return Json(new { data = 0 });
            }
            else
            {
                return Json(new { data = 1 });
            }
        }
        public IActionResult DeleteAdmissionSlot(int id)
        {
            AdmissionSlot type = admissionRepository.GetAdmissionSlotById(id);
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            admissionRepository.UpdateAdmissionSlot(type);
            return RedirectToAction(nameof(AdmissionSlot)).WithSuccess("Registration Slot", "Deleted Successfully");
        }
        public IActionResult SaveOrUpdateAdmissionSlot(long ID, string SlotName, DateTime StartDate, DateTime EndDate, long AcademicSessionId)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                var namechk = admissionRepository.GetAllAdmissionSlot().Where(a => a.SlotName == SlotName.ToUpper().Trim() && a.ID != ID).FirstOrDefault();
                if (namechk == null)
                {
                    AdmissionSlot type = admissionRepository.GetAdmissionSlotById(ID);
                    type.SlotName = SlotName.ToUpper();
                    type.AcademicSessionId = AcademicSessionId;
                    type.StartDate = StartDate;
                    type.EndDate = EndDate;
                    type.ModifiedId = userId;
                    admissionRepository.UpdateAdmissionSlot(type);
                    return RedirectToAction(nameof(AdmissionSlot)).WithSuccess("Registration Slot", "Updated Successfully");

                }
                else
                {
                    return RedirectToAction(nameof(AdmissionSlot)).WithDanger("Registration Slot", " Name already exist");

                }
            }
            else
            {
                var namechk = admissionRepository.GetAllAdmissionSlot().Where(a => a.SlotName == SlotName.ToUpper().Trim()).FirstOrDefault();
                if (namechk != null)
                {
                    return RedirectToAction(nameof(AdmissionSlot)).WithDanger("Registration Slot", "  Name already exist");

                }
                else
                {
                    AdmissionSlot type = new AdmissionSlot();
                    type.SlotName = SlotName.ToUpper();
                    type.AcademicSessionId = AcademicSessionId;
                    type.StartDate = StartDate;
                    type.EndDate = EndDate;
                    type.ModifiedId = userId;
                    type.InsertedId = userId;
                    type.IsAvailable = true;
                    type.Active = true;
                    type.Status = EntityStatus.ACTIVE;
                    admissionRepository.CreateAdmissionSlot(type);
                    return RedirectToAction(nameof(AdmissionSlot)).WithSuccess("Registration Slot", " Saved Successfully");

                }
            }

        }

        public IActionResult DownloadAdmissionSlotSample()
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            XLWorkbook oWB = new XLWorkbook();
            var ProfessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true).ToList();
            System.Data.DataTable professiondt = new System.Data.DataTable();
            professiondt.Columns.Add("Academic Session");
            foreach (var a in ProfessionList)
            {
                DataRow dr = professiondt.NewRow();
                dr["Academic Session"] = a.DisplayName;
                professiondt.Rows.Add(dr);
            }
            professiondt.TableName = "Academic Session";
            int lastCellNo1 = professiondt.Rows.Count + 1;
            oWB.AddWorksheet(professiondt);
            var worksheet1 = oWB.Worksheet(1);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("Academic Session");
            validationTable.Columns.Add("Slot Name");
            validationTable.Columns.Add("Start Date(MM/DD/YYYY)");
            validationTable.Columns.Add("End Date(MM/DD/YYYY)");

            validationTable.TableName = "AdmissionSlot_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"AdmissionRegistrationSlotSample.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadAdmissionSlot(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            string SlotName = firstWorksheet.Cells[i, 2].Value.ToString();
                            var slotlist = admissionRepository.GetAllAdmissionSlot().Where(b => b.SlotName == SlotName.ToUpper()).FirstOrDefault();
                            if (slotlist == null)
                            {
                                AdmissionSlot slot = new AdmissionSlot();
                                slot.ModifiedId = userId;
                                string sessionname = firstWorksheet.Cells[i, 1].Value.ToString();
                                slot.SlotName = SlotName.ToUpper();
                                Nullable<DateTime> startdate = Convert.ToDateTime(firstWorksheet.Cells[i, 3].Value);
                                Nullable<DateTime> enddate = Convert.ToDateTime(firstWorksheet.Cells[i, 4].Value);
                                slot.StartDate = startdate;
                                slot.EndDate = enddate;
                                slot.AcademicSessionId = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.DisplayName == sessionname).Select(a => a.ID).FirstOrDefault();
                                slot.Active = true;
                                slot.InsertedId = userId;
                                slot.InsertedDate = DateTime.Now;
                                slot.ModifiedDate = DateTime.Now;
                                slot.IsAvailable = true;
                                slot.ModifiedId = userId;
                                slot.Status = EntityStatus.ACTIVE;
                                admissionRepository.CreateAdmissionSlot(slot);
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionSlot)).WithSuccess("success", "Excel uploaded successfully"); ;
        }


        #endregion-------------------------------------------------

        #region---------------------Admission Source------------------
        public IActionResult AdmissionSource()
        {

            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Mst_LeadSource", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var res = admissionRepository.GetAllAdmissionSource().ToList();
            return View(res);
        }
        public IActionResult DeleteAdmissionSource(int id)
        {
            CheckLoginStatus();
            AdmissionSource type = admissionRepository.GetAdmissionSourceById(id);
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            admissionRepository.UpdateAdmissionSource(type);
            return RedirectToAction(nameof(AdmissionSource)).WithSuccess("Lead Source", "Deleted Successfully");
        }
        public IActionResult SaveOrUpdateAdmissionSource(long ID, string Name)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                var namechk = admissionRepository.GetAllAdmissionSource().Where(a => a.Name == Name.Trim() && a.ID != ID).FirstOrDefault();
                if (namechk == null)
                {
                    AdmissionSource type = admissionRepository.GetAdmissionSourceById(ID);
                    type.Name = Name.ToUpper();
                    type.ModifiedId = userId;
                    admissionRepository.UpdateAdmissionSource(type);
                    return RedirectToAction(nameof(AdmissionSource)).WithSuccess("Lead Source", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(AdmissionSource)).WithDanger("Lead Source", "Name already exist");
                }

            }
            else
            {

                var namechk = admissionRepository.GetAllAdmissionSource().Where(a => a.Name == Name.Trim()).FirstOrDefault();
                if (namechk != null)
                {
                    return RedirectToAction(nameof(AdmissionSource)).WithDanger("Lead Source", "Name already exist");
                }
                else
                {

                    AdmissionSource type = new AdmissionSource();
                    type.Name = Name.ToUpper();
                    type.ModifiedId = userId;
                    type.InsertedId = userId;
                    type.IsAvailable = true;
                    type.Active = true;
                    type.Status = EntityStatus.ACTIVE;
                    admissionRepository.CreateAdmissionSource(type);
                    return RedirectToAction(nameof(AdmissionSource)).WithSuccess("Lead Source", "Saved Successfully");

                }
            }
        }

        public IActionResult DownloadAdmissionSourceSample()
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            XLWorkbook oWB = new XLWorkbook();
            System.Data.DataTable validationTable = new System.Data.DataTable();

            validationTable.Columns.Add("Name");
            validationTable.TableName = "AdmissionLeadSource_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"AdmissionLeadSourceSample.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadAdmissionSource(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            try
            {
                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];
                        int totalRows = firstWorksheet.Dimension.Rows;
                        for (int i = 2; i <= totalRows; i++)
                        {
                            string Name = firstWorksheet.Cells[i, 1].Value.ToString();
                            var sourcelist = admissionRepository.GetAllAdmissionSource().Where(a => a.Name == Name.ToUpper().Trim()).FirstOrDefault();
                            if (sourcelist == null)
                            {
                                AdmissionSource student = new AdmissionSource();
                                student.Name = Name.ToUpper();
                                student.Active = true;
                                student.InsertedId = userId;
                                student.IsAvailable = true;
                                student.ModifiedId = userId;
                                student.Status = EntityStatus.ACTIVE;
                                admissionRepository.CreateAdmissionSource(student);
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionSource)).WithSuccess("Lead Source", "Excel Uploaded Successfully"); ;
        }

        #endregion----------------------------------------------------------------

        #region -------------------AdmissionStandardSetting-------------------------    



        public List<sessiondetailmodal> GetClassListByAcademicSession(long AcademicSessionId)
        {

            var orgAcademics = organizationAcademicRepository.ListAllAsync().
                    Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.Active == true).ToList().Select(a => a.ID);

            var boardStandards = academicStandardRepository.ListAllAsync().Result.
                    Where(a => orgAcademics.Contains(a.OrganizationAcademicId) && a.Active == true).ToList().Select(a => a.BoardStandardId);

            var standardsBoards = boardStandardRepository.ListAllAsync().
                    Result.Where(a => boardStandards.Contains(a.ID) && a.Active == true).ToList();
            var standards = (from a in standardsBoards
                             select new sessiondetailmodal
                             {
                                 boardClass = StandardRepository.GetStandardById(a.StandardId).Name,
                                 boardId = a.BoardId,
                                 boardname = boardRepository.GetBoardById(a.BoardId).Name,
                                 standardId = a.StandardId,
                                 boardStandardId = a.ID
                             }
                             ).Distinct().ToList();
            return standards;
        }

        public IActionResult AdmissionStandardSetting()
        {

            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_ClassSetting", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.Session = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();
            var admissionstandardsetinglist = admissionRepository.GetAllAdmissionStandardSetting().ToList();
            var session = academicSessionRepository.ListAllAsyncIncludeAll().Result;
            var boardStandard = boardStandardRepository.ListAllAsyncIncludeAll().Result;
            var standardlist = standardRepository.GetAllStandard().ToList();
            var boardlist = boardRepository.GetAllBoard().ToList();
            var orgacdlist = organizationAcademicRepository.ListAllAsync().Result;
            var orglist = organizationRepository.GetAllOrganization().ToList();

            var res = (from a in admissionstandardsetinglist
                       join b in session on a.AcademicSessionId equals b.ID
                       join c in boardStandard on a.BoardStandardId equals c.ID
                       join d in standardlist on c.StandardId equals d.ID
                       join e in boardlist on c.BoardId equals e.ID
                       join g in orgacdlist on a.OrganizationId equals g.ID
                       join f in orglist on g.OrganizationId equals f.ID
                       select new AdmissionStandardSettingModal
                       {
                           Code = a.Code,
                           ID = a.ID,
                           AcademicSessionName = b.DisplayName,
                           AcademicSessionId = a.AcademicSessionId,
                           BoardStandardId = c.ID,
                           StandardName = d.Name,
                           InsertedDate = a.InsertedDate,
                           Status = a.Status,
                           ExamRequired = a.ExamRequired,
                           PIRequired = a.PIRequired,
                           DocumentRequird = a.DocumentRequired,
                           Nucleus = a.Nucleus,
                           ExamDates = a.ExamDatesShow,
                           OrganizationId = a.OrganizationId,
                           OrganizationName = f.Name,
                           BoardId = e.ID,
                           BoardName = e.Name
                       }
                       ).ToList();
            return View(res);
        }
        public IActionResult DeleteAdmissionStandardSetting(int id)
        {
            CheckLoginStatus();
            AdmissionStandardSetting type = admissionRepository.GetAdmissionStandardSettingById(id);
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            admissionRepository.UpdateAdmissionStandardSetting(type);
            return RedirectToAction(nameof(AdmissionStandardSetting)).WithSuccess("Admission Standard Setting", "Deleted Successfully");
        }

        public IActionResult SaveAdmissionStandardSetting(long ID, long AcademicSessionId,
            long OrganizationId, long[] BoardStandardId, bool Nucleus, bool ExamRequired,
            bool PIRequired, bool DocumentRequired, bool ExamDateRequired)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                AdmissionStandardSetting setting = admissionRepository.GetAdmissionStandardSettingById(ID);

                setting.ModifiedId = userId;
                setting.ExamRequired = ExamRequired;
                setting.ExamDatesShow = ExamDateRequired;
                setting.Nucleus = Nucleus;
                setting.PIRequired = PIRequired;
                setting.DocumentRequired = DocumentRequired;
                admissionRepository.UpdateAdmissionStandardSetting(setting);
                return RedirectToAction(nameof(AdmissionStandardSetting)).WithSuccess("Class Wise Setting", "Updated Successfully");
            }
            else
            {
                foreach (long classid in BoardStandardId)
                {
                    var list = admissionRepository.GetAllAdmissionStandardSetting().Where(a => a.AcademicSessionId == AcademicSessionId && a.OrganizationId == OrganizationId && a.BoardStandardId == classid && a.Active == true).FirstOrDefault();
                    if (list == null)
                    {
                        AdmissionStandardSetting setting = new AdmissionStandardSetting();
                        setting.Code = " ";
                        setting.AcademicSessionId = AcademicSessionId;
                        setting.BoardStandardId = classid;
                        setting.WingId = 0;
                        setting.ModifiedId = userId;
                        setting.InsertedId = userId;
                        setting.OrganizationId = OrganizationId;
                        setting.IsAvailable = true;
                        setting.Active = true;
                        setting.Status = EntityStatus.ACTIVE;
                        setting.ExamRequired = ExamRequired;
                        setting.Nucleus = Nucleus;
                        setting.PIRequired = PIRequired;
                        setting.ExamDatesShow = ExamDateRequired;
                        setting.DocumentRequired = DocumentRequired;
                        admissionRepository.CreateAdmissionStandardSetting(setting);
                    }
                }
                return RedirectToAction(nameof(AdmissionStandardSetting)).WithSuccess("Class Wise Setting", "Saved Successfully");

            }

        }


        public IActionResult DownloadAdmissionStandardSettingSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();

            var BoardList = boardRepository.GetAllBoard().ToList();
            var ClassList = standardRepository.GetAllStandard().ToList();
            var OrganisationList = organizationRepository.GetAllOrganization().Where(a => a.Active == true).ToList();
            var SessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();



            System.Data.DataTable blooddt = new System.Data.DataTable();
            blooddt.Columns.Add("SessionName");
            foreach (var a in SessionList)
            {
                DataRow dr = blooddt.NewRow();
                dr["SessionName"] = "S_" + a.ID + "_" + a.DisplayName;
                blooddt.Rows.Add(dr);
            }
            blooddt.TableName = "Session";

            System.Data.DataTable categorydt = new System.Data.DataTable();
            categorydt.Columns.Add("OrganisationName");
            foreach (var a in OrganisationList)
            {
                DataRow dr = categorydt.NewRow();
                dr["OrganisationName"] = "O_" + a.ID + "_" + a.Name;
                categorydt.Rows.Add(dr);
            }
            categorydt.TableName = "Organisation";

            System.Data.DataTable nationalitydt = new System.Data.DataTable();
            nationalitydt.Columns.Add("BoardName");
            foreach (var a in BoardList)
            {
                DataRow dr = nationalitydt.NewRow();
                dr["BoardName"] = "B_" + a.ID + "_" + a.Name;
                nationalitydt.Rows.Add(dr);
            }
            nationalitydt.TableName = "Board";

            System.Data.DataTable religiondt = new System.Data.DataTable();
            religiondt.Columns.Add("ClassName");
            foreach (var a in ClassList)
            {
                DataRow dr = religiondt.NewRow();
                dr["ClassName"] = "C_" + a.ID + "_" + a.Name;
                religiondt.Rows.Add(dr);
            }
            religiondt.TableName = "Class";

            System.Data.DataTable examdt = new System.Data.DataTable();
            examdt.Columns.Add("Exam");
            DataRow dr3 = examdt.NewRow();
            dr3["Exam"] = "True";
            examdt.Rows.Add(dr3);
            DataRow dr4 = examdt.NewRow();
            dr4["Exam"] = "False";
            examdt.Rows.Add(dr4);
            examdt.TableName = "Exam Required";

            System.Data.DataTable commondt = new System.Data.DataTable();
            commondt.Columns.Add("Nucleus");
            DataRow dr9 = commondt.NewRow();
            dr9["Nucleus"] = "True";
            commondt.Rows.Add(dr9);
            DataRow dr10 = commondt.NewRow();
            dr10["Nucleus"] = "False";
            commondt.Rows.Add(dr10);
            commondt.TableName = "Nucleus";

            System.Data.DataTable pidt = new System.Data.DataTable();
            pidt.Columns.Add("PI");
            DataRow dr5 = pidt.NewRow();
            dr5["PI"] = "True";
            pidt.Rows.Add(dr5);
            DataRow dr6 = pidt.NewRow();
            dr6["PI"] = "False";
            pidt.Rows.Add(dr6);
            pidt.TableName = "PI Required";

            System.Data.DataTable documentdt = new System.Data.DataTable();
            documentdt.Columns.Add("Document");
            DataRow dr7 = documentdt.NewRow();
            dr7["Document"] = "True";
            documentdt.Rows.Add(dr7);
            DataRow dr8 = documentdt.NewRow();
            dr8["Document"] = "False";
            documentdt.Rows.Add(dr8);
            documentdt.TableName = "Document Required";




            int lastCellNo1 = blooddt.Rows.Count + 1;
            int lastCellNo2 = categorydt.Rows.Count + 1;
            int lastCellNo3 = nationalitydt.Rows.Count + 1;
            int lastCellNo4 = religiondt.Rows.Count + 1;

            int lastCellNo5 = commondt.Rows.Count + 1;
            int lastCellNo6 = examdt.Rows.Count + 1;
            int lastCellNo7 = pidt.Rows.Count + 1;
            int lastCellNo8 = documentdt.Rows.Count + 1;



            oWB.AddWorksheet(blooddt);
            oWB.AddWorksheet(categorydt);
            oWB.AddWorksheet(nationalitydt);
            oWB.AddWorksheet(religiondt);

            oWB.AddWorksheet(commondt);
            oWB.AddWorksheet(examdt);
            oWB.AddWorksheet(pidt);
            oWB.AddWorksheet(documentdt);



            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            var worksheet3 = oWB.Worksheet(3);
            var worksheet4 = oWB.Worksheet(4);
            var worksheet5 = oWB.Worksheet(5);
            var worksheet6 = oWB.Worksheet(6);
            var worksheet7 = oWB.Worksheet(7);
            var worksheet8 = oWB.Worksheet(8);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("Session (Compulsory)");
            validationTable.Columns.Add("Organisation (Compulsory)");
            validationTable.Columns.Add("Board (Compulsory)");
            validationTable.Columns.Add("Class (Compulsory)");
            validationTable.Columns.Add("Nucleus  (Compulsory)");
            validationTable.Columns.Add("Exam Required  (Compulsory)");
            validationTable.Columns.Add("PI Required  (Compulsory)");
            validationTable.Columns.Add("Document Required  (Compulsory)");
            validationTable.TableName = "StandardSettingSampleSample";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(2).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(3).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
            worksheet.Column(4).SetDataValidation().List(worksheet4.Range("A2:A" + lastCellNo4), true);
            worksheet.Column(5).SetDataValidation().List(worksheet5.Range("A2:A" + lastCellNo5), true);
            worksheet.Column(6).SetDataValidation().List(worksheet6.Range("A2:A" + lastCellNo6), true);
            worksheet.Column(7).SetDataValidation().List(worksheet7.Range("A2:A" + lastCellNo7), true);
            worksheet.Column(8).SetDataValidation().List(worksheet8.Range("A2:A" + lastCellNo8), true);

            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();
            worksheet4.Hide();
            worksheet5.Hide();
            worksheet6.Hide();
            worksheet7.Hide();
            worksheet8.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StandardSettingSampleSample.xlsx");
        }


        //public IActionResult DownloadAdmissionStandardSettingSample()
        //{
        //    long userId = HttpContext.Session.GetInt32("userId").Value;
        //    XLWorkbook oWB = new XLWorkbook();
        //    var ProfessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();
        //    var OrgList = organizationRepository.GetAllOrganization().ToList();
        //    int i = 1;
        //    var wsData = oWB.Worksheets.Add("Data");
        //    wsData.Cell(1, 1).Value = "Common";
        //    wsData.Cell(2, 1).Value = "True";
        //    wsData.Cell(3, 1).Value = "False";
        //    wsData.Range("A2:A" + 3).AddToNamed("Common");


        //    wsData.Cell(1, 2).Value = "Academic Session";
        //    i = 1;
        //    foreach (var a in ProfessionList)
        //    {
        //        wsData.Cell(++i, 2).Value = "C_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
        //    }
        //    wsData.Range("B2:B" + i).AddToNamed("Academic Session");


        //    int j = 2;
        //    foreach (var a in ProfessionList)
        //    {
        //        wsData.Cell(1, ++j).Value = "C_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
        //        int k = 1;
        //        foreach (var b in GetClassListByAcademicSession(a.ID))
        //        {
        //            wsData.Cell(++k, j).Value = "S_" + b.boardStandardId + "_"+ b.boardname + "_"+ b.boardClass.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
        //        }
        //        wsData.Range(wsData.Cell(2, j), wsData.Cell(k, j)).AddToNamed("C_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
        //    }

        //    int m = 3;
        //    foreach (var a in ProfessionList)
        //    {
        //        wsData.Cell(1, ++m).Value = "C_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
        //        int k = 1;
        //        foreach (var b in GetSchoolListByAcademicSession(a.ID))
        //        {
        //            wsData.Cell(++k, j).Value = "O_" + b.ID +  "_" + b.Name.Replace(" ", "_").Replace("-", "_").Replace("&", "And");
        //        }
        //        wsData.Range(wsData.Cell(2, m), wsData.Cell(k, m)).AddToNamed("C_" + a.ID + "_" + a.DisplayName.Replace(" ", "_").Replace("-", "_").Replace("&", "And"));
        //    }



        //    System.Data.DataTable validationTable = new System.Data.DataTable();
        //    validationTable.Columns.Add("Academic Session");          
        //    validationTable.Columns.Add("Class");           
        //    validationTable.Columns.Add("Nucleus");
        //    validationTable.Columns.Add("Exam Required");
        //    validationTable.Columns.Add("PI Required");
        //    validationTable.Columns.Add("Document Required");
        //    validationTable.Columns.Add("Organization");
        //    validationTable.TableName = "StandardSetting_Details";

        //    var worksheet = oWB.AddWorksheet(validationTable);
        //    worksheet.Column(1).SetDataValidation().List(wsData.Range("Academic Session"), true);
        //    worksheet.Column(1).SetDataValidation().ShowErrorMessage = true;
        //    worksheet.Column(1).SetDataValidation().ErrorTitle = "Warning";
        //    worksheet.Column(1).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
        //    worksheet.Column(1).SetDataValidation().ErrorMessage = "Please Select Session From  List";

        //    worksheet.Column(2).SetDataValidation().InCellDropdown = true;
        //    worksheet.Column(2).SetDataValidation().Operator = XLOperator.Between;
        //    worksheet.Column(2).SetDataValidation().AllowedValues = XLAllowedValues.List;
        //    worksheet.Column(2).SetDataValidation().List("=INDIRECT(SUBSTITUTE(A1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
        //    worksheet.Column(2).SetDataValidation().ShowErrorMessage = true;
        //    worksheet.Column(2).SetDataValidation().ErrorTitle = "Warning";
        //    worksheet.Column(2).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
        //    worksheet.Column(2).SetDataValidation().ErrorMessage = "Please Select From  List";


        //    worksheet.Column(3).SetDataValidation().List(wsData.Range("Common"), true);
        //    worksheet.Column(3).SetDataValidation().ShowErrorMessage = true;
        //    worksheet.Column(3).SetDataValidation().ErrorTitle = "Warning";
        //    worksheet.Column(3).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
        //    worksheet.Column(3).SetDataValidation().ErrorMessage = "Please Select Nucleus  List";

        //    worksheet.Column(4).SetDataValidation().List(wsData.Range("Common"), true);
        //    worksheet.Column(4).SetDataValidation().ShowErrorMessage = true;
        //    worksheet.Column(4).SetDataValidation().ErrorTitle = "Warning";
        //    worksheet.Column(4).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
        //    worksheet.Column(4).SetDataValidation().ErrorMessage = "Please Select Exam Required From  List";

        //    worksheet.Column(5).SetDataValidation().List(wsData.Range("Common"), true);
        //    worksheet.Column(5).SetDataValidation().ShowErrorMessage = true;
        //    worksheet.Column(5).SetDataValidation().ErrorTitle = "Warning";
        //    worksheet.Column(5).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
        //    worksheet.Column(5).SetDataValidation().ErrorMessage = "Please Select PI Required From  List";

        //    worksheet.Column(6).SetDataValidation().List(wsData.Range("Common"), true);
        //    worksheet.Column(6).SetDataValidation().ShowErrorMessage = true;
        //    worksheet.Column(6).SetDataValidation().ErrorTitle = "Warning";
        //    worksheet.Column(6).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
        //    worksheet.Column(6).SetDataValidation().ErrorMessage = "Please Select Document Required From  List";

        //    worksheet.Column(7).SetDataValidation().InCellDropdown = true;
        //    worksheet.Column(7).SetDataValidation().Operator = XLOperator.Between;
        //    worksheet.Column(7).SetDataValidation().AllowedValues = XLAllowedValues.List;
        //    worksheet.Column(7).SetDataValidation().List("=INDIRECT(SUBSTITUTE(B1," + '"' + " " + '"' + "," + '"' + "_" + '"' + "))", true);
        //    worksheet.Column(7).SetDataValidation().ShowErrorMessage = true;
        //    worksheet.Column(7).SetDataValidation().ErrorTitle = "Warning";
        //    worksheet.Column(7).SetDataValidation().ErrorStyle = XLErrorStyle.Warning;
        //    worksheet.Column(7).SetDataValidation().ErrorMessage = "Please Select From  List";


        //    wsData.Hide();
        //   // wsData1.Hide();
        //    Byte[] workbookBytes;
        //    MemoryStream ms = GetStream(oWB);
        //    workbookBytes = ms.ToArray();
        //    return File(workbookBytes, "application/ms-excel", $"StandardSettingSample.xlsx");
        //}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadAdmissionStandardSetting(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[8];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            long sessionId = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            long organisationId = Convert.ToInt64(firstWorksheet.Cells[i, 2].Value.ToString().Split('_')[1]);
                            long boardId = Convert.ToInt64(firstWorksheet.Cells[i, 3].Value.ToString().Split('_')[1]);
                            long classid = Convert.ToInt64(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]);

                            var organisationlist = GetSchoolListByAcademicSession(sessionId);
                            var boardstandardlist = GetBoardStandardListByBoardAndStandard(boardId, classid);
                            long orgid = organisationlist.FirstOrDefault().ID.ToString() != null ? organisationlist.FirstOrDefault().ID : 0;
                            long brdstandid = boardstandardlist != null ? boardstandardlist.FirstOrDefault().ID : 0;

                            bool Nucleus = false;
                            if (firstWorksheet.Cells[i, 5].Value != null && firstWorksheet.Cells[i, 5] != null)
                            {
                                Nucleus = Convert.ToBoolean(firstWorksheet.Cells[i, 5].Value.ToString());
                            }
                            bool ExamRequired = false;
                            if (firstWorksheet.Cells[i, 6].Value != null && firstWorksheet.Cells[i, 6] != null)
                            {
                                ExamRequired = Convert.ToBoolean(firstWorksheet.Cells[i, 6].Value.ToString());
                            }
                            bool PIRequired = false;
                            if (firstWorksheet.Cells[i, 7].Value != null && firstWorksheet.Cells[i, 7] != null)
                            {
                                PIRequired = Convert.ToBoolean(firstWorksheet.Cells[i, 7].Value.ToString());
                            }
                            bool DocumentRequired = false;
                            if (firstWorksheet.Cells[i, 8].Value != null && firstWorksheet.Cells[i, 8] != null)
                            {
                                DocumentRequired = Convert.ToBoolean(firstWorksheet.Cells[i, 8].Value.ToString());
                            }
                            if (sessionId != 0 && orgid != 0 && brdstandid != 0)
                            {
                                var list = admissionRepository.GetAllAdmissionStandardSetting().Where(a => a.AcademicSessionId == sessionId && a.BoardStandardId == brdstandid && a.OrganizationId == orgid && a.Active == true).FirstOrDefault();
                                if (list == null)
                                {
                                    AdmissionStandardSetting slot = new AdmissionStandardSetting();
                                    slot.AcademicSessionId = sessionId;
                                    slot.BoardStandardId = brdstandid;
                                    slot.OrganizationId = orgid;
                                    slot.Nucleus = Nucleus;
                                    slot.ExamRequired = ExamRequired;
                                    slot.PIRequired = PIRequired;
                                    slot.DocumentRequired = DocumentRequired;
                                    slot.Active = true;
                                    slot.WingId = 0;
                                    slot.Code = " ";
                                    slot.ModifiedId = userId;
                                    slot.InsertedId = userId;
                                    slot.IsAvailable = true;
                                    slot.ModifiedId = userId;
                                    slot.Status = EntityStatus.ACTIVE;
                                    admissionRepository.CreateAdmissionStandardSetting(slot);
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionStandardSetting)).WithSuccess("Standard Setting", "Excel Uploaded Successfully"); ;
        }
        #endregion

        #region----------AdmissionStandardSeatQuota---------------------
        public IActionResult AdmissionStandardSeatQuota()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_SeatIntake", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ResponseAdmissionStandardSeatQuotaModal list = new ResponseAdmissionStandardSeatQuotaModal();
            ViewBag.Session = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();
            var AdmissionStandardSeatQuotalist = admissionStandardSeatQuotaRepository.ListAllAsyncIncludeAll().Result;
            var session = academicSessionRepository.ListAllAsyncIncludeAll().Result;
            var boardStandard = boardStandardRepository.ListAllAsyncIncludeAll().Result;
            var organisationacademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result;
            var organisation = organizationRepository.GetAllOrganization().ToList();
            var standardlist = standardRepository.GetAllStandard().ToList();
            var board = boardRepository.GetAllBoard().ToList();
            var res = (from a in AdmissionStandardSeatQuotalist
                       join b in session on a.AcademicSessionId equals b.ID
                       join c in boardStandard on a.BoardStandardId equals c.ID
                       join d in standardlist on c.StandardId equals d.ID
                       join e in board on a.BoardId equals e.ID
                       join f in organisationacademic on a.OrganizationAcademicId equals f.ID
                       join g in organisation on f.OrganizationId equals g.ID
                       select new AdmissionStandardSeatQuotaModal
                       {
                           NoOfSeat = a.NoOfSeats,
                           ID = a.ID,
                           AcademicSessionName = b.DisplayName,
                           BoardStandardId = c.ID,
                           StandardName = d.Name,
                           InsertedDate = a.InsertedDate,
                           Status = a.Status,
                           BoardId = a.BoardId,
                           BoardName = e.Name,
                           OrganisationAcademicId = a.OrganizationAcademicId,
                           OrganisationName = g.Name
                       }
                       ).ToList();
            list.StandardSeatQuotaList = res;
            list.AcademicSessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();
            list.BoardList = boardRepository.GetAllBoard().ToList();
            return View(list);
        }
        public async Task<IActionResult> DeleteAdmissionStandardSeatQuota(int id)
        {
            CheckLoginStatus();
            AdmissionStandardSeatQuota type = admissionStandardSeatQuotaRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            await admissionStandardSeatQuotaRepository.UpdateAsync(type);
            return RedirectToAction(nameof(AdmissionStandardSeatQuota)).WithSuccess("Admission Standard Seat Quote", "Deleted Successfully");

        }
        public async Task<IActionResult> SaveAdmissionStandardSeatQuota(long ID, long NoOfSeat, long AcademicSessionId, long OraganisationAcademicId, long BoardId, long BoardStandardId)
        {
            CheckLoginStatus();

            if (ID != 0 && ID.ToString() != null)
            {
                var list = admissionStandardSeatQuotaRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID != ID && a.AcademicSessionId == AcademicSessionId && a.BoardId == BoardId && a.BoardStandardId == BoardStandardId && a.OrganizationAcademicId == OraganisationAcademicId).FirstOrDefault();
                if (list == null)
                {
                    AdmissionStandardSeatQuota setting = admissionStandardSeatQuotaRepository.GetByIdAsyncIncludeAll(ID).Result;
                    setting.NoOfSeats = NoOfSeat;
                    setting.ModifiedId = userId;
                    setting.ModifiedDate = DateTime.Now;
                    await admissionStandardSeatQuotaRepository.UpdateAsync(setting);
                    return RedirectToAction(nameof(AdmissionStandardSeatQuota)).WithSuccess("Admission Standard Seat Quote", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(AdmissionStandardSeatQuota)).WithDanger("Admission Standard Seat Quote", "Already Present .");

                }
            }
            else
            {
                var list = admissionStandardSeatQuotaRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.BoardId == BoardId && a.BoardStandardId == BoardStandardId && a.OrganizationAcademicId == OraganisationAcademicId).FirstOrDefault();
                if (list == null)
                {
                    AdmissionStandardSeatQuota setting = new AdmissionStandardSeatQuota();
                    setting.NoOfSeats = NoOfSeat;
                    setting.AcademicSessionId = AcademicSessionId;
                    setting.OrganizationAcademicId = OraganisationAcademicId;
                    setting.WindId = 0;
                    setting.StreamId = 0;
                    setting.BoardId = BoardId;
                    setting.BoardStandardId = BoardStandardId;
                    setting.ModifiedId = userId;
                    setting.InsertedId = userId;
                    setting.IsAvailable = true;
                    setting.Active = true;
                    setting.Status = EntityStatus.ACTIVE;
                    setting.InsertedDate = DateTime.Now;
                    setting.ModifiedDate = DateTime.Now;
                    await admissionStandardSeatQuotaRepository.AddAsync(setting);
                    return RedirectToAction(nameof(AdmissionStandardSeatQuota)).WithSuccess("Admission Standard Seat Quote", "Saved Successfully");

                }
                else
                {
                    return RedirectToAction(nameof(AdmissionStandardSeatQuota)).WithDanger("Admission Standard Seat Quote", "Already Present .");

                }

            }



        }


        public IActionResult DownloadAdmissionStandardSeatQuotaSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();

            var BoardList = boardRepository.GetAllBoard().ToList();
            var ClassList = standardRepository.GetAllStandard().ToList();
            var OrganisationList = organizationRepository.GetAllOrganization().Where(a => a.Active == true).ToList();
            var SessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();



            System.Data.DataTable blooddt = new System.Data.DataTable();
            blooddt.Columns.Add("SessionName");
            foreach (var a in SessionList)
            {
                DataRow dr = blooddt.NewRow();
                dr["SessionName"] = "S_" + a.ID + "_" + a.DisplayName;
                blooddt.Rows.Add(dr);
            }
            blooddt.TableName = "Session";

            System.Data.DataTable categorydt = new System.Data.DataTable();
            categorydt.Columns.Add("OrganisationName");
            foreach (var a in OrganisationList)
            {
                DataRow dr = categorydt.NewRow();
                dr["OrganisationName"] = "O_" + a.ID + "_" + a.Name;
                categorydt.Rows.Add(dr);
            }
            categorydt.TableName = "Organisation";

            System.Data.DataTable nationalitydt = new System.Data.DataTable();
            nationalitydt.Columns.Add("BoardName");
            foreach (var a in BoardList)
            {
                DataRow dr = nationalitydt.NewRow();
                dr["BoardName"] = "B_" + a.ID + "_" + a.Name;
                nationalitydt.Rows.Add(dr);
            }
            nationalitydt.TableName = "Board";

            System.Data.DataTable religiondt = new System.Data.DataTable();
            religiondt.Columns.Add("ClassName");
            foreach (var a in ClassList)
            {
                DataRow dr = religiondt.NewRow();
                dr["ClassName"] = "C_" + a.ID + "_" + a.Name;
                religiondt.Rows.Add(dr);
            }
            religiondt.TableName = "Class";



            int lastCellNo1 = blooddt.Rows.Count + 1;
            int lastCellNo2 = categorydt.Rows.Count + 1;
            int lastCellNo3 = nationalitydt.Rows.Count + 1;
            int lastCellNo4 = religiondt.Rows.Count + 1;

            oWB.AddWorksheet(blooddt);
            oWB.AddWorksheet(categorydt);
            oWB.AddWorksheet(nationalitydt);
            oWB.AddWorksheet(religiondt);

            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            var worksheet3 = oWB.Worksheet(3);
            var worksheet4 = oWB.Worksheet(4);


            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("Session (Compulsory)");
            validationTable.Columns.Add("Organisation (Compulsory)");
            validationTable.Columns.Add("Board (Compulsory)");
            validationTable.Columns.Add("Class (Compulsory)");
            validationTable.Columns.Add("NoOfSeat (Compulsory)");

            validationTable.TableName = "SeatInteckSample";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(2).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(3).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
            worksheet.Column(4).SetDataValidation().List(worksheet4.Range("A2:A" + lastCellNo4), true);

            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();
            worksheet4.Hide();

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"SeatInteckSample.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadAdmissionStandardSeatQuota(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[4];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            long sessionId = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            long organisationId = Convert.ToInt64(firstWorksheet.Cells[i, 2].Value.ToString().Split('_')[1]);
                            long boardId = Convert.ToInt64(firstWorksheet.Cells[i, 3].Value.ToString().Split('_')[1]);
                            long classid = Convert.ToInt64(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]);

                            var organisationlist = GetOrganisationAcademicByAcademicSessionId(sessionId);
                            var boardstandardlist = GetBoardStandardListByBoardAndStandard(boardId, classid);
                            long orgid = organisationlist.FirstOrDefault().Id.ToString() != null ? organisationlist.FirstOrDefault().Id : 0;
                            long brdstandid = boardstandardlist != null ? boardstandardlist.FirstOrDefault().ID : 0;


                            long NoOfSeat = 0;
                            if (firstWorksheet.Cells[i, 5].Value != null && firstWorksheet.Cells[i, 5] != null)
                            {
                                NoOfSeat = Convert.ToInt64(firstWorksheet.Cells[i, 5].Value.ToString());
                            }
                            if (sessionId != 0 && orgid != 0 && brdstandid != 0)
                            {
                                var list = admissionStandardSeatQuotaRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == sessionId && a.BoardId == boardId && a.BoardStandardId == brdstandid && a.OrganizationAcademicId == orgid).FirstOrDefault();
                                if (list == null)
                                {
                                    AdmissionStandardSeatQuota setting = new AdmissionStandardSeatQuota();
                                    setting.NoOfSeats = NoOfSeat;
                                    setting.AcademicSessionId = sessionId;
                                    setting.OrganizationAcademicId = orgid;
                                    setting.WindId = 0;
                                    setting.StreamId = 0;
                                    setting.BoardId = boardId;
                                    setting.BoardStandardId = brdstandid;
                                    setting.ModifiedId = userId;
                                    setting.InsertedId = userId;
                                    setting.IsAvailable = true;
                                    setting.Active = true;
                                    setting.Status = EntityStatus.ACTIVE;
                                    setting.InsertedDate = DateTime.Now;
                                    setting.ModifiedDate = DateTime.Now;
                                    await admissionStandardSeatQuotaRepository.AddAsync(setting);

                                }
                                else
                                {
                                    AdmissionStandardSeatQuota setting = admissionStandardSeatQuotaRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == sessionId && a.BoardId == boardId && a.BoardStandardId == brdstandid && a.OrganizationAcademicId == orgid).FirstOrDefault();
                                    if (setting != null)
                                    {
                                        setting.NoOfSeats = NoOfSeat;
                                        setting.ModifiedId = userId;
                                        setting.ModifiedDate = DateTime.Now;
                                        await admissionStandardSeatQuotaRepository.UpdateAsync(setting);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionStandardSeatQuota)).WithSuccess("Standard Setting", "Excel Uploaded Successfully"); ;
        }
        #endregion

        #region-------------------Admission Stanadard Exam-----------------
        public JsonResult GetAcademicStandardFromExamDate(long BoardId, long OraganisationAcademicId)
        {
            var standardexam = admissionStandardExamRepository.ListAllAsyncIncludeAll().Result;
          //  var boardstandard = boardStandardRepository.ListAllAsync().Result.Where(a => a.BoardId == BoardId && a.Active == true).Select(a => a.ID).ToList();
            //var boardStandards = academicStandardRepository.ListAllAsync().Result.Where(a => a.OrganizationAcademicId == OraganisationAcademicId
            //    && a.Active == true && boardstandard.Contains(a.BoardStandardId)).Select(a => a.BoardStandardId).ToList();
            if (standardexam != null)
            {
                var stexam = standardexam.Where(a => a.Active == true && a.OrganizationId == OraganisationAcademicId).Select(a => a.BoardStandardId.Value).ToList();
               
                var standards = boardStandardRepository
                    .ListAllAsync().Result.Where(a => a.Active == true && stexam.Contains(a.ID) && a.BoardId==BoardId).Distinct().ToList();

                //var academicstandard = commonsqlquery.Get_view_All_Academic_Standards().Where(a => a.AcademicSessionId == AcademicSessionId && a.OraganisationAcademicId == OraganisationAcademicId).ToList();
                var res = (from a in standards
                           join b in StandardRepository.GetAllStandard() on a.StandardId equals b.ID
                           select new CommonMasterModel
                           {
                               ID = a.ID, //StandardId
                               Name = b.Name
                           }).ToList();
                return Json(res);

            }
            return Json(null);

        }
        public JsonResult CheckstandardSetting(long BoardStandardId)
        {
            int data = 0;
            var admissionstantardsetting = admissionRepository.GetAllAdmissionStandardSetting().Where(a => a.BoardStandardId == BoardStandardId).FirstOrDefault();
            if (admissionstantardsetting != null)
            {
                if (admissionstantardsetting.ExamRequired == true)
                {
                    data = 1;
                }

            }
            return Json(data);
        }
        public JsonResult GetStandardByOraganisationAcademicId(long OraganisationAcademicId)
        {
            var organisationacademic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == OraganisationAcademicId).Select(a => a.OrganizationId).ToList();
            var admissionstandardsetting = admissionRepository.GetAllAdmissionStandardSetting().Where(a => organisationacademic.Contains(a.OrganizationId.Value)).ToList();
            var boardstandard = boardStandardRepository.ListAllAsyncIncludeAll().Result.ToList();
            var standard = standardRepository.GetAllStandard().ToList();
            var list = (from a in admissionstandardsetting
                        join b in boardstandard on a.BoardStandardId equals b.ID
                        join c in standard on b.StandardId equals c.ID
                        select new CommonStudentModal
                        {
                            Id = a.BoardStandardId,
                            Name = c.Name
                        }
                      ).OrderByDescending(b => b.Id).ToList();
            return Json(list);
        }
        public IActionResult AdmissionStanadardExam()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_ExamDate", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ResponseAdmissionStanadardExamModal exam = new ResponseAdmissionStanadardExamModal();
            exam.AcademicSessionList = academicSessionRepository.
                ListAllAsyncIncludeAll().Result.Where(a => a.Active == true && a.IsAdmission == true).ToList();

            var organisationacademic = organizationAcademicRepository.ListAllAsync().Result.Where(a => a.Active == true).ToList();




            //exam.OrganisationList = organizationRepository.GetAllOrganization().ToList();
            //exam.WingList = wingRepository.ListAllAsyncIncludeAll().Result.ToList();

            var admissionstandardsetting = admissionRepository.GetAllAdmissionStandardSetting();
            var admissionstanadardexam = admissionStandardExamRepository.ListAllAsyncIncludeAll().Result;
            var academicsession = academicSessionRepository.ListAllAsyncIncludeAll().Result.ToList();
            var boardstandard = boardStandardRepository.ListAllAsyncIncludeAll().Result.ToList();
            var standard = standardRepository.GetAllStandard().ToList();
            var organisation = organizationRepository.GetAllOrganization().ToList();
            var academicstandardwing = academicStandardWingRepository.ListAllAsync().Result;
            var boardstandardwing = boardStandardWingRepository.ListAllAsync().Result;
            var questions = admissionRepository.GetAllAdmissionStandardExamQuestion();
            var winglist = wingRepository.ListAllAsyncIncludeAll().Result.ToList();
            var list = (from a in admissionstandardsetting
                        join b in boardstandard on a.BoardStandardId equals b.ID
                        join c in standard on b.StandardId equals c.ID
                        select new CommonStudentModal
                        {
                            Id = a.BoardStandardId,
                            Name = c.Name
                        }
                      ).OrderByDescending(b => b.Id).ToList();

            exam.CommonStudentModal = list.ToList();
            if (admissionstanadardexam == null)
            {
                exam.AdmissionStanadardExamList = null;
            }
            else
            {
                var res = (from a in admissionstanadardexam
                           join b in academicsession on a.AcademicSessionId equals b.ID
                           join c in boardstandard on a.BoardStandardId equals c.ID
                           join d in standard on c.StandardId equals d.ID
                           join e in organisationacademic on a.OrganizationId equals e.ID
                           join f in organisation on e.OrganizationId equals f.ID

                           select new AdmissionStanadardExamModal
                           {
                               ID = a.ID,
                               Status = a.Status,
                               InsertedDate = a.InsertedDate,
                               ResultDate = a.ResultDate,
                               Examdatestring = NullableDateTimeToStringDate(a.ExamDate),
                               ExamDate = a.ExamDate,
                               StandardName = a.BoardStandardId != 0 && d.Name != null ? d.Name : "NA",
                               AcademicSessionName = a.AcademicSessionId != 0 && b.DisplayName != null ? b.DisplayName : "NA",
                               ExamName = a.ExamTimeDisplayName,
                               ExamEndTime = a.ExamEndTime.Value,
                               ExamStartTime = a.ExamStartTime.Value,
                               ReachTime = a.ReachTime.Value,
                               OrganisationId = a.OrganizationId.Value,
                               OrganisationName = a.OrganizationId.Value != 0 && f.Name != null ? f.Name : "NA",
                               NoOfStudent = a.NoOfStudent,
                               WingId = a.WingId.Value,
                               IsPublished = a.IsPublishDate,
                               PublishEndDate = a.PublishEndDate,
                               PublishStartDate = a.PublishStartDate,
                               WingName = a.WingId == 0 ? "" : winglist.Where(z => boardstandardwing.Where(m => academicstandardwing.Where(s => s.ID == a.WingId).Select(v => v.BoardStandardWingID).Contains(m.ID)).Select(m => m.WingID).Contains(z.ID)).FirstOrDefault().Name,
                               ResultDatestring = NullableDateTimeToStringDate(a.ResultDate),
                               QuestionLink = questions != null ? (questions.Where(g => g.OrganizationId == a.OrganizationId && g.BoardStandardId == c.ID && g.AcademicSessionId == b.ID && g.Active == true).FirstOrDefault()==null?"": questions.Where(g => g.OrganizationId == a.OrganizationId && g.BoardStandardId == c.ID && g.AcademicSessionId == b.ID && g.Active == true).FirstOrDefault().QuestionUrl) : ""

                           }).ToList();
                exam.AdmissionStanadardExamList = res.ToList();
            }
            return View(exam);
        }
        public async Task<IActionResult> DeleteAdmissionStanadardExam(int id)
        {
            CheckLoginStatus();
            AdmissionStandardExam type = admissionStandardExamRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            await admissionStandardExamRepository.UpdateAsync(type);
            return RedirectToAction(nameof(AdmissionStanadardExam)).WithSuccess("Standard Exam Date And Time", "Deleted Successfully");
        }
        public async Task<IActionResult> SaveAdmissionStanadardExam(long ID, long AcademicSessionId, long BoardStandardId, Nullable<DateTime> ExamDate,
            bool chkpublish, Nullable<DateTime> publishsdate, Nullable<DateTime> publishedate,
            string DisplayName, TimeSpan ExamStartTime, TimeSpan ExamEndTime, TimeSpan ReachTime, long NoOfStudent, long OrganisationId, long WingId, Nullable<DateTime> ResultDate)
        {
            CheckLoginStatus();

            if (ID != 0 && ID.ToString() != null)
            {
                AdmissionStandardExam type = admissionStandardExamRepository.GetByIdAsyncIncludeAll(ID).Result;
                type.ExamDate = ExamDate;
                type.ResultDate = ResultDate;
                type.ExamTimeDisplayName = DisplayName;
                type.ExamStartTime = ExamStartTime;
                type.ExamEndTime = ExamEndTime;
                type.ReachTime = ReachTime;
                type.IsPublishDate = chkpublish;
                type.PublishEndDate = publishedate;
                type.PublishStartDate = publishsdate;
                type.ExamEndTime = ExamEndTime;
                type.ReachTime = ReachTime;
                type.NoOfStudent = NoOfStudent;
                type.ModifiedId = userId;
                type.ModifiedDate = DateTime.Now;
                await admissionStandardExamRepository.UpdateAsync(type);
                return RedirectToAction(nameof(AdmissionStanadardExam)).WithSuccess("Standard Exam Date And Time", "Updated Successfully");

            }
            else
            {
                var list = admissionStandardExamRepository.ListAllAsyncIncludeAll().Result == null ? null : admissionStandardExamRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AcademicSessionId == AcademicSessionId && b.OrganizationId == OrganisationId && b.BoardStandardId == BoardStandardId && b.ExamDate.Value.Date == ExamDate.Value.Date).FirstOrDefault();
                if (list == null)
                {
                    AdmissionStandardExam type = new AdmissionStandardExam();
                    type.AcademicSessionId = AcademicSessionId;
                    type.BoardStandardId = BoardStandardId;
                    type.OrganizationId = OrganisationId;
                    type.WingId = WingId;
                    type.ExamDate = ExamDate;
                    type.ResultDate = ResultDate;
                    type.ExamTimeDisplayName = DisplayName;
                    type.ExamStartTime = ExamStartTime;
                    type.ExamEndTime = ExamEndTime;
                    type.ReachTime = ReachTime;
                    type.NoOfStudent = NoOfStudent;
                    type.IsPublishDate = chkpublish;
                    type.PublishEndDate = publishedate;
                    type.PublishStartDate = publishsdate;
                    type.InsertedId = userId;
                    type.ModifiedId = userId;
                    type.InsertedDate = DateTime.Now;
                    type.ModifiedDate = DateTime.Now;
                    type.Status = EntityStatus.ACTIVE;
                    type.IsAvailable = true;
                    type.Active = true;
                    await admissionStandardExamRepository.AddAsync(type);
                }
                return RedirectToAction(nameof(AdmissionStanadardExam)).WithSuccess("Standard Exam Date And Time", "Saved Successfully");

            }

        }

        public IActionResult DownloadAdmissionStanadardExamSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();

            var admissionstandardsetting = admissionRepository.GetAllAdmissionStandardSetting();
            var boardstandard = boardStandardRepository.ListAllAsyncIncludeAll().Result.ToList();
            var standard = standardRepository.GetAllStandard().ToList();
            var SessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();
            var winglist = wingRepository.ListAllAsyncIncludeAll().Result.ToList();
            var boardstandardlist = (from a in admissionstandardsetting
                                     join b in boardstandard on a.BoardStandardId equals b.ID
                                     join c in standard on b.StandardId equals c.ID
                                     select new CommonStudentModal
                                     {
                                         Id = a.BoardStandardId,
                                         Name = c.Name
                                     }
                     ).OrderByDescending(b => b.Id).ToList();

            System.Data.DataTable blooddt = new System.Data.DataTable();
            blooddt.Columns.Add("SessionName");
            foreach (var a in SessionList)
            {
                DataRow dr = blooddt.NewRow();
                dr["SessionName"] = "S_" + a.ID + "_" + a.DisplayName;
                blooddt.Rows.Add(dr);
            }
            blooddt.TableName = "Session";

            System.Data.DataTable categorydt = new System.Data.DataTable();
            categorydt.Columns.Add("ClassName");
            foreach (var a in boardstandardlist)
            {
                DataRow dr = categorydt.NewRow();
                dr["ClassName"] = "C_" + a.Id + "_" + a.Name;
                categorydt.Rows.Add(dr);
            }
            categorydt.TableName = "Class";


            System.Data.DataTable orgdt = new System.Data.DataTable();
            orgdt.Columns.Add("OrganisationName");
            foreach (var a in organizationRepository.GetAllOrganization().ToList())
            {
                DataRow dr = orgdt.NewRow();
                dr["OrganisationName"] = "O_" + a.ID + "_" + a.Name;
                orgdt.Rows.Add(dr);
            }
            orgdt.TableName = "Organisation";

            System.Data.DataTable wingdt = new System.Data.DataTable();
            wingdt.Columns.Add("WingName");
            foreach (var a in winglist)
            {
                DataRow dr = wingdt.NewRow();
                dr["WingName"] = "W_" + a.ID + "_" + a.Name;
                wingdt.Rows.Add(dr);
            }
            wingdt.TableName = "Wing";

            int lastCellNo1 = blooddt.Rows.Count + 1;
            int lastCellNo2 = categorydt.Rows.Count + 1;
            int lastCellNo3 = orgdt.Rows.Count + 1;
            int lastCellNo4 = wingdt.Rows.Count + 1;
            oWB.AddWorksheet(blooddt);
            oWB.AddWorksheet(categorydt);
            oWB.AddWorksheet(orgdt);
            oWB.AddWorksheet(wingdt);
            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            var worksheet3 = oWB.Worksheet(3);
            var worksheet4 = oWB.Worksheet(4);


            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("Session (Compulsory)");
            validationTable.Columns.Add("Organisation (Compulsory)");
            validationTable.Columns.Add("Class (Compulsory)");
            validationTable.Columns.Add("Wing (Compulsory)");
            validationTable.Columns.Add("Display Name (Compulsory)");
            validationTable.Columns.Add("Exam Date(MM/DD/YYYY) (Compulsory)");
            validationTable.Columns.Add("Exam Start Time(00:00:00)");
            validationTable.Columns.Add("Exam End Time(00:00:00)");
            validationTable.Columns.Add("Reach Time(00:00:00)");
            validationTable.Columns.Add("NoOfStudent (Compulsory)");
            validationTable.Columns.Add("Result Date(MM/DD/YYYY)");
            validationTable.TableName = "AdmissionStanadardExamSample";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(2).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
            worksheet.Column(3).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(4).SetDataValidation().List(worksheet4.Range("A2:A" + lastCellNo2), true);

            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();
            worksheet4.Hide();

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"AdmissionStanadardExamSample.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadAdmissionStanadardExam(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[4];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            long AcademicSessionId = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            long BoardStandardId = Convert.ToInt64(firstWorksheet.Cells[i, 3].Value.ToString().Split('_')[1]);
                            long OrganisationId = Convert.ToInt64(firstWorksheet.Cells[i, 2].Value.ToString().Split('_')[1]);
                            long WingId = Convert.ToInt64(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]);


                            var organisationlist = GetSchoolListByAcademicSession(AcademicSessionId);
                            long orgid = organisationlist.FirstOrDefault().ID.ToString() != null ? organisationlist.FirstOrDefault().ID : 0;

                            var list = admissionStandardExamRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AcademicSessionId == AcademicSessionId && b.OrganizationId == orgid && b.BoardStandardId == BoardStandardId && b.WingId == WingId).FirstOrDefault();
                            if (list == null)
                            {
                                string starttm = "00:00:00";
                                string endtm = "00:00:00";
                                string reachtm = "00:00:00";
                                string ExamTimeDisplayName = " ";
                                long noofstudent = 0;
                                Nullable<DateTime> ResultDate = null;
                                Nullable<DateTime> ExamDate = null;
                                if (firstWorksheet.Cells[i, 5] != null && firstWorksheet.Cells[i, 5].Value != null)
                                {
                                    ExamTimeDisplayName = firstWorksheet.Cells[i, 5].Value.ToString();
                                }
                                if (firstWorksheet.Cells[i, 6] != null && firstWorksheet.Cells[i, 6].Value != null)
                                {
                                    ExamDate = Convert.ToDateTime(firstWorksheet.Cells[i, 6].Value.ToString());
                                }
                                if (firstWorksheet.Cells[i, 7] != null && firstWorksheet.Cells[i, 7].Value != null)
                                {
                                    starttm = Convert.ToDateTime(firstWorksheet.Cells[i, 7].Value).ToString("hh:mm:ss");
                                }
                                if (firstWorksheet.Cells[i, 8] != null && firstWorksheet.Cells[i, 8].Value != null)
                                {
                                    endtm = Convert.ToDateTime(firstWorksheet.Cells[i, 8].Value).ToString("hh:mm:ss");
                                }
                                if (firstWorksheet.Cells[i, 9] != null && firstWorksheet.Cells[i, 9].Value != null)
                                {
                                    reachtm = Convert.ToDateTime(firstWorksheet.Cells[i, 9].Value).ToString("hh:mm:ss");
                                }
                                if (firstWorksheet.Cells[i, 10] != null && firstWorksheet.Cells[i, 10].Value != null)
                                {
                                    noofstudent = Convert.ToInt64(firstWorksheet.Cells[i, 10].Value);
                                }
                                if (firstWorksheet.Cells[i, 11] != null && firstWorksheet.Cells[i, 11].Value != null)
                                {
                                    ResultDate = Convert.ToDateTime(firstWorksheet.Cells[i, 11].Value);
                                }

                                AdmissionStandardExam type = new AdmissionStandardExam();
                                type.AcademicSessionId = AcademicSessionId;
                                type.BoardStandardId = BoardStandardId;
                                type.WingId = WingId;
                                type.OrganizationId = orgid;
                                type.ExamTimeDisplayName = ExamTimeDisplayName;
                                type.ExamDate = ExamDate;
                                type.ExamStartTime = TimeSpan.Parse(starttm);
                                type.ExamEndTime = TimeSpan.Parse(endtm);
                                type.ReachTime = TimeSpan.Parse(reachtm);
                                type.NoOfStudent = noofstudent;
                                type.ResultDate = ResultDate;
                                type.InsertedId = userId;
                                type.ModifiedId = userId;
                                type.InsertedDate = DateTime.Now;
                                type.ModifiedDate = DateTime.Now;
                                type.Status = EntityStatus.ACTIVE;
                                type.IsAvailable = true;
                                type.Active = true;
                                await admissionStandardExamRepository.AddAsync(type);

                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionStanadardExam)).WithSuccess("Standard Exam Date And Time", "Excel uploaded successfully"); ;
        }

        #endregion

        #region--------------MapClassToAdmission----------------------------------------
        public IActionResult MapClassToAdmission()
        {
            try
            {
                long accessId = HttpContext.Session.GetInt32("accessId").Value;
                long roleId = HttpContext.Session.GetInt32("roleId").Value;
                if (commonMethods.checkaccessavailable("Master_MapClassToAdmission", accessId, "List", "Admission", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.Session = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true).ToList();
                ViewBag.Board = boardRepository.GetAllBoard().ToList();
                ViewBag.Class = standardRepository.GetAllStandard().ToList();
                ViewBag.Wing = wingRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();

                var mapclasslist = mapClassToAdmissionRepository.ListAllAsyncIncludeAll().Result.ToList();
                var session = academicSessionRepository.ListAllAsyncIncludeAll().Result;
                var Standard = standardRepository.GetAllStandard().ToList();
                var board = boardRepository.GetAllBoard().ToList();
                var wing = wingRepository.ListAllAsyncIncludeAll().Result.ToList();

                var res = (from a in mapclasslist
                           join b in session on a.AcademicSessionId equals b.ID
                           join c in board on a.BoardId equals c.ID
                           join d in Standard on a.ClassId equals d.ID
                           join e in wing on a.WingId equals e.ID
                           select new MapClassToAddmissionModal
                           {
                               ID = a.ID,
                               AcademicSessionId = a.AcademicSessionId,
                               ClassId = a.ClassId,
                               WingId = a.WingId,
                               BoardId = a.BoardId,
                               InsertedDate = a.InsertedDate,
                               Status = a.Status,
                               BoardName = c.Name,
                               SessionName = b.DisplayName,
                               WingName = e.Name,
                               ClassName = d.Name,
                           }
                           ).ToList();
                return View(res);
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        public async Task<IActionResult> SaveOrUpdateMapClassToAdmission(long ID, long AcademicSessionId, long BoardId, long ClassId, long WingId, long[] MultiWingId)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                var namechk = mapClassToAdmissionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID != ID && a.AcademicSessionId == AcademicSessionId && a.BoardId == BoardId && a.ClassId == ClassId && a.WingId == WingId).FirstOrDefault();
                if (namechk != null)
                {

                    return RedirectToAction(nameof(MapClassToAdmission)).WithDanger("Mapping Class To Admission", "Already Present.Please Enter Another...");
                }
                else
                {
                    MapClassToAdmission type = mapClassToAdmissionRepository.GetByIdAsync(ID).Result;
                    type.AcademicSessionId = AcademicSessionId;
                    type.BoardId = BoardId;
                    type.ClassId = ClassId;
                    type.WingId = WingId;
                    type.ModifiedId = userId;
                    type.ModifiedDate = DateTime.Now;
                    await mapClassToAdmissionRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(MapClassToAdmission)).WithSuccess("Mapping Class To Admission", "Updated Successfully");
                }

            }
            else
            {
                foreach (var wing in MultiWingId)
                {
                    var chk = mapClassToAdmissionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.BoardId == BoardId && a.ClassId == ClassId && a.WingId == wing).FirstOrDefault();
                    if (chk == null)
                    {
                        MapClassToAdmission type = new MapClassToAdmission();
                        type.AcademicSessionId = AcademicSessionId;
                        type.BoardId = BoardId;
                        type.ClassId = ClassId;
                        type.WingId = wing;
                        type.ModifiedId = userId;
                        type.ModifiedDate = DateTime.Now;
                        type.InsertedDate = DateTime.Now;
                        type.InsertedId = userId;
                        type.IsAvailable = true;
                        type.Active = true;
                        type.Status = EntityStatus.ACTIVE;
                        await mapClassToAdmissionRepository.AddAsync(type);
                    }
                }
                return RedirectToAction(nameof(MapClassToAdmission)).WithSuccess("Mapping Class To Admission", "Saved Successfully");

            }
        }
        public IActionResult DeleteMapClassToAdmission(int id)
        {
            CheckLoginStatus();
            MapClassToAdmission type = mapClassToAdmissionRepository.GetByIdAsync(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            mapClassToAdmissionRepository.UpdateAsync(type);
            return RedirectToAction(nameof(MapClassToAdmission)).WithSuccess("Mapped Class", "Deleted Successfully");
        }

        public IActionResult DownloadMapClassToAdmissionSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();

            var BoardList = boardRepository.GetAllBoard().ToList();
            var ClassList = standardRepository.GetAllStandard().ToList();
            var WingList = wingRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true).ToList();
            var SessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true).ToList();



            System.Data.DataTable blooddt = new System.Data.DataTable();
            blooddt.Columns.Add("SessionName");
            foreach (var a in SessionList)
            {
                DataRow dr = blooddt.NewRow();
                dr["SessionName"] = "S_" + a.ID + "_" + a.DisplayName;
                blooddt.Rows.Add(dr);
            }
            blooddt.TableName = "Session";

            System.Data.DataTable nationalitydt = new System.Data.DataTable();
            nationalitydt.Columns.Add("BoardName");
            foreach (var a in BoardList)
            {
                DataRow dr = nationalitydt.NewRow();
                dr["BoardName"] = "B_" + a.ID + "_" + a.Name;
                nationalitydt.Rows.Add(dr);
            }
            nationalitydt.TableName = "Board";

            System.Data.DataTable religiondt = new System.Data.DataTable();
            religiondt.Columns.Add("ClassName");
            foreach (var a in ClassList)
            {
                DataRow dr = religiondt.NewRow();
                dr["ClassName"] = "C_" + a.ID + "_" + a.Name;
                religiondt.Rows.Add(dr);
            }
            religiondt.TableName = "Class";


            System.Data.DataTable categorydt = new System.Data.DataTable();
            categorydt.Columns.Add("WingName");
            foreach (var a in WingList)
            {
                DataRow dr = categorydt.NewRow();
                dr["WingName"] = "W_" + a.ID + "_" + a.Name;
                categorydt.Rows.Add(dr);
            }
            categorydt.TableName = "Wing";



            int lastCellNo1 = blooddt.Rows.Count + 1;
            int lastCellNo2 = nationalitydt.Rows.Count + 1;
            int lastCellNo3 = religiondt.Rows.Count + 1;
            int lastCellNo4 = categorydt.Rows.Count + 1;


            oWB.AddWorksheet(blooddt);
            oWB.AddWorksheet(nationalitydt);
            oWB.AddWorksheet(religiondt);
            oWB.AddWorksheet(categorydt);


            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            var worksheet3 = oWB.Worksheet(3);
            var worksheet4 = oWB.Worksheet(4);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("Session");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Wing");
            validationTable.TableName = "Students";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(2).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(3).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
            worksheet.Column(4).SetDataValidation().List(worksheet4.Range("A2:A" + lastCellNo4), true);

            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();
            worksheet4.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"MappingClassToAdmissionSample.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadMapClassToAdmission(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {
                //if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
                //{
                //    return RedirectToAction("AuthenticationFailed", "Accounts");
                //}
                if (file != null)
                {

                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[4];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            MapClassToAdmission student = new MapClassToAdmission();

                            var list = mapClassToAdmissionRepository.ListAllAsyncIncludeAll().Result.ToList();
                            long AcademicSessionId = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            long BoardId = Convert.ToInt64(firstWorksheet.Cells[i, 2].Value.ToString().Split('_')[1]);
                            long ClassId = Convert.ToInt64(firstWorksheet.Cells[i, 3].Value.ToString().Split('_')[1]);
                            long WingId = Convert.ToInt64(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]);
                            var chk = list.Where(a => a.AcademicSessionId == AcademicSessionId && a.BoardId == BoardId && a.ClassId == ClassId && a.WingId == WingId).FirstOrDefault();
                            if (chk == null)
                            {
                                MapClassToAdmission type = new MapClassToAdmission();
                                type.AcademicSessionId = AcademicSessionId;
                                type.BoardId = BoardId;
                                type.ClassId = ClassId;
                                type.WingId = WingId;
                                type.ModifiedId = userId;
                                type.ModifiedDate = DateTime.Now;
                                type.InsertedDate = DateTime.Now;
                                type.InsertedId = userId;
                                type.IsAvailable = true;
                                type.Active = true;
                                type.Status = EntityStatus.ACTIVE;
                                await mapClassToAdmissionRepository.AddAsync(type);
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(MapClassToAdmission)).WithSuccess("Map Class To Admission", "Excel uploaded successfully"); ;
        }
        #endregion

        #region-----------------------------------Admission Standard Exam Result (Not Use)------
        public IActionResult AdmissionStandardExamResultDate()
        {
            try
            {
                long accessId = HttpContext.Session.GetInt32("accessId").Value;
                long roleId = HttpContext.Session.GetInt32("roleId").Value;
                if (commonMethods.checkaccessavailable("AdmissionStandardExamResultDate", accessId, "List", "Admission", roleId) == false)
                {
                    return RedirectToAction("AuthenticationFailed", "Accounts");
                }
                ViewBag.Session = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAvailable == true).ToList();
                ViewBag.Board = boardRepository.GetAllBoard().ToList();
                var resultlist = admissionStandardExamResultDateRepository.ListAllAsyncIncludeAll().Result.ToList();
                var session = academicSessionRepository.ListAllAsyncIncludeAll().Result;
                var boardstandard = boardStandardRepository.ListAllAsyncIncludeAll().Result.ToList();
                var organisationacdemic = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.ToList();
                var Standard = standardRepository.GetAllStandard().ToList();
                var organisation = organizationRepository.GetAllOrganization().ToList();
                var board = boardRepository.GetAllBoard().ToList();

                var res = (from a in resultlist
                           join b in session on a.AcademicSessionId equals b.ID
                           join c in boardstandard on a.BoardStandardId equals c.ID
                           join d in Standard on c.StandardId equals d.ID
                           join e in organisationacdemic on a.OrganisationAcademicId equals e.ID
                           join f in organisation on e.OrganizationId equals f.ID
                           join g in board on c.BoardId equals g.ID
                           select new AdmissionStandardExamResultModal
                           {
                               ID = a.ID,
                               AcademicSessionId = a.AcademicSessionId,
                               BoardStandardId = a.BoardStandardId,
                               InsertedDate = a.InsertedDate,
                               Status = a.Status,
                               SessionName = b.DisplayName,
                               ClassName = d.Name,
                               ResultDate = a.ExamResultDate,
                               ResultDateString = NullableDateTimeToStringDate(a.ExamResultDate),
                               OrganisationAcademicId = a.OrganisationAcademicId,
                               OrganisationName = f.Name,
                               BoardId = g.ID,
                               BoardName = g.Name,
                               OrganisationId = f.ID
                           }
                           ).ToList();
                return View(res);
            }
            catch (Exception ex)
            {
                return View();
            }
        }
        public IActionResult DeleteAdmissionStandardExamResultDate(int id)
        {
            CheckLoginStatus();
            AdmissionStandardExamResultDate type = admissionStandardExamResultDateRepository.GetByIdAsync(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            admissionStandardExamResultDateRepository.UpdateAsync(type);
            return RedirectToAction(nameof(AdmissionStandardExamResultDate)).WithSuccess("Standard Exam Result", "Deleted Successfully");
        }
        public async Task<IActionResult> SaveOrUpdateAdmissionStandardExamResultDate(long ID, long AcademicSessionId, long OraganisationAcademicId, long[] BoardStandardId, DateTime ExamResultDate)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                //var namechk = admissionStandardExamResultDateRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID != ID && a.AcademicSessionId == AcademicSessionId && a.OrganisationAcademicId == OraganisationAcademicId).FirstOrDefault();
                //if (namechk != null)
                //{

                //    return RedirectToAction(nameof(AdmissionStandardExamResultDate)).WithDanger("Admission Standard Exam Result Date", "Already Present.Please Enter Another...");
                //}
                //else
                //{
                AdmissionStandardExamResultDate type = admissionStandardExamResultDateRepository.GetByIdAsync(ID).Result;
                type.ExamResultDate = ExamResultDate;
                type.ModifiedId = userId;
                type.ModifiedDate = DateTime.Now;
                await admissionStandardExamResultDateRepository.UpdateAsync(type);
                return RedirectToAction(nameof(AdmissionStandardExamResultDate)).WithSuccess("Admission Standard Exam Result Date", "Updated Successfully");
                //}

            }
            else
            {
                foreach (var id in BoardStandardId)
                {
                    var chk = admissionStandardExamResultDateRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AcademicSessionId == AcademicSessionId && a.OrganisationAcademicId == OraganisationAcademicId && a.BoardStandardId == id).FirstOrDefault();
                    if (chk == null)
                    {
                        AdmissionStandardExamResultDate type = new AdmissionStandardExamResultDate();
                        type.AcademicSessionId = AcademicSessionId;
                        type.BoardStandardId = id;
                        type.OrganisationAcademicId = OraganisationAcademicId;
                        type.ExamResultDate = ExamResultDate;
                        type.ModifiedId = userId;
                        type.ModifiedDate = DateTime.Now;
                        type.InsertedDate = DateTime.Now;
                        type.InsertedId = userId;
                        type.IsAvailable = true;
                        type.Active = true;
                        type.Status = EntityStatus.ACTIVE;
                        await admissionStandardExamResultDateRepository.AddAsync(type);
                    }
                }
                return RedirectToAction(nameof(AdmissionStandardExamResultDate)).WithSuccess("Admission Standard Exam Result Date", "Saved Successfully");

            }
        }

        public IActionResult DownloadAdmissionStandardExamResultDateSample()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            //if (commonMethods.checkaccessavailable("Student", accessId, "List", "Student", roleId) == false)
            //{
            //    return RedirectToAction("AuthenticationFailed", "Accounts");
            //}
            XLWorkbook oWB = new XLWorkbook();

            var BoardList = boardRepository.GetAllBoard().ToList();
            var ClassList = standardRepository.GetAllStandard().ToList();
            var OrganisationList = organizationRepository.GetAllOrganization().Where(a => a.Active == true).ToList();
            var SessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();



            System.Data.DataTable blooddt = new System.Data.DataTable();
            blooddt.Columns.Add("SessionName");
            foreach (var a in SessionList)
            {
                DataRow dr = blooddt.NewRow();
                dr["SessionName"] = "S_" + a.ID + "_" + a.DisplayName;
                blooddt.Rows.Add(dr);
            }
            blooddt.TableName = "Session";

            System.Data.DataTable categorydt = new System.Data.DataTable();
            categorydt.Columns.Add("OrganisationName");
            foreach (var a in OrganisationList)
            {
                DataRow dr = categorydt.NewRow();
                dr["OrganisationName"] = "O_" + a.ID + "_" + a.Name;
                categorydt.Rows.Add(dr);
            }
            categorydt.TableName = "Organisation";

            System.Data.DataTable nationalitydt = new System.Data.DataTable();
            nationalitydt.Columns.Add("BoardName");
            foreach (var a in BoardList)
            {
                DataRow dr = nationalitydt.NewRow();
                dr["BoardName"] = "B_" + a.ID + "_" + a.Name;
                nationalitydt.Rows.Add(dr);
            }
            nationalitydt.TableName = "Board";

            System.Data.DataTable religiondt = new System.Data.DataTable();
            religiondt.Columns.Add("ClassName");
            foreach (var a in ClassList)
            {
                DataRow dr = religiondt.NewRow();
                dr["ClassName"] = "C_" + a.ID + "_" + a.Name;
                religiondt.Rows.Add(dr);
            }
            religiondt.TableName = "Class";






            int lastCellNo1 = blooddt.Rows.Count + 1;
            int lastCellNo2 = categorydt.Rows.Count + 1;
            int lastCellNo3 = nationalitydt.Rows.Count + 1;
            int lastCellNo4 = religiondt.Rows.Count + 1;



            oWB.AddWorksheet(blooddt);
            oWB.AddWorksheet(categorydt);
            oWB.AddWorksheet(nationalitydt);
            oWB.AddWorksheet(religiondt);



            var worksheet1 = oWB.Worksheet(1);
            var worksheet2 = oWB.Worksheet(2);
            var worksheet3 = oWB.Worksheet(3);
            var worksheet4 = oWB.Worksheet(4);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("Session (Compulsory)");
            validationTable.Columns.Add("Organisation (Compulsory)");
            validationTable.Columns.Add("Board (Compulsory)");
            validationTable.Columns.Add("Class (Compulsory)");
            validationTable.Columns.Add("Exam Result Date(YYYY-MM-DD)");
            validationTable.TableName = "ExamResultDateSample";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet.Column(2).SetDataValidation().List(worksheet2.Range("A2:A" + lastCellNo2), true);
            worksheet.Column(3).SetDataValidation().List(worksheet3.Range("A2:A" + lastCellNo3), true);
            worksheet.Column(4).SetDataValidation().List(worksheet4.Range("A2:A" + lastCellNo4), true);

            worksheet1.Hide();
            worksheet2.Hide();
            worksheet3.Hide();
            worksheet4.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"ExamResultDateSampleSample.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UploadAdmissionStandardExamResultDate(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[4];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            var sessionId = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            long organisationId = Convert.ToInt64(firstWorksheet.Cells[i, 2].Value.ToString().Split('_')[1]);
                            long boardId = Convert.ToInt64(firstWorksheet.Cells[i, 3].Value.ToString().Split('_')[1]);
                            long classid = Convert.ToInt64(firstWorksheet.Cells[i, 4].Value.ToString().Split('_')[1]);

                            var organisationlist = GetSchoolListByAcademicSession(sessionId);
                            var boardstandardlist = GetBoardStandardListByBoardAndStandard(boardId, classid);
                            long orgid = organisationlist.FirstOrDefault().ID.ToString() != null ? organisationlist.FirstOrDefault().ID : 0;
                            long brdstandid = boardstandardlist != null ? boardstandardlist.FirstOrDefault().ID : 0;

                            if (orgid != 0 && brdstandid != 0)
                            {
                                var list = admissionStandardExamResultDateRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AcademicSessionId == sessionId && b.OrganisationAcademicId == orgid && b.BoardStandardId == brdstandid).FirstOrDefault();
                                if (list == null)
                                {
                                    AdmissionStandardExamResultDate slot = new AdmissionStandardExamResultDate();
                                    slot.ModifiedId = userId;
                                    Nullable<DateTime> examresultdate = Convert.ToDateTime(firstWorksheet.Cells[i, 5].Value);
                                    slot.ExamResultDate = examresultdate.Value;
                                    slot.AcademicSessionId = sessionId;
                                    slot.OrganisationAcademicId = orgid;
                                    slot.BoardStandardId = boardstandardlist.FirstOrDefault().ID;
                                    slot.Active = true;
                                    slot.InsertedId = userId;
                                    slot.InsertedDate = DateTime.Now;
                                    slot.ModifiedDate = DateTime.Now;
                                    slot.IsAvailable = true;
                                    slot.ModifiedId = userId;
                                    slot.Status = EntityStatus.ACTIVE;
                                    admissionStandardExamResultDateRepository.AddAsync(slot);
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionStandardExamResultDate)).WithSuccess("Exam Result Date", "Excel uploaded successfully"); ;
        }



        #endregion

        #region-------------------Admission Stanadard PI-----------------


        public IActionResult AdmissionPersonalInterviewAssign()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_PIDateAndTime", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ResponseAdmissionStanadardPIModal exam = new ResponseAdmissionStanadardPIModal();
            exam.AcademicSessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true && a.IsAdmission == true).ToList();
            exam.Boardlist = boardRepository.GetAllBoard().ToList();
            exam.PersonalInterviewlist = admissionPersonalInterviewAssignRepository.ListAllAsyncIncludeAll().Result.Select(a => a.PIDate).Distinct().ToList();
            var admissionstandardsetting = admissionRepository.GetAllAdmissionStandardSetting();
            var admissionstanadardpi = admissionPersonalInterviewAssignRepository.ListAllAsyncIncludeAll().Result.ToList();
            var academicsession = academicSessionRepository.ListAllAsyncIncludeAll().Result.ToList();
            var boardstandard = boardStandardRepository.ListAllAsyncIncludeAll().Result.ToList();
            var standard = standardRepository.GetAllStandard().ToList();
            var organisation = organizationRepository.GetAllOrganization().ToList();
            var list = (from a in admissionstandardsetting
                        join b in boardstandard on a.BoardStandardId equals b.ID
                        join c in standard on b.StandardId equals c.ID
                        select new CommonStudentModal
                        {
                            Id = a.BoardStandardId,
                            Name = c.Name
                        }
                      ).OrderBy(b => b.Name).ToList();

            exam.CommonStudentModal = list.ToList();
            var employeelist = employeeRepository.GetAllEmployee().ToList();
            exam.EmployeeList = employeelist;
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result.ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();
            exam.AdmissionStudentList = GetstudentDetailsWithFormNumber();
            var res = (from a in admissionstanadardpi
                           //join b in employeelist on a.EmployeeId equals b.ID
                       join c in admissionstudentlist on a.AdmissionStudentId equals c.ID
                       select new AdmissionStanadardPIModal
                       {
                           ID = a.ID,
                           Status = a.Status,
                           AdmissionStudentId = a.AdmissionStudentId,
                           AdmissionStudentName = c.FirstName + " " + c.LastName,
                           InsertedDate = a.InsertedDate,
                           PIDateString = NullableDateTimeToStringDate(a.PIDate),
                           PIDate = a.PIDate,
                           PITime = a.PITime,
                           IsAppearedstring = a.IsAppeared != null ? (a.IsAppeared.Value==true?"Yes":"No") : "NA",
                           IsAppeared = a.IsAppeared,
                           IsQualifiedstring = a.IsQualified != null ? (a.IsQualified.Value == true ? "Yes" : "No") : "NA",
                          
                         // IsQualified = a.IsQualified,
                           //AppearedDateTime = a.AppearedDateTime,
                           //AppearedDateTimeString = a.AppearedDateTime != null ? a.AppearedDateTime.Value.ToString("dd-MMM-yyyy hh:mm tt") : "NA",
                           //AssignedCounselorName= employeelist.Where(z=>z.ID==a.AssignedCounselorId).FirstOrDefault().FirstName,
                          // EmployeeId = a.EmployeeId,
                           //EmployeeName=b.FirstName +" "+b.LastName,
                           Remarks = a.Remarks != null ? a.Remarks : "",
                           //Score = a.Score,
                           ScoreString=a.Score!=null?(a.Score.Value==0? "NA": a.Score.Value.ToString()) :"NA"

                       }).ToList();
            exam.AdmissionStanadardPIList = res.ToList();
            return View(exam);
        }
        public async Task<IActionResult> DeleteAdmissionPersonalInterviewAssign(int id)
        {
            CheckLoginStatus();
            AdmissionPersonalInterviewAssign type = admissionPersonalInterviewAssignRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            await admissionPersonalInterviewAssignRepository.UpdateAsync(type);
            return RedirectToAction(nameof(AdmissionPersonalInterviewAssign)).WithSuccess("Admission Standard Setting", "Deleted Successfully");
        }
        public async Task<IActionResult>
            SaveOrUpdateAdmissionPersonalInterviewAssign(long ID, long AdmissionStudentId, long[] AdmissionStudentIdMulti,
            Nullable<DateTime> PIDate, TimeSpan PITime,
            long EmployeeId)
        {
            CheckLoginStatus();

            if (ID != 0 && ID.ToString() != null)
            {
                var list = admissionPersonalInterviewAssignRepository.ListAllAsyncIncludeAll().Result.Where(b => b.ID != ID && b.AdmissionStudentId == AdmissionStudentId && b.PIDate.Value.Date == PIDate.Value.Date).FirstOrDefault();
                if (list == null)
                {
                    AdmissionPersonalInterviewAssign type = admissionPersonalInterviewAssignRepository.GetByIdAsyncIncludeAll(ID).Result;
                    type.PIDate = PIDate;
                    type.PITime = PITime;
                    type.IsAppeared = null;
                    type.IsQualified = null;
                    //type.EmployeeId = EmployeeId;
                    type.ModifiedId = userId;
                    type.ModifiedDate = DateTime.Now;
                    await admissionPersonalInterviewAssignRepository.UpdateAsync(type);

                    string pidate = type.PIDate.Value.ToString("dd-MMM-yyyy");
                    string pitime = GetDateTimeFormat(type.PIDate.Value, type.PITime.Value);

                    long admissionid = admissionStudentRepository.GetByIdAsync(type.AdmissionStudentId).Result.AdmissionId;

                    AdmissionTimeline timeline = new AdmissionTimeline();
                    timeline.Timeline = "PI Date and Time : " +
                        GetDateTimeFormat(type.PIDate.Value, type.PITime.Value);
                    timeline.Active = true;
                    timeline.IsAvailable = true;
                    timeline.AdmissionId = admissionid;
                    timeline.Status = "PI Date and Time";
                    timeline.InsertedDate = DateTime.Now;
                    timeline.ModifiedDate = DateTime.Now;
                    timeline.InsertedId = userId;
                    timeline.ModifiedId = userId;
                    admissionRepository.CreateAdmissionTimeline(timeline);
                    #region--------- smg And Email (Complited)------------------

                    var details = studentAdmissionMethod.GetStudentDetailsByStudentId(type.AdmissionStudentId);
                    string Message = HttpAddSmg.PIDate_Time.Replace("{{StudentName}}", details.FullName).Replace("{{PI Date}}", pidate).Replace("{{PI Time}}", pitime);
                    if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                    {
                        smssend.SendPISms(details.Mobile, Message);
                    }
                    if (details.EmailId != null && details.EmailId != "")
                    {
                        Emailsend.PIDateAssign(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName, details.FormNumber, details.FullName, details.Mobile, details.EmailId, pidate, pitime, details.PiDetails.InterviewerName, Message, details.EmailId);
                    }
                    #endregion
                    return RedirectToAction(nameof(AdmissionPersonalInterviewAssign)).WithSuccess("Student PI", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(AdmissionPersonalInterviewAssign)).WithWarning("Student PI ", "Already Assigned");
                }
            }
            else
            {
                foreach (var studentid in AdmissionStudentIdMulti)
                {
                    var list = admissionPersonalInterviewAssignRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AdmissionStudentId == studentid && b.PIDate.Value.Date == PIDate.Value.Date).FirstOrDefault();
                    if (list == null)
                    {
                        AdmissionPersonalInterviewAssign type = new AdmissionPersonalInterviewAssign();
                        type.PIDate = PIDate;
                        type.PITime = PITime;
                        type.AdmissionStudentId = studentid;
                        type.EmployeeId = 0;
                        type.AssignedCounselorId = 0;
                        type.InsertedId = userId;
                        type.ModifiedId = userId;
                        type.InsertedDate = DateTime.Now;
                        type.ModifiedDate = DateTime.Now;
                        type.Status = EntityStatus.ACTIVE;
                        type.IsAvailable = true;
                        type.Active = true;
                        await admissionPersonalInterviewAssignRepository.AddAsync(type);
                        string pidate = type.PIDate.Value.ToString("dd-MMM-yyyy");
                        string pitime = GetDateTimeFormat(type.PIDate.Value, type.PITime.Value);

                        long admissionid = admissionStudentRepository.GetByIdAsync(type.AdmissionStudentId).Result.AdmissionId;

                        AdmissionTimeline timeline = new AdmissionTimeline();
                        timeline.Timeline = "PI Date and Time : " +
                            GetDateTimeFormat(type.PIDate.Value, type.PITime.Value);
                        timeline.Active = true;
                        timeline.IsAvailable = true;
                        timeline.AdmissionId = admissionid;
                        timeline.Status = "PI Date and Time";
                        timeline.InsertedDate = DateTime.Now;
                        timeline.ModifiedDate = DateTime.Now;
                        timeline.InsertedId = userId;
                        timeline.ModifiedId = userId;
                        admissionRepository.CreateAdmissionTimeline(timeline);
                        #region--------- smg And Email (Complited)------------------

                        var details = studentAdmissionMethod.GetStudentDetailsByStudentId(type.AdmissionStudentId);
                        string Message = HttpAddSmg.PIDate_Time.Replace("{{StudentName}}", details.FullName).Replace("{{PI Date}}", pidate).Replace("{{PI Time}}", pitime);
                        if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                        {
                            smssend.SendPISms(details.Mobile, Message);
                        }
                        if (details.EmailId != null && details.EmailId != "")
                        {
                            Emailsend.PIDateAssign(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName, details.FormNumber, details.FullName, details.Mobile, details.EmailId, pidate, pitime, details.PiDetails.InterviewerName, Message, details.EmailId);
                        }
                        #endregion

                    }

                }
                return RedirectToAction(nameof(AdmissionPersonalInterviewAssign)).WithSuccess("Student PI", "Saved Successfully");


            }

        }

        public IActionResult DownloadAdmissionPersonalInterviewAssign(long AcademicSessionId, long? OraganisationAcademicId, long? BoardId, long? BoardStandardId, long? WingId)
        {
            CheckLoginStatus();
            XLWorkbook oWB = new XLWorkbook();
            var AdmissionStudent = admissionStudentRepository.ListAllAsyncIncludeAll().Result.ToList();
            var Employee = employeeRepository.GetAllEmployee().ToList();
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result.ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();
            var studentlist = GetstudentDetailsWithFilter(true, true);
            if (OraganisationAcademicId != null && OraganisationAcademicId != 0)
            {
                long OraganisationId = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == OraganisationAcademicId).FirstOrDefault().OrganizationId;
                studentlist = studentlist.Where(a => a.OrganisationId == OraganisationId).ToList();
            }
            if (BoardId != null && BoardId != 0)
            {
                studentlist = studentlist.Where(a => a.BoardId == BoardId).ToList();
            }
            if (BoardStandardId != null && BoardStandardId != 0)
            {
                long StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().StandardId;
                studentlist = studentlist.Where(a => a.ClassId == StandardId).ToList();
            }
            if (WingId != null && WingId != 0)
            {
                studentlist = studentlist.Where(a => a.WingId == WingId).ToList();
            }
            System.Data.DataTable blooddt = new System.Data.DataTable();
            blooddt.Columns.Add("StudentName");
            foreach (var a in studentlist)
            {
                DataRow dr = blooddt.NewRow();
                dr["StudentName"] = "S_" + a.Id + "_" + a.FormNumber + "-" + a.Name;
                blooddt.Rows.Add(dr);
            }
            blooddt.TableName = "Student";



            //System.Data.DataTable orgdt = new System.Data.DataTable();
            //orgdt.Columns.Add("EmployeeName");
            //foreach (var a in Employee)
            //{
            //    DataRow dr = orgdt.NewRow();
            //    dr["EmployeeName"] = "E_" + a.ID + "_" + a.FirstName +" "+a.LastName;
            //    orgdt.Rows.Add(dr);
            //}
            //orgdt.TableName = "Employee";

            //System.Data.DataTable examdt = new System.Data.DataTable();
            //examdt.Columns.Add("Appeared");
            //DataRow dr3 = examdt.NewRow();
            //dr3["Appeared"] = "True";
            //examdt.Rows.Add(dr3);
            //DataRow dr4 = examdt.NewRow();
            //dr4["Appeared"] = "False";
            //examdt.Rows.Add(dr4);
            //examdt.TableName = "PI";

            int lastCellNo1 = blooddt.Rows.Count + 1;
            oWB.AddWorksheet(blooddt);
            var worksheet1 = oWB.Worksheet(1);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("AdmissionStudent (Compulsory)");
            validationTable.Columns.Add("PI Date (MM/DD/YYYY)");
            validationTable.Columns.Add("PI Time(00:00)");

            validationTable.TableName = "StudentPIDetails";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"StudentPIDetailsSample.xlsx");
            //return RedirectToAction(nameof(AdmissionPersonalInterviewAssign)).WithSuccess("Student PI Sample", "Download Excel Successfully"); 
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadAdmissionPersonalInterviewAssign(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            string PITime = "00:00:00";
                            //bool PI = false;                          
                            long studentid = 0;
                            // long employeeid= Convert.ToInt64(firstWorksheet.Cells[i, 2].Value.ToString().Split('_')[1]);
                            if (firstWorksheet.Cells[i, 1] != null && firstWorksheet.Cells[i, 1].Value != null)
                            {
                                studentid = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            }
                            if (firstWorksheet.Cells[i, 4] != null && firstWorksheet.Cells[i, 3].Value != null)
                            {
                                PITime = Convert.ToDateTime(firstWorksheet.Cells[i, 3].Value).ToString("hh:mm:ss");
                            }


                            Nullable<DateTime> Pidate = Convert.ToDateTime(firstWorksheet.Cells[i, 2].Value);

                            //if (firstWorksheet.Cells[i, 5] != null && firstWorksheet.Cells[i, 5].Value != null)
                            //{
                            //    PI =  Convert.ToBoolean(firstWorksheet.Cells[i, 5].Value.ToString());
                            //}                                                   
                            //if(PI==true)
                            //{
                            var list = admissionPersonalInterviewAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == studentid && a.PIDate.Value.Date == Pidate.Value.Date).FirstOrDefault();
                            if (list == null)
                            {
                                AdmissionPersonalInterviewAssign type = new AdmissionPersonalInterviewAssign();
                                type.PIDate = Pidate;
                                type.PITime = TimeSpan.Parse(PITime);
                                type.AdmissionStudentId = studentid;
                                type.EmployeeId = 0;
                                type.AssignedCounselorId = 0;
                                type.InsertedId = userId;
                                type.ModifiedId = userId;
                                type.InsertedDate = DateTime.Now;
                                type.ModifiedDate = DateTime.Now;
                                type.Status = EntityStatus.ACTIVE;
                                type.IsAvailable = true;
                                type.Active = true;
                                await admissionPersonalInterviewAssignRepository.AddAsync(type);

                                string pidate = type.PIDate.Value.ToString("dd-MMM-yyyy");
                                string pitime = GetDateTimeFormat(type.PIDate.Value, type.PITime.Value);

                                long admissionid = admissionStudentRepository.GetByIdAsync(type.AdmissionStudentId).Result.AdmissionId;

                                AdmissionTimeline timeline = new AdmissionTimeline();
                                timeline.Timeline = "PI Date and Time : " +
                                    GetDateTimeFormat(type.PIDate.Value, type.PITime.Value);
                                timeline.Active = true;
                                timeline.IsAvailable = true;
                                timeline.AdmissionId = admissionid;
                                timeline.Status = "PI Date and Time";
                                timeline.InsertedDate = DateTime.Now;
                                timeline.ModifiedDate = DateTime.Now;
                                timeline.InsertedId = userId;
                                timeline.ModifiedId = userId;
                                admissionRepository.CreateAdmissionTimeline(timeline);
                                #region--------- smg And Email (Complited)------------------

                                var details = studentAdmissionMethod.GetStudentDetailsByStudentId(type.AdmissionStudentId);
                                string Message = HttpAddSmg.PIDate_Time.Replace("{{StudentName}}", details.FullName).Replace("{{PI Date}}", pidate).Replace("{{PI Time}}", pitime);
                                if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                                {
                                    smssend.SendPISms(details.Mobile, Message);
                                }
                                if (details.EmailId != null && details.EmailId != "")
                                {
                                    Emailsend.PIDateAssign(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName, details.FormNumber, details.FullName, details.Mobile, details.EmailId, pidate, pitime, details.PiDetails.InterviewerName, Message, details.EmailId);
                                }
                                #endregion
                            }
                            // }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionPersonalInterviewAssign)).WithSuccess("Admission Standard PI", "Excel Uploaded Successfully");
        }

        #endregion



        #region------------Admission Stanadard Document(DocumentDateAndTime)------------

        public List<studentStandardModal> GetstudentDocumentDetailsWithFilter(bool documentDateActiveCheck, bool documentDateDoFilter)
        {
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result;
            var paymentlist = admissionFormPaymentRepository.ListAllAsyncIncludeAll().Result.Select(a => a.AdmissionId).Distinct().ToList();
            var list = admissionstudentlist.Where(a => paymentlist.Contains(a.AdmissionId)).ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();
            var detailslist = admissionStudentStandardRepository.ListAllAsyncIncludeAll().Result;
            var organisation = organizationRepository.GetAllOrganization().ToList();
            var winglist = wingRepository.ListAllAsyncIncludeAll().Result.ToList();
            var board = boardRepository.GetAllBoard().ToList();
            var classlist = StandardRepository.GetAllStandard().ToList();
            var studentlist = (from a in list
                               join b in admissionlist on a.AdmissionId equals b.ID
                               join c in detailslist on a.ID equals c.AdmissionStudentId
                               select new studentStandardModal
                               {
                                   Id = a.ID,
                                   AdmissionStudentId = c.AdmissionStudentId,
                                   BoardId = c.BoardId,
                                   BoardName = board.Where(z => z.ID == c.BoardId).FirstOrDefault() != null ? board.Where(z => z.ID == c.BoardId).FirstOrDefault().Name : "NA",                                 
                                   WingName = winglist.Where(z => z.ID == c.ID).FirstOrDefault() != null ? winglist.Where(z => z.ID == c.ID).FirstOrDefault().Name : "NA",
                                   OrganisationName = organisation.Where(z => z.ID == c.OrganizationId).FirstOrDefault() != null ? organisation.Where(z => z.ID == c.OrganizationId).FirstOrDefault().Name : "NA",                                  
                                   ClassName = classlist.Where(z => z.ID == c.StandardId).FirstOrDefault() != null ? classlist.Where(z => z.ID == c.StandardId).FirstOrDefault().Name : "NA",
                                   OrganisationId = c.OrganizationId,
                                   ClassId = c.StandardId,
                                   Name = a.FirstName + " " + a.LastName,
                                   FormNumber = b.FormNumber != "" && b.FormNumber != null ? b.FormNumber : " ",
                                   StudentName = admissionstudentlist.Where(z => z.ID == c.AdmissionStudentId).Select(x => new { name = x.FirstName + " " + x.LastName }).FirstOrDefault().name,
                                   PiList = documentDateDoFilter == true ? studentAdmissionMethod.GetDocumentDetailsList(c.AdmissionStudentId) : null,
                               }
                                ).ToList().AsParallel();

            List<studentStandardModal> dataList = new List<studentStandardModal>();

            if (documentDateActiveCheck)
            {
                Parallel.ForEach(studentlist, item =>
                {

                    int exist = 0;
                    if (item.PiList != null && item.PiList.Count > 0)
                    {
                        foreach (var inneritem in item.PiList)
                        {
                            if (inneritem.active == 1)
                            {
                                exist = 1;
                            }
                        }

                    }

                    if (exist == 0)
                    {
                        dataList.Add(item);
                    }


                });
            }
            else
                dataList.AddRange(studentlist);




            return dataList;
        }


        public IActionResult AdmissionDocumentAssign()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_DocumentDateAndTime", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ResponseAdmissionStanadardDocumentModal exam = new ResponseAdmissionStanadardDocumentModal();
            exam.AcademicSessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Active == true && a.IsAdmission == true).ToList();
            exam.Boardlist = boardRepository.GetAllBoard().ToList();
            exam.DocumentDatelist = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.Select(a => a.VerificationDate).Distinct().ToList();
            var admissionstandardsetting = admissionRepository.GetAllAdmissionStandardSetting();
            var admissionstanadarddocument = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.ToList();
            var academicsession = academicSessionRepository.ListAllAsyncIncludeAll().Result.ToList();
            var boardstandard = boardStandardRepository.ListAllAsyncIncludeAll().Result.ToList();
            var standard = standardRepository.GetAllStandard().ToList();
            var organisation = organizationRepository.GetAllOrganization().ToList();
            var employeelist = employeeRepository.GetAllEmployee().ToList();
            var list = (from a in admissionstandardsetting
                        join b in boardstandard on a.BoardStandardId equals b.ID
                        join c in standard on b.StandardId equals c.ID
                        select new CommonStudentModal
                        {
                            Id = a.BoardStandardId,
                            Name = c.Name
                        }
                      ).OrderBy(b => b.Name).ToList();

            exam.CommonStudentModal = list.ToList();
            exam.EmployeeList = employeelist;
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result.ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();
            exam.AdmissionStudentList = GetstudentDetailsWithFormNumber();
            var res = (from a in admissionstanadarddocument
                           //join b in employeelist on a.EmployeeId equals b.ID
                       join c in admissionstudentlist on a.AdmissionStudentId equals c.ID
                       select new AdmissionStanadardDocumentModal
                       {
                           ID = a.ID,
                           Status = a.Status,
                           AdmissionStudentId = a.AdmissionStudentId,
                           AdmissionStudentName = c.FirstName + " " + c.LastName,
                           InsertedDate = a.InsertedDate,
                           DocumentDateString = NullableDateTimeToStringDate(a.VerificationDate),
                           DocumentDate = a.VerificationDate,
                           DocumentTime = a.VerificationTime,
                           IsAppeared = a.IsAppeared,
                           IsAppearedstring = a.IsAppeared != null ? (a.IsAppeared.Value == true ? "Yes" : "No") : "NA",
                           IsQualifiedstring = a.IsQualified != null ? (a.IsQualified.Value == true ? "Yes" : "No") : "NA",
                           IsQualified = a.IsQualified,
                           AppearedDateTime = a.AppearedDateTime,
                           AppearedDateTimeString = a.AppearedDateTime != null ? a.AppearedDateTime.Value.ToString("dd-MMM-yyyy hh:mm tt") : "NA",
                           //AssignedCounselorName= employeelist.Where(z=>z.ID==a.AssignedCounselorId).FirstOrDefault().FirstName,
                           EmployeeId = a.EmployeeId,
                           Remarks=a.Remarks!=null&&a.Remarks!=""?a.Remarks:""
                           //EmployeeName = b.FirstName + " " + b.LastName

                       }).ToList();
            exam.AdmissionStanadardDocumentList = res.ToList();
            return View(exam);
        }
        public async Task<IActionResult> DeleteAdmissionDocumentAssign(int id)
        {
            CheckLoginStatus();
            AdmissionPersonalInterviewAssign type = admissionPersonalInterviewAssignRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            await admissionPersonalInterviewAssignRepository.UpdateAsync(type);
            return RedirectToAction(nameof(AdmissionPersonalInterviewAssign)).WithSuccess("Document Assign Date And Time", "Deleted Successfully");
        }
        public async Task<IActionResult> SaveOrUpdateAdmissionDocumentAssign(long ID, long AdmissionStudentId, Nullable<DateTime> VerificationDate, TimeSpan VerificationTime,
            long EmployeeId, long[] AdmissionStudentIdMulti)
        {
            CheckLoginStatus();

            if (ID != 0 && ID.ToString() != null)
            {
                var list = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.Where(b => b.ID != ID && b.AdmissionStudentId == AdmissionStudentId && b.VerificationDate.Value.Date == VerificationDate.Value.Date).FirstOrDefault();
                if (list == null)
                {
                    AdmissionDocumentVerificationAssign type = admissionDocumentVerificationAssignRepository.GetByIdAsyncIncludeAll(ID).Result;
                    type.VerificationDate = VerificationDate;
                    type.VerificationTime = VerificationTime;
                    //type.EmployeeId = EmployeeId;
                    type.ModifiedId = userId;
                    type.ModifiedDate = DateTime.Now;
                    await admissionDocumentVerificationAssignRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(AdmissionDocumentAssign)).WithSuccess("Document Assign Date And Time", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(AdmissionDocumentAssign)).WithWarning("Student Document Assign Date And Time", "Already Present..!");
                }
            }
            else
            {
                foreach (var studentid in AdmissionStudentIdMulti)
                {
                    var list = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.Where(b => b.AdmissionStudentId == studentid && b.VerificationDate.Value.Date == VerificationDate.Value.Date).FirstOrDefault();
                    if (list == null)
                    {
                        AdmissionDocumentVerificationAssign type = new AdmissionDocumentVerificationAssign();
                        type.VerificationDate = VerificationDate;
                        type.VerificationTime = VerificationTime;
                        type.AdmissionStudentId = studentid;
                        type.EmployeeId = EmployeeId;
                        type.AssignedCounselorId = 0;
                        type.InsertedId = userId;
                        type.ModifiedId = userId;
                        type.InsertedDate = DateTime.Now;
                        type.ModifiedDate = DateTime.Now;
                        type.Status = EntityStatus.ACTIVE;
                        type.IsAvailable = true;
                        type.Active = true;
                        await admissionDocumentVerificationAssignRepository.AddAsync(type);

                    }
                }
                return RedirectToAction(nameof(AdmissionDocumentAssign)).WithSuccess("Document Assign Date And Time", "Saved Successfully");


            }

        }

        public IActionResult DownloadAdmissionDocumentAssignSample(long? OraganisationAcademicId, long? BoardId, long? BoardStandardId, long? WingId,Nullable<DateTime> VerificationDate)
        {
            CheckLoginStatus();
            XLWorkbook oWB = new XLWorkbook();
            var AdmissionStudent = admissionStudentRepository.ListAllAsyncIncludeAll().Result.ToList();
            var Employee = employeeRepository.GetAllEmployee().ToList();
            var studentlist = GetstudentDocumentDetailsWithFilter(true, true);
            if (OraganisationAcademicId != null && OraganisationAcademicId != 0)
            {
                long OraganisationId = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == OraganisationAcademicId).FirstOrDefault().OrganizationId;
                studentlist = studentlist.Where(a => a.OrganisationId == OraganisationId).ToList();
            }
            if (BoardId != null && BoardId != 0)
            {
                studentlist = studentlist.Where(a => a.BoardId == BoardId).ToList();
            }
            if (BoardStandardId != null && BoardStandardId != 0)
            {
                long StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().StandardId;
                studentlist = studentlist.Where(a => a.ClassId == StandardId).ToList();
            }
            if (WingId != null && WingId != 0)
            {
                studentlist = studentlist.Where(a => a.WingId == WingId).ToList();
            }
            System.Data.DataTable blooddt = new System.Data.DataTable();
            blooddt.Columns.Add("StudentName");
            foreach (var a in studentlist)
            {
                DataRow dr = blooddt.NewRow();
                dr["StudentName"] = "S_" + a.Id + "_" + a.FormNumber + "-" + a.Name;
                blooddt.Rows.Add(dr);
            }
            blooddt.TableName = "Student";
            //System.Data.DataTable orgdt = new System.Data.DataTable();
            //orgdt.Columns.Add("EmployeeName");
            //foreach (var a in Employee)
            //{
            //    DataRow dr = orgdt.NewRow();
            //    dr["EmployeeName"] = "E_" + a.ID + "_" + a.FirstName + " " + a.LastName;
            //    orgdt.Rows.Add(dr);
            //}
            //orgdt.TableName = "Employee";



            //System.Data.DataTable commondt = new System.Data.DataTable();
            //commondt.Columns.Add("Qualified");
            //DataRow dr9 = commondt.NewRow();
            //dr9["Qualified"] = "True";
            //commondt.Rows.Add(dr9);
            //DataRow dr10 = commondt.NewRow();
            //dr10["Qualified"] = "False";
            //commondt.Rows.Add(dr10);
            //commondt.TableName = "Document";



            int lastCellNo1 = blooddt.Rows.Count + 1;
            //int lastCellNo2 = orgdt.Rows.Count + 1;           
            //int lastCellNo3 = commondt.Rows.Count + 1;

            oWB.AddWorksheet(blooddt);
            //oWB.AddWorksheet(orgdt);         
            //oWB.AddWorksheet(commondt);
            var worksheet1 = oWB.Worksheet(1);



            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("AdmissionStudent (Compulsory)");
            validationTable.Columns.Add("Verification Date (MM/DD/YYYY)");
            validationTable.Columns.Add("Verification Time(00:00)");

            validationTable.TableName = "DocumentVerificationSample";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"DocumentVerificationSample.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadAdmissionDocumentAssign(IFormFile file)
        {
            CheckLoginStatus();
            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            string PITime = "00:00:00";
                            // bool Document = false;
                            long studentid = 0;

                            if (firstWorksheet.Cells[i, 1] != null && firstWorksheet.Cells[i, 1].Value != null)
                            {
                                studentid = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            }
                            if (firstWorksheet.Cells[i, 3] != null && firstWorksheet.Cells[i, 3].Value != null)
                            {
                                PITime = Convert.ToDateTime(firstWorksheet.Cells[i, 3].Value).ToString("hh:mm:ss");
                            }
                            //if (firstWorksheet.Cells[i, 5] != null && firstWorksheet.Cells[i, 5].Value != null)
                            //{
                            //    Document = Convert.ToBoolean(firstWorksheet.Cells[i, 5].Value.ToString());
                            //}                          

                            Nullable<DateTime> verificationDate = Convert.ToDateTime(firstWorksheet.Cells[i, 2].Value);
                            var list = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == studentid && a.VerificationDate.Value.Date == verificationDate.Value.Date).FirstOrDefault();
                            if (list == null)
                            {
                                AdmissionDocumentVerificationAssign type = new AdmissionDocumentVerificationAssign();
                                type.VerificationDate = verificationDate;
                                type.VerificationTime = TimeSpan.Parse(PITime);
                                type.AdmissionStudentId = studentid;
                                type.EmployeeId = 0;
                                type.AssignedCounselorId = 0;
                                type.InsertedId = userId;
                                type.ModifiedId = userId;
                                type.InsertedDate = DateTime.Now;
                                type.ModifiedDate = DateTime.Now;
                                type.Status = EntityStatus.ACTIVE;
                                type.IsAvailable = true;
                                type.Active = true;
                                await admissionDocumentVerificationAssignRepository.AddAsync(type);
                                string verificationdate = type.VerificationDate.Value.ToString("dd-MMM-yyyy");
                                string verificationtime = GetDateTimeFormat(type.VerificationDate.Value, type.VerificationTime.Value);
                                long admissionid = admissionStudentRepository.GetByIdAsync(type.AdmissionStudentId).Result.AdmissionId;

                                AdmissionTimeline timeline = new AdmissionTimeline();

                                timeline.Timeline = "Document Verification Date and Time : " +
                                    GetDateTimeFormat(type.VerificationDate.Value, type.VerificationTime.Value);
                                timeline.Active = true;
                                timeline.IsAvailable = true;
                                timeline.Status = "Document Verification Date and Time";
                                timeline.InsertedDate = DateTime.Now;
                                timeline.AdmissionId = admissionid;
                                timeline.ModifiedDate = DateTime.Now;
                                timeline.InsertedId = userId;
                                timeline.ModifiedId = userId;
                                admissionRepository.CreateAdmissionTimeline(timeline);

                                #region--------- smg And Email (Complited)------------------

                                var details = studentAdmissionMethod.GetStudentDetailsByStudentId(type.AdmissionStudentId);
                                string Message = HttpAddSmg.DOCDATE_TIME.Replace("{{StudentName}}", details.FullName).Replace("{{Verification Date}}", verificationdate).Replace("{{Verification Time}}", verificationtime);
                                if (details.Mobile != null && details.Mobile != "" && details.Mobile != null && details.Mobile != "")
                                {
                                    smssend.SendDocumentSendSms(details.Mobile, Message);
                                }
                                if (details.EmailId != null && details.EmailId != "")
                                {
                                    Emailsend.DocumentDateAssign(details.AcademicSessionName, details.ClassDetails.schoolName, details.ClassDetails.boardName, details.ClassDetails.className, details.ClassDetails.wingName,
                                        details.FormNumber, details.FullName, details.Mobile, details.EmailId, verificationdate, verificationtime, details.DocumentDetails.InterviewerName, Message, details.EmailId);
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionDocumentAssign)).WithSuccess("Admission Standard PI", "Excel uploaded successfully"); ;
        }

        #endregion

        #region-----bulk Upload Pi and Document Score ------------------------
        public IActionResult DownloadAdmissionPIScoreSample(long sessionId, long? OraganisationAcademicId,
             long? BoardId, long? BoardStandardId, long? WingId, Nullable<DateTime> ScorepiDate)
        {
            CheckLoginStatus();
            XLWorkbook oWB = new XLWorkbook();
            var admissionstudentpi = admissionPersonalInterviewAssignRepository.ListAllAsyncIncludeAll().Result
                .Where(a=>a.PIDate.Value.Date== ScorepiDate.Value.Date).Select(a=>a.AdmissionStudentId).ToList();
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result.ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();

            List<studentStandardModal> dataList = new List<studentStandardModal>();

            var stdlist = GetstudentDetailsWithFilter(true, false);

            if (OraganisationAcademicId != null && OraganisationAcademicId != 0)
            {
                long OraganisationId = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == OraganisationAcademicId).FirstOrDefault().OrganizationId;
                stdlist = stdlist.Where(a => a.OrganisationId == OraganisationId).ToList();
            }
            if (BoardId != null && BoardId!= 0)
            {
                stdlist = stdlist.Where(a => a.BoardId == BoardId).ToList();
            }
            if (BoardStandardId != null && BoardStandardId != 0)
            {
                long StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().StandardId;
                stdlist = stdlist.Where(a => a.ClassId == StandardId).ToList();
            }
            if (WingId != null && WingId != 0)
            {
                stdlist = stdlist.Where(a => a.WingId == WingId).ToList();
            }

            stdlist = stdlist.Where(x => admissionstudentpi.Contains(x.AdmissionStudentId)).ToList();

            System.Data.DataTable commondt = new System.Data.DataTable();
            commondt.Columns.Add("Qualified");
            DataRow dr9 = commondt.NewRow();
            dr9["Qualified"] = "Yes";
            commondt.Rows.Add(dr9);
            DataRow dr10 = commondt.NewRow();
            dr10["Qualified"] = "No";
            commondt.Rows.Add(dr10);
            commondt.TableName = "PI";

          
            int lastCellNo2 = commondt.Rows.Count + 1;          
            oWB.AddWorksheet(commondt);
            var worksheet1 = oWB.Worksheet(1);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("School");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Wing");
            validationTable.Columns.Add("StudentName");
            validationTable.Columns.Add("PiDateList(DD/MM/YYYY)");
            validationTable.Columns.Add("Qualified");
            validationTable.Columns.Add("Score");
            validationTable.Columns.Add("Remark");

            #region rows
            if (stdlist != null)
            {
                foreach (var item in stdlist)
                {
                    DataRow dr = validationTable.NewRow();
                    dr["School"] = item.OrganisationName;
                    dr["Board"] = item.BoardName;
                    dr["Class"] = item.ClassName;
                    dr["Wing"] = item.WingName;
                    dr["StudentName"] ="S_"+item.AdmissionStudentId+"_"+ item.StudentName;
                    dr["PiDateList(DD/MM/YYYY)"] = ScorepiDate.Value.ToString("dd-MMM-yyyy");
                    validationTable.Rows.Add(dr);
                }
            }
            #endregion


            validationTable.TableName = "PIScoreExamSample";
            var worksheet = oWB.AddWorksheet(validationTable);          
            worksheet.Column(7).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo2), true);
            worksheet1.Hide();       

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"PIScoreExamSample.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadAdmissionPIScore(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {

                            long score = 0;
                            Nullable<DateTime> pidate = null;
                            long studentid = 0;
                            string Qualified = null;

                            if (firstWorksheet.Cells[i, 5] != null && firstWorksheet.Cells[i, 5].Value != null)
                            {
                                studentid = Convert.ToInt64(firstWorksheet.Cells[i, 5].Value.ToString().Split('_')[1]);
                            }
                            if (firstWorksheet.Cells[i, 6] != null && firstWorksheet.Cells[i, 6].Value != null)
                            {
                                pidate = Convert.ToDateTime(firstWorksheet.Cells[i, 6].Value);
                            }

                            if (firstWorksheet.Cells[i, 7] != null && firstWorksheet.Cells[i, 7].Value != null)
                            {
                                Qualified = firstWorksheet.Cells[i, 7].Value.ToString();
                            }
                            if (firstWorksheet.Cells[i, 8] != null && firstWorksheet.Cells[i, 8].Value != null)
                            {
                                score = Convert.ToInt64(firstWorksheet.Cells[i, 8].Value.ToString());
                            }
                            AdmissionPersonalInterviewAssign checklist = admissionPersonalInterviewAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == studentid && a.IsQualified==true).FirstOrDefault();
                            if (checklist == null)
                            {
                                AdmissionPersonalInterviewAssign type = admissionPersonalInterviewAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == studentid && a.PIDate.Value.Date == pidate.Value.Date).FirstOrDefault();

                                if (type != null)
                                {
                                    type.IsQualified = (Qualified != "Yes") ? false : true;
                                    type.Score = score;
                                    type.Remarks = firstWorksheet.Cells[i, 9].Value.ToString();
                                    type.ModifiedId = userId;
                                    type.ModifiedDate = DateTime.Now;
                                    await admissionPersonalInterviewAssignRepository.UpdateAsync(type);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionPersonalInterviewAssign)).WithSuccess("Student PI Result Score", "Excel Uploaded Successfully.."); ;
        }

        public IActionResult DownloadAdmissionDocumentScoreSample(long? sessionId, long? OraganisationAcademicId,
            long? schoolId, long? BoardId, long? BoardStandardId, long? WingId, Nullable<DateTime> ScoreVerificationDate)
        {
            CheckLoginStatus();
            XLWorkbook oWB = new XLWorkbook();



            var admissionstudentdocument = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result
               .Where(a => a.VerificationDate.Value.Date == ScoreVerificationDate.Value.Date).Select(a => a.AdmissionStudentId).ToList();
            var admissionstudentlist = admissionStudentRepository.ListAllAsyncIncludeAll().Result.ToList();
            var admissionlist = admissionRepository.GetAllAdmission().ToList();

            List<studentStandardModal> dataList = new List<studentStandardModal>();

            var stdlist = GetstudentDocumentDetailsWithFilter(true, false);

            if (OraganisationAcademicId != null && OraganisationAcademicId != 0)
            {
                long OraganisationId = organizationAcademicRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == OraganisationAcademicId).FirstOrDefault().OrganizationId;
                stdlist = stdlist.Where(a => a.OrganisationId == OraganisationId).ToList();
            }
            if (BoardId != null && BoardId != 0)
            {
                stdlist = stdlist.Where(a => a.BoardId == BoardId).ToList();
            }
            if (BoardStandardId != null && BoardStandardId != 0)
            {
                long StandardId = boardStandardRepository.ListAllAsyncIncludeAll().Result.Where(a => a.ID == BoardStandardId).FirstOrDefault().StandardId;
                stdlist = stdlist.Where(a => a.ClassId == StandardId).ToList();
            }
            if (WingId != null && WingId != 0)
            {
                stdlist = stdlist.Where(a => a.WingId == WingId).ToList();
            }

            stdlist = stdlist.Where(x => admissionstudentdocument.Contains(x.AdmissionStudentId)).ToList();

            System.Data.DataTable commondt = new System.Data.DataTable();
            commondt.Columns.Add("Qualified");
            DataRow dr9 = commondt.NewRow();
            dr9["Qualified"] = "Yes";
            commondt.Rows.Add(dr9);
            DataRow dr10 = commondt.NewRow();
            dr10["Qualified"] = "No";
            commondt.Rows.Add(dr10);
            commondt.TableName = "PI";

            int lastCellNo1 = commondt.Rows.Count + 1;
            oWB.AddWorksheet(commondt);
            var worksheet1 = oWB.Worksheet(1);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("School");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Wing");
            validationTable.Columns.Add("StudentName");
            validationTable.Columns.Add("Verifaction Date(DD/MM/YYYY)");
            validationTable.Columns.Add("Qualified");          
            validationTable.Columns.Add("Remark");

            #region rows
            if (stdlist != null)
            {
                foreach (var item in stdlist)
                {
                    DataRow dr = validationTable.NewRow();
                    dr["School"] = item.OrganisationName;
                    dr["Board"] = item.BoardName;
                    dr["Class"] = item.ClassName;
                    dr["Wing"] = item.WingName;
                    dr["StudentName"] = "S_" + item.AdmissionStudentId + "_" + item.StudentName;
                    dr["Verifaction Date(DD/MM/YYYY)"] = ScoreVerificationDate.Value.ToString("dd-MMM-yyyy");
                    validationTable.Rows.Add(dr);
                }
            }
            #endregion

            validationTable.TableName = "DocumentScoreSample";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(7).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();





            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"DocumentScoreSample.xlsx");
        }

        public async Task<IActionResult> UploadAdmissionDocumentScore(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {

                            string Remarks = "";
                            Nullable<DateTime> verificationdate = null;
                            long studentid = 0;
                            string Qualified = null;

                            if (firstWorksheet.Cells[i, 5] != null && firstWorksheet.Cells[i, 5].Value != null)
                            {
                                studentid = Convert.ToInt64(firstWorksheet.Cells[i, 5].Value.ToString().Split('_')[1]);
                            }
                            if (firstWorksheet.Cells[i, 6] != null && firstWorksheet.Cells[i, 6].Value != null)
                            {
                                verificationdate = Convert.ToDateTime(firstWorksheet.Cells[i, 6].Value);
                            }

                            if (firstWorksheet.Cells[i, 7] != null && firstWorksheet.Cells[i, 7].Value != null)
                            {
                                Qualified = firstWorksheet.Cells[i, 7].Value.ToString();
                            }
                            if (firstWorksheet.Cells[i, 8] != null && firstWorksheet.Cells[i, 8].Value != null)
                            {
                                Remarks =firstWorksheet.Cells[i, 8].Value.ToString();
                            }
                            AdmissionDocumentVerificationAssign checklist = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == studentid && a.IsQualified == true).FirstOrDefault();
                            if (checklist == null)
                            {
                                AdmissionDocumentVerificationAssign type = admissionDocumentVerificationAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == studentid && a.VerificationDate.Value.Date == verificationdate.Value.Date).FirstOrDefault();

                                if (type != null)
                                {
                                    type.IsQualified = (Qualified != "Yes") ? false : true;                                   
                                    type.Remarks = Remarks;
                                    type.ModifiedId = userId;
                                    type.ModifiedDate = DateTime.Now;
                                    await admissionDocumentVerificationAssignRepository.UpdateAsync(type);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionDocumentAssign)).WithSuccess("Student Document Result", "Excel Uploaded Successfully"); ;
        }
        #endregion

        #region---------------------Counsellor Status------------------
        public IActionResult CounsellorStatus()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_CounsellorStatus", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var res = counsellorStatusRepository.ListAllAsyncIncludeAll().Result.ToList();
            return View(res);
        }
        public async Task<IActionResult> DeleteCounsellorStatus(int id)
        {
            CheckLoginStatus();
            CounsellorStatus type = counsellorStatusRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            await counsellorStatusRepository.UpdateAsync(type);
            return RedirectToAction(nameof(CounsellorStatus)).WithSuccess("Counsellor Status", "Deleted Successfully");
        }
        public IActionResult SaveOrUpdateCounsellorStatus(long ID, string Name)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                var namechk = counsellorStatusRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Name == Name.ToUpper().Trim() && a.ID != ID).FirstOrDefault();
                if (namechk == null)
                {
                    CounsellorStatus type = counsellorStatusRepository.GetByIdAsyncIncludeAll(ID).Result;
                    type.Name = Name.ToUpper();
                    type.ModifiedId = userId;
                    counsellorStatusRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(CounsellorStatus)).WithSuccess("Status", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(CounsellorStatus)).WithDanger("Status", "Status Name already exist");
                }

            }
            else
            {

                var namechk = counsellorStatusRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Name == Name.ToUpper().Trim()).FirstOrDefault();
                if (namechk != null)
                {
                    return RedirectToAction(nameof(CounsellorStatus)).WithDanger("Status", "Status Name already exist");
                }
                else
                {

                    CounsellorStatus type = new CounsellorStatus();
                    type.Name = Name.ToUpper();
                    type.ModifiedId = userId;
                    type.ModifiedDate = DateTime.Now;
                    type.InsertedDate = DateTime.Now;
                    type.InsertedId = userId;
                    type.IsAvailable = true;
                    type.Active = true;
                    type.Status = EntityStatus.ACTIVE;
                    counsellorStatusRepository.AddAsync(type);
                    return RedirectToAction(nameof(CounsellorStatus)).WithSuccess("Status", "Saved Successfully");

                }
            }
        }



        #endregion----------------------------------------------------------------

        #region------------------------- Admission Counsellor----------------------

        public IActionResult AdmissionCounsellor()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_AdmissionCounsellor", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var res = admissionCounsellorRepository.ListAllAsyncIncludeAll().Result;
            var emp = employeeRepository.GetAllEmployee();

            var counsellort = (from a in res
                               join b in emp on a.EmployeeId equals b.ID
                               select new AdmissionCounsellorModal
                               {
                                   Id = a.ID,
                                   EmpId = a.EmployeeId,
                                   EmpName = b.FirstName + " " + b.LastName,
                                   StartDate = a.StartDate,
                                   EndDate = a.EndDate
                               }).ToList();

            return View(counsellort);
        }
        public IActionResult CreateOrEditAdmissionCounsellor(AdmissionCounsellorModal admission)
        {
            ViewBag.Emp = employeeRepository.GetAllEmployee();
            AdmissionCounsellorModal counsellorModal = new AdmissionCounsellorModal();
            if (admission.Id > 0)
            {
                var councellor = admissionCounsellorRepository.GetByIdAsyncIncludeAll(admission.Id).Result;
                counsellorModal.EmpId = councellor.EmployeeId;
                ViewBag.status = "Update";
            }
            else
            {
                counsellorModal.EmpId = 0;
                ViewBag.status = "Create";
            }
            return View(counsellorModal);
        }
        public IActionResult SaveOrUpdateAdmissionCounsellor(AdmissionCounsellorModal admission)
        {
            CheckLoginStatus();

            if (admission.Id > 0)
            {
                var list = admissionCounsellorRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == admission.EmpId && a.ID != admission.Id).FirstOrDefault();
                if (list == null)
                {
                    var councellor = admissionCounsellorRepository.GetByIdAsyncIncludeAll(admission.Id).Result;
                    councellor.EmployeeId = admission.EmpId;
                    councellor.ModifiedDate = DateTime.Now;
                    councellor.ModifiedId = userId;
                    admissionCounsellorRepository.UpdateAsync(councellor);
                    return RedirectToAction(nameof(AdmissionCounsellor)).WithSuccess("Admission Counsellor", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(AdmissionCounsellor)).WithDanger("Admission Counsellor", "Already Present");
                }


            }
            else
            {
                var list = admissionCounsellorRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == admission.EmpId).FirstOrDefault();
                if (list == null)
                {
                    AdmissionCounsellor admcouncellor = new AdmissionCounsellor();
                    admcouncellor.InsertedId = userId;
                    admcouncellor.ModifiedId = userId;
                    admcouncellor.InsertedDate = DateTime.Now;
                    admcouncellor.ModifiedDate = DateTime.Now;
                    admcouncellor.Active = true;
                    admcouncellor.Status = EntityStatus.ACTIVE;
                    admcouncellor.IsAvailable = true;
                    admcouncellor.EmployeeId = admission.EmpId;
                    admcouncellor.StartDate = DateTime.Now;
                    admcouncellor.EndDate = null;
                    admissionCounsellorRepository.AddAsync(admcouncellor);
                    return RedirectToAction(nameof(AdmissionCounsellor)).WithSuccess("Admission Counsellor", "Saved Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(AdmissionCounsellor)).WithDanger("Admission Counsellor", "Already Present");
                }

            }


        }

        public IActionResult DeleteAdmissionCounsellor(long id)
        {
            CheckLoginStatus();
            var councellor = admissionCounsellorRepository.GetByIdAsyncIncludeAll(id).Result;
            councellor.ModifiedDate = DateTime.Now;
            councellor.ModifiedId = userId;
            councellor.Active = false;
            councellor.Status = "Deleted";
            councellor.IsAvailable = false;
            councellor.EndDate = DateTime.Now;

            admissionCounsellorRepository.UpdateAsync(councellor);
            return RedirectToAction(nameof(AdmissionCounsellor)).WithSuccess("Admission Counsellor", "Deleted Successfully");
        }

        public IActionResult DownloadAdmissionCounsellor()
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            XLWorkbook oWB = new XLWorkbook();
            var ProfessionList = employeeRepository.GetAllEmployee().Where(a => a.IsAvailable == true).ToList();
            System.Data.DataTable professiondt = new System.Data.DataTable();
            professiondt.Columns.Add("CounsellorName");
            foreach (var a in ProfessionList)
            {
                DataRow dr = professiondt.NewRow();
                dr["CounsellorName"] = "C_" + a.ID + "_" + a.EmpCode + "-" + a.FirstName + " " + a.LastName;
                professiondt.Rows.Add(dr);
            }
            professiondt.TableName = "Counsellor";
            int lastCellNo1 = professiondt.Rows.Count + 1;
            oWB.AddWorksheet(professiondt);
            var worksheet1 = oWB.Worksheet(1);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("Counsellor(Compulsory)");
            validationTable.TableName = "AdmissionCounsellor";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"AdmissionCounsellorSample.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadAdmissionCounsellor(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            long empid = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            var list = admissionCounsellorRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == empid).FirstOrDefault();
                            if (list == null)
                            {
                                AdmissionCounsellor admcouncellor = new AdmissionCounsellor();
                                admcouncellor.InsertedId = userId;
                                admcouncellor.ModifiedId = userId;
                                admcouncellor.InsertedDate = DateTime.Now;
                                admcouncellor.ModifiedDate = DateTime.Now;
                                admcouncellor.Active = true;
                                admcouncellor.Status = EntityStatus.ACTIVE;
                                admcouncellor.IsAvailable = true;
                                admcouncellor.EmployeeId = empid;
                                admcouncellor.StartDate = DateTime.Now;
                                admcouncellor.EndDate = null;
                                await admissionCounsellorRepository.AddAsync(admcouncellor);

                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionCounsellor)).WithSuccess("Admission Counsellor", "Excel Uploaded Successfully"); ;
        }

        #endregion

        #region-------------- Admission Personal InterViewer----------------------------------

        public IActionResult AdmissionInterViewer()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_AdmissionInterViewer", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var res = admissionInterViewerRepository.ListAllAsyncIncludeAll().Result.ToList();
            var emp = employeeRepository.GetAllEmployee();

            var interviewer = (from a in res
                               join b in emp on a.EmployeeId equals b.ID
                               select new AdmissionCounsellorModal
                               {
                                   Id = a.ID,
                                   EmpId = a.EmployeeId,
                                   EmpName = b.FirstName + " " + b.LastName,
                                   StartDate = a.StartDate,
                                   EndDate = a.EndDate
                               }).ToList();

            return View(interviewer);
        }
        public IActionResult CreateOrEditAdmissionInterViewer(AdmissionCounsellorModal admission)
        {
            ViewBag.Emps = commonMethods.GetEmployeeFullName();
            AdmissionCounsellorModal interViewer = new AdmissionCounsellorModal();
            if (admission.Id > 0)
            {
                var interview = admissionInterViewerRepository.GetByIdAsyncIncludeAll(admission.Id).Result;
                interViewer.EmpId = interview.EmployeeId;
                ViewBag.status = "Update";
            }
            else
            {
                interViewer.EmpId = 0;
                ViewBag.status = "Create";
            }
            return View(interViewer);
        }
        public async Task<IActionResult> SaveOrUpdateAdmissionInterViewer(AdmissionCounsellorModal admission)
        {
            CheckLoginStatus();

            if (admission.Id > 0)
            {
                var list = admissionInterViewerRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == admission.EmpId && a.ID != admission.Id).FirstOrDefault();
                if (list == null)
                {
                    var interViewer = admissionInterViewerRepository.GetByIdAsyncIncludeAll(admission.Id).Result;
                    interViewer.EmployeeId = admission.EmpId;
                    interViewer.ModifiedDate = DateTime.Now;
                    interViewer.ModifiedId = userId;
                    await admissionInterViewerRepository.UpdateAsync(interViewer);
                    return RedirectToAction(nameof(AdmissionInterViewer)).WithSuccess("Admission InterViewer", "Updated Successfully");


                }
                else
                {
                    return RedirectToAction(nameof(AdmissionInterViewer)).WithDanger("Admission InterViewer", "Already Present");


                }
            }
            else
            {
                var list = admissionInterViewerRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == admission.EmpId).FirstOrDefault();
                if (list == null)
                {
                    AdmissionInterViewer admInterviewer = new AdmissionInterViewer();
                    admInterviewer.InsertedId = userId;
                    admInterviewer.ModifiedId = userId;
                    admInterviewer.InsertedDate = DateTime.Now;
                    admInterviewer.ModifiedDate = DateTime.Now;
                    admInterviewer.Active = true;
                    admInterviewer.Status = EntityStatus.ACTIVE;
                    admInterviewer.IsAvailable = true;
                    admInterviewer.EmployeeId = admission.EmpId;
                    admInterviewer.StartDate = DateTime.Now;
                    admInterviewer.EndDate = null;
                    await admissionInterViewerRepository.AddAsync(admInterviewer);
                    return RedirectToAction(nameof(AdmissionInterViewer)).WithSuccess("Admission InterViewer", "Saved Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(AdmissionInterViewer)).WithDanger("Admission InterViewer", "Already Present");
                }

            }


        }

        public IActionResult DeleteAdmissionInterViewer(long id)
        {
            CheckLoginStatus();
            var interviewer = admissionInterViewerRepository.GetByIdAsyncIncludeAll(id).Result;
            interviewer.ModifiedDate = DateTime.Now;
            interviewer.ModifiedId = userId;
            interviewer.Active = false;
            interviewer.Status = EntityStatus.INACTIVE;
            interviewer.IsAvailable = false;
            interviewer.EndDate = DateTime.Now;

            admissionInterViewerRepository.UpdateAsync(interviewer);
            return RedirectToAction(nameof(AdmissionInterViewer)).WithSuccess("Admission InterViewer", "Deleted Successfully");
        }

        public IActionResult DownloadAdmissionInterViewer()
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            XLWorkbook oWB = new XLWorkbook();
            var ProfessionList = employeeRepository.GetAllEmployee().Where(a => a.IsAvailable == true).ToList();
            System.Data.DataTable professiondt = new System.Data.DataTable();
            professiondt.Columns.Add("InterViewerName");
            foreach (var a in ProfessionList)
            {
                DataRow dr = professiondt.NewRow();
                dr["InterViewerName"] = "C_" + a.ID + "_" + a.EmpCode + "-" + a.FirstName + " " + a.LastName;
                professiondt.Rows.Add(dr);
            }
            professiondt.TableName = "InterViewer";
            int lastCellNo1 = professiondt.Rows.Count + 1;
            oWB.AddWorksheet(professiondt);
            var worksheet1 = oWB.Worksheet(1);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("InterViewer(Compulsory)");
            validationTable.TableName = "AdmissionInterViewer";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"AdmissionInterViewerSample.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadAdmissionInterViewer(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            long empid = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            var list = admissionInterViewerRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == empid).FirstOrDefault();
                            if (list == null)
                            {
                                AdmissionInterViewer admInterviewer = new AdmissionInterViewer();
                                admInterviewer.InsertedId = userId;
                                admInterviewer.ModifiedId = userId;
                                admInterviewer.InsertedDate = DateTime.Now;
                                admInterviewer.ModifiedDate = DateTime.Now;
                                admInterviewer.Active = true;
                                admInterviewer.Status = EntityStatus.ACTIVE;
                                admInterviewer.IsAvailable = true;
                                admInterviewer.EmployeeId = empid;
                                admInterviewer.StartDate = DateTime.Now;
                                admInterviewer.EndDate = null;
                                await admissionInterViewerRepository.AddAsync(admInterviewer);

                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionInterViewer)).WithSuccess("Admission Inverviewer", "Excel Uploaded Successfully"); ;
        }

        #endregion

        #region----------------- Admission Document Verifier----------------------------

        public IActionResult AdmissionVerifier()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_AdmissionVerifier", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var res = admissionVerifierRepository.ListAllAsyncIncludeAll().Result.ToList();
            var emp = employeeRepository.GetAllEmployee();

            var verifier = (from a in res
                            join b in emp on a.EmployeeId equals b.ID
                            select new AdmissionCounsellorModal
                            {
                                Id = a.ID,
                                EmpId = a.EmployeeId,
                                EmpName = b.FirstName + " " + b.LastName,
                                StartDate = a.StartDate,
                                EndDate = a.EndDate
                            }).ToList();

            return View(verifier);
        }
        public IActionResult CreateOrEditAdmissionVerifier(AdmissionCounsellorModal admission)
        {
            ViewBag.Emp = commonMethods.GetEmployeeFullName();
            AdmissionCounsellorModal verifier = new AdmissionCounsellorModal();
            if (admission.Id > 0)
            {
                var interview = admissionVerifierRepository.GetByIdAsyncIncludeAll(admission.Id).Result;
                verifier.EmpId = interview.EmployeeId;
                ViewBag.status = "Update";
            }
            else
            {
                verifier.EmpId = 0;
                ViewBag.status = "Create";
            }
            return View(verifier);
        }
        public async Task<IActionResult> SaveOrUpdateAdmissionVerifier(AdmissionCounsellorModal admission)
        {
            CheckLoginStatus();

            if (admission.Id > 0)
            {
                var list = admissionVerifierRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == admission.EmpId && a.ID != admission.Id).FirstOrDefault();
                if (list == null)
                {
                    var verifier = admissionVerifierRepository.GetByIdAsyncIncludeAll(admission.Id).Result;
                    verifier.EmployeeId = admission.EmpId;
                    verifier.ModifiedDate = DateTime.Now;
                    verifier.ModifiedId = userId;
                    await admissionVerifierRepository.UpdateAsync(verifier);
                    return RedirectToAction(nameof(AdmissionVerifier)).WithSuccess("Admission Verifier", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(AdmissionVerifier)).WithDanger("Admission Verifier", "Already Present");
                }
            }
            else
            {
                var list = admissionVerifierRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == admission.EmpId).FirstOrDefault();
                if (list == null)
                {
                    AdmissionVerifier verifier = new AdmissionVerifier();
                    verifier.InsertedId = userId;
                    verifier.ModifiedId = userId;
                    verifier.InsertedDate = DateTime.Now;
                    verifier.ModifiedDate = DateTime.Now;
                    verifier.Active = true;
                    verifier.Status = EntityStatus.ACTIVE;
                    verifier.IsAvailable = true;
                    verifier.EmployeeId = admission.EmpId;
                    verifier.StartDate = DateTime.Now;
                    verifier.EndDate = null;
                    await admissionVerifierRepository.AddAsync(verifier);
                    return RedirectToAction(nameof(AdmissionVerifier)).WithSuccess("Admission Verifier", "Saved Successfully");

                }
                else
                {
                    return RedirectToAction(nameof(AdmissionVerifier)).WithDanger("Admission Verifier", "Already Present");
                }

            }


        }

        public IActionResult DeleteAdmissionVerifier(long id)
        {
            CheckLoginStatus();
            var viewer = admissionVerifierRepository.GetByIdAsyncIncludeAll(id).Result;
            viewer.ModifiedDate = DateTime.Now;
            viewer.ModifiedId = userId;
            viewer.Active = false;
            viewer.Status = EntityStatus.INACTIVE;
            viewer.IsAvailable = false;
            viewer.EndDate = DateTime.Now;

            admissionVerifierRepository.UpdateAsync(viewer);
            return RedirectToAction(nameof(AdmissionVerifier));
        }

        public IActionResult DownloadAdmissionVerifier()
        {
            long userId = HttpContext.Session.GetInt32("userId").Value;
            XLWorkbook oWB = new XLWorkbook();
            var ProfessionList = employeeRepository.GetAllEmployee().Where(a => a.IsAvailable == true).ToList();
            System.Data.DataTable professiondt = new System.Data.DataTable();
            professiondt.Columns.Add("VerifierName");
            foreach (var a in ProfessionList)
            {
                DataRow dr = professiondt.NewRow();
                dr["VerifierName"] = "C_" + a.ID + "_" + a.EmpCode + "-" + a.FirstName + " " + a.LastName;
                professiondt.Rows.Add(dr);
            }
            professiondt.TableName = "Verifier";
            int lastCellNo1 = professiondt.Rows.Count + 1;
            oWB.AddWorksheet(professiondt);
            var worksheet1 = oWB.Worksheet(1);

            System.Data.DataTable validationTable = new System.Data.DataTable();
            validationTable.Columns.Add("InterViewer(Compulsory)");
            validationTable.TableName = "AdmissionVerifier";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(1).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();
            return File(workbookBytes, "application/ms-excel", $"AAdmissionVerifierSample.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadAdmissionVerifier(IFormFile file)
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;

            try
            {

                if (file != null)
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                    {
                        ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[1];
                        int totalRows = firstWorksheet.Dimension.Rows;

                        for (int i = 2; i <= totalRows; i++)
                        {
                            long empid = Convert.ToInt64(firstWorksheet.Cells[i, 1].Value.ToString().Split('_')[1]);
                            var list = admissionVerifierRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EmployeeId == empid).FirstOrDefault();
                            if (list == null)
                            {
                                AdmissionVerifier verifier = new AdmissionVerifier();
                                verifier.InsertedId = userId;
                                verifier.ModifiedId = userId;
                                verifier.InsertedDate = DateTime.Now;
                                verifier.ModifiedDate = DateTime.Now;
                                verifier.Active = true;
                                verifier.Status = EntityStatus.ACTIVE;
                                verifier.IsAvailable = true;
                                verifier.EmployeeId = empid;
                                verifier.StartDate = DateTime.Now;
                                verifier.EndDate = null;
                                await admissionVerifierRepository.AddAsync(verifier);

                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return RedirectToAction(nameof(AdmissionVerifier)).WithSuccess("Admission Verifier", "Excel Uploaded Successfully"); ;
        }
        #endregion

        #region------------------------------Admission Fee------------------
        public IActionResult AdmissionFee()
        {
            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            if (commonMethods.checkaccessavailable("Mst_AdmissionFee", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }

            var sessionlist = academicSessionRepository.ListAllAsyncIncludeAll().Result;
            var feelist = admissionFeeRepository.ListAllAsyncIncludeAll().Result.ToList();
            var list = (from a in feelist
                        join b in sessionlist on a.AcademicSessionId equals b.ID
                        select new AdmissionFeeModel
                        {

                            ID = a.ID,
                            InsertedDate = a.InsertedDate,
                            AcademicSessionId = a.AcademicSessionId,
                            AcademicSessionName = b.DisplayName,
                            Ammount = a.Ammount,
                            Name = a.Name,
                            Status = a.Status
                        }
                       ).ToList();
            ViewBag.AcademicSessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true).ToList();
            return View(list);
        }




        public IActionResult DeleteAdmissionFee(int id)
        {
            AdmissionFee type = admissionFeeRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            admissionFeeRepository.UpdateAsync(type);
            return RedirectToAction(nameof(AdmissionFee)).WithSuccess("Admission Fee", "Deleted Successfully");
        }
        public IActionResult SaveOrUpdateAdmissionFee(long ID, int Ammount, long AcademicSessionId, string Name)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                var namechk = admissionFeeRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Ammount == Ammount && a.ID != ID).FirstOrDefault();
                if (namechk == null)
                {
                    AdmissionFee type = admissionFeeRepository.GetByIdAsyncIncludeAll(ID).Result;
                    type.Ammount = Ammount;
                    type.Name = Name;
                    type.ModifiedId = userId;
                    admissionFeeRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(AdmissionFee)).WithSuccess("Admission Fee", "Updated Successfully");

                }
                else
                {
                    return RedirectToAction(nameof(AdmissionFee)).WithDanger("Admission Fee", " Name already exist");

                }
            }
            else
            {
                var namechk = admissionFeeRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Ammount == Ammount && a.AcademicSessionId == AcademicSessionId).FirstOrDefault();
                if (namechk != null)
                {
                    return RedirectToAction(nameof(AdmissionFee)).WithDanger("Admission Fee", "  Name already exist");

                }
                else
                {
                    AdmissionFee type = new AdmissionFee();
                    type.AcademicSessionId = AcademicSessionId;
                    type.Ammount = Ammount;
                    type.Name = Name;
                    type.ModifiedId = userId;
                    type.InsertedId = userId;
                    type.ModifiedDate = DateTime.Now;
                    type.InsertedDate = DateTime.Now;
                    type.IsAvailable = true;
                    type.Active = true;
                    type.Status = EntityStatus.ACTIVE;
                    admissionFeeRepository.AddAsync(type);
                    return RedirectToAction(nameof(AdmissionFee)).WithSuccess("Admission Fee", " Saved Successfully");

                }
            }

        }


        #endregion-------------------------------------------------


        #region---------------------Master Email------------------
        public IActionResult MasterEmail()
        {

            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Mst_MasterEmail", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var res = masterEmailRepository.ListAllAsyncIncludeAll().Result.ToList();
            return View(res);
        }
        public async Task<IActionResult> DeleteMasterEmail(int id)
        {
            CheckLoginStatus();
            MasterEmail type = masterEmailRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            await masterEmailRepository.UpdateAsync(type);
            return RedirectToAction(nameof(MasterEmail)).WithSuccess("Email Details", "Deleted Successfully");
        }
        public IActionResult SaveOrUpdateMasterEmail(long ID, string Provider, string Host, string FromEmail, bool EnableSSl, string Port, string Username, string Password)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                var namechk = masterEmailRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Port == Port.Trim() && a.ID != ID).FirstOrDefault();
                if (namechk == null)
                {
                    MasterEmail type = masterEmailRepository.GetByIdAsyncIncludeAll(ID).Result;
                    type.Provider = Provider;
                    type.Host = Host;
                    type.FromEmailAddress = FromEmail;
                    type.EnableSSl = EnableSSl;
                    type.Port = Port;
                    type.Username = Username;
                    type.Password = Password;
                    type.ModifiedDate = DateTime.Now;
                    type.ModifiedId = userId;
                    masterEmailRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(MasterEmail)).WithSuccess("Email Details", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(MasterEmail)).WithDanger("Email Details", "Name already exist");
                }

            }
            else
            {

                var namechk = masterEmailRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Port == Port.Trim()).FirstOrDefault();
                if (namechk != null)
                {
                    return RedirectToAction(nameof(MasterEmail)).WithDanger("Email Details", "Port already exist");
                }
                else
                {

                    MasterEmail type = new MasterEmail();
                    type.Provider = Provider;
                    type.Host = Host;
                    type.FromEmailAddress = FromEmail;
                    type.EnableSSl = EnableSSl;
                    type.Port = Port;
                    type.Username = Username;
                    type.Password = Password;
                    type.ModifiedId = userId;
                    type.InsertedId = userId;
                    type.ModifiedDate = DateTime.Now;
                    type.InsertedDate = DateTime.Now;
                    type.IsAvailable = true;
                    type.Active = true;
                    type.Status = EntityStatus.ACTIVE;
                    masterEmailRepository.AddAsync(type);
                    return RedirectToAction(nameof(MasterEmail)).WithSuccess("Email Details", "Saved Successfully");

                }
            }
        }



        #endregion----------------------------------------------------------------

        #region---------------------Master Email------------------
        public IActionResult MasterSMS()
        {

            long accessId = HttpContext.Session.GetInt32("accessId").Value;
            long roleId = HttpContext.Session.GetInt32("roleId").Value;
            long userId = HttpContext.Session.GetInt32("userId").Value;
            if (commonMethods.checkaccessavailable("Mst_MasterSMS", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var res = masterSMSRepository.ListAllAsyncIncludeAll().Result.ToList();
            return View(res);
        }
        public async Task<IActionResult> DeleteMasterSMS(int id)
        {
            CheckLoginStatus();
            MasterSMS type = masterSMSRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            await masterSMSRepository.UpdateAsync(type);
            return RedirectToAction(nameof(MasterSMS)).WithSuccess("SMS Send Details", "Deleted Successfully");
        }
        public IActionResult SaveOrUpdateMasterSMS(long ID, string Provider, string Host, string ApiKey, string SenderId, string route)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                var namechk = masterSMSRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Host == Host.Trim() && a.ID != ID).FirstOrDefault();
                if (namechk == null)
                {
                    MasterSMS type = masterSMSRepository.GetByIdAsyncIncludeAll(ID).Result;
                    type.Provider = Provider;
                    type.Host = Host;
                    type.ApiKey = ApiKey;
                    type.SenderId = SenderId;
                    type.Route = route;
                    type.ModifiedDate = DateTime.Now;
                    type.ModifiedId = userId;
                    masterSMSRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(MasterSMS)).WithSuccess("SMS Send Details", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(MasterSMS)).WithDanger("SMS ", "Ip Address already exist");
                }

            }
            else
            {

                var namechk = masterSMSRepository.ListAllAsyncIncludeAll().Result.Where(a => a.Host == Host.Trim()).FirstOrDefault();
                if (namechk != null)
                {
                    return RedirectToAction(nameof(MasterSMS)).WithDanger("SMS", "Ip Address already exist");
                }
                else
                {

                    MasterSMS type = new MasterSMS();
                    type.Provider = Provider;
                    type.Host = Host;
                    type.Provider = Provider;
                    type.Host = Host;
                    type.ApiKey = ApiKey;
                    type.SenderId = SenderId;
                    type.Route = route;
                    type.ModifiedId = userId;
                    type.InsertedId = userId;
                    type.ModifiedDate = DateTime.Now;
                    type.InsertedDate = DateTime.Now;
                    type.IsAvailable = true;
                    type.Active = true;
                    type.Status = EntityStatus.ACTIVE;
                    masterSMSRepository.AddAsync(type);
                    return RedirectToAction(nameof(MasterSMS)).WithSuccess("SMS Send Details", "Saved Successfully");

                }
            }
        }



        #endregion----------------------------------------------------------------

        #region -------------------AdmitCard Venue Address--------------------------

        public IActionResult AdmitcardVenueAddress()
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Mst_AdmitcardVenueAdd", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var venueAddress = admitcardVenueAddressRepository.GetAllAdmitcardVenueAddress().ToList();

            return View(venueAddress);
        }
        public IActionResult CreateOrEditAdmitcardVenueAddress(AdmitcardVenueAddress venueAddress)
        {
            CheckLoginStatus();
            AdmitcardVenueAddress av = new AdmitcardVenueAddress();
            if (venueAddress.ID > 0)
            {
                var venueAdd = admitcardVenueAddressRepository.GetAdmitcardVenueAddressById(venueAddress.ID);
                av.Address = venueAdd.Address;
                ViewBag.status = "Update";
            }
            else
            {
                av.Address = "";
                ViewBag.status = "Create";
            }

            return View(av);
        }
        public IActionResult SaveOrUpdateAdmitcardVenueAddress(AdmitcardVenueAddress venueAddress)
        {
            CheckLoginStatus();
            if (venueAddress.ID > 0)
            {
                var venueAdd = admitcardVenueAddressRepository.GetAdmitcardVenueAddressById(venueAddress.ID);
                venueAdd.ModifiedId = userId;
                venueAdd.ModifiedDate = DateTime.Now;
                venueAdd.Status = EntityStatus.UPDATED;
                venueAdd.Address = venueAddress.Address;

                admitcardVenueAddressRepository.UpdateAdmitcardVenueAddress(venueAdd);
            }
            else
            {
                AdmitcardVenueAddress address = new AdmitcardVenueAddress();
                address.InsertedId = userId;
                address.InsertedDate = DateTime.Now;
                address.ModifiedId = userId;
                address.ModifiedDate = DateTime.Now;
                address.Active = true;
                address.Status = EntityStatus.ACTIVE;
                address.IsAvailable = true;
                address.Address = venueAddress.Address;

                admitcardVenueAddressRepository.CreateAdmitcardVenueAddress(address);
            }

            return RedirectToAction(nameof(AdmitcardVenueAddress));
        }
        public IActionResult DeleteAdmitcardVenueAddress(long addressid)
        {
            CheckLoginStatus();
            var venueAdd = admitcardVenueAddressRepository.GetAdmitcardVenueAddressById(addressid);
            venueAdd.ModifiedId = userId;
            venueAdd.ModifiedDate = DateTime.Now;
            venueAdd.Status = EntityStatus.DELETED;
            venueAdd.Active = false;
            venueAdd.IsAvailable = false;

            admitcardVenueAddressRepository.UpdateAdmitcardVenueAddress(venueAdd);

            return RedirectToAction(nameof(AdmitcardVenueAddress));
        }

        #endregion

        //#region --------------AdmitCard Venue-----------------------

        //public IActionResult AdmitCardVenue()
        //{
        //    var admitcard = admitcardVenueRepository.GetAllAdmitcardVenue().ToList();
        //    var orgList = organizationRepository.GetAllOrganization();
        //    var admitcardVenueAdd = admitcardVenueAddressRepository.GetAllAdmitcardVenueAddress();
        //    var res = (from a in admitcard
        //               join b in orgList on a.OrganizationId equals b.ID
        //               join c in admitcardVenueAdd on a.AdmitcardVenueAddressId equals c.ID
        //               select new AdmitCardVenueModelClass
        //               {
        //                   ID = a.ID,
        //                   OrganizationName = b.Name,
        //                   Address = c.Address
        //               }).ToList();

        //    return View(res);
        //}
        //public IActionResult CreateOrEditAdmitCardVenue(AdmitCardVenueModelClass venueModel)
        //{
        //    AdmitCardVenueModelClass admit = new AdmitCardVenueModelClass();
        //    ViewBag.Organization = organizationRepository.GetAllOrganization();
        //    ViewBag.AdmitCardAdd = admitcardVenueAddressRepository.GetAllAdmitcardVenueAddress();
        //    if (venueModel.ID > 0)
        //    {
        //        var venue = admitcardVenueRepository.GetAdmitcardVenueById(venueModel.ID);
        //        admit.OrganizationId = venue.OrganizationId;
        //        admit.AdmitCardVenueAddressId = venue.AdmitcardVenueAddressId;
        //        ViewBag.status = "Update";
        //    }
        //    else
        //    {
        //        admit.OrganizationId = 0;
        //        admit.AdmitCardVenueAddressId = 0;
        //        ViewBag.status = "Create";
        //    }

        //    return View(admit);
        //}
        //public IActionResult SaveOrUpdateAdmitCardVenue(AdmitCardVenueModelClass venueModel)
        //{
        //    CheckLoginStatus();
        //    if (venueModel.ID > 0)
        //    {
        //        var venue = admitcardVenueRepository.GetAdmitcardVenueById(venueModel.ID);
        //        venue.ModifiedId = userId;
        //        venue.ModifiedDate = DateTime.Now;
        //        venue.Status = EntityStatus.UPDATED;
        //        venue.OrganizationId = Convert.ToInt32(venueModel.OrganizationId);
        //        venue.AdmitcardVenueAddressId = Convert.ToInt32(venueModel.AdmitCardVenueAddressId);

        //        admitcardVenueRepository.UpdateAdmitcardVenue(venue);
        //    }
        //    else
        //    {
        //        AdmitcardVenue admitCard = new AdmitcardVenue();
        //        admitCard.InsertedId = userId;
        //        admitCard.InsertedDate = DateTime.Now;
        //        admitCard.ModifiedId = userId;
        //        admitCard.ModifiedDate = DateTime.Now;
        //        admitCard.Active = true;
        //        admitCard.Status = EntityStatus.ACTIVE;
        //        admitCard.IsAvailable = true;
        //        admitCard.OrganizationId = Convert.ToInt32(venueModel.OrganizationId);
        //        admitCard.AdmitcardVenueAddressId = Convert.ToInt32(venueModel.AdmitCardVenueAddressId);

        //        admitcardVenueRepository.CreateAdmitcardVenue(admitCard);
        //    }

        //    return RedirectToAction(nameof(AdmitCardVenue));
        //}
        //public IActionResult DeleteAdmitCardVenue(long id)
        //{
        //    var venue = admitcardVenueRepository.GetAdmitcardVenueById(id);
        //    venue.ModifiedId = userId;
        //    venue.ModifiedDate = DateTime.Now;
        //    venue.Status = EntityStatus.DELETED;
        //    venue.Active = false;
        //    venue.IsAvailable = false;
        //    admitcardVenueRepository.UpdateAdmitcardVenue(venue);

        //    return RedirectToAction(nameof(AdmitCardVenue));
        //}

        //#endregion

        #region ----------------AdmitCard Details----------------

        public IActionResult AdmitcardDetails()
        {
            var det = admitcardDetailsRepository.GetAllAdmitcardDetails();
            var instructionsModel = admitcardInstructionsRepository.GetAllAdmicardInstructions();
            var admitcardVenue = admitcardVenueRepository.GetAllAdmitcardVenue();
            var sessions = academicSessionRepository.ListAllAsync().
                Result.Where(a => a.Active == true && a.IsAdmission == true).ToList();
            var acdorganisation = organizationAcademicRepository.ListAllAsync().Result;
            var organisation = organizationRepository.GetAllOrganization();
            var res = (from a in det
                       join b in instructionsModel on a.ID equals b.AdmitcardDetailsId
                       join c in admitcardVenue on a.ID equals c.AdmitcardDetailsId
                       join d in acdorganisation on a.OrganisationAcademicId equals d.ID
                       join e in organisation on d.OrganizationId equals e.ID
                       join f in sessions on d.AcademicSessionId equals f.ID
                       select new AdmitcardMastercls
                       {
                           Address = a.Address,
                           ID = a.ID,
                           Instruction = b.Instruction,
                           LogoName = a.LogoName,
                           OrganisationAcademicId = a.OrganisationAcademicId,
                           OrganisationName = c.OrganisationName,
                           Organization = e.Name,
                           sessionid = f.ID,
                           SessionName = f.DisplayName,
                           Title = a.Title,
                           Remarks = a.Remarks,
                           Tollfree = a.Tollfree,
                           VenueAddress = c.Address
                       }).ToList();

            return View(res);
        }
        public IActionResult CreateOrEditAdmitcardDetails(AdmitcardDetails admitcardDetails)
        {

            ViewBag.AcademicSessionList = academicSessionRepository.ListAllAsync().
                Result.Where(a => a.Active == true && a.IsAdmission == true).ToList();
            AdmitcardMastercls admit = new AdmitcardMastercls();
            if (admitcardDetails.ID > 0)
            {
                var ad = admitcardDetailsRepository.GetAdmitcardDetailsById(admitcardDetails.ID);
                AdmitcardInstructions instructionsModel = admitcardInstructionsRepository.GetAllAdmicardInstructions().Where(a => a.AdmitcardDetailsId == admitcardDetails.ID).FirstOrDefault();
                AdmitcardVenue admitcardVenue = admitcardVenueRepository.GetAllAdmitcardVenue().Where(a => a.AdmitcardDetailsId == admitcardDetails.ID).FirstOrDefault();

                admit.LogoName = ad.LogoName;
                admit.Address = ad.Address;
                admit.Tollfree = ad.Tollfree;
                admit.Title = ad.Title;
                admit.Remarks = ad.Remarks;
                admit.OrganisationAcademicId = ad.OrganisationAcademicId;
                var sessionid = organizationAcademicRepository.ListAllAsync().Result.Where(a => a.ID == ad.OrganisationAcademicId).FirstOrDefault().AcademicSessionId;
                admit.sessionid = sessionid;
                admit.ID = ad.ID;
                admit.Instruction = instructionsModel.Instruction;
                admit.OrganisationName = admitcardVenue.OrganisationName;
                admit.VenueAddress = admitcardVenue.Address;
                ViewBag.status = "Edit";
            }
            else
            {
                admit.LogoName = "";
                admit.Address = "";
                admit.Tollfree = "";
                admit.Title = "";
                admit.Remarks = "";
                admit.OrganisationAcademicId = 0;
                admit.sessionid = 0;
                admit.ID = 0;
                admit.Instruction = "";
                admit.OrganisationName = "";
                admit.VenueAddress = "";
                ViewBag.status = "Create";
            }

            return View(admit);
        }
        public IActionResult SaveOrUpdateAdmitcardDetails(AdmitcardMastercls admitcardDetails)
        {
            var Logo = HttpContext.Request.Form.Files["Logo"];
            CheckLoginStatus();
            AdmitcardDetails admit = new AdmitcardDetails();
            if (admitcardDetails.ID > 0)
            {
                var ad = admitcardDetailsRepository.GetAdmitcardDetailsById(admitcardDetails.ID);
                ad.Address = admitcardDetails.Address;
                ad.Tollfree = admitcardDetails.Tollfree;
                ad.Title = admitcardDetails.Title;
                ad.Remarks = admitcardDetails.Remarks;
                ad.OrganisationAcademicId = admitcardDetails.OrganisationAcademicId;

                if (Logo != null)
                {
                    FileInfo fi = new FileInfo(Logo.FileName);
                    var newFilename = "File" + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admitcard_logo\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        Logo.CopyTo(stream);
                    }
                    ad.LogoName = "/ODMImages/Admitcard_logo/" + pathToSave;
                    //attach.file = "/ODMImages/Tempfile/" + pathToSave;
                }

                admitcardDetailsRepository.UpdateAdmitcardDetails(ad);
                AdmitcardInstructions instructionsModel = admitcardInstructionsRepository.GetAllAdmicardInstructions().Where(a => a.AdmitcardDetailsId == admitcardDetails.ID).FirstOrDefault();

                instructionsModel.Instruction = admitcardDetails.Instruction;
                instructionsModel.Active = true;
                instructionsModel.ModifiedDate = DateTime.Now;
                instructionsModel.ModifiedId = userId;
                instructionsModel.IsAvailable = true;
                admitcardInstructionsRepository.UpdateAdmicardInstructions(instructionsModel);
                AdmitcardVenue admitcardVenue = admitcardVenueRepository.GetAllAdmitcardVenue().Where(a => a.AdmitcardDetailsId == admitcardDetails.ID).FirstOrDefault();

                admitcardVenue.OrganisationName = admitcardDetails.OrganisationName;
                admitcardVenue.Address = admitcardDetails.VenueAddress;
                admitcardVenue.Active = true;
                admitcardVenue.ModifiedDate = DateTime.Now;
                admitcardVenue.ModifiedId = userId;
                admitcardVenue.IsAvailable = true;
                admitcardVenueRepository.UpdateAdmitcardVenue(admitcardVenue);
            }
            else
            {
                AdmitcardDetails admitc = new AdmitcardDetails();
                admitc.InsertedId = userId;
                admitc.InsertedDate = DateTime.Now;
                admitc.ModifiedId = userId;
                admitc.ModifiedDate = DateTime.Now;
                admitc.Active = true;
                admitc.Status = EntityStatus.ACTIVE;
                admitc.IsAvailable = true;
                admitc.Address = admitcardDetails.Address;
                admitc.Remarks = admitcardDetails.Remarks;
                admitc.Tollfree = admitcardDetails.Tollfree;
                admitc.Title = admitcardDetails.Title;
                admitc.OrganisationAcademicId = admitcardDetails.OrganisationAcademicId;

                if (Logo != null)
                {
                    FileInfo fi = new FileInfo(Logo.FileName);
                    var newFilename = "File" + "_" + String.Format("{0:d}",
                                      (DateTime.Now.Ticks / 10) % 100000000) + fi.Extension;
                    var webPath = hostingEnvironment.WebRootPath;
                    string path = Path.Combine("", webPath + @"\ODMImages\Admitcard_logo\" + newFilename);
                    var pathToSave = newFilename;
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        Logo.CopyTo(stream);
                    }
                    admitc.LogoName = "/ODMImages/Admitcard_logo/" + pathToSave;
                    //attach.file = "/ODMImages/Tempfile/" + pathToSave;
                }

                var detailsid = admitcardDetailsRepository.CreateAdmitcardDetails(admitc);
                AdmitcardInstructions instructionsModel = new AdmitcardInstructions();
                instructionsModel.AdmitcardDetailsId = detailsid;
                instructionsModel.Instruction = admitcardDetails.Instruction;
                instructionsModel.Active = true;
                instructionsModel.InsertedDate = DateTime.Now;
                instructionsModel.InsertedId = userId;
                instructionsModel.ModifiedDate = DateTime.Now;
                instructionsModel.ModifiedId = userId;
                instructionsModel.IsAvailable = true;
                instructionsModel.Status = EntityStatus.ACTIVE;
                admitcardInstructionsRepository.CreateAdmicardInstructions(instructionsModel);
                AdmitcardVenue admitcardVenue = new AdmitcardVenue();
                admitcardVenue.AdmitcardDetailsId = detailsid;
                admitcardVenue.OrganisationName = admitcardDetails.OrganisationName;
                admitcardVenue.Address = admitcardDetails.VenueAddress;
                admitcardVenue.Active = true;
                admitcardVenue.InsertedDate = DateTime.Now;
                admitcardVenue.InsertedId = userId;
                admitcardVenue.ModifiedDate = DateTime.Now;
                admitcardVenue.ModifiedId = userId;
                admitcardVenue.IsAvailable = true;
                admitcardVenue.Status = EntityStatus.ACTIVE;
                admitcardVenueRepository.CreateAdmitcardVenue(admitcardVenue);

            }

            return RedirectToAction(nameof(AdmitcardDetails));
        }
        public IActionResult DeleteAdmitcardDetails(long id)
        {
            CheckLoginStatus();
            var ad = admitcardDetailsRepository.GetAdmitcardDetailsById(id);
            ad.ModifiedId = userId;
            ad.ModifiedDate = DateTime.Now;
            ad.Active = false;
            ad.Status = EntityStatus.DELETED;
            ad.IsAvailable = false;

            admitcardDetailsRepository.UpdateAdmitcardDetails(ad);
            return RedirectToAction(nameof(AdmitcardDetails));
        }

        #endregion

        //#region --------------Admitcard Instructions-------------------

        //public IActionResult AdmitcardInstructions()
        //{
        //    var adI = admitcardInstructionsRepository.GetAllAdmicardInstructions().ToList();
        //    var details = admitcardDetailsRepository.GetAllAdmitcardDetails().ToList();

        //    var res = (from a in adI
        //               join b in details on a.AdmitcardDetailsId equals b.ID
        //               select new AdmitcardInstructionsModel
        //               {
        //                   ID = a.ID,
        //                   LogoName = b.LogoName,
        //                   Address = b.Address,
        //                   Tollfree = b.Tollfree,
        //                   Title = b.Title,
        //                   Instruction = a.Instruction
        //               }).ToList();

        //    return View(res);
        //}
        //public IActionResult CreateOrEditAdmitcardInstructions(long? id)
        //{
        //    AdmitcardInstructionsModel model = new AdmitcardInstructionsModel();
        //    ViewBag.Details = admitcardDetailsRepository.GetAllAdmitcardDetails();
        //    if (id != null)
        //    {
        //        var instruction = admitcardInstructionsRepository.GetAdmicardInstructionsById(id.Value);
        //        model.AdmitCardDetailsId = instruction.AdmitcardDetailsId;
        //        model.Instruction = instruction.Instruction;

        //        ViewBag.Status = "Update";
        //    }
        //    else
        //    {
        //        model.AdmitCardDetailsId = 0;
        //        model.Instruction = "";

        //        ViewBag.Status = "Update";
        //    }

        //    return View(model);
        //}
        //public IActionResult SaveOrUpdateAdmitcardInstructions(AdmitcardInstructionsModel admitcard)
        //{
        //    AdmitcardInstructions instructionsModel = new AdmitcardInstructions();
        //    CheckLoginStatus();
        //    if (admitcard.ID > 0)
        //    {
        //        var instruction = admitcardInstructionsRepository.GetAdmicardInstructionsById(admitcard.ID);
        //        instructionsModel.AdmitcardDetailsId = Convert.ToInt32(admitcard.ID);
        //        instructionsModel.Instruction = admitcard.Instruction;
        //        instructionsModel.ModifiedDate = DateTime.Now;
        //        instructionsModel.ModifiedId = userId;
        //        instructionsModel.Status = EntityStatus.UPDATED;

        //        admitcardInstructionsRepository.UpdateAdmicardInstructions(instructionsModel);
        //    }
        //    else
        //    {
        //        instructionsModel.InsertedDate = DateTime.Now;
        //        instructionsModel.InsertedId = userId;
        //        instructionsModel.ModifiedDate = DateTime.Now;
        //        instructionsModel.ModifiedId = userId;
        //        instructionsModel.Active = true;
        //        instructionsModel.Status = EntityStatus.ACTIVE;
        //        instructionsModel.IsAvailable = true;
        //        instructionsModel.AdmitcardDetailsId = Convert.ToInt32(admitcard.AdmitCardDetailsId);
        //        instructionsModel.Instruction = admitcard.Instruction;

        //        admitcardInstructionsRepository.CreateAdmicardInstructions(instructionsModel);
        //    }

        //    return RedirectToAction(nameof(AdmitcardInstructions));
        //}
        //public IActionResult DeleteAdmitcardInstructions(long? id)
        //{
        //    AdmitcardInstructions instructionsModel = new AdmitcardInstructions();
        //    var instruction = admitcardInstructionsRepository.GetAdmicardInstructionsById(id.Value);
        //    instructionsModel.ModifiedDate = DateTime.Now;
        //    instructionsModel.ModifiedId = userId;
        //    instructionsModel.Active = false;
        //    instructionsModel.Status = EntityStatus.DELETED;
        //    instructionsModel.IsAvailable = false;

        //    admitcardInstructionsRepository.UpdateAdmicardInstructions(instructionsModel);
        //    return RedirectToAction(nameof(AdmitcardInstructions));

        //}

        //#endregion
    }
}
