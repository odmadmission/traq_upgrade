﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Areas.Admission.Models;
using OdmErp.Web.Models;
using Web.Controllers;

namespace OdmErp.Web.Areas.Admission.Controllers
{
    [Area("Admission")]
    public class ECMasterController : AppController
    {
        public CommonMethods commonMethods;
        public IAdmissionCampusTourRepository admissionCampusTourRepository;
        public IAdmissionCampusTourDateRepository admissionCampusTourDateRepository;
        public IAdmissionCampusTourSlotRepository admissionCampusTourSlotRepository;
        public IAdmissionCampusTourTimelineRepository admissionCampusTourTimelineRepository;
        public IAdmissionEcounsellingRepository admissionEcounsellingRepository;
        public IAdmissionEcounsellingDateRepository admissionEcounsellingDateRepository;
        public IAdmissionEcounsellingSlotRepository admissionEcounsellingSlotRepository;
        public IAdmissionEcounsellingTimelineRepository admissionEcounsellingTimelineRepository;
        public IAcademicSessionRepository academicSessionRepository;
        public IAdmissionRepository admissionRepository;
        public ECMasterController(CommonMethods commonMethods, IAdmissionCampusTourRepository admissionCampusTourRepository, 
            IAdmissionCampusTourDateRepository admissionCampusTourDateRepository, IAdmissionCampusTourSlotRepository admissionCampusTourSlotRepository, 
            IAdmissionCampusTourTimelineRepository admissionCampusTourTimelineRepository, IAdmissionEcounsellingRepository admissionEcounsellingRepository, 
            IAdmissionEcounsellingDateRepository admissionEcounsellingDateRepository, IAdmissionEcounsellingSlotRepository admissionEcounsellingSlotRepository, 
            IAdmissionEcounsellingTimelineRepository admissionEcounsellingTimelineRepository, IAcademicSessionRepository academicSessionRepository,
            IAdmissionRepository admissionRepository)
        {
            this.admissionCampusTourRepository = admissionCampusTourRepository;
            this.commonMethods = commonMethods;
            this.admissionCampusTourDateRepository = admissionCampusTourDateRepository;
            this.admissionCampusTourSlotRepository = admissionCampusTourSlotRepository;
            this.admissionCampusTourTimelineRepository = admissionCampusTourTimelineRepository;
            this.admissionEcounsellingRepository = admissionEcounsellingRepository;
            this.admissionEcounsellingDateRepository = admissionEcounsellingDateRepository;
            this.admissionEcounsellingSlotRepository = admissionEcounsellingSlotRepository;
            this.admissionEcounsellingTimelineRepository = admissionEcounsellingTimelineRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.admissionRepository = admissionRepository;


        }
        #region
        public string GetDateTimeFormat(DateTime d, TimeSpan span)
        {
            DateTime time = d.Add(span);
            return time.ToString("hh:mm tt");
        }
        #endregion

        #region---------------------Admission Campus Tour Date------------------
        public IActionResult AdmissionCampusTourDate()
        {

            CheckLoginStatus();

            if (commonMethods.checkaccessavailable("Mst_CampusTourDate", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.SessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true && a.IsAvailable == true);
            var res = admissionCampusTourDateRepository.ListAllAsyncIncludeAll().Result.ToList();
            var sessionlist = academicSessionRepository.ListAllAsyncIncludeAll().Result.ToList();
            var Slotlist = admissionCampusTourSlotRepository.ListAllAsyncIncludeAll().Result.ToList();

            var list = (from a in res
                        join b in sessionlist on a.AcademicSessionId equals b.ID
                        select new AdmissionCampusTourDateModel
                        {
                            ID = a.ID,
                            TourDate = a.CampusTourDate,
                            TourDatestring = NullableDateTimeToStringDate(a.CampusTourDate),
                            AcademicSessionName = b.DisplayName,
                            Status = a.Status,
                            SlotCount= Slotlist.Where(z=>z.AdmissionCampusTourDateID==a.ID).ToList().Count,
                        }).ToList();

            return View(list);
        }
        public async Task<IActionResult> DeleteAdmissionCampusTourDate(int id)
        {
            CheckLoginStatus();
            AdmissionCampusTourDate type = admissionCampusTourDateRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
           await admissionCampusTourDateRepository.UpdateAsync(type);
            return RedirectToAction(nameof(AdmissionCampusTourDate)).WithSuccess("CampusTour Date", "Deleted Successfully");
        }
        public async Task<IActionResult> SaveOrUpdateAdmissionCampusTourDate(long ID, DateTime TourDate, long AcdemicSessionId)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                var namechk = admissionCampusTourDateRepository.ListAllAsyncIncludeAll().Result.Where(a => a.CampusTourDate.Date == TourDate.Date && a.AcademicSessionId== AcdemicSessionId && a.ID != ID).FirstOrDefault();
                if (namechk == null)
                {
                    AdmissionCampusTourDate type = admissionCampusTourDateRepository.GetByIdAsyncIncludeAll(ID).Result;
                    type.CampusTourDate = TourDate;
                    type.ModifiedId = userId;
                   await admissionCampusTourDateRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(AdmissionCampusTourDate)).WithSuccess("Campus Tour Date", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(AdmissionCampusTourDate)).WithDanger("Campus Tour Date", "Name already exist");
                }

            }
            else
            {

                var namechk = admissionCampusTourDateRepository.ListAllAsyncIncludeAll().Result.Where(a => a.CampusTourDate.Date == TourDate.Date && a.AcademicSessionId== AcdemicSessionId).FirstOrDefault();
                if (namechk != null)
                {
                    return RedirectToAction(nameof(AdmissionCampusTourDate)).WithDanger("Campus Tour Date", "Date already exist...!");
                }
                else
                {

                    AdmissionCampusTourDate type = new AdmissionCampusTourDate();
                    type.CampusTourDate = TourDate;
                    type.AcademicSessionId = AcdemicSessionId;
                    type.ModifiedId = userId;
                    type.InsertedId = userId;
                    type.InsertedDate = DateTime.Now;
                    type.ModifiedDate = DateTime.Now;
                    type.IsAvailable = true;
                    type.Active = true;
                    type.Status = EntityStatus.ACTIVE;
                   await admissionCampusTourDateRepository.AddAsync(type);
                    return RedirectToAction(nameof(AdmissionCampusTourDate)).WithSuccess("Campus Tour Date", "Saved Successfully");

                }
            }
        }


        #endregion----------------------------------------------------------------

        #region---------------------Admission Campus Tour Slot------------------

        public IList<AdmissionCampusTourSlotModel> GetAllCampusSlotList(long AdmissionCampusTourDateID)
        {
            try
            {
                var CampusTourlist = admissionCampusTourDateRepository.ListAllAsyncIncludeAll().Result.ToList();
                var sessionlist = academicSessionRepository.ListAllAsyncIncludeAll().Result.ToList();
                var Slotlist = admissionCampusTourSlotRepository.ListAllAsyncIncludeAll().Result.Where(a=>a.AdmissionCampusTourDateID== AdmissionCampusTourDateID).ToList();
                var list = (from a in Slotlist
                            join b in CampusTourlist on a.AdmissionCampusTourDateID equals b.ID
                            join c in sessionlist on b.AcademicSessionId equals c.ID
                            select new AdmissionCampusTourSlotModel
                            {
                                AdmissionCampusTourDateID=a.AdmissionCampusTourDateID,
                                ID = a.ID,
                                TourDate = b.CampusTourDate,
                                TourDatestring = NullableDateTimeToStringDate(b.CampusTourDate),
                                AcademicSessionName = c.DisplayName,
                                Status = a.Status,
                                Name = a.Name,
                                No_Of_Registration = a.No_Of_Registration,
                                StartTime = a.StartTime,
                                //StartTimestring = GetDateTimeFormat(b.CampusTourDate, a.StartTime),
                                EndTime = a.EndTime,
                                //EndTimestring = GetDateTimeFormat(b.CampusTourDate, a.EndTime.Value),

                            }).ToList();
                return list;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public IActionResult AdmissionCampusTourSlot(long AdmissionCampusTourDateID)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Mst_CampusTourSlot", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }           
            var list = GetAllCampusSlotList(AdmissionCampusTourDateID);
            ViewBag.AdmissionCampusTourDateID = AdmissionCampusTourDateID;
            return View(list);
        }
        public async Task<IActionResult> DeleteAdmissionCampusTourSlot(int id,long AdmissionCampusTourDateID)
        {
            CheckLoginStatus();
            AdmissionCampusTourSlot type = admissionCampusTourSlotRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            await admissionCampusTourSlotRepository.UpdateAsync(type);
            return RedirectToAction(nameof(AdmissionCampusTourSlot),new { AdmissionCampusTourDateID }).WithSuccess("Campus Tour Slot", "Deleted Successfully");
        }
        public async Task<IActionResult> SaveOrUpdateAdmissionCampusTourSlot(long ID, string Name,string NoOfRegd,TimeSpan StartTime, Nullable<TimeSpan> EndTime, long AdmissionCampusTourDateID)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                AdmissionCampusTourSlot type = admissionCampusTourSlotRepository.GetByIdAsyncIncludeAll(ID).Result;
                if (type != null)
                {                   
                    type.Name = Name;
                    type.No_Of_Registration = NoOfRegd;
                    type.StartTime = StartTime;
                    type.EndTime = EndTime;                   
                    type.ModifiedId = userId;
                    type.ModifiedDate = DateTime.Now;
                    await admissionCampusTourSlotRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(AdmissionCampusTourSlot), new { AdmissionCampusTourDateID }).WithSuccess("Campus Tour Slot", "Updated Successfully");
                }              

            }
            else
            {

                    AdmissionCampusTourSlot type = new AdmissionCampusTourSlot();
                    type.Name = Name;
                    type.No_Of_Registration = NoOfRegd;
                    type.StartTime = StartTime;
                    type.EndTime = EndTime;
                    type.AdmissionCampusTourDateID = AdmissionCampusTourDateID;
                    type.ModifiedId = userId;
                    type.InsertedId = userId;
                    type.InsertedDate = DateTime.Now;
                    type.ModifiedDate = DateTime.Now;
                    type.IsAvailable = true;
                    type.Active = true;
                    type.Status = EntityStatus.ACTIVE;
                    await admissionCampusTourSlotRepository.AddAsync(type);
                    return RedirectToAction(nameof(AdmissionCampusTourSlot), new { AdmissionCampusTourDateID }).WithSuccess("Campus Tour Slot", "Saved Successfully");

                
            }
            return RedirectToAction(nameof(AdmissionCampusTourSlot), new { AdmissionCampusTourDateID });

        }

        #endregion-----------------------------------------------------------------

        #region---------------------Admission Ecounselling Date------------------
        public IActionResult AdmissionEcounsellingDate()
        {

            CheckLoginStatus();

            if (commonMethods.checkaccessavailable("Mst_CampusTourDate", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            ViewBag.SessionList = academicSessionRepository.ListAllAsyncIncludeAll().Result.Where(a => a.IsAdmission == true && a.IsAvailable == true);
            var res = admissionEcounsellingDateRepository.ListAllAsyncIncludeAll().Result.ToList();
            var sessionlist = academicSessionRepository.ListAllAsyncIncludeAll().Result.ToList();
            var Slotlist = admissionEcounsellingSlotRepository.ListAllAsyncIncludeAll().Result.ToList();

            var list = (from a in res
                        join b in sessionlist on a.AcademicSessionId equals b.ID
                        select new AdmissionCampusTourDateModel
                        {
                            ID = a.ID,
                            TourDate = a.EcounsellingDate,
                            TourDatestring = NullableDateTimeToStringDate(a.EcounsellingDate),
                            AcademicSessionName = b.DisplayName,
                            Status = a.Status,
                            SlotCount = Slotlist.Where(z => z.AdmissionEcounsellingDateID == a.ID).ToList().Count,
                        }).ToList();

            return View(list);
        }
        public async Task<IActionResult> DeleteAdmissionEcounsellingDate(int id)
        {
            CheckLoginStatus();
            AdmissionEcounsellingDate type = admissionEcounsellingDateRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            await admissionEcounsellingDateRepository.UpdateAsync(type);
            return RedirectToAction(nameof(AdmissionEcounsellingDate)).WithSuccess("Ecounselling Date", "Deleted Successfully");
        }
        public async Task<IActionResult> SaveOrUpdateAdmissionEcounsellingDate(long ID, DateTime EcounsellingDate, long AcdemicSessionId)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                var namechk = admissionEcounsellingDateRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EcounsellingDate.Date == EcounsellingDate.Date && a.AcademicSessionId == AcdemicSessionId && a.ID != ID).FirstOrDefault();
                if (namechk == null)
                {
                    AdmissionEcounsellingDate type = admissionEcounsellingDateRepository.GetByIdAsyncIncludeAll(ID).Result;
                    type.EcounsellingDate = EcounsellingDate;
                    type.ModifiedId = userId;
                    await admissionEcounsellingDateRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(AdmissionEcounsellingDate)).WithSuccess("Ecounselling Date", "Updated Successfully");
                }
                else
                {
                    return RedirectToAction(nameof(AdmissionEcounsellingDate)).WithDanger("Ecounselling Date", "Name already exist");
                }

            }
            else
            {

                var namechk = admissionEcounsellingDateRepository.ListAllAsyncIncludeAll().Result.Where(a => a.EcounsellingDate.Date == EcounsellingDate.Date && a.AcademicSessionId == AcdemicSessionId).FirstOrDefault();
                if (namechk != null)
                {
                    return RedirectToAction(nameof(AdmissionEcounsellingDate)).WithDanger("Ecounselling Date", "Date already exist...!");
                }
                else
                {

                    AdmissionEcounsellingDate type = new AdmissionEcounsellingDate();
                    type.EcounsellingDate = EcounsellingDate;
                    type.AcademicSessionId = AcdemicSessionId;
                    type.ModifiedId = userId;
                    type.InsertedId = userId;
                    type.InsertedDate = DateTime.Now;
                    type.ModifiedDate = DateTime.Now;
                    type.IsAvailable = true;
                    type.Active = true;
                    type.Status = EntityStatus.ACTIVE;
                    await admissionEcounsellingDateRepository.AddAsync(type);
                    return RedirectToAction(nameof(AdmissionEcounsellingDate)).WithSuccess("Ecounselling Date", "Saved Successfully");

                }
            }
        }


        #endregion----------------------------------------------------------------

        #region---------------------Admission Campus Tour Slot------------------

        public IList<AdmissionCampusTourSlotModel> GetAllEcounsellingSlotList(long AdmissionEcounsellingDateID)
        {
            try
            {
                var Ecounsellinglist = admissionEcounsellingDateRepository.ListAllAsyncIncludeAll().Result.ToList();
                var sessionlist = academicSessionRepository.ListAllAsyncIncludeAll().Result.ToList();
                var Slotlist = admissionEcounsellingSlotRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionEcounsellingDateID == AdmissionEcounsellingDateID).ToList();
                var list = (from a in Slotlist
                            join b in Ecounsellinglist on a.AdmissionEcounsellingDateID equals b.ID
                            join c in sessionlist on b.AcademicSessionId equals c.ID
                            select new AdmissionCampusTourSlotModel
                            {
                                AdmissionEcounsellingDateID = a.AdmissionEcounsellingDateID,
                                ID = a.ID,
                                TourDate = b.EcounsellingDate,
                                TourDatestring = NullableDateTimeToStringDate(b.EcounsellingDate),
                                AcademicSessionName = c.DisplayName,
                                Status = a.Status,
                                Name = a.Name,
                                No_Of_Registration = a.No_Of_Registration,
                                StartTime = a.StartTime,
                                //StartTimestring = GetDateTimeFormat(b.CampusTourDate, a.StartTime),
                                EndTime = a.EndTime,
                                //EndTimestring = GetDateTimeFormat(b.CampusTourDate, a.EndTime.Value),

                            }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IActionResult AdmissionEcounsellingSlot(long AdmissionEcounsellingDateID)
        {
            CheckLoginStatus();
            if (commonMethods.checkaccessavailable("Mst_EcounsellingSlot", accessId, "List", "Admission", roleId) == false)
            {
                return RedirectToAction("AuthenticationFailed", "Accounts");
            }
            var list = GetAllEcounsellingSlotList(AdmissionEcounsellingDateID);
            ViewBag.AdmissionEcounsellingDateID = AdmissionEcounsellingDateID;
            return View(list);
        }
        public async Task<IActionResult> DeleteAdmissionEcounsellingSlot(int id, long AdmissionEcounsellingDateID)
        {
            CheckLoginStatus();
            AdmissionEcounsellingSlot type = admissionEcounsellingSlotRepository.GetByIdAsyncIncludeAll(id).Result;
            type.ModifiedId = userId;
            type.ModifiedDate = DateTime.Now;
            type.Status = EntityStatus.INACTIVE;
            type.IsAvailable = false;
            type.Active = false;
            await admissionEcounsellingSlotRepository.UpdateAsync(type);
            return RedirectToAction(nameof(AdmissionEcounsellingSlot), new { AdmissionEcounsellingDateID }).WithSuccess("Campus Tour Slot", "Deleted Successfully");
        }
        public async Task<IActionResult> SaveOrUpdateAdmissionEcounsellingSlot(long ID, string Name, string NoOfRegd, TimeSpan StartTime, Nullable<TimeSpan> EndTime, long AdmissionEcounsellingDateID)
        {
            CheckLoginStatus();
            if (ID != 0 && ID.ToString() != null)
            {
                AdmissionEcounsellingSlot type = admissionEcounsellingSlotRepository.GetByIdAsyncIncludeAll(ID).Result;
                if (type != null)
                {
                    type.Name = Name;
                    type.No_Of_Registration = NoOfRegd;
                    type.StartTime = StartTime;
                    type.EndTime = EndTime;
                    type.ModifiedId = userId;
                    type.ModifiedDate = DateTime.Now;
                    await admissionEcounsellingSlotRepository.UpdateAsync(type);
                    return RedirectToAction(nameof(AdmissionEcounsellingSlot), new { AdmissionEcounsellingDateID }).WithSuccess("Ecounselling Slot", "Updated Successfully");
                }

            }
            else
            {

                AdmissionEcounsellingSlot type = new AdmissionEcounsellingSlot();
                type.Name = Name;
                type.No_Of_Registration = NoOfRegd;
                type.StartTime = StartTime;
                type.EndTime = EndTime;
                type.AdmissionEcounsellingDateID = AdmissionEcounsellingDateID;
                type.ModifiedId = userId;
                type.InsertedId = userId;
                type.InsertedDate = DateTime.Now;
                type.ModifiedDate = DateTime.Now;
                type.IsAvailable = true;
                type.Active = true;
                type.Status = EntityStatus.ACTIVE;
                await admissionEcounsellingSlotRepository.AddAsync(type);
                return RedirectToAction(nameof(AdmissionEcounsellingSlot), new { AdmissionEcounsellingDateID }).WithSuccess("Ecounselling Slot", "Saved Successfully");


            }
            return RedirectToAction(nameof(AdmissionEcounsellingSlot), new { AdmissionEcounsellingDateID });

        }

        #endregion-----------------------------------------------------------------


    }
}
