﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OdmErp.ApplicationCore.Entities;
using OdmErp.ApplicationCore.Entities.AdmissionAggregate;
using OdmErp.ApplicationCore.Interfaces;
using OdmErp.ApplicationCore.Query;
using OdmErp.Infrastructure.DTO;
using OdmErp.Web.Areas.Admission.Models;
using OdmErp.Web.Areas.Student.Models;
using OdmErp.Web.BillDesk;
using OdmErp.Web.DTO;
using OdmErp.Web.LeadSquared;
using OdmErp.Web.Models;
using OdmErp.Web.SendInBlue;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serialization.Json;
using SelectPdf;
using Web.Controllers;
using static OdmErp.Web.Areas.Admission.Controllers.BasicRegistrationController;
using static OdmErp.Web.LeadSquared.LeadManage;


namespace OdmErp.Web.Areas.Admission.Controllers
{
    [Area("Admission")]
    public class StudentController : AppController
    {
        public IAcademicSessionRepository academicSessionRepository;
        public IAdmissionRepository admissionRepository;
        public IOrganizationRepository organizationRepository;
        public IAcademicStandardRepository academicStandardRepository;
        public IOrganizationAcademicRepository organizationAcademicRepository;
        public IStandardRepository StandardRepository;
        public IBoardRepository boardRepository;
        public IWingRepository wingRepository;

        public IPaymentGatewayRepository pgRepo;
        public IBoardStandardRepository boardStandardRepository;
        public IAdmissionExamDateAssignRepository admissionExamDateAssignRepository;
        public IAcademicStandardWingRepository academicStandardWingRepository;
        public IBoardStandardWingRepository boardStandardWingRepository;
        public IAdmissionStudentRepository admissionStudentRepository;
        public IAdmissionStudentContactRepository admissionStudentContactRepository;
        public IAdmissionStudentAddressRepository admissionStudentAddressRepository;
        public IAdmissionStudentStandardRepository admissionStudentStandardRepository;
        public IAdmissionStudentAcademicRepository admissionStudentAcademicRepository;
        public IPaymentType paymentType;
        public SendInBlueEmailManager sendinBLUE;
        public IAdmissionFeeRepository admissionFeeRepository;
        public IAdmissionFormPaymentRepository admissionFormPaymentRepository;
        public IEmployeeRepository employeeRepository;
        public StudentAdmissionMethod studentAdmissionMethod;
        public CommonMethods commonMethods;
        public IAdmissionStudentCounselorRepository admissionStudentCounselorRepository;
        public IBoardStandardRepository BoardStandardRepository;
        public commonsqlquery commonsqlquery;
        public IPaymentType iPaymentType;
        public IAdmissionStudentExamRepository admissionStudentExamRepository;
        private StudentSqlQuery sqlQuery;
        private IAdmissionCounsellorRepository admissionCounsellorRepository;
        private IAdmissionInterViewerRepository admissionInterViewerRepository;
        public IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository;
        private IAdmissionStudentInterviewRepository admissionStudentInterviewRepository;
        private IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository;
        private IAdmissionDocumentAssignRepository admissionDocumentAssignRepository;
        private IAdmissionStudentDocumentRepository admissionStudentDocumentRepository;
        private IAdmissionVerifierRepository admissionVerifierRepository;
        private IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepository;
        private IAdmissionDocumentRepository admissionDocumentRepository;
        private IAdmissionStandardExamRepository admissionStandardExamRepository;
        public IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepository;
        public IDocumentTypeRepository documentTypeRepository;
        public IDocumentSubTypeRepository documentSubTypeRepository;
        public AdmissionStudentCommonMethod admissionStudentCommonMethod;
        public ICounsellorStatusRepository counsellorStatusRepository;
        public IOtpMessageRepository otpMessageRepository;
        public IAdmitcardDetailsRepository admitcardDetailsRepository;
        public IAdmitcardInstructionsRepository admitcardInstructionsRepository;
        public IAdmitcardVenueRepository admitcardVenueRepository;

        // public IOtpMessages OtpMessages;
        public smssend smsSendData;
        public AdmissionSqlQuery admissionSqlQuery;
        public StudentAdmissionMethod admissionViewModel;
        public PaymentManager pManager;
        public IHostingEnvironment hostingEnv;
        public IConfiguration Configuration { get; }
        public IAdmissionCampusTourDateRepository admissionCampusTourDateRepository;
        public IAdmissionCampusTourSlotRepository admissionCampusTourSlotRepository;
        public IAdmissionEcounsellingDateRepository admissionEcounsellingDateRepository;
        public IAdmissionEcounsellingSlotRepository admissionEcounsellingSlotRepository;
        public IAdmissionEcounsellingRepository admissionEcounsellingRepository;
        public IAdmissionCampusTourRepository admissionCampusTourRepository;
        public IAdmissionCampusTourTimelineRepository admissionCampusTourTimelineRepository;
        public IAdmissionEcounsellingTimelineRepository admissionEcounsellingTimelineRepository;

        public StudentController(IAdmissionCampusTourDateRepository admissionCampusTourDateRepo,
            IAdmissionCampusTourSlotRepository admissionCampusTourSlotRepo,
            IAdmissionEcounsellingDateRepository admissionEcounsellingDateRepo,
            IAdmissionEcounsellingSlotRepository admissionEcounsellingSlotRepo,
            IAdmissionEcounsellingRepository admissionEcounsellingRepo,
            IAdmissionCampusTourRepository admissionCampusTourRepo,
            IAdmissionCampusTourTimelineRepository admissionCampusTourTimelineRepo,
            IAdmissionEcounsellingTimelineRepository admissionEcounsellingTimelineRepo,
            IAdmitcardDetailsRepository admitcardDetailsRepo,
            IAdmitcardInstructionsRepository admitcardInstructionsRepo,
            IAdmitcardVenueRepository admitcardVenueRepo,
            StudentAdmissionMethod admissionView, AdmissionSqlQuery admissionSql, IConfiguration Config, IAcademicSessionRepository academicSessionRepository,
            IAdmissionRepository admissionRepository,
            IOrganizationRepository organizationRepository, IAcademicStandardRepository academicStandardRepository,
            IOrganizationAcademicRepository organizationAcademicRepository, IStandardRepository StandardRepository,
            IWingRepository wingRepository, IBoardStandardRepository boardStandardRepository, IBoardRepository boardRepository,
        IAcademicStandardWingRepository academicStandardWingRepository, IBoardStandardWingRepository boardStandardWingRepository,
        IAdmissionStudentRepository admissionStudentRepository, IAdmissionStudentContactRepository admissionStudentContactRepository,
        IAdmissionStudentAddressRepository admissionStudentAddressRepository, IAdmissionStudentStandardRepository admissionStudentStandardRepository,
        IPaymentType paymentType, IAdmissionFormPaymentRepository admissionFormPaymentRepository, IEmployeeRepository employeeRepository,
        StudentAdmissionMethod studentAdmissionMethod, CommonMethods commonMethods, IAdmissionStudentCounselorRepository admissionStudentCounselorRepository,
        IBoardStandardRepository BoardStandardRepository, IAdmissionStudentCounselorRepository admissionStudentCounsellorRepository,
         IAdmissionCounsellorRepository admissionCounsellorRepository,
           IAdmissionFeeRepository admissionFeeRepository,
           PaymentManager pManager,
           IPaymentType iPaymentType,
        SendInBlueEmailManager sendinBLUE,
        IPaymentGatewayRepository pgRepo,
        IHostingEnvironment hostingEnv,
        commonsqlquery commonsqlquery, IAdmissionStudentExamRepository admissionStudentExamRepository, StudentSqlQuery sqlQuery,
        IAdmissionInterViewerRepository admissionInterViewerRepository, IAdmissionStudentInterviewRepository admissionStudentInterviewRepository,
        IAdmissionPersonalInterviewAssignRepository admissionPersonalInterviewAssignRepository,
        IAdmissionDocumentAssignRepository admissionDocumentAssignRepository, IAdmissionStudentDocumentRepository admissionStudentDocumentRepository,
        IAdmissionVerifierRepository admissionVerifierRepository, IAdmissionDocumentVerificationAssignRepository admissionDocumentVerificationAssignRepositor, IAdmissionStudentAcademicRepository admissionStudentAcademicRepo, IAdmissionDocumentRepository admissionDocumentRepo, IAdmissionStandardExamRepository admissionStandardExamRepo, IAdmissionExamDateAssignRepository admissionExamDateAssignRepo, IAdmissionStandardExamResultDateRepository admissionStandardExamResultDateRepo, IDocumentTypeRepository documentTypeRepo, IDocumentSubTypeRepository documentSubTypeRepo, AdmissionStudentCommonMethod admissionStudentCommon,
        ICounsellorStatusRepository counsellorStatusRepo, smssend smsSend, IOtpMessageRepository otpMessageRepo)
        {
            admissionCampusTourDateRepository = admissionCampusTourDateRepo;
            admissionCampusTourSlotRepository = admissionCampusTourSlotRepo;
            admissionEcounsellingDateRepository = admissionEcounsellingDateRepo;
            admissionEcounsellingSlotRepository = admissionEcounsellingSlotRepo;
            admissionEcounsellingRepository = admissionEcounsellingRepo;
            admissionCampusTourRepository = admissionCampusTourRepo;
            admissionCampusTourTimelineRepository = admissionCampusTourTimelineRepo;
            admissionEcounsellingTimelineRepository = admissionEcounsellingTimelineRepo;

            admitcardDetailsRepository = admitcardDetailsRepo;
            admitcardInstructionsRepository = admitcardInstructionsRepo;
            admitcardVenueRepository = admitcardVenueRepo;

            admissionViewModel = admissionView;
            admissionSqlQuery = admissionSql;
            Configuration = Config;
            this.iPaymentType = iPaymentType;
            this.hostingEnv = hostingEnv;
            this.sendinBLUE = sendinBLUE;
            this.pgRepo = pgRepo;
            this.pManager = pManager;
            this.admissionFeeRepository = admissionFeeRepository;
            this.admissionDocumentAssignRepository = admissionDocumentAssignRepository;
            this.admissionStudentDocumentRepository = admissionStudentDocumentRepository;
            this.admissionCounsellorRepository = admissionCounsellorRepository;
            this.academicSessionRepository = academicSessionRepository;
            this.admissionRepository = admissionRepository;
            this.organizationRepository = organizationRepository;
            this.academicStandardRepository = academicStandardRepository;
            this.organizationAcademicRepository = organizationAcademicRepository;
            this.StandardRepository = StandardRepository;
            this.wingRepository = wingRepository;
            this.boardRepository = boardRepository;
            this.boardStandardRepository = boardStandardRepository;
            this.academicStandardWingRepository = academicStandardWingRepository;
            this.boardStandardWingRepository = boardStandardWingRepository;
            this.admissionStudentRepository = admissionStudentRepository;
            this.admissionStudentContactRepository = admissionStudentContactRepository;
            this.admissionStudentAddressRepository = admissionStudentAddressRepository;
            this.admissionStudentStandardRepository = admissionStudentStandardRepository;
            this.paymentType = paymentType;
            this.admissionFormPaymentRepository = admissionFormPaymentRepository;
            this.employeeRepository = employeeRepository;
            this.studentAdmissionMethod = studentAdmissionMethod;
            this.commonMethods = commonMethods;
            this.admissionStudentCounselorRepository = admissionStudentCounselorRepository;
            this.BoardStandardRepository = BoardStandardRepository;
            this.commonsqlquery = commonsqlquery;
            this.admissionStudentExamRepository = admissionStudentExamRepository;
            this.sqlQuery = sqlQuery;
            this.admissionStudentCounsellorRepository = admissionStudentCounsellorRepository;
            this.admissionInterViewerRepository = admissionInterViewerRepository;
            this.admissionStudentInterviewRepository = admissionStudentInterviewRepository;
            this.admissionPersonalInterviewAssignRepository = admissionPersonalInterviewAssignRepository;
            this.admissionVerifierRepository = admissionVerifierRepository;
            admissionDocumentVerificationAssignRepository = admissionDocumentVerificationAssignRepositor;
            admissionDocumentRepository = admissionDocumentRepo;
            admissionStudentAcademicRepository = admissionStudentAcademicRepo;
            admissionStandardExamRepository = admissionStandardExamRepo;
            admissionExamDateAssignRepository = admissionExamDateAssignRepo;
            admissionStandardExamResultDateRepository = admissionStandardExamResultDateRepo;
            documentTypeRepository = documentTypeRepo;
            documentSubTypeRepository = documentSubTypeRepo;
            admissionStudentCommonMethod = admissionStudentCommon;
            counsellorStatusRepository = counsellorStatusRepo;
            smsSendData = smsSend;
            otpMessageRepository = otpMessageRepo;
        }

        private IActionResult CheckLoginData()
        {
            if (HttpContext != null && HttpContext.Session != null)
            {
                loginId = HttpContext.Session.GetString(LoginSessionData.LOGIN_ID);

                if (loginId == null)
                {
                    return RedirectToAction(nameof(Error));
                }
                else
                {
                    return null;
                }

            }
            else
            {
                return RedirectToAction(nameof(Error));
            }
        }
        private IActionResult CheckSessionData()
        {
            long admId = 0;
            if (HttpContext != null && HttpContext.Session != null)

            {

                var uniquId = HttpContext.Session.
                   GetString(LoginSessionData.UNIQUE_ID);
                if (uniquId == null || uniquId.Length == 0)
                {
                    return RedirectToAction("Expired", "Student");
                }

                // admId = uniquId;
                var admission =
                    admissionRepository.GetAdmissionById(Convert.ToInt64(uniquId));



                if (admission == null)
                {
                    return RedirectToAction("Expired", "Student");
                }
                else
                    return Json(admission.ID);
            }
            else
            {
                return RedirectToAction("Expired", "Student");
            }



        }
        public IActionResult SendMail()
        {

            return View(nameof(Register));
        }
        public IActionResult lOGoUT()
        {
            HttpContext.Session.Clear();
            return RedirectToAction(nameof(Login));
        }
        public IActionResult MobileValidation(string id)
        {


            CheckSessionData();

            var data = HttpContext.Session.
                   GetString(LoginSessionData.OTP_ID);
            string decoded = UtilityModel.Decode(id.ToString());
            ViewBag.Id = decoded;
            ViewBag.OtpId = data ?? 0 + "";
            //ViewBag.Show = rid;

            //if (rid ==10)
            //{
            //    var admission = admissionRepository.GetAdmissionByLeadReferenceId(id);
            //    var std = admissionStudentRepository.GetByAdmissionId(admission.ID).Result;
            //    ViewBag.Mobile = admission.MobileNumber;
            //    ViewBag.Name = std.FirstName +" "+std.LastName;
            //}

            return View();

        }



        private void DeleteSessionData()
        {
            HttpContext.Session.Remove(LoginSessionData.UNIQUE_ID);
            HttpContext.Session.Remove(LoginSessionData.OTP_ID);
            HttpContext.Session.Remove(LoginSessionData.LOGIN_ID);

        }

        public IActionResult Login()
        {
            DeleteSessionData();


            return View();
        }

        public IActionResult Register()
        {
            DeleteSessionData();
            return View();
        }

        public IActionResult Payment(string id)
        {


            // CheckLoginData();
            string decoded = UtilityModel.Decode(id.ToString());
            // ID - LEAD REFERENCEID
            // ViewBag.Id = decoded;



            var model = studentAdmissionMethod.
                GetAdmissionByLeadReferenceId(id, false, true);
            if (model.error)
                return RedirectToAction("Error", "Student");
            else
            {
                var admFee = admissionFeeRepository.GetAdmissionFeeByName("online",
                    model.admissionViewModel.AcademicSessionId).Result;




                model.admissionViewModel.Amount = admFee == null ? 0 + "" : "" + admFee.Ammount;
                long fee = admFee == null ? 0 : admFee.Ammount;



                if (model.admissionViewModel.FormPayment.status == 0)
                {
                    var paymentGateway =
                  pgRepo.GetAllPaymentGateway().Where(a => a.Active == true).FirstOrDefault();

                    if (paymentGateway.ProviderName == "RP")
                    {

                        var d = GetRazorPayOrderId(fee,
                            model.admissionViewModel.AdmissionId,
                            paymentGateway.Host,
                            paymentGateway.HttpMethod, model.admissionViewModel.Mobile,
                            paymentGateway.CheckSum,
                            paymentGateway.MerchantId,
                            paymentGateway.ServerToServerCallback

                            );
                        if (d != null)

                            model.razor = d;
                        else
                        {
                            return RedirectToAction("Error", "Student");
                        }
                    }
                    return View(model);
                }
                else
                {
                    return RedirectToAction("Expired", "Student");
                }


            }
        }




        private RazorPayModel GetRazorPayOrderId(long amount,
            long id, string host, string method, string mobile,
            string secret, string key, string callback)
        {

            //CheckLoginData();

            RazorPayModel model = null;
            try
            {
                string text = Guid.NewGuid().ToString();
                RazorPayOrder order = new RazorPayOrder();
                order.amount = amount * 100;
                order.currency = "INR";
                order.receipt = text;

                var client = new RestClient
                   (host);
                client.Authenticator =
                    new HttpBasicAuthenticator(key, secret);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");

                request.AddJsonBody(order);
                IRestResponse response = client.Execute(request);

                RazorPayOrderRepsonse res =
                    new JsonDeserializer().
                    Deserialize<RazorPayOrderRepsonse>(response);

                if (res != null)
                {

                    PaymentSnapshot snapShot = new PaymentSnapshot();
                    snapShot.PaymentSnapshotId = Guid.NewGuid();
                    snapShot.Active = true;
                    snapShot.UniqueTxnId = res.id;
                    snapShot.TxnAmount = amount + "";
                    snapShot.RequestMsg = text;
                    snapShot.RelatedId = id;
                    snapShot.ResponseMsg = mobile;
                    snapShot.RelatedName = "AdmissionRegistration";
                    snapShot.InsertedDate = DateTime.Now;
                    snapShot.ModifiedDate = DateTime.Now;
                    snapShot.InsertedId = 0;
                    snapShot.ModifiedId = 0;

                    snapShot.TxnType = "20";

                    pgRepo.CreatePaymentSnapshot(snapShot);

                    model = new RazorPayModel();
                    model.key = key;
                    model.amount = amount * 100;
                    model.orderid = res.id;
                    model.callback = callback;


                }

            }
            catch (Exception e)
            {
                model = null;
            }


            return model;



        }
        //private async Task<string> GetRazorPayTransactionId(
        //    long admId,long paymentModeId,
        //    string transId,long sessionId)
        //{
        //    var admFee = await admissionFeeRepository.GetAdmissionFeeByName("online",
        //     sessionId);

        //    AdmissionFormPayment payment = new AdmissionFormPayment();
        //    payment.AdmissionId = admId;
        //    payment.PaymentModeId = paymentModeId;
        //    payment.TransactionId = transId;
        //    payment.AdmissionFeeId = admFee.Ammount;
        //    payment.PaidDate = DateTime.Now;
        //    payment.ReceivedId = 0;
        //    payment.Active = true;
        //    payment.IsAvailable = true;
        //    payment.Status = EntityStatus.ACTIVE;
        //    payment.InsertedDate = DateTime.Now;
        //    payment.ModifiedDate = DateTime.Now;
        //    payment.InsertedId = userId;
        //    payment.ModifiedId = userId;
        //    await admissionFormPaymentRepository.AddAsync(payment);

        //    return null;
        //}


        public IActionResult Dashboard(string id)
        {



            //CheckLoginData();

            var model = studentAdmissionMethod.
                GetAdmissionByLeadReferenceId(id, false, true);

            if (model.error)
                return RedirectToAction("Error", "Student");
            else
            {
                if (model.admissionViewModel.Status == "SUBMITTED")
                {

                    return View(model);
                }
                else
                {
                    return RedirectToAction("Profile", "Registration", new { Areas = "Admission", id = id });
                }
            }
        }

        public async Task<IActionResult> Admitcardpdffile(long id)
        {
            admitcardcls ob = new admitcardcls();
            var ss = admissionSqlQuery.GetView_Student_Admission_List().Where(s => s.ID == id).FirstOrDefault();
            ob.all_Admission = ss;
            ob.GetExamDet = admissionViewModel.GetExamDateDetails(ss.ID,
                                  ss.AcademicStandardWindId.Value,
                                 ss.AdmissionStudentId, "Qualified");
            var det = admitcardDetailsRepository.GetAllAdmitcardDetails().Where(a => a.Active == true).ToList();
            var instructionsModel = admitcardInstructionsRepository.GetAllAdmicardInstructions();
            var admitcardVenue = admitcardVenueRepository.GetAllAdmitcardVenue();
            var sessions = academicSessionRepository.ListAllAsync().
                Result.Where(a => a.Active == true && a.IsAdmission == true).ToList();
            var acdorganisation = organizationAcademicRepository.ListAllAsync().Result;
            var organisation = organizationRepository.GetAllOrganization();
            var res = (from a in det
                       join b in instructionsModel on a.ID equals b.AdmitcardDetailsId
                       join c in admitcardVenue on a.ID equals c.AdmitcardDetailsId
                       join d in acdorganisation on a.OrganisationAcademicId equals d.ID
                       join e in organisation on d.OrganizationId equals e.ID
                       join f in sessions on d.AcademicSessionId equals f.ID
                       where a.OrganisationAcademicId == ss.OrganisationAcademicId
                       select new AdmitcardMastercls
                       {
                           Address = a.Address,
                           ID = a.ID,
                           Instruction = b.Instruction,
                           LogoName = a.LogoName,
                           OrganisationAcademicId = a.OrganisationAcademicId,
                           OrganisationName = c.OrganisationName,
                           Organization = e.Name,
                           sessionid = f.ID,
                           SessionName = f.DisplayName,
                           Title = a.Title,
                           Tollfree = a.Tollfree,
                           VenueAddress = c.Address,
                           Remarks = a.Remarks
                       }).FirstOrDefault();
            ob.admitcard = res;
            return View(ob);
        }

        public async Task<IActionResult> DownloadAdmitcard(long id, long orgid)
        {
            HtmlToPdf converter = new HtmlToPdf();
            var webpath = Configuration.GetValue<string>("domain");
            // create a new pdf document converting an url 
            converter.Options.PdfPageSize = PdfPageSize.A4;
            converter.Options.PdfPageOrientation = PdfPageOrientation.Portrait;
            converter.Options.MarginLeft = 20;
            converter.Options.MarginRight = 20;
            converter.Options.MarginTop = 20;
            converter.Options.MarginBottom = 20;
            converter.Options.JavaScriptEnabled = true;
            converter.Options.CssMediaType = HtmlToPdfCssMediaType.Screen;
            // converter.ConvertUrl(webpath + "Report/Index/" + id);
            converter.Options.DisplayFooter = true;
            converter.Footer.Height = 50;

            var path = hostingEnv.WebRootPath;
            var det = admitcardDetailsRepository.GetAllAdmitcardDetails().Where(a => a.OrganisationAcademicId == orgid && a.Active == true).FirstOrDefault();
            if (det.Remarks != "")
            {
                PdfTextSection pdfText = new PdfTextSection(0, 10, "Remarks : " + det.Remarks, new System.Drawing.Font("Arial", 12));
                converter.Footer.Add(pdfText);
            }
            // var sbody = "<html><head></head><body>"+body+"</body></html>";
            PdfDocument doc = converter.ConvertUrl(webpath + "/Admission/Student/Admitcardpdffile/" + id);
            // save pdf document 
            byte[] pdf = doc.Save();
            // close pdf document 
            doc.Close();
            Stream stream = new MemoryStream(pdf);

            return File(stream, "application/pdf", $"Admitcard.pdf");

        }

        //public IActionResult CreateRegistration(string id)
        //{




        //    long idval = Convert.ToInt64(id);
        //    ViewBag.admissionId = idval;
        //    var getstudentList = admissionStudentRepository.ListAllAsyncIncludeAll().Result;
        //    var studentid = getstudentList.Where(a => a.AdmissionId == idval).FirstOrDefault().ID.ToString();
        //    var sidval = "sid_" + studentid.ToString();

        //    return View();
        //}


        public IActionResult Error()
        {
            ErrorDataViewModel model = new ErrorDataViewModel();
            model.error = "Something went wrong";
            return View(model);
        }
        public IActionResult Expired()
        {
            ErrorDataViewModel model = new ErrorDataViewModel();
            model.error = "Session expired";
            return View(model);
        }





        #region BD



        #endregion


        #region
        [HttpPost]
        public async Task<IActionResult> SaveOrUpdateStudentWiseExamById(long AdmissionStandardExamId)
        {
            AdmissionLoginStatus();
            long AdmissionStudentId = Convert.ToInt64(HttpContext.Request.Form["AdmissionStudentId"]);
            ViewBag.AdmissionId = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            long admissionid = Convert.ToInt64(HttpContext.Request.Form["AdmissionId"]);
            Guid leadreferenceid = admissionRepository.GetAllAdmission().Where(a => a.ID == admissionid).FirstOrDefault().LeadReferenceId;
            var exam = admissionExamDateAssignRepository.ListAllAsyncIncludeAll().Result;
            if (AdmissionStandardExamId != 0)
            {
                var list = exam.Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();
                if (list == null)
                {
                    AdmissionExamDateAssign assign = new AdmissionExamDateAssign();
                    assign.AdmissionStandardExamId = AdmissionStandardExamId;
                    assign.AdmissionStudentId = AdmissionStudentId;
                    assign.ModifiedId = userId;
                    assign.ModifiedDate = DateTime.Now;
                    assign.InsertedDate = DateTime.Now;
                    assign.Active = true;
                    assign.Status = EntityStatus.ACTIVE;
                    assign.IsAvailable = true;
                    assign.RollNo = exam.Count == 0 ? 2021001 : (2021001 + exam.LastOrDefault().ID);
                    await admissionExamDateAssignRepository.AddAsync(assign);
                    return RedirectToAction("Dashboard", "Student", new { id = leadreferenceid }).WithSuccess("Student Exam", "Saved Successfully");


                }
                else
                {
                    AdmissionExamDateAssign assign1 = admissionExamDateAssignRepository.ListAllAsyncIncludeAll().Result.Where(a => a.AdmissionStudentId == AdmissionStudentId).FirstOrDefault();
                    assign1.AdmissionStandardExamId = AdmissionStandardExamId;
                    assign1.AdmissionStudentId = AdmissionStudentId;
                    assign1.ModifiedDate = DateTime.Now;
                    assign1.Active = true;
                    assign1.Status = EntityStatus.ACTIVE;
                    assign1.IsAvailable = true;
                    await admissionExamDateAssignRepository.UpdateAsync(assign1);
                    return RedirectToAction("Dashboard", "Student", new { id = leadreferenceid }).WithSuccess("Student Exam", "Updated Successfully");


                }

            }

            return RedirectToAction("Dashboard", "Student", new { id = leadreferenceid });
        }


        #endregion


        //[HttpGet]
        //public IActionResult DownloadReceiptCard(string id)
        //{
        //    AdmitCardGenerator card = new AdmitCardGenerator();

        //    AdmitCardModel model = GetAdmitCardModel(id, null);

        //    return File(card.GenerateReceiptCard(model),
        //        "application/pdf", $"Receipt_" + DateTime.Now.Millisecond + ".pdf");
        //}
        #region Send Receipt
        public async Task<IActionResult> SendReceipt(long id)
        {
            var form = admissionFormPaymentRepository.
           GetByAdmissionId(id).Result;
            var fee = admissionFeeRepository.
                GetByIdAsync(form.AdmissionFeeId).Result;
            var admData = admissionSqlQuery.GetView_Student_Admission_List().Where(a => a.ID == id).FirstOrDefault();




            try
            {
                System.Uri address = new System.Uri("http://tinyurl.com/api-create.php?url=" +
              "http://tracq.odmps.org/Admission//Student/DownloadReceipt/" + admData.LeadReferenceId);
                System.Net.WebClient client = new System.Net.WebClient();
                string tinyUrl = client.DownloadString(address);

                Thread t1 = new Thread(delegate ()
                {
                    smsSendData.SendPaymentSms(
                      admData.Fullname, admData.Mobile,
                     fee.Ammount.ToString(), form.PaidDate.ToString("dd-MMM-yyyy hh:mm tt"), tinyUrl);


                });
                t1.Start();

                sendinBLUE.SendPaymentMail(
                    admData.AcademicSessionName, admData.OrganisationName, admData.BoardName, admData.StandardName,
                    admData.WingName, admData.Fullname, admData.Mobile,
                    form.TransactionId, admData.PaymentModeName, form.PaidDate.ToString("dd-MMM-yyyy hh:mm tt"), fee.Ammount.ToString(), tinyUrl,
                    admData.EmergencyEmail, admData.EmergencyEmail, admData.Fullname
                    );
            }
            catch
            {

            }


            return RedirectToAction("ViewAdmissionDetails", "Counseller", new { Areas = "Admission",id=admData.LeadReferenceId });
        }
        #endregion
        #region Download Receipt
        public IActionResult DownloadReceipt(string id)
        {
            AdmitCardGenerator card = new AdmitCardGenerator();

            AdmitCardModel model = GetAdmitCardModel(id, null);

            return File(card.GenerateReceiptCard(model),
                "application/pdf", $"Receipt_" + id + "" + DateTime.Now.Millisecond + ".pdf");
        }
        public AdmitCardModel GetAdmitCardModel(string admissionId, string path)
        {
            var admission = admissionRepository.
                GetAdmissionByLeadReferenceId(admissionId);
            var sessionval = academicSessionRepository.
                GetByIdAsync(admission.AcademicSessionId).Result.DisplayName;
            var admStudent = admissionRepository.
                GetAdmissionStudentById(admission.ID);
            var admConctact = admissionStudentContactRepository.GetStudentById
                (admStudent.ID).Result;


            var form = admissionFormPaymentRepository.
                GetByAdmissionId(admission.ID).Result;



            var fee = admissionFeeRepository.
                GetByIdAsync(form.AdmissionFeeId).Result;

            string fornNumber = null;
            if (form != null && admission.IsAdmissionFormAmountPaid)
            {
                if (admission.FormNumber == null)
                {
                    fornNumber = UtilityModel.GenerateFormNumber(form.PaidDate);
                    admission.FormNumber = fornNumber;
                    admissionRepository.UpdateAdmission(admission);
                }
                else
                {
                    fornNumber = admission.FormNumber;
                }
            }


            int startIndex = admission.MobileNumber.Length - 4;

            //  int endIndex = admission.MobileNumber.Length-1;

            //string fname = admStudent.FirstName.
            //    Substring(
            //        0, 3
            //        );
            //string mobile = admission.MobileNumber.Substring(
            //       startIndex, 4
            //         );

            AdmitCardModel model = new AdmitCardModel();
            model.pdfFilePath = path;
            //model.username = Encoding.ASCII.
            //    GetBytes(fname.ToUpper() + "" + mobile);
            var admData = admissionStudentCommonMethod.GetStudentIdData(
                       admStudent.ID);

            var org = organizationRepository.GetOrganizationById(admData.
               organizationId);
            var standard = StandardRepository.GetStandardById(admData.
             standardId);
            var wing = wingRepository.GetByIdAsync(admData.
             wingId).Result;

            var board = boardRepository.GetBoardById(admData.
                boardId);
            AdmitCardHeaderModel hmodel = new AdmitCardHeaderModel();

            var webPath = hostingEnv.WebRootPath;
            // var fileName = System.IO.Path.GetFileName("AdmitCard_" + admStudentContact.Mobile + ".pdf");
            string logoPath = System.IO.Path.Combine("", webPath + @"/images/odm_logo.png");

            hmodel.logoPath = logoPath;
            // hmodel.studentPath = studentPath;
            hmodel.organization = "ODM PUBLIC SCHOOL";
            hmodel.address = "Sishu Vihar,Infocity Road,\nPatia,Bhubaneswar - 751024";

            hmodel.tollfreeNo = " Phone Number - 1800-120-2316";
            hmodel.session = "Admission for the Session " + sessionval;
            model.headerModel = hmodel;


            #region AdmissionDetails

            AdmitCardStudentModel smodel = new AdmitCardStudentModel();
            smodel.title = "Registration Details";
            smodel.data = new List<List<string>>();
            string name = UtilityModel.GetFullName(
                admStudent.FirstName, admStudent.MiddleName, admStudent.LastName);

            List<string> data1 = new List<string>();
            data1.Add("Application No:" + fornNumber);

            smodel.data.Add(data1);

            List<string> data2 = new List<string>();
            data2.Add("Student Name:" + name);
            data2.Add("Mobile:" + admission.MobileNumber);
            smodel.data.Add(data2);

            List<string> data21 = new List<string>();
            data21.Add("Parent Name:" + admConctact.FullName);
            data21.Add("Email:" + admConctact.EmailId);
            smodel.data.Add(data21);

            List<string> data6 = new List<string>();
            if (org != null)
                data6.Add("School:" + org.Name);
            if (board != null)
                data6.Add("Board:" + board.Name);
            smodel.data.Add(data6);
            List<string> data9 = new List<string>();
            if (standard != null)
                data9.Add("Class:" + standard.Name);
            if (wing != null)
                data9.Add("Boarding Type:" + wing.Name);
            smodel.data.Add(data9);

            List<string> data31 = new List<string>();
            data31.Add("Date of Registration:" + admission.InsertedDate.ToString("dd-MMM-yyyy"));
            smodel.data.Add(data31);


            model.studentModel = smodel;

            #endregion

            #region FormDetails



            var mode = iPaymentType.
                GetPaymentTypeById(form.PaymentModeId);

            AdmitCardStudentModel fmodel = new AdmitCardStudentModel();
            fmodel.title = "Payment Details";
            fmodel.data = new List<List<string>>();

            List<string> data11 = new List<string>();
            data11.Add("Transaction Id:" + form.TransactionId);
            data11.Add("Payment Mode:" + mode.Name);
            fmodel.data.Add(data11);

            List<string> data111 = new List<string>();
            data111.Add("Amount (INR):" + fee.Ammount);
            data111.Add("Paid Date:" + form.PaidDate.ToString("dd-MMM-yyyy"));
            fmodel.data.Add(data111);


            model.formModel = fmodel;

            #endregion

            return model;


        }
        //public (string boardName, string className,
        // string schoolName, string wingName,
        // long boardId,
        // long standardId,
        //  long schoolId,
        // long wingId
        //) GetAdmissionClassDetails(long acwingId)
        //{
        //    var cadeStWing = academicStandardWingRepository.GetByIdAsync(acwingId).Result;
        //    var acStd = cadeStWing != null ?
        //        academicStandardRepository.GetByIdAsync(cadeStWing.AcademicStandardId).Result : null;
        //    var orgAcd = acStd != null ?
        //        organizationAcademicRepository.GetByIdAsync(acStd.OrganizationAcademicId).Result : null;
        //    var org = orgAcd != null ?
        //        organizationRepository.GetOrganizationById(orgAcd.OrganizationId) : null;
        //    var stdBoard = org != null ?
        //        boardStandardRepository.GetByIdAsync(acStd.BoardStandardId).Result : null;
        //    var wing = stdBoard != null ?
        //        wingRepository.GetByIdAsync(cadeStWing.WingID.Value).Result : null;
        //    var bstd = wing != null ?
        //        boardRepository.GetBoardById(stdBoard.BoardId) : null;
        //    var standard = bstd != null ?
        //        StandardRepository.GetStandardById(stdBoard.StandardId) : null;

        //    return (bstd != null ? bstd.Name : "NA", standard != null ? standard.Name : "NA",
        //        org != null ? org.Name : "NA", wing != null ? wing.Name : "NA",
        //       bstd != null ? bstd.ID : 0, standard != null ? standard.ID : 0,
        //       org != null ? org.ID : 0, wing != null ? wing.ID : 0);
        //}
        #endregion

        #region E-Counselling & Campus Tour
        public JsonResult GetEcounsellingDateList()
        {
            var session = academicSessionRepository.ListAllAsync().Result.Where(a => a.IsAdmission == true && a.Active == true).Select(a => a.ID).ToList();
            var counsellingdate = admissionEcounsellingDateRepository.ListAllAsync().Result.Where(a => a.Active == true && session.Contains(a.AcademicSessionId)).ToList();
            var res = (from a in counsellingdate
                       select new
                       {
                           id = a.ID,
                           name = a.EcounsellingDate.ToString("dd MMM yyyy")
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetEcounsellingSlotsList(long dateid)
        {
            var counsellingdate = admissionEcounsellingDateRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == dateid).ToList();
            var counsellingslots = admissionEcounsellingSlotRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AdmissionEcounsellingDateID==dateid).ToList();
           var counsellingcounts=admissionEcounsellingRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AdmissionEcounsellingDateId == dateid).ToList();
            var res = (from a in counsellingslots
                       where Convert.ToInt32(a.No_Of_Registration)> (counsellingcounts.Count==0?0:counsellingcounts.Where(m=>m.AdmissionEcounsellingSlotId==a.ID).ToList().Count)
                       select new
                       {
                           id = a.ID,
                           name=a.Name
                          // name = DateTime.ParseExact(a.StartTime.ToString(), "HH:mm", null).ToString("hh:mm tt")+"-"+ DateTime.ParseExact(a.EndTime.ToString(), "HH:mm", null).ToString("hh:mm tt")
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetCampusTourDateList()
        {
            var session = academicSessionRepository.ListAllAsync().Result.Where(a => a.IsAdmission == true && a.Active == true).Select(a => a.ID).ToList();
            var campustourdate = admissionCampusTourDateRepository.ListAllAsync().Result.Where(a => a.Active == true && session.Contains(a.AcademicSessionId)).ToList();
            var res = (from a in campustourdate
                       select new
                       {
                           id = a.ID,
                           name = a.CampusTourDate.ToString("dd MMM yyyy")
                       }).ToList();
            return Json(res);
        }
        public JsonResult GetCampustourSlotsList(long dateid)
        {
            var counsellingdate = admissionCampusTourDateRepository.ListAllAsync().Result.Where(a => a.Active == true && a.ID == dateid).ToList();
            var counsellingslots = admissionCampusTourSlotRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AdmissionCampusTourDateID == dateid).ToList();
            var counsellingcounts = admissionCampusTourRepository.ListAllAsync().Result.Where(a => a.Active == true && a.AdmissionCampusTourDateId == dateid).ToList();
            var res = (from a in counsellingslots
                       where Convert.ToInt32(a.No_Of_Registration) > (counsellingcounts.Count == 0 ? 0 : counsellingcounts.Where(m => m.AdmissionCampusTourSlotId == a.ID).ToList().Count)
                       select new
                       {
                           id = a.ID,
                           name = a.Name
                       }).ToList();
            return Json(res);
        }
        #endregion
    }
}
