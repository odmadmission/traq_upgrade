﻿using RestSharp;
using RestSharp.Serialization.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace OdmErp.Web.LeadSquared
{
    public class LeadManage
    {
        public LeadResponse createLead(List<LeadDto> dto)
        {
            var client = new RestClient
                (LeadConstants.URL + "LeadManagement.svc/Lead.Capture?accessKey=" + 
                LeadConstants.AccessKey + "&secretKey=" + LeadConstants.SecretKey);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(dto);
            IRestResponse response = client.Execute(request);
             response.StatusCode.ToString();
            
            return new JsonDeserializer().Deserialize<LeadResponse>(response);
        }

        public LeadResponse updateLead(List<LeadDto> dto)
        {
          
            var client = new RestClient
                (LeadConstants.URL + "LeadManagement.svc/Lead.Capture?accessKey=" +
                LeadConstants.AccessKey + "&secretKey=" + LeadConstants.SecretKey);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(dto);
            IRestResponse response = client.Execute(request);
            response.StatusCode.ToString();

            return new JsonDeserializer().Deserialize<LeadResponse>(response);

            //            var client = new RestClient
            //                (LeadConstants.URL + "LeadManagement.svc/Lead.Update?accessKey=" +
            //                LeadConstants.AccessKey + "&secretKey=" + LeadConstants.SecretKey+ 
            //                "&leadId="+LeadId);
            //            var request = new RestRequest(Method.POST);
            //            request.AddHeader("Content-Type", "application/json");
            ////request.AddJsonBody(dto);
            //            IRestResponse response = client.Execute(request);
            //            response.StatusCode.ToString();

            //            return new JsonDeserializer().Deserialize<LeadResponse>(response);
        }

        public LeadResponse updateLead(List<LeadDto> dto, string LeadId)
        {
            LeadDto obj = new LeadDto();
            obj.Attribute = "Phone";
            obj.Value =LeadId;

            dto.Add(obj);
            var client = new RestClient
                (LeadConstants.URL + "LeadManagement.svc/Lead.Capture?accessKey=" +
                LeadConstants.AccessKey + "&secretKey=" + LeadConstants.SecretKey);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(dto);
            IRestResponse response = client.Execute(request);
            response.StatusCode.ToString();

            return new JsonDeserializer().Deserialize<LeadResponse>(response);

//            var client = new RestClient
//                (LeadConstants.URL + "LeadManagement.svc/Lead.Update?accessKey=" +
//                LeadConstants.AccessKey + "&secretKey=" + LeadConstants.SecretKey+ 
//                "&leadId="+LeadId);
//            var request = new RestRequest(Method.POST);
//            request.AddHeader("Content-Type", "application/json");
////request.AddJsonBody(dto);
//            IRestResponse response = client.Execute(request);
//            response.StatusCode.ToString();

//            return new JsonDeserializer().Deserialize<LeadResponse>(response);
        }

        public class LeadResponse
        {
            public string Status { get; set; }
            public LeadMessage Message { get; set; }
           
        }

        public class LeadMessage
        {
            public string Id { get; set; }
            public string RelatedId { get; set; }
            public string IsCreated { get; set; }
            public int AffectedRows { get; set; }
        }
            public List<LeadDto> CreateLeadData(LeadCreateDto dto)
        {
            List<LeadDto> list = new List<LeadDto>();

            LeadDto obj = new LeadDto();
            obj.Attribute = "FirstName";
            obj.Value = dto.FirstName;

            list.Add(obj);

            LeadDto obj2 = new LeadDto();
            obj2.Attribute = "LastName";
            obj2.Value = dto.LastName;
            list.Add(obj2);
            LeadDto obj3 = new LeadDto();
            obj3.Attribute = "mx_EmailId";
            obj3.Value = dto.EmailAddress;
            list.Add(obj3);
            LeadDto obj4 = new LeadDto();
            obj4.Attribute = "Phone";
            obj4.Value = dto.Phone;
            list.Add(obj4);
            LeadDto obj5 = new LeadDto();
            obj5.Attribute = "mx_Id";
            obj5.Value = dto.AdmissionId + "";
            list.Add(obj5);
            LeadDto obj6 = new LeadDto();
            obj6.Attribute = "mx_Source";
            obj6.Value = dto.Source;
            list.Add(obj6);
            LeadDto obj7 = new LeadDto();
            obj7.Attribute = "mx_Class";
            obj7.Value = dto.ClassName;
            list.Add(obj7);
            LeadDto obj8 = new LeadDto();
            obj8.Attribute = "mx_School";
            obj8.Value = dto.SchoolName;
            list.Add(obj8);

            LeadDto obj9 = new LeadDto();
            obj9.Attribute = "mx_Status";
            obj9.Value = dto.Status;
            list.Add(obj9);

            LeadDto obj10 = new LeadDto();
            obj10.Attribute = "mx_Wing";
            obj10.Value = dto.WingName;
            list.Add(obj10);

            LeadDto obj11 = new LeadDto();
            obj11.Attribute = "mx_RegisteredOn";
            obj11.Value = dto.CreatedOn;
            list.Add(obj11);

            LeadDto obj12 = new LeadDto();
            obj12.Attribute = "mx_Contact_Name";
            obj12.Value = dto.Name;
            list.Add(obj12);

            LeadDto obj13 = new LeadDto();
            obj13.Attribute = "mx_PaidOn";
            obj13.Value = "NA";
            list.Add(obj13);
            

                 LeadDto obj14 = new LeadDto();
            obj14.Attribute = "SearchBy";
            obj14.Value = "Phone";
            list.Add(obj14);

            return list;
        }

        public List<LeadDto> UpdateLeadData(LeadCreateDto dto)
        {
            List<LeadDto> list = new List<LeadDto>();

            LeadDto obj = new LeadDto();
            obj.Attribute = "mx_Status";
            obj.Value = dto.Status;

            list.Add(obj);

            LeadDto obj2 = new LeadDto();
            obj2.Attribute = "mx_PaidOn";
            obj2.Value = dto.PaidOn;
            list.Add(obj2);

            LeadDto obj14 = new LeadDto();
            obj14.Attribute = "SearchBy";
            obj14.Value = "Phone";
            list.Add(obj14);
            return list;
        }

        public class LeadDto
        {
            public string Attribute { get; set; }
            public string Value { get; set; }
        }

        public static class LeadConstants
        {
            public static string ASYNC_API_KEY = "GXt8SUevQUaLUsr24MwHR9GBJ2x1kenq7IpVQNgI";
            public static string ASYNC_API_URL = "https://asyncapi-in21.leadsquared.com";
            public static string URL = "https://api-in21.leadsquared.com/v2/";
            public static string AccessKey = "u$r977acc995b90a8ca56142c4dcd9f9dda";
            public static string SecretKey = "06828afe389781ed36c7665a0dcfd4f5da817dd0";

        }

        public class LeadCreateDto
        {
            public string Source { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Name { get; set; }
            public string EmailAddress { get; set; }
            public string Phone { get; set; }
            public string SchoolName { get; set; }
            public string ClassName { get; set; }
            public string WingName { get; set; }
            public string City { get; set; }
            public string Status { get; set; }

            public string AdmissionId { get; set; }
            public string CreatedOn { get; set; }
            public string PaidOn { get; set; }
        }
    }
}
