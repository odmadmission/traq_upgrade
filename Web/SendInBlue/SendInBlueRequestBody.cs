﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OdmErp.Web.SendInBlue
{
    public class SendInBlueRequestBody
    { 
        public string subject { get; set; }
        public string htmlContent { get; set; }
        public SendInBlueReceiverSender replyTo { get; set; }
        public SendInBlueReceiverSender sender { get; set; }
        public List<SendInBlueReceiverSender> to { get; set; }

        public SendInBlueRequestBody(string html, List<SendInBlueReceiverSender> to)
        {
            this.htmlContent = html;
            this.to = to;
        }
    }

    public class SendInBlueReceiverSender
    {

        public string name { get; set; }
        public string email { get; set; }
       

    }
}
